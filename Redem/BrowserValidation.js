/**
 *@NApiVersion 2.x
 *@NScriptType ClientScript
 *@NModuleScope SameAccount
 */
define(['N/ui/message'], function(msg) {

    function pageInit(context) {
    
        if (validate()) {

            showMessage(msg);
        }

        return;
    }

    function saveRecord(context) {
        
        if (validate()) {

            showMessage(msg);

            return false;
        }

    }

    function validate(){

        var result = false;

        if(isIE9 || isIE10 || isIE11)
        {
            result = true;
        }

        return result;
    }

    function showMessage(objMsg){
        var myMsg = objMsg.create({
            title: "Invalid Browser",
            message: "Please use other browser. Recommended browsers are Google Chrome/Firefox.",
            type: objMsg.Type.ERROR
        }).show({
            duration: 0
        });
    }

    return {
        pageInit: pageInit,
        saveRecord: saveRecord
    }
});
