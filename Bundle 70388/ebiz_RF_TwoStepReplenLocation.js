/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplenLocation.js,v $
 *     	   $Revision: 1.1.2.1.2.1 $
 *     	   $Date: 2015/11/27 15:24:30 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplenLocation.js,v $
 * Revision 1.1.2.1.2.1  2015/11/27 15:24:30  aanchal
 * 2015.2 Issue Fix
 * 201415733
 *
 * Revision 1.1.2.1  2015/01/02 15:05:16  skreddy
 * Case# 201411349
 * Two step replen process
 *
 *****************************************************************************/

function Replenishment_Location(request, response){
	if (request.getMethod() == 'GET') {
		var reportNo = request.getParameter('custparam_repno');
		var zoneno = request.getParameter('custparam_zoneno');
		var cartlpno = request.getParameter('custparam_cartlpno');
		var html=getLocationScan(reportNo,zoneno,getLanguage,cartlpno);
		response.write(html);
	}
	else {
		try {
			var Reparray = new Array();

			var getLanguage = request.getParameter('hdngetLanguage');
			Reparray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);
			nlapiLogExecution('ERROR', 'hdnReplenType', request.getParameter('hdnReplenType'));

			var st8,st9;
			if( getLanguage == 'es_ES' || getLanguage =='es_AR')
			{

				st8 = "UBICACI&#211;N NO COINCIDEN";
				st9 = "LA UBICACI&#211;N ES NULL";
			}
			else
			{
				st8 = "LOCATION DO NOT MATCH";
				st9 = "PLEASE ENTER LOCATION";
			}

			var optedEvent = request.getParameter('cmdPrevious');
			var optedSkip=request.getParameter('cmdSkip');

			var getNumber = request.getParameter('hdngetnumber');
			nlapiLogExecution('Error', 'in response getNumber', getNumber);

			var fromLoc = request.getParameter('enterfromloc');
			var hdnBeginLocation = request.getParameter('hdnBeginLocation');			
			var reportNo = request.getParameter('hdnNo');	
			var beginLocID = request.getParameter('hdnbeginLocId');
			var zoneID = request.getParameter('hdnZoneId');
			var toloc = request.getParameter('hdnactendlocation');
			nlapiLogExecution('ERROR', 'toloc', toloc);
		
			nlapiLogExecution('ERROR', 'reportNo', reportNo);
			nlapiLogExecution('ERROR', 'hdnBeginLocation', hdnBeginLocation);
			nlapiLogExecution('ERROR', 'zoneID', zoneID);
			nlapiLogExecution('ERROR', 'toloc', toloc);
			nlapiLogExecution('ERROR', 'fromLoc', fromLoc);
			Reparray["custparam_screenno"] = '2stepRpLoc';
			
			Reparray["custparam_repno"] = request.getParameter('hdnNo');
			Reparray["custparam_repbegloc"] = fromLoc;
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
			Reparray["custparam_repsku"] = request.getParameter('hdnSku');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnexpQty');
			
			Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
		
			Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnactendlocationid');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnactendlocation');
			Reparray["custparam_zoneno"] = request.getParameter('hdnZoneId');
			
			Reparray["custparam_cartlpno"] = request.getParameter('hdnCartlpno');
			nlapiLogExecution('ERROR', 'hdnCartlpno', request.getParameter('hdnCartlpno'));  

			nlapiLogExecution('ERROR', 'test2', 'test2');  
			nlapiLogExecution('ERROR', 'ot internal which is being processed further', Reparray["custparam_repid"]);  

			if (optedEvent == 'F7') {
				nlapiLogExecution('ERROR', 'REPLENISHMENT LOCATION F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplencartscan', 'customdeploy_ebiz_twostepreplencartscan', false, Reparray);
			}
			else if(optedSkip=='F6')
			{
				var itemfilters = new Array();		
				if(reportNo != null && reportNo != "" && reportNo !='null')
				{
					itemfilters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
				}
				if(zoneID != null && zoneID != "" && zoneID !='null')
				{
					itemfilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', zoneID));
				}
				if(beginLocID != null && beginLocID != "")
				{
					itemfilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', beginLocID));
				}
				itemfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
				itemfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_skiptask');
				

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, itemfilters, columns);
				if (searchresults != null) 
				{
			      for(var m=0;m< searchresults.length;m++)
			      {
			    	  var skipCount = searchresults[m].getValue('custrecord_skiptask');
			    	  nlapiLogExecution('ERROR', 'skipCount', skipCount);  
			    	  if(skipCount == null || skipCount == '' || skipCount == 'null')
			    	  {
			    		  skipCount = 0;
			    	  }

			    	  skipCount = parseInt(skipCount) + 1;
			    	  nlapiSubmitField('customrecord_ebiznet_trn_opentask', searchresults[m].getId(), 'custrecord_skiptask', skipCount)

			      }
				response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
				}
				
			}
			else {
				
				
				

				if(toloc!=null && toloc!='' && toloc!='null')
				{
					var locationvalid=getLocation(toloc);
					nlapiLogExecution('ERROR', 'locationvalid', locationvalid);
					if(locationvalid==false)
					{
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
						var expectedqty = transaction.getFieldValue('custrecord_expe_qty');
						var Invrecid = transaction.getFieldValue('custrecord_invref_no');
						transaction.setFieldValue('custrecord_wms_status_flag', 32);
						transaction.setFieldValue('custrecord_act_qty', expectedqty);
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());					

						var result = nlapiSubmitRecord(transaction);

						var invtrecord=nlapiLoadRecord('customrecord_ebiznet_createinv', Invrecid);
						var Invallocqty = invtrecord.getFieldValue('custrecord_ebiz_alloc_qty');
						nlapiLogExecution('ERROR', 'Invallocqty',Invallocqty);
						nlapiLogExecution('ERROR', 'expectedqty',expectedqty);
						if((Invallocqty!=null && Invallocqty!='' && Invallocqty>0) && (expectedqty!=null && expectedqty!='' && expectedqty>0))
							invtrecord.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(Invallocqty)- parseInt(expectedqty));

						var invtrecid = nlapiSubmitRecord(invtrecord, false, true);

						Reparray["custparam_error"] = 'END LOCATION IS INACTIVE STATUS';
						Reparray["custparam_screenno"] = '2stepRpLoc';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						nlapiLogExecution('ERROR', 'Location is null');
						return;
					}
				}

				if (fromLoc != '') {
					if (fromLoc == hdnBeginLocation) {

						var fromlocationvalid=getfromLocation(fromLoc);
						updateTaskBeginTime(reportNo,zoneID,beginLocID);
						nlapiLogExecution('ERROR', 'Location Found and Matched', fromLoc);  
						

						if(fromlocationvalid==false)
						{
							var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
							var expectedqty = transaction.getFieldValue('custrecord_expe_qty');
							var Invrecid = transaction.getFieldValue('custrecord_invref_no');
							transaction.setFieldValue('custrecord_wms_status_flag', 32);
							transaction.setFieldValue('custrecord_act_qty', expectedqty);
							transaction.setFieldValue('custrecord_act_end_date', DateStamp());		
							var result = nlapiSubmitRecord(transaction);

							var invtrecord=nlapiLoadRecord('customrecord_ebiznet_createinv', Invrecid);
							var Invallocqty = invtrecord.getFieldValue('custrecord_ebiz_alloc_qty');
							nlapiLogExecution('ERROR', 'Invallocqty',Invallocqty);
							nlapiLogExecution('ERROR', 'expectedqty',expectedqty);
							if((Invallocqty!=null && Invallocqty!='' && Invallocqty>0) && (expectedqty!=null && expectedqty!='' && expectedqty>0))
								invtrecord.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(Invallocqty)- parseInt(expectedqty));

							var invtrecid = nlapiSubmitRecord(invtrecord, false, true);

							Reparray["custparam_error"] = 'FROM LOCATION IS INACTIVE STATUS';
							Reparray["custparam_screenno"] = 'R1';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
							nlapiLogExecution('ERROR', 'Location is null');
							return;
						}
						else
						{
							Reparray["custparam_number"] = getNumber;
							response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenitem', 'customdeploy_ebiz_twostepreplenitem', false, Reparray);
						}		
					}
					else {
						Reparray["custparam_error"] = 'LOCATION DO NOT MATCH';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						nlapiLogExecution('ERROR', 'Location not Matched');
					}
				}
				else {
					Reparray["custparam_error"] = 'LOCATION IS NULL';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					nlapiLogExecution('ERROR', 'Location is null');
				}
			}
		} 
		catch (e) {
			nlapiLogExecution('ERROR', 'exception',e);
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			nlapiLogExecution('ERROR', 'Catch: Location not found');
		}
	}
}




function getLocation(gettoloc)
{
	var validlocation=false;
	nlapiLogExecution('ERROR', 'gettoloc', gettoloc);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', gettoloc);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns=new Array();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

	if(searchresults!=null && searchresults!='')
		validlocation=true;

	nlapiLogExecution('ERROR', 'validlocation', validlocation);

	return validlocation;

}

function getfromLocation(fromLoc)
{
	var fromvalidlocation=false;
	nlapiLogExecution('ERROR', 'fromLoc', fromLoc);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', fromLoc);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns=new Array();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

	if(searchresults!=null && searchresults!='')
		fromvalidlocation=true;

	nlapiLogExecution('ERROR', 'fromvalidlocation', fromvalidlocation);

	return fromvalidlocation;
}

function getLocationScan(reportNo,zoneNo,getLanguage,cartLPNo)
{
	var total;

	nlapiLogExecution('ERROR', 'reportNo',reportNo);	
	nlapiLogExecution('ERROR', 'zoneNo',zoneNo);	


	var itemfilters = new Array();		
	if(reportNo != null && reportNo != "" && reportNo !='null')
	{
		itemfilters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	}
	if(zoneNo != null && zoneNo != "" && zoneNo !='null')
	{
		itemfilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', zoneNo));
	}
	itemfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	itemfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
	
	columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[4] = new nlobjSearchColumn('description','custrecord_sku','group');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
	columns[7] = new nlobjSearchColumn('name',null,'group');	
	columns[8] = new nlobjSearchColumn('salesdescription','custrecord_sku','group');
	columns[9] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
	columns[10] = new nlobjSearchColumn('custrecord_batch_no',null,'group');// case# 201416471

	columns[1].setSort();
	columns[9].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, itemfilters, columns);
	if (searchresults != null) 
	{
		nlapiLogExecution('ERROR', 'Itemsearchresults.length', searchresults.length);  
		var	total = searchresults.length;
		var	beginLoc = searchresults[0].getText('custrecord_actbeginloc',null,'group');
		var	beginLocId = searchresults[0].getValue('custrecord_actbeginloc',null,'group');
		var	whlocation = searchresults[0].getValue('custrecord_wms_location',null,'group');
		var	actEndLocationId = searchresults[0].getValue('custrecord_actendloc',null,'group');
		var	actEndLocation = searchresults[0].getText('custrecord_actendloc',null,'group');
		var	sku =  searchresults[0].getText('custrecord_sku',null,'group');
		var	skuNo =  searchresults[0].getValue('custrecord_sku',null,'group');

		var	expQty = searchresults[0].getValue('custrecord_expe_qty',null,'sum');

		var	Itemdescription = '';

		if(searchresults[0].getValue('description','custrecord_sku','group') != null && searchresults[0].getValue('description','custrecord_sku','group') != "")
				{	
			Itemdescription = searchresults[0].getValue('description','custrecord_sku','group');
				}
		else if(searchresults[0].getValue('salesdescription','custrecord_sku','group') != null && searchresults[0].getValue('salesdescription','custrecord_sku','group') != "")
				{	
			Itemdescription = searchresults[0].getValue('salesdescription','custrecord_sku','group');
				}
		if(Itemdescription != null && Itemdescription !='')
		{

			Itemdescription = Itemdescription.substring(0, 20);
		}

		//case20125658 and 20125602 start :spanish conversion

		var st0,st1,st2,st3,st4,st5,st6,st7,st8;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "DE UBICACI&#211;N :";
			st1 = "ART&#205;CULO :";
			st2 = "DESCRIPCI&#211;N DE ART&#205;CULO :";
			st3 = "CANTIDAD :";
			st4 = "ENTRAR / SCAN DE UBICACI&#211;N :";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
			st7 = "SIGUIENTE";
			st8 = "DE";

		}
		else
		{
			st0 = "FROM LOCATION :";
			st1 = "ITEM:";
			st2 = "ITEM DESCRIPTION: ";
			st3 = "QUANTITY: ";
			st4 = "ENTER/SCAN FROM LOCATION:";
			st5 = "SEND";
			st6 = "PREV";
			st7 = "SKIP";//"NEXT";
			st8 = "OF";

		}


		//case20125658 and 20125602 end

		var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_location'); 
		var html = "<html><head><title>REPLENISHMENT LOCATION</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_replenishment_location' method='POST'>";
		html = html + "		<table>";

		/*html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " "+st8+" <label>" + parseFloat(total) + "</label>";
	html = html + "			</tr>";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st0+"  <label>" + beginLoc + "</label></td>";
		html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + beginLoc + ">";

		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
		html = html + "				<input type='hidden' name='hdnactendlocationid' value=" + actEndLocationId + ">";
		html = html + "				<input type='hidden' name='hdnactendlocation' value=" + actEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
		html = html + "				<input type='hidden' name='hdnZoneId' value=" + zoneNo + ">";
		html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
		html = html + "				<input type='hidden' name='hdnSku' value=" + sku + ">";
		html = html + "				<input type='hidden' name='hdnexpQty' value=" + expQty + ">";
		html = html + "				<input type='hidden' name='hdnItemdescription' value=" + Itemdescription + ">";

		html = html + "				<input type='hidden' name='hdnCartlpno' value=" + cartLPNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
	/*	html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st1+"  <label>" + sku + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st2+" <label>" + Itemdescription + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+ st3+ " <label>" + expQty + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfromloc' id='enterfromloc' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st5+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSkip.disabled=true; return false'/>";
		html = html + "					"+st7+" <input name='cmdSkip' type='submit' value='F6'/>"+st6+" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "<script type='text/javascript'>document.getElementById('enterfromloc').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";
		return html;
	}
}


function updateTaskBeginTime(reportNo,zoneNo,beginLocID)
{
	nlapiLogExecution('Debug', 'Into updateTaskBeginTime', reportNo);

	nlapiLogExecution('Debug', 'reportNo', reportNo);
	nlapiLogExecution('Debug', 'zoneNo', zoneNo);
	nlapiLogExecution('Debug', 'beginLocID', beginLocID);
	

	var itemfilters = new Array();		
	if(reportNo != null && reportNo != "" && reportNo !='null')
	{
		itemfilters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	}
	if(zoneNo != null && zoneNo != "" && zoneNo !='null')
	{
		itemfilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', zoneNo));
	}
	if(beginLocID != null && beginLocID != "")
	{
		itemfilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', beginLocID));
	}
	itemfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	itemfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_skiptask');
	

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, itemfilters, columns);
	if (searchresults != null) 
	{
      for(var s=0;s< searchresults.length;s++)
      {
			//*** The following code is added by Satish.N on 12-Mar-2013 to update task begin time and user name***//

			var currentContext = nlapiGetContext();  
			var currentUserID = currentContext.getUser();
			nlapiLogExecution('ERROR', 'currentUserID', currentUserID);

			var fields=new Array();
			fields[0]='custrecord_taskassignedto';		
			fields[1]='custrecord_actualbegintime';	

			var Values=new Array();
			Values[0]=currentUserID;	
			Values[1]=TimeStamp();	

			var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',searchresults[s].getId(),fields,Values);

			//*** Upto here ***//
		}
	}

	nlapiLogExecution('Debug', 'Out of updateTaskBeginTime', reportNo);

}
