/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/UserEvents/ebiz_TransferOrder_UE.js,v $
 *     	   $Revision: 1.2.10.1.4.1 $
 *     	   $Date: 2013/03/19 12:08:11 $
 *     	   $Author: schepuri $
 *     	   $Name: t_NSWMS_2013_1_3_9 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_TransferOrder_UE.js,v $
 * Revision 1.2.10.1.4.1  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.2.10.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2  2011/07/21 08:13:29  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

/*User event to provide Fulfillment Order and fulfilment order link for Sales order
 */

function soAddFulfillCheckInLinkUE(type, form, request){
	//obtain the context object
	var ctx = nlapiGetContext();

	//only execute when the exec context is web browser and in view mode of the PO
	if (ctx.getExecutionContext() == 'userinterface' &&
			type == 'view') {
		//Add the "ebiznet Build Assembly" field  		
		form.getSubList('item').addField('custpage_do_link', 'text', 'Fulfillment Order', null);
		form.getSubList('item').addField('custpage_build_assembly', 'text', 'Build Assembly', null) ;

		//obtain the SO internal ID
		var soid = nlapiGetRecordId();
		var soname = nlapiGetFieldValue('tranid');
		var Location= nlapiGetFieldValue('location');
		var vitemno= nlapiGetFieldValue('item');

		//resolve the generic relative URL for the Suitelet
		var dolinkURL = nlapiResolveURL('SUITELET', 'customscript_ebiznet_deliveryorder', 'customdeploy_deliveryorder');
		var AssemblyLinkURL= nlapiResolveURL('SUITELET', 'customscript_kittoorder', 'customdeploy_kittoorder');
		//complete the URL as either for production or sandbox environment
		/*if (ctx.getEnvironment() == 'PRODUCTION') {
			dolinkURL = 'https://system.netsuite.com' + dolinkURL;
			AssemblyLinkURL= 'https://system.netsuite.com' + AssemblyLinkURL;
		}
		else 
			if (ctx.getEnvironment() == 'SANDBOX') {
				dolinkURL = 'https://system.sandbox.netsuite.com' + dolinkURL;
				AssemblyLinkURL = 'https://system.sandbox.netsuite.com' + AssemblyLinkURL;
			}
*/
		//loop through the item sublist
		for (var i = 0; i < form.getSubList('item').getLineItemCount(); i++) {
			var linkURL;            
			//add the SO ID, item, and line to the URL
			linkURL = dolinkURL + '&custparam_soid=' + soid;
			linkURL = linkURL + '&custparam_sovalue=' + soname;
			linkURL = linkURL + '&custparam_so_line_item=' + nlapiGetLineItemValue('item', 'item', i + 1);
			linkURL = linkURL + '&custparam_so_line_num=' + nlapiGetLineItemValue('item', 'line', i + 1);
			linkURL = linkURL + '&custparam_ordtype=TRANSFER';

			//This  link is for Assembly Build.
			var totalQty=0;
			var ordqty =nlapiGetLineItemValue('item', 'quantity', i + 1); 
			var fulfildQty = nlapiGetLineItemValue('item', 'quantityfulfilled', i + 1);
			var comtdQty = nlapiGetLineItemValue('item', 'quantitycommitted', i + 1);
			nlapiLogExecution('ERROR', 'ordqty', ordqty);
			nlapiLogExecution('ERROR', 'fulfildQty', fulfildQty);
			nlapiLogExecution('ERROR', 'comtdQty', comtdQty);
			if(isNaN(fulfildQty))
			{
				fulfildQty=0;
			}
			if(isNaN(comtdQty))
			{
				comtdQty=0;
			}

			totalQty = (parseFloat(ordqty)-parseFloat(comtdQty))-parseFloat(fulfildQty);
			nlapiLogExecution('ERROR', 'totalQty', totalQty);
			form.getSubList('item').setLineItemValue('custpage_do_link', i + 1, '<a href="' + linkURL + '">Fulfillment Order</a>');
		}
	}
}
