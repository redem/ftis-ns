/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_Pack_ScanContainer_SL.js,v $
 *     	   $Revision: 1.1.2.5.4.4.4.21 $
 *     	   $Date: 2015/07/15 15:19:35 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Pack_ScanContainer_SL.js,v $
 * Revision 1.1.2.5.4.4.4.21  2015/07/15 15:19:35  grao
 * 2015.2 ssue fixes  201413065
 *
 * Revision 1.1.2.5.4.4.4.20  2015/07/14 15:26:43  grao
 * 2015.2 ssue fixes  201413479
 *
 * Revision 1.1.2.5.4.4.4.19  2015/06/18 20:19:47  grao
 * SB issue fixes  201413106
 *
 * Revision 1.1.2.5.4.4.4.18  2015/02/17 13:10:43  schepuri
 * issue# 201411347
 *
 * Revision 1.1.2.5.4.4.4.17  2014/10/20 14:34:39  vmandala
 * Case# 201410772 Stdbundle issue fixed
 *
 * Revision 1.1.2.5.4.4.4.16  2014/06/27 07:47:37  skavuri
 * Case# 20149107 Compatability Issue Fixed
 *
 * Revision 1.1.2.5.4.4.4.15  2014/06/13 12:57:51  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.5.4.4.4.14  2014/06/11 15:31:03  sponnaganti
 * case# 20148845
 * Stnd Bundle Issue Fix
 *
 * Revision 1.1.2.5.4.4.4.13  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.5.4.4.4.12  2014/05/27 13:57:22  snimmakayala
 * Case#: 20148567
 * RF Packing issue fixes
 *
 * Revision 1.1.2.5.4.4.4.11  2014/05/13 15:33:02  skavuri
 * Case# 20148350 SB Issue Fixed
 *
 * Revision 1.1.2.5.4.4.4.10  2014/01/06 15:44:35  nneelam
 * case# 20126636
 * Std Bundle Issue Fix.
 *
 * Revision 1.1.2.5.4.4.4.9  2013/11/14 15:42:27  skreddy
 * Case# 20125642
 * Afosa SB issue fix
 *
 * Revision 1.1.2.5.4.4.4.8  2013/11/12 06:40:15  skreddy
 * Case# 20125642
 * Afosa SB issue fix
 *
 * Revision 1.1.2.5.4.4.4.7  2013/10/01 16:07:51  rmukkera
 * Case# 20124698
 *
 * Revision 1.1.2.5.4.4.4.6  2013/09/27 22:04:44  nneelam
 * Case#. 20124663
 * Error message as carton already tied up with another order after scanning carton no in RF Process
 *
 * Revision 1.1.2.5.4.4.4.5  2013/08/20 15:56:55  grao
 * RF packing item screen repeated while qty is zero related issue fixes  20123935
 *
 * Revision 1.1.2.5.4.4.4.4  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.5.4.4.4.3  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.5.4.4.4.2  2013/04/04 16:17:47  skreddy
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1.2.5.4.4.4.1  2013/03/19 11:53:20  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.5.4.4  2013/01/02 15:42:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Double Click restriction.
 * .
 *
 * Revision 1.1.2.5.4.3  2012/12/31 05:52:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Restrict Packing for Closed and Paymeny Hold Orders.
 *
 * Revision 1.1.2.5.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.5.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.5  2012/08/14 15:11:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Changed Container Label to Carton#.
 *
 * Revision 1.1.2.4  2012/06/28 07:54:40  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.1.2.3  2012/06/15 16:26:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.1.2.2  2012/06/15 10:08:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.1.2.1  2012/06/06 07:39:03  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.32.4.21  2012/05/14 14:37:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to validating LP
 *
 * Revision 1.32.4.20  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.32.4.19  2012/05/09 15:58:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing Trantype in query string was missing
 *
 * Revision 1.32.4.18  2012/04/27 07:25:07  gkalla
 * CASE201112/CR201113/LOG201121
 * QC check based on rule configuration
 *
 * Revision 1.32.4.17  2012/04/11 22:02:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related navigating to TO Item Status when the user click on Previous button.
 *
 * Revision 1.32.4.16  2012/04/10 22:59:15  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related navigating to previous screen is resolved.
 *
 * Revision 1.32.4.15  2012/03/21 11:08:48  schepuri
 * CASE201112/CR201113/LOG201121
 * Added movetask
 *
 * Revision 1.32.4.14  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.32.4.13  2012/03/06 11:00:05  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.12  2012/03/05 14:42:30  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.11  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.43  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.42  2012/02/21 07:36:44  spendyala
 * CASE201112/CR201113/LOG201121
 * code merged upto 1.32.4.9
 *
 * Revision 1.41  2012/02/14 07:13:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged upto 1.32.4.8.
 *
 * Revision 1.40  2012/02/13 13:39:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.39  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.38  2012/02/10 10:25:15  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.6.
 *
 * Revision 1.37  2012/02/07 07:24:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.4.
 *
 * Revision 1.36  2012/02/01 12:53:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.3.
 *
 * Revision 1.35  2012/02/01 06:30:36  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.32.4.2
 *
 * Revision 1.34  2012/01/19 00:43:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Changes
 *
 * Revision 1.33  2012/01/09 13:11:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Issue if there a space in batch no
 *
 * Revision 1.32  2012/01/06 13:03:09  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing, redirecting to serialno entry
 *
 * Revision 1.31  2012/01/04 20:18:38  gkalla
 * CASE201112/CR201113/LOG201121
 * To add Item to Batch entry
 *
 * Revision 1.30  2012/01/04 15:08:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.29  2011/12/30 14:09:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.28  2011/12/27 23:24:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.27  2011/12/23 23:36:41  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.26  2011/12/23 16:21:51  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.25  2011/12/23 13:24:07  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.24  2011/12/23 13:19:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.23  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.22  2011/12/15 14:10:40  schepuri
 * CASE201112/CR201113/LOG201121
 * modified field names in customrecord_ebiznet_trn_poreceipt
 *
 * Revision 1.21  2011/12/02 13:19:35  schepuri
 * CASE201112/CR201113/LOG201121
 * RF To generate location acc to the location defined in PO
 *
 * Revision 1.20  2011/11/17 08:44:28  spendyala
 * CASE201112/CR201113/LOG201121
 * wms status flag for create inventory is changed to 17 i .e, FLAG INVENTORY INBOUND
 *
 * Revision 1.19  2011/09/29 12:04:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.18  2011/09/28 16:08:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/09/26 19:58:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.16  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.14  2011/09/14 05:27:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/09/12 08:24:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/09/10 11:28:23  schepuri
 * CASE201112/CR201113/LOG201121
 *
 * redirecting to customscript_serial_no
 *
 * Revision 1.11  2011/07/05 12:36:44  schepuri
 * CASE201112/CR201113/LOG201121
 * RF CheckIN changes
 *
 * Revision 1.10  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.9  2011/06/17 07:39:47  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * Confirm putaway changes
 *
 * Revision 1.8  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.6  2011/06/06 07:10:13  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * WBC Issues
 *
 * Revision 1.5  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function Pack_ScanContainerNo_SL(request, response){
	if (request.getMethod() == 'GET') {

		var getOrderno = request.getParameter('custparam_orderno');
		var getContainerNo = request.getParameter('custparam_containerno');
		//var getContainerSize = request.getParameter('custparam_containersize');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getExpectedQty=request.getParameter('custparam_expectedquantity');
		var getActualQty = request.getParameter('custparam_actualquantity');
		nlapiLogExecution('DEBUG','getActualQty',getActualQty);
		var fetchedcontainerno=request.getParameter('custparam_fetchedcontainerlp');
		var waveno=request.getParameter('custparam_waveno');
		var wmslocation=request.getParameter('custparam_wmslocation');

		var getContainerSize='';
		var getContainerSizevalue='';
		var locationarray = new Array();
		locationarray.push('@NONE@');
		var filtersContiner = new Array();
		filtersContiner.push(new nlobjSearchFilter('custrecord_ebiz_container_defaultflag', null, 'is', 'T'));
		if(wmslocation!=null && wmslocation!='')
		{
			locationarray.push(wmslocation);
			filtersContiner.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', locationarray));
		}
		var colsContainer = new Array();
		colsContainer[0]=new nlobjSearchColumn('custrecord_cubecontainer');  
		colsContainer[1]=new nlobjSearchColumn('custrecord_tareweight');  
		colsContainer[2]=new nlobjSearchColumn('custrecord_inventory_in_on');
		colsContainer[3]=new nlobjSearchColumn('internalid');
		colsContainer[2]=new nlobjSearchColumn('name');

		var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersContiner, colsContainer);  
		nlapiLogExecution('DEBUG','ContainerSearchResults',ContainerSearchResults);
		if(ContainerSearchResults!=null && ContainerSearchResults!='' && ContainerSearchResults.length>0)
		{
			getContainerSize=ContainerSearchResults[0].getValue('name');
			getContainerSizevalue=ContainerSearchResults[0].getValue('internalid');
		
		}
		nlapiLogExecution('DEBUG','Containersize',getContainerSize);
		var getLanguage = request.getParameter('custparam_language');	
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6,st7,st8;
		//case 20125642 start: spanish conversion,added "es_AR"
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{//case 20125642 end
			st0 = "";
			st1 = "CAJA #";
			st2 = "WAVE #";
			st3 = "ENTRAR/SCAN CAJA #";
			st4 = "ENTRAR/SCAN TAMA&#209;O DEL CART&#211;N";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
			st7 = "AUTO ASSIGN LP";
			st8 = "CAJA CERRADO";
		}
		else
		{
			st0 = "";
			st1 = "CARTON#";
			st2 = "WAVE#";
			st3 = "ENTER/SCAN CARTON#";
			st4 = "ENTER/SCAN CARTON  SIZE";
			st5 = "SEND";
			st6 = "PREV";
			st7 = "AUTO ASSIGN LP";
			st8 = "CLOSE CARTON";
		}

		var functionkeyHtml=getFunctionkeyScript('_rf_pack_scancontainer'); 	
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entercontainerno').focus();";        
		//case# 20148845 starts
		//html = html + " function check(){var size=document.getElementById('entersize').value;if(size=='' || size==null){alert('pls enter carton size');return false;}else{return true;}}";   
		//html = html + "</script>";
		//case# 20148845 ends
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_pack_scancontainer' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 +": <label>" + getContainerNo + "</label>";
		html = html + "				<input type='hidden' name='hdnOrderNo' value=" + getOrderno + ">";	
		if(fetchedcontainerno==null || fetchedcontainerno=='')
		{
			html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerNo + ">";
		}
		else
		{
			html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + fetchedcontainerno + ">";	
		}
		html = html + "				<input type='hidden' name='hdnContainerSize' value='" + getContainerSize + "'>";
		html = html + "				<input type='hidden' name='hdnContainerSizevalue' value=" + getContainerSizevalue + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQty' value=" + getExpectedQty + ">";
		html = html + "				<input type='hidden' name='hdnActualdQty' value=" + getActualQty + ">";
		html = html + "				<input type='hidden' name='hdnwaveno' value=" + waveno + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnclosecarton'>";
		html = html + "				<input type='hidden' name='hdnsubmit'>";
		html = html + "				<input type='hidden' name='hdnprevious'>";
		html = html + "				<input type='hidden' name='hdnautolp'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ": <label>" + waveno + "</label>";

		html = html + "			</tr>";

		//html = html + "			<tr>";
		//html = html + "				<td align = 'left'>CONTAINER SIZE : <label>" + getContainerSize + "</label>";
		//html = html + "				</td>";
		//html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercontainerno' id='entercontainerno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		nlapiLogExecution('DEBUG', 'getContainerSize123', getContainerSize);
		html = html + "				<td align = 'left'><input name='entersize' id='entersize' type='text' value='" + getContainerSize.toString() + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnsubmit.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdclose.disabled=true;this.form.cmdautolp.disabled=true;return false'/>";
		html = html + "					" + st6 + "  <input name='cmdPrevious' type='submit' value='F7' /></br>";
		//case# 20148845 starts
		//html = html + "					" + st7 + "  <input name='cmdautolp' type='submit' value='F6' OnClick='return check()'/>";
		html = html + "					" + st7 + "  <input name='cmdautolp' type='submit' value='F6' onclick='this.form.hdnautolp.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdclose.disabled=true;this.form.cmdSend.disabled=true;return false'/>";
		//case# 20148845 ends
		html = html + "					" + st8 + " <input name='cmdclose' type='submit' value='F8' onclick='this.form.hdnclosecarton.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdautolp.disabled=true;return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercontainerno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else
	{
		var optedEvent = request.getParameter('cmdPrevious');
		if(optedEvent==null || optedEvent=='')
		{
			optedEvent = request.getParameter('hdnsubmit');
		}
		if(optedEvent==null || optedEvent=='')
		{
			//optedEvent = request.getParameter('cmdautolp');//Case# 20149107
			optedEvent = request.getParameter('hdnautolp');
		}
		if(optedEvent==null || optedEvent=='')
		{
			optedEvent = request.getParameter('hdnclosecarton');
		}
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
		var SOarray = new Array(); 
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]);


		var st9,st10,st11,st12,st13,st14,st15;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{			
			st9 = "LP CARTON YA EXISTE";
			st10 = "ENTER NUEVA CAJA # (OR) SELECCIONE OPCI&#211;N F6";
			st11 = "LP CAJA CERRADA YA";
			st12 = "CAJA DE RANGO NO V&#193;LIDO LP";
			st13 = "POR FAVOR, ENTRE CAJA # ";
			st14 = "LP CAJA NO V&#193;LIDO";
			st15 = "NO V&#193;LIDO TAMA&#209;O DEL CAJA NO";
		}
		else
		{			
			st9 = "CARTON LP ALREADY EXISTS";
			st10 = "ENTER NEW CARTON# (OR) SELECT F6 OPTION";
			st11 = "CARTON LP ALREADY CLOSED";
			st12 = "INVALID CARTON LP RANGE";
			st13 = "PLEASE ENTER CARTON# ";
			st14 = "INVALID CARTON LP";
			st15 = "INVALID CARTON SIZE";
		}

		SOarray["custparam_screenno"] = 'Pack2';
		SOarray["custparam_orderno"]=request.getParameter('hdnOrderNo');				
		SOarray["custparam_wmslocation"] = request.getParameter('custparam_wmslocation');
		SOarray["custparam_waveno"] = request.getParameter('hdnwaveno');
		SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');// case# 201416161
		
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');


		SOarray["custparam_fetchedcontainerlp"]=request.getParameter('hdnFetchedContainerLPNo');
		var poLoc=request.getParameter('custparam_wmslocation');
		var getContainerSize = request.getParameter('entersize');
		var orderno=request.getParameter('hdnOrderNo');	

		if(getContainerSize==null || getContainerSize=="")
		{
			getContainerSize = request.getParameter('hdnContainerSize');
			SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');	
			SOarray["custparam_containersizevalue"] = request.getParameter('hdnContainerSizevalue');	
		}
		else
		{
			var contsize='';
			if(request.getParameter('entersize') != null && request.getParameter('entersize')!='')
			{
				contsize = request.getParameter('entersize');

				var filtersContiner = new Array(); 

				filtersContiner.push(new nlobjSearchFilter('name', null, 'is', contsize));
				filtersContiner.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				var colsContainer = new Array();
				colsContainer[0]=new nlobjSearchColumn('custrecord_cubecontainer');  
				colsContainer[1]=new nlobjSearchColumn('custrecord_tareweight');  
				colsContainer[2]=new nlobjSearchColumn('custrecord_inventory_in_on');
				colsContainer[3]=new nlobjSearchColumn('internalid');
				colsContainer[2]=new nlobjSearchColumn('name');

				var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersContiner, colsContainer);  

				if(ContainerSearchResults!=null && ContainerSearchResults!='' && ContainerSearchResults.length>0)
				{
					getContainerSize=ContainerSearchResults[0].getValue('name');
					getContainerSizevalue=ContainerSearchResults[0].getValue('internalid');
				}
				else
				{
					getContainerSize='';
					getContainerSizevalue='';
				}

				SOarray["custparam_containersize"] = getContainerSize;	
				SOarray["custparam_containersizevalue"] = getContainerSizevalue;	
//				case#201411347
				if(getContainerSizevalue == '' || getContainerSizevalue == null)
				{
					SOarray["custparam_orderno"]=request.getParameter('hdnOrderNo');	
					SOarray["custparam_error"] = st15;			
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
			}
		}
		if (optedEvent == 'F7') 
		{
			SOarray["custparam_orderno"]=request.getParameter('hdnOrderNo');			
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanorder', 'customdeploy_ebiz_rf_pack_scanorder_di', false, SOarray);
		}
		else if (optedEvent == 'ENT') 
		{
			var getEnteredContainerNo = request.getParameter('entercontainerno');
			nlapiLogExecution('DEBUG', 'getEnteredContainerNo', getEnteredContainerNo);
			if(getEnteredContainerNo ==request.getParameter('hdnFetchedContainerLPNo'))
			{
				getEnteredContainerNo=request.getParameter('hdnFetchedContainerLPNo');
				SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
				SOarray["custparam_error"] = st9;			
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanitem', 'customdeploy_ebiz_rf_pack_scanitem_di', false, SOarray);	
			}
			if(getEnteredContainerNo==null || getEnteredContainerNo=="")
			{
				getEnteredContainerNo=request.getParameter('hdnFetchedContainerLPNo');
				SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
				SOarray["custparam_error"] = st10;			
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			}
			else
			{
				SOarray["custparam_containerno"] = request.getParameter('entercontainerno');

				var ventercartonno = request.getParameter('entercontainerno');

				var taskfilters = new Array();

				if(orderno!=""){
					taskfilters.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'isnot', orderno));
					//case #20126636 starts
					taskfilters.push( new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
					//End
				}

				if(ventercartonno!=null && ventercartonno!='')
				{
					taskfilters.push( new nlobjSearchFilter('custrecord_container_lp_no', null,'is', ventercartonno));
				}
				//case # 20124663 start
				taskfilters.push( new nlobjSearchFilter('custrecord_tasktype', null,'anyof', [3,14]));
				//case # 20124663 End
				var taskcolumns = new Array();
				taskcolumns[0] = new nlobjSearchColumn('name');

				var tasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskfilters, taskcolumns);

				if(tasksearchresults!=null && tasksearchresults!='' && tasksearchresults.length>0)
				{
					SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
					SOarray["custparam_error"] = 'Carton is already tied up with another Order.';			
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;					
				}

				nlapiLogExecution('DEBUG', 'poLoc', poLoc);		
				var LPReturnValue = "";	
				var wmsstatusflag = "";

				var filtermlp = new Array();
				filtermlp[0] = new nlobjSearchFilter('name', null, 'is', getEnteredContainerNo);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag');

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtermlp,columns);

				if (SrchRecord != null && SrchRecord.length > 0) 
				{
					wmsstatusflag = SrchRecord[0].getValue('custrecord_ebiz_lpmaster_wmsstatusflag');
					nlapiLogExecution('DEBUG', 'wmsstatusflag',wmsstatusflag);
					if(wmsstatusflag == '28')
					{
						SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
						SOarray["custparam_error"] = st11;			
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					else
					{
						nlapiLogExecution('DEBUG', 'IN');
					}
				}
				else
				{

					LPReturnValue = ebiznet_LPRange_CL(getEnteredContainerNo, '2',poLoc,'2');		
					nlapiLogExecution('DEBUG', 'LP Return Value new1', LPReturnValue);		
					//LP Checking in masterlp record starts
					if (LPReturnValue == false) {

						SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
						SOarray["custparam_error"] = st12;			
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					else
					{
						nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
						var filtersmlp = new Array();
						filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getEnteredContainerNo);

						var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

						if (SrchRecord != null && SrchRecord.length > 0) {
							nlapiLogExecution('DEBUG', 'LP FOUND');
							SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
							SOarray["custparam_error"] = st9;			
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;

						}
						else {
							nlapiLogExecution('DEBUG', 'LP NOT FOUND');
//							var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
//							customrecord.setFieldValue('name', getEnteredContainerNo);
//							customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getEnteredContainerNo);
//							var rec = nlapiSubmitRecord(customrecord, false, true);
						}
					}
				}
				nlapiLogExecution('DEBUG', 'OUT');
				var filters = new Array();
				nlapiLogExecution('DEBUG','orderno in getSalesOrderlist',orderno);

				if(orderno!=""){
					filters.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
				}
				filters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
				var contlp=request.getParameter('hdnFetchedContainerLPNo');
				if(contlp!=null && contlp!='')
				{
					nlapiLogExecution('DEBUG','contlp ',contlp);
					filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null,'is', contlp));
				}
				filters.push(new nlobjSearchFilter('custrecord_pack_confirmed_date',null, 'isempty', null));
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
				filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('name');
				columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				columns[4] = new nlobjSearchColumn('custrecord_comp_id');
				columns[5] = new nlobjSearchColumn('custrecord_site_id');
				columns[6] = new nlobjSearchColumn('custrecord_container');
				columns[7] = new nlobjSearchColumn('custrecord_total_weight');
				columns[8] = new nlobjSearchColumn('custrecord_sku');
				columns[9] = new nlobjSearchColumn('custrecord_wms_location');
				columns[10] = new nlobjSearchColumn('custrecord_expe_qty');
				columns[11] = new nlobjSearchColumn('custrecord_act_qty');
				columns[8].setSort();

				nlapiLogExecution('DEBUG','contlp ',contlp);
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
				nlapiLogExecution('DEBUG','searchresults ',searchresults);
				if(searchresults!=null && searchresults.length>0)
				{
					SOarray["custparam_iteminternalid"] = searchresults[0].getValue('custrecord_sku');
					//SOarray["custparam_expectedquantity"] = searchresults[0].getValue('custrecord_expe_qty');
					SOarray["custparam_expectedquantity"] = searchresults[0].getValue('custrecord_act_qty');
					SOarray["custparam_actualquantity"] = searchresults[0].getValue('custrecord_act_qty');
					nlapiLogExecution('DEBUG','SOarray["custparam_actualquantity"]',SOarray["custparam_actualquantity"]);
					SOarray["custparam_recordinternalid"]= searchresults[0].getId();
					SOarray["custparam_loopcount"]= searchresults.length;
					SOarray["custparam_containersize"]=searchresults[0].getText('custrecord_container');
				}
				else
				{
					nlapiLogExecution('ERROR','into else ');
					SOarray["custparam_error"] = 'NO CONTAINERS TO PACK';			
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanitem', 'customdeploy_ebiz_rf_pack_scanitem_di', false, SOarray);
			}
		}
		else if (optedEvent == 'F6') //auto assign lp
		{
			nlapiLogExecution('DEBUG','poLoc',poLoc);
			var	getAutoAssignLP = GetMaxLPNo(1, 2,poLoc);
			nlapiLogExecution('DEBUG','getAutoAssignLP',getAutoAssignLP);
			var filtersmlp = new Array();
			filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getAutoAssignLP);
			filtersmlp[1] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'is', poLoc);

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

			if (SrchRecord != null && SrchRecord.length > 0) {
				nlapiLogExecution('DEBUG', 'LP FOUND');
				SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
				SOarray["custparam_error"] = st9;			
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

			}
			else {
//				nlapiLogExecution('DEBUG', 'LP NOT FOUND');
//				var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
//				customrecord.setFieldValue('name', getAutoAssignLP);
//				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getAutoAssignLP);
//				var rec = nlapiSubmitRecord(customrecord, false, true);


				var filters = new Array();
				nlapiLogExecution('DEBUG','orderno in getSalesOrderlist',orderno);

				if(orderno!=""){
					filters.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
				}
				filters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
				var contlp=request.getParameter('hdnFetchedContainerLPNo');
				if(contlp!=null && contlp!='')
				{
					nlapiLogExecution('DEBUG','contlp ',contlp);
					filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null,'is', contlp));
				}
				filters.push(new nlobjSearchFilter('custrecord_pack_confirmed_date',null, 'isempty', null));
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
				filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('name');
				columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				columns[4] = new nlobjSearchColumn('custrecord_comp_id');
				columns[5] = new nlobjSearchColumn('custrecord_site_id');
				columns[6] = new nlobjSearchColumn('custrecord_container');
				columns[7] = new nlobjSearchColumn('custrecord_total_weight');
				columns[8] = new nlobjSearchColumn('custrecord_sku');
				columns[9] = new nlobjSearchColumn('custrecord_wms_location');
				columns[10] = new nlobjSearchColumn('custrecord_expe_qty');
				columns[11] = new nlobjSearchColumn('custrecord_act_qty');
				columns[8].setSort();

				nlapiLogExecution('DEBUG','contlp ',contlp);
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
				nlapiLogExecution('DEBUG','searchresults ',searchresults);
				if(searchresults!=null && searchresults.length>0)
				{
					SOarray["custparam_iteminternalid"] = searchresults[0].getValue('custrecord_sku');
					//SOarray["custparam_expectedquantity"] = searchresults[0].getValue('custrecord_expe_qty');
					SOarray["custparam_expectedquantity"] = searchresults[0].getValue('custrecord_act_qty');
					SOarray["custparam_actualquantity"] = searchresults[0].getValue('custrecord_act_qty');
					SOarray["custparam_recordinternalid"]= searchresults[0].getId();
					SOarray["custparam_loopcount"]= searchresults.length;
					//case 201410772 start
					SOarray["custparam_containersize"]=searchresults[0].getText('custrecord_container');
					//case 201410772 end
				}
				else
				{
					SOarray["custparam_error"] = 'NO CONTAINERS TO PACK';			
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				SOarray["custparam_containerno"] = getAutoAssignLP;
				nlapiLogExecution('DEBUG', 'getAutoAssignLP',getAutoAssignLP);
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanitem', 'customdeploy_ebiz_rf_pack_scanitem_di', false, SOarray);
			}
		}
		else if (optedEvent == 'F8') //close carton
		{

			var getEnteredContainerNo = request.getParameter('entercontainerno');
			nlapiLogExecution('DEBUG', 'getEnteredContainerNo', getEnteredContainerNo);
			if(getEnteredContainerNo==null || getEnteredContainerNo=="")
			{
				getEnteredContainerNo=request.getParameter('hdnFetchedContainerLPNo');
				SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
				SOarray["custparam_error"] = st13;			
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanitem', 'customdeploy_ebiz_rf_pack_scanitem_di', false, SOarray);	
			}
			else
			{
				SOarray["custparam_containerno"] = request.getParameter('entercontainerno');	

				nlapiLogExecution('DEBUG', 'poLoc', poLoc);		
				var LPReturnValue = "";		

				var filtermlp = new Array();
				filtermlp[0] = new nlobjSearchFilter('name', null, 'is', getEnteredContainerNo);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag');

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtermlp,columns);

				if (SrchRecord != null && SrchRecord.length > 0) 
				{
					wmsstatusflag = SrchRecord[0].getValue('custrecord_ebiz_lpmaster_wmsstatusflag');
					nlapiLogExecution('DEBUG', 'wmsstatusflag',wmsstatusflag);
					if(wmsstatusflag == '28')
					{
						SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
						SOarray["custparam_error"] = st11;			
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}

				}
				

					LPReturnValue = ebiznet_LPRange_CL(getEnteredContainerNo, '2',poLoc,'2');		
					nlapiLogExecution('DEBUG', 'LP Return Value new', LPReturnValue);		
					//LP Checking in masterlp record starts
					if (LPReturnValue == true) {
						try {
							nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
							var filtersmlp = new Array();
							filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getEnteredContainerNo);

						var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

						if (SrchRecord != null && SrchRecord.length > 0) {
							nlapiLogExecution('DEBUG', 'LP FOUND');
							SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
							SOarray["custparam_error"] = st9;			
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

						}
						else {
							nlapiLogExecution('DEBUG', 'LP NOT FOUND');
							var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
							customrecord.setFieldValue('name', getEnteredContainerNo);
							customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getEnteredContainerNo);
							var rec = nlapiSubmitRecord(customrecord, false, true);
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
						}
					} 
					catch (e) {
						nlapiLogExecution('DEBUG', 'Failed to Insert into Master LP Record');
					}
				}
				// Case# 20148350 starts
				else if (LPReturnValue == false) {
					LPReturnValue = ebiznet_LPRange_CL(getEnteredContainerNo, '1',poLoc,'2');		
					nlapiLogExecution('DEBUG', '-LP Return Value new', LPReturnValue);
					if (LPReturnValue == true) {
						try {
							nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
							var filtersmlp = new Array();
							filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getEnteredContainerNo);
							var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);
							if (SrchRecord != null && SrchRecord.length > 0) {
								nlapiLogExecution('DEBUG', 'LP FOUND');
								SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
								SOarray["custparam_error"] = st9;			
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							}
							else {
								nlapiLogExecution('DEBUG', 'LP NOT FOUND');
								var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
								customrecord.setFieldValue('name', getEnteredContainerNo);
								customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getEnteredContainerNo);
								var rec = nlapiSubmitRecord(customrecord, false, true);
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
							}
						} 
						catch (e) {
							nlapiLogExecution('DEBUG', 'Failed to Insert into Master LP Record');
						}
					}
					else
					{
						SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
						SOarray["custparam_error"] = st14;			
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					}
				}
				// Case# 20148350 end
				else
				{
					SOarray["custparam_containerno"] = request.getParameter('hdnFetchedContainerLPNo');
					SOarray["custparam_error"] = st14;			
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				}
			}
		}

	}

}

