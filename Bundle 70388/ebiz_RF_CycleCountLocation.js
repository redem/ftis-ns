/***************************************************************************
���������������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountLocation.js,v $
*� $Revision: 1.7.2.4.4.9.2.10.2.2 $
*� $Date: 2014/08/19 12:55:15 $
*� $Author: skavuri $
*� $Name: t_eBN_2014_2_StdBundle_0_57 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_CycleCountLocation.js,v $
*� Revision 1.7.2.4.4.9.2.10.2.2  2014/08/19 12:55:15  skavuri
*� Case# 20149982 Std Bundle issue fixed
*�
*� Revision 1.7.2.4.4.9.2.10.2.1  2014/07/09 13:16:49  rmukkera
*� Case # 20149282
*� jawbone issues applicable to standard also
*�
*� Revision 1.7.2.4.4.9.2.10  2014/06/13 10:10:34  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.7.2.4.4.9.2.9  2014/06/06 08:26:05  skavuri
*� Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
*�
*� Revision 1.7.2.4.4.9.2.8  2014/05/30 00:34:20  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.7.2.4.4.9.2.7  2013/09/23 07:12:55  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue related case#201216858 is resolved.
*�
*� Revision 1.7.2.4.4.9.2.6  2013/09/16 15:37:15  rmukkera
*� Case# 20124313
*�
*� Revision 1.7.2.4.4.9.2.5  2013/08/03 21:16:08  snimmakayala
*� Case# 201214994
*� Cycle Count Issue Fixes
*�
*� Revision 1.7.2.4.4.9.2.4  2013/08/02 15:34:46  rmukkera
*� add new item related changes
*�
*� Revision 1.7.2.4.4.9.2.3  2013/05/01 01:07:23  kavitha
*� CASE201112/CR201113/LOG201121
*� TSG Issue fixes
*�
*� Revision 1.7.2.4.4.9.2.2  2013/04/17 16:02:35  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.7.2.4.4.9.2.1  2013/03/01 14:34:54  skreddy
*� CASE201112/CR201113/LOG201121
*� Merged from FactoryMation and change the Company name
*�
*� Revision 1.7.2.4.4.9  2013/02/20 22:30:57  kavitha
*� CASE201112/CR201113/LOG201121
*� Cycle count process - Issue fixes
*�
*� Revision 1.7.2.4.4.8  2013/02/14 01:35:04  kavitha
*� CASE201112/CR201113/LOG201121
*� Cycle count process - Issue fixes
*�
*� Revision 1.7.2.4.4.7  2013/02/12 02:23:15  kavitha
*� CASE201112/CR201113/LOG201121
*� Serial # functionality - Inventory process - Gaps fixes
*�
*� Revision 1.7.2.4.4.6  2013/02/06 03:32:58  kavitha
*� CASE201112/CR201113/LOG201121
*� Serial # functionality - Inventory process
*�
*� Revision 1.7.2.4.4.5  2013/02/01 07:58:15  rrpulicherla
*� CASE201112/CR201113/LOG201121
*� Cyclecountlatestfixed
*�
*� Revision 1.7.2.4.4.3  2012/10/05 06:34:12  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting Multiple language into given suggestions
*�
*� Revision 1.7.2.4.4.2  2012/09/26 12:33:26  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.7.2.4.4.1  2012/09/24 10:08:56  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi Lnaguage
*�
*� Revision 1.7.2.4  2012/09/03 13:08:28  schepuri
*� CASE201112/CR201113/LOG201121
*� Previous button is driven according to LP Required Rule Value.
*�
*� Revision 1.7.2.3  2012/07/31 06:57:42  spendyala
*� CASE201112/CR201113/LOG201121
*� Depending Upon the System Rule is LP scan required or not will drive the user to scan LP.
*�
*� Revision 1.7.2.2  2012/03/16 14:08:54  spendyala
*� CASE201112/CR201113/LOG201121
*� Disable-button functionality is been added.
*�
*� Revision 1.7.2.1  2012/02/21 13:22:48  schepuri
*� CASE201112/CR201113/LOG201121
*� function Key Script code merged
*�
*� Revision 1.7  2011/12/08 07:21:05  spendyala
*� CASE201112/CR201113/LOG201121
*� added expected item internal id to CCArray
*�
*
****************************************************************************/




/**
 * @author LN
 * @version
 * @date
 */
function CycleCountLocation(request, response){
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('ERROR', 'Into Request', 'Success');
		
        var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
    
		var st0,st1,st2,st3,st4;


		if( getLanguage == 'es_ES')
		{
			st0 = "INVENTARIO C&#205;CLICO";
			st1 = "IR A PUNTO DE:";
			st2 = "INGRESAR / ESCANEAR UBICACI&#211;N";
			st3 = "CONT";
			st4 = "ANTERIOR";
			
		}
		else
		{
			st0 = "CYCLE COUNT";
			st1 = "GO TO LOCATION:";
			st2 = "ENTER/SCAN LOCATION:";
			st3 = "CONT";
			st4 = "PREV";
			
		}

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var fields = ['custrecord_ebiz_include_emploc'];
		var columns = nlapiLookupField('customrecord_ebiznet_cylc_createplan', getPlanNo, fields);
		var vemptybinloc= columns.custrecord_ebiz_include_emploc;
		nlapiLogExecution('ERROR', 'vemptybinloc', vemptybinloc);

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getPlanNo);
		filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
		var cols=new Array();

		cols[0]=new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl({custrecord_cyc_skip_task},0)").setSort();
		cols[1]=new nlobjSearchColumn('custrecord_inboundlocgroupid','custrecord_cycact_beg_loc','group').setSort();
		cols[2]=new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_cycact_beg_loc','group').setSort();
		cols[3]=new nlobjSearchColumn('custrecord_cycact_beg_loc',null,'group');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, cols);

		if (searchresults != null && searchresults.length > 0) {

			var getBeginLocationId =searchresults[0].getValue('custrecord_cycact_beg_loc',null,'group');//CCRec.getFieldValue('custrecord_cycact_beg_loc');
			var getBeginLocationName = searchresults[0].getText('custrecord_cycact_beg_loc',null,'group');//CCRec.getFieldText('custrecord_cycact_beg_loc');

			nlapiLogExecution('ERROR', 'Location Name', getBeginLocationName);
		}
		else
		{
			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di', false, null);
			return;
		}

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountlocation'); 
		var html = "<html><head><title>"+st0+"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends     
		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountlocation' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st1+"</td></tr>";
		html = html + "				<tr><td align = 'left'><label>" + getBeginLocationName + "</label></td></tr>";
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "						<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"</td></tr>";
		html = html + "				<tr><td align = 'left'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st3+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false' style='width:40;height:40;'/>";
		html = html + "					"+st4+" <input name='cmdPrevious' type='submit' value='F7' style='width:40;height:40;'/>";
		html = html + "					SKIP <input name='cmdSkip' type='submit' value='F8' style='width:40;height:40;'/>";
		//if(vemptybinloc == 'T')
		//{
	//	html = html + "					EMPTY LOCATION <input name='cmdEmptyLocation' type='submit' value='F9' style='width:40;height:40;'/>";
		//}
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
html = html + "					EMPTY LOCATION <input name='cmdEmptyLocation' type='submit' value='F9' />";
		html = html + "					ADD NEW ITEM <input name='cmdaddnewitem' type='submit' value='F10'/>";

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		try {
			var getCycleCountLocation = request.getParameter('enterlocation');
			nlapiLogExecution('ERROR', 'Location', getCycleCountLocation);

			var getCycleCountPlanNo = request.getParameter('custparam_planno');
			nlapiLogExecution('ERROR', 'getPlanNo', getCycleCountPlanNo);

			var CCarray = new Array();
			var getLanguage = request.getParameter('hdngetLanguage');
			CCarray["custparam_language"] = getLanguage;
			
			nlapiLogExecution('ERROR', 'getLanguage', CCarray["custparam_language"]);
			
			
			var st5;
			if( getLanguage == 'es_ES')
			{
				st5 = "UBICACI&#211;N NO V&#193;LIDA";
				
			}
			else
			{
				st5= "INVALID LOCATION";
			}
			CCarray["custparam_error"] =st5;
			CCarray["custparam_screenno"] = '29';
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getCycleCountPlanNo);
			filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
			var cols=new Array();

			cols[0]=new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl({custrecord_cyc_skip_task},0)").setSort();
			cols[1]=new nlobjSearchColumn('custrecord_inboundlocgroupid','custrecord_cycact_beg_loc','group').setSort();
			cols[2]=new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_cycact_beg_loc','group').setSort();
			cols[3]=new nlobjSearchColumn('custrecord_cycact_beg_loc',null,'group');

			//consolidating the binlocations
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, cols);
			var getRecordId='';
			var expecqty=0;
			var lp=new Array();
			nlapiLogExecution('ERROR', 'searchresultslength', searchresults);
			if (searchresults != null && searchresults.length > 0) {
				nlapiLogExecution('ERROR', 'searchresultslength', searchresults.length);
				var binlocation=searchresults[0].getValue('custrecord_cycact_beg_loc',null,'group');
				nlapiLogExecution('ERROR', 'binlocation', binlocation);
				filters[2]=new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof', binlocation);
				
				var bincols=new Array();
				bincols[0]=new nlobjSearchColumn('custrecord_cyclesku',null,'group');
				//bincols[1]=new nlobjSearchColumn('custrecord_cyc_skip_task',null,'group').setSort();
				bincols[1]=new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl({custrecord_cyc_skip_task},0)").setSort();
				//bincols[1]=new nlobjSearchColumn('custrecord_cycleexp_qty',null,'group');
				
				//consolidating the skus for the binlocation.
				var binSearchResults=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, bincols);
				nlapiLogExecution('ERROR', 'binSearchResults', binSearchResults);
                           if(binSearchResults!=null && binSearchResults!='' && binSearchResults.length>0)
                           {
                        	   nlapiLogExecution('ERROR', 'binSearchResultslength', binSearchResults.length);   

                        	   var sku=binSearchResults[0].getValue('custrecord_cyclesku',null,'group');
                        	   var batch=binSearchResults[0].getValue('custrecord_expcycleabatch_no',null,'group');
                        	   nlapiLogExecution('ERROR', 'sku', sku);
                        	   if(sku!=null && sku!='')
                        	   filters[3]=new nlobjSearchFilter('custrecord_cyclesku', null, 'anyof', sku);
                        	   if(batch!=null && batch!='')
                               filters[4]=new nlobjSearchFilter('custrecord_expcycleabatch_no', null, 'anyof', batch);
                        	   var skucols=new Array();
                        	   skucols[0]=new nlobjSearchColumn('custrecord_cycleexp_qty');
                        	   skucols[1]=new nlobjSearchColumn('custrecord_cyclelpno');
                        	   skucols[2]=new nlobjSearchColumn('custrecord_cyc_skip_task').setSort();
                        	   var SkuSearchResults=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, skucols);
                        	   nlapiLogExecution('ERROR', 'SkuSearchResults', SkuSearchResults);
                        	   if(SkuSearchResults!=null && SkuSearchResults!='' && SkuSearchResults.length>0)
                        	   {
                        		   nlapiLogExecution('ERROR', 'SkuSearchResults', SkuSearchResults.length);
                        		   getRecordId=SkuSearchResults[0].getId();
                        		   for(var s=0;s<SkuSearchResults.length;s++)
                        		   {
                        			  // var currrow=[SkuSearchResults[s].getValue('custrecord_cyclelpno'),SkuSearchResults[s].getValue('custrecord_cycleexp_qty')];
                        			   expecqty=parseInt(expecqty)+parseInt(SkuSearchResults[s].getValue('custrecord_cycleexp_qty'));
                        			  // lp.push(currrow);
                        		   }
                        	   }
                        	   
                        	   if(searchresults.length > 1)
                        	   {
                        		   var SearchnextResult = searchresults[1];
                        		   CCarray["custparam_nextlocation"] = searchresults[1].getText('custrecord_cycact_beg_loc',null,'group');

                        	   }
                        	   nlapiLogExecution('ERROR', 'getRecordId', getRecordId);
                        	   var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', getRecordId);
                        	    CCarray["custparam_recordid"] = getRecordId;
                				CCarray["custparam_planno"] = request.getParameter('custparam_planno');
                				CCarray["custparam_begin_location_id"] = CCRec.getFieldValue('custrecord_cycact_beg_loc');
                				CCarray["custparam_begin_location_name"] =  CCRec.getFieldText('custrecord_cycact_beg_loc');
                				CCarray["custparam_lpno"] = CCRec.getFieldValue('custrecord_cyclelpno');
              				    CCarray["custparam_packcode"] = CCRec.getFieldValue('custrecord_expcyclepc');
                				CCarray["custparam_expqty"] = CCRec.getFieldValue('custrecord_cycleexp_qty');
                				CCarray["custparam_expitem"] = CCRec.getFieldText('custrecord_cyclesku');
                				CCarray["custparam_expitemId"] = CCRec.getFieldValue('custrecord_cyclesku');
                				CCarray["custparam_expiteminternalid"] = CCRec.getFieldValue('custrecord_cyclesku');
                				CCarray["custparam_expitemdescription"] = CCRec.getFieldValue('custrecord_cycle_skudesc');
                				CCarray["custparam_expitemno"] = CCRec.getFieldValue('custrecord_cyclecount_exp_ebiz_sku_no');
                				CCarray["custparam_noofrecords"] = searchresults.length;	
                				CCarray["custparam_siteid"] = CCRec.getFieldValue('custrecord_cyclesite_id');
                				CCarray["custparam_skustatus"] = CCRec.getFieldValue('custrecord_expcyclesku_status');

                           }





//				var getRecordId = searchresults[0].getId();
//				nlapiLogExecution('ERROR', 'Search Results Id', getRecordId);

//				if(searchresults.length > 1)
//				{
//				var SearchnextResult = searchresults[1];
//				CCarray["custparam_nextlocation"] = SearchnextResult.getText('custrecord_cycact_beg_loc');

//				}

//				var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', getRecordId);

//				CCarray["custparam_recordid"] = getRecordId;
//				CCarray["custparam_planno"] = request.getParameter('custparam_planno');
//				CCarray["custparam_begin_location_id"] = CCRec.getFieldValue('custrecord_cycact_beg_loc');
//				CCarray["custparam_begin_location_name"] = CCRec.getFieldText('custrecord_cycact_beg_loc');
//				CCarray["custparam_lpno"] = CCRec.getFieldValue('custrecord_cyclelpno');
//				CCarray["custparam_packcode"] = CCRec.getFieldValue('custrecord_expcyclepc');
//				CCarray["custparam_expqty"] = CCRec.getFieldValue('custrecord_cycleexp_qty');
//				CCarray["custparam_expitem"] = CCRec.getFieldText('custrecord_cyclesku');
//				CCarray["custparam_expitemId"] = CCRec.getFieldValue('custrecord_cyclesku');
//				CCarray["custparam_expitemdescription"] = CCRec.getFieldValue('custrecord_cycle_skudesc');
//				CCarray["custparam_expitemno"] = CCRec.getFieldValue('custrecord_cyclecount_exp_ebiz_sku_no');
//				CCarray["custparam_noofrecords"] = searchresults.length;	
				
			}

			var optedEvent = request.getParameter('cmdPrevious');
			var optedEvent1 = request.getParameter('cmdSkip');
			var optedEvent2 = request.getParameter('cmdaddnewitem');
			var optedEvent3 = request.getParameter('cmdEmptyLocation');

			if (optedEvent == 'F7') {
				nlapiLogExecution('ERROR', 'Cycle Count Location F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di', false, CCarray);
			}
			else if(optedEvent2=='F10')
			{
				nlapiLogExecution('ERROR', 'Cycle Count AddnewItem Pressed');
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cyc_addnewitem', 'customdeploy_ebiz_rf_cyc_addnewitem_di', false, CCarray);
			}

			else if(optedEvent1=='F8')
			{
				nlapiLogExecution('ERROR','SKIP','SKIPthetask');
				nlapiLogExecution('ERROR', 'getRecordId', CCarray["custparam_recordid"]);

				/*var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'));
				var skipcount=CCRec.getFieldValue('custrecord_cyc_skip_task');
				nlapiLogExecution('ERROR','skipcount',skipcount);

				if(skipcount=='' || skipcount==null)
				{
					skipcount=0;
				}
				skipcount=parseInt(skipcount)+1;
				CCRec.setFieldValue('custrecord_cyc_skip_task',skipcount);
				var id=	nlapiSubmitRecord(CCRec,true);
				nlapiLogExecution('ERROR','skipid',id);*/

				var filters2 = new Array();
				filters2[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getCycleCountPlanNo);
				filters2[1] = new nlobjSearchFilter('custrecord_cycle_act_qty', null, 'isempty');
				filters2[2] = new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof', CCarray["custparam_begin_location_id"]);
				//filters2[3] = new nlobjSearchFilter('internalid', null, 'notequalto', request.getParameter('custparam_recordid'));

				var columns = new Array();
				columns[0]=new nlobjSearchColumn('custrecord_cyc_skip_task');

				var searchresults2 = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters2, columns);
				if(searchresults2 != null && searchresults2 != '')
				{
					for(var p=0;p<searchresults2.length;p++)
					{
						var skipcount=searchresults2[p].getValue('custrecord_cyc_skip_task');
						nlapiLogExecution('ERROR', 'skipcount',skipcount);
						if(skipcount=='' || skipcount==null)
						{
							skipcount=0;
						}
						skipcount=parseInt(skipcount)+1;
						var RecUpdate = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', searchresults2[p].getId());
						RecUpdate.setFieldValue('custrecord_cyc_skip_task',skipcount);
						var recid = nlapiSubmitRecord(RecUpdate, false, true);
						nlapiLogExecution('ERROR', 'updated remaining id',recid);
					}	
				}	

				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
				return;
			}
			else if(optedEvent3=='F9')
			{
				nlapiLogExecution('ERROR','EMPTY LOCATION','EMPTY LOCATION');			

				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_emptyloc', 'customdeploy_rf_cyclecount_emptyloc_di', false, CCarray);
				return;
			}
			else {
				if (request.getParameter('enterlocation') != '' && CCarray["custparam_begin_location_name"] == getCycleCountLocation) {
				//Case# 20149982 starts
					var filter=new Array();
					filter.push(new nlobjSearchFilter('name',null,'is',request.getParameter('enterlocation')));
					filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					var Searchrecord=nlapiSearchRecord('customrecord_ebiznet_location',null,filter,null);
					if(Searchrecord == null || Searchrecord == '')
					{
						nlapiLogExecution('ERROR', 'Bin Location is Inactive stage');
						CCarray["custparam_error"] ='ENTER VALID BIN LOCATION';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						return;
					}
					//Case# 20149982 ends
					nlapiLogExecution('ERROR', 'custparam_begin_location_name', CCarray["custparam_begin_location_name"]);
					var ruleValue=GetSystemRuleForLPRequired();
					CCarray["custparam_ruleValue"] = ruleValue;
					if(ruleValue=='Y')
					{

						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_lp', 'customdeploy_rf_cyclecount_lp_di', false, CCarray);
					}
					else
					{						
						if(CCRec.getFieldValue('custrecord_cyclelpno') == null || CCRec.getFieldValue('custrecord_cyclelpno') == '')
						{
							if(CCarray["custparam_siteid"] != null && CCarray["custparam_siteid"] != "")
							{
								CCarray["custparam_actlp"] = GetMaxLPNo(1, 1,CCarray["custparam_siteid"].toString());
							}
						}
						else
						{
							CCarray["custparam_actlp"] = CCRec.getFieldValue('custrecord_cyclelpno');							
						}
						CCarray["custparam_expqty"] = expecqty;
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_sku', 'customdeploy_rf_cyclecount_sku_di', false, CCarray);						
					}
				}
				else {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					nlapiLogExecution('ERROR', 'Cycle Count Location not found');
				}
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
		}
		nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
	}
}


function GetSystemRuleForLPRequired()
{
	try
	{
		var rulevalue='Y';
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for CycleCount?'));
		
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		
		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);
		
		if(searchresult!=null&&searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForLPRequired',exp);
	}
}
