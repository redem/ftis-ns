/***************************************************************************
??????????????????????????????? eBizNET
??????????????????????? eBizNET Solutions Inc
****************************************************************************
*
*? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_stagerules_CL.js,v $
*? $Revision: 1.5.2.1.8.1 $
*? $Date: 2013/04/04 14:31:59 $
*? $Author: schepuri $
*? $Name: t_NSWMS_2013_1_3_12 $
*
* DESCRIPTION
*? Functionality
*
* REVISION HISTORY
*? $Log: ebiz_stagerules_CL.js,v $
*? Revision 1.5.2.1.8.1  2013/04/04 14:31:59  schepuri
*? standard bundle issue fix
*?
*? Revision 1.5.2.1  2012/02/24 10:31:58  spendyala
*? CASE201112/CR201113/LOG201121
*? Removed unwanted Alert Msgs
*?
*? Revision 1.5  2012/01/06 06:57:36  spendyala
*? CASE201112/CR201113/LOG201121
*? added functionality to check for the duplication of sequence no. with a given site.
*?
*
****************************************************************************/


/**
 * @param type
 * @returns
 */
function fnCheckStageRules(type)
{
	var varRuleID =  nlapiGetFieldValue('name');
	var vCompany	= nlapiGetFieldValue('custrecord_ebizcompanystage');  	 
	var vSite	= nlapiGetFieldValue('custrecord_ebizsitestage');  
	var vSeq = nlapiGetFieldValue('custrecord_sequencenostgrule');

	/*
	 * Added on 1/4/2011 by Phani
	 * The below variable will fetch the internal id of the record selected.
	 */    

	var vId = nlapiGetFieldValue('id');
	if(varRuleID != "" && varRuleID != null &&  vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is',varRuleID);
		//	filters[1] = new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof',[vCompany]);	 
		filters[1] = new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof',[vSite]);

		/*
		 * Added on 1/4/2011 by Phani
		 * The below variable will filter based on the internal id of the record selected.
		 */

		if (vId != null && vId != "") {
			filters[2] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
		}

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters);

		if(searchresults)
		{
			alert("Stage rule already exists.");
			return false;
		}
		else
		{
			var seqValue=getSequenceNo(vSite,vSeq,vId);
			return seqValue;
		} 
	}
	else
	{
		return true;
	}
}

/**
 * This function is used to check weather the sequence no. entered is duplicate or not.
 * @param vSite
 * @param vSeq
 * @param vId
 * @returns {Boolean}
 */
function getSequenceNo(vSite,vSeq,vId)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof',[vSite]);
	filters[1] = new nlobjSearchFilter('custrecord_sequencenostgrule', null, 'equalto',vSeq);


	if (vId != null && vId != "") {
		filters[2] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
	}

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters);
	if(searchresults)
	{
		alert("SequenceNo# already exists.");
		return false;
	}
	else
	{
		return true;
	} 

}

function statusFieldChanged(type, name)
{
//	This particular code is 
//	based on the Cycle count method flag , and disables/enables both standard and custom fields.

	if (name == 'custrecord_directionstgrule')
	{
		var value = nlapiGetFieldText('custrecord_directionstgrule');
		//alert(value);
		/*if (value=='Outbound')
		{
			nlapiDisableField('custrecord_outboundlocationgroup', false);
		}
		else
		{
			nlapiDisableField('custrecord_outboundlocationgroup', true);
		}*/
		if (value=='Outbound')
		{
			//alert('value1new');
			nlapiDisableField('custrecord_outboundlocationgroup', false);
			nlapiDisableField('custrecord_inboundlocationgroupstg', true);
		}
		else if (value=='Inbound')
		{
			//alert('val33ue1');
			nlapiDisableField('custrecord_inboundlocationgroupstg', false);
			nlapiDisableField('custrecord_outboundlocationgroup', true);
		}
		//if (value=='Inventory / Both')
		else
		{
			//alert('2value');
			nlapiDisableField('custrecord_outboundlocationgroup', true);
			nlapiDisableField('custrecord_inboundlocationgroupstg', true);
		}
		
	}
}
