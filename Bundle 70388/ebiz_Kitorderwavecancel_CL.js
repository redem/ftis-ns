/***************************************************************************
������������������������������� � ����������������������������� ��eBizNET
�����������������������                           eBizNET SOLUTIONS LTD
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/Attic/ebiz_Kitorderwavecancel_CL.js,v $
*� $Revision: 1.1.2.2.8.2.4.4 $
*� $Date: 2015/07/01 15:28:02 $
*� $Author: skreddy $
*� $Name: t_eBN_2015_1_StdBundle_1_171 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_Kitorderwavecancel_CL.js,v $
*� Revision 1.1.2.2.8.2.4.4  2015/07/01 15:28:02  skreddy
*� Case# 201413306
*� Signwarehouse SB issue fix
*�
*� Revision 1.1.2.2.8.2.4.3  2015/06/23 15:14:38  grao
*� SW issue fixes  201412953
*�
*� Revision 1.1.2.2.8.2.4.2  2015/04/27 15:22:56  skreddy
*� Case# 201412403
*� True Fab Prod  issue fix
*�
*� Revision 1.1.2.2.8.2.4.1  2015/04/07 09:02:28  snimmakayala
*� 201412229
*�
*� Revision 1.1.2.2.8.2  2013/10/25 20:21:00  snimmakayala
*� GSUSA PROD ISSUE
*� Case# : 20125337
*�
*� Revision 1.1.2.2.8.1  2013/09/23 07:18:52  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue related case#20124483 is resolved.
*�
*� Revision 1.1.2.2  2012/08/16 12:43:27  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue fix as a part of 339 bundle i.e.,
*� Improper Alert statements .
*�
*
****************************************************************************/

/**
 * @param type
 * @param fld
 * @returns {Boolean}
 */
function Kititemvalidation(type, fld)
{
	var lineCnt = nlapiGetLineItemCount('custpage_wavecancellist');
	for (var s = 1; s <= lineCnt; s++) 
	{

		var linecheck= nlapiGetLineItemValue('custpage_wavecancellist','custpage_wavecancel',s);

		if(linecheck=='T')
		{
			var vkititem=nlapiGetLineItemValue('custpage_wavecancellist', 'custpage_kititem',s);
			var vkititemtext=nlapiGetLineItemValue('custpage_wavecancellist', 'custpage_kititemtext',s);
			var memeritem=nlapiGetLineItemValue('custpage_wavecancellist', 'custpage_item',s);
			var fono = nlapiGetLineItemValue('custpage_wavecancellist', 'custpage_fulfillordno',s);
			var Orderno = nlapiGetLineItemValue('custpage_wavecancellist', 'custpage_waveordernovalue',s);
			var memerItemvalue = nlapiGetLineItemValue('custpage_wavecancellist', 'custpage_itemvalue',s);

			//alert(vkititemtext);
			if(vkititem!=null && vkititem!='')
			{

//				var kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');
				var kititemTypesku=nlapiGetLineItemValue('custpage_wavecancellist','custpage_kititemtype',s);

				if(kititemTypesku=='Kit')
				{

					var memberItemsCount=0;
					var filters = new Array(); 			 
					filters[0] = new nlobjSearchFilter('internalid', null, 'is', vkititem);	

					var columns1 = new Array(); 
					columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
					columns1[1] = new nlobjSearchColumn( 'memberquantity' );

					var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 
					if(searchresults!=null && searchresults!='' && searchresults.length>0)
					{
						memberItemsCount=searchresults.length;
					}


					//alert('OpentTaskCount'+OpentTaskCount);
					//alert('memberItemsCount ::'+memberItemsCount);
					var vcount=0;
					for (var t = 1; t <= lineCnt; t++) 
					{
						var vinnkititem=nlapiGetLineItemValue('custpage_wavecancellist', 'custpage_kititem',t);
						var vinnfono = nlapiGetLineItemValue('custpage_wavecancellist', 'custpage_fulfillordno',t);
						var vinnlinecheck= nlapiGetLineItemValue('custpage_wavecancellist','custpage_wavecancel',t);

						if(vinnkititem==vkititem && fono==vinnfono)
						{
							if(vinnlinecheck=='F')
							{
								alert("Select All Component Items in Kit Item:"+vkititemtext);
								return false
							}
							else
							{
								vcount=vcount+1;
							}

						}
					}
					var componentitempicks=0;
					var vcomponentpicks = getcomponentpicks(vkititem,fono);
					if(vcomponentpicks!=null && vcomponentpicks!='')
					{
						componentitempicks=vcomponentpicks.length;
					}

					//alert("memberItemsCount: "+memberItemsCount);
					//alert("componentitempicks: "+componentitempicks);

					if(parseInt(memberItemsCount) != parseInt(componentitempicks))
					{
						//alert('vcount1 ::'+vcount);
						alert("Cann't Cancel Partial Component Items in Kit Item:"+vkititemtext);
						return false;
					}

					//alert('vcount ::'+vcount);
					/*if(parseInt(memberItemsCount) != parseInt(vcount))
					{
						//alert('vcount1 ::'+vcount);
						alert("Cann't Cancel Partial Component Items in Kit Item:"+vkititemtext);
						return false;
					}*/


//					var filters = new Array(); 			 
//					filters[0] = new nlobjSearchFilter('internalid', null, 'is', vkititem);	


//					var columns1 = new Array(); 
//					columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
//					columns1[1] = new nlobjSearchColumn( 'memberquantity' );

//					var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 	

//					if(searchresults!=null && searchresults!='')
//					{

//					for(var w=0; w<searchresults.length;w++) 
//					{
//					fulfilmentItem = searchresults[w].getValue('memberitem');
//					fulfilmentItemtext = searchresults[w].getText('memberitem');
//					memberitemqty = searchresults[w].getValue('memberquantity');


//					if(fulfilmentItemtext==memeritem)
//					{
//					if(linecheck=='F')
//					{
//					alert("Select All Component Items in Kit Item:"+vkititemtext);
//					return false;
//					}
//					else
//					{
//					break;
//					}

//					}
//					}
//					}				
				}
			}
		}
	}
	return true;
}

/**
 * @param type
 * @param name
 * @returns {Boolean}
 */
function changePageValues(type,name)
{	
	if(name=='custpage_selectpage'||name=='custpage_pagesize')
	{
		var sublist=nlapiGetLineItemCount('custpage_wavecancellist');		
		if(sublist!=null)
		{	        	  
			nlapiSetFieldValue('custpage_hiddenfield','F');  
		}
		var transfersublist  = nlapiGetLineItemCount('custpage_replen_items');	 
		if(transfersublist!=null)
		{
			nlapiSetFieldValue('custpage_hiddenfield','F');  
		}
		var putawaygen_locs  = nlapiGetLineItemCount('custpage_items');	 
		if(putawaygen_locs!=null)
		{
			nlapiSetFieldValue('custpage_hiddenfield','F');  
		}
		NLDoMainFormButtonAction("submitter",true);	
	}
	else
	{
		return true;
	}
}

function getcomponentpicks(parentitem,fono)
{
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', [29]))
	SOFilters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null, 'is', parentitem));
	SOFilters.push(new nlobjSearchFilter('name', null, 'is', fono));

	//201413306
	SOColumns.push(new nlobjSearchColumn('custrecord_sku',null,'group'));

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	return SOSearchResults;

}



