/***************************************************************************
?????????????????????????eBizNET
??????????????????eBizNET Solutions Inc
 ****************************************************************************
 *
 *? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_TaskTypeValidation_CL.js,v $
 *? $Revision: 1.1.2.2 $
 *? $Date: 2012/06/15 07:17:36 $
 *? $Author: spendyala $
 *? $Name: t_NSWMS_2012_1_2_1 $
 *
 * DESCRIPTION
 *? Functionality
 *
 * REVISION HISTORY
 *? $Log: ebiz_TaskTypeValidation_CL.js,v $
 *? Revision 1.1.2.2  2012/06/15 07:17:36  spendyala
 *? CASE201112/CR201113/LOG201121
 *? Task type validation issue resolved.
 *?
 *? Revision 1.1.2.1  2012/06/14 06:54:29  gkalla
 *? CASE201112/CR201113/LOG201121
 *? Task type creation validation issue
 *?
 *? Revision 1.1  2011/12/05 07:24:11  spendyala
 *? CASE201112/CR201113/LOG201121
 *? check/validate the tasktype provided by the user.weather the tasktype is already exist or not.
 *?
 *
 ****************************************************************************/

/**
 * tasktype validation is to authenticate user, if the name is already exist.
 * @returns {Boolean}
 */
function TaskTypeValidation() {
	try{
		 
		if(eventtype=='create')
		{	
			var vname=nlapiGetFieldValue('name');
			var filterTaskType=new nlobjSearchFilter('name',null,'is',vname);
			var searchrecord=nlapiSearchRecord('customrecord_ebiznet_tasktype',null,filterTaskType,null);
			if(searchrecord!=null&&searchrecord!="")
			{
				alert('Task Type already EXIST');
				return false;
			}
			else
				return true;
		}
		else
			return true;
	}
	catch(exe)
	{
		nlapiLogExecution('ERROR','Exeception',exe);
	}
}
function  myPageInit(type){
	 
	eventtype=type;
}