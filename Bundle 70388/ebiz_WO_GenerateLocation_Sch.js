/***************************************************************************
eBizNET Solutions Inc
****************************************************************************/
/* 
****************************************************************************
*
*     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WO_GenerateLocation_Sch.js,v $
*     	   $Revision: 1.1.2.2.2.1 $
*     	   $Date: 2015/09/21 14:02:25 $
*     	   $Author: deepshikha $
*
*   eBizNET version and checksum stamp.  Do not remove.
*   $eBiznet_VER: .............. $eBizNET_SUM: .....
* PRAMETERS
*
*
* DESCRIPTION
*
*  	Default Data for Interfaces
*
* NOTES AND WARNINGS
*
* INITATED FROM
*
* REVISION HISTORY
*  $Log: ebiz_WO_GenerateLocation_Sch.js,v $
*  Revision 1.1.2.2.2.1  2015/09/21 14:02:25  deepshikha
*  2015.2 issueFix
*  201414466
*
*  Revision 1.1.2.2  2015/04/13 09:25:57  rrpulicherla
*  Case#201412277
*
*  Revision 1.1.2.1  2014/09/26 15:24:09  skavuri
*  Case# 201410092 jawbone cr
*
*  Revision 1.3.2.11.4.4.4.13  2014/04/09 06:03:46  skreddy
*  case 20127925
*  LL sb  issue fix
*
*
*****************************************************************************/

function GenerateLocationsPOSTRequestSch(type)
{
	try
	{
		var woidname='';
		var woidno='';
		var context = nlapiGetContext(); 
		nlapiLogExecution('DEBUG', 'type', type);
		nlapiLogExecution('DEBUG', 'Into GenerateLocationsPOSTRequest', TimeStampinSec());

		//woidname = context.getSetting('SCRIPT', 'custscript_workorder');
		//woidno = context.getSetting('SCRIPT', 'custscript_woidno');
		//woidname = request.getParameter('custscript_workorder');
		//woidno = request.getParameter('custscript_woidno');
		woidname = context.getSetting('SCRIPT', 'custscript_workorder');
		woidno = context.getSetting('SCRIPT', 'custscript_woidno');

		nlapiLogExecution('Debug', 'Time Stamp at the start of scheduler',TimeStampinSec());

		nlapiLogExecution('Debug','Remaining usage at the start',context.getRemainingUsage());

		
		var str = 'woidno.' + woidno + '<br>';
		str = str + 'woidname.' + woidname + '<br>';

		nlapiLogExecution('DEBUG', 'WO Values', str);


			nlapiLogExecution('ERROR', 'Time Stamp before CheckAvailability',TimeStampinSec());
			CheckAvailability(woidno);
			nlapiLogExecution('ERROR', 'Time Stamp after CheckAvailability',TimeStampinSec());
			//AllocateInv(woidno,request);
			//response.writePage(form);

		nlapiLogExecution('ERROR', 'Out of GenerateLocationsPOSTRequest',TimeStampinSec());
	}
	catch(exp1) 
	{
		nlapiLogExecution('Debug', 'Exception in WorkGenerationSchScript1 : ', exp1);	
	}
}

/*var form = nlapiCreateForm('Generate Bin Location');
	var vitem = request.getParameter('cusppage_item');		
	var vloc  = request.getParameter('cusppage_site');
	var vqty  = request.getParameter('custpage_kitqty');
	var vdate = request.getParameter('custpage_sodate');
	var vmemo = request.getParameter('custpage_notes');
	var vlp   = request.getParameter('custpage_lp');
	var vheaderlotno=request.getParameter('custpage_lotnoserialno');
	//var vheaderlotnoVal;
	var vbinloc = request.getParameter('custpage_binlocation');
	var vBeginDate=request.getParameter('custpage_bgdate');
	var vBeginTime=request.getParameter('custpage_bgtime');
	var vSointernalId=request.getParameter('custpage_sointernalid');


	//var vaccountno = request.getParameter('cusppage_accountno');

	//Create Work Order 
	var Wo=nlapiCreateRecord('workorder');
	Wo.setFieldValue('assemblyitem',vitem);
	Wo.setFieldValue('location', vloc);			
	Wo.setFieldValue('quantity', vqty);
	Wo.setFieldValue('trandate', vdate);
	Wo.setFieldValue('createdfrom', vSointernalId);
	var lineCnt = request.getLineItemCount('custpage_deliveryord_items');

	for (var k = 1; k <= lineCnt; k++) 
	{				
		var vitem = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
		var vLocation = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', k);
		var vlp =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_lp', k);
		var vQty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);
		var vLotNo =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', k);

		nlapiLogExecution('ERROR', 'vLocation',vLocation);
		nlapiLogExecution('ERROR', 'vlp',vlp);	
		nlapiLogExecution('ERROR', 'vQty',vQty);	
		Wo.setLineItemValue('item', 'item', k,vitem);
		Wo.setLineItemValue('item', 'quantity', k,vQty) ;
		Wo.setLineItemValue('item', 'serialnumbers', k,vLotNo + "(" + vQty + ")") ;

		Wo.setLineItemValue('item', 'custcol_locationsitemreceipt', k,vLocation);
		Wo.setLineItemValue('item', 'custcol_ebiz_lp', k,vlp) ;

		Wo.selectLineItem('item', k);
		//Wo.commitLineItem('item');

	}

	var woid=nlapiSubmitRecord(Wo);
	nlapiLogExecution('ERROR', 'Outside If=', woid); 

	var filters = new Array();			   

	filters[0] = new nlobjSearchFilter('internalid', null, 'is', woid);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');			


	var searchResults = nlapiSearchRecord('workorder', null, filters, columns);
	var wovalue;
	if(searchResults!= null && searchResults != "" && searchResults.length >0)
	{
		wovalue=searchResults[0].getValue('tranid');
	}

	showInlineMessage(form, 'Confirmation', 'Work Order Created', wovalue);	

 */





function CheckAvailability(woidno)
{
	nlapiLogExecution('ERROR', 'Into Check Availablity '); 
	var vMessage= fnGetDetails(woidno);

	if(vMessage== 'SUCCESS') 
	{ 
		return true;
	} 
	else
	{
		if(vMessage!= null && vMessage != "")
		{
			var vArrMsg=vMessage.split('~');
			if(vArrMsg.length>1)
			{
				nlapiLogExecution('ERROR','Allocation Failed ',vArrMsg[1]);
				var ErrorMsg = nlapiCreateError('CannotAllocate',vArrMsg[1], true);
				throw ErrorMsg; //throw this error object, do not catch it
			}
			else
			{
				nlapiLogExecution('ERROR','eBiz Allocation Failed ');
				var ErrorMsg = nlapiCreateError('CannotAllocate','Allocation Failed', true);
				throw ErrorMsg; //throw this error object, do not catch it
			}	
		}
		return false;
	}

}


function fnGetDetails(vId)
{
	
	var context = nlapiGetContext(); 
	nlapiLogExecution('ERROR','Into Get Details');
	//var vId = request.getParameter('custpage_workorder');
	var SearchresultsArray = new Array();//added by santosh
	nlapiLogExecution('Debug','Time stamp 3',TimeStampinSec());
	var searchresults = nlapiLoadRecord('workorder', vId);
	nlapiLogExecution('Debug','Time stamp 4',TimeStampinSec());
	var itemcount= searchresults.getLineItemCount('item');
	var vWOId=searchresults.getFieldValue('tranid');
	var SkuNo=searchresults.getFieldValue('assemblyitem');
	var vordertype=searchresults.getFieldValue('custbody_nswmssoordertype');
	nlapiLogExecution('Debug','vordertype',vordertype);
	var varAssemblyQty=searchresults.getFieldValue('quantity');
	var vLocation= searchresults.getFieldValue('location');
	var vWMSlocation=searchresults.getFieldValue('location');
	var qty=0;
	if(qty1!= null && qty1!="" && parseFloat(qty1)>0)
		qty=qty1;

	//nlapiLogExecution('ERROR', 'vId', vId);
	//nlapiLogExecution('ERROR', 'vWOId', vWOId);
	/*var searchresults = nlapiLoadRecord('workorder', vId);
	if(searchresults !=null && searchresults != "" && searchresults.length>0)
		vWOId=searchresults.getFieldValue('tranid');
	nlapiLogExecution('ERROR', 'vWOId', vWOId);*/

	nlapiLogExecution('ERROR', 'ItemCount', itemcount);
	//nlapiLogExecution('ERROR', 'assemItem', assemItem);



	var vInvReturn=new Array();
	//alert("3");
	var strItem="",strQty=0,vcomponent,vlineno=0;

	/*	var ItemType = nlapiLookupField('item', vitem, 'recordType');=0
	nlapiLogExecution('ERROR','ItemType',ItemType);
	var searchresultsitem;
	if(ItemType!=null && ItemType !='')

	{
		nlapiLogExecution('ERROR','Item Type',ItemType);
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);

		searchresultsitem = nlapiLoadRecord(ItemType, vitem); //1020
	}

	else
	{
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
	}
	var SkuNo=searchresultsitem.getFieldValue('itemid');*/
	var recCount=0; 
	//Written by Ganesh
	//alert("ItemCount : "+ itemcount);
	var vAlert='';
	//nlapiLogExecution('ERROR','itemcount ',itemcount);
	var vItemArr=new Array();

	for(var s=1; s<=itemcount;s++)
	{
		var vCompItem = searchresults.getLineItemValue('item', 'item', s);
		var CompItemType = searchresults.getLineItemValue('item', 'itemtype', s);
		//nlapiLogExecution('ERROR','CompItemType',CompItemType);
		var vCommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', s);

		if(CompItemType != 'Service' && CompItemType != 'NonInvtPart' && vItemArr.indexOf(vCompItem) == -1 && (vCommittedQty !=null && vCommittedQty !='' && vCommittedQty !=0))
			vItemArr.push(vCompItem);
	}
	nlapiLogExecution('Debug','Time stamp 5',TimeStampinSec());
	if(vItemArr.length>0)
	{
		var filters2 = new Array();

		var columns2 = new Array();
		columns2[0] = new nlobjSearchColumn('custitem_item_family');
		columns2[1] = new nlobjSearchColumn('custitem_item_group');
		columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
		columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
		columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
		columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
		columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

		filters2.push(new nlobjSearchFilter('internalid', null, 'is', vItemArr));

		var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

		var filters3=new Array();
		filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		if(vordertype!=null&&vordertype!="")
			filters3.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@',vordertype]));

		var columns3=new Array();

		columns3[0] = new nlobjSearchColumn('name');
		columns3[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
		columns3[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
		//columns3[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
		columns3[3]=new nlobjSearchColumn('formulanumeric');
		columns3[3].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();
		columns3[4] = new nlobjSearchColumn('custrecord_ebizruleidpick');
		columns3[5] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
		columns3[6] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
		columns3[7] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');

		//columns3[3].setSort();
		var pickrulesarray = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters3, columns3);
	}
	
	var pfLocationResults = getPFLocationsForOrder(vWMSlocation,vItemArr);
	
	nlapiLogExecution('Debug','Time stamp 6',TimeStampinSec());
	for(var m=1; m<=itemcount;m++) 
	{
		var serialNo="";
		//var vBatch="";
		//alert("SkuNo" + SkuNo);
		strItem = searchresults.getLineItemValue('item', 'item', m);
		//alert("strItem" + strItem);
		strQty = searchresults.getLineItemValue('item', 'quantity', m);
		vlineno = searchresults.getLineItemValue('item','line',m);
		nlapiLogExecution('ERROR', 'vlineno', vlineno);
		var CompItemType = searchresults.getLineItemValue('item', 'itemtype', m);
		nlapiLogExecution('ERROR','CompItemType',CompItemType);
		var CommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', m);
		if(CommittedQty !=null && CommittedQty !='' && CommittedQty!=0)
			strQty=CommittedQty;
		if(CompItemType != 'Service' && CompItemType != 'NonInvtPart' && (CommittedQty !=null && CommittedQty !='' && CommittedQty !=0))
		{
			var Lotno = searchresults.getLineItemValue('item', 'serialnumbers', m);
			nlapiLogExecution('ERROR','Lotno',Lotno);
			if(Lotno== null || Lotno =="")
				Lotno="";

			var LotnoArray = Lotno.split('');
			if(LotnoArray.length>0)
			{
				nlapiLogExecution('ERROR', 'LotnoArray.length',LotnoArray.length);
				var vitemqty=0;
				var vrecid="" ;
				var vlp="",vlocation="",vqty="",vlotnoWithQty="",vremainqty="",location,vlocationText="";
				var vMultipleLp="",vMultipleQty="",vMultipleTempLot="",vMultipleLocation="",vMultipleRecid="";
				var vTempLot="";
				for(var z=0; z<LotnoArray.length;z++)
				{				
					nlapiLogExecution('ERROR', 'Z',z);
					var vBatch="";
					var vQty="";
					var vLotname="";
					var vLotno="";
					if(LotnoArray.length>1)// for multiple Lotno s
					{
						var vLotNumber=LotnoArray[z];      // ex:ebiz(1),batch(1)
						nlapiLogExecution('ERROR', 'LotNumber',vLotNumber);

						var pos = vLotNumber.indexOf("(")+1; 
						nlapiLogExecution('ERROR', 'pos ', pos);

						vQty=vLotNumber.slice(pos, -1);
						nlapiLogExecution('ERROR', 'vQty', vQty); //ex:1

						vLotname=vLotNumber.slice(0, pos-1);
						nlapiLogExecution('ERROR', 'vLotname', vLotname); //Ex:ebiz

					}
					else
					{
						vLotname=Lotno;
						vQty=strQty;
					}

					//Get Lotno Id;
					if(vLotname!=null && vLotname!='')
					{
						nlapiLogExecution('Debug','Time stamp 7',TimeStampinSec());
						var filterbatch = new Array();
						filterbatch[0] = new nlobjSearchFilter('name', null, 'is', vLotname);
						filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', strItem);
						var Lotnosearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch);
						nlapiLogExecution('Debug','Time stamp 8',TimeStampinSec());
						if(Lotnosearchresults!=null)
						{
							vLotno= Lotnosearchresults[0].getId();
						}
					}

					
					//var lineCount = request.getLineItemCount('custpage_deliveryord_items');
					var lineCount= context.getSetting('SCRIPT', 'custscript_linecount');
					nlapiLogExecution('ERROR','lineCount',lineCount);
					
					vBatch=vLotno;
					
					var vSkuArray = new Array();
					vSkuArray = context.getSetting('SCRIPT', 'custscript_vskuarray');
					if(vSkuArray !=null && vSkuArray !='' && vSkuArray.length>0)
					vSkuArray =vSkuArray.Split(',');
					
					var vOldLOTnoArray = new Array();
					vOldLOTnoArray = context.getSetting('SCRIPT', 'custscript_voldlotnoarray');
					if(vOldLOTnoArray !=null && vOldLOTnoArray !='' && vOldLOTnoArray.length>0)
					vOldLOTnoArray =vOldLOTnoArray.Split(',');
					
					var vNewLOTnoArray = new Array();
					vNewLOTnoArray = context.getSetting('SCRIPT', 'custscript_vnewlotnoarray');
					if(vNewLOTnoArray !=null && vNewLOTnoArray !='' && vNewLOTnoArray.length>0)
					vNewLOTnoArray =vNewLOTnoArray.Split(',');
					
					var gridLinenoArray= new Array();
					gridLinenoArray = context.getSetting('SCRIPT', 'custscript_gridlinenoarray');
					if(gridLinenoArray !=null && gridLinenoArray !='' && gridLinenoArray.length>0)
					gridLinenoArray =gridLinenoArray.Split(',');
					
					var vCommentsArray = new Array();
					vCommentsArray = context.getSetting('SCRIPT', 'custscript_vcommentsarray');
					if(vCommentsArray !=null && vCommentsArray !='' && vCommentsArray.length>0)
					vCommentsArray =vCommentsArray.Split(',');
					
					var str1 = 'vSkuArray.' + vSkuArray + '<br>';
					str1 = str1 + 'vOldLOTnoArray. ' + vOldLOTnoArray + '<br>';	
					str1 = str1 + 'vNewLOTnoArray. ' + vNewLOTnoArray + '<br>';	
					str1 = str1 + 'gridLinenoArray. ' + gridLinenoArray + '<br>';	
					str1 = str1 + 'vCommentsArray. ' + vCommentsArray + '<br>';	
					nlapiLogExecution('DEBUG', 'Lot Values', str1);
					//for(var k=1;k<=lineCount;k++){
					for(var k=0;k<lineCount;k++){
						nlapiLogExecution('ERROR','into for loop',k);	
						var vOldLOTno='';
						var vNewLOTno='';
						var vComments='';

						var vSku = vSkuArray[k];
						if(vOldLOTnoArray !=null && vOldLOTnoArray !='' && vOldLOTnoArray.length>0)
						vOldLOTno =vOldLOTnoArray[k];
						if(vNewLOTnoArray !=null && vNewLOTnoArray !='' && vNewLOTnoArray.length>0)
						vNewLOTno =vNewLOTnoArray[k];
						var gridLineno=gridLinenoArray[k];
						if(vCommentsArray !=null && vCommentsArray !='' && vCommentsArray.length>0)
						vComments = vCommentsArray[k];
						if(vOldLOTno=="" ||vOldLOTno==null)
						{
							vOldLOTno="";
						}
						if(vLotno=="" ||vLotno==null)
						{
							vLotno="";
						}
						if(vNewLOTno=="" ||vNewLOTno==null)
						{
							vNewLOTno="";
						}

						var str = 'vSku.' + vSku + '<br>';
						str = str + 'vNewLOTno. ' + vNewLOTno + '<br>';	
						str = str + 'vOldLOTno. ' + vOldLOTno + '<br>';	
						str = str + 'gridLineno. ' + gridLineno + '<br>';	
						str = str + 'vComments. ' + vComments + '<br>';	

						nlapiLogExecution('DEBUG', 'Lot Values', str);

						if((vNewLOTno!=vLotno) && (gridLineno==vlineno) && (strItem==vSku) && (vOldLOTno==vLotno))
						{
							vBatch=vNewLOTno;
							nlapiLogExecution('ERROR','Lot# s are not same',vBatch);
							break;
						}
//						else
//						{
//						vBatch=vLotno;
//						}

					}
					nlapiLogExecution('Debug','Time stamp 9',TimeStampinSec());
					var fields = ['custitem_item_family', 'custitem_item_group','recordType','custitem_ebizbatchlot'];
					
					
					var columns = nlapiLookupField('item', strItem, fields);
					var vItemType="";
					var batchflag="F";
					nlapiLogExecution('Debug','Time stamp 10',TimeStampinSec());
					if (columns != null && columns !='')
					{
						//nlapiLogExecution('ERROR','Hi2 ');
						//var columns = nlapiLookupField(ComponentItemType, strItem, fields);

						nlapiLogExecution('Debug','Time stamp 11',TimeStampinSec());
						var itemfamily = columns.custitem_item_family;
						var itemgroup = columns.custitem_item_group;
						vItemType = columns.recordType;
						batchflag= columns.custitem_ebizbatchlot;
					}
					//avlqty= (parseFloat(strQty)*parseFloat(varAssemblyQty));
					//alert("avlqty " + avlqty);
					//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, avlqty);
					//nlapiLogExecution('ERROR','Hi3 ');

					//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, strQty,vLocation,vBatch);

					/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/	
					// to get PickfaceLocaion details for the Item
					nlapiLogExecution('Debug','Time stamp before getPFLocationsForOrder',TimeStampinSec());
					//var pfLocationResults = getPFLocationsForOrder(vWMSlocation,strItem);
					nlapiLogExecution('Debug','Time stamp after getPFLocationsForOrder',TimeStampinSec());
					// PickfaceLocationResults added as a parameter to PickStrategieKittoStockNew function
					var arryinvt=PickStrategieKittoStockNew(strItem,vQty,vLocation,ItemInfoResults,pickrulesarray,vBatch,
							pfLocationResults,vItemType,batchflag);
					nlapiLogExecution('Debug','Time stamp after PickStrategieKittoStockNew',TimeStampinSec());
					/*** up to here ***/
					vitemText=searchresults.getLineItemText('item', 'item', m);
					if(arryinvt.length>0)
					{
						nlapiLogExecution('ERROR','arryinvt.length',arryinvt.length);
						var vLot="";
						var vPrevLot="";
						var vLotQty=0;
						var vPickzone=''
							var vPickStrategy='';
						var vPickMethod='';
						//var vTempLot="";
						var vBoolFound=false;
						for (j=0; j < arryinvt.length; j++) 
						{			
							var invtarray= arryinvt[j];  	

							//vitem =searchresults[i].getValue('memberitem');		
							//vitemText =searchresults[i].getText('memberitem');


							if(vlp == "" || vlp == null)
								vlp = invtarray[2];
							else
							{
								vlp = vlp+","+ invtarray[2];
								vMultipleLp=invtarray[2];
							}
							if(vTempLot == "" || vTempLot == null)
								vTempLot = invtarray[4];
							else
							{
								vTempLot = vTempLot+","+ invtarray[4];
								vMultipleTempLot=invtarray[4];
								nlapiLogExecution('ERROR','vMultipleTempLot',vMultipleTempLot);
							}
							if(vPrevLot==null || vPrevLot== "")
							{
								vPrevLot=invtarray[4];
								vLotQty=parseFloat(invtarray[0]);
								vLot=invtarray[4];
								vBoolFound=true;
							}
							else if(vPrevLot==invtarray[4])
							{
								vLotQty=vLotQty+ parseFloat(invtarray[0]);						
							}
							else if(vPrevLot!=invtarray[4])
							{
								if(vlotnoWithQty == "" || vlotnoWithQty == null)
									vlotnoWithQty = vPrevLot+"(" + vLotQty +")";//invtarray[7] is lotno id
								else
									vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + vPrevLot +"(" + vLotQty +")";

								if(vLot == "" || vLot == null)
									vLot = invtarray[4];
								else
									vLot = vLot+","+ invtarray[4];

								vPrevLot=invtarray[4];
								vLotQty=parseFloat(invtarray[0]);

							}
							if(vqty == "" || vqty == null)
								vqty = invtarray[0];
							else
							{
								vqty = vqty+","+ invtarray[0];
								vMultipleQty=invtarray[0];
							}

							var qty1 = invtarray[0];

							if(vlocationText == "" || vlocationText == null)
								vlocationText = invtarray[6];
							else
								vlocationText = vlocationText+","+ invtarray[6];

							if(vlocation == "" || vlocation == null)
								vlocation = invtarray[1];
							else
							{
								vlocation = vlocation+","+ invtarray[1];
								vMultipleLocation=invtarray[1];
							}

							nlapiLogExecution('ERROR','invtarray[3] ',invtarray[3]);

							if(vrecid == "" || vrecid == null)
								vrecid = invtarray[3];
							else
							{
								vrecid = vrecid+","+ invtarray[3];
								vMultipleRecid=invtarray[3];
							}



							if(vremainqty == "" || vremainqty == null)
								vremainqty = invtarray[5];
							else
								vremainqty = vremainqty+","+ invtarray[5];
							location = invtarray[1];
							vitemqty=parseFloat(vitemqty)+parseFloat(qty1);

							vPickzone=invtarray[8];
							vPickStrategy=invtarray[9];
							vPickMethod=invtarray[10]

							if(vMultipleLp!=""||vMultipleQty!=""||vMultipleTempLot!=""||vMultipleLocation!=""||vMultipleRecid!="")
							{
								nlapiLogExecution('ERROR', 'If_muliple ','sucess');
								var CurrentRow=[vId,vMultipleRecid,vWOId,strItem,vMultipleLp,vMultipleQty,vMultipleTempLot,vMultipleLocation,vWMSlocation,vComments,vlineno,vPickzone,vPickStrategy,vPickMethod];
								SearchresultsArray.push(CurrentRow);
								nlapiLogExecution('ERROR', 'CurrentRow for multiple ',CurrentRow);

							}
							else
							{
								nlapiLogExecution('ERROR', 'else_muliple','sucess');
								var CurrentRow=[vId,vrecid,vWOId,strItem,vlp,vqty,vTempLot,vlocation,vWMSlocation,vComments,vlineno,vPickzone,vPickStrategy,vPickMethod];
								SearchresultsArray.push(CurrentRow);
								nlapiLogExecution('ERROR', 'CurrentRow ',CurrentRow);

							}				
						}
						nlapiLogExecution('Debug','Time stamp 15',TimeStampinSec());
						if(vBoolFound==true)
						{
							/*if(vlotnoWithQty == "" || vlotnoWithQty == null)
							vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";//invtarray[7] is lotno id
						else
							vlotnoWithQty = vlotnoWithQty+","+ invtarray[4]+"(" + vLotQty +")";*/

							if(vlotnoWithQty == "" || vlotnoWithQty == null)
								vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";
							else
								vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + invtarray[4]+"(" + vLotQty +")";


							//alert("vTempLot" + vTempLot);
							//alert("vLot.split(',').length" + vLot.split(',').length);
							// alert("vLotQty " + vlotnoWithQty);				
							nlapiLogExecution('ERROR', 'vLot ', vLot);
							nlapiLogExecution('ERROR', 'vlotnoWithQty ', vlotnoWithQty);
							if(vLot.split(',').length>1)
								vLot=vlotnoWithQty;
						}
						nlapiLogExecution('ERROR', 'EndOf for Loop ', 'FORLoopEND');
					}
					else
					{
						// case # 20123356 start
						if(LotnoArray.length>1)
						{
							vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";
							nlapiLogExecution('ERROR', 'if muliple lot#s for Item,no Inventory for ',vitemText);
						}
						// case # 20123356 end				
					}
					nlapiLogExecution('Debug','Time stamp 16',TimeStampinSec());

				}//end of lotnoarray forloop

				if(strQty > vitemqty )			
				{ 
					vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";

				}
				else 
				{
					/*alert("vlp" + vlp);
					alert("location" + location);
					alert("vrecid" + vrecid);
					alert("vLot" + vLot);
					alert("vTempLot" + vTempLot);
					alert("vqty" + vqty);
					alert("vlocation" + vlocation);*/


					/*var filters = new Array();
					filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
					filters.push(new nlobjSearchFilter('status', null, 'is', 'WorkOrd:B'));
					filters.push(new nlobjSearchFilter('line', null, 'is', m));
					filters.push(new nlobjSearchFilter('id', null, 'is', m));


					var searchResultsWO = nlapiSearchRecord('workorder', null, filters, null);*/

					//nlapiSelectLineItem('item',m);
					//if(searchResultsWO != null && searchResultsWO != "")
					{
						try {

							//var InvValues = [vlp, location, vrecid, vLot,vTempLot,vqty,vlocation,vitemText,strItem,strQty];
							//vInvReturn.push(InvValues);
							/*searchResults.nlapiSetCurrentLineItemValue('item','custcol_ebiz_lp',vlp);
							nlapiSetCurrentLineItemValue('item','custcol_locationsitemreceipt', location);//previously it is vlocation
							nlapiSetCurrentLineItemValue('item','custcol_ebizwoinventoryref', vrecid);
							//alert("vLot "+ vLot);
							//nlapiSetCurrentLineItemValue('item','serialnumbers', vLot);
							// For default Lot
							vLot=vitemText.replace(/ /g,"-");
							//alert("vLot "+ vLot);
							nlapiSetCurrentLineItemValue('item','serialnumbers', vLot);
							//nlapiSetCurrentLineItemValue('item','serialnumbers_display', vLot);					
							var newLot = vLot.split(','); 
						newLot = newLot.join(String.fromCharCode(5)); 
						nlapiSetCurrentLineItemValue('item','serialnumbers', newLot);

							var newLot = vLot.split(','); 
							newLot = newLot.join(String.fromCharCode(5)); 
							nlapiLogExecution('ERROR', 'Before Set Lot no', newLot);
							nlapiSetCurrentLineItemValue('item','serialnumbers', newLot);
							nlapiLogExecution('ERROR', 'After Set Lot no', newLot);
							if(vTempLot != null && vTempLot != "")
								nlapiSetCurrentLineItemValue('item','custcol_ebizwolotinfo', vTempLot);
							nlapiSetCurrentLineItemValue('item','custcol_ebizwoavalqty', vqty);
							nlapiSetCurrentLineItemValue('item','custcol_ebizwobinloc', vlocation);
							nlapiSetCurrentLineItemValue('item','custcol_ebizwolotlineno', m);
							nlapiCommitLineItem('item');
							nlapiLogExecution('ERROR', 'linelot ', nlapiGetCurrentLineItemValue('item','custcol_ebizwolotlineno'));
							nlapiLogExecution('ERROR', 'Committed ', 'Success');*/
//							nlapiLogExecution('ERROR', 'vrecid ', vrecid);
//							nlapiLogExecution('ERROR', 'vlp ', vlp);
//							nlapiLogExecution('ERROR', 'location ', location);
//							nlapiLogExecution('ERROR', 'vTempLot ', vTempLot);
							searchresults.setLineItemValue('item','custcol_ebiz_lp',m,vlp);
							searchresults.setLineItemValue('item','custcol_locationsitemreceipt',m, location);//previously it is vlocation
							searchresults.setLineItemValue('item','custcol_ebizwoinventoryref', m,vrecid);
							nlapiLogExecution('ERROR', 'After ', 'After');

							var confirmLotToNS='Y';
							nlapiLogExecution('Debug','Time stamp 17',TimeStampinSec());
							confirmLotToNS=GetConfirmLotToNS(location);
							nlapiLogExecution('Debug','Time stamp 18',TimeStampinSec());
							if(confirmLotToNS=='N')
								vLot=vitemText.replace(/ /g,"-");

							nlapiLogExecution('ERROR', 'vItemType',vItemType);

							nlapiLogExecution('ERROR', 'confirmLotToNS',confirmLotToNS);
							nlapiLogExecution('ERROR', 'vLot',vLot);				 
							//if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T" )
							if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
								searchresults.setLineItemValue('item','serialnumbers',m, vLot);


							/*var newLot = vLot.split(','); 
							newLot = newLot.join(String.fromCharCode(5)); 
							nlapiLogExecution('ERROR', 'Before Set Lot no',m, newLot);
							searchresults.setLineItemValue('item','serialnumbers',m, newLot);*/
							//nlapiLogExecution('ERROR', 'After Set Lot no', newLot);

							//nlapiLogExecution('ERROR', 'invref ', searchresults.getLineItemValue('item', 'custcol_ebizwoinventoryref', m));

							searchresults.setFieldValue('custbody_ebiz_update_confirm','T');
							//if((vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T") && vTempLot != null && vTempLot != "")
							if((vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem") && vTempLot != null && vTempLot != "")
								searchresults.setLineItemValue('item','custcol_ebizwolotinfo',m, vTempLot);

							searchresults.setLineItemValue('item','custcol_ebizwoavalqty',m, vqty);
							searchresults.setLineItemValue('item','custcol_ebizwobinloc',m, vlocation);
							searchresults.setLineItemValue('item','custcol_ebizwobinlocvalue',m, vlocationText);						
							searchresults.setLineItemValue('item','custcol_ebizwolotlineno', m,m);

							//nlapiCommitLineItem('item');
							nlapiLogExecution('ERROR', 'linelot ', searchresults.getLineItemValue('item','custcol_ebizwolotlineno',m));


						}
						catch (e) {

							if (e instanceof nlobjError) 
							{
								nlapiLogExecution('ERROR', 'system error', e.getCode()+ '\n' + e.getDetails());

							}

							else 
							{ 
								nlapiLogExecution('ERROR', 'unexpected error', e.toString());
								//alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
							}
//							nlapiCancelLineItem('item');

							nlapiLogExecution('ERROR','Error ', e.toString());
							var ErrorMsg = nlapiCreateError('CannotAllocate',e.toString(), true);
							throw ErrorMsg;

							break;

						}
					}
				}


			}

		}
	}
	if(vAlert!=null && vAlert!='')
	{ 
		/*var vErrorRet=new Array();
		vErrorRet.push('FAIL');
		vErrorRet.push(vAlert);
		return vErrorRet;*/
		return 'FAIL~' + vAlert;
	}
	else
	{
		nlapiLogExecution('ERROR', 'SearchresultsArray',SearchresultsArray.length);
		//nlapiLogExecution('ERROR', 'Before commit ');
		//nlapiLogExecution('Debug','Time stamp 19',TimeStampinSec());
		var CommittedId = nlapiSubmitRecord(searchresults, true);
		nlapiLogExecution('Debug','Time stamp before AllocateInv',TimeStampinSec());
		AllocateInv(vId,SearchresultsArray,vWOId);
		nlapiLogExecution('Debug','Time stamp after AllocateInv',TimeStampinSec());
		//nlapiLogExecution('ERROR', 'CommittedId ', CommittedId);
		//nlapiLogExecution('ERROR', 'Committed ', 'Success');
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at End',context.getRemainingUsage());
		return 'SUCCESS';
	}
}
/**To get confirm LOT toNS based on system rule
 * 
 * @param Site
 * @returns Y or N
 */
function GetConfirmLotToNS(Site)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'WMSLOT');
		filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('ERROR', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}

function AllocateInv(vId,SearchresultsArray,vWOName)
{
	// Create a Record in Opentask
	var now = new Date();
	//a Date object to be used for a random value
	var now = new Date();
	//now= now.getHours();
	//Getting time in hh:mm tt format.
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	nlapiLogExecution('Debug','Time stamp 21',TimeStampinSec());
	if(SearchresultsArray!=null && SearchresultsArray!='' && SearchresultsArray.length>0)
	{
		nlapiLogExecution('ERROR', 'SearchresultsArray',SearchresultsArray.length);
		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();

		for (var i=0; i<SearchresultsArray.length; i++){
			var vId=SearchresultsArray[i][0];
			var vLineRec=SearchresultsArray[i][1];
			var vWOId=SearchresultsArray[i][2];
			var varsku=SearchresultsArray[i][3];
			var vLP=SearchresultsArray[i][4];
			var vLineQty=SearchresultsArray[i][5];
			var vBatch=SearchresultsArray[i][6];
			var vactLocationtext=SearchresultsArray[i][7];
			var vLocation=SearchresultsArray[i][8];
			var vComments=SearchresultsArray[i][9];
			var vlineno=SearchresultsArray[i][10];
			var vpickzone=SearchresultsArray[i][11];
			var vpickruleId=SearchresultsArray[i][12];
			var vpickmethod=SearchresultsArray[i][13];

			var str = 'vId.' + vId + '<br>';
			str = str + 'vLineRec.' + vLineRec + '<br>';	
			str = str + 'varsku. ' + varsku + '<br>';	
			str = str + 'vBatch. ' + vBatch + '<br>';	
			str = str + 'vactLocationtext. ' + vactLocationtext + '<br>';	
			str = str + 'vComments. ' + vComments + '<br>';	
			str = str + 'vLineQty. ' + vLineQty + '<br>';	
			str = str + 'vlineno. ' + vlineno + '<br>';	
			str = str + 'vpickzone. ' + vpickzone + '<br>';	
			str = str + 'vpickruleId. ' + vpickruleId + '<br>';	
			str = str + 'vpickmethod. ' + vpickmethod + '<br>';	


			nlapiLogExecution('DEBUG', 'SearchresultsArray values', str);


			nlapiLogExecution('Debug','Time stamp 22',TimeStampinSec());
			var scount=1;
			LABL1: for(var k=0;k<scount;k++)
			{
				try
				{
					var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vLineRec);
					nlapiLogExecution('Debug','Time stamp 23',TimeStampinSec());
					var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
					var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
					var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
					var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
					var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

					nlapiLogExecution('ERROR', 'qty', qty);
					nlapiLogExecution('ERROR', 'allocqty', allocqty);
					if(allocqty == null || allocqty == '')
						allocqty=0;
					nlapiLogExecution('ERROR', 'varqty', vLineQty);
					transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)+ parseFloat(vLineQty)).toFixed(5));
					transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');			 
					transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
					nlapiSubmitRecord(transaction, false, true);
					nlapiLogExecution('Debug','Time stamp 24',TimeStampinSec());
				}
				catch(ex)
				{
					nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					}  

					nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}

			if(vWOName == null || vWOName == '')
			{
				var rcptrecordid3 = nlapiLoadRecord('workorder', vId);

				var woidWO= rcptrecordid3.getFieldValue('tranid');
				nlapiLogExecution('ERROR','woidWO  ',woidWO);
				vWOId=woidWO;
			}
			else
				vWOId=vWOName;
			nlapiLogExecution('Debug','Time stamp 25',TimeStampinSec());
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
			customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP 
			//customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
			customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
			customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));

			//customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
			//customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
			//customrecord.setFieldValue('custrecord_expe_qty', varqty); 
			//nlapiLogExecution('ERROR','expe qty ',vLineQty);
			nlapiLogExecution('ERROR','binlocation',vactLocationtext);

			customrecord.setFieldValue('custrecord_expe_qty', parseFloat(vLineQty).toFixed(5));  //Commented and added to resovle decimal issue
			customrecord.setFieldValue('custrecord_act_qty', parseFloat(vLineQty).toFixed(5));
			customrecord.setFieldValue('custrecord_wms_status_flag', 9);	// 14 stands for outbound process


			customrecord.setFieldValue('custrecord_actbeginloc',vactLocationtext);	
			//customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
			//customrecord.setFieldValue('custrecord_lpno', vlp); 
			customrecord.setFieldValue('custrecord_upd_date', now);
			//customrecord.setFieldValue('custrecord_sku', vskuText);
			customrecord.setFieldValue('custrecord_ebiz_sku_no', varsku);
			customrecord.setFieldValue('name', vWOId);		
			
			customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
			customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
			//customrecord.setFieldValue('custrecord_ebiz_wave_no', eBizWaveNo);
			customrecord.setFieldValue('custrecord_ebiz_order_no', vId);
			customrecord.setFieldValue('custrecord_batch_no', vBatch);
			customrecord.setFieldValue('custrecord_ebiz_cntrl_no', vId);
			//customrecord.setFieldValue('custrecord_ebiz_cntrl_no', rcptrecordid);

			customrecord.setFieldValue('custrecord_sku', varsku);
			customrecord.setFieldValue('custrecord_line_no', parseFloat(vlineno));
			customrecord.setFieldValue('custrecord_lpno', vLP);
			customrecord.setFieldValue('custrecord_packcode', vPackcode);					
			customrecord.setFieldValue('custrecord_sku_status', vSKUStatus);
			customrecord.setFieldValue('custrecord_wms_location', vLocation);
			customrecord.setFieldValue('custrecord_invref_no', vLineRec);
			customrecord.setFieldValue('custrecord_notes', vComments);	
			customrecord.setFieldValue('custrecord_ebizrule_no', vpickruleId);
			customrecord.setFieldValue('custrecord_ebizmethod_no', vpickmethod);
			customrecord.setFieldValue('custrecord_ebizzone_no', vpickzone);

			if(vpickzone !=null && vpickzone !='')
			{
				customrecord.setFieldValue('custrecord_ebiz_zoneid', vpickzone);
			}


			nlapiLogExecution('DEBUG', 'beginLocationId',vactLocationtext);
			if(vactLocationtext!=null && vactLocationtext!='')
			{
				nlapiLogExecution('Debug','Time stamp 26',TimeStampinSec());
				var locgroupfields = ['custrecord_inboundlocgroupid','custrecord_outboundlocgroupid'];
				var locgroupcolumns = nlapiLookupField('customrecord_ebiznet_location', vactLocationtext, locgroupfields);
				nlapiLogExecution('Debug','Time stamp 27',TimeStampinSec());
				var inblocgroupid=locgroupcolumns.custrecord_inboundlocgroupid;
				var oublocgroupid=locgroupcolumns.custrecord_outboundlocgroupid;
				nlapiLogExecution('DEBUG', 'inblocgroupid',inblocgroupid);
				var locgroupseqfields = ['custrecord_sequenceno'];
				//code added on 011012 by suman
				//Allow to insert sequence no only if we get some value form inbonlog group Id.
				//if(inblocgroupid!=null&&inblocgroupid!="")
				if(oublocgroupid!=null && oublocgroupid!="")
				{
					nlapiLogExecution('Debug','Time stamp 28',TimeStampinSec());
					//var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', inblocgroupid, locgroupseqfields);
					var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', oublocgroupid, locgroupseqfields);
					var locgroupidseq=locgroupseqcolumns.custrecord_sequenceno;
					nlapiLogExecution('DEBUG', 'locgroupidseq',locgroupidseq);

					customrecord.setFieldValue('custrecord_bin_locgroup_seq', locgroupidseq);
					nlapiLogExecution('Debug','Time stamp 29',TimeStampinSec());
				}
			}

			var expiryDateInLot='';
			if(vBatch!=null && vBatch !=''){
				var filterbatch = new Array();
				filterbatch[0] = new nlobjSearchFilter('name', null, 'is', vBatch);
				filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', varsku);
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebizexpirydate'); 
				var Lotnosearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
				if(Lotnosearchresults!=null && Lotnosearchresults!='')
				{
					nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
					vLotno= Lotnosearchresults[0].getId();
					expiryDateInLot= Lotnosearchresults[0].getValue('custrecord_ebizexpirydate');
				}
				nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
				if(expiryDateInLot !=null && expiryDateInLot!='')
					customrecord.setFieldValue('custrecord_expirydate', expiryDateInLot);

			}

			nlapiLogExecution('Debug','Time stamp 30',TimeStampinSec());
			//nlapiLogExecution('ERROR','eBizWaveNo ',eBizWaveNo);


			nlapiSubmitRecord(customrecord);			
			nlapiLogExecution('Debug','Time stamp 31',TimeStampinSec());
			nlapiLogExecution('ERROR', 'Success');


		}

	}
	/*var WOarray = new Array();
	nlapiLogExecution('ERROR', 'vId',vId);
	WOarray["custparam_wo"] = vId;
	response.sendRedirect('SUITELET', 'customscript_ebiz_bom_report_sl', 'customdeploy_ebiz_bom_report_di', false,WOarray);
	return false;*/


}


function PickStrategieKittoStock(item,itemfamily,itemgroup,avlqty,location,serialNo){
	//alert("5");
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','itemgroup',itemgroup);
	nlapiLogExecution('ERROR','itemfamily',itemfamily);
	nlapiLogExecution('ERROR','location',location);
	nlapiLogExecution('ERROR','serialNo',serialNo);

	var filters = new Array();	

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]));

	if (itemgroup != "" && itemgroup != null) {
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul',null,  'anyof', ['@NONE@',itemgroup]));
	}
	if (itemfamily!= "" && itemfamily!= null) {
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@',itemfamily]));
	}

	if (location!= "" && location!= null) {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', ['@NONE@',location]));
	}


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	//Fetching pick rule
	nlapiLogExecution('ERROR','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		var searchresultpick = searchresults[i];				
		var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');			

		nlapiLogExecution('ERROR', "LP: " + LP);
		nlapiLogExecution('ERROR','PickZone',vpickzone);			
		var filterszone = new Array();	
		filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', ['@NONE@',vpickzone]));
		filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', vpickzone,null));
		filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',location]));

		var columnzone = new Array();
		columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');

		var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
		nlapiLogExecution('ERROR','Loc Group Fetching');

		for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {

			var searchresultzone = searchresultszone[j];
			var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');

			nlapiLogExecution('ERROR', 'LocGroup'+ searchresultzone.getValue('custrecord_locgroup_no'), searchresultzone.getText('custrecord_locgroup_no'));

			var filtersinvt = new Array();

			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
			filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));
			filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));

			if (location!= "" && location!= null) {
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [location]));
			}
			//code added by santosh on 13aug12
			if (serialNo!= "" && serialNo!= null) {
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', serialNo));
			}
			//end of the code on 13Aug12
			nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno'); 
			columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_qoh');			
			columnsinvt[7] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
			columnsinvt[4].setSort();
			columnsinvt[5].setSort();
			columnsinvt[6].setSort(false);

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
				var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
				var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
				var Recid = searchresult.getId(); 


				//alert(" 664 actqty:" +actqty);
				//alert(" 664 allocqty:" +allocqty);
				//alert(" 664 LP:" +LP);
				//alert(" 664 vactLocation:" +vactLocation);
				//alert(" 664 vactLocationtext:" +vactLocationtext);
				//alert(" 664 vlotno:" +vlotno);
				//alert(" 664 vlotText:" +vlotText);
				//alert(" 664 Recid:" +Recid);


				nlapiLogExecution('ERROR', 'Recid1', Recid);
				if (isNaN(allocqty)) {
					allocqty = 0;
				}
				if (allocqty == "") {
					allocqty = 0;
				}
				if( parseFloat(allocqty)<0)
				{
					allocqty=0;
				}
				if( parseFloat(actqty)<0)
				{
					actqty=0;
				}
				nlapiLogExecution('ERROR', "vactLocation: " + vactLocation);
				nlapiLogExecution('ERROR', "vactLocationtext: " + vactLocationtext);
				nlapiLogExecution('ERROR', "allocqty: " + allocqty);
				nlapiLogExecution('ERROR', "LP: " + LP);
				var remainqty=0;

				//Added by Ganesh not to allow negative or zero Qtys 
				if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0)
					remainqty = parseFloat(actqty) - parseFloat(allocqty);


				var cnfmqty;

////				alert("remainqty"+remainqty );
////				alert("avlqty"+avlqty );
				if (parseFloat(remainqty) > 0) {

					////alert("parseFloat(avlqty) "+parseFloat(avlqty));

					if ((parseFloat(avlqty) - parseFloat(actallocqty)) <= parseFloat(remainqty)) {
						cnfmqty = parseFloat(avlqty) - parseFloat(actallocqty);
						actallocqty = avlqty;
						////alert('cnfmqty in if '+cnfmqty);
					}
					else {
						cnfmqty = remainqty;
						////alert('cnfmqty2 ' +cnfmqty);
						actallocqty = parseFloat(actallocqty) + parseFloat(remainqty);
					}

					//alert(" 708 cnfmqty:" +cnfmqty);
					//alert(" 709 actallocqty:" +actallocqty);

					////alert('BeforeIf ' +cnfmqty);
					if (parseFloat(cnfmqty) > 0) {
						////alert('AfterIf ' +cnfmqty);
						//Resultarray
						var invtarray = new Array();
						////alert('cnfmqty3 ' +cnfmqty);
						invtarray[0]= cnfmqty;
						////alert('cnfmqty4 ' +cnfmqty);
						invtarray[1]= vactLocation;
						invtarray[2] = LP;
						invtarray[3] = Recid;
						invtarray[4] = vlotText;
						invtarray[5] =remainqty;
						invtarray[6] =vactLocationtext;
						invtarray[7] =vlotno;
						//alert("cnfmqty "+cnfmqty);
						//alert("cnfmqty " + cnfmqty);
						//alert(vlotno);
						//alert(vlotText);
						nlapiLogExecution('ERROR', 'RecId:',Recid);
						//invtarray[3] = vpackcode;
						//invtarray[4] = vskustatus;
						//invtarray[5] = vuomid;
						Resultarray.push(invtarray);								
					}


				}

				if ((avlqty - actallocqty) == 0) {

					return Resultarray;

				}

			} 
		}
	}

	return Resultarray;  
}

/*code merged from monobind on feb 27th 2013 by Radhika added kititemstatus filter condition */

function GetLotname(id)
{
	var batchname="";
	var filterbatch = new Array();
	filterbatch[0] = new nlobjSearchFilter('InternalId', null, 'is', id);

	var columns=new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('InternalId');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
	if(searchresults!=null)
	{
		batchname= searchresults[0].getValue('name');
		nlapiLogExecution('ERROR', 'NewBatchno', batchname);
		return batchname;
	}

}

var allocatedInv=new Array();
//added pickfacelocationresults as a parameter
function PickStrategieKittoStockNew(item,avlqty,location,ItemInfoArr,pickrulesarray,serialNo,pfLocationResults,vItemType,
		batchflag){
	nlapiLogExecution('ERROR','Into PickStrategieKittoStockNew');
	var actallocqty = 0;
	var Resultarray = new Array();
	/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/	
	var PfLocArray =new Array();
	/*** up to here ***/

	var ItemFamily;
	var ItemGroup;

	if(ItemInfoArr!= null && ItemInfoArr != '' )
	{
		nlapiLogExecution('ERROR', 'ItemInfoArr.length', ItemInfoArr.length);
		for(var p=0;p<ItemInfoArr.length;p++)
		{
			if(ItemInfoArr[p].getId()==item)
			{
				ItemFamily = ItemInfoArr[p].getValue('custitem_item_family');
				ItemGroup = ItemInfoArr[p].getValue('custitem_item_group');

			}	
		}	
	}

	var str = 'item.' + item + '<br>';
	str = str + 'ItemInfoArr.' + ItemInfoArr + '<br>';	
	str = str + 'ItemFamily.' + ItemFamily + '<br>';	
	str = str + 'ItemGroup. ' + ItemGroup + '<br>';	
	str = str + 'avlqty. ' + avlqty + '<br>';	
	str = str + 'location. ' + location + '<br>';
	str = str + 'pickrulesarray. ' + pickrulesarray + '<br>';
	str = str + 'serialNo. ' + serialNo + '<br>';
	str = str + 'pfLocationResults. ' + pfLocationResults + '<br>';
	str = str + 'vItemType. ' + vItemType + '<br>';
	str = str + 'batchflag. ' + batchflag + '<br>';

	nlapiLogExecution('DEBUG', 'Function parameters', str);

	nlapiLogExecution('Debug','Time stamp 101',TimeStampinSec());

	var filterStatus = new Array();

	filterStatus.push(new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T'));
	filterStatus.push(new nlobjSearchFilter('custrecord_activeskustatus', null, 'is','T'));
	if(location != null && location != '')
		filterStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof',location));
	var searchresultsStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filterStatus, null);
	nlapiLogExecution('Debug','Time stamp 102',TimeStampinSec());
	var vStatusArr=new Array();
	if(searchresultsStatus != null && searchresultsStatus != '')
	{
		for(var k=0;k<searchresultsStatus.length;k++)
		{
			vStatusArr.push(searchresultsStatus[k].getId());
		}

		nlapiLogExecution('ERROR', 'vStatusArr',vStatusArr);
	}	

	nlapiLogExecution('Debug','Time stamp 103',TimeStampinSec());


	var vPickRules=new Array();
	if(pickrulesarray != null && pickrulesarray != '' && pickrulesarray.length>0)
	{
		nlapiLogExecution('ERROR', 'pickrulesarray.length', pickrulesarray.length);
		for(var j = 0; j < pickrulesarray.length; j++)
		{
			if((pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul') ==ItemFamily))	
			{
				if((pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul') ==ItemGroup))	
				{
					if((pickrulesarray[j].getValue('custrecord_ebizskupickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskupickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskupickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskupickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskupickrul') ==item))	
					{
						vPickRules.push(j);
					}	
				}	
			}	
		}	
	}

	nlapiLogExecution('Debug','Time stamp 104',TimeStampinSec());



	if (vPickRules != null) {
		nlapiLogExecution('ERROR', 'rules count', vPickRules.length);

		var vRemainingQty=avlqty;
		nlapiLogExecution('ERROR', 'vRemainingQty', vRemainingQty);
		for (var s = 0; s < parseFloat(vPickRules.length) && parseFloat(vRemainingQty) > 0; s++) {
			var vPickZoneArr=new Array();

			var vpickzone=pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickzonerul');
			var pickRuleId = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizruleidpick');
			var pickMethodId = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickmethod');
			var pickBinlocGroup = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizlocationgrouppickrul');
			var vBinloction = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizlocationpickrul');
			var pickmethodpickface = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');

			var pfBinLocationgroupId=new Array();
			if ((pickmethodpickface == 'T') && (pfLocationResults != null && pfLocationResults.length > 0))
			{
				for(var p = 0; p < pfLocationResults.length; p++)
				{
					var vPFSKU=pfLocationResults[p].getValue('custrecord_pickfacesku');
					if(item == vPFSKU)
						pfBinLocationgroupId.push(pfLocationResults[p].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc'));
				}
			}

			var str = 'vPickRules[s].' + vPickRules[s] + '<br>';
			str = str + 'vRemainingQty.' + vRemainingQty + '<br>';	
			str = str + 'Id.' + pickrulesarray[vPickRules[s]].getId() + '<br>';	
			str = str + 'vpickzone. ' + vpickzone + '<br>';	
			str = str + 'pickRuleId. ' + pickRuleId + '<br>';	
			str = str + 'pickMethodId. ' + pickMethodId + '<br>';
			str = str + 'pickBinlocGroup. ' + pickBinlocGroup + '<br>';
			str = str + 'vBinloction. ' + vBinloction + '<br>';
			str = str + 'pickmethodpickface. ' + pickmethodpickface + '<br>';
			str = str + 'pfBinLocationgroupId. ' + pfBinLocationgroupId + '<br>';

			nlapiLogExecution('DEBUG', 'Pick strategy parameters', str);

			vPickZoneArr.push(vpickzone);
			//}	
			//}

			var vLocGrpArr=new Array();
			if(vPickZoneArr != null && vPickZoneArr != '')
			{
				nlapiLogExecution('Debug','Time stamp 104',TimeStampinSec());
				if(pickBinlocGroup ==null || pickBinlocGroup =='')
					vLocGrpArr=GetAllLocGroups(vPickZoneArr,location,-1);
				else 
					vLocGrpArr.push(pickBinlocGroup);
				nlapiLogExecution('Debug','Time stamp 104',TimeStampinSec());

			}
			else if(pickBinlocGroup !=null && pickBinlocGroup !='')
				vLocGrpArr.push(pickBinlocGroup);	


			var searchresultsinvt;
			if((vLocGrpArr != null && vLocGrpArr != '' && vLocGrpArr.length>0) ||(vBinloction != null && vBinloction != ''))
			{
				searchresultsinvt=GetAllInventory(item,vLocGrpArr,location,serialNo,vStatusArr,vItemType,batchflag,vBinloction,pfBinLocationgroupId);
				nlapiLogExecution('Debug','Time stamp 105',TimeStampinSec());

			}
			if(searchresultsinvt != null && searchresultsinvt != '')
			{
				/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
				//consider first Pickface location to fetch the Inventory for the Item
				if(pickmethodpickface == 'T')
				{
					if (pfLocationResults != null && pfLocationResults.length > 0)
					{
						var remainingQuantity=avlqty;
						//

						nlapiLogExecution('ERROR', 'Within PF Location Result search');
						nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);
						nlapiLogExecution('ERROR', 'pfLocationResults.length', pfLocationResults.length);

						for(var z = 0; z < pfLocationResults.length; z++)
						{
							if(parseFloat(remainingQuantity) > 0)
							{
								var pfItem = pfLocationResults[z].getValue('custrecord_pickfacesku');
								var Autoreplenflag=pfLocationResults[z].getValue('custrecord_autoreplen');

								if (pfItem == item)
								{
									nlapiLogExecution('ERROR', 'item Match',pfItem);
									pfBinLocationId = pfLocationResults[z].getValue('custrecord_pickbinloc');
									var pfBinLocationText = pfLocationResults[z].getText('custrecord_pickbinloc');
									pfMaximumQuantity = pfLocationResults[z].getValue('custrecord_maxqty');
									pfMaximumPickQuantity = pfLocationResults[z].getValue('custrecord_maxpickqty');
									pffixedlp = pfLocationResults[z].getValue('custrecord_pickface_ebiz_lpno');
									pfstatus=pfLocationResults[z].getValue('custrecord_pickface_itemstatus');
									replenrule=pfLocationResults[z].getValue('custrecord_pickruleid');
									var pfBinLocationGroup = pfLocationResults[z].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');
									var pfzone=pfLocationResults[z].getValue('custrecord_pickzone');

									var str = 'pfBinLocationId. = ' + pfBinLocationId + '<br>';
									str = str + 'pfMaximumQuantity. = ' + pfMaximumQuantity + '<br>';	
									str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
									str = str + 'pffixedlp. = ' + pffixedlp + '<br>';
									str = str + 'pfstatus. = ' + pfstatus + '<br>';
									str = str + 'replenrule. = ' + replenrule + '<br>';
									str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';
									str = str + 'pfBinLocationGroup. = ' + pfBinLocationGroup + '<br>';
									str = str + 'pfzone. = ' + pfzone + '<br>';

									nlapiLogExecution('ERROR', 'Pickface Details', str);

									PfLocArray.push(pfBinLocationId);

									if(parseFloat(pfMaximumPickQuantity) >= parseFloat(remainingQuantity))
									{

										var availableQty ="";
										var allocQty = "";
										var recordId = "";

										var itemStatus = "";
										var fromLP = "";
										var inventoryItem="";
										var fromLot="";
										// Get inventory details for this pickface location
										nlapiLogExecution('Debug','Time stamp 106',TimeStampinSec());
										var binLocnInvDtls = getAvailQtyFromInventory(searchresultsinvt, pfBinLocationId, allocatedInv,location,pfItem);
										nlapiLogExecution('Debug','Time stamp 107',TimeStampinSec());
										var fromLotId;
										if(binLocnInvDtls!=null && binLocnInvDtls.length>0)
										{
											nlapiLogExecution('Debug','binLocnInvDtls',binLocnInvDtls.length);

											for(var bincount=0;bincount<binLocnInvDtls.length;bincount++)
											{
												if(parseFloat(remainingQuantity) > 0)
												{
													availableQty = binLocnInvDtls[bincount][2];
													allocQty = binLocnInvDtls[bincount][3];
													recordId = binLocnInvDtls[bincount][8];
													if(binLocnInvDtls[bincount][4]!=null && binLocnInvDtls[bincount][4]!='')
														packCode = binLocnInvDtls[bincount][4];
													itemStatus = binLocnInvDtls[bincount][5];										
													whLocation = binLocnInvDtls[bincount][6];
													fromLP = binLocnInvDtls[bincount][7];											
													inventoryItem = binLocnInvDtls[bincount][9];
													fromLot = binLocnInvDtls[bincount][10];
													expiryDate = binLocnInvDtls[bincount][11];
													fromLotId = binLocnInvDtls[bincount][12];
													nlapiLogExecution('ERROR', 'binLocnInvDtls[0][10] For Batch',binLocnInvDtls[bincount][10]);
//													}
													/*
													 * If the allocation strategy is not null check in inventory for that item.						 
													 * 		if the allocation strategy = min quantity on the pallet
													 * 			search for the inventory where the inventory has the least quantity
													 * 		else
													 * 			search for the inventory where the inventory has the maximum quantity 
													 * 			on the pallet 
													 */

													var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
													str = str + 'availableQty. = ' + availableQty + '<br>';	

													nlapiLogExecution('ERROR', 'Item Status', str);

													//if(parseFloat(availableQty) > 0 && itemStatus== pfstatus)
													if(parseFloat(availableQty) > 0)
													{
														nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);
														// allocate to this bin location
														var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
														var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);
														remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);
														vRemainingQty=remainingQuantity;
														//nlapiLogExecution('ERROR', 'actualAllocQty', actualAllocQty);
														//nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);
														var expectedQuantity = parseFloat(actualAllocQty);

														var contsize="";								
														//var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
														//var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

														// Allocate the quantity to the pickface location if the allocation quantity is 
														// 	less than or equal to the pick face maximum quantity
														//nlapiLogExecution('ERROR', 'actualAllocQty', actualAllocQty);
														//nlapiLogExecution('ERROR', 'pfMaximumPickQuantity', pfMaximumPickQuantity);

														var str = 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
														str = str + 'actualAllocQty. = ' + actualAllocQty + '<br>';	
														str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';	

														nlapiLogExecution('ERROR', 'Qty Details Status', str);

														if(actualAllocQty <= pfMaximumPickQuantity)
														{
															//1-Ship Cartons	2-Build Cartons		3-Ship Pallets

															var allocatedInvRow = [pfBinLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
															allocatedInv.push(allocatedInvRow);
															//PfLocArray.push(pfBinLocationId);
														}
														if (parseFloat(actualAllocQty) > 0) {
															////alert('AfterIf ' +cnfmqty);
															//Resultarray

															nlapiLogExecution('Debug','Time stamp 108',TimeStampinSec());

															if(pfzone==null || pfzone=='')
															{

																var filtersLocGrp = new Array();
																filtersLocGrp.push(new nlobjSearchFilter('custrecord_locgroup_no', null, 'anyof', pfBinLocationGroup));
																var columnsLocGrp = new Array();
																columnsLocGrp[0] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
																var SearchLocGrp = nlapiSearchRecord('customrecord_zone_locgroup', null, filtersLocGrp, columnsLocGrp);
																nlapiLogExecution('ERROR', 'vpickzone b', vpickzone);
																if(SearchLocGrp != null && SearchLocGrp != '')
																{
																	pfzone = SearchLocGrp[0].getValue('custrecordcustrecord_putzoneid');
																}
															}

															nlapiLogExecution('ERROR', 'pfzone a', pfzone);


															var invtarray = new Array();
															////alert('cnfmqty3 ' +cnfmqty);
															invtarray[0]= actualAllocQty;
															////alert('cnfmqty4 ' +cnfmqty);
															invtarray[1]= pfBinLocationId;
															invtarray[2] = fromLP;
															invtarray[3] = recordId;
															invtarray[4] = fromLot;
															invtarray[5] =availableQty;
															invtarray[6] =pfBinLocationText;
															invtarray[7] =fromLotId;
															invtarray[8] =pfzone;
															invtarray[9] =pickRuleId;
															invtarray[10] =pickMethodId;
															//alert("cnfmqty "+cnfmqty);
															//alert("cnfmqty " + cnfmqty);
															//alert(vlotno);
															//alert(vlotText);
															nlapiLogExecution('ERROR', 'RecId:',Recid);
															nlapiLogExecution('Debug','Time stamp 109',TimeStampinSec());
															//invtarray[3] = vpackcode;
															//invtarray[4] = vskustatus;
															//invtarray[5] = vuomid;
															Resultarray.push(invtarray);
															if (parseFloat(remainingQuantity) == 0) {
																nlapiLogExecution('ERROR', 'remainingQuantity in jump statement:',remainingQuantity);
															}
														}
													}
													nlapiLogExecution('ERROR','remainingQuantity',remainingQuantity);
												}
											}
											if (parseFloat(remainingQuantity) <= 0)
												return Resultarray;
										}
									}
								}
							}
						}
					}
				}
				/*** Up to here ***/
				for (var l = 0; l < searchresultsinvt.length; l++) {

					var searchresult = searchresultsinvt[l];
					var actqty = searchresult.getValue('custrecord_ebiz_qoh');
					var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
					var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
					var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
					var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
					var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
					var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
					var Recid = searchresult.getId(); 

					var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, vactLocation,LP,item,vlotText);

					nlapiLogExecution('ERROR', 'alreadyAllocQty', alreadyAllocQty);
					nlapiLogExecution('ERROR', 'actqty', actqty);

					actqty = parseInt(actqty) - parseInt(alreadyAllocQty);

					nlapiLogExecution('ERROR', 'actqty', actqty);

					/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
					//to fetch remaining Invt from bulk Location
					if(remainingQuantity != null && remainingQuantity != '' && parseFloat(remainingQuantity)>0)
					{
						avlqty=remainingQuantity;
						vRemainingQty=remainingQuantity;
					}
					/*** up to here ***/
					//alert(" 664 actqty:" +actqty);
					//alert(" 664 allocqty:" +allocqty);
					//alert(" 664 LP:" +LP);
					//alert(" 664 vactLocation:" +vactLocation);
					//alert(" 664 vactLocationtext:" +vactLocationtext);
					//alert(" 664 vlotno:" +vlotno);
					//alert(" 664 vlotText:" +vlotText);
					//alert(" 664 Recid:" +Recid);
					/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
					//to restrict pickface Location for the Item
					var vBulkLocProceed='T';
					if(PfLocArray.length>0)
					{
						if(PfLocArray.indexOf(vactLocation)!=-1)
							vBulkLocProceed='F';
					}
					/*** up to here ***/

					nlapiLogExecution('ERROR', 'Recid1', Recid);
					if(vBulkLocProceed=='T')
					{
						if (isNaN(allocqty)) {
							allocqty = 0;
						}
						if (allocqty == "") {
							allocqty = 0;
						}
						if( parseFloat(allocqty)<0)
						{
							allocqty=0;
						}
						if( parseFloat(actqty)<0)
						{
							actqty=0;
						}

						var str = 'vactLocation. = ' + vactLocation + '<br>';
						str = str + 'vactLocationtext. = ' + vactLocationtext + '<br>';	
						str = str + 'allocqty. = ' + allocqty + '<br>';
						str = str + 'LP. = ' + LP + '<br>';
						str = str + 'vlotText. = ' + vlotText + '<br>';
						str = str + 'allocqty. = ' + allocqty + '<br>';
						str = str + 'actqty. = ' + actqty + '<br>';
						str = str + 'vpickzone. = ' + vpickzone + '<br>';

						nlapiLogExecution('ERROR', 'Bulk Loc Details', str);


						var remainqty=0;
						//Added by Ganesh not to allow negative or zero Qtys 
						if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0 && (parseFloat(actqty)-parseFloat(allocqty))>0)
						{
							remainqty = parseFloat(actqty) - parseFloat(allocqty);
							vRemainingQty=remainqty;
						}
						var cnfmqty;
						nlapiLogExecution('ERROR', "vRemainingQty: ",vRemainingQty );
						//nlapiLogExecution('ERROR', "remainqty: ",remainqty );
////						alert("remainqty"+remainqty );
////						alert("avlqty"+avlqty );
						if (parseFloat(remainqty) > 0) {
							////alert("parseFloat(avlqty) "+parseFloat(avlqty));
							if ((parseFloat(avlqty) - parseFloat(actallocqty)) <= parseFloat(remainqty)) {
								cnfmqty = parseFloat(avlqty) - parseFloat(actallocqty);
								actallocqty = avlqty;
								////alert('cnfmqty in if '+cnfmqty);
							}
							else {
								cnfmqty = remainqty;
								////alert('cnfmqty2 ' +cnfmqty);
								actallocqty = parseFloat(actallocqty) + parseFloat(remainqty);
							}
							//alert(" 708 cnfmqty:" +cnfmqty);
							//alert(" 709 actallocqty:" +actallocqty);
							////alert('BeforeIf ' +cnfmqty);
							if (parseFloat(cnfmqty) > 0) {
								////alert('AfterIf ' +cnfmqty);
								//Resultarray
								var invtarray = new Array();
								////alert('cnfmqty3 ' +cnfmqty);
								invtarray[0]= cnfmqty;
								////alert('cnfmqty4 ' +cnfmqty);
								invtarray[1]= vactLocation;
								invtarray[2] = LP;
								invtarray[3] = Recid;
								invtarray[4] = vlotText;
								invtarray[5] =remainqty;
								invtarray[6] =vactLocationtext;
								invtarray[7] =vlotno;
								invtarray[8] =vpickzone;
								invtarray[9] =pickRuleId;
								invtarray[10] =pickMethodId;
								//alert("cnfmqty "+cnfmqty);
								//alert("cnfmqty " + cnfmqty);
								//alert(vlotno);
								//alert(vlotText);
								nlapiLogExecution('ERROR', 'RecId:',Recid);
								//invtarray[3] = vpackcode;
								//invtarray[4] = vskustatus;
								//invtarray[5] = vuomid;
								Resultarray.push(invtarray);								
							}
						}
						if ((avlqty - actallocqty) == 0) {
							return Resultarray;
						}
					} 
				}
			}
		}
	}
	nlapiLogExecution('Debug','Time stamp 110',TimeStampinSec());
	return Resultarray;  
}

var vAllGrps=new Array();
function GetAllLocGroups(vpickzone,location,maxno){

	nlapiLogExecution('Debug','Into GetAllLocGroups',TimeStampinSec());

	var str = 'vpickzone.' + vpickzone + '<br>';
	str = str + 'location.' + location + '<br>';	
	str = str + 'maxno. ' + maxno + '<br>';	

	nlapiLogExecution('DEBUG', 'GetAllLocGroups Parameters', str);

	if(vpickzone!= null && vpickzone !='')
		vpickzone.push('@NONE@');

	var filterszone = new Array();
	filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', vpickzone));

	filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',location]));
	if(maxno!=-1)
	{

		filterszone.push(new nlobjSearchFilter('internalid', null, 'lessthan', parseFloat(maxno)));
	}
	else
		vAllGrps=new Array();

	var columnzone = new Array();
	columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');
	columnzone[1]=new nlobjSearchColumn('internalid');
	columnzone[1].setSort(true);

	var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
	nlapiLogExecution('ERROR','Loc Group Fetching');
	if(searchresultszone != null && searchresultszone != '')
	{	
		for(var k=0;k<searchresultszone.length;k++)
		{
			vAllGrps.push(searchresultszone[k].getValue('custrecord_locgroup_no'));
		}
		if(searchresultszone!=null && searchresultszone.length>=1000)
		{ 
			var maxno=searchresultszone[searchresultszone.length-1].getValue('internalid');		
			GetAllLocGroups(vpickzone,location,maxno); 
		}
	}

	nlapiLogExecution('Debug','Out of GetAllLocGroups',TimeStampinSec());
	return vAllGrps;
}


var vAllInv=new Array();
function GetAllInventory(item,vlocgroupno,location,serialNo,vStatusArr,vItemType,batchflag,binlocation,
		pfBinLocationgroupId){

	nlapiLogExecution('Debug','Into GetAllInventory',TimeStampinSec());

	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));

	if((vlocgroupno != null && vlocgroupno != '') && (pfBinLocationgroupId!=null && pfBinLocationgroupId !=''))
	{

		for(var j=0;j<pfBinLocationgroupId.length;j++)
			vlocgroupno.push(pfBinLocationgroupId[j]);

		filtersinvt.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid',null, 'anyof', vlocgroupno));
	}

	if((vlocgroupno != null && vlocgroupno != '') && (pfBinLocationgroupId ==null || pfBinLocationgroupId ==''))
	{

		filtersinvt.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid',null, 'anyof', vlocgroupno));
	}

	if(binlocation != null && binlocation != '')
	{

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'anyof', binlocation));
	}
	if (location!= "" && location!= null) {

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [location]));
	}

	//code added by santosh on 13aug12
	if (serialNo!= "" && serialNo!= null) {

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', serialNo));
	}

	if (vStatusArr!= "" && vStatusArr!= null) {

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', vStatusArr));
	}

	//end of the code on 13Aug12
	if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T" )
	{

		var expdate=DateStamp();
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_expdate', null, 'after', expdate));
		//end of the code on 13Aug12

	}

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));

	nlapiLogExecution('ERROR', "vStatusArr:  :  vItemType  : batchflag :pfBinLocationgroupId " , vStatusArr + ' : ' + vItemType + ' : ' + batchflag + ' : ' +pfBinLocationgroupId );
	nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku:  :  vlocgroupno  : binlocation  :   serialNo " , item + ' : ' + vlocgroupno + ' : ' + binlocation + ' : ' + serialNo );
	nlapiLogExecution('ERROR', "vlocgroupno.length:  :  vStatusArr.length " , vlocgroupno.length + ' : ' + vStatusArr.length );


	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columnsinvt[1]=new nlobjSearchColumn('internalid');
	columnsinvt[0].setSort();
	columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columnsinvt[5] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
	columnsinvt[7] = new nlobjSearchColumn('custrecord_pickseqno'); 
	columnsinvt[8] = new nlobjSearchColumn('custrecord_ebiz_qoh');			
	columnsinvt[9] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
	columnsinvt[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columnsinvt[11] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columnsinvt[12] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columnsinvt[13] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	/*** up to here ***/	
	columnsinvt[6].setSort();
	columnsinvt[7].setSort();
	columnsinvt[8].setSort(false);

	var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

	nlapiLogExecution('Debug','Out of GetAllInventory',TimeStampinSec() + " : " + searchresultsinvt);

	return searchresultsinvt;
}



/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/

function getAvailQtyFromInventory(inventorySearchResults, binLocation, allocatedInv,dowhlocation,fulfilmentItem){

	nlapiLogExecution('ERROR','Into getAvailQtyFromInventory',TimeStampinSec());

	var str = 'inventorySearchResults.' + inventorySearchResults + '<br>';
	str = str + 'binLocation.' + binLocation + '<br>';	
	str = str + 'allocatedInv. ' + allocatedInv + '<br>';	
	str = str + 'dowhlocation. ' + dowhlocation + '<br>';	
	str = str + 'fulfilmentItem. ' + fulfilmentItem + '<br>';	

	nlapiLogExecution('DEBUG', 'getAvailQtyFromInventory Parameters', str);

	var binLocnInvDtls = new Array();
	var matchFound = false;

	if(inventorySearchResults != null && inventorySearchResults.length > 0){
		nlapiLogExecution('ERROR','inventorySearchResults length',inventorySearchResults.length);
		for(var i = 0; i < inventorySearchResults.length; i++){
			if(!matchFound){
				invBinLocnId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_binloc');
				var InvWHLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
				var invItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');

				var str = 'dowhlocation. = ' + dowhlocation + '<br>';
				str = str + 'InvWHLocation. = ' + InvWHLocation + '<br>';	
				str = str + 'invBinLocnId. = ' + invBinLocnId + '<br>';
				str = str + 'binLocation. = ' + binLocation + '<br>';
				str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';
				str = str + 'invItem. = ' + invItem + '<br>';

				nlapiLogExecution('ERROR', 'Inventory & FO Details', str);

				if((dowhlocation==InvWHLocation)&&(parseFloat(invBinLocnId) == parseFloat(binLocation)) && fulfilmentItem==invItem){

					var actualQty = inventorySearchResults[i].getValue('custrecord_ebiz_qoh');
					var inventoryAvailableQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_avl_qty');
					var allocationQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_alloc_qty');
					var packCode = inventorySearchResults[i].getValue('custrecord_ebiz_inv_packcode');
					var itemStatus = inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku_status');
					var whLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
					var fromLP = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lp');
					var inventoryItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
					var fromLot = inventorySearchResults[i].getText('custrecord_ebiz_inv_lot');
					var fromLotId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lot');
					var expiryDate = inventorySearchResults[i].getValue('custrecord_ebiz_expdate');
					var str = 'Invt QOH. = ' + actualQty + '<br>';
					str = str + 'Alloc Qty. = ' + allocationQuantity + '<br>';	
					str = str + 'Avail Qty. = ' + inventoryAvailableQuantity + '<br>';
					str = str + 'packCode. = ' + packCode + '<br>';
					str = str + 'itemStatus. = ' + itemStatus + '<br>';
					str = str + 'whLocation. = ' + whLocation + '<br>';
					str = str + 'fromLP. = ' + fromLP + '<br>';
					str = str + 'inventoryItem. = ' + inventoryItem + '<br>';
					str = str + 'fromLot. = ' + fromLot + '<br>';

					nlapiLogExecution('ERROR', 'Inventory Details', str);

					var recordId = inventorySearchResults[i].getId();

					// Get allocation already done
					var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,inventoryItem,fromLot);

					nlapiLogExecution('ERROR', 'alreadyAllocQty', alreadyAllocQty);

					// Adjusting for allocations already done
					inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);

					nlapiLogExecution('ERROR', 'inventoryAvailableQuantity', inventoryAvailableQuantity);

					var currentRow = [i, actualQty, inventoryAvailableQuantity, allocationQuantity, packCode,
					                  itemStatus, whLocation, fromLP, recordId,inventoryItem,fromLot,expiryDate,fromLotId];

					binLocnInvDtls.push(currentRow);

					nlapiLogExecution('ERROR', 'inventoryAvailable_currentRow', currentRow);
					//matchFound = true;
					matchFound = false;
				}
			}
		}
	}

	nlapiLogExecution('ERROR','Out of getAvailQtyFromInventory',TimeStampinSec());
	return binLocnInvDtls;
}

function getPFLocationsForOrder(whlocation,skuList){
	var pfLocationResults = new Array();
	pfLocationResults = getSKUQtyInPickfaceLocns(whlocation,skuList);
	return pfLocationResults;
}

/**
 * Function to return the list of SKU quantity for all SKUs in all pickface locations
 * NOTE: WILL NEVER RETURN NULL; WILL ATLEAST RETURN AN EMPTY ARRAY
 * 
 * @returns {Array}
 */
function getSKUQtyInPickfaceLocns(whlocation,skulist){
	/*
	 * This function should return the pickface locn, min qty, max qty, qoh for all
	 * SKUs in all pickface locations
	 */

	nlapiLogExecution('ERROR', 'Into  getSKUQtyInPickfaceLocns',TimeStampinSec());
	nlapiLogExecution('ERROR', 'whlocation',whlocation);
	nlapiLogExecution('ERROR', 'skulist',skulist);
	var pfLocnResults = new Array();

	// Search for all active records with a valid replen rule id
	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('custrecord_pickruleid', null, 'noneof', ['@NONE@']));

	if(whlocation!=null && whlocation!='')
		filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', ['@NONE@',whlocation]));

	if(skulist!=null && skulist!='')
		filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skulist));

	var columns = new Array();    
	columns[0] = new nlobjSearchColumn('custrecord_pickfacesku');
	columns[1] = new nlobjSearchColumn('custrecord_replenqty');
	columns[2] = new nlobjSearchColumn('custrecord_maxqty');
	columns[3] = new nlobjSearchColumn('custrecord_pickbinloc');
	columns[4] = new nlobjSearchColumn('custrecord_pickruleid');
	columns[5] = new nlobjSearchColumn('custrecord_minqty');    
	columns[6] = new nlobjSearchColumn('custrecord_pickface_location');
	columns[7] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_maxpickqty');
	columns[9] = new nlobjSearchColumn('custrecord_pickface_itemstatus');
	columns[10] = new nlobjSearchColumn('custrecord_pickface_location');
	columns[11] = new nlobjSearchColumn('custrecord_roundqty');
	columns[12] = new nlobjSearchColumn('custrecord_pickface_location');	
	columns[13] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');
	columns[14] = new nlobjSearchColumn('custrecord_pickzone');
	columns[15] = new nlobjSearchColumn('custrecord_autoreplen');


	// sort by pick face SKU
	columns[0].setSort();

	// Search for all active
	pfLocnResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);    

	nlapiLogExecution('ERROR', 'Out of  getSKUQtyInPickfaceLocns',TimeStampinSec());

	return pfLocnResults;
}
function getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,item,lot){
	nlapiLogExecution('ERROR', 'Into getAlreadyAllocatedInv',TimeStampinSec());
	var alreadyAllocQty = 0;
	if(allocatedInv != null && allocatedInv.length > 0){
		for(var i = 0; i < allocatedInv.length; i++){
//			nlapiLogExecution('ERROR', 'fromLP',fromLP);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][2]',allocatedInv[i][2]);
//			nlapiLogExecution('ERROR', 'binLocation',binLocation);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][0]',allocatedInv[i][0]);
//			nlapiLogExecution('ERROR', 'item',item);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][3]',allocatedInv[i][3]);
//			nlapiLogExecution('ERROR', 'lot',lot);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][4]',allocatedInv[i][4]);

			if(fromLP == allocatedInv[i][2] && binLocation==allocatedInv[i][0] && item==allocatedInv[i][3] && lot==allocatedInv[i][4])
				alreadyAllocQty = parseFloat(alreadyAllocQty) + parseFloat(allocatedInv[i][1]);
		}
	}
	nlapiLogExecution('ERROR', 'Out of getAlreadyAllocatedInv',TimeStampinSec());
	return alreadyAllocQty;
}
/*** up to here ***/

