function putawayFieldChanged(type, name)
{
//  This particular code is 
// based on the Cycle count method flag , and disables/enables both standard and custom fields.
if (name == 'custrecord_mergeputaway')
{
var disable = nlapiGetFieldValue('custrecord_mergeputaway') == 'T'?false:true;
nlapiDisableField('custrecord_mixlotsputaway', disable);
nlapiDisableField('custrecord_mixpackcode', disable);
nlapiDisableField('custrecord_mixsku', disable);
nlapiDisableField('custrecord_mergelp', disable);

}
}

