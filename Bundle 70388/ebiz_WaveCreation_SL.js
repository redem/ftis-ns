/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_WaveCreation_SL.js,v $
<<<<<<< ebiz_WaveCreation_SL.js
 *     	   $Revision: 1.2.2.20.4.1.4.29.2.1 $
 *     	   $Date: 2015/10/09 07:29:07 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.2.2.20.4.1.4.29.2.1 $
 *     	   $Date: 2015/10/09 07:29:07 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.2.2.20.4.1.4.21
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveCreation_SL.js,v $
 * Revision 1.2.2.20.4.1.4.29.2.1  2015/10/09 07:29:07  skreddy
 * case:201414627
 * Briggs issue fix
 *
 * Revision 1.2.2.20.4.1.4.29  2015/02/04 07:46:08  sponnaganti
 * Case# 201411508
 * True Fab outbound process for selected lot in so/to
 *
 * Revision 1.2.2.20.4.1.4.28  2015/01/29 07:27:41  snimmakayala
 * Case#: 201411473
 *
 * Revision 1.2.2.20.4.1.4.27  2014/11/14 07:49:17  rrpulicherla
 * Std fixes
 *
 * Revision 1.2.2.20.4.1.4.26  2014/08/05 15:26:26  sponnaganti
 * Case# 20149816
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.20.4.1.4.25  2014/07/22 06:33:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149462
 *
 * Revision 1.2.2.20.4.1.4.24  2014/07/15 16:06:30  sponnaganti
 * Case# 20149468
 * Stnd Bundle issue fix
 *
 * Revision 1.2.2.20.4.1.4.23  2014/07/11 07:07:21  gkalla
 * case#20148894
 * Surftech  Wave generation concurrency issue fix
 *
 * Revision 1.2.2.20.4.1.4.22  2014/06/12 14:41:26  grao
 * Case#: 20148783  New GUI account issue fixes
 *
 * Revision 1.2.2.20.4.1.4.21  2014/06/06 14:28:37  skreddy
 * case # 20148808
 * SportsHq prod issue fix
 *
 * Revision 1.2.2.20.4.1.4.20  2014/05/22 15:42:17  skavuri
 * Case # 20148491 SB Issue Fixed
 *
 * Revision 1.2.2.20.4.1.4.19  2014/04/29 06:48:35  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148028
 *
 * Revision 1.2.2.20.4.1.4.18  2014/02/07 07:37:28  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127065
 *
 * Revision 1.2.2.20.4.1.4.17  2014/01/10 06:48:38  snimmakayala
 * Case# : 20126728
 * # of Lines Validation Issue.
 *
 * Revision 1.2.2.20.4.1.4.16  2013/12/13 13:48:24  grao
 * Added Location (site) filtetr in for Sports Hq..
 *
 * Revision 1.2.2.20.4.1.4.15  2013/09/16 15:53:35  nneelam
 * Case# 20124296
 * Order Summary Details  Issue Fix..
 *
 * Revision 1.2.2.20.4.1.4.14  2013/09/12 15:42:56  nneelam
 * Case#. 20124379�
 * Order Summary Details  Issue Fix..
 *
 * Revision 1.2.2.20.4.1.4.13  2013/08/05 13:56:30  rmukkera
 * Standard bundle Issue Fix for  case NO 20123734
 *
 * Revision 1.2.2.20.4.1.4.12  2013/07/25 06:12:11  spendyala
 * CASE201112/CR201113/LOG2012392
 * commented logs
 *
 * Revision 1.2.2.20.4.1.4.11  2013/07/11 06:27:57  schepuri
 * Case# 20121503
 * Batch Flag cond is added for lotnumbered item to display lot# link
 *
 * Revision 1.2.2.20.4.1.4.10  2013/06/21 23:33:44  skreddy
 * CASE201112/CR201113/LOG201121
 * to restrict when payment credit limit exceed
 *
 * Revision 1.2.2.20.4.1.4.9  2013/06/20 14:57:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * FM Multi location issues
 *
 * Revision 1.2.2.20.4.1.4.8  2013/06/19 14:33:27  schepuri
 * null values checking
 *
 * Revision 1.2.2.20.4.1.4.7  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.2.20.4.1.4.6  2013/05/09 15:23:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Issue related to wave generation filter based on Item family and group
 *
 * Revision 1.2.2.20.4.1.4.5  2013/05/03 09:46:38  grao
 * no message
 *
 * Revision 1.2.2.20.4.1.4.3  2013/03/19 11:45:09  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.2.2.20.4.1.4.2  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.2.2.20.4.1.4.1  2013/03/12 06:44:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Monobind.
 *
 * Revision 1.2.2.20.4.1  2012/10/26 08:04:19  gkalla
 * CASE201112/CR201113/LOG201121
 * Lot# selection popup in wave selection by line
 *
 * Revision 1.2.2.20  2012/09/03 14:35:29  mbpragada
 * 20120490
 * Added Crated date in search block and sublist
 *
 * Revision 1.2.2.19  2012/08/22 13:59:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Paging for Dynacraft
 *
 * Revision 1.2.2.18  2012/07/26 06:02:04  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.2.2.17  2012/06/07 15:07:55  gkalla
 * CASE201112/CR201113/LOG201121
 * Placed the start date object outside of if condition
 *
 * Revision 1.2.2.16  2012/05/09 07:52:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Partial Status Handling
 *
 * Revision 1.2.2.15  2012/05/07 09:54:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.2.2.14  2012/05/04 22:52:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.2.2.13  2012/04/20 22:52:20  gkalla
 * CASE201112/CR201113/LOG201121
 * To resolve selected into wave
 *
 * Revision 1.2.2.12  2012/04/20 14:30:50  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.2.2.11  2012/04/17 08:40:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pool of records
 *
 * Revision 1.2.2.10  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.14  2012/03/20 18:05:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation by sales order header
 *
 * Revision 1.13  2012/03/16 10:01:52  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.2.2.9
 *
 * Revision 1.12  2012/03/16 06:34:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fine tunned
 *
 * Revision 1.11  2012/03/14 07:07:41  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.10  2012/03/14 07:06:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.9  2012/03/13 12:31:06  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.2.2.6
 *
 * Revision 1.8  2012/03/09 10:47:01  rmukkera
 * CASE201112/CR201113/LOG201121
 * added wmscarrier for to
 *
 * Revision 1.7  2012/03/09 08:30:40  rmukkera
 * CASE201112/CR201113/LOG201121
 * Paging functionality added newly.
 *
 * Revision 1.6  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.5  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.4  2012/02/01 15:10:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation User Event
 *
 * Revision 1.3  2012/01/27 06:53:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation UE
 *
 * Revision 1.2  2012/01/05 14:01:49  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code tunning
 *
 * Revision 1.1  2011/12/21 16:45:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Gen User Event
 *  
 *
 *****************************************************************************/


/**
 * 
 * @param request
 * @param response
 */
function WaveGenSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Wave Selection by Line');
		nlapiLogExecution('DEBUG', 'Into GET');
		form.setScript('customscript_wavegeneration_cl');
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');
		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
		var hiddenwaveno = form.addField('custpage_hiddenwaveno', 'text', '').setDisplayType('hidden');
		hiddenwaveno.setDefaultValue('');	
		//form.setScript('customscript_inventoryclientvalidations');

		// Create the sublist
		createWaveGenSublist(form);

		var orderList = getOrdersForWaveGen(request,0,form);
		var vConfig=nlapiLoadConfiguration('accountingpreferences');
		var ruleValue=vConfig.getFieldValue('CUSTCREDLIMHANDLING');

		var vDueDaysList=null;
		var vCreditExceedList=null;
		var vDueDaysRestrict=null;
		//Add two functions
		if(ruleValue!=null && ruleValue!='' && ruleValue!='null' && ruleValue!='NONE')
		{
			vDueDaysList=getDueDaysList();
			vCreditExceedList=getCreditExceedList();

			//var vConfig=nlapiLoadConfiguration('accountingpreferences');

			if(vConfig != null && vConfig != '')
			{
				vDueDaysRestrict=vConfig.getFieldValue('CREDLIMDAYS');
			}
		}
		nlapiLogExecution('Error', 'ruleValue',ruleValue);

		if(orderList != null && orderList.length > 0){
			setPagingForSublist(orderList,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,ruleValue,request);


		}
		//codechanges for issue# 20121503
		form.addSubmitButton('Generate Wave');

		response.writePage(form);
	}
	else{
		nlapiLogExecution('DEBUG', 'Into Post');
		nlapiLogExecution('DEBUG', 'Time Stamp',TimeStampinSec());
		var start=new Date();

		generateWave(request, response);

		var end=new Date();

		var elapsed = (parseFloat(end.getTime()) - parseFloat(start.getTime()));

		nlapiLogExecution('DEBUG', 'Total Elapsed Time for all records: ',elapsed);

	}
}

/**
 * Create the sublist that will display the items for wave generation
 * 
 * @param form
 */
function createWaveGenSublist(form){
	var sublist = form.addSubList("custpage_items", "list", "ItemList");
	sublist.addMarkAllButtons();
	sublist.addField("custpage_so", "checkbox", "Confirm").setDefaultValue('F');
	sublist.addField("custpage_sono", "text", "Fulfillment Ord #");
	sublist.addField("custpage_solineno", "text", "Line #");

	sublist.addField("custpage_customer", "text", "Customer");
	sublist.addField("custpage_carrier", "text", "Carrier");
	sublist.addField("custpage_ordtype", "text", "Order Type");
	sublist.addField("custpage_ordpriority", "text", "Order Priority");
	sublist.addField("custpage_itemname", "text", "Item");
	sublist.addField("custpage_skustatus", "text", "Item Status").setDisplayType('hidden');
	sublist.addField("custpage_skustatustext", "text", "Item Status");

	sublist.addField("custpage_packcode", "text", "Packcode");
	sublist.addField("custpage_lotbatch", "text", "Lot#").setDisplayType('entry').setDisabled(true);//added by santosh
	sublist.addField("custpage_lotbatchlink", "text", "Select Lot#");
	sublist.addField("custpage_ordqty", "float", "Fulfillment Order Qty");
	sublist.addField("custpage_shipmentno", "text", "Shipment #");	
	sublist.addField("custpage_itemno", "text", "Item No").setDisplayType('hidden');
	sublist.addField("custpage_doinernno", "text", "DONo").setDisplayType('hidden');
	sublist.addField("custpage_soinernno", "text", "SONo").setDisplayType('hidden');
	sublist.addField("custpage_uomid", "text", "uomid").setDisplayType('hidden');
	sublist.addField("custpage_location", "text", "location").setDisplayType('hidden');
	sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
	sublist.addField("custpage_iteminfo1", "text", "Item Info1").setDisplayType('hidden');
	sublist.addField("custpage_iteminfo2", "text", "Item Info2").setDisplayType('hidden');
	sublist.addField("custpage_iteminfo3", "text", "Item Info3").setDisplayType('hidden');
	sublist.addField("custpage_sointernid", "text", "Item Info3").setDisplayType('hidden');
	sublist.addField("custpage_pickgenqty", "float", "Pick Gen Qty").setDisplayType('hidden');
	sublist.addField("custpage_wmsstatus", "text", "OrderLine Status");
	sublist.addField("custpage_ordcrtddate", "text", "Order Created Date");
	sublist.addField("custpage_slno", "text", "sl no").setDisplayType('hidden');
	sublist.addField("custpage_lotnoid", "text", "Lotid").setDisplayType('entry').setDisabled(true);//setDisabled(true);//added by santosh
	sublist.addField("custpage_paymenthold", "text", "Notes");
	sublist.addField("custpage_acctcreditpreference", "text", "Notes1").setDisplayType('hidden');
	sublist.addField("custpage_lineno", "text", "Line #").setDisplayType('hidden');
}

var searchResultArray=new Array();

function getOrdersForWaveGen(request,maxno,form){
	//var orderList = new Array();

	// Validating all the request parameters and pushing to a local array
	var localVarArray;
	//this is to maintain the querystring parameters while refreshing the paging drop down
	if (request.getMethod() == 'POST') {
		var queryparams=request.getParameter('custpage_qeryparams');
		//nlapiLogExecution('DEBUG', 'queryparams', queryparams);
		// queryparams=queryparams.
		var tempArray=new Array();
		tempArray.push(queryparams.split(','));
		localVarArray=tempArray;
		//nlapiLogExecution('DEBUG', 'tempArray', tempArray.length);
	}
	else
	{
		localVarArray = validateRequestParams(request,form);
		//nlapiLogExecution('DEBUG', 'localVarArray', localVarArray);
	}

	// Get all the search filters for wave generation
	var filters = new Array();   
	filters = specifyWaveFilters(localVarArray,maxno);

	// Get all the columns that the search should return
	var columns = new Array();
	columns = getWaveColumns();

	var orderlistnew = new Array();

	var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if( orderList!=null && orderList.length>=1000)
	{ 
		nlapiLogExecution('DEBUG', 'orderList length', orderList.length);

		label:for(var i=0; i<orderList.length;i++ )
		{  
			for(var j=0; j<orderlistnew.length;j++ )
			{
				if(orderlistnew[j].getValue('custrecord_lineord')==orderList[i].getValue('custrecord_lineord') 
						&& orderlistnew[j].getValue('custrecord_ordline')==orderList[i].getValue('custrecord_ordline')) 
				{
					nlapiDeleteRecord('customrecord_ebiznet_ordline',orderList[i].getId());
					continue label;
				}
			}
			orderlistnew[orderlistnew.length] = orderList[i];
		}

		searchResultArray.push(orderlistnew); 
		var maxno=orderlistnew[orderlistnew.length-1].getValue(columns[21]);
		getOrdersForWaveGen(request,maxno,form);	
	}
	else
	{		

		label:for(var i=0; orderList!=null && i<orderList.length;i++ )
		{  
			for(var j=0; j<orderlistnew.length;j++ )
			{
				if(orderlistnew[j].getValue('custrecord_lineord')==orderList[i].getValue('custrecord_lineord') 
						&& orderlistnew[j].getValue('custrecord_ordline')==orderList[i].getValue('custrecord_ordline')) 
				{
					nlapiDeleteRecord('customrecord_ebiznet_ordline',orderList[i].getId());
					continue label;
				}
			}
			orderlistnew[orderlistnew.length] = orderList[i];
		}

	searchResultArray.push(orderlistnew); 
	}

//	if( orderList!=null && orderList.length>=1000)
//	{ 
//	searchResultArray.push(orderList); 
//	var maxno=orderList[orderList.length-1].getValue(columns[21]);
//	getOrdersForWaveGen(request,maxno,form);	
//	}
//	else
//	{
//	searchResultArray.push(orderList); 
//	}
	return searchResultArray;
}

/**
 * Function to add one fulfillment order to the sublist on the form
 * 
 * @param form
 * @param currentOrder
 * @param i
 */
function addFulfilmentOrderToSublist(form, currentOrder, i,vDueDaysList,vCreditExceedList,vDueDaysRestrict,ruleValue,request){

	var vOrdQty = parseFloat(currentOrder.getValue('custrecord_ord_qty'));
	//var vnotes = 'HOLD'
	var vnotes = currentOrder.getValue('paymenteventresult','custrecord_ns_ord');
	var vPickGenQty=0;
	var folotnumber="";
	var folotqty="";
	var folotinternalid="";
	folotnumber = currentOrder.getValue('custrecord_ebiz_lot_number');
	folotqty = currentOrder.getValue('custrecord_ebiz_lotnumber_qty');
	folotinternalid = currentOrder.getValue('custrecord_ebiz_lotno_internalid');
	
	if(currentOrder.getValue('custrecord_pickgen_qty')!=null && currentOrder.getValue('custrecord_pickgen_qty')!='')	
		vPickGenQty = parseFloat(currentOrder.getValue('custrecord_pickgen_qty'));

	var vCarrier='';
	if(currentOrder.getText('custrecord_do_wmscarrier') != null && currentOrder.getText('custrecord_do_wmscarrier') != '')
		vCarrier=currentOrder.getText('custrecord_do_wmscarrier');
	else if(currentOrder.getText('custrecord_do_carrier') != null && currentOrder.getText('custrecord_do_carrier') != '')
		vCarrier=currentOrder.getText('custrecord_do_carrier');

	vOrdQty = parseFloat(vOrdQty)-parseFloat(vPickGenQty);

	form.getSubList('custpage_items').setLineItemValue('custpage_solineno', i + 1,
			currentOrder.getValue('custrecord_ordline'));

	form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1,
	'1');
	form.getSubList('custpage_items').setLineItemValue('custpage_customer', i + 1,
			currentOrder.getText('custrecord_do_customer'));
	form.getSubList('custpage_items').setLineItemValue('custpage_carrier', i + 1,
			vCarrier);
	form.getSubList('custpage_items').setLineItemValue('custpage_ordtype', i + 1,
			currentOrder.getText('custrecord_do_order_type'));
	form.getSubList('custpage_items').setLineItemValue('custpage_ordpriority', i + 1, 
			currentOrder.getText('custrecord_do_order_priority'));
	form.getSubList('custpage_items').setLineItemValue('custpage_itemno', i + 1, 
			currentOrder.getValue('custrecord_ebiz_linesku'));
	form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, 
			currentOrder.getText('custrecord_ebiz_linesku'));
	form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, 
			parseFloat(vOrdQty));
	form.getSubList('custpage_items').setLineItemValue('custpage_shipmentno', i + 1, 
			currentOrder.getValue('custrecord_shipment_no'));

	form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1, 
			currentOrder.getValue('custrecord_lineord'));
	form.getSubList('custpage_items').setLineItemValue('custpage_doinernno', i + 1, currentOrder.getId());
	form.getSubList('custpage_items').setLineItemValue('custpage_soinernno', i + 1, 
			currentOrder.getValue('name'));
	form.getSubList('custpage_items').setLineItemValue('custpage_skustatustext', i + 1, 
			currentOrder.getText('custrecord_linesku_status'));			
	form.getSubList('custpage_items').setLineItemValue('custpage_skustatus', i + 1, 
			currentOrder.getValue('custrecord_linesku_status'));
	form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, 
			currentOrder.getText('custrecord_linepackcode'));
	form.getSubList('custpage_items').setLineItemValue('custpage_uomid', i + 1, 
			currentOrder.getText('custrecord_lineuom_id'));
	if(folotnumber!=null && folotnumber!='' && folotnumber!='null' && folotqty!=null && folotqty!='' && folotqty!='null')
	{
		var folotnumberwithqty=folotnumber+"("+folotqty+")";
		form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, 
				folotnumberwithqty);
		form.getSubList('custpage_items').setLineItemValue('custpage_lotnoid', i + 1,folotinternalid);
	}
	else
	{
		form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, 
				currentOrder.getValue('custrecord_batch'));
	}
	/*form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, 
			currentOrder.getValue('custrecord_batch'));*/
	form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, 
			currentOrder.getValue('custrecord_ordline_wms_location'));
	form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo1', i + 1, 
			currentOrder.getText('custrecord_fulfilmentiteminfo1'));
	form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo2', i + 1, 
			currentOrder.getText('custrecord_fulfilmentiteminfo2'));
	form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo3', i + 1, 
			currentOrder.getText('custrecord_fulfilmentiteminfo3'));		
	form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1, 
			currentOrder.getValue('custrecord_ordline_company'));	
	form.getSubList('custpage_items').setLineItemValue('custpage_pickgenqty', i + 1, 
			currentOrder.getValue('custrecord_pickgen_qty'));
	form.getSubList('custpage_items').setLineItemValue('custpage_wmsstatus', i + 1, 
			currentOrder.getText('custrecord_linestatus_flag'));

	form.getSubList('custpage_items').setLineItemValue('custpage_ordcrtddate', i + 1, 
			currentOrder.getValue('custrecord_record_linedate'));

	form.getSubList('custpage_items').setLineItemValue('custpage_slno', i + 1, 
			i+1);
	//form.getSubList('custpage_items').setLineItemValue('custpage_lotnoid', i + 1,'');

	//added by santosh
	//form.getSubList('custpage_items').setLineItemValue('custpage_lotbatchlink', i + 1,"<a href='"+lotnourl+"' target='_blank'>Lot#</a>");
	//form.getSubList('custpage_items').setLineItemValue('custpage_lotbatchlink', i + 1,('<a href="#" onclick="javascript:window.open('"'"+lotnourl+"'"');">Lot#</a>'));
//	added by santosh
	//codechanges for issue# 20121503
	var fields = ['recordType', 'custitem_ebizbatchlot'];
	var columns =  nlapiLookupField('item', currentOrder.getValue('custrecord_ebiz_linesku'),fields);


	var vItemType = '';
	var BatchFlag = '';
	if(columns != null && columns != '')
	{
		vItemType = columns.recordType;	
		nlapiLogExecution('DEBUG','vItemType1',vItemType);
		BatchFlag = columns.custitem_ebizbatchlot;
	}

	//codechanges for issue# 20121503
	if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || BatchFlag =='T')
	{
		var splitflag=currentOrder.getValue('custentity_ebiz_notto_split','custrecord_do_customer');
		nlapiLogExecution('DEBUG','splitflag',splitflag);
		var vseq=i+1;
		var vcntrl=vseq;
		var lotnourl = nlapiResolveURL('SUITELET', 'customscript_ebiz_invtforitem', 'customdeploy_ebiz_invtforitem_di') + "&custparam_item=" + currentOrder.getValue('custrecord_ebiz_linesku')+"&custparam_itemstatus="+currentOrder.getValue('custrecord_linesku_status')+"&custparam_location="+currentOrder.getValue('custrecord_ordline_wms_location')+"&custparam_qty="+vOrdQty+"&cus_sl="+vcntrl+"&cus_flag="+splitflag;


		//form.getSubList('custpage_items').setLineItemValue('custpage_lotbatchlink', i + 1, "<a href=javascript:newwindow=window.open('" + lotnourl + "'); if (!newwindow.closed) {newwindow.focus()}; target='_blank' \" > Lot#</a>");
		if(folotnumber==null || folotnumber=='' || folotnumber=='null' || folotqty==null || folotqty=='' || folotqty=='null')
		{
			form.getSubList('custpage_items').setLineItemValue('custpage_lotbatchlink', i + 1, "<a href=\"#\" onclick=\"javascript:window.open('" + lotnourl + "');\" >Select Lot#</a>");
		}
	}
	//end of the code

	var vCustomer=currentOrder.getValue('entity','custrecord_ns_ord');
	var vStatus = currentOrder.getValue('status','custrecord_ns_ord');

	if(vStatus == 'closed' || vStatus == 'cancelled')
	{
		vnotes = vStatus.toUpperCase();
		form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, '<font color=#ff0000><b>' +  vnotes + '</b></font>');
	}
	else
	{
		if(vnotes == 'HOLD')
			form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, '<font color=#ff0000><b>' +  vnotes + '</b></font>');	//Payment hold status
		else if((vnotes == null || vnotes == '') && (vDueDaysList != null && vDueDaysList != '') || (vCreditExceedList != null && vCreditExceedList != ''))
		{
			if(vDueDaysRestrict != null && vDueDaysRestrict != '' && parseInt(vDueDaysRestrict)>0 && vDueDaysList!=null && vDueDaysList!='')
			{
				for(var k=0;k<vDueDaysList.length;k++)
				{
					if(vDueDaysList[k].getValue('internalid',null,'group')==vCustomer)
					{					
						vnotes='Days Over Due';
						form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, '<font color=#ff0000><b>' +  vnotes + '</b></font>');	//Payment hold status
						return;
					}	
				}
			}
			if(vCreditExceedList != null && vCreditExceedList != '')
			{
				for(var k=0;k<vCreditExceedList.length;k++)
				{				
					if(vCreditExceedList[k].getValue('internalid',null,'group')==vCustomer)
					{
						vnotes='Credit Limit Exceed';
						form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, '<font color=#ff0000><b>' +  vnotes + '</b></font>');	//Payment hold status
						return;
					}	
				}  
			}
		}
		else
			form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, vnotes);

		if(ruleValue !=null && ruleValue !='')
		{
			form.getSubList('custpage_items').setLineItemValue('custpage_acctcreditpreference', i + 1, ruleValue);
		}
	}

}

function validateRequestParams(request,form){
	var soNo = "";
	var companyName = "";
	var itemName= "";
	var customerName = "";
	var orderPriority = "";
	var orderType = "";
	var itemGroup = "";
	var itemFamily = "";
	var packCode = "";
	var UOM = "";
	var itemStatus = "";
	var itemInfo1 = "";
	var itemInfo2 = "";
	var itemInfo3 = "";
	var shippingCarrier = "";
	var shipCountry = ""; 
	var shipState = "";
	var shipCity = "";
	var shipAddr1 = "";
	var shipmentNo = "";
	var routeNo = "";
	var carrier = "";
	var shipdate = "";
	var vSite="";
	var wmsstatusflag = "",orderdate="";

	var localVarArray = new Array();


	if (request.getParameter('custpage_site')!=null && request.getParameter('custpage_site')!="" ){
		vSite=request.getParameter('custpage_site'); 
	}
	if (request.getParameter('custpage_qbso') != null && request.getParameter('custpage_qbso') != "") {
		soNo = request.getParameter('custpage_qbso');
	}

	if (request.getParameter('custpage_company') != null && request.getParameter('custpage_company') != "") {
		companyName = request.getParameter('custpage_company');
	}

	if (request.getParameter('custpage_item') != null && request.getParameter('custpage_item') != "") {
		itemName = request.getParameter('custpage_item');
	}

	if (request.getParameter('custpage_consignee') != null && request.getParameter('custpage_consignee') != "") {
		customerName = request.getParameter('custpage_consignee');
	}

	if (request.getParameter('custpage_ordpriority') != null && request.getParameter('custpage_ordpriority') != "") {
		orderPriority = request.getParameter('custpage_ordpriority');
	}

	if (request.getParameter('custpage_ordtype') != null && request.getParameter('custpage_ordtype') != "") {
		orderType = request.getParameter('custpage_ordtype');
	}

	if (request.getParameter('custpage_itemgroup') != null && request.getParameter('custpage_itemgroup') != "") {
		itemGroup = request.getParameter('custpage_itemgroup');
	}

	if (request.getParameter('custpage_itemfamily') != null && request.getParameter('custpage_itemfamily') != "") {
		itemFamily = request.getParameter('custpage_itemfamily');
	}

	if (request.getParameter('custpage_packcode') != null && request.getParameter('custpage_packcode') != "") {
		packCode = request.getParameter('custpage_packcode');//custrecord_linepackcode
	}

	if (request.getParameter('custpage_uom') != null && request.getParameter('custpage_uom') != "") {
		UOM = request.getParameter('custpage_uom');//custrecord_lineuom_id
	}

	if (request.getParameter('custpage_itemstatus') != null && request.getParameter('custpage_itemstatus') != "") {
		itemStatus = request.getParameter('custpage_itemstatus'); //custrecord_linesku_status
	}

	if (request.getParameter('custpage_siteminfo1')!=null && request.getParameter('custpage_siteminfo1')!="" ){
		itemInfo1=request.getParameter('custpage_siteminfo1'); 
	}

	if (request.getParameter('custpage_siteminfo2')!=null && request.getParameter('custpage_siteminfo2')!="" ){
		itemInfo2=request.getParameter('custpage_siteminfo2'); 
	}

	if (request.getParameter('custpage_siteminfo3')!=null && request.getParameter('custpage_siteminfo3')!="" ){
		itemInfo3=request.getParameter('custpage_siteminfo3'); 
	}

	if (request.getParameter('custpage_shippingcarrier')!=null && request.getParameter('custpage_shippingcarrier')!="" ){
		shippingCarrier=request.getParameter('custpage_shippingcarrier'); 
	}

	if (request.getParameter('custpage_country')!=null && request.getParameter('custpage_country')!="" ){
		shipCountry=request.getParameter('custpage_country'); 
	}

	if (request.getParameter('custpage_state')!=null && request.getParameter('custpage_state')!="" ){
		shipState=request.getParameter('custpage_state'); 
	}

	if (request.getParameter('custpage_city')!=null && request.getParameter('custpage_city')!="" ){
		shipCity=request.getParameter('custpage_city'); 
	}

	if (request.getParameter('custpage_addr1')!=null && request.getParameter('custpage_addr1')!="" ){
		shipAddr1=request.getParameter('custpage_addr1'); 
	}

	if (request.getParameter('custpage_shipmentno')!=null && request.getParameter('custpage_shipmentno')!="" ){
		shipmentNo=request.getParameter('custpage_shipmentno'); 
	}

	if (request.getParameter('custpage_routeno')!=null && request.getParameter('custpage_routeno')!="" ){
		routeNo=request.getParameter('custpage_routeno'); 
	}
	//added carrier By Suman
	if (request.getParameter('custpage_carrier')!=null && request.getParameter('custpage_carrier')!="" ){
		carrier=request.getParameter('custpage_carrier'); 
	}

	if (request.getParameter('custpage_soshipdate')!=null && request.getParameter('custpage_soshipdate')!="" ){
		shipdate=request.getParameter('custpage_soshipdate'); 
	}

	if (request.getParameter('custpage_wmsstatusflag')!=null && request.getParameter('custpage_wmsstatusflag')!="" ){
		wmsstatusflag=request.getParameter('custpage_wmsstatusflag'); 
	}
	if (request.getParameter('custpage_soorderdate')!=null && request.getParameter('custpage_soorderdate')!="" ){
		orderdate=request.getParameter('custpage_soorderdate'); 
	}
	nlapiLogExecution('DEBUG','request.getParameter(custpage_wmsstatusflag)',request.getParameter('custpage_wmsstatusflag'));
	var currentRow = [soNo, companyName, itemName, customerName, orderPriority, orderType, itemGroup, itemFamily,
	                  packCode, UOM, itemStatus, itemInfo1, itemInfo2, itemInfo3, shippingCarrier, shipCountry, 
	                  shipState, shipCity, shipAddr1, shipmentNo, routeNo, carrier, shipdate,wmsstatusflag,orderdate,vSite];
	localVarArray.push(currentRow);
	if (request.getParameter('custpage_qeryparams')==null )
	{
		var hiddenQueryParams=form.getField('custpage_qeryparams');
		nlapiLogExecution('DEBUG','localVarArray',localVarArray.toString());
		hiddenQueryParams.setDefaultValue(localVarArray.toString());
	}
	return localVarArray;
}

/**
 * Function to articulate all the search filters for wave generation
 * 
 * @param localVarArray
 * @returns {Array}
 */
function specifyWaveFilters(localVarArray,maxno){
	var filters = new Array();

	var str = 'Sales Order No. = ' + localVarArray[0][0] + '<br>';
	str = str + 'Company. = ' + localVarArray[0][1] + '<br>';
	str = str + 'Item Name. = ' + localVarArray[0][2] + '<br>';
	str = str + 'Customer Name. = ' + localVarArray[0][3] + '<br>';
	str = str + 'Order Priority. = ' + localVarArray[0][4] + '<br>';
	str = str + 'Order Type. = ' + localVarArray[0][5] + '<br>';
	str = str + 'Item Group. = ' + localVarArray[0][6] + '<br>';
	str = str + 'Item Family. = ' + localVarArray[0][7] + '<br>';
	str = str + 'Pack Code. = ' + localVarArray[0][8] + '<br>';
	str = str + 'UOM. = ' + localVarArray[0][9] + '<br>';
	str = str + 'Item Status. = ' + localVarArray[0][10] + '<br>';
	str = str + 'Item Info 1. = ' + localVarArray[0][11] + '<br>';
	str = str + 'Item Info 2. = ' + localVarArray[0][12] + '<br>';
	str = str + 'Item Info 3. = ' + localVarArray[0][13] + '<br>';
	str = str + 'Shipping Carrier. = ' + localVarArray[0][14] + '<br>';
	str = str + 'Ship Country. = ' + localVarArray[0][15] + '<br>';
	str = str + 'Ship State. = ' + localVarArray[0][16] + '<br>';
	str = str + 'Ship Address. = ' + localVarArray[0][17] + '<br>';
	str = str + 'Shipiment No. = ' + localVarArray[0][19] + '<br>';
	str = str + 'Route No. = ' + localVarArray[0][20] + '<br>';
	str = str + 'WMS Carrier. = ' + localVarArray[0][21] + '<br>';
	str = str + 'Ship Date. = ' + localVarArray[0][22] + '<br>';
	str = str + 'Status Flag. = ' + localVarArray[0][23] + '<br>';
	str = str + 'Location. = ' + localVarArray[0][25] + '<br>';
	str = str + 'Max No. = ' + maxno + '<br>';

	nlapiLogExecution('DEBUG', 'Filters ', str);


	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);

	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', vRoleLocation));
	}
	// Sales Order No
	if(localVarArray[0][0] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][0]));

	// Company
	if(localVarArray[0][1] != "")
		filters.push(new nlobjSearchFilter('custrecord_ordline_company', null, 'is', localVarArray[0][1]));

	// Item Name
	if(localVarArray[0][2] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'is', localVarArray[0][2]));

	// Customer Name
	if(localVarArray[0][3] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_customer', null, 'is', localVarArray[0][3]));

	// Order Priority
	if(localVarArray[0][4] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_order_priority', null, 'is', localVarArray[0][4]));

	// Order Type
	if(localVarArray[0][5] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_order_type', null, 'is', localVarArray[0][5]));

	// Item Group
	if(localVarArray[0][6] != "")
		//filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][6]));
		filters.push(new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_linesku', 'is', localVarArray[0][6]));

	// Item Family
	if(localVarArray[0][7] != "")
		//filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][7]));
		filters.push(new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_linesku', 'is', localVarArray[0][7]));

	// Pack Code
	if(localVarArray[0][8] != "")
		filters.push(new nlobjSearchFilter('custrecord_linepackcode', null, 'is', localVarArray[0][8]));

	// UOM
	if(localVarArray[0][9] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineuom_id', null, 'is', localVarArray[0][9]));

	// Item Status
	if(localVarArray[0][10] != "")
		filters.push(new nlobjSearchFilter('custrecord_linesku_status', null, 'is', localVarArray[0][10]));

	// Item Info 1
	if(localVarArray[0][11] != "")
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));

	// Item Info 2
	if(localVarArray[0][12] != "")
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo2', null, 'is', localVarArray[0][12]));

	// Item Info 3
	if(localVarArray[0][13] != "")
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo3', null, 'is', localVarArray[0][13]));

	// Shipping Carrier
	if(localVarArray[0][14] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_carrier',null, 'anyof', [localVarArray[0][14]]));	
		//filters.push(new nlobjSearchFilter('shipmethod', 'custrecord_ns_ord', 'anyof', [localVarArray[0][14]]));
	 //above filter was commented beacuse instead of checking shipmethod from Salesorder, checked from FO 

	// Ship Country
	if(localVarArray[0][15] != "")
		filters.push(new nlobjSearchFilter('shipcountry', 'custrecord_ns_ord', 'anyof', [localVarArray[0][15]]));

	// Ship State
	if(localVarArray[0][16] != "")
		filters.push(new nlobjSearchFilter('shipstate', 'custrecord_ns_ord', 'is', localVarArray[0][16]));

	// Ship City
	if(localVarArray[0][17] != "")
		filters.push(new nlobjSearchFilter('shipcity', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Masquerading the ship address with ship city
	if(localVarArray[0][18] != "")
		filters.push(new nlobjSearchFilter('shipaddress', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Shipment No.
	if(localVarArray[0][19] != "")
		filters.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'is', localVarArray[0][19]));

	// Route No.
	if(localVarArray[0][20] != "")
		filters.push(new nlobjSearchFilter('custrecord_route_no', null, 'anyof', [localVarArray[0][20]]));

	//11-Partially Picked, 13-Partially Shipped,15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filters.push(new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [11,13,15,25,26]));

	// Indicates the Sales Order header
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ns_ord', 'is', 'T'));			

	// WMS Carrier Added By Suman
	if(localVarArray[0][21] != "")
		//filters.push(new nlobjSearchFilter('custrecord_do_wmscarrier', null, 'anyof', [localVarArray[0][21]]));
		filters.push(new nlobjSearchFilter('custrecord_do_carrier', null, 'anyof', [localVarArray[0][21]]));// Case# 20148491

//	//Ship Date
//	if(localVarArray[0][22] != "")
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'on', [localVarArray[0][22]]));
	nlapiLogExecution('DEBUG','Shipdate',localVarArray[0][22]);
	if((localVarArray[0][22] != "" && localVarArray[0][22] != null))
	{
		//modified as on 15th July by suman.
		//If ship date is empty then replace the value with the created date of FO
		filters.push(new nlobjSearchFilter('formuladate', null, 'onorbefore', [localVarArray[0][22]]).setFormula("nvl({custrecord_ebiz_shipdate},{created})"));
	}

	//Status Flag
	if(localVarArray[0][23] != "")
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[0][23]]));

	//MaxNo
	filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));

	filters.push(new nlobjSearchFilter('custrecord_ord_qty', null, 'greaterthan', 0));

	if(localVarArray[0][24] != "")
		filters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'on', [localVarArray[0][24]]));

	if(localVarArray[0][25] !=null && localVarArray[0][25] !='')
		filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',localVarArray[0][25]));

	//new filter added by suman as on 040114
	//case# 20149816 starts (custrecord_pickqty is changed to custrecord_pickgen_qty)
	//filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickqty}),0)"));
	filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickgen_qty}),0)"));
	//case# 20149816 ends
	filters.push(new nlobjSearchFilter('isinactive',"custrecord_ebiz_linesku", 'is', "F"));
	//end of the code

	return filters;
}

/**
 * 
 * @returns {Array}
 */
function getWaveColumns(){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_linesku_status');
	columns[1] = new nlobjSearchColumn('custrecord_do_customer');
	columns[2] = new nlobjSearchColumn('custrecord_do_carrier');
	columns[3] = new nlobjSearchColumn('custrecord_do_order_type');
	columns[4] = new nlobjSearchColumn('custrecord_do_order_priority');
	columns[5] = new nlobjSearchColumn('custrecord_lineord');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_linesku');
	columns[7] = new nlobjSearchColumn('custrecord_ord_qty');
	columns[8] = new nlobjSearchColumn('name');
	//columns[8].setSort();
	columns[9] = new nlobjSearchColumn('custrecord_lineord');
	columns[9].setSort();		
	columns[10] = new nlobjSearchColumn('custrecord_ordline');
	columns[10].setSort();
	columns[11] = new nlobjSearchColumn('custrecord_linepackcode');
	columns[12] = new nlobjSearchColumn('custrecord_lineuom_id');
	columns[13] = new nlobjSearchColumn('custrecord_batch');
	columns[14] = new nlobjSearchColumn('custrecord_ordline_wms_location');
	columns[15] = new nlobjSearchColumn('custrecord_ordline_company');
	columns[16] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo1');
	columns[17] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo2');
	columns[18] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo3');	
	columns[19] = new nlobjSearchColumn('custrecord_pickgen_qty');	
	columns[20] = new nlobjSearchColumn('custrecord_linestatus_flag');
	columns[21] = new nlobjSearchColumn('id');
	columns[21].setSort();
	columns[22] = new nlobjSearchColumn('custrecord_ns_ord');
	columns[22].setSort();
	columns[23] = new nlobjSearchColumn('custrecord_do_wmscarrier');
	columns[24] = new nlobjSearchColumn('custrecord_shipment_no');
	columns[25] = new nlobjSearchColumn('custrecord_record_linedate');
	columns[26] = new nlobjSearchColumn('custentity_ebiz_notto_split','custrecord_do_customer');
	columns[27] = new nlobjSearchColumn('paymenteventresult','custrecord_ns_ord');
	columns[28] = new nlobjSearchColumn('entity','custrecord_ns_ord');//Customer
	columns[29] = new nlobjSearchColumn('status','custrecord_ns_ord');//status
	columns[30] = new nlobjSearchColumn('custrecord_ebiz_lot_number');//status
	columns[31] = new nlobjSearchColumn('custrecord_ebiz_lotnumber_qty');//status
	columns[32] = new nlobjSearchColumn('custrecord_ebiz_lotno_internalid');//status


	return columns;
}


/**
 * 
 * @param request
 * @param response
 */
function generateWave(request, response){
	nlapiLogExecution('DEBUG','Into generateWave');
	try{

		var form = nlapiCreateForm('Wave Selection by Line');
		form.setScript('customscript_wavegeneration_cl');
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');
		nlapiLogExecution('DEBUG','custpage_hiddenfieldselectpage',request.getParameter('custpage_hiddenfieldselectpage'));
		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','Remaining usage at the start',context.getRemainingUsage());

		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
		var linestatus="";
		if(request.getParameter('custpage_qeryparams')!=null)
		{
			hiddenQueryParams.setDefaultValue(request.getParameter('custpage_qeryparams'));
			var hiddenQueryParamsval = request.getParameter('custpage_qeryparams');
			if(hiddenQueryParamsval!=null && hiddenQueryParamsval!='')
			{
				nlapiLogExecution('DEBUG','hiddenQueryParamsval',hiddenQueryParamsval);
				var temparray=hiddenQueryParamsval.split(',');
				nlapiLogExecution('DEBUG','temparray 1',temparray);
				nlapiLogExecution('DEBUG','temparray length',temparray.length);
				nlapiLogExecution('DEBUG','temparray 2',temparray[0]);

				if(temparray.length>0 && temparray[23]!='')
				{ 
					linestatus=temparray[23];				
				}
			}
		}
		nlapiLogExecution('DEBUG','linestatus',linestatus);
		nlapiLogExecution('DEBUG','vWaveno',vWaveno);
		if(request.getParameter('custpage_hiddenfieldselectpage')=='F')
		{
			var vWaveno = request.getParameter('custpage_hiddenwaveno');
			nlapiLogExecution('DEBUG','vWaveno',vWaveno);

			/*
			 * get the list of fulfillment orders selected
			 * retrieve the fulfillment order#, sku, order qty, committed qty
			 * 
			 */
			// Validate all request parameters


			var hiddenwaveno = form.addField('custpage_hiddenwaveno', 'text', '').setDisplayType('hidden');
			// Get the next WAVE and REPORT sequences

			var eBizWaveNo = '';
			var eBizReportNo = '';

			if(vWaveno==null || vWaveno=='')
			{
				eBizWaveNo = GetMaxTransactionNo('WAVE');
				eBizReportNo = GetMaxTransactionNo('REPORT');
				nlapiLogExecution('DEBUG', 'Wave & Report No.', eBizWaveNo + '|', eBizReportNo);
				hiddenwaveno.setDefaultValue(eBizWaveNo);
			}
			else
			{
				eBizWaveNo = vWaveno;			
				nlapiLogExecution('DEBUG', 'Wave & Report No.', eBizWaveNo + '|', eBizReportNo);
				hiddenwaveno.setDefaultValue(eBizWaveNo);
				// The below code is for sending mail alerts for missing lines in a wave - Satish.N on 07/23/2012.
				try
				{
					// To fetch the recent line from fo line.
					var ordlist = getRecentOrdLinebyWave(vWaveno);
					if(ordlist!=null && ordlist!='' && ordlist.length>0)
					{
						var vordno = ordlist[ordlist.length-1].getValue('custrecord_lineord');
						var vordlineno = ordlist[ordlist.length-1].getValue('custrecord_ordline');

						// To check whether the line exist in wave order details.
						var waveorderList = getRecentWaveOrdbyWave(vWaveno,vordno,vordlineno);
						if(waveorderList!=null && waveorderList!='' && waveorderList.length>0)
						{

						}
						else
						{
							// If there is no record raise an exception.
							var exceptionname = 'Wave Creation';
							var functionality = 'Missing Order in Wave';
							var expmsg = 'Line # ' +vordlineno+ ' of Order ' +vordno+ ' is missing in Wave '+vWaveno;
							InsertExceptionLog(exceptionname,trantype, functionality, expmsg, vordno, '','','','', context.getUser());
						}

					}
				}
				catch(exp)
				{
					nlapiLogExecution('DEBUG', 'Error in sending mail alert' , +exp);
				}

				// Upto here - Satish.N on 07/23/2012.
			}
			// Get the list of fulfillment orders selected for wave generation
			// Every element of the returned list contains index, soInternalID, lineNo, doNo, doName, 
			// itemName, itemNo, itemStatus, availQty, orderQty, packCode, uom, lotBatchNo, company, 
			// itemInfo1, itemInfo2, itemInfo3, orderType and orderPriority
			var fulfillOrderList = getSelectedFulfillmentOrderInfo(request);

			if(fulfillOrderList !=null)
				nlapiLogExecution('DEBUG', 'fulfillOrderList.length', fulfillOrderList.length);

			// Proceed only if there is atleast 1 fulfillment order selected
			if(fulfillOrderList!=null && fulfillOrderList.length > 0){


				var waveDetails = new Array();
				nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);
				waveDetails["ebiz_wave_no"] = eBizWaveNo;
				waveDetails["short_pick"] = 'N';
				createwaveordrecord(form,fulfillOrderList,eBizWaveNo,linestatus);

//				form.addButton('custpage_print','Print Pick Report','Printreport2('+eBizWaveNo+')');
//				form.setScript('customscript_pickreportprint');
//				showInlineMessage(form, 'Confirmation', 'Wave generation completed successfully ', ' Wave# = ' + eBizWaveNo);

				response.sendRedirect('SUITELET', 'customscript_wavepickrpt', 'customdeploy_wavepickrpt', false, waveDetails);
				response.writePage(form);

			}


			nlapiLogExecution('DEBUG','Remaining usage at the end',context.getRemainingUsage());
		}
		else
		{
			//Create the sublist
			createWaveGenSublist(form);
			var vDueDaysList=getDueDaysList();
			var vCreditExceedList=getCreditExceedList();
			var orderList = getOrdersForWaveGen(request,0,form);
			var vConfig=nlapiLoadConfiguration('accountingpreferences');
			var ruleValue=vConfig.getFieldValue('CUSTCREDLIMHANDLING');
			var vDueDaysList=null;
			var vCreditExceedList=null;
			var vDueDaysRestrict=null;
			//Add two functions
			if(ruleValue!=null && ruleValue!='' && ruleValue!='null' && ruleValue!='NONE')
			{
				vDueDaysList=getDueDaysList();
				vCreditExceedList=getCreditExceedList();

				//var vConfig=nlapiLoadConfiguration('accountingpreferences');

				if(vConfig != null && vConfig != '')
				{
					vDueDaysRestrict=vConfig.getFieldValue('CREDLIMDAYS');
				}
			}

			if(orderList != null && orderList.length > 0){
				setPagingForSublist(orderList,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,ruleValue,request);
			}
			form.addSubmitButton('Generate Wave');
		}

		response.writePage(form);
	}


	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in generatewave ', exp);	
		showInlineMessage(form, 'DEBUG', 'Wave Generation Failed', "");
		response.writePage(form);
	}
}


function getRecentOrdLinebyWave(ebizwaveno)
{
	nlapiLogExecution('DEBUG', 'Into getRecentOrdLinebyWave');

	var filters = new Array();
	var columns = new Array();
	var orderList = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'is', ebizwaveno));
	filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [15]));

	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_lineord');
	columns[1].setSort();		
	columns[2] = new nlobjSearchColumn('custrecord_ordline');
	columns[2].setSort();

	orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	nlapiLogExecution('DEBUG', 'Out of getRecentOrdLinebyWave');

	return orderList;

}

function getRecentWaveOrdbyWave(ebizwave,ordno,ordline)
{
	nlapiLogExecution('DEBUG', 'Into getRecentWaveOrdbyWave');

	var filters = new Array();
	var columns = new Array();
	var waveorderList = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_waveorder_waveno', null, 'is', ebizwave));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_waveorder_orderno', null, 'is', ordno));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_waveorder_orderline', null, 'is', ordline));

	columns[0] = new nlobjSearchColumn('ebizwave');
	columns[1] = new nlobjSearchColumn('ordno');
	columns[2] = new nlobjSearchColumn('ordline');

	waveorderList = nlapiSearchRecord('customrecord_ebiz_waveord', null, filters, columns);

	nlapiLogExecution('DEBUG', 'Out of getRecentWaveOrdbyWave');

	return waveorderList;

}

function setPagingForSublist(orderList,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,ruleValue,request)
{
	nlapiLogExecution('DEBUG', 'Into setPagingForSublist');
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('DEBUG', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			//nlapiLogExecution('DEBUG', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				//nlapiLogExecution('DEBUG', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		nlapiLogExecution('DEBUG', 'orderListArray.length ', orderListArray.length); 
		var test='';

		if(orderListArray.length>0)
		{

			if(orderListArray.length>100)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("100");
					pagesizevalue=100;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue= 100;
						pagesize.setDefaultValue("100");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}
			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				//nlapiLogExecution('DEBUG', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				//nlapiLogExecution('DEBUG', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{

					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						//nlapiLogExecution('DEBUG', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						//nlapiLogExecution('DEBUG', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						//nlapiLogExecution('DEBUG', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}


			var c=0;
			var minvalue;
			minvalue=minval;

			for(var j = minvalue; j < maxval; j++){		

				var currentOrder = orderListArray[j];
				addFulfilmentOrderToSublist(form, currentOrder, c,vDueDaysList,vCreditExceedList,vDueDaysRestrict,ruleValue,request);
				c=c+1;
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of setPagingForSublist');
}
function createwaveordrecord(form,fulfillOrderList,eBizWaveNo,vlineStatus)
{
	nlapiLogExecution('DEBUG', 'Into  createwaveordrecord ', eBizWaveNo);	

	try
	{
		if(fulfillOrderList != null && fulfillOrderList.length > 0)
		{
			var context = nlapiGetContext();
			var currentUserID = context.getUser();	
			var count=0;

			var filters = new Array();

			//setting up the filters to search for blanks records in the current user�s record pool
			filters.push(new nlobjSearchFilter('custrecord_ebiz_waveorder_usedflag',null, 'is', 'F'));
			filters.push(new nlobjSearchFilter('custrecord_ebiz_waveorder_user',null, 'is', currentUserID));

			var blankresults = nlapiSearchRecord('customrecord_ebiz_waveord',null, filters);

			var prevordno='';
			var lastline='F';
			var lastlineinwave='F';

			if(blankresults!=null && blankresults!='' && (blankresults.length>=fulfillOrderList.length))
			{
				for(var i = 0; i < fulfillOrderList.length; i++)
				{					
					var ordno=fulfillOrderList[i][4];
					var ordlineno=fulfillOrderList[i][2];
					var company=fulfillOrderList[i][13];
					var location=fulfillOrderList[i][19];
					var fointrid=fulfillOrderList[i][3];


					var lotnums=fulfillOrderList[i][20];
					var Qtys=fulfillOrderList[i][21];
					var lotnoInternalId=fulfillOrderList[i][22];
					var vlotnumbers="";
					var vLotQty="";
					if(lotnums!=null && lotnums!="" && Qtys!=null && Qtys!=""){
						vlotnumbers=lotnums.substring(0,lotnums.length-1);//added by santosh
						vLotQty=Qtys.substring(0,Qtys.length-1);//added by santosh
					}

					var str = 'Prev Order No. = ' + prevordno + '<br>';
					str = str + 'Order No. = ' + ordno + '<br>';
					str = str + 'Fulfill Order Intr Id. = ' + fointrid + '<br>';
					str = str + 'Fulfillment Order List Length = ' + fulfillOrderList.length + '<br>';
					str = str + 'i = ' + i;
					nlapiLogExecution('DEBUG', 'Log for Iteration', str);

					if(prevordno!=ordno)
					{	
						if(i==fulfillOrderList.length-1)
							lastline='T';					
						else if(ordno != fulfillOrderList[i+1][4])
							lastline='T';
						else
							lastline='F';

						prevordno=ordno;
					}
					else
						if(i==fulfillOrderList.length-1)
							lastline='T';
						else if(ordno != fulfillOrderList[i+1][4])
							lastline='T';
						else 						
							lastline='F';

					if(i==fulfillOrderList.length-1)
						lastlineinwave='T';
					else
						lastlineinwave='F';
					var vAllowWave='T';
					if(fointrid != null && fointrid != '')
					{	
						var vFOLine= nlapiLoadRecord('customrecord_ebiznet_ordline', fointrid);
						if(vFOLine != null && vFOLine != '')
						{	
							var vFOPresentStatus= vFOLine.getFieldValue('custrecord_linestatus_flag');
							nlapiLogExecution('DEBUG', 'vFOPresentStatus', vFOPresentStatus);
							nlapiLogExecution('DEBUG', 'vlineStatus', vlineStatus);
							/*if(vFOPresentStatus == '15' && vlineStatus != '15')
								vAllowWave='F';*/
							if(vlineStatus != null && vlineStatus != '' && vFOPresentStatus != vlineStatus)
								vAllowWave='F';
						}
					}
					if(vAllowWave == 'T')
					{
						var vBoolupdate= updateFulfilmentOrder(fointrid, eBizWaveNo,vlotnumbers,vLotQty,lotnoInternalId);

						nlapiLogExecution('DEBUG', 'updateFulfilmentOrder',vBoolupdate);

						nlapiLogExecution('DEBUG', 'Updating wave ord record starts',TimeStampinSec());
						//var start=new Date();
						if(vBoolupdate)
						{
							var recId = blankresults[count].getId();

							var fieldNames = new Array(); 
							fieldNames.push('custrecord_ebiz_waveorder_usedflag');  
							fieldNames.push('custrecord_ebiz_waveorder_company');
							fieldNames.push('custrecord_ebiz_waveorder_location');
							fieldNames.push('custrecord_ebiz_waveorder_waveno');
							fieldNames.push('custrecord_ebiz_waveorder_orderno');
							fieldNames.push('custrecord_ebiz_waveorder_orderline');
							fieldNames.push('custrecord_ebiz_waveorder_lastline');
							fieldNames.push('custrecord_ebiz_waveorder_wavelastline');

							fieldNames.push('custrecord_ebiz_lotnumber');//added by santosh
							fieldNames.push('custrecord_ebiz_lotno_qty');//added by santosh
							fieldNames.push('custrecord_ebiz_lotnumber_internalid');//added by santosh



							var newValues = new Array(); 
							newValues.push('T');
							newValues.push(company);
							newValues.push(location);
							newValues.push(eBizWaveNo);
							newValues.push(ordno);
							newValues.push(ordlineno);					
							newValues.push(lastline);	
							newValues.push(lastlineinwave);		

							newValues.push(vlotnumbers);	//added by santosh
							newValues.push(vLotQty);//added by santosh
							newValues.push(lotnoInternalId);//added by santosh



							nlapiSubmitField('customrecord_ebiz_waveord', recId, fieldNames, newValues);
						}
					}
//					var waveparam = new Array();
//					waveparam['custscript_usedflag'] = 'T';
//					waveparam['custscript_company'] = company;
//					waveparam['custscript_location'] = location;					
//					waveparam['custscript_waveno'] = eBizWaveNo;
//					waveparam['custscript_ordno'] = ordno;
//					waveparam['custscript_ordlineno'] = ordlineno;
//					waveparam['custscript_ordlastline'] = lastline;
//					waveparam['custscript_wavelastline'] = lastlineinwave;
//					waveparam['custscript_waveordrecid'] = recId;					
//					waveparam['custscript_userid'] = currentUserID;

//					var status = nlapiScheduleScript('customscript_pickgenschscript', 'customdeploy_pickgenschscript',waveparam);
//					nlapiLogExecution('DEBUG', 'status',status);

//					var end=new Date();

//					var elapsed = (parseFloat(end.getTime()) - parseFloat(start.getTime()));

//					nlapiLogExecution('DEBUG', 'Elapsed Time for one record: ',elapsed);

					count++;

					nlapiLogExecution('DEBUG', 'Updating wave ord record ends',TimeStampinSec());

					nlapiLogExecution('DEBUG', 'Remaining Blank Rows: ',parseFloat(blankresults.length)-parseFloat(count));
				}

				//invoke a sched script to replenish the current user�s pool of blank records
				var param = new Array();
				param['custscript_employee'] = currentUserID;
				param['custscript_replenish'] = count;

				nlapiScheduleScript('customscript_replenish_waveord', 'customdeploy_replenish_waveord',param);

			}
			else
			{
				for(var i = 0; i < fulfillOrderList.length; i++)
				{					
					var ordno=fulfillOrderList[i][4];
					var ordlineno=fulfillOrderList[i][2];
					var company=fulfillOrderList[i][13];
					var location=fulfillOrderList[i][19];
					var fointrid=fulfillOrderList[i][3];

					var lotnums=fulfillOrderList[i][20];
					var Qtys=fulfillOrderList[i][21];
					var lotnoInternalId=fulfillOrderList[i][22];
					var vlotnumbers="";
					var vLotQty="";
					if(lotnums!=null && lotnums!="" && Qtys!=null && Qtys!=""){
						vlotnumbers=lotnums.substring(0,lotnums.length-1);//added by santosh
						vLotQty=Qtys.substring(0,Qtys.length-1);//added by santosh
					}

					var str = 'Prev Order No. = ' + prevordno + '<br>';
					str = str + 'Order No. = ' + ordno + '<br>';
					str = str + 'Fulfill Order Intr Id. = ' + fointrid + '<br>';
					str = str + 'Fulfillment Order List Length = ' + fulfillOrderList.length + '<br>';
					str = str + 'i = ' + i;
					nlapiLogExecution('DEBUG', 'Log for Iteration', str);

					if(i<fulfillOrderList.length-1)				
						nlapiLogExecution('DEBUG', 'Next Order', fulfillOrderList[i+1][4]);

					if(prevordno!=ordno)
					{	
						if(i==fulfillOrderList.length-1)
							lastline='T';					
						else if(ordno != fulfillOrderList[i+1][4])
							lastline='T';
						else
							lastline='F';

						prevordno=ordno;
					}
					else
						if(i==fulfillOrderList.length-1)
							lastline='T';
						else if(ordno != fulfillOrderList[i+1][4])
							lastline='T';
						else 						
							lastline='F';

					if(i==fulfillOrderList.length-1)
						lastlineinwave='T';
					else
						lastlineinwave='F';


					nlapiLogExecution('DEBUG', 'lastline ', lastline);

					//updateFulfilmentOrder(ordno, ordlineno, eBizWaveNo);
					var vAllowWave='T';
					if(fointrid != null && fointrid != '')
					{	
						var vFOLine= nlapiLoadRecord('customrecord_ebiznet_ordline', fointrid);
						if(vFOLine != null && vFOLine != '')
						{	
							var vFOPresentStatus= vFOLine.getFieldValue('custrecord_linestatus_flag');
							nlapiLogExecution('DEBUG', 'vFOPresentStatus', vFOPresentStatus);
							nlapiLogExecution('DEBUG', 'vlineStatus', vlineStatus);
							/*if(vFOPresentStatus == '15' && vlineStatus != '15')
									vAllowWave='F';*/
							if(vlineStatus != null && vlineStatus != '' && vFOPresentStatus != vlineStatus)
								vAllowWave='F';
						}
					}
					if(vAllowWave == 'T')
					{
						var vBoolupdate= updateFulfilmentOrder(fointrid, eBizWaveNo,vlotnumbers,vLotQty,lotnoInternalId);
						nlapiLogExecution('DEBUG', 'Creating wave ord record starts',TimeStampinSec());
						if(vBoolupdate)
						{
							var waveordRecord = nlapiCreateRecord('customrecord_ebiz_waveord');

							waveordRecord.setFieldValue('custrecord_ebiz_waveorder_usedflag', 'T');				// Used Flag
							waveordRecord.setFieldValue('custrecord_ebiz_waveorder_company', company);			// Company
							waveordRecord.setFieldValue('custrecord_ebiz_waveorder_location', location);		// Location
							waveordRecord.setFieldValue('custrecord_ebiz_waveorder_waveno', eBizWaveNo);		// Wave #
							waveordRecord.setFieldValue('custrecord_ebiz_waveorder_orderno', ordno);			// Order #
							waveordRecord.setFieldValue('custrecord_ebiz_waveorder_orderline', ordlineno);     	// Order Line #
							waveordRecord.setFieldValue('custrecord_ebiz_waveorder_lastline', lastline);       	// Last Line?
							waveordRecord.setFieldValue('custrecord_ebiz_lotnumber', vlotnumbers); //added by santosh
							waveordRecord.setFieldValue('custrecord_ebiz_lotno_qty', vLotQty); //added by santosh
							waveordRecord.setFieldValue('custrecord_ebiz_lotnumber_internalid', lotnoInternalId); //added by santosh
							if(currentUserID !=null && currentUserID !='')
								waveordRecord.setFieldValue('custrecord_ebiz_waveorder_user', currentUserID);

							if(i==fulfillOrderList.length-1)
								waveordRecord.setFieldValue('custrecord_ebiz_waveorder_wavelastline', 'T');    	// Last Line in Wave?
							var recId = nlapiSubmitRecord(waveordRecord,false,true);

							nlapiLogExecution('DEBUG', 'Creating wave ord record ends',TimeStampinSec());
						}
					}
				}

				//invoke a sched script to replenish the current user�s pool of blank records
				var param = new Array();
				param['custscript_employee'] = currentUserID;
				param['custscript_replenish'] = 400;
				nlapiScheduleScript('customscript_replenish_waveord', 'customdeploy_replenish_waveord',param);	

			}
		}
	}
	catch(exp) 
	{
		nlapiLogExecution('DEBUG', 'Exception in createwaveordrecord ', exp);	
		showInlineMessage(form, 'DEBUG', 'Wave Creation Failed', "");
		response.writePage(form);
	}
	nlapiLogExecution('DEBUG', 'Out of  createwaveordrecord ', eBizWaveNo);
}

/**
 * Return the fulfillment order information to be used for wave generation
 * NOTE: WILL NEVER RETURN NULL; WILL ALWAYS RETURN ATLEAST A BLANK ARRAY
 * 
 * @param request
 * @returns {Array}
 */
function getSelectedFulfillmentOrderInfo(request){
	nlapiLogExecution('DEBUG','into getSelectedFulfillmentOrderInfo', '');
	var fulfillOrderInfoArray = new Array();
	var orderInfoCount = 0;

	// Retrieve the number of items for which wave needs to be created
	var lineCount = request.getLineItemCount('custpage_items');

	for(var k = 1; k <= lineCount; k++){
		var selectValue = request.getLineItemValue('custpage_items', 'custpage_so', k);
		//nlapiLogExecution('DEBUG','selectValue', selectValue);

		if(isItemSelected(request,'custpage_items', 'custpage_so', k)){
			var itemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
			var itemNo = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
			var availQty = request.getLineItemValue('custpage_items', 'custpage_avlqty', k);
			var orderQty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
			var doNo = request.getLineItemValue('custpage_items', 'custpage_doinernno', k);
			var lineNo = request.getLineItemValue('custpage_items', 'custpage_solineno', k);
			var doName = request.getLineItemValue('custpage_items', 'custpage_sono', k);
			var soInternalID = request.getLineItemValue('custpage_items', 'custpage_soinernno', k);
			var itemStatus = request.getLineItemValue('custpage_items', 'custpage_skustatus', k);
			var packCode = request.getLineItemValue('custpage_items', 'custpage_packcode', k);
			var uom = request.getLineItemValue('custpage_items', 'custpage_uomid', k);
			var lotBatchNo = request.getLineItemValue('custpage_items', 'custpage_lotbatch', k);
			var location = request.getLineItemValue('custpage_items', 'custpage_location', k);
			var company = request.getLineItemValue('custpage_items', 'custpage_company', k);
			var itemInfo1 = request.getLineItemValue('custpage_items', 'custpage_iteminfo1', k);
			var itemInfo2 = request.getLineItemValue('custpage_items', 'custpage_iteminfo2', k);
			var itemInfo3 = request.getLineItemValue('custpage_items', 'custpage_iteminfo2', k);
			var orderType = request.getLineItemValue('custpage_items', 'custpage_ordtype', k);
			var orderPriority = request.getLineItemValue('custpage_items', 'custpage_ordpriority', k);
			// code added by santosh on 09 Oct12

			var vLotnoArr = request.getLineItemValue('custpage_items', 'custpage_lotbatch', k);
			var vLotnoId = request.getLineItemValue('custpage_items', 'custpage_lotnoid', k);
			var vLotno="";
			var vQty="";
			if(vLotnoArr!=null && vLotnoArr!="" && vLotnoArr.length>0)
			{
				nlapiLogExecution('DEBUG','vLotnoId',vLotnoId);
				nlapiLogExecution('DEBUG','vLotnoArr',vLotnoArr.length);

				var vlotarray=vLotnoArr.split(',');//santosh
				nlapiLogExecution('DEBUG','vlotarray[0]',vlotarray[0]);
				nlapiLogExecution('DEBUG','vlotarray[1]',vlotarray[1]);

				for(var j=0;j<vlotarray.length;j++)
				{

					var LotNumber=vlotarray[j];
					nlapiLogExecution('DEBUG', 'LotNumber',LotNumber);

					var pos = LotNumber.indexOf("(")+1;
					nlapiLogExecution('DEBUG', 'pos ', pos);

					vQty=vQty+LotNumber.slice(pos, -1)+",";
					nlapiLogExecution('DEBUG', 'vQty', vQty);

					vLotno=vLotno+LotNumber.slice(0, pos-1)+",";
					nlapiLogExecution('DEBUG', 'vLotno', vLotno);

				}
				nlapiLogExecution('DEBUG', 'vLotnumbers', vLotno);
				nlapiLogExecution('DEBUG', 'vQunatity', vQty);
				nlapiLogExecution('DEBUG', 'vLotnoId', vLotnoId);
				//end of code
			}
			var currentRow = [k, soInternalID, lineNo, doNo, doName, itemName, itemNo, itemStatus, 
			                  availQty, orderQty, packCode, uom, lotBatchNo, company, 
			                  itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority,location,vLotno,vQty,vLotnoId];

			fulfillOrderInfoArray[orderInfoCount++] = currentRow;
			nlapiLogExecution('DEBUG','currentRow', currentRow);
		}
	}
	nlapiLogExecution('DEBUG','fulfillOrderInfoArray', fulfillOrderInfoArray.length);

	nlapiLogExecution('DEBUG','out of getSelectedFulfillmentOrderInfo', '');

	return fulfillOrderInfoArray;

}

/**
 * 
 */
function isItemSelected(request, subList, element, index){
	var retVal = false;
	if(request.getLineItemValue(subList, element, index) == 'T')
		retVal = true;

	return retVal;
}

function updateFulfilmentOrder(fointrid, eBizWaveNo,vlotnumbers,vLotQty,lotnoInternalId){

	nlapiLogExecution('DEBUG', 'Into updateFulfilmentOrder ',TimeStampinSec());

	var fulfilmentOrderline = nlapiLoadRecord('customrecord_ebiznet_ordline', fointrid);
	if(fulfilmentOrderline != null && fulfilmentOrderline != '')
	{
		var vOrdQty=fulfilmentOrderline.getFieldValue('custrecord_ord_qty');
		var vPickGenQty=fulfilmentOrderline.getFieldValue('custrecord_pickgen_qty');
		if(vOrdQty == null || vOrdQty == '')
			vOrdQty=0;
		if(vPickGenQty == null || vPickGenQty == '')
			vPickGenQty=0;
		//nlapiLogExecution('DEBUG', 'vOrdQty ', vOrdQty);
		//nlapiLogExecution('DEBUG', 'vPickGenQty ', vPickGenQty);
		if(parseFloat(vOrdQty) > parseFloat(vPickGenQty))
		{
			fulfilmentOrderline.setFieldValue('custrecord_ebiz_wave', eBizWaveNo);
			fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '15'); //status flag--'W'

			fulfilmentOrderline.setFieldValue('custrecord_ebiz_lot_number', vlotnumbers);
			fulfilmentOrderline.setFieldValue('custrecord_ebiz_lotnumber_qty', vLotQty);
			fulfilmentOrderline.setFieldValue('custrecord_ebiz_lotno_internalid', lotnoInternalId);
			//case # 20124296�Start
			var sysdate=DateStamp();
			fulfilmentOrderline.setFieldValue('custrecord_upddate', sysdate);
			//case # 20124296�End
			nlapiSubmitRecord(fulfilmentOrderline, false, true);
			nlapiLogExecution('DEBUG', 'updateFulfilmentOrder ', 'Success');
			return true;
		}	
		else
			return false;
	}
	else
		return false;

	//nlapiLogExecution('DEBUG', 'Fulfilment Order Updated Successfully', fointrid);
	nlapiLogExecution('DEBUG', 'Out of updateFulfilmentOrder',TimeStampinSec());
}
