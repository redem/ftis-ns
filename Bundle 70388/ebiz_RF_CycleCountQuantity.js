/***************************************************************************
 eBizNET Solutions Inc              
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountQuantity.js,v $
 *     	   $Revision: 1.8.4.3.4.9.2.30.2.3 $
 *     	   $Date: 2015/11/26 13:38:47 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CycleCountQuantity.js,v $
 * Revision 1.8.4.3.4.9.2.30.2.3  2015/11/26 13:38:47  schepuri
 * case# 201415820
 *
 * Revision 1.8.4.3.4.9.2.30.2.2  2015/11/16 15:36:29  rmukkera
 * case # 201415115
 *
 * Revision 1.8.4.3.4.9.2.30.2.1  2015/11/02 14:42:15  grao
 * 2015.2 Issue Fixes 201414918
 *
 * Revision 1.8.4.3.4.9.2.30  2015/08/21 11:00:23  nneelam
 * case# 201414102
 *
 * Revision 1.8.4.3.4.9.2.29  2015/07/22 15:40:42  grao
 * 2015.2   issue fixes  201413591
 *
 * Revision 1.8.4.3.4.9.2.28  2015/04/14 14:02:59  schepuri
 * case# 201412332
 *
 * Revision 1.8.4.3.4.9.2.27  2014/09/26 15:26:54  sponnaganti
 * Case# 201410549
 * AlphaComm SB Issue fix
 *
 * Revision 1.8.4.3.4.9.2.26  2014/09/16 14:54:37  skreddy
 * case # 201410360
 * TPP SB issue fix
 *
 * Revision 1.8.4.3.4.9.2.25  2014/09/02 10:53:11  gkalla
 * case#20149227
 * ACE and JB cycle count issues
 *
 * Revision 1.8.4.3.4.9.2.24  2014/07/09 12:10:07  rmukkera
 * Case # 20149344 ,20149345 ,20149346
 * jawbone issues applicable to standard also
 *
 * Revision 1.8.4.3.4.9.2.23  2014/06/13 10:05:37  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.8.4.3.4.9.2.22  2014/06/06 12:18:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.8.4.3.4.9.2.21  2014/05/30 00:34:20  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.8.4.3.4.9.2.20  2014/05/13 15:29:25  skavuri
 * Case# 20148351 SB Issue Fixed
 *
 * Revision 1.8.4.3.4.9.2.19  2014/04/25 09:54:07  grao
 * Case# 20148171
 * SB iSSUES FIXES�
 *
 * Revision 1.8.4.3.4.9.2.18  2014/02/10 16:38:23  skavuri
 * Case#20127045  To check wheather  exp qty displaying or not in Qty Discrepancy page
 *
 * Revision 1.8.4.3.4.9.2.17  2014/01/24 14:48:39  rmukkera
 * Case # 20126924
 *
 * Revision 1.8.4.3.4.9.2.16  2013/12/27 13:53:01  schepuri
 * 20126237
 *
 * Revision 1.8.4.3.4.9.2.15  2013/11/22 14:47:07  grao
 * Case# 20125873  related issue fixes in SB 2014.1
 *
 * Revision 1.8.4.3.4.9.2.14  2013/10/24 06:47:19  skreddy
 * Case# 20120127
 * boombah prod  issue fix
 *
 * Revision 1.8.4.3.4.9.2.13  2013/10/22 15:39:31  rmukkera
 * Case # 20125187
 *
 * Revision 1.8.4.3.4.9.2.12  2013/10/18 15:47:00  skreddy
 * Case# 20124594
 * Affosa SB  issue fix
 *
 * Revision 1.8.4.3.4.9.2.11  2013/10/08 15:46:58  rmukkera
 * Case# 20124768
 *
 * Revision 1.8.4.3.4.9.2.10  2013/09/16 15:39:23  rmukkera
 * Case# 20124313
 *
 * Revision 1.8.4.3.4.9.2.9  2013/09/10 15:49:51  rmukkera
 * Case# 20124313
 *
 * Revision 1.8.4.3.4.9.2.8  2013/09/05 15:40:09  skreddy
 * Case# 20124255
 * standard bundle issue fix
 *
 * Revision 1.8.4.3.4.9.2.7  2013/08/03 21:16:08  snimmakayala
 * Case# 201214994
 * Cycle Count Issue Fixes
 *
 * Revision 1.8.4.3.4.9.2.6  2013/06/11 16:01:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Stdbundle issue fixes
 *
 * Revision 1.8.4.3.4.9.2.5  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.8.4.3.4.9.2.4  2013/04/30 15:57:08  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.8.4.3.4.9.2.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.8.4.3.4.9.2.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.8.4.3.4.9.2.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 *
 *****************************************************************************/


/**
 * @author LN
 *@version
 *@date
 */
function CycleCountQuantity(request, response){
	if (request.getMethod() == 'GET') {

		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var getRecordId = request.getParameter('custparam_recordid');
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName =  request.getParameter('custparam_begin_location_name');
		var getPackCode =  request.getParameter('custparam_packcode');
		var getExpectedQuantity =  request.getParameter('custparam_expqty');
		nlapiLogExecution('ERROR', 'Expected Quantity', getExpectedQuantity);
		var getExpQty =  request.getParameter('custparam_expqty');
		var getExpLPNo = request.getParameter('custparam_lpno');
		var getActLPNo = request.getParameter('custparam_actlp');
		var getExpItem = request.getParameter('custparam_expitem');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');
		var getActualItem = request.getParameter('custparam_actitem');
		var getActualPackCode = request.getParameter('custparam_actpackcode');		
		var getActualItemInternamId = request.getParameter('custparam_expiteminternalid');
		var getActualBatch=request.getParameter('custparam_actbatch');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');		
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'RecordCount', RecordCount);
		var planitem=request.getParameter('custparam_planitem');
		nlapiLogExecution('ERROR', 'getRecordId', getRecordId);
		//added by surendra 
		var blindPackcodeRule=request.getParameter('custparam_packcoderequired');
		nlapiLogExecution('ERROR','blindPackcodeRule',blindPackcodeRule);
		
		if(blindPackcodeRule=="Y")
		{
			getActualPackCode = request.getParameter('custparam_packcode');
		}
		//end
		//var invtrecId=nlapiLookupField('customrecord_ebiznet_cyclecountexe',getRecordId,'custrecord_invtid');
		//var res=CheckInvtRecord(invtrecId);
		//case # 20126924�start
		var fields = ['custrecord_cyclesku', 'custrecord_cycact_beg_loc', 'custrecord_expcyclesku_status',
		              'custrecord_invtid','custrecord_expcycleabatch_no','custrecord_cyclelpno','custrecord_cyclesite_id'];
		//case # 20126924�end
		var invtrecId=nlapiLookupField('customrecord_ebiznet_cyclecountexe',getRecordId,fields);
		var Item = invtrecId.custrecord_cyclesku;
		var InvtId = invtrecId.custrecord_invtid;
		var Binloc = invtrecId.custrecord_cycact_beg_loc;
		var Itemstatus = invtrecId.custrecord_expcyclesku_status;
		//case # 20126924�start
		var explpno=invtrecId.custrecord_cyclelpno;
		
		var vSite = invtrecId.custrecord_cyclesite_id;
		nlapiLogExecution('ERROR', 'vSite', vSite);
		//case # 20126924�end
		//custrecord_expcycleabatch_no
		var Batchno = "";
		var ruleValue=GetSystemRuleForLPRequired();
		if(invtrecId.custrecord_expcycleabatch_no != null && invtrecId.custrecord_expcycleabatch_no != "")
		{
			//case# 20125187 start
			var actitem=request.getParameter('custparam_actitem');
			var rec=nlapiLookupField('item', actitem, ['itemid']);
			var actitemText=rec.itemid;
			nlapiLogExecution('ERROR','ACTITEMTEXT',actitemText);
			Batchno = getLotBatchId(invtrecId.custrecord_expcycleabatch_no,actitemText,vSite);
			//case# 20125187 end
			nlapiLogExecution('ERROR', 'Batchno', Batchno);
		}
		if(InvtId != null && InvtId != "" && ruleValue=='Y')
		{
			//case # 20126924�start
			//case# start 20125873 added InvtId to the function
			var res=CheckInvtRecordNew(Item,Binloc,Itemstatus,Batchno,explpno,InvtId);
			//end
			//case # 20126924�end
			var IsRecordExist=res[0];
			var updatedQoh=res[1];
			var updatedAllocatedQty=res[2];		

			if(IsRecordExist=='F')
			{
				var CCarray= new Array();
				CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
				CCarray["custparam_planno"] = request.getParameter('custparam_planno');
				CCarray["custparam_begin_location_id"] =request.getParameter('custparam_begin_location_id');
				CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
				CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
				CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
				CCarray["custparam_expqty"] = 0;
				CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
				CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
				CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');
				CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
				CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
				CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
				CCarray["custparam_noofrecords"] = request.getParameter('custparam_noofrecords');
				CCarray["custparam_nextlocation"] = request.getParameter('custparam_nextlocation');
				CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
				CCarray["custparam_actbatch"] = request.getParameter('custparam_actbatch');
				CCarray["custparam_planitem"] = request.getParameter('custparam_planitem');
				CCarray["custparam_actualqty"] = 0;
				CCarray["custparam_confmsg"] = false ;
				nlapiLogExecution('ERROR', 'Cycle Count QTY Redirected OK');
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_confirm', 'customdeploy1', false, CCarray);
				return;
			}
			else
			{
				getExpectedQuantity =updatedQoh;
				getExpQty = updatedQoh;
			}
		}
		var blindQtyRule=false;
    // Case# 20148351 starts (for getting siteid from plan)
		var locsite='';
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',getPlanNo ));
		filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', ['20']));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_cyclesite_id');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);
		if(searchresults!=''&&searchresults!='null'&&searchresults!=null)
		locsite=searchresults[0].getValue('custrecord_cyclesite_id');
		nlapiLogExecution('ERROR', 'locsite is ', locsite);
		var BlindRuleValue='N';
		// Case# 20148351 ends
		var sysruleFilters = new Array();//
		sysruleFilters.push(new nlobjSearchFilter('name', null, 'is', 'CYCN - Blind Qty'));
		// Case# 20148351 starts
		if(locsite!=null && locsite!='' && locsite!='null')
		sysruleFilters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof',['@NONE@',locsite]));
		// Case# 20148351 ends
		var sysruleColumns = new Array();
		sysruleColumns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		var systemRulesSearchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, sysruleFilters, sysruleColumns);
		if(systemRulesSearchresults!=null && systemRulesSearchresults.length>0)
		{
			 BlindRuleValue=systemRulesSearchresults[0].getValue('custrecord_ebizrulevalue');
			nlapiLogExecution('ERROR', 'BlindRuleValue', BlindRuleValue);
			if(BlindRuleValue=='N')
			{
				blindQtyRule=false;
			}
			else
			{
				blindQtyRule=true;
			}
		}
		else
		{
			blindQtyRule=false;
		}

		if( blindQtyRule==true)
		{

			getExpectedQuantity='';
		}
		nlapiLogExecution('ERROR', 'BlindRuleValue', BlindRuleValue);
		if(getExpectedQuantity==null || getExpectedQuantity=='' || isNaN(getExpectedQuantity))
			getExpectedQuantity=0;
//Case # 20124594 : start : spanish language conversion
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		
		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "RECUENTO DE CICLO";
			st1 = " CANT EXP";
			st2 = " ENTER / SCAN";
			st3 = "CANT aCTUAL";
			st4 = "CONT";
            st5 = " ANTERIOR";
            st6 = "PASE";
           

		}
		else
		{
			st0 = "CYCLE COUNT";
			st1 = "EXP QTY";
			st2 = "ENTER/SCAN";
			st3 = "ACTUAL QTY";
			st4 = "CONT";
            st5 = "PREV";
            st6 = "SKIP";
		}
		
//case # 20124594 end
		var caseQty = fetchCaseQuantity(getActualItem,vSite,null);

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountquantity');
		var html = "<html><head><title>CYCLE COUNT QTY</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterqty').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html=html+"<SCRIPT LANGUAGE='javascript'>";
		html=html+"function noenter() {  return !(window.event && window.event.keyCode == 13); }</script>";
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountquantity' method='POST'>";
		html = html + "		<table>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st0  + " </td></tr>";
		if( blindQtyRule==false)
		{
			html = html + "				<tr><td align = 'left'>"+  st1  +"  :<label>" + getExpectedQuantity+ " Each</label>";
			var pikQtyBreakup = getQuantityBreakdown(caseQty,getExpectedQuantity);
			html = html + "	("+caseQty+"/Case:"+pikQtyBreakup+")";
			html = html + "			</td></tr>";
		}
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpQty + ">";		
		html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnActualItem' value=" + getActualItem + ">";
		html = html + "				<input type='hidden' name='hdnActualPackCode' value=" + getActualPackCode + ">";
		html = html + "				<input type='hidden' name='hdniteminternalid' value=" + getActualItemInternamId + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";		
		// case# 20127045 starts
		html = html + "				<input type='hidden' name='hdnruleValue' value=" + ruleValue + ">";
		html = html + "				<input type='hidden' name='hdnBlindRuleValue' value=" + BlindRuleValue + ">";
		//case# 20127045 end
		html = html + "				<input type='hidden' name='hdnpakcoderequired' value=" + blindPackcodeRule + ">";//added by surendra	
		html = html + "				</td>";
		html = html + "			</tr>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+ st2 + " </td></tr>";
		html = html + "				<tr><td align = 'left'> " + st3 + " ";
		html = html + "				</td>";
		html = html + "			</tr>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterqty' id='enterqty' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false;'/>";
		html = html + "					"+ st5  + " <input name='cmdPrevious' type='submit' value='F9'/>";
		html = html + "					" + st6  + " <input name='cmdSkip' type='submit' value='F8' style='width:40;height:40;'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterqty').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else 
	{
		var CCarray= new Array();
		var optedEvent = request.getParameter('cmdPrevious');
		var optedEvent1 = request.getParameter('cmdSkip');
		var getActualQty = request.getParameter('enterqty');  
		var getExpQty=request.getParameter('custparam_expqty');
		nlapiLogExecution('ERROR','getActualQty',getActualQty);
		var getLanguage = request.getParameter('hdngetLanguage');
		CCarray["custparam_language"] = getLanguage;

		// case# 20127045 starts
		var getRulevalue = request.getParameter('hdnruleValue');
		var getBlindRulevalue = request.getParameter('hdnBlindRuleValue');
		nlapiLogExecution('ERROR','getRulevalue is',getRulevalue);
		nlapiLogExecution('ERROR','getBlindRulevalue is',getBlindRulevalue);
		CCarray["custparam_Rulevalue"] = getRulevalue;
		CCarray["custparam_blindrulevalue"] = getBlindRulevalue;
		
		CCarray["custparam_ruleValue"] = getRulevalue;
		nlapiLogExecution('ERROR','getRulevalue in post',getRulevalue);
		// case# 20127045 send
		
		CCarray["custparam_error"] = 'INVALID QUANTITY';
		CCarray["custparam_screenno"] = '33';
		CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
		CCarray["custparam_planno"] = request.getParameter('custparam_planno');
		CCarray["custparam_begin_location_id"] =request.getParameter('custparam_begin_location_id');
		CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
		CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
		CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
		CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
		CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
		CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');
		CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
		CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
		CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
		CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
		CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
		CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
		CCarray["custparam_actbatch"] = request.getParameter('custparam_actbatch');
		CCarray["custparam_expqty"] = request.getParameter('hdnExpectedQuantity');
		CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
		CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');
		var CCrecid = request.getParameter('custparam_recordid');
		var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', CCrecid);

		var invtid = CCRec.getFieldValue('custrecord_invtid');
		//added by surendra
		CCarray["custparam_packcoderequired"]=request.getParameter('hdnpakcoderequired');
		var blindPackcodeRule=request.getParameter('hdnpakcoderequired');
		nlapiLogExecution('ERROR','blindPackcodeRule in post',blindPackcodeRule);
		//end
		var transaction = '';
		var allocQty = 0;
		if(invtid != null && invtid != '' && invtid != 'null')
		{
			transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',invtid);
			allocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
		}


		if (optedEvent == 'F9')
		{
			
			nlapiLogExecution('ERROR', 'Cycle Count QTY F7 Pressed');
			nlapiLogExecution('ERROR', 'Cycle Count QTY F7 Pressed CCarray["custparam_expiteminternalid"]',CCarray["custparam_expiteminternalid"]);
			var ItemType = nlapiLookupField('item', request.getParameter('custparam_actitem'), ['recordType','custitem_ebizbatchlot']);
			nlapiLogExecution('ERROR', 'ItemType',ItemType.recordType );
			nlapiLogExecution('ERROR', 'Batch/Lot',ItemType.custitem_ebizbatchlot );
			if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.custitem_ebizbatchlot=='T' ) {
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecountbatch', 'customdeploy_rf_cyclecountbatch_di', false, CCarray);
				return;
			}
			//added by surendra
			//response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_packcode', 'customdeploy_rf_cyclecount_packcode_di', false, CCarray);
			if(blindPackcodeRule=="Y")
			{
				nlapiLogExecution('DEBUG', 'Cycle Count PackCode F9 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_sku', 'customdeploy_rf_cyclecount_sku_di', false, CCarray);
			}
			else
			{
				nlapiLogExecution('DEBUG', 'Cycle Count PackCode F9 Pressed  else');
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_packcode', 'customdeploy_rf_cyclecount_packcode_di', false, CCarray);
			}
		}
		else if(optedEvent1=='F8')
		{
			nlapiLogExecution('ERROR','SKIP','SKIPthetask');
			nlapiLogExecution('ERROR', 'getRecordId', CCarray["custparam_recordid"]);
			var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'));
			var skipcount=CCRec.getFieldValue('custrecord_cyc_skip_task');
			nlapiLogExecution('ERROR','skipcount',skipcount);

			if(skipcount=='' || skipcount==null)
			{
				skipcount=0;
			}
			skipcount=parseInt(skipcount)+1;
			CCRec.setFieldValue('custrecord_cyc_skip_task',skipcount);
			var id=	nlapiSubmitRecord(CCRec,true);
			nlapiLogExecution('ERROR','skipid',id);


			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
		}
		else 
		{
			if (getActualQty != '') 
			{
				
				// case no 20126237
				if((isNaN(getActualQty)) || parseFloat(getActualQty) < 0)// case# 201412332
				{
					//CCarray["custparam_actualqty"] = getActualQty ;
					CCarray["custparam_error"] = 'Please enter valid ActualQty';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					return;
				}
				
				//case# 20124768�Start
				var palletQuantity = 0;
				palletQuantity = fetchPalletQuantity(CCarray["custparam_actitem"],null,null);
				//case# 20124768�End
				if (parseFloat(allocQty) > parseFloat(getActualQty)) 
				{
					CCarray["custparam_actualqty"] = getActualQty ;
					CCarray["custparam_error"] = 'AllocQty is greater than ActualQty';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					return;
				}
				else if((parseFloat(palletQuantity) != 0) && (parseFloat(getActualQty) > parseFloat(palletQuantity)))
				{
					CCarray["custparam_actualqty"] = getActualQty ;
					CCarray["custparam_error"] = 'ActualQty is greater than palletQuantity';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					return false;
					
				}
				else
				{
					CCarray["custparam_actualqty"] = getActualQty ;
					CCarray["custparam_confmsg"] = true; 
					var itemSubtype = nlapiLookupField('item', CCarray["custparam_actitem"], ['recordType','custitem_ebizserialin']);
					CCarray["custparam_itemtype"] = itemSubtype.recordType;
					nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype);
					/*if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T') 
					{
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_serialscan', 'customdeploy_rf_cyclecount_serialscan_di', false, CCarray);
						return;
					}
					else*/
					//{
						nlapiLogExecution('ERROR', 'Cycle Count QTY Redirected OK');
						var res=GetIsQuantityDiscrepancyRequired();
						nlapiLogExecution('ERROR', 'res',res);
						nlapiLogExecution('ERROR', 'getActualQty',getActualQty);
						nlapiLogExecution('ERROR', 'getExpQty',getExpQty);
						if(res=='Y'){
							if(getActualQty!=getExpQty){
								response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_qtydiscrepanc', 'customdeploy_rf_cyclcntt_qtydiscrepan_di', false, CCarray); 

							}
							else{
								response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_itemstatus', 'customdeploy_rf_cyclecount_itemstatus_di', false, CCarray);
							}


						}
						else{
							response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_itemstatus', 'customdeploy_rf_cyclecount_itemstatus_di', false, CCarray); 
						}
					//}					
				}
			}
			else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
				nlapiLogExecution('DEBUG', 'Catch : Cycle Count QTY not found');
			}
		}
	}
}


function CheckInvtRecord(invtrecId)
{
	try
	{
		nlapiLogExecution('ERROR','Into CheckInvtRecord',invtrecId);
		var RecArray=new Array();
		var Qoh="";
		var Allocatedqty="";
		var recExist="F";
		var filter=new Array();
		if(invtrecId!=null&&invtrecId!="")
			filter.push(new nlobjSearchFilter('internalid',null,'anyof',invtrecId));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_qoh');
		column[1]=new nlobjSearchColumn('custrecord_ebiz_alloc_qty');

		var searchres=nlapiSearchRecord('customrecord_ebiznet_createinv',null,filter,column);

		if(searchres!=null&&searchres!="")
		{
			Qoh=searchres[0].getValue('custrecord_ebiz_qoh');
			Allocatedqty=searchres[0].getValue('custrecord_ebiz_alloc_qty');
			recExist="T";
		}
		RecArray.push(recExist);
		RecArray.push(Qoh);
		RecArray.push(Allocatedqty);
		nlapiLogExecution('ERROR','RecArray',RecArray);
		return RecArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in CheckInvtRecord',exp);
	}
}

function CheckInvtRecordNew(Item,Binloc,Itemstatus,Batch,getActLPNo,invtrecId)
{
	try
	{
		nlapiLogExecution('ERROR','Into CheckInvtRecordNew',Item);
		nlapiLogExecution('ERROR','Into CheckInvtRecordNew',Binloc);
		nlapiLogExecution('ERROR','Into CheckInvtRecordNew',Itemstatus);
		nlapiLogExecution('ERROR','Into CheckInvtRecordNew',Batch);
		nlapiLogExecution('ERROR','getActLPNo',getActLPNo);
		nlapiLogExecution('ERROR','invtrecId',invtrecId);
		var RecArray=new Array();
		var Qoh=0;
		var Allocatedqty=0;
		var recExist="F";
		var filter=new Array();
		if(invtrecId!=null&&invtrecId!="")
			filter.push(new nlobjSearchFilter('internalid',null,'anyof',invtrecId));
		if(Item!=null&&Item!="")
			filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null,'anyof',Item));
		if(Binloc!=null&&Binloc!="")
			filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null,'anyof',Binloc));
		if(Itemstatus!=null&&Itemstatus!="")
			filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null,'anyof',Itemstatus));
		if(Batch!=null&&Batch!="")
			filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot',null,'anyof',Batch));
		if(getActLPNo!=null && getActLPNo!="")
			filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp',null,'is',getActLPNo));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_qoh');
		column[1]=new nlobjSearchColumn('custrecord_ebiz_alloc_qty');

		var searchres=nlapiSearchRecord('customrecord_ebiznet_createinv',null,filter,column);

		if(searchres!=null&&searchres!="")
		{
			//Qoh=searchres[0].getValue('custrecord_ebiz_qoh');
			//Allocatedqty=searchres[0].getValue('custrecord_ebiz_alloc_qty');
			for(var s=0;s<searchres.length;s++)
			{
				Qoh=parseFloat(Qoh)+parseFloat(searchres[s].getValue('custrecord_ebiz_qoh'));
				Allocatedqty=parseFloat(Allocatedqty)+parseFloat(searchres[s].getValue('custrecord_ebiz_alloc_qty')); 			  
			}			
			recExist="T";
		}
		RecArray.push(recExist);
		RecArray.push(Qoh);
		RecArray.push(Allocatedqty);
		nlapiLogExecution('ERROR','RecArray',RecArray);
		return RecArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in CheckInvtRecord',exp);
	}
}
//case # 20125187  start
function getLotBatchId(lotbatchtext,actitem,vSite)
{
	//Case # 20125187 End
	var batchid='';
	var filters = new Array();          
	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is',lotbatchtext));
	if(vSite!=null && vSite!='')
	filters.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'is',vSite));
	
	filters.push(new nlobjSearchFilter('itemid','custrecord_ebizsku', 'is', actitem));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	var batchdetails = new nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
	if (batchdetails !=null && batchdetails !='' && batchdetails.length>0) 
	{
		batchid=batchdetails[0].getValue('internalid');
	}
	return batchid;
}
function GetSystemRuleForLPRequired()
{
	try
	{
		var rulevalue='Y';
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for CycleCount?'));
		
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		
		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);
		
		if(searchresult!=null&&searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForLPRequired',exp);
	}
}
function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	nlapiLogExecution('ERROR','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', 3);

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

