/***************************************************************************
 	eBizNET Solutions Inc  
 ****************************************************************************/
/*******************************************************************************
 * ***************************************************************************
 * 
 * $Source:
 * /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_PickConfirmation_SL.js,v $
 * $Revision: 1.1.2.5.4.1 $ $Date: 2015/11/24 11:07:29 $ $Author: deepshikha $ $Name: t_WMS_2015_2_StdBundle_1_180 $
 * 
 * eBizNET version and checksum stamp. Do not remove. $eBiznet_VER:
 * .............. $eBizNET_SUM: ..... PRAMETERS
 * 
 * 
 * DESCRIPTION
 * 
 * Default Data for Interfaces
 * 
 * NOTES AND WARNINGS
 * 
 * INITATED FROM
 *  * REVISION HISTORY $Log: ebiz_ManualClustering_SL.js,v $
 *  * REVISION HISTORY Revision 1.1.2.5.4.1  2015/11/24 11:07:29  deepshikha
 *  * REVISION HISTORY 2015.2 Issue Fix
 *  * REVISION HISTORY 201414690
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.5  2013/12/13 13:59:18  schepuri
 *  * REVISION HISTORY 20126075
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.4  2013/09/17 15:30:01  nneelam
 *  * REVISION HISTORY Case#. 20122210
 *  * REVISION HISTORY No alert if wave not selected after Back Search Button clicked...
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.3  2013/06/10 14:54:54  rmukkera
 *  * REVISION HISTORY Issue Fix for
 *  * REVISION HISTORY 	I have selected two orders for Wave, and system has generated picks, but when I go to Manual Cluster Generation Screen, it is showing multiples. Please see the attached document for more details
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2  2013/06/04 15:26:45  rmukkera
 *  * REVISION HISTORY Issue Fix New Filters added
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.1  2013/06/03 15:38:48  rmukkera
 *  * REVISION HISTORY New File
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.20.4.3.4.4  2013/03/21 14:18:14  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG2012392
 *  * REVISION HISTORY Prod and UAT issue fixes.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.20.4.3.4.3  2013/03/15 09:28:43  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG2012392
 *  * REVISION HISTORY Production and UAT issue fixes.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.20.4.3.4.2  2013/03/08 14:45:50  skreddy
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Code merged from Endochoice as part of Standard bundle
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.20.4.3.4.1  2013/03/01 14:34:54  skreddy
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Merged from FactoryMation and change the Company name
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.20.4.3  2013/01/11 13:25:57  skreddy
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY issues related to Qty expection and loc exception
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.20.4.2  2012/11/01 14:55:02  schepuri
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Decimal Qty Conversions
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.20.4.1  2012/09/26 22:43:33  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG2012392
 *  * REVISION HISTORY Production Issue Fixes for FISK,BOOMBAH and TDG.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.20  2012/09/11 00:45:23  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Production Issue Fixes for FISK and BOOMBAH.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.19  2012/09/04 14:19:10  spendyala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY issue related to item fulfillment is resolved.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.18  2012/08/08 15:17:29  rrpulicherla
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Kit to order
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.17  2012/07/04 18:50:10  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Schedule script for confirming picks
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.16  2012/07/03 21:54:34  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Performance Tuning
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.15  2012/07/02 13:13:24  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Performance Tuning
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.14  2012/07/02 12:50:41  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Performance Tuning
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.13  2012/06/02 09:34:32  spendyala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Issue related to pick resolved.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.12  2012/05/09 07:52:37  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Partial Status Handling
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.11  2012/05/04 22:52:08  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Production issue fixes
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.10  2012/05/03 21:17:46  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Production issue fixes
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.9  2012/04/30 06:55:51  mbpragada
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.8  2012/04/20 14:14:07  schepuri
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY changing the Label of Batch #  field to Lot#
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.7  2012/04/05 13:14:20  schepuri
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY paging functionality is added
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.6  2012/04/03 15:19:16  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Performance Tunning
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55.2.5  2012/03/02 01:25:46  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Stable bundle issue fixes
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.63  2012/03/02 01:11:06  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Stable bundle issue fixes
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.62  2012/02/20 13:36:59  gkalla
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Added Line level WMS Carrier
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.61  2012/02/10 06:02:18  schepuri
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY issue fix is merged from  1.55.2.3
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.60  2012/02/08 12:03:08  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Code Merge
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.59  2012/02/02 11:26:27  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Stage Rule by Order Type
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.58  2012/02/01 15:12:23  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY ship complete validation
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.57  2012/01/27 08:21:24  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY shipcomplete validate
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.56  2012/01/23 23:46:03  gkalla
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Changed the alert message
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.55  2012/01/21 00:45:53  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.54  2012/01/19 15:20:35  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.53  2012/01/16 15:22:42  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.52  2012/01/16 14:43:56  gkalla
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY For OrdRec Array
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.51  2012/01/13 16:50:36  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Pick Generation User Event changes
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.50  2012/01/03 16:16:49  gkalla
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY For Ship Complete flag validation
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.49  2011/12/22 18:42:33  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY TO Shipping
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.48  2011/12/21 16:45:13  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Pick Gen User Event
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.47  2011/12/16 13:33:02  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.46  2011/12/13 10:22:18  schepuri
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY common function of GUI and RF for  PICK Exception
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.45  2011/12/12 12:48:08  schepuri
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Inserting mandatory field 'item'  in open task at STGM tasktype
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.44  2011/12/09 22:43:03  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY RF Inventory Move changes
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.43  2011/12/05 14:41:32  rmukkera
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY slaes order closed status confirmation related code was added.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.42  2011/11/28 12:19:59  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Pick Confirm Exceptions
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.41  2011/11/24 12:28:29  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Pick Exception.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.40  2011/11/22 13:48:20  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Delete record from inventory if QOH becomes 0
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.39  2011/11/22 12:55:24  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Delete record from inventory if QOH becomes 0
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.38  2011/11/17 22:45:34  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Delete record from inventory if QOH becomes 0
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.37  2011/11/15 20:00:29  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY include inactive condition in stock adjustment filters.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.36  2011/11/15 16:42:48  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Commented Invoke NS inventory adjustment
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.35  2011/11/15 16:04:48  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY set display flag to 'N' when updating inventory
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.34  2011/11/10 11:41:40  schepuri
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Qty discrepancy with short PICK issues fixing
 *  * REVISION HISTORY
 * REVISION HISTORY Revision 1.33  2011/11/09 14:37:54  gkalla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Added function to check Ship completed or not for picks
 * REVISION HISTORY $Log: ebiz_ManualClustering_SL.js,v $
 * REVISION HISTORY Revision 1.1.2.5.4.1  2015/11/24 11:07:29  deepshikha
 * REVISION HISTORY 2015.2 Issue Fix
 * REVISION HISTORY 201414690
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.5  2013/12/13 13:59:18  schepuri
 * REVISION HISTORY 20126075
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.4  2013/09/17 15:30:01  nneelam
 * REVISION HISTORY Case#. 20122210
 * REVISION HISTORY No alert if wave not selected after Back Search Button clicked...
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.3  2013/06/10 14:54:54  rmukkera
 * REVISION HISTORY Issue Fix for
 * REVISION HISTORY 	I have selected two orders for Wave, and system has generated picks, but when I go to Manual Cluster Generation Screen, it is showing multiples. Please see the attached document for more details
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.2  2013/06/04 15:26:45  rmukkera
 * REVISION HISTORY Issue Fix New Filters added
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.1  2013/06/03 15:38:48  rmukkera
 * REVISION HISTORY New File
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.20.4.3.4.4  2013/03/21 14:18:14  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG2012392
 * REVISION HISTORY Prod and UAT issue fixes.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.20.4.3.4.3  2013/03/15 09:28:43  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG2012392
 * REVISION HISTORY Production and UAT issue fixes.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.20.4.3.4.2  2013/03/08 14:45:50  skreddy
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Code merged from Endochoice as part of Standard bundle
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.20.4.3.4.1  2013/03/01 14:34:54  skreddy
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Merged from FactoryMation and change the Company name
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.20.4.3  2013/01/11 13:25:57  skreddy
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY issues related to Qty expection and loc exception
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.20.4.2  2012/11/01 14:55:02  schepuri
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Decimal Qty Conversions
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.20.4.1  2012/09/26 22:43:33  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG2012392
 * REVISION HISTORY Production Issue Fixes for FISK,BOOMBAH and TDG.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.20  2012/09/11 00:45:23  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Production Issue Fixes for FISK and BOOMBAH.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.19  2012/09/04 14:19:10  spendyala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY issue related to item fulfillment is resolved.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.18  2012/08/08 15:17:29  rrpulicherla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Kit to order
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.17  2012/07/04 18:50:10  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Schedule script for confirming picks
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.16  2012/07/03 21:54:34  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Performance Tuning
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.15  2012/07/02 13:13:24  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Performance Tuning
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.14  2012/07/02 12:50:41  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Performance Tuning
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.13  2012/06/02 09:34:32  spendyala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Issue related to pick resolved.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.12  2012/05/09 07:52:37  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Partial Status Handling
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.11  2012/05/04 22:52:08  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Production issue fixes
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.10  2012/05/03 21:17:46  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Production issue fixes
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.9  2012/04/30 06:55:51  mbpragada
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.8  2012/04/20 14:14:07  schepuri
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY changing the Label of Batch #  field to Lot#
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.7  2012/04/05 13:14:20  schepuri
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY paging functionality is added
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.6  2012/04/03 15:19:16  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Performance Tunning
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55.2.5  2012/03/02 01:25:46  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Stable bundle issue fixes
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.63  2012/03/02 01:11:06  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Stable bundle issue fixes
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.62  2012/02/20 13:36:59  gkalla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Added Line level WMS Carrier
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.61  2012/02/10 06:02:18  schepuri
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY issue fix is merged from  1.55.2.3
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.60  2012/02/08 12:03:08  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Code Merge
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.59  2012/02/02 11:26:27  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Stage Rule by Order Type
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.58  2012/02/01 15:12:23  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY ship complete validation
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.57  2012/01/27 08:21:24  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY shipcomplete validate
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.56  2012/01/23 23:46:03  gkalla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Changed the alert message
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.55  2012/01/21 00:45:53  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.54  2012/01/19 15:20:35  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.53  2012/01/16 15:22:42  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.52  2012/01/16 14:43:56  gkalla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY For OrdRec Array
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.51  2012/01/13 16:50:36  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Pick Generation User Event changes
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.50  2012/01/03 16:16:49  gkalla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY For Ship Complete flag validation
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.49  2011/12/22 18:42:33  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY TO Shipping
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.48  2011/12/21 16:45:13  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Pick Gen User Event
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.47  2011/12/16 13:33:02  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.46  2011/12/13 10:22:18  schepuri
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY common function of GUI and RF for  PICK Exception
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.45  2011/12/12 12:48:08  schepuri
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Inserting mandatory field 'item'  in open task at STGM tasktype
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.44  2011/12/09 22:43:03  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY RF Inventory Move changes
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.43  2011/12/05 14:41:32  rmukkera
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY slaes order closed status confirmation related code was added.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.42  2011/11/28 12:19:59  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Pick Confirm Exceptions
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.41  2011/11/24 12:28:29  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Pick Exception.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.40  2011/11/22 13:48:20  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Delete record from inventory if QOH becomes 0
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.39  2011/11/22 12:55:24  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Delete record from inventory if QOH becomes 0
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.38  2011/11/17 22:45:34  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Delete record from inventory if QOH becomes 0
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.37  2011/11/15 20:00:29  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY include inactive condition in stock adjustment filters.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.36  2011/11/15 16:42:48  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Commented Invoke NS inventory adjustment
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.35  2011/11/15 16:04:48  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY set display flag to 'N' when updating inventory
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.34  2011/11/10 11:41:40  schepuri
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Qty discrepancy with short PICK issues fixing
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.32  2011/11/07 12:03:04  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Exception Handling in Auto Packing and Email Alerts
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.31  2011/11/03 14:06:32  schepuri
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Qty discrepancy with short PICK issues fixing
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.30  2011/10/24 21:24:53  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Wrong Status updated in fulfillment order.
 * REVISION HISTORY It should be 8.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.29  2011/10/21 13:43:30  schepuri
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Qty discrepancy with short PICK
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.28  2011/10/04 18:59:25  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG20112
 * REVISION HISTORY Stage Rule Carrier Fetching issue
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.27  2011/09/30 16:44:16  gkalla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY For BOM Report in Work order screen
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.26  2011/09/24 11:04:22  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.25  2011/09/24 08:47:07  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.24  2011/09/21 12:21:58  rmukkera
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY code  modification in calling shipmanifestrecord
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.23  2011/09/16 14:55:20  rrpulicherla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY pickconfirmation
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.22  2011/09/15 16:33:53  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY UOM Conversion
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.21  2011/09/14 05:23:53  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.20  2011/09/08 15:35:51  rrpulicherla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Carrier Determination Related files
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.19  2011/09/07 23:19:00  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Outbound Storage Fix
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.18  2011/09/07 21:49:49  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Outbound Storage Fix
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.17  2011/08/31 13:10:08  rrpulicherla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Pick Confirmation Changes
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.16  2011/08/30 13:23:13  rrpulicherla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Pick Confirmation Changes
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.15  2011/08/29 13:21:57  snimmakayala
 * REVISION HISTORY CASE2011262/CR2011259/LOG2011187
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.14  2011/08/29 13:00:33  snimmakayala
 * REVISION HISTORY CASE2011262/CR2011259/LOG2011187
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.13  2011/08/29 12:44:59  snimmakayala
 * REVISION HISTORY CASE2011262/CR2011259/LOG2011187
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.12  2011/08/29 12:07:46  snimmakayala
 * REVISION HISTORY CASE2011262/CR2011259/LOG2011187
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.11  2011/08/29 10:55:32  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Ship Manifest related fixes.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.10  2011/08/27 08:45:20  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Ship Manifest related fixes.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.9  2011/08/27 06:48:26  snimmakayala
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY Ship Manifest related fixes.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.8  2011/08/25 12:25:45  rrpulicherla
 * REVISION HISTORY CASE201112/CR201113/LOG201121
 * REVISION HISTORY
 * REVISION HISTORY Pick Confirmation Changes
 * REVISION HISTORY Revision 1.7
 * 2011/07/29 11:41:38 rmukkera CASE201112/CR201113/LOG201121 Added
 * CreateShipManifest Functionality
 * 
 * Revision 1.6 2011/07/29 11:01:59 pattili CASE201112/CR201113/LOG201121
 * Corrected few minor issues
 * 
 * Revision 1.5 2011/07/21 06:52:08 pattili CASE201112/CR201113/LOG201121 1.
 * Added the CVS tag which was missing in this file 2. Issue pertaining to
 * container LP is fixed
 * 
 ******************************************************************************/

function ManualClusteringSuitelet(request, response) {
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Manual Cluster Generation');
		var WaveField = form.addField('custpage_wave', 'select', 'Wave').setMandatory(true);
		WaveField.setLayoutType('startrow', 'none');
		WaveField.addSelectOption("", "");
		FillWave(form, WaveField,-1);
		form.addSubmitButton('Display');

		response.writePage(form);
	} 
	else // this is the POST block
	{
		var form = nlapiCreateForm('Manual Cluster Generation');
		form.setScript('customscript_ebiz_manualclustering');
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		msg.setLayoutType('outside', 'startrow');
		var historyparameter = form.addField('custpage_historyparam', 'text', 'history').setDisplayType('hidden');

		var historyparam=-1;
		if((request.getParameter('custpage_historyparam')!=null ) && (request.getParameter('custpage_historyparam')!='null') && (request.getParameter('custpage_historyparam')!=''))
		{
			historyparam=request.getParameter('custpage_historyparam');
			var histvalue=parseInt(request.getParameter('custpage_historyparam'))-parseInt(1);
			historyparameter.setDefaultValue(histvalue);
			historyparam=parseInt(histvalue);
		}
		else
		{
			historyparameter.setDefaultValue(historyparam);

		}

		var linecount=request.getLineItemCount('custpage_items');
		if(linecount!=null && linecount!='' && linecount>0)
		{
			var isCreateCluster=false;
			for(var i=1;i<=linecount;i++)
			{
				nlapiLogExecution('ERROR','i',i);
				if(request.getLineItemValue('custpage_items','custpage_select',i)=='T')
				{
					isCreateCluster=true;
					break;
				}
			}
		}
		// case no 20126075
		var isWavecompleted=false;
		if(isCreateCluster)
		{
			var clusterno = GetMaxTransactionNo('WAVECLUSTER');
			var ordernoArray=new Array();
			var contsizeidArray=new Array();
			for(var i1=1;i1<=linecount;i1++)
			{

				var waveno=request.getParameter('custpage_wave');

				if(request.getLineItemValue('custpage_items','custpage_select',i1)=='T')
				{
					nlapiLogExecution('ERROR','i1',i1);
					var orderno=request.getLineItemValue('custpage_items','custpage_ordernoid',i1);

					var contsizeid=request.getLineItemValue('custpage_items','custpage_containersizeid',i1);
					if(ordernoArray.indexOf(orderno)==-1)
					{
						ordernoArray.push(orderno);
					}
					if((contsizeidArray.indexOf(contsizeid)==-1)&&(contsizeid!=null)&&(contsizeid!='')&&(contsizeid!='- None -'))
					{
						contsizeidArray.push(contsizeid);
					}
				}
			}
			nlapiLogExecution('ERROR','ordernoArray',ordernoArray);
			nlapiLogExecution('ERROR','contsizeidArray',contsizeidArray);
			var filtersso = new Array();
			filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
			//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
			filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9'])); 
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',waveno));
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',ordernoArray));
			if(contsizeidArray.length>0)
				filtersso.push(new nlobjSearchFilter('custrecord_container', null, 'anyof',contsizeidArray));

			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'isempty'));

			var columnso = new Array();
			columnso.push(new nlobjSearchColumn('name'));

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);
			nlapiLogExecution('ERROR','searchresults',searchresults);
			if(searchresults!=null && searchresults!='' && searchresults.length>0)
			{
				nlapiLogExecution('ERROR','searchresultslength',searchresults.length);
				for(var m=0;m<searchresults.length;m++)
				{
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', searchresults[m].getId(), 'custrecord_ebiz_clus_no',clusterno.toString());
				}
				if(parseInt(linecount) == parseInt(searchresults.length))
					isWavecompleted = true;

				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cluster Generated Successfully  "+clusterno+" .', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
				//historyparam=-2;
			}




		}


		if(isWavecompleted != true)
		{
			var WaveField = form.addField('custpage_wave', 'select', 'Wave');
			WaveField.setLayoutType('outside', 'startrow');
			if((request.getParameter('custpage_wave')!=null) && (request.getParameter('custpage_wave')!=''))
			{

			WaveField.setDefaultValue(request.getParameter('custpage_wave'));
			WaveField.setDisplayType('disabled');
			var sublist = form.addSubList("custpage_items", "list", "Orders List");	
			sublist.addField("custpage_select", "checkbox", "Create Cluster?").setDefaultValue('F');
			sublist.addField("custpage_ordno", "text", "Order #");
			sublist.addField("custpage_customer", "text", "Customer");
			sublist.addField("custpage_qty", "text", "Qty");//.setDisplayType('hidden');//santosh
			sublist.addField("custpage_containersize", "text", "Carton Size");
			sublist.addField("custpage_noofcartons", "text", "No Of Cartons");
			sublist.addField("custpage_containersizeid", "text", "").setDisplayType('hidden');
			sublist.addField("custpage_ordernoid", "text", "ordid").setDisplayType('hidden');

			var orderlist=getOrders(request.getParameter('custpage_wave'));
			if(orderlist!=null && orderlist!='')
			{
				
				sublist.addMarkAllButtons();
				for(var j=0;j<orderlist.length;j++)
				{
					nlapiLogExecution('ERROR','',orderlist[j].getValue('custrecord_container',null,'group'));
					form.getSubList('custpage_items').setLineItemValue('custpage_ordno', j + 1,
							orderlist[j].getValue('tranid','custrecord_ebiz_order_no','group'));
					form.getSubList('custpage_items').setLineItemValue('custpage_ordernoid', j + 1,
							orderlist[j].getValue('internalid','custrecord_ebiz_order_no','group'));
					form.getSubList('custpage_items').setLineItemValue('custpage_customer', j + 1,
							orderlist[j].getText('entity','custrecord_ebiz_order_no','group'));
					form.getSubList('custpage_items').setLineItemValue('custpage_qty', j + 1,
							orderlist[j].getValue('custrecord_expe_qty',null,'sum'));
					
					if((orderlist[j].getValue('custrecord_container',null,'group')!=null) && (orderlist[j].getValue('custrecord_container',null,'group')!='null')&& (orderlist[j].getValue('custrecord_container',null,'group')!=''))
					{
						form.getSubList('custpage_items').setLineItemValue('custpage_containersize', j + 1,
								orderlist[j].getText('custrecord_container',null,'group'));
						form.getSubList('custpage_items').setLineItemValue('custpage_containersizeid', j + 1,
								orderlist[j].getValue('custrecord_container',null,'group'));
					}
					else
					{
						form.getSubList('custpage_items').setLineItemValue('custpage_containersize', j + 1,'');
						form.getSubList('custpage_items').setLineItemValue('custpage_containersizeid', j + 1,'');
					}
					form.getSubList('custpage_items').setLineItemValue('custpage_noofcartons', j + 1,
							orderlist[j].getValue('custrecord_container_lp_no',null,'count'));
				}
			}


		}
		else
		{
			WaveField.addSelectOption("", "");
		}

			FillWave(form, WaveField,-1);
			form.addSubmitButton('Generate Cluster');
			
		}
		//case # 20124401 Start
		form.addButton('custpage_release','Back To Search','backtosearch()');
		//case # 20124401 End
		response.writePage(form);
	}
}
//case # 20124401 Start
function backtosearch(){

	var url=nlapiResolveURL('SUITELET', 'customscript_ebiz_manualclustering', 'customdeploy_ebiz_manualclustering_di');
	window.location.href = url;
}
//casse # 20124401 End
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}
function getOrders(waveno)
{
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9'])); 
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',waveno));  
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'isempty'));
	filtersso.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));

	var columnso = new Array();
	//columnso.push(new nlobjSearchColumn('id',null,'group').setSort('true'));	
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group').setSort('true'));
	//columnso.push(new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group'));
	columnso.push(new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group'));
	columnso.push(new nlobjSearchColumn('tranid','custrecord_ebiz_order_no','group'));
	columnso.push(new nlobjSearchColumn('internalid','custrecord_ebiz_order_no','group'));
	columnso.push(new nlobjSearchColumn('custrecord_expe_qty',null,'sum'));
	columnso.push(new nlobjSearchColumn('custrecord_container',null,'group'));
	columnso.push(new nlobjSearchColumn('id',null,'count'));
	columnso.push(new nlobjSearchColumn('custrecord_container_lp_no',null,'count'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);

	return searchresults;

}

function FillWave(form, WaveField,maxno){

	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9'])); 
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty'));  
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'isempty'));
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('id').setSort('true'));	
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave_no').setSort('true'));
	columnso.push(new nlobjSearchColumn('name'));

	//WaveField.addSelectOption("", "");


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		if(searchresults[i].getValue('custrecord_ebiz_wave_no') != null && searchresults[i].getValue('custrecord_ebiz_wave_no') != "" && searchresults[i].getValue('custrecord_ebiz_wave_no') != " ")
		{
			var resdo = form.getField('custpage_wave').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_wave_no'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		WaveField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_wave_no'), searchresults[i].getValue('custrecord_ebiz_wave_no'));
	}


	if(searchresults!=null && searchresults.length>=1000)
	{
		var maxno=searchresults[searchresults.length-1].getValue('id');		
		FillWave(form, WaveField,maxno);	
	}
}
