/***************************************************************************
	  		�eBizNET Solutions Inc    
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_WOSummaryDetails_SL.js,v $
 *     	   $Revision: 1.1.4.1.4.1.4.1.6.2 $
 *     	   $Date: 2014/08/08 15:02:52 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *


 *****************************************************************************/
/**
 * Function to return the current date in mm/dd/yyyy format
 * @author RamiReddy
 * @returns Current system date
 */
/*function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}*/
function getAllWorkOrders(){
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[2] = new nlobjSearchColumn('status');
	columns[3] = new nlobjSearchColumn('item');
	
	columns[0].setSort(true);	
	var searchResults = nlapiSearchRecord('workorder', null, filters, columns);
	//var searchResults = nlapiLoadRecord('workorder', null);
	
	return searchResults;
}
function addAllWorkOrdersToField(workorder, workorderList,form){
	
	if(workorderList != null && workorderList.length > 0){
		for (var i = 0; i < workorderList.length ; i++) {	
			
		var res=  form.getField('custpage_workorder').getSelectOptions(workorderList[i].getValue('tranid'), 'is');
		if (res != null) {
			if (res.length > 0) {
				continue;
			}
		}
		workorder.addSelectOption(workorderList[i].getValue('internalid'), workorderList[i].getValue('tranid'));

		}
	}	
}
function addAllWorkOrderstatusToField(WorkOrderStatus, workorderList,form){
	
	if(workorderList != null && workorderList.length > 0){
		for (var i = 0; i < workorderList.length ; i++) {	
			
		var res=  form.getField('custpage_wostatus').getSelectOptions(workorderList[i].getValue('status'), 'is');
		if (res != null) {
			if (res.length > 0) {
				continue;
			}
		}
		WorkOrderStatus.addSelectOption(workorderList[i].getValue('status'), workorderList[i].getValue('status'));

		}
	}	
}
function addAllWorkOrderMainItemField(WorkOrderMainitem, workorderList,form){
	
	if(workorderList != null && workorderList.length > 0){
		for (var i = 0; i < workorderList.length ; i++) {	
			
		var res=  form.getField('custpage_mainitem').getSelectOptions(workorderList[i].getText('item'),'is');
		if (res != null) {
			if (res.length > 0) {
				continue;
			}
		}
		WorkOrderMainitem.addSelectOption(workorderList[i].getValue('item'), workorderList[i].getText('item'));

		}
	}	
}

function ebiznetWOSummeryDetails(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('WO Summary Details Report');
		form.setScript('customscript_pickreportprint');
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');		
		var sysdate=DateStamp();
		fromdate.setDefaultValue(sysdate);

		
	     var WorkOrder  = form.addField('custpage_workorder', 'select', 'WO #').setLayoutType('startrow', 'none');
	 	WorkOrder.addSelectOption("","");
	 	var WorkOrderList = getAllWorkOrders();		
		addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);
	     
	var WorkOrderStatus = form.addField('custpage_wostatus', 'select', 'WO Status');
	WorkOrderStatus.addSelectOption("","");
 	var WorkOrderList = getAllWorkOrders();		
	addAllWorkOrderstatusToField(WorkOrderStatus, WorkOrderList,form);
	
	var todate = form.addField('custpage_todate', 'date', 'To Date');		 
	todate.setDefaultValue(sysdate);

	var WorkOrderMainitem = form.addField('custpage_mainitem', 'select', 'Assembly Item');
	WorkOrderMainitem.addSelectOption("","");
 	var WorkOrderList = getAllWorkOrders();		
	addAllWorkOrderMainItemField(WorkOrderMainitem, WorkOrderList,form);
	
	form.addSubmitButton('Display');

	response.writePage(form);
	}
	else
	{
		var form = nlapiCreateForm('WO Summary Details Report');
		
		var vQbfromdate = request.getParameter('custpage_fromdate');
		var vQbtodate = request.getParameter('custpage_todate');
		var QbWO;
		if(request.getParameter('custpage_workorder')!='' && request.getParameter('custpage_workorder')!=null)
		{
			QbWO = request.getParameter('custpage_workorder');
		}
		var QbWOstatus = request.getParameter('custpage_wostatus');
		var QbAssemblyitem = request.getParameter('custpage_mainitem');
		form.setScript('customscript_wosummary_details');
		nlapiLogExecution('ERROR', 'vQbfromdate', vQbfromdate);
		nlapiLogExecution('ERROR', 'vQbtodate', vQbtodate);
		form.addButton('custpage_print','Print','Printreport('+QbWO+')');
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date').setDefaultValue(vQbfromdate);
		

	     var WorkOrder  = form.addField('custpage_workorder', 'select', 'WO #').setLayoutType('startrow', 'none');
	 	WorkOrder.addSelectOption("","");
	 	var WorkOrderList = getAllWorkOrders();		
		addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);
		WorkOrder.setDefaultValue(QbWO);
		
		var WorkOrderStatus = form.addField('custpage_wostatus', 'select', 'WO Status');
		WorkOrderStatus.addSelectOption("","");
	 	var WorkOrderList = getAllWorkOrders();		
		addAllWorkOrderstatusToField(WorkOrderStatus, WorkOrderList,form);
		WorkOrderStatus.setDefaultValue(QbWOstatus);
		
		var todate = form.addField('custpage_todate', 'date', 'To Date').setDefaultValue(vQbtodate);

		var WorkOrderMainitem = form.addField('custpage_mainitem', 'select', 'Assembly Item');
		WorkOrderMainitem.addSelectOption("","");
	 	var WorkOrderList = getAllWorkOrders();		
		addAllWorkOrderMainItemField(WorkOrderMainitem, WorkOrderList,form);
		WorkOrderMainitem.setDefaultValue(QbAssemblyitem);
		
		form.addSubmitButton('Display');
		
		//Case# 20149836 starts
		var wostatusmodified='';
		var wostatus = request.getParameter('custpage_wostatus');
		nlapiLogExecution('ERROR', 'wostatus', wostatus);
		if (wostatus !='' && wostatus !='null' && wostatus !=null)
		{
			if(wostatus == 'fullyBuilt')
				wostatusmodified='WorkOrd:G';
			else if (wostatus == 'pendingBuild')
				wostatusmodified='WorkOrd:B';
			else if(wostatus == 'partiallyBuilt')
				wostatusmodified='WorkOrd:D';
			else if(wostatus == 'closed')
				wostatusmodified='WorkOrd:H';
		}
		//Case# 20149836 ends
		var sublist = form.addSubList("custpage_items", "list", "WO Summary List");

		sublist.addField("custpage_wo", "text", "WO #");
		sublist.addField("custpage_datecreated", "text", "Date Created");
		sublist.addField("custpage_mainitem", "text", "Assembly Item");	
		sublist.addField("custpage_qty", "text", "Qty on Work order");
		sublist.addField("custpage_status", "text", "Status");	

		sublist.addField("custpage_printed", "text", "Printed");
		
		
		var qty,location,woid,tranid,item,itemid,status,printflag;
		nlapiLogExecution('ERROR', 'QbWO', QbWO);
		var filters = new Array();
		if(QbWO!='' && QbWO!=null)
		{
		filters.push(new nlobjSearchFilter('internalid', null, 'is', QbWO));
		}
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {

			filters.push(new nlobjSearchFilter('trandate', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));			 
		} 
		if ((request.getParameter('custpage_mainitem') != "" && request.getParameter('custpage_mainitem') != null) ) {

			filters.push(new nlobjSearchFilter('item', null, 'anyof', request.getParameter('custpage_mainitem')));			 
		}
		//Case# 20149836 starts
		/*if ((request.getParameter('custpage_wostatus') != "" && request.getParameter('custpage_wostatus') != null) ) {

			filters.push(new nlobjSearchFilter('status', null, 'is', request.getParameter('custpage_wostatus')));			 
		}*/
		if (wostatus !='' && wostatus !='null' && wostatus !=null)
			{
			filters.push(new nlobjSearchFilter('status', null, 'is',wostatusmodified));
		}
		//Case# 20149836 ends
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('tranid');
		columns[1] = new nlobjSearchColumn('internalid');
		columns[2] = new nlobjSearchColumn('status');
		columns[3] = new nlobjSearchColumn('trandate');
		columns[4] = new nlobjSearchColumn('quantity');
		columns[5] = new nlobjSearchColumn('status');
		columns[6] = new nlobjSearchColumn('custbody_ebiz_isprinted');
		columns[7] = new nlobjSearchColumn('item');		
		
		var searchResults = nlapiSearchRecord('workorder', null, filters , columns);

		nlapiLogExecution('ERROR', 'searchresults', searchResults);
		for(var i = 0; searchResults != null && i < searchResults.length; i++) 
		{ 
			
			var WO = searchResults[i].getValue('tranid'); 
			var WOid = searchResults[i].getValue('internalid');			
			nlapiLogExecution('ERROR', 'WO', WO);			
			itemid=searchResults[i].getValue('item'); 			
			var itemText = searchResults[i].getText('item');			
			var WoCreatedDate=searchResults[i].getValue('trandate');			
			var itemQty=searchResults[i].getValue('quantity');			
			var itemStatus=searchResults[i].getValue('status');			
			var printflag=searchResults[i].getValue('custbody_ebiz_isprinted');
			if(printflag=='T')
				printflag='Yes';
			else
				printflag='No';
			
			form.getSubList('custpage_items').setLineItemValue('custpage_wo', i + 1, WO);
			form.getSubList('custpage_items').setLineItemValue('custpage_datecreated', i + 1, WoCreatedDate);
			form.getSubList('custpage_items').setLineItemValue('custpage_mainitem', i + 1, itemText);			
			form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, itemQty);			 
			form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, itemStatus);
			form.getSubList('custpage_items').setLineItemValue('custpage_printed', i + 1, printflag);
			
		}
		response.writePage(form);
	}
}


function Printreport(WOno){
	nlapiLogExecution('ERROR', 'into Printreport WOno',WOno);
	var fromdate = nlapiGetFieldValue('custpage_fromdate');
	var todate = nlapiGetFieldValue('custpage_todate');
	var status=nlapiGetFieldValue('custpage_wostatus');
	var item=nlapiGetFieldValue('custpage_mainitem');
	
	
	var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_wosummary_detailspdf', 'customdeploy_wosummary_detailspdf_di');
	/* var ctx = nlapiGetContext();
	 nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	 if (ctx.getEnvironment() == 'PRODUCTION') {
		 WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
    }
    else 
        if (ctx.getEnvironment() == 'SANDBOX') {
       	 WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
        }*/
	nlapiLogExecution('ERROR', 'Wave PDF URL',WavePDFURL);					
	WavePDFURL = WavePDFURL + '&custparam_wono='+ WOno+'&custparam_fromdate='+fromdate+'&custparam_todate='+todate+'&custparam_status='+status+'&custparam_item='+item;	
	//WavePDFURL = WavePDFURL + '&custparam_wono='+ WOno;
	window.open(WavePDFURL);
	
}