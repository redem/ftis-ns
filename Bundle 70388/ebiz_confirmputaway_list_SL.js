/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_confirmputaway_list_SL.js,v $
 *     	   $Revision: 1.2.4.3.8.1 $
 *     	   $Date: 2014/02/17 15:35:12 $
 *     	   $Author: nneelam $
 *     	   $Name: t_NSWMS_2014_1_2_0 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_confirmputaway_list_SL.js,v $
 * Revision 1.2.4.3.8.1  2014/02/17 15:35:12  nneelam
 * case#  20127188
 * Standard Bundle Issue Fix.
 *
 * Revision 1.2.4.3  2012/06/20 07:18:52  mbpragada
 * 20120490
 *
 * Revision 1.2.4.2  2012/06/14 13:46:10  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stable bundle issue fix
 *
 * Revision 1.2.4.1  2012/05/02 06:24:32  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new Column i.e., TO which on click redirects to TOConfirm putaway screen.
 *
 * Revision 1.2  2011/07/21 06:39:43  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function ebiznet_confirmputaway_list_SL(request, response){
	var list = nlapiCreateList('Confirm Putaway List');
	list.setStyle('normal');
	var column = list.addColumn('name', 'text', 'PO', 'LEFT');
	column.setURL(nlapiResolveURL('SUITELET', 'customscript_confirmputaway', 'customdeploy1'));
	column.addParamToURL('custparam_poid', 'custrecord_ebiz_cntrl_no', true);

	var columnTO = list.addColumn('transfer', 'text', 'TO', 'LEFT');
	columnTO.setURL(nlapiResolveURL('SUITELET', 'customscript_to_confirmputaway_sl', 'customdeploy_to_confirmputaway_dl'));
	columnTO.addParamToURL('custparam_poid', 'custrecord_ebiz_cntrl_no', true);

	var columnreceipt=list.addColumn('custrecord_receiptno', 'text', 'Receipt#', 'LEFT');
	columnreceipt.setURL(nlapiResolveURL('SUITELET', 'customscript_confirmputaway', 'customdeploy1'));
	columnreceipt.addParamToURL('custparam_receipt', 'custrecord_receiptno', true);	

	var columntrailer=list.addColumn('custrecord_ebiz_trailer_no', 'text', 'Appointment#', 'LEFT');
	columntrailer.setURL(nlapiResolveURL('SUITELET', 'customscript_confirmputaway', 'customdeploy1'));
	columntrailer.addParamToURL('custparam_trailer', 'custrecord_ebiz_trailer_no', true);

//	list.addColumn('trantype', 'text', 'Type', 'LEFT');

	list.addColumn('custrecord_sku', 'text', 'Item', 'LEFT');
	list.addColumn('custrecord_sku_status', 'text', 'Item Status', 'LEFT');

	var RoleLocation=getRoledBasedLocation();
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);
	//case # 20127188...Added location filter
	if(RoleLocation!='' && RoleLocation!=null)
	filters[2] = new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', RoleLocation);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_sku_status');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
	columns[6] = new nlobjSearchColumn('internalid');
	columns[6].setSort(true);
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	var po = new Object();
	var dupliarray = new Array();
	var redarray = new Array();
	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		redarray[i] = searchresults[i].getValue('name');
	}
	var vtrantype='';
	var oldcntrlno='';
	var redarray = removeDuplicateElement(redarray);
	for (var i = 0; i < redarray.length; i++) {
		for (var j = 0; searchresults != null && j < searchresults.length; j++) {
			if (redarray[i] == searchresults[j].getValue('name')) {
				try{
					var newcntrlno=searchresults[j].getValue('name');
					//nlapiLogExecution('ERROR', 'newcntrlno',newcntrlno);
					//nlapiLogExecution('ERROR', 'oldcntrlno',oldcntrlno);
					po["custrecord_receiptno"] = searchresults[j].getValue('custrecord_ot_receipt_no');
					po["custrecord_ebiz_trailer_no"] = searchresults[j].getValue('custrecord_ebiz_trailer_no');
					po["custrecord_sku"] = searchresults[j].getText('custrecord_sku');
					po["custrecord_sku_status"] = searchresults[j].getText('custrecord_sku_status');
					po["custrecord_ebiz_cntrl_no"] = searchresults[j].getValue('custrecord_ebiz_cntrl_no');
					if(oldcntrlno!=newcntrlno)
					{
						nlapiLogExecution('ERROR', 'newcntrlno',newcntrlno);
						vtrantype= nlapiLookupField('transaction',searchresults[j].getValue('custrecord_ebiz_cntrl_no'),'recordType');
						oldcntrlno=newcntrlno;
						nlapiLogExecution('ERROR', 'vtrantype',vtrantype);
					}
//					po["trantype"]=vtrantype;
					if(vtrantype!='transferorder')
					{
						po["name"] = searchresults[j].getValue('name');
						po["transfer"] ="";
					}
					else
					{
						po["name"] ="";
						po["transfer"] = searchresults[j].getValue('name');
					}
				}
				catch(exp)
				{
					nlapiLogExecution('ERROR', 'Exception in list load',exp);
				}
			}
		}
		list.addRow(po);
	}
	response.writePage(list);
}

function getRoledBasedLocation()
{
	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('Debug', 'subs', subs);
	if(subs != null && subs != '' && subs==true)
	{
		var context=nlapiGetContext();
		var vSubsid=context.getSubsidiary();
		var filters=new Array();
		if(vSubsid != null && vSubsid != '')
			filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', vSubsid));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_whlocationtype', null, 'anyof', 1));
		filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));		
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F')); 
		var searchresults = nlapiSearchRecord('location', null, filters, null);
		if(searchresults != null && searchresults !='')
		{
			var vRoleLocation=new Array();
			for(var k=0;k<searchresults.length;k++)
			{
				vRoleLocation.push(searchresults[k].getId());
			}
			return vRoleLocation;
		}
		else
			return null;
	}
	else
	{
		var vMultiSite=true;
		var context = nlapiGetContext();
		var vRoleLocation=context.getLocation();
		if(vMultiSite==true)
		{
			return vRoleLocation;

		}	
		else
			return null;
	}
}
