/***************************************************************************
�eBizNET Solutions Inc 
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_DisplayReGenerateItemFullfillment.js,v $
 *� $Revision: 1.1.4.2.4.1.4.3.4.3 $
 *� $Date: 2015/01/07 13:50:55 $
 *� $Author: schepuri $
 *� $Name: t_eBN_2014_2_StdBundle_0_204 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_DisplayReGenerateItemFullfillment.js,v $
 *� Revision 1.1.4.2.4.1.4.3.4.3  2015/01/07 13:50:55  schepuri
 *� issue#   201411371
 *�
 *� Revision 1.1.4.2.4.1.4.3.4.2  2015/01/07 13:42:20  schepuri
 *� issue#   201411331
 *�
 *� Revision 1.1.4.2.4.1.4.3.4.1  2014/12/23 12:32:43  snimmakayala
 *� Case#: 201411311
 *�
 *� Revision 1.1.4.2.4.1.4.3  2013/10/10 15:36:52  skreddy
 *� Case# 20124888
 *� tpp SB issue : Regenerate Item fulfillment
 *�
 *� Revision 1.1.4.2.4.1.4.2  2013/04/03 06:43:06  gkalla
 *� CASE201112/CR201113/LOG201121
 *� As part of standard bundle
 *�
 *� Revision 1.1.4.2.4.1.4.1  2013/03/01 15:01:12  rmukkera
 *� code is merged from FactoryMation production as part of Standard bundle
 *�
 *� Revision 1.1.4.2.4.1  2012/11/01 14:55:02  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.1.4.2  2012/07/18 13:33:13  schepuri
 *� CASE201112/CR201113/LOG201121
 *� wbc issuefix
 *�
 *� Revision 1.1.4.1  2012/04/20 14:03:29  schepuri
 *� CASE201112/CR201113/LOG201121
 *� changing the Label of Batch #  field to Lot#
 *�
 *� Revision 1.1  2011/11/11 11:45:43  spendyala
 *�  CASE201112/CR201113/LOG201121
 *�  It displays the task details and exception details of the respective order whose record is not confirmed by nswms,and regenerate the itemfullfillment
 *�
 *
 ****************************************************************************/

function ReGenerateItemFullfillmentSuitelet(request, response){
	if (request.getMethod() == 'GET') {

		var vfullfillord = request.getParameter('custparm_fullfillordno');
		nlapiLogExecution('ERROR','vfullfillord',vfullfillord);
		var searchresults ="";

		var form = nlapiCreateForm('Regenerate Itemfulfillment');


		var fullfillmentOrderNo=form.addField('custpage_hiddenfield','text','Hidden').setDisplayType('hidden');
		fullfillmentOrderNo.setDefaultValue(vfullfillord);



		var sublist =form.addSubList("custpage_exceptiondetails", "staticlist", "Exception Details"); 
		sublist.addField("custpage_name", "text", "Name");
		sublist.addField("custpage_id", "text", "Id").setDisplayType('inline');
		sublist.addField("custpage_transactiontype", "text", "Transaction Type").setDisplayType('inline');
		sublist.addField("custpage_functionality", "text", "Functionality");
		sublist.addField("custpage_exception", "text", "Exception").setDisplayType('inline');
		sublist.addField("custpage_reftwo", "text", "Reference2").setDisplayType('inline');
		sublist.addField("custpage_refone", "text", "Reference1");

		var sublist1 =form.addSubList("custpage_itemsfulfillment", "list", "Task Details"); 
		sublist1.addField('custpage_containerlp', 'text', 'Container Lp#');
		sublist1.addField("custpage_order", "text", "Order");
		sublist1.addField("custpage_name", "text", "Name");
		sublist1.addField("custpage_actualbegindate", "text", "Actual Begin Date").setDisplayType('inline');
		sublist1.addField("custpage_actualenddate", "text", "Actual End Date").setDisplayType('inline');
		//sublist1.addField("custpage_excepteddate", "text", "Excepted Date");
		sublist1.addField("custpage_actualqty", "text", "Actual Qty").setDisplayType('inline');
		sublist1.addField("custpage_batchno", "text", "LOT#").setDisplayType('inline');
		sublist1.addField("custpage_item", "text", "Item");
		sublist1.addField("custpage_waveno","text","eBizWave#").setDisplayType('inline');
		sublist1.addField("custpage_wmsstatusflag","text","WMS Status Flag");
		sublist1.addField("custpage_lineno", "text", "LineNo").setDisplayType('inline');
		sublist1.addField("custpage_lp", "text", "Lp#");
		sublist1.addField("custpage_packcode", "text", "Pack Code").setDisplayType('inline');
		sublist1.addField("custpage_tasktype", "text", "Task Type").setDisplayType('inline');
		sublist1.addField("custpage_actualbeginloc", "text", "Actual Begin Location");
		sublist1.addField("custpage_actualendloc","text","Actual End Location").setDisplayType('hidden');
		sublist1.addField("custpage_pickqty","text","Pick Qty");
		sublist1.addField("custpage_datecreated", "text", "Date Created").setDisplayType('inline');
		sublist1.addField("custpage_expectedqty", "text", "Expected Qty");
		sublist1.addField("custpage_wmsloc","text","WMS Location").setDisplayType('hidden');

		var filters = new Array();

		if (vfullfillord != "")
		{
			filters[0] = new nlobjSearchFilter( 'name', null, 'is', vfullfillord );
			filters[1] = new nlobjSearchFilter( 'custrecord_ebiz_nsconfirm_ref_no', null,'isempty');
			filters[2] = new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]);
			filters[3] = new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [28] );
			filters[4] = new nlobjSearchFilter('status','custrecord_ebiz_order_no', 'noneof', ['SalesOrd:F','SalesOrd:G','SalesOrd:H']);

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			columns[2] = new nlobjSearchColumn('name');
			columns[3] = new nlobjSearchColumn('custrecordact_begin_date');
			columns[4] = new nlobjSearchColumn('custrecord_act_end_date');
			columns[5] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[6] = new nlobjSearchColumn('custrecord_act_qty');
			columns[7] = new nlobjSearchColumn('custrecord_batch_no');
			columns[8] = new nlobjSearchColumn('custrecord_sku');
			columns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
			columns[10] = new nlobjSearchColumn('custrecord_wms_status_flag');
			columns[11] = new nlobjSearchColumn('custrecord_line_no');
			columns[12] = new nlobjSearchColumn('custrecord_lpno');
			columns[13] = new nlobjSearchColumn('custrecord_packcode');
			columns[14] = new nlobjSearchColumn('custrecord_tasktype');
			columns[15] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[16] = new nlobjSearchColumn('custrecord_actendloc');
			columns[17] = new nlobjSearchColumn('custrecord_pick_qty');
			//columns[18] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[18] = new nlobjSearchColumn('custrecord_wms_location');

			searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		}

		var OrderNo="";
		var ContainerLp="";
		if(searchresults != null && searchresults != ""){
			ContainerLp = searchresults[0].getValue('custrecord_container_lp_no');
			OrderNo = searchresults[0].getValue('trecord_ebiz_order_no');
			var name = searchresults[0].getValue('name');
			var ActBeginDate = searchresults[0].getValue('custrecordact_begin_date');
			var ActEndDate = searchresults[0].getValue('custrecord_act_end_date');
			//var ExptDate = searchresults[0].getValue('custrecord_expe_qty');
			var ActQty = searchresults[0].getValue('custrecord_act_qty');
			var BatchNo= searchresults[0].getValue('custrecord_batch_no');
			var Sku = searchresults[0].getValue('custrecord_sku');
var SkuText = searchresults[0].getText('custrecord_sku');
			var WaveNo = searchresults[0].getValue('custrecord_ebiz_wave_no');
			var WmsStatusFlag = searchresults[0].getValue('custrecord_wms_status_flag');
var WmsStatusFlagText = searchresults[0].getText('custrecord_wms_status_flag');
			var LineNo = searchresults[0].getValue('custrecord_line_no');
			var LpNo = searchresults[0].getValue('custrecord_lpno');
			var PackCode = searchresults[0].getValue('trecord_packcode');
			var TaskType = searchresults[0].getValue('record_tasktype');
	var TaskTypeText = searchresults[0].getText('record_tasktype');
			var ActBeginLoc = searchresults[0].getValue('custrecord_actbeginloc');
var ActBeginLocText = searchresults[0].getText('custrecord_actbeginloc');
			var ActEndLoc = searchresults[0].getValue('custrecord_actendloc');
			var PickQty = searchresults[0].getValue('custrecord_pick_qty');
			var ExptQty = searchresults[0].getValue('custrecord_expe_qty');
			var WmsLoc = searchresults[0].getValue('custrecord_wms_location');

			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_containerlp', 1,ContainerLp);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_order', 1, OrderNo);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_name', 1, name);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_actualbegindate', 1,ActBeginDate );
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_actualenddate', 1, ActEndDate);
			//form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_excepteddate', 1, ExptDate);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_actualqty', 1,ActQty);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_batchno', 1, BatchNo);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_item', 1, SkuText);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_waveno', 1, WaveNo);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_wmsstatusflag', 1,WmsStatusFlagText);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_lineno', 1, LineNo);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_lp', 1, LpNo);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_packcode', 1, PackCode);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_tasktype', 1, TaskTypeText);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_actualbeginloc', 1, ActBeginLocText);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_actualendloc', 1, ActEndLoc);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_pickqty',1,PickQty);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_datecreated', 1, PickQty);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_expectedqty', 1, ExptQty);
			form.getSubList('custpage_itemsfulfillment').setLineItemValue('custpage_wmsloc', 1, WmsLoc);
		}


		if(OrderNo!=null&&ContainerLp!=null)
		{
			var filterExceptionLog=new Array();
			filterExceptionLog.push(new nlobjSearchFilter('custrecord_exceptionlog_reference1',null,'is',OrderNo));
			filterExceptionLog.push(new nlobjSearchFilter('custrecord_exceptionlog_reference2',null,'is',ContainerLp));

			var columnExceptionLog=new Array();
			columnExceptionLog[0]=new nlobjSearchColumn('name');
			columnExceptionLog[1]=new nlobjSearchColumn('internalid');
			columnExceptionLog[2]=new nlobjSearchColumn('custrecord_exceptionlog_trantype');
			columnExceptionLog[3]=new nlobjSearchColumn('custrecord_exceptionlog_functionality');
			columnExceptionLog[4]=new nlobjSearchColumn('custrecord_exceptionlog_exception');
			columnExceptionLog[5]=new nlobjSearchColumn('custrecord_exceptionlog_reference2');
			columnExceptionLog[6]=new nlobjSearchColumn('custrecord_exceptionlog_reference1');

			var searchrecExceptionLog=nlapiSearchRecord('customrecord_exception_log',null,filterExceptionLog,columnExceptionLog);

			if(searchrecExceptionLog != null && searchrecExceptionLog != ""){
				form.getSubList('custpage_exceptiondetails').setLineItemValue('custpage_name', 1, searchrecExceptionLog[0].getValue('name'));
				form.getSubList('custpage_exceptiondetails').setLineItemValue('custpage_id', 1, searchrecExceptionLog[0].getValue('internalid'));
				form.getSubList('custpage_exceptiondetails').setLineItemValue('custpage_transactiontype', 1, searchrecExceptionLog[0].getText('custrecord_exceptionlog_trantype'));
				form.getSubList('custpage_exceptiondetails').setLineItemValue('custpage_functionality', 1, searchrecExceptionLog[0].getValue('custrecord_exceptionlog_functionality'));
				form.getSubList('custpage_exceptiondetails').setLineItemValue('custpage_exception', 1, searchrecExceptionLog[0].getValue('custrecord_exceptionlog_exception'));
				form.getSubList('custpage_exceptiondetails').setLineItemValue('custpage_reftwo',1,searchrecExceptionLog[0].getValue('custrecord_exceptionlog_reference2'));
				form.getSubList('custpage_exceptiondetails').setLineItemValue('custpage_refone', 1, searchrecExceptionLog[0].getValue('custrecord_exceptionlog_reference1'));

			}
		}

		form.addSubmitButton('Submit');
		response.writePage(form);
	}
	else //this is the POST block
	{
		var form = nlapiCreateForm('Regenerate Itemfulfillment');

		var vfullfillord = request.getParameter('custpage_hiddenfield');
		nlapiLogExecution('ERROR','vfullfillordPost',vfullfillord);
		var searchresults ="";
		var filters = new Array();
		if (vfullfillord != "")
		{
			filters[0] = new nlobjSearchFilter( 'name', null, 'is', vfullfillord );
			filters[1] = new nlobjSearchFilter( 'custrecord_ebiz_nsconfirm_ref_no', null,'isempty' );
			//filters[2] = new nlobjSearchFilter( 'custrecord_act_end_date', null,'isempty' );
			//case # 20124888 start : changed tasktype 'Pick' to 'Pack'
			filters[2] = new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [14]);
			//end of the case # 20124888
			filters[3] = new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [28] );
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_notes');
			columns[1] = new nlobjSearchColumn('name');
			columns[2] = new nlobjSearchColumn('custrecord_container_lp_no').setSort(true);
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		}
		var Soid="";
		var ContainerLp="";
		if(searchresults != null && searchresults != ""){
			for (var i = 0; i < searchresults.length; i++) 
			{
				//var searchresult = searchresults[i];
				var vNotes = searchresults[i].getValue('custrecord_notes');
				nlapiLogExecution('ERROR','vNotes',vNotes);
				var vname = searchresults[i].getValue('name');
				nlapiLogExecution('ERROR','name',vname);
				var vid = searchresults[i].getId();
				nlapiLogExecution('ERROR','vid',vid);
				Soid=searchresults[i].getValue('custrecord_ebiz_order_no');
				ContainerLp=searchresults[i].getValue('custrecord_container_lp_no');
				nlapiLogExecution('ERROR','Soid',Soid);
				nlapiLogExecution('ERROR','ContainerLp',ContainerLp);
				var ReGeneratefulfilmentOrderline = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', parseFloat(vid));

				nlapiLogExecution('ERROR','before','before');
				ReGeneratefulfilmentOrderline.setFieldValue('custrecord_notes', '');
				nlapiLogExecution('ERROR','after','after');
				nlapiSubmitRecord(ReGeneratefulfilmentOrderline, false, true);
				nlapiLogExecution('ERROR', 'updateFulfilmentOrder ', 'Success');

			}

		}
		else
		{
			var otFilters = new Array();
			var otColumns = new Array();

			otFilters[0] = new nlobjSearchFilter( 'name', null, 'is', vfullfillord );
			otFilters[1] = new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]);
			otFilters[2] = new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [9] );

			otColumns[0] = new nlobjSearchColumn('custrecord_notes');
			otColumns[1] = new nlobjSearchColumn('name');
			otColumns[2] = new nlobjSearchColumn('custrecord_container_lp_no');
			otColumns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			var otsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, otFilters, otColumns);
			if(otsearchresults==null || otsearchresults=='')
			{
				var pickFilters = new Array();
				var pickColumns = new Array();

				pickFilters[0] = new nlobjSearchFilter( 'name', null, 'is', vfullfillord );
				pickFilters[1] = new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]);
				pickFilters[2] = new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [8] );

				pickColumns[0] = new nlobjSearchColumn('custrecord_notes');
				pickColumns[1] = new nlobjSearchColumn('name');
				pickColumns[2] = new nlobjSearchColumn('custrecord_container_lp_no');
				pickColumns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				var psearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, pickFilters, pickColumns);	
				if(psearchresults!=null && psearchresults!='')
				{
					var vNotes = psearchresults[0].getValue('custrecord_notes');
					var vname = psearchresults[0].getValue('name');
					var vid = psearchresults[0].getId();
					Soid=psearchresults[0].getValue('custrecord_ebiz_order_no');
					ContainerLp=psearchresults[0].getValue('custrecord_container_lp_no');

					var ReGeneratefulfilmentOrderline = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', parseFloat(vid));					
					ReGeneratefulfilmentOrderline.setFieldValue('custrecord_device_upload_flag', 'T');
					nlapiSubmitRecord(ReGeneratefulfilmentOrderline, false, true);
				}
			}
		}
		//update the field with empty value in shipmanifest record 
		//so that it trigers the event and the item is confirmed to nswms
		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/		
		if( (Soid!=null && Soid !='')&& (ContainerLp!=null && ContainerLp !='') )
		{
			nlapiLogExecution('ERROR', 'Soid ', Soid);
			nlapiLogExecution('ERROR', 'ContainerLp', ContainerLp);
			/* Up to here */ 
			var filtershipManifest=new Array();
			filtershipManifest.push(new nlobjSearchFilter('custrecord_ship_contlp',null,'is',ContainerLp));
			filtershipManifest.push(new nlobjSearchFilter('custrecord_ship_orderno',null,'is',Soid));

			var searchrecShipManisfest=nlapiSearchRecord('customrecord_ship_manifest',null,filtershipManifest,null);
			if(searchrecShipManisfest!=null&&searchrecShipManisfest!="")
			{
				nlapiLogExecution('ERROR','searchrecShipManisfest.length',searchrecShipManisfest.length);
				/* The below line is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/				
				var recId=searchrecShipManisfest[0].getId();
				nlapiLogExecution('ERROR','recId',recId);
				nlapiSubmitField('customrecord_ship_manifest', recId, 'custrecord_ship_custom5',"");
				nlapiLogExecution('ERROR', 'updateShipManifest ', 'Success');
			}
		}

		response.sendRedirect('TASKLINK', 'LIST_TRAN_ITEMSHIP', null, null,
				null);
	}
}

