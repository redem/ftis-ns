/***************************************************************************
	  		   eBizNET Solutions Inc 
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_RMACheckinSKU.js,v $
 *     	   $Revision: 1.3.2.7.4.7.2.7.2.2 $
 *     	   $Date: 2015/06/29 15:33:41 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * DESCRIPTION
 * REVISION HISTORY
 * $Log: ebiz_RF_RMACheckinSKU.js,v $
 * Revision 1.3.2.7.4.7.2.7.2.2  2015/06/29 15:33:41  skreddy
 * Case# 201413275
 * PMM Prod issue fix
 *
 * Revision 1.3.2.7.4.7.2.7.2.1  2015/02/27 16:05:57  sponnaganti
 * Case# 201411626
 * PMM SB Issue fix
 *
 * Revision 1.3.2.7.4.7.2.7  2014/06/23 06:44:49  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.3.2.7.4.7.2.6  2014/06/13 06:16:53  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.3.2.7.4.7.2.5  2014/06/06 07:58:21  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.3.2.7.4.7.2.4  2014/05/30 00:26:51  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.2.7.4.7.2.3  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.3.2.7.4.7.2.2  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.2.7.4.7.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.3.2.7.4.7  2013/02/15 14:59:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.3.2.7.4.6  2013/02/01 15:22:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.3.2.7.4.5  2013/01/24 03:30:55  kavitha
 * CASE201112/CR201113/LOG201121
 * CASE201213525 - Factory Mation - RF RMA issue fixes
 *
 * Revision 1.3.2.7.4.4  2012/12/20 23:51:51  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3.2.7.4.3  2012/09/27 10:55:14  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.3.2.7.4.2  2012/09/26 12:48:52  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.3.2.7.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.3.2.7  2012/04/30 10:00:49  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal'
 *
 * Revision 1.3.2.6  2012/04/27 13:13:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Invaid SKU
 *
 * Revision 1.3.2.5  2012/04/10 23:07:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Navigation to previous screen issue is resolved.
 *
 * Revision 1.3.2.4  2012/04/09 14:35:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * DUplicate line items in Transactions
 *
 * Revision 1.3.2.3  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.2.2  2012/03/09 14:17:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing  BaseUomQty to the query string.
 *
 * Revision 1.3.2.1  2012/02/22 12:42:23  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.3  2011/12/27 15:36:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.2  2011/09/24 15:51:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.1  2011/09/22 08:37:21  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 *
 *****************************************************************************/

function RMACheckInSKU(request, response){
	if (request.getMethod() == 'GET') 
	{
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		//Variable Declaration
		var html = '';

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		nlapiLogExecution('DEBUG', 'Into Request', getPONo);

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var whLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);
		var whCompany= request.getParameter('custparam_company');


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "RMA";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULOS";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";			

		}
		else
		{
			st0 = "";
			st1 = "RMA";
			st2 = "ENTER/SCAN ITEM";
			st3 = "SEND";
			st4 = "PREV";

		}



		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_sku'); 
		html = "<html><head><title>" + st0 + "</title>" ;
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends      
		//html = html + " document.getElementById('enteritem').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_checkin_sku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " : <label>" + getPONo + "</label>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnWhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;  return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var ActualBeginTime;
		var getItem = request.getParameter('enteritem');
		var getActualBeginTime = request.getParameter('hdnActualBeginTime'); 
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');
		var whLocation  = request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);
		var whCompany  = request.getParameter('hdnWhCompany');		
		// This variable is to hold the SKU entered.
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st5,st6;
		if( getLanguage == 'es_ES')
		{
			st5 = "ART&#205;CULO INV&#193;LIDO ";
			st6 = "NO ITEM DIEMENSIONS";

		}
		else
		{
			st5 = "INVALID ITEM";
			st6 = "NO ITEM DIEMENSIONS";

		}


		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('enteritem');
		POarray["custparam_error"] = st5;

		POarray["custparam_screenno"] = '2R';
		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');	
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_company"] = request.getParameter('hdnWhCompany');


		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;	//TimeArray[1];

		//  try {
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent != 'F7') {
			//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
			//            if (optedEvent != '' && optedEvent != null) {
			nlapiLogExecution('DEBUG', 'custparam_poitem', POarray["custparam_poitem"]);

			if (POarray["custparam_poitem"] != "") {

				var actItemid = validateCHECKINSKU(POarray["custparam_poitem"],  POarray["custparam_whlocation"], POarray["custparam_company"]);
				nlapiLogExecution('DEBUG', 'After validateSKU',actItemid);

				if((actItemid==null) || (actItemid==""))
				{
					nlapiLogExecution('DEBUG', 'After validateSKU return Value::',actItemid);
				}

				//var itemSearch = nlapiSearchRecord('item', null, new nlobjSearchFilter('itemid', null, 'is', POarray["custparam_poitem"]));

				//var itemSearch = nlapiSearchRecord('item', null, new nlobjSearchFilter('itemid', null, 'is', actItemid));

				/*var filters = new Array();
        		filters.push(new nlobjSearchFilter('itemid', null, 'is', actItemid));
        		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

        		var columns = new Array();
        		columns[0] = new nlobjSearchColumn('itemid');
        		columns[0].setSort(false);

        		var itemSearch = nlapiSearchRecord('item', null, filters, columns);*/

				// Changed On 30/4/12 by Suman

				var filteritem=new Array();
				filteritem.push(new nlobjSearchFilter('nameinternal', null, 'is', actItemid));
				filteritem.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
				var columnsitem = new Array();
				columnsitem[0] = new nlobjSearchColumn('itemid');
				columnsitem[0].setSort(false);
				var itemSearch = nlapiSearchRecord('item', null,filteritem,columnsitem);


				// End of Changes as On 30/4/12

				if (itemSearch == null ||itemSearch == '') 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
				}
				else 
				{
					nlapiLogExecution('DEBUG', 'Length of Item Search', itemSearch.length);
					for (var s = 0; s < itemSearch.length; s++) 
					{
						var ItemSearchedId = itemSearch[s].getId();
						nlapiLogExecution('DEBUG', 'Fetched Item ID', ItemSearchedId);
						//var ItemDescription = itemSearch[s].getFieldValue('purchasedescription');
					}
					var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot','purchasedescription'];
					var columns = nlapiLookupField('item', ItemSearchedId, fields);
					var ItemType = columns.recordType;	
					var serialInflg="F";		
					serialInflg = columns.custitem_ebizserialin;
					var batchflag="F";
					batchflag= columns.custitem_ebizbatchlot;
					//var ItemRec = nlapiLoadRecord(ItemType, ItemSearchedId);
					//var ItemRec = nlapiLoadRecord('inventoryitem', ItemSearchedId);
					//var ItemDescription = ItemRec.getFieldValue('purchasedescription');
					var ItemDescription = columns.purchasedescription;
					nlapiLogExecution('DEBUG', 'POarray["custparam_poitem"]', POarray["custparam_poitem"]);


					var Transactionlinedetails=eBiz_RF_GetTransactionlinedetails(POarray["custparam_poid"], ItemSearchedId);

					var trantype='';


					var POfilters = new Array();
					POfilters[0] = new nlobjSearchFilter('mainline', null, 'is', 'F');
					POfilters[1] = new nlobjSearchFilter('tranid', null, 'is', POarray["custparam_poid"]);
					POfilters[2] = new nlobjSearchFilter('item', null, 'is', ItemSearchedId);

					var POColumns = new Array();
					POColumns[0] = new nlobjSearchColumn('item');
					POColumns[1] = new nlobjSearchColumn('line');
					POColumns[2] = new nlobjSearchColumn('custcol_ebiz_po_cube');

					// Added by Phani on 03-25-2011
					POColumns[3] = new nlobjSearchColumn('location');
					POColumns[4] = new nlobjSearchColumn('quantity');

					var searchresults = nlapiSearchRecord('returnauthorization', null, POfilters, POColumns);


					var vIndex=0;
					var vBoolFount=false;
					if(Transactionlinedetails!=null && Transactionlinedetails!='')
					{
						/* The below code is merged from tek production account on 03-04-2013 by Radhika as part of Standard bundle*/
						/*	for(var i=0; i<Transactionlinedetails.length; i++)
						{
							var tranlineno=Transactionlinedetails[i].getValue('custrecord_orderlinedetails_orderline_no');
							var trancheckinqty=Transactionlinedetails[i].getValue('custrecord_orderlinedetails_checkin_qty');

							for(var j=0; j<searchresults.length; j++)
							{
								var polineno=searchresults[j].getValue('line');
								var pocheckinqty=searchresults[j].getValue('quantity');
								if(polineno==tranlineno)
								{
									if(parseFloat(pocheckinqty)>parseFloat(trancheckinqty))
									{
										if(!vBoolFount){
											vIndex=j;
											nlapiLogExecution('DEBUG', 'POInternalId', searchresults[j].getValue('line'));
											vBoolFount=true;
										}
										//goto Skiphere;
									}
								}
								else
								{
									if(!vBoolFount){
										vIndex=j;
										vBoolFount=true;
									}
									//goto Skiphere;
								}	

							}
						}*/
						/* Up to here */ 
						if(searchresults!=null && searchresults!='' && searchresults.length>0)
						{
							nlapiLogExecution('DEBUG','POarray["custparam_pointernalid"]',request.getParameter("custparam_pointernalid"));
							if(request.getParameter("custparam_pointernalid")!=null && request.getParameter("custparam_pointernalid")!="")
							{
								trantype = nlapiLookupField('transaction', request.getParameter("custparam_pointernalid"), 'recordType');
								POarray["custparam_trantype"] = trantype;
							}

							nlapiLogExecution('DEBUG','trantype',trantype);
							for(var j=0;vBoolFount==false && j<searchresults.length; j++)
							{
								var vTempFlag=false;
								var polineno=searchresults[j].getValue('line');
								nlapiLogExecution('DEBUG', 'polinenoStart',polineno);
								for(var k=0; k<Transactionlinedetails.length;k++)
								{
									var tranlineno=Transactionlinedetails[k].getValue('custrecord_orderlinedetails_orderline_no');
									nlapiLogExecution('DEBUG', 'tranlineno', tranlineno);
									nlapiLogExecution('DEBUG', 'polineno', polineno);
									if(tranlineno==polineno)
									{
										var pocheckinqty=searchresults[j].getValue('quantity');
										//201413275
										if(trantype=='returnauthorization' && pocheckinqty!= null && pocheckinqty!= '' && parseFloat(pocheckinqty)<0)
											pocheckinqty=parseFloat(pocheckinqty)*-1;
										var trancheckinqty=Transactionlinedetails[k].getValue('custrecord_orderlinedetails_checkin_qty');
										if(parseFloat(pocheckinqty)>parseFloat(trancheckinqty))
										{
											if(!vBoolFount){
												vIndex=j;
												nlapiLogExecution('DEBUG', 'POInternalId', searchresults[j].getValue('line'));
												vBoolFount=true;
											}
											//goto Skiphere;
										}
										vTempFlag=true;
										break; 
									}									 	
								}	
								if(vTempFlag==false)
								{
									vIndex=j;
									vBoolFount=true;
									break;
								}	

							}
						}

					}

					//	var ItemInternalId = searchresults[0].getValue(POColumns[0]);
					//	 nlapiLogExecution('DEBUG', 'ItemInternalId ', ItemInternalId);

					if (searchresults != null && searchresults.length > 0) {
						var POInternalId = searchresults[vIndex].getId();
						nlapiLogExecution('DEBUG', 'POInternalId', POInternalId);

						POarray["custparam_lineno"] = searchresults[vIndex].getValue(POColumns[1]);
						POarray["custparam_fetcheditemid"] = ItemSearchedId;
						POarray["custparam_pointernalid"] = POInternalId;
						POarray["custparam_fetcheditemname"] = getItem;
						POarray["custparam_itemdescription"] = ItemDescription;

						nlapiLogExecution('DEBUG', 'Line #', POarray["custparam_lineno"]);
						nlapiLogExecution('DEBUG', 'Fetched Item id', POarray["custparam_fetcheditemid"]);
						nlapiLogExecution('DEBUG', 'POInternalId', POarray["custparam_pointernalid"]);

						// Added by Phani on 03-25-2011
						POarray["custparam_whlocation"] = searchresults[vIndex].getValue(POColumns[3]);
						nlapiLogExecution('DEBUG', 'POarray["custparam_whlocation"]', POarray["custparam_whlocation"]);
						nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

//						POarray["custparam_itemcube"] = searchresults[0].getValue('custcol_ebiz_po_cube');
//						nlapiLogExecution('DEBUG', 'Item Cube', POarray["custparam_itemcube"]);


						nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
						nlapiLogExecution('DEBUG', 'custrecord_ebizitemdims ', getItem);
						nlapiLogExecution('DEBUG', 'custrecord_ebizitemdims ', ItemSearchedId);

						var ItemDimensionsFilter = new Array();
						ItemDimensionsFilter[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ItemSearchedId);
						ItemDimensionsFilter[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

						var ItemDimensionsColumns = new Array();
						ItemDimensionsColumns[0] = new nlobjSearchColumn('custrecord_ebizcube');
						ItemDimensionsColumns[1] = new nlobjSearchColumn('custrecord_ebizqty');

						var ItemDimensionsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, ItemDimensionsFilter, ItemDimensionsColumns);

						if (ItemDimensionsSearchResults != null && ItemDimensionsSearchResults.length > 0) 
						{
							POarray["custparam_itemcube"] = ItemDimensionsSearchResults[0].getValue('custrecord_ebizcube');
							//Added on 9Mar By suman.
							POarray["custparam_baseuomqty"]=ItemDimensionsSearchResults[0].getValue('custrecord_ebizqty');
							nlapiLogExecution('DEBUG', 'Item Cube', POarray["custparam_itemcube"]);
							nlapiLogExecution('DEBUG', 'BASEUomQty', POarray["custparam_baseuomqty"]);

							if (POarray["custparam_poitem"] != "") {
								response.sendRedirect('SUITELET', 'customscript_rf_rma_checkinqty', 'customdeploy_rf_rmacheckin_qty_di', false, POarray);
							}
							else {
								//	if the 'Send' button is clicked without any option value entered,
								//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
								nlapiLogExecution('DEBUG', 'No SKU value is entered ', POarray["custparam_poitem"]);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
							nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
						}
						else
						{   
							POarray["custparam_error"] = st6;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'ItemDimensionsSearchResults ', 'No Item Dimensions For this item');

						}
					}
					else {
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
					}
				}
			}                
			else {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
			}
		}
		else {
			response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin', 'customdeploy_rf_rma_checkin_di', false, POarray);
		}
		/*    } 
        catch (e) {
            response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
            nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
        }*/
	} //end of first if condition


} //end of function.


function validateCHECKINSKU(itemNo,location,company)
{
	nlapiLogExecution('DEBUG', 'validateSKU:Start',itemNo);

	nlapiLogExecution('DEBUG', 'Calling After Method calling Location ',location);

	var actItem="";

	/*var invitemfilters = new Array();
	invitemfilters[0] = new nlobjSearchFilter('name',null, 'is',itemNo);
	var invLocCol = new Array();
	invLocCol[0] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);*/

	// Changed On 30/4/12 by Suman
	var invitemfilters = new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemNo));
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	var invLocCol = new Array();
	invLocCol[0] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item', null,invitemfilters,invLocCol);

	// End of Changes as On 30/4/12


	if(invitemRes!=null)
	{
		actItem=invitemRes[0].getValue('itemid');
	}
	else
	{					
		var invLocfilters = new Array();
		invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));
		//Changed on 30/4/12 by suman
		invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		//End of change as on 30/4/12
		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);
		if(invtRes != null)
		{
			actItem=invtRes[0].getValue('itemid');
		}
		else
		{

			nlapiLogExecution('DEBUG', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			skuAliasFilters[0] = new nlobjSearchFilter('name', null, 'is', itemNo);

			//if(location!=null && location!="")					
			skuAliasFilters[1] = new nlobjSearchFilter('custrecord_ebiz_location', null, 'is', location);

			//if(company != null && company != "")
			//	skuAliasFilters[2] = new nlobjSearchFilter('custrecord_ebiz_company', null, 'is', company);

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
				actItem=skuAliasResults[0].getText('custrecord_ebiz_item');

		}			
	}

	return actItem;
}
