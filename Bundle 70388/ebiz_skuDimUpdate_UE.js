/***************************************************************************
��������������������   eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/UserEvents/ebiz_skuDimUpdate_UE.js,v $
 *� $Revision: 1.3.2.2.4.2.4.1.8.1 $
 *� $Date: 2015/09/21 14:02:07 $
 *� $Author: deepshikha $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_skuDimUpdate_UE.js,v $
 *� Revision 1.3.2.2.4.2.4.1.8.1  2015/09/21 14:02:07  deepshikha
 *� 2015.2 issueFix
 *� 201414466
 *�
 *� Revision 1.3.2.2.4.2.4.1  2013/02/27 13:22:58  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Merged from Monobind
 *�
 *� Revision 1.3.2.2.4.2  2012/11/01 14:54:57  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.3.2.2.4.1  2012/10/23 17:08:39  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Merged code form branch.
 *�
 *� Revision 1.3.2.2  2012/06/02 09:53:13  schepuri
 *� CASE201112/CR201113/LOG201121
 *� added new filters related to default item dimensions
 *�
 *� Revision 1.3.2.1  2012/04/05 15:52:30  gkalla
 *� CASE201112/CR201113/LOG201121
 *� For default item dims creation
 *�
 *� Revision 1.4  2012/04/05 15:48:18  gkalla
 *� CASE201112/CR201113/LOG201121
 *� For default item dims creation
 *�
 *� Revision 1.3  2011/12/23 19:14:22  skota
 *� CASE201112/CR201113/LOG201121
 *� commented the line where company id is hard coded
 *�
 *� Revision 1.2  2011/12/01 11:57:20  spendyala
 *� CASE201112/CR201113/LOG201121
 *� default creating sku dimension with EACH and PALLET when ever a new item is created by the user
 *�
 *� Revision 1.1  2011/12/01 06:53:02  spendyala
 *� CASE201112/CR201113/LOG201121
 *� when ever the weight in the item master is changed then this event fires and sets the corresponding weight against the item in the item dimensions.
 *�
 *
 ****************************************************************************/

/**
 * It triggers after page is submitted 
 * @param type
 * @param form
 * @param request
 */
function SkuDimensionsUpdate(type, form, request)
{
	nlapiLogExecution('ERROR','type',type);
	if(type=='edit'||type=='create')
	{
		try{
			var weight=nlapiGetFieldValue('weight');
			nlapiLogExecution('ERROR','Weight',weight);
			if(weight == null || weight == '')
				weight=0;

			//fetching itemfamily and item group from default item dims

			var itemfam=nlapiGetFieldValue('custitem_item_family');
			nlapiLogExecution('ERROR','custitem_item_family',itemfam);

			var itemgrp=nlapiGetFieldValue('custitem_item_group');
			nlapiLogExecution('ERROR','custitem_item_group',itemgrp);

			var filterdefaultdimen=new Array();
			filterdefaultdimen.push(new nlobjSearchFilter('custrecord_ebizdefitemdimsfam', null, 'anyof', ['@NONE@',itemfam]));
			filterdefaultdimen.push(new nlobjSearchFilter('custrecord_ebizdefitemdimsgroup', null, 'anyof', ['@NONE@',itemgrp]));

			var searchresultsdefaultdimen = nlapiSearchRecord('customrecord_ebiznet_default_skudims', null, filterdefaultdimen, null);

			if(searchresultsdefaultdimen == null || searchresultsdefaultdimen == "")
			{
				var id=nlapiGetRecordId();

				var filter1=new Array();
				filter1.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', id ));

				var columns1=new Array();
				columns1.push(new nlobjSearchColumn('custrecord_ebizqty'));
				var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter1, columns1); 
				if(searchresults1 != null && searchresults1  != "" && searchresults1.length>0)
				{
					if(weight!=null && weight!="" && parseFloat(weight)!=0)
					{
						for(var k=0;k<searchresults1.length ; k++)
						{
							var IntId=searchresults1[k].getId();
							var qty=searchresults1[k].getValue('custrecord_ebizqty');
							var newweight=parseFloat(weight)*parseFloat(qty);
							nlapiLogExecution('ERROR','newweight',newweight);
							//code added from Monobind account on 26Feb13 by santosh
							//convert into float
							nlapiSubmitField('customrecord_ebiznet_skudims', IntId, 'custrecord_ebizweight',parseFloat(newweight).toFixed(5));
							//upto here
						}
					}
					/*else					
					{
						for(var k=0;k<searchresults1.length ; k++)
						{
							var IntId=searchresults1[k].getId();
							var qty=searchresults1[k].getValue('custrecord_ebizqty');
							var newweight=0;
							nlapiLogExecution('ERROR','newweight',newweight);
							//code added from Monobind account on 26Feb13 by santosh
							//convert into float
							nlapiSubmitField('customrecord_ebiznet_skudims', IntId, 'custrecord_ebizweight',parseFloat(newweight).toFixed(5));
							//upto here
						}
					}*/
				}
				else
				{
					var location="";
					location=nlapiGetFieldValue('location');
					var itemid=nlapiGetFieldValue('itemid');

					//creating a record for sku dimension with EACH
					var name=itemid+'_EACH';
					var newRecord = nlapiCreateRecord('customrecord_ebiznet_skudims');
					newRecord.setFieldValue('name',name);
					newRecord.setFieldValue('custrecord_ebizsiteskudim',location);
					//newRecord.setFieldValue('custrecord_ebizcompanyskudimension',52);
					newRecord.setFieldValue('custrecord_ebizpackcodeskudim','1');
					newRecord.setFieldValue('custrecord_ebizuomskudim',1);//1=EACH
					newRecord.setFieldValue('custrecord_ebizuomlevelskudim',1);//1is internal id for UOMLevel "Each"
					newRecord.setFieldValue('custrecord_ebizqty',1);
					newRecord.setFieldValue('custrecord_ebizlength',1);
					newRecord.setFieldValue('custrecord_ebizwidth',1);
					newRecord.setFieldValue('custrecord_ebizheight',1);
					newRecord.setFieldValue('custrecord_ebizweight',parseFloat(weight).toFixed(5));
					newRecord.setFieldValue('custrecord_ebizitemdims',id);
					newRecord.setFieldValue('custrecord_ebizbaseuom','T');
					newRecord.setFieldValue('custrecord_ebizcube',1);//1*1*1*=1
					var intid =nlapiSubmitRecord(newRecord);
					nlapiLogExecution('ERROR','IntID',intid);

					//creating a record for sku dimension with PALLET
					var namepallet=itemid+'_PALLET';
					var newRecordPallet = nlapiCreateRecord('customrecord_ebiznet_skudims');
					newRecordPallet.setFieldValue('name',namepallet);
					newRecordPallet.setFieldValue('custrecord_ebizsiteskudim',location);
					//newRecordPallet.setFieldValue('custrecord_ebizcompanyskudimension',52);
					newRecordPallet.setFieldValue('custrecord_ebizpackcodeskudim','1');
					newRecordPallet.setFieldValue('custrecord_ebizuomskudim',3);//3=Pallet UOM
					newRecordPallet.setFieldValue('custrecord_ebizuomlevelskudim',3);//3 is internal id for UOMLevel "Pallet"
					newRecordPallet.setFieldValue('custrecord_ebizqty',999);
					newRecordPallet.setFieldValue('custrecord_ebizlength',48);
					newRecordPallet.setFieldValue('custrecord_ebizwidth',42);
					newRecordPallet.setFieldValue('custrecord_ebizheight',54);
					newRecordPallet.setFieldValue('custrecord_ebizweight',(parseFloat(weight)*999).toFixed(5));
					newRecordPallet.setFieldValue('custrecord_ebizitemdims',id);
					newRecordPallet.setFieldValue('custrecord_ebizcube',108864);//48*42*54=108864
					var intidpallet =nlapiSubmitRecord(newRecordPallet);
					nlapiLogExecution('ERROR','IntIDPallet',intidpallet);
				}
			}
		}
		catch(exeception)
		{
			nlapiLogExecution('ERROR','Exeception',exeception);	
		}
	}
}







