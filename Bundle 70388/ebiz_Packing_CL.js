/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/Attic/ebiz_Packing_CL.js,v $
 *     	   $Revision: 1.1.2.5.4.5.4.41.2.6 $
 *     	   $Date: 2015/11/28 18:00:11 $
 *     	   $Author: rrpulicherla $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Packing_CL.js,v $
 * Revision 1.1.2.5.4.5.4.41.2.6  2015/11/28 18:00:11  rrpulicherla
 * Lovely skin issue fixes
 *
 * Revision 1.1.2.5.4.5.4.41.2.5  2015/11/05 07:50:08  deepshikha
 * 2015.2 issue fixes
 * 201415366
 *
 * Revision 1.1.2.5.4.5.4.41.2.4  2015/10/28 17:01:44  grao
 * Lovely SkinIssue Fixes 201415254
 *
 * Revision 1.1.2.5.4.5.4.41.2.3  2015/10/20 15:37:12  nneelam
 * case# 201415141
 *
 * Revision 1.1.2.5.4.5.4.41.2.2  2015/10/13 15:42:58  deepshikha
 * 2015.2 issues
 * 201414659
 *
 * Revision 1.1.2.5.4.5.4.41.2.1  2015/09/23 14:58:10  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.2.5.4.5.4.41  2015/08/26 06:17:20  rrpulicherla
 * Case#201413343
 *
 * Revision 1.1.2.5.4.5.4.40  2015/08/26 06:04:32  rrpulicherla
 * Case#201413343
 *
 * Revision 1.1.2.5.4.5.4.39  2015/08/12 09:29:16  grao
 * 2015.2   issue fixes  201413962
 *
 * Revision 1.1.2.5.4.5.4.38  2015/08/07 10:19:08  schepuri
 * case# 201413894
 *
 * Revision 1.1.2.5.4.5.4.37  2015/06/12 15:48:35  grao
 * SB issue fixes 201413011
 *
 * Revision 1.1.2.5.4.5.4.36  2015/05/14 07:15:18  schepuri
 * case# 201412734
 *
 * Revision 1.1.2.5.4.5.4.35  2015/05/07 14:39:14  skreddy
 * Case# 201412597
 * Packlist Printing after Packing completed
 *
 * Revision 1.1.2.5.4.5.4.34  2015/04/28 15:39:57  skreddy
 * Case# 201412440
 * Standard bundle  issue fix
 *
 * Revision 1.1.2.5.4.5.4.33  2015/04/21 15:54:20  skreddy
 * Case# 201412440
 * standard bundle issue fix
 *
 * Revision 1.1.2.5.4.5.4.32  2014/10/17 13:20:48  skavuri
 * Case# 201410586 Std bundle Issue fixed
 *
 * Revision 1.1.2.5.4.5.4.31  2014/09/25 15:59:02  sponnaganti
 * Case# 201410510
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.5.4.5.4.30  2014/09/19 11:21:56  skavuri
 * case #201410354 CT issue fixed
 *
 * Revision 1.1.2.5.4.5.4.29  2014/09/15 15:51:27  skavuri
 * Case# 201410334 CT issue fixed
 *
 * Revision 1.1.2.5.4.5.4.28  2014/07/30 15:23:32  skavuri
 * Case# 20149772 SB Issue Fixed
 *
 * Revision 1.1.2.5.4.5.4.27  2014/07/25 06:52:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149502
 *
 * Revision 1.1.2.5.4.5.4.26  2014/06/10 13:16:25  rmukkera
 * Case # 20148832
 * VRA functionality added
 *
 * Revision 1.1.2.5.4.5.4.25  2014/05/26 15:01:46  skreddy
 * case #20148428
 * Sonic SB issue fix
 *
 * Revision 1.1.2.5.4.5.4.24  2014/05/26 14:34:22  grao
 * Case#: 20148534  Sonic Sb issue fixes
 *
 * Revision 1.1.2.5.4.5.4.23  2014/05/22 15:38:41  sponnaganti
 * case# 20148242 20148493
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.5.4.5.4.22  2014/05/15 15:49:03  sponnaganti
 * Case# 20148411
 * Standard Bundle Issue fix
 *
 * Revision 1.1.2.5.4.5.4.21  2014/05/01 10:12:27  skreddy
 * case # 20148228
 * Nucourse prod issue fix
 *
 * Revision 1.1.2.5.4.5.4.20  2014/04/24 13:20:18  grao
 * Case#: 20148150 Sonic Sb issue fixes
 *
 * Revision 1.1.2.5.4.5.4.19  2014/03/18 07:23:28  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to variable declaration
 *
 * Revision 1.1.2.5.4.5.4.18  2014/03/12 06:23:58  nneelam
 * case#  20127433
 * Standard Bundle Issue Fix.
 *
 * Revision 1.1.2.5.4.5.4.17  2014/03/11 06:58:25  nneelam
 * case#  20127590, 20127433�
 * std bundle.
 *
 * Revision 1.1.2.5.4.5.4.16  2014/03/10 16:06:32  skavuri
 * Case# 20127590 issue fixed
 *
 * Revision 1.1.2.5.4.5.4.15  2014/03/07 16:07:32  skavuri
 * Case# 20127588 issue fixed
 *
 * Revision 1.1.2.5.4.5.4.14  2014/03/06 15:20:54  skavuri
 * Case # 20127542(System  allows to pack the qty as more than the picked qty and system is not displaying any validation message.)  Fixed
 *
 * Revision 1.1.2.5.4.5.4.13  2014/01/17 23:25:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20126869
 *
 * Revision 1.1.2.5.4.5.4.12  2014/01/06 07:59:22  schepuri
 * 20126325
 *
 * Revision 1.1.2.5.4.5.4.11  2013/12/16 14:49:23  rmukkera
 * Case # 20126103
 *
 * Revision 1.1.2.5.4.5.4.10  2013/10/22 15:37:24  rmukkera
 * Case # 20124405�
 *
 * Revision 1.1.2.5.4.5.4.9  2013/09/11 15:26:28  nneelam
 * Case#.20124349��
 * Packing Screen Scan Quantity  Issue Fix..
 *
 * Revision 1.1.2.5.4.5.4.8  2013/09/06 14:54:51  rmukkera
 * Case# 20124281
 *
 * Revision 1.1.2.5.4.5.4.7  2013/08/27 13:24:38  grao
 * Issue Fixes Related to the 20124093 for Cesium Prod.
 *
 * Revision 1.1.2.5.4.5.4.6  2013/08/14 14:41:25  nneelam
 * Case# 20123882
 * Standard Bundle Issue Fixed..
 *
 * Revision 1.1.2.5.4.5.4.5  2013/07/15 14:26:38  rrpulicherla
 * case# 20123425
 * GFT Issue fixes
 *
 * Revision 1.1.2.5.4.5.4.4  2013/04/04 14:32:22  schepuri
 * alert for ship complete issue fix
 *
 * Revision 1.1.2.5.4.5.4.3  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.1.2.5.4.5.4.2  2013/03/19 11:46:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.5.4.5.4.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.1.2.5.4.5  2012/12/14 15:47:43  dtummala
 * LOG2012392
 * In packing if user scan invalid item we need to show Messaqge.
 *
 * Revision 1.1.2.5.4.3  2012/12/12 10:11:55  dtummala
 * LOG2012392
 * In packing Show message after all items scan modifications for DJN
 *
 * Revision 1.1.2.5.4.2  2012/12/11 10:39:24  dtummala
 * LOG2012392
 * Packing screen modifications for DJN
 *
 * Revision 1.1.2.5  2012/07/31 10:55:52  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue related to Auto Assign LP
 *
 * Revision 1.1.2.4  2012/06/15 15:45:01  gkalla
 * CASE201112/CR201113/LOG201121
 * Packing
 *
 * Revision 1.1.2.3  2012/06/14 18:15:04  gkalla
 * CASE201112/CR201113/LOG201121
 * GUI Packing
 *
 * Revision 1.1.2.2  2012/06/14 17:43:22  gkalla
 * CASE201112/CR201113/LOG201121
 * GUI Packing
 *
 * Revision 1.1.2.1  2012/06/14 17:36:24  gkalla
 * CASE201112/CR201113/LOG201121
 * GUI Packing
 *  
 *
 * 
 *****************************************************************************/



/**
 * @returns {Boolean}
 */ 

var IsfromOnchange='F';

var TotalPickQty=0;
var TotalPackQty=0;

function OnPalletClose(type,name)
{
	//alert('nlapiGetFieldValue(custpage_hiddenfieldselectpage)' + nlapiGetFieldValue('custpage_hiddenfieldselectpage'));
	/*alert('type' + type);
	return false;*/
	//Case # 20149772 starts (if condition removing, 'custpage_hiddenfieldselectpage' field not exist in suitelet script)
	//if(nlapiGetFieldValue('custpage_hiddenfieldselectpage') =='F')
	//{	
	// Case # 20149772 ends
	if(IsfromOnchange=='T')
	{
		IsfromOnchange='F';
		//case# 20123882 start
		//return false;
		//case# 20123882 end
	}

	var IsAuto=nlapiGetFieldValue('custpage_lpcreate');

//	alert('IsAuto ' + IsAuto);

	var ScanItem=nlapiGetFieldValue('custpage_scanitem');
	if(ScanItem == null || ScanItem =='')
	{		
		//case# 201410510 starts (For displaying alert message when carton weight is having characters)
		var weight= nlapiGetFieldValue('custpage_weight');

		//if( weight==null || weight=='' || isNaN(weight) )
		if(isNaN(weight))
		{
			alert('Please enter valid Weight');
			nlapiSetFieldValue('custpage_weight','');
			return false;
		}
		//case# 201410510 ends
		var NewContLP=nlapiGetFieldValue('custpage_contlp');

		//case # 20127590

		var lpExists='N';
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', NewContLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) {
			nlapiLogExecution('ERROR', 'LP FOUND');

			lpExists = 'Y';
		}

		if(lpExists =='N')
		{	
			var LPReturnValue = ebiznet_LPRange_CL_withLPType(NewContLP.replace(/\s+$/,""), '2','2',siteid);//'2'UserDefiend,'2'PICK
			if(LPReturnValue==false && IsAuto != 'AUTO' && IsAuto != '')// case# 201413894
			{

				if(LPReturnValue==false)
				{	
					alert('Invalid LP Range');
					return false;
				}
			}
		}
		/*	else
		{
			alert('Entered Closed LP');
			return false;
		}*/


		var soid=nlapiGetFieldValue('custpage_ebizso');
		var trantype = nlapiLookupField('transaction', soid, 'recordType');
		  //alert('trantype '+ trantype);
		var OrderType = nlapiGetFieldValue('custpage_ordertype');
	    //alert('order type '+ OrderType);
	    if(OrderType!=null && OrderType!='')
	    	{
	    
	    if(OrderType!=trantype)
	    	{
	    	alert('Please select the correct order type');
	    	return false;
	    	}
	    	}




		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/		
		var soid=nlapiGetFieldValue('custpage_ebizso');

		//case # 20124405�start
		try
		{
			var trantype = nlapiLookupField('transaction', soid, 'recordType');
			//var sorec= nlapiLoadRecord('salesorder', soid);
			//var sorec= nlapiLoadRecord(trantype, soid);
			
			

			var filters = new Array();
			filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));	
			filters.push(new nlobjSearchFilter('internalidnumber', null, 'equalto', soid));
			filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
			filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
			if(trantype=="transferorder")
			{
				filters.push(new nlobjSearchFilter('quantitycommitted', null, 'greaterthan', 0));
			}

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('internalid');	
			columns[1] = new nlobjSearchColumn('line');
			columns[2] = new nlobjSearchColumn('quantitycommitted');
			columns[3] = new nlobjSearchColumn('quantity');
			columns[4] = new nlobjSearchColumn('item');
			columns[5] = new nlobjSearchColumn('quantityshiprecv');
			columns[6] = new nlobjSearchColumn('entity');	
			columns[7] = new nlobjSearchColumn('status');	
			columns[8] = new nlobjSearchColumn('paymenteventresult');
			columns[9] = new nlobjSearchColumn('daysoverdue');

			var searchresults = new nlapiSearchRecord(trantype, null, filters, columns);

			if(searchresults!=null && searchresults!='')
			{
				/*var paymentstatus = sorec.getFieldValue('paymenteventresult');	
				var customer = sorec.getFieldValue('entity');
				var DaysOverDue = null;//sorec.getFieldValue('daysoverdue');
				var OrdStatus = sorec.getFieldValue('status');*/

				var paymentstatus = searchresults[0].getValue('paymenteventresult');	
				var customer = searchresults[0].getValue('entity');
				var DaysOverDue = '';
				var OrdStatus = searchresults[0].getValue('status');
				var str = 'paymentstatus. = ' + paymentstatus + '<br>';
				str = str+ 'customer. = ' + customer + '<br>';
				str = str+ 'DaysOverDue. = ' + DaysOverDue + '<br>';
				str = str+ 'OrdStatus = ' + OrdStatus + '<br>';
				nlapiLogExecution('ERROR', 'SO Validation Log', str);

				var DaysOverDue=nlapiGetFieldValue('custpage_daysoverdue');
				var ruleValue=nlapiGetFieldValue('custpage_rulevalue');


				if(OrdStatus=='Closed' || OrdStatus=='Cancelled')
				{

					//nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
					alert('Order Closed');
					return false;

				}
				else if(paymentstatus=='HOLD')
				{

					//nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
					alert('Payment Hold');
					return false;
				}
				else if(DaysOverDue != null && DaysOverDue != '' && parseInt(DaysOverDue)>0)
				{

					if(customer != null && customer != '')
					{					
						var filtersLP = new Array(); 
						filtersLP.push(new nlobjSearchFilter('internalid', null, 'is', customer));
						var searchresultscustomer = nlapiSearchRecord('customer', 'customsearch_ebiz_customer_due_days', filtersLP, null);
						if(searchresultscustomer != null && searchresultscustomer != '' && searchresultscustomer.length>0)
						{					
							alert('Credit limit exceeded');
							return false;		
						}							
					}

				}	
				else if(ruleValue!=null && ruleValue!='' && ruleValue!='null' && ruleValue =='HOLD')
				{	
					if(customer != null && customer != '')
					{	
						var filtersLP = new Array(); 
						filtersLP.push(new nlobjSearchFilter('internalid', null, 'is', customer));
						var searchresultsoverdue = nlapiSearchRecord('customer', 'customsearch_ebiz_customer_overdue', filtersLP, null);
						if(searchresultsoverdue != null && searchresultsoverdue != '' && searchresultsoverdue.length>0)
						{
							alert('Credit limit exceeded');
							return false;		
						}	 
					}
				}

			}
		}
		catch(exp)
		{
			nlapiLogExecution('Error', 'Exception in fnCreateFo', exp);
		}
		//case # 20124405�end

		//alert('soid:' + soid);
		var commtdqty = 0;
		var orderqty = 0;
		var fulfildQty = 0;
		var kititemTypesku = '';
		/* Up to here */ 

//		alert(flag);
		//alert(NewContLP);
		if((NewContLP==null || NewContLP ==''))
		{

			alert('Please enter value for: Carton#');
			return false;


		}




		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/
//		var trantype = nlapiLookupField('transaction', soid, 'recordType');
//
//		var filters = new Array();
//		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));	
//		filters.push(new nlobjSearchFilter('internalidnumber', null, 'equalto', soid));
//		filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
//		filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
//		if(trantype=="transferorder")
//		{
//			filters.push(new nlobjSearchFilter('quantitycommitted', null, 'greaterthan', 0));
//		}
//
//		var columns = new Array();
//		columns[0] = new nlobjSearchColumn('internalid');	
//		columns[1] = new nlobjSearchColumn('line');
//		columns[2] = new nlobjSearchColumn('quantitycommitted');
//		columns[3] = new nlobjSearchColumn('quantity');
//		columns[4] = new nlobjSearchColumn('item');
//		columns[5] = new nlobjSearchColumn('quantityshiprecv');
//
//		var searchresults = new nlapiSearchRecord(trantype, null, filters, columns);
		/* Up to here */ 

		var siteid='';
		var lineCnt = nlapiGetLineItemCount('custpage_items');
		var vLineChecked=false;
		var vLineqtyEqual=false;
		var extraqtyenter=false; //  case # 20127542
		for (var s = 1; s <= lineCnt; s++) {
			//alert('s:' + s);
			var vPackQty1 = nlapiGetLineItemValue('custpage_items','custpage_qty',s);
			var vPickQty1= nlapiGetLineItemValue('custpage_items','custpage_pickqty',s);
			var linenumber = nlapiGetLineItemValue('custpage_items','custpage_ordlineno',s);	
			var parentsku = nlapiGetLineItemValue('custpage_items','custpage_parentsku',s);	
			var vfono=nlapiGetLineItemValue('custpage_items', 'custpage_ebizcntrlno',s);

//			alert('linenumber:' + linenumber);
//			alert('parentsku:' + parentsku);


			if(vPackQty1 != null && vPackQty1 != '' && parseFloat(vPackQty1)>0)		 
			{
				vLineChecked=true;
			}
			if(parseFloat(vPackQty1)<0 || isNaN(vPackQty1))
			{
				alert('Please enter vaild Pack Quantity');
				return false;
			}
			//alert('vPickQty1  :' + vPickQty1);
			//alert('vPackQty1  :' + vPackQty1);

			if(vPickQty1 != vPackQty1)
			{	
				//alert('if');
				var shipcomplete='N';
				var filtersfo = new Array();

				filtersfo.push(new nlobjSearchFilter('internalid', null, 'is', vfono));				
				filtersfo.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', linenumber));

				var columnsfo = new Array();
				columnsfo[0] = new nlobjSearchColumn('custrecord_shipcomplete');

				var searchresultsfo = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersfo, columnsfo);
				if(searchresultsfo!=null)
					shipcomplete=searchresultsfo[0].getValue('custrecord_shipcomplete');

				if(shipcomplete=='T')
				{

					alert('This SO/TO has created with Ship Complete set ON');
					return false;
				}

			}	

			if(parseFloat(vPackQty1) < parseFloat(vPickQty1))		 
			{
				vLineqtyEqual=true;
			}
			// Case # 20127542 starts
			if(parseFloat(vPackQty1) > parseFloat(vPickQty1))		 
			{
				extraqtyenter=true;
			}
			// Case # 20127542 ends

		}
		if(vLineqtyEqual == true && extraqtyenter !=true)
		{
			//case # 20127433
			if(!(confirm('Are you sure you want to proceed ? Pack qty is less than pick qty')) )
				return false;
			/*	else
				return false;*/
		}
		// Case # 20127542 starts
		if(extraqtyenter == true)
		{
			alert('More Than Picked qty is not allowed.');
			return false;
		}
		// Case # 20127542 ends
		if(vLineChecked == false)
		{
			alert('No items scanned,please scan items to pack.');
			return false;
		}
		else if(trantype =="salesorder")
		{
			/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/
			//Case# 201410586 starts
			//nlapiSetFieldValue('custpage_lpcreate','AUTO'); 	
			//Case# 201410586 ends

			//alert('searchresults:' + searchresults.length);

			for (var t = 0; searchresults!=null && t<searchresults.length; t++) {

				var solineno = searchresults[t].getValue('line');
				commtdqty=parseFloat(searchresults[t].getValue('quantitycommitted'));
				fulfildQty=parseFloat(searchresults[t].getValue('quantityshiprecv'));
				var vPackQty2=0;
				var otparentsku = '';

				if(fulfildQty==null || fulfildQty=='' || isNaN(fulfildQty))
				{
					fulfildQty=0;
				}
				if(commtdqty==null || commtdqty=='' || isNaN(commtdqty))
				{
					commtdqty=0;
				}

				var totalcommtdqty=parseFloat(commtdqty)+parseFloat(fulfildQty);

				//alert('lineCnt:' + lineCnt);

				for (var x = 1; x <= lineCnt; x++) {

					var otlinenumber = nlapiGetLineItemValue('custpage_items','custpage_ordlineno',x);						
					//alert('solineno:' + solineno);
					//alert('otlinenumber:' + otlinenumber);
					if(solineno==otlinenumber)
					{
						otparentsku = nlapiGetLineItemValue('custpage_items','custpage_parentsku',x);	
						vPackQty2=parseFloat(vPackQty2)+parseFloat(nlapiGetLineItemValue('custpage_items','custpage_qty',x));	

//						alert('fulfildQty:' + fulfildQty);
//						alert('commtdqty:' + commtdqty);
//						alert('totalcommtdqty:' + totalcommtdqty);
//						alert('vPackQty2:' + vPackQty2);

						if((parseFloat(fulfildQty)+parseFloat(vPackQty2))>parseFloat(totalcommtdqty))
						{
							//alert('otparentsku:' + otparentsku);

							if(otparentsku!=null && otparentsku!='')
							{
								kititemTypesku = nlapiLookupField('item', otparentsku, 'recordType');
							}
							//alert('kititemTypesku:' + kititemTypesku);
							if(kititemTypesku!='kititem')
							{
								alert('Excess pack qty entered');
								nlapiSetLineItemValue('custpage_items','custpage_qty',x,0);
								nlapiSetFieldValue('custpage_scanitem','');
								nlapiSetFieldValue('custpage_scanqty','');
								document.getElementById('custpage_scanitem').focus();
								return false;
							}
						}
					}
				}

			}
			/* Up to here */ 

			return true;
		}
		else
		{
			return true;
		}


	}
	else
		return false;
	PrintPacklist();
	//Case20149772 starts
	// }
	//else
	//return true;
	// Case20149772 ends
}

function backtogeneratesearch()
{ 	 
	var printername=nlapiGetFieldValue('custpage_ordprintername');
	var tempwarehouse=nlapiGetFieldValue('custpage_tempwarehousesecond');
	var salesorder=nlapiGetFieldValue('custpage_tempsalesorder');
	if(printername==null)
	{
		printername='';

	}
	var PackingURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_packing', 'customdeploy_ebiz_packing_di');
	var ctx = nlapiGetContext();
	/*if (ctx.getEnvironment() == 'PRODUCTION') {
		PackingURL = 'https://system.netsuite.com' + PackingURL+'&printerName='+tempwarehouse+'&salesorder='+salesorder;;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			PackingURL = 'https://system.sandbox.netsuite.com' + PackingURL+'&printerName='+tempwarehouse+'&salesorder='+salesorder;;				
		}*/
	//window.open(WavePDFURL);

	window.location.href = PackingURL;
}

function PrintPacklist1()
{ 	 

	var htmlstring=nlapiGetFieldValue('custpage_temphtmlstring');

	var imgurl=nlapiGetFieldValue('custpage_tempimgurl');	
	var prtContent = document.getElementById("ifmcontentstoprint");
	WinPrint.document.write(prtContent.innerHTML);
	WinPrint.document.close();
	WinPrint.focus();
	WinPrint.print();
	WinPrint.close();
//	pri.document.write(content.innerHTML);
//	pri.document.close();
//	pri.focus();
//	pri.print();




}

function PrintPacklist()
{ 	 
	var htmlstring=nlapiGetFieldValue('custpage_temphtmlstring');
	var contlp=nlapiGetFieldValue('custpage_tempnewcontlp');

	var imgurl=nlapiGetFieldValue('custpage_tempimgurl');	

	//if((htmlstring!=null && htmlstring!='')&&(imgurl!=null && imgurl!=''))
	//{

	//nlapiLogExecution('ERROR', 'into Printreport ebizwaveno',eBizWaveNo);
	var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_packlistprint', 'customdeploy_deploy_packslipprint');
	nlapiLogExecution('ERROR', 'Wave PDF URL',WavePDFURL);					
	WavePDFURL = WavePDFURL + '&custparam_ebiz_containerlp='+ contlp;
	//nlapiLogExecution('ERROR', 'WavePDFURL',WavePDFURL);
	//alert(WavePDFURL);
	var printWindow=window.open(WavePDFURL);

	//var printContent = document.getElementById('totalhtml');
	//printWindow.document.write(printContent.innerHTML);
	//printWindow.document.close();
	printWindow.focus();
	printWindow.print();
	//printWindow.close();
	//printWin.document.write(WavePDFURL);
	//printWin.focus();
	//printWin.print();	
	//printWin.close();
	//}







//	var htmlstring=nlapiGetFieldValue('custpage_temphtmlstring');

//	var imgurl=nlapiGetFieldValue('custpage_tempimgurl');	

//	if((htmlstring!=null && htmlstring!='')&&(imgurl!=null && imgurl!=''))
//	{
//	var printWin = window.open('','_blank');
//	printWin.document.write("<html>  <boby>");
//	printWin.document.write(htmlstring);

//	printWin.document.write("<div style='width:100%;'>");
//	printWin.document.write("<img id='img' src='" + imgurl.src + "'/>");
//	printWin.document.write("</div>");

//	printWin.focus();
//	printWin.print();
//	printWin.close();
//	}


	//printWin.onunload = print(printWin);
	//printWin.close();


}

function print(printWin)
{
	printWin.print()
}

function show_image(src, width, height, alt) {
	var img = document.createElement("img");
	img.src = src;
	img.width = width;
	img.height = height;
	img.alt = alt;

	// This next line will just add it to the <body> tag
	document.body.appendChild(img);
}


function backtogeneratesearchNew()
{ 	 
	var PackingURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_packingnew', 'customdeploy_ebiz_packingnew_di');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		PackingURL = 'https://system.netsuite.com' + PackingURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			PackingURL = 'https://system.sandbox.netsuite.com' + PackingURL;				
		}*/
	//window.open(WavePDFURL);

	window.location.href = PackingURL;
}

function setDefaultValue(type)
{
	IsfromOnchange='F';
	var vPackType=nlapiGetFieldValue('custpage_packtype','bb');
	LoadQuantity();

	if(vPackType != null && document.getElementById('custpage_scanqty') != null){
		if(vPackType=='bb')
		{
			document.getElementById('custpage_scanqty').disabled = "";
			//nlapiSetFieldValue('custpage_scanqty','');
		}

		else
		{
			document.getElementById('custpage_scanqty').disabled = "disabled";
		}	
	}
}
var calculatedtotwt=0;
var calculateditemweight=0;
function onChange(type,name)
{

	//alert('repeat ' + nlapiGetFieldValue('custpage_repeat'));

	// start of calculate Pack Quantity for message showing after all items scan

	//case # 20124349 Start

	var ordId=nlapiGetFieldValue('custpage_ordhead');	
	if(name=='custpage_scanqty'){
		var tempscanqty=nlapiGetFieldValue('custpage_scanqty');
		if(tempscanqty=='0')
		{
			alert('Scan Quantity should be greater than zero');
			return false;
		}	

	}
	//case # 20124349 End

	if(name=='custpage_qty')
	{

		var lineCnt = nlapiGetLineItemCount('custpage_items');	
		TotalPackQty=0;
		var totnew=nlapiGetFieldValue('custpage_weight');
		for(var p=1;p<=lineCnt;p++)
		{
			var OldPackQty= nlapiGetLineItemValue('custpage_items','custpage_qty',p);
			var itemid=nlapiGetLineItemValue('custpage_items','custpage_ebizskuno',p);
			var hiddenqty=nlapiGetLineItemValue('custpage_items','custpage_hiddenqty',p);
			var TotalWeight=nlapiGetLineItemValue('custpage_items','custpage_totalweight',p);

			if(OldPackQty=='')
			{
				OldPackQty=0;

			}

			//alert('OldPackQty'+OldPackQty);
			TotalPackQty=parseInt(TotalPackQty)+parseInt(OldPackQty);		
			//alert('TotalPackQty'+TotalPackQty);
			// case# 201412734
			if(OldPackQty!=null && OldPackQty!='0.0' && OldPackQty!=hiddenqty)
			{

				totnew = parseFloat(totnew) + parseFloat(TotalWeight);

				nlapiSetFieldValue('custpage_weight',parseFloat(totnew).toFixed(5));
				nlapiSetLineItemValue('custpage_items','custpage_hiddenqty',p,OldPackQty);

//				var filter = new Array();
//				filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
//				filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
//				filter.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));	

//				var column = new Array();
//				column[0] = new nlobjSearchColumn('custrecord_ebizweight') ;

//				searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
////				var calculatedtotwtnew=0;
//				if(searchRec != null && searchRec != '')
//				{

//				//var newpackqty= nlapiGetLineItemValue('custpage_items','custpage_qty',p);
//				//alert('newpackqty'+newpackqty);
//				var itemdimwt = searchRec[0].getValue('custrecord_ebizweight');
//				var calculatedwt = parseFloat(itemdimwt) *  parseFloat(OldPackQty);



//				}
			}

		}



	}
	// End of calculate Pack Quantity for message showing after all items scan
//	case no 20126325
	if(name == 'custpage_weight')
	{
		var weight= nlapiGetFieldValue('custpage_weight');

		//if( weight==null || weight=='' || isNaN(weight) )
		if(isNaN(weight) )
		{
			alert('Please enter valid Weight');
			return false;
		}
	}

	var Itemexist=false;
	var vPackType=nlapiGetFieldValue('custpage_packtype','bb');
	var item=nlapiGetFieldValue('custpage_scanitem');

	if(vPackType != null && document.getElementById('custpage_scanqty') != null){
		if(vPackType=='bb')
		{


			document.getElementById('custpage_scanqty').disabled = "";			

		}
		else
		{
			//document.getElementById('custpage_scanqty').disabled = "disabled";
			var packqty=nlapiGetFieldValue('custpage_scanqty');

			if(packqty!=null && packqty!='')	
			{
				nlapiSetFieldValue('custpage_scanqty','');
			}
			document.getElementById('custpage_scanqty').disabled = "disabled";

			//var test='';
			//nlapiSetFieldValue('custpage_scanqty',test);
			//document.getElementById('custpage_scanqty').clear;




		}
	}

//	alert("item:" + item);
	if(item!=null && item!='' && item!='undefined')
	{
		var vitemimage=nlapiGetFieldValue('custpage_image');
		var vItemArr = validateSKUId(item);
		if(vItemArr == null || vItemArr ==''|| vItemArr =='F')// case# 201415249
		{
			alert('Please enter valid Item');
			return false;
		}
		var vItemType;
		var Itemimage;
		var currItem;
		//nlapiLogExecution('ERROR','vItemArr',vItemArr);
		if(vItemArr != null && vItemArr != '' )
		{
			vItemType=vItemArr[0];
			currItem=vItemArr[1];
			Itemimage=vItemArr[2];
		}
		if(Itemimage!=null && Itemimage!='')
		{
			var url;
			/*var ctx = nlapiGetContext();
			nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
			if (ctx.getEnvironment() == 'PRODUCTION') 
			{
				url = 'https://system.netsuite.com';			
			}
			else if (ctx.getEnvironment() == 'SANDBOX') 
			{
				url = 'https://system.sandbox.netsuite.com';				
			}


			var file = url+Itemimage;*/
			var file = Itemimage;
			var html = "<html><body>";
			html=html +"<img src='" + file + "'></img>";
			html = html + "</body></html>";

			nlapiSetFieldValue('custpage_image',html);

		}

		var ordId=nlapiGetFieldValue('custpage_ordhead');	 
		if(ordId != null && ordId != '')
		{
			if(name=='custpage_scanitem')
			{
				IsfromOnchange='T';
				if(vPackType=='bb')
				{	
					document.getElementById('custpage_scanqty').focus();
					//return false;
				}
				else
				{
					//AddPackNew(type,name);
					//document.getElementById('custpage_addpack').focus();
					//document.getElementById('custpage_scanitem').focus();
					//alert('Hi2');
					var vItemInfo;
					if(vPackType =='bi')
					{	
						if(vItemType=='F')
						{
							//vItemInfo=getSKUCubeAndWeight(currItem,'');
							//alert('vItemInfo' + vItemInfo);
							if(vItemInfo != null && vItemInfo != '')
								vPackQty=vItemInfo[1];
							else
								vPackQty=1;
							//alert('vPackQty'+vPackQty);
						}	
						else
						{
							
							vItemInfo=eBiz_RF_GetItemCubeForItemAlias(currItem,item);
							if(vItemInfo != null && vItemInfo != '')
								vPackQty=vItemInfo[1];
						}
					}
					//alert(vPackQty);
					//alert('vItemType' + vItemType);
					//alert(currItem);
					if(currItem != null && currItem != '')
					{

						var lineCnt = nlapiGetLineItemCount('custpage_items');
						//alert('lineCnt' + lineCnt);
						//alert('Scanned Item id ' + currItem);
						var vRemQty=vPackQty;
						for(var p=1;p<=lineCnt && parseFloat(vRemQty)>0 ;p++)
						{

							var OldPackQty= nlapiGetLineItemValue('custpage_items','custpage_qty',p);
							if(OldPackQty==null||OldPackQty=="")
								OldPackQty=0;
							//alert('OldPackQty' + OldPackQty);
							var OldPickQty= nlapiGetLineItemValue('custpage_items','custpage_pickqty',p);
							//alert('OldPickQty' + OldPickQty);
							var EbizSKU= nlapiGetLineItemValue('custpage_items','custpage_ebizskuno',p);
							//alert('EbizSKU' + EbizSKU);
							if(EbizSKU==currItem && parseFloat(OldPickQty) > parseFloat(OldPackQty))
							{
								var vQtyNeedtoPack=parseFloat(OldPackQty)+ parseFloat(vRemQty);
								//alert('vQtyNeedtoPack' + vQtyNeedtoPack);
								//alert('vRemQty' + vRemQty);
								
								if(parseFloat(vQtyNeedtoPack)>parseFloat(OldPickQty))
								{
									vRemQty=vQtyNeedtoPack-parseFloat(OldPickQty);
									alert('Excess ' + vRemQty + ' pack qty entered');
									return false;
								}	
								var RemQtyPack=parseFloat(OldPickQty) - parseFloat(OldPackQty);
								//alert('RemQtyPack' + RemQtyPack);
								//alert('vRemQty' + vRemQty);
								if(RemQtyPack != null && RemQtyPack != '' && parseFloat(RemQtyPack)< parseFloat(vRemQty))
								{

									vRemQty=parseFloat(vRemQty)-parseInt(RemQtyPack);
									//Case # 20126103 Start
									if(vRemQty != null && vRemQty != '' && parseFloat(vRemQty)>0)	
										nlapiSetLineItemValue('custpage_items','custpage_qty',p,parseFloat(OldPackQty) + parseFloat(RemQtyPack));
									nlapiSetFieldValue('custpage_scanitem','');
									//Case # 20126103 End
									return false;
								}	
								else
								{
									//Case # 20126103 Start
									if(vRemQty != null && vRemQty != '' && parseFloat(vRemQty)>0)									
										nlapiSetLineItemValue('custpage_items','custpage_qty',p,parseFloat(OldPackQty) + parseFloat(vRemQty));
									nlapiSetFieldValue('custpage_scanitem','');
									document.getElementById('custpage_scanitem').focus();
									
									
									//alert('vRemQty' + vRemQty);
									//case# 201412734
									//alert('currItem'+currItem);
									var filter = new Array();
									filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', currItem));
									filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
									filter.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));	

									var column = new Array();
									column[0] = new nlobjSearchColumn('custrecord_ebizweight') ;

									searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

									if(searchRec != null && searchRec != '')
									{
										var itemdimwt = searchRec[0].getValue('custrecord_ebizweight');
									//	alert('itemdimwt'+itemdimwt);
										var calculatedwt = parseFloat(itemdimwt) *  1;//itemdimensionwt*packqty

										if(isNaN(calculatedwt))
										{
											calculatedwt = '0.1';
																						
										 }
										//var calculatedtotwt = nlapiGetLineItemValue('custpage_items','custpage_weight',p);
										
										var calculatedtotwt=nlapiGetFieldValue('custpage_weight');
										
									//	alert('calculatedtotwt before'+calculatedtotwt);
										calculatedtotwt = parseFloat(calculatedtotwt) + parseFloat(calculatedwt);
									 //    alert('calculatedtotwt'+calculatedtotwt);						
										
									
										nlapiSetFieldValue('custpage_weight',parseFloat(calculatedtotwt).toFixed(4));
										
										
										

									}
									// Start of if all scan is completed then we need to show message
									//var comapreQty=parseInt(OldPackQty) + parseInt(vRemQty);
									TotalPackQty=parseInt(TotalPackQty) + 1;								
									if(TotalPickQty==TotalPackQty)
									{
										nlapiSetFieldValue('custpage_message',' <h1 style="font-size: 14px; color: 993300; background-color:#C8C8C8; align:center;">All items scanned, proceed to get weight and close box.</h1>');
									}
									// End of if all scan is completed then we need to show message

									vRemQty=0;return false;
								}	

							}
							if(EbizSKU==currItem)
								Itemexist=true;

						}


						if(Itemexist==false)
						{
							alert('Invalid Item');
							nlapiSetFieldValue('custpage_scanitem','');
							nlapiSetFieldValue('custpage_scanqty','');
							document.getElementById('custpage_scanitem').focus();
							return false;
						}	
						if(vRemQty != null && vRemQty != '' && parseFloat(vRemQty)>0)
						{


							alert('Excess ' + vRemQty + ' pack qty entered');
							nlapiSetFieldValue('custpage_scanitem','');
							nlapiSetFieldValue('custpage_scanqty','');
							document.getElementById('custpage_scanitem').focus();
							return false;
						}	


					}
					else
					{
						alert('Contact your lead/supervisor.');
						nlapiSetFieldValue('custpage_scanitem','');
						document.getElementById('custpage_scanitem').disabled = "disabled";
						document.getElementById('custpage_scanitem').disabled = "disabled";
						//document.getElementById('custpage_scanitem').focus();
						document.getElementById('tbl_submitter').style.display= "none";	
						document.getElementById('tbl_secondarysubmitter').style.display= "none";
						document.getElementById('tbl_custpage_autoassignlp').style.display= "none";	
						document.getElementById('tbl_secondarycustpage_autoassignlp').style.display= "none";
						document.getElementById('tbl_custpage_generatesearch').style.display= "none";	
						document.getElementById('tbl_secondarycustpage_generatesearch').style.display= "none";
						document.getElementById('tbl_custpage_addpack').style.display= "none";	
						document.getElementById('tbl_secondarycustpage_addpack').style.display= "none";
						return false;
					}	
					nlapiSetFieldValue('custpage_scanitem','');
					nlapiSetFieldValue('custpage_scanqty','');
					nlapiSetFieldValue('custpage_repeat','1');
				}	

			}
			if(name=='custpage_scanqty')
			{

				IsfromOnchange='T';	
				if(vPackType=='bb')
				{
					//alert('vPackType1 ' + vPackType);
					IsfromOnchange='T';	
					if(vPackType=='bb')
					{

						AddPackNew(type,name);
						var packqty=nlapiGetFieldValue('custpage_scanqty');
						var calculatedtotwt=nlapiGetFieldValue('custpage_weight');



						if(packqty!=null && packqty!='')	
						{
							nlapiSetFieldValue('custpage_scanqty','');
						}
						document.getElementById('custpage_scanitem').focus();

					}	

				}	

			}

		}
		//nlapiSetFieldValue('custpage_scanitem','');
		//nlapiSetFieldValue('custpage_scanqty','');
		//nlapiSetFieldValue('custpage_repeat','1');
	}
	document.getElementById('custpage_scanitem').focus();
	return false;
	//document.getElementById('custpage_scanitem').focus();
}




function AddPackNew(type,name)
{



	var ordId=nlapiGetFieldValue('custpage_ordhead');
	var vItem=nlapiGetFieldValue('custpage_scanitem');
	var vPackQty=nlapiGetFieldValue('custpage_scanqty');
	var vPackType=nlapiGetFieldValue('custpage_packtype','bb');
	var Itemexist=false;
	var currItem="";
	if(vItem == null || vItem == '')
	{
		//alert("Please scan Item");
		document.getElementById('custpage_scanitem').focus();
		return false;
	}//Case # 20126103 Start
	else if(vPackQty == null || vPackQty ==''  || vPackQty =='0' || parseFloat(vPackQty)==0)
	{
		vPackQty=1;
	}//Case # 20126103 End	
	else if(vPackQty != null && vPackQty != '' && parseFloat(vPackQty)<0)
	{
		//alert("Pack Qty should not be less than zero");
		nlapiSetFieldValue('custpage_scanqty',0);
		document.getElementById('custpage_scanqty').focus();
		return false;
	}
	//alert('ordId' + ordId);
	if(ordId != null && ordId != '')
	{



		//alert('vItemCount' + vItemCount);
		//alert('TaskCount' + TaskCount);

		var vItemArr = validateSKUId(vItem);
		//alert('vItemArr' + vItemArr);
		var vItemType='F';
		if(vItemArr != null && vItemArr != '' && vItemArr.length>1)
		{
			vItemType=vItemArr[0];
			currItem=vItemArr[1];
		}
		var vItemInfo;
		//alert('vPackType'+vPackType);
		if(vPackType =='bi')
		{	
			if(vItemType=='F')
			{
				//vItemInfo=getSKUCubeAndWeight(currItem,'');
				//alert('vItemInfo' + vItemInfo);
				if(vItemInfo != null && vItemInfo != '')
					vPackQty=vItemInfo[1];
				else
					vPackQty=1;
				//alert('vPackQty'+vPackQty);
			}	
			else
			{
				vItemInfo=eBiz_RF_GetItemCubeForItemAlias(currItem,vItem);
				if(vItemInfo != null && vItemInfo != '')
					vPackQty=vItemInfo[1];
			}
		}
		//alert(vPackQty);
		//alert('vItemType' + vItemType);
		//alert(currItem);
		if(currItem != null && currItem != '')
		{
			var lineCnt = nlapiGetLineItemCount('custpage_items');
			//alert('lineCnt' + lineCnt);
			//alert('Scanned Item id ' + currItem);
			var vRemQty=vPackQty;
			for(var p=1;p<=lineCnt && parseFloat(vRemQty)>0 ;p++)
			{
				var OldPackQty= nlapiGetLineItemValue('custpage_items','custpage_qty',p);

				if(OldPackQty==''|| OldPackQty==null)
				{
					OldPackQty=0;
				}

				//alert('OldPackQty' + OldPackQty);
				var OldPickQty= nlapiGetLineItemValue('custpage_items','custpage_pickqty',p);
				//alert('OldPickQty' + OldPickQty);
				var EbizSKU= nlapiGetLineItemValue('custpage_items','custpage_ebizskuno',p);
				//alert('EbizSKU' + EbizSKU);
				if(EbizSKU==currItem && parseFloat(OldPickQty) > parseFloat(OldPackQty))
				{
					var RemQtyPack=parseFloat(OldPickQty) - parseFloat(OldPackQty);
					if(RemQtyPack != null && RemQtyPack != '' && parseFloat(RemQtyPack)< parseFloat(vRemQty))
					{
						//var remainingPackQty=parseInt(vRemQty)-parseInt(RemQtyPack);


						nlapiSetFieldValue('custpage_scanitem','');
						vRemQty=parseFloat(vRemQty)-parseFloat(RemQtyPack);
						// Start of if all scan is completed then we need to show message
						//Case # 20126103 Start
						nlapiSetLineItemValue('custpage_items','custpage_qty',p,parseFloat(OldPackQty) + parseFloat(RemQtyPack));
						//Case # 20126103 End
						var comapreQty=parseInt(OldPackQty) + parseInt(RemQtyPack);
						TotalPackQty=parseInt(TotalPackQty) + parseInt(comapreQty);

						if((vRemQty != null && vRemQty != '') && (parseFloat(vRemQty)>0))
						{
							//Case # 20124281�Start
							//nlapiSetLineItemValue('custpage_items','custpage_qty',p,'');
							//Case # 20124281�End
						}

						var TotalWeight=nlapiGetLineItemValue('custpage_items','custpage_totalweight',p);

						//var calculatedwt = parseFloat(TotalWeight) * parseFloat(OldPickQty);//itemdimensionwt*packqty

						calculatedtotwt = parseFloat(calculatedtotwt) + parseFloat(TotalWeight);


						nlapiSetFieldValue('custpage_weight',parseFloat(calculatedtotwt).toFixed(5));


						if(TotalPickQty==TotalPackQty)
						{
							nlapiSetFieldValue('custpage_message',' <h1 style="font-size: 14px; color: 993300; background-color:#C8C8C8; align:center;">All items scanned, proceed to get weight and close box.</h1>');
						}

						// End of if all scan is completed then we need to show message
						//return false;
					}	
					else
					{

						//Case # 20126103 Start
						if(vRemQty != null && vRemQty != '' && parseFloat(vRemQty)>0)
							nlapiSetLineItemValue('custpage_items','custpage_qty',p,parseFloat(OldPackQty) + parseFloat(vRemQty));
						//Case # 20126103 End
						nlapiSetFieldValue('custpage_scanitem','');
						var TotalWeight=nlapiGetLineItemValue('custpage_items','custpage_totalweight',p);

						//var calculatedwt = parseFloat(TotalWeight) * parseFloat(OldPickQty);//itemdimensionwt*packqty

						calculatedtotwt = parseFloat(calculatedtotwt) + parseFloat(TotalWeight);


						nlapiSetFieldValue('custpage_weight',parseFloat(calculatedtotwt).toFixed(5));


						// Start of if all scan is completed then we need to show message						
						var comapreQty=parseInt(OldPackQty) + parseInt(vRemQty);
						TotalPackQty=parseInt(TotalPackQty) + parseInt(vRemQty);	
						//alert(TotalPackQty);
						if(TotalPickQty==TotalPackQty)
						{
							nlapiSetFieldValue('custpage_message',' <h1 style="font-size: 14px; color: 993300; background-color:#C8C8C8; align:center;">All items scanned, proceed to get weight and close box.</h1>');
						}
						// End of if all scan is completed then we need to show message
						vRemQty=0;return false;
					}	
				}
				if(EbizSKU==currItem)
					Itemexist=true;

			}
			if(Itemexist==false)
			{
				alert('Invalid Item');
				nlapiSetFieldValue('custpage_scanitem','');
				nlapiSetFieldValue('custpage_scanqty','');
				document.getElementById('custpage_scanitem').focus();
				return false;
			}	
			if(vRemQty != null && vRemQty != '' && parseFloat(vRemQty)>0)
			{
				alert('Excess ' + vRemQty + ' pack qty entered');				
				nlapiSetFieldValue('custpage_scanitem','');
				//nlapiSetFieldValue('custpage_scanqty','');
				document.getElementById('custpage_scanitem').focus();
				return false;
			}	

			//nlapiSetLineItemValue('custpage_items','custpage_qty',1,vPackQty);
			/*var filters=new Array();
				filters.push( new nlobjSearchFilter('name', null, 'is', ordId));


				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
				filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', vItem));//Task Type - PICK

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('name');
				columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				columns[4] = new nlobjSearchColumn('custrecord_comp_id');
				columns[5] = new nlobjSearchColumn('custrecord_site_id');
				columns[6] = new nlobjSearchColumn('custrecord_container');
				columns[7] = new nlobjSearchColumn('custrecord_total_weight');
				columns[8] = new nlobjSearchColumn('custrecord_sku');
				columns[9] = new nlobjSearchColumn('custrecord_act_qty');
				columns[10] = new nlobjSearchColumn('custrecord_wms_location');


				columns[0].setSort();
				columns[1].setSort();
				columns[10].setSort();
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
				if(searchresults != null && searchresults != '')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_internalid',searchresults[0].getId(),false);
					nlapiSetCurrentLineItemValue('custpage_items','custpage_skuin',currItem,false);
					nlapiSetCurrentLineItemValue('custpage_items','custpage_qty',searchresults[0].getValue('custrecord_act_qty'),false);
					nlapiSetCurrentLineItemValue('custpage_items','custpage_actqty',searchresults[0].getValue('custrecord_act_qty'),false);
				}*/	
		}
		else
		{
			alert('Invalid Item');
			nlapiSetFieldValue('custpage_scanitem','');
			document.getElementById('custpage_scanitem').focus();
			return false;
		}	


		/*alert("Name: " + name);
		alert("type: " +type);*/
	}
	//alert('Hi');
	nlapiSetFieldValue('custpage_scanitem','');
	nlapiSetFieldValue('custpage_scanqty','');
	nlapiSetFieldValue('custpage_repeat','1');

	document.getElementById('custpage_scanitem').focus();
	//return false;
}

function AutoAssignLP()
{
	var ordId=nlapiGetFieldValue('custpage_ordhead');
	var cntLP=nlapiGetFieldValue('custpage_contlp');

	var lineCnt = nlapiGetLineItemCount('custpage_items');
	var vLineChecked=false;
	for (var s = 1; s <= lineCnt; s++) {

		var vsiteid= nlapiGetLineItemValue('custpage_items','custpage_siteid',s);

	}

	var newContainerLpNo=GetMaxLPNo('1', '2',vsiteid);
	nlapiSetFieldValue('custpage_contlp',newContainerLpNo);
	nlapiSetFieldValue('custpage_lpcreate','AUTO');

	var autolp=nlapiGetFieldValue('custpage_lpcreate');
//	alert('autolp ' + autolp);
//	nlapiLogExecution('ERROR','AutoAssignLPAutolp', autolp);
}

function validateSKUId(itemNo)
{
	nlapiLogExecution('Error', 'Into validateSKU',itemNo);

	var actItem=new Array();


	//alert('validateSKUId ' + itemNo);

	var invitemfilters = new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal',null, 'is',itemNo));
	// Changed On 30/4/12 by Suman
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	var invLocCol = new Array();
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	invLocCol[0] = new nlobjSearchColumn('externalid');
	//invLocCol[1] = new nlobjSearchColumn('custitem_djn_internal_thumbnail');
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	actItem[0]='F';
	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);
	if(invitemRes!=null && invitemRes!='')
	{

		//actItem=invitemRes[0].getValue('externalid');

		actItem[1]=invitemRes[0].getId();
		actItem[2]=invitemRes[0].getText('custitem_djn_internal_thumbnail');
	}
	else
	{

		nlapiLogExecution('ERROR', 'inSide UPCCODE',itemNo);

		var invLocfilters = new Array();
		invLocfilters[0] = new nlobjSearchFilter('upccode', null, 'is', itemNo);
		invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F')); //Case# 201410334

		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);

		if(invtRes!=null && invtRes!='')
		{
			//alert('In UPC');
			//actItem=invtRes[0].getValue('externalid');
			actItem[1]=invtRes[0].getId();
		}
		else
		{

			nlapiLogExecution('ERROR', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemNo));
			skuAliasFilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
			{

				actItem[0]='T';
				actItem[1]=skuAliasResults[0].getValue('custrecord_ebiz_item');
				/*var Itype = nlapiLookupField('item', actItem, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, actItem);			
				actItem=	ItemRec.getFieldValue('itemid');*/
			}

		}			
	}



	return actItem;
}
function eBiz_RF_GetItemCubeForItemAlias(itemID,pitem){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemCubeForItem', 'Start');

	var BaseUOM=eBiz_RF_GetUOMAgainstItemAlias(itemID,pitem);
	var ItemInfo=new Array();
	var itemCube = 0;
	var BaseUomQty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemID);
	filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', BaseUOM);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var itemDimSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(itemDimSearchResults != null && itemDimSearchResults.length > 0){
		itemCube = itemDimSearchResults[0].getValue('custrecord_ebizcube');
		//code added on 13 feb 2012 by suman
		//To get the baseuom qty .
		BaseUomQty=itemDimSearchResults[0].getValue('custrecord_ebizqty');
		//end of code as of 13 feb 2012.
	}
	ItemInfo.push(itemCube);
	ItemInfo.push(BaseUomQty);
	nlapiLogExecution('ERROR', 'Retrieved Item Cube', 'Item Cube = ' + itemCube);
	nlapiLogExecution('ERROR', 'Retrieved BaseUomQty', 'BaseUomQty = ' + BaseUomQty);
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemCubeForItem', 'End');

	return ItemInfo;
}
function eBiz_RF_GetUOMAgainstItemAlias(itemID,pitem)
{
	nlapiLogExecution('ERROR', 'eBiz_RF_GetUOMAgainstItemAlias', 'Start');

	var baseuom = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_item', null, 'anyof', itemID));
	if(pitem!=null && pitem!='')
	filters.push(new nlobjSearchFilter('name', null, 'is', pitem));
	/*if(location!=null && location!='')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));*/

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_uom');

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, filters, columns);

	if(itemSearchResults != null)
		baseuom = itemSearchResults[0].getValue('custrecord_ebiz_uom');

	var logMsg = 'baseuom = ' + baseuom;
	nlapiLogExecution('ERROR', 'baseuom Retrieved', logMsg);
	nlapiLogExecution('ERROR', 'eBiz_RF_GetUOMAgainstItemAlias', 'End');

	return baseuom;
}
/**
 * 
 * @param skuNo
 * @param uom
 * @returns {dimArray0}
 */
function getSKUCubeAndWeight(skuNo, uom){


	var timestamp1 = new Date();
	var cube = 0;
	var BaseUOMQty = 0;

	var dimArray = new Array();
	if (uom == "")
		uom = 1;



	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', skuNo));
	filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(skuDimsSearchResults != null){
		for (var i = 0; i < skuDimsSearchResults.length; i++) {
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	//dimArray["BaseUOMItemCube"] = parseFloat(cube);
	//dimArray["BaseUOMQty"] = BaseUOMQty;
	dimArray[0] = parseFloat(cube);//BaseUOMItemCube
	dimArray[1] = BaseUOMQty;//BaseUOMQty

	var timestamp2 = new Date();


	return dimArray;
}


function LoadQuantity()
{
	nlapiLogExecution('ERROR', 'Enter to Quantity', 'LoadQuantity');
	var lineCnt = nlapiGetLineItemCount('custpage_items');	

	for(var i=1;i<=lineCnt;i++)
	{
		var OldPickQty= nlapiGetLineItemValue('custpage_items','custpage_pickqty',i);

		TotalPickQty=parseInt(TotalPickQty)+parseInt(OldPickQty);
		//alert(TotalPickQty);
		var OldPackQty= nlapiGetLineItemValue('custpage_items','custpage_qty',i);
		TotalPackQty=parseInt(TotalPackQty)+parseInt(OldPackQty);
	}
}

function eBiz_RF_GetItemForItemIdWithArr(itemId,location){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemForItemIdWithArr', 'Start');

	var itemRecordArr = new Array();

	if(itemId != null && itemId != ""){
		var logMsg = 'ItemId = |' + itemId + '|';
		nlapiLogExecution('ERROR', 'Input Parameters', logMsg);
		nlapiLogExecution('ERROR', 'location', location);
		//alert('itemId'+itemId);
		//alert('location'+location);
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', itemId));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(location!=null && location!='')
			filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('externalid');
		columns[0].setSort(true);
		//alert('itemId11'+itemId);
		var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);
		if(itemSearchResults != null && itemSearchResults.length > 0){
			for(var i=0;i<itemSearchResults.length;i++)
			{	
				var itemInternalId = itemSearchResults[i].getId();
				nlapiLogExecution('ERROR', 'Internal Id', itemInternalId);
				itemRecordArr.push(itemInternalId);
				//var itemcolumns = nlapiLookupField('item', itemInternalId, [ 'recordType']);
				//var itemType = itemcolumns.recordType;
				//alert('itemInternalId'+itemInternalId);
				//nlapiLogExecution('ERROR', 'itemType', itemType);

				//itemRecord = nlapiLoadRecord(itemType, itemInternalId);
			}
		}
		else
		{
			nlapiLogExecution('ERROR', 'Inside UPC', location);

			var invLocfilters = new Array();
			var invLocCol = new Array();
			invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemId));
			invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

			var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);
			if(invtRes != null && invtRes.length > 0){
				for(var i=0;i<invtRes.length;i++)
				{	
					var itemInternalId = invtRes[i].getId();
					nlapiLogExecution('ERROR', 'Internal Id', itemInternalId);
					itemRecordArr.push(itemInternalId);			
				}
			}
		}
	}

	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemForItemId', 'End');
	return itemRecordArr;
}
function eBiz_RF_GetSOLineDetailsForItemArr(poID, itemIDArr,trantype){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetPOLineDetailsForItemArr', 'Start');
	var logMsg = 'PO ID = ' + poID + '<br>';
	logMsg = logMsg + 'Item ID = ' + itemIDArr;
	nlapiLogExecution('ERROR', 'Input Parameters', logMsg);


	//alert('Order Id ' + poID);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('tranid', null, 'is', poID));
	filters.push(new nlobjSearchFilter('item', null, 'anyof', itemIDArr));
	filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('item');
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custcol_ebiz_po_cube');
	columns[3] = new nlobjSearchColumn('location');
	columns[4] = new nlobjSearchColumn('quantity');
	columns[5] = new nlobjSearchColumn('quantityshiprecv');

	var SOLineSearchResults = nlapiSearchRecord(trantype, null, filters, columns);

	if(SOLineSearchResults != null && SOLineSearchResults.length > 0)
		nlapiLogExecution('ERROR', 'No. of PO Lines Retrieved', SOLineSearchResults.length);

	nlapiLogExecution('ERROR', 'eBiz_RF_GetPOLineDetailsForItemArr', 'End');

	return SOLineSearchResults;
}
