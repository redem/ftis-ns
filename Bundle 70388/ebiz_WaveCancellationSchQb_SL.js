/***************************************************************************
eBizNET Solutions Inc
 ****************************************************************************
 *
 ** $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_WaveCancellationSchQb_SL.js,v $
 ** $Revision: 1.1.2.1.8.1 $
 ** $Date: 2015/11/27 22:11:26 $
 ** $Author: skreddy $
 ** $Name: t_WMS_2015_2_StdBundle_1_196 $
 *
 * DESCRIPTION
 ** Functionality
 *
 * REVISION HISTORY
 ** $Log: ebiz_WaveCancellationSchQb_SL.js,v $
 ** Revision 1.1.2.1.8.1  2015/11/27 22:11:26  skreddy
 ** case# 201415888
 ** dynacraft prod issue fix
 **
 ** Revision 1.1.2.1  2013/04/30 23:33:11  snimmakayala
 ** CASE201112/CR201113/LOG2012392
 ** Prod and UAT issue fixes.
 **
 ** Revision 1.2.2.5  2012/04/03 10:44:46  gkalla
 ** CASE201112/CR201113/LOG201121
 ** To populate all wavenums in dropdown means greater than 1000
 **
 *
 ****************************************************************************/


function WaveCancellationQbSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start of GET',context.getRemainingUsage());

		var form = nlapiCreateForm('Wave Cancellation');

		var selectcriteria = form.addField('custpage_selectcriteria', 'select', 'Criteria').setLayoutType('startrow','none');
		selectcriteria.addSelectOption('WAVE', 'Wave Level');
		selectcriteria.addSelectOption('HEADER_LEVEL', 'Order Level');
		selectcriteria.addSelectOption('DETAIL_LEVEL', 'Order Line Level');

		var waveListField = form.addField('custpage_wavelist', 'select', 'Wave #').setLayoutType('startrow', 'none');
		waveListField.addSelectOption("","");		
		//var openTaskRecordsforWave = getAllOpenTaskRecordsForWaveCancel('HEADER_LEVEL', null, null);	
		// Add information to the selection fields
		//addValuesToFields(null, waveListField, null, openTaskRecordsforWave);
		var searchResult=	getAllOpenTaskRecordsForWaveCancel(form, waveListField,-1);
		for (var j = 0; searchResult != null && j < searchResult.length; j++) {
			var openTaskRecords=searchResult[j];
			for (var i = 0; openTaskRecords != null && i < openTaskRecords.length; i++) {
				var resdo = form.getField('custpage_wavelist').getSelectOptions(openTaskRecords[i].getValue('custrecord_ebiz_wave_no'), 'is');
				if (resdo != null) {
					if (resdo.length > 0) {
						continue;
					}
				}
				waveListField.addSelectOption(openTaskRecords[i].getValue('custrecord_ebiz_wave_no'), openTaskRecords[i].getValue('custrecord_ebiz_wave_no'));
			}
		}

		var orderField = form.addField('custpage_orderlist', 'select', 'Order #').setLayoutType('startrow', 'none');
		orderField.addSelectOption("","");
		var openTaskRecordsfororder = getAllOpenTaskRecordsFororderlevel('HEADER_LEVEL', null,0);
		addValuesToFields(orderField, null, null, openTaskRecordsfororder);
		//var salesOrderList = getAllSalesOrders();

		// Add all sales orders to SO Field
		//addAllSalesOrdersToField(orderField, salesOrderList);

		var orderlineField = form.addField('custpage_orderlinelist', 'select', 'Fullfilment Order #').setLayoutType('startrow', 'none');
		orderlineField.addSelectOption("","");
		var openTaskRecordsfororderline = getAllOpenTaskRecordsFororderlinelevel('HEADER_LEVEL', null, null,0);
		addValuesToFields(null, null, orderlineField, openTaskRecordsfororderline);
		//var salesOrderLineList=getAllSalesOrderslines();
		//AddAllSalesorderlines(orderlineField,salesOrderLineList,form);

		//addValuesToFields(null, orderField, null, openTaskRecords);

		nlapiLogExecution('ERROR','Remaining usage at the end of GET',context.getRemainingUsage());

		var button = form.addSubmitButton('Display');
		form.setScript('customscript_wavecancellation_cl');
		response.writePage(form);
	}
	else
	{
		var waveArray = new Array();
		waveArray["custpage_checkfield"] = request.getParameter('custpage_checkfield');
		waveArray["custpage_selectcriteria"] = request.getParameter('custpage_selectcriteria');
		waveArray["custpage_wavelist"] = request.getParameter('custpage_wavelist');
		waveArray["custpage_orderlist"] = request.getParameter('custpage_orderlist');
		waveArray["custpage_orderlinelist"] = request.getParameter('custpage_orderlinelist');

		response.sendRedirect('SUITELET', 'customscript_wave_cancellationsch','customdeploy_wave_cancellationsch', false, waveArray);

	}
}
function getOpenTaskColumns(levelInd){
	var columns = new Array();
	nlapiLogExecution('ERROR', 'levelInd', levelInd);
	if(levelInd == 'HEADER_LEVEL'){
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_line_no');
		columns[2] = new nlobjSearchColumn('custrecord_tasktype');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[7] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[6].setSort(true); // Sort by wave no.
	} else if(levelInd == 'DETAIL_LEVEL') {
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[6] = new nlobjSearchColumn('custrecord_line_no');
		columns[7] = new nlobjSearchColumn('custrecord_tasktype');
		columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[10] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[11] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
		columns[12] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[13] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[16] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[17] = new nlobjSearchColumn('custrecord_invref_no');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_sku_status');
		columns[20] = new nlobjSearchColumn('custrecord_uom_id');
		columns[21] = new nlobjSearchColumn('custrecord_batch_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebizrule_no');
		columns[23] = new nlobjSearchColumn('custrecord_ebizmethod_no');
		columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
		columns[25] = new nlobjSearchColumn('custrecord_comp_id');
		columns[26] = new nlobjSearchColumn('custrecord_wms_location');						
		columns[27] = new nlobjSearchColumn('custrecord_ebizuser');		
		columns[28] = new nlobjSearchColumn('custrecord_upd_ebiz_user_no');						
		columns[29] = new nlobjSearchColumn('custrecord_taskassignedto');
		columns[12].setSort(true);  // Sort by wave number
	}

	return columns;
}
var searchResultsForWave = new Array();
function getAllOpenTaskRecordsForWaveCancel(form, customerField,maxno){
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsForWaveCancel', 'Start');

	var filters = new Array();
	// Look for valid wave#

	filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty');

	// Look for records with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','11','12','13','26']);
	if(maxno!=-1)
	{
		nlapiLogExecution('ERROR', 'maxno', maxno);
		filters[2]=new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno));
	}

	// Get list of columns to retrieve based on the level of granularity
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[1] = new nlobjSearchColumn('id');
	columns[0].setSort(true);
	columns[1].setSort(true);
	columns[1] = new nlobjSearchColumn('id');

	columns[1].setSort(true);

	// Load all the open task records as per filter criteria
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

//	for (var i = 0; openTaskRecords != null && i < openTaskRecords.length; i++) {
//	var resdo = form.getField('custpage_wavelist').getSelectOptions(openTaskRecords[i].getValue('custrecord_ebiz_wave_no'), 'is');
//	if (resdo != null) {
//	if (resdo.length > 0) {
//	continue;
//	}
//	}
//	customerField.addSelectOption(openTaskRecords[i].getValue('custrecord_ebiz_wave_no'), openTaskRecords[i].getValue('custrecord_ebiz_wave_no'));
//	}
	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{
//		var column = new Array();
//		column[0]=new nlobjSearchColumn('custrecord_ebiz_wave_no');

//		column[0].setSort(true);
		searchResultsForWave.push(openTaskRecords);
		//var waveSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
//		var column = new Array();
//		column[0]=new nlobjSearchColumn('custrecord_ebiz_wave_no');

//		column[0].setSort(true);

//		var waveSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, column);

		var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[1]);		
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue('id');		
		getAllOpenTaskRecordsForWaveCancel(form, customerField,maxno);	

	}
	else
	{
		searchResultsForWave.push(openTaskRecords);
	}
	return searchResultsForWave;
}
var searchResultsForOrder = new Array();
function getAllOpenTaskRecordsFororderlevel(levelInd, waveCancelLevel, maxid){
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlevel', 'Start');

	var filters = new Array();
	// Look for valid wave#
	if((levelInd == 'HEADER_LEVEL') || (levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE')){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty');

	}else if(levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE'){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'equals', waveNo);

	}

	// Look for records with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','11','12','13','26']);
	filters[2]=new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxid));
	// Get list of columns to retrieve based on the level of granularity
	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[1] = new nlobjSearchColumn('id');
	columns[0].setSort(true);
	columns[1].setSort(true);
	// Load all the open task records as per filter criteria
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{
		nlapiLogExecution('ERROR', 'openTaskRecords', openTaskRecords.length);
		searchResultsForOrder.push(openTaskRecords);
		//	var ordSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[1]);		
		getAllOpenTaskRecordsFororderlevel(levelInd, waveCancelLevel, maxno);	
	}
	else
	{
		searchResultsForOrder.push(openTaskRecords);
	}

//	logCountMessage('ERROR', openTaskRecords);

	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlevel', 'End');
	return searchResultsForOrder;
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlevel', 'End');
	//return openTaskRecords;
}
var searchResultsForOrderlinelevel = new Array();
function getAllOpenTaskRecordsFororderlinelevel(levelInd, waveCancelLevel, waveNo,maxid){
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlinelevel', 'Start');

	var filters = new Array();
	// Look for valid wave#
	if((levelInd == 'HEADER_LEVEL') || (levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE')){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty');

	}else if(levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE'){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'equals', waveNo);

	}

	// Look for records with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']);
	if(maxid !=0)
	filters[2] = new nlobjSearchFilter('id', null, 'lessthan', maxid);
	// Get list of columns to retrieve based on the level of granularity
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name',null,'group');
	//columns[1] = new nlobjSearchColumn('id');

	columns[0].setSort(true);
	//columns[1].setSort(true);
	// Load all the open task records as per filter criteria
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{
		nlapiLogExecution('ERROR', 'openTaskRecords', openTaskRecords.length);
		// var ordlineSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		searchResultsForOrderlinelevel.push(openTaskRecords);
		//var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[1]);		
		//getAllOpenTaskRecordsFororderlinelevel(levelInd, waveCancelLevel, waveNo,maxno,vRoleLocation);
	}
	else
	{
		searchResultsForOrderlinelevel.push(openTaskRecords);
	}

//	logCountMessage('ERROR', openTaskRecords);

	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlinelevel', 'End');
	return searchResultsForOrderlinelevel;
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlinelevel', 'End');
	return openTaskRecords;
}
function addValuesToFields(OrderField, waveListField, fulfillOrderList, searchResult){
	nlapiLogExecution('ERROR', 'addValuesToFields', 'Start');
	var ordersAlreadyAddedList = new Array();
	var wavesAlreadyAddedList = new Array();

	if(searchResult != null && searchResult.length > 0){
		for(var j = 0; j < searchResult.length; j++){
			var openTaskRecords=searchResult[j];
			for(var i = 0; i < openTaskRecords.length; i++){
				var currentTask = openTaskRecords[i];
				//var fulfillOrderNo = currentTask.getValue('name');
				var fulfillOrderNo = currentTask.getValue('name',null,'group');
				var waveNo = currentTask.getValue('custrecord_ebiz_wave_no');
				var OrderNo=currentTask.getText('custrecord_ebiz_order_no');
				var ebizOrderNo=currentTask.getValue('custrecord_ebiz_order_no');
				if(fulfillOrderList != null){
					if(!isValueAlreadyAdded(ordersAlreadyAddedList, fulfillOrderNo)){

						fulfillOrderList.addSelectOption(fulfillOrderNo, fulfillOrderNo);
						ordersAlreadyAddedList.push(fulfillOrderNo);
					}
				}
				if(OrderField != null){
					if(!isValueAlreadyAdded(ordersAlreadyAddedList, ebizOrderNo)){

						OrderField.addSelectOption(ebizOrderNo, OrderNo);
						ordersAlreadyAddedList.push(ebizOrderNo);
					}
				}
				if(waveListField != null)
				{
					if(!isValueAlreadyAdded(wavesAlreadyAddedList, waveNo)){

						waveListField.addSelectOption(waveNo, waveNo);
						wavesAlreadyAddedList.push(waveNo);
					}
				}
			}
		}
	}
}
function isValueAlreadyAdded(alreadyAddedList, currentValue){
	var alreadyAdded = false;

	for(var i = 0; i < alreadyAddedList.length; i++){
		if(alreadyAddedList[i] == currentValue){
			alreadyAdded = true;
			break;
		}
	}

	return alreadyAdded;
}