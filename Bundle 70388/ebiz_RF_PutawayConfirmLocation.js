/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ebiz_RF_PutawayConfirmLocation.js,v $
 *     	   $Revision: 1.4.4.2.4.4.2.4 $
 *     	   $Date: 2014/06/13 06:37:58 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PutawayConfirmLocation.js,v $
 * Revision 1.4.4.2.4.4.2.4  2014/06/13 06:37:58  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.4.2.4.4.2.3  2014/05/30 00:26:50  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.2.4.4.2.2  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.4.2.4.4.2.1  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.4.2.4.4  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.4.4.2.4.3  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.4.4.2.4.2  2012/09/26 12:48:01  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.4.4.2.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.4.4.2  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.4.1  2012/02/22 12:37:05  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PutawayConfirmLocation(request, response)
{
    if (request.getMethod() == 'GET') 
	{
      	//	Get the LP#, Quantity, Location 
        //  from the previous screen, which is passed as a parameter	
        var getLPNo = request.getParameter('custparam_lpno');
        var getQuantity = request.getParameter('custparam_quantity');
        var getFetchedLocation = request.getParameter('custparam_location');
        var getFetchedItem = request.getParameter('custparam_item');
        var getFetchedItemDescription = request.getParameter('custparam_itemDescription');
		
        var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
					
			st0 = "";
			st1 = "CONFIRMAR UBICACI&#211;N";
			st2 = "LOC OVERRIDE (Y / N)?";
			st3= "CONT";
			st4 = "ANTERIOR";
			
		}
		else
		{
			st0 = "";
			st1 = "CONFIRM LOCATION";
			st2 = "LOC OVERRIDE (Y/N)?";
			st3= "CONT";
			st4 = "PREV";

		}
        
        
        var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
        var html = "<html><head><title>" + st0 + "</title>";
        html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
        html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
        //html = html + " document.getElementById('enteroverrideoption').focus();";        
        html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
        html = html + "</script>";
        html = html +functionkeyHtml;
		 html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
        html = html + "	<form name='_rf_putaway_lp' method='POST'>";
        html = html + "		<table>";
		html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st1 + ": <label>" + getFetchedLocation + "</label>";
        html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st2;
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'><input name='enteroverrideoption' id='enteroverrideoption' type='text'/>";
        html = html + "				</td>";
        html = html + "			</tr>";		
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st3 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;  return false'/>";
        html = html + "					" + st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "		 </table>";
        html = html + "	</form>";
      //Case# 20148882 (added Focus Functionality for Textbox)
        html = html + "<script type='text/javascript'>document.getElementById('enteroverrideoption').focus();</script>";
        html = html + "</body>";
        html = html + "</html>";
        
        response.write(html);
    }
    else 
	{
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
		
		// This variable is to hold the LP entered.
		var getOverrideOption = request.getParameter('enteroverrideoption');
		nlapiLogExecution('DEBUG', 'getOverrideOption', getOverrideOption);
		
		var getLPNo = request.getParameter('custparam_lpno');
		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);		
		
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', 'L');
		filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('custrecord_lpno');
        
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		
        var POarray = new Array();
    	var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
    	
		
		var st5;
		if( getLanguage == 'es_ES')
		{
			st5 = "UBICACI&#211;N NO V&#193;LIDA";
			
		}
		else
		{

			st5 = "INVALID LOCATION";
			
		}
		
		POarray["custparam_error"] = st5;
		POarray["custparam_screenno"] = '11';
			
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
        // to the previous screen.
        var optedEvent = request.getParameter('cmdPrevious');
        
        if (getOverrideOption == 'N') 
		{
            try 
			{
                if (searchresults != null && searchresults.length > 0) 
				{
                    nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');
                    
                    var getLPId = searchresults[0].getId();
                    nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);
                    
                    // Load a record into a variable from opentask table for the LP# entered
                    var PORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getLPId);
                    
                    POarray["custparam_lpno"] = PORec.getFieldValue('custrecord_lpno');
                    POarray["custparam_quantity"] = PORec.getFieldValue('custrecord_expe_qty');
                    POarray["custparam_location"] = PORec.getFieldValue('custrecord_actbeginloc');
                    POarray["custparam_item"] = PORec.getFieldValue('custrecord_sku');
                    POarray["custparam_itemDescription"] = PORec.getFieldValue('custrecord_skudesc');
                    
                    response.sendRedirect('SUITELET', 'customscript_rf_putaway_location', 'customdeploy_rf_putaway_location_di', false, POarray);
                }
				
	            //	if the previous button 'F7' is clicked, it has to go to the previous screen 
	            //  ie., it has to go to accept PO #.
                if (optedEvent == 'F7') 
				{
                    response.sendRedirect('SUITELET', 'customscript_rf_putaway_qty_exception', 'customdeploy_rf_putaway_qty_exception_di', false, POarray);
                }
                else 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_putaway_location', 'customdeploy_rf_putaway_location_di', false, POarray);
                    nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
                }
            } 
            catch (e) 
			{
                response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
                nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
            }
            nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
        }
    }
}
