/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_serialnumbersmismatch_byitem_SL.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2013/05/10 08:57:15 $
 *     	   $Author: rmukkera $
 *     	   $Name: t_NSWMS_2013_1_3_23 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_serialnumbersmismatch_byitem_SL.js,v $
 * Revision 1.1.2.1  2013/05/10 08:57:15  rmukkera
 * New File
 *
 *****************************************************************************/
function ebiznet_SerialNumbers_SL(request, response){
	if (request.getMethod() == 'GET') {        
		var form = nlapiCreateForm('Serial numbers mismatch Ebiz Vs NS');
		var salesorder = form.addField('custpage_so', 'text', 'SO #');
		var item = form.addField('custpage_item', 'select', "Item", "Item");
		var site = form.addField('custpage_site', 'select', "Location", "Location");
		//salesorder.setMandatory(true);
		var button = form.addSubmitButton('Display');
		response.writePage(form);        
	}
	else {

		var form = nlapiCreateForm('Serial numbers mismatch Ebiz Vs NS');        
		var msg= form.addField('custpage_message', 'inlinehtml', null, null, null).setLayoutType('startrow', 'startcol');
		var soidno = request.getParameter('custpage_so');
		var itemno = request.getParameter('custpage_item');
		var site = request.getParameter('custpage_site');
		var salesorder = form.addField('custpage_so', 'text', 'SO #');
		var itemfield = form.addField('custpage_item', 'select', "Item", "Item");
		var sitefield = form.addField('custpage_site', 'select', "Location", "Location");
		//salesorder.setMandatory(true);
		salesorder.setDefaultValue(soidno);
		itemfield.setDefaultValue(itemno);
		sitefield.setDefaultValue(site);

		var soid = GetSOInternalId(soidno);
		var sublist = form.addSubList("custpage_items", "list", "Items List");
		sublist.addField("custpage_line", "text", "Line#");
		sublist.addField("custpage_itemname", "text", "item").setDisplayType('inline');;
		sublist.addField("custpage_quantity", "text", "Order Qty");
		sublist.addField("custpage_location", "text", "Location");
		sublist.addField("custpage_ebzserialnumbers", "textarea", "Ebiz Serial Numbers");
		sublist.addField("custpage_nsserialnumbers", "textarea", "NS Serial Numbers");
		sublist.addField("custpage_diffserialnumbers", "textarea", "Difference Serial Numbers");
		if((soid != null && soid != ""))
		{
			nlapiLogExecution('ERROR', 'soid',soid);




			var trantype = nlapiLookupField('transaction', soid, 'recordType');
			nlapiLogExecution('ERROR', 'trantype', trantype);

			var transaction=null;
			if(soid!=null && soid!='' && soid!='null')
			{
				if(trantype!='salesorder')
				{
					transaction = nlapiLoadRecord('transferorder', soid);
				}
				else
				{
					transaction = nlapiLoadRecord('salesorder', soid);
				}
			}


			var line, itemvalue, ordqty, location, locationname,itemText;
			var NSserialnumbers = "";
			var diffserialnumbers = "";
			var ebizserialnumbers = ""; 
			if (transaction != null)
			{            	
				nlapiLogExecution('ERROR', 'transaction.getLineItemCount item', transaction.getLineItemCount('item'));
				for (var i = 0; i < transaction.getLineItemCount('item'); i++)  // Even though SO is having one line, search results are giving two records, assumtion is first record might be expense record 
				{     		

					line = transaction.getLineItemValue('item', 'line', i + 1);
					if(request.getParameter('custpage_item')!=null && request.getParameter('custpage_item')!='' && request.getParameter('custpage_item')!='null')
					{
						itemvalue=request.getParameter('custpage_item');
					}
					else
						{
						itemvalue = transaction.getLineItemValue('item', 'item', i + 1);
						}
					
					itemText=transaction.getLineItemText('item', 'item', i + 1);
					ordqty = transaction.getLineItemValue('item', 'quantity', i + 1);
					
					if(request.getParameter('custpage_site')!=null && request.getParameter('custpage_site')!='' && request.getParameter('custpage_site')!='null')
					{
						location=request.getParameter('custpage_site');
						var locRec=nlapiLookupField('location',location,['name'])
						  locationname=locRec.name;
					}
					else
						{
					location = transaction.getLineItemValue('item', 'location', i + 1);	
					locationname = transaction.getLineItemText('item', 'location', i + 1);
						}

					nlapiLogExecution('ERROR', 'ordqty', ordqty);
					nlapiLogExecution('ERROR', 'itemvalue', itemvalue);
					nlapiLogExecution('ERROR', 'location', location);

					var itemTypesku = nlapiLookupField('item', itemvalue, 'recordType');
					nlapiLogExecution('ERROR', 'itemTypesku', itemTypesku);

					if(itemTypesku == "serializedinventoryitem"){


					/*	var filters = new Array();
						filters[0] = new nlobjSearchFilter('internalid',null,'is', itemvalue);
						filters[1] = new nlobjSearchFilter('serialnumberlocation',null,'anyof', location);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('serialnumber');
						var searchresultsitem = nlapiSearchRecord('serializedinventoryitem', null, filters, columns);*/
						//nlapiLogExecution('DEBUG', 'searchresults.length', searchresults.length);
						var searchresultsitem=getNSNumbers(itemvalue,location,-1);
						for (var j = 0; searchresultsitem != null && j < searchresultsitem.length; j++) {		
							var searchresultitem = searchresultsitem[j];
							//serial = searchresult.getValue('serialnumberlocation');
							if(NSserialnumbers == "")
							{
								NSserialnumbers = searchresultitem;//.getValue('serialnumber');
							}
							else
							{
								NSserialnumbers = NSserialnumbers + " , " + searchresultitem;//.getValue('serialnumber');
							}
						}

						if(request.getParameter('custpage_item')!=null && request.getParameter('custpage_item')!='' && request.getParameter('custpage_item')!='null')
						{
							itemvalue=request.getParameter('custpage_item');
							
											
							var itemRec=nlapiLookupField('item',itemvalue,['name'])
							  itemText=itemRec.name;	
							
						}
						if(request.getParameter('custpage_site')!=null && request.getParameter('custpage_site')!='' && request.getParameter('custpage_site')!='null')
						{
							location=request.getParameter('custpage_site');
							var locRec=nlapiLookupField('location',location,['name'])
							  locationname=locRec.name;
						}
						var searchResultArray = new Array();
						searchResultArray = getSerialnumbersforItem(line,itemvalue,location,soid,-1);
						if (searchResultArray !=null)
						{
							//ebizserialnumbers = getSerialNoCSV(searchResultArray);
							var orderListArray=new Array();		
							for(k=0;k<searchResultArray.length;k++)
							{
								var ordsearchresult = searchResultArray[k];

								if(ordsearchresult!=null)
								{
									for(var l=0;l<ordsearchresult.length;l++)
									{
										orderListArray[orderListArray.length]=ordsearchresult[l];				
									}
								}
							}

							for(var a = 0; a < orderListArray.length; a++){		

								if(ebizserialnumbers == "")
								{
									ebizserialnumbers = orderListArray[a].getValue('custrecord_serialnumber');
								}
								else
								{
									ebizserialnumbers = ebizserialnumbers + " , " + orderListArray[a].getValue('custrecord_serialnumber');
								}

								if(NSserialnumbers != "")
								{
									if(searchresultsitem.indexOf(orderListArray[a].getValue('custrecord_serialnumber')) == -1)
									{
										if(diffserialnumbers == "")
										{
											diffserialnumbers = orderListArray[a].getValue('custrecord_serialnumber');
										}
										else
										{
											diffserialnumbers = diffserialnumbers + " , " + orderListArray[a].getValue('custrecord_serialnumber');
										}
									}
								}
							}						
						}
					}			    

					if(NSserialnumbers == "")
						diffserialnumbers = ebizserialnumbers;
					nlapiLogExecution('ERROR', 'ebizserialnumbers', ebizserialnumbers);
					nlapiLogExecution('ERROR', 'NSserialnumbers', NSserialnumbers);
					nlapiLogExecution('ERROR', 'diffserialnumbers', diffserialnumbers);
					
					  var report = nlapiResolveURL('SUITELET', 'customscript_serialnumbermismatch_detail', 'customdeploy_serialnumbermismatch_detail');
					 report = report+ '&custparam_item='+ itemvalue;
					 report = report+ '&custparam_soid='+ soid;
					 report = report+ '&custparam_location='+ location;
					 report=report+'&custparam_line='+line;

					form.getSubList('custpage_items').setLineItemValue('custpage_line', i+1, line);
					form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i+1, itemText);
					if(ordqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i+1, ordqty);
					if(locationname != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_location', i+1, locationname);
					if(ebizserialnumbers != "")
					{
						if((ebizserialnumbers.length)>3955)
						{
							form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i+1, '<a href='+report+' target="_blank">'+itemText+'</a>');
							form.getSubList('custpage_items').setLineItemValue('custpage_ebzserialnumbers', i+1, ebizserialnumbers.substring(0,3950)+ ".....");
						}
						else
						{
							form.getSubList('custpage_items').setLineItemValue('custpage_ebzserialnumbers', i+1, ebizserialnumbers);
						}
					}
					if(NSserialnumbers != "")
					{
						if((NSserialnumbers.length)>3955)
						{
							form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i+1, '<a href='+report+' target="_blank">'+itemText+'</a>');
							form.getSubList('custpage_items').setLineItemValue('custpage_nsserialnumbers', i+1, NSserialnumbers.substring(0,3950)+".....");
						}
						else
						{
							
							form.getSubList('custpage_items').setLineItemValue('custpage_nsserialnumbers', i+1, NSserialnumbers);
						}
					}
					if(diffserialnumbers != "")
					{
						if((diffserialnumbers.length)>3955)
						{
							form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i+1, '<a href='+report+' target="_blank">'+itemText+'</a>');
							form.getSubList('custpage_items').setLineItemValue('custpage_diffserialnumbers', i+1, diffserialnumbers.substring(0,3950)+".....");
						}
						else
						{
							form.getSubList('custpage_items').setLineItemValue('custpage_diffserialnumbers', i+1, diffserialnumbers);
						}
					}					

					line = null; itemvalue = null; ordqty = null; location = null; locationname = null; ebizserialnumbers = ""; NSserialnumbers = ""; diffserialnumbers = "";

				}

			}
		}
		else if(itemno!=null && itemno!='')
		{
			var itemTypesku = nlapiLookupField('item', itemno, 'recordType');
			nlapiLogExecution('ERROR', 'itemTypesku', itemTypesku);
			
			if(itemTypesku == "serializedinventoryitem"){


				var location=null;
				var searchResultArray = new Array();
				if(request.getParameter('custpage_site')!=null && request.getParameter('custpage_site')!='' && request.getParameter('custpage_site')!='null')
				{
					location=request.getParameter('custpage_site');
					searchResultArray = getSerialnumbersforItem(null,itemno,location,null,-1);
					
					var searchresultsitem=getNSNumbers(itemno,location,-1);
					var NSserialnumbers='';var ebizserialnumbers='';var diffserialnumbers='';
					for (var j = 0; searchresultsitem != null && j < searchresultsitem.length; j++) {		
						var searchresultitem = searchresultsitem[j];
						//serial = searchresult.getValue('serialnumberlocation');
						if(NSserialnumbers == "")
						{
							NSserialnumbers = searchresultitem;//.getValue('serialnumber');
						}
						else
						{
							NSserialnumbers = NSserialnumbers + " , " + searchresultitem;//.getValue('serialnumber');
						}
					}
					if (searchResultArray !=null)
					{

						var orderListArray=new Array();		
						for(k=0;k<searchResultArray.length;k++)
						{
							var ordsearchresult = searchResultArray[k];

							if(ordsearchresult!=null)
							{
								for(var l=0;l<ordsearchresult.length;l++)
								{
									orderListArray[orderListArray.length]=ordsearchresult[l];				
								}
							}
						}

						if(orderListArray!=null)
						{
							for(var l=0;l<orderListArray.length;l++)
							{
								if(ebizserialnumbers == "")
								{
									ebizserialnumbers = orderListArray[l].getValue('custrecord_serialnumber');
								}
								else
								{
									ebizserialnumbers = ebizserialnumbers + " , " + orderListArray[l].getValue('custrecord_serialnumber');
								}
								if(NSserialnumbers != "")
								{
									if(searchresultsitem.indexOf(orderListArray[l].getValue('custrecord_serialnumber')) == -1)
									{
										if(diffserialnumbers == "")
										{
											diffserialnumbers = orderListArray[l].getValue('custrecord_serialnumber');
										}
										else
										{
											diffserialnumbers = diffserialnumbers + " , " + orderListArray[l].getValue('custrecord_serialnumber');
										}
									}
								}
							}
							var report = nlapiResolveURL('SUITELET', 'customscript_serialnumbermismatch_detail', 'customdeploy_serialnumbermismatch_detail');
							 report = report+ '&custparam_item='+ itemno;
							 report = report+ '&custparam_soid='+ '';
							 report = report+ '&custparam_location='+ location;
							 report=report+'&custparam_line='+'';
							 var itemText='';
							 var site='';
							 if(orderListArray!=null && orderListArray!='' &&  orderListArray.length>0)
								 {
								  itemText=orderListArray[0].getText('custrecord_serialitem');
								 site=orderListArray[0].getText('custrecord_serial_location');
								 }
							 else
								 {
								 var locRec=nlapiLookupField('location',location,['name'])
								  site=locRec.name; 
								 var itemRec=nlapiLookupField('item',itemno,['name'])
								  itemText=itemRec.name; 
								 }
								 
							form.getSubList('custpage_items').setLineItemValue('custpage_line', 1, '');
							form.getSubList('custpage_items').setLineItemValue('custpage_itemname', 1, itemText);

							form.getSubList('custpage_items').setLineItemValue('custpage_quantity', 1, '');

							form.getSubList('custpage_items').setLineItemValue('custpage_location', 1, site);
							if(ebizserialnumbers != null && ebizserialnumbers!='' && ebizserialnumbers!='null' && ebizserialnumbers!='undefined')
							{
								if((ebizserialnumbers.length)>3955)
								{
									form.getSubList('custpage_items').setLineItemValue('custpage_itemname', 1, '<a href='+report+' target="_blank">'+itemText+'</a>');
									form.getSubList('custpage_items').setLineItemValue('custpage_ebzserialnumbers', 1, ebizserialnumbers.substring(0,3950)+ ".....");
								}
								else
								{
									form.getSubList('custpage_items').setLineItemValue('custpage_ebzserialnumbers', 1, ebizserialnumbers);
								}
							}
							if(NSserialnumbers != null && NSserialnumbers!='' && NSserialnumbers!='null' && NSserialnumbers!='undefined')
							{
								if((NSserialnumbers.length)>3955)
								{
									form.getSubList('custpage_items').setLineItemValue('custpage_itemname', 1, '<a href='+report+' target="_blank">'+itemText+'</a>');
									form.getSubList('custpage_items').setLineItemValue('custpage_nsserialnumbers', 1, NSserialnumbers.substring(0,3950)+".....");
								}
								else
								{
									form.getSubList('custpage_items').setLineItemValue('custpage_nsserialnumbers', 1, NSserialnumbers);
								}
							}
							if(diffserialnumbers != null && diffserialnumbers!='' && diffserialnumbers!='null' && diffserialnumbers!='undefined')
							{
								if((diffserialnumbers.length)>3955)
								{
									form.getSubList('custpage_items').setLineItemValue('custpage_itemname', 1, '<a href='+report+' target="_blank">'+itemText+'</a>');
									form.getSubList('custpage_items').setLineItemValue('custpage_diffserialnumbers',1, diffserialnumbers.substring(0,3950)+".....");
								}
								else
								{
									form.getSubList('custpage_items').setLineItemValue('custpage_diffserialnumbers', 1, diffserialnumbers);
								}
							}					

							line = null; itemvalue = null; ordqty = null; location = null; locationname = null; ebizserialnumbers = ""; NSserialnumbers = ""; diffserialnumbers = "";

						}
					}
				}
				else
				{
					var ItemlocResults=getItemLocations(itemno);
					
					if(ItemlocResults!=null && ItemlocResults!='' && ItemlocResults.length>0)
					{
						var index=0;
						
						for(var k=0;k<ItemlocResults.length;k++)
						{

							var location=ItemlocResults[k].getValue('custrecord_serial_location',null,'group');
							
							if(location!=null && location!='' && location!='null')
								{
								var searchresultsitem=getNSNumbers(itemno,location,-1);
								index=parseInt(index)+1;
								var searchResultArray = getSerialnumbersforItem(null,itemno,location,null,-1);
								var NSserialnumbers='';var ebizserialnumbers='';var diffserialnumbers='';
								for (var j = 0; searchresultsitem != null && j < searchresultsitem.length; j++) {		
								var searchresultitem = searchresultsitem[j];
								//serial = searchresult.getValue('serialnumberlocation');
								if(NSserialnumbers == "")
								{
									NSserialnumbers = searchresultitem;//.getValue('serialnumber');
								}
								else
								{
									NSserialnumbers = NSserialnumbers + " , " + searchresultitem;//.getValue('serialnumber');
								}
							}
							if (searchResultArray !=null)
							{

								var orderListArray=new Array();		
								for(k1=0;k1<searchResultArray.length;k1++)
								{
									var ordsearchresult = searchResultArray[k1];

									if(ordsearchresult!=null)
									{
									for(var l=0;l<ordsearchresult.length;l++)
										{
											orderListArray[orderListArray.length]=ordsearchresult[l];				
										}
									}
								}

								if(orderListArray!=null)
								{
									for(var l=0;l<orderListArray.length;l++)
									{
										if(ebizserialnumbers == "")
										{
											ebizserialnumbers = orderListArray[l].getValue('custrecord_serialnumber');
										}
										else
										{
											ebizserialnumbers = ebizserialnumbers + " , " + orderListArray[l].getValue('custrecord_serialnumber');
										}
										if(NSserialnumbers != "")
										{
											if(searchresultsitem.indexOf(orderListArray[l].getValue('custrecord_serialnumber')) == -1)
											{
												if(diffserialnumbers == "")
												{
													diffserialnumbers = orderListArray[l].getValue('custrecord_serialnumber');
												}
												else
												{
													diffserialnumbers = diffserialnumbers + " , " + orderListArray[l].getValue('custrecord_serialnumber');
												}
											}
										}
									}
									var report = nlapiResolveURL('SUITELET', 'customscript_serialnumbermismatch_detail', 'customdeploy_serialnumbermismatch_detail');
									 report = report+ '&custparam_item='+ itemno;
									 report = report+ '&custparam_soid='+ '';
									 report = report+ '&custparam_location='+ location;
									 report=report+'&custparam_line='+'';
									 var itemText='';
									 var site='';
									 if(orderListArray!=null && orderListArray!='' &&  orderListArray.length>0)
										 {
										  itemText=orderListArray[0].getText('custrecord_serialitem');
										 site=orderListArray[0].getText('custrecord_serial_location');
										 }
									 else
										 {
										 var locRec=nlapiLookupField('location',location,['name'])
										  site=locRec.name; 
										 var itemRec=nlapiLookupField('item',itemno,['name'])
										  itemText=itemRec.name; 
										 }
										 
									
									
									
									form.getSubList('custpage_items').setLineItemValue('custpage_line', index, '');
									form.getSubList('custpage_items').setLineItemValue('custpage_itemname', index, itemText);

									form.getSubList('custpage_items').setLineItemValue('custpage_quantity', index, '');

									form.getSubList('custpage_items').setLineItemValue('custpage_location', index, site);
									if(ebizserialnumbers != null && ebizserialnumbers!='' && ebizserialnumbers!='null' && ebizserialnumbers!='undefined')
									{
										if((ebizserialnumbers.length)>3955)
										{
											form.getSubList('custpage_items').setLineItemValue('custpage_itemname', index, '<a href='+report+' target="_blank">'+itemText+'</a>');
											form.getSubList('custpage_items').setLineItemValue('custpage_ebzserialnumbers', index, ebizserialnumbers.substring(0,3950)+ ".....");
										}
										else
										{
											form.getSubList('custpage_items').setLineItemValue('custpage_ebzserialnumbers', index, ebizserialnumbers);
										}
									}
									if(NSserialnumbers != null && NSserialnumbers!='' && NSserialnumbers!='null' && NSserialnumbers!='undefined')
									{
										if((NSserialnumbers.length)>3955)
										{
											form.getSubList('custpage_items').setLineItemValue('custpage_itemname', index, '<a href='+report+' target="_blank">'+itemText+'</a>');
											form.getSubList('custpage_items').setLineItemValue('custpage_nsserialnumbers', index, NSserialnumbers.substring(0,3950)+".....");
										}
										else
										{
											form.getSubList('custpage_items').setLineItemValue('custpage_nsserialnumbers', index, NSserialnumbers);
										}
									}
									if(diffserialnumbers != null && diffserialnumbers!='' && diffserialnumbers!='null' && diffserialnumbers!='undefined')
									{
										if((diffserialnumbers.length)>3955)
										{
											form.getSubList('custpage_items').setLineItemValue('custpage_itemname', index, '<a href='+report+' target="_blank">'+itemText+'</a>');
											form.getSubList('custpage_items').setLineItemValue('custpage_diffserialnumbers',index, diffserialnumbers.substring(0,3950)+".....");
										}
										else
										{
											form.getSubList('custpage_items').setLineItemValue('custpage_diffserialnumbers', index, diffserialnumbers);
										}
									}					

									line = null; itemvalue = null; ordqty = null; location = null; locationname = null; ebizserialnumbers = ""; NSserialnumbers = ""; diffserialnumbers = "";

								}
							}
						
						}
						}
					}

				}



			}
		}
		else
		{

			var msgtext = "Invalid Order# : "+soidno;
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', '"+ msgtext + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
		}
		var button = form.addSubmitButton('Display');

		response.writePage(form);


	}

}


function GetSOInternalId(SOid)
{
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	salesorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', SOid)); 

	var SearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,null);	
	if(SearchResults==null)
	{
		nlapiLogExecution('ERROR', 'transferorder','transferorder');
		SearchResults=nlapiSearchRecord('transferorder',null,salesorderFilers,null);
	}

	if(SearchResults != null && SearchResults != "")
	{
		nlapiLogExecution('ERROR', 'SearchResults.length ',SearchResults.length);
		return SearchResults[0].getId();
	}
	else
		return null;
}

var searchResultArray=new Array();
var linearray = new Array();

function getSerialnumbersforItem(line,itemvalue,location,soid,maxno){

if(maxno==-1)
	{
	searchResultArray=new Array();
	}
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itemvalue));
	if(location!=null && location!='' && location!='null')
		filters.push(new nlobjSearchFilter('custrecord_serial_location', null, 'anyof', location));
//	else
//		filters.push(new nlobjSearchFilter('custrecord_serial_location', null, 'noneof', ['@NONE@']));

	if(line!=null && line!='' && line!='null')
		filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null, 'is', line));

	if(soid!=null && soid!='' && soid!='null')
		filters.push(new nlobjSearchFilter('custrecord_serialebizsono', null, 'is', soid));

	filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));
	linearray.push(line);
	if(maxno!=-1)
	{
		nlapiLogExecution('ERROR', 'Into getOpentaskSearchResults maxno',maxno);
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', maxno));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[2] = new nlobjSearchColumn('custrecord_serialsolineno');
	columns[3] = new nlobjSearchColumn('custrecord_serial_location');
	columns[4] = new nlobjSearchColumn('custrecord_serialsolineno');
	columns[5]=new nlobjSearchColumn('custrecord_serialitem');
	// LN, execute the search, passing null filter and return columns
	columns[1].setSort();
	columns[3].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
	if( searchresults!=null && searchresults.length>=1000)
	{ 
		nlapiLogExecution('Error', 'searchresults.length', searchresults.length);
		searchResultArray.push(searchresults); 
		var maxno=searchresults[searchresults.length-1].getValue(columns[1]);
		nlapiLogExecution('Error', 'maxno', maxno);
		getSerialnumbersforItem(line,itemvalue,location,request,maxno);	
	}
	else
	{
		//nlapiLogExecution('Error', 'searchresults.length', searchresults.length);
		searchResultArray.push(searchresults); 
	}
	return searchResultArray;
}
function getItemLocations(itemid)
{

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itemid));

	filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));


	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_serial_location',null,'group');


	var searchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

	return searchresults;

}
function getSerialNoCSV(arrayLP){
	var csv = "";
	nlapiLogExecution('ERROR', 'arrayLP.length',arrayLP.length);
	if(arrayLP.length == 1)
	{
		csv = arrayLP[0][0];
	}
	else
	{
		for (var i = 0; i < arrayLP.length; i++) {
			if(i == arrayLP.length -1)
			{
				csv += arrayLP[i][0].getValue('custrecord_serialnumber');
			}
			else
			{
				csv += arrayLP[i][0].getValue('custrecord_serialnumber') + ",";
			}
		}
	}
	return csv;
}
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}
var tempNSArray=new Array();
function getNSNumbers(item,site,maxno)
{
	if(maxno==-1)
		{
		tempNSArray=new Array();
		}
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('item',null,'is', item);
	filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', site);
        filters[2] = new nlobjSearchFilter('isonhand', null, 'is', 'T'); //new filter added by terry
        filters[3] = new nlobjSearchFilter('location', null, 'anyof', site); //new filter added by terry
	if(maxno!=-1)
	{
		filters[1] = new nlobjSearchFilter('internalidnumber',null,'greaterthan', maxno);
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('inventorynumber');
	columns[1] = new nlobjSearchColumn('internalid').setSort();
	var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);
	//nlapiLogExecution('DEBUG', 'searchresults.length', searchresults.length);
	if(searchresultsitem!=null && searchresultsitem!='')
	{
		if(searchresultsitem.length>=1000)

		{
			for (var j = 0; searchresultsitem != null && j < searchresultsitem.length; j++) {        
				var searchresultitem = searchresultsitem[j];


				tempNSArray.push(searchresultitem.getValue('inventorynumber'));
			}
			var maxno=searchresultsitem[searchresultsitem.length-1].getId();
			getNSNumbers(item,site,maxno);

		}
		else
		{
			for (var j = 0; searchresultsitem != null && j < searchresultsitem.length; j++) {        
				var searchresultitem = searchresultsitem[j];


				tempNSArray.push(searchresultitem.getValue('inventorynumber'));
			}
		}
	}
	return tempNSArray;
}
