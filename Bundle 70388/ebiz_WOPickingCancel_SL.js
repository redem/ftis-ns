/***************************************************************************
	  	 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WOPickingCancel_SL.js,v $
 *     	   $Revision: 1.1.2.1.4.1.6.2 $
 *     	   $Date: 2014/08/04 15:22:42 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WOPickingCancel_SL.js,v $
 * Revision 1.1.2.1.4.1.6.2  2014/08/04 15:22:42  skreddy
 * case # 20149745
 * jawbone SB issue fix
 *
 * Revision 1.1.2.1.4.1.6.1  2014/07/25 13:48:28  skreddy
 * case # 20149396
 * jawbone SB issue fix
 *
 * Revision 1.1.2.1.4.1  2013/03/08 14:43:37  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * 
 *
 *****************************************************************************/

function WOPickingCancellation(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('WorkOrder PickReversal');

		var woid=request.getParameter('custpage_workorder');
		nlapiLogExecution('ERROR','woid',woid);

		var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		WorkOrder.addSelectOption("","");
		fillWOField(form, WorkOrder,-1);

		WorkOrder.setMandatory(true);
		//fillsalesorderField(form, salesorder,-1);

		form.addSubmitButton('Display');


		response.writePage(form);

	}
	else  
	{ 

		var form = nlapiCreateForm('WorkOrder PickReversal');
		var tempflag = form.addField('custpage_tempflag', 'text','tempory flag').setDisplayType('hidden');
		var woid=request.getParameter('custpage_workorder');
		var tempflagtest = request.getParameter('custpage_tempflag');
		nlapiLogExecution('ERROR','woid',woid);
		form.setScript('customscript_inventoryclientvalidations');
		var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		WorkOrder.addSelectOption("","");
		fillWOField(form, WorkOrder,-1);
		WorkOrder.setMandatory(true);

		//fillsalesorderField(form, salesorder,-1);


		//var KitItemCnt = CheckKitItem(woid);
		//nlapiLogExecution('ERROR', 'KitItemCnt',KitItemCnt);

		form.addSubmitButton('Display');
		//	form.setScript('customscript_workorder_report');
		//form.addButton('custpage_print','Print','Printreport('+woid+')');
		// end of code on 04July2012 


		if(tempflagtest!='Confirm')
		{
			nlapiLogExecution("ERROR", 'confirmbutton', 'if');
			nlapiLogExecution("ERROR", 'tempflagtest', tempflagtest);

			/*if(KitItemCnt == 0)
			{*/		
				//form.addField('custpage_chkbox', 'checkbox', 'AdjustPickingQty').setLayoutType('startrow', 'none');
				WorkOrder.setDefaultValue(woid);
				var ItemSubList = form.addSubList("custpage_woreport", "list", "Components");
				ItemSubList.addField("custpage_woreport_item", "text","Item");
				ItemSubList.addField("custpage_woreport_binloc", "text", "Bin Location");
				ItemSubList.addField("custpage_woreport_lp", "text", "LP");
				ItemSubList.addField("custpage_woreport_qty", "text", "Quantity");   
				ItemSubList.addField("custpage_woreport_lotno", "text", "Lot#");
				ItemSubList.addField("custpage_woreport_line", "text", "Lineno");

				nlapiLogExecution('ERROR','INTOwoid1',woid);
				var WOFilers = new Array();
				WOFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', woid));
				//WOFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', 5));//kts
				WOFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//pick Generate, Pick confirm
			WOFilers.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));//nsconfirm# is empty

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_line_no');
				columns[1] = new nlobjSearchColumn('custrecord_sku');
				columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
				columns[3] = new nlobjSearchColumn('custrecord_lpno');
				columns[4] = new nlobjSearchColumn('custrecord_batch_no');
				columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
				columns[6] = new nlobjSearchColumn('custrecord_act_qty'); 
				columns[7] = new nlobjSearchColumn('custrecord_actendloc');                                 
				columns[0].setSort();

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, WOFilers,columns);
				if(searchresults!=null && searchresults!='') 
				{ 	    	
					for (var i = 0; i < searchresults.length; i++) {

						var taskid = searchresults[i].getId();
						var vitem = searchresults[i].getText('custrecord_sku');
						var vactbeginloc = searchresults[i].getText('custrecord_actbeginloc');
						var vactendloc = searchresults[i].getText('custrecord_actendloc');
						var vlpno = searchresults[i].getValue('custrecord_lpno');
						var vactqty = searchresults[i].getValue('custrecord_act_qty');
						var vlotno = searchresults[i].getValue('custrecord_batch_no');
						var vline = searchresults[i].getValue('custrecord_line_no');
						nlapiLogExecution('ERROR','vlotno',vlotno);

						form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_item', i+1, vitem);
						form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_binloc', i+1, vactbeginloc);
						form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_lp', i+1, vlpno);
						form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_qty', i+1, vactqty);  
						form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_lotno', i+1, vlotno);
						form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_line', i+1, vline);

					}
				form.addField('custpage_chkbox', 'checkbox', 'Adjust Picked Qty').setLayoutType('startrow', 'none');
				form.addButton('custpage_confirmrpln', 'Submit','Display()');
				}
				else
				{
					nlapiLogExecution('ERROR','Workorder Cancel','Failed' );
					showInlineMessage(form, 'Error','Cannot Cancel this Work order ', '');


				}
				//form.addButton('custpage_confirmrpln', 'Cancel WO','Display()');

			//}
			/*else
			{
				nlapiLogExecution('ERROR','Workorder Cancel','Failed' );
				showInlineMessage(form, 'Error','Cannot Cancel this Work order ', '');


			}*/

			response.writePage(form);
		}
		else
		{
			nlapiLogExecution("ERROR", 'confirmbutton', 'succes');
			nlapiLogExecution("ERROR", 'tempflagtest', tempflagtest);

			nlapiLogExecution('ERROR', 'vWOId', 'vWOId');
			var chkselect = request.getParameter('custpage_chkbox');
			var CancelFlag = false;
			if(chkselect=='T')
				CancelFlag = true;	

			var ResultArray = WOInvReversal(woid,CancelFlag);
			if(ResultArray.length>0)
			{ 
				DeleteKitItem(woid);
				nlapiLogExecution('ERROR', 'ResultArray[0]', ResultArray[0]);
				if(ResultArray[0]=='E')
				{
					nlapiLogExecution('ERROR', 'ResultArray[1]', ResultArray[1]);
					showInlineMessage(form, 'Error',''+ResultArray[1]+' ', '');
					response.writePage(form);
					return false;

				}
				else if(ResultArray[0]=='S')
				{
					nlapiLogExecution('ERROR', 'ResultArray[1]', ResultArray[1]);
					showInlineMessage(form, 'Confirmation',''+ResultArray[1]+' ', '');
					response.writePage(form);
					return false;

				}


			}
			response.writePage(form);
		}

	}
}

	function fillWOField(form, WOField,maxno){
		var WOFilers = new Array();
		WOFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	WOFilers.push(new nlobjSearchFilter('status', null, 'is', ['WorkOrd:B','WorkOrd:D']));

		if(maxno!=-1)
		{
			WOFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
		}


		WOField.addSelectOption("", "");

		var columns=new Array();
		columns[0]=new nlobjSearchColumn('tranid');
		columns[0].setSort(true);
		columns[1]=new nlobjSearchColumn('internalid');
		columns[2]=new nlobjSearchColumn('type');


		var customerSearchResults = nlapiSearchRecord('workorder', null, WOFilers,columns);


		for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

			//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
			if(customerSearchResults[i].getValue('tranid') != null && customerSearchResults[i].getValue('tranid') != "" && customerSearchResults[i].getValue('tranid') != " ")
			{
				var resdo = form.getField('custpage_workorder').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
				if (resdo != null) {
					if (resdo.length > 0) {
						continue;
					}
				}
			}
			WOField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
		}
		if(customerSearchResults!=null && customerSearchResults.length>=1000)
		{
			var column=new Array();
			column[0]=new nlobjSearchColumn('tranid');		
			column[1]=new nlobjSearchColumn('internalid');
			column[1].setSort(true);

			var OrderSearchResults = nlapiSearchRecord('workorder', null, WOFilers,column);

			var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
			fillWOField(form, WOField,maxno);	

		}
	}

//}








function DeleteKitItem(vWOId){
	try
	{
		nlapiLogExecution('ERROR', 'vWOId', 'vWOId');
		var count=0;
		var filterOpentask = new Array();

	filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['2']));	//	Status - Picks Generated
	filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['5']));

	if(vWOId != null && vWOId != "")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
	}

	var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,null);

	if(WOSearchResults!=null && WOSearchResults !='')
	{ 
			for(var j=0;j<WOSearchResults.length;j++)
			{
				nlapiDeleteRecord('customrecord_ebiznet_trn_opentask',WOSearchResults[j].getId());
			} 
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'exception in DeleteKitItem ', e);	
	}


}