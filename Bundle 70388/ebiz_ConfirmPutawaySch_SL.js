/***************************************************************************
	  		   eBizNET Solutions .
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_ConfirmPutawaySch_SL.js,v $
 *     	   $Revision: 1.1.2.1.8.2.4.1 $
 *     	   $Date: 2015/11/16 17:09:42 $
 *     	   $Author: deepshikha $
 *
 *   eBizNET version and checksum/ stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SU/M: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *� $Log: ebiz_ConfirmPutawaySch_SL.js,v $
 *� Revision 1.1.2.1.8.2.4.1  2015/11/16 17:09:42  deepshikha
 *� 2015.2 issues
 *� 201415665
 *�
 *� Revision 1.1.2.1.8.2  2014/05/27 07:36:59  snimmakayala
 *� Case#: 20148135
 *� Schedule Script Status
 *�
 *� Revision 1.1.2.1.8.1  2013/09/11 12:03:17  rmukkera
 *� Case# 20124374
 *�
 *� Revision 1.1.2.1  2012/07/16 06:31:22  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Confirm Putaway Schedule Script.
 *�
 *�
 *****************************************************************************/
function ebiznet_ConfirmPutaway_sch(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Confirm Putaway');

		//$LN, LOT/Batch # Validation...

		form.setScript('customscript_chknlpputwvalidation');	

		var vMsg;
		nlapiLogExecution('ERROR', 'msg', request.getParameter('custpage_msg'));
		if (request.getParameter('custpage_msg') != null && request.getParameter('custpage_msg') != "") {
			vMsg = request.getParameter('custpage_msg');
			nlapiLogExecution('ERROR', 'vMsg', vMsg);
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null); 
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '" + vMsg + "', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		}
		else
		{
			//To get PO location and recordType.
			var pointid='';
			var VarPO = form.addField('custpage_po', 'text', 'PO #');
			var polocation = form.addField('custpage_loc_id', 'select', 'Location', 'location');
			polocation.setMandatory(true);
			var chknCompany = form.addField('custpage_compid', 'select', 'Company', 'customrecord_ebiznet_company');
			nlapiLogExecution('ERROR','paramter PO Id',request.getParameter('custparam_poid'));
			if(request.getParameter('custparam_poid')!=""&&request.getParameter('custparam_poid')!=null)
			{
				var poname = request.getParameter('custparam_poid');

				var filterspo = new Array();
				filterspo.push(new nlobjSearchFilter( 'name', null, 'is', poname));
				filterspo.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [2]));			

				var columnspo = new Array();
				columnspo[0] = new nlobjSearchColumn('id');	 
				columnspo[1] = new nlobjSearchColumn('name');
				columnspo[2] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');

				var poSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterspo,columnspo);

				if(poSearchResults!=null && poSearchResults!='' && poSearchResults.length>0)
				{
					pointid = poSearchResults[0].getValue('custrecord_ebiz_cntrl_no');
				}

				nlapiLogExecution('ERROR', 'PO Id', pointid);

				var fields = ['recordType', 'location','custbody_nswms_company'];
				var columns = nlapiLookupField('transaction', pointid, fields);
				var tranType = columns.recordType;
				var trnlocation = columns.location;
				var vcompany= columns.custbody_nswms_company;

				var str = 'Tran Type. = ' + tranType + '<br>';
				str = str + 'Location. = ' + trnlocation + '<br>';
				str = str + 'Company. = ' + vcompany + '<br>';

				nlapiLogExecution('ERROR', 'PO Details', str);

				var vponum='';

				try
				{
					var posearchresults = new Array();
					var vponum= '';

					if(tranType!='transferorder')
					{
						posearchresults = nlapiLoadRecord('purchaseorder', pointid); //1020
						vponum= posearchresults.getFieldValue('tranid');
					}
					else
					{
						posearchresults = nlapiLoadRecord('transferorder', pointid); //1020
						vponum= posearchresults.getFieldValue('tranid');
						trnlocation = posearchresults.getFieldValue('transferlocation');
						nlapiLogExecution('ERROR', 'trnlocation',trnlocation);
					}
					nlapiLogExecution('ERROR', 'vponum',vponum);

				}
				catch(exp)
				{
					nlapiLogExecution('ERROR', 'Exception trantypefld',exp);
				}

				VarPO.setDefaultValue(vponum);

				//Added for Location select field
				polocation.setDefaultValue(trnlocation);


				try
				{
					var trantypefld = form.addField('custpage_trantype', 'text', 'Trantype');
					trantypefld.setDefaultValue(tranType);
					trantypefld.setDisplayType('hidden');
				}
				catch(exp)
				{
					nlapiLogExecution('ERROR', 'Exception trantypefld',exp);
				}

				try
				{
					chknCompany.setDefaultValue(vcompany);
					chknCompany.setDisplayType('inline');
				}
				catch(exp)
				{
					nlapiLogExecution('ERROR', 'Exception chknCompany',exp);
				}
			}

			var taskEmployee= form.addField('custpage_taskassignedempid','select','Task assigned to','employee');

//			var receiptid=request.getParameter('custparam_receipt');
			var Receiptno= form.addField('custpage_receiptno','Text','Receipt#');
//			Receiptno.setDefaultValue(receiptid);
//			nlapiLogExecution('ERROR','Receiptno',receiptid);

//			var trailerid=request.getParameter('custparam_trailer');
			var Trailerno= form.addField('custpage_trailerno','Text','Trailer#');
//			Trailerno.setDefaultValue(trailerid);
//			nlapiLogExecution('ERROR','Trailerno',trailerid);

			var receiptid='';
			var trailerid='';

			var sublist = form.addSubList("custpage_items", "inlineeditor", "Confirm Putaway");

			sublist.addField("custpage_polocgen", "checkbox", "Confirm").setDefaultValue('T');
			sublist.addField("custpage_po", "text", "PO#");
			sublist.addField("custpage_receipt", "text", "Receipt#");
			sublist.addField("custpage_trailer", "text", "Trailer#");
			sublist.addField("custpage_line", "text", "Line#");
			sublist.addField("custpage_chkn_lp", "text", "LP#");
			sublist.addField("custpage_iteminternalid", "select", "Item", "item");
			sublist.addField("custpage_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDefaultValue('1');
			sublist.addField("custpage_item_pc", "text", "Pack Code").setDefaultValue('1');
			sublist.addField("custpage_lotbatch", "text", "LOT#");
			sublist.addField("custpage_quantity", "text", "Quantity");
			sublist.addField("custpage_location", "select", "Bin Location", "customrecord_ebiznet_location").setMandatory(true);
			sublist.addField("custpage_pointid", "text", "poIntid").setDisplayType('hidden');
			sublist.addField("custpage_poskuid", "text", "skuIntid").setDisplayType('hidden');
			sublist.addField("custpage_poskudesc", "text", "skudesc").setDisplayType('hidden');
			sublist.addField("custpage_serialno", "text", "SerialNo").setDisplayType('hidden');
			sublist.addField("custpage_serialnos", "textarea", "Serial #");
			sublist.addField("custpage_batchwithqty", "textarea", "LOT with quantiy").setDisplayType('hidden');
			sublist.addField("custpage_line_item", "text", "LineItem").setDisplayType('hidden');
			sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
			sublist.addField("custpage_oldlocation", "select", "old Bin Location", "customrecord_ebiznet_location").setDisplayType('hidden');
			sublist.addField("custpage_opentaskid", "text", "Open Task ID").setDisplayType('hidden');
			sublist.addField("custpage_displayedquantity", "text", "Displayed Quantity").setDisplayType('hidden');
			sublist.addField("custpage_ebizmethodno", "text", "Method ID").setDisplayType('hidden');
			sublist.addField("custpage_putstrategy", "text", "Rule ID").setDisplayType('hidden');
			sublist.addField("custpage_oldqty", "text", "Old Qty").setDisplayType('hidden');
			sublist.addField("custpage_uomlevel", "text", "UOM LEVEL").setDisplayType('hidden');
			sublist.addField("custpage_trailer_hdn", "text", "ebizTrailer").setDisplayType('hidden');
			sublist.addField("custpage_receipt_hdn", "text", "ebizReceipt").setDisplayType('hidden');
			sublist.addField("custpage_wmsstatus", "text", "WMS Status").setDisplayType('hidden');
			sublist.addField("custpage_name", "text", "name").setDisplayType('hidden');
			sublist.addField("custpage_begindate", "text", "begin date").setDisplayType('hidden');
			sublist.addField("custpage_begintime", "text", "begin time").setDisplayType('hidden');

			var button = form.addSubmitButton('Confirm');

			//LN,Serial no's from serial Entry
			var arrayLP = new Array();
			var lpcolumns = new Array();
			lpcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
			lpcolumns[1] = new nlobjSearchColumn('custrecord_serialnumber');
			var lpsearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, null, lpcolumns);
			for (var i = 0; lpsearchresults != null && i < lpsearchresults.length; i++) {
				var SerialLP = new Array();
				SerialLP[0] = lpsearchresults[i].getValue('custrecord_serialparentid');
				SerialLP[1] = lpsearchresults[i].getValue('custrecord_serialnumber');
				arrayLP.push(SerialLP);

			}

			//LN, define search filters
			var filters = new Array();

			if((trailerid != "" && trailerid != null) || (receiptid != "" && receiptid != null))
			{
				if (receiptid != "" && receiptid != null)
					filters.push(new nlobjSearchFilter('custrecord_ot_receipt_no', null, 'is', receiptid));
				if (trailerid != "" && trailerid != null)
					filters.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerid));
			}
			else
			{
				if (pointid != "" && pointid != null) 
					filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid));
			}

			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]));
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]));

			var columns = new Array();
			columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));		
			columns.push(new nlobjSearchColumn('custrecord_ebiz_lpno'));
			columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
			columns.push(new nlobjSearchColumn('custrecord_sku_status'));
			columns.push(new nlobjSearchColumn('custrecord_batch_no'));
			columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
			columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			columns.push(new nlobjSearchColumn('custrecord_sku')); //Retreive SKU Details
			columns.push(new nlobjSearchColumn('custrecord_skudesc')); //Retreive SKU desc Details
			columns.push(new nlobjSearchColumn('custrecord_packcode'));
			columns.push(new nlobjSearchColumn('custrecord_lotnowithquantity'));
			columns.push(new nlobjSearchColumn('custrecord_comp_id'));
			columns.push(new nlobjSearchColumn('custrecord_ebiz_trailer_no'));
			columns.push(new nlobjSearchColumn('custrecord_ebiz_receipt_no'));
			columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			columns.push(new nlobjSearchColumn('custrecord_line_no'));
			columns.push(new nlobjSearchColumn('custrecord_ebizrule_no'));
			columns.push(new nlobjSearchColumn('custrecord_ebizmethod_no'));
			columns.push(new nlobjSearchColumn('custrecord_uom_level'));
			columns.push(new nlobjSearchColumn('custrecord_ebiz_trailer_no'));
			columns.push(new nlobjSearchColumn('custrecord_ebiz_ot_receipt_no'));
			columns.push(new nlobjSearchColumn('custrecord_wms_status_flag'));
			columns.push(new nlobjSearchColumn('name'));
			columns.push(new nlobjSearchColumn('custrecordact_begin_date'));
			columns.push(new nlobjSearchColumn('custrecord_actualbegintime'));
			columns[0].setSort('true');
			columns[15].setSort('true');

			//LN, execute the  search, passing null filter and return columns
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			var lp, line, itemname, itemstatus, lotbatch, qty, loc, itemid, itemdesc,wmsstatus, 
			itempackcode,batchwithqty,opentaskid,uomlevel,name,begindate,begintime;
			var trailer,receipt,po,methodid,ruleid;
			var poInternalId;
			var rcptqty = new Array();

			/*
         LN, Searching records from custom record and dynamically adding to the sublist lines
			 */

			for (var i = 0; searchresults != null && i < searchresults.length; i++) {		
				var searchresult = searchresults[i];

				opentaskid = searchresult.getId();
				line = searchresult.getValue('custrecord_line_no');
				lp = searchresult.getValue('custrecord_ebiz_lpno');
				itemid = searchresult.getValue('custrecord_ebiz_sku_no');
				itemstatus = searchresult.getValue('custrecord_sku_status');
				lotbatch = searchresult.getValue('custrecord_batch_no');
				qty = searchresult.getValue('custrecord_expe_qty');
				loc = searchresult.getValue('custrecord_actbeginloc');
				itemname = searchresult.getValue('custrecord_sku');
				itemdesc = searchresult.getValue('custrecord_skudesc');
				itempackcode = searchresult.getValue('custrecord_packcode');
				batchwithqty =  searchresult.getValue('custrecord_lotnowithquantity');
				uomlevel = searchresult.getValue('custrecord_uom_level');
				retserialcsv = getSerialNoCSV(arrayLP, lp);
				vCompany =  searchresult.getValue('custrecord_comp_id');
				trailer=searchresult.getValue('custrecord_ebiz_trailer_no');
				receipt=searchresult.getValue('custrecord_ebiz_receipt_no');
				po=searchresult.getValue('custrecord_ebiz_cntrl_no');
				poInternalId=searchresult.getValue('custrecord_ebiz_order_no');
				poname=searchresult.getText('custrecord_ebiz_order_no');
				methodid=searchresult.getValue('custrecord_ebizmethod_no');
				ruleid=searchresult.getValue('custrecord_ebizrule_no');
				wmsstatus = searchresult.getValue('custrecord_wms_status_flag');
				name = searchresult.getValue('name');
				begindate = searchresult.getValue('custrecordact_begin_date');
				begintime = searchresult.getValue('custrecord_actualbegintime');

				var vEbizTrailerNo='';
				if(searchresult.getValue('custrecord_ebiz_trailer_no') != null && searchresult.getValue('custrecord_ebiz_trailer_no') != '')
				{
					vEbizTrailerNo=searchresult.getValue('custrecord_ebiz_trailer_no');
				}
				var vEbizReceiptNo='';
				if(searchresult.getValue('custrecord_ebiz_ot_receipt_no') != null && searchresult.getValue('custrecord_ebiz_ot_receipt_no') != '')
				{
					vEbizReceiptNo=searchresult.getValue('custrecord_ebiz_ot_receipt_no');
				}

				form.getSubList('custpage_items').setLineItemValue('custpage_line', i + 1, line);
				form.getSubList('custpage_items').setLineItemValue('custpage_chkn_lp', i + 1, lp);
				form.getSubList('custpage_items').setLineItemValue('custpage_iteminternalid', i + 1, itemid);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, itemstatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_item_pc', i + 1, itempackcode);
				form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, lotbatch);
				form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, qty);
				form.getSubList('custpage_items').setLineItemValue('custpage_oldqty', i + 1, qty);
				form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, loc);
				if((trailerid != "" && trailerid != null) || (receiptid != "" && receiptid != null))
				{
					form.getSubList('custpage_items').setLineItemValue('custpage_pointid', i + 1, poInternalId);
				}
				else
				{
					form.getSubList('custpage_items').setLineItemValue('custpage_pointid', i + 1, pointid);
				}
				form.getSubList('custpage_items').setLineItemValue('custpage_poskuid', i + 1, itemid);
				form.getSubList('custpage_items').setLineItemValue('custpage_poskudesc', i + 1, itemdesc);
				form.getSubList('custpage_items').setLineItemValue('custpage_serialnos', i + 1, retserialcsv);
				form.getSubList('custpage_items').setLineItemValue('custpage_batchwithqty', i + 1, batchwithqty);
				form.getSubList('custpage_items').setLineItemValue('custpage_line_item', i + 1, itemid);
				form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1, vCompany);	
				form.getSubList('custpage_items').setLineItemValue('custpage_oldlocation', i + 1, loc);
				form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i + 1, opentaskid);
				form.getSubList('custpage_items').setLineItemValue('custpage_uomlevel', i + 1, uomlevel);
				form.getSubList('custpage_items').setLineItemValue('custpage_displayedquantity', i + 1, qty);
				form.getSubList('custpage_items').setLineItemValue('custpage_po', i + 1, poname);
				form.getSubList('custpage_items').setLineItemValue('custpage_receipt', i + 1, receipt);
				form.getSubList('custpage_items').setLineItemValue('custpage_trailer', i + 1, trailer);
				form.getSubList('custpage_items').setLineItemValue('custpage_ebizmethodno', i + 1, methodid);
				form.getSubList('custpage_items').setLineItemValue('custpage_putstrategy', i + 1, ruleid);
				form.getSubList('custpage_items').setLineItemValue('custpage_trailer', i + 1, vEbizTrailerNo);
				form.getSubList('custpage_items').setLineItemValue('custpage_receipt', i + 1, vEbizReceiptNo);
				form.getSubList('custpage_items').setLineItemValue('custpage_wmsstatus', i + 1, wmsstatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_name', i + 1, name);
				form.getSubList('custpage_items').setLineItemValue('custpage_begindate', i + 1, begindate);
				form.getSubList('custpage_items').setLineItemValue('custpage_begintime', i + 1, begintime);


			}
			nlapiSelectLineItem('custpage_items', 1);
			if(request.getParameter('custparam_poid')!=''&&request.getParameter('custparam_poid')!=null)
				form.addPageLink('crosslink', 'Back to PO', nlapiResolveURL('RECORD', tranType, request.getParameter('custparam_poid')));

		}

		response.writePage(form);
	}
	else {
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start of POST',context.getRemainingUsage());
		nlapiLogExecution('ERROR', 'Time Stamp at the start of POST',TimeStampinSec());

		var poloc = request.getParameter('custpage_loc_id');
		var tranType = request.getParameter('custpage_trantype');
		var PutwCompany = request.getParameter('custpage_compid');
		var empId= request.getParameter('custpage_taskassignedempid');
		var vPO = request.getParameter('custpage_po');
		nlapiLogExecution('ERROR', 'vPO ', vPO);   

		try {
			var lineCnt = request.getLineItemCount('custpage_items');
			nlapiLogExecution('ERROR', 'lineCnt ', lineCnt);   
			var cnt=0;
			var vtaskid='';

			for (var s = 1; s <= lineCnt ; s++) 
			{
				var confirmFlag= request.getLineItemValue('custpage_items', 'custpage_polocgen', s);
				var taskid = request.getLineItemValue('custpage_items', 'custpage_opentaskid',s);				
				var vpoid = request.getLineItemValue('custpage_items', 'custpage_pointid',s);
				var vponame = request.getLineItemValue('custpage_items', 'custpage_po',s);
				var vqty = request.getLineItemValue('custpage_items', 'custpage_quantity',s);
				nlapiLogExecution('ERROR', 'vqty ', vqty);   

				var vtranType = nlapiLookupField('transaction', vpoid, 'recordType');
			if((taskid!=null && taskid!='' && taskid!='null') && (vpoid!=null && vpoid!='' && vpoid!='null'))
					{
				var opentaskRec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', taskid);
				var expqtyn = opentaskRec.getFieldValue('custrecord_expe_qty');
				nlapiLogExecution('ERROR', 'expqtyn ', expqtyn);   
  
				    if(vqty!=expqtyn)
				    	{
						nlapiLogExecution('ERROR', 'vqty 1', vqty);   

				    	opentaskRec.setFieldValue('custrecord_expe_qty', vqty);
				    	nlapiSubmitRecord(opentaskRec, false, true);
				    	
				    	}
			
					}
				if(confirmFlag == 'T')
				{
					if(cnt==0)
					{
						vtaskid=taskid;
						cnt++;
					}
					else
					{
						vtaskid=vtaskid+','+taskid;
						cnt++;
					}
				}
			}

			var param = new Array();
			param['custscript_puttaskids'] = vtaskid;

			nlapiScheduleScript('customscript_putconfirm_scheduler', 'customdeploy_putconfirm_scheduler',param);

			nlapiLogExecution('ERROR','Remaining usage after calling Schedule script',context.getRemainingUsage());

			var currentUserID = context.getUser();

			updateScheduleScriptStatus('Confirm Putaway',currentUserID,'Submitted',vPO,vtranType);

			var POarray=new Array();
			POarray ["custpage_msg"] ='Put Confirmation process has been initiated successfully';

			response.sendRedirect('SUITELET', 'customscript_putconfirmsch', 'customdeploy_putconfirmsch', false, POarray );
		} 
		catch (exp) {
			nlapiLogExecution('ERROR', 'Err in summing Qty', exp);
		}

		nlapiLogExecution('ERROR','Remaining usage at the End of POST',context.getRemainingUsage());
		nlapiLogExecution('ERROR', 'Time Stamp at the End of POST',TimeStampinSec());
	}
}

function getSerialNoCSV(arrayLP, lp){
	var csv = "";
	for (var i = 0; i < arrayLP.length; i++) {
		if (arrayLP[i][0] == lp) {
			csv += arrayLP[i][1] + " , ";
		}
	}
	return csv;
}

