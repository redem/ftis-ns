/***************************************************************************
eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CreateInventoryExpDate.js,v $
 *� $Revision: 1.1.2.1.4.4.2.5 $
 *� $Date: 2015/02/23 17:51:34 $
 *� $Author: rrpulicherla $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_CreateInventoryExpDate.js,v $
 *� Revision 1.1.2.1.4.4.2.5  2015/02/23 17:51:34  rrpulicherla
 *� Case#201411317
 *�
 *� Revision 1.1.2.1.4.4.2.4  2014/05/30 00:34:19  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.1.4.4.2.3  2013/04/17 16:02:37  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.1.2.1.4.4.2.2  2013/03/19 11:53:20  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production and UAT issue fixes.
 *�
 *� Revision 1.1.2.1.4.4.2.1  2013/03/05 13:35:38  rmukkera
 *� Merging of lexjet Bundle files to Standard bundle
 *�
 *� Revision 1.1.2.1.4.4  2013/02/07 08:49:04  skreddy
 *� CASE201112/CR201113/LOG201121
 *�  RF Lot auto generating FIFO enhancement
 *�
 *� Revision 1.1.2.1.4.3  2012/10/05 06:34:12  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting Multiple language into given suggestions
 *�
 *� Revision 1.1.2.1.4.2  2012/09/26 12:33:02  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.1.2.1.4.1  2012/09/21 15:01:30  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.1.2.1  2012/05/17 13:13:44  spendyala
 *� CASE201112/CR201113/LOG201121
 *� New script for expiry date validation.
 *�
 *
 ****************************************************************************/

function CreateInventoryExpDate(request, response){
	if (request.getMethod() == 'GET') {

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getLocationId = request.getParameter('custparam_locationId');
		var getBinLocationId = request.getParameter('custparam_binlocationid');
		var getBinLocationName = request.getParameter('custparam_binlocationname');    
		var getItemId = request.getParameter('custparam_itemid');
		var getItem = request.getParameter('custparam_item');
		var getItemType = request.getParameter('custparam_ItemType');
		var getQuantity = request.getParameter('custparam_quantity');
		var getBatchNo = request.getParameter('custparam_BatchNo');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
		var ItemShelfLife = request.getParameter('custparam_shelflife');
		var CaptureExpiryDate = request.getParameter('custparam_captureexpirydate');
		var CaptureFifoDate = request.getParameter('custparam_capturefifodate');



		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{

			st0 = "CHECKIN FECHA DE CADUCIDAD";
			st1 = "FECHA DE MANUFACTURA";
			st2 = "FECHA DE EXPIRACI&#211;N:";
			st3 = "FECHA DE CADUCIDAD";
			st4 = "FORMATO: MMDDAA";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";

		}
		else
		{
			st0 = "CHECKIN EXPIRY DATE";
			st1 = "MFG DATE";
			st2 = "EXP DATE:";
			st3 = "BEST BEFORE DATE";
			st4 = "FORMAT : MMDDYY";
			st5 = "SEND";
			st6 = "PREV";

		}


		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":</td>";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value=" + getBinLocationName + ">";
		html = html + "					<input type='hidden' name='hdnLocationId' value=" + getLocationId + ">";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem+ ">";
		html = html + "					<input type='hidden' name='hdnQty' value=" + getQuantity + ">";
		html = html + "					<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + getItem + "'>";
		html = html + "				    <input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "			    	<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				    <input type='hidden' name='hdnItemShelfLife' value=" + ItemShelfLife + ">";
		html = html + "				    <input type='hidden' name='hdnCaptureExpiryDate' value=" + CaptureExpiryDate + ">";
		html = html + "				    <input type='hidden' name='hdnCaptureFifoDate' value=" + CaptureFifoDate + ">";


		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entermfgdate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + "</td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterexpdate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + ": </td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbbdate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		try {

			var optedEvent = request.getParameter('cmdPrevious');

			var getMfgDate = request.getParameter('entermfgdate');
			var getExpDate = request.getParameter('enterexpdate');
			var getBestBeforeDate = request.getParameter('enterbbdate');
			var getBatchNo=request.getParameter('hdnBatchNo');
			var ItemId= request.getParameter('hdnItemId');
			var locationId = request.getParameter('hdnLocationId');
			nlapiLogExecution('ERROR', 'locationId tst ', locationId);
			var getActualBeginDate = request.getParameter('custparam_actualbegindate');
			var getActualBeginTime = request.getParameter('custparam_actualbegintime');
			var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
			var Itemtype= request.getParameter('hdnitemType');
			nlapiLogExecution('ERROR', 'optedEvent', optedEvent);

			var CIarray = new Array();

			var getLanguage = request.getParameter('hdngetLanguage');
			CIarray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);


			var st7,st8,st9;
			if( getLanguage == 'es_ES')
			{
				st7 = "ENTER LOTE #";
				st8 = "MFGDATE SHLD NO SER MAYOR O IGUAL A EXPDATE";
				st9 = "FECHAS NO V&#193;LIDAS";
			}
			else
			{
				st7 = "ENTER LOT#";
				st8 = "MFGDATE SHLD NOT BE GREATER THAN OR EQUAL TO EXPDATE";
				st9 = "INVALID DATES";
			}




			CIarray["custparam_binlocationid"] = request.getParameter('hdnBinLocationId');
			CIarray["custparam_binlocationname"] = request.getParameter('hdnBinLocation');
			CIarray["custparam_itemid"] = request.getParameter('hdnItemId');
			CIarray["custparam_item"] = request.getParameter('hdnItem');
//			CIarray["custparam_item"] = Item;
			CIarray["custparam_quantity"] = request.getParameter('hdnQty');
			CIarray["custparam_itemtype"]=Itemtype;
			CIarray["custparam_actualbegindate"] = getActualBeginDate;
			CIarray["custparam_locationId"] = locationId;
			CIarray["custparam_actualbegintime"] = getActualBeginTime;
			CIarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
			CIarray["custparam_screenno"] = '15B';
			CIarray["custparam_BatchNo"] = getBatchNo;
			CIarray["custparam_error"] = st7;
			CIarray["custparam_shelflife"] = request.getParameter('hdnItemShelfLife');
			CIarray["custparam_captureexpirydate"] = request.getParameter('hdnCaptureExpiryDate');
			CIarray["custparam_capturefifodate"] = request.getParameter('hdnCaptureFifoDate');
			var itemShelflife = request.getParameter('hdnItemShelfLife');
			var CaptureExpirydate = request.getParameter('hdnCaptureExpiryDate');
			var CaptureFifodate = request.getParameter('hdnCaptureFifoDate');
			nlapiLogExecution('ERROR', 'itemShelflife', itemShelflife);
			nlapiLogExecution('ERROR', 'CaptureExpirydate', CaptureExpirydate);
			nlapiLogExecution('ERROR', 'CaptureFifodate', CaptureFifodate);

			var error='';
			var errorflag='F';

			if (optedEvent == 'F7') {
				nlapiLogExecution('ERROR', 'CHECK IN EXP DATE F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_createinventory_batchno', 'customdeploy_rf_createinventory_batchno', false, CIarray);
			}
			else {
				var ValueMfgDate="";
				var ValueExpDate="";

				if ((getMfgDate != '' && getExpDate == '') || (getMfgDate == '' && getExpDate != '') || (getMfgDate != '' && getExpDate != '') || (getMfgDate != '' && getExpDate != '' && getBestBeforeDate != '')) {
					if (getMfgDate != '' && getExpDate == '')
					{
						var Mfgdate= RFDateFormat(getMfgDate);
						if(Mfgdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Mfgdate',Mfgdate[1]);
							CIarray["custparam_mfgdate"]=Mfgdate[1];
							ValueMfgDate=Mfgdate[1];
						}
						else {
							errorflag='T';
							error='MfgDate:'+Mfgdate[1];
						}
					}
					if (getMfgDate == '' && getExpDate != '')
					{
						var Expdate= RFDateFormat(getExpDate);
						if(Expdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Expdate',Expdate[1]);
							CIarray["custparam_expdate"]=Expdate[1];
							ValueExpDate=Expdate[1];
						}
						else {
							errorflag='T';
							error=error+'Expdate:'+Expdate[1];
						}

					}
					if(getMfgDate != '' && getExpDate != '')
						{
						var Mfgdate= RFDateFormat(getMfgDate);
						if(Mfgdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Mfgdate',Mfgdate[1]);
							CIarray["custparam_mfgdate"]=Mfgdate[1];
							ValueMfgDate=Mfgdate[1];
						}
						else {
							errorflag='T';
							error='MfgDate:'+Mfgdate[1];
						}

						var Expdate= RFDateFormat(getExpDate);
						if(Expdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Expdate',Expdate[1]);
							CIarray["custparam_expdate"]=Expdate[1];
							ValueExpDate=Expdate[1];
						}
						else {
							errorflag='T';
							error=error+'Expdate:'+Expdate[1];
						}
						}
					if (getMfgDate != '' && getExpDate != '' && getBestBeforeDate != '') {

						var Mfgdate= RFDateFormat(getMfgDate);
						if(Mfgdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Mfgdate',Mfgdate[1]);
							CIarray["custparam_mfgdate"]=Mfgdate[1];
							ValueMfgDate=Mfgdate[1];
						}
						else {
							errorflag='T';
							error='MfgDate:'+Mfgdate[1];
						}

						var Expdate= RFDateFormat(getExpDate);
						if(Expdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Expdate',Expdate[1]);
							CIarray["custparam_expdate"]=Expdate[1];
							ValueExpDate=Expdate[1];
						}
						else {
							errorflag='T';
							error=error+'Expdate:'+Expdate[1];
						}

						var BestBeforedate= RFDateFormat(getBestBeforeDate);
						if(BestBeforedate[0]=='true')
						{
							nlapiLogExecution('ERROR','BestBeforedate',BestBeforedate[1]);
							CIarray["custparam_bestbeforedate"]=BestBeforedate[1];
						}
						else {
							errorflag='T';
							error=error+'BestBeforeDate:'+BestBeforedate[1];
						}

					}
					nlapiLogExecution('ERROR','error',error);
					if(errorflag=='F')
					{
						//Checking Condn that MfgDate shld not be greater than or equal to ExpDate. 
						nlapiLogExecution('ERROR','ValueMfgDate',ValueMfgDate);
						nlapiLogExecution('ERROR','ValueExpDate',ValueExpDate);
						
						var currdate = DateStamp();
						nlapiLogExecution('DEBUG','currdate',currdate);
						if (Date.parse(ValueExpDate) <= Date.parse(currdate)) 
								{
								nlapiLogExecution('DEBUG','currdate new',currdate);
								CIarray["custparam_error"] = "The Expiry Date should not be less than or equal to the current date";
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
								return;
								}

						var d = new Date();
						var vExpiryDate='';
						if(ValueExpDate ==null || ValueExpDate =='') //if expiryDate is null 
						{
							var vExpiryDate='';
							if(itemShelflife !=null && itemShelflife!='')
							{
								d.setDate(d.getDate()+parseInt(itemShelflife));
								vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
							}
							else
							{
								vExpiryDate='01/01/2099';										     
							}
							CIarray["custparam_expdate"]=vExpiryDate;
							nlapiLogExecution('ERROR','vExpiryDate',vExpiryDate);
							ValueExpDate=vExpiryDate;
						}
						nlapiLogExecution('ERROR','ValueExpDate',ValueExpDate);

						if(ValueMfgDate == null || ValueMfgDate=='')
							CIarray["custparam_mfgdate"]='';
						if(CIarray["custparam_bestbeforedate"]==null || CIarray["custparam_bestbeforedate"]=='')
							CIarray["custparam_bestbeforedate"]='';


						if (Date.parse(ValueMfgDate) >= Date.parse(ValueExpDate)) 
						{
							CIarray["custparam_error"] = st8;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
						}
						else
						{
							if(CaptureFifodate =='F')
							{
								//If ItemCapatureFifoDate Unchecked
								CIarray["custparam_fifodate"]=DateStamp();
								CIarray["custparam_lastdate"]='';
								CIarray["custparam_fifocode"]='';

								var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
								customrecord.setFieldValue('name', CIarray["custparam_BatchNo"]);
								customrecord.setFieldValue('custrecord_ebizlotbatch', CIarray["custparam_BatchNo"]);
								customrecord.setFieldValue('custrecord_ebizmfgdate', CIarray["custparam_mfgdate"]);
								customrecord.setFieldValue('custrecord_ebizbestbeforedate', CIarray["custparam_bestbeforedate"]);
								customrecord.setFieldValue('custrecord_ebizexpirydate', CIarray["custparam_expdate"]);
								customrecord.setFieldValue('custrecord_ebizfifodate', CIarray["custparam_fifodate"]);
								customrecord.setFieldValue('custrecord_ebizfifocode', CIarray["custparam_fifocode"]);
								customrecord.setFieldValue('custrecord_ebizlastavldate', CIarray["custparam_lastdate"]);
								if(locationId!=null && locationId!='')
								customrecord.setFieldValue('custrecord_ebizsitebatch', locationId);
								nlapiLogExecution('ERROR', '1');
								nlapiLogExecution('ERROR', 'Item Id',ItemId);
								customrecord.setFieldValue('custrecord_ebizsku', ItemId);
//								customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);
								//customrecord.setFieldValue('custrecord_ebizsitebatch',CIarray["custparam_locationId"]);
								var rec = nlapiSubmitRecord(customrecord, false, true);

								nlapiLogExecution('ERROR', 'rec',rec);
								nlapiLogExecution('ERROR', 'getBatchNo',getBatchNo);
								CIarray["custparam_BatchId"] = rec;
								CIarray["custparam_BatchNo"] = getBatchNo;										

								response.sendRedirect('SUITELET', 'customscript_rf_create_invt_itemstat', 'customdeploy_rf_create_invt_itemstat_di', false, CIarray);
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_rf_create_invt_fifodate', 'customdeploy_rf_create_invt_fifodate_di', false, CIarray);
							}

						}
						//response.sendRedirect('SUITELET', 'customscript_rf_create_invt_fifodate', 'customdeploy_rf_create_invt_fifodate_di', false, CIarray);
					}
					else
					{
						CIarray["custparam_error"] = error;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					}
				}
				else {
					CIarray["custparam_error"] = st9;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				}
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
			nlapiLogExecution('ERROR', 'Catch: Location not found',e);

		}
	}
}
