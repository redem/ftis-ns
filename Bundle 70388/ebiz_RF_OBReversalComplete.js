/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_OBReversalComplete.js,v $
 *     	   $Revision: 1.1.2.3.4.3.4.7 $
 *     	   $Date: 2014/06/13 13:07:50 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_OBReversalComplete.js,v $
 * Revision 1.1.2.3.4.3.4.7  2014/06/13 13:07:50  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.3.4.3.4.6  2014/06/06 07:06:58  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.3.4.3.4.5  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3.4.3.4.4  2013/12/16 11:16:04  rmukkera
 * Case # 20126321�
 *
 * Revision 1.1.2.3.4.3.4.3  2013/11/28 06:06:31  skreddy
 * Case# 20125929
 * Afosa SB issue fix
 *
 * Revision 1.1.2.3.4.3.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.3.4.3.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.3.4.3  2012/12/17 23:00:13  kavitha
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal Process Issue fixes
 *
 * Revision 1.1.2.3.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.3.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.3  2012/09/14 20:12:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.1.2.2  2012/06/25 14:54:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.1.2.1  2012/06/21 10:17:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.4.4.2  2012/03/19 10:08:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.4.1  2012/02/22 12:35:52  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4  2011/08/24 12:41:49  schepuri
 * CASE201112/CR201113/LOG201121
 * RF PutAway complete based on putseq no
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function ReversalComplete(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		nlapiLogExecution('DEBUG', 'Into GET');
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7;
		
		//case20125929 start : for spanish conversion
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{		
			//case20125929 end
			st1 = "PRESIONE F8 PARA SCAN NUEVA TAREA";
			st2 = "PRESIONE F8 PARA SCAN NUEVA UCC";
			st3 = "INVERSI&#211;N COMPLETA";
			st4 = "INVERSI&#211;N DE HECHO.";
			st5 = "NEXT";
		}
		else
		{
			st1 = "PRESS F8 TO SCAN NEW TASK";
			st2 = "PRESS F8 TO SCAN NEW UCC";
			st3 = "REVERSAL COMPLETE";
			st4 = "REVERSAL COMPLETED";
			st5 = "NEXT";
			
			
		}

		var getReversalBy = request.getParameter('custparam_uccreversalby');
		//Case # 20126321��start
		var getReversalType = request.getParameter('custparam_uccreversaltype');
		//Case # 20126321��end
		var vMessage='';

		if(getReversalBy!=null && getReversalBy!='')
			vMessage=st1;//'PRESS F8 TO SCAN NEW TASK';
		else
			vMessage=st2;//'PRESS F8 TO SCAN NEW UCC';	

		var functionkeyHtml=getFunctionkeyScript('_rfpickingmenu'); 
		var html = "<html><head><title>"+ st3 +"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('cmdSend').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfpickingmenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st4 +"</td></tr><tr>";
		html = html + "				<td align = 'left'>"+ vMessage +"";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnReversalby' value=" + getReversalBy + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		//Case # 20126017�start
		html = html + "				<input type='hidden' name='hdnReversalType' value=" + getReversalType + ">";
		//Case # 20126017�end
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +" <input name='cmdSend' id='cmdSend' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Post');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		
		var getLanguage =  request.getParameter('hdngetLanguage');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		//case20125929 start : for spanish conversion
		var SOarray=new Array();
		SOarray["custparam_language"] = getLanguage;	
		//case20125929 end
		var optedEvent = request.getParameter('hdnflag');
		var vreversalby = request.getParameter('hdnReversalby');
		nlapiLogExecution('DEBUG', 'optedEvent',optedEvent);
		nlapiLogExecution('DEBUG', 'vreversalby',vreversalby);
		var getEbizOrdNo = request.getParameter('custparam_uccebizordno');
		var getEbizItemNo = request.getParameter('custparam_uccebizitem');
		var getCartonNo = request.getParameter('custparam_ucccontlpno');
        var getReversalType=request.getParameter('hdnReversalType');
		if (optedEvent == 'F8') 
		{
			if(vreversalby!=null && vreversalby!='')
			{

				//Case # 20126321��start
				if(getEbizOrdNo != null || getEbizItemNo != null || getCartonNo != null)
				{
					var taskFilters = new Array();
					var taskColumns = new Array();

					if(getReversalType.indexOf('C') != -1 && getCartonNo!=null && getCartonNo!="")
						taskFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getCartonNo));
					if(getReversalType.indexOf('O') != -1 && getEbizOrdNo!=null && getEbizOrdNo!="")
						taskFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', getEbizOrdNo));
					if(getReversalType.indexOf('I') != -1 && getEbizItemNo!=null && getEbizItemNo!="")
						taskFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', getEbizItemNo));
					// 3 - PICK
					taskFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); 
					// 7 - BUILD SHIP UNIT 	8 - PICK CONFIRMED	28 - PACK COMPLETE
					taskFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [7,8,28]));

					taskColumns[0] = new nlobjSearchColumn('custrecord_sku');
					taskColumns[1] = new nlobjSearchColumn('custrecord_packcode');
					taskColumns[2] = new nlobjSearchColumn('custrecord_act_qty');
					taskColumns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					taskColumns[4] = new nlobjSearchColumn('custrecord_line_no');
					taskColumns[5] = new nlobjSearchColumn('custrecord_wms_status_flag');
					taskColumns[6] = new nlobjSearchColumn('custrecord_ebiz_ship_lp_no');
					taskColumns[7] = new nlobjSearchColumn('custrecord_wms_location');
					taskColumns[8] = new nlobjSearchColumn('custrecord_comp_id');
					taskColumns[9] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
					taskColumns[10] = new nlobjSearchColumn('custrecord_invref_no');
					taskColumns[11] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
					taskColumns[12] = new nlobjSearchColumn('name');
					taskColumns[13] = new nlobjSearchColumn('custrecord_total_weight');
					taskColumns[14] = new nlobjSearchColumn('custrecord_totalcube');
					taskColumns[15] = new nlobjSearchColumn('custrecord_uom_level');
					taskColumns[16] = new nlobjSearchColumn('custrecord_actendloc');
					taskColumns[17] = new nlobjSearchColumn('custrecord_container_lp_no');

					var taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);
					if(taskSearchResults !=null && taskSearchResults!='')
					{
						var UCCarray=new Array();

						UCCarray["custparam_uccitem"] = taskSearchResults[0].getText('custrecord_sku');
						UCCarray["custparam_uccebizitem"] = taskSearchResults[0].getValue('custrecord_sku');
						UCCarray["custparam_uccpackcode"] = taskSearchResults[0].getValue('custrecord_packcode');
						UCCarray["custparam_uccactqty"] =taskSearchResults[0].getValue('custrecord_act_qty');
						UCCarray["custparam_uccordno"] = taskSearchResults[0].getText('custrecord_ebiz_order_no');
						UCCarray["custparam_uccebizordno"] = taskSearchResults[0].getValue('custrecord_ebiz_order_no');
						UCCarray["custparam_uccordlineno"] = taskSearchResults[0].getValue('custrecord_line_no');
						UCCarray["custparam_uccwmsstatus"] = taskSearchResults[0].getValue('custrecord_wms_status_flag');
						UCCarray["custparam_uccshiplpno"] = taskSearchResults[0].getValue('custrecord_ebiz_ship_lp_no');
						UCCarray["custparam_ucclocation"] = taskSearchResults[0].getValue('custrecord_wms_location');  
						UCCarray["custparam_ucccompany"] = taskSearchResults[0].getValue('custrecord_comp_id');
						UCCarray["custparam_uccnsrefno"] =  taskSearchResults[0].getValue('custrecord_ebiz_nsconfirm_ref_no');
						UCCarray["custparam_uccinvrefno"] = taskSearchResults[0].getValue('custrecord_invref_no');
						UCCarray["custparam_uccfointrid"] =taskSearchResults[0].getValue('custrecord_ebiz_cntrl_no');
						UCCarray["custparam_ucctaskintrid"] = taskSearchResults[0].getId();
						UCCarray["custparam_uccname"] = taskSearchResults[0].getValue('name');
						UCCarray["custparam_ucctaskweight"] = taskSearchResults[0].getValue('custrecord_total_weight');
						UCCarray["custparam_ucctaskcube"] = taskSearchResults[0].getValue('custrecord_totalcube');
						UCCarray["custparam_uccuomlevel"] = taskSearchResults[0].getValue('custrecord_uom_level');
						UCCarray["custparam_uccendlocation"] = taskSearchResults[0].getText('custrecord_actendloc');
						//	UCCarray["custparam_uccendlocation"] = request.getParameter('hdnendlocation');
						UCCarray["custparam_ucccontlpno"] = taskSearchResults[0].getValue('custrecord_container_lp_no');
						UCCarray["custparam_uccreversalby"] = request.getParameter('hdnReversalby');
						UCCarray["custparam_uccreversaltype"] = request.getParameter('hdnReversalType');
						response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalcontinue', 'customdeploy_rf_outboundreversalcontinue', false, UCCarray);
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalbytask', 'customdeploy_rf_outboundreversalbytask', false, SOarray);
					}
				}
				//Case # 20126321��end

			}
			else
				response.sendRedirect('SUITELET', 'customscript_rf_scanucclabel', 'customdeploy_rf_scanucclabel', false, SOarray);				
		}

	}
}
