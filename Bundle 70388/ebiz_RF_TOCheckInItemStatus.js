/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_TOCheckInItemStatus.js,v $
 *     	   $Revision: 1.2.2.11.4.5.2.12.2.3 $
 *     	   $Date: 2015/12/02 15:08:37 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TOCheckInItemStatus.js,v $
 * Revision 1.2.2.11.4.5.2.12.2.3  2015/12/02 15:08:37  grao
 * 2015.2 Issue Fixes 201415937
 *
 * Revision 1.2.2.11.4.5.2.12.2.2  2015/11/27 15:31:01  nneelam
 * case# 201415891
 *
 * Revision 1.2.2.11.4.5.2.12.2.1  2015/11/14 12:42:38  aanchal
 * 2015.2 Issue fix
 * 201415633
 *
 * Revision 1.2.2.11.4.5.2.12  2015/07/23 15:28:40  grao
 * 2015.2   issue fixes  201412876
 *
 * Revision 1.2.2.11.4.5.2.11  2015/04/27 13:28:16  schepuri
 * case# 201412498
 *
 * Revision 1.2.2.11.4.5.2.10  2015/02/03 13:50:33  schepuri
 * issue # 201411370
 *
 * Revision 1.2.2.11.4.5.2.9  2014/11/04 15:16:31  sponnaganti
 * Case# 201410597
 * DCD SB Issue fixed
 *
 * Revision 1.2.2.11.4.5.2.8  2014/07/25 15:09:58  skavuri
 * Case# 20149685/20149687 Compatibility issue fixed
 *
 * Revision 1.2.2.11.4.5.2.7  2014/06/13 05:57:32  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.2.11.4.5.2.6  2014/06/06 06:10:44  skavuri
 * Case# 20148749 (Refresh Functionality ) SB Issue Fixed
 *
 * Revision 1.2.2.11.4.5.2.5  2014/05/30 00:26:51  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.2.11.4.5.2.4  2014/02/04 16:36:59  skavuri
 * case # 20126898 (for not displaying default item status no)
 *
 * Revision 1.2.2.11.4.5.2.3  2013/07/02 15:17:23  gkalla
 * Case# 20123214, Case# 20122713
 * Standard bundle Issue Fix
 *
 * Revision 1.2.2.11.4.5.2.2  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.2.11.4.5.2.1  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.2.11.4.5  2013/02/15 14:59:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.2.2.11.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.11.4.3  2012/09/27 10:55:14  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.2.2.11.4.2  2012/09/26 12:48:52  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.2.11.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.2.2.11  2012/08/21 22:38:41  spendyala
 * CASE201112/CR201113/LOG201121
 * Redirecting to user to scan lot no even though the item is not lot controled item.
 *
 * Revision 1.2.2.10  2012/07/31 06:56:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Item Status was resolved.
 *
 * Revision 1.2.2.9  2012/06/11 13:41:33  spendyala
 * CASE201112/CR201113/LOG201121
 * Invalid parameter passing.
 *
 * Revision 1.2.2.8  2012/05/18 17:59:59  gkalla
 * CASE201112/CR201113/LOG201121
 * Display all item statuses
 *
 * Revision 1.2.2.7  2012/05/03 17:07:56  gkalla
 * CASE201112/CR201113/LOG201121
 * For TO Checkin in
 *
 * Revision 1.2.2.6  2012/05/02 06:19:06  spendyala
 * CASE201112/CR201113/LOG201121
 * Fetching Itemstatus issue is resolved.
 *
 * Revision 1.2.2.5  2012/04/16 08:35:24  vrgurujala
 * t_NSWMS_LOG201121_120
 *
 * Revision 1.2.2.4  2012/04/11 22:06:57  spendyala
 * CASE201112/CR201113/LOG201121
 * Navigation to previous screen issue is resolved.
 *
 * Revision 1.2.2.3  2012/04/11 12:36:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * show item status based on PO location
 *
 * Revision 1.2.2.2  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.2.1  2012/02/23 13:25:33  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix related to check null values
 *
 * Revision 1.2  2011/12/27 15:36:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.1  2011/09/29 10:43:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Transfer Order Item status screen
 *
 * 
 *
 *
 *****************************************************************************/

function CheckInItemStatus(request, response){
	if (request.getMethod() == 'GET') {
		var ItemStatus;
		var itemStatusId;
		var itemStatusLoopCount = 0;
		var nextClicked;

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		var getItemBaseUOM =request.getParameter('custparam_baseuomqty');

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var getWHLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG','getWHLocation',getWHLocation);

		var pickfaceEnteredOption = request.getParameter('custparam_enteredOption');

		var itemStatusFilters = new Array();
		itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
		itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
		itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', [getWHLocation]);

		/*	var itemStatusColumns = new Array();
		itemStatusColumns[0] = new nlobjSearchColumn('name');
		itemStatusColumns[1] = new nlobjSearchColumn('internalid');
		itemStatusColumns[2] = new nlobjSearchColumn('custrecord_defaultskustaus');

		// Case # 20126898?end
		itemStatusColumns[0].setSort();
		itemStatusColumns[1].setSort();*/


		var itemStatusColumns = new Array();
		itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
		itemStatusColumns[1] = new nlobjSearchColumn('name');
		itemStatusColumns[2] = new nlobjSearchColumn('custrecord_defaultskustaus');
		itemStatusColumns[0].setSort();
		itemStatusColumns[1].setSort();

		var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);	




		var  itemStatusCount; 

		var itemStatusCount=0;
		if(itemStatusSearchResult!=null&&itemStatusSearchResult!="")
			itemStatusCount = itemStatusSearchResult.length;

		//if (request.getParameter('custparam_count') != null  && (itemStatusSearchResult!='' && itemStatusSearchResult!=''))
		if (request.getParameter('custparam_count') != null)
		{
			//itemStatusCount = itemStatusSearchResult.length;
			var itemStatusRetrieved = request.getParameter('custparam_count');
			nlapiLogExecution('DEBUG', 'itemStatusRetrieved', itemStatusRetrieved);

			nextClicked = request.getParameter('custparam_nextclicked');
			nlapiLogExecution('DEBUG', 'nextClicked', nextClicked);

			itemStatusCount = itemStatusCount - itemStatusRetrieved;
			nlapiLogExecution('DEBUG', 'itemStatusCount', itemStatusCount);
		}
		else
		{
			var itemStatusRetrieved = 0;
			nlapiLogExecution('DEBUG', 'itemStatusRetrieved', itemStatusRetrieved);
		}

		/*if (itemStatusCount > 4)
		{
			itemStatusLoopCount = 4;
		}
		else
		{
			itemStatusLoopCount = itemStatusCount;
		}*/
		itemStatusLoopCount = itemStatusCount;

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INGRESAR SELECCI&#211;N";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";			

		}
		else
		{
			st0 = "";
			st1 = "ENTER ITEM STATUS";
			st2 = "SEND";
			st3 = "PREV";

		}


		var html = "<html><head><title>" + st0 +  "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript'>";
		html = html + "function OnKeyDown_CL() ";
		html = html + " { ";     
		html = html + "         if (";
		html = html + " event.keyCode == 112 || event.keyCode == 113 || event.keyCode == 114 || event.keyCode == 115 || event.keyCode == 116 || event.keyCode == 117 ||";
		html = html + " event.keyCode == 118 || event.keyCode == 119 || event.keyCode == 120 || event.keyCode == 121 || event.keyCode == 122) {";
		html = html + " var arrElements = document.getElementsByTagName('input');";
		html = html + " var keyFound = false;";
		html = html + " for (i = 0; i < arrElements.length; i++) {";
		html = html + " if (arrElements[i].type == 'submit') {";
		html = html + " switch (event.keyCode) {";
		html = html + " case 112:";
		html = html + " if (arrElements[i].value == 'F1')";	//F7 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 113:";
		html = html + " if (arrElements[i].value == 'F2')";		//F8 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 114:";
		html = html + " if (arrElements[i].value == 'F3')";	//F9 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 115:";
		html = html + " if (arrElements[i].value == 'F4')";		//F10 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 116:";
		html = html + " if (arrElements[i].value == 'F5')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 117:";
		html = html + " if (arrElements[i].value == 'F6')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 118:";
		html = html + " if (arrElements[i].value == 'F7')";		//F7 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 119:";
		html = html + " if (arrElements[i].value == 'F8')";		//F8 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 120:";
		html = html + " if (arrElements[i].value == 'F9')";		//F9 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 121:";
		html = html + " if (arrElements[i].value == 'F10')";		//F10 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 122:";
		html = html + " if (arrElements[i].value == 'F11')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " }";
		html = html + " if (keyFound == true) {";        
		html = html + " eval('document._rf_checkin_po.' + arrElements[i].name + '.click();');";
		html = html + " return false;";
		html = html + " }";
		html = html + " }";
		html = html + " }";
		html = html + " }    ";        
		html = html + "    return true; ";
		html = html + "    }";        
		html = html + " </SCRIPT>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterstatus').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";		
		html = html + "</head><body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_itemstatus' method='POST'>";
		html = html + "		<table>";

		nlapiLogExecution('DEBUG', 'itemStatusLoopCount', itemStatusLoopCount);
		var StatusNo=1;
		for (var i = itemStatusRetrieved; i < (parseFloat(itemStatusRetrieved) + parseFloat(itemStatusLoopCount)); i++) {
			var itemStatusSearchResults = itemStatusSearchResult[i];
// case# 201416769
			if (itemStatusSearchResults != null)
			{
				itemStatus = itemStatusSearchResults.getValue('name');
				itemStatusId = itemStatusSearchResults.getId();				
			}
			var value = parseFloat(i) + 1;
			html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";   
			nlapiLogExecution('DEBUG', 'getPOLineItemStatus', getPOLineItemStatus);
			nlapiLogExecution('DEBUG', 'itemStatus', itemStatus);
			
			if(getPOLineItemStatus==itemStatus)
			{
				StatusNo=value;

				nlapiLogExecution('DEBUG', 'i', i);
				nlapiLogExecution('DEBUG', 'value', value);
				nlapiLogExecution('DEBUG', 'StatusNo', StatusNo);
				nlapiLogExecution('DEBUG', 'itemStatusSearchResult[i].getValue(custrecord_defaultskustaus)', itemStatusSearchResult[i].getValue('custrecord_defaultskustaus'));

				if (itemStatusSearchResult[i].getValue('custrecord_defaultskustaus') == 'T') // case# 201411370
				{
					StatusNo=value;

				}
			}
			nlapiLogExecution('DEBUG', 'StatusNo after', StatusNo);
		}

		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>ENTER ITEM STATUS : <label>" + getPOLineItemStatus + "</label>";
		html = html + "			<tr><td align = 'left'>" + st1 + "</td></tr> ";
		html = html + "			<tr>";
		// case # 20126898 starts (for not displaying default item status no)
		/*html = html + "				<td align = 'left'><input name='enterstatus' type='text' value='"+ StatusNo +"'/>";*/
		//Case # 20149685 (Compatibility Issue)starts
		//	html = html + "				<td align = 'left'><input name='enterstatus' type='text' value=''/>";//case# 20126898 ends

		html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' value='"+ StatusNo +"'/>";

		//Case # 20149685 (Compatibility Issue) ends
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getPOLineItemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnItemBaseUOM' value=" + getItemBaseUOM + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnNextClicked' value=" + nextClicked + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnPickfaceEnteredOption' value=" + pickfaceEnteredOption + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + StatusNo + "></td>";
		html = html + "				<input type='hidden' name='hdnCount' value=" + i + "></td>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		//if (itemStatusLoopCount > 4)
		/*if (itemStatusCount > 4)
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>NEXT <input name='cmdNext' type='submit' value='F8'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}*/
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterstatus').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
//		Commented by Phani 04-08-2011    	
//		var getItemStatus = request.getParameter('enterstatus');
		var getReturnedItemStatus = request.getParameter('hdnItemStatus');

//		Added by Phani 04-08-2011
		var getItemStatusOption = request.getParameter('enterstatus');

		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');

		var nextClicked = request.getParameter('hdnNextClicked');
		nlapiLogExecution('DEBUG', 'nextClicked', nextClicked);

		var getWHLocation= request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'getWHLocation',getWHLocation);

		// This variable is to hold the Quantity entered.
		var POarray = new Array();

//		var itemStatus;


		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		var st4,st5,st6,st7;
		if( getLanguage == 'es_ES')
		{
			st4 = "ESTADO ELEMENTO NO V&#193;LIDO";
			st5 = "USTED NO PUEDE RECIBIR CON EL ESTADO";

		}
		else
		{
			st4 = "INVALID ITEM STATUS";
			st5 = "YOU CANNOT RECEIVE WITH THIS STATUS";

		}

		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_StatusNo"] = request.getParameter('hdnItemStatusNo');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
		POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode'); //request.getParameter('custparam_polinepackcode');
		//POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
		//POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_polinequantity'); //request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_polinequantityreceived'); //request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_enteredOption"] = request.getParameter('hdnPickfaceEnteredOption');

		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');

		nlapiLogExecution('DEBUG', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);

		POarray["custparam_error"] = st4;
		POarray["custparam_screenno"] = '4T';

		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');

		//        var ActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');

		//		var TimeArray = new Array();
		//		TimeArray = ActualBeginTime.split(' ');
		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; //TimeArray[1];
		nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_poitem"] ', POarray["custparam_poitem"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_poid"] ', POarray["custparam_poid"]);

		// Added by Phani on 03-25-2011
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		nlapiLogExecution('DEBUG', 'optedField', POarray["custparam_option"]);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent; //= request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
//		if (optedEvent == 'F7') {		
		if (request.getParameter('cmdPrevious') == 'F7') {
			if (nextClicked != 'Y')
			{
				response.sendRedirect('SUITELET', 'customscript_rf_to_checkinqty', 'customdeploy_rf_tocheckin_qty_di', false, POarray);
			}
			else
			{
				POarray["custparam_count"] = 0;
				nlapiLogExecution('DEBUG', 'nextClicked', nextClicked);
				nlapiLogExecution('DEBUG', 'Next in F7', POarray["custparam_count"]);
				response.sendRedirect('SUITELET', 'customscript_rf_itemstatus', 'customdeploy_rf_itemstatus_di', false, POarray);
			}
		}
		//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
		else if (request.getParameter('cmdNext') == 'F8') {
			POarray["custparam_count"] = request.getParameter('hdnCount');
			POarray["custparam_nextclicked"] = "Y";

			nlapiLogExecution('DEBUG', 'Next', POarray["custparam_count"]);
			response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
		}
		else// if (request.getParameter('cmdSend') == 'ENT')
		{

			var itemSubtype;
			var batchflag="F";
			var serialInflg="F";	
			if(POarray["custparam_fetcheditemid"] != null && POarray["custparam_fetcheditemid"] != '')
			{
				var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
				var columns = nlapiLookupField('item', POarray["custparam_fetcheditemid"], fields);
				//var columns = nlapiLookupField('item', POarray["custparam_fetcheditemid"], 'recordType');

				if(columns != null && columns != '')
				{				
					itemSubtype = columns.recordType;
					serialInflg = columns.custitem_ebizserialin;
					batchflag= columns.custitem_ebizbatchlot;
					nlapiLogExecution('DEBUG', 'citem subtype is =', itemSubtype);
				}
			}
//			Added by Phani on 04-08-2011            
			if (getItemStatusOption == "")
			{
				//	if the 'Send' button is clicked without any option value entered,
				//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Entered Item Status', 'No data');
			}

			/* 
			 * This is commented as the item status has to drive based on the menu rather than the default item status 
			 *            if (getItemStatus == "") {
                if (getReturnedItemStatus == "") {
                //	if the 'Send' button is clicked without any option value entered,
                //  it has to show an error message. The next screen to which it has to navigate is to the error screen.
                response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
                nlapiLogExecution('DEBUG', 'Entered Item Status', 'No data');
                 }
                else {
                    var defstatus;
                    var defsearchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T'));
                    if (defsearchresults != null && defsearchresults.length > 0) {
                        defstatus = defsearchresults[0].getId();
                        nlapiLogExecution('DEBUG', 'DefStatus', defstatus);
                        if (itemSubtype == 'lotnumberedinventoryitem') {
                            POarray["custparam_polineitemstatus"] = defstatus;// request.getParameter('hdnItemStatus');
                            response.sendRedirect('SUITELET', 'customscript_rf_checkin_batch_no', 'customdeploy_rf_checkin_batch_no_di', false, POarray);
                            nlapiLogExecution('DEBUG', 'Routing towards LOT/BATCH screens');
                        }
                        else {

                            POarray["custparam_polineitemstatus"] = defstatus;// request.getParameter('hdnItemStatus');
                            response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
                        }
                    }

			else {
				POarray["custparam_error"] = 'NO DEFAULT ITEM STATUS';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Item Status Not Found as DEFAULT.Please check Default checkbox');
			}
		}
	}
			 */
			else {
				/*
				 * Added by Phani on 04-08-2011.
				 * This part of the code is to fetch the Item Status from the option selected from the Item Status menu.
				 */
				nlapiLogExecution('DEBUG', 'Item Status Option', getItemStatusOption);

				var itemStatusFilters = new Array();
				itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
				itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
				if(getWHLocation!=null && getWHLocation!="")
					itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', [getWHLocation]);

				/*	var itemStatusColumns = new Array();
				itemStatusColumns[0] = new nlobjSearchColumn('name');
				itemStatusColumns[1] = new nlobjSearchColumn('custrecord_allowrcvskustatus');*/


//				case# 201412498
				/*var itemStatusColumns = new Array();
				itemStatusColumns[0] = new nlobjSearchColumn('name');
				itemStatusColumns[1] = new nlobjSearchColumn('internalid');
				itemStatusColumns[2] = new nlobjSearchColumn('custrecord_defaultskustaus');
				itemStatusColumns[3] = new nlobjSearchColumn('custrecord_allowrcvskustatus');
				itemStatusColumns[0].setSort();*/

				var itemStatusColumns = new Array();
				itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
				itemStatusColumns[1] = new nlobjSearchColumn('name');
				itemStatusColumns[2] = new nlobjSearchColumn('custrecord_defaultskustaus');
				itemStatusColumns[3] = new nlobjSearchColumn('custrecord_allowrcvskustatus');

				itemStatusColumns[0].setSort();
				itemStatusColumns[1].setSort();

				var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);



				var count = request.getParameter('hdnCount');
				nlapiLogExecution('DEBUG', 'count', count);

				if (parseFloat(count) >= parseFloat(getItemStatusOption))
				{
					if (itemStatusSearchResult != null && itemStatusSearchResult!='')
					{
						var itemStatusCount = itemStatusSearchResult.length;
						var ItemStatusRcvFlag;
						var itemStatus='';
						for (var i = 0; i <= getItemStatusOption; i++) {
							var itemStatusSearchResults = itemStatusSearchResult[i];
							if (parseFloat(getItemStatusOption) == parseFloat(i+1))
							{
								nlapiLogExecution('DEBUG', 'here');
								itemStatus = itemStatusSearchResults.getValue('name');//
								itemStatusId = itemStatusSearchResults.getId();
								nlapiLogExecution('DEBUG', 'Latest Item Status', itemStatus);
								//	ItemStatusRcvFlag=itemStatusSearchResults.getValue(itemStatusColumns[1]);
								ItemStatusRcvFlag=itemStatusSearchResults.getValue(itemStatusColumns[3]);
							}
							else
							{
								POarray["custparam_error"] = st4;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Item Status entered is not a valid status');
							}
						}
						nlapiLogExecution('DEBUG', 'ItemStatusRcvFlag', ItemStatusRcvFlag);
						if(ItemStatusRcvFlag=='T')
						{
							if (itemStatus != null)
							{
								/*           	
								 * This part of the code is commented on 04-08-2011 by Phani.
								 * The item status screen on the RF is changed in order to display all the item status available.
								 * Based on the list, the user can select any option required.

                var chksearchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, new nlobjSearchFilter('name', null, 'is', request.getParameter('enterstatus')));
                if (chksearchresults != null && chksearchresults.length > 0) {
                    nlapiLogExecution('DEBUG', 'Check Status Whether Exists OR Not', chksearchresults[0].getId());
								 */
								if (itemSubtype == 'lotnumberedinventoryitem' || itemSubtype=="lotnumberedassemblyitem" || batchflag=="T") {
									//POarray["custparam_polineitemstatus"] = itemStatusId;	//chksearchresults[0].getId();
									POarray["custparam_polineitemstatus"] = itemStatus;//case# 201410597 when we press F7 in batch screen not able to retrieve same item status
									response.sendRedirect('SUITELET', 'customscript_rf_checkin_batch_no', 'customdeploy_rf_checkin_batch_no_di', false, POarray);
									nlapiLogExecution('DEBUG', 'Routing towards LOT/BATCH screens');
								}
								else {
									//POarray["custparam_polineitemstatus"] = itemStatusId;	//chksearchresults[0].getId();
									POarray["custparam_polineitemstatus"] = itemStatus;//case# 201410597 when we press F7 in batch screen not able to retrieve same item status
									response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
									nlapiLogExecution('DEBUG', 'Item Status ', POarray["custparam_polineitemstatus"]);
								}
							}
							else {
								POarray["custparam_error"] = st4;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Item Status Not Found as Valid in ITEMSTATUS RECORD');
							}
						}
						else
						{
							POarray["custparam_error"] = st5;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'RCV Status flag F');

						}
					}
				}
				else {
					POarray["custparam_error"] = st4;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Item Status entered is not a valid status');
				}
				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			}
		}
	}
}
