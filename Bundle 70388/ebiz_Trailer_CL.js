/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_Trailer_CL.js,v $
 *     	   $Revision: 1.6.4.2 $
 *     	   $Date: 2012/08/14 15:12:00 $
 *     	   $Author: mbpragada $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Trailer_CL.js,v $
 * Revision 1.6.4.2  2012/08/14 15:12:00  mbpragada
 * 20120490
 * validationon trailer length
 *
 * Revision 1.5  2011/09/27 14:37:01  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 
 *****************************************************************************/
function fnCheckTrailerEntry(type)
{
	var trailername = nlapiGetFieldValue('name');        
	var vSite = nlapiGetFieldValue('custrecord_ebizsitetrailer');
	var vCompany = nlapiGetFieldValue('custrecord_ebizcompanytrailer');
	var appointment	= nlapiGetFieldValue('custrecord_ebizappointmenttrailer');
	//alert(trailername);
	var vId1 = nlapiGetFieldValue('id');
	var filters1 = new Array();
	filters1[0] = new nlobjSearchFilter('name', null, 'is', trailername);
	if (vId1 != null && vId1 != "") {
		filters1[1] = new nlobjSearchFilter('internalid', null, 'noneof', vId1);
	}
	var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_trailer', null, filters1);

	if (searchresults1 != null && searchresults1.length > 0) 
	{
		alert("Trailer Name already exists.");
		return false;
	} 
	else
	{
		if(trailername.length > 15)
		{
			alert("Trailer Name length should be less than 15 characters");
			return false;
		}
		
	}
	if(appointment != null && appointment != "" )
	{   

		nlapiLogExecution('ERROR', 'INTO SCRIPT ','DONE');
		var vId = nlapiGetFieldValue('id');
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebizappointmenttrailer', null, 'is', appointment);        
		filters[1] = new nlobjSearchFilter('custrecord_ebizsitetrailer', null, 'anyof', [vSite]);
		//filters[2] = new nlobjSearchFilter('custrecord_ebizcompanytrailer', null, 'anyof', [vCompany]);

		if (vId != null && vId != "") {
			filters[2] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
		}

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, filters);

		if (searchresults != null && searchresults.length > 0) 
		{
			//alert("Appointment/Trip# already exists.");
			//return false;
		} 
	}
	var iChars = "~!@#$%^&*()+=[]\\\';,./{}|\":<>? ";
	for (var i = 0; i < trailername.length; i++) 
	{    
		if (iChars.indexOf(trailername.charAt(i)) != -1)
		{ 
			alert ("Trailer name cannot have any of the following characters @ # $ % ^ & < > : / \ ? * and Space");   
			return false;      
		}  
	}
	return true;
}


