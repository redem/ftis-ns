/***************************************************************************
  eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_putaway_LP.js,v $
 *     	   $Revision: 1.1.2.2.4.17 $
 *     	   $Date: 2015/04/28 16:00:49 $
 *     	   $Author: nneelam $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_putaway_LP.js,v $
 * Revision 1.1.2.2.4.17  2015/04/28 16:00:49  nneelam
 * case# 201412527
 *
 * Revision 1.1.2.2.4.16  2015/01/21 13:40:39  schepuri
 * issue # 201411366
 *
 * Revision 1.1.2.2.4.15  2014/11/27 05:16:36  vmandala
 * case#  201411041 stdbundle issue fixed
 *
 * Revision 1.1.2.2.4.14  2014/07/25 13:45:03  skreddy
 * case # 20149507
 * jawbone SB issue fix
 *
 * Revision 1.1.2.2.4.13  2014/06/13 08:47:06  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2.4.12  2014/06/03 13:58:50  skreddy
 * case # 20148716
 * PCT prod issue fix
 *
 * Revision 1.1.2.2.4.11  2014/05/30 00:34:25  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.10  2014/04/16 16:07:47  gkalla
 * case#20148019
 * PCT WO outbound stage issue
 *
 * Revision 1.1.2.2.4.9  2014/02/21 14:55:55  rmukkera
 * Case# 20127203
 *
 * Revision 1.1.2.2.4.8  2014/01/09 14:08:58  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.2.4.7  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.2.4.6  2013/10/28 15:58:24  gkalla
 * Case# 20125380
 * Monobind RF WO issue for put strategies
 *
 * Revision 1.1.2.2.4.5  2013/09/19 15:19:13  rmukkera
 * Case# 20124445
 *
 * Revision 1.1.2.2.4.4  2013/06/14 07:05:45  gkalla
 * CASE201112/CR201113/LOG201121
 * PCT Case# 20122979. To select item status while RF WO confirm build
 *
 * Revision 1.1.2.2.4.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.2.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.2  2012/12/03 15:42:53  rmukkera
 * CASE201112/CR201113/LOG2012392
 * Issue fix Reference error
 *
 * Revision 1.1.2.1  2012/11/23 09:11:58  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 * Revision 1.32.4.29.4.5  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 *
 *****************************************************************************/
/**
 * @param request
 * @param response
 */
function PutawayLP(request, response){
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') {
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('ERROR', 'getOptedField', getOptedField);

		var getItemLP;

		var getWONo = request.getParameter('custparam_woname');
		var getWOItem = request.getParameter('custparam_item');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getWOInternalId = request.getParameter('custparam_woid');
		var getWOQtyEntered = request.getParameter('custparam_expectedquantity');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getnoofrecords =request.getParameter('custparam_noofrecords');
		var getwoStatus =request.getParameter('custparam_wostatus');
		var getWOLinePackCode = request.getParameter('custparam_packcode');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getFetchedLocationId = request.getParameter('custparam_beginlocinternalid');



		var getExpDate = request.getParameter('custparam_expdate');
		var getMfgDate=request.getParameter('custparam_mfgdate');
		var getBestBeforeDate=request.getParameter('custparam_bestbeforedate');
		var getFifoDate=request.getParameter('custparam_fifodate');
		var getLastAvlDate=request.getParameter('custparam_lastdate');
		var getBatchNo=request.getParameter('custparam_batchno');
		var getFifoCode = request.getParameter('custparam_fifocode');
		var getItemType = request.getParameter('custparam_itemtype');		

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getAutogenerateLp = request.getParameter('custparam_autogenerateLp');


		nlapiLogExecution('ERROR','getWHLocation',getWHLocation);
		nlapiLogExecution('ERROR','getBatchNo',getBatchNo);

		//var getWONo = request.getParameter('custparam_poid');
		//var getWOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		//var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		//var getWOInternalId = request.getParameter('custparam_pointernalid');
		//var getWOQtyEntered = request.getParameter('custparam_poqtyentered');
		//var getWOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getWOPackCode = request.getParameter('custparam_polinepackcode');
		var getWOItemStatusValue = request.getParameter('custparam_polineitemstatusValue');
		nlapiLogExecution('ERROR','getWOItemStatusValue',getWOItemStatusValue);

		var getWOItemStatus = request.getParameter('custparam_polineitemstatus');
		nlapiLogExecution('ERROR', 'Item Status', getWOItemStatus);
		nlapiLogExecution('ERROR', 'getWOQtyEntered', getWOQtyEntered);

		var getItemCube = request.getParameter('custparam_itemcube');

		var getBaseUomQty = request.getParameter('custparam_baseuomqty');
		nlapiLogExecution('ERROR', 'getBaseUomQty', getBaseUomQty);
		/*		var getItemQuantity = request.getParameter('hdnQuantity');
		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');*/
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');

		var pickfaceEnteredOption = request.getParameter('custparam_enteredOption'); 
		var vAutoLotFlag = request.getParameter('custparam_autolot'); 

		//var getWHLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('ERROR','vAutoLotFlag',vAutoLotFlag);



		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9,st10;

		if( getLanguage == 'es_ES')
		{
			st0 = "RECEPCI&#211;N DE MEN&#218;";
			st1 = "INGRESAR / ESCANEAR N&#218;MERO DE PEDIDO #";	
			st2 = "ENTRAR / EXPLORACI&#211;N PLACA";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st6 = "FABRICACION LOTE #";
			st7 = "LOT autogenerado # :";	
			st8 = "";
			st9 = "";
			st10 = "";

		}
		else
		{
			st0 = "WO#";
			st1 = "ASSEMBLY ITEM"; 
			st2 = "ENTER / SCAN LP";
			st3 = "BUILD ASSEMBLY";
			st4 = "PREV";
			st6 = "MANUFACTURE LOT# :";
			st7 = "Autogenerated LOT# :";
			st8 = "BUILD QTY";
			st9 = "LOT#";
			st10 = "AUTO ASSIGN  LP";

		}






		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_lp'); 
		var html = "<html><head><title>WORKORDER PUTAWAY</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		//html = html + " document.getElementById('enterlp').focus();";

		html = html + "</script>";       
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_checkin_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st0 + ":  <label>" + getWONo + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":  <label>" + getWOItem + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st8 + ":  <label>" + getWOQtyEntered + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";

		if(getBatchNo != null && getBatchNo!= '')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st9 + ":  <label>" + getBatchNo + "</label></td>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}


		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " :";
		html = html + "				<input type='hidden' name='hdnWOName' value=" + getWONo + ">";	
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getWOItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getWOPackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnbaseuomqty' value=" + getBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getWOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnWOQuantityEntered' value=" + parseFloat(getWOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnWOInternalId' value=" + getWOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdnLastAvlDate' value=" + getLastAvlDate + ">";
		html = html + "				<input type='hidden' name='hdnFifoDate' value=" + getFifoDate + ">";
		html = html + "				<input type='hidden' name='hdnFifoCode' value=" + getFifoCode + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnPickfaceEnteredOption' value=" + pickfaceEnteredOption + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnwoqtyenetered' value=" + getWOQtyEntered + ">";
		html = html + "				<input type='hidden' name='hdnAutoLotFlag' value=" + vAutoLotFlag + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + getWOItemStatusValue + "></td>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnBeginlocname' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginlocId' value=" + getFetchedLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getWOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnnoofrecords' value=" + getnoofrecords + ">";
		html = html + "				<input type='hidden' name='hdnAutoGenLp' value=" + getAutogenerateLp + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		if (request.getParameter('custparam_option') == '4') {
			html = html + "				<td align = 'left'><input name='entercartlp' type='text'/>";
			html = html + "				</td>";
		}
		else { 
			if(request.getParameter('custparam_autogenerateLp') != null && request.getParameter('custparam_autogenerateLp') != '')
			{
				nlapiLogExecution('ERROR', 'AutogenerateLp', getAutogenerateLp);

				html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text' value ="+getAutogenerateLp+" >";
				html = html + "				</td>";
			}
			else
			{
				html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
				html = html + "				</td>";
			}
		}

		html = html + "			</tr>";
		if(vAutoLotFlag=='Y' || vAutoLotFlag == true || vAutoLotFlag == 'TRUE')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> "+ st6 ;//MANUFACTURE LOT# :";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='entermanufactlot' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";

			html = html + "			<tr>";
			html = html + "				<td align = 'left'> "+ st7 ;//Autogenerated LOT# :";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + getBatchNo;
			html = html + "				</td>";
			html = html + "			</tr>";
		}	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdAutoLp.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st10 +" <input name='cmdAutoLp' type='submit' value='F8'/>";
		html = html + "					" + st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var getItemLP = "", getItemCartLP = "",getsysItemLP="";
		if (request.getParameter('enterlp') != null) {
			getItemLP = request.getParameter('enterlp');
		}
		if (request.getParameter('entercartlp') != null) {
			getItemCartLP = request.getParameter('entercartlp');
		}
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getAutoLotFlag = request.getParameter('hdnAutoLotFlag');
		var getManufactureLot = request.getParameter('entermanufactlot');

		// var isMastLP = nlapiLookupField('item', getFetchedItemId, 'custitem_scanmasterlp');
		nlapiLogExecution('ERROR', 'after enterlp', getItemLP);
		nlapiLogExecution('ERROR', 'custparam_option', request.getParameter('custparam_option'));
		if (request.getParameter('custparam_option') == '4') {
			//getItemLP = 'PALT' + GetMaxLPNo(1, 1);
			getItemLP = GetMaxLPNo(1, 1);
			getsysItemLP = GetMaxLPNo(1, 1);

			nlapiLogExecution('ERROR', 'get Item LP now added', getItemLP);
		}
		// This variable is to hold the Quantity entered.
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);


		var st5;
		if( getLanguage == 'es_ES')
		{
			st5 = "LP NO V&#193;LIDO";

		}
		else
		{
			st5 = "INVALID LP";
		}
		var st6="Location not Generated for this Item,check Itemcube and Putaway Strategies";

		var EndLocationArray = new Array();

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getWONo = request.getParameter('custparam_woname');
		var getWOItem = request.getParameter('custparam_item');
		var getPOLineNo = request.getParameter('custparam_lineno');

		var getWOInternalId = request.getParameter('custparam_woid');
		var getWOQtyEntered = request.getParameter('hdnwoqtyenetered');
		var getBeginLocId = request.getParameter('hdnBeginlocId');
		var getBeginLocName = request.getParameter('hdnBeginlocname');



		nlapiLogExecution('ERROR', 'getWOQtyEntered1', getWOQtyEntered);
		getWOQtyEntered = request.getParameter('hdnwoqtyenetered');
		nlapiLogExecution('ERROR', 'getWOQtyEntered2', getWOQtyEntered);
		var getWOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getWOPackCode = request.getParameter('hdnItemPackCode');
		var getWOItemStatus = request.getParameter('custparam_polineitemstatus');
		var getPOLineQuantity = request.getParameter('custparam_polinequantity');
		var getPOLineQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		//var getItemCube = request.getParameter('custparam_itemcube');

		var getBaseUomQty = request.getParameter('custparam_baseuomqty');

		var getActualBeginDate = request.getParameter('hdnActualBeginDate');

		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');
		//nlapiLogExecution('ERROR', 'getItemCube', getItemCube);
		//nlapiLogExecution('ERROR', 'getBaseUomQty', getBaseUomQty);

		//nlapiLogExecution('ERROR', 'getWOItemStatus', getWOItemStatus);
		var vCarrier ='';  
		var vSite;
		var vCompany;
		var stgDirection="OUB";

		var getLineCount = 1;
		var getBaseUOM = 'EACH';
		var getBinLocation = "";
		var getuomlevel = "";
		var getItemCube="";

		var eBizItemDims=geteBizItemDimensions(getFetchedItemId);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					getBaseUOM = eBizItemDims[z].getText('custrecord_ebizuomskudim');
					getuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
					getBaseUomQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					getItemCube = eBizItemDims[z].getValue('custrecord_ebizcube');
				}
			}
		}

		nlapiLogExecution('ERROR', 'getBaseUOM', getBaseUOM);
		nlapiLogExecution('ERROR', 'getuomlevel', getuomlevel);
		nlapiLogExecution('ERROR', 'getBaseUomQty', getBaseUomQty);
		nlapiLogExecution('ERROR', 'getItemCube', getItemCube);



		POarray["custparam_woid"] = request.getParameter('custparam_woid');
		POarray["custparam_item"] = request.getParameter('custparam_item');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_expectedquantity"] = request.getParameter('hdnwoqtyenetered');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_noofrecords"] = request.getParameter('hdnnoofrecords');
		POarray["custparam_woname"] = request.getParameter('hdnWOName');

		POarray["custparam_error"] = st5;
		POarray["custparam_polineitemstatusValue"]=request.getParameter('hdnItemStatusNo');
		POarray["custparam_polineitemstatus"]=request.getParameter('hdnItemStatus');
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_woid');
		POarray["custparam_poqtyentered"] = request.getParameter('custparam_expectedquantity');
		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('custparam_packcode');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_expectedquantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_expectedquantity');
		POarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_itemquantity"] = request.getParameter('hdnQuantity');

		POarray["custparam_enteredOption"] = request.getParameter('hdnPickfaceEnteredOption');

		nlapiLogExecution('ERROR', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);

		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_baseuomqty"] = getBaseUomQty;

		POarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		POarray["custparam_beginlocinternalid"] = request.getParameter('hdnBeginlocId');
		POarray["custparam_beginlocationname"] = request.getParameter('hdnBeginlocname');

		//		var TimeArray = new Array();
		//		TimeArray = getActualBeginTime.split(' ');
		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; //TimeArray[1];
		nlapiLogExecution('ERROR', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('ERROR', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

		POarray["custparam_screenno"] = 'WOPutGenLP';

		POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
		POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
		POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
		POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
		POarray["custparam_lastdate"] = request.getParameter('hdnLastAvlDate');
		POarray["custparam_fifodate"] = request.getParameter('hdnFifoDate');
		POarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');
		POarray["custparam_cartlp"] = getItemCartLP;
		POarray["custparam_manufacturelot"] = getManufactureLot;
		POarray["custparam_autolot"] = getAutoLotFlag;
		nlapiLogExecution('ERROR', 'getAutoLotFlag', getAutoLotFlag);
		nlapiLogExecution('ERROR', 'getManufactureLot', getManufactureLot);
		nlapiLogExecution('ERROR', 'getItemCartLP', getItemCartLP);
		nlapiLogExecution('ERROR','hdnBatchNo',request.getParameter('hdnBatchNo'));

		nlapiLogExecution('DEBUG', 'POarray["custparam_batchno"]', POarray["custparam_batchno"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_mfgdate"]', POarray["custparam_mfgdate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_expdate"]', POarray["custparam_expdate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_bestbeforedate"]', POarray["custparam_bestbeforedate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_lastdate"]', POarray["custparam_lastdate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_fifodate"]', POarray["custparam_fifodate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_fifocode"]', POarray["custparam_fifocode"]);
		nlapiLogExecution('ERROR', 'POarray["custparam_fetcheditemid"]', POarray["custparam_fetcheditemid"]);
		nlapiLogExecution('ERROR', 'POarray["custparam_poitem"]', POarray["custparam_poitem"]);
		nlapiLogExecution('ERROR', 'POarray["custparam_itemquantity"]', POarray["custparam_itemquantity"]);
		nlapiLogExecution('ERROR', 'POarray.length2', POarray.length);

		// Added by Phani on 03-25-2011
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		var poLoc = request.getParameter('hdnWhLocation');
		nlapiLogExecution('ERROR', 'WH Location', POarray["custparam_whlocation"]);		

		if(request.getParameter('custparam_autogenerateLp') != null && request.getParameter('custparam_autogenerateLp') !='')
		{
			getsysItemLP = getItemLP;
			nlapiLogExecution('ERROR', 'getsysItemLP', getsysItemLP);	
		}

		/*var POfilters = new Array();
		nlapiLogExecution('ERROR', 'getWOInternalId', getWOInternalId);
		POfilters[0] = new nlobjSearchFilter('name', null, 'is', getWOInternalId);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, POfilters, null);

		var getPOReceiptNo = '';

		if (searchresults != null && searchresults.length > 0) {
			for (var i = 0; searchresults != null && i < searchresults.length; i++) {
				getPOReceiptNo = searchresults[i].getValue('custrecord_ebiz_poreceipt_receiptno');
			}

			nlapiLogExecution('ERROR', 'PO Receipt No', getPOReceiptNo);
		}*/

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optedEvent1 = request.getParameter('cmdAutoLp');
		nlapiLogExecution('ERROR', 'POarray["custparam_pointernalid"]', POarray["custparam_pointernalid"]);
		var trantype=null;
		if(POarray["custparam_pointernalid"]!=null && POarray["custparam_pointernalid"]!='')
		{
			trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');
			POarray["custparam_trantype"] = trantype;
			nlapiLogExecution('ERROR', 'trantype', trantype);
		}
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		
		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_status', 'customdeploy_ebiz_rf_wo_put_status_dl', false, POarray);
			//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_loc', 'customdeploy_ebiz_rf_wo_putaway_loc_di', false, POarray);
		}
		//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
		//            if (optedEvent != '' && optedEvent != null) {
		else 
			if(optedEvent1 == 'F8')
			{
				nlapiLogExecution('ERROR', 'AutoGenerateLP_poLoc', poLoc);
				var AutoGenerateLP = GetMaxLPNo('1', '1',poLoc);
				nlapiLogExecution('ERROR', 'AutoGenerateLP', AutoGenerateLP);
				POarray["custparam_autogenerateLp"] =AutoGenerateLP ;
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_lp', 'customdeploy_ebiz_rf_wo_putaway_lp_di', false, POarray);

			}
			else
			{
				if (getItemLP == "") {
					//	if the 'Send' button is clicked without any option value entered,
					//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('ERROR', 'Entered Item LP', getItemLP);
				}
				else 
				{
					//Code Added on 28th Jun 2012 by suman.

					/*
					 * This part of the code is ment only to restict the user not to checkin more than the PO qty
					 * Even if user try to roll back to the LP screen through back button option of the browser and try to resumit with new Valid LP.
					 */

					var poOverage=GetPoOverage(getFetchedItemId,getWOInternalId,poLoc);

					nlapiLogExecution('ERROR','poOverage', poOverage);

					var ItemRemaininingQuantity = itemRemainingQuantity(getWOInternalId, getFetchedItemId, getPOLineNo,POarray["custparam_polinequantity"], POarray["custparam_polinequantityreceived"], poLoc, null);

					var palletQuantity = fetchPalletQuantity(getFetchedItemId,poLoc,null); //'9999';
					var ItemRecommendedQuantity =0;
					var hdnItemRecmdQtyWithPOoverage=0;
					nlapiLogExecution('ERROR','ItemRemaininingQuantity', ItemRemaininingQuantity);
					nlapiLogExecution('ERROR','palletQuantity', palletQuantity);
					nlapiLogExecution('ERROR','ItemQuantity', POarray["custparam_polinequantity"]);

					if (poOverage == null){
						poOverage = 0;	
					}
					//else{
					hdnItemRecmdQtyWithPOoverage = Math.min (ItemRemaininingQuantity + (poOverage * POarray["custparam_polinequantity"])/100, palletQuantity);
					nlapiLogExecution('ERROR', 'hdnItemRecmdQtyWithPOoverage', hdnItemRecmdQtyWithPOoverage);
					//End of the code as of 28th Jun 2012.
					if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "" && parseFloat(POarray["custparam_poqtyentered"]) <= parseFloat(hdnItemRecmdQtyWithPOoverage))
					{

						nlapiLogExecution('ERROR', 'Item LP', getItemLP);
						var LPReturnValue = "";
						if(getsysItemLP=="")
						{
							nlapiLogExecution('ERROR', 'getsysItemLP', 'userdefined');
							LPReturnValue = ebiznet_LPRange_CL(getItemLP, '2',poLoc,1);
						}
						else
						{
							nlapiLogExecution('ERROR', 'getsysItemLP', 'systemgenerated');
							LPReturnValue = ebiznet_LPRange_CL(getItemLP, '1',poLoc,'');
						}
						nlapiLogExecution('ERROR', 'LP Return Value new', LPReturnValue);

						var lpExists = 'N';
						//LP Checking in masterlp record starts
						if (LPReturnValue == true) {
							try {
								nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
								var filtersmlp = new Array();
								filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getItemLP);

								var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

								if (SrchRecord != null && SrchRecord.length > 0) {
									nlapiLogExecution('ERROR', 'LP FOUND');

									lpExists = 'Y';
								}
								else {
									nlapiLogExecution('ERROR', 'LP NOT FOUND');
									var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
									customrecord.setFieldValue('name', getItemLP);
									customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getItemLP);
									var rec = nlapiSubmitRecord(customrecord, false, true);
								}
							} 
							catch (e) {
								nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
							}
							//LP Checking in masterlp record ends
							nlapiLogExecution('ERROR', 'POarray.length1', POarray.length);
							nlapiLogExecution('ERROR', 'lp Exists111', lpExists);
							var remainingCube = '';
//							if (LPReturnValue == true) {
							if(lpExists != 'Y')
							{
								//POarray["custparam_polineitemlp"] = getItemLP;
								//POarray["custparam_poreceiptno"] = getPOReceiptNo;

								//code added on 13 feb 2012 by suman
								//To Calculate totalitemcube based on the baseuomqty.
								var TotalItemCube=CubeCapacity(getWOQtyEntered,getBaseUomQty,getItemCube);
//								var TotalItemCube = parseFloat(getItemCube) * parseFloat(getWOQtyEntered);

								//end of code as of 13 feb 2012.


								nlapiLogExecution('ERROR', 'Total Item Cube ', TotalItemCube );

								//	BeginLocationArray = GenerateLocation(getWOQtyEntered, getPOLineNo, getLineCount, getFetchedItemId, getWOItem, getBinLocation, getItemCube, getBeginLocation)
								var itemSubtype = nlapiLookupField('item', POarray["custparam_fetcheditemid"], ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
								POarray["custparam_recordtype"] = itemSubtype.recordType;
								var batchflag=itemSubtype.custitem_ebizbatchlot;
								nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype.recordType);
								nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
								nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);

								var ItemDimensionsFilters = new Array();
								ItemDimensionsFilters.push(new nlobjSearchFilter('internalid', 'custrecord_ebizitemdims', 'is', getFetchedItemId));

								var ItemDimensionsColumns = new Array();

								ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizitemdims'));
								ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizpackcodeskudim'));

								var ItemDimensionsResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, ItemDimensionsFilters, ItemDimensionsColumns);

								if (ItemDimensionsResults != null) 
								{
									var ItemDimensionsResult = ItemDimensionsResults[0];
									var getFetchedPackCodeId = ItemDimensionsResult.getId();
									getWOPackCode = ItemDimensionsResult.getValue('custrecord_ebizpackcodeskudim');
								}
								var getWOItemStatusname="";
								getWOItemStatusname=request.getParameter('hdnItemStatus')
								getWOItemStatusname=request.getParameter('hdnItemStatusNo')


								if(getWOItemStatus==null || getWOItemStatus=="")
								{
									// Fetch the item status for the item entered
									var ItemStatusFilters = new Array();
									ItemStatusFilters.push(new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T'));

									var ItemStatusColumns = new Array();

									ItemStatusColumns.push(new nlobjSearchColumn('internalid'));
									ItemStatusColumns.push(new nlobjSearchColumn('name'));
									ItemStatusColumns.push(new nlobjSearchColumn('custrecord_ebizsiteskus'));

									var ItemStatusResults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemStatusFilters, ItemStatusColumns);

									if (ItemStatusResults != null) 
									{
										var ItemStatusResult = ItemStatusResults[0];
										getWOItemStatus = ItemStatusResult.getId();
										getWOItemStatusname = ItemStatusResult.getValue('name');
										var getFetchedSiteLocation = ItemStatusResult.getValue('custrecord_ebizsiteskus');
									}
								}		

								nlapiLogExecution('ERROR', 'getWOItemStatusname', getWOItemStatusname);
								nlapiLogExecution('ERROR', 'getWOPackCode', getWOPackCode);
								nlapiLogExecution('ERROR', 'getBeginLocId', getBeginLocId);

								var getBeginLocationId ='';
								if(getBeginLocId == null || getBeginLocId =='')
								{
									//Case # 20127203 Start
									var priorityPutawayLocnArr = priorityPutaway(getFetchedItemId, getWOQtyEntered,poLoc,getWOItemStatus);
									//Case # 20127203 End
									var priorityRemainingQty = priorityPutawayLocnArr[0];
									var priorityQty = priorityPutawayLocnArr[1];
									var priorityLocnID = priorityPutawayLocnArr[2];


									nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityRemainingQty', priorityRemainingQty);
									nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityQty', priorityQty);
									nlapiLogExecution('ERROR', 'checkInPOSTRequest:POQtyEntered', getWOQtyEntered);
									nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityLocnID', priorityLocnID);

									var pickfaceEnteredOption = request.getParameter('hdnPickfaceEnteredOption');
									var putmethod,putrule,putzoneid;
									var qcRuleCheck=getQCRule(getFetchedItemId);
									nlapiLogExecution('ERROR', 'qcRuleCheck', qcRuleCheck);
									if(priorityQty < getWOQtyEntered){			//(priorityRemainingQty != 0){
//										getBeginLocation = GetPutawayLocation(getFetchedItemId, getWOPackCode, getWOItemStatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType);
										var getBeginLocation=null;
										if(!qcRuleCheck)
										{
											var context = nlapiGetContext();
											nlapiLogExecution('ERROR','Remaining usage 1',context.getRemainingUsage());

											var filters2 = new Array();

											var columns2 = new Array();
											columns2[0] = new nlobjSearchColumn('custitem_item_family');
											columns2[1] = new nlobjSearchColumn('custitem_item_group');
											columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
											columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
											columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
											columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
											columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');
											filters2.push(new nlobjSearchFilter('internalid', null, 'is', getFetchedItemId));
											var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

											var filters3=new Array();
											filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
											filters3.push(new nlobjSearchFilter('custrecord_ebiz_wo_flag', null, 'is', 'T'));
											var columns3=new Array();
											columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
											columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
											columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
											columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
											columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
											columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
											columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
											columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
											columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
											columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
											columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
											columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
											columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
											columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
											//columns3[14] = new nlobjSearchColumn('custrecord_sequencenumberpickrule');
											columns3[14] = new nlobjSearchColumn('formulanumeric');
											columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
											columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
											//case 201411041 start
											columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
											//case 201411041 end
											columns3[14].setSort();




											var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);
											if(putrulesearchresults != null && putrulesearchresults !='')
											{
												nlapiLogExecution('ERROR','getWOItemStatus',getWOItemStatus);
												//getBeginLocation = generatePutawayLocation(getFetchedItemId, getWOPackCode, getWOItemStatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType,poLoc);
												getBeginLocation = GetPutawayLocationNew(getFetchedItemId, getWOPackCode, getWOItemStatus, getBaseUOM, parseFloat(TotalItemCube),itemSubtype.recordType,poLoc,ItemInfoResults,putrulesearchresults);
											}
											else
											{
												nlapiLogExecution('ERROR','No putaway strategies with WO Flag set');
												POarray["custparam_error"] = st6;
												response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
												return false;
											}
											nlapiLogExecution('ERROR','Remaining usage 2',context.getRemainingUsage());
										}
										//					nlapiLogExecution('ERROR', 'BeginLocation', BeginLocationArray[1]);
										nlapiLogExecution('ERROR', 'Begin Location', getBeginLocation);

										//					var getBeginLocation = BeginLocationArray[1];

										var vLocationname="";
										if (getBeginLocation != null && getBeginLocation != '') {
											var getBeginLocationId = getBeginLocation[2];
											vLocationname= getBeginLocation[0];
											nlapiLogExecution('ERROR', 'Begin Location Id', getBeginLocationId);
										}
										else {
											var getBeginLocationId = "";
											nlapiLogExecution('ERROR', 'Begin Location Id is null', getBeginLocationId);
										}



										if(getBeginLocation!=null && getBeginLocation != '')
											remainingCube = getBeginLocation[1];

										nlapiLogExecution('ERROR', 'remainingCube', remainingCube);
										if (getBeginLocation != null && getBeginLocation != '') {
											putmethod = getBeginLocation[9];
											putrule = getBeginLocation[10];
											putzoneid = getBeginLocation[8];
										}
										else
										{
											putmethod = "";
											putrule = "";
											putzoneid = "";
										}
									}
								}
								else
								{
									getBeginLocationId = getBeginLocId ;

								}

								nlapiLogExecution('ERROR', 'getBeginLocationId', getBeginLocationId);

								var getActualEndDate = DateStamp();
								nlapiLogExecution('ERROR', 'getActualEndDate', getActualEndDate);
								var getActualEndTime = TimeStamp();
								nlapiLogExecution('ERROR', 'getActualEndTime', getActualEndTime);


								/*
								 * This is to insert a record in Transaction Line Details custom record
								 * The purpose of this is to identify the quantity checked-in, location generated for and putaway confirmation
								 * */                    
//								var trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');
								/*	TrnLineUpdation(trantype, 'CHKN', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
									POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
									POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"",getWOItemStatus);*/

								/*
								 * If the received quantity has to be putaway to a pickface location based on priority putaway flag, 
								 * 	create PUTW task in TRN_OPENTASK 
								 * 		set taskType = 2(PUTW) and wmsStatusFlag = 2(INBOUND/LOCATIONS ASSIGNED)
								 * 		beginLocation = priorityLocnID and endLocn = ""
								 */	

								if (priorityQty >= getWOQtyEntered){			//(priorityQty != 0){
									taskType = "5"; // KTS
									wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
									getBeginLocationId = priorityLocnID;
									var LocRemCube = GeteLocCube(getBeginLocationId);

									nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
									if (parseFloat(LocRemCube) > parseFloat(TotalItemCube)) 
										remainingCube = parseFloat(LocRemCube) - parseFloat(TotalItemCube);
//									endLocn = "";
									//var getItemLP = GetMaxLPNo(1, 1);
								}
								nlapiLogExecution('ERROR', 'Before custChknPutwRecCreation calling (getBeginLocationId)', getBeginLocationId);
								nlapiLogExecution('ERROR', 'getWOInternalId', getWOInternalId);
								nlapiLogExecution('ERROR', 'getWONo', getWONo);
								nlapiLogExecution('ERROR', 'getWOQtyEntered', getWOQtyEntered);
								nlapiLogExecution('ERROR', 'getPOLineNo', getPOLineNo);
								nlapiLogExecution('ERROR', 'getWOItemRemainingQty', getWOItemRemainingQty);
								nlapiLogExecution('ERROR', 'getLineCount', getLineCount);
								nlapiLogExecution('ERROR', 'getFetchedItemId', getFetchedItemId);
								nlapiLogExecution('ERROR', 'getWOItem', getWOItem);
								nlapiLogExecution('ERROR', 'getItemDescription', getItemDescription);
								nlapiLogExecution('ERROR', 'getWOItemStatus', getWOItemStatus);
								nlapiLogExecution('ERROR', 'getWOPackCode', getWOPackCode);
								nlapiLogExecution('ERROR', 'getBaseUOM', getBaseUOM);
								nlapiLogExecution('ERROR', 'getItemLP', getItemLP);
								nlapiLogExecution('ERROR', 'getBinLocation', getBinLocation);
								nlapiLogExecution('ERROR', 'getPOLineQuantityReceived', getPOLineQuantityReceived);
								nlapiLogExecution('ERROR', 'getActualEndDate', getActualEndDate);
								nlapiLogExecution('ERROR', 'getActualEndTime', getActualEndTime);
//								nlapiLogExecution('ERROR', 'getPOReceiptNo', getPOReceiptNo);
								nlapiLogExecution('ERROR', 'getActualBeginDate', getActualBeginDate);
								nlapiLogExecution('ERROR', 'getActualBeginTime', getActualBeginTime);
								nlapiLogExecution('ERROR', 'getActualBeginTimeAMPM', getActualBeginTimeAMPM);
								nlapiLogExecution('ERROR', 'getBeginLocationId', getBeginLocationId);
								nlapiLogExecution('ERROR', 'POarray["custparam_batchno"]', POarray["custparam_batchno"]);
								nlapiLogExecution('ERROR', 'itemSubtype.recordType', itemSubtype.recordType);
								nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizbatchlot', itemSubtype.custitem_ebizbatchlot);
								nlapiLogExecution('ERROR', 'batchflag', batchflag);
								nlapiLogExecution('ERROR', 'getItemCartLP', getItemCartLP);
								nlapiLogExecution('ERROR', 'POarray["custparam_whlocation"]', POarray["custparam_whlocation"]);
								nlapiLogExecution('ERROR', 'batchflag', batchflag);
								nlapiLogExecution('ERROR', 'putmethod', putmethod);
								nlapiLogExecution('ERROR', 'putrule', putrule);
								nlapiLogExecution('ERROR', 'qcRuleCheck', qcRuleCheck);
								nlapiLogExecution('ERROR', 'putzoneid', putzoneid);
								var getPOReceiptNo='';


								var vValid=custChknPutwRecCreation(getWOInternalId, getWONo, getWOQtyEntered, getPOLineNo, getWOItemRemainingQty, getLineCount, getFetchedItemId, 
										getWOItem, getItemDescription, getWOItemStatus, getWOPackCode, getBaseUOM, getItemLP, getBinLocation, 
										getPOLineQuantityReceived, getActualEndDate, getActualEndTime, getPOReceiptNo, "", getActualBeginDate, 
										getActualBeginTime, getActualBeginTimeAMPM, getBeginLocationId, POarray["custparam_batchno"], 
										POarray["custparam_mfgdate"], POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
										POarray["custparam_lastdate"], POarray["custparam_fifodate"], POarray["custparam_fifocode"], 
										itemSubtype.recordType, getItemCartLP, POarray["custparam_whlocation"],batchflag,putmethod,putrule,getuomlevel,qcRuleCheck,putzoneid);
								nlapiLogExecution('ERROR', 'vValid', vValid);


								if(vValid==false)
								{
									//response.sendRedirect('SUITELET', 'customscript_rf_checkin_cnf', 'customdeploy_rf_checkin_cnf', false, POarray);
									WOarray["custparam_screenno"] = 'WOPutawayGen';
									POarray["custparam_error"] = 'LOCATIONS ARE NOT GENERATED';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									nlapiLogExecution('ERROR', 'Entered Item LP', getItemLP);
								}
								else
								{

									/*
									 * This is to insert a record in inventory custom record.
									 * The data that is to be inserted is PO Internal Id, Item, Item Status, Pack Code, Quantity, LP#
									 * The parameters list is: 
									 * pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, warehouse location
									 * 
									 */                    

									/*
								TrnLineUpdation(trantype, 'ASPW', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
										POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
										POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"");

								rf_checkin(getWOInternalId, getFetchedItemId, getItemDescription, getWOItemStatus, 
										getWOPackCode, getWOQtyEntered, getItemLP, POarray["custparam_whlocation"],
										POarray["custparam_batchno"], POarray["custparam_mfgdate"], 
										POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
										POarray["custparam_lastdate"], POarray["custparam_fifodate"], 
										POarray["custparam_fifocode"],getuomlevel);*/

									try
									{

										nlapiLogExecution('ERROR', 'getFetchedItemId', getFetchedItemId);
										nlapiLogExecution('ERROR', 'getWOQtyEntered',getWOQtyEntered);
										nlapiLogExecution('ERROR', 'getWOInternalId',getWOInternalId);
										nlapiLogExecution('ERROR', 'getItemLP',getItemLP);
										nlapiLogExecution('ERROR', 'getWOPackCode',getWOPackCode);
										nlapiLogExecution('ERROR', 'poLoc',poLoc);
										var vstageLoc ='';
									//case# 201411366
									var tranordertype = nlapiLookupField('workorder', getWOInternalId, 'custbody_nswmssoordertype');
										if(vstageLoc==null || vstageLoc=='')
											vstageLoc = GetPickStageLocation(getFetchedItemId, vCarrier, poLoc, vCompany,stgDirection,null,null,null,tranordertype);
										nlapiLogExecution('ERROR', 'vstagLoc', vstageLoc);

										if (vstageLoc != null && vstageLoc != "" && vstageLoc != "-1") 
										{																		

											DeleteInvtRecdOfMemberItems(getWOInternalId, vstageLoc,poLoc);


											var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
											invtRec.setFieldValue('name', getFetchedItemId);
											invtRec.setFieldValue('custrecord_ebiz_inv_binloc',vstageLoc);
											invtRec.setFieldValue('custrecord_ebiz_inv_lp', getItemLP);
											invtRec.setFieldValue('custrecord_ebiz_inv_sku', getFetchedItemId);
											invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', getWOItemStatus);
											if(getWOPackCode!=null && getWOPackCode!="")
												invtRec.setFieldValue('custrecord_ebiz_inv_packcode', getWOPackCode);
											invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(getWOQtyEntered).toFixed(4));
											nlapiLogExecution('ERROR', 'itemSubtype.recordType',itemSubtype.recordType);
											nlapiLogExecution('ERROR', 'batchflag',batchflag);
											nlapiLogExecution('ERROR', 'POarray[custparam_batchno]',POarray["custparam_batchno"]);
											if(itemSubtype.recordType == "lotnumberedinventoryitem" || itemSubtype.recordType == "lotnumberedassemblyitem" || itemSubtype.recordType == "assemblyitem" ||batchflag=='T')
											{
												var filterBatch=new Array();
												filterBatch[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',POarray["custparam_batchno"]);
												if(getFetchedItemId!=null&&getFetchedItemId!="")
													filterBatch[1]=new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',getFetchedItemId);
												var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatch,null);
												var BatchID=rec[0].getId();
												nlapiLogExecution('ERROR', 'BatchID',BatchID);
												if(BatchID!=null && BatchID!='')
												invtRec.setFieldValue('custrecord_ebiz_inv_lot', BatchID);
											}
											invtRec.setFieldValue('custrecord_inv_ebizsku_no', getFetchedItemId);
											invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(getWOQtyEntered).toFixed(4));
											invtRec.setFieldValue('custrecord_ebiz_inv_loc', poLoc);
											invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
											//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
											invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
											invtRec.setFieldValue('custrecord_wms_inv_status_flag','36');//36=FLAG.INVENTORY.WIP
											invtRec.setFieldValue('custrecord_invttasktype', '3');
											invtRec.setFieldValue('custrecord_ebiz_transaction_no', getWOInternalId);

											var invtrecid = nlapiSubmitRecord(invtRec, false, true);
											nlapiLogExecution('ERROR', 'invtrecid',invtrecid);
										}
									}
									catch(e)
									{
										nlapiLogExecution('ERROR', 'Failed to Insert MainItem into Inventory Record',e);
									}
									nlapiLogExecution('ERROR', 'getBeginLocationId ', getBeginLocationId);
									if(getBeginLocationId!=null && getBeginLocationId!='')
									{
										UpdateLocCube(getBeginLocationId, remainingCube);
									}



									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di', false, POarray);
								}
							}
							else {
								POarray["custparam_error"] = 'LP ALREADY EXISTS';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('ERROR', 'Entered Item LP', getItemLP);
							}
						}
						else {

							nlapiLogExecution('ERROR', 'Entered Item LP', getItemLP);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;
						}
					}
					else {
						POarray["custparam_error"] = 'QUANTITY EXCEEDS OVERAGE LIMIT';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('ERROR', 'Qty exceeds PO Overage', getItemLP);
					}
				}
			}
		nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
			{
			//WOPutScanLp
			POarray["custparam_screenno"] = 'WOPutScanLp';
			POarray["custparam_error"] = 'LP ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}
	}    
}


function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, 
		ItemDesc, ItemStatus, PackCode, BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, 
		poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, ActualBeginTimeAMPM, BeginLocationId, 
		BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, itemrectype, cartlp, 
		WHLocation,batchflag,putmethod,putrule,uomlevel,qcRuleCheck,putzoneid)
{
	nlapiLogExecution('ERROR', 'uomlevel', uomlevel);
	nlapiLogExecution('ERROR', 'BeginLocationId', BeginLocationId);
	nlapiLogExecution('ERROR', 'WHLocation', WHLocation);
	nlapiLogExecution('ERROR', 'putzoneid', putzoneid);


	var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
	var columns = nlapiLookupField('item', ItemId, fields);
	var ItemType = columns.recordType;					
	var batchflg = columns.custitem_ebizbatchlot;
	var itemfamId= columns.custitem_item_family;
	var itemgrpId= columns.custitem_item_group;
	var getlotnoid="";

//	if(ItemStatus==null || ItemStatus=='')
//	ItemStatus='12';

//	nlapiLogExecution('ERROR', 'putLotBatch', putLotBatch);

	var vValid=true;
	nlapiLogExecution('ERROR', 'Into custChknPutwRecCreation', 'custChknPutwRecCreation');
	var now = new Date();
	var stagelocid, docklocid;

	try
	{
		stagelocid = GetInboundStageLocation(WHLocation, null, 'INB');
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in  GetInboundStageLocation', exp);
	}

	if(stagelocid==null || stagelocid=='')
		stagelocid = getStagingLocation(WHLocation);

	nlapiLogExecution('ERROR', 'Stage Location', stagelocid);

	if(stagelocid==null || stagelocid=='')
		stagelocid=BeginLocationId;

	docklocid = getDockLocation(WHLocation);
	nlapiLogExecution('ERROR', 'Dock Location', docklocid);

	if(docklocid==null || docklocid=='')
		docklocid=stagelocid;

	if (BeginLocationId != null && BeginLocationId != '' ) //Creating custom record with CHKN and PUTW task,
	{
		//create the openTask record With CHKN Task Type
		/*var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('ERROR', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_uom_level', uomlevel);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);

		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('ERROR', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);
		nlapiLogExecution('ERROR', 'Submitting CHKN record', 'TRN_OPENTASK');

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		//code added on 16Feb by suman
		if(po!=null && po!="")
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('ERROR', 'Done CHKN Record Insertion :', 'Success');
		nlapiLogExecution('ERROR', 'BeginLocationId', BeginLocationId);*/

		//create the openTask record With PUTW Task Type
		if(!qcRuleCheck && (BeginLocationId==null || BeginLocationId==""))
			return false;
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('ERROR', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_uom_level', uomlevel);
		putwrecord.setFieldValue('custrecord_tasktype', 5); //For Putaway (PUTW)

		putwrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//For eBiznet Receipt Number .
		//putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		putwrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		nlapiLogExecution('ERROR', 'qcRuleCheck', qcRuleCheck);
		//Status flag .
		if(qcRuleCheck)
		{
			putwrecord.setFieldValue('custrecord_wms_status_flag', 23);
		}
		else
		{
			if (BeginLocationId != "") {
				putwrecord.setFieldValue('custrecord_wms_status_flag', 2);
			}
			else {
				putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
			}
			nlapiLogExecution('ERROR', 'Fetched Begin Location', BeginLocation);

			putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		}
		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		//code added on 16Feb by suman
		if(po!=null && po!="")
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);
		// case# 201417113
		if(putzoneid !=null && putzoneid !='')
		{
			putwrecord.setFieldValue('custrecord_ebizzone_no', putzoneid);
			putwrecord.setFieldValue('custrecord_ebiz_zoneid', putzoneid);
		}

		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);
		/*try
		{
			MoveTaskRecord(recid);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception in  MoveTaskRecord');
		}*/
		nlapiLogExecution('ERROR', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');
		return true;
	}//end if binloc is not null
	else {
		nlapiLogExecution('ERROR', 'elsePart', 'success');
		//create the openTask record With CHKN Task Type
		/*var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('ERROR', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);

		nlapiLogExecution('ERROR', 'custrecord_ebiz_sku_no tesing', 'ItemId');

		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_uom_level', uomlevel);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{

			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		//code added on 16Feb by suman
		if(po!=null && po!="")
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);


		nlapiLogExecution('ERROR', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('ERROR', 'Done CHKN Record Insertion 1:', 'Success');*/

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('ERROR', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', quan);
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_uom_level', uomlevel);
		putwrecord.setFieldValue('custrecord_tasktype', 5); //For Putaway (PUTW)
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		//putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		putwrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);

		nlapiLogExecution('ERROR', 'qcRuleCheck', qcRuleCheck);
		//Status flag .
		if(qcRuleCheck)
		{
			putwrecord.setFieldValue('custrecord_wms_status_flag', 23);
		}
		else
		{ 
			putwrecord.setFieldValue('custrecord_wms_status_flag', 6);

		}

		//putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" ||itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");

		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		//code added on 16Feb by suman
		if(po!=null && po!="")
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		nlapiLogExecution('ERROR', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		/*try
		{
			MoveTaskRecord(recid);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception in  MoveTaskRecord');
		}*/
		nlapiLogExecution('ERROR', 'Done CHKN Record Insertion :', 'Success');
		return false;
	}
}

function GenerateLocation(getWOQtyEntered, getPOLineNo, getLineCount, getFetchedItemId, getWOItem, getBinLocation, getItemCube){
	var BinLocationIDArray = new Array();
	var RemainingCubeArray = new Array();
	var BinLocationNameArray = new Array();
	var ReturnRemainingCubeValueArray = new Array();
	var LocationIdUpdateArray = new Array();

	var TempRemCube, BinLocationIdUpdate, ReturnRemainingCubeValue;

	var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, null, null);

	for (var s = 0; s < Math.min(100, LocationSearch.length); s++) {
		var LocationSearchedId = LocationSearch[s].getId();
		//        nlapiLogExecution('ERROR', 'Fetched Location Searched Id', LocationSearchedId);

		//        LocationRec = nlapiLoadRecord(LocationSearch[i].getRecordType(), LocationSearch[i].getId());
		LocationRec = nlapiLoadRecord('customrecord_ebiznet_location', LocationSearch[s].getId());
		//Internal Id
		BinLocationIDArray[s] = LocationSearch[s].getId();
		//Remaining Cube
		RemainingCubeArray[s] = LocationRec.getFieldValue('custrecord_remainingcube');
		//Location name
		BinLocationNameArray[s] = LocationRec.getFieldValue('custrecord_ebizlocname');
	}

	for (var i = 0; i <= getLineCount; i++) {
		var ItemLineQuantity = getWOQtyEntered;
		var TotalItemCube = parseFloat(getItemCube) * parseFloat(ItemLineQuantity);
		nlapiLogExecution('ERROR', 'ItemLineQuantity', ItemLineQuantity);
		nlapiLogExecution('ERROR', 'TotalItemCube', TotalItemCube);

		for (var LocationGen = 1; LocationGen <= BinLocationNameArray.length; LocationGen++) {
			if (parseFloat(TotalItemCube) <= parseFloat(RemainingCubeArray[LocationGen])) {
				ReturnRemainingCubeValue = parseFloat(RemainingCubeArray[LocationGen]) - parseFloat(TotalItemCube);
				BinLocationIdUpdate = BinLocationIdUpdate + "," + BinLocationIDArray[LocationGen];
				TempRemCube = TempRemCube + "," + ReturnRemainingCubeValue;
				LocationGen = BinLocationNameArray.length;
			}

			LocationIdUpdateArray = BinLocationIdUpdate.split(',');
			ReturnRemainingCubeValueArray = TempRemCube.split(',');

			/*            
             for (var i = 1; i < LocationIdUpdateArray.length; i++)
             {
             nlapiLogExecution('ERROR', 'LocationGen' + '-' + i, LocationIdUpdateArray[i]);
             }
			 */
			//Cube update.
			for (LocUpdate = 1; LocUpdate < LocationIdUpdateArray.length; LocUpdate++) {
				var LocUpdateRec = nlapiLoadRecord('customrecord_ebiznet_location', LocationIdUpdateArray[LocUpdate]);
				LocUpdateRec.setFieldValue('custrecord_remainingcube', ParseFloat(ReturnRemainingCubeValueArray[LocUpdate]).toFixed(4));
				nlapiSubmitRecord(LocUpdateRec, false, true);
			}
		}
	}

	return LocationIdUpdateArray;
}

function getStagingLocation(site){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));
	stageFilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	//code added on 5Mar By suman
	//Searching for the record with the site id and lowest seqNo.

	//SITE
	if(site!=null && site!="")
		stageFilters.push(new nlobjSearchFilter('custrecord_ebizsiteloc', 'custrecord_inboundlocgroupid', 'anyof', ["NONE",site]));

	//SEQUENCE NO.
	var stageColumn=new Array();
	stageColumn[0]=new nlobjSearchColumn('custrecord_sequenceno','custrecord_inboundlocgroupid');
	stageColumn[0].setSort();

	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters, stageColumn);

	//end of modifing code as of 5Mar.

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}

function GetInboundStageLocation(vSite, vCompany, stgDirection){

	nlapiLogExecution('ERROR', 'Into GetInboundStageLocation');

	var str = 'vSite. = ' + vSite + '<br>';
	str = str + 'vCompany. = ' + vCompany + '<br>';	
	str = str + 'stgDirection. = ' + stgDirection + '<br>';

	nlapiLogExecution('ERROR', 'GetInboundStageLocation Parameters', str);

	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}


	var columns = new Array();
	var filters = new Array();

	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}

	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < searchresults.length; i++) {

			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			var vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');

			var str = 'Stage Location ID. = ' + vstgLocation + '<br>';
			str = str + 'Stage Location Text. = ' + vstgLocationText + '<br>';	

			nlapiLogExecution('ERROR', 'Stage Location Details', str);

			if (vstgLocation != null && vstgLocation != "") {
				nlapiLogExecution('ERROR', 'Out of GetInboundStageLocation',vstgLocation);
				return vstgLocation;
			}

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('Error', 'LocGroup ', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('Error', 'searchresultsloc ', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {

					var vLocid=searchresultsloc[0].getId();
					nlapiLogExecution('ERROR', 'Out of GetInboundStageLocation',vLocid);
					return vLocid;

				}				
			}
		}
	}

	nlapiLogExecution('ERROR', 'Out of GetInboundStageLocation');

}

function getDockLocation(site){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));
	dockFilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	//code added on 5Mar By suman
	//Searching for the record with the site id and lowest seqNo.

	//SITE
	if(site!=null && site!="")
		dockFilters.push(new nlobjSearchFilter('custrecord_ebizsiteloc', 'custrecord_inboundlocgroupid', 'anyof', ["NONE",site]));

	//SEQUENCE NO.
	var dockColumn=new Array();
	dockColumn[0]=new nlobjSearchColumn('custrecord_sequenceno','custrecord_inboundlocgroupid');
	dockColumn[0].setSort();

	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters,dockColumn);

	//end of modifing code as of 5Mar.

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}

function rf_checkin(pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, whLocation,
		batchno, mfgdate, expdate, bestbeforedate, lastdate, fifodate, fifocode,uomlevel){
	nlapiLogExecution('ERROR', 'inside rf_checkin', 'Success');
	nlapiLogExecution('ERROR', 'pointid', pointid);
	nlapiLogExecution('ERROR', 'itemid', itemid);
	nlapiLogExecution('ERROR', 'itemdesc', itemdesc);
	nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
	nlapiLogExecution('ERROR', 'itempackcode', itempackcode);
	nlapiLogExecution('ERROR', 'quantity', quantity);
	nlapiLogExecution('ERROR', 'invtlp', invtlp);
	nlapiLogExecution('ERROR', 'whLocation', whLocation);
	nlapiLogExecution('ERROR', 'batchno', batchno);
	nlapiLogExecution('ERROR', 'mfgdate', mfgdate);
	nlapiLogExecution('ERROR', 'expdate', expdate);
	nlapiLogExecution('ERROR', 'bestbeforedate', bestbeforedate);
	nlapiLogExecution('ERROR', 'lastdate', lastdate);
	nlapiLogExecution('ERROR', 'fifodate', fifodate);
	nlapiLogExecution('ERROR', 'fifocode', fifocode);
	nlapiLogExecution('ERROR', 'uomlevel', uomlevel);




//	nlapiLogExecution('ERROR', 'whLocation', whLocation);
//	nlapiLogExecution('ERROR', 'PO Internal ID', pointid);

	if(whLocation==null || whLocation=="")
	{
		whLocation = nlapiLookupField('purchaseorder', pointid, 'location');

		nlapiLogExecution('ERROR', 'whLocation (from lookup)', whLocation);
	}

	/*
	 * Get the stage location. This will be the bin location to which the item will be 
	 * placed at the time of check-in. The same data is to be inserted in inventory record after it is
	 * done in opentask record.
	 */
	var stagelocid;

	try
	{
		stagelocid = GetInboundStageLocation(whLocation, null, 'INB');
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in  GetInboundStageLocation', exp);
	}

	if(stagelocid==null || stagelocid=='')
		stagelocid = getStagingLocation(whLocation);

	nlapiLogExecution('ERROR', 'Stage Location', stagelocid);

	/*
	 * To create inventory record.
	 */
	var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

	nlapiLogExecution('ERROR', 'Creating Inventory Record', 'INVT');

	var filtersAccount = new Array();
	if(whLocation!=null && whLocation!=""){
		filtersAccount.push(new nlobjSearchFilter('custrecord_location', null, 'anyof', whLocation));
	}

	var accountNumber = "";
//	var columnsAccount = new Array();
//	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

//	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);
//	if(accountSearchResults!= null && accountSearchResults.length > 0)
//	accountNumber = accountSearchResults[0].getValue('custrecord_accountno');
//	if(accountSearchResults!= null && accountSearchResults.length > 0)
//	{

	//nlapiLogExecution('ERROR', 'Account # incondition',accountNumber);

	invtRec.setFieldValue('name', pointid);
	invtRec.setFieldValue('custrecord_ebiz_inv_binloc', stagelocid);
	invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
	invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
	invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
	invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
	//invtRec.setFieldValue('custrecord_ebiz_qoh', quantity);
	invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
//	invtRec.setFieldValue('custrecord_wms_inv_status_flag','1');
	invtRec.setFieldValue('custrecord_wms_inv_status_flag','17');//17=FLAG.INVENTORY.INBOUND
	invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
	invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
	invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
	invtRec.setFieldValue('custrecord_invttasktype', 1);
	if(uomlevel!=null && uomlevel!='')
		invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);
	//custrecord_ebiz_uomlvl


	if(batchno!=null && batchno!=''&& batchno!='null')
	{
		nlapiLogExecution('ERROR', 'batchno into if', batchno);
		try
		{
			//Added on 02/02/12 by suman.
			//since custrecord_ebiz_in_lot is a list need to pass InternalId.
			var filterBatch=new Array();
			filterBatch[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batchno);
			if(itemid!=null&&itemid!="")
				filterBatch[1]=new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid);
			var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatch,null);
			var BatchID=rec[0].getId();
			//end of code additon as on 02/02/12.
//			invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
			invtRec.setFieldValue('custrecord_ebiz_inv_lot', BatchID);
			invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception', exp);
		}
	}

	//code added on 16Feb by suman
	if(pointid!=null && pointid!="")
		invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);

	var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
//	}
//	else
//	{
//	response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, "Invalid Account #");
//	//nlapiLogExecution('ERROR', 'Entered Item LP', getItemLP);
//	}
	nlapiLogExecution('ERROR', 'After Submitting invtrecid', invtRecordId);
	if (invtRecordId != null) {
		nlapiLogExecution('ERROR', 'Inventory record creation is successful', invtRecordId);
	}
	else {
		nlapiLogExecution('ERROR', 'Inventory record creation is failure', 'Fail');
	}
} 



//Function added by suman on 13th Feb 2012
//This  function returns the item cube capacity depending upon the base uom value.
/**
 * @param RcvQty
 * @param BaseUOMQty
 * @param cube
 * @returns
 */
function CubeCapacity(RcvQty,BaseUOMQty,cube)
{
	nlapiLogExecution('ERROR', 'CubeCapacity : RcvQty', RcvQty);
	nlapiLogExecution('ERROR', 'CubeCapacity : BaseUOMQty', BaseUOMQty);
	nlapiLogExecution('ERROR', 'CubeCapacity : cube', cube);
	try{
		var itemCube;
		if (cube != "" && cube != null) 
		{
			if (!isNaN(cube)) 
			{
				var uomqty = ((parseFloat(RcvQty))/(parseFloat(BaseUOMQty)));			
				nlapiLogExecution('ERROR', 'uomqty', uomqty);
				itemCube = (parseFloat(uomqty) * parseFloat(cube));
				nlapiLogExecution('ERROR', 'ItemCube', itemCube);
			}
			else 
			{
				itemCube = 0;
			}
		}
		else 
		{
			itemCube = 0;
		}	
		nlapiLogExecution('ERROR','itemcube',itemCube);
		return itemCube;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in cubecapacity',exp);
	}
}

function getQCRule(Item)
{
	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus','custitem_item_info_1','custitem_item_info_2','custitem_item_info_3'];

	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	//nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);

	var ItemStatus = columns.custitem_ebizdefskustatus;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;



	var i=0;
	var filters = new Array();

	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';

	if(ItemFamily!=null && ItemFamily != "")
	{

		nlapiLogExecution('ERROR','Into ItemFamily',ItemFamily);


		filters.push(new nlobjSearchFilter('custrecord_qcrules_item_family', null, 'anyof', ['@NONE@', ItemFamily]));

	}
	if(ItemGroup!=null && ItemGroup != "")
	{

		nlapiLogExecution('ERROR','Into ItemGroup',ItemGroup);

		filters.push(new nlobjSearchFilter('custrecord_qcrules_item_group', null, 'anyof', ['@NONE@', ItemGroup]));

	}
	if(ItemStatus!=null && ItemStatus != "")
	{
		nlapiLogExecution('ERROR','Into ItemStatus',ItemStatus);

		filters.push(new nlobjSearchFilter('custrecord_qcrules_item_status', null, 'anyof', ['@NONE@', ItemStatus]));

	}	 
	if(Item != null && Item != "")
	{
		nlapiLogExecution('ERROR','Into Item',Item);

		filters.push(new nlobjSearchFilter('custrecord_qcrules_item', null, 'anyof', ['@NONE@', Item]));

	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));



	var t1 = new Date();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_qc_rules', null, filters, null);
	if(searchresults !=null && searchresults != '' && searchresults.length>0)
	{
		return true;
	}	
	else
		return false;
}

function itemRemainingQuantity(poInternalId, itemId, lineno, itemQuantity, itemQuantityReceived,location, company){
	/*
	 * Fetch the check-in, putconfirm quantity from the transaction order details for the PO# and the line#
	 * if check-in quantity is 0 (Zero)
	 * 	remaining quantity = order quantity
	 * else if check-in quantity has value 
	 *		remaining quantity = order quantity - (check-in quantity) - quantity received
	 */

	nlapiLogExecution('ERROR','itemId',itemId);
	nlapiLogExecution('ERROR','lineno',lineno);
	nlapiLogExecution('ERROR','poInternalId',poInternalId);
	nlapiLogExecution('ERROR','itemQuantity',itemQuantity);
	nlapiLogExecution('ERROR','itemQuantityReceived',itemQuantityReceived);

	nlapiLogExecution('ERROR','Item Remaining Quantity','About to calculate');

	var checkinQuantity = 0;
	var remainingQuantity = 0;
	var putConfirmQuantity = 0;

	var transactionFilters = new Array();
	transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poInternalId);
	transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', itemId);
	if(lineno!=null && lineno !=""){
		transactionFilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno);
	}

	var transactionColumns = new Array();
	transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
	transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	transactionColumns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	transactionColumns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');

	var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);

	if (transactionSearchresults != null && transactionSearchresults.length > 0)
	{
		for(var i = 0; i <= transactionSearchresults.length; i++)
		{
			checkinQuantity = transactionSearchresults[0].getValue('custrecord_orderlinedetails_checkin_qty');
			putConfirmQuantity = transactionSearchresults[0].getValue('custrecord_orderlinedetails_putconf_qty');
		}
	}

	if(checkinQuantity == "")
	{
		checkinQuantity = 0;
	}

	if(putConfirmQuantity == "")
	{
		putConfirmQuantity = 0;
	}

	nlapiLogExecution('ERROR','Check-In Quantity',checkinQuantity);
	nlapiLogExecution('ERROR','Quantity',itemQuantity);
	nlapiLogExecution('ERROR','Received Quantity',itemQuantityReceived);
	nlapiLogExecution('ERROR','Put Confirm Quantity',putConfirmQuantity);

	if (checkinQuantity == 0)
	{
		remainingQuantity = parseFloat(itemQuantity);
	}
	else{
		remainingQuantity = parseFloat(itemQuantity) - (parseFloat(checkinQuantity)-parseFloat(putConfirmQuantity)) - parseFloat(itemQuantityReceived);
		nlapiLogExecution('ERROR','Remaining Quantity',remainingQuantity);

		/*		if(putConfirmQuantity == 0)		
		{
			remainingQuantity = parseFloat(itemQuantity) - parseFloat(checkinQuantity) - parseFloat(itemQuantityReceived);
			nlapiLogExecution('ERROR','Remaining Quantity: Checked-in',remainingQuantity);
		}
		else
		{
			remainingQuantity = parseFloat(itemQuantity) - parseFloat(itemQuantityReceived);
			nlapiLogExecution('ERROR','Remaining Quantity: Putaway Confirmed',remainingQuantity);
		}
		 */
	}
	return remainingQuantity;
}

function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	nlapiLogExecution('ERROR','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;

	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 	
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true); 	

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null && itemSearchResults != '')
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,customizetype,StageBinInternalId,ordertype){

	nlapiLogExecution('Error', 'into GetPickStageLocation', Item);
	nlapiLogExecution('Error', 'Item', Item);
	nlapiLogExecution('Error', 'vSite', vSite);
	nlapiLogExecution('Error', 'vCompany', vCompany);
	nlapiLogExecution('Error', 'stgDirection', stgDirection);
	nlapiLogExecution('Error', 'vCarrier', vCarrier);
	nlapiLogExecution('Error', 'vCarrierType', vCarrierType);
	nlapiLogExecution('Error', 'ordertype', ordertype);
	nlapiLogExecution('Error', 'customizetype', customizetype);

	//var ordertype='5'; //Order Type ="WORK ORDER";
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();

	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 = "";
	var ItemInfo3 = "";
	var columns = nlapiLookupField('item', Item, fields);
	if(columns!=null){
		ItemFamily = columns.custitem_item_family;
		ItemGroup = columns.custitem_item_group;
		ItemStatus = columns.custitem_ebizdefskustatus;
		ItemInfo1 = columns.custitem_item_info_1;
		ItemInfo2 = columns.custitem_item_info_2;
		ItemInfo3 = columns.custitem_item_info_3;
	}

	nlapiLogExecution('Error', 'ItemFamily', ItemFamily);
	nlapiLogExecution('Error', 'ItemGroup', ItemGroup);
	nlapiLogExecution('Error', 'ItemStatus', ItemStatus);
	nlapiLogExecution('Error', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('Error', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('Error', 'ItemInfo3', ItemInfo3);

	nlapiLogExecution('Error', 'Before Variable declaration');

	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}

	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');
	columns[5] = new nlobjSearchColumn('custrecord_stagerule_ordtype');

	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
	if (vCarrier != null && vCarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

	}
	if (vCarrierType != null && vCarrierType != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}
	if (ordertype != null && ordertype != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@', ordertype]));

	}
	else
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@']));
	if (customizetype != null && customizetype != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@', customizetype]));

	}
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction]));
	filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'anyof', ['@NONE@','3']));

	/*if (StageBinInternalId != null && StageBinInternalId != "")
		//filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
	filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_outboundlocationgroup', 'anyof', StageBinInternalId));*/
	nlapiLogExecution('Error', 'Before Searching of Stageing Rules');

	var vstgLocation,vstgLocationText;
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < Math.min(10, searchresults.length); i++) {
			//GetPickStagLpCount                
			//fectch stage location for Ordertype = WORK ORDER		  
			var ordertype = searchresults[i].getText('custrecord_stagerule_ordtype');
			nlapiLogExecution('Error', 'ordertype::', ordertype);
			//if(ordertype == 'WORK ORDER'){
				vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');
				vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');
			//}

			nlapiLogExecution('Error', 'Stageing Location Value::', vstgLocation);
			nlapiLogExecution('Error', 'Stageing Location Text::', vstgLocationText);


			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');

			nlapiLogExecution('Error', 'vnoofFootPrints::', vnoofFootPrints);


			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetPickStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}


			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);
			nlapiLogExecution('Error', 'LocGroup::', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}

				//This code is commented on 9th September 2011 by ramana
				/*
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				for (var k = 0; searchresults != null && k < searchresults.length; k++) {
					var vLocid=searchresults[k].getId();
						var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
						var vnoofFootPrints = searchresults[k].getValue('custrecord_nooffootprints');

						//--
						if (vnoofFootPrints != null && vnoofFootPrints != "") {
								if (vLpCnt < vnoofFootPrints) {
									return vLocid;
								}
						}
					else {
						return vLocid;
						}
				}
				 */

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('Error', 'searchresultsloc::', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();
//					var vLocid=searchresultsloc[0].getId();

					nlapiLogExecution('Error', 'vLocid::', vLocid);

					var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');

					//--
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}
			}
		}
	}
	return -1;
}



function GetPickStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');

	var outLPCount = 0;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));

	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));

	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');

	return outLPCount;
}





function DeleteInvtRecdOfMemberItems(WoIntid, BinLocId,WHLocation)
{
	nlapiLogExecution('ERROR','Inside DeleteInvtRecCreatedforCHKNTask ','Funciton');
	nlapiLogExecution('ERROR','WoIntid ',WoIntid);
	nlapiLogExecution('ERROR','BinLocId ',BinLocId);
	nlapiLogExecution('ERROR','WHLocation',WHLocation);



	var Ifilters = new Array();
	Ifilters[0] = new nlobjSearchFilter('custrecord_ebiz_transaction_no', null, 'anyof', WoIntid);
	Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18,36]);
	//Ifilters[2] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', BinLocId);
	Ifilters[2] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', WHLocation);

	var invtId="";
	var invtType="";
	var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (serchInvtRec) 
	{
		for (var s = 0; s < serchInvtRec.length; s++) {
			nlapiLogExecution('ERROR','serchInvtRec.length',serchInvtRec.length);
			nlapiLogExecution('ERROR','Inside loop','loop');
			var searchresult = serchInvtRec[ s ];
			nlapiLogExecution('ERROR','need to print this','print');
			invtId = serchInvtRec[s].getId();
			invtType= serchInvtRec[s].getRecordType();
			nlapiLogExecution('ERROR','CHKN INVT Id',invtId);
			nlapiLogExecution('ERROR','CHKN invtType ',invtType);
			nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
			nlapiLogExecution('ERROR','Invt Deleted record Id',invtId);

		}
	}
}
