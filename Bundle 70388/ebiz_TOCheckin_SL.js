/***************************************************************************
	  		 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_TOCheckin_SL.js,v $
 *     	   $Revision: 1.3.2.11.4.3.2.27.2.2 $
 *     	   $Date: 2015/11/05 17:35:19 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * DESCRIPTION
 * REVISION HISTORY
 * $Log: ebiz_TOCheckin_SL.js,v $
 * Revision 1.3.2.11.4.3.2.27.2.2  2015/11/05 17:35:19  nneelam
 * case# 201415348
 *
 * Revision 1.3.2.11.4.3.2.27.2.1  2015/09/21 11:37:07  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.3.2.11.4.3.2.27  2015/05/27 08:00:34  grao
 * SB issue fixes  201412876
 *
 * Revision 1.3.2.11.4.3.2.26  2015/04/30 13:06:00  schepuri
 * case# 201412575
 *
 * Revision 1.3.2.11.4.3.2.25  2015/04/13 09:26:10  skreddy
 * Case# 201412323
 * changed the url path which was hard coded
 *
 * Revision 1.3.2.11.4.3.2.24  2014/12/24 13:34:58  schepuri
 * issue# 201411241
 *
 * Revision 1.3.2.11.4.3.2.23  2014/09/18 13:27:15  sponnaganti
 * Case# 201410423
 * Stnd Bundle Issue fix
 *
 * Revision 1.3.2.11.4.3.2.22  2014/08/25 15:36:32  skavuri
 * Case# 20149908 StdBundle Issue Fixed
 *
 * Revision 1.3.2.11.4.3.2.21  2014/08/08 15:00:28  skavuri
 * Case# 20149908 SB Issue Fixed
 *
 * Revision 1.3.2.11.4.3.2.20  2014/07/22 15:58:30  sponnaganti
 * Case# 20149551
 * Compatibility Issue fix
 *
 * Revision 1.3.2.11.4.3.2.19  2014/07/03 15:28:33  sponnaganti
 * case# 20149259 20149260
 * Comaptability Acc Issue Fix
 *
 * Revision 1.3.2.11.4.3.2.18  2014/05/27 15:36:12  sponnaganti
 * case# 20148549
 * Stnd Bundle Issue fix
 *
 * Revision 1.3.2.11.4.3.2.17  2014/05/20 15:22:20  sponnaganti
 * Case# 20148438
 * Stnd Bundle Issue fix
 *
 * Revision 1.3.2.11.4.3.2.16  2014/05/12 14:01:41  skreddy
 * case # 20148356
 * Ryonet prod issue fix
 *
 * Revision 1.3.2.11.4.3.2.15  2014/03/04 08:58:47  rmukkera
 * Case # 20127196
 *
 * Revision 1.3.2.11.4.3.2.14  2014/02/26 15:14:41  nneelam
 * case#  20127381
 * Standard Bundle Issue Fix.
 *
 * Revision 1.3.2.11.4.3.2.13  2014/02/12 15:16:36  grao
 * Case# 20127054 related issue fixes in MHP SB issue fixes
 *
 * Revision 1.3.2.11.4.3.2.12  2014/02/05 14:47:49  sponnaganti
 * case# 20126898
 * Itemstatus in line level is binded with default item status of location
 *
 * Revision 1.3.2.11.4.3.2.11  2014/02/05 06:27:45  grao
 * Case# 20127067  related issue fixes in Standard bundle issue fixes
 *
 * Revision 1.3.2.11.4.3.2.10  2014/02/04 14:23:47  rmukkera
 * Case # 20127042,20127044
 *
 * Revision 1.3.2.11.4.3.2.9  2014/01/29 15:24:01  rmukkera
 * Case # 20126898
 *
 * Revision 1.3.2.11.4.3.2.8  2014/01/24 14:49:22  rmukkera
 * Case # 20126898
 *
 * Revision 1.3.2.11.4.3.2.7  2014/01/23 14:14:51  rrpulicherla
 * Ryonet issue fix
 *
 * Revision 1.3.2.11.4.3.2.6  2014/01/16 13:36:51  schepuri
 * 20126831
 * standard bundle issue fix
 *
 * Revision 1.3.2.11.4.3.2.5  2014/01/15 13:44:22  schepuri
 * 20126736
 * standard bundle issue fix
 *
 * Revision 1.3.2.11.4.3.2.4  2013/05/21 15:06:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.3.2.11.4.3.2.3  2013/05/16 17:31:13  kavitha
 * CASE201112/CR201113/LOG201121
 * Lexjet production Issue fix
 *
 * Revision 1.3.2.11.4.3.2.2  2013/05/07 15:11:16  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.3.2.11.4.3.2.1  2013/03/01 14:34:59  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.3.2.11.4.3  2013/02/20 23:26:39  kavitha
 * CASE201112/CR201113/LOG201121
 * 20121556  Ryonet SB issue
 *
 * Revision 1.3.2.11.4.2  2013/02/07 08:50:40  skreddy
 * CASE201112/CR201113/LOG201121
 * GUI Lot auto generating FIFO enhancement
 *
 * Revision 1.3.2.11.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.2.11  2012/09/03 10:19:30  schepuri
 * CASE201112/CR201113/LOG201121
 * issue related  to recommended qty
 *
 * Revision 1.3.2.10  2012/08/24 18:24:39  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changes
 *
 * Revision 1.3.2.9  2012/07/31 10:47:21  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue related to Qty as NAN
 *
 * Revision 1.3.2.8  2012/07/30 23:20:38  gkalla
 * CASE201112/CR201113/LOG201121
 * NSUOM issue
 *
 * Revision 1.3.2.7  2012/05/28 15:37:24  spendyala
 * CASE201112/CR201113/LOG201121
 * Setting generate location checkbox checked by default.
 *
 * Revision 1.3.2.6  2012/04/30 15:12:23  gkalla
 * CASE201112/CR201113/LOG201121
 * To get item status list based on location in transferorder
 *
 * Revision 1.3.2.5  2012/04/25 15:46:55  spendyala
 * CASE201112/CR201113/LOG201121
 * While Showing Recommended qty POoverage is not considered.
 *
 * Revision 1.3.2.4  2012/04/20 13:13:08  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.3.2.3  2012/04/16 15:00:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Calculating POoverage is moved to general function.
 *
 * Revision 1.3.2.2  2012/04/13 22:21:51  spendyala
 * CASE201112/CR201113/LOG201121
 * Depending upon POoverage Value mentioned in the item master
 * value of poOverage will be calculated accordingly.
 *
 * Revision 1.3.2.1  2012/04/11 22:10:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Displaying CheckIn qty is resolved.
 *
 * Revision 1.3  2011/12/15 14:10:12  schepuri
 * CASE201112/CR201113/LOG201121
 * modified field names in customrecord_ebiznet_trn_poreceipt
 *
 * Revision 1.2  2011/10/18 16:54:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * TO Generate Locations
 *
 * Revision 1.1  2011/09/28 11:40:05  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.1  2011/09/22 08:36:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 *
 *****************************************************************************/
function getTranIDForPO(tranType, poID) {
	var tranID = null;
	var poRecord = nlapiLoadRecord(tranType, poID);
	if (poRecord != null)
		tranID = poRecord.getFieldValue('tranid');
	return tranID;
}


function addPOFieldAsHRef(form) {
	var poField = form.addField('custpage_po', 'select', 'Created From',
	'transaction');
	poField.setDefaultValue(request.getParameter('poID'));
	poField.setDisplayType('inline');
	poField.setLayoutType('startrow');
}

/*
 * Create a HREF for New PO Receipt. This will be next to the receipt drop down.
 */
function addPOReceiptURL(environment, poId, tranId, poLineNum) {
	var poReceiptURL = nlapiResolveURL('SUITELET',
			'customscript_ebiznet_poreceipt', 'customdeploy_poreceipt');

	// Get the FQDN depending on the context environment
	/*var fqdn = getFQDNForHost(environment);

	var ctx = nlapiGetContext();

	if (fqdn == '')
		nlapiLogExecution('ERROR', 'Unable to retrieve FQDN based on context');
	else
		poReceiptURL = getFQDNForHost(ctx.getEnvironment()) + poReceiptURL;*/

	poReceiptURL = poReceiptURL + '&custparam_poid=' + poId;
	poReceiptURL = poReceiptURL + '&custparam_povalue=' + tranId;
	poReceiptURL = poReceiptURL + '&custparam_polinenum=' + poLineNum;

	return poReceiptURL;
}

/*
 * Add receipt field and fill data in drop down NOTE: This does not add the New
 * PO Receipt HREF
 */
function addReceiptFieldToForm(form) {
	var poRcptQtyArr = new Array;
	var selectionArr = new Array(); // temporary array to return rcptQty,
	// trailer, bol

	var receipt = form.addField('custpage_poreceipt', 'select', 'Receipt');

	var rcptFilters = new Array();
	rcptFilters[0] = new nlobjSearchFilter('custrecord_ebiz_poreceipt_pono', null, 'anyof', request.getParameter('poID'));
	rcptFilters[1] = new nlobjSearchFilter('custrecord_ebiz_poreceipt_lineno', null, 'is', request.getParameter('poLineNum'));

	var rcptColumns = new Array();
	rcptColumns[0] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptno');
	rcptColumns[1] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptqty');
	rcptColumns[2] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_trailerno');
	rcptColumns[3] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_bol');

	var rcptSearchResults = nlapiSearchRecord(
			'customrecord_ebiznet_trn_poreceipt', null, rcptFilters,
			rcptColumns);

	if (rcptSearchResults != null) {
		var resultCount = rcptSearchResults.length;
		nlapiLogExecution('ERROR', 'rcptSearchResults.length', resultCount);
		for ( var i = 0; i < Math.min(500, resultCount); i++) {
			receipt.addSelectOption(rcptSearchResults[i].getValue('custrecord_ebiz_poreceipt_receiptno'),
					rcptSearchResults[i].getValue('custrecord_ebiz_poreceipt_receiptno'));
			poRcptQtyArr[i] = rcptSearchResults[i].getValue('custrecord_ebiz_poreceipt_receiptqty');
			nlapiLogExecution('ERROR', 'PO Rcpt Qty', poRcptQtyArr[i]);
		}

		receipt.setDefaultValue(rcptSearchResults[resultCount - 1].getValue('custrecord_ebiz_poreceipt_receiptno'));
		selectionArr[0] = poRcptQtyArr[resultCount - 1];
		selectionArr[1] = rcptSearchResults[resultCount - 1].getValue('custrecord_ebiz_poreceipt_trailerno');
		selectionArr[2] = rcptSearchResults[resultCount - 1].getValue('custrecord_ebiz_poreceipt_bol');
	}

	return selectionArr;
}

/*
 * Get total check in quantity
 */
function getTotalCheckinQuantity(poID, poLineNum) {
	// Define search filter to get checkin qty for particular CHKN & PO
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is',
			poID);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', 1);// CHKN
	// task
	// type
	filters[2] = new nlobjSearchFilter('custrecord_line_no', null, 'is',
			poLineNum);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty');

	var searchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',
			null, filters, columns);
	var checkInQty = 0;

	if (searchResults != null) {
		for ( var i = 0; i < searchResults.length; i++) {
			var searchResult = searchResults[i];
			checkInQty = checkInQty
			+ parseFloat(searchResult.getValue('custrecord_expe_qty'));
		}
	}

	nlapiLogExecution('ERROR', 'CheckIn Qty', checkInQty);

	return checkInQty;
}

/*
 * Adding item sublist for checkin
 */
function addItemSublistToForm(form, itemID, poLineNum, poSerialNos, poRcptQty, poItemStatus,poQtyChkn,poOverage,location,CaptureExpiryDate,ShelfLife,poID) {
	var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
	var columns = nlapiLookupField('item', itemID, fields);
	var ItemType;
	var serialInflg="F";		
	var batchflag="F";
	var vExpDate,vBatchNo;

	if(columns != null && columns != '')
	{
		ItemType = columns.recordType;	
		serialInflg = columns.custitem_ebizserialin;
		batchflag= columns.custitem_ebizbatchlot;
	}


	var itemRec = nlapiLoadRecord(ItemType, itemID);
	var itemName = itemRec.getFieldValue('itemid');
	var itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
	nlapiLogExecution('ERROR', 'CheckIn itemName', itemName);
	nlapiLogExecution('ERROR', 'request.getParameter(poSerialNos)',
			poSerialNos);

	var itemSubList = form.addSubList("custpage_items", "inlineeditor",
	"ItemList");
	itemSubList.addField("custpage_chkn_pfvalidateflag", "text", "validateflag").setDisplayType('hidden');

	itemSubList.addField("custpage_chkn_line", "text", "Line #")
	.setDefaultValue(poLineNum);
	itemSubList.addField("custpage_chkn_sku", "text", "Item").setDisabled(true)
	.setDefaultValue(itemName);
	itemSubList.addField("custpage_chkn_itemdesc", "text", "Item Desc")
	.setDisabled(true).setDefaultValue(itemDesc);
	
	var vItemStatus = itemSubList.addField("custpage_chkn_skustatus", "select", "Item Status",'customrecord_ebiznet_sku_status').setDefaultValue(poItemStatus);

/*	var vItemStatus = itemSubList.addField("custpage_chkn_skustatus", "select", "Item Status");

	// Case# 20126898 starts (for displaying particular item status in TO
	// Checkin UI )

	var itemStatusFilters = new Array();
	itemStatusFilters[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	itemStatusFilters[1] = new nlobjSearchFilter(
			'custrecord_allowrcvskustatus', null, 'is', 'T');

	// Case # 20126898?start
	itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',
			null, 'anyof', location);

	var itemStatusColumns = new Array();

	itemStatusColumns[0] = new nlobjSearchColumn('name');
	itemStatusColumns[1] = new nlobjSearchColumn('internalid');
	itemStatusColumns[2] = new nlobjSearchColumn('custrecord_defaultskustaus');

	// Case # 20126898?end
	itemStatusColumns[0].setSort();
	itemStatusColumns[1].setSort();

	var itemStatusSearchResult = nlapiSearchRecord(
			'customrecord_ebiznet_sku_status', null, itemStatusFilters,
			itemStatusColumns);

	nlapiLogExecution('ERROR', 'Location infomartion for check in', location);

	if (itemStatusSearchResult != null) {
		for ( var i = 0; i < itemStatusSearchResult.length; i++) {
			vItemStatus.addSelectOption(itemStatusSearchResult[i]
					.getValue('internalid'), itemStatusSearchResult[i]
					.getValue('name'));
		}
		for ( var i = 0; i < itemStatusSearchResult.length; i++) {
			nlapiLogExecution('ERROR', 'default item status check',
					itemStatusSearchResult[i]
							.getValue('custrecord_defaultskustaus'));
			if (itemStatusSearchResult[i]
					.getValue('custrecord_defaultskustaus') == 'T') {
				vItemStatus.setDefaultValue(itemStatusSearchResult[i]
						.getValue('internalid'));
			}
		}
	}*/

	// for itemstatus of particular item
	/*
	 * var filters = new Array(); filters.push(new
	 * nlobjSearchFilter('internalid', null, 'is', itemID)); filters.push(new
	 * nlobjSearchFilter('isinactive', null, 'is', 'F'));
	 * 
	 * var columns = new Array(); columns[0] = new nlobjSearchColumn('itemid');
	 * columns[1] = new nlobjSearchColumn('custitem_ebizdefskustatus');
	 * 
	 * var itemSearchResults = nlapiSearchRecord('item', null, filters,
	 * columns);
	 * 
	 * var vpoItemStatus,vpoItemStatus_Text;
	 * 
	 * if (itemSearchResults != null){ vpoItemStatus =
	 * itemSearchResults[0].getValue('custitem_ebizdefskustatus');
	 * vpoItemStatus_Text =
	 * itemSearchResults[0].getText('custitem_ebizdefskustatus'); //for (var i =
	 * 0; i < itemSearchResults.length; i++) //{
	 * //vItemStatus.addSelectOption(itemSearchResults[i].getValue('custitem_ebizdefskustatus'),
	 * itemSearchResults[i].getText('custitem_ebizdefskustatus')); //} }
	 * 
	 * vItemStatus.setDefaultValue(vpoItemStatus);
	 */
	// case# 20126898 ends
	itemSubList.addField("custpage_chkn_pc", "text", "Pack Code")
	.setDefaultValue('1');
	itemSubList.addField("custpage_chkn_buom", "text", "Base UOM")
	.setDefaultValue('EACH');

	if (poRcptQty != "" && poRcptQty != null)
		itemSubList.addField("custpage_chkn_rcvngqty", "text", "Receive Qty")
		.setDefaultValue(poRcptQty);
	else
		itemSubList.addField("custpage_chkn_rcvngqty", "text", "Receive Qty")
		.setMandatory(true);

	itemSubList.addField("custpage_chkn_lp", "text", "LP(License Plate)")
	.setMandatory(true);
	//case# 201410423 starts (Enable LOT# only for lot item)
	//itemSubList.addField("custpage_lotbatch", "text", "LOT#");
	if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
	{
		itemSubList.addField("custpage_lotbatch", "text", "LOT#");
	}
	else
	{
		itemSubList.addField("custpage_lotbatch", "text", "LOT#").setDisabled(true);
	}
	//case# 201410423 ends
	if(CaptureExpiryDate == 'T')
	{
		itemSubList.addField("custpage_hdncatchexpdate", "text", "Expiry Date(MMDDYY)");
	}
	else
	{
		itemSubList.addField("custpage_hdncatchexpdate", "text", "Expiry Date(MMDDYY)").setDisplayType('hidden');
	}
	itemSubList.addField("custpage_capexpirydateflag", "text", "CapExpiryFalg ").setDisplayType('hidden').setDefaultValue(CaptureExpiryDate);
	itemSubList.addField("custpage_shelflife", "text", "ShelfLife").setDisplayType('hidden').setDefaultValue(ShelfLife);
	itemSubList.addField("custpage_chkn_binlocation", "select", "Bin Location",
	"customrecord_ebiznet_location");

	itemSubList.addField("custpage_iteminternalid", "text", "Item Id")
	.setDisplayType('hidden').setDefaultValue(itemID);
	itemSubList.addField("custpage_serialno", "text", "Item SerialNo")
	.setDisplayType('hidden').setDefaultValue(poSerialNos);
	nlapiLogExecution('ERROR', 'pooverage',	poOverage);
	itemSubList.addField("custpage_pooverage", "text", "PoOverage")
	.setDisplayType('hidden').setDefaultValue(poOverage);

	nlapiLogExecution('ERROR', 'poRcptQty',	parseFloat(poRcptQty));
	nlapiLogExecution('ERROR', 'poOverage',	poOverage);

	itemSubList.addField("custpage_chkn_qty", "text", "CheckIn Qty").setDisplayType('hidden');
//	start of case no 20126736
//	validating bathno from item fulfillment for advanced bin management
	var vAdvBinManagement=false;
	var ItemType = '';
	var batchflg = 'F';

	var ctx = nlapiGetContext();
	if(ctx != null && ctx != '')
	{
		if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
			vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
	}
	var fields = ['recordType', 'custitem_ebizbatchlot'];
	var columns = nlapiLookupField('item', itemID, fields);
	if(columns != null && columns != '')
	{
		ItemType = columns.recordType;					
		batchflg = columns.custitem_ebizbatchlot;
	}

	if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || ItemType=="serializedinventoryitem")
	{
		var IsValidSerailNumber='F';
		var trecord = nlapiLoadRecord('transferorder', poID);
		//case# 20149551 starts
		var itemfulfilserialno='';
		var itemTempfulfilserialno='';
		//case# 20149551 ends
		var links=trecord.getLineItemCount('links');
		if(links!=null  && links!='')
		{
			for(var j=0;j<links &&  IsValidSerailNumber=='F';j++)
			{
				var id=trecord.getLineItemValue('links','id',(parseInt(j)+1));
				var linktype = trecord.getLineItemValue('links','type',(parseInt(j)+1));

				nlapiLogExecution('ERROR', 'linktype',linktype);
				nlapiLogExecution('ERROR', 'id',id);

				if(linktype=='Item Fulfillment' || linktype=='Item Shipment')
				{
					var frecord = nlapiLoadRecord('itemfulfillment', id);
					if(vAdvBinManagement)
					{		
						//var itemfulfilserialno;
						//var itemTempfulfilserialno;
						//case# 20148549 starts
						var itemcount=frecord.getLineItemCount('item');
						for(var h=1;h<=itemcount;h++)
						{
							//frecord.selectLineItem('item', 1);
							frecord.selectLineItem('item', h);
							
							var compSubRecord = frecord.viewCurrentLineItemSubrecord('item','inventorydetail');
							if(compSubRecord !=null && compSubRecord!='' && compSubRecord!='null' && compSubRecord!='undefined')
							{
								var polinelength = compSubRecord.getLineItemCount('inventoryassignment');
								nlapiLogExecution('ERROR', 'polinelength', polinelength);
								
								for(var k=1;k<=polinelength ;k++)
								{
									itemfulfilserialno=compSubRecord.getLineItemText('inventoryassignment','issueinventorynumber',k);
									nlapiLogExecution('ERROR', 'itemfulfilserialno', itemfulfilserialno);
									if(itemfulfilserialno!=null && itemfulfilserialno!='')
									{
										if(itemTempfulfilserialno == null || itemTempfulfilserialno == '')
											itemTempfulfilserialno=itemfulfilserialno;
										else										 
											itemTempfulfilserialno= itemTempfulfilserialno + ',' + itemfulfilserialno;

									}
									nlapiLogExecution('ERROR', 'itemTempfulfilserialno', itemTempfulfilserialno);
								}
								
							}
						}//case# 20148549 ends
						//case# 20149551 starts(if links length is greaterthan 1 then addfield is calling two times here script error occured)
						/*itemSubList.addField("custpage_serial_fulfillserialno", "text", "PoOverage11")
						.setDisplayType('hidden').setDefaultValue(itemTempfulfilserialno);*/
						//case# 20149551 ends
					}
					// end of case no 20126736
				}
			}
			//case# 20149551 starts
			if(itemTempfulfilserialno!=null && itemTempfulfilserialno!='')
			itemSubList.addField("custpage_serial_fulfillserialno", "text", "PoOverage11")
			.setDisplayType('hidden').setDefaultValue(itemTempfulfilserialno);
			//case# 20149551 ends
			if(IsValidSerailNumber=='F')
			{
				alert("Lot# is not matching with Lot# picked on the same TO ");

				return false; 
			}
			else
				return true;
		}
	}



}

function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', 3);
	//Case # 20127044 Start
	if(location!=null && location!='')
	{
		itemFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]);
	}
	//Case # 20127044 End
	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function checkPOOverage(POId,location,company){
	nlapiLogExecution('ERROR','PO Internal Id', POId);
	var poOverage = 0;

	var poFields = ['custbody_nswmsporeceipttype'];
	var poColumns = nlapiLookupField('transaction', POId, poFields);

	var receiptType = poColumns.custbody_nswmsporeceipttype;

	nlapiLogExecution('ERROR','here', receiptType);

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		nlapiLogExecution('ERROR','here', receiptType);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('ERROR','Out of check PO Overage', poOverage);
	return poOverage;
}

/*
 * Process HTTP Get Request
 */
function TOcheckInGETRequest(request, response) {
	// create the Suitelet form
	var form = nlapiCreateForm('TransferOrder Check-In');

	var ctx = nlapiGetContext();
	var ShelfLife='',CaptureExpiryDate=''; 

	/*
	 * Registering client script This script is registered against a client
	 * script. This is invoked whenever the 'ADD' button is selected against any
	 * order line
	 */
	form.setScript('customscript_chknlpvalidation');

	var poID = request.getParameter('poID');
	var poLineNum = request.getParameter('poLineNum');
	var poLineQty = request.getParameter('poLineQty');
	var poQtyChkn = request.getParameter('QtyChkn');
	var poQtyRem = request.getParameter('QtyRem');
	var itemID = request.getParameter('poLineItem');
	var poSerialNos = request.getParameter('poSerialNos');
	var poItemStatus = request.getParameter('poItemStatus');
	var poItemUOM = request.getParameter('poItemUOM');	
	var poItemPackcode = request.getParameter('poItemPackcode');	
	nlapiLogExecution('ERROR', 'poQtyRem before', poQtyRem);
	nlapiLogExecution('ERROR', 'poQtyChkn before', poQtyChkn);
	// To get PO location and recordType
	var fields = [ 'recordType', 'transferlocation', 'custbody_nswms_company','tranid'];
	var columns = nlapiLookupField('transaction', poID, fields);
	var tranType = columns.recordType;
	var location = columns.transferlocation;
	var company = columns.custbody_nswms_company;
	var poValue = columns.tranid;

	nlapiLogExecution('ERROR', 'poname', poValue);
	var vbaseuom='';
	var vbaseuomqty='';
	var vuomqty='';
	var vuomlevel='';

	if(poItemUOM!=null && poItemUOM!='')
	{
		var eBizItemDims=geteBizItemDimensions(itemID);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					vbaseuom = eBizItemDims[z].getText('custrecord_ebizuomskudim');
					vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
				}
				nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
				nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
				if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
				{
					vuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
					vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
				}
			}
			if(vuomqty==null || vuomqty=='')
			{
				vuomqty=vbaseuomqty;
			}

			if(poLineQty==null || poLineQty=='' || isNaN(poLineQty))
				poLineQty=0;
			else
				poLineQty = (parseFloat(poLineQty)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

			if(poQtyChkn==null || poQtyChkn=='' || isNaN(poQtyChkn))
				poQtyChkn=0;
			else
				poQtyChkn = (parseFloat(poQtyChkn)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

			if(poQtyRem==null || poQtyRem=='' || isNaN(poQtyRem))
				//poQtyChkn=0;
				poQtyRem=0; // Case# 20149908
			else
				poQtyRem = (parseFloat(poQtyRem)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

		}
	}

	if(parseFloat(poQtyRem) > parseFloat(poLineQty))
		poQtyRem = poLineQty;



	nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
	nlapiLogExecution('ERROR', 'baseuom', vbaseuom);
	nlapiLogExecution('ERROR', 'baseuomqty', vbaseuomqty);
	nlapiLogExecution('ERROR', 'uomqty', vuomqty);
	nlapiLogExecution('ERROR', 'uomlevel', vuomlevel);
	nlapiLogExecution('ERROR', 'poLineQty', poLineQty);
	nlapiLogExecution('ERROR', 'quantityRcvd', poQtyChkn);
	nlapiLogExecution('ERROR', 'RemQty', poQtyRem);

	var transactionFilters = new Array();
	transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poID);
	transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', itemID);
	transactionFilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', poLineNum);


	var transactionColumns = new Array();
	transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
	transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	transactionColumns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	transactionColumns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');
	transactionColumns[4] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');

	var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);
	var vCheckQty=0,putConfirmQuantity=0;
	if(transactionSearchresults != null && transactionSearchresults != "")
	{
		for(var h=0;h<transactionSearchresults.length;h++)
		{
			var vordQty = transactionSearchresults[h].getValue('custrecord_orderlinedetails_order_qty');
			vCheckQty = transactionSearchresults[h].getValue('custrecord_orderlinedetails_checkin_qty');
			putConfirmQuantity = transactionSearchresults[h].getValue('custrecord_orderlinedetails_putconf_qty');
			nlapiLogExecution('ERROR','vCheckQty',vCheckQty);
			nlapiLogExecution('ERROR','vordQty',vordQty);
			if(vordQty==null||vordQty=="")
				vordQty=0;
			if(vCheckQty==null||vCheckQty=="")
				vCheckQty=0;
			//poQtyRem=parseFloat(vordQty) - parseFloat(vCheckQty);
			//Case # 20149908 starts
			if(putConfirmQuantity ==null || putConfirmQuantity=="")
				putConfirmQuantity=0;
			//Case# 20149908 ends
		}	
	}	

	if (vCheckQty == 0)
	{
		poQtyRem = parseFloat(poLineQty);
	}
	else{
		//poQtyRem = parseFloat(poLineQty) - (parseFloat(vCheckQty)-parseFloat(putConfirmQuantity)) - parseFloat(poQtyChkn);
		poQtyRem=parseFloat(vordQty) - parseFloat(vCheckQty);// case# 201412575
		nlapiLogExecution('ERROR','Remaining Quantity',poQtyRem);
	}

	var genLocnCheckBox = form.addField('custpage_genloccheck', 'checkbox',
	'Generate Locations').setDefaultValue('T');

	nlapiLogExecution('ERROR', 'PO Id from Client', poID);
	nlapiLogExecution('ERROR', 'location', location);

	// Getting Tran Id for this particular id and sending to PO receipt screen.
	var tranID = getTranIDForPO(tranType, poID);
	nlapiLogExecution('ERROR', 'tranID', tranID);

	// add the PO field and make it a hyperlink
	addPOFieldAsHRef(form);

	// Added for Location select field
	var poLocation = form.addField('custpage_loc_id', 'select', 'Location','location');
	poLocation.setDefaultValue(location);

	// Adding the receipt dropdown and the new PO receipt URL text
	// poOtherAttributesArr consists of 3 records i.e. poRcptQty, trailerNo, bol
	var poOtherAttributesArr = addReceiptFieldToForm(form);
	nlapiLogExecution('ERROR', 'after tranID poOtherAttributesArr[0]', poOtherAttributesArr[0]);
	var poRcptURL = addPOReceiptURL(ctx.getEnvironment(), poID, tranID,
			poLineNum);

	// add the quantity field, make it editable
	var itemQuanField = form.addField('custpage_item_quan', 'integer',
			'Quantity', null);
	itemQuanField.setDefaultValue(poLineQty);
	itemQuanField.setDisplayType('inline');

	// Add the PO Value field and make it hidden
	//var poValue = request.getParameter('poValue');
	var poValueField = form.addField('custpage_povalue', 'text', 'Tran Value');
	poValueField.setDefaultValue(poValue);
	poValueField.setDisplayType('hidden');
	//case# 20127054� starts
	// PO internalId
	var poIDpost=form.addField('custpage_poidpost','text','poIDpost').setVisible(false);
	poIDpost.setDefaultValue(poID);
	//case#20127054� ends
	// Item
	var item = form.addField('custpage_line_item', 'select', 'Item', 'item');
	item.setDefaultValue(itemID);
	item.setDisplayType('inline');

	// Line No.
	var lineNo = form.addField('custpage_line_itemlineno', 'integer', 'Line No');
	lineNo.setDefaultValue(poLineNum);
	lineNo.setDisplayType('inline');

	// Trailer
	var trailer = poOtherAttributesArr[1];

	var trailerNo = form.addField('custpage_trailerno', 'select', 'Trailer',
	'customrecord_ebiznet_trailer');
	if (trailer != null && trailer != "") {
		trailerNo.setDefaultValue(trailer);
		trailerNo.setDisplayType('inline');
	}

	// New PO Receipt URL
//	var createNewReqLink = form.addField('custpage_porecipt_link',
//	'inlinehtml', null, null, null);
//	createNewReqLink.setDefaultValue('<B><A HREF="#" onclick="javascript:window.open('
//	+ "'" + poRcptURL + "'" + ');">New RMA Receipt</A></B>');

	// add the remaining quantity field, make it editable
	var remainingItemQtyField = form.addField('custpage_remainitem_quan',
			'float', 'Remaining Quantity', null).setDisabled(true);
	//remainingItemQtyField.setDefaultValue(request.getParameter('custparam_remainingqty'));
	remainingItemQtyField.setDefaultValue(poQtyRem);

	// add the Check-In Quantity field, make it editable
	var checkinQtyField = form.addField('custpage_checkin_quan', 'float',
			'Check-In Quantity', null);
	checkinQtyField.setDisabled(true);
	//checkinQtyField.setDefaultValue(getTotalCheckinQuantity(poID, poLineNum));
	checkinQtyField.setDefaultValue(poQtyChkn);

	var transactionTypeField = form.addField('custpage_trantype', 'text',
	'Trantype');
	transactionTypeField.setDefaultValue(tranType);
	transactionTypeField.setDisplayType('hidden');

	// BOL
	var bol = poOtherAttributesArr[2];
	var bolNoField = form.addField('custpage_bolno', 'text', 'BOL');
	if (bol != null && bol != "") {
		bolNoField.setDefaultValue(bol);
		bolNoField.setDisplayType('inline');
	}

	// Company
	var chknCompany = form.addField('custpage_company', 'select', 'Company',
	'customrecord_ebiznet_company');
	chknCompany.setDefaultValue(company);
	chknCompany.setDisplayType('inline');

	// add the Priority Putaway Quantity field
	nlapiLogExecution('ERROR', 'location', location);
	var priorityPutawayLocnArr = priorityPutaway(itemID, poLineQty,location);
	var priorityQty = priorityPutawayLocnArr[1];

	var priorityQtyField = form.addField('custpage_priority_quan', 'float',
			'Pick Face Recommended Quantity', null);
	priorityQtyField.setDisabled(true);
	priorityQtyField.setDefaultValue(priorityQty);

	// add the Recommended Quantity field//shylaja

	var ItemRecommendedQuantity =0;
	var hdnItemRecmdQtyWithPOoverage=0;

	var poOverage=GetPoOverage(itemID,poID,location);

	//var poOverage = checkPOOverage(getPOInternalId,getWHLocation, null);
	nlapiLogExecution('ERROR','poOverage', poOverage);

	var palletQuantity = fetchPalletQuantity(itemID,location,null);
	if (poOverage == null || poOverage == ""){
		poOverage = 0;	
	}
//	else{
//	ItemRecommendedQuantity = Math.min (parseFloat(poQtyRem) + parseFloat((poOverage * poLineQty)/100), palletQuantity);

	hdnItemRecmdQtyWithPOoverage = Math.min (parseFloat(poQtyRem) + parseFloat((poOverage * poLineQty)/100), palletQuantity);
	ItemRecommendedQuantity = Math.min (parseFloat(poQtyRem), palletQuantity);
//	}

	nlapiLogExecution('ERROR', 'RecommendedQtyField poLineQty', poLineQty);
	nlapiLogExecution('ERROR', 'RecommendedQtyField poOverage', poOverage);
	nlapiLogExecution('ERROR', 'RecommendedQtyField ItemRecommendedQuantity', parseFloat(poLineQty) + parseFloat((poOverage * poLineQty)/100));
	nlapiLogExecution('ERROR', 'RecommendedQtyField least ItemRecommendedQuantity', ItemRecommendedQuantity.toString());
	var RecommendedQtyField = form.addField('custpage_recommended_quan', 'integer',
			'Recommended Quantity', null);
	RecommendedQtyField.setDisabled(true);
	if(ItemRecommendedQuantity==null||ItemRecommendedQuantity==""||ItemRecommendedQuantity.toString()=="NaN")
	{
		ItemRecommendedQuantity = 0;
	}
	nlapiLogExecution('ERROR', 'RecommendedQtyField least ItemRecommendedQuantity', ItemRecommendedQuantity.toString());
	RecommendedQtyField.setDefaultValue(ItemRecommendedQuantity.toString());

	var ItemRecmdQtyWithPOoverageField = form.addField('custpage_itemrecmd_qty_withpooverage', 'float',
			'Recommended Quantity with poOverage', null);
	ItemRecmdQtyWithPOoverageField.setDisplayType('hidden');
	if(hdnItemRecmdQtyWithPOoverage==null||hdnItemRecmdQtyWithPOoverage==""||hdnItemRecmdQtyWithPOoverage.toString()=="NaN")
	{
		hdnItemRecmdQtyWithPOoverage = 0;
	}
	nlapiLogExecution('ERROR', 'RecommendedQtyField least ItemRecommendedQuantity', hdnItemRecmdQtyWithPOoverage.toString());
	ItemRecmdQtyWithPOoverageField.setDefaultValue(hdnItemRecmdQtyWithPOoverage.toString());

//	case no 20126831 start
	// field custitem_ebizdimprompt_2 is not available
	var poItemFields = ['custitem_ebizdimprompt','custitem_ebizweightprompt','upccode','custitem_ebiz_item_shelf_life','custitem_ebiz_item_cap_expiry_date'];
	// case no 20126831 end
	var rec = nlapiLookupField('item', itemID, poItemFields);

	//rec=nlapiLoadRecord('item',itemID);
	var promtDim=rec.custitem_ebizdimprompt;
	var promtWt=rec.custitem_ebizweightprompt;
	var UPC=rec.upccode;
	ShelfLife=rec.custitem_ebiz_item_shelf_life;
	CaptureExpiryDate=rec.custitem_ebiz_item_cap_expiry_date;


	// adding item sublist to the form
	addItemSublistToForm(form, itemID, poLineNum, poSerialNos,
			poOtherAttributesArr[0], poItemStatus,poQtyChkn,poOverage,location,CaptureExpiryDate,ShelfLife,poID);

	form.addSubmitButton('Submit');
	form.addPageLink('crosslink', 'Back to TransferOrder Check-In', nlapiResolveURL(
			'RECORD', tranType, poID));
	response.writePage(form);
}

/*
 * Get Stage Locn
 */
/*function getStageLocn() {
	var stageLocn = '';
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', null,
			'anyof', [ 29 ]);
	//var locnSearch = nlapiSearchRecord('customrecord_ebiznet_location', null,filters);
	var locnSearch = getStagingLocation();
	if (locnSearch != null)
		stageLocn = locnSearch[0].getId();

	return stageLocn;
}*/


/*
 * Get dock location
 */
/*function getDockLocn() {
	var dockLocn = '';
	var dockLocnFilter = new Array();
	dockLocnFilter[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid',
			null, 'anyof', [ 30 ]);
	//var dockLocnSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockLocnFilter);
	var dockLocnSearchResults = getDockLocation();

	if (dockLocnSearchResults != null)
		dockLocn = dockLocnSearchResults[0].getId();

	return dockLocn;
}*/

/*
 * Getting location attribute
 */
function getLocationAttribute(locnDimArray, attrName){
	var retVal;

	if(attrName == "LOCN_NAME"){
		if(locnDimArray[0] != "")
			retVal = locnDimArray[0];
		else
			retVal = "";
	}else if(attrName == "REMAINING_CUBE"){
		if (!isNaN(locnDimArray[1])) {
			retVal = locnDimArray[1];
		} else {
			retVal = 0;
		}
	}else if(attrName == "LOCN_ID"){
		if (!isNaN(locnDimArray[2])) {
			retVal = locnDimArray[2];
		} else {
			retVal = "";
		}
	}else if(attrName == "OB_LOCN_ID"){
		if (!isNaN(locnDimArray[3])) {
			retVal = locnDimArray[3];
		} else {
			retVal = "";
		}
	} else if (attrName == "OB_LOCN_ID") {
		if (!isNaN(locnDimArray[4])) {
			retVal = locnDimArray[4];
		} else {
			retVal = "";
		}
	} else if (attrName == "IB_LOCN_ID") {
		if (!isNaN(locnDimArray[5])) {
			retVal = locnDimArray[5];
		} else {
			retVal = "";
		}
	} else if (attrName == "PUT_SEQ") {
		if (!isNaN(locnDimArray[6])) {
			retVal = locnDimArray[6];
		} else {
			retVal = "";
		}
	} else if (attrName == "LOCN_REMAINING_CUBE") {
		if (!isNaN(locnDimArray[7])) {
			retVal = locnDimArray[7];
		} else {
			retVal = "";
		}
	}

	return retVal;
}
/*
 * Processing HTTP POST method
 */
function checkInPOSTRequest(request, response) {
	// this is the POST block
	/* To fetch Stage(Out) location */
	var stageLocn = '';
	var dockLocn = '';

	var t1 = new Date();
	
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'StageLocn Search', getElapsedTimeDuration(t1, t2));

	t1 = new Date();
	dockLocn = getDockLocation();
	t2 = new Date();
	nlapiLogExecution('ERROR', 'DockLocn Search', getElapsedTimeDuration(t1, t2));

	/*
	 * Qc Validation. We check QC flag in Item Master and if it exists bring the
	 * INB-Loc from QC validations record and create CHKN and PUTW records with
	 * status flag as 'N' and redirect to QC validation suitelet.
	 */

	// extract the values from the previous page
	var qcID = request.getParameter('custpage_line_item');
	var qcLineNo = request.getParameter('custpage_line_itemlineno');
	var fields = [ 'recordType', 'custitem_ebizqualitycheck' ];
	var columns = nlapiLookupField('item', qcID, fields);
	var lgItemType = columns.recordType;
	var qcValidate = columns.custitem_ebizqualitycheck;
	var company = request.getParameter('custpage_company');
	var po = request.getParameter('custpage_po');
	var poLocn = request.getParameter('custpage_loc_id');
	var poValue = request.getParameter('custpage_povalue');
	var quantity = request.getParameter('custpage_item_quan');
	var lineNo = request.getParameter('custpage_line_itemlineno');
	var remainingQty = request.getParameter('custpage_remainitem_quan');
	var lineCount = request.getLineItemCount('custpage_items');
	var poRcptNo = request.getParameter('custpage_poreceipt');
	var transactionType = request.getParameter('custpage_trantype');
	var genLocnCheckVal = request.getParameter('custpage_genloccheck');
	nlapiLogExecution('ERROR', 'Generate Location Selection', genLocnCheckVal);
	nlapiLogExecution('ERROR', 'transactionType', transactionType);
	nlapiLogExecution('ERROR', 'poLocn', poLocn);
	
	
	stageLocn = getStagingLocation(poLocn);

	var checkinLineQty = 0;
	var taskType = "";
	var wmsStatusFlag = "";
	var beginLocn = "";
	var endLocn = "";
	var chkAssignputW="";
	var chkQty=0;
	var vReturnProcess="";

	var vitemId = "";
	var vitemStatusinfo="";

	//Checking the Qty by opening mutiple browsers
	nlapiLogExecution('ERROR', 'lineCount1', lineCount);
	for ( var k = 1; k <= lineCount; k++) {

		chkQty = parseFloat(chkQty) + parseFloat(request.getLineItemValue('custpage_items','custpage_chkn_rcvngqty', k));
		vitemId = request.getLineItemValue('custpage_items','custpage_iteminternalid', k);
	}
	nlapiLogExecution('ERROR', 'po', po);
	//Added for checking qty purpose Added by ramana
	var posearchresults = nlapiLoadRecord('transferorder', po); //1020
	nlapiLogExecution('ERROR', 'posearchresults', posearchresults);
	var vponum= posearchresults.getFieldValue('tranid');		
	nlapiLogExecution('ERROR', 'vponum', vponum);
	//alert(vponum);
	var Qtyfilters = new Array();          
	Qtyfilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'is',vponum);
	Qtyfilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto',lineNo);
	Qtyfilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_item', null, 'is',vitemId);

	// Qtyfilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'equalTo',vponum);
	// Qtyfilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalTo',lineNo);

	var Qtycolumns = new Array();
	Qtycolumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');

	var searchQtyresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details',null, Qtyfilters, Qtycolumns);
	var PutwQty = 0;


	if(searchQtyresults!=null)
	{    						  			    	
		PutwQty = searchQtyresults[0].getValue('custrecord_orderlinedetails_checkin_qty');  
	}    				    
	nlapiLogExecution('ERROR', 'vponum', vponum);
	nlapiLogExecution('ERROR', 'lineNo', lineNo);	
	nlapiLogExecution('ERROR', 'PutwQty', PutwQty);				    
	nlapiLogExecution('ERROR', 'chkQty', chkQty);

	var totalchkQty = parseFloat(PutwQty) + parseFloat(chkQty);

	nlapiLogExecution('ERROR', 'totalchkQty', totalchkQty);		
	nlapiLogExecution('ERROR', 'quantity', quantity);		

	//if(parseFloat(PutwQty)>=parseFloat(chkQty))
	/*
	if(parseFloat(totalchkQty)>parseFloat(quantity))
	{			 
		nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
		var form = nlapiCreateForm('Check in');
		nlapiLogExecution('ERROR', 'Form Called', 'form');					  
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'PO Overage is not allowed. Change the quantity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
		nlapiLogExecution('ERROR', 'message', msg);					  
		response.writePage(form);
		nlapiLogExecution('ERROR', 'Write Page to form', 'Written');		
		return;					
	}*/  
	//}

	/*if(vReturnProcess=="CHECK")
		{
			nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
			var form = nlapiCreateForm('Check in');
			nlapiLogExecution('ERROR', 'Form Called', 'form');					  
			var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'PO Overage is not allowed. Change the quantity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
			nlapiLogExecution('ERROR', 'message', msg);					  
			response.writePage(form);
			nlapiLogExecution('ERROR', 'Write Page to form', 'Written');		
			return;	
		}*/
	//upto to here


	//upto to here




	/*
	 * Logic
	 * If QC Validation is not set for the item,
	 *		For all lines
	 * 			Create a CHKIN task in TRN_OPENTASK with begin locn as Stage Locn and End locn as Dock Locn
	 * 			Move record to EBIZTASK
	 * 			If LP has been specified, Create an LP record
	 * 			If Bin Location has been specified,
	 * 				Create a PUTW task in TRN_OPENTASK
	 * 			Else if Bin Location is not specified
	 * 				If user has selected 'Generate Location', then
	 * 					If priority putaway needs to be done,
	 * 						Get priority putaway locations
	 * 						Create PUTW task at priority putaway location
	 * 					End If
	 * 				End If
	 * 			End If
	 * 		End For
	 * End If
	 */

	if (qcValidate == 'F') {
		for ( var s = 1; s <= lineCount; s++) {
			// Retreiving line item details
			var itemName = request.getLineItemValue('custpage_items','custpage_chkn_sku', s);
			var pfqtyvalidateflag = request.getLineItemValue('custpage_items','custpage_chkn_pfvalidateflag', s);
			var itemStatus = request.getLineItemValue('custpage_items','custpage_chkn_skustatus', s);
			var packCode = request.getLineItemValue('custpage_items','custpage_chkn_pc', s);
			var baseUOM = request.getLineItemValue('custpage_items','custpage_chkn_buom', s);
			var lp = request.getLineItemValue('custpage_items','custpage_chkn_lp', s);
			var binLocn = request.getLineItemValue('custpage_items','custpage_chkn_binlocation', s);
			var receiveQty = request.getLineItemValue('custpage_items','custpage_chkn_rcvngqty', s);
			var itemDesc = request.getLineItemValue('custpage_items','custpage_chkn_itemdesc', s);
			var itemId = request.getLineItemValue('custpage_items','custpage_iteminternalid', s);
			var lotBatchNo = request.getLineItemValue('custpage_items','custpage_lotbatch', s);
			var lotWithQty = lotBatchNo + "(" + receiveQty + ")";

			chkQty = receiveQty;

			vitemStatusinfo = itemStatus;
			var ExpDate = request.getLineItemValue('custpage_items','custpage_hdncatchexpdate', s);
			var ShelfLife = request.getLineItemValue('custpage_items','custpage_shelflife', s);
			var CaptureExpiryDate = request.getLineItemValue('custpage_items','custpage_capexpirydateflag', s);
			nlapiLogExecution('ERROR', 'CaptureExpiryDate', CaptureExpiryDate);
			nlapiLogExecution('ERROR', 'ShelLife', ShelfLife);
			nlapiLogExecution('ERROR', 'ExpDate', ExpDate);



			// Suming the QTY to Send to Trn Tables.
			checkinLineQty = parseFloat(checkinLineQty) + parseFloat(receiveQty);

			// Creating CHKN record even if BIN Location is null or not.
			// Create CHKN task in trn_opentask
			// tasktype = 1(CHKN) and wmsStatusFlag = 1(INBOUND/CHECK-IN)
			// beginLocn = dockLocn and endLocn = stageLocn
			taskType = "1";
			wmsStatusFlag = "1";
			beginLocn = dockLocn;
			endLocn = stageLocn;
			//create new record in batch entry
			CreateBatchentryRecord( itemId,lotBatchNo,company,packCode,itemStatus,poLocn,ExpDate,CaptureExpiryDate,ShelfLife);

			var chknRecID = createRecordLocationNULL(poValue, itemId, quantity, receiveQty, lp, lineNo, 
					packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag,
					qcID, itemDesc, lotBatchNo, poLocn, lgItemType, lotWithQty, company);

			// Move task from TRN_OPENTASK to EBIZTASK
			MoveTaskRecord(chknRecID);

			// Create LP Record
			if (lp != "")
				createLPRecord(lp);

			if (binLocn != null){
				taskType = "2"; // PUTW
				wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
				beginLocn = binLocn;
				endLocn = "";

				// Create PUTW task
				var retPutRecId = createRecordLocationNULL(poValue, itemId, quantity, receiveQty, lp, lineNo, packCode, itemStatus,
						baseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn,
						lgItemType, lotWithQty, company);
			}else{
				if (genLocnCheckVal == 'T') {					
					// Calling Priority putaway function to get priority putaway location
					nlapiLogExecution('ERROR', 'Test', 'Test');
					//case# 20148438 starts
					//var priorityPutawayLocnArr = priorityPutaway(itemId, receiveQty);
					var priorityPutawayLocnArr = priorityPutaway(itemId, receiveQty,poLocn);	
					//case# 20148438 ends
					var priorityRemainingQty = priorityPutawayLocnArr[0];
					var priorityQty = priorityPutawayLocnArr[1];
					var priorityLocnID = priorityPutawayLocnArr[2];
					var zoneid = priorityPutawayLocnArr[3];

					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityRemainingQty', priorityRemainingQty);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityQty', priorityQty);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityLocnID', priorityLocnID);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityZoneID', zoneid);

					// If quantity can be putaway'ed on priority, create PUTW task in TRN_OPENTASK
					// set taskType = 2(PUTW) and wmsStatusFlag = 2(INBOUND/LOCATIONS ASSIGNED)
					// beginLocation = priorityLocnID and endLocn = ""
					if(pfqtyvalidateflag != "Y")
					{
						if (priorityQty != 0){
							taskType = "2"; // PUTW
							wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
							beginLocn = priorityLocnID;
							endLocn = "";
							//var systemlp = GetMaxLPNo(1, 1);
							var retPutRecId = CreatePUTWwithLocation(poValue, itemId, quantity, priorityQty, lp, lineNo,
									packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo,
									wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn, lgItemType, lotWithQty, company,'',zoneid);
						}
					}
					else
					{
						priorityRemainingQty = receiveQty;
					}
					nlapiLogExecution('ERROR', 'priorityRemainingQty', priorityRemainingQty);
					// Check if there is any remaining quantity to be putaway
					if (parseFloat(priorityRemainingQty) != 0) {
						// Item dimensions
						var arrDims = new Array();
						arrDims = getSKUCubeAndWeight(itemId, 1);
						nlapiLogExecution('ERROR', 'arrDims.length', arrDims.length);
						var itemCube = 0;
						if (arrDims.length > 0 && (!isNaN(arrDims[0]))) //BaseUOMItemCube
						{
							var uomqty = ((parseFloat(priorityRemainingQty))/(parseFloat(arrDims[1])));	//BaseUOMQty		
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));//BaseUOMItemCube
							//itemCube = (parseFloat(arrDims[0]) * parseFloat(priorityRemainingQty));
							nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
						} else {

							itemCube = 0;
							nlapiLogExecution('ERROR', 'elsepart:itemCube', itemCube);
						}

						//case# 20149259 20149260 starts()
						var filters2 = new Array();

						var columns2 = new Array();
						columns2[0] = new nlobjSearchColumn('custitem_item_family');
						columns2[1] = new nlobjSearchColumn('custitem_item_group');
						columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
						columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
						columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
						columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
						columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');
						filters2.push(new nlobjSearchFilter('internalid', null, 'is', itemId));
						var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

						var filters3=new Array();
						filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						var columns3=new Array();
						columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
						columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
						columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
						columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
						columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
						columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
						columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
						columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
						columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
						columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
						columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
						columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
						columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
						columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
						columns3[14] = new nlobjSearchColumn('formulanumeric');
						columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
						columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
						//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
						columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
						// Upto here on 29OCT2013 - Case # 20124515
						columns3[14].setSort();

						var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);
												
						
						//var LoctDims = GetPutawayLocation(itemId, null, itemStatus, null, itemCube, lgItemType,poLocn);
						var LoctDims = GetPutawayLocationNew(itemId, packCode, itemStatus, packCode, parseFloat(itemCube), lgItemType,poLocn,ItemInfoResults,putrulesearchresults);
						//case# 20149259 20149260 ends
						var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube,putmethod,putrule;
						if (LoctDims.length > 0) {
							LocName = getLocationAttribute(LoctDims, "LOCN_NAME");
							RemCube = getLocationAttribute(LoctDims, "REMAINING_CUBE");
							LocId = getLocationAttribute(LoctDims, "LOCN_ID");
							OubLocId = getLocationAttribute(LoctDims, "OB_LOCN_ID");
							PickSeq = getLocationAttribute(LoctDims, "PICK_SEQ");
							InbLocId = getLocationAttribute(LoctDims, "IB_LOCN_ID");
							PutSeq = getLocationAttribute(LoctDims, "PUT_SEQ");
							LocRemCube = getLocationAttribute(LoctDims, "LOCN_REMAINING_CUBE");
							zoneid=LoctDims[8];
							putmethod=LoctDims[9];
							putrule=LoctDims[10];
						} else {
							LocName = "";
							LocId = "";
							RemCube = "";
							InbLocId = "";
							OubLocId = "";
							PickSeq = "";
							PutSeq = "";
							zoneid="";
							putmethod='';
							putrule='';
						}

						chkAssignputW  = "Y";
						// Creating Putaway record in TRN_OPENTASK
						taskType = "2"; // PUTW
						wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
						beginLocn = LocId; // Identified location
						endLocn = "";						
						var putwrecordid = CreatePUTWwithLocation(poValue, itemId, quantity, priorityRemainingQty, lp, lineNo,
								packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo,
								wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn, lgItemType, lotWithQty, company,itemCube, zoneid,putmethod,putrule);
						if (putwrecordid != "") {
							nlapiLogExecution('ERROR', 'binLocn', binLocn);
							nlapiLogExecution('ERROR', 'RemCube', RemCube);
							if (binLocn == null && RemCube >= 0 && LocId!=null && LocId!='') {
								var retValue = UpdateLocCube(LocId, RemCube);

							}
						}
					}// /If priroty remaining QTY is Zero
				} else {
					// create PUTW task in TRN_OPENTASK
					taskType = "2"; // PUTW
					wmsStatusFlag = "6"; // INBOUND/NO LOCATIONS ASSIGNED
					beginLocn = "";
					endLocn = "";
					var retPutRecId = createRecordLocationNULL(poValue, itemId, quantity, receiveQty, lp, lineNo, packCode,
							itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag, qcID, itemDesc,
							lotBatchNo, poLocn, lgItemType, lotWithQty, company);
				}
			}
		}// end of For loop lines count.
	} else if (qcValidate == 'T') {
		nlapiLogExecution('ERROR', 'IF QC validate flag is True', qcValidate);
		nlapiLogExecution('ERROR', 'IF QC validate flag is lineCnt ', lineCount);

		for ( var s = 1; s <= lineCount; s++) {

			// Retreiving line item details
			var ItemName = request.getLineItemValue('custpage_items', 'custpage_chkn_sku', s);
			var ItemStatus = request.getLineItemValue('custpage_items', 'custpage_chkn_skustatus', s);
			var PackCode = request.getLineItemValue('custpage_items', 'custpage_chkn_pc', s);
			var BaseUOM = request.getLineItemValue('custpage_items', 'custpage_chkn_buom', s);
			var LP = request.getLineItemValue('custpage_items', 'custpage_chkn_lp', s);
			var BinLoc = request.getLineItemValue('custpage_items', 'custpage_chkn_binlocation', s);
			var RcvQty = request.getLineItemValue('custpage_items', 'custpage_chkn_rcvngqty', s);
			var ItemDesc = request.getLineItemValue('custpage_items', 'custpage_chkn_itemdesc', s);
			var ItemId = request.getLineItemValue('custpage_items', 'custpage_iteminternalid', s);
			var LotBatch = request.getLineItemValue('custpage_items', 'custpage_lotbatch', s);
			var lotwithqty = LotBatch + "(" + RcvQty + ")";

			vitemStatusinfo = ItemStatus;

			// Suming the QTY to Send to Trn Tables.
			checkinLineQty = parseFloat(checkinLineQty) + parseFloat(RcvQty);

			// Create CHKN task in TRN_OPENTASK
			// taskType = 1(CHKN) and wmsStatusFlag = 1(INBOUND/CHECK-IN)
			// beginLocn = dockLocn and endLocn = stageLocn
			//create new record in batch entry.
			CreateBatchentryRecord( ItemId,LotBatch,company,PackCode,ItemStatus,poLocn,ExpDate,CaptureExpiryDate,ShelfLife);
			var QcChknRecId = createRecordLocationNULL(poValue, ItemId,
					quantity, RcvQty, LP, lineNo, PackCode, ItemStatus,
					BaseUOM, "1", dockLocn, stageLocn, po, poRcptNo,
					"1", qcID, ItemDesc, LotBatch, poLocn,
					lgItemType, lotwithqty, company);

			// move task to EBIZTASK
			MoveTaskRecord(QcChknRecId);

			// Create LP Record
			if (LP != "") {
				createLPRecord(LP);
			}

			// Create PUTW task in TRN_OPENTASK
			// taskType = 2(PUTW) and wmsStatusFlag = 23(INBOUND/QUALITY CHECK)
			// beginLocn = "" and endLocn = ""
			taskType = "2"; // PUTW
			wmsStatusFlag = "23"; // INBOUND/QUALITY CHECK
			beginLocn = "";
			endLocn = "";
			var QcChknRecId = createRecordLocationNULL(poValue, ItemId, quantity, RcvQty, LP, lineNo, PackCode, ItemStatus,
					BaseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag, qcID, ItemDesc, LotBatch, poLocn,
					lgItemType, lotwithqty, company);
		}

	}
	nlapiLogExecution('ERROR', 'transactionType', transactionType);
	var type = transactionType;// "purchaseorder";
	var putGenQty = 0;
	var putConfQty = 0;
	var ttype = "CHKN";
	var confQty = checkinLineQty;

	nlapiLogExecution('ERROR', 'checkInPOSTRequest:quantity', quantity);
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:confQty', confQty);
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:poValue', poValue);

	nlapiLogExecution('ERROR', 'chkAssignputW Value::', chkAssignputW);

	var tranValue = TrnLineUpdation(type, ttype, poValue, po, qcLineNo, qcID,
			request.getParameter('custpage_item_quan'), confQty,chkAssignputW,vitemStatusinfo);

	var poArray = new Array();
	poArray["custparam_po"] = po;
	poArray["custparam_povalue"] = poValue;






	// Redirecting to QC Validation screen if QC Validate is TRUE
	if (qcValidate == 'T') {
		nlapiLogExecution('ERROR', 'Navigating TO QC Screen');
		poArray["custparam_poqctemid"] = qcID;
		poArray["custparam_poqctemlineno"] = qcLineNo;
		poArray["custparam_company"] = company;

		response.sendRedirect('SUITELET', 'customscript_qc_list',
				'customdeploy_qc_list', false, poArray);
	} else {
		// redirect to the record in view mode(customscript_assignputaway)

		nlapiLogExecution('ERROR', 'Navigating', 'To Assign putaway');
		response.sendRedirect('SUITELET', 'customscript_to_putawayreport_sl',
				'customdeploy_to_putawayreport_di', false, poArray);
		var context = nlapiGetContext();
		var usageRemaining = context.getRemainingUsage();
		nlapiLogExecution('ERROR', 'usageRemaining', usageRemaining);
	}








}

/*
 * Main function to process PO Check-in
 */
function TOcheckInConceptSuitelet(request, response) {
	// obtain the context object
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') {
		// Creating the form for check-in

		TOcheckInGETRequest(request, response);
	} else {
		checkInPOSTRequest(request, response);
	}// End of else block.
}

/*
 * Create CHKN and PUTW Records .
 */
function createRecordLocationNULL(poValue, itemID, quantity, receiveQty, lp,
		lineNo, packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn,
		po, poReceiptNo, wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn,
		lgItemType, lotWithQty, vCompany) {
	var timeStamp1 = new Date();

	var t3 = new Date();
	var customRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	var t4 = new Date();
	nlapiLogExecution('ERROR', 'createRecordLocationNULL:nlapiCreateRecord|trn_opentask',
			getElapsedTimeDuration(t4, t3));

	// populating the fields
	customRecord.setFieldValue('name', poValue);
	customRecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
	if(po!=null&&po!="")
		customRecord.setFieldValue('custrecord_ebiz_order_no',po);
	customRecord.setFieldValue('custrecord_ebiz_sku_no', itemID);
	//customRecord.setFieldValue('custrecord_act_qty', parseFloat(quantity).toFixed(5));
	customRecord.setFieldValue('custrecord_act_qty', parseFloat(receiveQty).toFixed(5));
	customRecord.setFieldValue('custrecord_expe_qty', parseFloat(receiveQty).toFixed(5));
	customRecord.setFieldValue('custrecord_lpno', lp);
	customRecord.setFieldValue('custrecord_ebiz_lpno', lp);
	customRecord.setFieldValue('custrecord_line_no', lineNo);
	customRecord.setFieldValue('custrecord_packcode', packCode);
	customRecord.setFieldValue('custrecord_sku_status', itemStatus);
	customRecord.setFieldValue('custrecord_uom_id', baseUOM);
	customRecord.setFieldValue('custrecord_tasktype', taskType);
	customRecord.setFieldValue('custrecord_actbeginloc', beginLocn);

	// taskType = 1 (CHKN)
	if (taskType == "1") {
		customRecord.setFieldValue('custrecord_actbeginloc', beginLocn);
		customRecord.setFieldValue('custrecord_actendloc', endLocn);
		customRecord.setFieldValue('custrecordact_begin_date', DateStamp());
		customRecord.setFieldValue('custrecord_act_end_date', DateStamp());

		// Adding fields to update time zones.
		customRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
		customRecord.setFieldValue('custrecord_actualendtime', TimeStamp());
		customRecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customRecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
	}

	customRecord.setFieldValue('custrecord_current_date', DateStamp());
	customRecord.setFieldValue('custrecord_upd_date', DateStamp());
	customRecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
	customRecord.setFieldValue('custrecord_ebiz_receipt_no', poReceiptNo);

	// Status flag .
	customRecord.setFieldValue('custrecord_wms_status_flag', wmsStatusFlag);

	// Added for Item name and desc
	customRecord.setFieldValue('custrecord_sku', qcID);
	customRecord.setFieldValue('custrecord_skudesc', itemDesc);

	// LotBatch and batch with Quantity.
	if (lgItemType == "lotnumberedinventoryitem") {
		customRecord.setFieldValue('custrecord_batch_no', lotBatchNo);
		customRecord.setFieldValue('custrecord_lotnowithquantity', lotWithQty);
	}

	customRecord.setFieldValue('custrecord_wms_location', poLocn);
	customRecord.setFieldValue('custrecord_fifodate', DateStamp());
	customRecord.setFieldValue('custrecord_comp_id', vCompany);

	var currentUserID = getCurrentUser();

	customRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
	customRecord.setFieldValue('custrecord_ebizuser', currentUserID);
	customRecord.setFieldValue('custrecord_taskassignedto', currentUserID);

	// commit the record to NetSuite
	t3 = new Date();
	var recordId = nlapiSubmitRecord(customRecord);
	t4 = new Date();
	nlapiLogExecution('ERROR', 'createRecordLocationNULL:nlapiSubmitRecord|trn_opentask',
			getElapsedTimeDuration(t4, t3));

	nlapiLogExecution('ERROR', 'Check-In Record Insertion', 'Success');

	var invtRecordId = createInvtRecord(taskType, po, endLocn, lp, itemID,
			itemStatus, itemDesc, receiveQty, poLocn, lgItemType, packCode,
			vCompany);

	var timeStamp2 = new Date();
	nlapiLogExecution('ERROR', 'Elapsed time for createRecordLocationNULL',
			getElapsedTimeDuration(timeStamp1, timeStamp2));
	return recordId;
}

function createInvtRecord(taskType, po, endLocn, lp, itemId, itemStatus, itemDesc,
		receiveQty, poLocn, lgItemType, packCode, vCompany) {
	// Creating inventory for taskType = 1 (CHKN)
	if (taskType == "1") {
		try {
			// Creating Inventory Record.
			var t1 = new Date();
			var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
			var t2 = new Date();
			nlapiLogExecution('ERROR', 'createInvtRecord:nlapiCreateRecord|createinv',
					getElapsedTimeDuration(t2, t1)); 


			nlapiLogExecution('ERROR', 'Location',poLocn);                
			var varAccountNo='';
			var filtersAccNo = new Array();
			// filtersAccNo[0] = new nlobjSearchFilter('custrecord_siteid', null, 'is', 'New Jersey');
			filtersAccNo[0] = new nlobjSearchFilter('custrecord_location', null, 'is', poLocn);
			var colsAcc = new Array();
			colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
			var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);              
			if(Accsearchresults!=null&&Accsearchresults!="")
				varAccountNo = Accsearchresults[0].getValue('custrecord_accountno');                                
			nlapiLogExecution('ERROR', 'Account ',varAccountNo );


			invtRec.setFieldValue('name', po);
			invtRec.setFieldValue('custrecord_ebiz_inv_binloc', endLocn);
			invtRec.setFieldValue('custrecord_ebiz_inv_lp', lp);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemId);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
			invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packCode);
			invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(receiveQty).toFixed(5));

			invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemId);

			invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(receiveQty).toFixed(5));
			invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemDesc);
			invtRec.setFieldValue('custrecord_ebiz_inv_loc', poLocn);

			invtRec.setFieldValue('custrecord_invttasktype', 1); // CHKN..
			//invtRec.setFieldValue('custrecord_wms_inv_status_flag', 1); // CHKN
			invtRec.setFieldValue('custrecord_wms_inv_status_flag', 17); // CHKN,not receipt chk ,it is flag inv receiving at dock
			//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', 1);
			invtRec.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);
			//

			invtRec.setFieldValue('customrecord_ebiznet_location', endLocn); //Added by ramana on 30th may

			invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
			//

			nlapiLogExecution('ERROR', 'Location Information', endLocn);

			invtRec.setFieldValue('custrecord_ebiz_inv_company', vCompany);

			t1 = new Date();
			var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
			t2 = new Date();
			nlapiLogExecution('ERROR', 'createInvtRecord:nlapiSubmitRecord:createinv',
					getElapsedTimeDuration(t2, t1));

			if (invtRecordId != null) {
				nlapiLogExecution('ERROR', 'Inventory Record Creation', invtRecordId);
			} else {
				nlapiLogExecution('ERROR', 'Inventory Record Creation', 'Failed');
			}
		} catch (error) {
			nlapiLogExecution('ERROR', 'Check for error msg in create invt', error);
		}
	}
}

function createLPRecord(lp) {
	var timestamp1 = new Date();
	var t1 = new Date();
	var lpRecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord:nlapiCreateRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	lpRecord.setFieldValue('name', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);
	t1 = new Date();
	var recid = nlapiSubmitRecord(lpRecord);
	t2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord:nlapiSubmitRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	var timestamp2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord Duration',
			getElapsedTimeDuration(timestamp1, timestamp2));
}

function getStagingLocation(location){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	//case # 20127381
	
	stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', 'null', 'anyof', location));

	stageFilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', 'null', 'anyof', '8'));
	
	var Columns = new Array();
	Columns[0]=new nlobjSearchColumn('custrecord_locgrouptype', 'custrecord_inboundlocgroupid');
	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters,Columns);

	// This search could return more than one inbound staging location.  We are using the first one.
	var id="";
	for(var s = 0; stageLocnResults!=null && s < stageLocnResults.length ; s++)
	{
		var locgrptype = stageLocnResults[s].getText('custrecord_locgrouptype','custrecord_inboundlocgroupid');
		nlapiLogExecution('ERROR', 'locgrptype', locgrptype);
		if(locgrptype =='INBOUND')
		{
			nlapiLogExecution('ERROR', 'test1', locgrptype);
			retInboundStagingLocn = stageLocnResults[0].getId();
		}

		if(locgrptype=='BOTH')
		{
			nlapiLogExecution('ERROR', 'test2', locgrptype);
			id = stageLocnResults[0].getId();
		}
	}

	if(retInboundStagingLocn==null || retInboundStagingLocn=="")
		retInboundStagingLocn=id;

	return retInboundStagingLocn;
}

function getDockLocation(){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}




function CreatePUTWwithLocation(poValue, ItemId, quan, PriortyQty, LP, lineno,
		PackCode, ItemStatus, BaseUOM, tasktype, beginloc, endloc, po,
		poreceiptno, wmsStsflag, QcID, ItemDesc, LotBatch, poloc, lgItemType,
		lotwithqty, VCompany,itemCube, zoneid,putmethod,putrule) {
	var t1 = new Date();
	var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'CreatePUTWwithLocation:nlapiCreateRecord|trn_opentask',
			getElapsedTimeDuration(t2, t1));

	nlapiLogExecution('ERROR', 'Creating PUTW record with Location',
	'TRN_OPENTASK');
	// populating the fields
	putwrecord.setFieldValue('name', poValue);
	putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
	putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
	//putwrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(5));
	putwrecord.setFieldValue('custrecord_act_qty', parseFloat(PriortyQty).toFixed(5));
	putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(PriortyQty).toFixed(5));
	putwrecord.setFieldValue('custrecord_lpno', LP);
	putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
	putwrecord.setFieldValue('custrecord_line_no', lineno);
	putwrecord.setFieldValue('custrecord_packcode', PackCode);
	putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
	putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
	putwrecord.setFieldValue('custrecord_tasktype', tasktype);
	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();
	putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
	putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
	putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
	putwrecord.setFieldValue('custrecord_actbeginloc', beginloc);
	putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
	putwrecord.setFieldValue('custrecord_ebizzone_no', zoneid);
	//Case # 20127042 Start
	putwrecord.setFieldValue('custrecord_ebiz_order_no', po);
	//Case # 20127042 End
	//Addfor total cube	
	putwrecord.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(5));
	//
	nlapiLogExecution('ERROR', 'Total Cube in Checkin', parseFloat(itemCube).toFixed(5));

	// For eBiznet Receipt Number .
	putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

	// month+1 / date / year
	putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

	// Adding fields to update time zones.
	// hour:min:am/pm
	putwrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
	putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());

	// month+1 / date / year
	putwrecord.setFieldValue('custrecord_current_date', DateStamp());
	putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

	// Status flag .
	//case# 20149551 starts(if actualbegin loc is empty then we are updating wmsstatus with 6(Putaway locations not available))
	if(beginloc==null || beginloc=='')
	{
		putwrecord.setFieldValue('custrecord_wms_status_flag', '6');
	}
	else
	putwrecord.setFieldValue('custrecord_wms_status_flag', wmsStsflag);
	//case# 20149551 ends
	// Added for Item name and desc
	putwrecord.setFieldValue('custrecord_sku', ItemId);
	putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

	// LotBatch and batch with Quantity.
	if (lgItemType == "lotnumberedinventoryitem") {
		nlapiLogExecution('ERROR', 'lotnumberedinventoryitem PUTW record',
		'LOT-->TRN_OPENTASK');
		putwrecord.setFieldValue('custrecord_batch_no', LotBatch);
		putwrecord.setFieldValue('custrecord_lotnowithquantity', lotwithqty);
		putwrecord.setFieldValue('custrecord_fifodate', DateStamp());
	}
	putwrecord.setFieldValue('custrecord_wms_location', poloc);
	//putwrecord.setFieldValue('custrecord_fifodate', DateStamp());
	putwrecord.setFieldValue('custrecord_comp_id', VCompany);
	putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
	putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);
	// commit the record to NetSuite
	t2 = new Date();
	var putwrecordid = nlapiSubmitRecord(putwrecord);
	t1 = new Date();
	nlapiLogExecution('ERROR', 'CreatePUTWwithLocation:nlapiSubmitRecord|trn_opentask',
			getElapsedTimeDuration(t2, t1));
}
function CreateBatchentryRecord( ItemId,LotBatch,VCompany,PackCode,ItemStatus,poLocn,expdate,CaptureExpiryDate,ShelfLife)
{
	nlapiLogExecution('ERROR', 'CreateBatchentryRecord','done');
	nlapiLogExecution('ERROR', 'ItemId',ItemId);
	nlapiLogExecution('ERROR', 'LotBatch',LotBatch);
	nlapiLogExecution('ERROR', 'VCompany',VCompany);
	nlapiLogExecution('ERROR', 'PackCode',PackCode);
	nlapiLogExecution('ERROR', 'ItemStatus',ItemStatus);
	nlapiLogExecution('ERROR', 'poLocn',poLocn);
	nlapiLogExecution('ERROR', 'expdate',expdate);
	nlapiLogExecution('ERROR', 'CaptureExpiryDate',FifoDate);	
	nlapiLogExecution('ERROR', 'ShelfLife',ShelfLife);

	if(LotBatch!=null && LotBatch!='')
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[ItemId]));
		filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',LotBatch));

		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
		column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
		column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
		column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
		column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
		column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');

		var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);

		if(searchRecord==null || searchRecord=='')
		{



			var FifoDate='';
			var ExpiryDate='';
			FifoDate=DateStamp();
			if(CaptureExpiryDate == 'F'){
				if(ShelfLife !=null && ShelfLife!=''){
					var sysdate=DateStamp();
					var d = new Date(sysdate);
					d.setDate( d.getDate()+ parseInt(ShelfLife));
					ExpiryDate=((d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear());
					nlapiLogExecution('ERROR', 'ExpiryDate', ExpiryDate);
					nlapiLogExecution('ERROR', 'sysdate', sysdate);
				}
				else
				{

					ExpiryDate='01/01/2099';
				}
			}
			else
			{
				if(expdate != null && expdate != '')
				{	
					var vdate= RFDateFormat(expdate);
					if(vdate!=null && vdate !=''&& vdate[0]=='true')
					{
						nlapiLogExecution('ERROR','Mfgdate',vdate[1]);
						ExpiryDate=vdate[1];

					}
				}
				else
				{
					if(ShelfLife !=null && ShelfLife!=''){
						var sysdate=DateStamp();
						var d = new Date(sysdate);
						d.setDate( d.getDate()+ parseInt(ShelfLife));
						ExpiryDate=((d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear());
						nlapiLogExecution('ERROR', 'ExpiryDate', ExpiryDate);
						nlapiLogExecution('ERROR', 'sysdate', sysdate);
					}
					else
					{

						ExpiryDate='01/01/2099';
					}
				}	

			}


			var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
			customrecord.setFieldValue('name', LotBatch);
			customrecord.setFieldValue('custrecord_ebizlotbatch', LotBatch);
			customrecord.setFieldValue('custrecord_ebizsku', ItemId);
			customrecord.setFieldValue('custrecord_ebizpackcode',PackCode);
			customrecord.setFieldValue('custrecord_ebizsitebatch',poLocn);
			customrecord.setFieldValue('custrecord_ebizcompanybatch',VCompany);
			customrecord.setFieldValue('custrecord_ebizexpirydate',ExpiryDate);
			customrecord.setFieldValue('custrecord_ebizfifodate',FifoDate);
			var rec = nlapiSubmitRecord(customrecord, false, true);
		}
	}
}
