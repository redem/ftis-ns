/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/UserEvents/Attic/ebiz_TranOrd_UpdatePrice_UE.js,v $
 *     	   $Revision: 1.1.2.6 $
 *     	   $Date: 2012/05/21 16:37:36 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_NSWMS_2013_1_3_14 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_TranOrd_UpdatePrice_UE.js,v $
 * Revision 1.1.2.6  2012/05/21 16:37:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * User Event to setTransfer Order's Transfer price
 *
 * Revision 1.1.2.5  2012/05/18 12:52:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 * User Event to setTransfer Order's Transfer price
 *
 * Revision 1.1.2.4  2012/05/18 12:45:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * User Event to setTransfer Order's Transfer price
 *
 * Revision 1.1.2.3  2012/05/18 08:44:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * User Event to setTransfer Order's Transfer price
 *
 

 **************************************************************************** */


//before submit function that will check the lines on a TO and any without transfer price will update with current avg cost

function UpdateTOLinePrice(type)
{	
	try{
		if(type == 'edit' || type == 'create')
		{
			var vTORestrictFlag='';
			
			var vConfig=nlapiLoadConfiguration('accountingpreferences');
			if(vConfig != null && vConfig != '')
			{
				vTORestrictFlag=vConfig.getFieldValue('ITEMCOSTASTRNFRORDCOST');
			}

			nlapiLogExecution('ERROR', 'vTORestrictFlag', vTORestrictFlag);

			if(vTORestrictFlag =='F')
			{
				var tran_ord = nlapiGetNewRecord();

				for(var line = 1; line <= tran_ord.getLineItemCount('item'); line++)
				{
					if(tran_ord.getLineItemValue('item', 'rate', line) != null && tran_ord.getLineItemValue('item', 'rate', line) == 0.0)
					{
						var avg_cost = tran_ord.getLineItemValue('item', 'averagecost', line);
						nlapiLogExecution('ERROR', 'avg_cost', avg_cost);
						if(avg_cost != null && avg_cost > 0)
						{
							tran_ord.setLineItemValue('item', 'rate', line, avg_cost);
						}
					}					
				}
			}
		}
	}catch(e){
		if(e instanceof nlobjError){
			nlapiLogExecution('ERROR', 'ERROR - TFP_Update_TO_LinePrice - system error', 'Error - ' + e.getCode() + '\n' + e.getDetails());
		}
		else{
			nlapiLogExecution('ERROR', 'ERROR - TFP_Update_TO_LinePrice - unexpected error', 'Error - ' + e.toString());
		}
	}
}