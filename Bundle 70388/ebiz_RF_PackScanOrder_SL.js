/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_PackScanOrder_SL.js,v $
 *     	   $Revision: 1.1.2.3.4.4.4.11 $
 *     	   $Date: 2014/06/23 08:17:02 $
 *     	   $Author: skavuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PackScanOrder_SL.js,v $
 * Revision 1.1.2.3.4.4.4.11  2014/06/23 08:17:02  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.1.2.3.4.4.4.10  2014/06/13 12:47:12  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.3.4.4.4.9  2014/05/30 00:41:03  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3.4.4.4.8  2014/05/27 13:57:22  snimmakayala
 * Case#: 20148567
 * RF Packing issue fixes
 *
 * Revision 1.1.2.3.4.4.4.7  2014/03/26 15:44:58  nneelam
 * case#  20127805
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3.4.4.4.6  2013/12/02 08:58:26  schepuri
 * 20125974
 *
 * Revision 1.1.2.3.4.4.4.5  2013/11/28 06:06:59  skreddy
 * Case# 20125944
 * Afosa SB issue fix
 *
 * Revision 1.1.2.3.4.4.4.4  2013/11/12 06:40:15  skreddy
 * Case# 20125642
 * Afosa SB issue fix
 *
 * Revision 1.1.2.3.4.4.4.3  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.3.4.4.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.3.4.4.4.1  2013/04/04 16:17:47  skreddy
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1.2.3.4.4  2013/01/02 15:42:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Double Click restriction.
 * .
 *
 * Revision 1.1.2.3.4.3  2012/12/31 05:52:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Restrict Packing for Closed and Paymeny Hold Orders.
 *
 * Revision 1.1.2.3.4.2  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.3.4.1  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.3  2012/06/28 08:33:39  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.1.2.2  2012/06/15 10:08:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.1.2.1  2012/06/06 07:39:03  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.32.4.21  2012/05/14 14:37:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to validating LP
 *
 * Revision 1.32.4.20  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.32.4.19  2012/05/09 15:58:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing Trantype in query string was missing
 *
 * Revision 1.32.4.18  2012/04/27 07:25:07  gkalla
 * CASE201112/CR201113/LOG201121
 * QC check based on rule configuration
 *
 * Revision 1.32.4.17  2012/04/11 22:02:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related navigating to TO Item Status when the user click on Previous button.
 *
 * Revision 1.32.4.16  2012/04/10 22:59:15  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related navigating to previous screen is resolved.
 *
 * Revision 1.32.4.15  2012/03/21 11:08:48  schepuri
 * CASE201112/CR201113/LOG201121
 * Added movetask
 *
 * Revision 1.32.4.14  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.32.4.13  2012/03/06 11:00:05  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.12  2012/03/05 14:42:30  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.11  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.43  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.42  2012/02/21 07:36:44  spendyala
 * CASE201112/CR201113/LOG201121
 * code merged upto 1.32.4.9
 *
 * Revision 1.41  2012/02/14 07:13:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged upto 1.32.4.8.
 *
 * Revision 1.40  2012/02/13 13:39:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.39  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.38  2012/02/10 10:25:15  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.6.
 *
 * Revision 1.37  2012/02/07 07:24:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.4.
 *
 * Revision 1.36  2012/02/01 12:53:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.3.
 *
 * Revision 1.35  2012/02/01 06:30:36  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.32.4.2
 *
 * Revision 1.34  2012/01/19 00:43:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Changes
 *
 * Revision 1.33  2012/01/09 13:11:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Issue if there a space in batch no
 *
 * Revision 1.32  2012/01/06 13:03:09  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing, redirecting to serialno entry
 *
 * Revision 1.31  2012/01/04 20:18:38  gkalla
 * CASE201112/CR201113/LOG201121
 * To add Item to Batch entry
 *
 * Revision 1.30  2012/01/04 15:08:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.29  2011/12/30 14:09:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.28  2011/12/27 23:24:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.27  2011/12/23 23:36:41  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.26  2011/12/23 16:21:51  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.25  2011/12/23 13:24:07  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.24  2011/12/23 13:19:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.23  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.22  2011/12/15 14:10:40  schepuri
 * CASE201112/CR201113/LOG201121
 * modified field names in customrecord_ebiznet_trn_poreceipt
 *
 * Revision 1.21  2011/12/02 13:19:35  schepuri
 * CASE201112/CR201113/LOG201121
 * RF To generate location acc to the location defined in PO
 *
 * Revision 1.20  2011/11/17 08:44:28  spendyala
 * CASE201112/CR201113/LOG201121
 * wms status flag for create inventory is changed to 17 i .e, FLAG INVENTORY INBOUND
 *
 * Revision 1.19  2011/09/29 12:04:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.18  2011/09/28 16:08:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/09/26 19:58:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.16  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.14  2011/09/14 05:27:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/09/12 08:24:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/09/10 11:28:23  schepuri
 * CASE201112/CR201113/LOG201121
 *
 * redirecting to customscript_serial_no
 *
 * Revision 1.11  2011/07/05 12:36:44  schepuri
 * CASE201112/CR201113/LOG201121
 * RF CheckIN changes
 *
 * Revision 1.10  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.9  2011/06/17 07:39:47  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * Confirm putaway changes
 *
 * Revision 1.8  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.6  2011/06/06 07:10:13  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * WBC Issues
 *
 * Revision 1.5  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function Packing_ScanOrder_SL(request, response){
	if (request.getMethod() == 'GET') 
	{
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12,st13,st14,st15;
		
		// 20125974
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st1 = "INGRESAR / ESCANEAR PEDIDO";
			//st2 = "SCANNED CANTIDAD MAYOR QUE EL PEDIDO CANTIDAD";
			st2 = "ENVIAR";
			st3 ="ANTERIOR";
			st4 = "ORDEN DE CIERRE";
			st5 = "PEDIDO #: ";
			st6 = "CAJA #: ";
			st7 = "ART&#205;CULO INV&#193;LIDO";
			st8 = "CANT ANALIZAR";
			st9 = "CANT";
			st10 = "CANT ENTER: ";
			st11 = "PR&#211;XIMO";
			

		}
		else
		{
			st1 = "ENTER/SCAN ORDER#: ";
			st2 = "SEND";
			st3 = "PREV";
			st4 = "CLOSE ORDER";			
			
		}
		
		var orderno=request.getParameter('custparam_orderno');
		var functionkeyHtml=getFunctionkeyScript('_rf_packing_scanorder'); 	
		var	html = "<html><head>" ;
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterOrder').focus();";        
		html = html + "</script>";	
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_packing_scanorder' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		if(orderno!=null && orderno!='')
		html = html + "				<td align = 'left'><input name='enterOrder' id='enterOrder' type='text' value=" + orderno + ">";
		else
		html = html + "				<td align = 'left'><input name='enterOrder' id='enterOrder' type='text'  />";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td >";
		html = html + "				<input type='hidden' name='hdncloseorder'>";
		html = html + "				<input type='hidden' name='hdnsubmit'>";
		html = html + "				<input type='hidden' name='hdnprevious'>";
		//html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnsubmit.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdcloseorder.disabled=true;return false'/>";
		html = html + "					"+st3+"  <input name='cmdPrevious' type='submit' value='F7' onclick='this.form.hdnprevious.value=this.value;this.form.submit();this.disabled=true;this.form.cmdcloseorder.disabled=true;this.form.cmdSend.disabled=true;return false'/></br>";
		html = html + "					"+st4+" <input name='cmdcloseorder' type='submit' value='F9' onclick='this.form.hdncloseorder.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSend.disabled=true;return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterOrder').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		//var optedEvent = request.getParameter('cmdPrevious');
		//var optedEventclose = request.getParameter('cmdcloseorder');

		var optedEvent = request.getParameter('hdnprevious');
		var optedEventclose = request.getParameter('hdncloseorder');
		var orderno = request.getParameter('enterOrder');
		//case 20125642 Start
		var getLanguage = request.getParameter('hdngetLanguage');
		var POarray=new Array();
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
       //end 20125642

		nlapiLogExecution('DEBUG','optedEvent',optedEvent);
		nlapiLogExecution('DEBUG','optedEventclose',optedEventclose);
		nlapiLogExecution('DEBUG','orderno',orderno);

		
		POarray["custparam_screenno"] = 'Pack1';
		
		//var SOID= GetSOInternalId(orderno,'salesorder');
		//nlapiLogExecution('DEBUG', 'SOID', SOID);
		

		if(optedEvent != 'F7' && optedEventclose!='F9' ){

			if(orderno!=null && orderno!='')
			{
				var filters = new Array();
				nlapiLogExecution('DEBUG','orderno in getSalesOrderlist',orderno);

				if(orderno!=""){
					filters.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
				}
				//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));//Status Flag - C (Picks generated)
				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('name');
				columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

				if(searchresults == null ||  searchresults == ''){

					POarray["custparam_error"] = "Invalid Order# :"+orderno+" ";
					POarray["custparam_orderno"] = orderno;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				else if(searchresults!=null && searchresults!='' && searchresults.length>0)
				{
					var vsalesOrderId = searchresults[0].getValue('custrecord_ebiz_order_no');

					var sostatus='';
					var paymentstatus='';

					var sostatusdet = new Array();

					sostatusdet = isOrderClosed(vsalesOrderId);
					if(sostatusdet!=null && sostatusdet!='' && sostatusdet.length>0)
					{
						sostatus=sostatusdet[0].getValue('status');
						paymentstatus=sostatusdet[0].getValue('paymenteventresult');
					}

					var str = 'sostatus. = ' + sostatus + '<br>';
					str += 'paymentstatus. = ' + paymentstatus + '<br>';

					nlapiLogExecution('DEBUG', 'SO Status Log', str);

					if(sostatus == 'closed')
					{
						POarray["custparam_error"] = "Order is already closed in Netsuite.So Packing is not allowed in eBizNET";
						POarray["custparam_orderno"] = orderno;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return;
					}

					if(paymentstatus == 'HOLD')
					{
						POarray["custparam_error"] = "A payment hold has been placed on this Sales Order.So Packing is not allowed in eBizNET";
						POarray["custparam_orderno"] = orderno;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return;
					}
				}

				var filterspickgen = new Array();
				nlapiLogExecution('DEBUG','orderno in getSalesOrderlist',orderno);

				if(orderno!=""){
					filterspickgen.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
				}

				filterspickgen.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8,9,28]));//Status Flag - C (Picks generated)
				filterspickgen.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK

				var columnspickgen = new Array();
				columnspickgen[0] = new nlobjSearchColumn('name');

				var searchresultspickgen = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterspickgen, columnspickgen);	

				if(searchresultspickgen == null ||  searchresultspickgen == ''){

					POarray["custparam_error"] = "Picks not generated for Order# :"+orderno+" ";
					POarray["custparam_orderno"] = orderno;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}


				var filterspickconfirmed = new Array();
				nlapiLogExecution('DEBUG','orderno in getSalesOrderlist',orderno);

				if(orderno!=""){
					filterspickconfirmed.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
				}

				filterspickconfirmed.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8,28]));//Status Flag - C (Picks Confirmed)
				filterspickconfirmed.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK

				var columnspickconfirmed = new Array();
				columnspickconfirmed[0] = new nlobjSearchColumn('name');

				var searchresultspickconfirmed = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterspickconfirmed, columnspickconfirmed);	

				if(searchresultspickconfirmed == null ||  searchresultspickconfirmed == ''){

					POarray["custparam_error"] = "Picks not Confirmed for Order# :"+orderno+" ";
					POarray["custparam_orderno"] = orderno;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}


				// Get containers to pack based on search criteria defined
				var salesOrderList = getSalesOrderlist(orderno);// case# 201417008
				if(salesOrderList != null && salesOrderList.length > 0){
					var orderno, containerno,containersize;
					for (var i = 0; i < salesOrderList.length; i++){

						containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
						containersize = salesOrderList[i].getValue('custrecord_container');
						skuno = salesOrderList[i].getValue('custrecord_sku');
						POarray["custparam_orderno"] = orderno;
						POarray["custparam_containerno"] = containerno;
						POarray["custparam_containersize"] = containersize;
						POarray["custparam_iteminternalid"] = skuno;
						POarray['custparam_wmslocation']= salesOrderList[i].getValue('custrecord_wms_location');
						POarray['custparam_loopcount']=salesOrderList.length;
						POarray['custparam_count']=salesOrderList.length;
						//POarray["custparam_expectedquantity"] = salesOrderList[i].getValue('custrecord_expe_qty');
						POarray["custparam_expectedquantity"] = salesOrderList[i].getValue('custrecord_act_qty');
						POarray["custparam_actualquantity"] = salesOrderList[i].getValue('custrecord_act_qty');						
						POarray["custparam_recordinternalid"]=salesOrderList[i].getId();
						POarray["custparam_waveno"]=salesOrderList[i].getValue('custrecord_ebiz_wave_no');
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scancontainer', 'customdeploy_ebiz_rf_pack_scancontainer', false, POarray);

					}
				}
				else
				{
					//POarray["custparam_error"] = "Invalid Order#### :"+orderno+" ";


					POarray["custparam_error"] = "Packing complete Order #  :"+orderno+" ";
					POarray["custparam_orderno"] = orderno;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
			}
			else
			{
				POarray["custparam_error"] = 'Please Enter Order #';				
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				return;
			}
		}
		else if(optedEventclose == 'F9')
		{
			if(orderno!=null && orderno!='')
			{
				var openTaskFilters = new Array();	

				if(orderno!=null && orderno!='')
				{
					openTaskFilters.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
				}
				openTaskFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); //PICK
				openTaskFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['9','25','15','26','8']));//9 //Picks generated or STATUS.INBOUND_OUTBOUND.EDIT or STATUS.OUTBOUND.SELECTED_INTO_WAVE or STATUS.OUTBOUND.FAILED
				// openTaskFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));

				var openTaskColumns = new Array();
				openTaskColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				openTaskColumns[1] = new nlobjSearchColumn('name');

				var openTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openTaskFilters, openTaskColumns);
				nlapiLogExecution('DEBUG', 'openSearchResults', openTaskSearchResults);

				if(openTaskSearchResults==null )
				{
					var openTaskFilters1 = new Array();
					var openTaskColumns = new Array();
					openTaskColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					openTaskColumns[1] = new nlobjSearchColumn('name');//
					openTaskColumns[2] = new nlobjSearchColumn('custrecord_wms_location');
					if(orderno!=null && orderno!='')
					{
						openTaskFilters1.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
					}
					openTaskFilters1.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
					//openTaskFilters1.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8']));
					var openTaskSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openTaskFilters1, openTaskColumns);
					nlapiLogExecution('DEBUG', 'openSearchResults1', openTaskSearchResults1);
					if(openTaskSearchResults1!=null && openTaskSearchResults1.length>0)   
					{
						nlapiLogExecution('DEBUG', 'openSearchResults1', openTaskSearchResults1.length);
						var vebizOrdNo=openTaskSearchResults1[0].getValue('custrecord_ebiz_order_no');
						var fulfillmentnumber=openTaskSearchResults1[0].getValue('name');
						var whlocation=openTaskSearchResults1[0].getValue('custrecord_wms_location');
						nlapiLogExecution('DEBUG', 'vebizOrdNo', vebizOrdNo);
						nlapiLogExecution('DEBUG', 'fulfillmentnumber', fulfillmentnumber);
						if(vebizOrdNo!=null && vebizOrdNo!='' && fulfillmentnumber!=null && fulfillmentnumber !='')
						{
							AutoPacking(vebizOrdNo,whlocation);
							var printername='';
							createpacklisthtml(vebizOrdNo,fulfillmentnumber,printername,orderno);

						}
					}
					else
					{
						POarray["custparam_error"] = "Invalid Order# :"+orderno+" ";
						POarray["custparam_orderno"] = orderno;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
				}
				else
				{				

					POarray["custparam_error"] =  "There are open picks/packs for this Order# "+orderno+", you cannot close.";
					POarray["custparam_orderno"] = orderno;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				}
			}
			else
			{
				POarray["custparam_error"] = 'Please Enter Order #';				
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}

		}////
		else
		{

			response.sendRedirect('SUITELET', 'customscript_rf_packing_menu', 'customdeploy_rf_packing_menu_di');
		}
	}
}

function getSalesOrderlist(orderno){
	var filters = new Array();
	nlapiLogExecution('DEBUG','orderno in getSalesOrderlist',orderno);

	if(orderno!=""){
		filters.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));// case# 201417008 have different while configuring the names of TO,SO,WO
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no',null, 'anyof', orderno)); // case# 201416831
	}
	filters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_comp_id');
	columns[5] = new nlobjSearchColumn('custrecord_site_id');
	columns[6] = new nlobjSearchColumn('custrecord_container');
	columns[7] = new nlobjSearchColumn('custrecord_total_weight');
	columns[8] = new nlobjSearchColumn('custrecord_sku');
	columns[9] = new nlobjSearchColumn('custrecord_wms_location');
	columns[10] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[12] = new nlobjSearchColumn('custrecord_act_qty');	
	
	//case # 20127805�
	columns[8].setSort();
	//columns[0].setSort();
	//columns[1].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	return searchresults;
}

function DataRec(vCompId, vSiteId, vEbizOrdNo,vOrdNo,vContainer,vPackStation,vEbizControlNo,vItem) 
{
	if(vCompId != null && vCompId != '')
		this.DataRecCompId = vCompId;
	else
		this.DataRecCompId=null;

	if(vSiteId != null && vSiteId != '')
		this.DataRecSiteId = vSiteId;
	else
		this.DataRecSiteId=null;
	this.DataRecEbizOrdNo = vEbizOrdNo;
	this.DataRecOrdNo = vOrdNo;
	this.DataRecContainer = vContainer;
	this.DataRecPackStation = vPackStation;
	this.DataRecEbizControlNo = vEbizControlNo;
	this.DataRecItem = vItem;	
}	
function createpacklisthtml(vebizOrdNo,fulfillmentnumber,printername,orderno)
{
	try
	{	
		nlapiLogExecution('DEBUG', 'createpackcodehtml ', 'createpackcodehtml');
		nlapiLogExecution('DEBUG', 'createpackcodehtmlvebizOrdNo ', vebizOrdNo);
		nlapiLogExecution('DEBUG', 'fulfillmentnumber ', fulfillmentnumber);
		var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
		nlapiLogExecution('DEBUG', 'trantype', trantype);

		var salesorderdetails =nlapiLoadRecord(trantype, vebizOrdNo);
		var billtoaddress=salesorderdetails.getFieldValue('billaddress'); 
		var shipaddress=salesorderdetails.getFieldValue('shipaddress');
		var locationId =salesorderdetails.getFieldValue('location');
		var expdate=salesorderdetails.getFieldValue('custbody_nswmspoexpshipdate');
		var orderdate=salesorderdetails.getFieldValue('trandate');
		var ordernumber=salesorderdetails.getFieldValue('tranid');
		var terms=salesorderdetails.getFieldText('terms');
		var customerpo=salesorderdetails.getFieldValue('otherrefnum');
		var entity=salesorderdetails.getFieldText('entity');
		if((expdate==null)||(expdate==''))
		{
			expdate="";
		}
		if((orderdate==null)||(orderdate==''))
		{
			orderdate="";
		}
		if((expdate==null)||(expdate==''))
		{
			expdate="";
		}
		if((customerpo==null)||(customerpo==''))
		{
			customerpo="";
		}
		if((terms==null)||(terms==''))
		{
			terms="";
		}

		var billaddressee="";var billaddr1="";var billcity="";var billcountry="";var billstate="";var billzip="";var billstateandcountry="";
		billaddressee=salesorderdetails.getFieldValue('billaddressee');
		billaddr1=salesorderdetails.getFieldValue('billaddr1');
		billcity=salesorderdetails.getFieldValue('billcity');
		billcountry=salesorderdetails.getFieldValue('billcountry');
		billstate=salesorderdetails.getFieldValue('billstate');
		billzip=salesorderdetails.getFieldValue('billzip');
		billstateandcountry=billcity+","+billstate+","+billzip;
		var shipaddressee="";var shipaddr1="";var shipcity="";var shipcountry="";var shipstate="";var shipzip="";var shipstateandcountry="";
		shipaddressee=salesorderdetails.getFieldValue('shipaddressee'); 
		shipaddr1=salesorderdetails.getFieldValue('shipaddr1'); 
		shipcity=salesorderdetails.getFieldValue('shipcity'); 
		shipcountry=salesorderdetails.getFieldValue('shipcountry'); 
		shipstate=salesorderdetails.getFieldValue('shipstate'); 
		shipzip=salesorderdetails.getFieldValue('shipzip'); 
		shipstateandcountry=shipcity+","+shipstate+","+shipzip;

		var shipmethod=salesorderdetails.getFieldText('shipmethod');
		var totalLine=salesorderdetails.getLineItemCount('item');
		var totalcube=0;
		var totalweight=0;
		for(var j=1;j<= parseFloat(totalLine);j++)
		{
			var item =salesorderdetails.getLineItemValue('item','item',j);
			var vUOM="";
			if (vUOM == "") {
				vUOM =1;
			}

			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', item));
			filters.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', vUOM));
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
			columns[1] = new nlobjSearchColumn('custrecord_ebiztareweight');
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

			for (var i = 0; searchresults != null && i < searchresults.length; i++)
			{
				var searchresult = searchresults[i];
				var vCube = searchresult.getValue('custrecord_ebizcube');
				totalcube=parseFloat(totalcube)+parseFloat(vCube);
				var vweight = searchresult.getText('custrecord_ebiztareweight');


				totalweight=parseFloat(totalweight)+parseFloat(vweight);
			}
		}
		if((totalweight.toString()=="NaN")||(totalweight==''))
		{
			totalweight='';
		}

		nlapiLogExecution('DEBUG', 'totalcube ', totalcube);
		nlapiLogExecution('DEBUG', 'totalweight ', totalweight);
		nlapiLogExecution('DEBUG', 'location ', locationId);
		var locationadress =nlapiLoadRecord('Location',locationId);

		var addr1="";var city="";var state="";var zip=""; var stateandzip=""; var returnadresse="";
		addr1=locationadress.getFieldValue('addr1');
		city=locationadress.getFieldValue('city');
		state=locationadress.getFieldValue('state');
		zip=locationadress.getFieldValue('zip');
		returnadresse=locationadress.getFieldValue('addressee');
		stateandzip=city+","+state+","+zip;

		var groupopentaskfilterarray = new Array();
		//Please uncheck  the fillementnumber code 
		groupopentaskfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',vebizOrdNo));
		groupopentaskfilterarray.push(new nlobjSearchFilter('name', null, 'is',fulfillmentnumber.toString()));
		groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3])); 

		var groupopentaskcolumnarray = new Array();
		groupopentaskcolumnarray[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
		groupopentaskcolumnarray[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
		groupopentaskcolumnarray[2] = new nlobjSearchColumn('custrecord_line_no', null, 'group');
		groupopentaskcolumnarray[3] = new nlobjSearchColumn('custrecord_act_end_date', null, 'group');

		groupopentaskcolumnarray[4] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');	

		var groupopentasksearchresults  = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, groupopentaskfilterarray, groupopentaskcolumnarray);
		var appendcontlp="";
		var actualenddate="";
		var strVar="";
		nlapiLogExecution('DEBUG', 'groupopentasksearchresults.length ', groupopentasksearchresults.length);
		if(groupopentasksearchresults.length!=null)
		{
			for(var g=0; g<groupopentasksearchresults.length;g++)
			{
				var repeatcontlp;
				var vContainerLpNo = groupopentasksearchresults [g].getValue('custrecord_container_lp_no', null, 'group');
				actualenddate = groupopentasksearchresults [g].getValue('custrecord_act_end_date', null, 'group');
				if(repeatcontlp!=vContainerLpNo)
				{ 
					if((vContainerLpNo!="")&&(vContainerLpNo!=null)&&(vContainerLpNo!="- None -"))
						appendcontlp +=vContainerLpNo +","; 
				}
				repeatcontlp=vContainerLpNo;
			}
			var totalamount='';
			var groupcount=groupopentasksearchresults.length;
			var grouplength=0;
			var invoicetasklength=groupopentasksearchresults.length;
			var linenumber=1;
			var pagecount=1;

			var totalinvoice=0;

			var totalcount=groupopentasksearchresults.length;
			var totalshipqty=0;
			while(0<totalcount)
			{
				//for(var h=0; h<totalcount;h++)
				//{
				var count=0;
				strVar +="<html>";
				strVar +="<body style=\"padding-right:0px; padding-left:0px;padding-top:0px;padding-bottom:0px;\">";
				strVar += "<table width=\"95%\" style=\"height:100%;\">";
				strVar += "<tr style=\"vertical-align:top\">";
				strVar += "                <td align=\"left\" style=\"width: 40%; font-size: 12px; vertical-align: top;\">";
				strVar += "                    <img src=\"headerimage\"\/><br \/>";
				strVar += "                    Ph. Inquiries:1-800-811-9342<br \/>";
				strVar += "                    Fax Inquiries:1-800-643-0639";
				strVar += "                <\/td>";
				strVar += "                <td align=\"left\" style=\"font-size: 12px; width: 20%; \">";
				strVar += "                    ***PACKING SLIP ***";
				strVar += "                <\/td>";
				strVar += "                <td align=\"right\" style=\"width: 40%\">";
				strVar += "                    <b>PACKING SLIP<\/b><br \/>";
				strVar += "                    <table border=\"0\" width=\"250px\" frame=\"box\" rules=\"all\" cellpadding=\"2\" cellspacing=\"0\" style=\"vertical-align: top;\">";
				strVar += "                        <tr>";
				strVar += "                              <td style=\"visibility: hidden; border: none;\">";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; background-color: Gray; border-top: 1px solid black;";
				strVar += "                                border-left: 1px solid black; border-bottom: 1px solid black;\">";
				strVar += "                                Taken By";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-top: 1px solid black; border-right: 1px solid black;";
				strVar += "                                border-left: 1px solid black; background-color: Gray; border-bottom: 1px solid black;\">";
				strVar += "                                ORDER #";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"visibility: hidden; border: none;\">";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-right: 1px solid black;\">";
				strVar += "                                maje";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black;\">";
				strVar += "                              "+fulfillmentnumber+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr style=\"background-color: Gray;\">";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;\">";
				strVar += "                                ORDER DATE";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;\">";
				strVar += "                                CUSTOMER PO#";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;";
				strVar += "                                border-right: 1px solid black;\">";
				strVar += "                                PAGE";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 12px; border-left: 1px solid black; border-top: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; text-align: center;\">";
				strVar += "                                "+orderdate+"";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-left: 1px solid black; border-top: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; text-align: center;\">";
				strVar += "                                "+customerpo+"";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; border-top: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;\">";
				strVar += "                                "+pagecount+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "            <\/tr>";
				strVar += "            <tr style=\"vertical-align:top\">";
				strVar += "                <td>";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 10px; white-space: nowrap; vertical-align: top;\">";
				strVar += "                                &nbsp;&nbsp;&nbsp;BILL TO:";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 14px;\">";

				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+billaddressee+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+billaddr1+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";

				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+billstateandcountry+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "                <td style=\"font-size: 10px; vertical-align: top;\">";
				strVar += "                    CUST #:&nbsp;&nbsp;"+entity+"";
				strVar += "                <\/td>";
				strVar += "                <td>";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 9px; vertical-align: top;\">";
				strVar += "                                RETURNS TO:";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+returnadresse+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";	
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+addr1+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+stateandzip+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "            <\/tr>";
				strVar += "            <tr style=\"vertical-align:top\">";
				strVar += "                <td>";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 10px; white-space: nowrap; vertical-align: top;\">";
				strVar += "                                &nbsp;&nbsp;&nbsp;SHIP TO:";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+shipaddressee+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+shipaddr1+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+shipstateandcountry+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "                <td>";
				strVar += "                <\/td>";
				strVar += "                <td style=\"vertical-align: top;\">";
				strVar += "                    <table  style=\"border: 1px solid black;\" frame=\"box\" rules=\"all\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">";
				strVar += "                        <tr style=\"background-color: Gray;\">";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black;width:50%; border-bottom: 1px solid black;\"";
				strVar += "                                   colspan=\"2\">";
				strVar += "                                SHIP POINT";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; width:50% border-bottom: 1px solid black;\"";
				strVar += "                                colspan=\"2\">";
				strVar += "                                INSTRUCTIONS";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black;\" colspan=\"2\">";
				strVar += "                                Girl Scouts of the usa";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black;\" colspan=\"2\">";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr style=\"background-color: Gray;\">";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                                SHIP VIA";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                                REQUEST DATE";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                                PICKED";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-bottom: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                                TERMS";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                               "+shipmethod+" ";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                               "+expdate+"";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                               "+actualenddate+"";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-top: 1px solid black;\">";
				strVar += "                             "+terms+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "            <\/tr>";
				strVar += "<tr style=\"vertical-align:top\">";
				strVar += "<td colspan=\"3\">";
				strVar +="<table>";
				strVar +="<tr>";
				strVar +="<td  style=\"font-size: 12px;\" colspan=\"3\">";
				strVar +="LEAST EXPENSIVE SHIPPING METHOD/GSM TO DECIDE(RECOMENDED)EFFECTIVE 10/21/09";
				strVar +="<\/td>";
				strVar +="<\/tr>";
				strVar +="<tr>";
				strVar += "                <td colspan=\"3\">";
				strVar += "                    <table width=\"100%\" border=\"0\" frame=\"box\" rules=\"all\" cellpadding=\"2\" cellspacing=\"0\">";
				strVar += "                        <tr style=\"background-color: Gray;\">";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-left: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; border-right: 1px solid black; text-align: center;\">";
				strVar += "                                Line No";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                PRODUCT AND DESCRIPTION";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                QUANTITY ORDERED";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                QUANTITY SHIPPED";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                QUANTITY B.O.";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                SUGGESTED RETAIL";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                UNIT PRICE";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                DISCOUNT %";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                NET UNIT PRICE";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                AMOUNT (NET)";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";


				if(pagecount==1)
				{
					strVar += "                        <tr style=\"line-height:10px;\">";
					strVar += "                            <td style=\"border-left: 1px solid black; border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"font-size: 12px; white-space: nowrap; border-right: 1px solid black;\">";
					strVar += "                                This Order Is Contained in the Following Carton(s):";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                        <\/tr>";	
					strVar += "                        <tr style=\"background-color: Gray;line-height:10px;\">";
					strVar += "                            <td style=\"border-left: 1px solid black; border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"font-size: 12px;  white-space: nowrap; border-right: 1px solid black;\">";
					strVar += "                                "+appendcontlp+"";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                        <\/tr>";

				}



				for(var g=grouplength; g<groupopentasksearchresults.length;g++)
				{
					count++;
					grouplength++;
					var vContainerLpNo = groupopentasksearchresults [g].getValue('custrecord_container_lp_no', null, 'group');
					var itemText = groupopentasksearchresults [g].getText('custrecord_sku', null, 'group');
					var lineno = groupopentasksearchresults [g].getValue('custrecord_line_no', null, 'group');
					var totalqty = groupopentasksearchresults [g].getValue('custrecord_expe_qty', null, 'sum');
					totalshipqty =parseInt (totalshipqty)+parseInt (totalqty);
					nlapiLogExecution('DEBUG', 'totalqty ', totalqty);
					var unitvalue,backordervalue,decscription;
					nlapiLogExecution('DEBUG', 'lineNum ', totalLine);
					nlapiLogExecution('DEBUG', 'groupopentasklineno ', lineno);
					unitvalue=salesorderdetails.getLineItemValue('item','amount',lineno);
					backordervalue=salesorderdetails.getLineItemValue('item','backordered',lineno);
					decscription=salesorderdetails.getLineItemValue('item','description',lineno); 
					var totalamount=parseFloat(totalqty)*parseFloat(unitvalue);
					totalinvoice=parseFloat(totalinvoice)+parseFloat(totalamount);
					nlapiLogExecution('DEBUG', 'totalinvoice ', totalinvoice);
					var backgroundcolor="";
					if(pagecount==1)
					{
						backgroundcolor="Gray";

					}
					else
					{
						backgroundcolor="white";

					}

					strVar += "                            <tr style=\"background-color: ;line-height:10px;\">";
					strVar += "                                <td style=\"font-size: 12px;border-left: 1px solid black; border-right: 1px solid black; font-size: 10px;\">";
					strVar += "                                  "+linenumber+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px; white-space: nowrap; border-right: 1px solid black;\">";
					strVar += "                                   "+itemText+"  "+decscription+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                            <\/tr>";
					strVar += "                            <tr style=\"line-height:10px;\">";
					strVar += "                                <td style=\"border-left: 1px solid black; border-right: 1px solid black;\">&nbsp;";

					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px; white-space: nowrap; border-right: 1px solid black;\">";
					strVar += "                                  "+vContainerLpNo+" "+totalqty+"Each";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px; text-align:right ;border-right: 1px solid black;\">";
					strVar += "                                 "+totalqty+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+totalqty+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+backordervalue+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+unitvalue+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 0.00";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+totalamount+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+totalamount+"";
					strVar += "                                <\/td>";
					strVar += "                            <\/tr>";

					if(g==groupopentasksearchresults.length-1)
					{
						strVar += "<tr style=\"line-height:10px;\">";
						strVar += "                                <td style=\"border-left: 1px solid black; text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ; white-space: nowrap; border-right: 1px solid black;\">";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                  Total";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                 "+totalinvoice+"";
						strVar += "                                <\/td>";

						strVar += "<\/tr>";

						strVar += "<tr style=\"line-height:10px;\">";
						strVar += "                                <td style=\"border-left: 1px solid black;text-align:right ; border-right: 1px solid black;\">&nbsp;";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px; white-space: nowrap;text-align:right ; border-right: 1px solid black;\">";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                  Invoice";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                  Total";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                 $"+totalinvoice+"";
						strVar += "                                <\/td>";
						strVar += "<\/tr>";
					}
					linenumber++;


					if(pagecount==1)
					{
						if(count==6)
						{
							break;
						}
					}
					else
					{
						if(count==7)
						{
							break;
						}

					}

				}
				var totalshipped='';
				var totalshipcube='';
				var totalshipweight='';
				var totallines='';
				if(grouplength==groupopentasksearchresults.length)
				{
					totallines=linenumber-1;
					totalshipped=totalshipqty;
					totalshipcube=totalcube;
					totalshipweight=totalweight;
				}
				else
				{ 
					totallines='';
					totalshipcube='';
					totalshipweight='';
					totalshipped='';
				}


				strVar += "                        <tfoot style=\"border-collapse:collapse\">";
				strVar += "                            <tr>";
				strVar += "                                <td colspan=\"10\" style=\"border-top: none;border-top: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;";
				strVar += "                                    border-bottom: 1px solid black; font-size: 10px\">";
				strVar += "                                    Orders received should be checked against packing list<br \/>";
				strVar += "                                   All claims against this order must be filed within 30days.Inquiries should be submitted ";
				strVar += "                                   either by phone or fax using the telephone numbers shown above";
				strVar += "                                    <br \/>";
				strVar += "                                    GSUSA Open Account customers will receive their invoice separately .The Merchandise";
				strVar += "                                    Total above does not include Freight,Handling or Tax<br \/>";
				strVar += "                                   GSUSA Cash and Credit Card customers are on automatic back order;merchandise will";
				strVar += "                                      be shipped when available.All applicable refunds will be mailed under separate";
				strVar += "                                    cover.Please allow 4-6 weeks from ship date.When returning <br \/>";
				strVar += "                                   merchandise ,a copy of original packing slip should be enclosed and should be sent";
				strVar += "                                  to the address shown above,Randolph,NJ ";
				strVar += "                                <\/td>";
				strVar += "                            <\/tr>";
				strVar += "                            <tr>";
				strVar += "                                <td colspan=\"10\">";
				strVar += "                                    <table width=\"100%\" cellpadding=\"2\" frame=\"box\" rules=\"all\" cellspacing=\"0\">";
				strVar += "                                        <tr>";
				strVar += "                                            <td style=\" border-right: 1px solid black; border-left: 1px solid black; border-bottom: 1px solid black;font-size: 12px;\">";
				strVar += "                                               "+totallines+"&nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                LINES TOTAL";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                # OF LINES NOT PRINTED";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                0";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                QTY SHIPPED TOTAL";
				strVar += "                                                <img src=\"\" height=\"2px\" \/>";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                               "+totalshipped+"&nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\" border-bottom: 1px solid black;border-right: 1px solid black; font-size: 12px;\"";
				strVar += "                                                colspan=\"4\">";
				strVar += "                                                No.OF CARTONS";
				strVar += "                                            <\/td>";
				strVar += "                                        <\/tr>";
				strVar += "                                        <tr style=\"background-color: Gray; font-size: 11px;\">";
				strVar += "                                            <td style=\" border-right: 1px solid black; border-bottom: 1px solid black;font-size: 12px;\">";

				strVar += "                                                PICKED BY";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;border-left: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                PACKED BY";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                CHECKED BY";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                CUBE";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                WEIGHT";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                FREIGHT CHARGE";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                RECEIVED BY";
				strVar += "                                            <\/td>";
				strVar += "                                            <td colspan=\"2\" style=\" border-right: 1px solid black;border-bottom: 1px solid black;";
				strVar += "                                                font-size: 12px;\">";
				strVar += "                                                DATE RECEIVED";
				strVar += "                                            <\/td>";
				strVar += "                                        <\/tr>";
				strVar += "                                        <tr>";
				strVar += "<td style=\"border-right: 1px solid black; border-bottom: 1px solid black;  border-left: 1px solid black;font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                               "+totalshipcube+" &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                               "+totalshipweight+"&nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td colspan=\"2\" style=\" border-bottom: 1px solid black;border-right: 1px solid black;font-size: 12px;\"> &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                        <\/tr>";
				strVar += "                                    <\/table>";
				strVar += "                                <\/td>";
				strVar += "                            <\/tr>";
				strVar += "                        <\/tfoot>";
				strVar += "                    <\/table>";
				strVar +="<\/td>";
				strVar +="<\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "            <\/tr>";
				strVar += "        <\/table>";
				strVar += " </body>";
				strVar += " </html>";
				if(grouplength==groupopentasksearchresults.length)
				{
					strVar +="<p style=\" page-break-after:avoid\"></p>";
				}
				else
				{
					strVar +="<p style=\" page-break-after:always\"></p>";
				}

				pagecount++;	 
				nlapiLogExecution('DEBUG', 'totalcount', totalcount);
				totalcount=parseFloat(totalcount)-parseFloat(count);
				nlapiLogExecution('DEBUG', 'totalcountafter', totalcount);
			}
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Recordsnotfound', 'Recordsnotfound');
		}
		var tasktype='14';
		var labeltype='PackList';
		var print='F';
		var reprint='F';
		var company='';
		var location='';
		var formattype='html';
		var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
		labelrecord.setFieldValue('name', fulfillmentnumber); 
		labelrecord.setFieldValue('custrecord_labeldata',strVar);  
		labelrecord.setFieldValue('custrecord_label_refno',fulfillmentnumber);     
		labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);//chkn task   
		labelrecord.setFieldValue('custrecord_labeltype',labeltype);    
		labelrecord.setFieldValue('custrecord_label_lp',fulfillmentnumber);

		labelrecord.setFieldValue('custrecord_label_print', print);
		labelrecord.setFieldValue('custrecord_label_reprint', reprint);
		labelrecord.setFieldValue('custrecord_label_company', company);
		labelrecord.setFieldValue('custrecord_label_location', location);
		labelrecord.setFieldValue('custrecord_label_printername',printername);

		//nlapiLogExecution('DEBUG', 'htmlstring ', strVar);

		var tranid = nlapiSubmitRecord(labelrecord);

		if(tranid!=null && tranid!='')
		{
			var getCartonLPNo = request.getParameter('custparam_cartonno');
			nlapiLogExecution('DEBUG', 'getCartLPNo', getCartonLPNo);
			var getFONO= request.getParameter('custparam_fono');
			nlapiLogExecution('DEBUG', 'getFONO', getFONO);


			var POarray=new Array();
			POarray["custparam_orderno"] = orderno;			
			//case 20125944 start : added language
			var getLanguage =  request.getParameter('hdngetLanguage');
			POarray["custparam_language"]=getLanguage;	
			//case 20125944 end
			response.sendRedirect('SUITELET', 'customscript_rf_packing_complete', 'customdeploy_rf_packing_complete_di', false, POarray);

		}

	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'exceptionhtmlpackcode ', exp);

	}
}
function AutoPacking(vebizOrdNo,whlocation){	
	nlapiLogExecution('DEBUG', 'into AutoPacking (SO Internal Id)', vebizOrdNo);
	nlapiLogExecution('DEBUG', 'into AutoPacking (WH Location)', whlocation);
	if(vebizOrdNo!=null && vebizOrdNo!='')
	{
		nlapiLogExecution('DEBUG', 'into AutoPacking vebizOrdNo is not null', vebizOrdNo);
		var vOrdAutoPackFlag="";
		var vStgAutoPackFlag="";
		var vStgCarrierType="";
		var vCarrierType="";
		var vStgCarrierName="";
		var vorderType='';
		var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vebizOrdNo);
		if(vOrderAutoPackFlag!=null&& vOrderAutoPackFlag.length>0){
			vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
			vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
			vorderType = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
			nlapiLogExecution('DEBUG', 'Order Type',vOrdAutoPackFlag);
			nlapiLogExecution('DEBUG', 'AutoPackFlag @ Order Type',vOrdAutoPackFlag);
			nlapiLogExecution('DEBUG', 'Carrier @ Order Type',vCarrierType);
		}

		if(vOrdAutoPackFlag!='T' || (vCarrierType=="" || vCarrierType==null)){
			var vStageAutoPackFlag = getAutoPackFlagforStage(whlocation,vorderType);
			if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
				vStgCarrierType = vStageAutoPackFlag[0][0];
				vStgAutoPackFlag = vStageAutoPackFlag[0][1];
				vStgCarrierName = vStageAutoPackFlag[0][2];
				nlapiLogExecution('DEBUG', 'AutoPackFlag(Stage)',vStgAutoPackFlag);
				nlapiLogExecution('DEBUG', 'Carrier(Stage)',vStgCarrierType);
			}
		}

		if(vCarrierType=="" && vStgCarrierType!="")
			vCarrierType=vStgCarrierType;

		nlapiLogExecution('DEBUG', 'Carrier', vCarrierType);

		if(vOrdAutoPackFlag=='T' || vStgAutoPackFlag=='T'){
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage at the start of UpdatesInOpenTask ',context.getRemainingUsage());
			UpdatesInOpenTask(vebizOrdNo,vCarrierType,vStgCarrierName);
		}
	}
	nlapiLogExecution('DEBUG', 'out of AutoPacking');
}
function getAutoPackFlagforOrdType(vebizOrdNo){
	nlapiLogExecution('DEBUG', 'into getAutoPackFlagforOrdType eBizOrdNo',vebizOrdNo);
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
	columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	columns[2] = new nlobjSearchColumn('custbody_nswmssoordertype');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
//	var	searchresults = nlapiLoadRecord('salesorder', vebizOrdNo);
	nlapiLogExecution('DEBUG', 'out of getAutoPackFlagforOrdType', searchresults);
	return searchresults;

}
function getAutoPackFlagforStage(whlocation,vorderType){
	nlapiLogExecution('DEBUG', 'into getAutoPackFlagforStage');
	nlapiLogExecution('DEBUG', 'whlocation',whlocation);
	nlapiLogExecution('DEBUG', 'vorderType',vorderType);
	var vStgRule = new Array();
	//getStageRule(Item, vCarrier, vSite, vCompany, stgDirection,ordertype)
	vStgRule = getStageRule('', '', whlocation, '', 'OUB',vorderType);
	nlapiLogExecution('DEBUG', 'Stage Rule', vStgRule);

	nlapiLogExecution('DEBUG', 'out of getAutoPackFlagforStage');
	return vStgRule;
}
function UpdatesInOpenTask(vebizOrdNo,vCarrierType,CarrieerName){
	nlapiLogExecution('DEBUG', 'into UpdatesInOpenTask function', vebizOrdNo);
	nlapiLogExecution('DEBUG', 'CarrierType', vCarrierType);
	var packstation = getPackStation();
	var DataRecArr=new Array();
	//var containerlpArray=new Array();
	var ebizorderno;
	var filters = new Array();
	nlapiLogExecution('DEBUG','orderno in getSalesOrderlist',vebizOrdNo);

	if(vebizOrdNo!=""){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vebizOrdNo));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	//filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLp));//Task Type - PICK

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_comp_id');
	columns[5] = new nlobjSearchColumn('custrecord_site_id');
	columns[6] = new nlobjSearchColumn('custrecord_container');
	columns[7] = new nlobjSearchColumn('custrecord_total_weight');
	columns[8] = new nlobjSearchColumn('custrecord_sku');

	//columns[0].setSort();
	columns[1].setSort();
	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	if(salesOrderList != null && salesOrderList.length > 0){
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item;	
		for (var i = 0; i < salesOrderList.length; i++){
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage in the for loop starting ',context.getRemainingUsage());
			orderno = salesOrderList[i].getValue('name');
			containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
			ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
			ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
			compid = salesOrderList[i].getValue('custrecord_comp_id');
			siteid = salesOrderList[i].getValue('custrecord_site_id');
			containersize = salesOrderList[i].getValue('custrecord_container');
			totweight = salesOrderList[i].getValue('custrecord_total_weight');
			item = salesOrderList[i].getValue('custrecord_sku');//modified on dec 12

			internalid = salesOrderList[i].getId();
			//containerlpArray[i]=containerno;
			DataRecArr.push(new DataRec(compid,siteid,ebizorderno,orderno,containerno,packstation,ebizcntrlno,item));
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage Before updating status to pack complete to open task ',context.getRemainingUsage());
			updateStatusInOpenTask(internalid,28);//28 - Status Flag - Outbound Pack Complete
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage After update status to opentask ',context.getRemainingUsage());
			//updateStatusInLPMaster(containerno,28);//28 - Status Flag - Outbound Pack Complete Moved to below function
			//CreatePACKTask(compid,siteid,ebizorderno,orderno,containerno,14,packstation,packstation,28,ebizcntrlno,item);//14 - Task Type - PACK

//			if(vCarrierType=="PC")
//			CreateShippingManifestRecord(ebizorderno,containerno,CarrieerName);
		} 


		if(DataRecArr.length>0)
		{
			nlapiLogExecution('DEBUG', 'DataRecArr.length', DataRecArr.length);
			var oldcontainer="";
			for (var k = 0; k < DataRecArr.length; k++)
			{	
				var DataRecord=DataRecArr[k];
				var containerlpno =  DataRecord.DataRecContainer;
				nlapiLogExecution('DEBUG', 'Old Container', oldcontainer);
				nlapiLogExecution('DEBUG', 'New Container', containerlpno);
				if(oldcontainer!=containerlpno)
				{
					updateStatusInLPMaster(containerno,28);//28 - Status Flag - Outbound Pack Complete
					var context = nlapiGetContext();
					nlapiLogExecution('DEBUG','Remaining usage after update status in lp master ',context.getRemainingUsage());
					CreatePACKTask(DataRecord.compid,DataRecord.DataRecSiteId,DataRecord.DataRecEbizOrdNo,DataRecord.DataRecOrdNo,DataRecord.DataRecContainer,14,DataRecord.DataRecPackStation,DataRecord.DataRecPackStation,28,DataRecord.DataRecEbizControlNo,DataRecord.DataRecItem);//14 - Task Type - PACK
					var context = nlapiGetContext();
					nlapiLogExecution('DEBUG','Remaining usage after pack task creation ',context.getRemainingUsage()); 
					if(vCarrierType=="PC")
					{	
						CreateShippingManifestRecord(ebizorderno,containerlpno,CarrieerName);
					}
					var context = nlapiGetContext();
					nlapiLogExecution('DEBUG','Remaining usage after Ship manifest ',context.getRemainingUsage());
					oldcontainer = containerlpno;

				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'out of UpdatesInOpenTask function', vebizOrdNo);
}
function GetSOInternalId(SOText,ordertype)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',SOText);
	nlapiLogExecution('ERROR','ordertype',ordertype);

	var ActualSoID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));

	var columns=new Array();
	columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord(ordertype,null,filter,columns);
	/*if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}*/
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualSoID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualSoID);

	return ActualSoID;
}