/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_RF_sealnumber.js,v $
 *     	   $Revision: 1.2.4.3.4.2.4.5 $
 *     	   $Date: 2014/06/13 12:07:35 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_sealnumber.js,v $
 * Revision 1.2.4.3.4.2.4.5  2014/06/13 12:07:35  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.4.3.4.2.4.4  2014/05/30 00:41:06  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.3.4.2.4.3  2013/10/23 16:14:10  nneelam
 * Case# 20125225
 * DEPT Name on button changed to ENT..
 *
 * Revision 1.2.4.3.4.2.4.2  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.4.3.4.2.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.3.4.2  2012/12/24 13:18:44  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for nulls
 *
 * Revision 1.2.4.3.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.2.4.3  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.2  2012/02/22 13:00:10  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2.4.1  2012/02/10 10:49:10  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing  related depart
 *
 * Revision 1.2  2011/09/28 13:23:13  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 *
 *****************************************************************************/

function SealNumberMenu(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		var trailerno = request.getParameter('custparam_trlrno');
		var getshiplp = request.getParameter('custparam_shilp');
		var getLanguage = request.getParameter('custparam_language');	
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			st0 = "TRAILER DE CARGA";
			st1 = "UNIDAD DE BARCO CARGADO CON &#201;XITO";
			st2 = "ENTER SEAL #";
			st3 = "SALIDA";
					
    	}
		else
		{
			st0 = "TRAILER LOADING";
			st1 = "SHIP UNIT LOADED SUCCESSFULLY";
			st2 = " ENTER SEAL #";
			st3 = "DEPART ";
			
			
			
		}    	
		var functionkeyHtml=getFunctionkeyScript('_rfload'); 
		var html = "<html><head><title>" + st0 + " </title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('selectSealno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfload' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> ENTER SEAL #";
		html = html + "				<input type='hidden' name='hdntrlrNo' value='" + trailerno + "'>";
		html = html + "				<input type='hidden' name='hdnshiplp' value=" + getshiplp + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectSealno' id='selectSealno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdDept' type='submit' value='ENT'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectSealno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var sealno = request.getParameter('selectSealno');
		var optedEvent = request.getParameter('cmdDept');
		var trailerName = request.getParameter('hdntrlrNo');
		var shiplp = request.getParameter('hdnshiplp');
		var sealnoarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		sealnoarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', sealnoarray["custparam_language"]);   
		sealnoarray["custparam_sealno"]=sealno;
		sealnoarray["custparam_trlrno"]=trailerName;
		sealnoarray["custparam_shilp"]=shiplp;
//case # 20125225,Button name changed from DEPT to ENT.
		if (request.getParameter('cmdDept') == 'ENT') {
			//End
			response.sendRedirect('SUITELET', 'customscript_rf_departtrlrconfirmation', 'customdeploy_rf_deprttrlrconfirmation_di', false, sealnoarray);
		}

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}