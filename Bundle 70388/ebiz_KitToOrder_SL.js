/***************************************************************************
	  		  eBizNET Solutions Inc .
****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
****************************************************************************
*
*     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_KitToOrder_SL.js,v $
*     	   $Revision: 1.6.2.2.4.1.4.1 $
*     	   $Date: 2013/09/11 12:16:50 $
*     	   $Author: rmukkera $
*
*   eBizNET version and checksum stamp.  Do not remove.
*   $eBiznet_VER: .............. $eBizNET_SUM: .....
* PRAMETERS
*
*
* DESCRIPTION
*
*  	Default Data for Interfaces
*
* NOTES AND WARNINGS
*
* INITATED FROM
*
* REVISION HISTORY
*
*****************************************************************************/
/*
 * Processing HTTP GET Request
 */
function addAllWorkOrdersToField(workorder, workorderList,form){
	if(workorderList != null && workorderList.length > 0){
		for (var i = 0; i < workorderList.length ; i++) {	
		var res=  form.getField('custpage_workorder').getSelectOptions(workorderList[i].getValue('tranid'), 'is');
		if (res != null) {
			if (res.length > 0) {
				continue;
			}
		}
		workorder.addSelectOption(workorderList[i].getValue('internalid'), workorderList[i].getValue('tranid'));
//		for(var i = 0; i < workorderList.length; i++){
//			workorder.addSelectOption(workorderList[i].getValue('internalid'), workorderList[i].getValue('tranid'));
//		}
		}
	}
	
}

function getAllWorkOrders(){
	
		
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[0].setSort(true);	
	var searchResults = nlapiSearchRecord('workorder', null, null, columns);
	return searchResults;
}
function kitToOrderGETRequest(request, response){
	nlapiLogExecution('ERROR', 'kitToOrderGETRequest Start', 'kitToOrderGETRequest Start');
	//create the Suitelet form
	var form = nlapiCreateForm('Kit to Order');
	var vSOId = "";
	var vSOName = "";
	var vSOLocation = "";
	var vSOLineItem = "";
	var vSOLineItemName = "";
	var vSOLineNum = "";
	var vSOLineQty = "";
	var vSOCommitedLineQty = "";
	var vSOFulfilledLineQty = "";
	
	vSOId = request.getParameter('custparam_soid');
	vSOName = request.getParameter('custparam_soname');
	vSOLocation = request.getParameter('custparam_soloc');
	vSOLineItem = request.getParameter('custparam_lneitem');
	vSOLineItemName = request.getParameter('custparam_lneitxt');
	vSOLineNum = request.getParameter('custparam_lneno');
	vSOLineQty = request.getParameter('custparam_lneqty');
	
	
	var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
	WorkOrder.addSelectOption("","");
	
	// Retrieve all sales orders
	var WorkOrderList = getAllWorkOrders();
	
	// Add all sales orders to SO Field
	addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);
	
	var soDate = form.addField('custpage_sodate', 'date', 'Date');
	soDate.setLayoutType('normal', 'startrow');
	soDate.setDefaultValue(nlapiDateToString(new Date()));
	
	//var item = form.addField('cusppage_item', 'select', 'Kit Item', 'custom');
	var itemtext = form.addField('cusppage_itemtext', 'text', 'Kit Item', 'custom');
	var item = form.addField('cusppage_item', 'text', 'Kit Item', 'custom').setDisplayType('hidden');
	/*var itemData = nlapiSearchRecord('item', null, new nlobjSearchFilter('type', null, 'anyof', ['@NONE@','Assembly']),
			[new nlobjSearchColumn('internalid'), new nlobjSearchColumn('itemid')]);*/
	/*var itemData = nlapiSearchRecord('item', null, null,
			[new nlobjSearchColumn('internalid'), new nlobjSearchColumn('itemid')]);
	item.addSelectOption('select','Select');
	for(i=0; i<itemData.length; i++){
		item.addSelectOption(itemData[i].getValue("internalid"), itemData[i].getValue("itemid"));
	}*/ 
	
	nlapiLogExecution('ERROR','vSolineitem '+ vSOLineItem);
	if(vSOLineItem!= null && vSOLineItem != "")
	{
		nlapiLogExecution('ERROR','vSolineitem '+ vSOLineItem);
		/*var itemsearch = nlapiSearchRecord('item', null, new nlobjSearchFilter('internalid', null, 'is', vSOLineItem),
				[new nlobjSearchColumn('internalid'), new nlobjSearchColumn('name')]);*/
		
		// Changed On 30/4/12 by Suman

		var filteritem=new Array();
		filteritem.push(new nlobjSearchFilter('internalid', null, 'is', vSOLineItem));
		filteritem.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		var columnsitem = new Array();
		columnsitem[0] = new nlobjSearchColumn('internalid');
		columnsitem[1] =new nlobjSearchColumn('name');
		var itemsearch = nlapiSearchRecord('item', null,filteritem,columnsitem);

		// End of Changes as On 30/4/12
		nlapiLogExecution('ERROR','itemsearch ', itemsearch);
		nlapiLogExecution('ERROR','itemsearch ', itemsearch.length);
		if(itemsearch!= null && itemsearch != "" && itemsearch.length>0)
		{		
			itemtext.setDefaultValue(itemsearch[0].getValue('name'));
		}
	}
	item.setDefaultValue(vSOLineItem);
	item.setLayoutType('normal', 'startrow');

	var kitQty = form.addField('custpage_kitqty', 'text', 'Qty');
	if(vSOLineQty != null && vSOLineQty != "")
	kitQty.setDefaultValue(vSOLineQty);
	nlapiLogExecution('ERROR','kitQty ', kitQty);
	 
		
	kitQty.setLayoutType('normal', 'startrow');
	
	
	   
	var doField = form.addField('custpage_refno', 'text', 'Ref#');        
	doField.setLayoutType('normal', 'startrow');
	doField.setDefaultValue(vSOName);
                      
                          
    var site = form.addField('cusppage_site', 'select', 'Location', 'location');
	site.setLayoutType('normal', 'startrow');
	site.setDefaultValue(vSOLocation);
	
	var soInternalNum = form.addField('custpage_sointernno', 'text', 'sono');
	soInternalNum.setDisplayType('hidden');
	soInternalNum.setDefaultValue(vSOId); 
	
	var chknCompany = form.addField('custpage_company', 'select', 'Company', 'customrecord_ebiznet_company');
	form.addSubmitButton('Get Kit Details');  
	response.writePage(form);
}

/*
 * Processing HTTP POST Request
 */
function kitToOrderPOSTRequest(request, response){
	nlapiLogExecution('DEBUG', 'kitToOrderPostRequest Start', 'kitToOrderPostRequest Start');
	var item = request.getParameter('cusppage_item');		
	var location =request.getParameter('cusppage_site');
	var qty = request.getParameter('custpage_kitqty');
	var vDate = request.getParameter('custpage_sodate');
	var vRefNo = request.getParameter('custpage_refno');
	var vSOInternalNo = request.getParameter('custpage_sointernno');
	var vRefCompany = request.getParameter('custpage_company');	
	var vWorkOrder = request.getParameter('custpage_workorder');
	var kitArray = new Array();
	kitArray["custparam_item"] = item;
	kitArray["custparam_locaiton"] = location;
	if(qty!= null && qty !="")
	kitArray["custpage_kitqty"] = parseFloat(qty);
	kitArray["custpage_sodate"] = vDate;
	kitArray["custpage_company"] = vRefCompany;		
	kitArray["custparam_soid"] = vSOInternalNo;	
	kitArray["custpage_workorder"] = vWorkOrder;	
	response.sendRedirect('SUITELET', 'customscript_kittoorderdets', 'customdeploy_kittoorderdets_di',
			false, kitArray);
}

function kitToOrderSuitelet(request, response){
    if (request.getMethod() == 'GET') {
    	kitToOrderGETRequest(request, response);
    } else {
    	kitToOrderPOSTRequest(request, response);
    }    
}





