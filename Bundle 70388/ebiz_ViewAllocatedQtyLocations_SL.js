/***************************************************************************
 							eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_ViewAllocatedQtyLocations_SL.js,v $
 *     	   $Revision: 1.1.2.2.4.4.2.5.2.1 $
 *     	   $Date: 2015/12/04 15:17:39 $
 *     	   $Author: skreddy $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_222 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  
 *
 *****************************************************************************/

function ViewAllocatedQtyBinLocation(request,response)
{
	if(request.getMethod()=='GET')
	{
		var ItemId,Lp;
		var totalQty = 0;
		var form = nlapiCreateForm('View Locations');
		try{
			if(request.getParameter('custparam_Item')!=null){		 

				ItemId=request.getParameter('custparam_Item');
				Lp=request.getParameter('custparam_lp');
				Itemstatus=request.getParameter('custparam_itemstatus');
				Lotno=request.getParameter('custparam_lotno');
				Packcode=request.getParameter('custparam_packcode');
				InvRef=request.getParameter('custparam_InvRef');
				nlapiLogExecution('ERROR', 'Item',ItemId );
				nlapiLogExecution('ERROR', 'LP',Lp );
				nlapiLogExecution('ERROR', 'Itemstatus',Itemstatus );
				nlapiLogExecution('ERROR', 'Lotno',Lotno );
				nlapiLogExecution('ERROR', 'Packcode',Packcode );
				nlapiLogExecution('ERROR', 'InvRef',InvRef );
				var j=0;

				var sublist = form.addSubList("custpage_items", "list", "View Locations");
				sublist.addField("custpage_item", "text", "Item#");
				sublist.addField("custpage_wave", "text", "Wave#");	
				sublist.addField("custpage_fono", "text", "FO#/REPORT#");	
				sublist.addField("custpage_lineno", "text", "Line#");					
				sublist.addField("custpage_lp", "text", "LP#");				
				sublist.addField("custpage_itemstatus", "text", "Item Status");
				sublist.addField("custpage_tasktype", "text", "Task Type");
				sublist.addField("custpage_lotno", "text", "LOT#");
				sublist.addField("custpage_openallqty", "text", "Allocated Quantity in OpenTask");
				sublist.addField("custpage_beginlocation", "text", "Actual Begin Location");
				sublist.addField("custpage_endlocation", "text", "Actual End Location");

				//var btnsunmit = form.addSubmitButton('Submit');
				var openfilter= new Array();
				openfilter.push(new nlobjSearchFilter('custrecord_sku',null, 'anyof', ItemId));
				//case # 20147953
				openfilter.push(new nlobjSearchFilter('custrecord_lpno',null, 'is', Lp));
				if(InvRef!=null && InvRef!='')
					openfilter.push(new nlobjSearchFilter('custrecord_invref_no',null, 'equalto', InvRef));
				openfilter.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', ['3','8']));
				openfilter.push(new nlobjSearchFilter('custrecord_act_end_date',null, 'isempty'));
				openfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof',['9','20']));
				//	openfilter.push(new nlobjSearchFilter('custrecord_lpno', null, 'isnotempty'));
				if(Itemstatus!=null && Itemstatus!="")
				{
					openfilter.push(new nlobjSearchFilter('custrecord_sku_status',null, 'anyof', Itemstatus));
				}
				if(Lotno!=null && Lotno!="")
				{
					openfilter.push(new nlobjSearchFilter('custrecord_batch_no',null, 'is', Lotno));
				}
//				if(Packcode!=null && Packcode!="")
//				{
//				openfilter.push(new nlobjSearchFilter('custrecord_packcode',null, 'is', Packcode));
//				}	


				var columns = new Array();	
				columns[0] = new nlobjSearchColumn('custrecord_lpno');
				columns[1] = new nlobjSearchColumn('custrecord_sku');			 
				columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
				columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
				columns[4] = new nlobjSearchColumn('custrecord_actendloc');
				columns[5] = new nlobjSearchColumn('custrecord_sku_status');
				columns[6] = new nlobjSearchColumn('custrecord_batch_no');
				columns[7] = new nlobjSearchColumn('custrecord_tasktype');
				columns[8] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
				columns[9] = new nlobjSearchColumn('custrecord_line_no');
				columns[10]= new nlobjSearchColumn('name');


				var opentaskResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openfilter, columns);
				if(opentaskResults!=null)
				{	

					for(i=0;i<opentaskResults.length; i++)
					{
						j=i+1;
						//nlapiLogExecution('ERROR','opentaskResults',opentaskResults.length);
						var LP=opentaskResults[i].getValue('custrecord_lpno');
						var Item=opentaskResults[i].getText('custrecord_sku');
						var Qty=opentaskResults[i].getValue('custrecord_expe_qty');
						var BeginLoc=opentaskResults[i].getText('custrecord_actbeginloc');
						var EndLoc=opentaskResults[i].getText('custrecord_actendloc');
						var ItemStatus=opentaskResults[i].getText('custrecord_sku_status');
						var Lotno=opentaskResults[i].getText('custrecord_batch_no');
						var Tasktype=opentaskResults[i].getText('custrecord_tasktype');
						var Waveno=opentaskResults[i].getValue('custrecord_ebiz_wave_no');
						var FOno =opentaskResults[i].getValue('name');
						var Lineno=opentaskResults[i].getValue('custrecord_line_no');					
						totalQty=totalQty+parseFloat(Qty);



						form.getSubList('custpage_items').setLineItemValue('custpage_lp', j, LP);
						form.getSubList('custpage_items').setLineItemValue('custpage_item', j, Item);
						form.getSubList('custpage_items').setLineItemValue('custpage_openallqty', j, Qty);
						form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', j, ItemStatus);
						form.getSubList('custpage_items').setLineItemValue('custpage_lotno', j, Lotno);
						form.getSubList('custpage_items').setLineItemValue('custpage_beginlocation', j, BeginLoc);
						form.getSubList('custpage_items').setLineItemValue('custpage_endlocation', j, EndLoc);
						form.getSubList('custpage_items').setLineItemValue('custpage_tasktype', j, Tasktype);
						form.getSubList('custpage_items').setLineItemValue('custpage_wave', j, Waveno);
						form.getSubList('custpage_items').setLineItemValue('custpage_fono', j, FOno);
						form.getSubList('custpage_items').setLineItemValue('custpage_lineno', j, Lineno);
					}
					form.getSubList('custpage_items').setLineItemValue('custpage_lp', j+1, 'Total');// case# 201412783
					form.getSubList('custpage_items').setLineItemValue('custpage_openallqty', j+1, totalQty.toString());


				}
				else//this block is in else because we have both LP and From LP for a record then rows are displaying double time
				{
					var openfilter2= new Array();
					openfilter2.push(new nlobjSearchFilter('custrecord_sku',null, 'anyof', ItemId));
					//case # 20147953
					openfilter2.push(new nlobjSearchFilter('custrecord_from_lp_no',null, 'is', Lp));
					if(InvRef!=null && InvRef!='')
						openfilter2.push(new nlobjSearchFilter('custrecord_invref_no',null, 'equalto', InvRef));
					openfilter2.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', ['3','8']));
					openfilter2.push(new nlobjSearchFilter('custrecord_act_end_date',null, 'isempty'));
					openfilter2.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof',['9','20']));
					if(Itemstatus!=null && Itemstatus!="")
					{
						openfilter2.push(new nlobjSearchFilter('custrecord_sku_status',null, 'anyof', Itemstatus));
					}
					if(Lotno!=null && Lotno!="")
					{
						openfilter2.push(new nlobjSearchFilter('custrecord_batch_no',null, 'is', Lotno));
					}
//					if(Packcode!=null && Packcode!="")
//					{
//					openfilter2.push(new nlobjSearchFilter('custrecord_packcode',null, 'is', Packcode));
//					}	

					var columns2 = new Array();	
					columns2[0] = new nlobjSearchColumn('custrecord_from_lp_no');
					columns2[1] = new nlobjSearchColumn('custrecord_sku');			 
					columns2[2] = new nlobjSearchColumn('custrecord_expe_qty');
					columns2[3] = new nlobjSearchColumn('custrecord_actbeginloc');
					columns2[4] = new nlobjSearchColumn('custrecord_actendloc');
					columns2[5] = new nlobjSearchColumn('custrecord_sku_status');
					columns2[6] = new nlobjSearchColumn('custrecord_batch_no');
					columns2[7] = new nlobjSearchColumn('custrecord_tasktype');
					columns2[8] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
					columns2[9] = new nlobjSearchColumn('custrecord_line_no');
					columns2[10]= new nlobjSearchColumn('name');


					var opentaskResults2 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openfilter2, columns2);
					nlapiLogExecution('ERROR','opentaskResults2 ',opentaskResults2);
					if(opentaskResults2!=null)
					{	

						nlapiLogExecution('ERROR','opentaskResults2 length ',opentaskResults2.length);
						for(k=0;k<opentaskResults2.length; k++)
						{
							j=j+1;
							//nlapiLogExecution('ERROR','opentaskResults',opentaskResults.length);
							var LP=opentaskResults2[k].getValue('custrecord_from_lp_no');
							var Item=opentaskResults2[k].getText('custrecord_sku');
							var Qty=opentaskResults2[k].getValue('custrecord_expe_qty');
							var BeginLoc=opentaskResults2[k].getText('custrecord_actbeginloc');
							var EndLoc=opentaskResults2[k].getText('custrecord_actendloc');
							var ItemStatus=opentaskResults2[k].getText('custrecord_sku_status');
							var Lotno=opentaskResults2[k].getText('custrecord_batch_no');
							var Tasktype=opentaskResults2[k].getText('custrecord_tasktype');
							var Waveno=opentaskResults2[k].getValue('custrecord_ebiz_wave_no');
							var FOno =opentaskResults2[k].getValue('name');
							var Lineno=opentaskResults2[k].getValue('custrecord_line_no');					
							totalQty=totalQty+parseFloat(Qty);

							form.getSubList('custpage_items').setLineItemValue('custpage_lp', j, LP);
							form.getSubList('custpage_items').setLineItemValue('custpage_item', j, Item);
							form.getSubList('custpage_items').setLineItemValue('custpage_openallqty', j, Qty);
							form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', j, ItemStatus);
							form.getSubList('custpage_items').setLineItemValue('custpage_lotno', j, Lotno);
							form.getSubList('custpage_items').setLineItemValue('custpage_beginlocation', j, BeginLoc);
							form.getSubList('custpage_items').setLineItemValue('custpage_endlocation', j, EndLoc);
							form.getSubList('custpage_items').setLineItemValue('custpage_tasktype', j, Tasktype);
							form.getSubList('custpage_items').setLineItemValue('custpage_wave', j, Waveno);
							form.getSubList('custpage_items').setLineItemValue('custpage_fono', j, FOno);
							form.getSubList('custpage_items').setLineItemValue('custpage_lineno', j, Lineno);

						}
						form.getSubList('custpage_items').setLineItemValue('custpage_lp', j+1, 'Total');
						form.getSubList('custpage_items').setLineItemValue('custpage_openallqty', j+1, totalQty.toString());


					}
				}
			}

		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception ',exp);
		}

		response.writePage(form);

	}
	else
	{



	}

}


