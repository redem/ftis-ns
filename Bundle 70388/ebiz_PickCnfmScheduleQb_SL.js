/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_PickCnfmScheduleQb_SL.js,v $
 *     	   $Revision: 1.1.2.1.4.1 $
 *     	   $Date: 2012/11/01 14:55:02 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PickCnfmScheduleQb_SL.js,v $
 * Revision 1.1.2.1.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.1  2012/07/04 18:52:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Schedule script for confirming picks
 *
 * Revision 1.4.4.2  2012/05/15 23:00:27  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * pick confirm wave# drop down dispaly more than 1000 records
 *
 * Revision 1.4.4.1  2012/04/05 13:13:25  schepuri
 * CASE201112/CR201113/LOG201121
 * paging functionality is added
 *
 * Revision 1.4  2011/10/31 13:53:42  gkalla
 * CASE201112/CR201113/LOG201121
 * Added CVS header
 *
 * Revision 1.18  2011/10/31 11:39:48  gkalla
 * CASE201112/CR201113/LOG201121
 * To get the Wave#, fulfillment Order# and cluster#s in Sorting order
 *
 * Revision 1.17  2011/10/13 17:06:09  spendyala
 * CASE201112/CR201113/LOG201121
 * added printreport2 function for wavepickreport generation
 *
 * Revision 1.16  2011/10/12 20:00:15  spendyala
 * CASE201112/CR201113/LOG201121
 * uncommitted the display button
 *
 * Revision 1.15  2011/10/12 19:34:02  spendyala
 * CASE201112/CR201113/LOG201121
 * T&T issue is fixed
 *
 *
 *
 **********************************************************************************************************************/
function PickcnfmScheduleQbSuitelet(request, response){
if (request.getMethod() == 'GET') {
    var form = nlapiCreateForm('Pick Confirmation');
    
		var WaveField = form.addField('custpage_wave', 'select', 'Wave #');
		WaveField.setLayoutType('startrow', 'none');
		FillWave(form, WaveField,-1);
//		var filtersso = new Array();
//		filtersso[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'); //PICK
//		filtersso[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', '9');
//		 var columnsinvt = new Array();
//		columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
//		columnsinvt[0].setSort('true');
//		
//		WaveField.addSelectOption("", "");
//		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnsinvt);
//		for (var i = 0; searchresults!=null && i < searchresults.length; i++) {
//			var res=  form.getField('custpage_wave').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_wave_no'), 'is');
//			if (res != null) {
//				nlapiLogExecution('ERROR', 'res.length', res.length + searchresults[i].getValue('custrecord_ebiz_wave_no') );
//				if (res.length > 0) {
//					continue;
//				}
//			}
//			WaveField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_wave_no'), searchresults[i].getValue('custrecord_ebiz_wave_no'));
//		}
		
		var DoField = form.addField('custpage_do', 'select', 'Fulfillment Order #');
		DoField.setLayoutType('startrow', 'none');
		var filtersdo = new Array();
		filtersdo[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'); //Pick
		filtersdo[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is',  '9');
		
		var columnsdo = new Array();
		columnsdo[0] = new nlobjSearchColumn('name');
		columnsdo[0].setSort('true');
		
		DoField.addSelectOption("", "");
		var searchresultsdo = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersdo,columnsdo);
		
		for (var i = 0; searchresultsdo!=null && i <  searchresultsdo.length; i++) {
			var res=  form.getField('custpage_do').getSelectOptions(searchresultsdo[i].getValue('name'), 'is');
			if (res != null) {
				//nlapiLogExecution('ERROR', 'res.length', res.length + searchresultsdo[i].getValue('name') );
				if (res.length > 0) {
					continue;
				}
			}
			
			DoField.addSelectOption(searchresultsdo[i].getValue('name'), searchresultsdo[i].getValue('name'));
		}	 
		 form.addSubmitButton('Display');
        response.writePage(form);
    }
    else //this is the POST block
    {
		 var vwave = request.getParameter('custpage_wave');
		 var vQbdo = request.getParameter('custpage_do');
		  var SOarray = new Array();
		if (vwave!=null &&  vwave != "") {
        SOarray ["ebiz_wave_no"] = vwave;
		}
		if (vQbdo!=null &&  vQbdo != "") {
		SOarray ["custpage_do"] = vQbdo;       
		}
		response.sendRedirect('SUITELET', 'customscript_pickconfirmsch', 'customdeploy_pickconfirmsch', false, SOarray );
		
      
    }
}
function FillWave(form, WaveField,maxno){

	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9'])); 
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty', null));
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('id').setSort('true'));	
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave_no').setSort('true'));
	columnso.push(new nlobjSearchColumn('name'));

	WaveField.addSelectOption("", "");


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		if(searchresults[i].getValue('custrecord_ebiz_wave_no') != null && searchresults[i].getValue('custrecord_ebiz_wave_no') != "" && searchresults[i].getValue('custrecord_ebiz_wave_no') != " ")
		{
			var resdo = form.getField('custpage_wave').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_wave_no'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		WaveField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_wave_no'), searchresults[i].getValue('custrecord_ebiz_wave_no'));
	}


	if(searchresults!=null && searchresults.length>=1000)
	{
		var maxno=searchresults[searchresults.length-1].getValue('id');		
		FillWave(form, WaveField,maxno);	
	}
}

function myGenerateLocationButton(){
	alert("Hi"); 
}
