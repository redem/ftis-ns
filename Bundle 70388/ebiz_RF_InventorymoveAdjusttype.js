/***************************************************************************
	  		   eBizNET Solutions LTD               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/ebiz_RF_InventorymoveAdjusttype.js,v $
 *     	   $Revision: 1.3.2.4.4.3.4.4 $
 *     	   $Date: 2014/06/13 09:58:31 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS	*
 *
 * 
 *
 *****************************************************************************/


function InventoryMoveAdjustType(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var getNewLP = request.getParameter('custparam_newLP');
		var getMoveQuantity = request.getParameter('custparam_moveqty');
		var getMoveQty = request.getParameter('custparam_moveqty');
		var getBinLocationId = request.getParameter('custparam_imbinlocationid');
		var getBinLocation = request.getParameter('custparam_imbinlocationname');
		var getBatchNo = request.getParameter('custparam_batch');  
		var getItemId = request.getParameter('custparam_imitemid');
		var getItem = request.getParameter('custparam_imitem');
		var getTotQuantity = request.getParameter('custparam_totquantity');
		var getAvailQuantity = request.getParameter('custparam_availquantity');    
		var getUOMId = request.getParameter('custparam_uomid');
		var getUOM = request.getParameter('custparam_uom');
		var getStatus = request.getParameter('custparam_status');
		var getStatusId = request.getParameter('custparam_statusid');
		var getLOTId = request.getParameter('custparam_lotid');
		var getLOTNo = request.getParameter('custparam_lot');  
		var getLPId = request.getParameter('custparam_lpid');
		var getLP = request.getParameter('custparam_lp');  
		var getItemType = request.getParameter('custparam_itemtype');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getInvtRecID=request.getParameter('custparam_invtrecid');
		var totqtymoved=request.getParameter('custparam_totqtymoveed');
		var locationId=request.getParameter('custparam_locationId');//
		var skustatus=request.getParameter('custparam_skustatus');
		var adjustflag=request.getParameter('custparam_adjustflag');
		var ItemDesc = request.getParameter('custparam_itemdesc');
		var compId=request.getParameter('custparam_compid');//
		var fifodate=request.getParameter('custparam_fifodate');//

		nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId'));

		nlapiLogExecution('ERROR', 'getInvtRecID ',getInvtRecID);
		var ItemStatus;
		var itemStatusId;
		var AdjTypeLoopCount = 0;
		var nextClicked;
		var Adjustvalue;

		var AdyTypesSearchResult = getAdyTypesList();

		var AdyTypesCount =0;

		if(AdyTypesSearchResult!=null && AdyTypesSearchResult!='')
			AdyTypesCount = AdyTypesSearchResult.length;

		if (request.getParameter('custparam_count') != null)
		{
			var AdjTypeRetrieved = request.getParameter('custparam_count');
			nlapiLogExecution('ERROR', 'AdjTypeRetrieved', AdjTypeRetrieved);

			nextClicked = request.getParameter('custparam_nextclicked');
			nlapiLogExecution('ERROR', 'nextClicked', nextClicked);

			AdyTypesCount = AdyTypesCount - AdjTypeRetrieved;
			nlapiLogExecution('ERROR', 'itemStatusCount', AdyTypesCount);
		}
		else
		{
			var AdjTypeRetrieved = 0;
			nlapiLogExecution('ERROR', 'AdjTypeRetrieved', AdjTypeRetrieved);
		}

		if (AdyTypesCount > 10)
		{
			AdjTypeLoopCount = 10;
		}
		else
		{
			AdjTypeLoopCount = AdyTypesCount;
		}
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "";
			st1 = "SELECCIONAR TIPO DE REGULACI&#211;N:";
			st2 = "INGRESAR SELECCI&#211;N";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "PR&#211;XIMO";
			
		}
		else
		{
			st0 = "";
			st1 = "SELECT ADJUSTMENT TYPE :";
			st2 = "ENTER SELECTION";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "NEXT";
	
		}
		
		

		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript'>";
		
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		html = html + "function OnKeyDown_CL() ";
		html = html + " { ";     
		html = html + "         if (";
		html = html + " event.keyCode == 112 || event.keyCode == 113 || event.keyCode == 114 || event.keyCode == 115 || event.keyCode == 116 || event.keyCode == 117 ||";
		html = html + " event.keyCode == 118 || event.keyCode == 119 || event.keyCode == 120 || event.keyCode == 121 || event.keyCode == 122) {";
		html = html + " var arrElements = document.getElementsByTagName('input');";
		html = html + " var keyFound = false;";
		html = html + " for (i = 0; i < arrElements.length; i++) {";
		html = html + " if (arrElements[i].type == 'submit') {";
		html = html + " switch (event.keyCode) {";
		html = html + " case 112:";
		html = html + " if (arrElements[i].value == 'F1')";	//F7 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 113:";
		html = html + " if (arrElements[i].value == 'F2')";		//F8 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 114:";
		html = html + " if (arrElements[i].value == 'F3')";	//F9 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 115:";
		html = html + " if (arrElements[i].value == 'F4')";		//F10 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 116:";
		html = html + " if (arrElements[i].value == 'F5')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 117:";
		html = html + " if (arrElements[i].value == 'F6')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 118:";
		html = html + " if (arrElements[i].value == 'F7')";		//F7 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 119:";
		html = html + " if (arrElements[i].value == 'F8')";		//F8 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 120:";
		html = html + " if (arrElements[i].value == 'F9')";		//F9 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 121:";
		html = html + " if (arrElements[i].value == 'F10')";		//F10 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 122:";
		html = html + " if (arrElements[i].value == 'F11')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " }";
		html = html + " if (keyFound == true) {";        
		html = html + " eval('document._rf_checkin_po.' + arrElements[i].name + '.click();');";
		html = html + " return false;";
		html = html + " }";
		html = html + " }";
		html = html + " }";
		html = html + " }    ";        
		html = html + "    return true; ";
		html = html + "    }";        
		html = html + " </SCRIPT>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";     
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
	//	html = html + " document.getElementById('enteradjusttype').focus();";        
		html = html + "</script>";		
		html = html + "</head><body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_itemstatus' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";        
		html = html + "			</tr>";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteradjusttype' id='enteradjusttype' type='text'/>";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "					<input type='hidden' name='hdntotqtymoved' value=" + totqtymoved + ">";
		html = html + "					<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "					<input type='hidden' name='hdnLPId' value=" + getLPId + ">";        
		html = html + "					<input type='hidden' name='hdnLOTNo' value=" + getLOTNo + ">";
		html = html + "					<input type='hidden' name='hdnLOTNoId' value=" + getLOTId + ">";
		html = html + "					<input type='hidden' name='hdnStatus' value=" + getStatus + ">";
		html = html + "					<input type='hidden' name='hdnStatusId' value=" + getStatusId + ">";
		html = html + "					<input type='hidden' name='hdnUOM' value=" + getUOM + ">";
		html = html + "					<input type='hidden' name='hdnUOMId' value=" + getUOMId + ">";
		html = html + "					<input type='hidden' name='hdnTotQty' value=" + getTotQuantity + ">";
		html = html + "					<input type='hidden' name='hdnAvailQty' value=" + getAvailQuantity + ">";
		html = html + "					<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "					<input type='hidden' name='hdnMoveQty' value=" + getMoveQuantity + ">";
		html = html + "					<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "					<input type='hidden' name='hdnInvtRecID' value=" + getInvtRecID + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnlocationId' value=" + locationId + ">";
		html = html + "				    <input type='hidden' name='hdncompId' value=" + compId + ">";
		html = html + "				    <input type='hidden' name='hdnskustatus' value=" + skustatus + ">";
		html = html + "				    <input type='hidden' name='hdnAdjustvalue' value=" + Adjustvalue + ">";
		html = html + "				    <input type='hidden' name='hdnAdjustflag' value=" + adjustflag + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + ItemDesc + "'>";
		html = html + "				    <input type='hidden' name='hdnfifodate' value='" + fifodate + "'>";
		html = html + "				<td><input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		nlapiLogExecution('ERROR', 'AdjTypeLoopCount', AdjTypeLoopCount);	
		if (AdjTypeLoopCount > 10)
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st5 + " <input name='cmdNext' type='submit' value='F8'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}

		var StatusNo=1;
		for (var i = AdjTypeRetrieved; i < (parseFloat(AdjTypeRetrieved) + parseFloat(AdjTypeLoopCount)); i++) {
			var AdyTypesSearchResults = AdyTypesSearchResult[i];

			if (AdyTypesSearchResults != null)
			{
				itemStatus = AdyTypesSearchResults.getValue('name');
				Adjustvalue=AdyTypesSearchResults.getValue('internalid');
				itemStatusId = AdyTypesSearchResults.getId();				
			}
			var value = parseFloat(i) + 1;
			html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";

		}
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteradjusttype').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var getNewLPId = "";
		var getActualBeginDate = request.getParameter('hdnActualBeginDate');
		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');
		var getAdjustTypeoption =request.getParameter('enteradjusttype');
		nlapiLogExecution('ERROR', 'AdjustType option', getAdjustTypeoption);	
		var getAdjustType;
		var AdjTypeResult=getAdyTypesList();

		if(AdjTypeResult != null && AdjTypeResult != '')
		{	
			for(i=0;i<AdjTypeResult.length;i++)
			{
				var adjtypeoption=i+1;

				if(adjtypeoption==getAdjustTypeoption)
				{
					getAdjustType = AdjTypeResult[i].getId();	
				}
			}
		}
//		nlapiLogExecution('ERROR', 'Adjust Type ', getAdjustType);	

		getMoveQuantity = request.getParameter('hdnMoveQty');
		getAvailQuantity = request.getParameter('hdnAvailQty');

		var IMarray = new Array();
		
		var getLanguage = request.getParameter('hdngetLanguage');
		IMarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', IMarray["custparam_language"]);
    	
		
		var st6;
		if( getLanguage == 'es_ES')
		{
			
			st6 = "TIPO DE AJUSTE NO V&#193;LIDO";
			
		}
		else
		{
			
			st6 = "INVALID ADJUSTMENT TYPE";
			
		}

		IMarray["custparam_newLP"] = getNewLP;	

		IMarray["custparam_actualbegindate"] = getActualBeginDate;

		IMarray["custparam_screenno"] = '22';

		var TimeArray = new Array();
		IMarray["custparam_actualbegintime"] = getActualBeginTime;
		IMarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
		IMarray["custparam_error"] = st6;

		IMarray["custparam_totqtymoveed"] = request.getParameter('hdntotqtymoved');
		IMarray["custparam_imbinlocationid"] = request.getParameter('hdnBinLocationId');
		IMarray["custparam_imbinlocationname"] = request.getParameter('hdnBinLocation');
		IMarray["custparam_imitemid"] = request.getParameter('hdnItemId');
		IMarray["custparam_imitem"] = request.getParameter('hdnItem');
		IMarray["custparam_totquantity"] = request.getParameter('hdnTotQty');
		IMarray["custparam_availquantity"] = request.getParameter('hdnAvailQty');
		IMarray["custparam_moveqty"] = request.getParameter('hdnMoveQty');
		IMarray["custparam_status"] = request.getParameter('hdnStatus');
		IMarray["custparam_statusid"] = request.getParameter('hdnStatusId');
		IMarray["custparam_uom"] = request.getParameter('hdnUOM');
		IMarray["custparam_uomid"] = request.getParameter('hdnUOMId');		
		IMarray["custparam_lot"] = request.getParameter('hdnLOTNo');
		IMarray["custparam_lotid"] = request.getParameter('hdnLOTNoId');
		IMarray["custparam_lp"] = request.getParameter('hdnLP');
		IMarray["custparam_lpid"] = request.getParameter('hdnLPId');
		IMarray["custparam_itemtype"]=request.getParameter('hdnitemType');
		IMarray["custparam_invtrecid"] = request.getParameter('hdnInvtRecID');
		IMarray["custparam_locationId"] = request.getParameter('hdnlocationId');		
		IMarray["custparam_skustatus"]=request.getParameter('hdnskustatus');
		IMarray["custparam_adjusttype"]=getAdjustType;
		IMarray["custparam_adjustflag"]=request.getParameter('hdnAdjustflag');
		IMarray["custparam_existingLP"]='';
		IMarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		IMarray["custparam_itemdesc"] = request.getParameter('hdnItemdesc');
		IMarray["custparam_compid"] = request.getParameter('hdncompId');
		IMarray["custparam_fifodate"] = request.getParameter('hdnfifodate');
		nlapiLogExecution('ERROR', 'Compid', request.getParameter('hdncompId'));

		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') 
		{       		
			//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_qty', 'customdeploy_rf_inventory_move_qty_di', false, IMarray);
			response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_status', 'customdeploy_rf_inventory_move_status_di', false, IMarray);
		}
		else 
		{
			if(getAdjustType != null && getAdjustType != '')
			{
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_complete', 'customdeploy_rf_inventory_move_comple_di', false, IMarray);
				//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_endloc', 'customdeploy_rf_inventory_move_endloc_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Navigating to LP', 'Success');
			}
			else
			{
				POarray["custparam_error"] = st6;				 
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('ERROR', 'itemRecord', 'INVALID ADJUSTMENT TYPE');
			}
		}        
	}
}

function getAdyTypesList()
{
	var AdjustmentTypeSearchResult = new Array();		
	var AdjustmentTypeFilters = new Array();		

	AdjustmentTypeFilters.push(new nlobjSearchFilter( 'custrecord_adjusttasktype', null, 'anyOf', '18')); //Task Type-XFER
	AdjustmentTypeFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var AdjustmentTypeColumns = new Array();
	AdjustmentTypeColumns[0] = new nlobjSearchColumn('name');
	AdjustmentTypeColumns[1] = new nlobjSearchColumn('internalid');
	AdjustmentTypeColumns[1].setSort();

	AdjustmentTypeSearchResult = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, AdjustmentTypeFilters, AdjustmentTypeColumns);

	return AdjustmentTypeSearchResult;

}