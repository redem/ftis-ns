/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_Putaway_ItemStatus.js,v $
 *     	   $Revision: 1.1.2.6 $
 *     	   $Date: 2014/06/13 08:48:15 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_Putaway_ItemStatus.js,v $
 * Revision 1.1.2.6  2014/06/13 08:48:15  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.5  2014/05/30 00:34:24  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.4  2014/01/30 15:51:09  sponnaganti
 * case# 20127005
 * In Itemstatus field Default value  is removed
 *
 * Revision 1.1.2.3  2013/12/26 15:52:51  gkalla
 * case#20126513
 * Standard bundle issue
 *
 * Revision 1.1.2.2  2013/12/24 16:23:43  gkalla
 * case#20126513
 * Standard bundle issue
 *
 * Revision 1.1.2.1  2013/06/14 07:05:45  gkalla
 * CASE201112/CR201113/LOG201121
 * PCT Case# 20122979. To select item status while RF WO confirm build
 *
 * Revision 1.15.2.10.4.8.2.5  2013/06/04 10:54:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes 
 *
 *****************************************************************************/

function CheckInItemStatus(request, response){
	if (request.getMethod() == 'GET') {
		var ItemStatus;
		var itemStatusId;
		var itemStatusLoopCount = 0;
		var nextClicked;

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('ERROR', 'getOptedField', getOptedField);

		var getItemLP;

		var getWONo = request.getParameter('custparam_woname');
		var getWOItem = request.getParameter('custparam_item');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getWOInternalId = request.getParameter('custparam_woid');
		var getWOQtyEntered = request.getParameter('custparam_expectedquantity');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getnoofrecords =request.getParameter('custparam_noofrecords');
		var getwoStatus =request.getParameter('custparam_wostatus');
		var getWOLinePackCode = request.getParameter('custparam_packcode');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getFetchedLocationId = request.getParameter('custparam_beginlocinternalid');



		var getExpDate = request.getParameter('custparam_expdate');
		var getMfgDate=request.getParameter('custparam_mfgdate');
		var getBestBeforeDate=request.getParameter('custparam_bestbeforedate');
		var getFifoDate=request.getParameter('custparam_fifodate');
		var getLastAvlDate=request.getParameter('custparam_lastdate');
		var getBatchNo=request.getParameter('custparam_batchno');
		var getFifoCode = request.getParameter('custparam_fifocode');
		var getItemType = request.getParameter('custparam_itemtype');		

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getAutogenerateLp = request.getParameter('custparam_autogenerateLp');


		nlapiLogExecution('ERROR','getWHLocation',getWHLocation);
		nlapiLogExecution('ERROR','getBatchNo',getBatchNo);

		//var getWONo = request.getParameter('custparam_poid');
		//var getWOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		//var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		//var getWOInternalId = request.getParameter('custparam_pointernalid');
		//var getWOQtyEntered = request.getParameter('custparam_poqtyentered');
		//var getWOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getWOPackCode = request.getParameter('custparam_polinepackcode');
		var getWOItemStatusValue = request.getParameter('custparam_polineitemstatusValue');
		nlapiLogExecution('ERROR','getWOItemStatusValue',getWOItemStatusValue);

		var getWOItemStatus = request.getParameter('custparam_polineitemstatus');
		nlapiLogExecution('ERROR', 'Item Status', getWOItemStatus);
		nlapiLogExecution('ERROR', 'getWOQtyEntered', getWOQtyEntered);

		var getItemCube = request.getParameter('custparam_itemcube');

		var getBaseUomQty = request.getParameter('custparam_baseuomqty');
		nlapiLogExecution('ERROR', 'getBaseUomQty', getBaseUomQty);
		/*		var getItemQuantity = request.getParameter('hdnQuantity');
		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');*/
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');

		var pickfaceEnteredOption = request.getParameter('custparam_enteredOption'); 
		var vAutoLotFlag = request.getParameter('custparam_autolot'); 

		//var getWHLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('ERROR','vAutoLotFlag',vAutoLotFlag);



		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);


	    
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INGRESAR SELECCI&#211;N";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";
			
		}
		else
		{
			st0 = "";
			st1 = "ENTER SELECTION";
			st2 = "SEND";
			st3 = "PREV";

		}
		var itemStatusFilters = new Array();
		itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
		itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
		itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',[getWHLocation]);

		var itemStatusColumns = new Array();
		itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
		itemStatusColumns[1] = new nlobjSearchColumn('name');
		itemStatusColumns[0].setSort();
		itemStatusColumns[1].setSort();

		var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);
		var itemStatusCount=0;
		if(itemStatusSearchResult!=null&&itemStatusSearchResult!="")
			itemStatusCount = itemStatusSearchResult.length;
		var poloneitemStatus;
		var polineitemstatusvalue;
		var displayseq;
		var getPOLineItemStatusValue="";
		if(itemStatusSearchResult!=null&&itemStatusSearchResult!="")
		{
			/*for(var k =0; k<itemStatusSearchResult.length ;k++)
			{
				polineitemstatusvalue =  itemStatusSearchResult[k].getId();
				if(polineitemstatusvalue == getPOLineItemStatusValue)
				{
					poloneitemStatus = itemStatusSearchResult[k].getValue('name');
					displayseq = itemStatusSearchResult[k].getValue('custrecord_display_sequence');
				}
			}*/
			polineitemstatusvalue =  itemStatusSearchResult[0].getId();
			 
			poloneitemStatus = itemStatusSearchResult[0].getValue('name');
			displayseq = itemStatusSearchResult[0].getValue('custrecord_display_sequence');
		}
		if (request.getParameter('custparam_count') != null)
		{
			var itemStatusRetrieved = request.getParameter('custparam_count');
			nlapiLogExecution('ERROR', 'itemStatusRetrieved', itemStatusRetrieved);

			nextClicked = request.getParameter('custparam_nextclicked');
			nlapiLogExecution('ERROR', 'nextClicked', nextClicked);

			itemStatusCount = itemStatusCount - itemStatusRetrieved;
			nlapiLogExecution('ERROR', 'itemStatusCount', itemStatusCount);
		}
		else
		{
			var itemStatusRetrieved = 0;
			nlapiLogExecution('ERROR', 'itemStatusRetrieved', itemStatusRetrieved);
		}	
		
		
		itemStatusLoopCount = itemStatusCount;
		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_itemstatus'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterstatus').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_itemstatus' method='POST'>";
		html = html + "		<table>";

		nlapiLogExecution('ERROR', 'itemStatusLoopCount', itemStatusLoopCount);
		var StatusNo=1;
		for (var i = itemStatusRetrieved; i < (parseFloat(itemStatusRetrieved) + parseFloat(itemStatusLoopCount)); i++) {
			var itemStatusSearchResults = itemStatusSearchResult[i];

			if (itemStatusSearchResults != null)
			{
				itemStatus = itemStatusSearchResults.getValue('name');
				itemStatusId = itemStatusSearchResults.getId();				
			}
			var value = parseFloat(i) + 1;
			//html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";   
			if(poloneitemStatus==itemStatus)
			{
				StatusNo=value; 
			}

			nlapiLogExecution('ERROR', 'StatusNo', StatusNo);
		}
		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>ENTER ITEM STATUS : <label>" + getPOLineItemStatus + "</label>";
		html = html + "			<tr><td align = 'left'>" + st1 +"</td></tr> ";
		html = html + "			<tr>";
		//case# 20127005 starts(StatusNo removed from value of enter status)
		html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' value=''/>";
		//case# 20127005 end
		//html = html + "				<input type='hidden' name='hdndisplayseq' value=" + displayseq + "></td>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + poloneitemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnWOName' value=" + getWONo + ">";	
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getWOItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getWOPackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnbaseuomqty' value=" + getBaseUomQty + ">";
		//html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getWOItemRemainingQty) + ">";
		//html = html + "				<input type='hidden' name='hdnWOQuantityEntered' value=" + parseFloat(getWOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnWOInternalId' value=" + getWOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdnLastAvlDate' value=" + getLastAvlDate + ">";
		html = html + "				<input type='hidden' name='hdnFifoDate' value=" + getFifoDate + ">";
		html = html + "				<input type='hidden' name='hdnFifoCode' value=" + getFifoCode + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnPickfaceEnteredOption' value=" + pickfaceEnteredOption + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnwoqtyenetered' value=" + getWOQtyEntered + ">";
		html = html + "				<input type='hidden' name='hdnAutoLotFlag' value=" + vAutoLotFlag + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + getWOItemStatusValue + "></td>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnBeginlocname' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginlocId' value=" + getFetchedLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getWOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnnoofrecords' value=" + getnoofrecords + ">";
		html = html + "				<input type='hidden' name='hdnAutoGenLp' value=" + getAutogenerateLp + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + getPOLineItemStatusValue + "></td>";
		//html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + StatusNo + "></td>";
		html = html + "				<input type='hidden' name='hdnCount' value=" + i + "></td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";




		for (var i = itemStatusRetrieved; i < (parseFloat(itemStatusRetrieved) + parseFloat(itemStatusLoopCount)); i++) {
			var itemStatusSearchResults = itemStatusSearchResult[i];

			if (itemStatusSearchResults != null)
			{
				itemStatus = itemStatusSearchResults.getValue('name');
				itemStatusId = itemStatusSearchResults.getId();				
			}
			var value = parseFloat(i) + 1;
			html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";   
			if(poloneitemStatus==itemStatus)
			{
				StatusNo=value; 
			}

			nlapiLogExecution('ERROR', 'StatusNo', StatusNo);
		}


		/*if (itemStatusLoopCount > 4)
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>NEXT <input name='cmdNext' type='submit' value='F8'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}*/
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterstatus').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
//		Commented by Phani 04-08-2011    	
//		var getItemStatus = request.getParameter('enterstatus');
		var getReturnedItemStatus = request.getParameter('hdnItemStatus');
		var itemStatus = '';
		var itemStatusId = '';

//		Added by Phani 04-08-2011
		var getItemStatusOption = request.getParameter('enterstatus');

		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');

		var nextClicked = request.getParameter('hdnNextClicked');
		nlapiLogExecution('ERROR', 'nextClicked', nextClicked);

		// This variable is to hold the Quantity entered.
		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);
    	
		
		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			st4 = "ESTADO ELEMENTO NO V&#193;LIDO";
			st5 = "USTED NO PUEDE RECIBIR CON EL ESTADO";
			
		}
		else
		{

			st4 = "INVALID ITEM STATUS";
			st5 = " YOU CANNOT RECEIVE WITH THIS STATUS";
			
		}
		

//		var itemStatus;
		POarray["custparam_polineitemstatusValue"]=request.getParameter('hdnItemStatusNo');
		POarray["custparam_StatusNo"] = request.getParameter('hdnItemStatusNo');
 

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		nlapiLogExecution('ERROR', 'optedField', POarray["custparam_option"]);
		POarray["custparam_woid"] = request.getParameter('custparam_woid');
		POarray["custparam_item"] = request.getParameter('custparam_item');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_expectedquantity"] = request.getParameter('hdnwoqtyenetered');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_noofrecords"] = request.getParameter('hdnnoofrecords');
		POarray["custparam_woname"] = request.getParameter('hdnWOName');

		POarray["custparam_error"] = st5;
		POarray["custparam_polineitemstatusValue"]=request.getParameter('hdnItemStatusNo');
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_woid');
		POarray["custparam_poqtyentered"] = request.getParameter('custparam_expectedquantity');
		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('custparam_packcode');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_expectedquantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_expectedquantity');
		POarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_itemquantity"] = request.getParameter('hdnQuantity');

		POarray["custparam_enteredOption"] = request.getParameter('hdnPickfaceEnteredOption');

		nlapiLogExecution('ERROR', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);

		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		var getBaseUomQty=request.getParameter('hdnbaseuomqty');
		POarray["custparam_baseuomqty"] = getBaseUomQty;

		POarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		POarray["custparam_beginlocinternalid"] = request.getParameter('hdnBeginlocId');
		POarray["custparam_beginlocationname"] = request.getParameter('hdnBeginlocname');

		//		var TimeArray = new Array();
		//		TimeArray = getActualBeginTime.split(' ');
		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; //TimeArray[1];
		nlapiLogExecution('ERROR', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('ERROR', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

		POarray["custparam_screenno"] = 'WOPutGenStatus';

		POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
		POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
		POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
		POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
		POarray["custparam_lastdate"] = request.getParameter('hdnLastAvlDate');
		POarray["custparam_fifodate"] = request.getParameter('hdnFifoDate');
		POarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');
		//POarray["custparam_cartlp"] = getItemCartLP;
		//POarray["custparam_manufacturelot"] = getManufactureLot;
		//POarray["custparam_autolot"] = getAutoLotFlag;
		//nlapiLogExecution('ERROR', 'getAutoLotFlag', getAutoLotFlag);
		//nlapiLogExecution('ERROR', 'getManufactureLot', getManufactureLot);
		//nlapiLogExecution('ERROR', 'getItemCartLP', getItemCartLP);
		nlapiLogExecution('ERROR','hdnBatchNo',request.getParameter('hdnBatchNo'));

		nlapiLogExecution('DEBUG', 'POarray["custparam_batchno"]', POarray["custparam_batchno"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_mfgdate"]', POarray["custparam_mfgdate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_expdate"]', POarray["custparam_expdate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_bestbeforedate"]', POarray["custparam_bestbeforedate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_lastdate"]', POarray["custparam_lastdate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_fifodate"]', POarray["custparam_fifodate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_fifocode"]', POarray["custparam_fifocode"]);
		nlapiLogExecution('ERROR', 'POarray["custparam_fetcheditemid"]', POarray["custparam_fetcheditemid"]);
		nlapiLogExecution('ERROR', 'POarray["custparam_poitem"]', POarray["custparam_poitem"]);
		nlapiLogExecution('ERROR', 'POarray["custparam_itemquantity"]', POarray["custparam_itemquantity"]);
		nlapiLogExecution('ERROR', 'POarray.length2', POarray.length);

		// Added by Phani on 03-25-2011
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		var poLoc = request.getParameter('hdnWhLocation');
		nlapiLogExecution('ERROR', 'WH Location', POarray["custparam_whlocation"]);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent; //= request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
//		if (optedEvent == 'F7') {		
		if (request.getParameter('cmdPrevious') == 'F7') {
			if (nextClicked != 'Y')
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_loc', 'customdeploy_ebiz_rf_wo_putaway_loc_di', false, POarray);
				 
			}
			else
			{
				POarray["custparam_count"] = 0;
				nlapiLogExecution('ERROR', 'nextClicked', nextClicked);
				nlapiLogExecution('ERROR', 'Next in F7', POarray["custparam_count"]);
				response.sendRedirect('SUITELET', 'customscript_rf_itemstatus', 'customdeploy_rf_itemstatus_di', false, POarray);
			}
		}
		//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
		else if (request.getParameter('cmdNext') == 'F8') {
			POarray["custparam_count"] = request.getParameter('hdnCount');
			POarray["custparam_nextclicked"] = "Y";

			nlapiLogExecution('ERROR', 'Next', POarray["custparam_count"]);
			response.sendRedirect('SUITELET', 'customscript_rf_itemstatus', 'customdeploy_rf_itemstatus_di', false, POarray);
			 
		}
		else// if (request.getParameter('cmdSend') == 'ENT')
		{
			 
//			Added by Phani on 04-08-2011            
			if (getItemStatusOption == "")
			{
				//	if the 'Send' button is clicked without any option value entered,
				//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Entered Item Status', 'No data');
			} 
			else {
				/*
				 * Added by Phani on 04-08-2011.
				 * This part of the code is to fetch the Item Status from the option selected from the Item Status menu.
				 */
				nlapiLogExecution('ERROR', 'Item Status Option', getItemStatusOption);
				var getWHLocation='';
				if(POarray["custparam_whlocation"] != null && POarray["custparam_whlocation"] != '')
					getWHLocation=POarray["custparam_whlocation"];
				var itemStatusFilters = new Array();
				itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
				itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
				itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', [getWHLocation]);

				var itemStatusColumns = new Array();
				//itemStatusColumns[0] = new nlobjSearchColumn('name');
				//itemStatusColumns[1] = new nlobjSearchColumn('custrecord_allowrcvskustatus');
				//itemStatusColumns[0].setSort();

				itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
				itemStatusColumns[1] = new nlobjSearchColumn('name');
				itemStatusColumns[2] = new nlobjSearchColumn('custrecord_allowrcvskustatus');
				itemStatusColumns[0].setSort();
				itemStatusColumns[1].setSort();

				var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);

				var itemStatusCount=0;
				if(itemStatusSearchResult!=null && itemStatusSearchResult!='')
				 itemStatusCount = itemStatusSearchResult.length;

				var count = request.getParameter('hdnCount');
				nlapiLogExecution('ERROR', 'count', count);

				if (parseFloat(count) >= parseFloat(getItemStatusOption))
				{
					if (itemStatusSearchResult != null)
					{
						var ItemStatusRcvFlag;
						for (var i = 0; i <= getItemStatusOption; i++) {
							var itemStatusSearchResults = itemStatusSearchResult[i];
							if (parseFloat(getItemStatusOption) == parseFloat(i+1))
							{
								nlapiLogExecution('ERROR', 'here');
								itemStatus = itemStatusSearchResults.getValue('name');
								itemStatusId = itemStatusSearchResults.getId();
								nlapiLogExecution('ERROR', 'Latest Item Status', itemStatus);
								ItemStatusRcvFlag=itemStatusSearchResults.getValue('custrecord_allowrcvskustatus');
							}
							else
							{
								POarray["custparam_error"] = st4;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Item Status entered is not a valid status');
							}
						}
						nlapiLogExecution('ERROR', 'ItemStatusRcvFlag', ItemStatusRcvFlag);
						if(ItemStatusRcvFlag=='T')
						{
							if (itemStatus != null)
							{
								
								POarray["custparam_polineitemstatustext"] = itemStatus;
								 
								  
									POarray["custparam_polineitemstatus"] = itemStatusId;	//chksearchresults[0].getId();
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_lp', 'customdeploy_ebiz_rf_wo_putaway_lp_di', false, POarray);									 
									nlapiLogExecution('DEBUG', 'Item Status ', POarray["custparam_polineitemstatus"]);
								 
							}
							else {
								POarray["custparam_error"] = st4;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Item Status Not Found as Valid in ITEMSTATUS RECORD');
							}
						}
						else
						{
							POarray["custparam_error"] = st5;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'RCV Status flag F');

						}
					}
				}
				else {
					POarray["custparam_error"] = st4;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Item Status entered is not a valid status');
				}
				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			}
		}
	}
}
