/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_Cart_FifoDateConfirm.js,v $
 *     	   $Revision: 1.1.2.1.2.7 $
 *     	   $Date: 2014/05/30 00:26:47 $
 *     	   $Author: nneelam $
 *     	   $Name: t_eBN_2014_1_StdBundle_3_62 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Cart_FifoDateConfirm.js,v $
 * Revision 1.1.2.1.2.7  2014/05/30 00:26:47  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.2.6  2014/02/21 14:52:01  rmukkera
 * Case# 20127203
 *
 * Revision 1.1.2.1.2.5  2013/10/18 08:53:51  schepuri
 * 20125026
 *
 * Revision 1.1.2.1.2.4  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.1.2.3  2013/06/04 07:26:55  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Parsing CR for PCT
 *
 * Revision 1.1.2.1.2.2  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.1.2.1  2013/02/07 08:41:49  skreddy
 * CASE201112/CR201113/LOG201121
 *  RF Lot auto generating FIFO enhancement
 *
 * Revision 1.18.2.55  2012/08/28 15:49:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Added screen for cluster pick qty exception.
 *
 *
 *****************************************************************************/

function FIFODateConfirm(request, response){
	if (request.getMethod() == 'GET') {
		//	Get the Error Message from the previous screen, which is passed as a parameter		
		getError = request.getParameter('custparam_error');
		nlapiLogExecution('DEBUG', 'Into Request', getError);
		var screenNo=request.getParameter('custparam_screenno');
		
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
		/*var getItemQuantity = request.getParameter('hdnQuantity');
		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');*/
		
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getMfgDate = request.getParameter('custparam_mfgdate');
		var getExpDate = request.getParameter('custparam_expdate');
		var getBestBeforeDate = request.getParameter('custparam_bestbeforedate');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var ItemShelfLife = request.getParameter('custparam_shelflife');
		var CaptureExpiryDate = request.getParameter('custparam_captureexpirydate');
		var CaptureFifoDate = request.getParameter('custparam_capturefifodate');	
		var actScannedBatchno = request.getParameter('custparam_actscanbatchno');
		
		
		var functionkeyHtml=getFunctionkeyScript('_rf_error'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_error' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONFIRMATION: <br><label>" + getError + "</label>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getPOLineItemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseInt(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseInt(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemShelfLife' value=" + ItemShelfLife + ">";
		html = html + "				<input type='hidden' name='hdnCaptureExpiryDate' value=" + CaptureExpiryDate + ">";
		html = html + "				<input type='hidden' name='hdnCaptureFifoDate' value=" + CaptureFifoDate + ">";		
		html = html + "				<input type='hidden' name='hdnscanbatchno' value=" + actScannedBatchno + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONTINUE <input name='cmdContinue' type='submit' value='F8'/>";
		html = html + "					BACK <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var optedEvent = request.getParameter('cmdPrevious');
		var getError = request.getParameter('custparam_error');
		var getScreenNo = request.getParameter('custparam_screenno');
		nlapiLogExecution('DEBUG', 'Screen No', getScreenNo);
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);

		var POarray = new Array();

		// From Check-In process
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('custparam_poqtyentered');
		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('custparam_polinepackcode');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');
		POarray["custparam_polineitemstatusValue"] = request.getParameter('custparam_polineitemstatusValue');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		POarray["custparam_actualbegintime"] = request.getParameter('custparam_actualbegintime');
		POarray["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');
		POarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		POarray["custparam_batchno"] = request.getParameter('custparam_batchno');
		POarray["custparam_fifodate"]=request.getParameter('custparam_fifodate');
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');		
		POarray["custparam_shelflife"] = request.getParameter('custparam_shelflife');
		POarray["custparam_captureexpirydate"] = request.getParameter('custparam_captureexpirydate');
		POarray["custparam_capturefifodate"] = request.getParameter('custparam_capturefifodate');
		POarray["custparam_fifocode"] = request.getParameter('custparam_fifocode');
		POarray["custparam_bestbeforedate"] = request.getParameter('custparam_bestbeforedate');
		POarray["custparam_mfgdate"] = request.getParameter('custparam_mfgdate');
		POarray["custparam_lastdate"] = request.getParameter('custparam_lastdate');
		POarray["custparam_expdate"] = request.getParameter('custparam_expdate');
		POarray["custparam_actscanbatchno"] = request.getParameter('custparam_actscanbatchno');




		// From Putaway process
		/*POarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		POarray["custparam_quantity"] = request.getParameter('custparam_quantity');
		POarray["custparam_exceptionquantity"] = request.getParameter('custparam_exceptionquantity');
		POarray["custparam_exceptionQuantityflag"] = request.getParameter('custparam_exceptionQuantityflag');
		POarray["custparam_location"] = request.getParameter('custparam_location');
		POarray["custparam_beginlocation"] =  request.getParameter('custparam_beginlocation');
		POarray["custparam_item"] = request.getParameter('custparam_item');
		POarray["custparam_itemDescription"] = request.getParameter('custparam_itemDescription');
		POarray["custparam_itemid"] = request.getParameter('custparam_itemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_polinenumber"] = request.getParameter('custparam_polinenumber');
		POarray["custparam_poitemstatus"] = request.getParameter('custparam_poitemstatus');
		POarray["custparam_popackcode"] = request.getParameter('custparam_popackcode');
		POarray["custparam_beginlocation"] = request.getParameter('custparam_beginlocation');
		POarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
		POarray["custparam_itemtext"] = request.getParameter('custparam_itemtext');
		POarray["custparam_mfgdate"] = request.getParameter('custparam_mfgdate');
		POarray["custparam_expdate"] = request.getParameter('custparam_expdate');
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		POarray["custparam_recordid"] = request.getParameter('custparam_recordid');
		POarray["custparam_option"] = request.getParameter('custparam_option');
		POarray["custparam_recordcount"] = request.getParameter('custparam_recordcount');
		POarray["custparam_lpNumbersArray"] = request.getParameter('custparam_lpNumbersArray');
		POarray["custparam_skipid"] = request.getParameter('custparam_skipid');
		POarray["custparam_trantype"] = request.getParameter('custparam_trantype');
		POarray["custparam_itemstatus"] = request.getParameter('custparam_itemstatus');
		nlapiLogExecution('DEBUG', 'trantype', POarray["custparam_trantype"]);
		POarray["custparam_uomidtext"] = request.getParameter('custparam_uomidtext');
		POarray["custparam_number"] = parseInt(request.getParameter('custparam_number'));
		POarray["custparam_polineitemlp"]= request.getParameter('custparam_polineitemlp');
		POarray["custparam_serialno"] = request.getParameter('custparam_serialno');*/

		if (optedEvent == 'F7') {
			nlapiLogExecution('DEBUG', 'CHECK IN FIFO DATE F7 Pressed');
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_chkinfifodate', 'customdeploy_ebiz_rf_cart_chkinfifodate', false, POarray);

		}

		else
		{
			
			var BatchDetails = new Array();
			BatchDetails[0] = POarray["custparam_batchno"];
			BatchDetails[1] = POarray["custparam_mfgdate"];
			BatchDetails[2] = POarray["custparam_expdate"];
			BatchDetails[3] = POarray["custparam_bestbeforedate"];
			BatchDetails[4] = POarray["custparam_lastdate"];
			BatchDetails[5] = POarray["custparam_fifodate"];
			BatchDetails[6] = POarray["custparam_fifocode"];
		
			 CheckinLp(request.getParameter('custparam_fetcheditemid'), request.getParameter('custparam_poid'), request.getParameter('custparam_poitem'), request.getParameter('custparam_lineno'), request.getParameter('custparam_pointernalid'), request.getParameter('custparam_poqtyentered'), request.getParameter('custparam_poitemremainingqty'), request.getParameter('custparam_polinepackcode'), request.getParameter('custparam_polineitemstatus'), request.getParameter('custparam_polinequantity'), request.getParameter('custparam_polinequantityreceived'), request.getParameter('custparam_itemdescription'), request.getParameter('custparam_itemcube'), request.getParameter('custparam_actualbegindate'), request.getParameter('custparam_actualbegintime'), request.getParameter('custparam_actualbegintimeampm'),BatchDetails,POarray["custparam_actscanbatchno"]);
			
			//response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
		}

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}



function CheckinLp(ItemId,poid,poitem,lineno,pointernalid,poqtyentered,poitemremainingqty,polinepackcode,polineitemstatus,polinequantity,polinequantityreceived,itemdescription,itemcube,ActualBeginDate,ActualBeginTime,ActualBeginTimeAMPM,BatchDetails,ScannedBatch)
{

	nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
	nlapiLogExecution('DEBUG', 'ScannedBatch', ScannedBatch);
	
	var getItemLP = "", getItemCartLP = "",getsysItemLP="";
	getItemCartLP = request.getParameter('custparam_cartlpno');
	var getFetchedItemId = ItemId;
	
	nlapiLogExecution('DEBUG', 'custparam_option', request.getParameter('custparam_option'));
	
	var POarray = new Array();
	
	// Added by Phani on 03-25-2011
	//POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
	//var poLoc = request.getParameter('hdnWhLocation');
	
	POarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
	var poLoc = request.getParameter('custparam_whlocation');
	
	nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);	


		getsysItemLP = GetMaxLPNo(1, 1,poLoc);
		
		nlapiLogExecution('DEBUG', 'getsysItemLP LP now added', getsysItemLP);
	//}
	// This variable is to hold the Quantity entered.
	
	var EndLocationArray = new Array();

	
	var getPONo = poid;
	var getPOItem = poitem;
	var getPOLineNo = lineno;

	var getPOInternalId = pointernalid;
	var getPOQtyEntered = poqtyentered;
	var getPOItemRemainingQty = poitemremainingqty;
	var getPOLinePackCode = polinepackcode;
	var getPOLineItemStatus = polineitemstatus;
	var getPOLineQuantity = polinequantity;
	var getPOLineQuantityReceived = polinequantityreceived;
	var getItemDescription = itemdescription;
	var getItemCube = itemcube;
	var getActualBeginDate = ActualBeginDate;

	var getActualBeginTime = ActualBeginTime;
	var getActualBeginTimeAMPM = ActualBeginTimeAMPM;

	var getLineCount = 1;
	var getBaseUOM = 'EACH';
	var getBinLocation = "";

	POarray["custparam_error"] = 'INVALID LP';

	POarray["custparam_poid"] = poid;
	POarray["custparam_poitem"] = poitem;
	POarray["custparam_lineno"] = lineno;
	POarray["custparam_fetcheditemid"] =ItemId;
	POarray["custparam_pointernalid"] = pointernalid;
	POarray["custparam_poqtyentered"] = poqtyentered;
	POarray["custparam_poitemremainingqty"] = poitemremainingqty;
	POarray["custparam_polinepackcode"] = polinepackcode;
	POarray["custparam_polinequantity"] = polinequantity;
	POarray["custparam_polinequantityreceived"] = polinequantityreceived;
	POarray["custparam_polineitemstatus"] = polineitemstatus;
	POarray["custparam_itemdescription"] = itemdescription;
	POarray["custparam_itemquantity"] = polinequantity;
	POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
	POarray["custparam_enteredOption"] = request.getParameter('hdnPickfaceEnteredOption');

	nlapiLogExecution('DEBUG', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);

	POarray["custparam_itemcube"] = itemcube;

	POarray["custparam_actualbegindate"] =ActualBeginDate;

	//		var TimeArray = new Array();
	//		TimeArray = getActualBeginTime.split(' ');
	POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
	POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; //TimeArray[1];
	nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
	nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

	POarray["custparam_screenno"] = 'CRT8';

	/*POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
	POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
	POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
	POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
	POarray["custparam_lastdate"] = request.getParameter('hdnLastAvlDate');
	POarray["custparam_fifodate"] = request.getParameter('hdnFifoDate');
	POarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');*/
	POarray["custparam_cartlp"] = request.getParameter('custparam_cartlp');
	
	POarray["custparam_batchno"] = BatchDetails[0];
	POarray["custparam_mfgdate"] = BatchDetails[1];
	POarray["custparam_expdate"] = BatchDetails[2];
	POarray["custparam_bestbeforedate"] = BatchDetails[3];
	POarray["custparam_lastdate"] = BatchDetails[4];
	POarray["custparam_fifodate"] = BatchDetails[5];
	POarray["custparam_fifocode"] = BatchDetails[6];

	
	var POfilters = new Array();
	nlapiLogExecution('DEBUG', 'getPOInternalId', getPOInternalId);
	POfilters[0] = new nlobjSearchFilter('name', null, 'is', getPOInternalId);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, POfilters, null);

	var getPOReceiptNo = '';

	if (searchresults != null && searchresults.length > 0) {
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			getPOReceiptNo = searchresults[i].getValue('custrecord_ebiz_poreceipt_receiptno');
		}

		nlapiLogExecution('DEBUG', 'PO Receipt No', getPOReceiptNo);
	}

	// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
	// to the previous screen.
	//var optedEvent = request.getParameter('cmdPrevious');


	//	if the previous button 'F7' is clicked, it has to go to the previous screen 
	//  ie., it has to go to RF Main Menu.
//	if (optedEvent == 'F7') {
//		response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
//	}
	//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
	//            if (optedEvent != '' && optedEvent != null) {
	//else 
//		if (getItemLP == "") {
//			//	if the 'Send' button is clicked without any option value entered,
//			//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
//			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
//			nlapiLogExecution('DEBUG', 'Entered Item LP', getItemLP);//sss
//		}
//		else {
			//nlapiLogExecution('DEBUG', 'Item LP', getItemLP);
			var LPReturnValue = "";
//			if(getsysItemLP=="")
//			{
				nlapiLogExecution('DEBUG', 'getsysItemLP', 'userdefined');
				nlapiLogExecution('DEBUG', 'poLoc', poLoc);
				LPReturnValue = ebiznet_LPRange_CL(getsysItemLP, '1',poLoc);
//			}
//			else
//			{
//				nlapiLogExecution('DEBUG', 'getsysItemLP', 'systemgenerated');
//				LPReturnValue = ebiznet_LPRange_CL(getItemLP, '1');
//			}
			nlapiLogExecution('DEBUG', 'LP Return Value new', LPReturnValue);

			var lpExists = 'N';
			//LP Checking in masterlp record starts
			if (LPReturnValue == true) {
				try {
					nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
					var filtersmlp = new Array();
					filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getsysItemLP);

					var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

					if (SrchRecord != null && SrchRecord.length > 0) {
						nlapiLogExecution('DEBUG', 'LP FOUND');

						lpExists = 'Y';
					}
					else {
						nlapiLogExecution('DEBUG', 'LP NOT FOUND');
						var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
						customrecord.setFieldValue('name', getsysItemLP);
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getsysItemLP);
						var rec = nlapiSubmitRecord(customrecord, false, true);
					}
				} 
				catch (e) {
					nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
				}
				//LP Checking in masterlp record ends
				nlapiLogExecution('DEBUG', 'POarray.length1', POarray.length);
				nlapiLogExecution('DEBUG', 'lp Exists111', lpExists);
				var remainingCube = '';
//				if (LPReturnValue == true) {
				if(lpExists != 'Y')
				{
					POarray["custparam_polineitemlp"] = getsysItemLP;
					POarray["custparam_poreceiptno"] = getPOReceiptNo;
					var TotalItemCube = parseFloat(getItemCube) * parseFloat(getPOQtyEntered);
					nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );

					//					BeginLocationArray = GenerateLocation(getPOQtyEntered, getPOLineNo, getLineCount, getFetchedItemId, getPOItem, getBinLocation, getItemCube, getBeginLocation)
					var itemSubtype = nlapiLookupField('item', POarray["custparam_fetcheditemid"], ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
					POarray["custparam_recordtype"] = itemSubtype.recordType;
					var batchflag=itemSubtype.custitem_ebizbatchlot;
					nlapiLogExecution('DEBUG', 'itemSubtype.recordType =', itemSubtype.recordType);
					nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
					nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);
					//Case # 20127203 Start
					var priorityPutawayLocnArr = priorityPutaway(getFetchedItemId, getPOQtyEntered,poLoc,getPOLineItemStatus);
					//Case # 20127203 End
					var priorityRemainingQty = priorityPutawayLocnArr[0];
					var priorityQty = priorityPutawayLocnArr[1];
					var priorityLocnID = priorityPutawayLocnArr[2];

					nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityRemainingQty', priorityRemainingQty);
					nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityQty', priorityQty);
					nlapiLogExecution('DEBUG', 'checkInPOSTRequest:POQtyEntered', getPOQtyEntered);
					nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityLocnID', priorityLocnID);

					var pickfaceEnteredOption = request.getParameter('hdnPickfaceEnteredOption');
					var putmethod,putrule;

					if(priorityQty < getPOQtyEntered){			//(priorityRemainingQty != 0){
//						getBeginLocation = GetPutawayLocation(getFetchedItemId, getPOLinePackCode, getPOLineItemStatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType);
						getBeginLocation = generatePutawayLocation(getFetchedItemId, getPOLinePackCode, getPOLineItemStatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType,poLoc);

						//					nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocationArray[1]);
						nlapiLogExecution('DEBUG', 'Begin Location', getBeginLocation);

						//					var getBeginLocation = BeginLocationArray[1];

						var vLocationname="";
						if (getBeginLocation != null && getBeginLocation != '') {
							var getBeginLocationId = getBeginLocation[2];
							vLocationname= getBeginLocation[0];
							nlapiLogExecution('DEBUG', 'Begin Location Id', getBeginLocationId);
						}
						else {
							var getBeginLocationId = "";
							nlapiLogExecution('DEBUG', 'Begin Location Id is null', getBeginLocationId);
						}

						

						if(getBeginLocation!=null && getBeginLocation != '')
							remainingCube = getBeginLocation[1];

						nlapiLogExecution('DEBUG', 'remainingCube', remainingCube);
						if (getBeginLocation != null && getBeginLocation != '') {
						putmethod = getBeginLocation[9];
						putrule = getBeginLocation[10];
						}
						else
						{
							putmethod = "";
							putrule = "";
						}
					}

					var getActualEndDate = DateStamp();
					nlapiLogExecution('DEBUG', 'getActualEndDate', getActualEndDate);
					var getActualEndTime = TimeStamp();
					nlapiLogExecution('DEBUG', 'getActualEndTime', getActualEndTime);

					if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T') {
						nlapiLogExecution('DEBUG', 'serializedinventoryitem', '');
						var dimfilters = new Array();
						dimfilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', POarray["custparam_fetcheditemid"]);
						dimfilters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');
						var dimcolumn = new Array();
						dimcolumn[0] = new nlobjSearchColumn('custrecord_ebizuomskudim');
						dimcolumn[1] = new nlobjSearchColumn('custrecord_ebizqty');
						var dimsearchresult = nlapiSearchRecord('customrecord_ebiznet_skudims', null, dimfilters, dimcolumn);

						if (dimsearchresult != null && dimsearchresult.length > 0) {
							POarray["custparam_uomid"] = dimsearchresult[0].getValue('custrecord_ebizuomskudim');
							POarray["custparam_uomidtext"] = dimsearchresult[0].getText('custrecord_ebizuomskudim');
							POarray["custparam_uomqty"] = dimsearchresult[0].getText('custrecord_ebizqty');
							nlapiLogExecution('DEBUG', 'POarray["custparam_uomid"]', POarray["custparam_uomid"]);
						}
						POarray["custparam_locid"] = getBeginLocationId;
						// POarray["custparam_location"] = getBeginLocation;
						POarray["custparam_location"] = vLocationname;
						POarray["custparam_number"] = 0;
						nlapiLogExecution('DEBUG', 'getBeginLocationId', getBeginLocationId);
						nlapiLogExecution('DEBUG', 'POarray["custparam_number"]', POarray["custparam_number"]);
						nlapiLogExecution('DEBUG', 'POarray.length', POarray.length);

						//response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no', false, POarray);
						response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no_di', false, POarray);

						//response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no', false, null);						
						nlapiLogExecution('DEBUG', 'Towards Serial No. Screen');
						return;
						nlapiLogExecution('DEBUG', 'This is after return statement');
					}
					nlapiLogExecution('DEBUG', 'This is before executing PUTW record creation');

					/*
					 * This is to insert a record in Transaction Line Details custom record
					 * The purpose of this is to identify the quantity checked-in, location generated for and putaway confirmation
					 * */                    
					var trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');
					TrnLineUpdation(trantype, 'CHKN', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
							POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
							POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"",getPOLineItemStatus);

					/*
					 * If the received quantity has to be putaway to a pickface location based on priority putaway flag, 
					 * 	create PUTW task in TRN_OPENTASK 
					 * 		set taskType = 2(PUTW) and wmsStatusFlag = 2(INBOUND/LOCATIONS ASSIGNED)
					 * 		beginLocation = priorityLocnID and endLocn = ""
					 */	

					if (priorityQty >= getPOQtyEntered){			//(priorityQty != 0){
						taskType = "2"; // PUTW
						wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
						getBeginLocationId = priorityLocnID;
						var LocRemCube = GeteLocCube(getBeginLocationId);
						
						nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
						if (parseFloat(LocRemCube) > parseFloat(TotalItemCube)) 
							remainingCube = parseFloat(LocRemCube) - parseFloat(TotalItemCube);
//						endLocn = "";
						//var getItemLP = GetMaxLPNo(1, 1);
					}
					nlapiLogExecution('DEBUG', 'Before custChknPutwRecCreation calling (getBeginLocationId)', getBeginLocationId);
					nlapiLogExecution('DEBUG', 'getPOInternalId', getPOInternalId);
					nlapiLogExecution('DEBUG', 'getPONo', getPONo);
					nlapiLogExecution('DEBUG', 'getPOQtyEntered', getPOQtyEntered);
					nlapiLogExecution('DEBUG', 'getPOLineNo', getPOLineNo);
					nlapiLogExecution('DEBUG', 'getPOItemRemainingQty', getPOItemRemainingQty);
					nlapiLogExecution('DEBUG', 'getLineCount', getLineCount);
					nlapiLogExecution('DEBUG', 'getFetchedItemId', getFetchedItemId);
					nlapiLogExecution('DEBUG', 'getPOItem', getPOItem);
					nlapiLogExecution('DEBUG', 'getItemDescription', getItemDescription);
					nlapiLogExecution('DEBUG', 'getPOLineItemStatus', getPOLineItemStatus);
					nlapiLogExecution('DEBUG', 'getPOLinePackCode', getPOLinePackCode);
					nlapiLogExecution('DEBUG', 'getBaseUOM', getBaseUOM);
					//nlapiLogExecution('DEBUG', 'getItemLP', getItemLP);
					nlapiLogExecution('DEBUG', 'getBinLocation', getBinLocation);
					nlapiLogExecution('DEBUG', 'getPOLineQuantityReceived', getPOLineQuantityReceived);
					nlapiLogExecution('DEBUG', 'getActualEndDate', getActualEndDate);
					nlapiLogExecution('DEBUG', 'getActualEndTime', getActualEndTime);
					nlapiLogExecution('DEBUG', 'getPOReceiptNo', getPOReceiptNo);
					nlapiLogExecution('DEBUG', 'getActualBeginDate', getActualBeginDate);
					nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
					nlapiLogExecution('DEBUG', 'getActualBeginTimeAMPM', getActualBeginTimeAMPM);
					nlapiLogExecution('DEBUG', 'getBeginLocationId', getBeginLocationId);
					nlapiLogExecution('DEBUG', 'POarray["custparam_batchno"]', POarray["custparam_batchno"]);
					nlapiLogExecution('DEBUG', 'itemSubtype.recordType', itemSubtype.recordType);
					nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizbatchlot', itemSubtype.custitem_ebizbatchlot);
					nlapiLogExecution('DEBUG', 'batchflag', batchflag);
					nlapiLogExecution('DEBUG', 'getItemCartLP', getItemCartLP);
					nlapiLogExecution('DEBUG', 'POarray["custparam_whlocation"]', POarray["custparam_whlocation"]);
					nlapiLogExecution('DEBUG', 'batchflag', batchflag);
					nlapiLogExecution('DEBUG', 'putmethod', putmethod);
					nlapiLogExecution('DEBUG', 'putrule', putrule);
					
					var vValid=custChknPutwRecCreation(getPOInternalId, getPONo, getPOQtyEntered, getPOLineNo, getPOItemRemainingQty, getLineCount, getFetchedItemId, 
							getPOItem, getItemDescription, getPOLineItemStatus, getPOLinePackCode, getBaseUOM, getsysItemLP, getBinLocation, 
							getPOLineQuantityReceived, getActualEndDate, getActualEndTime, getPOReceiptNo, "", getActualBeginDate, 
							getActualBeginTime, getActualBeginTimeAMPM, getBeginLocationId, POarray["custparam_batchno"], 
							POarray["custparam_mfgdate"], POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
							POarray["custparam_lastdate"], POarray["custparam_fifodate"], POarray["custparam_fifocode"], 
							itemSubtype.recordType, getItemCartLP, POarray["custparam_whlocation"],batchflag,putmethod,putrule);
					nlapiLogExecution('DEBUG', 'vValid', vValid);
					if(vValid==false)
					{
						response.sendRedirect('SUITELET', 'customscript_rf_checkin_cnf', 'customdeploy_rf_checkin_cng_di', false, POarray);
					}
					else
					{

						/*
						 * This is to insert a record in inventory custom record.
						 * The data that is to be inserted is PO Internal Id, Item, Item Status, Pack Code, Quantity, LP#
						 * The parameters list is: 
						 * pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, warehouse location
						 * 
						 */                    
						

						TrnLineUpdation(trantype, 'ASPW', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
								POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
								POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"");
						if(getBinLocation!=null && getBinLocation!='')
						{
							UpdateLocCube(getBeginLocationId, remainingCube);
						}

						if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == "lotnumberedassemblyitem" ||itemSubtype.recordType == "assemblyitem" || itemSubtype.custitem_ebizbatchlot == 'T') {
							//checks for Batch/Lot exists in Batch Entry, Update if exists if not insert
							try {
								nlapiLogExecution('DEBUG', 'in');
								var filtersbat = new Array();
								filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', POarray["custparam_batchno"]);
								if(getFetchedItemId!=null&&getFetchedItemId!="")
									filtersbat[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', getFetchedItemId);

								var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);
								nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
								if (SrchRecord) {

									nlapiLogExecution('DEBUG', ' BATCH FOUND');
									var transaction = nlapiLoadRecord('customrecord_ebiznet_batch_entry', SrchRecord[0].getId());
									transaction.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
									transaction.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
									transaction.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
									transaction.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
									transaction.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
									transaction.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
									if(ScannedBatch != null && ScannedBatch != '')
										transaction.setFieldValue('custrecord_ebiz_manufacturelot', ScannedBatch);
									var rec = nlapiSubmitRecord(transaction, false, true);

								}
								else {
									nlapiLogExecution('DEBUG', 'BATCH NOT FOUND');
									var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

									customrecord.setFieldValue('name', POarray["custparam_batchno"]);
									customrecord.setFieldValue('custrecord_ebizlotbatch', POarray["custparam_batchno"]);
									customrecord.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
									customrecord.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
									customrecord.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
									customrecord.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
									customrecord.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
									customrecord.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
									nlapiLogExecution('DEBUG', '1',POarray["custparam_fifodate"]);
									nlapiLogExecution('DEBUG', 'Item Id',getFetchedItemId);
									//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
									customrecord.setFieldValue('custrecord_ebizsku', getFetchedItemId);
									customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);
									//customrecord.setFieldValue('custrecord_ebizsitebatch',POarray["custparam_whlocation"]);
									if(ScannedBatch != null && ScannedBatch != '')
									{
										customrecord.setFieldValue('custrecord_ebiz_manufacturelot',ScannedBatch);													
									}
									
									var rec = nlapiSubmitRecord(customrecord, false, true);
								}
							} 
							catch (e) {
								nlapiLogExecution('DEBUG', 'Failed to Update/Insert into BATCH ENTRY Record');
							}
						}
						rf_checkin(getPOInternalId, getFetchedItemId, getItemDescription, getPOLineItemStatus, 
								getPOLinePackCode, getPOQtyEntered, getsysItemLP, POarray["custparam_whlocation"],
								POarray["custparam_batchno"], POarray["custparam_mfgdate"], 
								POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
								POarray["custparam_lastdate"], POarray["custparam_fifodate"], 
								POarray["custparam_fifocode"]);
						POarray["custparam_trantype"]=trantype;
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, POarray);
					}
				}
				else {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Entered Item LP', getsysItemLP);
				}
			}
			else {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Entered Item LP', getsysItemLP);
			}
		//}
	nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

}


function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, 
		ItemDesc, ItemStatus, PackCode, BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, 
		poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, ActualBeginTimeAMPM, BeginLocationId, 
		BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, itemrectype, cartlp, 
		WHLocation,batchflag,putmethod,putrule)
{
	var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
	var columns = nlapiLookupField('item', ItemId, fields);
	var ItemType = columns.recordType;					
	var batchflg = columns.custitem_ebizbatchlot;
	var itemfamId= columns.custitem_item_family;
	var itemgrpId= columns.custitem_item_group;
	var getlotnoid="";
	
//	if(ItemStatus==null || ItemStatus=='')
//		ItemStatus='12';
	
//	nlapiLogExecution('DEBUG', 'putLotBatch', putLotBatch);
	
	var vValid=true;
	nlapiLogExecution('DEBUG', 'Into custChknPutwRecCreation', 'custChknPutwRecCreation');
	var now = new Date();
	var stagelocid, docklocid;

	stagelocid = getStagingLocation();
	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);

	docklocid = getDockLocation();
	nlapiLogExecution('DEBUG', 'Dock Location', docklocid);

	if (BeginLocationId != null && BeginLocationId != '' ) //Creating custom record with CHKN and PUTW task,
	{
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', quan);
		customrecord.setFieldValue('custrecord_expe_qty', quan);
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
	//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);
		nlapiLogExecution('DEBUG', 'Submitting CHKN record', 'TRN_OPENTASK');

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		if(po!=null && po!="")
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);
		
		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		nlapiLogExecution('DEBUG', 'BeginLocationId', BeginLocationId);

		//create the openTask record With PUTW Task Type
		if(BeginLocationId==null || BeginLocationId=="")
			return false;
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', quan);
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)

		nlapiLogExecution('DEBUG', 'Fetched Begin Location', BeginLocation);

		putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//For eBiznet Receipt Number .
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//putwrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		if (BeginLocationId != "") {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 2);
		}
		else {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		}
		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
		putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
		putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		if(po!=null && po!="")
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);
		
		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);
		
		try
		{
			MoveTaskRecord(recid);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in  MoveTaskRecord',exp);
		}
		
		
		nlapiLogExecution('DEBUG', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');
		return true;
	}//end if binloc is not null
	else {
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);

		nlapiLogExecution('DEBUG', 'custrecord_ebiz_sku_no tesing', 'ItemId');

		customrecord.setFieldValue('custrecord_act_qty', quan);
		customrecord.setFieldValue('custrecord_expe_qty', quan);
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
	//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{
		
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		if(po!=null && po!="")
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);
		
		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', quan);
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" ||itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");

		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
		putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
		putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);
//20125026
		if(po!=null && po!="")
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);
		
		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		return false;
	}
}











function getStagingLocation(){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}
function getDockLocation(){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}

function rf_checkin(pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, whLocation,
		batchno, mfgdate, expdate, bestbeforedate, lastdate, fifodate, fifocode){
	nlapiLogExecution('DEBUG', 'inside rf_checkin', 'Success');

	nlapiLogExecution('DEBUG', 'whLocation', whLocation);
	nlapiLogExecution('DEBUG', 'PO Internal ID', pointid);

	if(whLocation==null || whLocation=="")
	{
		whLocation = nlapiLookupField('purchaseorder', pointid, 'location');

		nlapiLogExecution('DEBUG', 'whLocation (from lookup)', whLocation);
	}

	/*
	 * Get the stage location. This will be the bin location to which the item will be 
	 * placed at the time of check-in. The same data is to be inserted in inventory record after it is
	 * done in opentask record.
	 */
	var stagelocid;

	stagelocid = getStagingLocation();
	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);

	/*
	 * To create inventory record.
	 */
	var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

	nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');

	var filtersAccount = new Array();
	if(whLocation!=null && whLocation!=""){
		filtersAccount.push(new nlobjSearchFilter('custrecord_location', null, 'anyof', whLocation));
	}

	var accountNumber = "";
//	var columnsAccount = new Array();
//	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

//	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);
//	if(accountSearchResults!= null && accountSearchResults.length > 0)
//	accountNumber = accountSearchResults[0].getValue('custrecord_accountno');
//	if(accountSearchResults!= null && accountSearchResults.length > 0)
//	{

	//nlapiLogExecution('DEBUG', 'Account # incondition',accountNumber);

	invtRec.setFieldValue('name', pointid);
	invtRec.setFieldValue('custrecord_ebiz_inv_binloc', stagelocid);
	invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
	invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
	invtRec.setFieldValue('custrecord_ebiz_inv_qty', quantity);
	invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
	invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
	//invtRec.setFieldValue('custrecord_ebiz_qoh', quantity);
	invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
//	invtRec.setFieldValue('custrecord_wms_inv_status_flag','1');
	invtRec.setFieldValue('custrecord_wms_inv_status_flag','17');//17=FLAG.INVENTORY.INBOUND
	invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
	invtRec.setFieldValue('custrecord_ebiz_qoh', quantity);
	invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
	invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
	invtRec.setFieldValue('custrecord_invttasktype', 1);

	nlapiLogExecution('DEBUG', 'batchno', batchno);

	if(batchno!=null && batchno!='')
	{
		try
		{
			//Added on 18/06/12 by suman.
			//since custrecord_ebiz_in_lot is a list need to pass InternalId.
			var filterBatch=new Array();
			filterBatch[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batchno);
			if(itemid!=null&&itemid!="")
				filterBatch[1]=new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid);
			var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatch,null);
			var BatchID=rec[0].getId();
			//end of code additon as on 18/06/12.
//			invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
			invtRec.setFieldValue('custrecord_ebiz_inv_lot', BatchID);
			invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception', exp);
		}
	}

	var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
//	}
//	else
//	{
//	response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, "Invalid Account #");
//	//nlapiLogExecution('DEBUG', 'Entered Item LP', getItemLP);
//	}
	nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtRecordId);
	if (invtRecordId != null) {
		nlapiLogExecution('DEBUG', 'Inventory record creation is successful', invtRecordId);
	}
	else {
		nlapiLogExecution('DEBUG', 'Inventory record creation is failure', 'Fail');
	}
} 







