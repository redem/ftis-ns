/***************************************************************************
	  	eBizNET Solutions
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/UserEvents/ebiz_AfterTransferInventory_UE.js,v $
 *     	   $Revision: 1.5.2.8.4.1.4.2.4.1 $
 *     	   $Date: 2015/09/21 14:02:05 $
 *     	   $Author: deepshikha $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_AfterTransferInventory_UE.js,v $
 * Revision 1.5.2.8.4.1.4.2.4.1  2015/09/21 14:02:05  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.5.2.8.4.1.4.2  2014/03/03 07:19:02  snimmakayala
 * 20127286
 *
 * Revision 1.5.2.8.4.1.4.1  2013/03/01 14:35:04  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.5.2.8.4.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.5.2.8  2012/09/03 13:57:09  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.5.2.7  2012/04/30 11:41:12  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.5.2.6  2012/04/11 12:16:47  gkalla
 * CASE201112/CR201113/LOG201121
 * True lot functionality
 *
 * Revision 1.5.2.5  2012/03/19 06:05:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * fulfillment for transfer order
 *
 * Revision 1.5.2.4  2012/03/02 14:26:44  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * cluster pick report
 *
 * Revision 1.5.2.3  2012/02/23 09:31:41  spendyala
 * CASE201112/CR201113/LOG20112
 * Issue related to TnT is fixed.
 *
 * Revision 1.5.2.2  2012/02/09 10:42:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.5  2012/01/21 00:45:53  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2012/01/20 15:29:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.3  2012/01/19 15:56:21  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.2  2012/01/18 00:24:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * User Event for Transfer Inventory
 *
 * Revision 1.1  2012/01/16 23:58:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * User Event for Transfer Inventory
 *

 ********************************************************************************


/**
 * It triggers after page is submitted 
 * @param type
 * @param form
 * @param request
 */
function AfterTransferInventory(type, form, request)
{
	try{
		var TransferInvRecId=nlapiGetRecordId();
		nlapiLogExecution('ERROR','TransferInvRecId',TransferInvRecId);
		if(TransferInvRecId==null||TransferInvRecId==""){
			var fromlocation = nlapiGetFieldValue('location');
			var tolocation = nlapiGetFieldValue('transferlocation');

			nlapiLogExecution('ERROR','fromlocation',fromlocation);
			nlapiLogExecution('ERROR','tolocation',tolocation);

			var frommwhsiteflag='F';
			var tomwhsiteflag='F';	
			var fromlocstatus;
			var tolocstatus;

			if(fromlocation!=null && fromlocation!='')
			{
				var fields = ['custrecord_ebizwhsite'];

				var fromlocationcolumns = nlapiLookupField('Location', fromlocation, fields);
				if(fromlocationcolumns!=null)
					frommwhsiteflag = fromlocationcolumns.custrecord_ebizwhsite;
			}

			if(tolocation!=null && tolocation!='')
			{
				var fields = ['custrecord_ebizwhsite'];

				var tolocationcolumns = nlapiLookupField('Location', tolocation, fields);
				if(tolocationcolumns!=null)
					tomwhsiteflag = tolocationcolumns.custrecord_ebizwhsite;
			}


			nlapiLogExecution('ERROR','from mwhsite flag',frommwhsiteflag);
			nlapiLogExecution('ERROR','to mwhsite flag',tomwhsiteflag);

			if(frommwhsiteflag=='F' && tomwhsiteflag=='T')
			{
				var vitem = '';
				var vtransferqty = '';
				var cube=0.01;
				var BaseUOMQty=0.01;
				var Loctdims = new Array();
				var itemCube=0;
				var LocName = "";
				var LocId = "";
				var RemCube = "";
				var InbLocId = "";
				var OubLocId = "";
				var PickSeq = "";
				var PutSeq = "";
				var putmethod="";
				var putstrategy="";
				var LotNo="";
				var itemdesc='';
				nlapiLogExecution('ERROR','nlapiGetLineItemCount(inventory)',nlapiGetLineItemCount('inventory'));
				for(var i =0; i < nlapiGetLineItemCount('inventory'); i++)
				{
					nlapiLogExecution('ERROR','count',i+1);
					vitem = nlapiGetLineItemValue('inventory', 'item', i + 1);
					vtransferqty = nlapiGetLineItemValue('inventory', 'adjustqtyby', i + 1);
					LotNo=nlapiGetLineItemValue('inventory', 'serialnumbers', i + 1);
					itemdesc=nlapiGetLineItemText('inventory', 'item', i + 1);
					nlapiLogExecution('ERROR','vitem',vitem);
					nlapiLogExecution('ERROR','vtransferqty',vtransferqty);
					nlapiLogExecution('ERROR','LotNo',LotNo);			

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', vitem));            
					filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					filters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@',tolocation]));

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
					columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

					var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

					if(skuDimsSearchResults != null && skuDimsSearchResults != ''){
						for (var j = 0; j < skuDimsSearchResults.length; j++) {
							var skuDim = skuDimsSearchResults[j];
							cube = skuDim.getValue('custrecord_ebizcube');
							BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
						}
					}

					if (!isNaN(cube)) 
					{
						nlapiLogExecution('ERROR', 'vtransferqty', vtransferqty);
						nlapiLogExecution('ERROR', 'BaseUOMQty', BaseUOMQty);
						var uomqty = ((parseFloat(vtransferqty))/(parseFloat(BaseUOMQty)));	
						nlapiLogExecution('ERROR', 'uomqty', uomqty);
						nlapiLogExecution('ERROR', 'cube', cube);
						itemCube = (parseFloat(uomqty) * parseFloat(cube));
						nlapiLogExecution('ERROR', 'ItemCube', itemCube);
					}

					Loctdims = GetPutawayLocation(vitem, null, null, null, itemCube,null,tolocation);

					if (Loctdims.length > 0) 
					{
						if (Loctdims[0] != "") 
						{
							LocName = Loctdims[0];
							nlapiLogExecution('ERROR', 'LocName', LocName);
						}
						else 
						{
							LocName = "";
							nlapiLogExecution('ERROR', 'in else LocName', LocName);
						}
						if (!isNaN(Loctdims[1])) 
						{
							RemCube = Loctdims[1];
						}
						else 
						{
							RemCube = 0;
						}
						if (!isNaN(Loctdims[2])) 
						{
							LocId = Loctdims[2];
							nlapiLogExecution('ERROR', 'LocId', LocId);
						}
						else 
						{
							LocId = "";
							nlapiLogExecution('ERROR', 'in else LocId', LocId);
						}
						if (!isNaN(Loctdims[3])) 
						{
							OubLocId = Loctdims[3];
						}
						else 
						{
							OubLocId = "";
						}
						if (!isNaN(Loctdims[4])) 
						{
							PickSeq = Loctdims[4];
						}
						else 
						{
							PickSeq = "";
						}
						if (!isNaN(Loctdims[5])) 
						{
							InbLocId = Loctdims[5];
						}
						else 
						{
							InbLocId = "";
						}
						if (!isNaN(Loctdims[6])) {
							PutSeq = Loctdims[6];
						}
						else {
							PutSeq = "";
						}
						if (!isNaN(Loctdims[7])) {
							LocRemCube = Loctdims[7];
						}
						else {
							LocRemCube = "";
						}

						putmethod=Loctdims[9];
						putstrategy=Loctdims[10];
					}
					else 
					{
						nlapiLogExecution('ERROR', 'If Loc Length is Null', 'Making Values Null');
						LocName = "";
						LocId = "";
						RemCube = "";
						InbLocId = "";
						OubLocId = "";
						PickSeq = "";
						PutSeq = "";
						putmethod="";
						putstrategy="";
					}

					if (LocId != null && LocId != "") 
					{
						var MultipleLP = GetMultipleLP(1,1,1,tolocation);				
						var maxLPPrefix = MultipleLP.split(",")[0];
						var maxLP = MultipleLP.split(",")[1];;
						maxLP = parseFloat(maxLP) - parseFloat(1); 
						maxLP = parseFloat(maxLP) + 1;
						var LP = maxLPPrefix + maxLP.toString();

						nlapiLogExecution('ERROR', 'MAX LP:', maxLP);
						nlapiLogExecution('ERROR', 'Pallet LP', LP);

						var itemStatus='';
						var defpackcode='';
						var fromitemstatus='';
						var toitemstatus='';

						var fields = ['custitem_ebizdefskustatus','custitem_ebizdefpackcode'];
						var columns= nlapiLookupField('item',vitem,fields);
						if(columns!=null)
						{
							itemStatus = columns.custitem_ebizdefskustatus;
							defpackcode = columns.custitem_ebizdefpackcode;
						}

						var itemStatusFilters = new Array();
						itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
						itemStatusFilters[1] = new nlobjSearchFilter('custrecord_item_status_location_map',null, 'is',fromlocation);

						var itemStatusColumns = new Array();						
						itemStatusColumns[0] = new nlobjSearchColumn('internalid');
						itemStatusColumns[0].setSort();						

						var fromStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);

						if(fromStatusSearchResult!=null && fromStatusSearchResult!='')
						{
							fromitemstatus=fromStatusSearchResult[0].getValue('internalid');
						}

						var toitemStatusFilters = new Array();
						toitemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
						toitemStatusFilters[1] = new nlobjSearchFilter('custrecord_item_status_location_map',null, 'is',tolocation);

						var toitemStatusColumns = new Array();						
						toitemStatusColumns[0] = new nlobjSearchColumn('internalid');
						toitemStatusColumns[0].setSort();						

						var toStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, toitemStatusFilters, toitemStatusColumns);
						if(toStatusSearchResult!=null && toStatusSearchResult!='')
						{
							toitemstatus=toStatusSearchResult[0].getValue('internalid');
						}

						createInvtRecord(LocId,LocName, LP, vitem, itemStatus, vtransferqty, tolocation, defpackcode,LotNo,itemdesc,fromitemstatus,toitemstatus);

						if(RemCube<0)
							RemCube=0;

						var retValue = UpdateLocCube(LocId, parseFloat(RemCube));

						nlapiLogExecution('ERROR', 'UpdateLocCube', retValue);			
					}
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception',exp);
	}
}


function createInvtRecord(binlocation,binlocationname, lp, itemId, itemStatus, qty, location, packCode,LotNo,itemdesc,fromitemstatus,toitemstatus) {

	try {
		// Creating Inventory Record.

		nlapiLogExecution('ERROR', 'binlocation', binlocation);
		nlapiLogExecution('ERROR', 'itemStatus', itemStatus);
		nlapiLogExecution('ERROR', 'itemdesc', itemdesc);
		nlapiLogExecution('ERROR', 'location', location);
		if(location=='13')
		{
			itemStatus='8';
		}
		var filtersbat = new Array();
		filtersbat.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', LotNo));

		//Code changed on 230212 by suman
		//Previously instead of location LotNo is passed.
		if(location!=null&&location!="")
			filtersbat.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof',['@NONE@',location]));		
		//End of Code Changed as of 230212.

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);
		nlapiLogExecution('ERROR', 'SrchRecord', SrchRecord);
		if (SrchRecord==null) {

			nlapiLogExecution('ERROR', 'BATCH NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

			customrecord.setFieldValue('name', LotNo);
			customrecord.setFieldValue('custrecord_ebizlotbatch', LotNo);		
			customrecord.setFieldValue('custrecord_ebizsku', itemId);				
			customrecord.setFieldValue('custrecord_ebizsitebatch',location);
			customrecord.setFieldValue('custrecord_ebizskustatus',itemStatus);			

			var rec = nlapiSubmitRecord(customrecord, false, true);
		}

		var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');


		invtRec.setFieldValue('name', binlocationname);
		invtRec.setFieldValue('custrecord_ebiz_inv_binloc', binlocation);
		invtRec.setFieldValue('custrecord_ebiz_inv_lp', lp);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemId);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
		invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packCode);
		invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(qty).toFixed(5));
		invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemId);
		invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(qty).toFixed(5));
		invtRec.setFieldValue('custrecord_ebiz_inv_loc', location);
		invtRec.setFieldValue('custrecord_invttasktype', 10); // INVT..
		invtRec.setFieldValue('custrecord_wms_inv_status_flag', 19); //19=FLAG.INVENTORY.STORAGE
		invtRec.setFieldValue('customrecord_ebiznet_location', binlocation); 
		invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
		invtRec.setFieldText('custrecord_ebiz_inv_lot', LotNo);

		var invtRecordId = nlapiSubmitRecord(invtRec, false, true);

		if (invtRecordId != null) {
			nlapiLogExecution('ERROR', 'Inventory Record Creation Success', invtRecordId);
		} else {
			nlapiLogExecution('ERROR', 'Inventory Record Creation', 'Failed');
		}

		Opentaskcreation(binlocation,binlocationname, lp, itemId, itemStatus, qty, location, packCode,LotNo,itemdesc,fromitemstatus,toitemstatus);
		if(itemdesc!=LotNo)
		{
			InvokeNSInventoryAdjustmentNew(itemId,itemStatus,location,qty,itemdesc);
			InvokeNSInventoryAdjustmentNew(itemId,itemStatus,location,-(parseFloat(qty)),LotNo);

		}

	} 
	catch (error) {
		nlapiLogExecution('ERROR', 'Exception in create invt', error);
	}
}

function Opentaskcreation(binlocation,binlocationname, lp, itemId, itemStatus, qty, location, packCode,LotNo,itemdesc,fromitemstatus,toitemstatus)
{
	var now = new Date();
	//now= now.getHours();
	//Getting time in hh:mm tt format.
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	var opentaskrec = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	nlapiLogExecution('Error', 'binlocation', binlocation);
	nlapiLogExecution('Error', 'binlocationname', binlocationname);
	nlapiLogExecution('Error', 'lp', lp);
	nlapiLogExecution('Error', 'itemId', itemId);
	nlapiLogExecution('Error', 'itemStatus', itemStatus);
	nlapiLogExecution('Error', 'qty', qty);
	nlapiLogExecution('Error', 'location', location);
	nlapiLogExecution('Error', 'packCode', packCode);
	//nlapiLogExecution('Error', 'InvAdjType :', InvAdjType);
	nlapiLogExecution('Error', 'LotNo', LotNo);
	nlapiLogExecution('Error', 'itemdesc', itemdesc);


	//Added for Item id.
	opentaskrec.setFieldValue('name', binlocationname);
	opentaskrec.setFieldValue('custrecord_ebiz_sku_no', itemId);
	opentaskrec.setFieldValue('custrecord_act_qty', parseFloat(qty).toFixed(5));//Assigning new quantity
	opentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(qty).toFixed(5));//Assigning new quantity


	opentaskrec.setFieldValue('custrecord_lpno', lp);
	opentaskrec.setFieldValue('custrecord_ebiz_lpno', lp);


	opentaskrec.setFieldValue('custrecord_tasktype', '18');

	opentaskrec.setFieldValue('custrecord_actbeginloc', binlocation);
	opentaskrec.setFieldValue('custrecord_actendloc', binlocation);

	//Added for Item name and desc
	opentaskrec.setFieldValue('custrecord_sku', itemId);
	//opentaskrec.setFieldValue('custrecord_skudesc', ItemName);

	opentaskrec.setFieldValue('custrecordact_begin_date', DateStamp());
	opentaskrec.setFieldValue('custrecord_act_end_date', DateStamp());

	//Adding fields to update time zones.
	opentaskrec.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	opentaskrec.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	opentaskrec.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	opentaskrec.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	opentaskrec.setFieldValue('custrecord_current_date', DateStamp());
	opentaskrec.setFieldValue('custrecord_upd_date', DateStamp());
	//Status flag .
	opentaskrec.setFieldValue('custrecord_wms_status_flag', 19);
	opentaskrec.setFieldValue('custrecord_batch_no', LotNo);
	opentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', nlapiGetContext().getUser());
	opentaskrec.setFieldValue('custrecord_wms_location', location);

	opentaskrec.setFieldValue('custrecord_sku_status', fromitemstatus);
	opentaskrec.setFieldValue('custrecord_ebiz_new_sku_status', toitemstatus);

	opentaskrec.setFieldValue('custrecord_packcode', packCode);

	opentaskrec.setFieldValue('custrecord_from_lp_no', lp);
	opentaskrec.setFieldValue('custrecord_ebiz_new_lp', lp);
	opentaskrec.setFieldValue('custrecord_lotnowithquantity', LotNo + "(" + qty + ")");



	nlapiLogExecution('Error', 'Submitting MOVE record', 'TRN_OPENTASK');

	//commit the record to NetSuite
	var recid = nlapiSubmitRecord(opentaskrec, false, true);
}

function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,lot)
{
	try{		

		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(loc);

		var vItemname;

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null && itemdetails !='' && itemdetails.length>0) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
		}	


		var Taskfilters = new Array();
		var tasktyppe;
		Taskfilters.push(new nlobjSearchFilter('name', null, 'is', 'ADJT'));
		var Taskvalue = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, Taskfilters, null);	
		if(Taskvalue!=null && Taskvalue!='')
		{
			tasktyppe=	Taskvalue[0].getId();
		}
		var Adjfilters = new Array();
		var Adjcolumns = new Array();
		Adjfilters.push(new nlobjSearchFilter('custrecord_adjusttasktype', null, 'anyof', tasktyppe));
		var adjustvalue = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, Adjfilters, null);		
		if(adjustvalue!=null)
			var adjustmenttype=adjustvalue[0].getId();

		var AccountNo = getStockAdjustmentAccountNoNew(adjustmenttype);
		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', AccountNo);
		outAdj.setFieldValue('adjlocation', loc);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, loc);	
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
		if(vAvgCost != null &&  vAvgCost != "")	
		{
			nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vAvgCost);
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vCost);
		}
		//outAdj.setFieldValue('subsidiary', 4);

		//Added by Sudheer on 093011 to send lot# for inventory posting

		if(lot!=null && lot!='')
		{
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);

			/*if(confirmLotToNS=='N')
				lot=vItemname;	*/	

			nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
			outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
		}

		nlapiSubmitRecord(outAdj, true, true);
		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryAdjustmentNew ', exp);	

	}
}

/**To get confirm LOT toNS based on system rule
 * 
 * @param Site
 * @returns Y or N
 */
function GetConfirmLotToNS(Site)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'WMSLOT');
		filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('ERROR', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}

function getStockAdjustmentAccountNoNew(adjusttype)
{	
	nlapiLogExecution('ERROR', 'adjusttype', adjusttype);
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();

	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	//colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	if(StockAdjustAccResults!=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		//StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
	}

	return StAdjustmentAccountNo;	
}