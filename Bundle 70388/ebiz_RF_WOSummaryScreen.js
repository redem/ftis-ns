/***************************************************************************
   eBizNET Solutions Inc             
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WOSummaryScreen.js,v $
 *     	   $Revision: 1.1.2.1.4.5.2.1 $
 *     	   $Date: 2014/08/18 12:28:25 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 ** REVISION HISTORY
 * $Log: ebiz_RF_WOSummaryScreen.js,v $
 * Revision 1.1.2.1.4.5.2.1  2014/08/18 12:28:25  snimmakayala
 * Case: 20149972 & 20149973
 * JAWBONE WO RF PICKING ISSUES
 *
 * Revision 1.1.2.1.4.5  2014/06/06 07:41:51  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.1.4.4  2014/05/30 00:34:25  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.4.3  2014/02/03 16:02:33  sponnaganti
 * case# 20127004
 *  (now form name passed correctly to work keyboard enter button)
 *
 * Revision 1.1.2.1.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.8.4.3.4.9.2.1  2013/02/26 13:02:23  snimmakayala

 *
 * 
 *****************************************************************************/
function WOSummaryScreen(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		nlapiLogExecution('DEBUG', 'Into Request', 'Into Request');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8;

		if( getLanguage == 'es_ES')
		{
			st1 = "NO. DE PEDIDOS";
			st2 = "NO. De SELECCIONES:";
			st3 = "NO. LUGARES DE SELECCI&#211;N:";
			st4 = "NO. DE CAJAS:";
			st5 = "CUBE TOTAL:";
			st6 = "PESO TOTAL:";
			st7 = "ENVIAR";
			st8 = "ANTERIOR";


		}
		else
		{
			st1 = "NO. OF ORDERS ";
			st2 = "NO. OF PICKS:";
			st3 = "NO. OF PICK LOCATIONS:";
			st4 = "NO. OF CARTONS:";
			st5 = "TOTAL CUBE :";
			st6 = "TOTAL WEIGHT:";
			st7 = "SEND";
			st8 = "PREV";

		}

		var noofOrders=0;
		var noofPicks=0;
		var noofLocations=0;
		var noofCartons =0;
		var orderArray=new Array();
		var PickArray=new Array();
		var LocArray=new Array();
		var CartonArray=new Array();
		var CartonSizeArray=new Array();
		var totcube=0;
		var totwgt=0;
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getwoid = request.getParameter('custparam_woid');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginlocationname');
		var vBatchNo = request.getParameter('custparam_batchno');	
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var ItemStatus=request.getParameter('custparam_itemstatus');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var OrdNo=request.getParameter('name');
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');

		if(getFetchedLocation==null || getFetchedLocation == "")
		{
			getFetchedLocation=NextLocation;
		}

		if(getItemInternalId==null || getItemInternalId == "")
		{
			getItemInternalId=NextItemInternalId;
		}

		nlapiLogExecution('Error', 'getwoid', getwoid);

		var WOFilters = new Array();
		WOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
		WOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

		if(getwoid !== null && getwoid !="")
		{
			WOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getwoid));
		}
		if(getZoneNo!=null && getZoneNo!="" && getZoneNo!="null")
		{
			WOFilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', getZoneNo));
		}
		var WOColumns = new Array();

		WOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
		WOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
		WOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
		WOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
		WOColumns.push(new nlobjSearchColumn('custrecord_sku'));
		WOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
		WOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
		WOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
		WOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
		WOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
		WOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
		WOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
		WOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
		WOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
		WOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
		WOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
		WOColumns.push(new nlobjSearchColumn('name'));
		WOColumns.push(new nlobjSearchColumn('custrecord_container'));				
		WOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
		WOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
		WOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
		WOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
		WOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
		WOColumns.push(new nlobjSearchColumn('custrecord_totalcube'));
		WOColumns.push(new nlobjSearchColumn('custrecord_total_weight'));

		WOColumns[0].setSort();
		WOColumns[1].setSort();
		WOColumns[2].setSort();
		WOColumns[3].setSort();
		WOColumns[4].setSort();
		WOColumns[5].setSort(true);

		var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, WOFilters, WOColumns);

		var Orders,Picks,Locations,Cartons,Totalcube,TotalWeight,CartonsSize;
		for (var i = 0; i < WOSearchResults.length; i++){

			Orders=WOSearchResults[i].getValue('custrecord_ebiz_order_no');
			Picks=WOSearchResults[i].getText('custrecord_expe_qty');
			Locations=WOSearchResults[i].getText('custrecord_actbeginloc');

			noofPicks=WOSearchResults.length;

			PickArray[i]=Picks;
			LocArray[i]=Locations;
		}

		if(LocArray.length>0)
		{
			noofLocations=removeDuplicateElement(LocArray);
			nlapiLogExecution('Error', 'noofLocations', noofLocations);
		}


		nlapiLogExecution('Error', 'NextItemInternalId', NextItemInternalId); 
		//case# 20127004 starts (now form name passed correctly to work keyboard enter button)
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		//case# 20127004 end
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + " document.getElementById('enterwaveno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getwoid + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getFetchedBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo  + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + vBatchNo  + ">";	
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";	
		html = html + "				<input type='hidden' name='hdnOrdNo' value=" + OrdNo + ">";
		html = html + "				<input type='hidden' name='hdnitemstatus' value=" + ItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";

		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>"+noofPicks+"</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>"+noofLocations+"</label>";
		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st7 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st8 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('Error', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st9,st10;

		if( getLanguage == 'es_ES')
		{
			st9 = "NO V&#193;LIDO WAVE #";
			st10 = "GRUPO DE VALIDEZ #";

		}
		else
		{
			st9 = "INVALID WORKORDER #";
			st10 = "INVALID CLUSTER #";
		}


		var getwoid=request.getParameter('hdnWOid');
		var vClusterNo=request.getParameter('hdnClusterNo');
		var vPickType=request.getParameter('hdnpicktype');
		var vItemStatus=request.getParameter('hdnitemstatus');

		var WOarray = new Array();
		WOarray["custparam_language"] = getLanguage;
		var vSkipId=request.getParameter('hdnskipid');
		WOarray["custparam_skipid"] = request.getParameter('hdnskipid');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') 
		{
			WOarray["custparam_error"] = st9;//'INVALID WAVE #';
			WOarray["custparam_screenno"] = '12';
			WOarray["custparam_type"] ="Wave";
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking', 'customdeploy_ebiz_rf_wo_picking_di', false, WOarray);

		}
		else 
		{
			nlapiLogExecution('ERROR', 'getwoid', getwoid);


			var vSOId;
			var vOrdFormat='W';
			var vZoneId;
			var SOFilters = new Array();
			var SOColumns = new Array();

			if(getwoid!= null && getwoid!= "")
			{

				nlapiLogExecution('ERROR', 'Type:WaveNo inside if', getwoid); 

				WOarray["custparam_woid"] =getwoid; 
				WOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
				WOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
				WOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
				WOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocation');
				WOarray["custparam_item"] = request.getParameter('hdnItem');
				WOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
				WOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
				WOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
				WOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
				WOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
				WOarray["custparam_beginLocationname"] = request.getParameter('custparam_beginlocationname');
				WOarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
				WOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
				WOarray["custparam_whcompany"] = request.getParameter('hdnwhCompany');
				WOarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
				WOarray["name"] =  request.getParameter('hdnOrdNo');
				WOarray["custparam_containersize"] =  request.getParameter('hdnContainerSize');
				WOarray["custparam_ebizordno"] =  request.getParameter('hdnsoid');
				WOarray["custparam_itemstatus"] =  request.getParameter('hdnitemstatus');
				WOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');

				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, WOarray);

			}
		}
	}
}




function removeDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray.length;
}


/**
 * @param SOFilters
 * @returns {Array}
 */
function ContainerSizeCount(SOFilters)
{
	try
	{
		var SOColumns = new Array();
		SOColumns[0] = new nlobjSearchColumn('name',null,'group');
		SOColumns[1] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
		SOColumns[2] = new nlobjSearchColumn('custrecord_container',null,'group');
		SOColumns[1].setSort(true);
		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		var totalarray=new Array();
		var tempcontainersizeArray=new Array();
		var vcount=0;
		for(var x=0;x<SOSearchResults.length;x++)
		{
			var count=0;
			var vcontainerSize=SOSearchResults[x].getText('custrecord_container',null,'group');

			nlapiLogExecution('ERROR','index',tempcontainersizeArray.indexOf(vcontainerSize));
			if(tempcontainersizeArray.indexOf(vcontainerSize)==-1)
			{
				for(var y=0;y<SOSearchResults.length;y++)
				{
					var tempcontainersize=SOSearchResults[y].getText('custrecord_container',null,'group');
					if(vcontainerSize==tempcontainersize)
					{
						tempcontainersizeArray.push(vcontainerSize);
						count++;
					}
				}
				totalarray[vcount]=new Array();
				totalarray[vcount][0]=vcontainerSize;
				totalarray[vcount][1]=count;
				vcount++;
			}
		}

		return totalarray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in ContianerSizeCount',exp);
	}
}

function TotalWeightandCube(SOSearchResults,CartonArray)
{
	try
	{
		var vtotalwt=0;
		var vtotalcube=0;
		var multiplier=0;
		//To get the LPcount for the distinct LPs.
		//Eg: lp1-2;lp2-5
		var LP_Count=ContainerLPCount(SOSearchResults);
		nlapiLogExecution('ERROR','LP_Count',LP_Count);

		//To get the distinct LP Values.
		var duplicateContainerLp=vremoveDuplicateElement(CartonArray);
		nlapiLogExecution('ERROR','duplicateContainerLp',duplicateContainerLp);

		for ( var count = 0; count < duplicateContainerLp.length; count++) 
		{
			for(var x=0;x<LP_Count.length;x++)
			{
				if(LP_Count[x][0]==duplicateContainerLp[count])
				{
					multiplier=LP_Count[x][1];
				}
			}
			var result=GetCubeWt_LpMaster(duplicateContainerLp[count]);
			vtotalwt=vtotalwt+(parseFloat(result[0])*parseFloat(multiplier));
			vtotalcube=vtotalcube+(parseFloat(result[1])*parseFloat(multiplier));
		}
		nlapiLogExecution('ERROR','vtotalwt',vtotalwt);
		nlapiLogExecution('ERROR','vtotalcube',vtotalcube);

		var FinalArray=new Array();
		FinalArray[0]=vtotalwt;
		FinalArray[1]=vtotalcube;

		return FinalArray;

	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in ContianerSizeCount',exp);
	}
}


function GetCubeWt_LpMaster(ContainerLP)
{
	try
	{
		var vtotalwt=0;
		var vtotalcube=0;
		var vTotalArray=new Array();
		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp',null,'is',ContainerLP));
		filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
		column[1]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');

		var searchResults=nlapiSearchRecord('customrecord_ebiznet_master_lp',null,filter,column);

		if(searchResults!=null&&searchResults!="")
		{
			vtotalwt=searchResults[0].getValue('custrecord_ebiz_lpmaster_totwght');
			vtotalcube=searchResults[0].getValue('custrecord_ebiz_lpmaster_totcube');
		}
		vTotalArray[0]=vtotalwt;
		vTotalArray[1]=vtotalcube;
		return vTotalArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetCubeWt_LpMaster',exp);
	}
}


function vremoveDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}


/**
 * @param SOFilters
 * @returns {Array}
 */
function ContainerLPCount(SOSearchResults)
{
	try
	{
		var totalarray=new Array();
		var tempcontainerLPArray=new Array();
		var vcount=0;
		for(var x=0;x<SOSearchResults.length;x++)
		{
			var count=0;
			var vcontainerLP=SOSearchResults[x].getValue('custrecord_container_lp_no');

			nlapiLogExecution('ERROR','index',tempcontainerLPArray.indexOf(vcontainerLP));
			if(tempcontainerLPArray.indexOf(vcontainerLP)==-1)
			{
				for(var y=0;y<SOSearchResults.length;y++)
				{
					var tempcontainerLP=SOSearchResults[y].getValue('custrecord_container_lp_no');
					if(vcontainerLP==tempcontainerLP)
					{
						tempcontainerLPArray.push(vcontainerLP);
						count++;
					}
				}
				totalarray[vcount]=new Array();
				totalarray[vcount][0]=vcontainerLP;
				totalarray[vcount][1]=count;
				vcount++;
			}
		}

		return totalarray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in ContianerSizeCount',exp);
	}
}