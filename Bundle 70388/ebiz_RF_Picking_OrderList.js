/***************************************************************************
 eBizNET Solutions                
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_Picking_OrderList.js,v $
 *     	   $Revision: 1.1.4.2 $
 *     	   $Date: 2015/07/09 13:01:30 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * 
 *****************************************************************************/

function PickingOrderList(request, response){
	nlapiLogExecution('DEBUG', 'Into Request', 'Into Request');

	var fastpick = request.getParameter('custparam_fastpick');
	nlapiLogExecution('DEBUG', 'fastpick', 'fastpick');

	var getLanguage = request.getParameter('custparam_language');
	nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);

	var vOrdFormat = 'O';
	var st1;
	if( getLanguage == 'es_ES')
	{	
		st1 = "ANTERIOR ";
	}
	else
	{
		st1 = "PREV ";
	}


	if (request.getMethod() == 'GET') 
	{
		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();
		nlapiLogExecution('ERROR', 'currentUserID', currentUserID);		
		var SOFilters = new Array();
		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
		SOFilters.push(new nlobjSearchFilter('custrecord_taskassignedto', null, 'anyof', ['@NONE@',currentUserID]));
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty'));

		var SOColumns = new Array();

		SOColumns.push(new nlobjSearchColumn('name',null,'group'));				
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group'));
		SOColumns.push(new nlobjSearchColumn('custrecord_taskassignedto',null,'group'));
		SOColumns[0].setSort(true);	
		SOColumns[2].setSort(true);

		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		var checkInURL = nlapiResolveURL('SUITELET', 'customscript_rf_picking_containerprocess', 'customdeploy_rf_picking_containerprocess');
		var linkURL = checkInURL; 
		var functionkeyHtml=getFunctionkeyScript('_rf_pickingcontainercontinue'); 

		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('enterwaveno').focus();";  

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_pickingcontainercontinue' method='POST'>";
		html = html + "		<table>";
		html = html + "		<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";			
		html = html + "				</td>";
		html = html + "		</tr>";
		html = html + "			<tr><td><table>";
		html = html + "			<tr>";
		//html = html + "				<td>";
		//html = html + "				<label><u>Wave#</u></label>";
		//html = html + "				</td>";
		html = html + "				<td>";
		html = html + "				<label><u>Order No#</u></label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		if(SOSearchResults != null && SOSearchResults != '' && SOSearchResults.length>0)
		{	
			for (var i = 0; i < Math.min(5, SOSearchResults.length); i++) {
				var waveno=SOSearchResults[i].getValue('custrecord_ebiz_wave_no',null,'group');
				var orderno=SOSearchResults[i].getValue('name',null,'group');

				//html = html + "			<tr><td align = 'left' width='50%'><a href='" + linkURL +"&custparam_waveno=" + waveno + "&custparam_orderno=" +orderno+"&custparam_language=" +getLanguage+"&custparam_fastpick=" +fastpick+"' style='text-decoration: none'>" + waveno + "</a></td>" ;
				html = html + "			<td align = 'left' width='50%'><a href='" + linkURL + "&custparam_waveno=" + waveno + "&custparam_orderno=" +orderno+"&custparam_language=" +getLanguage+"&custparam_fastpick=" +fastpick+"&custparam_picktype="+vOrdFormat+"' style='text-decoration: none'>" + orderno + "</a></td>" ;
				html = html + "	</td></tr>";
			}
		}
		else
		{
			var SOarray = new Array();
			SOarray["custparam_screenno"] = '12';
			nlapiLogExecution('ERROR', 'SOSearchResults: ','empty' );
			SOarray["custparam_error"] = 'No Records Available';
			nlapiLogExecution('ERROR', 'Error: ', 'No records to show');
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			return;	

		}
		html = html + "			<tr>";
		html = html + "			<td align = 'left'>" + st1 + "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		var SOarray = new Array();
		var fastpick = request.getParameter('hdnfastpick');

		nlapiLogExecution('DEBUG', 'fastpick', 'fastpick');
		var getLanguage = request.getParameter('hdngetLanguage');
		var optedEvent = request.getParameter('cmdPrevious');
		nlapiLogExecution('ERROR', 'optedEvent', optedEvent);
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di', false, SOarray);
		}
	}	
}