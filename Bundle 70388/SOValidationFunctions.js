/***************************************************************************
 eBizNET Solutions LTD               
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/Attic/SOValidationFunctions.js,v $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: SOValidationFunctions.js,v $
 * Revision 1.1.4.6.2.3  2015/07/07 15:41:42  snimmakayala
 * Case#: 201413392
 *
 * Revision 1.1.4.6.2.2  2014/08/06 15:43:49  nneelam
 * case#  20149864
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.4.6.2.1  2014/07/25 15:20:34  skavuri
 * Case# 20149651Compatibility issue fixed
 *
 * Revision 1.1.4.6  2014/01/20 13:39:55  snimmakayala
 * Case# : 20126712
 *
 * Revision 1.1.4.5  2013/10/24 13:35:59  rmukkera
 * Case# 20124702
 *
 * Revision 1.1.4.4  2013/10/16 07:23:38  rrpulicherla
 * Case# 20124807
 * Fos created saved searches
 *
 * Revision 1.1.4.3  2013/06/21 23:33:44  skreddy
 * CASE201112/CR201113/LOG201121
 * to restrict when payment credit limit exceed
 *
 * Revision 1.1.4.2  2013/02/26 14:41:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Boombah.
 *
 * Revision 1.1.2.1  2013/02/26 13:01:17  skreddy
 * CASE201112/CR201113/LOG201121
 * merged boombah changes
 *
 *
 *****************************************************************************/


function fnCreateFo(soid)
{
	nlapiLogExecution('Error', 'Into fnCreateFo (SO Internalid)', soid);
	var createfo='T';

	if(soid!=null && soid!='')
	{
		try
		{
			nlapiLogExecution('DEBUG', 'salesorderstart',TimeStampinSec());	
			var trantype = nlapiLookupField('transaction', soid, 'recordType');
			//var sorec= nlapiLoadRecord('salesorder', soid);
			//var sorec= nlapiLoadRecord(trantype, soid);

			var filters = new Array();
			filters.push(new nlobjSearchFilter('internalid', null, 'is', soid));
			filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('entity');	
			columns[1] = new nlobjSearchColumn('status');	
			columns[2] = new nlobjSearchColumn('paymenteventresult');
			columns[3] = new nlobjSearchColumn('daysoverdue');

			var sorec = new nlapiSearchRecord(trantype, null, filters, columns);

			nlapiLogExecution('DEBUG', 'salesorderend',TimeStampinSec());	
			if(sorec!=null && sorec!='')
			{

				/*var paymentstatus = sorec.getFieldValue('paymenteventresult');	
				var customer = sorec.getFieldValue('entity');
				var DaysOverDue = null;//sorec.getFieldValue('daysoverdue');
				var OrdStatus = sorec.getFieldValue('status');*/

				var paymentstatus = sorec[0].getValue('paymenteventresult');	
				var customer = sorec[0].getValue('entity');
				var DaysOverDue = '';
				var OrdStatus = sorec[0].getValue('status');

				var str = 'paymentstatus. = ' + paymentstatus + '<br>';
				str = str+ 'customer. = ' + customer + '<br>';
				str = str+ 'DaysOverDue. = ' + DaysOverDue + '<br>';
				str = str+ 'OrdStatus = ' + OrdStatus + '<br>';
				nlapiLogExecution('ERROR', 'SO Validation Log', str);
				nlapiLogExecution('DEBUG', 'sovalidationend',TimeStampinSec());

				var vConfig=nlapiLoadConfiguration('accountingpreferences');
				var ruleValue=vConfig.getFieldValue('CUSTCREDLIMHANDLING');

				if(vConfig != null && vConfig != '')
				{
					DaysOverDue=vConfig.getFieldValue('CREDLIMDAYS');
				}
				if(OrdStatus.toUpperCase()=='CLOSED' || OrdStatus.toUpperCase()=='CANCELLED')
				{
					createfo='F';
					nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
					nlapiLogExecution('Error', 'Reason is Order Closed');
					return createfo;

				}
				else if(paymentstatus=='HOLD')
				{
					createfo='F';
					nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
					nlapiLogExecution('Error', 'Reason is payment Hold');
					return createfo;
				}
				else if(DaysOverDue != null && DaysOverDue != '' && parseInt(DaysOverDue)>0)
				{
					//Case # 20124702 start
					if(customer != null && customer != '')
					{					
						var filtersLP = new Array(); 
						filtersLP.push(new nlobjSearchFilter('internalid', null, 'is', customer));
						var searchresults = nlapiSearchRecord('customer', 'customsearch_ebiz_customer_due_days', filtersLP, null);
						if(searchresults != null && searchresults != '' && searchresults.length>0)
						{
							createfo='F';
							nlapiLogExecution('Error', 'searchresults.length', searchresults.length);
							nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
							nlapiLogExecution('Error', 'Reason is Credit limit exceeded');
							return createfo;		
						}							
					}
					//Case # 20124702 End.

				}	
				else if(ruleValue!=null && ruleValue!='' && ruleValue!='null' && ruleValue =='HOLD')
				{	
					if(customer != null && customer != '')
					{	
						var filtersLP = new Array(); 
						filtersLP.push(new nlobjSearchFilter('internalid', null, 'is', customer));
						var searchresults = nlapiSearchRecord('customer', 'customsearch_ebiz_customer_overdue', filtersLP, null);
						if(searchresults != null && searchresults != '' && searchresults.length>0)
						{
							createfo='F';
							nlapiLogExecution('Error', 'searchresults.length', searchresults.length);
							nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
							nlapiLogExecution('Error', 'Reason is Credit limit exceeded');
							return createfo;		
						}	 
					}
				}

			}
		}
		catch(exp)
		{
			nlapiLogExecution('Error', 'Exception in fnCreateFo', exp);
		}
	}

	nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);

	return createfo;
}

function isSOClosed(soid)
{
	nlapiLogExecution('Error', 'Into fnCreateFo (SO Internalid)', soid);
	var createfo='T';

	if(soid!=null && soid!='')
	{
		try
		{
			//var sorec= nlapiLoadRecord('salesorder', soid);

			var trantype = nlapiLookupField('transaction', soid, 'recordType');

			var filters = new Array();
			filters.push(new nlobjSearchFilter('internalid', null, 'is', soid));
			filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('entity');	
			columns[1] = new nlobjSearchColumn('status');	
			columns[2] = new nlobjSearchColumn('paymenteventresult');
			columns[3] = new nlobjSearchColumn('daysoverdue');

			var sorec = new nlapiSearchRecord(trantype, null, filters, columns);

			if(sorec!=null && sorec!='')
			{
				//var paymentstatus = sorec.getFieldValue('paymenteventresult');	
				//var customer = sorec.getFieldValue('entity');
				//var DaysOverDue = sorec.getFieldValue('daysoverdue');
				//var OrdStatus = sorec.getFieldValue('status');

				var paymentstatus = sorec[0].getValue('paymenteventresult');	
				var customer = sorec[0].getValue('entity');
				var DaysOverDue = sorec[0].getValue('daysoverdue');
				var OrdStatus = sorec[0].getValue('status');

				var str = 'paymentstatus. = ' + paymentstatus + '<br>';
				str = str+ 'customer. = ' + customer + '<br>';
				str = str+ 'DaysOverDue. = ' + DaysOverDue + '<br>';
				str = str+ 'OrdStatus = ' + OrdStatus + '<br>';
				nlapiLogExecution('ERROR', 'SO Validation Log', str);

				var vConfig=nlapiLoadConfiguration('accountingpreferences');
				var ruleValue=vConfig.getFieldValue('CUSTCREDLIMHANDLING');
				if(OrdStatus.toUpperCase()=='CLOSED' || OrdStatus.toUpperCase()=='CANCELLED')
				{
					createfo='F';
					nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
					nlapiLogExecution('Error', 'Reason is Order Closed');
					return createfo;

				}
				else if(paymentstatus=='HOLD')
				{
					createfo='F';
					nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
					nlapiLogExecution('Error', 'Reason is payment Hold');
					return createfo;
				}
				else if(DaysOverDue != null && DaysOverDue != '' && parseInt(DaysOverDue)>0)
				{
					createfo='F';
					nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
					nlapiLogExecution('Error', 'Reason is Days over due');
					return createfo;
				}	
				else if(ruleValue!=null && ruleValue!='' && ruleValue!='null' && ruleValue =='HOLD')
				{	
					if(customer != null && customer != '')
					{	
						var filtersLP = new Array(); 
						filtersLP.push(new nlobjSearchFilter('internalid', null, 'is', customer));
						var searchresults = nlapiSearchRecord('customer', 'customsearch_ebiz_customer_overdue', filtersLP, null);
						if(searchresults != null && searchresults != '' && searchresults.length>0)
						{
							createfo='F';
							nlapiLogExecution('Error', 'searchresults.length', searchresults.length);
							nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);
							nlapiLogExecution('Error', 'Reason is Credit limit exceeded');
							return createfo;		
						}	 
					}
				}

			}
		}
		catch(exp)
		{
			nlapiLogExecution('Error', 'Exception in fnCreateFo', exp);
		}
	}

	nlapiLogExecution('Error', 'Out of fnCreateFo (Create FO Flag)', createfo);

	return createfo;
}


function TimeStampinSec(){
	var timestamp;
	var now = new Date();
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = now.getHours();
	var curr_min = now.getMinutes();
	var curr_sec = now.getSeconds();


	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	if (curr_sec.length == 1)
		curr_sec = "0" + curr_sec;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + ":" + curr_sec + " " + a_p;

	return timestamp;
}
