/***************************************************************************
  eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_PurchaseOrder_CL.js,v $
 *     	   $Revision: 1.12.2.9.4.1.4.33.2.4 $
 *     	   $Date: 2015/11/13 15:31:13 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PurchaseOrder_CL.js,v $
 * Revision 1.12.2.9.4.1.4.33.2.4  2015/11/13 15:31:13  schepuri
 * case# 201415439
 *
 * Revision 1.12.2.9.4.1.4.33.2.3  2015/10/27 18:21:42  grao
 * 2015.2 Issue Fixes 201415210
 *
 * Revision 1.12.2.9.4.1.4.33.2.2  2015/10/21 15:24:05  sponnaganti
 * case# 201415153
 * Delmed Prod issue fix
 *
 * Revision 1.12.2.9.4.1.4.33.2.1  2015/10/20 10:49:06  grao
 *  CT issue fixes 201415135
 *
 * Revision 1.12.2.9.4.1.4.33  2015/05/25 15:31:23  nneelam
 * case# 201412333
 *
 * Revision 1.12.2.9.4.1.4.32  2015/05/04 15:37:11  nneelam
 * case# 201412620
 *
 * Revision 1.12.2.9.4.1.4.31  2015/03/16 10:18:47  skreddy
 * Case# 201412014
 * Lonely Planet SB  issue fix
 *
 * Revision 1.12.2.9.4.1.4.30  2014/12/31 13:24:23  spendyala
 * Prod issue fixes
 *
 * Revision 1.12.2.9.4.1.4.29  2014/12/26 14:23:34  skreddy
 * Case# 201411324
 * CT Prod Issue Fixed
 *
 * Revision 1.12.2.9.4.1.4.28  2014/12/24 12:17:51  snimmakayala
 * Case#: 201411299
 *
 * Revision 1.12.2.9.4.1.4.24.2.4  2014/12/23 12:31:44  snimmakayala
 * Case#: 201411299
 *
 * Revision 1.12.2.9.4.1.4.24.2.3  2014/12/22 16:54:20  rrpulicherla
 * Case#201411317
 *
 * Revision 1.12.2.9.4.1.4.27  2014/12/22 16:44:25  rrpulicherla
 * Case#201411317
 *
 * Revision 1.12.2.9.4.1.4.26  2014/11/14 07:49:16  rrpulicherla
 * Std fixes
 *
 * Revision 1.12.2.9.4.1.4.25  2014/10/16 14:03:16  vmandala
 * Case# 201410680
 * Std bundle issue fixed
 *
 * Revision 1.12.2.9.4.1.4.24  2014/06/19 15:44:39  sponnaganti
 * case# 20148981
 * Compatability test acc issue fixes
 *
 * Revision 1.12.2.9.4.1.4.23  2014/06/06 15:01:03  skavuri
 * Case# 20148728 SB Issue Fixed
 *
 * Revision 1.12.2.9.4.1.4.22  2014/05/29 15:43:50  sponnaganti
 * case# 20148232
 * Stnd Bundle Issue fix
 *
 * Revision 1.12.2.9.4.1.4.21  2014/05/23 15:16:18  sponnaganti
 * case# 20148232
 * Stnd Bundle Issue fix
 *
 * Revision 1.12.2.9.4.1.4.20  2014/03/24 15:06:21  grao
 * Case# 20127199 related issue fixes in Sonic  electrox issue fixes
 *
 * Revision 1.12.2.9.4.1.4.19  2014/03/19 15:14:51  grao
 * Case#  (No Case#:) related issue fixes in SportsHq prod issue fixes
 *
 * Revision 1.12.2.9.4.1.4.18  2014/03/13 15:24:11  snimmakayala
 * Case #: 20127667
 *
 * Revision 1.12.2.9.4.1.4.9.2.10  2014/03/13 15:01:50  snimmakayala
 * Case #: 20127667
 *
 * Revision 1.12.2.9.4.1.4.9.2.9  2014/02/13 15:25:09  skavuri
 * case # 20127121 (we can not edit the item after checkin in po) is fixed
 *
 * Revision 1.12.2.9.4.1.4.9.2.8  2014/02/12 15:10:09  sponnaganti
 * case# 20127132
 * (after checkin process then any changes to po line qty is updated to Transaction order line details)
 *
 * Revision 1.12.2.9.4.1.4.9.2.7  2014/02/11 15:16:09  sponnaganti
 * case# 20127112
 * (New Filter added for receipt type)
 *
 * Revision 1.12.2.9.4.1.4.9.2.6  2014/02/10 14:48:58  sponnaganti
 * case# 20127103
 * (if line number if condition added)
 *
 * Revision 1.12.2.9.4.1.4.9.2.5  2014/01/30 16:04:14  sponnaganti
 * case# 20127007
 * In PO after Checkin qty may be increase not decrase working fine
 *
 * Revision 1.12.2.9.4.1.4.9.2.4  2014/01/24 15:26:34  skavuri
 * case# 20126939
 * change the filter condition putconf_qty to checkin_qty for Decreasing the qty after doing cart check in .
 *
 * Revision 1.12.2.9.4.1.4.9.2.3  2013/12/30 15:35:55  skreddy
 * case#20126601
 * Standard bundle issue fix
 *
 * Revision 1.12.2.9.4.1.4.9.2.2  2013/12/27 19:14:23  grao
 * Case# 20126187 related issue fixes in Sb issue fixes
 *
 * Revision 1.12.2.9.4.1.4.9.2.1  2013/12/26 21:50:08  grao
 * Case# 20126187 related issue fixes in Sb issue fixes
 *
 * Revision 1.12.2.9.4.1.4.9  2013/10/15 15:28:35  grao
 * issue fixes related to the 20124970
 *
 * Revision 1.12.2.9.4.1.4.8  2013/09/04 15:37:38  skreddy
 * Case# 20124227
 * standard bundle issue fix
 *
 * Revision 1.12.2.9.4.1.4.7  2013/07/05 08:57:33  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Dynacraft :: Performance fixes.
 * Case# : 1712182
 *
 * Revision 1.12.2.9.4.1.4.6  2013/07/05 08:46:52  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Dynacraft :: Performance fixes.
 * Case# : 1712182
 *
 * Revision 1.12.2.9.4.1.4.5  2013/06/07 06:35:40  mbpragada
 * 20120490
 *
 * Revision 1.12.2.9.4.1.4.4  2013/05/21 14:40:54  grao
 * PO Closed related Validation
 * Case#:20122721 issues fixes in PMM Prod account.
 *
 * Revision 1.12.2.9.4.1.4.3  2013/04/23 15:30:48  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.12.2.9.4.1.4.2  2013/04/09 13:20:19  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to restrict to delete PO line after check in
 *
 * Revision 1.12.2.9.4.1.4.1  2013/03/05 14:51:26  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.12.2.9.4.1  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.12.2.9  2012/08/29 13:40:52  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Purchaseorder validations
 *
 * Revision 1.12.2.7  2012/05/25 06:43:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * item allias
 *
 * Revision 1.12.2.6  2012/05/22 07:03:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * By pass qty validation for discount items
 *
 * Revision 1.12.2.5  2012/05/15 05:28:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.12.2.4  2012/04/11 12:32:13  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Dates null condition checking
 *
 * Revision 1.12.2.3  2012/02/22 14:09:39  spendyala
 * CASE201112/CR201113/LOG201121
 * In GetRecord function filter criteria is changed from order field to control no. field.
 *
 * Revision 1.12.2.2  2012/02/21 11:03:59  spendyala
 * CASE201112/CR201113/LOG201121
 * Need To allow user to change the Qty in PO Line before it proceed for inbound process.
 *
 * Revision 1.12.2.1  2012/01/18 14:41:56  spendyala
 * CASE201112/CR201113/LOG201121
 * commented line in onDropChangeType function.
 *
 * Revision 1.12  2011/12/02 09:26:37  schepuri
 * CASE201112/CR201113/LOG201121
 * inbound issue fixing
 *
 * Revision 1.11  2011/11/18 22:57:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Delete record from inventory if QOH becomes 0
 *
 * Revision 1.10  2011/11/02 19:18:17  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate SKU status and packcode while change the item in PO Line
 *
 * Revision 1.9  2011/11/02 16:56:56  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate SKU status and packcode while change the item in SO Line
 *
 * Revision 1.8  2011/10/25 15:52:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * PO client file
 *
 * Revision 1.7  2011/10/19 21:03:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * PO Client Generate Locations
 *
 * Revision 1.6  2011/10/19 19:42:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * PO Client Generate Locations
 *
 * Revision 1.5  2011/10/17 18:11:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * PO client script
 *
 * Revision 1.4  2011/09/16 10:06:49  mbpragada
 * CASE201112/CR201113/LOG201121
 * Added validation for item status in PO line
 *
 * Revision 1.3  2011/07/21 04:48:21  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.10  2011/07/20 12:57:48  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.4  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
/*
This Script is called when PO is Saved (Checks whether PO line is edited 
quantity with less value than prev ord qty. 
 */
/**
 * @param type
 * @returns {Boolean}
 */
function onSave(type)
{
	try 
	{
		var podate = nlapiGetFieldValue('trandate');
		var shipdate = nlapiGetFieldValue('custbody_nswmspoexpshipdate');
		var arrivaldate = nlapiGetFieldValue('custbody_nswmspoarrdate');
		//case# 20148232 starts
		var locationId=nlapiGetFieldValue('location');
		var rolelocation=getRoledBasedLocation();
		//alert('rolelocation'+ rolelocation);
		//alert('locationId'+ locationId);
		var mwhsiteflag;
		if(locationId!=null && locationId!='')
		{
			var fields = ['custrecord_ebizwhsite'];

			var locationcolumns = nlapiLookupField('Location', locationId, fields);
			if(locationcolumns!=null)
				mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
		}
		if(mwhsiteflag=='T')
		{	
			if(locationId!=null && locationId !=''&& rolelocation!='null' && rolelocation!=null && rolelocation!='' && rolelocation!=0)
			{

				if(rolelocation.indexOf(parseInt(locationId))== -1)
				{
					alert("INVALID LOCATION");
					return false;
				}

			}
			//case# 20148232		

			//case# 20127112 starts
			var receipttype=nlapiGetFieldText('custbody_nswmsporeceipttype');
			var vloc1=nlapiGetFieldValue('location');
			//alert(receipttype);
			//alert(vloc1);
			var rcfilters=new Array();
			if(receipttype!=null && receipttype!='')
				rcfilters.push(new nlobjSearchFilter('custrecord_ebizreceipttype', null, 'is', receipttype));
			if(vloc1!=null && vloc1!='')
				rcfilters.push(new nlobjSearchFilter('custrecord_ebizsiterecpt', null, 'anyof', vloc1));

			var rcsearchresults=nlapiSearchRecord('customrecord_ebiznet_receipt_type', null, rcfilters,null);
			if(rcsearchresults==null)
			{
				alert('Receipt type not matched with given location');
				return false;
			}
			//case# 20127112 end
			var Ponumber= nlapiGetFieldValue('tranid');
			//alert('Ponumber :' +Ponumber);

			if(Ponumber ==null || Ponumber == '')
			{
				alert('Please enter value for: PO# ');
				return false;
			}

			if ((podate != '' && arrivaldate != '') || (podate != '' && shipdate != '')) 
			{
				if ((podate != '' && podate != null ) && (arrivaldate != '' && arrivaldate != null)) 
				{
					if (CompareDates_New(podate, arrivaldate) == false) 
					{
						alert('Arriaval Date should not less than Todays Date');
						return false;
					}	            
				}
				if (arrivaldate != '' && arrivaldate != null) 
				{
					if (CompareDates_New(DateStamp(), arrivaldate) == false) {
						alert('Arriaval Date should not less than Current Date');
						return false;
					}	            
				}
				if ((podate != '' && podate != null) && (shipdate != '' &&  shipdate != null)) 
				{
					var varCheckShipdate = CompareDates_New(podate, shipdate);
					if (varCheckShipdate == false) 
					{
						alert('Exp Ship Date should not less than Todays Date');
						return false;
					}	            
				}
				if ((arrivaldate != '' && arrivaldate != null) && (shipdate != '' && shipdate != null)) 
				{
					if (CompareDates_New(shipdate, arrivaldate) == false) 
					{
						alert('Arrival Date should not less than Exp Ship Date');
						return false;
					}	            
				}

			}

			var Poid = nlapiGetFieldValue('id');
			var lineCnt = nlapiGetLineItemCount('item');
			if (Poid != null && Poid > 1) 
			{
				//var sorec = nlapiLoadRecord('purchaseorder', Poid);
				//alert('Poid '+Poid);
				var opentaskrecords=GetPORecords(Poid);

				//nlapiLogExecution('ERROR', 'Time Stamp before PO search',TimeStampinSec());

				var pofilters = new Array();
				pofilters[0] = new nlobjSearchFilter('internalid', null, 'is', Poid);
				pofilters[1] = new nlobjSearchFilter('mainline', null, 'is', 'F');

				var pocolumns = new Array();
				pocolumns[0] = new nlobjSearchColumn('line');
				pocolumns[1] = new nlobjSearchColumn('internalid');
				pocolumns[2] = new nlobjSearchColumn('tranid');
				pocolumns[3] = new nlobjSearchColumn('quantity');
				pocolumns[4] = new nlobjSearchColumn('location');
				pocolumns[1].setSort();

				var POsearchresults=nlapiSearchRecord('purchaseorder', null, pofilters,pocolumns);

				//nlapiLogExecution('ERROR', 'Time Stamp after PO search',TimeStampinSec());

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('name');
				columns[1] = new nlobjSearchColumn('internalid');
				columns[2] = new nlobjSearchColumn('custrecord_ebizwhsite');
				columns[1].setSort();
				var Locsearchresults=nlapiSearchRecord('location', null, null,columns);

				for (var s = 1; s <= lineCnt; s++) 
				{

					var transactionFilters = new Array();
					transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', Poid);
					//transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', Lineno);

					var transactionColumns = new Array();
					transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
					transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
					transactionColumns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');

					var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);

					if(transactionSearchresults==null || transactionSearchresults=='')
					{
						return true;
					}

					//var sorec = nlapiLoadRecord('purchaseorder', Poid);
					//alert('Poid '+Poid);
					var opentaskrecords=GetPORecords(Poid);

					//nlapiLogExecution('ERROR', 'Time Stamp before PO search',TimeStampinSec());

					/*var pofilters = new Array();
					pofilters[0] = new nlobjSearchFilter('internalid', null, 'is', Poid);
					pofilters[1] = new nlobjSearchFilter('mainline', null, 'is', 'F');

					var pocolumns = new Array();
					pocolumns[0] = new nlobjSearchColumn('line');
					pocolumns[1] = new nlobjSearchColumn('internalid');
					pocolumns[2] = new nlobjSearchColumn('tranid');
					pocolumns[3] = new nlobjSearchColumn('quantity');
					pocolumns[4] = new nlobjSearchColumn('location');
					pocolumns[1].setSort();

					var POsearchresults=nlapiSearchRecord('purchaseorder', null, pofilters,pocolumns);

					//nlapiLogExecution('ERROR', 'Time Stamp after PO search',TimeStampinSec());

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('name');
					columns[1] = new nlobjSearchColumn('internalid');
					columns[2] = new nlobjSearchColumn('custrecord_ebizwhsite');
					columns[1].setSort();
					var Locsearchresults=nlapiSearchRecord('location', null, null,columns);*/

					for (var s = 1; s <= lineCnt; s++) 
					{
						var ordqty = nlapiGetLineItemValue('item', 'quantity', s);
						var Lineno = nlapiGetLineItemValue('item', 'line', s);
						var actqty = 0;
						var LineLoc = '';
						//alert('ordqty '+ordqty);
						//alert('Lineno '+Lineno);
						//case# 20127132 starts (after checkin process then any changes to po line qty is updated to Transaction order line details)
						/*var transactionFilters = new Array();
						transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', Poid);
						transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', Lineno);

						var transactionColumns = new Array();
						transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
						transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');

						var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);*/

						if(transactionSearchresults!=null && transactionSearchresults!='')
						{
							for(var scount=0;scount<transactionSearchresults.length;scount++)
							{
								var transactionline=transactionSearchresults[scount].getValue("custrecord_orderlinedetails_orderline_no");
								var transactionOrdQty=transactionSearchresults[scount].getValue("custrecord_orderlinedetails_order_qty");
								//Only update the linee when qty got changed
								if((Lineno==transactionline)&&(ordqty!=transactionOrdQty))
								{
									var trid=transactionSearchresults[scount].getId();
									var fields = new Array();
									var values = new Array();
									fields.push('custrecord_orderlinedetails_order_qty');
									values.push(ordqty);
									nlapiSubmitField('customrecord_ebiznet_order_line_details', trid, fields, values);	
									break;
//									var trid=transactionSearchresults[0].getId();
//									var rec=nlapiLoadRecord('customrecord_ebiznet_order_line_details',trid);
//									rec.setFieldValue('custrecord_orderlinedetails_order_qty',ordqty);
//									var tranid = nlapiSubmitRecord(rec);
								}

							}

						}

						/*

						The below code is commented by Saish.N on 24OCT2014 as the alert message is already commented

						//case# 20127132 end
						if(POsearchresults!=null && POsearchresults!='')
						{
							for (var x = 0; x < POsearchresults.length; x++) 
							{ 
								var poline = POsearchresults[x].getValue('line');

								if(poline==Lineno)
								{
									actqty = POsearchresults[x].getValue('quantity');
									LineLoc = POsearchresults[x].getValue('location');
									break;
								}
							}
						}


						//nlapiLogExecution('ERROR', 'Time Stamp before open task search',TimeStampinSec());

						//Added on 21Feb 2012 by suman.
						//To Check weather for the given PO and line# do v have any record in open task or not.


						var RecExist=GetRecord(Poid,Lineno,opentaskrecords);

						//nlapiLogExecution('ERROR', 'Time Stamp after open task search',TimeStampinSec());
						if(RecExist=='T')
						{
							var Location;
							var Whflag;
							if(LineLoc!='' && LineLoc!=null)
							{
								for(var i =0; i<Locsearchresults.length>0; i++)
								{					
									if(LineLoc == Locsearchresults[i].getValue('internalid'))
									{						
										Whflag = "";
										Whflag=Locsearchresults[i].getValue('custrecord_ebizwhsite');

										if(Whflag !='' && Whflag=='T')
										{
											if(actqty !=null && actqty !='')
											{
												if (parseFloat(ordqty) < parseFloat(actqty)) 
												{
													//alert("Line #" + Lineno + "  In process");
													//alert("Po Line#:" + Lineno + " is already considered for inbound process in WMS, so you cannot decrement the quantity.");
													//return false;
												}
											}
										}					
									}
								}
							}
						}*/
					}			
				}			
			}
			// Case# 201410680 starts
			var location = nlapiGetFieldValue('location');
			var itemStatusFilters = new Array();
			itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
			itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
			if(location != null && location != '')
				itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',location);
			var itemStatusColumns = new Array();
			itemStatusColumns[0] = new nlobjSearchColumn('internalid');
			itemStatusColumns[1] = new nlobjSearchColumn('name');
			itemStatusColumns[0].setSort();
			itemStatusColumns[1].setSort();
			var headitemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);
			for (var s = 1; s <= lineCnt; s++) 
			{
				var linelocation = nlapiGetLineItemValue('item', 'location', s);
				var itemstatus=nlapiGetLineItemValue('item','custcol_ebiznet_item_status',s);
				//alert(itemstatus);
				if(itemstatus != null && itemstatus != '')
				{
				if(linelocation!=null && linelocation!='' && linelocation!=location){
					var itemStatusFilters = new Array();
					itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
					itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
					if(linelocation != null && linelocation != '')
						itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',linelocation);

					var itemStatusColumns = new Array();
					itemStatusColumns[0] = new nlobjSearchColumn('internalid');
					itemStatusColumns[1] = new nlobjSearchColumn('name');
					itemStatusColumns[0].setSort();
					itemStatusColumns[1].setSort();
					var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);
					if (itemStatusSearchResult != null){
						var errorflag='F';
						for (var i = 0; i < itemStatusSearchResult.length; i++) 
						{		
							//if(itemstatus !=itemStatusSearchResult[i].getValue('name'))
							if(itemstatus ==itemStatusSearchResult[i].getId())
							{
								errorflag='T';
							}
						}
						if(errorflag=='F'){
							alert('Item Status not match with Location');
							return false;
						}
					}
					else
					{
						alert('Item Status not match with Location');
						return false;
					}
				}
				else{
					if (headitemStatusSearchResult != null){
						var errorflag='F';
						for (var i = 0; i < headitemStatusSearchResult.length; i++) 
						{		
							//if(itemstatus !=itemStatusSearchResult[i].getValue('name'))
							if(itemstatus ==headitemStatusSearchResult[i].getId())
							{
								errorflag='T';
							}
						}
						if(errorflag=='F'){
							alert('Item Status not match with Location');
							return false;
						}
					}
					else
					{
						alert('Item Status not match with Location');
						return false;
						}
					}
				}
			}
			// Case# 201410680 end
		}
	} 
	catch (exps) 
	{
		alert('Error ' + JSON.stringify(exps));
		return false;
	}

	return true;
}

/**
 * @param type
 * @param name
 * @returns {Boolean}
 */
function onAddLine(type,name)
{			
	//alert(type);
	if(type == 'item')
	{ 

		var itemstatus=nlapiGetCurrentLineItemValue('item','custcol_ebiznet_item_status');
		var packcode=nlapiGetCurrentLineItemValue('item','custcol_nswmspackcode');
		var isLineClosed=nlapiGetCurrentLineItemValue('item','isclosed');
		var isNewLine=nlapiGetCurrentLineItemValue('item','line');
		var itemID = nlapiGetCurrentLineItemValue('item','item');
		//alert(itemID);
		if(itemID!=null && itemID!='')
		{
			//var OrdQty1=nlapiGetCurrentLineItemValue('purchaseorder','quantity');
			//alert('purchaseorderOrdQty'+OrdQty1);
			var location = nlapiGetFieldValue('location');
			var mwhsiteflag;
			if(location!=null && location!='')
			{
				var fields = ['custrecord_ebizwhsite'];

				var locationcolumns = nlapiLookupField('Location', location, fields);
				if(locationcolumns!=null)
					mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
			}

			var Poid = nlapiGetFieldValue('id');


			//alert('isLineClosed : '+ isLineClosed);
			//alert('isNewLine : '+ isNewLine);

			var postatus = nlapiGetFieldValue('status');
			//alert('postatus : '+ postatus);
			if(postatus == 'Closed')
			{
				if((isLineClosed=='T') ||(isNewLine=='' ) || (isNewLine == null)){
					alert('PO is Closed,not allowed to add more lines ');
					return false;
				}

			}
			if(mwhsiteflag=='T')
			{
				var lineCnt = nlapiGetLineItemCount('item');
				//alert('lineCnt'+lineCnt);


				//	var ordqty = nlapiGetLineItemValue('item', 'quantity', s);
				var OrdQty = nlapiGetCurrentLineItemValue('item', 'quantity');	

				var itemtype=nlapiGetCurrentLineItemValue('item','itemtype');		

				if( itemtype!='Description' && itemtype!='Discount' && itemtype!='Payment' && itemtype!='Markup' && itemtype!='Subtotal' && itemtype!='NonInvtPart' )
				{
					if(OrdQty < 0 || OrdQty == 0 )
					{
						alert('Quantity should be greater than 0');
						return false;
					}
					if(itemstatus == "" || itemstatus == null)
					{
						alert('Please Select Item Status');
						return false;
					}
					if(packcode == "" || packcode == null)
					{
						alert('Please Select packcode');
						return false;
					}
				}
				// Case# 20148728 starts
				var location = nlapiGetFieldValue('location');
				var linelocation=nlapiGetCurrentLineItemValue('item','location');
				var itemStatusFilters = new Array();
				itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
				itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
				if(linelocation != null && linelocation != '')
					itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',linelocation);
				else if(location != null && location != '')
					itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',location);
				var itemStatusColumns = new Array();
				itemStatusColumns[0] = new nlobjSearchColumn('internalid');
				itemStatusColumns[1] = new nlobjSearchColumn('name');
				itemStatusColumns[0].setSort();
				itemStatusColumns[1].setSort();
				var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);
				if (itemStatusSearchResult != null){
					var errorflag='F';
					for (var i = 0; i < itemStatusSearchResult.length; i++) 
					{		
						//if(itemstatus !=itemStatusSearchResult[i].getValue('name'))
						if(itemstatus ==itemStatusSearchResult[i].getId())
						{
							errorflag='T';
						}
					}
					if(errorflag=='F'){
						alert('Item Status not match with Location');
						return false;
					}
				}
				// Case# 20148728 end
				var lineNo = nlapiGetCurrentLineItemValue('item','line');
				var poItemUOM = nlapiGetCurrentLineItemValue('item','units');

				//alert('poItemUOM:'+poItemUOM);
				var vbaseuom='';
				var vbaseuomqty='';
				var vuomqty='';
				var vuomlevel='';
				if(poItemUOM!=null && poItemUOM!='null' && poItemUOM!='')
				{
					var eBizItemDims=geteBizItemDimensions(itemID);
					if(eBizItemDims!=null&&eBizItemDims.length>0)
					{
						for(z=0; z < eBizItemDims.length; z++)
						{
							if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
							{
								vbaseuom = eBizItemDims[z].getText('custrecord_ebizuomskudim');
								vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
							}

							if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
							{
								vuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
								vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
							}
						}

						if(vuomqty==null || vuomqty=='' || isNaN(vuomqty))
							vuomqty=vbaseuomqty;



					}
				}
				// case# 201415439
				if(vuomqty==null || vuomqty=='' || isNaN(vuomqty))
					vuomqty=1;
				//alert(Poid);
				//alert(lineNo);
				if(lineNo!=null&&lineNo!=""&&lineNo!="null"&&Poid!=null&&Poid!=""&&Poid!="null") 
				{

					var searchRecords = getRecordDetails(Poid,lineNo,'noneof');
					if(searchRecords != null && searchRecords != '')
					{
						var vChknQty = searchRecords[0].getValue('custrecord_orderlinedetails_checkin_qty',null,'sum');
						//alert('OrdQty '+OrdQty);
						//alert('vChknQty '+vChknQty);
						vChknQty = (parseFloat(vChknQty)/parseFloat(vuomqty));
						if(parseInt(OrdQty) < parseInt(vChknQty)) {
							//alert('if');
							alert("This Line Item is already considered for Inbound process in WMS, so you cannot Change it.");
							return false;		
						}
					}
					/*else
				return true;*/
				}

				//var Poid = nlapiGetFieldValue('id');
				//alert('Poid'+Poid);
				//case 20126601 start
				if(Poid !=null && Poid !='')
				{
					var lineNo = nlapiGetCurrentLineItemValue('item','line');
					//var lineCnt = nlapiGetLineItemCount('item');
					//alert('lineNo' +lineNo);
					//var searchRecords = getRecordDetails(Poid,lineNo,'noneof');
					if(lineNo!=null && lineNo!='')
					{
						var searchRecords = getRecordDetailsWithCheckQty(Poid,lineNo);

						//alert('searchRecords'+searchRecords);
						if(searchRecords != null && searchRecords != '' && searchRecords.length>0)
						{	
							var vChknQty = searchRecords[0].getValue('custrecord_orderlinedetails_checkin_qty',null,'sum');

							var searchresults = nlapiLoadRecord('purchaseorder', Poid);

							for(var s = 1; s <=lineCnt; s++) {

								var vLineno=searchresults.getLineItemValue('item','line',s);
								var vOrderQty=searchresults.getLineItemValue('item','quantity',s);
								var vlinechangedQty=nlapiGetCurrentLineItemValue('item','quantity');
								var vTotCheckedQty=0;

								vTotCheckedQty = searchRecords[0].getValue('custrecord_orderlinedetails_checkin_qty',null,'sum');
								if(vTotCheckedQty == null || vTotCheckedQty == '')
									vTotCheckedQty=0;
								//alert("vlinechangedQty @ addline:" + vlinechangedQty);
								//alert("vOrderQty @ Addline:" + vOrderQty);
								//case# 20127103 starts (if line number if condition added)
								if(vLineno==lineNo)
								{	
									//if(parseInt(vlinechangedQty) < parseInt(vOrderQty)) {
									vTotCheckedQty = (parseFloat(vTotCheckedQty)/parseFloat(vuomqty));
									if(parseInt(vlinechangedQty) < parseInt(vTotCheckedQty)) {
										//alert("vOrderQty in if:" + vOrderQty);
										alert("This Line Item is already considered for Inbound process in WMS, so you cannot Change it.");
										nlapiSetCurrentLineItemValue('item', 'quantity',vOrderQty, false);
										return false;
									}
								}
								//case# 20127103 end

							}
						}
					}//case 20126601 end

				}
			}
		}
	}
	return true;
}


/**
 * @param type
 * @param name
 * @param numItems
 */
function onDropChangeType(type,name,numItems)
{  

	var itemId;
	
	//var lineCnt = nlapiGetLineItemCount('item');
	
	//alert('lineCnt:' + lineCnt);
	if((type =='item') && name == 'item')
	{ 
		//commented by suman as nlapiGetCurrentLineITemValue itself points to the current index so no need to pass line no again.
//		itemId=nlapiGetCurrentLineItemValue('item','item',numItems);

		itemId=nlapiGetCurrentLineItemValue('item','item');

		if(itemId != null && itemId!='')
		{
			//Code to set default UOM			
			var SearchResults=GetRecords(itemId);
			if(SearchResults!= null && SearchResults != '')
			{
				var uomvalue;
				var checkvalue;
				var vPackcode;
				for(var i=0;i<SearchResults .length;i++)
				{
					uomvalue =SearchResults[i].getValue('custrecord_ebizbaseuom');
					vPackcode =SearchResults[i].getValue('custrecord_ebizpackcodeskudim');
					nlapiLogExecution('ERROR', 'uomvalue ', uomvalue);
					if(uomvalue =='T')
					{
						checkvalue=SearchResults[i].getText('custrecord_ebizuomskudim');
						nlapiSetCurrentLineItemValue('item','custcol_nswmssobaseuom',checkvalue);
						if(vPackcode !=null && vPackcode!='')
							nlapiSetCurrentLineItemValue('item','custcol_nswmspackcode',vPackcode);
						nlapiLogExecution('ERROR', 'checkvalue', checkvalue);
					}			 
				}
			}
			/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
			var seleteditem = nlapiGetCurrentLineItemValue('item', 'item');		
			var fromLocation = nlapiGetFieldValue('location');
			var mwhsiteflag;
			if(fromLocation!=null && fromLocation!='')
			{
				var fields = ['custrecord_ebizwhsite'];

				var locationcolumns = nlapiLookupField('Location', fromLocation, fields);
				if(locationcolumns!=null)
					mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
			}
			if(mwhsiteflag=='T')
			{
				//var toLocation = nlapiGetFieldValue('transferlocation');
				var searchresult=SetItemStatus("PurchaseOrder",seleteditem,fromLocation,null);
				// alert('searchresult1' + searchresult);
				if(searchresult != null && searchresult != '')
				{	
					nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',searchresult[0],false);
					nlapiSetCurrentLineItemValue('item','custcol_nswmspackcode',searchresult[1],false);
				}
				/*** upto here ***/
				var itemtype=nlapiGetCurrentLineItemValue('item','itemtype');
				if( itemtype!='Description' && itemtype!='Discount' && itemtype!='Payment' && itemtype!='Markup' && itemtype!='Subtotal' )
				{
					//Code to set default Item status and packcode

					var fields = ['custitem_ebizdefpackcode','custitem_ebizdefskustatus'];
					var columns = nlapiLookupField('item', itemId, fields);

					if(columns != null && columns != '')
					{
						var skuStatus = columns.custitem_ebizdefskustatus;
						var packCode= columns.custitem_ebizdefpackcode; 

						if(skuStatus != null && skuStatus != '')
							nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',skuStatus,false);

						if(packCode != null && packCode != '')
							nlapiSetCurrentLineItemValue('item','custcol_nswmspackcode',packCode,false);				 
					}
				}
			}
		}

		if(name == 'isclosed')
		{

			//var itemid = itemId=nlapiGetCurrentLineItemValue('item','item');
			var lineNo = nlapiGetCurrentLineItemValue('item','line');
			var Poid = nlapiGetFieldValue('id');

			var isClosed=nlapiGetCurrentLineItemValue('item','isclosed');

			if(isClosed == 'T')
			{

				var transactionFilters = new Array();
				transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', Poid);
				transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineNo);

				var transactionColumns = new Array();
				transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
				transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');

				var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);

				if(transactionSearchresults!=null && transactionSearchresults!='')
				{
					alert('The Selected Line# is Processing, So cannot Close it.');
					nlapiSetCurrentLineItemValue('item', 'isclosed','F', false);
					return false;
				}


			}
		}

		/*	if(name == 'quantity'){

		var Poid = nlapiGetFieldValue('id');
		//alert('Poid'+Poid);
		//case 20126601 start
		if(Poid !=null && Poid !='')
		{
			var lineNo = nlapiGetCurrentLineItemValue('item','line');
			var lineCnt = nlapiGetLineItemCount('item');
			//alert('lineNo'+lineNo);
			//var searchRecords = getRecordDetails(Poid,lineNo,'noneof');
			var searchRecords = getRecordDetailsWithCheckQty(Poid,lineNo);

			//alert('searchRecords'+searchRecords);
			if(searchRecords != null && searchRecords != '' && searchRecords.length>0)
			{	
				var vChknQty = searchRecords[0].getValue('custrecord_orderlinedetails_checkin_qty',null,'sum');

				for(var s = 1; s <=lineCnt; s++) {
					var searchresults = nlapiLoadRecord('purchaseorder', Poid);
					var vLineno=searchresults.getLineItemValue('item','line',s);
					var vOrderQty=searchresults.getLineItemValue('item','quantity',s);
					var vlinechangedQty=nlapiGetCurrentLineItemValue('item','quantity');
					var vTotCheckedQty=0;

					vTotCheckedQty = searchRecords[0].getValue('custrecord_orderlinedetails_checkin_qty',null,'sum');
					if(vTotCheckedQty == null || vTotCheckedQty == '')
						vTotCheckedQty=0;
					//alert("vLineno:" + vLineno);
					//alert("lineNo:" + lineNo);

					if(vLineno==lineNo)
					{	
						//alert("vlinechangedQty:" + vlinechangedQty);
						//alert("vTotCheckedQty:" + vTotCheckedQty);
						if(parseInt(vlinechangedQty) < parseInt(vTotCheckedQty)) {

							//alert("vOrderQty in if:" + vOrderQty);
							alert("This Line Item is already considered for Inbound process in WMS, so you cannot Change it.");
							nlapiSetCurrentLineItemValue('item', 'quantity',vOrderQty, false);
							return false;
						}
					}
					//case# 20127103 end

				}
			}
		}//case 20126601 end

	}*/
		//sri added case# 20127121 starts
		if(name == 'item'){

			var Poid = nlapiGetFieldValue('id');
			//alert('Poid'+Poid);

			if(Poid !=null && Poid !='')
			{
				var lineNo = nlapiGetCurrentLineItemValue('item','line');
				var lineCnt = nlapiGetLineItemCount('item');
				//alert('lineNo'+lineNo);
				var searchRecords = getRecordDetails(Poid,lineNo,'noneof');
				//alert('searchRecords'+searchRecords);
				if(searchRecords != null && searchRecords != '' && searchRecords.length>0)
				{	var searchresults = nlapiLoadRecord('purchaseorder', Poid);
				for(var s = 1; s <=lineCnt; s++) 
				{
					//var searchresults = nlapiLoadRecord('purchaseorder', Poid);
					var vLineno=searchresults.getLineItemValue('item','line',s);
					//var vOrderQty=searchresults.getLineItemValue('item','quantity',s);
					//var vlinechangedQty=nlapiGetCurrentLineItemValue('item','quantity');
					var Actualitem=searchresults.getLineItemValue('item','item',s);
					var vchangeditem=nlapiGetCurrentLineItemValue('item','item');

					if(vLineno==lineNo)
					{	
						if(Actualitem != vchangeditem)
						{
							//alert("vOrderQty in if:" + vOrderQty);
							alert("This Line Item is already considered for Inbound process in WMS, so you cannot Change it.");
							nlapiSetCurrentLineItemValue('item', 'item',Actualitem, false);
							return false;
						}
					}


				}
				}
			}

		}
		// sri case # 20127121 ends
	}
	else
	{

		
		itemId=nlapiGetCurrentLineItemValue('item','item');
		var vitemstatus = nlapiGetCurrentLineItemValue('item', 'custcol_ebiznet_item_status');	
		if((itemId != null && itemId!='')&&(vitemstatus == null || vitemstatus == ''))
		{
			//var seleteditem = nlapiGetCurrentLineItemValue('item', 'item');		
			var fromLocation = nlapiGetFieldValue('location');
			var mwhsiteflag;
			if(fromLocation!=null && fromLocation!='')
			{
				var fields = ['custrecord_ebizwhsite'];

				var locationcolumns = nlapiLookupField('Location', fromLocation, fields);
				if(locationcolumns!=null)
					mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
			}
			if(mwhsiteflag=='T')
			{
				//var toLocation = nlapiGetFieldValue('transferlocation');
				var searchresult=SetItemStatus("PurchaseOrder",itemId,fromLocation,null);
				// alert('searchresult1' + searchresult);
				if(searchresult != null && searchresult != '')
				{	
					nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',searchresult[0],false);
					nlapiSetCurrentLineItemValue('item','custcol_nswmspackcode',searchresult[1],false);
				}
			}
		}
		
	}
}

/**
 * @param pointid
 * @returns
 */
function GetRecords(pointid) {  


	var filters= new Array();

	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', pointid);
	filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom',null,'is','T');

	var columns = new Array();    
	columns[0] = new nlobjSearchColumn('custrecord_ebizbaseuom');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpackcodeskudim');

	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns ); 
	return SearchResults;
	//nlapiLogExecution('ERROR', 'val1', SearchResults.length);

}

/**
 * @param PoId
 * @param lineno
 * @returns {String}
 */
function GetRecord(PoId,lineno,Searchrecord)
{
	try{
		var flag='F';

		if(Searchrecord!=null && Searchrecord!='')
		{
			for (var z = 0; z < Searchrecord.length; z++) 
			{ 
				var poline = Searchrecord[z].getValue('custrecord_line_no');

				if(poline==lineno)
				{					
					flag='T';
					return flag;
				}
			}
		}

		return flag;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetRecord function',exp);
	}
}


function GetPORecords(PoId)
{
	try{
		var flag='F';
		var filterOpenTask=new Array();
		filterOpenTask.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no',null,'is',PoId));
		//filterOpenTask.push(new nlobjSearchFilter('custrecord_line_no',null,'is',lineno));
		//filterOpenTask.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',[2]));
		//filterOpenTask.push(new nlobjSearchFilter('custrecord_act_end_date',null,'isempty',null));

		var columns = new Array();

		columns[0] = new nlobjSearchColumn('custrecord_line_no');

		var Searchrecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filterOpenTask,columns);

	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetPORecords function',exp);
	}

	return Searchrecord;
}


/**
 * client event when user removes line in an SO
 * @param type
 * @returns {Boolean}
 */

function DeletePoLine(type)
{

	var PoId=nlapiGetFieldValue('id');
	var lineNo = nlapiGetCurrentLineItemValue('item','line');
	//alert("lineNo;"+lineNo);
	if(lineNo!=null&&lineNo!=""&&lineNo!="null"&&PoId!=null&&PoId!=""&&PoId!="null") 
	{
		var searchRecords = getRecordDetails(PoId,lineNo);
		//var RecExist = GetRecord(PoId,lineNo);

		//var getLineNo=nlapiGetFieldValue('custbody_ebiz_lines_deleted');
		var afterconcatinate;

		if(searchRecords != null && searchRecords != '')
		{

			alert("This Line Item is already considered for Inbound process in WMS, so you cannot delete it.");
			return false;		
		}
		else
		{

			/*if(getLineNo==null||getLineNo=='')
				afterconcatinate=lineNo.toString()+',';
			else
				afterconcatinate=getLineNo+lineNo.toString()+',';
			nlapiSetFieldValue('custbody_ebiz_lines_deleted', afterconcatinate, 'F', true);	*/	
			return true;
		}
	}
	else
	{
		return true;
	}

}







/**
 * searching for records
 * @param soId
 * @param lineNo
 * @returns
 */
function getRecordDetails(poId,lineNo){

	var filter= new Array();
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poId));
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineNo));

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty',null,'sum');
	columns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty',null,'sum');
	columns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no',null,'group');


	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filter, columns);
	return searchRecords;
}

function getRecordDetails1(poId,lineNo){	

	var filter= new Array();
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poId));
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineNo));
	//case# 20126939 starts
	//case# 20127007 starts(filter condition changed from checkin_qty to putconf_qty)
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_putconf_qty', null, 'isnotempty'));
	//case# 20127007 end
	//filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_checkin_qty', null, 'isnotempty'));
	//case# 20126939 end

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');		

	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filter, columns);
	return searchRecords;
}
function getRecordDetailsWithCheckQty(poId,lineNo){

	//alert(poId);
	//alert(lineNo);

	var filter= new Array();
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poId));
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty',null,'sum');

	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filter, columns);
	return searchRecords;
}


function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}