/***************************************************************************
						eBizNET Solutions Inc 	
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_CheckInBatchNo.js,v $
<<<<<<< ebiz_RF_Cart_CheckInBatchNo.js
 *� $Revision: 1.2.2.7.4.9.2.40.2.3 $
 *� $Date: 2015/11/24 11:14:42 $
 *� $Author: deepshikha $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *� $Revision: 1.2.2.7.4.9.2.40.2.3 $
 *� $Date: 2015/11/24 11:14:42 $
 *� $Author: deepshikha $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.2.2.7.4.9.2.29
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_Cart_CheckInBatchNo.js,v $
 *� Revision 1.2.2.7.4.9.2.40.2.3  2015/11/24 11:14:42  deepshikha
 *� 2015.2 Issue Fix
 *� 201415093
 *�
 *� Revision 1.2.2.7.4.9.2.40.2.2  2015/11/06 15:49:43  skreddy
 *� case 201415347
 *� 2015.2  issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.40.2.1  2015/09/16 15:57:08  deepshikha
 *� 2015.2 issueFix
 *� 201414436
 *�
 *� Revision 1.2.2.7.4.9.2.40  2015/08/07 19:05:19  snimmakayala
 *� Case#: 201413571
 *�
 *� Revision 1.2.2.7.4.9.2.39  2015/08/03 15:44:21  skreddy
 *� Case# 201413785
 *� 2015.2 compatibility issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.38  2015/07/02 15:24:27  grao
 *� 2015.2 issue fixes  201413140
 *�
 *� Revision 1.2.2.7.4.9.2.37  2014/10/29 14:49:43  skavuri
 *� Case# 201410517 Std bundle issue fixed
 *�
 *� Revision 1.2.2.7.4.9.2.36  2014/10/17 13:01:06  skavuri
 *� Case# 201410581 Std bundle Issue fixed
 *�
 *� Revision 1.2.2.7.4.9.2.35  2014/09/26 15:05:32  sponnaganti
 *� Case# 201410517
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.34  2014/08/08 14:55:37  skreddy
 *� case # 20149881
 *� True Fabrications SB issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.33  2014/07/10 16:01:30  sponnaganti
 *� Case# 20149377
 *� Compatibility issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.32  2014/07/07 16:11:48  sponnaganti
 *� case# 20149288
 *� Comaptibility Issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.31  2014/07/03 15:24:48  skavuri
 *� Case# 20149238 Compatability Issue Fixed
 *�
 *� Revision 1.2.2.7.4.9.2.30  2014/06/18 13:50:43  grao
 *� Case#: 20148959  New GUI account issue fixes
 *�
 *� Revision 1.2.2.7.4.9.2.29  2014/06/13 08:26:19  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.2.2.7.4.9.2.28  2014/05/30 00:26:47  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.2.2.7.4.9.2.27  2014/05/27 14:11:55  sponnaganti
 *� case# 20148483
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.26  2014/04/28 14:33:34  rmukkera
 *� Case # 20145365
 *�
 *� Revision 1.2.2.7.4.9.2.25  2014/03/27 16:06:40  skavuri
 *� Case # 20127851 issue fixed
 *�
 *� Revision 1.2.2.7.4.9.2.24  2014/02/28 14:22:27  sponnaganti
 *� case# 20127403
 *� Standard Bundle Issue fix.
 *�
 *� Revision 1.2.2.7.4.9.2.23  2014/02/17 14:48:12  rmukkera
 *� Case # 20127169
 *�
 *� Revision 1.2.2.7.4.9.2.22  2014/02/06 15:32:20  nneelam
 *� case#  20127055
 *� std bundle issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.21  2014/02/03 14:10:49  rmukkera
 *� Case # 20126963
 *�
 *� Revision 1.2.2.7.4.9.2.20  2014/01/24 13:41:22  schepuri
 *� 20126901
 *� standard bundle issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.19  2013/12/24 15:13:17  rmukkera
 *� Case # 20126458
 *�
 *� Revision 1.2.2.7.4.9.2.18  2013/11/08 14:36:43  schepuri
 *� Case# 20125257
 *� Afosa SB issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.17  2013/10/24 13:00:38  rmukkera
 *� Case# 20125285
 *�
 *� Revision 1.2.2.7.4.9.2.16  2013/10/23 16:12:44  nneelam
 *� Case# 20125226
 *� Variable not declared.
 *�
 *� Revision 1.2.2.7.4.9.2.15  2013/10/22 15:06:24  skreddy
 *� Case# 20125226
 *� standard bundle issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.14  2013/10/18 08:58:26  schepuri
 *� 20125027�
 *�
 *� Revision 1.2.2.7.4.9.2.13  2013/10/07 13:49:04  schepuri
 *� 20124823
 *�
 *� Revision 1.2.2.7.4.9.2.12  2013/09/26 15:47:49  rmukkera
 *� Case# 20124514�
 *�
 *� Revision 1.2.2.7.4.9.2.11  2013/09/18 14:34:18  rmukkera
 *� Case# 20124300
 *�
 *� Revision 1.2.2.7.4.9.2.10  2013/09/16 15:42:13  rmukkera
 *� Case# 20124300
 *�
 *� Revision 1.2.2.7.4.9.2.9  2013/08/26 15:50:00  skreddy
 *� Case# 20124071
 *� standard bundle- issue fix
 *�
 *� Revision 1.2.2.7.4.9.2.8  2013/08/21 14:22:59  skreddy
 *� Case# 20123285
 *�  restict space charater in lot# feild
 *�
 *� Revision 1.2.2.7.4.9.2.7  2013/08/02 15:42:02  rmukkera
 *� Case# 20123695
 *� Issue fix for
 *� RF Cart Putaway :: When we enter the Cart LP screen is displaying invalid Cart LP message.
 *�
 *� Revision 1.2.2.7.4.9.2.6  2013/07/15 11:39:14  snimmakayala
 *� Case# 20123430
 *� GFT UAT ISSUE
 *�
 *� Revision 1.2.2.7.4.9.2.5  2013/06/11 14:30:40  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.2.2.7.4.9.2.4  2013/06/04 07:26:55  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Lot Parsing CR for PCT
 *�
 *� Revision 1.2.2.7.4.9.2.3  2013/04/17 16:04:01  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.2.2.7.4.9.2.2  2013/03/01 14:34:59  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Merged from FactoryMation and change the Company name
 *�
 *� Revision 1.2.2.7.4.9.2.1  2013/02/26 13:02:23  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Marged from Boombah.
 *�
 *� Revision 1.2.2.7.4.9  2013/02/07 15:10:34  schepuri
 *� CASE201112/CR201113/LOG201121
 *� disabling ENTER Button func added
 *�
 *� Revision 1.2.2.7.4.8  2013/02/07 08:42:58  skreddy
 *� CASE201112/CR201113/LOG201121
 *�  RF Lot auto generating FIFO enhancement
 *�
 *� Revision 1.2.2.7.4.7  2013/01/08 15:41:28  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Lexjet issue for batch entry irrespective of location
 *�
 *� Revision 1.2.2.7.4.6  2012/12/24 15:19:27  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Merged code form 2012.2 branch.
 *�
 *� Revision 1.2.2.7.4.5  2012/12/11 14:53:30  schepuri
 *� CASE201112/CR201113/LOG201121
 *� batchentry is not filtering according to whlocation
 *�
 *� Revision 1.2.2.7.4.4  2012/11/01 14:55:34  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.2.2.7.4.3  2012/09/27 10:53:53  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.2.2.7.4.2  2012/09/26 12:42:58  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.2.2.7.4.1  2012/09/21 14:57:16  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.2.2.7  2012/09/04 07:16:16  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Missing TranType while passing to query string.
 *�
 *� Revision 1.2.2.6  2012/09/03 13:45:29  schepuri
 *� CASE201112/CR201113/LOG201121
 *� added date stamp
 *�
 *� Revision 1.2.2.5  2012/06/28 14:33:46  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Missing parameters while passing to query string.
 *�
 *� Revision 1.2.2.4  2012/04/20 10:36:12  schepuri
 *� CASE201112/CR201113/LOG201121
 *� changing the Label of Batch #  field to Lot#
 *�
 *� Revision 1.2.2.3  2012/03/16 13:56:23  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Disable-button functionality is been added.
 *�
 *� Revision 1.2.2.2  2012/02/22 12:14:37  schepuri
 *� CASE201112/CR201113/LOG201121
 *� function Key Script code merged
 *�
 *� Revision 1.2  2012/02/16 10:26:42  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Added FunctionkeyScript
 *�
 *� Revision 1.1  2012/02/02 08:53:39  rmukkera
 *� CASE201112/CR201113/LOG201121
 *�   cartlp new file
 *�
 *
 ****************************************************************************/


function CheckInBatchNo(request, response){
	if (request.getMethod() == 'GET') {

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
//		var getItemQuantity = request.getParameter('hdnQuantity');
//		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getWHLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG','getWHLocation',getWHLocation);
		var getPOLineItemStatusText = request.getParameter('custparam_polineitemstatustext');//Case# 201410581

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		nlapiLogExecution('DEBUG', 'getPOItem', getPOItem);

		var st0,st1,st2,st3,st4;

		if( getLanguage == 'es_ES')
		{

			st0 = "ENTRADAD LOTNO";
			st1 = "ART&#205;CULO";
			st2 = "INGRESAR LOTE";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";

		}
		else
		{
			st0 = "CHECKIN LOTNO";
			st1 = "ITEM";
			st2 = "ENTER LOT#:";
			st3 = "SEND";
			st4 = "PREV";

		}


		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title> " + st0 +  " </title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterbatch').focus();";     

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";       

		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " :  <label>" + getPOItem + "</label></td>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getPOLineItemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnitemstatustext' value='" + getPOLineItemStatusText + "'>";//Case# 201410581
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbatch' id='enterbatch' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterbatch').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		try {
			var POarray = new Array();

			var getLanguage = request.getParameter('hdngetLanguage');
			POarray["custparam_language"] = getLanguage;
			nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


			var st5,st6;
			if( getLanguage == 'es_ES')
			{
				st5 = "NO V&#193;LIDO LOTE N";
				st6 = "Embalaje Men&#250;";
			}
			else
			{
				st5 = "INVALID LOT NO";
				st6 = "LOT NO IS NULL";
			}


			var optedEvent = request.getParameter('cmdPrevious');
			var getBatch = request.getParameter('enterbatch');
			var ActualScanBatch = request.getParameter('enterbatch');

			nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
			nlapiLogExecution('DEBUG', 'getBatch', getBatch);
			POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
			POarray["custparam_poid"] = request.getParameter('custparam_poid');
			POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
			POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
			POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
			POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
			POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
			POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
			POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode');//request.getParameter('custparam_polinepackcode');
			POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
			POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
			POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
			POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
			POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
			POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');

			POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
			nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);
			nlapiLogExecution('DEBUG', 'Fetched Item Id', POarray["custparam_fetcheditemid"]);
			POarray["custparam_actscanbatchno"] = ActualScanBatch;
			nlapiLogExecution('DEBUG', 'ActualScanBatch', ActualScanBatch);
			POarray["custparam_polineitemstatustext"] = request.getParameter('hdnitemstatustext');//Case# 201410581

			POarray["custparam_error"] = st5;
			POarray["custparam_screenno"] = 'CRT6';
			POarray["custparam_polineitemstatusValue"] = request.getParameter('hdnItemStatus');
			var ItemType = '';					
			var batchflg = '';
			if (optedEvent == 'F7') {
				nlapiLogExecution('DEBUG', 'REPLENISHMENT LOCATION F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinitemsts', 'customdeploy_ebiz_rf_cart_checkinitemsts', false, POarray);
			}
			else {
				//case # 20125226, ActualBatch variable which is not declared replaced with ActualScanBatch variable in if condition.
				if(ActualScanBatch != null && ActualScanBatch != '')
				{
					//end	
					//case 20123285 start
					var result=ValidateSplCharacter(ActualScanBatch,'Lot #');
					nlapiLogExecution('ERROR','result',result);
					if(result == false)
					{
						POarray["custparam_error"] = 'SPECIAL CHARS NOT ALLOWED IN LOT#';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
					else
					{//end
						if(POarray["custparam_pointernalid"] !=null && POarray["custparam_pointernalid"] !='')
						{
							//case no 20125257
							var fields = ['recordType','location'];
							var columns= nlapiLookupField('transaction',POarray["custparam_pointernalid"],fields);
							var trantype= '';
							var vLocation = '';
							var ToLocation = '';
							
							try
							{
									var filterBatchn=new Array();


									filterBatchn[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',ActualScanBatch);

									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');

									columns[1] = new nlobjSearchColumn('isinactive');

									var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatchn,columns);

									if(rec!=null && rec !='' && rec != 'null')
									{   

										var inactivflag = rec[0].getValue('isinactive');
										nlapiLogExecution('DEBUG', 'Entered Item batch into if', ActualScanBatch);


										if(inactivflag == 'T')
										{
											/*POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');*/
											POarray["custparam_error"] = "Inactive Lot";
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											nlapiLogExecution('DEBUG', 'Entered Item batch', ActualScanBatch);
											return;

										}
									}
								
							}
							catch(exc)
							{
									nlapiLogExecution('DEBUG', 'exc', exc);
							}
							
							//var trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');
							if(columns != null && columns != '')
							{
								trantype = columns.recordType;
								vLocation = columns.location;
								//ToLocation = columns.transferlocation;
							}


							nlapiLogExecution('DEBUG', 'Before Parsing ',getBatch);
							nlapiLogExecution('DEBUG', 'trantype ',trantype);
							nlapiLogExecution('DEBUG', 'vLocation ',vLocation);
							nlapiLogExecution('DEBUG', 'ToLocation ',ToLocation);
							if(trantype=='purchaseorder')
							{
								var vendor = nlapiLookupField('purchaseorder',POarray["custparam_pointernalid"],'entity');

								var getBatch = GetLotParsingResults(POarray["custparam_fetcheditemid"],vendor,getBatch);
								nlapiLogExecution('DEBUG', 'After Parsing ',getBatch);
							}
							var ctx = nlapiGetContext();
							if(ctx != null && ctx != '')
							{
								if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
									vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
								nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
							}

							//case # 20124071 start
							if(trantype=='transferorder')
							{
								
								var fields1 = ['transferlocation'];
								var columns1= nlapiLookupField('transaction',POarray["custparam_pointernalid"],fields1);
								ToLocation = columns1.transferlocation;
								
								var fields = ['recordType', 'custitem_ebizbatchlot'];
								var columns = nlapiLookupField('item', POarray["custparam_fetcheditemid"], fields);
								if(columns != null && columns != '')
								{
									ItemType = columns.recordType;					
									batchflg = columns.custitem_ebizbatchlot;
								}
								if(ItemType == "inventoryitem" && batchflg == 'T')
								{
									if(vLocation!=null && vLocation!='')
									{
										var fields = ['custrecord_ebizwhsite'];

										var locationcolumns = nlapiLookupField('Location', vLocation, fields);
										if(locationcolumns!=null)
											mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
									}
									if(ToLocation!=null && ToLocation!='')
									{
										var fields = ['custrecord_ebizwhsite'];

										var locationcolumns = nlapiLookupField('Location', ToLocation, fields);
										if(locationcolumns!=null)
											Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;
									}
									nlapiLogExecution('Error', 'mwhsiteflag', mwhsiteflag);
									nlapiLogExecution('Error', 'Tomwhsiteflag', Tomwhsiteflag);
									nlapiLogExecution('Error', 'POarray[custparam_pointernalid]', POarray["custparam_pointernalid"]);

									if(mwhsiteflag == 'T' && Tomwhsiteflag == 'T')
									{
										batchflg = 'T';
									}
									else
										batchflg = 'F';
								}
								if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == 'T')
								{
									var IsValidSerailNumber='F';
									var trecord = nlapiLoadRecord('transferorder', POarray["custparam_pointernalid"]);

									var links=trecord.getLineItemCount('links');
									if(links!=null  && links!='')
									{
										for(var j=0;j<links &&  IsValidSerailNumber=='F';j++)
										{
											var id=trecord.getLineItemValue('links','id',(parseInt(j)+1));
											var linktype = trecord.getLineItemValue('links','type',(parseInt(j)+1));

											nlapiLogExecution('DEBUG', 'linktype',linktype);
											nlapiLogExecution('DEBUG', 'id',id);

											if(linktype=='Item Fulfillment' || linktype=='Item Shipment')
											{
												var frecord = nlapiLoadRecord('itemfulfillment', id);
// case no start 20126901
												if(vAdvBinManagement)
												{
													//case # 20127403 getting the linecount and looping that no of times.
												var itemlinecount=frecord.getLineItemCount('item');	
													for(var n=1; n<=itemlinecount; n++){		
														frecord.selectLineItem('item', n);
														//frecord.selectLineItem('item', 1);   before 20127403
														var compSubRecord = frecord.viewCurrentLineItemSubrecord('item','inventorydetail');
														//Case # 20127169?Start
														if(compSubRecord !=null && compSubRecord!='' && compSubRecord!='null')
														{
															//Case # 20127169?End
															var polinelength = compSubRecord.getLineItemCount('inventoryassignment');
															nlapiLogExecution('ERROR', 'polinelength', polinelength);
															var itemfulfilserialno;
															var itemTempfulfilserialno;
															for(var k=1;k<=polinelength ;k++)
															{
																itemfulfilserialno=compSubRecord.getLineItemText('inventoryassignment','issueinventorynumber',k);
																nlapiLogExecution('ERROR', 'itemfulfilserialno', itemfulfilserialno);
																if(itemfulfilserialno!=null && itemfulfilserialno!='')
																{
																	if(itemTempfulfilserialno == null || itemTempfulfilserialno == '')
																		itemTempfulfilserialno=itemfulfilserialno;
																	else										 
																		itemTempfulfilserialno= itemTempfulfilserialno + ',' + itemfulfilserialno;

																}
																nlapiLogExecution('ERROR', 'itemTempfulfilserialno', itemTempfulfilserialno);
															}
															var  serialnumbers= itemTempfulfilserialno;
															if(serialnumbers!=null && serialnumbers!='')
															{
																//case # 20127055,,,......split based on ,
																var tserials=serialnumbers.split(',');
																//nlapiLogExecution('DEBUG', 'serialnumbers',tserials[0]);
																if(tserials!=null && tserials!='' && tserials.length>0)
																{
																	//Case # 20126963? Start
																	if(tserials.indexOf(ActualScanBatch)!=-1)
																	{
																		nlapiLogExecution('DEBUG', 'getSerialNo111111',ActualScanBatch);
																		//Case # 20126963? End
																		IsValidSerailNumber='T';
																		break;
																	}
																}
															}
															// case no end 20126901
															//Case # 20127169?End
														}
													}
													//Case # 20127169?Start
												}
												else
												{
													var fitemcount=frecord.getLineItemCount('item');
													for(var f=1;f<=fitemcount;f++)
													{
														var fitem=frecord.getLineItemValue('item','item',f);
														var fline=frecord.getLineItemValue('item','orderline',f);
														var pofline= fline-1;

													nlapiLogExecution('DEBUG', 'fitem',fitem);
													nlapiLogExecution('DEBUG', 'POarray[custparam_fetcheditemid]',POarray["custparam_fetcheditemid"]);

													if(fitem==POarray["custparam_fetcheditemid"]) //&& parseInt(getPOLineNo)==parseInt(pofline))
													{
														var  serialnumbers=frecord.getLineItemValue('item','serialnumbers',(parseInt(f)));
														nlapiLogExecution('DEBUG', 'serialnumbers',serialnumbers);
														/*if(serialnumbers!=null && serialnumbers!='')
												{
													if(serialnumbers.indexOf(ActualScanBatch)!=-1)
													{
														IsValidSerailNumber='T';
														break;
													}
												}*/
														if(serialnumbers!=null && serialnumbers!='')
														{
															var tserials=serialnumbers.split('');
															//nlapiLogExecution('DEBUG', 'serialnumbers',tserials[0]);
															if(tserials!=null && tserials!='' && tserials.length>0)
															{
																if(tserials.indexOf(ActualScanBatch)!=-1)
																{
																	nlapiLogExecution('DEBUG', 'getSerialNo111111',ActualScanBatch);
																	IsValidSerailNumber='T';
																	break;
																}
															}
														}
														else
														{
															if(ItemType == "inventoryitem" && batchflg == 'T')
															{
																nlapiLogExecution('DEBUG', 'serialnumbers new',serialnumbers);
																var filters = new Array();
																//filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'is', putPOId));
																filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', POarray["custparam_pointernalid"]));
																filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3])); //taskType = pick
																filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', [14])); // wmsStatusFlag=INBOUND/CHECK-IN
																filters.push(new nlobjSearchFilter('custrecord_ebiztask_sku', null, 'anyof', POarray["custparam_fetcheditemid"])); 
																filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconf_refno_ebiztask', null, 'is', id)); 


																	var columns = new Array();
																	columns[0] = new nlobjSearchColumn('custrecord_ebiztask_batch_no');

																	// execute the  search, passing null filter and return columns
																	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);

																	nlapiLogExecution('DEBUG', 'serialnumbers new1',serialnumbers);

																	var serialnumbers = '';
																	if(searchresults != null && searchresults != '')
																	{
																		serialnumbers = searchresults[0].getValue('custrecord_ebiztask_batch_no');

																	nlapiLogExecution('DEBUG', 'serialnumbers new new',serialnumbers);

																	if(serialnumbers.indexOf(ActualScanBatch)!=-1)
																	{
																		IsValidSerailNumber='T';
																		break;
																	}

																}


															}

															}
														}
													}
												}
											}
										}
									}

									if(IsValidSerailNumber=='F')
									{
										//POarray["custparam_error"] = "Matching Serial Number required.</br>The Serial number on a transfer order receipt must have been fulfilled.";
										POarray["custparam_screenno"] = 'CRT6';
										POarray["custparam_error"] = "Lot# is not matching with Lot# picked on the same TO ";
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										return; 
									}
								}


							}
						}//end



					}
					//case# 20149288 starts
					/*//Case# 20149238 starts
					var getValidBatch = getBatchNo(POarray["custparam_fetcheditemid"],POarray["custparam_polinepackcode"],
							POarray["custparam_whlocation"],getBatch);
					if(getValidBatch=='' ||getValidBatch=='null' || getValidBatch==null)
					{
						POarray["custparam_error"] = st8;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG','Invalid Lot#','Invalid Lot#');
						return;
					}
					//Case# 20149238 ends*/
					//case# 20149288 ends
					var getSearchBatch = getBatchNoByMfcLot(POarray["custparam_fetcheditemid"],POarray["custparam_polinepackcode"],
							POarray["custparam_whlocation"],ActualScanBatch);


					/*var getSearchBatch = getBatchNo(POarray["custparam_fetcheditemid"],POarray["custparam_polinepackcode"],
						POarray["custparam_whlocation"],getBatch);*/

					if(getSearchBatch != null && getSearchBatch!=''){
						//case# 20149288 starts
						/*var getValidBatch = getBatchNo(POarray["custparam_fetcheditemid"],POarray["custparam_polinepackcode"],
								POarray["custparam_whlocation"],getBatch);
						if(getValidBatch=='' ||getValidBatch=='null' || getValidBatch==null)
						{
							POarray["custparam_error"] = st8;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG','Invalid Lot#','Invalid Lot#');
							return;
						}*/
						//case# 20149288 ends
						POarray["custparam_mfgdate"] = getSearchBatch[0].getValue('custrecord_ebizmfgdate');
						POarray["custparam_expdate"] = getSearchBatch[0].getValue('custrecord_ebizexpirydate');
						POarray["custparam_bestbeforedate"] = getSearchBatch[0].getValue('custrecord_ebizbestbeforedate');
						POarray["custparam_fifodate"]=getSearchBatch[0].getValue('custrecord_ebizfifodate');
						POarray["custparam_lastdate"]=getSearchBatch[0].getValue('custrecord_ebizlastavldate');
						POarray["custparam_batchno"] = getBatch;
						POarray["custparam_fifocode"] = getSearchBatch[0].getValue('custrecord_ebizfifocode');

						var BatchDetails = new Array();
						BatchDetails[0] = getBatch;
						BatchDetails[1] = getSearchBatch[0].getValue('custrecord_ebizmfgdate');
						BatchDetails[2] = getSearchBatch[0].getValue('custrecord_ebizexpirydate');
						BatchDetails[3] = getSearchBatch[0].getValue('custrecord_ebizbestbeforedate');
						BatchDetails[4] = getSearchBatch[0].getValue('custrecord_ebizlastavldate');
						BatchDetails[5] = getSearchBatch[0].getValue('custrecord_ebizfifodate');
						BatchDetails[6] = getSearchBatch[0].getValue('custrecord_ebizfifocode');

						//response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
						CheckinLp(request.getParameter('custparam_fetcheditemid'), request.getParameter('custparam_poid'), request.getParameter('custparam_poitem'), request.getParameter('custparam_lineno'), request.getParameter('hdnPOInternalId'), request.getParameter('hdnPOQuantityEntered'), request.getParameter('hdnItemRemaininingQuantity'), request.getParameter('hdnItemPackCode'), request.getParameter('hdnItemStatus'), request.getParameter('hdnQuantity'), request.getParameter('hdnQuantityReceived'), request.getParameter('custparam_itemdescription'), request.getParameter('custparam_itemcube'), request.getParameter('hdnActualBeginDate'), request.getParameter('hdnActualBeginTime'), request.getParameter('hdnActualBeginTimeAMPM'),BatchDetails,request.getParameter('custparam_cartlpno'),POarray["custparam_actscanbatchno"],request,response);
					}
					else{
						if (getBatch != '') {
							if(POarray["custparam_fetcheditemid"]!=null && POarray["custparam_fetcheditemid"]!='')
							{
								//case# 201410517 (Taking default values for CaptureExpirydate,CaptureFifodate)
								var CaptureExpirydate='F';
								var CaptureFifodate='F';
								POarray["custparam_batchno"] = getBatch;
								var vSKUID=POarray["custparam_fetcheditemid"];
								nlapiLogExecution('DEBUG','vSKUID',vSKUID);
								var Itype = nlapiLookupField('item', vSKUID, 'recordType');
								var itemRecord = nlapiLoadRecord(Itype, vSKUID);
								var itemShelflife = itemRecord.getFieldValue('custitem_ebiz_item_shelf_life');
								CaptureExpirydate= itemRecord.getFieldValue('custitem_ebiz_item_cap_expiry_date');
								CaptureFifodate= itemRecord.getFieldValue('custitem_ebiz_item_cap_fifo_date');

								nlapiLogExecution('DEBUG','CaptureExpirydate',CaptureExpirydate);
								nlapiLogExecution('DEBUG','CaptureExpirydate',CaptureFifodate);
								nlapiLogExecution('DEBUG','itemShelflife',itemShelflife);
								POarray["custparam_batchno"] = getBatch;
								
								//Case# 201410517 starts
								if(CaptureExpirydate =='' ||CaptureExpirydate =='null' ||CaptureExpirydate ==null)
								{
									CaptureExpirydate='F';
								}
								if(CaptureFifodate =='' ||CaptureFifodate =='null' ||CaptureFifodate ==null)
								{
									CaptureFifodate='F';
								}
								//Case# 201410517 ends
								
								POarray["custparam_shelflife"] = itemShelflife;
								POarray["custparam_captureexpirydate"] = CaptureExpirydate;
								POarray["custparam_capturefifodate"] = CaptureFifodate;
								var d = new Date();
								var vExpiryDate='';
								if(itemShelflife !=null && itemShelflife!='')
								{
									//Case # 20126458 Start
									var ctx = nlapiGetContext();
									var setpreferencesdateformate = ctx.getPreference('DATEFORMAT');

									d.setDate(d.getDate()+parseInt(itemShelflife));
									nlapiLogExecution('DEBUG', 'setpreferencesdateformate',setpreferencesdateformate);
									if(setpreferencesdateformate=='DD/MM/YYYY' || setpreferencesdateformate=='DD-MM-YYYY')
									{
										vExpiryDate=((d.getDate())+"/"+(d.getMonth()+1)+"/"+(d.getFullYear()));
										
									}
									else
									{
										vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
									}
									//Case # 20126458 End
								}
								else
								{
									vExpiryDate='01/01/2099';										     
								}
								nlapiLogExecution('DEBUG', 'vExpiryDate',vExpiryDate);

								if(CaptureExpirydate =='T' ||(CaptureExpirydate =='T' && CaptureFifodate =='T'))
								{				
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_chkexpirydate', 'customdeploy_ebiz_rf_cart_chkexpirydate', false, POarray);
								}
								else if(CaptureExpirydate !='T' && CaptureFifodate=='T' )
								{
									POarray["custparam_fifodate"]='';
									POarray["custparam_mfgdate"]='';
									POarray["custparam_bestbeforedate"]='';
									POarray["custparam_lastdate"]='';
									POarray["custparam_fifocode"]='';																			
									POarray["custparam_expdate"]= vExpiryDate;

									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_chkinfifodate', 'customdeploy_ebiz_rf_cart_chkinfifodate', false, POarray);

								}
								else if(CaptureExpirydate =='F' && CaptureFifodate =='F' )
								{

									POarray["custparam_fifodate"]=DateStamp();
									POarray["custparam_mfgdate"]='';
									POarray["custparam_bestbeforedate"]='';
									POarray["custparam_lastdate"]='';
									POarray["custparam_fifocode"]='';
									POarray["custparam_expdate"]= vExpiryDate;

									var BatchDetails = new Array();
									BatchDetails[0] = getBatch;
									BatchDetails[1] = POarray["custparam_mfgdate"];
									BatchDetails[2] = POarray["custparam_expdate"];
									BatchDetails[3] = POarray["custparam_bestbeforedate"];
									BatchDetails[4] = POarray["custparam_lastdate"];
									BatchDetails[5] = POarray["custparam_fifodate"];
									BatchDetails[6] = POarray["custparam_fifocode"];

									CheckinLp(request.getParameter('custparam_fetcheditemid'), request.getParameter('custparam_poid'), request.getParameter('custparam_poitem'), request.getParameter('custparam_lineno'), request.getParameter('hdnPOInternalId'), request.getParameter('hdnPOQuantityEntered'), request.getParameter('hdnItemRemaininingQuantity'), request.getParameter('hdnItemPackCode'), request.getParameter('hdnItemStatus'), request.getParameter('hdnQuantity'), request.getParameter('hdnQuantityReceived'), request.getParameter('custparam_itemdescription'), request.getParameter('custparam_itemcube'), request.getParameter('hdnActualBeginDate'), request.getParameter('hdnActualBeginTime'), request.getParameter('hdnActualBeginTimeAMPM'),BatchDetails,request.getParameter('custparam_cartlpno'),POarray["custparam_actscanbatchno"],request,response);		

								}
							}

						}

						//POarray["custparam_batchno"] = getBatch;
						//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_chkexpirydate', 'customdeploy_ebiz_rf_cart_chkexpirydate', false, POarray);
						else {
							POarray["custparam_error"] = st6;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;
						}
					}
				}
				else
				{
					POarray["custparam_error"] = st6;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				//case 20125226 script error start
			}
			//case 20125226 end
			
		
	}
	catch (e) {
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		nlapiLogExecution('DEBUG', 'Catch: Location not found');

	}
}
}

function getBatchNo(item, packCode,location, batch ){
	nlapiLogExecution('DEBUG', 'getBatchNo function');
	nlapiLogExecution('DEBUG', 'getitem',item);
	nlapiLogExecution('DEBUG', 'getpackcode',packCode);
	nlapiLogExecution('DEBUG', 'getlocation',location);
	nlapiLogExecution('DEBUG', 'getbatch',batch);

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[item]));
	//Case# 20149238 starts
	if(packCode!=null && packCode!='')
	filter.push(new nlobjSearchFilter('custrecord_ebizpackcode',null,'anyof',[packCode]));
	if(location!=null && location!='')
	filter.push(new nlobjSearchFilter('custrecord_ebizsitebatch',null,'anyof',[location]));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//Case# 20149238 ends
	filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batch));
	filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
	column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
	column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
	column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
	column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
	column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');

	var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);

	return searchRecord;
}
function CheckinLp(ItemId,poid,poitem,lineno,pointernalid,poqtyentered,poitemremainingqty,polinepackcode,polineitemstatus,polinequantity,polinequantityreceived,itemdescription,itemcube,ActualBeginDate,ActualBeginTime,ActualBeginTimeAMPM,BatchDetails,cartlpno,ScannedBatch,request,response)
{

	nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

	var getItemLP = "", getItemCartLP = "",getsysItemLP="";
	getItemCartLP = request.getParameter('custparam_cartlpno');
	var getFetchedItemId = ItemId;

	nlapiLogExecution('DEBUG', 'custparam_option', request.getParameter('custparam_option'));
	var POarray = new Array();

	var getLanguage = request.getParameter('hdngetLanguage');
	POarray["custparam_language"] = getLanguage;
	nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


	var st7;
	if( getLanguage == 'es_ES')
	{
		st7 = "LP NO V&#193;LIDO";

	}
	else
	{
		st7 = "INVALID LP";

	}



	// Added by Phani on 03-25-2011
	POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
	var poLoc = request.getParameter('hdnWhLocation');
	nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);	

	getsysItemLP = GetMaxLPNo(1, 1,poLoc);

	nlapiLogExecution('DEBUG', 'getsysItemLP LP now added', getsysItemLP);
	//}
	// This variable is to hold the Quantity entered.

	var EndLocationArray = new Array();


	var getPONo = poid;
	var getPOItem = poitem;
	var getPOLineNo = lineno;

	var getPOInternalId = pointernalid;
	var getPOQtyEntered = poqtyentered;
	var getPOItemRemainingQty = poitemremainingqty;
	var getPOLinePackCode = polinepackcode;
	var getPOLineItemStatus = polineitemstatus;
	var getPOLineQuantity = polinequantity;
	var getPOLineQuantityReceived = polinequantityreceived;
	var getItemDescription = itemdescription;
	var getItemCube = itemcube;
	var getActualBeginDate = ActualBeginDate;

	var getActualBeginTime = ActualBeginTime;
	var getActualBeginTimeAMPM = ActualBeginTimeAMPM;

	var getLineCount = 1;
	var getBaseUOM = 'EACH';
	var getBinLocation = "";

	POarray["custparam_error"] = st7;
	POarray["custparam_poid"] = poid;
	POarray["custparam_poitem"] = poitem;
	POarray["custparam_lineno"] = lineno;
	POarray["custparam_fetcheditemid"] =ItemId;
	POarray["custparam_pointernalid"] = pointernalid;
	POarray["custparam_poqtyentered"] = poqtyentered;
	POarray["custparam_poitemremainingqty"] = poitemremainingqty;
	POarray["custparam_polinepackcode"] = polinepackcode;
	POarray["custparam_polinequantity"] = polinequantity;
	POarray["custparam_polinequantityreceived"] = polinequantityreceived;
	POarray["custparam_polineitemstatus"] = polineitemstatus;
	POarray["custparam_itemdescription"] = itemdescription;
	POarray["custparam_itemquantity"] = polinequantity;
	POarray["custparam_enteredOption"] = request.getParameter('hdnPickfaceEnteredOption');

	nlapiLogExecution('DEBUG', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);

	POarray["custparam_itemcube"] = itemcube;

	POarray["custparam_actualbegindate"] =ActualBeginDate;

	//		var TimeArray = new Array();
	//		TimeArray = getActualBeginTime.split(' ');
	POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
	POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; //TimeArray[1];
	nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
	nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

	POarray["custparam_screenno"] = 'CRT6';
	POarray["custparam_batchno"] = BatchDetails[0];
	POarray["custparam_mfgdate"] = BatchDetails[1];
	POarray["custparam_expdate"] = BatchDetails[2];
	POarray["custparam_bestbeforedate"] = BatchDetails[3];
	POarray["custparam_lastdate"] = BatchDetails[4];
	POarray["custparam_fifodate"] = BatchDetails[5];
	POarray["custparam_fifocode"] = BatchDetails[6];
	POarray["custparam_cartlpno"] = cartlpno;

	var POfilters = new Array();
	nlapiLogExecution('DEBUG', 'getPOInternalId', getPOInternalId);
	POfilters.push(new nlobjSearchFilter('name', null, 'is', getPOInternalId));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, POfilters, null);

	var getPOReceiptNo = '';

	if (searchresults != null && searchresults.length > 0) {
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			getPOReceiptNo = searchresults[i].getValue('custrecord_ebiz_poreceipt_receiptno');
		}

		nlapiLogExecution('DEBUG', 'PO Receipt No', getPOReceiptNo);
	}
	
	
	var LPReturnValue = true;	
	
	var lpExists = 'N';
	//LP Checking in masterlp record starts
	if (LPReturnValue == true) {
		try 
		{

			nlapiLogExecution('DEBUG', 'Inserting LP');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', getsysItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getsysItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_site', poLoc);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 4);//LP type=CART
			var rec = nlapiSubmitRecord(customrecord, false, true);

		} 		
		catch (e) 
		{
			nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
		}
		//LP Checking in masterlp record ends
		nlapiLogExecution('DEBUG', 'POarray.length1', POarray.length);
		nlapiLogExecution('DEBUG', 'lp Exists111', lpExists);
		var remainingCube = '';
//		if (LPReturnValue == true) {
		if(lpExists != 'Y')
		{
			POarray["custparam_polineitemlp"] = getsysItemLP;
			POarray["custparam_poreceiptno"] = getPOReceiptNo;
			var TotalItemCube = parseFloat(getItemCube) * parseFloat(getPOQtyEntered);
			nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );

			//					BeginLocationArray = GenerateLocation(getPOQtyEntered, getPOLineNo, getLineCount, getFetchedItemId, getPOItem, getBinLocation, getItemCube, getBeginLocation)
			var itemSubtype = nlapiLookupField('item', POarray["custparam_fetcheditemid"], ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
			POarray["custparam_recordtype"] = itemSubtype.recordType;
			var batchflag=itemSubtype.custitem_ebizbatchlot;
			nlapiLogExecution('DEBUG', 'itemSubtype.recordType =', itemSubtype.recordType);
			nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
			nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);

			var priorityPutawayLocnArr = priorityPutaway(getFetchedItemId, getPOQtyEntered,poLoc);
			var priorityRemainingQty = priorityPutawayLocnArr[0];
			var priorityQty = priorityPutawayLocnArr[1];
			var priorityLocnID = priorityPutawayLocnArr[2];

			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityRemainingQty', priorityRemainingQty);
			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityQty', priorityQty);
			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:POQtyEntered', getPOQtyEntered);
			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityLocnID', priorityLocnID);

			var pickfaceEnteredOption = request.getParameter('hdnPickfaceEnteredOption');
			var putmethod,putrule;

			if(priorityQty < getPOQtyEntered){			//(priorityRemainingQty != 0){
//				getBeginLocation = GetPutawayLocation(getFetchedItemId, getPOLinePackCode, getPOLineItemStatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType);
				//case# 20149377 starts
				//getBeginLocation = generatePutawayLocation(getFetchedItemId, getPOLinePackCode, getPOLineItemStatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType,poLoc);
				var filters2 = new Array();

				var columns2 = new Array();
				columns2[0] = new nlobjSearchColumn('custitem_item_family');
				columns2[1] = new nlobjSearchColumn('custitem_item_group');
				columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
				columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
				columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
				columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
				columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');
				filters2.push(new nlobjSearchFilter('internalid', null, 'is', getFetchedItemId));
				var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

				var filters3=new Array();
				filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var columns3=new Array();
				columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
				columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
				columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
				columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
				columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
				columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
				columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
				columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
				columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
				columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
				columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
				columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
				columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
				columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
				columns3[14] = new nlobjSearchColumn('formulanumeric');
				columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
				columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
				//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
				columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
				// Upto here on 29OCT2013 - Case # 20124515
				columns3[14].setSort();

				var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);

				getBeginLocation = GetPutawayLocationNew(getFetchedItemId, getPOLinePackCode, getPOLineItemStatus, getBaseUOM, parseFloat(TotalItemCube),itemSubtype.recordType,poLoc,ItemInfoResults,putrulesearchresults);
				//case# 20149377 ends
				//					nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocationArray[1]);
				nlapiLogExecution('DEBUG', 'Begin Location', getBeginLocation);

				//					var getBeginLocation = BeginLocationArray[1];

				var vLocationname="";
				if (getBeginLocation != null && getBeginLocation != '') {
					var getBeginLocationId = getBeginLocation[2];
					vLocationname= getBeginLocation[0];
					nlapiLogExecution('DEBUG', 'Begin Location Id', getBeginLocationId);
				}
				else {
					var getBeginLocationId = "";
					nlapiLogExecution('DEBUG', 'Begin Location Id is null', getBeginLocationId);
				}



				if(getBeginLocation!=null && getBeginLocation != '')
					remainingCube = getBeginLocation[1];

				nlapiLogExecution('DEBUG', 'remainingCube', remainingCube);
				if (getBeginLocation != null && getBeginLocation != '') {
					putmethod = getBeginLocation[9];
					putrule = getBeginLocation[10];
				}
				else
				{
					putmethod = "";
					putrule = "";
				}
			}

			var getActualEndDate = DateStamp();
			nlapiLogExecution('DEBUG', 'getActualEndDate', getActualEndDate);
			var getActualEndTime = TimeStamp();
			nlapiLogExecution('DEBUG', 'getActualEndTime', getActualEndTime);

			if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T') {
				nlapiLogExecution('DEBUG', 'serializedinventoryitem', '');
				var dimfilters = new Array();
				dimfilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', POarray["custparam_fetcheditemid"]));
				dimfilters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
				dimfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var dimcolumn = new Array();
				dimcolumn[0] = new nlobjSearchColumn('custrecord_ebizuomskudim');
				dimcolumn[1] = new nlobjSearchColumn('custrecord_ebizqty');
				var dimsearchresult = nlapiSearchRecord('customrecord_ebiznet_skudims', null, dimfilters, dimcolumn);

				if (dimsearchresult != null && dimsearchresult.length > 0) {
					POarray["custparam_uomid"] = dimsearchresult[0].getValue('custrecord_ebizuomskudim');
					POarray["custparam_uomidtext"] = dimsearchresult[0].getText('custrecord_ebizuomskudim');
					POarray["custparam_uomqty"] = dimsearchresult[0].getText('custrecord_ebizqty');
					nlapiLogExecution('DEBUG', 'POarray["custparam_uomid"]', POarray["custparam_uomid"]);
				}
				POarray["custparam_locid"] = getBeginLocationId;
				// POarray["custparam_location"] = getBeginLocation;
				POarray["custparam_location"] = vLocationname;
				POarray["custparam_number"] = 0;
				nlapiLogExecution('DEBUG', 'getBeginLocationId', getBeginLocationId);
				nlapiLogExecution('DEBUG', 'POarray["custparam_number"]', POarray["custparam_number"]);
				nlapiLogExecution('DEBUG', 'POarray.length', POarray.length);

				//response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no', false, POarray);
				response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no_di', false, POarray);

				//response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no', false, null);						
				nlapiLogExecution('DEBUG', 'Towards Serial No. Screen');
				return;
				nlapiLogExecution('DEBUG', 'This is after return statement');
			}
			nlapiLogExecution('DEBUG', 'This is before executing PUTW record creation');

			/*
			 * This is to insert a record in Transaction Line Details custom record
			 * The purpose of this is to identify the quantity checked-in, location generated for and putaway confirmation
			 * */                    
			var trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');
			TrnLineUpdation(trantype, 'CHKN', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
					POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
					POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"",getPOLineItemStatus);

			/*
			 * If the received quantity has to be putaway to a pickface location based on priority putaway flag, 
			 * 	create PUTW task in TRN_OPENTASK 
			 * 		set taskType = 2(PUTW) and wmsStatusFlag = 2(INBOUND/LOCATIONS ASSIGNED)
			 * 		beginLocation = priorityLocnID and endLocn = ""
			 */	

			if (priorityQty >= getPOQtyEntered){			//(priorityQty != 0){
				taskType = "2"; // PUTW
				wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
				getBeginLocationId = priorityLocnID;
				var LocRemCube = GeteLocCube(getBeginLocationId);

				nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
				if (parseFloat(LocRemCube) > parseFloat(TotalItemCube)) 
					remainingCube = parseFloat(LocRemCube) - parseFloat(TotalItemCube);
//				endLocn = "";
				//var getItemLP = GetMaxLPNo(1, 1);
			}
			nlapiLogExecution('DEBUG', 'Before custChknPutwRecCreation calling (getBeginLocationId)', getBeginLocationId);
			nlapiLogExecution('DEBUG', 'getPOInternalId', getPOInternalId);
			nlapiLogExecution('DEBUG', 'getPONo', getPONo);
			nlapiLogExecution('DEBUG', 'getPOQtyEntered', getPOQtyEntered);
			nlapiLogExecution('DEBUG', 'getPOLineNo', getPOLineNo);
			nlapiLogExecution('DEBUG', 'getPOItemRemainingQty', getPOItemRemainingQty);
			nlapiLogExecution('DEBUG', 'getLineCount', getLineCount);
			nlapiLogExecution('DEBUG', 'getFetchedItemId', getFetchedItemId);
			nlapiLogExecution('DEBUG', 'getPOItem', getPOItem);
			nlapiLogExecution('DEBUG', 'getItemDescription', getItemDescription);
			nlapiLogExecution('DEBUG', 'getPOLineItemStatus', getPOLineItemStatus);
			nlapiLogExecution('DEBUG', 'getPOLinePackCode', getPOLinePackCode);
			nlapiLogExecution('DEBUG', 'getBaseUOM', getBaseUOM);
			//nlapiLogExecution('DEBUG', 'getItemLP', getItemLP);
			nlapiLogExecution('DEBUG', 'getBinLocation', getBinLocation);
			nlapiLogExecution('DEBUG', 'getPOLineQuantityReceived', getPOLineQuantityReceived);
			nlapiLogExecution('DEBUG', 'getActualEndDate', getActualEndDate);
			nlapiLogExecution('DEBUG', 'getActualEndTime', getActualEndTime);
			nlapiLogExecution('DEBUG', 'getPOReceiptNo', getPOReceiptNo);
			nlapiLogExecution('DEBUG', 'getActualBeginDate', getActualBeginDate);
			nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
			nlapiLogExecution('DEBUG', 'getActualBeginTimeAMPM', getActualBeginTimeAMPM);
			nlapiLogExecution('DEBUG', 'getBeginLocationId', getBeginLocationId);
			nlapiLogExecution('DEBUG', 'POarray["custparam_batchno"]', POarray["custparam_batchno"]);
			nlapiLogExecution('DEBUG', 'itemSubtype.recordType', itemSubtype.recordType);
			nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizbatchlot', itemSubtype.custitem_ebizbatchlot);
			nlapiLogExecution('DEBUG', 'batchflag', batchflag);
			nlapiLogExecution('DEBUG', 'getItemCartLP', getItemCartLP);
			nlapiLogExecution('DEBUG', 'POarray["custparam_whlocation"]', POarray["custparam_whlocation"]);
			nlapiLogExecution('DEBUG', 'batchflag', batchflag);
			nlapiLogExecution('DEBUG', 'putmethod', putmethod);
			nlapiLogExecution('DEBUG', 'putrule', putrule);

			var vValid=custChknPutwRecCreation(getPOInternalId, getPONo, getPOQtyEntered, getPOLineNo, getPOItemRemainingQty, getLineCount, getFetchedItemId, 
					getPOItem, getItemDescription, getPOLineItemStatus, getPOLinePackCode, getBaseUOM, getsysItemLP, getBinLocation, 
					getPOLineQuantityReceived, getActualEndDate, getActualEndTime, getPOReceiptNo, "", getActualBeginDate, 
					getActualBeginTime, getActualBeginTimeAMPM, getBeginLocationId, POarray["custparam_batchno"], 
					POarray["custparam_mfgdate"], POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
					POarray["custparam_lastdate"], POarray["custparam_fifodate"], POarray["custparam_fifocode"], 
					itemSubtype.recordType, getItemCartLP, POarray["custparam_whlocation"],batchflag,putmethod,putrule);
			nlapiLogExecution('DEBUG', 'vValid', vValid);
			if(vValid==false)
			{
				response.sendRedirect('SUITELET', 'customscript_rf_checkin_cnf', 'customdeploy_rf_checkin_cnf', false, POarray);
			}
			else
			{

				/*
				 * This is to insert a record in inventory custom record.
				 * The data that is to be inserted is PO Internal Id, Item, Item Status, Pack Code, Quantity, LP#
				 * The parameters list is: 
				 * pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, warehouse location
				 * 
				 */                    


				TrnLineUpdation(trantype, 'ASPW', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
						POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
						POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"");
				//case# 20148483 starts 
				//if(getBinLocation!=null && getBinLocation!='')
				if(getBeginLocationId!=null && getBeginLocationId!='')
				{//case# 20148483 ends
					UpdateLocCube(getBeginLocationId, remainingCube);
				}

				if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == "lotnumberedassemblyitem" ||itemSubtype.recordType == "assemblyitem" || itemSubtype.custitem_ebizbatchlot == 'T') {
					//checks for Batch/Lot exists in Batch Entry, Update if exists if not insert
					try {
						nlapiLogExecution('DEBUG', 'in');
						var filtersbat = new Array();
						filtersbat.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', POarray["custparam_batchno"]));
						filtersbat.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

						if(getFetchedItemId!=null&&getFetchedItemId!="")
							filtersbat.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', POarray["custparam_fetcheditemid"]));

						//	if(poLoc!=null && poLoc!='')
						//		filtersbat.push(new nlobjSearchFilter('custrecord_ebizsitebatch',null,'anyof',[poLoc]));

						var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);
						nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
						if (SrchRecord) {

							nlapiLogExecution('DEBUG', ' BATCH FOUND');
							var transaction = nlapiLoadRecord('customrecord_ebiznet_batch_entry', SrchRecord[0].getId());
							transaction.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
							transaction.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
							transaction.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
							transaction.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
							transaction.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
							transaction.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
							if(ScannedBatch != null && ScannedBatch != '')
								transaction.setFieldValue('custrecord_ebiz_manufacturelot', ScannedBatch);
							var rec = nlapiSubmitRecord(transaction, false, true);

						}
						else {
							nlapiLogExecution('DEBUG', 'BATCH NOT FOUND');
							nlapiLogExecution('DEBUG', 'ScannedBatch',ScannedBatch);
							var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

							customrecord.setFieldValue('name', POarray["custparam_batchno"]);
							customrecord.setFieldValue('custrecord_ebizlotbatch', POarray["custparam_batchno"]);
							customrecord.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
							customrecord.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
							customrecord.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
							customrecord.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
							customrecord.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
							customrecord.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
							nlapiLogExecution('DEBUG', 'Item Id',getFetchedItemId);
							//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
							customrecord.setFieldValue('custrecord_ebizsku', getFetchedItemId);
							customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);
							//customrecord.setFieldValue('custrecord_ebizsitebatch',POarray["custparam_whlocation"]);
							if(ScannedBatch != null && ScannedBatch != '')
							{
								customrecord.setFieldValue('custrecord_ebiz_manufacturelot',ScannedBatch);													
							}
							var rec = nlapiSubmitRecord(customrecord, false, true);
						}
					} 
					catch (e) {
						nlapiLogExecution('DEBUG', 'Failed to Update/Insert into BATCH ENTRY Record in exp');
					}
				}
				rf_checkin(getPOInternalId, getFetchedItemId, getItemDescription, getPOLineItemStatus, 
						getPOLinePackCode, getPOQtyEntered, getsysItemLP, POarray["custparam_whlocation"],
						POarray["custparam_batchno"], POarray["custparam_mfgdate"], 
						POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
						POarray["custparam_lastdate"], POarray["custparam_fifodate"], 
						POarray["custparam_fifocode"],getBeginLocationId);// Case# 20127851 

				POarray["custparam_trantype"]=trantype;
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, POarray);
			}
		}
		else {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'Entered Item LP', getsysItemLP);
		}
	}
	else {
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		nlapiLogExecution('DEBUG', 'Entered Item LP', getsysItemLP);
	}
	//}
	nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

}

function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, 
		ItemDesc, ItemStatus, PackCode, BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, 
		poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, ActualBeginTimeAMPM, BeginLocationId, 
		BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, itemrectype, cartlp, 
		WHLocation,batchflag,putmethod,putrule)
{
	var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
	var columns = nlapiLookupField('item', ItemId, fields);
	var ItemType = columns.recordType;					
	var batchflg = columns.custitem_ebizbatchlot;
	var itemfamId= columns.custitem_item_family;
	var itemgrpId= columns.custitem_item_group;
	var getlotnoid="";

//	if(ItemStatus==null || ItemStatus=='')
//	ItemStatus='12';

//	nlapiLogExecution('DEBUG', 'putLotBatch', putLotBatch);

	var vValid=true;
	nlapiLogExecution('DEBUG', 'Into custChknPutwRecCreation', 'custChknPutwRecCreation');
	var now = new Date();
	var stagelocid, docklocid;

	stagelocid = getStagingLocation(WHLocation);
	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);

	docklocid = getDockLocation();
	nlapiLogExecution('DEBUG', 'Dock Location', docklocid);

	if (BeginLocationId != null && BeginLocationId != '' ) //Creating custom record with CHKN and PUTW task,
	{
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		if(po != null && po != '')
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);

		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			nlapiLogExecution('DEBUG', 'ExpDate', ExpDate);

			// Case # 20126458  Start
			if(ExpDate!=null && ExpDate!='' && ExpDate!='null')
			{
				customrecord.setFieldValue('custrecord_expirydate', ExpDate);
				nlapiLogExecution('DEBUG', 'ExpDate', ExpDate);
			}
			nlapiLogExecution('DEBUG', 'FifoDate', FifoDate);
			if(FifoDate!=null && FifoDate!='' && FifoDate!='null')
			{
				customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			}
			//Case # 20126458  End
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);
		nlapiLogExecution('DEBUG', 'Submitting CHKN record', 'TRN_OPENTASK');

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		nlapiLogExecution('DEBUG', 'BeginLocationId', BeginLocationId);

		//create the openTask record With PUTW Task Type
		if(BeginLocationId==null || BeginLocationId=="")
			return false;
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)

		nlapiLogExecution('DEBUG', 'Fetched Begin Location', BeginLocation);

		putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		if(po != null && po != '')
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//For eBiznet Receipt Number .
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//putwrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		if (BeginLocationId != "") {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 2);
		}
		else {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		}
		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			//Case # 20126458  Start
			if(ExpDate!=null && ExpDate!='' && ExpDate!='null')
			{
				putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			}
			if(FifoDate!=null && FifoDate!='' && FifoDate!='null')
			{
				putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			}
			//Case # 20126458  End
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);

		try
		{
			MoveTaskRecord(recid);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in  MoveTaskRecord',exp);
		}

		nlapiLogExecution('DEBUG', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');
		return true;
	}//end if binloc is not null
	else {
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);

		nlapiLogExecution('DEBUG', 'custrecord_ebiz_sku_no tesing', 'ItemId');

		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		if(po != null && po != '')
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{

			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		if(po != null && po != '')
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" ||itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");

		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		return false;
	}
}
//case # 20145365 start
function getStagingLocation(site){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));
	if(site!=null && site!="")
		stageFilters.push(new nlobjSearchFilter('custrecord_ebizsiteloc', 'custrecord_inboundlocgroupid', 'anyof', ["NONE",site]));
	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}
//case # 20145365 end
function getDockLocation(){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}

function rf_checkin(pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, whLocation,
		batchno, mfgdate, expdate, bestbeforedate, lastdate, fifodate, fifocode,getBeginLocationId){// Case# 20127851 
	nlapiLogExecution('DEBUG', 'inside rf_checkin', 'Success');

	nlapiLogExecution('DEBUG', 'whLocation', whLocation);
	nlapiLogExecution('DEBUG', 'PO Internal ID', pointid);

	if(whLocation==null || whLocation=="")
	{
		whLocation = nlapiLookupField('purchaseorder', pointid, 'location');

		nlapiLogExecution('DEBUG', 'whLocation (from lookup)', whLocation);
	}

	/*
	 * Get the stage location. This will be the bin location to which the item will be 
	 * placed at the time of check-in. The same data is to be inserted in inventory record after it is
	 * done in opentask record.
	 */
	var stagelocid;

	stagelocid = getStagingLocation(whLocation);
	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);

	/*
	 * To create inventory record.
	 */
	var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

	nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');

	var filtersAccount = new Array();
	if(whLocation!=null && whLocation!=""){
		filtersAccount.push(new nlobjSearchFilter('custrecord_location', null, 'anyof', whLocation));
	}

	var accountNumber = "";
//	var columnsAccount = new Array();
//	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

//	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);
//	if(accountSearchResults!= null && accountSearchResults.length > 0)
//	accountNumber = accountSearchResults[0].getValue('custrecord_accountno');
//	if(accountSearchResults!= null && accountSearchResults.length > 0)
//	{

	//nlapiLogExecution('DEBUG', 'Account # incondition',accountNumber);

	invtRec.setFieldValue('name', pointid);
	// Case# 20145365 starts
	invtRec.setFieldValue('custrecord_ebiz_inv_binloc', stagelocid);
	//invtRec.setFieldValue('custrecord_ebiz_inv_binloc', getBeginLocationId);
	// Case# 20145365 ends
	invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
	invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
	invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
	invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
	//invtRec.setFieldValue('custrecord_ebiz_qoh', quantity);
	invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
//	invtRec.setFieldValue('custrecord_wms_inv_status_flag','1');
	invtRec.setFieldValue('custrecord_wms_inv_status_flag','17');//17=FLAG.INVENTORY.INBOUND
	invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
	invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
	invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
	invtRec.setFieldValue('custrecord_invttasktype', 1);

	nlapiLogExecution('DEBUG', 'batchno', batchno);

	if(batchno!=null && batchno!='')
	{
		try
		{
			invtRec.setFieldText('custrecord_ebiz_inv_lot', batchno);
			invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception', exp);
		}
	}

	var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
//	}
//	else
//	{
//	response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, "Invalid Account #");
//	//nlapiLogExecution('DEBUG', 'Entered Item LP', getItemLP);
//	}
	nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtRecordId);
	if (invtRecordId != null) {
		nlapiLogExecution('DEBUG', 'Inventory record creation is successful', invtRecordId);
	}
	else {
		nlapiLogExecution('DEBUG', 'Inventory record creation is failure', 'Fail');
	}
} 





function getBatchNoByMfcLot(item, packCode,location, batch ){
	nlapiLogExecution('DEBUG', 'getBatchNo function');
	nlapiLogExecution('DEBUG', 'getitem',item);
	nlapiLogExecution('DEBUG', 'getpackcode',packCode);
	nlapiLogExecution('DEBUG', 'getlocation',location);
	nlapiLogExecution('DEBUG', 'getbatch',batch);


	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
	column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
	column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
	column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
	column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
	column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');


	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[item]));
	filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batch));
	filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

	var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);
	if(searchRecord == null || searchRecord == '')
	{

		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[item]));
		//if(packCode!=null && packCode!='')
		//filter.push(new nlobjSearchFilter('custrecord_ebizpackcode',null,'anyof',[packCode]));
		/*if(location!=null && location!='')
		filter.push(new nlobjSearchFilter('custrecord_ebizsitebatch',null,'anyof',[location]));*/
		//filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batch));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_manufacturelot',null,'is',batch));

		var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);

	}


	return searchRecord;
}

//case  20123285 start

function ValidateSplCharacter(string,name)
{
	var iChars = "*|,\":<>[]{}`\';()@&$#% ";
	var length=string.length;
	var flag = 'N';
	for(var i=0;i<length;i++)
	{
		if(iChars.indexOf(string.charAt(i))!=-1)
		{
			flag='Y';
			break;
		}
	}
	if(flag == 'Y')
	{
		return false;
	}
	else
	{
		return true;
	}

}
//end
