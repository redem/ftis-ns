/***************************************************************************
 eBizNET Solutions                
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingContainer.js,v $
 *     	   $Revision: 1.8.2.17.4.8.2.36.2.1 $
 *     	   $Date: 2015/09/24 12:58:27 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingContainer.js,v $
 * Revision 1.8.2.17.4.8.2.36.2.1  2015/09/24 12:58:27  schepuri
 * case# 201414594
 *
 * Revision 1.8.2.17.4.8.2.36  2015/04/17 15:26:23  grao
 * SB issue fixes  201412398
 *
 * Revision 1.8.2.17.4.8.2.35  2014/11/06 15:39:50  sponnaganti
 * Case# 201410963
 * True Fab SB Issue fix
 *
 * Revision 1.8.2.17.4.8.2.34  2014/10/22 14:12:04  schepuri
 * 201410679 issue fix
 *
 * Revision 1.8.2.17.4.8.2.33  2014/09/25 17:57:17  skreddy
 * case # 201410534
 * Alpha comm CR (''pick by carton LP")
 *
 * Revision 1.8.2.17.4.8.2.32  2014/08/23 01:12:40  gkalla
 * case#201410109
 * TrueFab SB issue is with wave number with .0
 *
 * Revision 1.8.2.17.4.8.2.31  2014/07/14 06:07:45  skreddy
 * case # 20149330
 * sonic sb issue fix
 *
 * Revision 1.8.2.17.4.8.2.30  2014/06/16 07:19:32  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148916
 *
 * Revision 1.8.2.17.4.8.2.29  2014/06/13 12:34:10  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.8.2.17.4.8.2.28  2014/06/12 15:25:39  sponnaganti
 * case# 20148052
 * Stnd Bundle Issue Fix
 *
 * Revision 1.8.2.17.4.8.2.27  2014/05/30 00:41:04  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.8.2.17.4.8.2.26  2014/05/26 07:08:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.8.2.17.4.8.2.25  2014/05/19 06:29:01  skreddy
 * case # 20148192
 * Sonic SB issue fix
 *
 * Revision 1.8.2.17.4.8.2.24  2014/03/19 07:10:22  gkalla
 * case#20127670
 * LL issue fix
 *
 * Revision 1.8.2.17.4.8.2.23  2014/03/11 15:32:50  skavuri
 * Case# 20127636 issue fixed
 *
 * Revision 1.8.2.17.4.8.2.22  2014/03/07 15:48:40  skavuri
 * Case# 20127587 issue fixed
 *
 * Revision 1.8.2.17.4.8.2.21  2014/01/27 13:54:29  schepuri
 * 20126951
 * Standadr Bundle issue fix
 *
 * Revision 1.8.2.17.4.8.2.20  2014/01/20 13:39:55  snimmakayala
 * Case# : 20126712
 *
 * Revision 1.8.2.17.4.8.2.19  2014/01/20 09:59:34  snimmakayala
 * Case# : 20126503
 * Cartonization Issue
 *
 * Revision 1.8.2.17.4.8.2.18  2014/01/20 09:58:46  snimmakayala
 * Case# : 20126503
 * Cartonization Issue
 *
 * Revision 1.8.2.17.4.8.2.17  2013/12/10 14:29:13  snimmakayala
 * Case# : 20126115
 * MHP UAT Fixes.
 *
 * Revision 1.8.2.17.4.8.2.16  2013/11/28 06:07:30  skreddy
 * Case# 20123888
 * Afosa SB issue fix
 *
 * Revision 1.8.2.17.4.8.2.15  2013/11/01 13:43:22  schepuri
 * 20125431
 *
 * Revision 1.8.2.17.4.8.2.14  2013/10/29 15:19:56  grao
 * Issue fix related to the 20125391  && 20125393
 *
 * Revision 1.8.2.17.4.8.2.13  2013/09/16 09:40:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.8.2.17.4.8.2.12  2013/09/05 00:04:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Afosa changes
 * case#20123755
 *
 * Revision 1.8.2.17.4.8.2.11  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.8.2.17.4.8.2.10  2013/06/24 15:45:32  grao
 * CASE201112/CR201113/LOG201121
 * NLS SB Issues fixes
 * Wave Generation - Wave already completed
 * Issue :  While generating picks if system generates failed picks and if we scan that wave# in RF picking system is saying " Wave already completed" . The expected error message is " Invalid Wave#"
 *
 * Revision 1.8.2.17.4.8.2.9  2013/06/19 22:56:06  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 *
 * Revision 1.8.2.17.4.8.2.8  2013/06/19 06:50:38  spendyala
 * CASE201112/CR201113/LOG2012392
 * Driving to this screen even when user provides FO or SO as an i/p.
 *
 * Revision 1.8.2.17.4.8.2.7  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.8.2.17.4.8.2.6  2013/05/02 14:24:11  schepuri
 * wave with zone picking issue fix
 *
 * Revision 1.8.2.17.4.8.2.5  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.8.2.17.4.8.2.4  2013/04/16 14:55:56  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.8.2.17.4.8.2.3  2013/04/08 08:47:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.8.2.17.4.8.2.2  2013/04/03 01:59:33  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.8.2.17.4.8.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.8.2.17.4.8  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.8.2.17.4.7  2012/12/03 16:42:36  schepuri
 * CASE201112/CR201113/LOG201121
 * If wave is already picked then should error message as �wave complete�.
 *
 * Revision 1.8.2.17.4.6  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.8.2.17.4.5  2012/11/09 14:39:39  skreddy
 * CASE201112/CR201113/LOG201121
 * displaying error messages based on selection
 *
 * Revision 1.8.2.17.4.4  2012/10/11 10:27:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick confirmation and other inventory issues
 *
 * Revision 1.8.2.17.4.3  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.8.2.17.4.2  2012/10/04 10:28:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.8.2.17.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.8.2.17  2012/08/26 17:55:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Pick Confirm issues for FISK
 *
 * Revision 1.8.2.16  2012/08/23 13:46:03  schepuri
 * CASE201112/CR201113/LOG201121
 * issue related to invalid SO no
 *
 * Revision 1.8.2.15  2012/08/16 07:53:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah UAT Issue Fixes.
 * Cartonization and Customization
 *
 * Revision 1.8.2.14  2012/08/14 06:45:20  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue while entering SO is resolved.
 *
 * Revision 1.8.2.13  2012/08/09 08:04:25  skreddy
 * CASE201112/CR201113/LOG201121
 * To navigate to Cluster Or Wave Summary Screen
 *
 * Revision 1.8.2.12  2012/08/08 12:56:41  schepuri
 * CASE201112/CR201113/LOG201121
 * invalid option issue
 *
 * Revision 1.8.2.11  2012/08/01 07:16:42  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Routing based on Loc group seq
 *
 * Revision 1.8.2.10  2012/07/30 16:55:55  gkalla
 * CASE201112/CR201113/LOG201121
 * replenishment merging
 *
 * Revision 1.8.2.9  2012/06/15 15:20:48  mbpragada
 * 20120490
 *
 * Revision 1.8.2.8  2012/04/24 13:49:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.8.2.7  2012/04/19 15:00:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.8.2.6  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.8.2.5  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.8.2.4  2012/03/15 15:31:44  gkalla
 * CASE201112/CR201113/LOG201121
 * Not to allow user to scan Batch no
 *
 * Revision 1.8.2.3  2012/02/22 12:56:20  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.8.2.2  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.10  2012/01/23 23:21:17  gkalla
 * CASE201112/CR201113/LOG201121
 * RF Picking customized
 *
 * Revision 1.9  2012/01/09 13:12:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Issue if there a space in batch no
 *
 * Revision 1.8  2011/12/16 12:30:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/12/12 12:58:54  schepuri
 * CASE201112/CR201113/LOG201121
 * adding new button for RF PICK Exception
 *
 * Revision 1.7  2011/09/29 16:17:23  gkalla
 * CASE201112/CR201113/LOG201121
 * To integrate Serial no entry in RF outbound
 *
 * Revision 1.6  2011/09/10 07:24:50  skota
 * CASE201112/CR201113/LOG201121
 * code changes to redirect to right suitelet for capturing serial nos
 *
 * Revision 1.5  2011/09/09 18:08:24  skota
 * CASE201112/CR201113/LOG201121
 * If condition change to consider serial out flag
 *
 * 
 *****************************************************************************/
function PickingContainer(request, response){
	nlapiLogExecution('DEBUG', 'Into Request', 'Into Request');

	var fastpick = request.getParameter('custparam_fastpick');
	var getLanguage = request.getParameter('custparam_language');
	nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
	var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12,st15;

	if( getLanguage == 'es_ES')
	{
		st1 = "N&#218;MERO DE ONDA";
		st2 = "Y / O ";
		st3 = "N&#218;MERO DE ZONA ";
		st4 = "CUMPLIMIENTO DEL N&#218;MERO DE PEDIDO ";
		st5 = "N&#218;MERO DEL PEDIDO DE VENTA ";
		st6 = "ENVIAR ";
		st7 = "ANTERIOR ";
		st8 = "Y";
		st15 = "CARTON NO";

	}
	else
	{
		st1 = "WAVE NO ";
		st2 = "AND/OR ";
		st3 = "ZONE NO ";
		st4 = "FULFILLMENT ORD NO ";
		st5 = "SALES ORDER NO ";
		st6 = "SEND ";
		st7 = "PREV ";
		st8 = "AND ";
		st15 = "CARTON NO";
	}


	if (request.getMethod() == 'GET') 
	{
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterwaveno').focus();";  

		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+ st1;//WAVE NO
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterwaveno' id='enterwaveno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		//case# 20148052 ends
		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+ st8; //AND/OR;
		html = html + "								   "+ st3;//ZONE NO;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterzoneno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		//case# 20148052 ends
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2; //AND/OR;
		html = html + "								   "+ st4;//FULFILLMENT ORD NO
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterorderno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2; //AND/OR;
		html = html + "								   "+ st5;//SALES ORDER NO
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersono' type='text'/>";
		html = html + "				</td>";
		//case# 20148052 starts
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+ st8; //AND/OR;
		html = html + "								   "+ st3;//ZONE NO;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterzoneno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//case# 20148052 ends
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";			
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2; //AND/OR;
		html = html + "								   "+ st15;//CARTON NO", 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercarton' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";			
		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 + "<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st7 + "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterwaveno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var fastpick = request.getParameter('hdnfastpick');	
		var getLanguage = request.getParameter('hdngetLanguage');

		var st8,st9,st10,st11,st12,st13,st14;

		if( getLanguage == 'es_ES')
		{
			st8 = "OPCI&#211;N V&#193;LIDA ";
			st9 = "VENTAS DE PEDIDO NO V&#193;LIDO # ";
			st10 = "ZONA NO V&#193;LIDO ";
			st11 = "NO V&#193;LIDO WAVE # ";
			st12 = "ORDEN NO V&#193;LIDO # ";			
			st13 =  "INVALID CARTON #";
			st14 = "NO TAREAS Recoger Abrir PARA CAJA #";
		}
		else
		{
			st8 = "INVALID OPTION ";
			st9 = "INVALID SALES ORDER # ";
			st10 = "INVALID ZONE ";
			st11 = "INVALID WAVE # ";
			st12 = "INVALID ORDER # ";
			st13 = "INVALID CARTON #";
			st14 = "NO OPEN PICK TASKS FOR CARTON#";// case# 201414594
		}
		var SOarray = new Array();

		getWaveNo = request.getParameter('enterwaveno');
		// Case 20125391? validating charecters
		// Case# 20127587 starts
		/*if (isNaN(getWaveNo)) { 
			SOarray["custparam_error"] = st11;
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			return;
		}*/
		// Case# 20127587 ends
		//Case end


		getOrdNo=request.getParameter('enterorderno');
		var getSONo=request.getParameter('entersono');

		var getZoneNo = request.getParameter('enterzoneno');
		var getCortonNo = request.getParameter('entercarton');
		// This variable is to hold the SO# entered.

		SOarray["custparam_error"] = st8;//INVALID OPTION
		SOarray["custparam_screenno"] = '12';
		var vSkipId=request.getParameter('hdnskipid');
		vSkipId=0;
		SOarray["custparam_skipid"] = vSkipId;
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_fastpick"] = fastpick;
		
		// Case# 20127636 starts
		if (isNaN(getWaveNo)) { 
			SOarray["custparam_error"] = st11;
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			return;
		}
		// Case# 20127636 ends

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di', false, SOarray);
		}
		else 
		{			
			var str = 'getWaveNo. = ' + getWaveNo + '<br>';
			str = str + 'getOrdNo. = ' + getOrdNo + '<br>';	
			str = str + 'getSONo. = ' + getSONo + '<br>';	
			str = str + 'getZoneNo. = ' + getZoneNo + '<br>';
			str = str + 'getCortonNo. = ' + getCortonNo + '<br>';

			nlapiLogExecution('DEBUG', 'Input Parameter Values', str);

			var vSOId;
			//var vOrdFormat='W';
			var vOrdFormat='';
			var vForErrormsz='';
			var vZoneId;
			var vCarton='';

//			case no 20125431
			var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|/g;

			if(getWaveNo != null && getWaveNo !="")
				getWaveNo=getWaveNo.replace(replaceChar,'');
			else
				getWaveNo="";
			// case # 20127587 starts
			if (isNaN(getWaveNo)) { 
				SOarray["custparam_error"] = st11;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}
			//case# 20148052 starts
			if(getZoneNo != null && getZoneNo != '')
			{
				nlapiLogExecution('DEBUG', 'getZoneNo', getZoneNo);
				if((getWaveNo == null || getWaveNo =="") && (getSONo == null || getSONo == '') && (getOrdNo==null || getOrdNo==''))
				{
					nlapiLogExecution('DEBUG', 'Inside of', 'NULL');
					SOarray["custparam_error"] = "PLEASE ENTER WAVENO OR FULFILLMENT ORDER NO OR SALES ORDER NO ";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
			}	
			//case# 20148052 ends
			// case # 20127587 starts
			if(getWaveNo != null && getWaveNo != '')
			{	
				var SOFilters = new Array();
				nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));

				var SOColumns = new Array();

				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));

				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
				nlapiLogExecution('DEBUG', 'Wave SearchResults of given Order', SOSearchResults);
				if (SOSearchResults != null && SOSearchResults != '') {

					var SOFilters = new Array();
					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));//pick gen
					nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));

					var SOColumns = new Array();

					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('DEBUG', 'Wave1 SearchResults of given Order', SOSearchResults);
// case no start 20126951
					var SOFailedpickFilters = new Array();
					SOFailedpickFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [26]));//Failed Picks
					nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
					SOFailedpickFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
					var SOFailedpickColumns = new Array();
					SOFailedpickColumns.push(new nlobjSearchColumn('custrecord_wms_status_flag'));

					var SOFailedPickResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFailedpickFilters, SOFailedpickColumns);

// case no end 20126951					
					if (SOSearchResults == null || SOSearchResults == '') {

						//case 20125393 allowing lines for pick confirmation even it conatins one failed picks 
						if (SOFailedPickResults != null && SOFailedPickResults != ''){	
							nlapiLogExecution('DEBUG', 'SOFailedPickResults: ', SOFailedPickResults.length);
							SOarray["custparam_error"] = st11;
							nlapiLogExecution('DEBUG', 'Error: ', 'Failed Picks');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;						

						}


						if (SOSearchResults == null || SOSearchResults == ''|| SOFailedPickResults == null && SOFailedPickResults == '') {	
							SOarray["custparam_error"] = 'WAVE ALREADY COMPLETED';
							nlapiLogExecution('DEBUG', 'Error: ', 'Wave already completed');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
						}
						//case end 
					}	


				}
				else
				{
					SOarray["custparam_error"] = 'INVALID WAVE';
					nlapiLogExecution('DEBUG', 'Error: ', 'INVALID WAVE');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
			}


			if(getSONo != null && getSONo != '')
			{	
				var SOFilters = new Array();
				var SOColumns = new Array();


				SOFilters.push(new nlobjSearchFilter('tranid', null, 'is', getSONo));
				SOFilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
				var SOSearchResults = nlapiSearchRecord('salesorder', null, SOFilters, SOColumns);
				if(SOSearchResults != null && SOSearchResults != '')
				{
					vSOId=SOSearchResults[0].getId();
				}	
				else
				{
					//case 20123888 start : for transfer order
					//for Transfer Order
					var TOSearchResults = nlapiSearchRecord('transferorder', null, SOFilters, SOColumns);
					if(TOSearchResults !=null && TOSearchResults !='')
					{
						vSOId=TOSearchResults[0].getId();
					}
					else
					{
						//case 20123888 end
						SOarray["custparam_error"] = st9;//'INVALID SALES ORDER #';						
						nlapiLogExecution('DEBUG', 'Error: ', 'Sales Order not found');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
				}
			}

			if(getZoneNo != null && getZoneNo != '')
			{
				nlapiLogExecution('DEBUG', 'Into Zone Validate', 'Into Zone Validate');
				var ZoneFilters = new Array();
				var ZoneColumns = new Array();

				ZoneFilters.push(new nlobjSearchFilter('custrecord_putzoneid', null, 'is', getZoneNo));
				ZoneFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var ZoneSearchResults = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters, ZoneColumns);
				if(ZoneSearchResults != null && ZoneSearchResults != '' && ZoneSearchResults.length>0)
				{
					vZoneId=ZoneSearchResults[0].getId();
				}
				else
				{
					nlapiLogExecution('DEBUG', 'Into Zone Validate Else', 'Into Zone Validate Else');
					var ZoneFilters1 = new Array();
					var ZoneColumns1 = new Array();
					ZoneFilters1.push(new nlobjSearchFilter('name', null, 'is', getZoneNo));
					ZoneFilters1.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					var ZoneSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters1, ZoneColumns1);
					if(ZoneSearchResults1 != null && ZoneSearchResults1 != '' && ZoneSearchResults1.length>0)
					{
						vZoneId=ZoneSearchResults1[0].getId();
					}
					else
					{
						SOarray["custparam_error"] = st10;//'INVALID ZONE';
						SOarray["custparam_screenno"] = '12';
						nlapiLogExecution('DEBUG', 'Error: ', 'Zone not found');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
				}
			}

			if(getCortonNo != null && getCortonNo != '')
			{
				nlapiLogExecution('DEBUG', 'Into Corton', 'Into Corton');
				nlapiLogExecution('DEBUG', 'getWaveNo inside if', getCortonNo); 
				var vResult=CheckCortonNo(getCortonNo,'');
				nlapiLogExecution('DEBUG', 'vResult', vResult);
				
				if(vResult==true)
				{
					var CartonFilters = new Array();
					CartonFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getCortonNo));
					CartonFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));//pick gen
					

					var SOColumns = new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, CartonFilters, SOColumns);
					if(SOSearchResults != null && SOSearchResults != '' && SOSearchResults.length>0)
					{
						nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length); 
						vCarton = getCortonNo;
					}
					else
					{
						SOarray["custparam_error"] = st14;
						SOarray["custparam_screenno"] = '12';
						nlapiLogExecution('DEBUG', 'Error: ', 'no open pick tasks for carton# ');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;

					}
					

				}
				else
				{
					SOarray["custparam_error"] = st13;//'INVALID CARTON';
					SOarray["custparam_screenno"] = '12';
					nlapiLogExecution('DEBUG', 'Error: ', 'Corton# not found in LP master');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;

				}
				
				
			}
			//nlapiLogExecution('DEBUG', 'vZoneId',vZoneId);
			if ((getWaveNo != null && getWaveNo != '')  || (getOrdNo != null && getOrdNo != '') || (getSONo != null && getSONo != '') || (getZoneNo != null && getZoneNo != '')||(getCortonNo != null && getCortonNo != '') ) 
			{

				var SOFilters = new Array();
				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
				if(getWaveNo != null && getWaveNo != "")
				{
					nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
					SOarray["custparam_error"] = st11;//'INVALID WAVE #';
					vOrdFormat = 'W';
					vForErrormsz='W';
				}
				if(getOrdNo!=null && getOrdNo!="")
				{
					nlapiLogExecution('DEBUG', 'getOrdNo inside if', getOrdNo);					
					SOFilters.push(new nlobjSearchFilter('name', null, 'is', getOrdNo));
					SOarray["custparam_error"] = st12;//'INVALID ORDER #';
					vOrdFormat = vOrdFormat + 'O';
					vForErrormsz=vForErrormsz +'O';
				} 
				if(getSONo!=null && getSONo!="")
				{
					nlapiLogExecution('DEBUG', 'getSONo inside if', getSONo);
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
					SOarray["custparam_error"] = st9;//'INVALID SALES ORDER #';
					vOrdFormat =  vOrdFormat + 'S';
					vForErrormsz= vForErrormsz +'S';
				}
				if(getZoneNo!=null && getZoneNo!="" && getZoneNo!='null')
				{
					nlapiLogExecution('DEBUG', 'getZoneNo inside if', getZoneNo);
					SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
					SOarray["custparam_error"] = st9;//'Invallid Zone#'
					vOrdFormat =  vOrdFormat + 'Z';
					vForErrormsz= vForErrormsz +'Z';
				}

				if(getCortonNo!=null && getCortonNo!="" && getCortonNo!='null')
				{
					nlapiLogExecution('DEBUG', 'getCortonNo inside if', getCortonNo);
					SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getCortonNo));
					SOarray["custparam_error"] = st13;//'INVALID CARTON'
					vOrdFormat =  vOrdFormat + 'C';
					vForErrormsz= vForErrormsz +'C';
				}
				if(getSONo != null && getSONo != "" && getWaveNo!=null && getWaveNo!="" && 
						getOrdNo!=null && getOrdNo!="" && getZoneNo!=null && getZoneNo!="" && getCortonNo!=null && getCortonNo!="")
				{
					vOrdFormat = 'ALL';
				}	
				var vRoleLocation=getRoledBasedLocation();
				if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
				{
					SOFilters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

				}
				SOarray["custparam_picktype"] = vOrdFormat;
				var SOColumns = new Array();

				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//				SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
				//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
				SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
				//Code end as on 290414
				SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
				SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
				SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
				SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
				SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
				SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
				SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
				SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
				SOColumns.push(new nlobjSearchColumn('name'));
				SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
				SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
				SOColumns.push(new nlobjSearchColumn('custrecord_taskassignedto'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

				SOColumns[0].setSort();
				SOColumns[1].setSort();
				SOColumns[2].setSort();
				SOColumns[3].setSort();
				SOColumns[4].setSort();
				SOColumns[5].setSort(true);
				var taskassignedto;
				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

				if (SOSearchResults != null && SOSearchResults.length > 0) {

					nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResults.length);

					var vorderno;
					var HoldStatus;
					var vordername;
					//var HoldStatusFlag = 'T';
					var OrdeNoArray = new Array();
					var vArrDistictFO=new Array();
					var vArrDistictSO=new Array();
					var vDuplicateFOs=false;
					var vArrDistictLines=new Array();
					
					for(var f=0; f < SOSearchResults.length; f++)
					{
						var currrow=[SOSearchResults[f].getValue('custrecord_ebiz_order_no'),SOSearchResults[f].getText('custrecord_ebiz_order_no')];
						OrdeNoArray.push(currrow);
						if(SOSearchResults[f].getValue('name') != null && SOSearchResults[f].getValue('name') != '' && vArrDistictFO.indexOf(SOSearchResults[f].getValue('name'))== -1 )
						{
							vArrDistictFO.push(SOSearchResults[f].getValue('name'));
							var veBizOrderId=SOSearchResults[f].getValue('custrecord_ebiz_order_no');
							var vlineNo=SOSearchResults[f].getValue('custrecord_line_no');

							if(veBizOrderId != null && veBizOrderId != '') 
							{
								if(vArrDistictSO.indexOf(veBizOrderId) != -1)
								{
									if(vlineNo != null && vlineNo != '') 
									{
										if(vArrDistictLines.indexOf(vlineNo) != -1 )
										{
											vDuplicateFOs=true;
											break;
										}
										else
											vArrDistictLines.push(vlineNo);
									}
								}
								else
								{
									vArrDistictSO.push(veBizOrderId);
									vArrDistictLines.push(vlineNo);
								}
							}
						}	

					}
					nlapiLogExecution('DEBUG', 'vArrDistictFO', vArrDistictFO);
					nlapiLogExecution('DEBUG', 'vArrDistictSO', vArrDistictSO);
					nlapiLogExecution('DEBUG', 'vDuplicateFOs', vDuplicateFOs);
					if(vDuplicateFOs==true)
					{
						//SOarray["custparam_error"] = st13;//'INVALID ZONE';
						SOarray["custparam_error"]='This Wave or Sales Order Contain Two different FOs, Either Pick by FO or delete the FOs and then recreate and release the FOs to pick by Order'
						SOarray["custparam_screenno"] = '12';

						nlapiLogExecution('DEBUG', 'Error: ', 'This Wave or Sales Order Contain Two different FOs, Either Pick by FO or delete the FOs and then recreate and release the FOs to pick by Order');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					nlapiLogExecution('Debug', 'Time Stamp at the start of determining Hold Status',TimeStampinSec());
					if(OrdeNoArray != null && OrdeNoArray != '')
					{
						OrdeNoArray = removeDuplicateElement(OrdeNoArray); 

						for(var u=0;u<OrdeNoArray.length;u++)
						{
							HoldStatus = isSOClosed(OrdeNoArray[u][0]);

							if(HoldStatus == 'F')
							{
								nlapiLogExecution('DEBUG', 'Hold Status Order',OrdeNoArray[u][1] );
								vordername = OrdeNoArray[u][1];
								break;
							}

						}
					}
					nlapiLogExecution('Debug', 'Time Stamp at the end of determining Hold Status',TimeStampinSec());

					taskassignedto=SOSearchResults[0].getValue('custrecord_taskassignedto');
					nlapiLogExecution('DEBUG', 'taskassignedto', taskassignedto);
					var currentContext = nlapiGetContext();  
					var currentUserID = currentContext.getUser();
					nlapiLogExecution('DEBUG', 'currentUserID', currentUserID);
					nlapiLogExecution('Debug', 'Time Stamp at the start of updating picking user',TimeStampinSec());
					if(taskassignedto==null || taskassignedto=='' )
					{
						for ( var i = 0; i < SOSearchResults.length; i++)
						{
							var RecID=SOSearchResults[i].getId();

							var fields=new Array();
							fields[0]='custrecord_taskassignedto';
							fields[1]='custrecord_actualbegintime';
							var Values=new Array();
							Values[0]=currentUserID;
							Values[1]=TimeStamp();
							var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',RecID,fields,Values);
						}
					}
					else if(currentUserID!=taskassignedto)
					{
						nlapiLogExecution('DEBUG', 'taskassignedto1', taskassignedto);

						var msg1='Picking Is In-Progress for this ';
						var msg2='. Contact Supervisor.';
						var errormsz='';
						nlapiLogExecution('DEBUG', 'vOrdFormat', vOrdFormat);
						var s = vForErrormsz;
						if(s.length>1){
							nlapiLogExecution('DEBUG', 's.length', s.length);
							for(var j=0;j<s.length;j++)
							{
								nlapiLogExecution('DEBUG', 'vmessage.length',s.charAt(i));
								if(j!=0 && j!= s.length-1)
									errormsz=errormsz +',';	
								if(j== s.length-1)
									errormsz=errormsz +' And';
								if(s.charAt(j)=='W')
									errormsz=errormsz +'Wave';
								if(s.charAt(j) =='O')
									errormsz=errormsz+" "+'Fulfillment Order';
								if(s.charAt(j) =='S')
									errormsz=errormsz+" "+'Sales Order';
								if(s.charAt(j)=='Z')
									errormsz=errormsz+" "+'Zone';
								if(s.charAt(j)=='C')
									errormsz=errormsz+" "+'Carton';
							}
						}
						else
						{nlapiLogExecution('DEBUG', 'else', vForErrormsz);
						if(vForErrormsz =='W')
							errormsz='Wave';
						if(vForErrormsz =='O')
							errormsz='Fulfillment Order';
						if(vForErrormsz =='S')
							errormsz='Sales Order';
						if(vForErrormsz =='Z')
							errormsz='Zone';
						if(vForErrormsz =='C')
							errormsz='Carton';
						}
						nlapiLogExecution('DEBUG', 'errormsz', errormsz);
						SOarray["custparam_error"] = msg1+errormsz+msg2;
						nlapiLogExecution('DEBUG', 'errorMessage', msg1+errormsz+msg2);
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					vSkipId=0;
					if(SOSearchResults.length <= parseFloat(vSkipId))
					{
						vSkipId=0;
					}
					nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
					var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];

//					SOarray["custparam_waveno"] = getWaveNo;
					SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no');
					SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
					//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
					SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
					SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
					SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
					SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
					SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
					SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
					SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
					SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
					SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
					SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
					SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
					SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
					SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
					SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
					SOarray["custparam_noofrecords"] = SOSearchResults.length;
					SOarray["name"] =  SOSearchResult.getValue('name');
					SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
					SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
					SOarray["custparam_venterSO"]=vSOId;
					SOarray["custparam_venterFO"]=getOrdNo;
					SOarray["custparam_venterwave"]=getWaveNo;
					SOarray["custparam_venterzone"]=vZoneId;
					SOarray["custparam_ventercarton"]=getCortonNo;
					/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/					
					SOarray["custparam_nextlocation"]='';	
					SOarray["custparam_nextiteminternalid"]='';
					/* Up to here */ 
					SOarray["custparam_fastpick"] = fastpick;
					var st13="Either SO is Closed/Cancelled or Payment is HOLD/Days Over Due/Credit Limit Exceed for  ";
					if(getZoneNo!=null && getZoneNo!="")
					{
						SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
					}
					else
					{
						SOarray["custparam_ebizzoneno"] = '';
					}
					/*//code added by santosh on 7Aug2012
					if(getWaveNo != null && getWaveNo !="" )
					{*/
					SOarray["custparam_type"] ="Wave";
					SOarray["custparam_fastpick"] = fastpick;
					nlapiLogExecution('DEBUG', 'WaveClusterScreen','WaveClusterScreen' );
					nlapiLogExecution('DEBUG', 'Location newly added', SOSearchResult.getText('custrecord_actbeginloc'));
					if(HoldStatus == "F")
					{
						SOarray["custparam_error"] = st13 + vordername;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					else
					{
						
						var orderArray=ISOrderPartiallyPickedForShipCompleteOrders(getWaveNo,getOrdNo,getSONo,vSOId,getCortonNo);//

						if(orderArray!=null&&orderArray!="")
						{
							var msg="";
							for(var j=0;j<orderArray.length;j++)
							{
								if(j==0)
									msg=orderArray[j];
								else
									msg=msg+","+orderArray[j];
							}
							SOarray["custparam_error"] = 'Picks generated partially for an orders# "' + msg + '" with ship complete';
							nlapiLogExecution('DEBUG', 'Error: ', 'Ship complete found in one of the order');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
						}
						
						if(fastpick!='Y')
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusterwave_summary', 'customdeploy_ebiz_rf_clusterwave_summary', false, SOarray);
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
						}
					}
					/*}
					else
					{
						nlapiLogExecution('DEBUG', 'Location', SOSearchResult.getText('custrecord_actbeginloc'));
						if(HoldStatus == "F")
						{
							SOarray["custparam_error"] = st13 + vordername;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
						}
						else
						{
							if(fastpick!='Y')
							{
								response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
							}
						}
					}*/
					//end of the code on 7Aug2012
					var getItemNo=SOSearchResult.getValue('custrecord_sku');
					var getControlno=SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
				}
				else {
					nlapiLogExecution('DEBUG', 'Error: ', 'Wave # not found');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

				}
			}
			else
			{
				nlapiLogExecution('DEBUG', 'Error: ', 'Wave # not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			}
		}
	}
}

function IsValid(num) {
	var valid=true;
	if(num!=null && num!='' && num!='null' && !isNaN(num) &&  num >=0 )
	{
		valid=true;
	}
	else
	{
		valid=false;

	}


	return valid;
}


function ISOrderPartiallyPickedForShipCompleteOrders(getWaveNo,getOrdNo,getSONo,vSOId,getCortonNo)
{
	try
	{
		var SOFilters = new Array();
		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9,26]));
		if(getWaveNo != null && getWaveNo != "")
		{
			nlapiLogExecution('ERROR', 'getWaveNo inside if', getWaveNo); 
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
		}
		if(getOrdNo!=null && getOrdNo!="")
		{
			nlapiLogExecution('ERROR', 'getOrdNo inside if', getOrdNo);					
			SOFilters.push(new nlobjSearchFilter('name', null, 'is', getOrdNo));
		} 
		if(getSONo!=null && getSONo!="")
		{
			nlapiLogExecution('ERROR', 'getSONo inside if', getSONo);
			nlapiLogExecution('ERROR', 'vSOId inside if', vSOId);
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
		}
		if(getCortonNo!=null && getCortonNo!="" && getCortonNo!='null')
		{
			nlapiLogExecution('DEBUG', 'getCortonNo inside if', getCortonNo);
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getCortonNo));
			
		}
		var SOColumns = new Array();
		SOColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		var SOSearchResult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		var SOArray=new Array();
		var OrderArray=new Array();
		if(SOSearchResult!=null&&SOSearchResult!="")
		{
			for(var i=0;i<SOSearchResult.length;i++)
			{
				SOArray.push(SOSearchResult[i].getValue('custrecord_ebiz_order_no'));
			}

			nlapiLogExecution('DEBUG', 'SOArray', SOArray);
			if(SOArray!=null&&SOArray!="")
			{
				var filter = new Array();
				filter[0]= new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', SOArray);
				filter[1]= new nlobjSearchFilter('custrecord_shipcomplete', null, 'is', "T"); 
				filter[2]= new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickgen_qty}),0)");

				var column = new Array();
				column[0] = new nlobjSearchColumn('custrecord_lineord');
				column[1] = new nlobjSearchColumn('custrecord_shipcomplete');
				column[2] = new nlobjSearchColumn('custrecord_pickgen_qty');
				column[3] = new nlobjSearchColumn('custrecord_ebiz_linesku');

				var searchRec = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);

				if(searchRec!=null&&searchRec!="")
				{
					for(var j=0;j<searchRec.length;j++)
						OrderArray.push(searchRec[j].getValue("custrecord_lineord"));			
				}
				nlapiLogExecution('DEBUG', 'OrderArray', OrderArray);
			}
		}
		return OrderArray;
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception in ISOrderPartiallyPickedForShipCompleteOrders",exp);
	}
}




function CheckCortonNo(vCarton,whloc)
{
	try
	{
		nlapiLogExecution('Debug', 'vCarton', vCarton);
		var filters = new Array();

		//filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', '1');
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', vCarton));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'anyof', [2]));
	
		if(whloc!=null && whloc!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', [whloc]));

	
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters,null);
		if(searchresults !=null && searchresults!='')
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	catch(e)
	{
		nlapiLogExecution('Debug', 'exception in vaild carton', e);
		return false;
	}


}
