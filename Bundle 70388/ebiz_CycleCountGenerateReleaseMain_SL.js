/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_CycleCountGenerateReleaseMain_SL.js,v $
<<<<<<< ebiz_CycleCountGenerateReleaseMain_SL.js
 *     	   $Revision: 1.14.2.11.4.9.2.37.2.10 $
 *     	   $Date: 2015/06/18 20:19:13 $
 *     	   $Author: grao $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_150 $
=======
 *     	   $Revision: 1.14.2.11.4.9.2.37.2.10 $
 *     	   $Date: 2015/06/18 20:19:13 $
 *     	   $Author: grao $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_150 $
>>>>>>> 1.14.2.11.4.9.2.34
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_CycleCountGenerateReleaseMain_SL.js,v $
 * Revision 1.14.2.11.4.9.2.37.2.10  2015/06/18 20:19:13  grao
 * SB issue fixes  201413119
 *
 * Revision 1.14.2.11.4.9.2.37.2.9  2015/04/13 07:41:49  rrpulicherla
 * Case#201412277
 *
 * Revision 1.14.2.11.4.9.2.37.2.8  2014/12/22 16:54:20  rrpulicherla
 * Case#201411317
 *
 * Revision 1.14.2.11.4.9.2.46  2014/12/22 16:49:09  rrpulicherla
 * Case#201411317
 *
 * Revision 1.14.2.11.4.9.2.45  2014/12/22 16:44:25  rrpulicherla
 * Case#201411317
 *
 * Revision 1.14.2.11.4.9.2.44  2014/11/05 10:09:57  sponnaganti
 * Case# 201410665
 * TPP SB Issue fixed
 *
 * Revision 1.14.2.11.4.9.2.43  2014/09/16 16:07:00  sponnaganti
 * Case# 201410326
 * TPP SB Issue fix
 *
 * Revision 1.14.2.11.4.9.2.42  2014/09/05 13:21:48  sponnaganti
 * case# 201410252
 * Stnd Bundle issue fix
 *
 * Revision 1.14.2.11.4.9.2.41  2014/09/02 10:59:27  gkalla
 * case#20149227
 * ACE and JB cycle count issues
 *
 * Revision 1.14.2.11.4.9.2.40  2014/07/23 23:49:20  gkalla
 * case#20149672
 * Alphacomm Cycle count issue
 *
 * Revision 1.14.2.11.4.9.2.39  2014/07/15 15:46:01  skavuri
 * Case# 20149384 Compatability Issue Fixed
 *
 * Revision 1.14.2.11.4.9.2.38  2014/07/10 15:21:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Alphacommchanges
 *
 * Revision 1.14.2.11.4.9.2.37  2014/06/13 11:04:45  grao
 * Case#: 20148881 New GUI account issue fixes
 *
 * Revision 1.14.2.11.4.9.2.35  2014/06/12 14:29:15  grao
 * Case#: 20148840  New GUI account issue fixes
 *
 * Revision 1.14.2.11.4.9.2.34  2014/06/06 14:29:46  skreddy
 * case # 20148750
 * sonic issue fix
 *
 * Revision 1.14.2.11.4.9.2.33  2014/04/16 07:50:02  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to20148011
 *
 * Revision 1.14.2.11.4.9.2.32  2014/03/29 21:33:45  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to 201218959
 *
 * Revision 1.14.2.11.4.9.2.31  2014/03/19 15:53:22  skavuri
 * Case # 20127750 issue fixed
 *
 * Revision 1.14.2.11.4.9.2.30  2014/03/13 15:48:02  skavuri
 * Case # 20127666 issue fixed
 *
 * Revision 1.14.2.11.4.9.2.29  2014/03/12 15:58:19  skavuri
 * Case # 20127666 issue fixed
 *
 * Revision 1.14.2.11.4.9.2.28  2014/02/24 14:48:44  rmukkera
 * Case # 20127333
 *
 * Revision 1.14.2.11.4.9.2.27  2014/01/07 09:39:54  rmukkera
 * Case # 20126424
 *
 * Revision 1.14.2.11.4.9.2.26  2014/01/06 15:20:40  gkalla
 * case#20126652
 * TSG Cycle count issue for bin location group selection
 *
 * Revision 1.14.2.11.4.9.2.25  2013/12/24 16:20:13  gkalla
 * case#20126522
 * Standard bundle issue
 *
 * Revision 1.14.2.11.4.9.2.24  2013/12/10 13:06:32  schepuri
 * 20126281
 *
 * Revision 1.14.2.11.4.9.2.23  2013/11/08 16:20:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Epicuren and surftech issue fixes
 *
 * Revision 1.14.2.11.4.9.2.22  2013/11/01 12:50:50  rmukkera
 * Case# 20124148
 *
 * Revision 1.14.2.11.4.9.2.21  2013/10/19 13:10:15  grao
 * Issue fixes 20125158
 *
 * Revision 1.14.2.11.4.9.2.20  2013/10/11 14:11:49  rmukkera
 * Case# 20124322�,20124323� ,20124764�,20124765�
 *
 * Revision 1.14.2.11.4.9.2.19  2013/09/06 07:33:12  rmukkera
 * Case# 20124148
 *
 * Revision 1.14.2.11.4.9.2.18  2013/08/05 15:17:22  rrpulicherla
 * case# 20123755
 * BB Cycle count issues
 *
 * Revision 1.14.2.11.4.9.2.17  2013/08/05 13:54:12  rmukkera
 * Standard bundle Issue Fix for  case NO 20123742
 *
 * Revision 1.14.2.11.4.9.2.16  2013/08/03 21:16:08  snimmakayala
 * Case# 201214994
 * Cycle Count Issue Fixes
 *
 * Revision 1.14.2.11.4.1  2012/10/30 06:10:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.14.2.11  2012/09/05 03:07:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Location Group filter issue.
 *
 * Revision 1.14.2.10  2012/07/04 14:09:52  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issues
 *
 * Revision 1.14.2.9  2012/06/28 16:03:06  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Cycle count Resolve files
 *
 * Revision 1.14.2.7  2012/05/22 07:05:00  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * By pass qty validation for discount items
 *
 * Revision 1.14.2.6  2012/04/27 15:13:48  spendyala
 * CASE201112/CR201113/LOG201121
 * Paging is added.
 *
 * Revision 1.14.2.5  2012/04/25 06:54:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Blind Cycle Count
 *
 * Revision 1.14.2.4  2012/04/06 14:03:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.14.2.3  2012/03/20 15:12:17  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to looping the bin location is resolved.
 *
 * Revision 1.14.2.2  2012/02/27 13:46:21  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing of lp search filter modified from 'anyof' to 'is'
 *
 * Revision 1.14.2.1  2012/02/07 12:34:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cyclecounting issue fixes
 *
 * Revision 1.15  2012/02/06 22:19:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * cycle count issue fixes
 *
 * Revision 1.14  2012/01/02 10:56:38  spendyala
 * CASE201112/CR201113/LOG201121
 * changed itemtext to itemvalue while passing to create cycle count record.
 *
 * Revision 1.13  2011/11/08 14:37:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cycle Count - Lot/Batch Issue Fixes
 *
 * Revision 1.12  2011/11/05 10:12:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cycle Count Issue Fixes
 *
 * Revision 1.11  2011/11/04 14:28:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cycle Count Issue Fixes
 *
 * Revision 1.10  2011/11/03 17:03:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Inventory status should be Storage once the CC plan is resolved or ignored
 *
 * Revision 1.9  2011/11/02 12:05:44  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Cycle Count file
 *
 * Revision 1.7  2011/10/25 13:13:44  gkalla
 * CASE201112/CR201113/LOG201121
 * To update NS while doing Cycle count Resolve
 *
 * Revision 1.6  2011/07/21 06:35:04  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function CycleCountGenerateMainSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Generate & Release');
		// case no 20126281
		nlapiLogExecution('ERROR', 'memberlineCnt', memberlineCnt);
		var msgFld=request.getParameter('custpage_msgfield');
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null).setLayoutType('outside','startrow');
		form.addField('custpage_msgfield','text','').setDisplayType('hidden');
		if(msgFld!=null && msgFld!='' && msgFld=='nolinesselected')
		{
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Please select atleast one line', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");	
		}
		var varCCplanNo = request.getParameter('custpage_ccount');
		nlapiLogExecution('ERROR','varCCplanNo get',varCCplanNo);

		form.setScript('customscript_cyclecountresolvesubmit');
		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');

		var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
		hiddenfieldselectpage.setDefaultValue('F');


		var hiddenfieldCyclecount=form.addField('custpage_ccount', 'text', 'hiddenflagcyclecount').setDisplayType('hidden');
		hiddenfieldCyclecount.setDefaultValue(varCCplanNo);

		var planno = form.addField('custpage_cyclecountplan', 'text', 'Cycle Count Plan No:').setDisplayType('disabled');
		planno.setDefaultValue(varCCplanNo);


		addFieldsToForm(form);
		//Case#: 20125158 Start -->restrcit the error if plan# not selecting.
		var fields;
		var columns;
		var vaisle;
		var vemptybinloc;
		if(varCCplanNo!=null && varCCplanNo!=''){
			fields = ['custrecord_ebiz_aisle', 'custrecord_ebiz_include_emploc'];
			columns = nlapiLookupField('customrecord_ebiznet_cylc_createplan', varCCplanNo, fields);

			vaisle = columns.custrecord_ebiz_aisle;
			vemptybinloc= columns.custrecord_ebiz_include_emploc;
		}
//		end
		var orderlist_Emptybin="";
		if(vemptybinloc=='T')
			orderlist_Emptybin=GetEmptyBinList(request,-1,form);

		var orderList = getOrdersForCycleCount(request,-1,form);
		nlapiLogExecution('ERROR','orderlist_Emptybin',orderlist_Emptybin);
		nlapiLogExecution('ERROR','orderList',orderList);
		if((orderList != null && orderList!="")||(orderlist_Emptybin!=null&&orderlist_Emptybin!=""))
		{
			setPagingForSublist(orderList,form,varCCplanNo,orderlist_Emptybin,request);
		}
		response.writePage(form);
		form.addSubmitButton('Generate & Release');

	}
	else //After submit clicked
	{
		var varCCplanNo = request.getParameter('custpage_ccount');
		if (request.getParameter('custpage_hiddenfieldselectpage') != 'F')
		{
			nlapiLogExecution('ERROR','into IF');
			var varCCplanNo = request.getParameter('custpage_ccount');

			nlapiLogExecution('ERROR','varCCplanNo post',varCCplanNo);
			var form = nlapiCreateForm('Generate & Release');
			form.setScript('customscript_cyclecountresolvesubmit');
			var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');

			var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
			hiddenfieldselectpage.setDefaultValue('F');

			var hiddenfieldCyclecount=form.addField('custpage_ccount', 'text', 'hiddenflagcyclecount').setDisplayType('hidden');
			hiddenfieldCyclecount.setDefaultValue(varCCplanNo);


			addFieldsToForm(form);

			var fields;
			var columns;
			var vaisle;
			var vemptybinloc;
			if(varCCplanNo!=null && varCCplanNo!=''){
				fields = ['custrecord_ebiz_aisle', 'custrecord_ebiz_include_emploc'];
				columns = nlapiLookupField('customrecord_ebiznet_cylc_createplan', varCCplanNo, fields);

				vaisle = columns.custrecord_ebiz_aisle;
				vemptybinloc= columns.custrecord_ebiz_include_emploc;
			}
			var orderlist_Emptybin="";
			if(vemptybinloc=='T')
				orderlist_Emptybin=GetEmptyBinList(request,-1,form);

			var orderList = getOrdersForCycleCount(request,-1,form);
			if((orderList != null && orderList.length > 0)||(orderlist_Emptybin!=null&&orderlist_Emptybin!=""))
			{
				setPagingForSublist(orderList,form,varCCplanNo,orderlist_Emptybin,request);
			}
			response.writePage(form);
			form.addSubmitButton('Generate & Release');
		}
		else
		{
			nlapiLogExecution('ERROR','into ELSE');

			var memberlineCnt = request.getLineItemCount('custpage_cyclecountgenerate_items');
			//Case # 20125456� Start
			if(memberlineCnt >0)
			{
				//Case # 20125456� End
				var newRecord = nlapiLoadRecord('customrecord_ebiznet_cylc_createplan', 
						request.getLineItemValue('custpage_cyclecountgenerate_items', 'custpage_cyclecountinternalid', 1)); // 2 UNITS
				nlapiLogExecution('ERROR', 'Recid Length', newRecord.length);


				var vloc = newRecord.getFieldValue('custrecord_ebiz_cycl_binloc');        
				var vitemfamily = newRecord.getFieldValue('custrecord_ebiz_skufamily');
				var vitemgroup = newRecord.getFieldValue('custrecord_ebiz_skugroup');
				var vitem = newRecord.getFieldText('custrecord_ebiz_sku');
				var vitemid = newRecord.getFieldValue('custrecord_ebiz_sku');
				var vitemMultiSel = newRecord.getFieldValue('custrecord_ebiz_sku_multisel');

				var vitemMultiSelTemp = new Array();
				if(vitemMultiSel != null && vitemMultiSel != '')
				{			
					vitemMultiSelTemp = vitemMultiSel.split('');
					nlapiLogExecution('ERROR', 'vitemMultiSelTemp', vitemMultiSelTemp.length);
				}

				nlapiLogExecution('ERROR', 'vitemMultiSelTemp', vitemMultiSelTemp);

				var vpackcode = newRecord.getFieldText('custrecord_ebiz_cycl_packcode');
				var vlotbatch = newRecord.getFieldValue('custrecord_ebiz_batch');
				var vaisle = newRecord.getFieldText('custrecord_ebiz_aisle');
				var vlevel = newRecord.getFieldText('custrecord_ebiz_level');
				var vbinlocgroup = newRecord.getFieldText('custrecord_ebiz_locationgroup');
				var vbinlocgroupid = newRecord.getFieldValue('custrecord_ebiz_locationgroup');
				var vlp = newRecord.getFieldText('custrecord_cycl_lp');
				var vlpid = newRecord.getFieldValue('custrecord_cycl_lp');
				var vcompany = newRecord.getFieldValue('custrecord_cycle_count_company');
				var vSiteLoc = newRecord.getFieldValue('custrecord_ebiz_cyclecntplan_location');
				nlapiLogExecution('ERROR', 'vSiteLoc:', vSiteLoc);

				var vABCVelocity = newRecord.getFieldValue('custrecord_ebiz_abcvelocity');
				nlapiLogExecution('ERROR', 'vABCVelocity in else:', vABCVelocity);

				var vEmptyBinLoc = newRecord.getFieldValue('custrecord_ebiz_include_emploc');
				nlapiLogExecution('ERROR', 'vEmptyBinLoc', vEmptyBinLoc);
				//Case # 20125456� Start
			}
			//Case # 20125456� End.

			var isSelected=false;
			for (var k = 1; k <= memberlineCnt; k++) {
				var checkflag = request.getLineItemValue('custpage_cyclecountgenerate_items', 'custpage_cyclecount', k);
				var varloc = request.getLineItemValue('custpage_cyclecountgenerate_items', 'custpage_cyclecountlocationvalue', k);
				var varlocText = request.getLineItemValue('custpage_cyclecountgenerate_items', 'custpage_cyclecountlocation', k);
				var cycnplanid = request.getLineItemValue('custpage_cyclecountgenerate_items', 'custpage_cyclecountinternalid', k);
				if (checkflag == 'T') {
					isSelected=true;
					nlapiLogExecution('ERROR', 'varloc', varloc);
					nlapiLogExecution('ERROR', 'varlocText', varlocText);
					var vhold = 'T';
					var vcyclecounthold = 'T';

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [varloc]));
					filters.push(new nlobjSearchFilter('custrecord_ebiz_invholdflg', null, 'is', 'F'));
					filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

					if (vloc != null && vloc != "") {
						nlapiLogExecution('ERROR', 'LOCATION filtered',vloc);
						filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [vloc]));
					}
					if (vitemfamily != null && vitemfamily != "") {
						nlapiLogExecution('ERROR', 'vitemfamily filtered', vitemfamily);
						filters.push(new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_inv_sku', 'is', vitemfamily));
					}
					if (vitemgroup != null && vitemgroup != "") {
						nlapiLogExecution('ERROR', 'vitemgroup filtered', vitemgroup);
						filters.push(new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_inv_sku', 'is', vitemgroup));
					}
					if (vitemid != null && vitemid != "") {
						nlapiLogExecution('ERROR', 'ITEM filtered',vitemid);
						filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', [vitemid]));
					}
					if (vitemid == null || vitemid == "") {
						if(vitemMultiSelTemp != null && vitemMultiSelTemp != '')
						{
							nlapiLogExecution('DEBUG', 'vitemMultiSel filtered',vitemMultiSelTemp);
							filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', vitemMultiSelTemp));
						}
					}
					if (vpackcode != null && vpackcode != "") {
						nlapiLogExecution('ERROR', 'PACK CODE filtered');
						filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', ['@NONE@',vpackcode]));
					}
					if (vlotbatch != null && vlotbatch != "") {
						nlapiLogExecution('ERROR', 'LOT filtered');
						filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', ['@NONE@',vlotbatch]));
					}
					if (vlevel != null && vlevel != "") {
						nlapiLogExecution('ERROR', 'LEVEL filtered');
						filters.push(new nlobjSearchFilter('custrecord_ebiz_loclvl', null, 'is', vlevel));
					}
					if (vbinlocgroup != null && vbinlocgroup != "") {
						nlapiLogExecution('ERROR', 'Bin Location Group filtered');
						//filters.push(new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', ['@NONE@',vbinlocgroupid]));
						//filters.push(new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', [vbinlocgroupid]));
						filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', [vbinlocgroup]));
					}
					if (vlp != null && vlp != "") {
						nlapiLogExecution('ERROR', 'LP filtered', vlp);
						filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', vlp));
					}
					if (vcompany != null && vcompany != "") {
						nlapiLogExecution('ERROR', 'Company filtered');
						filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', ['@NONE@',vcompany]));
					}

					if (vSiteLoc != null && vSiteLoc != "") {
						nlapiLogExecution('ERROR', 'Site filtered',vSiteLoc);
						filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [vSiteLoc]));
					}

					filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag',null, 'anyof', ['19']));

					if (vABCVelocity != null && vABCVelocity != "") 
					{
						nlapiLogExecution('ERROR', 'vABCVelocity filtered',vABCVelocity);

						var filtersvelocity = new Array();
						filtersvelocity.push(new nlobjSearchFilter('custitem_ebizabcvelitem', null, 'anyof', vABCVelocity));

						var colsvelocity = new Array();
						colsvelocity[0]=new nlobjSearchColumn('itemid');


						var searchresults = nlapiSearchRecord('item', null, filtersvelocity, colsvelocity);

						if (searchresults != null && searchresults != "")
						{
							nlapiLogExecution('ERROR', 'searchresults.length filtered11',searchresults.length);
							var TotalListOfSKUs=new Array();
							for ( var x = 0; x < searchresults.length; x++) 
							{
								TotalListOfSKUs[x]=searchresults[x].getId();

							}
						}

						if (TotalListOfSKUs != null && TotalListOfSKUs != "")
						{
							nlapiLogExecution('ERROR', 'TotalListOfSKUs filtered11',TotalListOfSKUs);
							filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof', TotalListOfSKUs));

						}
					}


					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
					columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
					columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
					columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
					columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
					columns[5] = new nlobjSearchColumn('custrecord_ebiz_qoh');
					columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
					columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
					columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_adjusttype');
					columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_account_no');
					columns[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
					columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_note1');
					columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_note2');
					columns[13] = new nlobjSearchColumn('custrecord_ebiz_itemdesc');
					columns[0].setSort(true);
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
					if(searchresults==null && vEmptyBinLoc=='T')
					{
						var binloc=request.getLineItemValue('custpage_cyclecountgenerate_items', 'custpage_cyclecountlocationvalue', k);
						var whloc=request.getLineItemValue('custpage_cyclecountgenerate_items', 'custpage_ebizsiteid', k);
						nlapiLogExecution('ERROR', 'binloc ',binloc);
						var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
						customrecord.setFieldValue('custrecord_tasktype', 7);
						customrecord.setFieldValue('custrecordact_begin_date', DateStamp());//(parseInt(d.getMonth()) + 1) + '/' + (parseInt(d.getDate())) + '/' + d.getFullYear());
						customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp()); //((curr_hour) + ":" + (curr_min) + " " + a_p));
						customrecord.setFieldValue('custrecord_expe_qty', null);
						customrecord.setFieldValue('custrecord_lpno', null);
						customrecord.setFieldValue('custrecord_sku', null);
						customrecord.setFieldValue('custrecord_ebiz_sku_no', null);
						customrecord.setFieldValue('custrecord_actbeginloc', binloc);
						customrecord.setFieldValue('custrecord_ebiz_cntrl_no', cycnplanid);					
						customrecord.setFieldValue('name', cycnplanid);

						var opentaskid = nlapiSubmitRecord(customrecord);
						var cyclecntrec = nlapiCreateRecord('customrecord_ebiznet_cyclecountexe');
						cyclecntrec.setFieldValue('custrecord_cycle_count_plan', cycnplanid);
						cyclecntrec.setFieldValue('name', cycnplanid);
						cyclecntrec.setFieldValue('custrecord_cyclerec_date', DateStamp());					
						cyclecntrec.setFieldValue('custrecord_cycrec_time', TimeStamp());
						cyclecntrec.setFieldValue('custrecord_cyclesku',null);
						cyclecntrec.setFieldValue('custrecord_cyclecount_exp_ebiz_sku_no', null);
						cyclecntrec.setFieldValue('custrecord_cycleexp_qty', null);
						cyclecntrec.setFieldValue('custrecord_cyclelpno', null);
						cyclecntrec.setFieldValue('custrecord_cyclestatus_flag', 20);//status flag is 'R'
						cyclecntrec.setFieldValue('custrecord_cycact_beg_loc',binloc);
						cyclecntrec.setFieldValue('custrecord_opentaskid', opentaskid);
						cyclecntrec.setFieldValue('custrecord_invtid', null);
						cyclecntrec.setFieldValue('custrecord_expcycleabatch_no',null);
						cyclecntrec.setFieldValue('custrecord_expcyclesku_status', null);
						cyclecntrec.setFieldValue('custrecord_expcyclepc', null);
						cyclecntrec.setFieldValue('custrecord_cyclesite_id', vSiteLoc);
						//nlapiLogExecution('ERROR', 'expserialno ', expserialno);
						//cyclecntrec.setFieldValue('custrecord_cycle_expserialno', expserialno);
						var cyclecntRecId = nlapiSubmitRecord(cyclecntrec);
						nlapiLogExecution('ERROR', 'Out of Cycle Count Execution creation ');


					}
					else
					{			

						var vinvlocvalue, vinvloc, recid;
						var vqty, lpID, vsku, vskuText,batchNo, vItemStatus,vPackcode,Site,SiteId;

						if(searchresults!=null && searchresults!='')
						{
							nlapiLogExecution('ERROR', 'searchresults.length',searchresults.length);
							if(searchresults.length>100)
							{

								var form1 = nlapiCreateForm('Generate & Release');
								var msg2 = form1.addField('custpage_messages', 'inlinehtml', null, null, null);
								msg2.setLayoutType('outside','startcol');


								var param = new Array();
								param['custscript_cyccvarloc'] = varloc;
								param['custscript_cyccvloc'] = vloc;
								param['custscript_cyccitemfamily'] = vitemfamily;
								param['custscript_cyccitemgroup'] = vitemgroup;
								param['custscript_cyccitemid'] = vitemid;
								param['custscript_cyccmultiitem'] = vitemMultiSelTemp;
								param['custscript_cyccpackcode'] = vpackcode;
								param['custscript_cycclotbatch'] = vlotbatch;
								param['custscript_cyccvlevel'] = vlevel;
								param['custscript_cyccbinlocgroup'] = vbinlocgroup;
								param['custscript_cycclp'] = vlp;
								param['custscript_cycccompany'] = vcompany;
								param['custscript_cyccSiteLoc'] = vSiteLoc;
								param['custscript_cyccABCVelocity'] = vABCVelocity;
								param['custscript_cyccplan'] = cycnplanid;

								nlapiScheduleScript('customscript_nswms_cyclecountgeneration', null,param);
								showInlineMessage(form1, 'Confirmation', 'Cycle count Generate & Release has been initiated');
//								msg2.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'QuickShip Scheduler process is initiated' , NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
								response.writePage(form1);
								refreshpage=null;
								return true;

							}
							else
							{

								for (var i = 0; searchresults != null && i < searchresults.length; i++) {
									nlapiLogExecution('ERROR', 'Into searchresults');
									var searchresult = searchresults[i];
									vinvloc = searchresult.getValue('custrecord_ebiz_inv_binloc');
									recid = searchresult.getId();
									vqty = searchresult.getValue('custrecord_ebiz_qoh');
									vsku = searchresult.getValue('custrecord_ebiz_inv_sku');
									vskuText = searchresult.getText('custrecord_ebiz_inv_sku');
									lpID = searchresult.getValue('custrecord_ebiz_inv_lp');
									batchNo= searchresult.getText('custrecord_ebiz_inv_lot');
									vItemStatus = searchresult.getValue('custrecord_ebiz_inv_sku_status');
									vPackcode = searchresult.getValue('custrecord_ebiz_inv_packcode');
									Site = searchresult.getText('custrecord_ebiz_inv_loc');
									SiteId = searchresult.getValue('custrecord_ebiz_inv_loc');
									//nlapiLogExecution('ERROR', 'SS--->vinvloc', vinvloc);

									var filters = new Array();
									filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', vsku);
									filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lpID);
									filters[2] = new nlobjSearchFilter('custrecord_serial_location', null, 'is', SiteId);
									filters[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']);

									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
									var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             
									var expserialno = "";
									if(searchResults != null && searchResults != "")
									{ 
										nlapiLogExecution('ERROR', 'searchResults', searchResults.length);
										nlapiLogExecution('ERROR', 'vqty', vqty);
										for (var a = 0; a < searchResults.length ; a++) 
										{   


											if(a < parseFloat(vqty)){
												//nlapiLogExecution('ERROR', 'a', a);
												if(searchResults[a].getValue('custrecord_serialnumber') != null && searchResults[a].getValue('custrecord_serialnumber') != "")
												{
													if(expserialno == "")
													{
														expserialno = searchResults[a].getValue('custrecord_serialnumber');
													}
													else
													{
														expserialno = expserialno + "," + searchResults[a].getValue('custrecord_serialnumber');
													}									
												}
											}
											else
											{
												nlapiLogExecution('ERROR', 'from break', a);
												break;
											}
										}
									}
									if (vinvloc != "") {
										var vIntValue="N";
										nlapiLogExecution('ERROR', 'recid for update INside ', recid);


										var scount=1;

										LABL1: for(var i1=0;i1<scount;i1++)
										{	

											nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i1);
											try
											{
												var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', recid);
												transaction.setFieldValue('custrecord_ebiz_invholdflg', vhold);
												transaction.setFieldValue('custrecord_ebiz_cycl_count_hldflag', vcyclecounthold);						
												transaction.setFieldValue('custrecord_ebiz_callinv', vIntValue);
												nlapiSubmitRecord(transaction, false, true);
												nlapiLogExecution('ERROR', 'Cyclecount generated for inv ref ',recid);										


											}
											catch(ex)
											{
												var exCode='CUSTOM_RECORD_COLLISION'; 
												var wmsE='Inventory record being updated by another user. Please try again...';
												if (ex instanceof nlobjError) 
												{	
													wmsE=ex.getCode() + '\n' + ex.getDetails();
													exCode=ex.getCode();
												}
												else
												{
													wmsE=ex.toString();
													exCode=ex.toString();
												} 									
												nlapiLogExecution('ERROR', 'Exception in Cycle count Generate release : ', wmsE); 
												if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
												{ 
													scount=scount+1;
													continue LABL1;
												}
												else break LABL1;
											}
										}
									}

									nlapiLogExecution('ERROR', 'Into CYCC task creation ');
									var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
									customrecord.setFieldValue('custrecord_tasktype', 7);
									customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
									customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
									customrecord.setFieldValue('custrecord_expe_qty', vqty);
									customrecord.setFieldValue('custrecord_lpno', lpID);
									customrecord.setFieldValue('custrecord_sku', vsku);
									customrecord.setFieldValue('custrecord_ebiz_sku_no', vsku);
									customrecord.setFieldValue('custrecord_actbeginloc', vinvloc);
									customrecord.setFieldValue('custrecord_ebiz_cntrl_no', cycnplanid);					
									customrecord.setFieldValue('name', (cycnplanid + "-" + lpID + "-" + i));

									var opentaskid = nlapiSubmitRecord(customrecord);
									nlapiLogExecution('ERROR', 'Out of CYCC task creation', recid);
									nlapiLogExecution('ERROR', 'Into Cycle Count Execution creation ');
									var cyclecntrec = nlapiCreateRecord('customrecord_ebiznet_cyclecountexe');
									cyclecntrec.setFieldValue('custrecord_cycle_count_plan', cycnplanid);
									cyclecntrec.setFieldValue('name', (cycnplanid + "-" + lpID + "-" + i));
									cyclecntrec.setFieldValue('custrecord_cyclerec_date', DateStamp());					
									cyclecntrec.setFieldValue('custrecord_cycrec_time', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cyclesku', vsku);
									cyclecntrec.setFieldValue('custrecord_cyclecount_exp_ebiz_sku_no', vsku);
									cyclecntrec.setFieldValue('custrecord_cycleexp_qty', vqty);
									cyclecntrec.setFieldValue('custrecord_cyclelpno', lpID);
									cyclecntrec.setFieldValue('custrecord_cyclestatus_flag', 20);//status flag is 'R'
									cyclecntrec.setFieldValue('custrecord_cycact_beg_loc', vinvloc);
									cyclecntrec.setFieldValue('custrecord_opentaskid', opentaskid);
									cyclecntrec.setFieldValue('custrecord_invtid', recid);
									cyclecntrec.setFieldValue('custrecord_expcycleabatch_no', batchNo);
									cyclecntrec.setFieldValue('custrecord_expcyclesku_status', vItemStatus);
									cyclecntrec.setFieldValue('custrecord_expcyclepc', vPackcode);
									cyclecntrec.setFieldValue('custrecord_cyclesite_id', vSiteLoc);
									cyclecntrec.setFieldValue('custrecord_cycle_expserialno', expserialno);
									nlapiLogExecution('ERROR', 'expserialno ',expserialno);
									var cyclecntRecId = nlapiSubmitRecord(cyclecntrec);
									nlapiLogExecution('ERROR', 'Out of Cycle Count Execution creation ');
								}
							}
						}
					}
				}

			}
			if(isSelected)
			{
				//need to navigate to other record.
				response.sendRedirect('SUITELET', 'customscript_cyclecountrecord', 'customdeploy_cyclecountrecord', false);
			}
			else
			{
				var CCArray= new Array();
				CCArray["custpage_msgfield"]="nolinesselected";
				CCArray["custpage_ccount"]=request.getParameter('custpage_ccount');   			
				response.sendRedirect('SUITELET', 'customscript_cycc_generate_main', 'customdeploy_cycc_generate_main_di',false, CCArray);
				return;
			}
		}
	}
} //end of main function



var searchResultArray=new Array();
function getOrdersForCycleCount(request,maxno,form)
{
	try{
		var localVarArray;
		localVarArray = validateRequestParams(request,form);
		nlapiLogExecution('Error', 'localVarArrayinelse', localVarArray);

		// Get all the search filters for wave generation
		var filters = new Array();   
		filters = specifyInvtFilters(localVarArray,maxno);


		nlapiLogExecution('Error', 'afterspecifyfilters', localVarArray);
		// Adding search columns
		var columns = new Array();
		columns=addColumnsForSearch();
		nlapiLogExecution('Error', 'afterspecifycolumns', localVarArray);

		var orderList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

		if( orderList!=null && orderList.length>=1000)
		{
			nlapiLogExecution('Error', 'orderList.length', orderList.length);
			searchResultArray.push(orderList); 
			var maxno=orderList[orderList.length-1].getValue(columns[1]);
			nlapiLogExecution('Error', 'maxno', maxno);
			getOrdersForCycleCount(request,maxno,form);	

		}
		else if(orderList!=null&&orderList!="")
		{
			searchResultArray.push(orderList); 
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in getOrdersForCycleCount',exp);

	}
	nlapiLogExecution('ERROR','searchResultArray',searchResultArray);
	return searchResultArray;
}


function setPagingForSublist(orderList,form,varCCplanNo,orderlist_Emptybin,request)
{
	var lengthofOrderlist=0;
	if((orderList != null && orderList!="")||(orderlist_Emptybin != null && orderlist_Emptybin!="")){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();	
		//If create inventory search result is not empty then load orderlisArray with each obj of search result
		if(orderList!=null&&orderList!="")
		{
			for(k=0;k<orderList.length;k++)
			{
				nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
				var ordsearchresult = orderList[k];

				if(ordsearchresult!=null)
				{
					nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
					for(var j=0;j<ordsearchresult.length;j++)
					{
						orderListArray[orderListArray.length]=ordsearchresult[j];				
					}
				}
			}
		}
		//lengthofOrderlist gives the last searchrec no. which is pushed to orderlistArray .
		//This Variable is used at the time of binding to the sublist.
		lengthofOrderlist=orderListArray.length;
		nlapiLogExecution('ERROR','lengthofOrderlist',lengthofOrderlist);

		//If emptybin location results returns value then the below code with push each record to the orderlistArray.
		if(orderlist_Emptybin!=null&&orderlist_Emptybin!="")
		{
			for(var i=0;i<orderlist_Emptybin.length;i++)
			{
				nlapiLogExecution('ERROR', 'orderlist_Emptybin[i]', orderlist_Emptybin[i]); 
				var ordsearchresult = orderlist_Emptybin[i];

				if(ordsearchresult!=null)
				{
					nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
					for(var j=0;j<ordsearchresult.length;j++)
					{
						orderListArray[orderListArray.length]=ordsearchresult[j];				
					}
				}
			}
		}
		else
			lengthofOrderlist=lengthofOrderlist+1;
		nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 

		var test='';

		if(orderListArray.length>0)
		{

			if(orderListArray.length>100)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("100");
					pagesizevalue=100;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{pagesizevalue= request.getParameter('custpage_pagesize');}
					else
					{pagesizevalue= 100;pagesize.setDefaultValue("100");}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);
				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	
					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	
					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}
			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{

					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}

			var c=0;
			var minvalue;
			minvalue=minval;
			nlapiLogExecution('ERROR','minvalue',minvalue);
			nlapiLogExecution('ERROR','maxval',maxval);
			for(var j = minvalue; j < maxval; j++){		
				var currentOrder = orderListArray[j];
				setLineItemValuesFromSearch(form, currentOrder, c,varCCplanNo,j,lengthofOrderlist);
				c=c+1;
			}
		}
	}
}


function addFieldsToForm(form)
{
	/*
	 * Defining SubList lines
	 */
	var ItemSubList = form.addSubList("custpage_cyclecountgenerate_items", "list", "Generate & Release");

	ItemSubList.addMarkAllButtons();
	ItemSubList.addField("custpage_cyclecount", "checkbox", "CycleCount").setDefaultValue('F');
	ItemSubList.addField("custpage_cyclecountlocation", "text", "Location");
	ItemSubList.addField("custpage_cyclecountlocationvalue", "text", "LocationValue").setDisplayType('hidden');

	ItemSubList.addField("custpage_cyclecountinternalid", "text", "Intid").setDisplayType('hidden');
	ItemSubList.addField("custpage_ebizsiteid", "text", "siteid");//.setDisplayType('hidden');
}


function addColumnsForSearch()
{	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_loc',null,'group');
	columns[0].setSort();

	return columns;
}

function specifyInvtFilters(localVarArray,maxno)
{
	var vloc = localVarArray[0][0];
	var vitemfamily = localVarArray[0][1];
	var vitemgroup = localVarArray[0][2];
	var vitemid = localVarArray[0][3];
	var vpackcode = localVarArray[0][4];
	var vlotbatch = localVarArray[0][5];
	var vlevel = localVarArray[0][6];
	var vbinlocgroup = localVarArray[0][7];
	var vlp = localVarArray[0][8];
	var vcompany = localVarArray[0][9];
	var vSiteLoc = localVarArray[0][10];
	var vABCVelocity = localVarArray[0][11];
	var vAisle = localVarArray[0][12];
	var vpalletposition = localVarArray[0][13];
	var vitemMultiSel = localVarArray[0][15];
	var varCCplanNo = localVarArray[0][16];


	var filters = new Array();

	// Case# 20127750 starts
	if (vloc != null && vloc != "") {
		nlapiLogExecution('DEBUG', 'vloc filtered', vloc);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'anyof', vloc));
	}
	// Case# 20127750 end


	if (vitemfamily != null && vitemfamily != "") {
		nlapiLogExecution('ERROR', 'vitemfamily filtered', vitemfamily);
		filters.push(new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_inv_sku', 'is', vitemfamily));
	}
	if (vitemgroup != null && vitemgroup != "") {
		nlapiLogExecution('ERROR', 'vitemgroup filtered', vitemgroup);
		filters.push(new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_inv_sku', 'is', vitemgroup));
	}
	if (vitemid != null && vitemid != "") {
		nlapiLogExecution('ERROR', 'ITEM filtered',vitemid);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', [vitemid]));
	}
	if (vpackcode != null && vpackcode != "") {
		nlapiLogExecution('ERROR', 'PACK CODE filtered',vpackcode);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof',['@NONE@',vpackcode]));
	}
	if (vlotbatch != null && vlotbatch != "") {
		nlapiLogExecution('ERROR', 'LOT filtered',vlotbatch);
		//Case # 20126424� Start
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', ['@NONE@',vlotbatch]));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', [vlotbatch]));
		//Case # 20126424� End

	}

	if (vlp != null && vlp != "") {

		nlapiLogExecution('ERROR', 'LP filtered text',vlp);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', vlp));
	}
	if (vcompany != null && vcompany != "") {
		nlapiLogExecution('ERROR', 'Company filtered',vcompany);
		//Case# 20149384 starts
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', ['@NONE@',vcompany]));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [vcompany]));
		//Case# 20149384 ends
	}

	if (vSiteLoc != null && vSiteLoc != "") {
		nlapiLogExecution('ERROR', 'Site filtered',vSiteLoc);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'anyof', [vSiteLoc]));
	}

	if (vitemid == null || vitemid == "") {

		if(vitemMultiSel != null && vitemMultiSel != '')
		{
			nlapiLogExecution('ERROR', 'vitemMultiSel filtered',vitemMultiSel);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', vitemMultiSel));
		}
	}

	nlapiLogExecution('ERROR', 'Location Type filtered');

	filters.push(new nlobjSearchFilter('custrecord_ebiz_invholdflg', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc', 'is', 'F'));
	if (vABCVelocity != null && vABCVelocity != "") 
	{

		nlapiLogExecution('ERROR', 'vABCVelocity filtered',vABCVelocity);

		var filtersvelocity = new Array();
		filtersvelocity.push(new nlobjSearchFilter('custitem_ebizabcvelitem', null, 'anyof', vABCVelocity));

		var colsvelocity = new Array();
		colsvelocity[0]=new nlobjSearchColumn('itemid');

		var searchresults = nlapiSearchRecord('item', null, filtersvelocity, colsvelocity);

		if (searchresults != null && searchresults != "")
		{
			nlapiLogExecution('ERROR', 'searchresults.length filtered11',searchresults.length);
			var TotalListOfSKUs=new Array();
			for ( var x = 0; x < searchresults.length; x++) 
			{
				TotalListOfSKUs[x]=searchresults[x].getId();

			}
		}

		if (TotalListOfSKUs != null && TotalListOfSKUs != "")
		{
			nlapiLogExecution('ERROR', 'TotalListOfSKUs filtered11',TotalListOfSKUs);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof', TotalListOfSKUs));
		}

	}

	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag',null, 'anyof', ['19']));
	//case #20127333 Start
	if(vAisle!=null && vAisle!="")
	{
		nlapiLogExecution('ERROR', 'vAisle',vAisle);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_aisle', null, 'is', vAisle));

	}
	if(vpalletposition!=null && vpalletposition!="")
	{
		nlapiLogExecution('ERROR', 'vpalletposition',vpalletposition);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_palletposi', null, 'is', vpalletposition));

	}
	if(vlevel!=null && vlevel!="")
	{
		nlapiLogExecution('ERROR', 'vlevel',vlevel);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_level', null, 'is', vlevel));

	}	

	if(vbinlocgroup != null && vbinlocgroup != '')
	{
		filters.push(new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', vbinlocgroup));
	}
	var Totalbinloc=null;
	if((vitemid==null || vitemid=='')&&(vloc==null||vloc=="")&&(vlp==null||vlp=="")&&(vitemMultiSel==null||vitemMultiSel==""))
	{
		if((vAisle == null || vAisle == '') && (vpalletposition == null || vpalletposition == '') && (vbinlocgroup == null || vbinlocgroup == '') && (vlevel == null || vlevel == ''))
		{
			filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', 'custrecord_ebiz_inv_binloc', 'anyof', ['6','7']));
		}
		else
		{	
			Totalbinloc=GetTotalBinLoc(vAisle,vpalletposition,vbinlocgroup,vlevel,-1,vSiteLoc);
		}
	}//case# 201410252 (added } for if condition)
	if(Totalbinloc!=null && Totalbinloc!="")
	{
		nlapiLogExecution('ERROR', 'LOCATION filtered from LocGroup',Totalbinloc);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', Totalbinloc));

	}
	else
	{
		// Case#20127666 starts
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', ['@NONE@']));
		nlapiLogExecution('ERROR', 'Totalbinloc else loc is',vloc);
		if(vloc!=null && vloc!= '')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', ['@NONE@',vloc]));
		// Case#20127666 end
	}	
	//case #20127333 Start

	var CycFiletrs = new Array();
	CycFiletrs.push(new nlobjSearchFilter('custrecord_cyclesku', null, 'anyof', '[@NONE]'));
	CycFiletrs.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', '20'));
	CycFiletrs.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', [varCCplanNo]));

	var CycColumns = new Array();
	CycColumns[0] = new nlobjSearchColumn('custrecord_cycact_beg_loc');
	var CYCsearchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, CycFiletrs, CycColumns);

	if(CYCsearchresults!=null && CYCsearchresults!='')
	{
		nlapiLogExecution('ERROR', 'CYCsearchresults.length filtered11',CYCsearchresults.length);
		var TotalListOfCYCLocs=new Array();
		for ( var m = 0; m < CYCsearchresults.length; m++) 
		{
			TotalListOfCYCLocs[m]=CYCsearchresults[m].getValue(CycColumns[0]);

		}
	}

	if (TotalListOfCYCLocs != null && TotalListOfCYCLocs != "")
	{
		nlapiLogExecution('ERROR', 'TTotalListOfCYCLocs',TotalListOfCYCLocs);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'noneof', TotalListOfCYCLocs));

	}
	return filters;
}

function validateRequestParams(request,form)
{
	var localVarArray = new Array();
	try
	{

		var varCCplanNo = request.getParameter('custpage_ccount');
		var varCCplanSite = request.getParameter('custpage_vsite');
		var vitemMultiSel = new Array();

		var newRecord = nlapiLoadRecord('customrecord_ebiznet_cylc_createplan', varCCplanNo);

		var vloc = newRecord.getFieldValue('custrecord_ebiz_cycl_binloc');
		var vlocText = newRecord.getFieldText('custrecord_ebiz_cycl_binloc');
		var vitemfamily = newRecord.getFieldValue('custrecord_ebiz_skufamily');
		var vitemgroup = newRecord.getFieldValue('custrecord_ebiz_skugroup');
		var vitem = newRecord.getFieldText('custrecord_ebiz_sku');
		var vitemid = newRecord.getFieldValue('custrecord_ebiz_sku');
		var vpackcode = newRecord.getFieldText('custrecord_ebiz_cycl_packcode');
		var vlotbatch = newRecord.getFieldValue('custrecord_ebiz_batch');
		var vaisle = newRecord.getFieldValue('custrecord_ebiz_aisle');
		var vlevel = newRecord.getFieldValue('custrecord_ebiz_level');
		var vbinlocgroup = newRecord.getFieldValue('custrecord_ebiz_locationgroup');
		var vlp = newRecord.getFieldText('custrecord_cycl_lp');
		var vlpid = newRecord.getFieldValue('custrecord_cycl_lp');
		var vcompany = newRecord.getFieldValue('custrecord_cycle_count_company');
		var vemptybinloc = newRecord.getFieldValue('custrecord_ebiz_include_emploc');
		var vSiteLoc = newRecord.getFieldValue('custrecord_ebiz_cyclecntplan_location');
		var vABCVelocity = newRecord.getFieldValue('custrecord_ebiz_abcvelocity');
		var vpalletPosition = newRecord.getFieldValue('custrecord_ebiz_cycl_palletposition');
		var vEmptyBinLocation = newRecord.getFieldValue('custrecord_ebiz_include_emploc');
		vitemMultiSel = newRecord.getFieldValue('custrecord_ebiz_sku_multisel');

		nlapiLogExecution('ERROR', 'vloc', vloc);
		nlapiLogExecution('ERROR', 'vlocText', vlocText);
		nlapiLogExecution('ERROR', 'vitemfamily', vitemfamily);
		nlapiLogExecution('ERROR', 'vitemgroup', vitemgroup);
		nlapiLogExecution('ERROR', 'vitemid', vitemid);
		nlapiLogExecution('ERROR', 'vpackcode', vpackcode);
		nlapiLogExecution('ERROR', 'vlotbatch', vlotbatch);
		nlapiLogExecution('ERROR', 'vaisle', vaisle);
		nlapiLogExecution('ERROR', 'vlevel', vlevel);
		nlapiLogExecution('ERROR', 'vbinlocgroup', vbinlocgroup);
		nlapiLogExecution('ERROR', 'vlp', vlp);
		nlapiLogExecution('ERROR', 'vcompany', vcompany);
		nlapiLogExecution('ERROR', 'vemptybinloc', vemptybinloc);
		nlapiLogExecution('ERROR', 'vSiteLoc', vSiteLoc);
		nlapiLogExecution('ERROR', 'vABCVelocity', vABCVelocity);
		nlapiLogExecution('ERROR', 'vpalletPosition', vpalletPosition);
		nlapiLogExecution('ERROR', 'vEmptyBinLocation', vEmptyBinLocation);

		var vitemMultiSelTemp = new Array();
		if(vitemMultiSel != null && vitemMultiSel != '')
		{			
			vitemMultiSelTemp = vitemMultiSel.split('');
			nlapiLogExecution('ERROR', 'vitemMultiSelTemp', vitemMultiSelTemp.length);
		}

		nlapiLogExecution('ERROR', 'vitemMultiSelTemp', vitemMultiSelTemp);

		var currentRow = [vloc,vitemfamily,vitemgroup,vitemid,vpackcode,vlotbatch,vlevel,vbinlocgroup,vlp,vcompany,vSiteLoc,vABCVelocity,vaisle,vpalletPosition,vEmptyBinLocation,vitemMultiSelTemp,varCCplanNo];


		localVarArray.push(currentRow);

	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in validaterequestparams',exp);

	}
	return localVarArray;
}


/*
 * Set line item level values based on search results
 */
function setLineItemValuesFromSearch(form, searchResults,i,varCCplanNo,minvalue,lengthofOrderlist)
{
	var opentaskloc, opentaskloctext;
	var whloc='';
	nlapiLogExecution('ERROR','minvalue',minvalue+'/'+lengthofOrderlist);

	i=i+1;
	if(minvalue>=lengthofOrderlist)
	{
		nlapiLogExecution('ERROR','into empty bin');
		opentaskloc = searchResults.getId();
		opentaskloctext = searchResults.getValue('name');
		whloc=searchResults.getText('custrecord_ebizsitelocf');
		nlapiLogExecution('ERROR','whloc',whloc);

	}
	else
	{
		opentaskloc = searchResults.getValue('custrecord_ebiz_inv_binloc',null,'group');
		opentaskloctext = searchResults.getText('custrecord_ebiz_inv_binloc',null,'group');
		whloc=searchResults.getText('custrecord_ebiz_inv_loc',null,'group');

	}
	form.getSubList('custpage_cyclecountgenerate_items').setLineItemValue('custpage_cyclecountlocation', i, opentaskloctext);
	form.getSubList('custpage_cyclecountgenerate_items').setLineItemValue('custpage_cyclecountlocationvalue', i, opentaskloc);
	form.getSubList('custpage_cyclecountgenerate_items').setLineItemValue('custpage_cyclecountinternalid', i, varCCplanNo);
	form.getSubList('custpage_cyclecountgenerate_items').setLineItemValue('custpage_ebizsiteid', i, whloc);
}


var arrayLocIds = new Array();
function GetTotalBinLoc(vaisle,vpalletposition,vbinlocgroup,vlevel,max,vloc)
{
	nlapiLogExecution('ERROR','vaisle',vaisle);
	nlapiLogExecution('ERROR','vpalletposition',vpalletposition);
	nlapiLogExecution('ERROR','vbinlocgroup',vbinlocgroup);
	nlapiLogExecution('ERROR','vlevel',vlevel);
	nlapiLogExecution('DEBUG','vloc',vloc);

	var filter=new Array();
	if(vaisle!=null&&vaisle!="")
		filter.push(new nlobjSearchFilter('custrecord_aisle_row', null, 'is', vaisle));
	if(vpalletposition!=null&&vpalletposition!="")
		filter.push(new nlobjSearchFilter('custrecord_palletposition', null, 'is', vpalletposition));
	if(vlevel!=null&&vlevel!="")
		filter.push(new nlobjSearchFilter('custrecord_level', null, 'is', vlevel));
	if(vbinlocgroup!=null&&vbinlocgroup!="")
		filter.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [vbinlocgroup]));
	if(vloc!=null && vloc!="")
		filter.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', vloc));
	if(max!=-1)
		filter.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(max)));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('id');	 
	columns[0].setSort(true);

	var locsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filter, columns);
	if(locsearchresults!=null&&locsearchresults!="")
	{
		for (var a = 0; locsearchresults != null && a < locsearchresults.length; a++) 
		{
			//arrayLocIds.push(locsearchresults[a].getId());
			arrayLocIds.push(parseFloat(locsearchresults[a].getId()));
		}

		if(locsearchresults!=null && locsearchresults.length>=1000)
		{	 
			nlapiLogExecution('ERROR', 'locsearchresults length',locsearchresults.length);
			var maxno=locsearchresults[locsearchresults.length-1].getId();
			nlapiLogExecution('ERROR', 'maxno',maxno);
			GetTotalBinLoc(vaisle,vpalletposition,vbinlocgroup,vlevel,maxno,vloc);
		}
	}
	nlapiLogExecution('ERROR','arrayLocIds',arrayLocIds);
	return arrayLocIds;
}

var searchEmptyBinResultArray=new Array();
function GetEmptyBinList(request,maxno,form)
{
	try
	{
		var localVarArray;

		localVarArray = validateRequestParams(request,form);
		nlapiLogExecution('Error', 'GetEmptyBinList localVarArrayinelse', localVarArray);

		// Get all the search filters for wave generation
		var filters = new Array();   
		filters = specifyEmptybinFilters(localVarArray,maxno);
		nlapiLogExecution('Error', 'GetEmptyBinList afterspecifyfilters', filters);

		// Adding search columns
		var columns = new Array();
		columns=addColumnsForEmptyBinSearch();
		nlapiLogExecution('Error', 'GetEmptyBinList afterspecifycolumns', columns);

		var orderList = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

		if( orderList!=null && orderList.length>=1000)
		{
			nlapiLogExecution('Error', 'GetEmptyBinList orderList.length', orderList.length);
			searchEmptyBinResultArray.push(orderList); 
			var maxno=orderList[orderList.length-1].getId();
			nlapiLogExecution('Error', 'GetEmptyBinList maxno', maxno);
			GetEmptyBinList(request,maxno,form);	

		}
		else if(orderList!=null&&orderList!="")
		{
			searchEmptyBinResultArray.push(orderList); 
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','Exception in GetEmptyBinList',e);
	}
	nlapiLogExecution('ERROR','searchEmptyBinResultArray',searchEmptyBinResultArray);
	return searchEmptyBinResultArray;
}

function addColumnsForEmptyBinSearch()
{	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');	
	columns[1] = new nlobjSearchColumn('custrecord_ebizsitelocf');
	columns[0].setSort(true);

	return columns;
}

function specifyEmptybinFilters(localVarArray,maxno)
{
	var vloc = localVarArray[0][0];
	var vitemfamily = localVarArray[0][1];
	var vitemgroup = localVarArray[0][2];
	var vitemid = localVarArray[0][3];
	var vpackcode = localVarArray[0][4];
	var vlotbatch = localVarArray[0][5];
	var vlevel = localVarArray[0][6];
	var vbinlocgroup = localVarArray[0][7];
	var vlp = localVarArray[0][8];
	var vcompany = localVarArray[0][9];
	var vSiteLoc = localVarArray[0][10];
	var vABCVelocity = localVarArray[0][11];
	var vaisle = localVarArray[0][12];
	var vpalletPosition = localVarArray[0][13];
	var vemptybinloc = localVarArray[0][14];
	var vitemMultiSel = localVarArray[0][15];
	var varCCplanNo = localVarArray[0][16];

	var filter=new Array(); 
	tempInvtResultsArray = new Array();

	nlapiLogExecution('ERROR', 'EmptybinFilters vaisle',vaisle);
	nlapiLogExecution('ERROR', 'EmptybinFilters vpalletPosition',vpalletPosition);
	nlapiLogExecution('ERROR', 'EmptybinFilters vlevel',vlevel);
	nlapiLogExecution('ERROR', 'EmptybinFilters vbinlocgroup',vbinlocgroup);
	nlapiLogExecution('ERROR', 'EmptybinFilters vSiteLoc',vSiteLoc);
	nlapiLogExecution('ERROR', 'EmptybinFilters maxno',maxno);


	//filter.push(new nlobjSearchFilter('custrecord_aisle_row', 'custrecord_ebiz_inv_binloc', 'is', '13'));
	if(vaisle!=null&&vaisle!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_aisle',null, 'is', vaisle));
	if(vpalletPosition!=null&&vpalletPosition!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_palletposition',null, 'is', vpalletPosition));
	if(vlevel!=null&&vlevel!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_level',null, 'is', vlevel));
	if(vbinlocgroup!=null&&vbinlocgroup!="")
		filter.push(new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', [vbinlocgroup]));
	//filter.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', [vbinlocgroup]));
	if (vSiteLoc != null && vSiteLoc != "") {
		nlapiLogExecution('ERROR', 'Site filtered',vSiteLoc);
		//filter.push(new nlobjSearchFilter('custrecord_ebizsitelocf','custrecord_ebiz_inv_binloc', 'anyof', [vSiteLoc]));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'anyof', [vSiteLoc]));
	}	
	filter.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null , 'greaterthan', 0));
	filter.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc', 'is', 'F'));
	filter.push(new nlobjSearchFilter('custrecord_ebizlocationtype', 'custrecord_ebiz_inv_binloc', 'anyof', ['6','7']));
	if(maxno!=-1)
		filter.push(new nlobjSearchFilter('id', 'custrecord_ebiz_inv_binloc', 'lessthan', parseInt(maxno)));

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	column[1] = new nlobjSearchColumn('internalid');
	column[1].setSort(false);

	var invtlist= nlapiSearchRecord('customrecord_ebiznet_createinv', null, filter, column);
	if(invtlist != null && invtlist != '')
	{	
		for(var j=0;j<invtlist.length;j++)
		{
			if(tempInvtResultsArray.indexOf(invtlist[j].getValue('custrecord_ebiz_inv_binloc'))==-1)
				tempInvtResultsArray.push(invtlist[j].getValue('custrecord_ebiz_inv_binloc'));
		}
	}

	var OpenEmptylocationsfromCyclecountExecutionPlan=getOpenEmptylocationsfromCyclecountExecutionPlan(0);
	nlapiLogExecution('ERROR', 'OpenEmptylocationsfromCyclecountExecutionPlan',OpenEmptylocationsfromCyclecountExecutionPlan);
	if(OpenEmptylocationsfromCyclecountExecutionPlan!=null && OpenEmptylocationsfromCyclecountExecutionPlan!='')
	{
		for(var j=0;j<OpenEmptylocationsfromCyclecountExecutionPlan.length;j++)
		{
			var tempOpenEmptylocationsfromCyclecountExecutionPlan=OpenEmptylocationsfromCyclecountExecutionPlan[j];
			for(var j1=0;j1<tempOpenEmptylocationsfromCyclecountExecutionPlan.length;j1++)
			{
				//qnlapiLogExecution('ERROR', 'tempOpenEmptylocationsfromCyclecountExecutionPlan[j1].getValue(custrecord_cycact_beg_loc)',tempOpenEmptylocationsfromCyclecountExecutionPlan[j1].getValue('custrecord_cycact_beg_loc'));
				if(tempInvtResultsArray.indexOf(tempOpenEmptylocationsfromCyclecountExecutionPlan[j1].getValue('custrecord_cycact_beg_loc'))==-1)
					tempInvtResultsArray.push(tempOpenEmptylocationsfromCyclecountExecutionPlan[j1].getValue('custrecord_cycact_beg_loc'));
			}
		}
	}




	var filters=new Array();
	if(vaisle!=null&&vaisle!="")
		filters.push(new nlobjSearchFilter('custrecord_aisle_row', null, 'is', vaisle));
	if(vpalletPosition!=null&&vpalletPosition!="")
		filters.push(new nlobjSearchFilter('custrecord_palletposition', null, 'is', vpalletPosition));
	if(vlevel!=null&&vlevel!="")
		filters.push(new nlobjSearchFilter('custrecord_level', null, 'is', vlevel));
	if(vbinlocgroup!=null&&vbinlocgroup!="")
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [vbinlocgroup]));
	if (vSiteLoc != null && vSiteLoc != "") {
		nlapiLogExecution('ERROR', 'Site filtered',vSiteLoc);
		filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null, 'anyof', [vSiteLoc]));
	}	
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));
	if(maxno!=-1)
		filters.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));

	if(tempInvtResultsArray != null && tempInvtResultsArray != '')
		filters.push(new nlobjSearchFilter('internalid',null,'noneof',tempInvtResultsArray));

	//var orderList = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, null);
	//var k=0;


	return filters;
}



var OpenEmptylocationsfromCyclecountExecutionPlan=new Array();
function getOpenEmptylocationsfromCyclecountExecutionPlan(maxno)
{
	var cycexeFilters= new Array();
	cycexeFilters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', ['20','31']));
	cycexeFilters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));

	var cycexeColumn = new Array();
	cycexeColumn[0] = new nlobjSearchColumn('custrecord_cycact_beg_loc');
	cycexeColumn[1] = new nlobjSearchColumn('internalid');
	cycexeColumn[1].setSort(false);
	var CycleCountExecutionPlanOrderList = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, cycexeFilters, cycexeColumn );

	if(CycleCountExecutionPlanOrderList !=null && CycleCountExecutionPlanOrderList!='' && CycleCountExecutionPlanOrderList.length>=1000)
	{
		OpenEmptylocationsfromCyclecountExecutionPlan.push(CycleCountExecutionPlanOrderList);
		var maxno=CycleCountExecutionPlanOrderList[CycleCountExecutionPlanOrderList.length-1].getId();
		nlapiLogExecution('ERROR', 'maxno',maxno);
		getOpenEmptylocationsfromCyclecountExecutionPlan(maxno);
	}
	else
	{
		if(CycleCountExecutionPlanOrderList!=null && CycleCountExecutionPlanOrderList!='' && CycleCountExecutionPlanOrderList.length>0)
		{
			OpenEmptylocationsfromCyclecountExecutionPlan.push(CycleCountExecutionPlanOrderList);
		}
	}

	return OpenEmptylocationsfromCyclecountExecutionPlan;
}


function CyclecountSch(type)
{
	try
	{
		var context = nlapiGetContext(); 
		var vcuruserId = context.getUser();
		var varloc=context.getSetting('SCRIPT', 'custscript_cyccvarloc')
		var vloc=context.getSetting('SCRIPT', 'custscript_cyccvloc');
		var vitemfamily=context.getSetting('SCRIPT', 'custscript_cyccitemfamily');
		var vitemgroup=context.getSetting('SCRIPT', 'custscript_cyccitemgroup');
		var vitemid=context.getSetting('SCRIPT', 'custscript_cyccitemid');
		var vitemMultiSelTemp=context.getSetting('SCRIPT', 'custscript_cyccmultiitem');
		var vpackcode=context.getSetting('SCRIPT', 'custscript_cyccpackcode');
		var vlotbatch=context.getSetting('SCRIPT', 'custscript_cycclotbatch');
		var vlevel=context.getSetting('SCRIPT', 'custscript_cyccvlevel');
		var vbinlocgroup=context.getSetting('SCRIPT', 'custscript_cyccbinlocgroup');
		var vlp=context.getSetting('SCRIPT', 'custscript_cycclp');
		var vcompany=context.getSetting('SCRIPT', 'custscript_cycccompany');
		var vSiteLoc=context.getSetting('SCRIPT', 'custscript_cyccSiteLoc');
		var vABCVelocity=context.getSetting('SCRIPT', 'custscript_cyccABCVelocity');
		var cycnplanid=context.getSetting('SCRIPT', 'custscript_cyccplan');



		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [varloc]));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_invholdflg', null, 'is', 'F'));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

		if (vloc != null && vloc != "") {
			nlapiLogExecution('ERROR', 'LOCATION filtered',vloc);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [vloc]));
		}
		if (vitemfamily != null && vitemfamily != "") {
			nlapiLogExecution('ERROR', 'vitemfamily filtered', vitemfamily);
			filters.push(new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_inv_sku', 'is', vitemfamily));
		}
		if (vitemgroup != null && vitemgroup != "") {
			nlapiLogExecution('ERROR', 'vitemgroup filtered', vitemgroup);
			filters.push(new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_inv_sku', 'is', vitemgroup));
		}
		if (vitemid != null && vitemid != "") {
			nlapiLogExecution('ERROR', 'ITEM filtered',vitemid);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', [vitemid]));
		}
		if (vitemid == null || vitemid == "") {
			if(vitemMultiSelTemp != null && vitemMultiSelTemp != '')
			{
				nlapiLogExecution('DEBUG', 'vitemMultiSel filtered',vitemMultiSelTemp);
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', vitemMultiSelTemp));
			}
		}

		if (vpackcode != null && vpackcode != "") {
			nlapiLogExecution('ERROR', 'PACK CODE filtered');
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', ['@NONE@',vpackcode]));
		}
		if (vlotbatch != null && vlotbatch != "") {
			nlapiLogExecution('ERROR', 'LOT filtered');
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', ['@NONE@',vlotbatch]));
		}
		if (vlevel != null && vlevel != "") {
			nlapiLogExecution('ERROR', 'LEVEL filtered');
			filters.push(new nlobjSearchFilter('custrecord_ebiz_loclvl', null, 'is', vlevel));
		}
		if (vbinlocgroup != null && vbinlocgroup != "") {
			nlapiLogExecution('ERROR', 'Bin Location Group filtered');
			//filters.push(new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', ['@NONE@',vbinlocgroupid]));
			//filters.push(new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', [vbinlocgroupid]));
			filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', [vbinlocgroup]));
		}
		if (vlp != null && vlp != "") {
			nlapiLogExecution('ERROR', 'LP filtered', vlp);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', vlp));
		}
		if (vcompany != null && vcompany != "") {
			nlapiLogExecution('ERROR', 'Company filtered',vcompany);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', ['@NONE@',vcompany]));
		}

		if (vSiteLoc != null && vSiteLoc != "") {
			nlapiLogExecution('ERROR', 'Site filtered',vSiteLoc);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [vSiteLoc]));
		}

		filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag',null, 'anyof', ['19']));

		if (vABCVelocity != null && vABCVelocity != "") 
		{
			nlapiLogExecution('ERROR', 'vABCVelocity filtered',vABCVelocity);

			var filtersvelocity = new Array();
			filtersvelocity.push(new nlobjSearchFilter('custitem_ebizabcvelitem', null, 'anyof', vABCVelocity));

			var colsvelocity = new Array();
			colsvelocity[0]=new nlobjSearchColumn('itemid');


			var searchresults = nlapiSearchRecord('item', null, filtersvelocity, colsvelocity);

			if (searchresults != null && searchresults != "")
			{
				nlapiLogExecution('ERROR', 'searchresults.length filtered11',searchresults.length);
				var TotalListOfSKUs=new Array();
				for ( var x = 0; x < searchresults.length; x++) 
				{
					TotalListOfSKUs[x]=searchresults[x].getId();

				}
			}

			if (TotalListOfSKUs != null && TotalListOfSKUs != "")
			{
				nlapiLogExecution('ERROR', 'TotalListOfSKUs filtered11',TotalListOfSKUs);
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof', TotalListOfSKUs));

			}
		}


		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_qoh');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
		columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
		columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_adjusttype');
		columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_account_no');
		columns[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
		columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_note1');
		columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_note2');
		columns[13] = new nlobjSearchColumn('custrecord_ebiz_itemdesc');
		columns[0].setSort(true);
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

		if(searchresults!=null && searchresults!='')
		{
			updateScheduleScriptStatus('Cyclecount',vcuruserId,'Submitted',cycnplanid,null);
			for (var i = 0; searchresults != null && i < searchresults.length; i++) {
				nlapiLogExecution('ERROR', 'Into searchresults');
				var searchresult = searchresults[i];
				vinvloc = searchresult.getValue('custrecord_ebiz_inv_binloc');
				recid = searchresult.getId();
				vqty = searchresult.getValue('custrecord_ebiz_qoh');
				vsku = searchresult.getValue('custrecord_ebiz_inv_sku');
				vskuText = searchresult.getText('custrecord_ebiz_inv_sku');
				lpID = searchresult.getValue('custrecord_ebiz_inv_lp');
				batchNo= searchresult.getText('custrecord_ebiz_inv_lot');
				vItemStatus = searchresult.getValue('custrecord_ebiz_inv_sku_status');
				vPackcode = searchresult.getValue('custrecord_ebiz_inv_packcode');
				Site = searchresult.getText('custrecord_ebiz_inv_loc');
				SiteId = searchresult.getValue('custrecord_ebiz_inv_loc');
				//nlapiLogExecution('ERROR', 'SS--->vinvloc', vinvloc);

				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', vsku);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lpID);
				filters[2] = new nlobjSearchFilter('custrecord_serial_location', null, 'is', SiteId);
				filters[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             
				var expserialno = "";
				if(searchResults != null && searchResults != "")
				{ 
					nlapiLogExecution('ERROR', 'searchResults', searchResults.length);
					nlapiLogExecution('ERROR', 'vqty', vqty);
					for (var a = 0; a < searchResults.length ; a++) 
					{   


						if(a < parseFloat(vqty)){
							//nlapiLogExecution('ERROR', 'a', a);
							if(searchResults[a].getValue('custrecord_serialnumber') != null && searchResults[a].getValue('custrecord_serialnumber') != "")
							{
								if(expserialno == "")
								{
									expserialno = searchResults[a].getValue('custrecord_serialnumber');
								}
								else
								{
									expserialno = expserialno + "," + searchResults[a].getValue('custrecord_serialnumber');
								}									
							}
						}
						else
						{
							nlapiLogExecution('ERROR', 'from break', a);
							break;
						}
					}
				}
				if (vinvloc != "") {
					var vIntValue="N";
					nlapiLogExecution('ERROR', 'recid for update INside ', recid);


					var scount=1;

					LABL1: for(var i1=0;i1<scount;i1++)
					{	

						nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i1);
						try
						{
							var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', recid);
							transaction.setFieldValue('custrecord_ebiz_invholdflg', 'T');
							transaction.setFieldValue('custrecord_ebiz_cycl_count_hldflag', 'T');						
							transaction.setFieldValue('custrecord_ebiz_callinv', vIntValue);
							nlapiSubmitRecord(transaction, false, true);
							nlapiLogExecution('ERROR', 'Cyclecount generated for inv ref ',recid);										


						}
						catch(ex)
						{
							var exCode='CUSTOM_RECORD_COLLISION'; 
							var wmsE='Inventory record being updated by another user. Please try again...';
							if (ex instanceof nlobjError) 
							{	
								wmsE=ex.getCode() + '\n' + ex.getDetails();
								exCode=ex.getCode();
							}
							else
							{
								wmsE=ex.toString();
								exCode=ex.toString();
							} 									
							nlapiLogExecution('ERROR', 'Exception in Cycle count Generate release : ', wmsE); 
							if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
							{ 
								scount=scount+1;
								continue LABL1;
							}
							else break LABL1;
						}
					}
				}

				nlapiLogExecution('ERROR', 'Into CYCC task creation ');
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
				customrecord.setFieldValue('custrecord_tasktype', 7);
				customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
				customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
				customrecord.setFieldValue('custrecord_expe_qty', vqty);
				customrecord.setFieldValue('custrecord_lpno', lpID);
				customrecord.setFieldValue('custrecord_sku', vsku);
				customrecord.setFieldValue('custrecord_ebiz_sku_no', vsku);
				customrecord.setFieldValue('custrecord_actbeginloc', vinvloc);
				customrecord.setFieldValue('custrecord_ebiz_cntrl_no', cycnplanid);					
				customrecord.setFieldValue('name', (cycnplanid + "-" + lpID + "-" + i));

				var opentaskid = nlapiSubmitRecord(customrecord);
				nlapiLogExecution('ERROR', 'Out of CYCC task creation', recid);
				nlapiLogExecution('ERROR', 'Into Cycle Count Execution creation ');
				var cyclecntrec = nlapiCreateRecord('customrecord_ebiznet_cyclecountexe');
				cyclecntrec.setFieldValue('custrecord_cycle_count_plan', cycnplanid);
				cyclecntrec.setFieldValue('name', (cycnplanid + "-" + lpID + "-" + i));
				cyclecntrec.setFieldValue('custrecord_cyclerec_date', DateStamp());					
				cyclecntrec.setFieldValue('custrecord_cycrec_time', TimeStamp());
				cyclecntrec.setFieldValue('custrecord_cyclesku', vsku);
				cyclecntrec.setFieldValue('custrecord_cyclecount_exp_ebiz_sku_no', vsku);
				cyclecntrec.setFieldValue('custrecord_cycleexp_qty', vqty);
				cyclecntrec.setFieldValue('custrecord_cyclelpno', lpID);
				cyclecntrec.setFieldValue('custrecord_cyclestatus_flag', 20);//status flag is 'R'
				cyclecntrec.setFieldValue('custrecord_cycact_beg_loc', vinvloc);
				cyclecntrec.setFieldValue('custrecord_opentaskid', opentaskid);
				cyclecntrec.setFieldValue('custrecord_invtid', recid);
				cyclecntrec.setFieldValue('custrecord_expcycleabatch_no', batchNo);
				cyclecntrec.setFieldValue('custrecord_expcyclesku_status', vItemStatus);
				cyclecntrec.setFieldValue('custrecord_expcyclepc', vPackcode);
				cyclecntrec.setFieldValue('custrecord_cyclesite_id', vSiteLoc);
				cyclecntrec.setFieldValue('custrecord_cycle_expserialno', expserialno);
				nlapiLogExecution('ERROR', 'expserialno ',expserialno);
				var cyclecntRecId = nlapiSubmitRecord(cyclecntrec);
				nlapiLogExecution('ERROR', 'Out of Cycle Count Execution creation ');
			}
			updateScheduleScriptStatus('Cyclecount',vcuruserId,'Completed',cycnplanid,null);
		}


	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception in Quickship Sch",exp);
	}
}

