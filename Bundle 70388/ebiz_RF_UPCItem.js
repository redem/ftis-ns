function UPCItem(request, response){
	if (request.getMethod() == 'GET') {
		var getPlanNo = request.getParameter('custparam_planno');
		var getsku = request.getParameter('custparam_sku');
		var gettasktype = request.getParameter('custparam_tasktype');
		var getlocation = request.getParameter('custparam_location');
		var getbeginlocationid = request.getParameter('custparam_actbeginlocationid');
		var getbeginlocationname = request.getParameter('custparam_actbeginlocationname');
		var getskuno = request.getParameter('custparam_skuno');
		nlapiLogExecution('ERROR', 'getskuno', getskuno);
		var Itemdescription='';
		var Itemdescription1='';
		/*var Itype = nlapiLookupField('item', getskuno, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getskuno);
//		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
//		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var Itemdescription='';
		var Itemdescription1='';
		nlapiLogExecution('ERROR', 'getsku', getsku);	
	
		var getItemName = ItemRec.getFieldValue('itemid').substring(0, 20);
		//var getItemName = ItemRec.getFieldValue('externalid');

		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('description');
		}
		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('salesdescription').substring(0, 40);
		}	
		//Itemdescription = Itemdescription.substring(0, 20);
		Itemdescription1=Itemdescription.substring(0, 40);
		

		nlapiLogExecution('ERROR', 'getItem', getItemName);
		nlapiLogExecution('ERROR', 'getItemdescription', Itemdescription);*/
		

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enteritem').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";		
		html = html + "				<td align = 'left'>ENTER/SCAN ITEM ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "				<input type='hidden' name='hdnplanno' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnItemdesc' value=" + Itemdescription1 + ">";
		html = html + "				<input type='hidden' name='hdntasktype' value=" + gettasktype + ">";
		html = html + "				<input type='hidden' name='hdnlocation' value=" + getlocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getbeginlocationid + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationname' value=" + getbeginlocationname + ">";	
		html = html + "				<input type='hidden' name='hdnItemId' value=" + getskuno + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN UPC CODE ";
		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterupc' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var getEnteredItem = request.getParameter('enteritem');
		nlapiLogExecution('ERROR', 'Entered Item', getEnteredItem);
		
		var getEnteredUPC = request.getParameter('enterupc');
		nlapiLogExecution('ERROR', 'Entered UPC', getEnteredUPC);
		
		var getplanno = request.getParameter('hdnplanno');
		var getItemName = request.getParameter('hdnItemName');
		var getItemdesc = request.getParameter('hdnItemdesc');
		var getTasktype = request.getParameter('hdntasktype');
		var getlocation = request.getParameter('hdnlocation');
		var getBeginLocationid = request.getParameter('hdnBeginLocation');
		var getBeginLocationame = request.getParameter('hdnBeginLocationname');
		var ItemId=request.getParameter('hdnItemId');
		var optedEvent = request.getParameter('cmdPrevious');
		
		var UPCarray = new Array();
		UPCarray["custparam_planno"] = getplanno;
		UPCarray["custparam_sku"] = getEnteredItem;
		UPCarray["custparam_tasktype"] = getTasktype;
		UPCarray["custparam_location"] = getlocation;
		UPCarray["custparam_actbeginlocationid"] = getBeginLocationid;
		UPCarray["custparam_actbeginlocationname"] = getBeginLocationame;
		UPCarray["custparam_error"] = 'INVALID ITEM';
		UPCarray["custparam_screenno"] = 'UPC3';		
		
		
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_upccode_location', 'customdeploy_rf_upccode_location_di', false, UPCarray);
		}
		else if((getEnteredItem!=null && getEnteredItem!='')&&(getEnteredUPC!=null && getEnteredUPC!=''))
		{
			var result=validate(getEnteredItem);
			var actItemid=result[1];
			nlapiLogExecution('ERROR', 'actItemid',actItemid);
			
			if(actItemid!=null&&actItemid!="")
			{
				var result=IsValidBinItem(actItemid,getBeginLocationid);
				if(result==true)
				{
					UPCarray["custparam_skuno"] = actItemid;
					var searchresult=validate(getEnteredUPC);
					var UPCItem=searchresult[1];
					nlapiLogExecution('ERROR', 'UPCItem',UPCItem);
					
					if(actItemid==UPCItem || (UPCItem=='' || UPCItem==null))
					{
						UPCarray["custparam_newupccode"] = getEnteredUPC;
						response.sendRedirect('SUITELET', 'customscript_rf_upccode_confirmation', 'customdeploy_rf_upccode_confirmation_di', false, UPCarray);
					}
					else
					{
						UPCarray["custparam_error"] = 'ALREADY ASSIGNED UPC TO :' +searchresult[0];
						//UPCarray["custparam_newupccode"] = getEnteredItem;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UPCarray);
						nlapiLogExecution('ERROR', 'Invalid UPC ');
					}
				}
				else
				{
					UPCarray["custparam_error"] = 'INVALID ITEM FOR BIN LOC:  '+getBeginLocationame;
					//UPCarray["custparam_newupccode"] = getEnteredUPC;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UPCarray);
					nlapiLogExecution('ERROR', 'Invalid bin loc item');
				}
			}
			else
			{
				//UPCarray["custparam_newupccode"] = getEnteredUPC;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UPCarray);
				nlapiLogExecution('ERROR', 'Invalid item');
			}
				 
		}
		else
		{
			if(getEnteredItem==null || getEnteredItem=='')
				UPCarray["custparam_error"] = 'INVALID ITEM';
			else if(getEnteredUPC==null || getEnteredUPC=='')
				UPCarray["custparam_error"] = 'INVALID UPC';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UPCarray);
			nlapiLogExecution('ERROR', 'Item/UPC not found');
		}
	}
}



function validate(itemNo)
{
	nlapiLogExecution('Error', 'Into validateSKU',itemNo);

	var TotalArray=new Array();
	var actItem="";
	var actItemId="";

	var invitemfilters = new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal',null, 'is',itemNo));
	// Changed On 30/4/12 by Suman
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	var invLocCol = new Array();
	invLocCol[0] = new nlobjSearchColumn('itemid');
	//invLocCol[0] = new nlobjSearchColumn('externalid');
	//invLocCol[0] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);
	if(invitemRes!=null && invitemRes!='')
	{
		actItem=invitemRes[0].getValue('itemid');
		actItemId=invitemRes[0].getId();
	}
	else
	{
		nlapiLogExecution('ERROR', 'inSide UPCCODE',itemNo);

		var invLocfilters = new Array();
		invLocfilters[0] = new nlobjSearchFilter('upccode', null, 'is', itemNo);

		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);

		if(invtRes!=null && invtRes!='')
		{
			actItem=invtRes[0].getValue('itemid');
			actItemId=invtRes[0].getId();
		}
				
	}

	nlapiLogExecution('Error', 'actItem',actItem);
	TotalArray.push(actItem);
	TotalArray.push(actItemId);
	nlapiLogExecution('Error', 'Out of validateSKU',TotalArray);
	return TotalArray;
}


function IsValidBinItem(actItemid,getBeginLocationid)
{
	try
	{
		nlapiLogExecution('ERROR','Into IsValidBinItem function');
		nlapiLogExecution('ERROR','actItemid',actItemid);
		nlapiLogExecution('ERROR','getBeginLocationid',getBeginLocationid);
		var flag=false;
		var filter=new Array();
		if(getBeginLocationid!=null&&getBeginLocationid!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null,'anyof',getBeginLocationid));
		if(actItemid!=null&&actItemid!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null,'anyof',actItemid));
		
		var searchRec=nlapiSearchRecord('customrecord_ebiznet_createinv',null,filter,null);
		if(searchRec!=null&&searchRec!="")
			flag=true;
		nlapiLogExecution('ERROR','End of IsValidBinITem',flag);
		return flag;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','EXception in IsValidBinItem',exp);
	}
}