/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_serialnumbersmismatch_detail_SL.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2013/05/10 08:57:06 $
 *     	   $Author: rmukkera $
 *     	   $Name: t_NSWMS_2013_1_3_23 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_serialnumbersmismatch_detail_SL.js,v $
 * Revision 1.1.2.1  2013/05/10 08:57:06  rmukkera
 * New File
 *
 *****************************************************************************/
function ebiznet_SerialNumbers_SL(request, response){
    if (request.getMethod() == 'GET') {        
        var form = nlapiCreateForm('Serial numbers mismatch Ebiz Vs NS');
        
            var item=request.getParameter('custparam_item');
            var  soid=request.getParameter('custparam_soid');
            var location=request.getParameter('custparam_location');
            var line=request.getParameter('custparam_line');
            var ebizserialno = form.addField('custpage_ebizserialno', 'longtext', 'ebizSerialNo').setLayoutType('outsidebelow', 'startrow');
            var Nsserialno = form.addField('custpage_nsserialno', 'longtext', 'Ns SerialNo').setLayoutType('outsidebelow', 'startrow');
            var Diffserialno = form.addField('custpage_diffserialno', 'longtext', 'DiffSerailno').setLayoutType('outsidebelow', 'startrow');
		nlapiLogExecution('ERROR', 'item', item);
		nlapiLogExecution('ERROR', 'location', location);
		/*var filters = new Array();
    	filters[0] = new nlobjSearchFilter('internalid',null,'is', item);
    	filters[1] = new nlobjSearchFilter('serialnumberlocation',null,'anyof', location);

    	var columns = new Array();
    	columns[0] = new nlobjSearchColumn('serialnumber');
    	var searchresultsitem = nlapiSearchRecord('serializedinventoryitem', null, filters, columns);*/
		
		var searchresultsitem=getNSNumbers(item,location,-1);
    	nlapiLogExecution('ERROR', 'searchresults.length', searchresultsitem.length);
          var NSserialnumbers="";
          var ebizserialnumbers="";
          var diffserialnumbers='';
    	for (var j = 0; searchresultsitem != null && j < searchresultsitem.length; j++) {		
    		var searchresultitem = searchresultsitem[j];
    		//serial = searchresult.getValue('serialnumberlocation');
    		if(NSserialnumbers == "")
			{
				NSserialnumbers =searchresultsitem[j]; //searchresultitem.getValue('serialnumber');
			}
			else
			{
				NSserialnumbers = NSserialnumbers + " , " + searchresultsitem[j];//searchresultitem.getValue('serialnumber');
			}
    	}
    	 nlapiLogExecution('ERROR', 'NSserialnumbers', NSserialnumbers);
    	Nsserialno.setDefaultValue(NSserialnumbers);
		
		
		var searchResultArray = new Array();
		searchResultArray = getSerialnumbersforItem(line,item,location,-1,soid);
		if (searchResultArray !=null)
		{
			//ebizserialnumbers = getSerialNoCSV(searchResultArray);
			var orderListArray=new Array();		
			for(k=0;k<searchResultArray.length;k++)
			{
				var ordsearchresult = searchResultArray[k];

				if(ordsearchresult!=null)
				{
					for(var l=0;l<ordsearchresult.length;l++)
					{
						orderListArray[orderListArray.length]=ordsearchresult[l];				
					}
				}
			}
			
			for(var a = 0; a < orderListArray.length; a++){		

				if(ebizserialnumbers == "")
				{
					ebizserialnumbers = orderListArray[a].getValue('custrecord_serialnumber');
				}
				else
				{
					ebizserialnumbers = ebizserialnumbers + " , " + orderListArray[a].getValue('custrecord_serialnumber');
				}
				
				if(NSserialnumbers != "")
				{
					if(searchresultsitem.indexOf(orderListArray[a].getValue('custrecord_serialnumber')) == -1)
				    {
						if(diffserialnumbers == "")
						{
							diffserialnumbers = orderListArray[a].getValue('custrecord_serialnumber');
						}
						else
						{
							diffserialnumbers = diffserialnumbers + " , " + orderListArray[a].getValue('custrecord_serialnumber');
						}
				    }
				}
				else
					{
					if(NSserialnumbers == "")
				    	diffserialnumbers = ebizserialnumbers;
					}
			}
		}
		
		
		
		ebizserialno.setDefaultValue(ebizserialnumbers);
		Diffserialno.setDefaultValue(diffserialnumbers);
    			    
    
		
		
		
		
		
		var button = form.addSubmitButton('Display');
		response.writePage(form);        
    }
    else {
    	
    }
    
}

var tempNSArray=new Array();
function getNSNumbers(item,site,maxno)
{
	if(maxno==-1)
		{
		tempNSArray=new Array();	
		}
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('item',null,'is', item);
	filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', site);
        filters[2] = new nlobjSearchFilter('isonhand', null, 'is', 'T'); //new filter added by terry
        filters[3] = new nlobjSearchFilter('location', null, 'anyof', site); //new filter added by terry
	if(maxno!=-1)
	{
		filters[1] = new nlobjSearchFilter('internalidnumber',null,'greaterthan', maxno);
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('inventorynumber');
	columns[1] = new nlobjSearchColumn('internalid').setSort();
	var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);
	//nlapiLogExecution('DEBUG', 'searchresults.length', searchresults.length);
	if(searchresultsitem!=null && searchresultsitem!='')
	{
		if(searchresultsitem.length>=1000)

		{
			for (var j = 0; searchresultsitem != null && j < searchresultsitem.length; j++) {        
				var searchresultitem = searchresultsitem[j];


				tempNSArray.push(searchresultitem.getValue('inventorynumber'));
			}
			var maxno=searchresultsitem[searchresultsitem.length-1].getId();
			getNSNumbers(item,site,maxno);

		}
		else
		{
			for (var j = 0; searchresultsitem != null && j < searchresultsitem.length; j++) {        
				var searchresultitem = searchresultsitem[j];


				tempNSArray.push(searchresultitem.getValue('inventorynumber'));
			}
		}
	}
	return tempNSArray;
}


function GetSOInternalId(SOid)
{
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	salesorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', SOid)); 
	
	var SearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,null);	
	if(SearchResults==null)
	{
		nlapiLogExecution('ERROR', 'transferorder','transferorder');
		SearchResults=nlapiSearchRecord('transferorder',null,salesorderFilers,null);
	}
	
	if(SearchResults != null && SearchResults != "")
	{
		nlapiLogExecution('ERROR', 'SearchResults.length ',SearchResults.length);
		return SearchResults[0].getId();
	}
	else
		return null;
}

var searchResultArray=new Array();
var linearray = new Array();

function getSerialnumbersforItem(line,itemvalue,location,maxno,soid){

	if(linearray.indexOf(line) == -1)
		searchResultArray = [];
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itemvalue));
	if(location!=null && location!='' && location!='null')
	filters.push(new nlobjSearchFilter('custrecord_serial_location', null, 'anyof', location));
	if(soid!=null && soid!='' && soid!='null')
	filters.push(new nlobjSearchFilter('custrecord_serialebizsono', null, 'is', soid));
	if(line!=null && line!='' && line!='null')
	filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null, 'is', line));
	linearray.push(line);
	if(maxno!=-1)
	{
		nlapiLogExecution('ERROR', 'Into getOpentaskSearchResults maxno',maxno);
		filters.push( new nlobjSearchFilter('internalidnumber', null, 'greaterthan', maxno));
	}
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
	columns[1] = new nlobjSearchColumn('internalid');

	// LN, execute the search, passing null filter and return columns
	var searchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
	if( searchresults!=null && searchresults.length>=1000)
	{ 
		nlapiLogExecution('Error', 'searchresults.length', searchresults.length);
		searchResultArray.push(searchresults); 
		var maxno=searchresults[searchresults.length-1].getValue(columns[1]);
		nlapiLogExecution('Error', 'maxno', maxno);
		getSerialnumbersforItem(line,itemvalue,location,maxno,soid);	
	}
	else
	{
		//nlapiLogExecution('Error', 'searchresults.length', searchresults.length);
		searchResultArray.push(searchresults); 
	}
	return searchResultArray;
}

function getSerialNoCSV(arrayLP){
	var csv = "";
	nlapiLogExecution('ERROR', 'arrayLP.length',arrayLP.length);
	if(arrayLP.length == 1)
	{
		csv = arrayLP[0][0];
	}
	else
	{
		for (var i = 0; i < arrayLP.length; i++) {
			if(i == arrayLP.length -1)
			{
				csv += arrayLP[i][0].getValue('custrecord_serialnumber');
			}
			else
			{
				csv += arrayLP[i][0].getValue('custrecord_serialnumber') + ",";
			}
		}
	}
	return csv;
}
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}