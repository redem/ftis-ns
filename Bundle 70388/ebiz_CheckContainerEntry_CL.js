/***************************************************************************
                                                                 
   eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_CheckContainerEntry_CL.js,v $
*  $Revision: 1.2.14.1 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_CheckContainerEntry_CL.js,v $
*  Revision 1.2.14.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function fnCheckContainerEntry(type)
{		
		var vContainerName = nlapiGetFieldValue('name');
		var vSite = nlapiGetFieldValue('custrecord_ebizsitecontainer');
		var vCompany = nlapiGetFieldValue('custrecord_ebizcompanycontainer');
		
		if(vContainerName != "" && vContainerName != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
		{
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('name', null, 'is', vContainerName);
			filters[1] = new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', [vSite]);
			filters[2] = new nlobjSearchFilter('custrecord_ebizcompanycontainer', null, 'anyof', [vCompany]);
			filters[3] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
			
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_container', null, filters);
			
			if (searchresults != null && searchresults.length > 0) 
			{
				alert("The container # already exists.");
				return false;
			}
			else 
			{
				return true;
			}
		}
		else	
		{
			return true;
		}
}

function fnCheckContainerEntryEdit(type,name)
{		
		var vId = nlapiGetFieldValue('id');
		var vContainerName = nlapiGetFieldValue('name');
		var vSite = nlapiGetFieldValue('custrecord_ebizsitecontainer');
		var vCompany = nlapiGetFieldValue('custrecord_ebizcompanycontainer');
		
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is', vContainerName);
		filters[1] = new nlobjSearchFilter('internalid', null, 'noneof',vId);
		filters[2] = new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', [vSite]);
		filters[3] = new nlobjSearchFilter('custrecord_ebizcompanycontainer', null, 'anyof', [vCompany]);
		filters[4] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
		
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_container', null, filters);
		
		if (searchresults != null && searchresults.length > 0) 
		{
			alert("The container # already exists.");
			return false;
		}
		else 
		{
			return true;
		}
}