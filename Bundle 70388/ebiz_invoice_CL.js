
/***************************************************************************
 eBizNET Solutions Inc .
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_invoice_CL.js,v $
 *     	   $Revision: 1.3.10.1.4.1 $
 *     	   $Date: 2013/09/11 12:16:37 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log  $
 *****************************************************************************/

function validateQuantity()
{
	var totalqty=0;
	var lineCnt = nlapiGetLineItemCount('custpage_invoice_items');//to get total line count of sublist
	var orderqty=nlapiGetLineItemValue('custpage_invoice_items','custpage_itemsqty',1);
	var remainingQty=0;
	var checkselect=false;
	//loop through the sublist items to get the total quantity entered.and check wheter qty is empty
	for (var s = 1; s <= lineCnt; s++) {
		var chk = nlapiGetLineItemValue('custpage_invoice_items','custpage_itemcheckbox',s);
		var tempqty=0;
		var avalQty=0;
		var pickQty=0;
		if(nlapiGetLineItemValue('custpage_invoice_items','custpage_invitem_qty',s) != null && nlapiGetLineItemValue('custpage_invoice_items','custpage_invitem_qty',s) != "")			
			tempqty=nlapiGetLineItemValue('custpage_invoice_items','custpage_invitem_qty',s);
		if(nlapiGetLineItemValue('custpage_invoice_items','custpage_invitem_avalqty',s) != null && nlapiGetLineItemValue('custpage_invoice_items','custpage_invitem_avalqty',s) != "")
			avalQty = nlapiGetLineItemValue('custpage_invoice_items','custpage_invitem_avalqty',s);
		if(nlapiGetLineItemValue('custpage_invoice_items','custpage_item_pickedqty',s) != null && nlapiGetLineItemValue('custpage_invoice_items','custpage_item_pickedqty',s) != "")
			pickQty = nlapiGetLineItemValue('custpage_invoice_items','custpage_item_pickedqty',1);

		if(pickQty=='')
		{
			pickQty=0;
		}

		remainingQty=parseFloat(orderqty)-parseFloat(pickQty);


		if(tempqty==''&& chk=='T')
		{
			alert('Please enter quantity for line no:'+s);
			return false;
		}
		else if(isNaN(tempqty) == true)
		{
			alert('Please enter New Quantity as a number at line no:'+s);				
			return false;
		}
		else if((parseFloat(tempqty)>parseFloat(avalQty))&&chk=='T')
		{
			alert('quantity is greater than avalible qunatity at line no:'+s);
			return false;
		}
		else
		{
			if(chk=='T')
			{
				checkselect=true;
				totalqty=parseFloat(totalqty)+parseFloat(tempqty);
			}



		}

	}


	//check whether total quantity entered is with in the range of invoice quantity or not
	if(checkselect ==false)
	{
		alert('Please select atleast one line');
		return false;
	}
	if((parseFloat(totalqty)>parseFloat(remainingQty))&& checkselect==true)
	{
		alert('quantity is greater than order/invoice qunatity');
		return false;
	}
	else
	{                         

		return true;


	}

}




