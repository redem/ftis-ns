/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_FastPiclLotException.js,v $
 *     	   $Revision: 1.1.2.5.2.1 $
 *     	   $Date: 2015/11/16 16:42:38 $
 *     	   $Author: aanchal $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_FastPiclLotException.js,v $
 * Revision 1.1.2.5.2.1  2015/11/16 16:42:38  aanchal
 * 2015.2 Issue fix
 * 201415382
 *
 * Revision 1.1.2.5  2014/11/20 15:46:42  skavuri
 * Case# 201410762 Std bundle issue fixed
 *
 * Revision 1.1.2.4  2014/10/31 15:01:50  skavuri
 * Case # 201410762 Std Bundle issue fixed
 *
 * Revision 1.1.2.3  2014/06/13 13:14:15  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2  2014/05/30 00:41:01  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1  2013/11/14 06:32:04  skreddy
 * Case# 201215000
 * Fast Picking issue fix
 *
 * Revision 1.1.2.2  2013/09/16 09:40:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.1.2.1  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 *
 *
 *****************************************************************************/

function FastPickingLotExp(request, response){
	if (request.getMethod() == 'GET'){
		
		var getitemname = request.getParameter('custparam_itemname');
		var getebizzoneno = request.getParameter('custparam_ebizzoneno');
		var geterror = request.getParameter('custparam_error');
		var getcontainerlpno = request.getParameter('custparam_containerlpno');
		var getpicktype = request.getParameter('custparam_picktype');
		var getwaveno = request.getParameter('custparam_waveno');
		var getskipid = request.getParameter('custparam_skipid');
		var getorderlineno = request.getParameter('custparam_orderlineno');
		var getitem = request.getParameter('custparam_item');
		var getiteminternalid = request.getParameter('custparam_iteminternalid');
		var getfastpick = request.getParameter('custparam_fastpick');
		var getname = request.getParameter('name');
		var getexpectedquantity = request.getParameter('custparam_expectedquantity');
		var getebizordno = request.getParameter('custparam_ebizordno');
		var getinvoicerefno = request.getParameter('custparam_invoicerefno');
		var getendlocation = request.getParameter('custparam_endlocation');
		var getnextlocation = request.getParameter('custparam_nextlocation');
		var getclusterno = request.getParameter('custparam_clusterno');
		var getnextiteminternalid = request.getParameter('custparam_nextiteminternalid');
		var getendlocinternalid = request.getParameter('custparam_endlocinternalid');
		var getnextexpectedquantity = request.getParameter('custparam_nextexpectedquantity');
		var getbatchno = request.getParameter('custparam_batchno');
		var getbeginLocation = request.getParameter('custparam_beginLocation');
		var getscreenno = request.getParameter('custparam_screenno');
		var getserialno = request.getParameter('custparam_serialno');
		var getrecordinternalid = request.getParameter('custparam_recordinternalid');
		var getdolineid = request.getParameter('custparam_dolineid');
		var getbeginLocationname = request.getParameter('custparam_beginLocationname');
		var getbeginlocationname = request.getParameter('custparam_beginlocationname');
		var getwhlocation = request.getParameter('custparam_whlocation');
		var getnewcontainerlp = request.getParameter('custparam_newcontainerlp');
		var getitemdescription = request.getParameter('custparam_itemdescription');
	
		var EntLoc = request.getParameter('custparam_EntLoc');// Case# 201410762
		var ExceptionFlag = request.getParameter('custparam_Exc');// Case# 201410762

		
		nlapiLogExecution('Error', 'getContainerLpNo');
		
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no');
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";      
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entercontainerno').focus();";   

		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LOT : <label>" + getbatchno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN LOT# ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlotno' id='enterlotno' type='text'/>";
		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<input type='hidden' name='hdngetitemname' value=" + getitemname + ">";
		html = html + "				<input type='hidden' name='hdngetebizzoneno' value=" + getebizzoneno + ">";
		html = html + "				<input type='hidden' name='hdngeterror' value=" + geterror + ">";
		html = html + "				<input type='hidden' name='hdngetcontainerlpno' value=" + getcontainerlpno + ">";
		html = html + "				<input type='hidden' name='hdngetpicktype' value=" + getpicktype + ">";
		html = html + "				<input type='hidden' name='hdngetskipid' value=" + getskipid + ">";
		html = html + "				<input type='hidden' name='hdnwaveno' value=" + getwaveno + ">";
		html = html + "				<input type='hidden' name='hdngetorderlineno' value=" + getorderlineno + ">";
		html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
		html = html + "				<input type='hidden' name='hdngetiteminternalid' value=" + getiteminternalid + ">";
		html = html + "				<input type='hidden' name='hdngetfastpick' value=" + getfastpick + ">";
		html = html + "				<input type='hidden' name='hdngetname' value=" + getname + ">";
		html = html + "				<input type='hidden' name='hdngetexpectedquantity' value=" + getexpectedquantity + ">";
		html = html + "				<input type='hidden' name='hdngetebizordno' value=" + getebizordno + ">";
		html = html + "				<input type='hidden' name='hdngetinvoicerefno' value=" + getinvoicerefno + ">";
		html = html + "				<input type='hidden' name='hdngetendlocation' value=" + getendlocation + ">";
		html = html + "				<input type='hidden' name='hdngetnextlocation' value=" + getnextlocation + ">";
		html = html + "				<input type='hidden' name='hdngetclusterno' value=" + getclusterno + ">";
		html = html + "				<input type='hidden' name='hdngetnextiteminternalid' value=" + getnextiteminternalid + ">";
		html = html + "				<input type='hidden' name='hdngetendlocinternalid' value=" + getendlocinternalid + ">";
		html = html + "				<input type='hidden' name='hdngetnextexpectedquantity' value=" + getnextexpectedquantity + ">";
		html = html + "				<input type='hidden' name='hdngetbatchno' value=" + getbatchno + ">";
		html = html + "				<input type='hidden' name='hdngetbeginLocation' value=" + getbeginLocation + ">";
		html = html + "				<input type='hidden' name='hdngetscreenno' value=" + getscreenno + ">";
		html = html + "				<input type='hidden' name='hdngetserialno' value=" + getserialno + ">";
		html = html + "				<input type='hidden' name='hdngetrecordinternalid' value=" + getrecordinternalid + ">";
		html = html + "				<input type='hidden' name='hdngetdolineid' value=" + getdolineid + ">";
		html = html + "				<input type='hidden' name='hdngetbeginLocationname' value=" + getbeginLocationname + ">";
		html = html + "				<input type='hidden' name='hdnWaveNogetbeginlocationname' value=" + getbeginlocationname + ">";
		html = html + "				<input type='hidden' name='hdngetwhlocation' value=" + getwhlocation + ">";
		html = html + "				<input type='hidden' name='hdngetnewcontainerlp' value=" + getnewcontainerlp + ">";
		html = html + "				<input type='hidden' name='hdngetitemdescription' value=" + getitemdescription + ">";
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";//Case# 201410762
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";//Case# 201410762
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnConfirmflag.value=this.value;this.form.submit();return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		</table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlotno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		nlapiLogExecution('Error', 'getContainerLpNo');
		
		response.write(html);
		
	}
	else{

		var vitemname = request.getParameter('hdngetitemname');
		var vebizzoneno = request.getParameter('hdngetebizzoneno');
		var verror = request.getParameter('hdngeterror');
		var vcontainerlpno = request.getParameter('hdngetcontainerlpno');
		var vpicktype = request.getParameter('hdngetpicktype');
		var vskipid = request.getParameter('hdngetskipid');
		var vwaveno = request.getParameter('hdnwaveno');
		var vorderlineno = request.getParameter('hdngetorderlineno');
		var vitem = request.getParameter('hdngetitem');
		var viteminternalid = request.getParameter('hdngetiteminternalid');
		var vfastpick = request.getParameter('hdngetfastpick');
		var vname = request.getParameter('hdngetname');
		var vexpectedquantity = request.getParameter('hdngetexpectedquantity');
		var vebizordno = request.getParameter('hdngetebizordno');
		var vinvoicerefno = request.getParameter('hdngetinvoicerefno');
		var vendlocation = request.getParameter('hdngetendlocation');
		var vnextlocation = request.getParameter('hdngetnextlocation');
		var vclusterno = request.getParameter('hdngetclusterno');
		var vnextiteminternalid = request.getParameter('hdngetnextiteminternalid');
		var vendlocinternalid = request.getParameter('hdngetendlocinternalid');
		var vnextexpectedquantity = request.getParameter('hdngetnextexpectedquantity');
		var vbatchno = request.getParameter('hdngetbatchno');
		var vbeginLocation = request.getParameter('hdngetbeginLocation');
		var getitemname = request.getParameter('hdngetscreenno');
		var vscreenno = request.getParameter('hdngetserialno');
		var vrecordinternalid = request.getParameter('hdngetrecordinternalid');
		var vdolineid = request.getParameter('hdngetdolineid');
		var vbeginLocationname = request.getParameter('hdngetbeginLocationname');
		var vWaveNogetbeginlocationname = request.getParameter('hdnWaveNogetbeginlocationname');
		var vwhlocation = request.getParameter('hdngetwhlocation');
		var vnewcontainerlp = request.getParameter('hdngetnewcontainerlp');
		var vitemdescription = request.getParameter('hdngetitemdescription');
		var EntLoc = request.getParameter('hdnEntLoc');// Case# 201410762
		var ExceptionFlag = request.getParameter('hdnExceptionFlag'); //Case# 201410762
		var SOarray = new Array();
		SOarray["custparam_itemname"] = vitemname;
		SOarray["custparam_ebizzoneno"] = vebizzoneno;
		SOarray["custparam_error"] = verror;
		SOarray["custparam_containerlpno"] = vcontainerlpno;
		SOarray["custparam_picktype"] = vpicktype;
		/*SOarray["custparam_waveno"] = vskipid;
		SOarray["custparam_skipid"] = vwaveno;*/
		SOarray["custparam_waveno"] = vwaveno;
		SOarray["custparam_skipid"] = vskipid;
		SOarray["custparam_orderlineno"] = vorderlineno;
		SOarray["custparam_item"] = vitem;
		SOarray["custparam_iteminternalid"] = viteminternalid;
		SOarray["custparam_fastpick"] = vfastpick;
		SOarray["name"] = vname;
		SOarray["custparam_expectedquantity"] = vexpectedquantity;
		SOarray["custparam_ebizordno"] = vebizordno;
		SOarray["custparam_invoicerefno"] = vinvoicerefno;
		SOarray["custparam_endlocation"] = vendlocation;
		SOarray["custparam_nextlocation"] = vnextlocation;
		SOarray["custparam_clusterno"] = vclusterno;
		SOarray["custparam_nextiteminternalid"] = vnextiteminternalid;
		SOarray["custparam_endlocinternalid"] = vendlocinternalid;
		SOarray["custparam_nextexpectedquantity"] = vnextexpectedquantity;
		SOarray["custparam_batchno"] = vbatchno;
		SOarray["custparam_beginLocation"] = vbeginLocation;
		SOarray["custparam_serialno"] = getitemname;
		SOarray["custparam_screenno"] = vscreenno;
		SOarray["custparam_recordinternalid"] = vrecordinternalid;
		SOarray["custparam_dolineid"] = vdolineid;
		SOarray["custparam_beginLocationname"] = vbeginLocationname;
		SOarray["custparam_beginlocationname"] = vWaveNogetbeginlocationname;
		SOarray["custparam_whlocation"] = vwhlocation;
		SOarray["custparam_newcontainerlp"] = vnewcontainerlp;
		SOarray["custparam_itemdescription"] = vitemdescription;

		SOarray["custparam_error"] = 'INVALID LOT #';
		SOarray["custparam_screenno"] = 'FASTPickLotExp';
		SOarray["custparam_EntLoc"] = EntLoc; // Case# 201410762
		SOarray["custparam_Exc"] = ExceptionFlag; //Case# 201410762
		
		var newlotno=request.getParameter('enterlotno');

		var optedEvent = request.getParameter('cmdPrevious');

		nlapiLogExecution('Error', 'optedEvent',optedEvent);

		if(optedEvent!='F7'){
			nlapiLogExecution('Error', 'newlotno',newlotno);
			if(newlotno!='' && newlotno!=null){

				if(vbatchno != newlotno)
				{
					// check for valid Lot
					var result=GetBatchId(viteminternalid,newlotno);
					var BatchId=result[0];
					var IsValidflag=result[1];
					if(IsValidflag=='T')
					{

						//check inventory is available for that Lot and Binlocation
						var result=IsBatchNoValid(vendlocinternalid,viteminternalid,vexpectedquantity,BatchId);

						if((result.length>0)&& (result[0] == 'T'))
						{
							var IsValidBatchForBinLoc=result[0];
							var NewInvtRecId=result[1];
							var NewLp=result[2];
							var NewPackcode=result[3];
							var NewItemStatus=result[4];

							nlapiLogExecution('DEBUG', 'IsValidBatchForBinLoc', IsValidBatchForBinLoc);
							nlapiLogExecution('DEBUG', 'NewInvtRecId', NewInvtRecId);
							nlapiLogExecution('DEBUG', 'NewItemStatus', NewItemStatus);

							SOarray["custparam_Exc"] = 'L';
							SOarray["custparam_EntLocRec"] = NewInvtRecId;
							SOarray["custparam_batchno"] = newlotno;
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);



						}
						else
						{
							SOarray["custparam_error"] = 'INVENTORY IS NOT AVAILABLE FOR THE LOT#';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						}


					}
					else
					{
						SOarray["custparam_error"] = 'INVALID LOT #';
						SOarray["custparam_screenno"] = 'FASTPickLotExp';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					}
					//SOarray["custparam_batchno"] = newlotno;
					//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
				}

			}
			else{
				SOarray["custparam_error"] = 'INVALID LOT #';
				SOarray["custparam_screenno"] = 'FASTPickLotExp';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			}	
		}
		else{
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);

		}

	}
}

