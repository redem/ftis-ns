/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplenMenu.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2015/01/02 15:05:21 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PARAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplenMenu.js,v $
 * Revision 1.1.2.1  2015/01/02 15:05:21  skreddy
 * Case# 201411349
 * Two step replen process
 *
 *
 ****************************************************************************/

function TwoStepReplenMenu(request, response){
	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4,st5,st6,st7,st8;

		if( getLanguage == 'es_ES')
		{
			st0 = "TWO STEP REPLENISHMENT MENU";
			st1 = "PICK REPLEN";
			st2 = "PUTAWAY REPLEN";
			st3 = "ENTER SELECTION";
			st4 = "SEND";
			st5 = "PREV";		

		}
		else
		{
			st0 = "TWO STEP REPLENISHMENT MENU";
			st1 = "PICK REPLEN";
			st2 = "PUTAWAY REPLEN";
			st3 = "ENTER SELECTION";
			st4 = "SEND";
			st5 = "PREV";			
		}

		//Case# 20148880 (LinkButton Functionality)
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_ebiz_twostepreplenreportno', 'customdeploy_ebiz_twostepreplenreportno');
		var linkURL_1 = checkInURL_1; 
		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_ebiz_twostep_putawaycartlp', 'customdeploy_ebiz_twostep_putawaycartlp');
		var linkURL_2 = checkInURL_2; 

		var functionkeyHtml=getFunctionkeyScript('_rfinventorymenu'); 
		var html = "<html><head><title>" + st0 +  "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfinventorymenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 2. <a href='" + linkURL_2 + "' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"  + st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + "<input id='cmdSend' type='submit' value='ENT'/>";
		html = html + "				"	+ st5 + "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		// This variable is to get the option selected. If option selected is 1, it navigates to
		// the check-in screen. If the option selected is 2, it navigates to the putaway screen.
		var optedField = request.getParameter('selectoption');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		//Case # 20126437 Start
		var getLanguage = request.getParameter('hdngetLanguage');
		//Case # 20126437 End
		//case # 20124383Ã¯Â¿Â½ Start  
		var st11,st12;
		if( getLanguage == 'es_ES')
		{
			st11 = "OPCI&#211;N V&#193;LIDA";
			st12 = "InventoryMenu";
		}
		else
		{
			st11 = "INVALID OPTION";
			st12 = "InventoryMenu";
		}
	
		var POarray = new Array();  
		POarray["custparam_language"] = getLanguage;
		POarray["custparam_error"] = st11;
		POarray["custparam_screenno"] = '2stepMenu';	

		nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, POarray);
		}
		else			
			if (optedField == '1') {
				response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenreportno', 'customdeploy_ebiz_twostepreplenreportno', false, POarray);
			}
			else 
				if (optedField == '2') {
					response.sendRedirect('SUITELET', 'customscript_ebiz_twostep_putawaycartlp', 'customdeploy_ebiz_twostep_putawaycartlp', false, POarray);
				}			
				else{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				}

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}