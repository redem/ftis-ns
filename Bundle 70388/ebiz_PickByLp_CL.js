/***************************************************************************
??????????????????????????????? ? ????????????????????????????? ??eBizNET
???????????????????????                           eBizNET Solutions Inc
****************************************************************************
*
*? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Client/Attic/ebiz_PickByLp_CL.js,v $
*? $Revision: 1.1.2.1.4.1 $
*? $Date: 2012/11/01 14:55:22 $
*? $Author: schepuri $
*? $Name: t_NSWMS_2013_1_2_7 $
*
* DESCRIPTION
*? Functionality
*
* REVISION HISTORY
*? $Log: ebiz_PickByLp_CL.js,v $
*? Revision 1.1.2.1.4.1  2012/11/01 14:55:22  schepuri
*? CASE201112/CR201113/LOG201121
*? Decimal Qty Conversions
*?
*? Revision 1.1.2.1  2012/04/17 09:20:27  rrpulicherla
*? CASE201112/CR201113/LOG201121
*?
*? Pick By LP
*?
*
****************************************************************************/
function PickByLp_CL_Save()
{
	var lineCnt = nlapiGetLineItemCount('custpage_items');

	if (lineCnt > 0) 
	{
		var checkitemsselection=false;
		var fullfillordreno=nlapiGetFieldText('custpage_qbso');

		var returnvar=true;
		for (var s = 1; s <= lineCnt; s++) {
			var chk = nlapiGetLineItemValue('custpage_items','custpage_so',s);
				if(chk=='T')
				{
					checkitemsselection=true;
					var lp = nlapiGetLineItemValue('custpage_items','custpage_lp',s);
					var qty = nlapiGetLineItemValue('custpage_items','custpage_qty',s);
					var ordqty = nlapiGetLineItemValue('custpage_items','custpage_orderqty',s);
					if(lp=='Select')
					{
						alert('Please Choose Lp .');
						returnvar=false;
					}

					if(qty=='')
					{
						alert('Please Choose qty .');
						returnvar=false;
					}
					else if(qty!='' && isNaN(parseFloat(qty)))
					{
						alert('Please enter qty as number.');
						returnvar=false;
					}
					else
					{
						var value = nlapiGetLineItemValue('custpage_items','custpage_lp',s);
						var valuesArray=value.split(',');
						var allocatedqty=valuesArray[0];
						var sku_status=valuesArray[1];
						var packcode=valuesArray[2];
						var sku=valuesArray[3];
						var lot=valuesArray[5];
						var binloc=valuesArray[6];
						var quantity=Math.min(ordqty,allocatedqty);
						if(parseFloat(qty)>parseFloat(quantity))
						{

							alert('Please enter quantity lessthan or equal to '+quantity);
							returnvar=false;
						}
					}
				}
			
		}
if(fullfillordreno==(nlapiGetLineItemValue('custpage_items','custpage_sono',1)) && checkitemsselection==false)
{
returnvar=false;
alert("Please Select atleast one Order");
}
		//}
		
		return returnvar;
	}
	
}
function onLpChange(type, name,linenum)
{
	var returnvar=true;
	if(name=='custpage_so')
		{
		var chk = nlapiGetLineItemValue('custpage_items','custpage_so',linenum);
		if(chk=='T')
			{
			var lp = nlapiGetLineItemValue('custpage_items','custpage_lp',linenum);
			var qty = nlapiGetLineItemValue('custpage_items','custpage_qty',linenum);
			if(lp=='Select')
				{
				alert('Please Choose Lp .');
				returnvar=false;
				}
			if(qty!='' && isNaN(parseFloat(qty)))
				{
				alert('Please enter qty as number.');
				returnvar=false;
				}
			}
		}
	if(name=='custpage_lp')
		{
		var value = nlapiGetLineItemValue('custpage_items','custpage_lp',linenum);
		var valuesArray=value.split(',');
		var allocatedqty=valuesArray[0];
		var sku_status=valuesArray[1];
		var packcode=valuesArray[2];
		var sku=valuesArray[3];
		var lp=valuesArray[4];
		var beginloc=valuesArray[5];
		var lot=valuesArray[6];
		var binloc=valuesArray[5];var chklot=true;var chkbinloc=true;
		var selectedLotValue = nlapiGetLineItemValue('custpage_items','custpage_lotbatch',linenum);
		var selectedBinLocValue = nlapiGetLineItemValue('custpage_items','custpage_binlocations',linenum);
		
		if((selectedLotValue!=null && selectedLotValue!='Select' && selectedLotValue!=''))
	      {
			if((lot!=selectedLotValue))
			{
				chklot=false;
			}
	      }
		if((selectedBinLocValue!=null && selectedBinLocValue!='Select' && selectedBinLocValue!=''))
	      {
			if((binloc!=selectedBinLocValue))
			{
				chkbinloc=false;
			}
	      }


		var id=valuesArray[7];//nlapiGetLineItemValue('custpage_items','custpage_skustatustext',linenum)==sku_status &&&&  nlapiGetLineItemValue('custpage_items','custpage_packcode',linenum)==packcode
		if(nlapiGetLineItemValue('custpage_items','custpage_itemname',linenum)==sku && nlapiGetLineItemValue('custpage_items','custpage_skustatustext',linenum)==sku_status &&  nlapiGetLineItemValue('custpage_items','custpage_packcode',linenum)==packcode && chklot==true && chkbinloc==true && allocatedqty>0)
		{

				var orderqty=nlapiGetLineItemValue('custpage_items','custpage_orderqty',linenum);
				nlapiSetLineItemValue('custpage_items','custpage_createinvntid',linenum,id);
				nlapiSetLineItemValue('custpage_items','custpage_lpno',linenum,lp);
				nlapiSetLineItemValue('custpage_items','custpage_beginloc',linenum,beginloc);	
				nlapiSetLineItemValue('custpage_items','custpage_lpavailqty',linenum,allocatedqty);
				nlapiSetLineItemValue('custpage_items','custpage_qty',linenum,Math.min(allocatedqty,orderqty));
				

		}
		else
			{
			if(chklot==false)
				{
				alert('Not a Valid lot/Bacth no for this Lp');
				}
			else if(chkbinloc==false)
				{
				alert('Not a Valid binLocation  for this Lp');
				}
			else
				{
				alert('Not a valid Lp for this item.')
				}
			nlapiSetLineItemValue('custpage_items','custpage_createinvntid',linenum,'');
			nlapiSetLineItemValue('custpage_items','custpage_lpno',linenum,'');
			nlapiSetLineItemValue('custpage_items','custpage_beginloc',linenum,'');	
			nlapiSetLineItemValue('custpage_items','custpage_lpavailqty',linenum,'');
			nlapiSetLineItemValue('custpage_items','custpage_qty',linenum,'');
			
			returnvar=false;
			}
	      }
		
	if(name=='custpage_qty')
		{
		var lp = nlapiGetLineItemValue('custpage_items','custpage_lp',linenum);
		var qty = nlapiGetLineItemValue('custpage_items','custpage_qty',linenum);
		if(lp=='Select')
			{
			alert('Please Choose Lp .');
			returnvar=false;
			}
		if(qty!='' && isNaN(parseFloat(qty)))
		{
		alert('Please enter qty as number.');
		nlapiSetLineItemValue('custpage_items','custpage_qty',linenum,'');
		returnvar=false;
		}
		
		}
	
	if(name=='custpage_lotbatch')
		{
		var lotnum = nlapiGetLineItemValue('custpage_items','custpage_lotbatch',linenum);
		
//		var createInventoryFilters = new Array();
//		var createInventoryCols=new Array();
//		createInventoryCols[0]= new nlobjSearchColumn('custrecord_ebiz_inv_lp');
//		createInventoryCols[1]= new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
//		createInventoryCols[2]=  new nlobjSearchColumn('custrecord_ebiz_inv_sku').setSort();
//		createInventoryCols[3]=  new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
//		createInventoryCols[4]=  new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
//		createInventoryCols[5]=  new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
//		createInventoryCols[3]=  new nlobjSearchColumn('custrecord_ebiz_qoh');
//		createInventoryCols[4]=  new nlobjSearchColumn('custrecord_ebiz_inv_lot');
//		createInventoryCols[5]=  new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
//		
//		createInventoryFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']));
//		createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_cycl_count_hldflag', null, 'is', 'F'));
//		createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_invholdflg', null, 'is', 'F'));
//		createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
//		createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', lotnum));
//		
//		var createInventoryLotList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, createInventoryFilters, createInventoryCols);
//		
//		
//		alert(createInventoryLotList.length);
//		// NLDoMainFormButtonAction("submitter",true);	
		
	//	nlapiSetFieldValue('custpage_hdnlotnumber',lotnum);
	//	NLDoMainFormButtonAction("submitter",true);	
		}
	if(name=='custpage_binlocations')
	{
	//var binloc = nlapiGetLineItemValue('custpage_items','custpage_binlocations',linenum);
	//nlapiSetFieldValue('custpage_hdnbinloc',binloc);
	//NLDoMainFormButtonAction("submitter",true);	
	}
	return returnvar;
}
