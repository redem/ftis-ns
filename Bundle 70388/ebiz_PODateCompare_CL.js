/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_PODateCompare_CL.js,v $
 *     	   $Revision: 1.4 $
 *     	   $Date: 2012/05/14 14:15:53 $
 *     	   $Author: rgore $
 *     	   $Name: t_NSWMS_2012_1_1_9 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PODateCompare_CL.js,v $
 * Revision 1.4  2012/05/14 14:15:53  rgore
 * CASE201112/CR201113/LOG201121
 * Sanitized 'ALERT' messages for spelling and grammar.
 * - Ratnakar
 * 14 May 2012
 *
 * Revision 1.3  2011/07/21 04:49:46  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.10  2011/07/20 12:57:48  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.4  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function PODateCompare(){
	var podate = nlapiGetFieldValue('trandate');
	var shipdate = nlapiGetFieldValue('custbody_nswmspoexpshipdate');
	var arrivaldate = nlapiGetFieldValue('custbody_nswmspoarrdate');
	if ((podate != '' && arrivaldate != '') || (podate != '' && shipdate != '')) {

		if (podate != '' && arrivaldate != '') {
			if (CompareDates(arrivaldate, podate) == false) {
				alert('Arrival Date should be later than PO Date');
				return false;
			}
			else {
				return true;
			}
		}
		if (podate != '' && shipdate != '') {
			if (CompareDates(shipdate, podate) == false) {
				alert('Expected Ship Date should be later than PO Date');
				return false;
			}
			else {
				return true;
			}
		}

	}
}
