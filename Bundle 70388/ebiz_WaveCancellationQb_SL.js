/***************************************************************************
�� eBizNET Solutions
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_WaveCancellationQb_SL.js,v $
 *� $Revision: 1.2.2.10.4.1.4.4.4.1 $
 *� $Date: 2014/10/15 15:44:50 $
 *� $Author: skavuri $
 *� $Name: t_eBN_2014_2_StdBundle_0_135 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_WaveCancellationQb_SL.js,v $
 *� Revision 1.2.2.10.4.1.4.4.4.1  2014/10/15 15:44:50  skavuri
 *� Case# 201410681 Std bundle issue fixed
 *�
 *� Revision 1.2.2.10.4.1.4.4  2013/12/02 13:47:04  gkalla
 *� case#20126005
 *� Dynacraft wave cancellation befor wave generation issue
 *�
 *� Revision 1.2.2.10.4.1.4.3  2013/08/09 16:07:00  nneelam
 *� Case#. �20123807
 *� Set Wave # to mandatory.
 *�
 *� Revision 1.2.2.10.4.1.4.2  2013/03/26 13:27:42  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.2.2.10.4.1.4.1  2013/03/05 14:57:31  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Code merged from Lexjet production as part of Standard bundle
 *�
 *� Revision 1.2.2.10.4.1  2012/11/01 14:55:02  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.2.2.10  2012/09/13 12:50:02  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Added new  filter  criteria i.e., Cluster# in canceling the wave.
 *�
 *� Revision 1.2.2.9  2012/08/10 17:47:52  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� UAT Issue Fixes
 *�
 *� Revision 1.2.2.8  2012/07/10 23:33:37  gkalla
 *� CASE201112/CR201113/LOG201121
 *� to populate more than 1000 records
 *�
 *� Revision 1.2.2.7  2012/06/18 07:34:38  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� UAT and Prod issue fixes
 *�
 *� Revision 1.2.2.6  2012/06/04 14:57:57  spendyala
 *� CASE201112/CR201113/LOG201121
 *� issue related to JAE is resolved.
 *�
 *� Revision 1.2.2.5  2012/04/03 10:44:46  gkalla
 *� CASE201112/CR201113/LOG201121
 *� To populate all wavenums in dropdown means greater than 1000
 *�
 *� Revision 1.2.2.4  2012/03/30 15:48:05  gkalla
 *� CASE201112/CR201113/LOG201121
 *� To populate all wavenums in dropdown means greater than 1000
 *�
 *� Revision 1.2.2.3  2012/03/15 14:12:49  schepuri
 *� CASE201112/CR201113/LOG201121
 *� morethan 1000 rec retrive
 *�
 *� Revision 1.2.2.2  2012/02/20 15:24:24  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Stable bundle issue fixes
 *�
 *� Revision 1.2.2.1  2012/01/25 13:47:51  spendyala
 *� CASE201112/CR201113/LOG201121
 *� moved to branch.
 *�
 *
 ****************************************************************************/


function WaveCancellationQbSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start of GET',context.getRemainingUsage());

		var form = nlapiCreateForm('Wave Cancellation');

		var selectcriteria = form.addField('custpage_selectcriteria', 'select', 'Criteria').setLayoutType('startrow','none');
		selectcriteria.addSelectOption('WAVE', 'Wave Level');
		selectcriteria.addSelectOption('HEADER_LEVEL', 'Order Level');
		selectcriteria.addSelectOption('DETAIL_LEVEL', 'Order Line Level');
		selectcriteria.addSelectOption('CLUSTER', 'Cluster Level');

		var waveListField = form.addField('custpage_wavelist', 'select', 'Wave #').setLayoutType('startrow', 'none');
		waveListField.addSelectOption("","");	
		//Case # 20123807 start
		waveListField.setMandatory(true);
		//Case # 20123807 end

		var vWaveArr=new Array();
		//var openTaskRecordsforWave = getAllOpenTaskRecordsForWaveCancel('HEADER_LEVEL', null, null);	
		// Add information to the selection fields
		//addValuesToFields(null, waveListField, null, openTaskRecordsforWave);
		/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
		//rolebased location
		var vRoleLocation=getRoledBasedLocation();
		var searchResult=	getAllOpenTaskRecordsForWaveCancel(form, waveListField,-1,vRoleLocation);
		/*** upto here ***/
		for (var j = 0; searchResult != null && j < searchResult.length; j++) {
			var openTaskRecords=searchResult[j];
			for (var i = 0; openTaskRecords != null && i < openTaskRecords.length; i++) {
				if(openTaskRecords[i].getValue('custrecord_ebiz_wave_no') != null && openTaskRecords[i].getValue('custrecord_ebiz_wave_no') != '' && vWaveArr.indexOf(openTaskRecords[i].getValue('custrecord_ebiz_wave_no')) == -1)
				{	
					vWaveArr.push(openTaskRecords[i].getValue('custrecord_ebiz_wave_no'));

					/*var resdo = form.getField('custpage_wavelist').getSelectOptions(openTaskRecords[i].getValue('custrecord_ebiz_wave_no'), 'is');
				if (resdo != null) {
					if (resdo.length > 0) {
						continue;
					}
				}*/
					if (!isNaN(openTaskRecords[i].getValue('custrecord_ebiz_wave_no')))
					{
						waveListField.addSelectOption(openTaskRecords[i].getValue('custrecord_ebiz_wave_no'), openTaskRecords[i].getValue('custrecord_ebiz_wave_no'));
					}//Case# 201410681
					}
			}
		}

		var orderField = form.addField('custpage_orderlist', 'select', 'Order #').setLayoutType('startrow', 'none');
		orderField.addSelectOption("","");
		/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
		//rolebased location
		var openTaskRecordsfororder = getAllOpenTaskRecordsFororderlevel('HEADER_LEVEL', null,0,vRoleLocation);
		/*** upto here ***/
		addValuesToFields(orderField, null, null, openTaskRecordsfororder);
		//var salesOrderList = getAllSalesOrders();

		// Add all sales orders to SO Field
		//addAllSalesOrdersToField(orderField, salesOrderList);

		var orderlineField = form.addField('custpage_orderlinelist', 'select', 'Fullfilment Order #').setLayoutType('startrow', 'none');
		orderlineField.addSelectOption("","");
		/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
		//rolebased location
		var openTaskRecordsfororderline = getAllOpenTaskRecordsFororderlinelevel('HEADER_LEVEL', null, null,0,vRoleLocation);
		/*** upto here ***/
		addValuesToFields(null, null, orderlineField, openTaskRecordsfororderline);
		//var salesOrderLineList=getAllSalesOrderslines();
		//AddAllSalesorderlines(orderlineField,salesOrderLineList,form);

		//addValuesToFields(null, orderField, null, openTaskRecords);

		//code written as on 120912 by suman
		//added new filter criteria i.e., Cluster#
		var vclusterArr=new Array();
		var ClusterField = form.addField('custpage_cluster', 'select', 'Cluster #').setLayoutType('startrow', 'none');
		ClusterField.addSelectOption("","");
		/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
		var vsearchResult=GetClusters(-1,vRoleLocation);
		/*** upto here ***/
		for (var j = 0; vsearchResult != null && j < vsearchResult.length; j++) {
			var vopenTaskRecords=vsearchResult[j];
			for (var i = 0; vopenTaskRecords != null && i < vopenTaskRecords.length; i++) {
				if(vopenTaskRecords[i].getValue('custrecord_ebiz_clus_no') != null && vopenTaskRecords[i].getValue('custrecord_ebiz_clus_no') != '' && vclusterArr.indexOf(vopenTaskRecords[i].getValue('custrecord_ebiz_clus_no')) == -1)
				{	
					vclusterArr.push(vopenTaskRecords[i].getValue('custrecord_ebiz_clus_no'));
					ClusterField.addSelectOption(vopenTaskRecords[i].getValue('custrecord_ebiz_clus_no'), vopenTaskRecords[i].getValue('custrecord_ebiz_clus_no'));
				}
			}
		}
		//end of code as of 120912.

		nlapiLogExecution('ERROR','Remaining usage at the end of GET',context.getRemainingUsage());

		var button = form.addSubmitButton('Display');
		form.setScript('customscript_wavecancellation_cl');
		response.writePage(form);
	}
	else
	{
		var waveArray = new Array();
		waveArray["custpage_checkfield"] = request.getParameter('custpage_checkfield');
		waveArray["custpage_selectcriteria"] = request.getParameter('custpage_selectcriteria');
		waveArray["custpage_wavelist"] = request.getParameter('custpage_wavelist');
		waveArray["custpage_orderlist"] = request.getParameter('custpage_orderlist');
		waveArray["custpage_orderlinelist"] = request.getParameter('custpage_orderlinelist');
		var waveNo;
		var orderno;
		var orderlineno;
		waveArray["custpage_cluster"] = request.getParameter('custpage_cluster');
	waveNo = request.getParameter('custpage_wavelist');
		orderno = request.getParameter('custpage_orderlist');
		orderlineno = request.getParameter('custpage_orderlinelist');

		var vOrdDets=getFODetailsOfWave(waveNo,orderno,orderlineno);

		if(vOrdDets == null || vOrdDets == '' || vOrdDets.length == null)		
		response.sendRedirect('SUITELET', 'customscript_wave_cancellation','customdeploy_wave_cancellation_di', false, waveArray);
		else
		{
						 
			var form = nlapiCreateForm('Wave Cancellation');
			var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'Wave generation is in progress', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			var selectcriteria = form.addField('custpage_selectcriteria', 'select', 'Criteria').setLayoutType('startrow','none');
			selectcriteria.addSelectOption('WAVE', 'Wave Level');
			selectcriteria.addSelectOption('HEADER_LEVEL', 'Order Level');
			selectcriteria.addSelectOption('DETAIL_LEVEL', 'Order Line Level');

			var waveListField = form.addField('custpage_wavelist', 'select', 'Wave #').setLayoutType('startrow', 'none');
			waveListField.addSelectOption("","");		
			//var openTaskRecordsforWave = getAllOpenTaskRecordsForWaveCancel('HEADER_LEVEL', null, null);	
			// Add information to the selection fields
			//addValuesToFields(null, waveListField, null, openTaskRecordsforWave);
			var searchResult=	getAllOpenTaskRecordsForWaveCancel(form, waveListField,-1);
			for (var j = 0; searchResult != null && j < searchResult.length; j++) {
				var openTaskRecords=searchResult[j];
				for (var i = 0; openTaskRecords != null && i < openTaskRecords.length; i++) {
					var resdo = form.getField('custpage_wavelist').getSelectOptions(openTaskRecords[i].getValue('custrecord_ebiz_wave_no'), 'is');
					if (resdo != null) {
						if (resdo.length > 0) {
							continue;
						}
					}
					waveListField.addSelectOption(openTaskRecords[i].getValue('custrecord_ebiz_wave_no'), openTaskRecords[i].getValue('custrecord_ebiz_wave_no'));
				}
			}

			var orderField = form.addField('custpage_orderlist', 'select', 'Order #').setLayoutType('startrow', 'none');
			orderField.addSelectOption("","");
			var openTaskRecordsfororder = getAllOpenTaskRecordsFororderlevel('HEADER_LEVEL', null,0);
			addValuesToFields(orderField, null, null, openTaskRecordsfororder);
			//var salesOrderList = getAllSalesOrders();

			// Add all sales orders to SO Field
			//addAllSalesOrdersToField(orderField, salesOrderList);

			var orderlineField = form.addField('custpage_orderlinelist', 'select', 'Fullfilment Order #').setLayoutType('startrow', 'none');
			orderlineField.addSelectOption("","");
			var openTaskRecordsfororderline = getAllOpenTaskRecordsFororderlinelevel('HEADER_LEVEL', null, null,0);
			addValuesToFields(null, null, orderlineField, openTaskRecordsfororderline);
			//var salesOrderLineList=getAllSalesOrderslines();
			//AddAllSalesorderlines(orderlineField,salesOrderLineList,form);

			//addValuesToFields(null, orderField, null, openTaskRecords);

			//nlapiLogExecution('ERROR','Remaining usage at the end of GET',context.getRemainingUsage());

			var button = form.addSubmitButton('Display');
			form.setScript('customscript_wavecancellation_cl');
			response.writePage(form);
		}	

	}
}
function getOpenTaskColumns(levelInd){
	var columns = new Array();
	nlapiLogExecution('ERROR', 'levelInd', levelInd);
	if(levelInd == 'HEADER_LEVEL'){
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_line_no');
		columns[2] = new nlobjSearchColumn('custrecord_tasktype');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[7] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[6].setSort(true); // Sort by wave no.
	} else if(levelInd == 'DETAIL_LEVEL') {
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[6] = new nlobjSearchColumn('custrecord_line_no');
		columns[7] = new nlobjSearchColumn('custrecord_tasktype');
		columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[10] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[11] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
		columns[12] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[13] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[16] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[17] = new nlobjSearchColumn('custrecord_invref_no');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_sku_status');
		columns[20] = new nlobjSearchColumn('custrecord_uom_id');
		columns[21] = new nlobjSearchColumn('custrecord_batch_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebizrule_no');
		columns[23] = new nlobjSearchColumn('custrecord_ebizmethod_no');
		columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
		columns[25] = new nlobjSearchColumn('custrecord_comp_id');
		columns[26] = new nlobjSearchColumn('custrecord_wms_location');						
		columns[27] = new nlobjSearchColumn('custrecord_ebizuser');		
		columns[28] = new nlobjSearchColumn('custrecord_upd_ebiz_user_no');						
		columns[29] = new nlobjSearchColumn('custrecord_taskassignedto');
		columns[12].setSort(true);  // Sort by wave number
	}

	return columns;
}

var searchResultsForWave = new Array();
/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
//added vRoleLocation as a parameter
function getAllOpenTaskRecordsForWaveCancel(form, customerField,maxno,vRoleLocation){
	/*** upto here ***/
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsForWaveCancel', 'Start');

	var filters = new Array();
	// Look for valid wave#

	filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty');

	// Look for records with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']);
	if(parseFloat(maxno)!=-1)
	{
		nlapiLogExecution('ERROR', 'maxno', maxno);
		filters[2]=new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno));
	}
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));
	}
	/*** upto here ***/
	// Get list of columns to retrieve based on the level of granularity
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[1] = new nlobjSearchColumn('id');
	//columns[0].setSort(true);
	columns[1].setSort(true);

	// Load all the open task records as per filter criteria
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{
		nlapiLogExecution('ERROR', 'openTaskRecords for Wave1', openTaskRecords.length);
		//var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[1]);		
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue('id');	
		searchResultsForWave.push(openTaskRecords);
		getAllOpenTaskRecordsForWaveCancel(form, customerField,maxno,vRoleLocation);	

	}
	else
	{
		nlapiLogExecution('ERROR', 'openTaskRecords for Wave2', openTaskRecords.length);
		searchResultsForWave.push(openTaskRecords);
	}
	return searchResultsForWave;
}

var searchResultsForOrder = new Array();
function getAllOpenTaskRecordsFororderlevel(levelInd, waveCancelLevel, maxid,vRoleLocation){
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlevel', 'Start');

	var filters = new Array();
	// Look for valid wave#
	if((levelInd == 'HEADER_LEVEL') || (levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE')){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty');

	}else if(levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE'){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'equals', waveNo);

	}

	// Look for records with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']);
	if(maxid!=0)
		filters[2]=new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxid));
	// Get list of columns to retrieve based on the level of granularity
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/		
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));
	}
	/*** upto here ***/
	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[1] = new nlobjSearchColumn('id');
	//columns[0].setSort(true);
	columns[1].setSort(true);
	// Load all the open task records as per filter criteria
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{
		nlapiLogExecution('ERROR', 'openTaskRecords for Order1', openTaskRecords.length);
		searchResultsForOrder.push(openTaskRecords);
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[1]);		
		getAllOpenTaskRecordsFororderlevel(levelInd, waveCancelLevel, maxno,vRoleLocation);	
	}
	else
	{
		nlapiLogExecution('ERROR', 'openTaskRecords for Order2', openTaskRecords.length);
		searchResultsForOrder.push(openTaskRecords);
	}

	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlevel', 'End');
	return searchResultsForOrder;
//	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlevel', 'End');
//	return openTaskRecords;
}
var searchResultsForOrderlinelevel = new Array();
function getAllOpenTaskRecordsFororderlinelevel(levelInd, waveCancelLevel, waveNo,maxid,vRoleLocation){
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlinelevel', 'Start');

	var filters = new Array();
	// Look for valid wave#
	if((levelInd == 'HEADER_LEVEL') || (levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE')){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty');

	}else if(levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE'){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'equals', waveNo);

	}

	// Look for records with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']);
	if(maxid!=0)
		filters[2] = new nlobjSearchFilter('id', null, 'lessthan', maxid);
	// Get list of columns to retrieve based on the level of granularity
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/	
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));
	}
	/*** upto here ***/
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('id');

	columns[0].setSort(true);
	columns[1].setSort(true);
	// Load all the open task records as per filter criteria
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{
		nlapiLogExecution('ERROR', 'openTaskRecords fulfillment1', openTaskRecords.length);
		searchResultsForOrderlinelevel.push(openTaskRecords);
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[1]);		
		getAllOpenTaskRecordsFororderlinelevel(levelInd, waveCancelLevel, waveNo,maxno,vRoleLocation);
	}
	else
	{
		nlapiLogExecution('ERROR', 'openTaskRecords fulfillment2', openTaskRecords.length);
		searchResultsForOrderlinelevel.push(openTaskRecords);
	}

	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlinelevel', 'End');
	return searchResultsForOrderlinelevel;
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsFororderlinelevel', 'End');
	return openTaskRecords;
}

function addValuesToFields(OrderField, waveListField, fulfillOrderList, searchResult){
	nlapiLogExecution('ERROR', 'addValuesToFields', 'Start');
	var ordersAlreadyAddedList = new Array();
	var wavesAlreadyAddedList = new Array();

	if(searchResult != null && searchResult.length > 0){
		for(var j = 0; j < searchResult.length; j++){
			var openTaskRecords=searchResult[j];
			for(var i = 0; i < openTaskRecords.length; i++){
				var currentTask = openTaskRecords[i];
				var fulfillOrderNo = currentTask.getValue('name');
				var waveNo = currentTask.getValue('custrecord_ebiz_wave_no');
				var OrderNo=currentTask.getText('custrecord_ebiz_order_no');
				var ebizOrderNo=currentTask.getValue('custrecord_ebiz_order_no');
				if(fulfillOrderList != null){
					/*if(!isValueAlreadyAdded(ordersAlreadyAddedList, fulfillOrderNo)){

						fulfillOrderList.addSelectOption(fulfillOrderNo, fulfillOrderNo);
						ordersAlreadyAddedList.push(fulfillOrderNo);
					}*/
					if(ordersAlreadyAddedList.indexOf(fulfillOrderNo) == -1)
					{	  
						ordersAlreadyAddedList.push(fulfillOrderNo);
						fulfillOrderList.addSelectOption(fulfillOrderNo, fulfillOrderNo);
					}

				}
				if(OrderField != null){
					/*if(!isValueAlreadyAdded(ordersAlreadyAddedList, ebizOrderNo)){

						OrderField.addSelectOption(ebizOrderNo, OrderNo);
						ordersAlreadyAddedList.push(ebizOrderNo);
					}*/
					if(ordersAlreadyAddedList.indexOf(ebizOrderNo) == -1)
					{	  
						ordersAlreadyAddedList.push(ebizOrderNo);
						OrderField.addSelectOption(ebizOrderNo, OrderNo);
					}
				}
				if(waveListField != null)
				{
					/*if(!isValueAlreadyAdded(wavesAlreadyAddedList, waveNo)){

						waveListField.addSelectOption(waveNo, waveNo);
						wavesAlreadyAddedList.push(waveNo);
					}*/
					if(wavesAlreadyAddedList.indexOf(waveNo) == -1)
					{	  
						wavesAlreadyAddedList.push(waveNo);
						waveListField.addSelectOption(waveNo, waveNo);
					}
				}
			}
		}
	}
}

function isValueAlreadyAdded(alreadyAddedList, currentValue){
	var alreadyAdded = false;

	for(var i = 0; i < alreadyAddedList.length; i++){
		if(alreadyAddedList[i] == currentValue){
			alreadyAdded = true;
			break;
		}
	}

	return alreadyAdded;
}

var searchResultsForCluster = new Array();
function GetClusters(maxno,vRoleLocation)
{
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26'])); 
	if(parseFloat(maxno)!=-1)
	{
		nlapiLogExecution('ERROR', 'maxno', maxno);
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/	
	nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
		filtersso.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));
	}
	/*** upto here ***/
	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_clus_no').setSort('true'));
	columnso[1] = new nlobjSearchColumn('id');
	columnso[1].setSort(true);

	var openTaskRecords = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);
	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{
		nlapiLogExecution('ERROR', 'openTaskRecords for Wave1', openTaskRecords.length);
		//var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[1]);		
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue('id');	
		searchResultsForCluster.push(openTaskRecords);
		GetClusters(maxno,vRoleLocation);	

	}
	else
	{
		//nlapiLogExecution('ERROR', 'openTaskRecords for Wave2', openTaskRecords.length);
		searchResultsForCluster.push(openTaskRecords);
	}
	return searchResultsForCluster;
}
function getFODetailsOfWave(waveNo,orderno,orderlineno){
	nlapiLogExecution('ERROR', 'getFODetailsOfWave', 'Start');
	nlapiLogExecution('ERROR', 'orderlineno', orderlineno);
	nlapiLogExecution('ERROR', 'orderno', orderno);
	nlapiLogExecution('ERROR', 'waveNo', waveNo);
	// Filter open task records by wave number and wms_status_flag
	var filters = new Array();
	if(waveNo!=null && waveNo!='')
	{ 	 
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto',parseFloat(waveNo)));

	}
	if(orderno!=null && orderno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', orderno));
	}
	if(orderlineno!=null && orderlineno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', orderlineno));
	}
	filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['15']));

	var cols = new Array();
	cols[0]=new nlobjSearchColumn('name');

	// Retrieve all open fulfillment order lines
	var openFODetails = new nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, cols);
	if(openFODetails != null && openFODetails != '')	 
		nlapiLogExecution('ERROR', 'openFODetails', openFODetails.length);

	return openFODetails;
}