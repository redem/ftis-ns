/***************************************************************************
??????????????????????????????? ? ????????????????????????????? ??eBizNET
???????????????????????                           eBizNET Solutions Inc
****************************************************************************
*
*? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Suitelet/ebiz_systemRuleNewRecord.js,v $
*? $Revision: 1.1.2.1 $
*? $Date: 2012/04/16 15:06:34 $
*? $Author: spendyala $
*? $Name: t_NSWMS_2012_1_1_2 $
*
* DESCRIPTION
*? Functionality
*
* REVISION HISTORY
*? $Log: ebiz_systemRuleNewRecord.js,v $
*? Revision 1.1.2.1  2012/04/16 15:06:34  spendyala
*? CASE201112/CR201113/LOG201121
*? Issue related to Exception is resolved.
*?
*? Revision 1.1  2011/12/28 07:34:07  spendyala
*?  CASE201112/CR201113/LOG201121
*?  inserting new records into system rules
*?
*
****************************************************************************/

/**
 * To Create New record for system rule
 * @param request
 * @param response
 */
function CreateNewRecord(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var form = nlapiCreateForm('System Rule List');
		var delField=form.addField('deleted_lines','text','Deleted Lines').setDisplayType('hidden');

		form.setScript('customscript_systemrule_cl');
		var sublist = form.addSubList('custpage_rules', 'inlineeditor', 'SysRule List');
		var location = sublist.addField("custpage_location", "select", "Location");
		var company = sublist.addField("custpage_company", "select", "Company");
		sublist.addField("custpage_processtype", "select", "Process Type","customlist_ebiz_process_type");
		sublist.addField("custpage_tasktype", "select", "Task Type", "customrecord_ebiznet_tasktype");
		var defaultid=sublist.addField("custpage_defaultid", "select", "Rule ID");
		sublist.addField("custpage_description", "text", "Description").setDisplayType('disabled');
		sublist.addField("custpage_ruletype", "text", "Rule Type").setDisplayType('disabled');
		sublist.addField("custpage_rulevalue", "select", "Rule Value");
		sublist.addField("custpage_internalid","text","internalid").setDisplayType('hidden');
		sublist.addField("custpage_id","text","Id").setDisplayType('hidden');
		sublist.addField("custpage_rulevaluetext","text","rule text").setDisplayType('hidden');
		
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizsite').setSort(true);
		column[1]=new nlobjSearchColumn('custrecord_ebizcomp');
		column[2]=new nlobjSearchColumn('custrecord_ebizprocesstype');
		column[3]=new nlobjSearchColumn('custrecord_ebiztasktype');
		column[4]=new nlobjSearchColumn('name');
		column[5]=new nlobjSearchColumn('custrecord_ebizdescription');
		column[6]=new nlobjSearchColumn('custrecord_ebizruletype');
		column[7]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		column[8]=new nlobjSearchColumn('internalId').setSort();

		var searchrecord=nlapiSearchRecord('customrecord_ebiznet_sysrules', null, null, column);
		if(searchrecord!=null&&searchrecord!='')
		{
			//get all the default id
			defaultid.addSelectOption('','');
			for(var count=0;count<searchrecord.length;count++)
			{
				defaultid.addSelectOption(searchrecord[count].getId(),searchrecord[count].getValue('name'));
			}
		}
//		//get all the default rule ids
//		
//		var columnRuleIds=new Array();
//		columnRuleIds[0]=new nlobjSearchColumn('name');
//		var searchRuleIds=nlapiSearchRecord('customrecord_ebiznet_sysrulevalues',null,null,columnRuleIds);
//		for(var count=0;count<searchRuleIds.length;count++)
//			{
//				defaultid.addSelectOption(searchRuleIds[count].getId(),searchRuleIds[count].getValue('name'));
//			}
			
	

		//Get the Location-records whose make warehouse site is said to T
		location.addSelectOption('','');
		var filterLocation = new Array();
		filterLocation.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));
		var columnLocation = new Array();
		columnLocation[0] = new nlobjSearchColumn('name');
		var searchLocationRecord = nlapiSearchRecord('Location', null, filterLocation, columnLocation);
		for(var count=0;count < searchLocationRecord.length ;count++)
		{
			location.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('name'));

		}

		//Get the Company cust records whose status is active 
		var filterCompany = new Array();
		filterCompany.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		var columnCompany = new Array();
		columnCompany[0] = new nlobjSearchColumn('name');
		var searchLocationRecord = nlapiSearchRecord('customrecord_ebiznet_company', null, filterCompany, columnCompany);
		company.addSelectOption('','');
		for(var count1=0;count1 < searchLocationRecord.length ;count1++)
		{
			company.addSelectOption(searchLocationRecord[count1].getId(),searchLocationRecord[count1].getValue('name'));
		}
		
		var button = form.addSubmitButton('save');
		response.writePage(form);
	}
	else
	{
		nlapiLogExecution('ERROR','RULEVALUE',request.getLineItemValue('custpage_rules','custpage_rulevalue', 1));
		for(var i=1; i <= request.getLineItemCount('custpage_rules'); i++)
		{
			var newRecord = nlapiCreateRecord('customrecord_ebiznet_sysrules');
			newRecord.setFieldValue('custrecord_ebizsite',request.getLineItemValue('custpage_rules','custpage_location', i));
			newRecord.setFieldValue('custrecord_ebizcomp',request.getLineItemValue('custpage_rules','custpage_company', i));
			newRecord.setFieldValue('name',request.getLineItemValue('custpage_rules','custpage_internalid', i).toString());
			newRecord.setFieldValue('custrecord_ebizprocesstype',request.getLineItemValue('custpage_rules','custpage_processtype', i));
			newRecord.setFieldValue('custrecord_ebiztasktype',request.getLineItemValue('custpage_rules','custpage_tasktype', i));
			newRecord.setFieldValue('custrecord_ebizdescription',request.getLineItemValue('custpage_rules','custpage_description', i));
			newRecord.setFieldValue('custrecord_ebizruletype',request.getLineItemValue('custpage_rules','custpage_ruletype', i));
			newRecord.setFieldValue('custrecord_ebizrulevalue',request.getLineItemValue('custpage_rules','custpage_rulevaluetext', i).toString());
			newRecord.setFieldValue('custrecord_ebizruleval_refid',request.getLineItemValue('custpage_rules','custpage_rulevalue', i));
			nlapiSubmitRecord(newRecord);
		}
		response.sendRedirect('SUITELET', 'customscript_system_rules', 'customdeploy_system_rules', false,'');

	}
}