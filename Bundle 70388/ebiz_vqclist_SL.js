/***************************************************************************
	  		  eBizNET Solutions Inc .
****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
****************************************************************************
*
*     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_vqclist_SL.js,v $
*     	   $Revision: 1.1.14.1 $
*     	   $Date: 2013/09/11 12:18:16 $
*     	   $Author: rmukkera $
*
*   eBizNET version and checksum stamp.  Do not remove.
*   $eBiznet_VER: .............. $eBizNET_SUM: .....
* PRAMETERS
*
*
* DESCRIPTION
*
*  	Default Data for Interfaces
*
* NOTES AND WARNINGS
*
* INITATED FROM
*
* REVISION HISTORY
*
*****************************************************************************/
function ebiznet_vqclist_SL(request, response){
    var list = nlapiCreateList('QC List');
    list.setStyle('normal');
    //  list.setScript(239);    
    //list.addColumn('id', 'text', 'Transaction Id', 'LEFT');//.setDisplayType('hidden');
    list.addColumn('name', 'text', 'PO', 'LEFT');
    list.addColumn('custrecord_sku', 'text', 'Item', 'LEFT');
    list.addColumn('custrecord_sku_status', 'text', 'Item Status', 'LEFT');
    var column = list.addColumn('custrecord_lpno', 'text', 'LP #', 'LEFT');
    column.setURL(nlapiResolveURL('SUITELET', 'customscript_qcscreening', 'customdeploy_qcscreening'));
    column.addParamToURL('custparam_poid', 'name', true);
    column.addParamToURL('custparam_sku', 'custrecord_sku', true);
    column.addParamToURL('custparam_lp', 'custrecord_lpno', true);
    column.addParamToURL('custparam_internalid', 'id', true);
    column.addParamToURL('custparam_ebiz_sku_no', 'custrecord_ebiz_sku_no', true);
	column.addParamToURL('custparam_compid', 'custrecord_compid', true);
	column.addParamToURL('custparam_wmslocation', 'custrecord_wmslocation', true);
    
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
    filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [23]);
    
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('name');
    columns[1] = new nlobjSearchColumn('custrecord_sku');
    columns[2] = new nlobjSearchColumn('custrecord_sku_status');
    columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
    columns[4] = new nlobjSearchColumn('custrecord_lpno');
    columns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[6] = new nlobjSearchColumn('custrecord_comp_id');
	columns[7] = new nlobjSearchColumn('custrecord_wms_location');	
    
    var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
    var po = new Object();
    var dupliarray = new Array();
    var redarray = new Array();
    nlapiLogExecution('DEBUG', 'OK');
    
    for (var j = 0; searchresults != null && j < searchresults.length; j++) {
    
        nlapiLogExecution('DEBUG', 'OK2');
        po["name"] = searchresults[j].getValue('name');
        po["custrecord_sku"] = searchresults[j].getText('custrecord_sku');
        po["custrecord_sku_status"] = searchresults[j].getText('custrecord_sku_status');
        po["custrecord_lpno"] = searchresults[j].getValue('custrecord_lpno');
        po["id"] = searchresults[j].getId();
        po["custrecord_ebiz_sku_no"] = searchresults[j].getValue('custrecord_ebiz_sku_no');
		po["custrecord_compid"] = searchresults[j].getValue('custrecord_comp_id');
		po["custrecord_wmslocation"] = searchresults[j].getValue('custrecord_wms_location');
        list.addRow(po);
    }
    
    
    
    nlapiLogExecution('DEBUG', 'END OF LIST');
    
    response.writePage(list);
    
}
