/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_PutawayQuantityException.js,v $
<<<<<<< ebiz_RF_PutawayQuantityException.js
<<<<<<< ebiz_RF_PutawayQuantityException.js
<<<<<<< ebiz_RF_PutawayQuantityException.js
<<<<<<< ebiz_RF_PutawayQuantityException.js
<<<<<<< ebiz_RF_PutawayQuantityException.js
 *     	   $Revision: 1.4.2.12.4.8.2.26.2.12 $
 *     	   $Date: 2015/08/04 15:29:52 $
 *     	   $Author: skreddy $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_243 $
=======
 *     	   $Revision: 1.4.2.12.4.8.2.26.2.12 $
 *     	   $Date: 2015/08/04 15:29:52 $
=======
 *     	   $Revision: 1.4.2.12.4.8.2.26.2.12 $
 *     	   $Date: 2015/08/04 15:29:52 $
>>>>>>> 1.4.2.12.4.8.2.12
 *     	   $Author: skreddy $
=======
 *     	   $Revision: 1.4.2.12.4.8.2.26.2.12 $
 *     	   $Date: 2015/08/04 15:29:52 $
 *     	   $Author: skreddy $
>>>>>>> 1.4.2.12.4.8.2.13
=======
 *     	   $Revision: 1.4.2.12.4.8.2.26.2.12 $
 *     	   $Date: 2015/08/04 15:29:52 $
 *     	   $Author: skreddy $
>>>>>>> 1.4.2.12.4.8.2.14
=======
 *     	   $Revision: 1.4.2.12.4.8.2.26.2.12 $
 *     	   $Date: 2015/08/04 15:29:52 $
 *     	   $Author: skreddy $
>>>>>>> 1.4.2.12.4.8.2.16
 *     	   $Name: t_eBN_2015_1_StdBundle_1_243 $
>>>>>>> 1.4.2.12.4.8.2.11
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PutawayQuantityException.js,v $
 * Revision 1.4.2.12.4.8.2.26.2.12  2015/08/04 15:29:52  skreddy
 * Case# 201413766
 * 2015.2 compatibility issue fix
 *
 * Revision 1.4.2.12.4.8.2.26.2.11  2015/06/23 14:19:01  schepuri
 * case# 201413008
 *
 * Revision 1.4.2.12.4.8.2.26.2.10  2015/05/26 22:25:25  sponnaganti
 * Case# 201412924
 * We have issue while doing qty exception if entered qty is lessthan expected qty then Transaction order line is upadting with negative value.
 *
 * Revision 1.4.2.12.4.8.2.26.2.9  2015/04/20 13:27:14  schepuri
 * case# 201412360
 *
 * Revision 1.4.2.12.4.8.2.26.2.8  2015/02/03 13:45:51  schepuri
 * issue # 201411379
 *
 * Revision 1.4.2.12.4.8.2.26.2.7  2014/11/05 10:52:55  sponnaganti
 * Case# 201410939
 * True Fab SB Issue fixed
 *
 * Revision 1.4.2.12.4.8.2.26.2.6  2014/08/20 14:44:09  grao
 * Case#: 20149869  �Standard bundle  issue fixes
 *
 * Revision 1.4.2.12.4.8.2.26.2.5  2014/08/05 15:30:47  sponnaganti
 * Case# 20149823
 * Stnd Bundle Issue fix
 *
 * Revision 1.4.2.12.4.8.2.26.2.4  2014/07/25 15:27:36  nneelam
 * case#  20149707
 * New UI Issue Fix.
 *
 * Revision 1.4.2.12.4.8.2.26.2.3  2014/07/16 15:36:23  skavuri
 * Case# 20149237 Compatability Issue Fixed
 *
 * Revision 1.4.2.12.4.8.2.26.2.2  2014/07/04 14:18:34  skavuri
 * Case # 20149237 Compatibility Issue Fixed
 *
 * Revision 1.4.2.12.4.8.2.26.2.1  2014/06/27 11:32:10  sponnaganti
 * case# 20149037
 * Compatability Test Acc issue fix
 *
 * Revision 1.4.2.12.4.8.2.26  2014/06/13 06:25:54  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.2.12.4.8.2.25  2014/06/06 06:49:41  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.4.2.12.4.8.2.24  2014/06/04 15:07:52  sponnaganti
 * Case# 20148658  20147951
 * Stnd Bundle Issue fix
 *
 * Revision 1.4.2.12.4.8.2.23  2014/05/30 00:26:50  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.2.12.4.8.2.22  2014/04/25 09:59:52  grao
 * Case#: 20148125 Sonic Sb issue fixes
 *
 * Revision 1.4.2.12.4.8.2.21  2014/03/05 15:37:41  skavuri
 * Case # 20127470 issue fixed
 *
 * Revision 1.4.2.12.4.8.2.20  2014/03/04 15:38:36  skavuri
 * Case # 20127470 po status report issue fixed
 *
 * Revision 1.4.2.12.4.8.2.19  2014/03/04 14:02:44  nneelam
 * case#  20127471
 * Standard Bundle Issue Fix.
 *
 * Revision 1.4.2.12.4.8.2.18  2014/02/28 16:00:47  skavuri
 * case # 20127405 issue fixed
 *
 * Revision 1.4.2.12.4.8.2.17  2014/01/06 13:12:59  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.4.2.12.4.8.2.16  2013/12/26 15:23:35  rmukkera
 * Case # 20126444
 *
 * Revision 1.4.2.12.4.8.2.15  2013/12/23 14:09:08  schepuri
 * 20126444
 *
 * Revision 1.4.2.12.4.8.2.14  2013/11/09 01:56:32  gkalla
 * case#20125644
 * TO Qty exception failure
 *
 * Revision 1.4.2.12.4.8.2.13  2013/10/18 08:56:50  schepuri
 * 20125008
 *
 * Revision 1.4.2.12.4.8.2.12  2013/09/25 15:50:07  rmukkera
 * Case# 20124534
 *
 * Revision 1.4.2.12.4.8.2.11  2013/08/16 15:39:42  rmukkera
 * issue fix related to 20123249
 *
 * Revision 1.4.2.12.4.8.2.10  2013/08/07 16:01:44  skreddy
 * Case# 20123773
 * issue rellated to putwtasks for remaining qty
 *
 * Revision 1.4.2.12.4.8.2.9  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.2.12.4.8.2.8  2013/05/31 15:17:36  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.4.2.12.4.8.2.7  2013/05/23 07:34:44  grao
 * While Doing Qty Exception Reminaing Qty updated with Inbound Stanging in Inventory
 *
 * Revision 1.4.2.12.4.8.2.6  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.2.12.4.8.2.5  2013/04/10 15:48:31  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.4.2.12.4.8.2.4  2013/04/02 14:38:18  schepuri
 * tsg sb putaway qty exception
 *
 * Revision 1.4.2.12.4.8.2.3  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.4.2.12.4.8.2.2  2013/03/15 15:00:13  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.4.2.12.4.8.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.4.2.12.4.8  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.4.2.12.4.7  2012/12/24 15:19:27  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.4.2.12.4.6  2012/12/20 07:48:48  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.4.2.12.4.5  2012/12/12 08:53:51  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4.2.12.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4.2.12.4.3  2012/09/27 11:00:41  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.4.2.12.4.2  2012/09/24 22:45:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issues related to Expiry date is resolved.
 *
 * Revision 1.4.2.12.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.4.2.12  2012/09/03 13:45:29  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.4.2.11  2012/08/20 23:37:41  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix as a part of 339 bundle i.e.,
 * Driving user to scan next lp task when
 * user choose zero quantity(at actual qty) in Qty exception screen
 *
 * Revision 1.4.2.10  2012/08/16 12:47:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix as a part of 339 bundle i.e.,
 * Driving user to complete the putaway task if qty exception
 * is made for zero(actual qty).
 *
 * Revision 1.4.2.9  2012/08/13 05:07:43  skreddy
 * t_NSWMS_LOG201121_343
 * Added new Lp field
 *
 * Revision 1.4.2.8  2012/06/12 13:18:46  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.4.2.7  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4.2.6  2012/05/31 12:49:33  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.4.2.5  2012/05/24 15:23:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to wbc was resolved.
 *
 * Revision 1.4.2.4  2012/05/02 06:57:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Created new field i.e., RemainingQty.
 *
 * Revision 1.4.2.3  2012/04/24 15:02:40  spendyala
 * CASE201112/CR201113/LOG201121
 * when ever the user performs qty exception .need to create a rec with chkn and putaway with the remaining exception qty.
 *
 * Revision 1.4.2.2  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.2.1  2012/02/22 12:40:22  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4  2011/12/28 06:58:04  spendyala
 * CASE201112/CR201113/LOG201121
 * made changes for qty exception
 *
 * Revision 1.3  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PutawayQuantityException(request, response)
{
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');

	var user=context.getUser();

	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') 
	{
		//	Get the LP#, Quantity, Location 
		//  from the previous screen, which is passed as a parameter

		var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
		nlapiLogExecution('DEBUG', 'lpNumbersFromArray', TempLPNoArray);

		var getLPNo = request.getParameter('custparam_lpno');
		var getQuantity = request.getParameter('custparam_quantity');
		var getFetchedLocation = request.getParameter('custparam_location');
		var getFetchedItem = request.getParameter('custparam_item');
		var getFetchedItemDescription = request.getParameter('custparam_itemDescription');
		var EnteredLpNo = request.getParameter('custparam_enteredLPNo');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "PLACA"; 
			st2 = "INGRESAR LA CANTIDAD ACTUAL";			
			st3 = "RAZ&#211;N";
			st4 = "CANTIDAD RESTANTE";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";			


		}
		else
		{
			st0 = "";
			st1 = "LP"; 
			st2 = "ENTER ACTUAL QUANTITY";			
			st3 = "REASON";
			st4 = "REMAINING QUANTITY";
			st5 = "SEND";
			st6 = "PREV";			

		}




		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends

		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('enterquantity').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "<body>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " : <label>" + getLPNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterquantity' id='enterquantity' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " : ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterreason' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " : ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterremqty' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 +" : ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "				<td><input type='hidden' name='hdnlpNumbersArray' value=" + TempLPNoArray + "></td>";
		html = html + "				<td><input type='hidden' name='hdnitemdescription' value='" + getFetchedItemDescription + "'></td>";
		html = html + "				<input type='hidden' name='hdnenteredlp' value=" + EnteredLpNo + ">";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterquantity').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{

		// This variable is to hold the LP entered.
		var QuantityEntered = request.getParameter('enterquantity');
		nlapiLogExecution('DEBUG', 'QuantityEntered', QuantityEntered);

		var getReason = request.getParameter('enterreason');
		nlapiLogExecution('DEBUG', 'getReason', getReason);

		var getLPNo = request.getParameter('custparam_lpno');
		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);

		var RemainingQtyEntered=request.getParameter('enterremqty');
		nlapiLogExecution('DEBUG', 'RemainingQtyEntered', RemainingQtyEntered);
		//code added by santosh on 07Aug2012
		var NewLp=request.getParameter('enterlp');
		var LPReturnValue = "";
		//End of the code on 07Aug2012

		var tempRemainingQtyEntered=RemainingQtyEntered;
		if(tempRemainingQtyEntered==""||tempRemainingQtyEntered==null)
			tempRemainingQtyEntered=0;

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', 2);//2 STATUS.INBOUND.LOCATIONS_ASSIGNED
		if(getLPNo!=null&&getLPNo!='')
			filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lpno');
		columns[1] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[3] = new nlobjSearchColumn('custrecord_line_no');
		columns[4] = new nlobjSearchColumn('custrecord_comp_id');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_sku_status');
		columns[7] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[8] = new nlobjSearchColumn('custrecord_batch_no');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st7,st8,st9;
		if( getLanguage == 'es_ES')
		{
			st7 = "CANTIDAD INV&#193;LIDA";
			st8 = "LP NO V&#193;LIDO";
			st9 = "NO V&#193;LIDO cantidad restante";
		}
		else
		{
			st7 = "INVALID QUANTITY";
			st8 = "INVALID LP";
			st9 = "INVALID REMAINING QUANTITY";
		}



		POarray["custparam_error"] = st7;
		POarray["custparam_screenno"] = '10';
		POarray["custparam_option"] = request.getParameter('custparam_option');
		POarray["custparam_cartno"] = request.getParameter('custparam_cartno');
		POarray["custparam_beginlocation"] = request.getParameter('custparam_beginlocation');
		POarray["custparam_lpNumbersArray"] = request.getParameter('hdnlpNumbersArray');
		POarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		POarray["custparam_itemDescription"] = request.getParameter('hdnitemdescription');
		POarray["custparam_enteredLPNo"] = request.getParameter('hdnenteredlp');
		nlapiLogExecution('DEBUG','POArray',POarray["custparam_option"]);
		nlapiLogExecution('DEBUG','POArray',POarray["custparam_cartno"]);
		nlapiLogExecution('DEBUG','POArray',POarray["custparam_beginlocation"]);
		nlapiLogExecution('DEBUG','POArray tst custparam_itemDescription',POarray["custparam_itemDescription"]);

		//case # 20127471 start
		if(QuantityEntered<0){
			nlapiLogExecution('DEBUG','QuantityEntered',QuantityEntered);
			POarray["custparam_error"] = "ACT QTY CANNOT BE NEGATIVE";
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			return;
			//nlapiLogExecution('DEBUG', 'QuantityEntered is null ', QuantityEntered);
		}
		if(tempRemainingQtyEntered<0){
			POarray["custparam_error"] = "REM QTY CANNOT BE NEGATIVE";
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			return;
			//nlapiLogExecution('DEBUG', 'QuantityEntered is null ', QuantityEntered);
		}
		else if((tempRemainingQtyEntered=='' || tempRemainingQtyEntered==null) && (NewLp!="" && NewLp!=null))// case# 201411379�
		{
			POarray["custparam_error"] = 'Please Enter Remaining Quantity for NewLP';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			return;
		}
		//end

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		if (sessionobj!=context.getUser()) {
			try 
			{
				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

				//	if the previous button 'F7' is clicked, it has to go to the previous screen 
				//  ie., it has to go to accept PO #.
				POarray["custparam_exceptionqty"]=QuantityEntered;


				if (optedEvent == 'F7') 
				{
					nlapiLogExecution('DEBUG','POArray custparam_exceptionqty',POarray["custparam_exceptionqty"]);
					response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
					return;
				}
				else 
				{
					if(QuantityEntered != null && QuantityEntered !=""){
						if (searchresults != null && searchresults.length > 0) 
						{
							nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');

							var expectedQty=searchresults[0].getValue('custrecord_expe_qty');
							nlapiLogExecution('DEBUG','expectedQty',expectedQty);

//							var location = searchresults[0].getValue('custrecord_wms_location');
//							nlapiLogExecution('DEBUG','location',location);

//							var company = searchresults[0].getValue('custrecord_comp_id');
//							nlapiLogExecution('DEBUG','company',company);

							var vBatchNo = searchresults[0].getValue('custrecord_batch_no');
							nlapiLogExecution('DEBUG','vBatchNo',vBatchNo);

							var POId =searchresults[0].getValue('custrecord_ebiz_cntrl_no');
							nlapiLogExecution('DEBUG','POId',POId);

							var lineno =searchresults[0].getValue('custrecord_line_no');
							nlapiLogExecution('DEBUG','lineno',lineno);

							POarray["custparam_skustatus"] = searchresults[0].getValue('custrecord_sku_status');
							POarray["custparam_recordid"]= searchresults[0].getId();
							var trantype = nlapiLookupField('transaction', POId, 'recordType');
							var PORec = nlapiLoadRecord(trantype, POId);
							var vorglineno = lineno;
							if(trantype=='transferorder'){
								var PORec = nlapiLoadRecord(trantype, POId);

								var vTempLineNo=lineno;
								if(PORec != null && PORec != '')
								{
									var POItemLength= PORec.getLineItemCount('item');
									if(POItemLength != null && POItemLength != '')
									{
										for(var l=1;l<=POItemLength;l++)
										{
											if(PORec.getLineItemValue('item','line',l)==lineno)
											{
												vTempLineNo=l;
											}
										}
									}

								}
								lineno=vTempLineNo;
							}
							var POIdText =PORec.getFieldValue('tranid');
							nlapiLogExecution('DEBUG','POIdText',POIdText);

							var receiptTypeId=PORec.getFieldValue('custbody_nswmsporeceipttype');
							nlapiLogExecution('DEBUG','receiptTypeId',receiptTypeId);

							var itemid=PORec.getLineItemValue('item','item',lineno);
							nlapiLogExecution('DEBUG','itemid',itemid);

							var itemQuantity=PORec.getLineItemValue('item','quantity',lineno);
							nlapiLogExecution('DEBUG','itemQuantity',itemQuantity);

							if(trantype=="transferorder")
								var WmsLocation=PORec.getFieldValue('transferlocation');
							else
								var WmsLocation=PORec.getFieldValue('location');

							var itempackcode=PORec.getLineItemValue('item','custcol_nswmspackcode',lineno);
							nlapiLogExecution('DEBUG','itempackcode',itempackcode);

							if(itempackcode==null||itempackcode=="")
							{
								var filterItemDim=new Array();
								filterItemDim[0]=new nlobjSearchFilter('custrecord_ebizitemdims',null,'anyof',itemid);
								filterItemDim[1]=new nlobjSearchFilter('custrecord_ebizbaseuom',null,'is','T');

								var columnItemDim=new Array();
								columnItemDim[0]=new nlobjSearchColumn('custrecord_ebizpackcodeskudim');

								var rec=nlapiSearchRecord('customrecord_ebiznet_skudims',null,filterItemDim,columnItemDim);
								if(rec!=null&&rec!="")
									itempackcode=rec[0].getValue('custrecord_ebizpackcodeskudim');
							}

							var itemQuantityReceived=PORec.getLineItemValue('item','quantityreceived',lineno);
							nlapiLogExecution('DEBUG','itemQuantityReceived',itemQuantityReceived);
							//code added by santosh on 07Aug12
							if(NewLp!="" && NewLp!=null)
							{
								nlapiLogExecution('DEBUG', 'getsysItemLP', 'userdefined');
								LPReturnValue = ebiznet_LPRange_CL(NewLp, '2',WmsLocation,1);

								nlapiLogExecution('DEBUG', 'LPReturnValue', LPReturnValue);
								var lpExists = 'N';
								if(LPReturnValue== false)
								{
									nlapiLogExecution('DEBUG', 'LPReturnValue_Lpcheckin',LPReturnValue);
									POarray["custparam_error"] = 'INVALID LP';
									POarray["custparam_screenno"] = '10';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									return ;
								}
								else
								{
									try {
										nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
										nlapiLogExecution('DEBUG', 'poLoc',WmsLocation);
										var filtersmlp = new Array();
										filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', NewLp);
										filtersmlp[1] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', WmsLocation);
										var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

										if (SrchRecord != null && SrchRecord.length > 0) {
											nlapiLogExecution('DEBUG', 'LP FOUND');

											lpExists = 'Y';
										}
										else {
											nlapiLogExecution('DEBUG', 'LP NOT FOUND');
											var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
											customrecord.setFieldValue('name', NewLp);
											customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', NewLp);
											customrecord.setFieldValue('custrecord_ebiz_lpmaster_site', WmsLocation);
											var rec = nlapiSubmitRecord(customrecord, false, true);
										}

										if(lpExists=='Y')
										{
											nlapiLogExecution('DEBUG', 'lpExists',lpExists);
											POarray["custparam_error"] = 'INVALID LP';
											POarray["custparam_screenno"] = '10';
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											return ;
										}
									} 
									catch (e) {
										nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
									}
								}

							}				
							//end of the code on 07Aug 12	


							//if((QuantityEntered=='')||(isNaN(parseFloat(QuantityEntered))))
							if((QuantityEntered=='')||(QuantityEntered<0)||(isNaN(QuantityEntered)))
							{
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return;
							}
							else
							{

								//case # 20123773  start						
								if(RemainingQtyEntered==null || RemainingQtyEntered=='' || isNaN(RemainingQtyEntered))
									RemainingQtyEntered=0;	//end

								if(parseFloat(QuantityEntered) == parseFloat(RemainingQtyEntered) && parseFloat(RemainingQtyEntered) >= parseFloat(expectedQty))
								{
									POarray["custparam_error"] = 'INVALID QUANTITY';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									return;
								}
								if(parseFloat(QuantityEntered)>parseFloat(expectedQty))
								{
									var PoOveageQty=checkPOOverage(receiptTypeId);
									nlapiLogExecution('DEBUG','PoOveageQty',PoOveageQty);

									var ItemRemaininingQuantity = parseFloat(itemQuantity)- parseFloat(itemQuantityReceived);
									nlapiLogExecution('DEBUG','ItemRemaininingQuantity',ItemRemaininingQuantity);

									var recommendedQty=parseFloat(ItemRemaininingQuantity) + (parseFloat(PoOveageQty) * parseFloat(itemQuantity))/100;
									nlapiLogExecution('DEBUG','recommendedQty',recommendedQty);
									//case # 20127405 starts
									//if(parseFloat(QuantityEntered)<=parseFloat(expectedQty))

									if(parseFloat(QuantityEntered)<=parseFloat(recommendedQty))
										//case # 20127405 ends
									{
										var filterTransactionOrderLine=new Array();
										filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no',null,'equalto',POId));
										filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no',null,'equalto',lineno));
										filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_item',null,'anyof',itemid));


										var columnTransactionOrderLine=new Array();
										columnTransactionOrderLine[0]=new nlobjSearchColumn('custrecord_orderlinedetails_putexcep_qty');

										var searchTransactionOrderLineRec=nlapiSearchRecord('customrecord_ebiznet_order_line_details',null,filterTransactionOrderLine,columnTransactionOrderLine);

										if(searchTransactionOrderLineRec!=null&&searchTransactionOrderLineRec!='')
										{
											var previousQtyException=searchTransactionOrderLineRec[0].getValue('custrecord_orderlinedetails_putexcep_qty');


											if(previousQtyException==null||previousQtyException=='')
												previousQtyException=0;
											nlapiLogExecution('DEBUG','previousQtyException',previousQtyException);

											var qtyException=parseFloat(QuantityEntered)-parseFloat(expectedQty);
											nlapiLogExecution('DEBUG','qtyException',qtyException);

											var newQtyException=parseFloat(previousQtyException)+parseFloat(qtyException);

											var recId=searchTransactionOrderLineRec[0].getId();
											nlapiLogExecution('DEBUG','recId',recId);
											nlapiSubmitField('customrecord_ebiznet_order_line_details', recId, 'custrecord_orderlinedetails_putexcep_qty',newQtyException);
										}
										POarray["custparam_exceptionqty"]=QuantityEntered;
										POarray["custparam_qtyexceptionflag"]='true';
										POarray["custparam_remainingqty"]=parseFloat(QuantityEntered)-parseFloat(expectedQty);
										nlapiLogExecution('Error','POarray["custparam_remainingqty"]',POarray["custparam_remainingqty"]);
										nlapiLogExecution('DEBUG','if','chkpt');
										response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
										nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

									}
									else
									{
										nlapiLogExecution('DEBUG','else','chkpt');
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
									}
								}
								else if(parseFloat(QuantityEntered)<parseFloat(expectedQty))
								{
									var filterTransactionOrderLine=new Array();
									filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no',null,'equalto',POId));
									filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no',null,'equalto',lineno));
									filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_item',null,'anyof',itemid));

									var columnTransactionOrderLine=new Array();
									columnTransactionOrderLine[0]=new nlobjSearchColumn('custrecord_orderlinedetails_putexcep_qty');

									var searchTransactionOrderLineRec=nlapiSearchRecord('customrecord_ebiznet_order_line_details',null,filterTransactionOrderLine,columnTransactionOrderLine);

									if(searchTransactionOrderLineRec!=null&&searchTransactionOrderLineRec!='')
									{
										var previousQtyException=searchTransactionOrderLineRec[0].getValue('custrecord_orderlinedetails_putexcep_qty');

										if(previousQtyException==null||previousQtyException=='')
											previousQtyException=0;
										nlapiLogExecution('DEBUG','previousQtyException',previousQtyException);

										//var qtyException=parseFloat(QuantityEntered)-parseFloat(expectedQty);
										var qtyException=parseFloat(expectedQty)-parseFloat(QuantityEntered);
										nlapiLogExecution('DEBUG','qtyException',qtyException);

										var newQtyException=parseFloat(previousQtyException)+parseFloat(qtyException);

										var recId=searchTransactionOrderLineRec[0].getId();
										nlapiLogExecution('DEBUG','recId',recId);
										nlapiSubmitField('customrecord_ebiznet_order_line_details', recId, 'custrecord_orderlinedetails_putexcep_qty',parseFloat(newQtyException).toFixed(4));
									}

									POarray["custparam_exceptionqty"]=QuantityEntered;
									POarray["custparam_qtyexceptionflag"]='true';
									POarray["custparam_remainingqty"]=parseFloat(expectedQty)-parseFloat(QuantityEntered);
									nlapiLogExecution('DEBUG', 'POarray["custparam_remainingqty"]', POarray["custparam_remainingqty"]);
									try
									{
										var ActualBeginDate = DateStamp();
										var ActualBeginTime = TimeStamp();

										nlapiLogExecution('DEBUG', 'ActualBeginDate', ActualBeginDate);
										nlapiLogExecution('DEBUG', 'ActualBeginTime', ActualBeginTime);

										POarray["custparam_actualbegindate"] = ActualBeginDate;
										var TimeArray = new Array();
										TimeArray = ActualBeginTime.split(' ');

										var getActualBeginTime = TimeArray[0];
										var getActualBeginTimeAMPM = TimeArray[1];

										nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
										nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);
										var expqty=parseFloat(expectedQty)-parseFloat(QuantityEntered);
										var qtyreceived=0;
										var itemremqty=0;
										var containerLP="";

										var ItemInfo = eBiz_RF_GetItemCubeForItem(itemid);


										nlapiLogExecution('DEBUG','ITEMINFO[0]',ItemInfo[0]);
										nlapiLogExecution('DEBUG','ITEMINFO[1]',ItemInfo[1]);

										POarray["custparam_itemcube"]=ItemInfo[0];
										POarray["custparam_baseuomqty"]=ItemInfo[1];
										var TotQtyEntered=parseFloat(tempRemainingQtyEntered)+parseFloat(QuantityEntered);
										nlapiLogExecution('DEBUG','TotQtyEntered',TotQtyEntered);
										nlapiLogExecution('DEBUG','expectedQty',expectedQty);
										if(TotQtyEntered<=parseFloat(expectedQty))
										{
											updateTrnOrderLine(POId,vorglineno,expqty);
											updateChknTaskQty(getLPNo,QuantityEntered,POarray["custparam_recordid"],vBatchNo);
											if(RemainingQtyEntered!=""&&RemainingQtyEntered!=null)
												expqty=parseFloat(RemainingQtyEntered);
											nlapiLogExecution('DEBUG','expqty',expqty);
											if(parseFloat(RemainingQtyEntered)!=0)
											{
												//case# 20149823 (changed because need to update serialparentid in serial entry for remaining qty)
												//POarray["custparam_remainingqty"]=RemainingQtyEntered;
												POarray["custparam_remainingqtyserial"]=RemainingQtyEntered;
												//case# 20149823 ends
												var NewGenLP="";//added by santosh
												//code added by santosh on 07Aug12
												if(NewLp!=null && NewLp!="")
												{
													NewGenLP=NewLp;

												}
												else
												{
													var getsysItemLP = GetMaxLPNo(1, 1,WmsLocation);
													nlapiLogExecution('DEBUG', 'getsysItemLP LP now added iis', getsysItemLP);	

													var LPReturnValue = "";
													nlapiLogExecution('DEBUG', 'getsysItemLP', getsysItemLP);
													LPReturnValue = true;
													nlapiLogExecution('DEBUG', 'LP Return Value new', LPReturnValue);

													//LP Checking in masterlp record starts
													/*if (LPReturnValue == true) {
												try {
													nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
													var filtersmlp = new Array();
													filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getsysItemLP);

													var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

													if (SrchRecord != null && SrchRecord.length > 0) {
														nlapiLogExecution('DEBUG', 'LP FOUND');

														lpExists = 'Y';
													}
													else {
														nlapiLogExecution('DEBUG', 'LP NOT FOUND');
														var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
														customrecord.setFieldValue('name', getsysItemLP);
														customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getsysItemLP);
														var rec = nlapiSubmitRecord(customrecord, false, true);
													}
												} 
												catch (e) {
													nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
												}
											}*/

													getsysItemLP=CheckForLPExist(getsysItemLP,WmsLocation);
													nlapiLogExecution('DEBUG', 'getsysItemLP', getsysItemLP);
													NewGenLP=getsysItemLP;
												}
												//end of the code by 07Aug12
												NewLp = NewGenLP;
												nlapiLogExecution('DEBUG', 'NewLp back to newlp', NewLp);

												CheckinLp(itemid, POIdText,itemid, vorglineno,POId, expqty,itemremqty, 
														itempackcode,POarray["custparam_skustatus"],
														POarray["custparam_quantity"], qtyreceived,
														POarray["custparam_itemDescription"], POarray["custparam_itemcube"], 
														ActualBeginDate, getActualBeginTime, getActualBeginTimeAMPM,
														WmsLocation,containerLP,NewLp,vBatchNo,POarray);
											}
										}
										else
										{
											POarray["custparam_error"] = st9;
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
											return;
										}
									}
									catch(exp)
									{
										nlapiLogExecution('DEBUG','EXCEPTION in checkin lp',exp);
									}

									nlapiLogExecution('DEBUG','elseif','chkpt');


									var recid=searchresults[0].getId();

									var fields = new Array();
									var values = new Array();
									fields[0] = 'custrecord_expe_qty';
									values[0] = parseInt(QuantityEntered);


									var vputwrecid1=nlapiSubmitField('customrecord_ebiznet_trn_opentask', recid, fields, values);
									nlapiLogExecution('DEBUG', 'vputwrecid1', vputwrecid1);

									var itemSubtype = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
									var serialFlag = itemSubtype.custitem_ebizserialin;

									if(parseInt(QuantityEntered)==0)
									{
										//a Date object to be used for a random value
										var now = new Date();
										//now= now.getHours();
										//Getting time in hh:mm tt format.
										var a_p = "";
										var d = new Date();
										var curr_hour = now.getHours();
										if (curr_hour < 12) {
											a_p = "am";
										}
										else {
											a_p = "pm";
										}
										if (curr_hour == 0) {
											curr_hour = 12;
										}
										if (curr_hour > 12) {
											curr_hour = curr_hour - 12;
										}

										var curr_min = now.getMinutes();

										curr_min = curr_min + "";

										if (curr_min.length == 1) {
											curr_min = "0" + curr_min;
										}
										nlapiLogExecution("ERROR", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p));

										var currentUserID = getCurrentUser();
										nlapiLogExecution('DEBUG', 'currentUserID', currentUserID);

										var EndLocationId=searchresults[0].getValue('custrecord_actbeginloc');
										nlapiLogExecution('DEBUG', 'EndLocationId', EndLocationId);
										/*
										 * This block is to update the remaining cube for the bin location that was assigned
										 * by the system. 
										 */
										var vexpqty=parseFloat(expectedQty)-parseFloat(QuantityEntered);


										var itemDimensions = getSKUCubeAndWeight(itemid,"");
										var itemCube = itemDimensions[0];

										var TotalItemCube = parseFloat(itemCube) * parseFloat(vexpqty);
										nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );

										var oldBinLocationId = EndLocationId;
										var oldBinLocationRemainingCube =  GeteLocCube(oldBinLocationId);
										var actualRemainingCube = parseFloat(oldBinLocationRemainingCube) + parseFloat(TotalItemCube);

										nlapiLogExecution('DEBUG', 'oldBinLocationId ', oldBinLocationId );
										nlapiLogExecution('DEBUG', 'oldBinLocationRemainingCube ', oldBinLocationRemainingCube );
										nlapiLogExecution('DEBUG', 'actualRemainingCube ', actualRemainingCube );

										UpdateLocCube(oldBinLocationId, parseFloat(actualRemainingCube));
										//update the remaining loc cube.

										var recid=searchresults[0].getId();
										var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
										transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
										transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
										transaction.setFieldValue('custrecord_upd_date', DateStamp());
										transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
										transaction.setFieldValue('custrecord_act_end_date', DateStamp());
										transaction.setFieldValue('custrecord_wms_status_flag', 3);
										transaction.setFieldValue('custrecord_act_qty', parseFloat(QuantityEntered).toFixed(4));
										transaction.setFieldValue('custrecord_taskassignedto', currentUserID);
										var vputwrecid=nlapiSubmitRecord(transaction, false, true);
										nlapiLogExecution('DEBUG', 'vputwrecid', vputwrecid);

										//MoveTaskRecord(vputwrecid);
										/*
										 * 	This is to delete the check-in transactions from the inventory record while putaway confirmation. 
										 */						
										//case# 20149707
										DeleteInvtRecCreatedforCHKNTask(POId, getLPNo);
										response.sendRedirect('SUITELET', 'customscript_rf_putaway_complete', 'customdeploy_rf_putaway_complete_di', false, POarray);
										return;
									}
									else
									{
										POarray["custparam_itemqty"]=parseFloat(expectedQty); // case# 201413008

										//response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
										//POarray["custparam_polineitemlp"] = NewLp;
										nlapiLogExecution('DEBUG', 'NewGenLP in post before redirect', POarray["custparam_polineitemlp"]);
										if (itemSubtype.recordType == "serializedinventoryitem" || itemSubtype.recordType == "serializedassemblyitem" || serialFlag == 'T') 
										{
											response.sendRedirect('SUITELET', 'customscript_putawayqtyexp_serial_no', 'customdeploy_putawayqtyexp_serial_no', false, POarray);
											return;
										}
										else
										{
											response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
											return;
										}
									}
									nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
								}
								else if(parseFloat(QuantityEntered)==parseFloat(expectedQty))
								{
									POarray["custparam_error"] = 'INVALID QUANTITY';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									return;
								}
							}

//							var getLPId = searchresults[0].getId();
//							nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);

////							Load a record into a variable from opentask table for the LP# entered
//							var PORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getLPId);

//							POarray["custparam_lpno"] = PORec.getFieldValue('custrecord_lpno');
//							POarray["custparam_quantity"] = PORec.getFieldValue('custrecord_expe_qty');
//							POarray["custparam_location"] = PORec.getFieldValue('custrecord_actbeginloc');
//							POarray["custparam_item"] = PORec.getFieldValue('custrecord_sku');
//							POarray["custparam_itemDescription"] = PORec.getFieldValue('custrecord_skudesc');
						}

//						if (getQuantity != '') 
//						{
//						response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirm_loc', 'customdeploy_rf_putaway_confirm_loc_di', false, POarray);
//						nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
//						}
//						else 
//						{
//						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
//						nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
//						}
					}

					else {
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'QuantityEntered is null ', QuantityEntered);
					}
				}
			} 
			catch (e) 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
			}
			finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			POarray["custparam_screenno"] = '6';
			POarray["custparam_error"] = 'LP ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		}
	}
}

function updateChknTaskQty(getLPNo,QuantityEntered,putawayRecId,vBatchNo)
{
	try
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', 1);//1 STATUS.INBOUND.CHECK_IN 
		if(getLPNo!=null&&getLPNo!='')
			filters[1] = new nlobjSearchFilter('custrecord_ebiztask_lpno', null, 'is', getLPNo);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, null);
		if(searchresults!=null&&searchresults!="")
		{
			var RecId=searchresults[0].getId();
			var Id=nlapiSubmitField('customrecord_ebiznet_trn_ebiztask', RecId, 'custrecord_ebiztask_act_qty', parseFloat(QuantityEntered).toFixed(4));
			nlapiLogExecution('DEBUG','Chkn rec Id which is updated with new exp qty',Id);
		}
		if(putawayRecId!=null&&putawayRecId!="")
		{
			var fields=new Array();
			fields.push('custrecord_expe_qty');
			fields.push('custrecord_lotnowithquantity');

			var values=new Array();
			values.push(QuantityEntered);
			values.push(vBatchNo + "(" + QuantityEntered + ")");
			var vId=nlapiSubmitField('customrecord_ebiznet_trn_opentask', putawayRecId, fields, values);
			nlapiLogExecution('DEBUG','putawayRecId',vId);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in updateChknTaskQty',exp);	
	}

}


function checkPOOverage(receiptType){
	var poOverage = 0;

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('DEBUG','Out of check PO Overage', poOverage);
	return poOverage;
}


function CheckinLp(ItemId,POIdText,poitem,lineno,pointernalid,poqtyentered,poitemremainingqty,polinepackcode,
		polineitemstatus,polinequantity,polinequantityreceived,itemdescription,itemcube,
		ActualBeginDate,ActualBeginTime,ActualBeginTimeAMPM,WmsLocation,containerLP,NewLp,vBatchNo,POarray)
{

	//Case # 20126444 Start
	var stagelocid = getStagingLocation(WmsLocation);
	// Case # 20126444 End

	var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

	nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');

	/*var filtersAccount = new Array();
	if(WmsLocation!=null && WmsLocation!=""){
		filtersAccount.push(new nlobjSearchFilter('custrecord_location', null, 'anyof', WmsLocation));
	}*/

	var accountNumber = "";

	invtRec.setFieldValue('name', pointernalid);
	invtRec.setFieldValue('custrecord_ebiz_inv_binloc', stagelocid);
	invtRec.setFieldValue('custrecord_ebiz_inv_lp', NewLp);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', polineitemstatus);

	if(polinepackcode==null || polinepackcode=='')
		polinepackcode=1;

	invtRec.setFieldValue('custrecord_ebiz_inv_packcode', polinepackcode);
	invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(poqtyentered).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_inv_loc', WmsLocation);
	invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);

	invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');

	invtRec.setFieldValue('custrecord_wms_inv_status_flag','17');//17=FLAG.INVENTORY.INBOUND
	invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
	invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(poqtyentered).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdescription);
	invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
	invtRec.setFieldValue('custrecord_invttasktype', 1);
//	if(uomlevel!=null && uomlevel!='')
	//	invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);



	if(vBatchNo!=null && vBatchNo!=''&& vBatchNo!='null')
	{
		nlapiLogExecution('DEBUG', 'batchno into if', vBatchNo);
		try
		{

			var filterBatch=new Array();
			filterBatch[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',vBatchNo);
			//if(itemid!=null&&itemid!="")//Case# 20149237
			if(ItemId!=null && ItemId!="")
				filterBatch[1]=new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',ItemId);
			var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatch,null);
			var BatchID=rec[0].getId();

			invtRec.setFieldValue('custrecord_ebiz_inv_lot', BatchID);
			if(POarray["custparam_expdate"]!=null && POarray["custparam_expdate"]!='' && POarray["custparam_expdate"]!='undefined')
				invtRec.setFieldValue('custrecord_ebiz_expdate', POarray["custparam_expdate"]);
			if(POarray["custparam_fifodate"]!=null && POarray["custparam_fifodate"]!='' && POarray["custparam_fifodate"]!='undefined')
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', POarray["custparam_fifodate"]);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception', exp);
		}
	}


	if(pointernalid!=null && pointernalid!="")
		invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointernalid);

	var invtRecordId = nlapiSubmitRecord(invtRec, false, true);


	//end

	var lpExists = 'N';
	//var POarray=new Array();
	nlapiLogExecution('DEBUG','ItemId',ItemId);
	nlapiLogExecution('DEBUG','POIdText',POIdText);
	nlapiLogExecution('DEBUG','poitem',poitem);
	nlapiLogExecution('DEBUG','pointernalid',pointernalid);
	nlapiLogExecution('DEBUG','poqtyentered',poqtyentered);
	nlapiLogExecution('DEBUG','poitemremainingqty',poitemremainingqty);
	nlapiLogExecution('DEBUG','polinepackcode',polinepackcode);
	nlapiLogExecution('DEBUG','polineitemstatus',polineitemstatus);
	nlapiLogExecution('DEBUG','polinequantity',polinequantity);
	nlapiLogExecution('DEBUG','polinequantityreceived',polinequantityreceived);
	nlapiLogExecution('DEBUG','itemdescription',itemdescription);
	nlapiLogExecution('DEBUG','itemcube',itemcube);
	nlapiLogExecution('DEBUG','ActualBeginDate',ActualBeginDate);
	nlapiLogExecution('DEBUG','ActualBeginTime',ActualBeginTime);
	nlapiLogExecution('DEBUG','ActualBeginTimeAMPM',ActualBeginTimeAMPM);
	nlapiLogExecution('DEBUG','WmsLocation',WmsLocation);
	nlapiLogExecution('DEBUG','vBatchNo',vBatchNo);

	var getPONo = POIdText;
	var getPOItem = poitem;
	var getPOLineNo = lineno;

	var getPOInternalId = pointernalid;
	var getPOQtyEntered = poqtyentered;
	var getPOItemRemainingQty = poitemremainingqty;
	var getPOLinePackCode = polinepackcode;
	var getPOLineItemStatus = polineitemstatus;
	var getPOLineQuantity = polinequantity;
	var getPOLineQuantityReceived = polinequantityreceived;
	var getItemDescription = itemdescription;
	var getItemCube = itemcube;
	var getActualBeginDate = ActualBeginDate;
	var getFetchedItemId =ItemId;
	var getActualBeginTime = ActualBeginTime;
	var getActualBeginTimeAMPM = ActualBeginTimeAMPM;

	var getLineCount = 1;
	var getBaseUOM = 'EACH';
	var getBinLocation = "";

//	POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
	POarray["custparam_batchno"] =vBatchNo;
	POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
	POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
	POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
	POarray["custparam_lastdate"] = request.getParameter('hdnLastAvlDate');
	POarray["custparam_fifodate"] = request.getParameter('hdnFifoDate');
	POarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');
	POarray["custparam_cartlp"] = request.getParameter('custparam_cartlp');



	var POfilters = new Array();
	nlapiLogExecution('DEBUG', 'getPOInternalId', pointernalid);
	POfilters[0] = new nlobjSearchFilter('name', null, 'is', pointernalid);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, POfilters, null);

	var getPOReceiptNo = '';

	if (searchresults != null && searchresults.length > 0) {
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			getPOReceiptNo = searchresults[i].getValue('custrecord_ebiz_poreceipt_receiptno');
		}

		nlapiLogExecution('DEBUG', 'PO Receipt No', getPOReceiptNo);
	}



	if(lpExists != 'Y')
	{
		var itemSubtype = nlapiLookupField('item', ItemId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
		POarray["custparam_recordtype"] = itemSubtype.recordType;
		var batchflag=itemSubtype.custitem_ebizbatchlot;
		nlapiLogExecution('DEBUG', 'itemSubtype.recordType =', itemSubtype.recordType);
		nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
		nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);

		//POarray["custparam_polineitemlp"] = getsysItemLP;
		//POarray["custparam_polineitemlp"] = NewGenLP;//added by santosh on 07Aug12
		POarray["custparam_polineitemlp"] = NewLp;//added by santosh on 07Aug12
		POarray["custparam_poreceiptno"] = getPOReceiptNo;
		var TotalItemCube = parseFloat(itemcube) * parseFloat(poqtyentered);
		nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );


		var priorityPutawayLocnArr = priorityPutaway(getFetchedItemId, getPOQtyEntered,WmsLocation,getPOLineItemStatus);

		var priorityRemainingQty = priorityPutawayLocnArr[0];
		var priorityQty = priorityPutawayLocnArr[1];
		var priorityLocnID = priorityPutawayLocnArr[2];		
		var putmethod,putrule;
		nlapiLogExecution('DEBUG', 'priorityQty ', priorityQty );
		nlapiLogExecution('DEBUG', 'getPOQtyEntered', getPOQtyEntered);
		nlapiLogExecution('DEBUG', 'priorityLocnID', priorityLocnID);

		if (priorityQty >= getPOQtyEntered){			
			taskType = "2"; 
			wmsStatusFlag = "2"; 
			getBeginLocationId = priorityLocnID;
			var LocRemCube = GeteLocCube(getBeginLocationId);

			nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
			if (parseFloat(LocRemCube) > parseFloat(TotalItemCube)) 
				remainingCube = parseFloat(LocRemCube) - parseFloat(TotalItemCube);

		}

		if(priorityQty < getPOQtyEntered)
		{

			var filters2 = new Array();

			var columns2 = new Array();
			columns2[0] = new nlobjSearchColumn('custitem_item_family');
			columns2[1] = new nlobjSearchColumn('custitem_item_group');
			columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
			columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
			columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
			columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
			columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');
			filters2.push(new nlobjSearchFilter('internalid', null, 'is', getFetchedItemId));
			var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

			var filters3=new Array();
			filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			var columns3=new Array();
			columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
			columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
			columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
			columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
			columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
			columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
			columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
			columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
			columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
			columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
			columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
			columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
			columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
			columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
			columns3[14] = new nlobjSearchColumn('formulanumeric');
			columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
			columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
			//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
			columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
			// Upto here on 29OCT2013 - Case # 20124515
			columns3[14].setSort();

			var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);

			nlapiLogExecution('Debug', 'Time Stamp at the start of GetPutawayLocationNew',TimeStampinSec());
			getBeginLocation = GetPutawayLocationNew(ItemId, polinepackcode, polineitemstatus, getBaseUOM, parseFloat(TotalItemCube),itemSubtype.recordType,WmsLocation,ItemInfoResults,putrulesearchresults);// case# 201412360
			nlapiLogExecution('Debug', 'Time Stamp at the end of GetPutawayLocationNew',TimeStampinSec());

			//var getBeginLocation = generatePutawayLocation(ItemId, polinepackcode,
			//		polineitemstatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType,WmsLocation);


			nlapiLogExecution('DEBUG','getBeginLocation',getBeginLocation);


			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage 2 ',context.getRemainingUsage());
			nlapiLogExecution('DEBUG', 'Begin Location', getBeginLocation);

			var vLocationname="";
			if (getBeginLocation != null && getBeginLocation != '') {
				var getBeginLocationId = getBeginLocation[2];
				vLocationname= getBeginLocation[0];
				nlapiLogExecution('DEBUG', 'Begin Location Id', getBeginLocationId);
			}
			else {
				var getBeginLocationId = "";
				nlapiLogExecution('DEBUG', 'Begin Location Id is null', getBeginLocationId);
			}

			if(getBeginLocation!=null && getBeginLocation != '')
				var remainingCube = getBeginLocation[1];

			nlapiLogExecution('DEBUG', 'remainingCube', remainingCube);
			if (getBeginLocation != null && getBeginLocation != '') {
				putmethod = getBeginLocation[9];
				putrule = getBeginLocation[10];
			}
			else
			{
				putmethod = "";
				putrule = "";
			}
		}
		var getActualEndDate = DateStamp();
		nlapiLogExecution('DEBUG', 'getActualEndDate', getActualEndDate);
		var getActualEndTime = TimeStamp();
		nlapiLogExecution('DEBUG', 'getActualEndTime', getActualEndTime);

		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','Remaining usage 3 ',context.getRemainingUsage());
		var trantype= nlapiLookupField('transaction',pointernalid,'recordType');
		TrnLineUpdation(trantype, 'CHKN', POIdText,pointernalid,lineno, ItemId, 
				polinequantity, poqtyentered,"",polineitemstatus);

		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','Remaining usage 4 ',context.getRemainingUsage());
		var vValid = custChknPutwRecCreation(getPOInternalId, getPONo, getPOQtyEntered, getPOLineNo, getPOItemRemainingQty, getLineCount, getFetchedItemId, 
				getPOItem, getItemDescription, getPOLineItemStatus, getPOLinePackCode, getBaseUOM, NewLp, getBinLocation, 
				getPOLineQuantityReceived, getActualEndDate, getActualEndTime, getPOReceiptNo, "", getActualBeginDate, 
				getActualBeginTime, getActualBeginTimeAMPM, getBeginLocationId, POarray["custparam_batchno"], 
				POarray["custparam_mfgdate"], POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
				POarray["custparam_lastdate"], POarray["custparam_fifodate"], POarray["custparam_fifocode"], 
				itemSubtype.recordType, containerLP, WmsLocation,batchflag,putmethod,putrule);
		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','Remaining usage 5 ',context.getRemainingUsage());
		nlapiLogExecution('DEBUG', 'vValid', vValid);
		if(vValid==false)
		{
			nlapiLogExecution('DEBUG','Exception in generating Locations');
		}
		else
		{


			/*	* This is to insert a record in inventory custom record.
			 * The data that is to be inserted is PO Internal Id, Item, Item Status, Pack Code, Quantity, LP#
			 * The parameters list is: 
			 * pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, warehouse location
			 * */

			TrnLineUpdation(trantype, 'ASPW', POIdText,pointernalid,lineno, ItemId, 
					polinequantity, poqtyentered,"",polineitemstatus);

			updatePutQtyExc(pointernalid,lineno);
			if(getBeginLocationId!=null && getBeginLocationId!='')
			{
				UpdateLocCube(getBeginLocationId, remainingCube);
			}
		}
	}
	else
	{
		nlapiLogExecution('DEBUG','exception :LP ALEADY Exist.so chkn and putaway task records are failed to create');
	}
}

function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, 
		ItemDesc, ItemStatus, PackCode, BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, 
		poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, ActualBeginTimeAMPM, BeginLocationId, 
		BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, itemrectype, cartlp, 
		WHLocation,batchflag,putmethod,putrule)
{
	var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
	var columns = nlapiLookupField('item', ItemId, fields);
	var ItemType = columns.recordType;					
	var batchflg = columns.custitem_ebizbatchlot;
	var itemfamId= columns.custitem_item_family;
	var itemgrpId= columns.custitem_item_group;
	var getlotnoid="";

//	if(ItemStatus==null || ItemStatus=='')
//	ItemStatus='12';

//	nlapiLogExecution('DEBUG', 'putLotBatch', putLotBatch);

	var vValid=true;
	nlapiLogExecution('DEBUG', 'Into custChknPutwRecCreation', 'custChknPutwRecCreation');
	var now = new Date();
	var stagelocid, docklocid;
	try
	{
		stagelocid = GetInboundStageLocation(WHLocation, null, 'INB');
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in  GetInboundStageLocation', exp);
	}
//	Case # 20126444 Start
	if(stagelocid==null || stagelocid=='')
	stagelocid = getStagingLocation(WHLocation);
	//Case # 20126444 End
	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);
	if(stagelocid!=null && stagelocid!='')
		docklocid = stagelocid;
	if(docklocid==null || docklocid=='')
	docklocid = getDockLocation(WHLocation);
	nlapiLogExecution('DEBUG', 'Dock Location', docklocid);

	if (BeginLocationId != null && BeginLocationId != '' ) //Creating custom record with CHKN and PUTW task,
	{
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);

		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		if(po != null && po != '')
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);
		nlapiLogExecution('DEBUG', 'Submitting CHKN record', 'TRN_OPENTASK');

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		//commit the record to NetSuite
		var reccheckid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		nlapiLogExecution('DEBUG', 'BeginLocationId', BeginLocationId);

		//create the openTask record With PUTW Task Type
		if(BeginLocationId==null || BeginLocationId=="")
			return false;
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)

		nlapiLogExecution('DEBUG', 'Fetched Begin Location', BeginLocation);

		putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//For eBiznet Receipt Number .
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//putwrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		if (BeginLocationId != "") {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 2);
		}
		else {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		}
		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		if(po != null && po != '')
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);


		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);
		/*try
		{
			MoveTaskRecord(reccheckid);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in  MoveTaskRecord');
		}*/
		nlapiLogExecution('DEBUG', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');
		return true;
	}//end if binloc is not null
	else {
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);

		nlapiLogExecution('DEBUG', 'custrecord_ebiz_sku_no tesing', 'ItemId');

		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		if(po != null && po != '')
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{

			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var reccheckid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" ||itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");

		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		if(po != null && po != '')
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		/*try
		{
			MoveTaskRecord(reccheckid);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in  MoveTaskRecord');
		}*/
		nlapiLogExecution('DEBUG', 'Done PUTW Record Insertion :', 'Success');
		return false;
	}
}
//Case # 20126444 Start
function getStagingLocation(WHLocation){
//	Case # 20126444 End
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));
	// case no 20126444
	var RoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR', 'RoleLocation', RoleLocation);
	if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
	{
//		stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', RoleLocation));
		if(WHLocation!=null && WHLocation!='')
			stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', WHLocation));
	}


	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}
function getDockLocation(site){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));
	if(site!=null && site!="")
		dockFilters.push(new nlobjSearchFilter('custrecord_ebizsiteloc', 'custrecord_inboundlocgroupid', 'anyof', ["NONE",site]));
	dockFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}

function updateTrnOrderLine(poid,lineno,expQty)
{
	try{
		nlapiLogExecution('DEBUG','poid',poid);
		nlapiLogExecution('DEBUG','lineno',lineno);
		nlapiLogExecution('DEBUG','expQty',expQty);
		var chkQty=0;
		var PwtQty=0;
		var RecId;	
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poid));
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
		columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');

		var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);

		if(searchResults!=null&&searchResults!="")
		{
			chkQty=searchResults[0].getValue('custrecord_orderlinedetails_checkin_qty');
			PwtQty=searchResults[0].getValue('custrecord_orderlinedetails_putgen_qty');
			RecId=searchResults[0].getId();
			nlapiLogExecution('DEBUG','chkQty',chkQty);
			nlapiLogExecution('DEBUG','PwtQty',PwtQty);
			chkQty=parseFloat(chkQty)-parseFloat(expQty);
			PwtQty=parseFloat(PwtQty)-parseFloat(expQty);
			nlapiLogExecution('DEBUG','chkQty AFTER',chkQty);
			nlapiLogExecution('DEBUG','PwtQty AFTER',PwtQty);
			var Fields=new Array();
			Fields[0]='custrecord_orderlinedetails_checkin_qty';
			Fields[1]='custrecord_orderlinedetails_putgen_qty';
			Fields[2]='custrecord_orderlinedetails_putexcep_qty';

			var Values=new Array();
			Values[0]=parseFloat(chkQty).toFixed(4);
			Values[1]=parseFloat(PwtQty).toFixed(4);
			Values[2]=parseFloat(expQty).toFixed(4);
			var ID=nlapiSubmitField('customrecord_ebiznet_order_line_details', RecId, Fields, Values);
			nlapiLogExecution('DEBUG','Trn Order Line RecID',ID);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in UpdateTrnOrderLine function',exp);
	}
}

function updatePutQtyExc(poid,lineno)
{
	try{
		nlapiLogExecution('DEBUG','poid',poid);
		nlapiLogExecution('DEBUG','lineno',lineno);


		var RecId;	
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poid));
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));

		var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, null);

		if(searchResults!=null&&searchResults!="")
		{
			RecId=searchResults[0].getId();
			var ID=nlapiSubmitField('customrecord_ebiznet_order_line_details', RecId, 'custrecord_orderlinedetails_putexcep_qty', '');
			nlapiLogExecution('DEBUG','Trn Order Line RecID',ID);
		}
	}
	catch(exp)
	{

		nlapiLogExecution('DEBUG','Exception in UpdateTrnOrderLine function',exp);
	}
}


function CheckForLPExist(getsysItemLP,WmsLocation)
{
	try {
		nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getsysItemLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) {
			nlapiLogExecution('DEBUG', 'LP FOUND');

			getsysItemLP = GetMaxLPNo(1, 1,WmsLocation);
			nlapiLogExecution('DEBUG', 'getsysItemLP LP now added iis', getsysItemLP);	
			CheckForLPExist(getsysItemLP,WmsLocation);
		}
		else {
			nlapiLogExecution('DEBUG', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', getsysItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getsysItemLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
		}
	} 
	catch (e) {
		nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
	}
	return getsysItemLP;
}

/**
 * @param pointid
 * @param lp
 */
function DeleteInvtRecCreatedforCHKNTask(pointid, lp)
{
	nlapiLogExecution('DEBUG','Inside DeleteInvtRecCreatedforCHKNTask ','Funciton');
	var Ifilters = new Array();
	//Ifilters.push(new nlobjSearchFilter('name', null, 'is', pointid));
	Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [1,17]));
	Ifilters.push(new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [1]));
	Ifilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp));

	var invtId="";
	var invtType="";
	var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (serchInvtRec) 
	{
		for (var s = 0; s < serchInvtRec.length; s++) {
			nlapiLogExecution('DEBUG','Inside loop','loop');
			var searchresult = serchInvtRec[ s ];
			nlapiLogExecution('DEBUG','need to print this','print');
			invtId = serchInvtRec[s].getId();
			invtType= serchInvtRec[s].getRecordType();
			nlapiLogExecution('DEBUG','CHKN INVT Id',invtId);
			nlapiLogExecution('DEBUG','CHKN invtType ',invtType);
			nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
			nlapiLogExecution('DEBUG','Invt Deleted record Id',invtId);

		}
	}
}
function GetInboundStageLocation(vSite, vCompany, stgDirection){

	nlapiLogExecution('DEBUG', 'Into GetInboundStageLocation');

	var str = 'vSite. = ' + vSite + '<br>';
	str = str + 'vCompany. = ' + vCompany + '<br>';	
	str = str + 'stgDirection. = ' + stgDirection + '<br>';

	nlapiLogExecution('DEBUG', 'GetInboundStageLocation Parameters', str);

	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}


	var columns = new Array();
	var filters = new Array();

	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}

	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < searchresults.length; i++) {

			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			var vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');

			var str = 'Stage Location ID. = ' + vstgLocation + '<br>';
			str = str + 'Stage Location Text. = ' + vstgLocationText + '<br>';	

			nlapiLogExecution('DEBUG', 'Stage Location Details', str);

			if (vstgLocation != null && vstgLocation != "") {
				nlapiLogExecution('DEBUG', 'Out of GetInboundStageLocation',vstgLocation);
				return vstgLocation;
			}

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('DEBUG', 'LocGroup ', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				filtersLocGroup.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('DEBUG', 'searchresultsloc ', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {

					var vLocid=searchresultsloc[0].getId();
					nlapiLogExecution('DEBUG', 'Out of GetInboundStageLocation',vLocid);
					return vLocid;

				}				
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of GetInboundStageLocation');

}