/***************************************************************************
	  		   eBizNET Solutions
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Common/Suitelet/InventoryGeneralFunctions.js,v $
 *     	   $Revision: 1.16.2.33.4.15.2.76.2.6 $
 *     	   $Date: 2015/12/02 15:07:49 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: InventoryGeneralFunctions.js,v $
 * Revision 1.16.2.33.4.15.2.76.2.6  2015/12/02 15:07:49  grao
 * 2015.2 Issue Fixes 201415845
 *
 * Revision 1.16.2.33.4.15.2.76.2.5  2015/12/01 15:29:08  grao
 * 2015.2 Issue Fixes 20145825
 *
 * Revision 1.16.2.33.4.15.2.76.2.4  2015/11/30 17:12:26  nneelam
 * case# 201415827 , 201415823
 *
 * Revision 1.16.2.33.4.15.2.76.2.3  2015/11/27 13:23:06  schepuri
 * case# 201415868
 *
 * Revision 1.16.2.33.4.15.2.76.2.2  2015/11/05 23:09:18  sponnaganti
 * case# 201415424
 * Issue is in pick strategy zone is not configured because of this while getting zone loc groups we are getting error.
 *
 * Revision 1.16.2.33.4.15.2.76.2.1  2015/11/03 23:36:31  rrpulicherla
 * FO changes changes
 *
 * Revision 1.16.2.33.4.15.2.76  2015/09/02 07:13:45  schepuri
 * case# 201414131
 *
 * Revision 1.16.2.33.4.15.2.75  2015/08/25 10:19:13  schepuri
 * case# 201413936
 *
 * Revision 1.16.2.33.4.15.2.74  2015/07/17 13:26:04  schepuri
 * case# 201413535
 *
 * Revision 1.16.2.33.4.15.2.73  2015/06/04 06:30:51  schepuri
 * case# 201412851
 *
 * Revision 1.16.2.33.4.15.2.72  2015/05/13 13:41:24  schepuri
 * case# 201412729
 *
 * Revision 1.16.2.33.4.15.2.71  2015/04/22 09:56:03  skreddy
 * Case# 201412458
 * standard bundle issue fix
 *
 * Revision 1.16.2.33.4.15.2.70  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.16.2.33.4.15.2.69  2015/04/08 13:55:46  schepuri
 * case# 201412224
 *
 * Revision 1.16.2.33.4.15.2.68  2015/04/06 11:59:43  gkalla
 * Case# 201411877,201412181
 * TF replenishment with inactive bin location and  Nautilus Pick reversal issue
 *
 * Revision 1.16.2.33.4.15.2.67  2015/03/19 15:52:35  sponnaganti
 * Case# 201412130
 * LP SB  issue fix
 *
 * Revision 1.16.2.33.4.15.2.66  2015/03/18 15:39:57  sponnaganti
 * Case# 201412069
 * LP SB issue fix
 *
 * Revision 1.16.2.33.4.15.2.65  2015/01/23 07:32:27  snimmakayala
 * no message
 *
 * Revision 1.16.2.33.4.15.2.64  2015/01/02 15:05:53  skreddy
 * Case# 201411349
 * Two step replen process
 *
 * Revision 1.16.2.33.4.15.2.63  2014/09/19 14:00:27  nneelam
 * case# 201410462
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.16.2.33.4.15.2.62  2014/09/16 14:52:03  skreddy
 * case # 201410327/201410328
 * TPP SB issue fix
 *
 * Revision 1.16.2.33.4.15.2.61  2014/09/12 13:14:04  grao
 * Case#: 201410311 Dc detnal sb  issue fixes
 *
 * Revision 1.16.2.33.4.15.2.60  2014/09/11 15:47:23  skavuri
 * Case# 20149789 Std Bundle issue fixed
 *
 * Revision 1.16.2.33.4.15.2.59  2014/09/10 17:58:48  snimmakayala
 * Case: 201410288
 * Pick Strategies by Order Priority
 *
 * Revision 1.16.2.33.4.15.2.58  2014/09/01 16:22:48  gkalla
 * case#201220815
 * ACE replen issue
 *
 * Revision 1.16.2.33.4.15.2.57  2014/08/23 01:11:24  gkalla
 * case#201220815
 * Changed the code to generate
 * replen for item status matched inventory only
 *
 * Revision 1.16.2.33.4.15.2.56  2014/08/13 09:23:08  gkalla
 * case#20149227
 * JB-Cycle Count - Even though the Cycle Count is in process user should be able to pick the product
 *
 * Revision 1.16.2.33.4.15.2.55  2014/08/08 16:54:25  grao
 * Case#: 20149889 LL issue fixes
 *
 * Revision 1.16.2.33.4.15.2.54  2014/08/05 12:25:22  snimmakayala
 * Case: 20149826
 * Performance fine tune
 *
 * Revision 1.16.2.33.4.15.2.53  2014/08/05 12:08:29  snimmakayala
 * Case: 20149826
 * Performance fine tune
 *
 * Revision 1.16.2.33.4.15.2.52  2014/07/10 15:26:45  skavuri
 * Case# 20149355 Compatibility Issue Fixed
 *
 * Revision 1.16.2.33.4.15.2.51  2014/06/27 10:03:22  sponnaganti
 * case# 20148980
 * New UI Compatability issue fix
 *
 * Revision 1.16.2.33.4.15.2.50  2014/06/16 07:05:15  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148854
 *
 * Revision 1.16.2.33.4.15.2.49  2014/06/06 12:18:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.16.2.33.4.15.2.48  2014/06/04 15:27:19  nneelam
 * case#  20148643
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.16.2.33.4.15.2.47  2014/06/03 16:07:41  gkalla
 * case#20148553
 * nuCourse over allocation from pick face issue fix
 *
 * Revision 1.16.2.33.4.15.2.46  2014/05/27 07:37:00  snimmakayala
 * Case#: 20148135
 * Schedule Script Status
 *
 * Revision 1.16.2.33.4.15.2.45  2014/05/26 15:02:32  skreddy
 * case # 20148479
 * Sonic SB issue fix
 *
 * Revision 1.16.2.33.4.15.2.44  2014/05/23 06:48:03  gkalla
 * case#20148502
 * SportsHQ issue Outbound reversal pack code is not updating in create inventory
 *
 * Revision 1.16.2.33.4.15.2.43  2014/04/29 06:02:40  skreddy
 * case # 20148179
 * Sonic SB  issue fix
 *
 * Revision 1.16.2.33.4.15.2.42  2014/04/16 15:33:10  nneelam
 * case#  20148032
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.16.2.33.4.15.2.41  2014/04/15 13:07:43  snimmakayala
 * Case #: 201219227
 *
 * Revision 1.16.2.33.4.15.2.40  2014/03/26 14:38:39  rmukkera
 * Case # 20127751
 *
 * Revision 1.16.2.33.4.15.2.39  2014/02/17 15:32:54  nneelam
 * case#  20127190
 * Standard Bundle Issue Fix.
 *
 * Revision 1.16.2.33.4.15.2.38  2014/02/05 07:11:06  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201218314
 *
 * Revision 1.16.2.33.4.15.2.37  2014/01/31 15:42:18  gkalla
 * case#201218314ï¿½
 * Sports HQ for pick gen if bin location mapped to the pick rule
 *
 * Revision 1.16.2.33.4.15.2.36  2014/01/28 15:05:56  rmukkera
 * Case # 20126895ï¿½,20126896
 *
 * Revision 1.16.2.33.4.15.2.35  2014/01/24 14:37:23  rmukkera
 * Case # 20126884
 *
 * Revision 1.16.2.33.4.15.2.34  2014/01/22 14:40:11  rmukkera
 * Case # 20126895ï¿½,20126896ï¿½
 *
 * Revision 1.16.2.33.4.15.2.33  2014/01/22 14:33:06  rmukkera
 * Case # 20126895ï¿½,20126896ï¿½
 *
 * Revision 1.16.2.33.4.15.2.32  2014/01/17 14:14:20  schepuri
 * 20126835
 * standard bundle issue fix
 *
 * Revision 1.16.2.33.4.15.2.31  2013/12/12 15:34:09  gkalla
 * case#20126328
 * MHP over allocation of replen task
 *
 * Revision 1.16.2.33.4.15.2.30  2013/12/11 22:08:50  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20125991
 *
 * Revision 1.16.2.33.4.15.2.29  2013/12/05 11:51:02  snimmakayala
 * no message
 *
 * Revision 1.16.2.33.4.15.2.28  2013/12/05 06:39:38  snimmakayala
 * Case# : 20125973
 * MHP UAT Fixes.
 *
 * Revision 1.16.2.33.4.15.2.27  2013/11/28 06:07:51  skreddy
 * Case# 20125987
 * Afosa SB issue fix
 *
 * Revision 1.16.2.33.4.15.2.26  2013/11/27 14:09:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * prodissue fixes
 *
 * Revision 1.16.2.33.4.15.2.25  2013/11/22 14:41:41  grao
 * Case# 20125876   related issue fixes in SB 2014.1
 *
 * Revision 1.16.2.33.4.15.2.24  2013/11/08 15:36:06  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * SportsHQ issue fixes
 *
 * Revision 1.16.2.33.4.15.2.23  2013/11/01 13:40:08  schepuri
 * 20125361
 *
 * Revision 1.16.2.33.4.15.2.22  2013/10/31 17:04:36  gkalla
 * case#20125403
 * for outbound reversal item fulfillment not deducting
 *
 * Revision 1.16.2.33.4.15.2.21  2013/10/25 20:06:10  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20125294
 *
 * Revision 1.16.2.33.4.15.2.20  2013/10/22 14:08:20  schepuri
 * 20125228
 *
 * Revision 1.16.2.33.4.15.2.19  2013/10/08 15:47:41  rmukkera
 * Case# 20124808
 *
 * Revision 1.16.2.33.4.15.2.18  2013/09/17 16:04:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * standardbundlechanges
 * case#20124221
 *
 * Revision 1.16.2.33.4.15.2.17  2013/09/05 15:40:44  skreddy
 * Case# 20124258
 * standard bundle issue fix
 *
 * Revision 1.16.2.33.4.15.2.16  2013/08/23 21:56:28  nneelam
 * Case#.  20123574
 * Replen Issue in GFT..
 *
 * Revision 1.16.2.33.4.15.2.15  2013/08/05 15:04:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * BB Cycle count issues
 * case#20123755
 *
 * Revision 1.16.2.33.4.15.2.14  2013/07/15 14:26:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * GFT Issue fixes
 *
 * Revision 1.16.2.33.4.15.2.13  2013/07/04 18:51:24  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixed against the case#201215160
 *
 * Revision 1.16.2.33.4.15.2.12  2013/06/26 15:00:35  snimmakayala
 * Case# : 201213655
 * GSUSA :: Issue Fixes
 *
 * Revision 1.16.2.33.4.15.2.11  2013/06/19 23:01:39  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of optimistic locking
 *
 * Revision 1.16.2.33.4.15.2.10  2013/06/18 13:33:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * nls issue fixes
 *
 * Revision 1.16.2.33.4.15.2.9  2013/06/11 14:30:56  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.16.2.33.4.15.2.8  2013/06/03 06:55:48  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inventory Changes
 *
 * Revision 1.16.2.33.4.15.2.7  2013/05/14 14:15:44  schepuri
 * GSUSA SB Task Priority issue
 *
 * Revision 1.16.2.33.4.15.2.6  2013/05/02 15:21:33  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.16.2.33.4.15.2.5  2013/04/16 14:55:58  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.16.2.33.4.15.2.4  2013/03/15 15:00:13  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.16.2.33.4.15.2.3  2013/03/05 14:48:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.16.2.33.4.15.2.2  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.16.2.33.4.15.2.1  2013/02/26 12:52:09  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.16.2.33.4.15  2013/02/20 15:40:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.16.2.33.4.14  2013/02/16 12:11:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.16.2.33.4.13  2013/02/01 15:22:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.16.2.33.4.12  2013/01/21 13:12:27  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * New function to clear allocations.
 *
 * Revision 1.16.2.33.4.11  2013/01/21 09:49:20  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * RF Picking fine tuning in Carton scan.
 *
 * Revision 1.16.2.33.4.10  2013/01/11 13:24:29  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to outbound process
 *
 * Revision 1.16.2.33.4.9  2013/01/09 15:18:44  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Round Qty functionality in replenishment.
 * .
 *
 * Revision 1.16.2.33.4.8  2013/01/08 14:17:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.16.2.33.4.7  2012/12/31 05:53:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Inventory Sync issue fixes
 * .
 *
 * Revision 1.16.2.33.4.6  2012/12/24 13:21:42  schepuri
 * CASE201112/CR201113/LOG201121
 * invalid columns issue
 *
 * Revision 1.16.2.33.4.5  2012/12/21 09:05:15  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.16.2.33.4.4  2012/11/22 01:00:32  gkalla
 * CASE201112/CR201113/LOG201121
 * While doing order pick reversal we are missing serial number in creating inventory.
 *
 * Revision 1.16.2.33.4.3  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.16.2.33.4.2  2012/10/26 08:08:20  gkalla
 * CASE201112/CR201113/LOG201121
 * Lot# selection popup in wave selection by line
 *
 * Revision 1.16.2.33.4.1  2012/10/11 14:50:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * REPLEN CHANGES
 *
 * Revision 1.16.2.33  2012/09/14 10:30:29  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Replen changes
 *
 * Revision 1.16.2.32  2012/09/04 22:34:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Invt move exceptions
 *
 * Revision 1.16.2.31  2012/09/04 16:40:13  gkalla
 * CASE201112/CR201113/LOG201121
 * Replen issue fix
 *
 * Revision 1.16.2.30  2012/09/03 19:10:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 * LEFO based picking
 *
 * Revision 1.16.2.29  2012/08/29 18:18:33  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * replens generation based on Auto replen flag
 *
 * Revision 1.16.2.28  2012/08/26 15:40:01  spendyala
 * CASE201112/CR201113/LOG201121
 * Replenishment issue
 *
 * Revision 1.16.2.27  2012/08/24 18:28:08  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changes
 *
 * Revision 1.16.2.26  2012/08/21 22:55:49  spendyala
 * CASE201112/CR201113/LOG201121
 * In getSKUQtyInPickfaceLocnsforReplen function invalid search column name issue resolved.
 *
 * Revision 1.16.2.25  2012/08/03 21:36:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.16.2.24  2012/07/30 23:19:08  gkalla
 * CASE201112/CR201113/LOG201121
 * NSUOM issue
 *
 * Revision 1.16.2.23  2012/07/30 16:45:36  gkalla
 * CASE201112/CR201113/LOG201121
 * replenishment merging
 *
 * Revision 1.16.2.22  2012/07/26 10:26:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.16.2.21  2012/07/17 12:42:49  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning (Bulk Insert & Update)
 *
 * Revision 1.16.2.20  2012/07/12 07:26:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.16.2.19  2012/06/25 22:48:18  gkalla
 * CASE201112/CR201113/LOG201121
 * Wave note released issue
 *
 * Revision 1.16.2.18  2012/06/25 14:54:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.16.2.17  2012/06/25 11:32:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.16.2.16  2012/06/22 12:27:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.16.2.15  2012/06/21 10:15:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.16.2.14  2012/06/20 10:18:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.16.2.13  2012/05/25 12:34:49  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix for checking null values
 *
 * Revision 1.16.2.12  2012/05/21 17:15:04  gkalla
 * CASE201112/CR201113/LOG201121
 * To get Pickface round qty
 *
 * Revision 1.16.2.11  2012/05/11 09:54:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue fixes for GSUSA
 *
 * Revision 1.16.2.10  2012/04/24 13:49:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.16.2.9  2012/04/23 23:33:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Zone picking
 *
 * Revision 1.16.2.8  2012/04/17 08:40:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pool of records
 *
 * Revision 1.16.2.7  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.30  2012/03/20 18:05:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation by sales order header
 *
 * Revision 1.29  2012/03/12 07:56:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.19  2012/03/09 16:38:16  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed the operator any to anyof  while fetching generate location
 *
 * Revision 1.4  2011/10/03 09:02:42  jrnarsupalli
 * CASE201112/CR201113/LOG201121
 * RM Comments: Corrected CVS header to have CVS key words.
 *
 *
 *****************************************************************************/
/**
 * Function to return the list of SKU quantity for all SKUs in all pickface locations
 * NOTE: WILL NEVER RETURN NULL; WILL ATLEAST RETURN AN EMPTY ARRAY
 * 
 * @returns {Array}
 */
function getSKUQtyInPickfaceLocns(whlocation,skulist,uom){
	/*
	 * This function should return the pickface locn, min qty, max qty, qoh for all
	 * SKUs in all pickface locations
	 */


	nlapiLogExecution('DEBUG', 'Into  getSKUQtyInPickfaceLocns');
	nlapiLogExecution('DEBUG', 'whlocation',whlocation);
	nlapiLogExecution('DEBUG', 'skulist',skulist);
	nlapiLogExecution('DEBUG', 'uom',uom);
	var pfLocnResults = new Array();

	// Search for all active records with a valid replen rule id
	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('custrecord_pickruleid', null, 'noneof', ['@NONE@']));

	if(whlocation!=null && whlocation!='')
		filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', ['@NONE@',whlocation]));

	if(skulist!=null && skulist!='')
		filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skulist));
	if(uom!=null && uom!='')
		filters.push(new nlobjSearchFilter('custrecord_packfaceuom', null, 'anyof', ['@NONE@',uom]));

	/* code merged from boombah on feb 25th by Radhika */
	// To filter the pickface locations of which binlocations are active. (Ratnaji - 02/19)
	filters.push(new nlobjSearchFilter('isinactive', 'custrecord_pickbinloc','is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_replenqty', null, 'greaterthan', 0));
	var columns = new Array();    
	columns[0] = new nlobjSearchColumn('custrecord_pickfacesku');
	columns[1] = new nlobjSearchColumn('custrecord_replenqty');
	columns[2] = new nlobjSearchColumn('custrecord_maxqty');
	columns[3] = new nlobjSearchColumn('custrecord_pickbinloc');
	columns[4] = new nlobjSearchColumn('custrecord_pickruleid');
	columns[5] = new nlobjSearchColumn('custrecord_minqty');    
	columns[6] = new nlobjSearchColumn('custrecord_pickface_location');
	columns[7] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_maxpickqty');
	columns[9] = new nlobjSearchColumn('custrecord_pickface_itemstatus');
	columns[10] = new nlobjSearchColumn('custrecord_pickface_location');
	columns[11] = new nlobjSearchColumn('custrecord_roundqty');
	columns[12] = new nlobjSearchColumn('custrecord_pickface_location');	
	columns[13] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');
	columns[14] = new nlobjSearchColumn('custrecord_pickzone');
	columns[15] = new nlobjSearchColumn('custrecord_autoreplen');
	columns[16] = new nlobjSearchColumn('custrecord_packfaceuom');


	// sort by pick face SKU
	columns[0].setSort();

	// Search for all active
	pfLocnResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);    

	return pfLocnResults;
}

var tempInventoryResultsArray=new Array();
//function getSKUQtyInPickfaceLocnsforReplen(maxno,itemlist,whlocation,itemfamily,itemgroup){
function getSKUQtyInPickfaceLocnsforReplen(maxno,itemlist,whlocation,itemfamily,itemgroup,inblocgroup,openrplnitems){
	nlapiLogExecution('DEBUG','Into getSKUQtyInPickfaceLocnsforReplen');
	nlapiLogExecution('DEBUG','maxno',maxno);
	nlapiLogExecution('DEBUG','itemlist',itemlist);
	nlapiLogExecution('DEBUG','whlocation',whlocation);
	nlapiLogExecution('DEBUG','itemfamily',itemfamily);
	nlapiLogExecution('DEBUG','itemgroup',itemgroup);
	/*
	 * This function should return the pickface locn, min qty, max qty, qoh for all
	 * SKUs in all pickface locations
	 */

	//var pfLocnResults = new Array();

	// Search for all active records with a valid replen rule id
	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('custrecord_pickruleid', null, 'noneof', ['@NONE@']));

	/* code added from FactoryMation on 28Feb13 by santosh, Float Conversion */
	if(parseFloat(maxno)> 0)
	{
		filters.push(new nlobjSearchFilter('internalid', null, 'lessthan', parseFloat(maxno)));
	}
	/* upto here */
	if(itemlist!=null && itemlist!='')
		filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', itemlist));

	if(openrplnitems!=null && openrplnitems!='')
		filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'noneof', openrplnitems));


	if(whlocation!=null && whlocation!='')
		//filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', whlocation));
		filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', ['@NONE@',whlocation]));// case# 201412729


	if(itemfamily!=null && itemfamily!='')
		filters.push(new nlobjSearchFilter('custitem_item_family', 'custrecord_pickfacesku', 'anyof', itemfamily));
	if(itemgroup!=null && itemgroup!='')
		filters.push(new nlobjSearchFilter('custitem_item_group', 'custrecord_pickfacesku', 'anyof', itemgroup));

	nlapiLogExecution('DEBUG','inblocgroup1234',inblocgroup);
	if(inblocgroup!=null && inblocgroup!='')
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_pickbinloc', 'anyof', inblocgroup));

	filters.push(new nlobjSearchFilter('isinactive', 'custrecord_pickbinloc', 'is', 'F'));
	var columns = new Array();    
	columns[0] = new nlobjSearchColumn('custrecord_pickfacesku');
	columns[1] = new nlobjSearchColumn('custrecord_replenqty');
	columns[2] = new nlobjSearchColumn('custrecord_maxqty');
	columns[3] = new nlobjSearchColumn('custrecord_pickbinloc');
	columns[4] = new nlobjSearchColumn('custrecord_pickruleid');
	columns[5] = new nlobjSearchColumn('custrecord_minqty');    
	columns[6] = new nlobjSearchColumn('custrecord_pickface_location');
	columns[7] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_maxpickqty');
	columns[9] = new nlobjSearchColumn('custrecord_pickface_itemstatus');
	columns[10] = new nlobjSearchColumn('internalid').setSort(true);
	columns[11] = new nlobjSearchColumn('custrecord_roundqty');
	columns[12] = new nlobjSearchColumn('custrecord_pickfacepackcode');


	// sort by pick face SKU
	columns[0].setSort();

	// Search for all active
	var pfLocnResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns); 
	if(pfLocnResults!=null)
	{

		for(var k=0;k<pfLocnResults.length;k++)
		{
			tempInventoryResultsArray.push(pfLocnResults[k]);
		}
		/*
		if(pfLocnResults.length>=1000)
		{
			var maxno1=pfLocnResults[pfLocnResults.length-1].getValue('internalid');
			for(var k=0;k<pfLocnResults.length;k++)
			{
				tempInventoryResultsArray.push(pfLocnResults[k]);
			}
			getSKUQtyInPickfaceLocnsforReplen(maxno1,itemlist,whlocation,itemfamily,itemgroup);
		}
		else
		{
			for(var k=0;k<pfLocnResults.length;k++)
			{
				tempInventoryResultsArray.push(pfLocnResults[k]);
			}

		}*/
	}

	nlapiLogExecution('DEBUG','Out of getSKUQtyInPickfaceLocnsforReplen');

	return tempInventoryResultsArray;
}

/**
 * 
 * @param skuList
 * @param binLocList
 * @returns {Array}
 */

function pendingReplenQuantity(skuQtyInPickfaceLocnsList){

	nlapiLogExecution('DEBUG', 'Into pendingReplenQuantity');

	var pendingReplenQuantity = new Array();
	var pendingReplenQtyCount = 0;
	var pfLocationList1 = getPFLocationListForQuery(skuQtyInPickfaceLocnsList);
	var pfskusList1 = getPFskuListForQuery(skuQtyInPickfaceLocnsList);
	if(pfLocationList1!=null && pfLocationList1!='')
	{	
		nlapiLogExecution('DEBUG', 'binLocList',pfLocationList1);	
	}
	if(pfskusList1!=null && pfskusList1!='')
	{	
		nlapiLogExecution('DEBUG', 'skulist',pfskusList1);	
	}

	try{
		var filters = new Array();
		// taskType = PUTW or RPLN
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2, 8]));

		// wmsStatusFlag = INBOUND/LOCATIONS ASSIGNED(2) or OUTBOUND/PICK GENERATED(9) or
		// 					INVENTORY/INTRANSIT (20) or INVENTORY/READY FOR REPLEN(21)
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 9, 20, 21])); 
		filters.push(new nlobjSearchFilter('custrecord_expe_qty', null, 'greaterthan',0));
		if(pfskusList1!=null && pfskusList1!='')
			filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', pfskusList1));

		// Fencing with multilocation flag to enable querying the opentask
		// records for more than one pickface location at a time
		if(pfLocationList1!=null && pfLocationList1!='')
			filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', pfLocationList1));
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', ['@NONE@']));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');
		columns[1] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');

		var searchOpenTask = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		if(searchOpenTask != null && searchOpenTask != '' && searchOpenTask.length > 0){
			nlapiLogExecution('DEBUG', 'searchOpenTask length',searchOpenTask.length);
			for(var j = 0; j < searchOpenTask.length; j++){
				var expectedQty = searchOpenTask[j].getValue('custrecord_expe_qty', null, 'sum');
				var pfLocn = searchOpenTask[j].getValue('custrecord_actbeginloc',null,'group');

				var currentRow = [pfLocn, expectedQty];
				//pendingReplenQuantity[j].push(currentRow);
				pendingReplenQuantity.push(currentRow);
			}
		}
	}catch(exps){
		nlapiLogExecution('DEBUG', 'pendingReplenPutawayTasks:Exception', exps);
	}

	nlapiLogExecution('DEBUG', 'Out of pendingReplenQuantity');

	return pendingReplenQuantity;
}

function getPFLocationListForQuery(skuQtyInPickfaceLocnsList){
	nlapiLogExecution('DEBUG','Into getPFLocationListForQuery',skuQtyInPickfaceLocnsList);

	var pfLocationList = new Array();

	if(skuQtyInPickfaceLocnsList!=null && skuQtyInPickfaceLocnsList!='')
	{
		for(var i = 0; i < skuQtyInPickfaceLocnsList.length; i++){
			pfLocationList.push(skuQtyInPickfaceLocnsList[i].getValue('custrecord_pickbinloc'));

		}

	}


	nlapiLogExecution('DEBUG','Out of getPFLocationListForQuery (Before Removing Duplicates)',pfLocationList);

	if(pfLocationList!=null && pfLocationList!='')
	{
		pfLocationList=removeDuplicateElement(pfLocationList);
	}


	nlapiLogExecution('DEBUG','Out of getPFLocationListForQuery',pfLocationList);

	return pfLocationList;
}
function getPFskuListForQuery(skuQtyInPickfaceLocnsList){
	nlapiLogExecution('DEBUG','Into getPFLocationListForQuery',skuQtyInPickfaceLocnsList);

	var pfskuList = new Array();

	if(skuQtyInPickfaceLocnsList!=null && skuQtyInPickfaceLocnsList!='')
	{
		for(var i = 0; i < skuQtyInPickfaceLocnsList.length; i++){
			pfskuList.push(skuQtyInPickfaceLocnsList[i].getValue('custrecord_pickfacesku'));

		}

	}
	if(pfskuList!=null && pfskuList!='')
	{
		pfskuList=removeDuplicateElement(pfskuList);
	}
	nlapiLogExecution('DEBUG','Out of getPFLocationListForQuery (Before Removing Duplicates)',pfskuList);

	nlapiLogExecution('DEBUG','Out of getPFLocationListForQuery',pfskuList);

	return pfskuList;
}

/**
 * Remove duplicates from an array
 * @param arrayName
 * @returns {Array}
 */
function removeDuplicateElement(arr){
	var dups = {}; 
	return arr.filter(
			function(el) { 
				var hash = el.valueOf(); 
				var isDup = dups[hash]; 
				dups[hash] = true; 
				return !isDup; 
			}
	); 
}

/**
 * 
 * @param item
 * @param itemFamily
 * @param itemGroup
 * @param availableQty
 * @param replenRule
 * @param maxQty
 * @param minQty
 * @returns {Array}
 */
function getReplenMethodZone(replenRule){
//	(item, itemFamily, itemGroup, availableQty, replenRule, maxQty, minQty){
	var replenMethodZone =  new Array();

	var fields = ['custrecord_replen_strategy_method', 'custrecord_replen_strategy_zone'];
	var columns = nlapiLookupField('customrecord_ebiznet_replenish_strategy', replenRule, fields);
	replenMethodZone[0] = columns.custrecord_replen_strategy_method;
	replenMethodZone[1]	= columns.custrecord_replen_strategy_zone;

	nlapiLogExecution('DEBUG','replenMethodZone', replenMethodZone);

	return replenMethodZone;
}

/**
 * Function to retrieve all replen zones and form them into a comma 
 * separated string
 * NOTE: WILL RETURN NULL IF THERE ARE NO ZONES CONFIGURED
 * 
 * @returns List of Replen Zones
 */
function getListForAllReplenZones(DOwhLocation){
	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//case start 20125876
	if(DOwhLocation!=null && DOwhLocation!='')//end  
		filters.push(new nlobjSearchFilter('custrecord_replen_location', null, 'anyof', DOwhLocation)); 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_replen_strategy_method');
	columns[1] = new nlobjSearchColumn('custrecord_replen_strategy_zone');
	columns[2] = new nlobjSearchColumn('custrecord_replen_strategy_itemstatus');

	var replenRulesList = new nlapiSearchRecord('customrecord_ebiznet_replenish_strategy', null, filters, columns);

//	nlapiLogExecution('DEBUG','getListForAllReplenZones:replenRulesList',replenRulesList.length);

	return replenRulesList;
}

function getReplenLocnGroups(){
	var replenLocnGroupList = new Array();

	var replenRulesList = getListForAllReplenZones();
//	nlapiLogExecution('DEBUG','getReplenLocnGroups:replenRulesList',replenRulesList.length);

	var replenZoneList = "";
	var replenZoneTextList = "";

	if (replenRulesList != null){
		for (var i = 0; i < replenRulesList.length; i++){

			if (i == 0){
				replenZoneList += replenRulesList[i].getValue('custrecord_replen_strategy_zone');
				replenZoneTextList += replenRulesList[i].getText('custrecord_replen_strategy_zone');
			} else {
				replenZoneList += ',';
				replenZoneList += replenRulesList[i].getValue('custrecord_replen_strategy_zone');

				replenZoneTextList += ',';
				replenZoneTextList += replenRulesList[i].getText('custrecord_replen_strategy_zone');
			}
		}
	} 

	replenLocnGroupList = getZoneLocnGroupsList(replenZoneList);

	return replenLocnGroupList;
}

/**
 * Function to return the list of location group IDs for those groups that are defined as
 * replenishment zones.  The list if formatted like [1, 2, 3 ...] where the entries are the
 * internal IDs of the location groups
 * 
 * NOTE: WILL NEVER RETURN NULL; WILL ALWAYS RETURN ATLEAST A BLANK STRING
 * @returns {String}
 */
function getZoneLocnGroupsList(zoneList,DOwhLocation){
	/*
	 * Return the location groups for all the replen zones
	 */
	var zoneLocnGroupList = new Array();

	// Get the list of replen zones
	if (zoneList == null){
		zoneList = getReplenishmentZoneId(DOwhLocation);
	}

	var listOfZones = zoneList;
	nlapiLogExecution('DEBUG','List Of Zones',listOfZones);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(listOfZones != null && listOfZones != '')
		filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', listOfZones));
	if(DOwhLocation!=null && DOwhLocation!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', DOwhLocation));



	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_locgroup_no');
	columns[1] = new nlobjSearchColumn('custrecord_zone_seq');
	columns[1].setSort();

	var zoneGroups = new nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);

	nlapiLogExecution('DEBUG','Zone Groups',zoneGroups.length);

	if(zoneGroups != null && zoneGroups.length > 0){
		for(var i = 0; i < zoneGroups.length; i++){
			zoneLocnGroupList.push(parseFloat(zoneGroups[i].getValue('custrecord_locgroup_no')));
		}
	}

	return zoneLocnGroupList;
}

/*** The below code is merged from Lexjet production account on 04thFeb13 by Santosh as part of Standard bundle***/
function getAllZoneLocnGroupsList(zoneList){
	/*
	 * Return the location groups for all the replen zones
	 */
	var zoneLocnGroupList = new Array();

	// Get the list of replen zones
	if (zoneList == null){
		zoneList = getReplenishmentZoneId();
	}

	var listOfZones = zoneList;
	nlapiLogExecution('DEBUG','List Of Zones',listOfZones);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(listOfZones != null && listOfZones != '')
		filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', listOfZones));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_locgroup_no');
	columns[1] = new nlobjSearchColumn('custrecord_zone_seq');
	columns[2] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
	columns[1].setSort();

	var zoneGroups = new nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);

	nlapiLogExecution('DEBUG','Zone Groups',zoneGroups.length);

	if(zoneGroups != null && zoneGroups.length > 0){
		for(var i = 0; i < zoneGroups.length; i++){
			var locgroupno = zoneGroups[i].getValue('custrecord_locgroup_no');
			var zoneno = zoneGroups[i].getValue('custrecordcustrecord_putzoneid');

			var currentRow = [zoneno,locgroupno];

			zoneLocnGroupList.push(currentRow);
		}
	}

	return zoneLocnGroupList;
}

/*** upto here***/


var vPFitemList = new Array();
var ItemList = new Array();
function getReplenItems(maxno,itemfamily,itemgroup,whlocation,inblocgroup)
{
	nlapiLogExecution('ERROR','Into getReplenItems');
	nlapiLogExecution('ERROR','inblocgroup',inblocgroup);
	var ItemList = new Array();
	var itemCount = 0;

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(parseInt(maxno)> 0)
	{
		filters.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)))
		//filters.push(new nlobjSearchFilter('internalid', null, 'lessthan', parseInt(maxno)));
	}
	if(itemfamily!=null && itemfamily!='')
		filters.push(new nlobjSearchFilter('custitem_item_family', 'custrecord_pickfacesku', 'anyof', itemfamily));
	if(itemgroup!=null && itemgroup!='')
		filters.push(new nlobjSearchFilter('custitem_item_group', 'custrecord_pickfacesku', 'anyof', itemgroup));

	if(inblocgroup!=null && inblocgroup!='')
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_pickbinloc', 'anyof', inblocgroup));

	// To filter the pickface locations of which binlocations are active. (Ratnaji - 02/19)
	filters.push(new nlobjSearchFilter('isinactive', 'custrecord_pickbinloc','is', 'F'));
	filters.push(new nlobjSearchFilter('isinactive', 'custrecord_pickfacesku','is', 'F'));

	//if(whlocation!=null && whlocation!='')
	//filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', whlocation));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_pickfacesku');
	columns[1] = new nlobjSearchColumn('name');
	columns[2] = new nlobjSearchColumn('id');
	columns[2].setSort(true);
	var Locationlist = new nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);

	//nlapiLogExecution('ERROR','Zone Groups',zoneGroups.length);

	if(Locationlist.length>=1000)
	{
		var maxno1=Locationlist[Locationlist.length-1].getValue('id');
		for(var i = 0; i < Locationlist.length; i++)
		{
			ItemList.push(parseFloat(Locationlist[i].getValue('custrecord_pickfacesku')));
			var vItem = Locationlist[i].getValue('custrecord_pickfacesku');
			var vPFitem = [vItem];

			vPFitemList[itemCount++] = vPFitem;
		}
		getReplenItems(maxno1,itemfamily,itemgroup,whlocation,inblocgroup);
	}
	else
	{
		for(var i = 0; i < Locationlist.length; i++)
		{
			ItemList.push(parseFloat(Locationlist[i].getValue('custrecord_pickfacesku')));
			var vItem = Locationlist[i].getValue('custrecord_pickfacesku');
			var vPFitem = [vItem];

			vPFitemList[itemCount++] = vPFitem;
		}

	}


	//return ItemList;
	return vPFitemList;
}

var allzoneLocnGroupList = new Array();

function getAllZoneLocnGroupsList(pickzones,maxno){

	nlapiLogExecution('DEBUG','Into getAllZoneLocnGroupsList',maxno);
	nlapiLogExecution('DEBUG','Into getAllZoneLocnGroupsList - pickzones',pickzones);
	/*
	 * Return the location groups for all the replen zones
	 */
	//var zoneLocnGroupList = new Array();
	if(pickzones !=null && pickzones !='')
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', pickzones));

		if(maxno!=-1)
		{
			filters.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
		}
		else
		{
			if(allzoneLocnGroupList.length>=0)
				allzoneLocnGroupList.splice(0, allzoneLocnGroupList.length);
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_locgroup_no');
		columns[1] = new nlobjSearchColumn('internalid');
		columns[2] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
		columns[3] = new nlobjSearchColumn('custrecord_zone_seq');

		columns[1].setSort(true);

		var zoneGroups = new nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);	

		if(zoneGroups != null && zoneGroups.length > 0){

			nlapiLogExecution('DEBUG','zoneGroups length',zoneGroups.length);

			if(zoneGroups.length>=1000)
			{
				var maxno1=zoneGroups[zoneGroups.length-1].getValue(columns[1]);

				for(var i = 0; i < zoneGroups.length; i++){
					//allzoneLocnGroupList.push(zoneGroups[i]);

					var currow = [zoneGroups[i].getValue('custrecordcustrecord_putzoneid'),zoneGroups[i].getValue('custrecord_locgroup_no'),zoneGroups[i].getValue('custrecord_zone_seq')];
					allzoneLocnGroupList.push(currow);
				}

				getAllZoneLocnGroupsList(pickzones,maxno1);
			}
			else
			{
				for(var i = 0; i < zoneGroups.length; i++){
					//allzoneLocnGroupList.push(zoneGroups[i]);
					var currow = [zoneGroups[i].getValue('custrecordcustrecord_putzoneid'),zoneGroups[i].getValue('custrecord_locgroup_no'),zoneGroups[i].getValue('custrecord_zone_seq')];
					allzoneLocnGroupList.push(currow);
				}
			}
		}
	}
	nlapiLogExecution('DEBUG','Out of getAllZoneLocnGroupsList');

	return allzoneLocnGroupList;
}

function getZoneLocnGroups(pickZone,allzonelocgroups)
{	
	nlapiLogExecution('DEBUG','Into getZoneLocnGroups - pickZone', pickZone);

	var locgroupslist = new Array();

	if(allzonelocgroups!=null && allzonelocgroups!='' && allzonelocgroups.length>0)
	{
		nlapiLogExecution('DEBUG','allzonelocgroups length', allzonelocgroups.length);

		allzonelocgroups.sort(function(a,b){return parseFloat(a[2]) - parseFloat(b[2]);});

		for(var i = 0; i < allzonelocgroups.length; i++){

			var locgroupzone = allzonelocgroups[i][0];
			var zoneseqno = allzonelocgroups[i][2];

			//nlapiLogExecution('DEBUG','zoneseqno', zoneseqno);

			if(pickZone == locgroupzone)
			{
				//nlapiLogExecution('DEBUG','zoneseqno', zoneseqno);
				locgroupslist.push(allzonelocgroups[i][1]);
			}
		}
	}

	nlapiLogExecution('DEBUG','Out of getZoneLocnGroups - locgroupslist', locgroupslist);

	return locgroupslist;
}
function getZonewhlocationList(Item){
	/*
	 * Return the location groups for all the replen zones
	 */
	var WHLocnGroupList = new Array();

	// Get the list of replen zones


	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(Item != null && Item != '')
		filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', Item));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_pickface_location',null,'group');


	var Locationlist = new nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);

	//nlapiLogExecution('DEBUG','Zone Groups',zoneGroups.length);

	if(Locationlist != null && Locationlist.length > 0){
		for(var i = 0; i < Locationlist.length; i++){
			WHLocnGroupList.push(Locationlist[i].getValue('custrecord_pickface_location',null,'group'));
		}
	}

	return WHLocnGroupList;
}

/**
 * Function to return the list of replen zones configured
 * NOTE: WILL NEVER RETURN NULL
 */
function getReplenishmentZoneId(loc){
	//var replenZoneList = "";
	var replenZoneList = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_zonetype', null, 'is', ['3','9']));
	if(loc!=null && loc!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizsiteput', null, 'anyof', loc));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_putzoneid');

	var zoneList = new nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, filters, columns);

	if(zoneList != null && zoneList.length > 0){
		for(var i = 0; i < zoneList.length; i++){
			/*if(i == 0)
				replenZoneList += zoneList[i].getId();
			else{
				replenZoneList += ",";
				replenZoneList += zoneList[i].getId();
			}*/
			replenZoneList.push(parseFloat(zoneList[i].getId()));
		}
	}

	return replenZoneList;
}

/**
 * Function to return the inventory records for all SKUs from all the replen zone locations
 * NOTE: THIS WILL NEVER RETURN NULL; WILL ATLEAST RETURN AN EMPTY ARRAY
 * 
 * @returns {Array}
 */
function getSKUQtyInZone(ZoneList,Item,replenzonestatuslist,whlocation,itemfamily,itemgroup,inblocgroup){
	/*
	 * This function will return the list of SKUs in the configured replen zone
	 * along with their quantity on hand in each of the bin locations in those replen zones
	 */
	nlapiLogExecution('DEBUG','itemfamily',itemfamily);
	nlapiLogExecution('DEBUG','itemgroup',itemgroup);
	var inventorySearchResults = new Array();
	var locnGroupList = new Array();
	//var WHlocnList=new Array(); ;
	var ItemArray=new Array();
	var itemdimsarr=new Array();
	// Call function to get the zone location group id list
	if(Item==null || Item=='')
	{
		var maxno=-1;
		//	ItemArray=getReplenItems(maxno,itemfamily,itemgroup,whlocation,inblocgroup);
		itemdimsarr=getReplenItems(maxno,itemfamily,itemgroup,whlocation);



		if (itemdimsarr != null && itemdimsarr.length > 0) 
		{	
			nlapiLogExecution('ERROR', 'AllItemDims.length', itemdimsarr.length);
			for(var p = 0; p < itemdimsarr.length; p++)
			{
				ItemArray.push(itemdimsarr[p][0]);

				if(ItemArray!=null && ItemArray!='')
				{
					nlapiLogExecution('DEBUG','ItemArray',ItemArray.length);
				}
			}
		}
		Item=ItemArray;
	}

	locnGroupList = getZoneLocnGroupsList(ZoneList);
	//WHlocnList=getZonewhlocationList(Item);
	nlapiLogExecution('DEBUG','locnGroupList',locnGroupList);
	nlapiLogExecution('DEBUG','whlocation',whlocation);
	nlapiLogExecution('DEBUG','Item',Item);
	var filters = new Array();
	//code added from FactoryMation on 28Feb13 by santosh
	// check Location group not equall to null
	if(locnGroupList!=null && locnGroupList!='')
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', locnGroupList));
	///up to here
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(Item!=null && Item!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', Item));

	if(whlocation!=null && whlocation!='')		
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', whlocation));


//	if(replenzonestatuslist!=null && replenzonestatuslist!="" && replenzonestatuslist!=","){	
//	var replenzonestatuslistnew = new Array();
//	for (var t = 0; t < replenzonestatuslist.length; t++) {
//	if(replenzonestatuslist[t]!="" && replenzonestatuslist[t]!=null && replenzonestatuslist[t]!="," ){	
//	//nlapiLogExecution('DEBUG', 'replenzonestatuslist ', replenzonestatuslist[t]);
//	replenzonestatuslistnew.push(replenzonestatuslist[t]);				
//	}
//	}
//	nlapiLogExecution('DEBUG', 'replenzonestatuslistnew', replenzonestatuslistnew);
//	if(replenzonestatuslistnew!="" && replenzonestatuslistnew!=null)
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', replenzonestatuslistnew));
//	}
	//filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype','custrecord_ebiz_inv_binloc', 'anyof', [6,7]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));	
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));

	var vRoleLocation=getRoledBasedLocationNew();
	nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', vRoleLocation));	
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_qoh');		
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');		//	Warehouse Location (WH Site)
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columns[13] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_ebiz_inv_binloc');

	columns[0].setSort();
	columns[3].setSort();
	columns[4].setSort(true);
	columns[13].setSort();

	inventorySearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	if(inventorySearchResults!=null && inventorySearchResults!='')
	{
		nlapiLogExecution('DEBUG','inventorySearchResults',inventorySearchResults.length);
	}

	return inventorySearchResults;
}


/**
 * 
 * @param actualQty
 * @param allocatedQty
 * @param availableQty
 * @param actualBinLocation
 * @param lp
 * @param RecordId
 * @param replenMethodId
 * @param replenZoneId
 * @param actualBatch
 * @param itemStatus
 * @returns {Array}
 */
function getInventoryData(actualQty, allocatedQty, actualBinLocationId, lp, RecordId, itemStatus){
//	replenMethodId, replenZoneId, actualBatch, itemStatus){

	var actAllocatedQty = 0;
	var Resultarray = new Array();
	var availableQty = 0;

	if (isNaN(allocatedQty)) {
		allocqty = 0;
	}
	if (allocatedQty == "") {
		allocatedQty = 0;
	}
	if (parseFloat(actualQty) < 0) {
		actualQty = 0;
	}

	var remainingQty = actualQty - allocatedQty;

	var confirmQty = 0;

	if (remainingQty > 0) {
		if ((availableQty - actAllocatedQty) <= remainingQty) {
			confirmQty = availableQty - actAllocatedQty;
			actAllocatedQty = availableQty;
		}
		else {
			confirmQty = remainingQty;
			actAllocatedQty = actAllocatedQty + remainingQty;
		}

		if (confirmQty > 0) {
			var inventoryArray = new Array();
			inventoryArray[0] = confirmQty;
			inventoryArray[1] = actualBinLocationId;
			inventoryArray[2] = lp;
			inventoryArray[3] = RecordId;
			inventoryArray[4] = "";//replenMethodId;
			inventoryArray[5] = "";//replenZoneId;
			inventoryArray[6] = "";//actualBatch;
			inventoryArray[7] = itemStatus;
			Resultarray.push(inventoryArray);
		}
	}

//	if ((availableQty - actAllocatedQty) == 0) {
	return Resultarray;
//	}
}

/**
 * Will return the allocated quantity from the list of available LPs in the inventory
 * If the LP is not located, return value is -1.
 * NOTE: INSTEAD OF LP, THE MATCH IS DONE ON INDEX WITHIN THE INVENTORYSEARCHRESULTS ARRAY
 * 
 * @param alreadyAllocIndex
 * @param index
 * @returns {Number}
 */
function isIndexAllocated(alreadyAllocIndex, index){
	var allocQty = -1;

	for(var i = 0; i < alreadyAllocIndex.length; i++){
		if(alreadyAllocIndex[i][0] == index)
			allocQty = alreadyAllocIndex[i][1];
	}

	return allocQty;
}

function getIndexInInventoryForSKU(inventorySearchResults, alreadyAllocIndex, replenItem, 
		replenQty, partReplenFlag){
	var index = -1;
	var itemFound = false;

	for(var i = 0; i < inventorySearchResults.length; i++){
		if(!itemFound){
			var invItem = inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
			var actualQty = inventorySearchResults[i].getValue('custrecord_ebiz_qoh');

			if(! partReplenFlag){
				if(invItem == replenItem && parseFloat(actualQty) >= parseFloat(replenQty)){
					index = i;
					itemFound = true;
				}
			} else {
				if(invItem == replenItem && parseFloat(actualQty) > 0){
					var indexAllocQty = isIndexAllocated(alreadyAllocIndex, i);
					if(indexAllocQty == -1){
						index = i;
						itemFound = true;
					}
				}
			}
		}
	}

	return index;
}

function replenRuleZoneLocation_BySeq(inventorySearchResults, replenItemArray, reportNo, stageLocation){
	for (var s = 0; s < replenItemArray.length; s++){
		var alreadyAllocIndex = new Array();
		var count = 0;

		var replenItem = replenItemArray[s][0];			// SKU to be replenished
		var replenQty = replenItemArray[s][1];			// Quantity for each replen record
		var replenTaskCount = replenItemArray[s][9];	// Number of replen tasks
		var replenTempQty = replenItemArray[s][8];		// Balance Qty after replen tasks
		var actEndLocationId = replenItemArray[s][7];	// Pickface Location

		// Determining the total qty to be replenished
		var totalReplenRequired = (parseFloat(replenQty) * parseFloat(replenTaskCount)) + parseFloat(replenTempQty);

		nlapiLogExecution('DEBUG','Item', replenItem);
		nlapiLogExecution('DEBUG','Replen Qty', replenQty);
		nlapiLogExecution('DEBUG','Task Count', replenTaskCount);
		nlapiLogExecution('DEBUG','Temp Qty', replenTempQty);
		nlapiLogExecution('DEBUG','actEndLocationId', actEndLocationId);

		for(var i = 0; i <= replenTaskCount; i++){
			// Inventory record located
			var invIndex = getIndexInInventoryForSKU(inventorySearchResults, alreadyAllocIndex,
					replenItem, replenQty, true);

			// Getting data from inventory search results
			var actualQty = inventorySearchResults[invIndex].getValue('custrecord_ebiz_qoh');
			var inventoryQty = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_qty');
			var allocatedQty = inventorySearchResults[invIndex].getValue('custrecord_ebiz_alloc_qty');
			var lpNo = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_lp');
			var actBinLocationId = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_binloc');
			var actBinLocation = inventorySearchResults[invIndex].getText('custrecord_ebiz_inv_binloc');
			var itemStatus = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_sku_status');
			var whLocation = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_loc');
			var recordId = inventorySearchResults[invIndex].getId();

			if(i == replenTaskCount && parseFloat(replenTempQty) > 0){
				// Create opentask record
				createOpenTaskRecord(replenTempQty, actBinLocationId, item, whLocation, reportNo, 
						lpNo, actBatchText, stageLocation, actEndLocationId,salesOrderNo);
				nlapiLogExecution('DEBUG', 'Allocating Qty @ Bulk Location', replenTempQty + '|' + lpNo);

				// Decrementing the balance replen
				totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(replenTempQty);
				nlapiLogExecution('DEBUG', 'Balance Replenishment', totalReplenRequired);

				// Updating the inventory with allocation quantity
				updateInventoryWithAllocationQuantity(recordId, replenTempQty, itemStatus);
			} else {
				var allocationQty = 0;

				/*
				 * Determining the allocation quantity depending on the actual quantity 
				 * and balance replenishment required
				 * If the actual quantity is greater than the balance replenishment required, then 
				 * allocate the balance replenishment quantity; Otherwise allocate the actual quantity
				 */
				if(parseFloat(actualQty) > parseFloat(replenQty)){
					var qtyAllocThisTime = 0;
					var tempAllocQty = actualQty;

					// Determining the allocation quantity with regards to balance replen
					if(parseFloat(totalReplenRequired) < parseFloat(replenQty))
						allocationQty = totalReplenRequired;
					else
						allocationQty = replenQty;

					while(parseFloat(tempAllocQty) > 0){
						// This condition is satisfied only if we have more than one task for the same
						// bulk bin location
						if(parseFloat(tempAllocQty) < parseFloat(replenQty))
							allocationQty = tempAllocQty;

						createOpenTaskRecord(allocationQty, actBinLocationId, item, whLocation, reportNo, 
								lpNo, actBatchText, stageLocation, actEndLocationId);
						nlapiLogExecution('DEBUG', 'Allocating Qty @ Bulk Location', allocationQty + '|' + lpNo);

						// Decrementing the balance replen
						totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(allocationQty);
						nlapiLogExecution('DEBUG', 'Balance Replenishment', totalReplenRequired);

						// Updating the inventory with allocation quantity
						updateInventoryWithAllocationQuantity(recordId, allocationQty, itemStatus);

						tempAllocQty = parseFloat(tempAllocQty) - parseFloat(allocationQty);

						// Incrementing the task count
						i++;

						// Increment Quantity Allocated for this index
						qtyAllocThisTime = parseFloat(qtyAllocThisTime) + parseFloat(allocationQty);
					}

					// Maintaining an index to quantity allocated array
					var currentRow = [invIndex, qtyAllocThisTime];
					alreadyAllocIndex[count++] = currentRow;
				} else {
					// Determining the allocation quantity with regards to balance replen
					if(parseFloat(totalReplenRequired) < parseFloat(actualQty))
						allocationQty = totalReplenRequired;
					else
						allocationQty = actualQty;

					createOpenTaskRecord(allocationQty, actBinLocationId, item, whLocation, reportNo, 
							lpNo, actBatchText, stageLocation, actEndLocationId);
					nlapiLogExecution('DEBUG', 'Allocating Qty @ Bulk Location', allocationQty + '|' + lpNo);

					// Decrementing the balance replen
					totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(allocationQty);
					nlapiLogExecution('DEBUG', 'Balance Replenishment', totalReplenRequired);

					// Updating the inventory with allocation quantity
					updateInventoryWithAllocationQuantity(recordId, allocationQty, itemStatus);

					// Maintaining an index to quantity allocated array
					var currentRow = [invIndex, allocationQty];
					alreadyAllocIndex[count++] = currentRow;
				}
			}
		}
	}

	return true;
}

function replenRuleZoneLocation_ByMaxLP(inventorySearchResults, replenItemArray,
		reportNo, stageLocation){
	for (var s = 0; s < replenItemArray.length; s++){
		var replenItem = replenItemArray[s][0];			// SKU to be replenished
		var replenQty = replenItemArray[s][1];			// Quantity for each replen record
		var replenTaskCount = replenItemArray[s][9];	// Number of replen tasks
		var replenTempQty = replenItemArray[s][8];		// Balance Qty after replen tasks
		var actEndLocationId = replenItemArray[s][7];	// Pickface Location

		// Determining the total qty to be replenished
		var totalReplenRequired = (parseFloat(replenQty) * parseFloat(replenTaskCount)) + parseFloat(replenTempQty);

		nlapiLogExecution('DEBUG','Item', replenItem);
		nlapiLogExecution('DEBUG','Replen Qty', replenQty);
		nlapiLogExecution('DEBUG','Task Count', replenTaskCount);
		nlapiLogExecution('DEBUG','Temp Qty', replenTempQty);
		nlapiLogExecution('DEBUG','actEndLocationId', actEndLocationId);

		for(var i = 0; i <= replenTaskCount; i++){
			// Inventory record located
			var invIndex = getIndexInInventoryForSKU(inventorySearchResults, replenItem, replenQty, false);

			// Getting data from inventory search results
			var actualQty = inventorySearchResults[invIndex].getValue('custrecord_ebiz_qoh');
			var inventoryQty = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_qty');
			var allocatedQty = inventorySearchResults[invIndex].getValue('custrecord_ebiz_alloc_qty');
			var lpNo = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_lp');
			var actBinLocationId = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_binloc');
			var actBinLocation = inventorySearchResults[invIndex].getText('custrecord_ebiz_inv_binloc');
			var itemStatus = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_sku_status');
			var whLocation = inventorySearchResults[invIndex].getValue('custrecord_ebiz_inv_loc');
			var recordId = inventorySearchResults[invIndex].getId();

			if(i == replenTaskCount && parseFloat(replenTempQty) > 0){
				// Create opentask record
				createOpenTaskRecord(replenTempQty, actBinLocationId, item, whLocation, reportNo, 
						lpNo, actBatchText, stageLocation, actEndLocationId);
				nlapiLogExecution('DEBUG', 'Allocating Qty @ Bulk Location', replenTempQty + '|' + lpNo);

				// Decrementing the balance replen
				totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(replenTempQty);
				nlapiLogExecution('DEBUG', 'Balance Replenishment', totalReplenRequired);

				// Updating the inventory with allocation quantity
				updateInventoryWithAllocationQuantity(recordId, replenTempQty, itemStatus);
			} else {
				createOpenTaskRecord(actualQty, actBinLocationId, item, whLocation, reportNo, 
						lpNo, actBatchText, stageLocation, actEndLocationId);
				nlapiLogExecution('DEBUG', 'Allocating Qty @ Bulk Location', actualQty + '|' + lpNo);

				// Decrementing the balance replen
				totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(actualQty);
				nlapiLogExecution('DEBUG', 'Balance Replenishment', totalReplenRequired);

				// Updating the inventory with allocation quantity
				updateInventoryWithAllocationQuantity(recordId, replenQty, itemStatus);
			}
		}

		// Check if all qty has been replenished
		while(parseFloat(totalReplenRequired) > 0){
			var invIdx = getIndexInInventoryForSKU(inventorySearchResults, replenItem, totalReplenRequired, true);

			// Getting data from inventory search results
			var actualQty = inventorySearchResults[invIdx].getValue('custrecord_ebiz_qoh');
			var inventoryQty = inventorySearchResults[invIdx].getValue('custrecord_ebiz_inv_qty');
			var allocatedQty = inventorySearchResults[invIdx].getValue('custrecord_ebiz_alloc_qty');
			var lpNo = inventorySearchResults[invIdx].getValue('custrecord_ebiz_inv_lp');
			var actBinLocationId = inventorySearchResults[invIdx].getValue('custrecord_ebiz_inv_binloc');
			var actBinLocation = inventorySearchResults[invIdx].getText('custrecord_ebiz_inv_binloc');
			var itemStatus = inventorySearchResults[invIdx].getValue('custrecord_ebiz_inv_sku_status');
			var whLocation = inventorySearchResults[invIdx].getValue('custrecord_ebiz_inv_loc');
			var recordId = inventorySearchResults[invIdx].getId();

			var allocationQty = 0;

			/*
			 * Determining the allocation quantity depending on the actual quantity
			 * If the actual quantity is greater than the balance replenishment required, then 
			 * allocate the balance replenishment quantity; Otherwise allocate the actual quantity
			 */
			if(parseFloat(actualQty) >= parseFloat(totalReplenRequired)){
				allocationQty = totalReplenRequired;
			} else {
				allocationQty = actualQty;
			}

			// Create opentask record
			createOpenTaskRecord(allocationQty, actBinLocationId, item, whLocation, reportNo, 
					lpNo, actBatchText, stageLocation, actEndLocationId);
			nlapiLogExecution('DEBUG', 'Allocating Qty @ Bulk Location', allocationQty + '|' + lpNo);

			// Decrementing the balance replen
			totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(allocationQty);
			nlapiLogExecution('DEBUG', 'Balance Replenishment', totalReplenRequired);

			// Updating the inventory with allocation quantity
			updateInventoryWithAllocationQuantity(recordId, allocationQty, itemStatus);
		}
	}

	return true;
}

/**
 * Determines the allocation quantity depending on the actual quantity, replen qty and balance
 * replenishment
 */
function getAllocationQty(tempActQty, replenQty, totalReplenRequired){
	return Math.min(tempActQty, Math.min(replenQty, totalReplenRequired));
}


/**
 * To get the Stage LPs and update the LP Range with the max LP number against the same record fetched
 */
function getStageLPsAndUpdateLPRange(TaskCount,whLoc){
	var maxLPNo = 0;
	var stageLPs = new Array();
	var lpPrefix = "";
	var RecordId;
	var whLocation;
	nlapiLogExecution('DEBUG','Into Stage LP range',whLoc);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype',null,'anyof', [1]));	// System Generated
	//filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [2]));	// PICK
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [1]));	// PALT
	if(whLoc!=null && whLoc!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', [whLoc]));	
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype');
	columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpmax');
	columns[3] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
	columns[4] = new nlobjSearchColumn('custrecord_ebiznet_lprange_site');

	var searchResults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);
	if(searchResults != null && searchResults.length > 0){
		for (var i = 0; i < searchResults.length; i++){
			maxLPNo = searchResults[0].getValue('custrecord_ebiznet_lprange_lpmax');
			lpPrefix = searchResults[0].getValue('custrecord_ebiznet_lprange_lpprefix');
			RecordId =  searchResults[0].getId();
			whLocation = searchResults[0].getValue('custrecord_ebiznet_lprange_site');
		}
	}

	nlapiLogExecution('DEBUG','TaskCount',TaskCount);

	for (var j = 0; j < TaskCount; j++){
		maxLPNo = parseFloat(maxLPNo) + 1;
		stageLPs[j] = lpPrefix + maxLPNo;
		nlapiLogExecution('DEBUG','stageLPs',stageLPs[j]);
	}

	updateMaxLPNo(RecordId, maxLPNo, whLocation);
	nlapiLogExecution('DEBUG','RecordId',RecordId);
	return stageLPs;
}


/**
 * Update the max LP number of LP Range for a specific record id that is passed.
 * @param RecordId
 * @param maxLPNo
 */
function updateMaxLPNo(RecordId, maxLPNo, whLocation){
	var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', RecordId);

	transaction.setFieldValue('custrecord_ebiznet_lprange_lpmax', maxLPNo);
	transaction.setFieldValue('custrecord_ebiznet_lprange_site', whLocation);

	nlapiLogExecution("ERROR", "maxLPNo", maxLPNo);
	nlapiSubmitRecord(transaction, true,true);

	return;
}

/**
 * 
 * @param item
 * @param itemfamily
 * @param itemgroup
 * @param availableQty
 * @param replenrule
 * @param maxqty
 * @param minQty
 * @returns {Array}
 */
function replenRuleZoneLocation(inventorySearchResults, replenItemArray, reportNo, stageLocation,whLocation,
		salesOrderNo,eBizWaveNo,taskpriority,ZoneAndLocationGrp,expectedQuantity){
	var returnval=false;
	var replnArray=new Array();
	for (var s = 0; s < replenItemArray.length; s++){
		var replenItem = replenItemArray[s][0];
		var replenQty = replenItemArray[s][1];
		var replenTaskCount = replenItemArray[s][9];
		var replenTempQty = replenItemArray[s][8];
		var actEndLocationId = replenItemArray[s][7];
		var pickfaceFixedLP = replenItemArray[s][10];
		var replenstatus=replenItemArray[s][11];
		var replenstatusId=replenItemArray[s][12];
		var pfRoundQty=replenItemArray[s][13];// case# 201414131
		var stageLPNo = 0;

		nlapiLogExecution('DEBUG','Item', replenItem);
		nlapiLogExecution('DEBUG','pickface FixedLP', pickfaceFixedLP);
		nlapiLogExecution('DEBUG','actEndLocationId',actEndLocationId);
		nlapiLogExecution('DEBUG','Task Count', replenTaskCount);
		nlapiLogExecution('DEBUG','Replen Qty', replenQty);
		nlapiLogExecution('DEBUG','Replen Temp Qty', replenTempQty);
		nlapiLogExecution('DEBUG','replenstatus', replenstatus);
		nlapiLogExecution('DEBUG','stageLocation', stageLocation);
		nlapiLogExecution('DEBUG','salesOrderNo', salesOrderNo);
		nlapiLogExecution('DEBUG','replenstatusId', replenstatusId);
		nlapiLogExecution('DEBUG','ZoneAndLocationGrp', ZoneAndLocationGrp);
		//var totalReplenRequired = (parseFloat(replenTaskCount) * parseFloat(replenQty)) + parseFloat(replenTempQty);
		var totalReplenRequired = parseFloat(replenTempQty);

//		if (replenTempQty >0){
//		replenTaskCount++;
//		}

		var stageLPs = getStageLPsAndUpdateLPRange(parseFloat(replenTaskCount),whLocation);

		var qtypertask = Math.floor(parseFloat(totalReplenRequired)/parseFloat(replenTaskCount));

		nlapiLogExecution('DEBUG','totalReplenRequired', totalReplenRequired);
		nlapiLogExecution('DEBUG','replenTaskCount', replenTaskCount);
		nlapiLogExecution('DEBUG','qtypertask', qtypertask);

		var totallocqty=0;

		if (inventorySearchResults != null) {
			for (var t = 0; t < inventorySearchResults.length; t++) {
				var item = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku');

				var actBatchText = "";
				var vPickZone ="";
				if(inventorySearchResults[t].getValue('custrecord_ebiz_inv_lot') != null)
					actBatchText=inventorySearchResults[t].getText('custrecord_ebiz_inv_lot');	

				var RecordId = inventorySearchResults[t].getId();
				nlapiLogExecution('DEBUG','RecordId', RecordId);
				var actBinLocationId = inventorySearchResults[t].getValue('custrecord_ebiz_inv_binloc');
				var invStatusId = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku_status');
				//Case# 20149789 starts
				if(replenstatusId =='' || replenstatusId ==null || replenstatusId =='null')
				{
					replenstatusId=replenstatus;
				}
				//Case# 20149789 ends
				if (replenItem == item && actEndLocationId != actBinLocationId  && replenstatusId == invStatusId){

					var newinvtrecord = nlapiLoadRecord('customrecord_ebiznet_createinv',RecordId);
					nlapiLogExecution('DEBUG','Inside the item condition check',item);

					var actualQty = newinvtrecord.getFieldValue('custrecord_ebiz_qoh');
					var inventoryQty = newinvtrecord.getFieldValue('custrecord_ebiz_inv_qty');
					var allocatedQty = newinvtrecord.getFieldValue('custrecord_ebiz_alloc_qty');
					var expdate = newinvtrecord.getFieldValue('custrecord_ebiz_expdate');
					var vLocationGroup = newinvtrecord.getFieldValue('custrecord_outboundinvlocgroupid');
					nlapiLogExecution('DEBUG','actualQty', actualQty);
					nlapiLogExecution('DEBUG','inventoryQty', inventoryQty);
					nlapiLogExecution('DEBUG','allocatedQty', allocatedQty);
					nlapiLogExecution('DEBUG','expdate', expdate);
					nlapiLogExecution('DEBUG','vLocationGroup', vLocationGroup);


					/*var actualQty = inventorySearchResults[t].getValue('custrecord_ebiz_qoh');
					var inventoryQty = inventorySearchResults[t].getValue('custrecord_ebiz_inv_qty');
					var allocatedQty = inventorySearchResults[t].getValue('custrecord_ebiz_alloc_qty');*/
					var lpNo = inventorySearchResults[t].getValue('custrecord_ebiz_inv_lp');
					var actBinLocationId = inventorySearchResults[t].getValue('custrecord_ebiz_inv_binloc');					
					var actBinLocation = inventorySearchResults[t].getText('custrecord_ebiz_inv_binloc');
					nlapiLogExecution('DEBUG','actBinLocation', actBinLocation);
					var itemStatus = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku_status');
					var whLocation = inventorySearchResults[t].getValue('custrecord_ebiz_inv_loc');

//					nlapiLogExecution('DEBUG','WH Location',whLocation);

					/*
					 * While actualQty > replenQty and TaskCount > 0
					 * 		Create open task record
					 * 		update everything else
					 * 		reduce actual quantity and task count
					 * loop
					 */
					nlapiLogExecution('DEBUG','allocatedQty', allocatedQty);
					nlapiLogExecution('DEBUG','actualQty', actualQty);

					if(allocatedQty==null || allocatedQty=='')
						allocatedQty=0;
					if(actualQty==null || actualQty=='')
						actualQty=0;
					var Availqty=parseFloat(actualQty)-parseFloat(allocatedQty);

					nlapiLogExecution('DEBUG','Availqty', Availqty);

					nlapiLogExecution('DEBUG','Availqty', Availqty);
					nlapiLogExecution('DEBUG','ZoneAndLocationGrp', ZoneAndLocationGrp);

					if(ZoneAndLocationGrp !=null && ZoneAndLocationGrp !='' && ZoneAndLocationGrp.length>0)
					{
						for( var k=0 ;k<ZoneAndLocationGrp.length; k++)
						{
							nlapiLogExecution('DEBUG','ZoneAndLocationGrp', ZoneAndLocationGrp[k][0]);
							if(parseFloat(vLocationGroup) == parseFloat(ZoneAndLocationGrp[k][0]))
							{
								vPickZone =ZoneAndLocationGrp[k][1]
							}

						}

					}

					nlapiLogExecution('DEBUG','vPickZone', vPickZone);

					//if(parseFloat(totalReplenRequired) > 0 && parseFloat(Availqty) <= parseFloat(totalReplenRequired))
					//{
					//	returnval=true;
					nlapiLogExecution('DEBUG','replenQty', replenQty);
					nlapiLogExecution('DEBUG','totalReplenRequired here', totalReplenRequired);

					if(parseFloat(Availqty)>0 && parseFloat(Availqty) <= parseFloat(replenQty) && parseFloat(totalReplenRequired) > 0){
						var tempAllocQty = getAllocationQty(Availqty, replenQty, totalReplenRequired);
						nlapiLogExecution('DEBUG','taskpriority from pick excep in inv gen func if', taskpriority);
						nlapiLogExecution('DEBUG','stageLPs[stageLPNo]', stageLPs[stageLPNo]);
						nlapiLogExecution('DEBUG','stageLPNo', stageLPNo);




						nlapiLogExecution('Debug','tempAllocQty 1 in if',tempAllocQty);

						var tempActwithRoundQty = Math.floor(parseFloat(tempAllocQty)/parseFloat(pfRoundQty));

						if(tempActwithRoundQty == null || tempActwithRoundQty == '' || isNaN(tempActwithRoundQty))
							tempActwithRoundQty=0;

						nlapiLogExecution('Debug','tempActwithRoundQty',tempActwithRoundQty);

						tempAllocQty = parseInt(pfRoundQty)*parseInt(tempActwithRoundQty);

						nlapiLogExecution('Debug','tempAllocQty 2 in if',tempAllocQty);
						if(tempAllocQty > 0)
						{
							createOpenTaskRecord(tempAllocQty, actBinLocationId, item, whLocation, reportNo, 
									pickfaceFixedLP, stageLPs[stageLPNo], actBatchText, stageLocation, 
									actEndLocationId, lpNo,RecordId,itemStatus,salesOrderNo,eBizWaveNo,taskpriority,'',expdate,vPickZone);

							totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(tempAllocQty);

							// Updating the inventory with allocation quantity
							updateInventoryWithAllocationQuantity(RecordId, tempAllocQty, itemStatus);
//							nlapiLogExecution('DEBUG', 'Bulk Bin Location', actBinLocationId);
//							nlapiLogExecution('DEBUG', 'totalReplenRequired', totalReplenRequired);
//							nlapiLogExecution('DEBUG', 'StageLPNo', stageLPs[stageLPNo]);

							//stageLPNo++;

//							nlapiLogExecution('DEBUG', 'StageLPNo', stageLPs[stageLPNo]);

							totallocqty=parseFloat(totallocqty)+parseFloat(tempAllocQty);

//							if(parseFloat(totallocqty) >= parseFloat(qtypertask))
//							stageLPNo++;
							returnval=true;
						}

					} else {
						var tempActQty = Availqty;

						nlapiLogExecution('DEBUG','tempActQty', replenQty);
						nlapiLogExecution('DEBUG','totalReplenRequired here', totalReplenRequired);

						while(parseFloat(tempActQty) > 0 && parseFloat(totalReplenRequired) > 0){
							nlapiLogExecution('DEBUG','tempActQty in while', tempActQty);
							nlapiLogExecution('DEBUG','totalReplenRequired in while', totalReplenRequired);

							var tempAllocQty = getAllocationQty(tempActQty, replenQty, totalReplenRequired);
							nlapiLogExecution('DEBUG','taskpriority from pick excep in inv gen func else', taskpriority);
							totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(tempAllocQty);//
							//tempActQty = parseFloat(tempActQty) - parseFloat(tempAllocQty);
							if(tempAllocQty > 0)
							{
								nlapiLogExecution('DEBUG','stageLPs[stageLPNo] in else', stageLPs[stageLPNo]);
								nlapiLogExecution('DEBUG','stageLPNo in else', stageLPNo);
								createOpenTaskRecord(tempAllocQty, actBinLocationId, item, whLocation, reportNo, 
										pickfaceFixedLP, stageLPs[stageLPNo], actBatchText, stageLocation, 
										actEndLocationId, lpNo,RecordId,itemStatus,salesOrderNo,eBizWaveNo,taskpriority,'',expdate,vPickZone);

								/*totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(tempAllocQty);
							tempActQty = parseFloat(tempActQty) - parseFloat(tempAllocQty);*/

								// Updating the inventory with allocation quantity
								updateInventoryWithAllocationQuantity(RecordId, tempAllocQty, itemStatus);

//								nlapiLogExecution('DEBUG', 'Bulk Bin Location', actBinLocationId);
//								nlapiLogExecution('DEBUG', 'totalReplenRequired', totalReplenRequired);
//								nlapiLogExecution('DEBUG', 'else: StageLPNo', stageLPs[stageLPNo]);
								tempActQty=parseFloat(tempActQty)-parseFloat(tempAllocQty);
								totallocqty=parseFloat(totallocqty)+parseFloat(tempAllocQty);

								nlapiLogExecution('Debug','tempActQty 1',tempActQty);

								var tempActwithRoundQty = Math.floor(parseFloat(tempActQty)/parseFloat(pfRoundQty));

								if(tempActwithRoundQty == null || tempActwithRoundQty == '' || isNaN(tempActwithRoundQty))
									tempActwithRoundQty=0;

								nlapiLogExecution('Debug','tempActwithRoundQty',tempActwithRoundQty);

								tempActQty = parseInt(pfRoundQty)*parseInt(tempActwithRoundQty);

								nlapiLogExecution('Debug','tempActQty 2',tempActQty);



//								if(parseFloat(totallocqty) >= parseFloat(qtypertask))
//								stageLPNo++;
								returnval=true;
								nlapiLogExecution('DEBUG', 'else: StageLPNo', stageLPNo);
							}
							else
							{
								if(parseFloat(totalReplenRequired) > 0)
								{
									continue;	
								}
								else
								{
									returnval=true;
									break;
								}

							}
						}
					}
					nlapiLogExecution('DEBUG', 'StageLPNo', stageLPNo);
					//}
				}
			}
		}
	}
	/*if(returnval==false)
	{
		return -1;
	}
	else
	{
		return true;	
	}*/



	if(returnval==false)
	{
		replnArray.push(false);
		replnArray.push(-1);
		replnArray.push(0);
	}
	else
	{
		var remainingqty=0;
		nlapiLogExecution('DEBUG', 'expectedQuantity', expectedQuantity);
		nlapiLogExecution('DEBUG', 'totallocqty', totallocqty);
		replnArray.push(true);
		if(parseInt(expectedQuantity)>parseInt(totallocqty))
			remainingqty=parseInt(expectedQuantity)-parseInt(totallocqty);
		replnArray.push(remainingqty);
		if(parseInt(expectedQuantity)>parseInt(totallocqty))
			replnArray.push(totallocqty);
		else
			replnArray.push(expectedQuantity);
	}
	nlapiLogExecution('DEBUG', 'replnArray', replnArray);
	return replnArray;

}

/**
 * This function is to update the allocation quantity in the inventory for a particular record.
 * @param RecordId
 * @param replenQty
 */
function updateInventoryWithAllocationQuantity(RecordId, replenQty, itemStatus){
	var scount=1;

	LABL1: for(var i=0;i<scount;i++)
	{	

		nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', i);
		try
		{
			var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', RecordId);
			var invtAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
			var qty = transaction.getFieldValue('custrecord_ebiz_inv_qty');
			var QOH = transaction.getFieldValue('custrecord_ebiz_qoh');

			if (replenQty == null || isNaN(replenQty) || replenQty == "") {
				replenQty = 0;
			}

			if (invtAllocQty == null || isNaN(invtAllocQty) || invtAllocQty == "") {
				invtAllocQty = 0;
			}

			invtAllocQty = parseFloat(invtAllocQty) + parseFloat(replenQty);
			transaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(invtAllocQty).toFixed(4));
			transaction.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
			transaction.setFieldValue('custrecord_ebiz_inv_packcode', 1);
			if (qty == null || isNaN(qty) || qty == "") {//case# 201415868
				transaction.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(QOH).toFixed(4));
			}


			nlapiLogExecution("ERROR", "Alloc Qty", parseFloat(invtAllocQty).toFixed(4));
			nlapiSubmitRecord(transaction, true);




			/*var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', RecordId);
			var invtAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');

			if (replenQty == null || isNaN(replenQty) || replenQty == "") {
				replenQty = 0;
			}

			if (invtAllocQty == null || isNaN(invtAllocQty) || invtAllocQty == "") {
				invtAllocQty = 0;
			}

			invtAllocQty = parseInt(invtAllocQty) + parseInt(replenQty);
			transaction.setFieldValue('custrecord_ebiz_alloc_qty', invtAllocQty);
			transaction.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
			transaction.setFieldValue('custrecord_ebiz_inv_packcode', 1);

			nlapiLogExecution("ERROR", "Alloc Qty", invtAllocQty);
			nlapiSubmitRecord(transaction, true,true);
			nlapiLogExecution('DEBUG', 'Replen Generated for inv ref ',RecordId);*/


		}
		catch(ex)
		{
			var exCode='CUSTOM_RECORD_COLLISION'; 
			var wmsE='Inventory record being updated by another user. Please try again...';
			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			} 
			/*var exceptionname='RF Picking';
			var functionality='RFPick';
			var trantype=3;
			var vInvRecId=invrefno;
			var vcontainerLp=contlpno;
			nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
			nlapiLogExecution('DEBUG', 'vcontainerLp', vcontainerLp);
			nlapiLogExecution('DEBUG', 'vInvRecId', invrefno);
			var reference3=ActPickQty;
			var reference4 ="";
			var reference5 ="";
			var userId = nlapiGetUser();
			InsertExceptionLog(exceptionname,trantype, functionality, wmsE, invrefno, vcontainerLp,reference3,reference4,reference5, userId);*/
			nlapiLogExecution('DEBUG', 'Exception in RF Inv move : ', wmsE); 
			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{ 
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;
		}
	}

	return;
}

/**
 * @param replenQty
 * @param actBeginLocationId
 * @param item
 * @param whLocation
 * @param reportNo
 * @param lpNo
 * @param replenMethod
 * @param replenZone
 * @param batchNo
 * @param stageLocation
 * @param actEndLocationId
 * @returns
 */
function createOpenTaskRecord(replenQty, actBeginLocationId, item, whLocation, reportNo, pickfaceFixedLP, 
		stageLPNo, batchNo,stageLocation,actEndLocationId, lpNo,invrefno,itemStatus,salesOrderNo,eBizWaveNo,taskpriority,temp,expdate,vPickZone){

	nlapiLogExecution('DEBUG','inside opentask to create replen task',actEndLocationId);
	nlapiLogExecution('DEBUG','salesOrderNo',salesOrderNo);
	nlapiLogExecution('DEBUG','taskpriority',taskpriority);
	nlapiLogExecution('DEBUG','expdate',expdate);
	nlapiLogExecution('DEBUG','vPickZone',vPickZone);
	nlapiLogExecution('DEBUG','pickfaceFixedLP',pickfaceFixedLP);
	nlapiLogExecution('DEBUG','stageLPNo',stageLPNo);

	var openTaskRecordId = "";
	if(pickfaceFixedLP==null || pickfaceFixedLP=='' || pickfaceFixedLP=='undefined')// case# 201413535
		pickfaceFixedLP=stageLPNo;
	nlapiLogExecution('DEBUG','pickfaceFixedLP after',pickfaceFixedLP);
	var customRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	customRecord.setFieldValue('custrecord_tasktype', 8);
	customRecord.setFieldValue('custrecordact_begin_date', DateStamp());
	customRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
	customRecord.setFieldValue('custrecord_expe_qty', parseFloat(replenQty).toFixed(4));
	//customRecord.setFieldValue('custrecord_act_qty', replenQty);
	customRecord.setFieldValue('custrecord_actbeginloc', actBeginLocationId);                        
	customRecord.setFieldValue('custrecord_wms_status_flag', 20); 
	customRecord.setFieldValue('custrecord_upd_date', DateStamp());
	customRecord.setFieldValue('custrecord_ebiz_sku_no', item);
	customRecord.setFieldValue('custrecord_sku', item);                        
	customRecord.setFieldValue('name', reportNo);
	//Case # 20127751ï¿½start
	//customRecord.setFieldValue('custrecord_lpno', stageLPNo);
	customRecord.setFieldValue('custrecord_lpno', pickfaceFixedLP);
	//Case # 20127751ï¿½end
	customRecord.setFieldValue('custrecord_ebizmethod_no', '');
	customRecord.setFieldValue('custrecord_ebizzone_no', '');
	customRecord.setFieldValue('custrecord_batch_no', batchNo);
	customRecord.setFieldValue('custrecord_wms_location',whLocation);
	//Case # 20127751ï¿½start
	//customRecord.setFieldValue('custrecord_container_lp_no',stageLPNo);
	customRecord.setFieldValue('custrecord_container_lp_no', pickfaceFixedLP);
	//Case # 20127751ï¿½end

	customRecord.setFieldValue('custrecord_from_lp_no',lpNo);
	customRecord.setFieldValue('custrecord_invref_no',invrefno);
	customRecord.setFieldValue('custrecord_actendloc', actEndLocationId);

	if(taskpriority!=null && taskpriority!='')
	{
		nlapiLogExecution('DEBUG','taskpriority in if',taskpriority);
		customRecord.setFieldValue('custrecord_taskpriority', taskpriority.toString());
	}
	else
	{
		nlapiLogExecution('DEBUG','taskpriority in else so hard code',taskpriority);
		customRecord.setFieldValue('custrecord_taskpriority', '5');
	}

	if(eBizWaveNo!=null && eBizWaveNo!='')
		customRecord.setFieldValue('custrecord_ebiz_wave_no', eBizWaveNo);
	if(salesOrderNo!=null && salesOrderNo!='')
		customRecord.setFieldValue('custrecord_ebiz_cntrl_no', salesOrderNo);
	if(expdate!=null && expdate!='')
		customRecord.setFieldValue('custrecord_expirydate', expdate);


	if(itemStatus!=null && itemStatus!='')
		customRecord.setFieldValue('custrecord_sku_status', itemStatus);
	if(vPickZone !=null && vPickZone !='')
	{
		customRecord.setFieldValue('custrecord_ebiz_zoneid', vPickZone);
		customRecord.setFieldValue('custrecord_ebizzone_no', vPickZone);
	}

	if (stageLocation != null && stageLocation != "" && stageLocation != "-1") {
		customRecord.setFieldValue('custrecord_actendloc', stageLocation);
		var stageRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		stageRecord.setFieldValue('custrecord_tasktype', 8);
		stageRecord.setFieldValue('custrecordact_begin_date', DateStamp());
		stageRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
		stageRecord.setFieldValue('custrecord_expe_qty', parseFloat(replenQty).toFixed(4));
		//stageRecord.setFieldValue('custrecord_act_qty', replenQty);
		stageRecord.setFieldValue('custrecord_actbeginloc', stageLocation);
		stageRecord.setFieldValue('custrecord_actendloc', actEndLocationId);
		stageRecord.setFieldValue('custrecord_wms_status_flag', 21);
		stageRecord.setFieldValue('custrecord_upd_date', DateStamp());
		stageRecord.setFieldValue('custrecord_ebiz_sku_no', item);
		stageRecord.setFieldValue('custrecord_sku', item);
		stageRecord.setFieldValue('name', reportNo);
		stageRecord.setFieldValue('custrecord_lpno', pickfaceFixedLP);
		stageRecord.setFieldValue('custrecord_ebizmethod_no', '');
		stageRecord.setFieldValue('custrecord_ebizzone_no', '');
		stageRecord.setFieldValue('custrecord_batch_no', batchNo);
		stageRecord.setFieldValue('custrecord_wms_location',whLocation);
		stageRecord.setFieldValue('custrecord_container_lp_no',pickfaceFixedLP);
		stageRecord.setFieldValue('custrecord_from_lp_no',lpNo);
		//stageRecord.setFieldValue('custrecord_taskpriority', '5');
		if(taskpriority!=null && taskpriority!='')
			stageRecord.setFieldValue('custrecord_taskpriority', taskpriority.toString());
		else
			stageRecord.setFieldValue('custrecord_taskpriority', '5');


		var stageRecordId= nlapiSubmitRecord(stageRecord);                        		  
		customRecord.setFieldValue('custrecord_ebiz_task_no', stageRecordId);
	}
	else
	{
		nlapiLogExecution('DEBUG','here',actEndLocationId);
		customRecord.setFieldValue('custrecord_actendloc', actEndLocationId);

	}

	openTaskRecordId = nlapiSubmitRecord(customRecord);
	return openTaskRecordId;
}


function getItemDetails(locnGroupList, itemList,allocationstrategy,pfBinLocationgroupId,
		lefoflag,vLotExcepId,vLotNoSplit,vOrdQty,samelot,latestexpirydt,binlocid){

	nlapiLogExecution('DEBUG', 'Into getItemDetails');

	var str = 'locnGroupList. = ' + locnGroupList + '<br>';
	str = str + 'itemList. = ' + itemList + '<br>';	
	str = str + 'allocationstrategy. = ' + allocationstrategy + '<br>';	
	str = str + 'pfBinLocationgroupId. = ' + pfBinLocationgroupId + '<br>';	
	str = str + 'lefoflag. = ' + lefoflag + '<br>';
	str = str + 'vLotExcepId. = ' + vLotExcepId + '<br>';
	str = str + 'vLotNoSplit. = ' + vLotNoSplit + '<br>';
	str = str + 'samelot. = ' + samelot + '<br>';
	str = str + 'latestexpirydt. = ' + latestexpirydt + '<br>';
	str = str + 'binlocid. = ' + binlocid + '<br>';
	nlapiLogExecution('DEBUG', 'getItemDetails Parameters', str);
	var vLotList=new Array();


	if(vLotNoSplit=='T')
	{
		var filtersLot = new Array();
		filtersLot.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(itemList!=null && itemList!=""){
			filtersLot.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemList));
		}
		//filtersLot.push(new nlobjSearchFilter('custrecord_ebizlocationtype','custrecord_ebiz_inv_binloc', 'anyof', [6,7]));
		filtersLot.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', vOrdQty).setSummaryType('sum'));	
		filtersLot.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));
		if((pfBinLocationgroupId==null || pfBinLocationgroupId=='') && binlocid != null && binlocid != '')
		{
			filtersLot.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'anyof', binlocid));
		}
		//If bin location assigned to pick strategy and also pick face enabled then we need to fetch that bin location group also
		if(pfBinLocationgroupId!=null && pfBinLocationgroupId!='' && binlocid != null && binlocid != '')
		{
			var filtersLocGrp = new Array();
			filtersLocGrp.push(new nlobjSearchFilter('internalid', null, 'is', binlocid));
			var columnsLocGrp = new Array();
			columnsLocGrp[0] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
			var SearchLocGrp = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGrp, columnsLocGrp);

			if(SearchLocGrp != null && SearchLocGrp != '')
			{
				if(SearchLocGrp[0].getValue('custrecord_outboundlocgroupid') != null && SearchLocGrp[0].getValue('custrecord_outboundlocgroupid') != '')
					locnGroupList.push(SearchLocGrp[0].getValue('custrecord_outboundlocgroupid'));
			}	
		}
		if((locnGroupList!="" && locnGroupList!=null) && (pfBinLocationgroupId!=null && pfBinLocationgroupId!=''))
		{
			nlapiLogExecution('DEBUG', 'test1', locnGroupList);
			filtersLot.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid', null, 'anyof', locnGroupList,pfBinLocationgroupId));
		}
		if((locnGroupList!="" && locnGroupList!=null) && (pfBinLocationgroupId==null || pfBinLocationgroupId==''))
		{
			nlapiLogExecution('DEBUG', 'test2', locnGroupList);
			filtersLot.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid', null, 'anyof', locnGroupList));
		}
		if(vLotExcepId!="" && vLotExcepId!=null)
		{
			nlapiLogExecution('DEBUG', 'vLotExcepId', vLotExcepId);
			var vLotExc=vLotExcepId.split(',');
			nlapiLogExecution('DEBUG', 'vLotExc', vLotExc);
			filtersLot.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', vLotExc));
		}

		var columnsLot = new Array();
		columnsLot[0] = new nlobjSearchColumn('custrecord_ebiz_expdate',null,'group');		 	
		columnsLot[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lot',null,'group');


		if(lefoflag=='T')
		{
			columns[0].setSort(true); 		// Expiry Date in descending.
		}

		var invtSearchResultsLot = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersLot, columnsLot);
		if(invtSearchResultsLot != null && invtSearchResultsLot != '')
		{
			for(var s=0;s<invtSearchResultsLot.length;s++)
			{
				vLotList.push(invtSearchResultsLot[s].getValue('custrecord_ebiz_inv_lot',null,'group'));
			}	
		}
	}	

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc',  'is','F'));
	//filters.push(new nlobjSearchFilter('custrecord_ebiz_cycl_count_hldflag', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('custrecord_ebiz_invholdflg', null, 'is', 'F'));
	if(itemList!=null && itemList!=""){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemList));
	}
	//filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype','custrecord_ebiz_inv_binloc', 'anyof', [6,7]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));	
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));
//	case no 20125228
	filters.push(new nlobjSearchFilter('isinactive','custrecord_ebiz_inv_binloc', 'is', 'F'));
	if((pfBinLocationgroupId==null || pfBinLocationgroupId=='') && binlocid != null && binlocid != '')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'anyof', binlocid));
	}
	//If bin location assigned to pick strategy and also pick face enabled then we need to fetch that bin location group also
	if(pfBinLocationgroupId!=null && pfBinLocationgroupId!='' && binlocid != null && binlocid != '')
	{
		var filtersLocGrp = new Array();
		filtersLocGrp.push(new nlobjSearchFilter('internalid', null, 'is', binlocid));
		var columnsLocGrp = new Array();
		columnsLocGrp[0] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
		var SearchLocGrp = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGrp, columnsLocGrp);

		if(SearchLocGrp != null && SearchLocGrp != '')
		{
			if(SearchLocGrp[0].getValue('custrecord_outboundlocgroupid') != null && SearchLocGrp[0].getValue('custrecord_outboundlocgroupid') != '')
				locnGroupList.push(SearchLocGrp[0].getValue('custrecord_outboundlocgroupid'));
		}	
	}
	if((locnGroupList!="" && locnGroupList!=null) && (pfBinLocationgroupId!=null && pfBinLocationgroupId!=''))
	{
		nlapiLogExecution('DEBUG', 'test1', locnGroupList);
		filters.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid',null, 'anyof', locnGroupList,pfBinLocationgroupId));
	}
	if((locnGroupList!="" && locnGroupList!=null) && (pfBinLocationgroupId==null || pfBinLocationgroupId==''))
	{
		nlapiLogExecution('DEBUG', 'test2', locnGroupList);
		filters.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid',null, 'anyof', locnGroupList));
	}
	nlapiLogExecution('DEBUG', 'vLotNoSplit', vLotNoSplit);
	nlapiLogExecution('DEBUG', 'vLotList', vLotList);
	nlapiLogExecution('DEBUG', 'vLotExc', vLotExc);
	if(vLotNoSplit=='T')
	{
		if(vLotList !="" && vLotList!=null && vLotList.length>0)
		{
			nlapiLogExecution('DEBUG', 'vLotList', vLotList);

			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', vLotList));
		}
		else if(vLotExcepId!="" && vLotExcepId!=null)
		{
			nlapiLogExecution('DEBUG', 'vLotExcepId', vLotExcepId);
			var vLotExc=vLotExcepId.split(',');
			nlapiLogExecution('DEBUG', 'vLotExc', vLotExc);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', vLotExc));
		}

	}	
	else if(vLotExcepId!="" && vLotExcepId!=null)
	{
		nlapiLogExecution('DEBUG', 'vLotExcepId', vLotExcepId);
		var vLotExc=vLotExcepId.split(',');
		nlapiLogExecution('DEBUG', 'vLotExc', vLotExc);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', vLotExc));
	}

	if(samelot=='T' && latestexpirydt!=null && latestexpirydt!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_expdate', null, 'onorafter', latestexpirydt));
	}

	/*if(samelot=='F' && latestexpirydt!=null && latestexpirydt!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_expdate', null, 'after', latestexpirydt));
	}*/
//	case # 20148032
	/*if((locnGroupList=="" || locnGroupList==null) && (pfBinLocationgroupId==null || pfBinLocationgroupId=='')
			&& (binlocid==null || binlocid==''))
	{
		return null;
	}*/

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');	
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[13] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_ebiz_inv_binloc');
	columns[14] = new nlobjSearchColumn('custrecord_ebiz_inv_masterlp');
	columns[15] = new nlobjSearchColumn('custrecord_ebiz_inv_adjusttype');
	columns[16] = new nlobjSearchColumn('custrecord_ebiz_inv_company');
	columns[17] = new nlobjSearchColumn('custrecord_ebiz_inv_note1');
	columns[18] = new nlobjSearchColumn('custrecord_ebiz_inv_note2');
	columns[19] = new nlobjSearchColumn('custrecord_ebiz_invholdflg');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_uomlvl');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_cycl_count_hldflag');
	columns[22] = new nlobjSearchColumn('custrecord_ebiz_itemdesc');
	columns[23] = new nlobjSearchColumn('custrecord_ebiz_model');
	columns[24] = new nlobjSearchColumn('custrecord_invttasktype');
	columns[25] = new nlobjSearchColumn('custrecord_outboundinvlocgroupid');
	columns[26] = new nlobjSearchColumn('custrecord_inboundinvlocgroupid');
	columns[27] = new nlobjSearchColumn('custrecord_ebiz_transaction_no');
	columns[28] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');

	if(lefoflag=='T')
	{
		columns[0].setSort(true); 		// Expiry Date in descending.
	}
	else
	{
		columns[0].setSort(); // Expiry Date in ascending. 
	}

	if(allocationstrategy!=null && allocationstrategy!='')
	{
		if(allocationstrategy=='1')
			columns[2].setSort(); 		// Qty Ascending.
		else
			columns[2].setSort(true);	// Qty Descending.

		columns[5].setSort(); 			// FIFO Date
	}
	else
	{
		columns[5].setSort();			// FIFO Date	
	}

	columns[13].setSort(); 				// Location Pick Sequence

	var invtSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('DEBUG', 'Out of getItemDetails');

	return invtSearchResults;
}

function getKitItemDetails(locnGroupList, itemList,allocationstrategy,pfBinLocationgroupId,
		lefoflag,samelot,latestexpirydt){

	nlapiLogExecution('DEBUG', 'Into getKitItemDetails');

	var str = 'locnGroupList. = ' + locnGroupList + '<br>';
	str = str + 'itemList. = ' + itemList + '<br>';	
	str = str + 'allocationstrategy. = ' + allocationstrategy + '<br>';	
	str = str + 'pfBinLocationgroupId. = ' + pfBinLocationgroupId + '<br>';	
	str = str + 'lefoflag. = ' + lefoflag + '<br>';
	str = str + 'samelot. = ' + samelot + '<br>';
	str = str + 'latestexpirydt. = ' + latestexpirydt + '<br>';

	nlapiLogExecution('DEBUG', 'getItemDetails Parameters', str);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(itemList!=null && itemList!=""){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemList));
	}
	//filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype','custrecord_ebiz_inv_binloc', 'anyof', [6,7]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));	
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));

	if((locnGroupList!="" && locnGroupList!=null) && (pfBinLocationgroupId!=null && pfBinLocationgroupId!=''))
	{
		nlapiLogExecution('DEBUG', 'test1', locnGroupList);
		filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', locnGroupList,pfBinLocationgroupId));
	}
	if((locnGroupList!="" && locnGroupList!=null) && (pfBinLocationgroupId==null || pfBinLocationgroupId==''))
	{
		nlapiLogExecution('DEBUG', 'test2', locnGroupList);
		filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', locnGroupList));
	}

	if((locnGroupList=="" || locnGroupList==null) && (pfBinLocationgroupId!=null && pfBinLocationgroupId!=''))
	{
		nlapiLogExecution('DEBUG', 'test3', pfBinLocationgroupId);
		filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', pfBinLocationgroupId));
	}
	if((locnGroupList=="" || locnGroupList==null) && (pfBinLocationgroupId==null || pfBinLocationgroupId==''))
	{
		nlapiLogExecution('DEBUG', 'test4', pfBinLocationgroupId);
		filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', ['@NONE@']));
	}

	if(samelot=='T' && latestexpirydt!=null && latestexpirydt!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_expdate', null, 'onorafter', latestexpirydt));
	}

	if(samelot=='F' && latestexpirydt!=null && latestexpirydt!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_expdate', null, 'after', latestexpirydt));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');	
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[13] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_ebiz_inv_binloc');
	columns[14] = new nlobjSearchColumn('custrecord_ebiz_inv_masterlp');
	columns[15] = new nlobjSearchColumn('custrecord_ebiz_inv_adjusttype');
	columns[16] = new nlobjSearchColumn('custrecord_ebiz_inv_company');
	columns[17] = new nlobjSearchColumn('custrecord_ebiz_inv_note1');
	columns[18] = new nlobjSearchColumn('custrecord_ebiz_inv_note2');
	columns[19] = new nlobjSearchColumn('custrecord_ebiz_invholdflg');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_uomlvl');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_cycl_count_hldflag');
	columns[22] = new nlobjSearchColumn('custrecord_ebiz_itemdesc');
	columns[23] = new nlobjSearchColumn('custrecord_ebiz_model');
	columns[24] = new nlobjSearchColumn('custrecord_invttasktype');
	columns[25] = new nlobjSearchColumn('custrecord_outboundinvlocgroupid');
	columns[26] = new nlobjSearchColumn('custrecord_inboundinvlocgroupid');
	columns[27] = new nlobjSearchColumn('custrecord_ebiz_transaction_no');
	columns[28] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');

	if(lefoflag=='T')
	{
		columns[0].setSort(true); 		// Expiry Date in descending.
	}
	else
	{
		columns[0].setSort(); // Expiry Date in ascending. 
	}

	if(allocationstrategy!=null && allocationstrategy!='')
	{
		if(allocationstrategy=='1')
			columns[2].setSort(); 		// Qty Ascending.
		else
			columns[2].setSort(true);	// Qty Descending.
	}

	columns[13].setSort(); 				// Location Pick Sequence

	var invtSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('DEBUG', 'Out of getKitItemDetails');

	return invtSearchResults;
}


function getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslistOld,whLocation,lefoflag,fulfillmentItemStatus){

	nlapiLogExecution('DEBUG', 'Into getItemDetailsforRepln');

	var str = 'locnGroupList. = ' + locnGroupList + '<br>';
	str = str + 'itemList. = ' + itemList + '<br>';	
	str = str + 'replenzonestatuslist. = ' + replenzonestatuslistOld + '<br>';	
	str = str + 'whLocation. = ' + whLocation + '<br>';	
	str = str + 'lefoflag. = ' + lefoflag + '<br>';
	str = str + 'fulfillmentItemStatus. = ' + fulfillmentItemStatus + '<br>';

	nlapiLogExecution('DEBUG', 'getItemDetailsforRepln Parameters', str);

	var replenzonestatuslist=replenzonestatuslistOld.split(',');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc',  'is','F'));
	if(itemList!=null && itemList!=""){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemList));
	}

	if(fulfillmentItemStatus!=null && fulfillmentItemStatus!="" && fulfillmentItemStatus!="undefined" ){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', fulfillmentItemStatus));
	}
	//filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype','custrecord_ebiz_inv_binloc', 'anyof', [6,7]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));	
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));
	if(whLocation!=null && whLocation!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'is', whLocation));


	if(locnGroupList!=null && locnGroupList!="" && locnGroupList!=","){	
		var locnGroupListnew = new Array();
		for (var t = 0; t < locnGroupList.length; t++) {
			if(locnGroupList[t]!="" && locnGroupList[t]!=null && locnGroupList[t]!="," ){	
				//nlapiLogExecution('DEBUG', 'locnGroupList ', locnGroupList[t]);
				locnGroupListnew.push(locnGroupList[t]);				
			}
		}
		nlapiLogExecution('DEBUG', 'locnGroupListnew', locnGroupListnew);
		if(locnGroupListnew!="" && locnGroupListnew!=null)
			filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', locnGroupListnew));
	}

	if(replenzonestatuslist!=null && replenzonestatuslist!="" && replenzonestatuslist!=","){	
		var replenzonestatuslistnew = new Array();
		for (var t = 0; t < replenzonestatuslist.length; t++) {
			if(replenzonestatuslist[t]!="" && replenzonestatuslist[t]!=null && replenzonestatuslist[t]!="," ){	
				//nlapiLogExecution('DEBUG', 'replenzonestatuslist ', replenzonestatuslist[t]);
				replenzonestatuslistnew.push(replenzonestatuslist[t]);				
			}
		}
		nlapiLogExecution('DEBUG', 'replenzonestatuslistnew', replenzonestatuslistnew);
		if(replenzonestatuslistnew!="" && replenzonestatuslistnew!=null)
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', replenzonestatuslistnew));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_qoh');		
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');		//	Warehouse Location (WH Site)
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');

	//columns[12] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	//columns[13] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_ebiz_inv_binloc');

	columns[12] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_ebiz_inv_binloc');// case# 201413936
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columns[14] = new nlobjSearchColumn('custrecord_outboundinvlocgroupid');//case# 201416311


	if(lefoflag=='T')
	{
		columns[0].setSort(true); 		// Expiry Date in descending.
	}
	else
	{
		columns[0].setSort(); // Expiry Date in ascending. 
	}

	// Specify the sort sequence
	columns[4].setSort();				//	FIFO.
	//columns[12].setSort(true);			//	Qty Descending.
	//columns[13].setSort();				//	Location Pick Sequence.
	columns[12].setSort();
	columns[13].setSort(true);

	var invtSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('DEBUG', 'Out of getItemDetailsforRepln');

	return invtSearchResults;
}

function getAllContainersList(vlocation)
{
	nlapiLogExecution('DEBUG', 'into getAllContainersList', vlocation);

	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	if(vlocation!=null && vlocation!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', vlocation));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');
	columns[1] = new nlobjSearchColumn('custrecord_cubecontainer');
	columns[2] = new nlobjSearchColumn('custrecord_maxweight');	
	columns[3] = new nlobjSearchColumn('custrecord_tareweight');
	columns[4] = new nlobjSearchColumn('custrecord_containername');
	columns[5] = new nlobjSearchColumn('custrecord_packfactor');
	columns[6] = new nlobjSearchColumn('custrecord_length');
	columns[7] = new nlobjSearchColumn('custrecord_widthcontainer');
	columns[8] = new nlobjSearchColumn('custrecord_heightcontainer');
	columns[9] = new nlobjSearchColumn('custrecord_ebizsitecontainer');

	columns[1].setSort(false);
	columns[2].setSort(false);

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);

	nlapiLogExecution('DEBUG', 'out of getAllContainersList', '');

	return containerslist;
}
function showInlineMessage(form, messageType, messageText, messageVariable){
	var msg;
	var priority;

	// Create the message field in the form
	msg = form.addField('custpage_message', 'inlinehtml', null, null, null);

	if(messageType == 'Confirmation')
		priority = 'NLAlertDialog.TYPE_LOWEST_PRIORITY';
	else if(messageType == 'DEBUG')
		priority = 'NLAlertDialog.TYPE_HIGH_PRIORITY';
	else
		priority = 'NLAlertDialog.TYPE_HIGH_PRIORITY';

	// Set the message value
	if(messageVariable != null)
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', '" +
				messageType + "', '" + messageText + ":" + messageVariable + "', " +
				priority + ",  '100%', null, null, null);</script></div>");
	else
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', '" +
				messageType + "', '" + messageText + "', " +
				priority + ",  '100%', null, null, null);</script></div>");
}

function getOpenPickqty(itemid,binlocation)
{
	nlapiLogExecution('DEBUG', 'Into getOpenPickqty');
	nlapiLogExecution('DEBUG', 'itemid',itemid);
	nlapiLogExecution('DEBUG', 'binlocation',binlocation);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', binlocation));
	filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', itemid));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));	 // TASK TYPE - PICK	
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	 // STATUS - PICKS GENERATED
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	nlapiLogExecution('DEBUG', 'Out of getOpenPickqty',searchresults);
	return searchresults;
}

function getQOHinPFLocation(itemid,binlocation){

	nlapiLogExecution('DEBUG', 'Into getQOHinPFLocation');
	nlapiLogExecution('DEBUG', 'itemid',itemid);
	nlapiLogExecution('DEBUG', 'binlocation',binlocation);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', binlocation));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemid));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('DEBUG', 'Out of pfSKUInvList',pfSKUInvList);
	return pfSKUInvList;
}

function updateItemFulfillment(nsrefno,ordlineno,taskqty,vBatchno)
{
	nlapiLogExecution('DEBUG', 'Into updateItemFulfillment');
	nlapiLogExecution('DEBUG', 'nsrefno',nsrefno);
	nlapiLogExecution('DEBUG', 'taskqty',taskqty);
	nlapiLogExecution('DEBUG', 'vBatchno',vBatchno);

	if(nsrefno!=null && nsrefno!='')
	{
		var vAdvBinManagement=false;

		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		}  
		nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);
		try
		{
			var itemIndex=0;
			var totalqty=taskqty;
			var vLotQty=0;
			var boolfound=true;

			var TransformRec = nlapiLoadRecord('itemfulfillment', nsrefno);
			if(TransformRec != null && TransformRec != '')
			{
				nlapiLogExecution('DEBUG', "ordlineno", ordlineno);
				var vTranType=TransformRec.getFieldValue('ordertype');
				//Case # 20126895ï¿½start
				if(vTranType=='TrnfrOrd')
				{
					ordlineno=parseInt(ordlineno)+1;
				}
				//Case # 20126895ï¿½end
				// To get the no of lines from item fulfillment record
				var IFLength = TransformRec.getLineItemCount('item');  

				for (var j = 1; IFLength!=null && j <= IFLength; j++) {

					var itemLineNo = TransformRec.getLineItemValue('item', 'orderline', j);

					if (itemLineNo == ordlineno) {
						itemIndex=j;    				
					}

				}

				nlapiLogExecution('DEBUG', "itemIndex", itemIndex);

				if(itemIndex!=0)
				{
					var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
					var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
					if(itemname!=null && itemname!='')
						itemname=itemname.replace(/ /g,"-");
					var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
					var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);
					var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);


					nlapiLogExecution('DEBUG', 'item_id', item_id);
					nlapiLogExecution('DEBUG', 'itemname', itemname);
					nlapiLogExecution('DEBUG', 'NSOrdUOM', NSOrdUOM);

					var vItemDimsArr = geteBizItemDimensionsArray(item_id);

					if(NSOrdUOM!=null && NSOrdUOM!="")
					{

						var veBizUOMQty=0;
						var vBaseUOMQty=0;

						if(vItemDimsArr!=null && vItemDimsArr.length>0)
						{
							for(z=0; z < vItemDimsArr.length; z++)
							{
								if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
								{	
									if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
										nlapiLogExecution('DEBUG', 'vBaseUOMQty', vBaseUOMQty);
									}
									nlapiLogExecution('DEBUG', 'Dim UOM', vItemDimsArr[z].getValue('custrecord_ebiznsuom'));
									nlapiLogExecution('DEBUG', 'NS UOM', NSOrdUOM);
									if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
									{
										nlapiLogExecution('DEBUG', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
										veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
										nlapiLogExecution('DEBUG', 'veBizUOMQty', veBizUOMQty);
									}
								}
							}	
							if(veBizUOMQty==null || veBizUOMQty=='')
							{
								veBizUOMQty=vBaseUOMQty;
							}
							if(veBizUOMQty!=0)
								totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
							nlapiLogExecution('DEBUG', 'totalqty', totalqty);

							vLotQty =totalqty;
						}
					}

					if(boolfound)
					{
						if(totalqty>0)
						{
							TransformRec.selectLineItem('item', itemIndex);
							var oldpickqty = TransformRec.getCurrentLineItemValue('item', 'quantity');
							var newpickqty = parseFloat(oldpickqty)-parseFloat(totalqty);
							nlapiLogExecution('Debug', 'oldpickqty', oldpickqty);
							nlapiLogExecution('Debug', 'newpickqty', newpickqty);
							if(parseFloat(newpickqty)>0)
							{
								TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
								TransformRec.setCurrentLineItemValue('item', 'quantity', newpickqty);					
								TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

								var ItemType;
								if(item_id!= null && item_id != "")
								{
									columns = nlapiLookupField('item', item_id, [ 'recordType','custitem_ebizserialout' ]);
									ItemType = columns.recordType;
									nlapiLogExecution('Debug', 'ItemType in else', ItemType);
								}
								//Case # 20126896ï¿½Start
								if(vAdvBinManagement)
								{
									if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
									{

										var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');

										vLotQty=parseInt(newpickqty);
										var polinelength = compSubRecord.getLineItemCount('inventoryassignment');
										nlapiLogExecution('ERROR', 'polinelength', polinelength);
										var itemfulfilserialno;
										var itemTempfulfilserialno;
										var qtyToReverse=totalqty;
										for(var k=1;k<=polinelength && (parseFloat(qtyToReverse)<=parseFloat(totalqty) && (parseFloat(qtyToReverse) > 0)) ;k++)
										{

											var itemfulfilserialno=compSubRecord.getLineItemText('inventoryassignment','issueinventorynumber',k);
											var tQty=compSubRecord.getLineItemValue('inventoryassignment','quantity',k);
											var tempReverseQty=0;
											nlapiLogExecution('ERROR', 'qtyToReverse', qtyToReverse);
											nlapiLogExecution('ERROR', 'tQty', tQty);
											if(vBatchno == itemfulfilserialno)
											{
												if(parseFloat(qtyToReverse) >= parseFloat(tQty))
												{
													qtyToReverse=parseFloat(qtyToReverse) - parseFloat(tQty);
													tempReverseQty=parseFloat(tQty);
													if(parseFloat(qtyToReverse)==0)
														tempReverseQty=0;
												}
												else
												{
													qtyToReverse=parseFloat(tQty) - parseFloat(qtyToReverse);
													tempReverseQty=parseFloat(qtyToReverse);
												}
												nlapiLogExecution('ERROR', 'itemfulfilserialno', itemfulfilserialno);
												nlapiLogExecution('ERROR', 'qtyToReverse', qtyToReverse);
												nlapiLogExecution('ERROR', 'tempReverseQty', tempReverseQty);
												
												if(tempReverseQty==0)
												{
													// remove the line when quantity is 0 in inventory details
													nlapiLogExecution('ERROR', 'tempReverseQty1', tempReverseQty);
													nlapiLogExecution('ERROR', 'k', k);
													compSubRecord.removeLineItem('inventoryassignment',k);
												}
												else
												{
													nlapiLogExecution('ERROR', ' in to tempReverseQty', tempReverseQty);
													compSubRecord.selectLineItem('inventoryassignment',k);
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(tempReverseQty));
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemfulfilserialno);

													compSubRecord.commitLineItem('inventoryassignment');
												}
											}

										}

										compSubRecord.commit();

										/***upto here***/
										//New code end

									}
									else if(ItemType == "serializedinventoryitem")
									{

										var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');

										vLotQty=parseInt(newpickqty);
										var polinelength = compSubRecord.getLineItemCount('inventoryassignment');
										nlapiLogExecution('ERROR', 'polinelength', polinelength);
										var itemfulfilserialno;
										var itemTempfulfilserialno;
										var qtyToReverse=totalqty;
										//case # 20127190.....looping the number of times equal to reversal qty.
										for(var k=1;k<=qtyToReverse;k++)
										{


											var itemfulfilserialno=compSubRecord.getLineItemText('inventoryassignment','issueinventorynumber',k);
											nlapiLogExecution('ERROR', 'itemfulfilserialno', itemfulfilserialno);
											nlapiLogExecution('ERROR', 'qtyToReverse', qtyToReverse);
											compSubRecord.removeLineItem('inventoryassignment',k);



										}

										compSubRecord.commit();

										/***upto here***/
										//New code end

									}

								}
								//Case # 20126896ï¿½End
								TransformRec.commitLineItem('item');
							}
							else
							{
								nlapiLogExecution('DEBUG', 'Picked Qty is Zero', newpickqty);
								nlapiLogExecution('DEBUG', 'IFLength', IFLength);

								if(IFLength==1)
								{
									nlapiDeleteRecord('itemfulfillment', nsrefno);
									return;
								}
								else
									TransformRec.setLineItemValue('item','itemreceive',itemIndex,'F');

							}
						}
						else
						{
							nlapiLogExecution('DEBUG', 'Picked Qty is Zero', totalqty);
							nlapiLogExecution('DEBUG', 'IFLength', IFLength);

							if(IFLength==1)
							{
								nlapiDeleteRecord('itemfulfillment', nsrefno);
								return;
							}
							else
								TransformRec.setLineItemValue('item','itemreceive',itemIndex,'F');
						}
					}
				}

				nlapiLogExecution('DEBUG', 'Before Submit', '');

				if(TransformRec!=null)
				{
					var TransformRecId = nlapiSubmitRecord(TransformRec, true);
					nlapiLogExecution('DEBUG', 'After Submit', '');
				}
			}
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in updateItemFulfillment',exp);
		}
	}

	nlapiLogExecution('DEBUG', 'Out of updateItemFulfillment');
}




function updateOpentaskandInventory(invrefno,reversalqty,taskid,taskactqty,taskweight,taskcube,taskuomlevel,itemid,lprecid,ordintrid)
{
	nlapiLogExecution('DEBUG', 'Into updateOpentaskandInventory');

	var str = 'invrefno. = ' + invrefno + '<br>';
	str = str + 'reversalqty. = ' + reversalqty + '<br>';	
	str = str + 'taskactqty. = ' + taskactqty + '<br>';	
	str = str + 'opentaskid. = ' + taskid + '<br>';
	str = str + 'taskweight. = ' + taskweight + '<br>';
	str = str + 'taskcube. = ' + taskcube + '<br>';
	str = str + 'taskuomlevel. = ' + taskuomlevel + '<br>';
	str = str + 'itemid. = ' + itemid + '<br>';
	str = str + 'lprecid. = ' + lprecid + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var inventoryTransaction='-1';
	var opentaskTransaction='-1';
	var wmsstatus;

	try
	{
		opentaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', taskid);
		wmsstatus = opentaskTransaction.getFieldValue('custrecord_wms_status_flag');
		nlapiLogExecution('DEBUG', 'wmsstatus',wmsstatus);	
	}
	catch(exp)
	{
		opentaskTransaction='-1';
		nlapiLogExecution('DEBUG', 'Exception in Loading Opentask Record',exp);	
	}


	if(wmsstatus!='29')
	{
		if(lprecid!=null && lprecid!='')
			nlapiSubmitField('customrecord_ebiznet_master_lp', lprecid, 'isinactive', 'T');

		var itemweight=0;
		var itemcube=0;
		var itemqty=0;

		var itemweightcube = getItemCubeandWeight(itemid,taskuomlevel);
		if(itemweightcube!=null && itemweightcube!='')
		{
			itemweight = itemweightcube[0].getValue('custrecord_ebizweight');
			itemcube = itemweightcube[0].getValue('custrecord_ebizcube');
			itemqty = itemweightcube[0].getValue('custrecord_ebizqty');

			if(isNaN(itemweight))
				itemweight=0;

			if(isNaN(itemcube))
				itemcube=0;	

			itemweight = (reversalqty*itemweight)/parseFloat(itemqty);
			itemcube = (reversalqty*itemcube)/parseFloat(itemqty);

			//The below is specific to Dynacraft.
			//updateSalesOrderHeader(ordintrid,itemweight,itemcube);
		}

		if(isNaN(itemweight))
			itemweight=0;

		if(isNaN(itemcube))
			itemcube=0;		

		taskweight = parseFloat(taskweight)- parseFloat(itemweight);
		taskcube = parseFloat(taskcube)- parseFloat(itemcube);

		var str1 = 'itemweight. = ' + itemweight + '<br>';
		str1 = str1 + 'itemcube. = ' + itemcube + '<br>';	
		str1 = str1 + 'itemqty. = ' + itemqty + '<br>';	
		str1 = str1 + 'new taskcube. = ' + taskcube + '<br>';
		str1 = str1 + 'new taskcube. = ' + taskcube + '<br>';

		nlapiLogExecution('DEBUG', 'Task and Weight Parameters', str1);



		var scount=1;

		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
			try
			{

				try
				{
					inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				}
				catch(exp)
				{
					inventoryTransaction='-1';
					nlapiLogExecution('DEBUG', 'Exception in Loading Invt Record',exp);	
				}

				if(inventoryTransaction!=null && inventoryTransaction!='' && inventoryTransaction!='-1')
				{
					nlapiLogExecution('DEBUG', 'Into Update Inventory');

					var qoh = inventoryTransaction.getFieldValue('custrecord_ebiz_qoh');

					if (isNaN(qoh))
					{
						qoh = 0;
					}

					var totQOH=parseFloat(qoh)+parseFloat(reversalqty);

					inventoryTransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totQOH).toFixed(4));
					inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
					inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');
					inventoryTransaction.setFieldValue('custrecord_ebiz_inv_note1', 'Updated by Pick Reversal Process');  

					nlapiSubmitRecord(inventoryTransaction,false,true);

					nlapiLogExecution('DEBUG', 'Out of Update Inventory');
				}		

			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				}			
				nlapiLogExecution('ERROR', 'Exception in Pick Reversal : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

		if(inventoryTransaction ==  null || inventoryTransaction == '' || inventoryTransaction =='-1')
		{


			if(opentaskTransaction!=null && opentaskTransaction!='' && opentaskTransaction!='-1')
			{
				var binloc = opentaskTransaction.getFieldValue('custrecord_actendloc');
				var lp = opentaskTransaction.getFieldValue('custrecord_lpno');
				var item = opentaskTransaction.getFieldValue('custrecord_sku');
				var itemstatus = opentaskTransaction.getFieldValue('custrecord_sku_status');
				var packcode = opentaskTransaction.getFieldValue('custrecord_packcode');
				var qty = opentaskTransaction.getFieldValue('custrecord_act_qty');
				var uomlevel = opentaskTransaction.getFieldValue('custrecord_uom_level');
				var whloc = opentaskTransaction.getFieldValue('custrecord_wms_location');
				var company = opentaskTransaction.getFieldValue('custrecord_comp_id');
				var lot = opentaskTransaction.getFieldValue('custrecord_batch_no');
				var serialNo = opentaskTransaction.getFieldValue('custrecord_serial_no');
				//ExpDate Start
				var expdt=opentaskTransaction.getFieldValue('custrecord_expirydate');
				//End
				if(packcode == null || packcode == '')
				{
					var columns = nlapiLookupField('item', item,[ 'recordType','custitem_ebizdefpackcode']);
					if(columns != null && columns != '' && columns.custitem_ebizdefpackcode != null && columns.custitem_ebizdefpackcode != '' )
						packcode=columns.custitem_ebizdefpackcode;
					else
						packcode=1;
				}	
				createInventory(binloc,lp,item,itemstatus,packcode,reversalqty,uomlevel,lot,whloc,company,serialNo,expdt);

			}
		}

		updateOpenTask(taskid,reversalqty,taskactqty,taskweight,taskcube);
	}

	nlapiLogExecution('DEBUG', 'Out of updateOpentaskandInventory');
}

function updateSalesOrderHeader(ordintrid,itemweight,itemcube)
{
	nlapiLogExecution('DEBUG', 'Into updateSalesOrderHeader');
	try
	{
		var str = 'ordintrid. = ' + ordintrid + '<br>';
		str = str + 'itemweight. = ' + itemweight + '<br>';	
		str = str + 'itemcube. = ' + itemcube + '<br>';

		var trantype = nlapiLookupField('transaction', ordintrid, 'recordType');

		str = str + 'trantype. = ' + trantype + '<br>';
		nlapiLogExecution('DEBUG', 'Function Parameters', str);

		var sototweight=0;
		var sototvolume=0;

		var filter=new Array();
		filter.push(new nlobjSearchFilter('internalid',null,'is',ordintrid));
		filter.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custbody_total_weight');
		column[1]=new nlobjSearchColumn('custbody_total_vol');

		var searchRec=nlapiSearchRecord(trantype,null,filter,column);
		if(searchRec!=null&&searchRec!="")
		{
			sototweight = searchRec[0].getValue('custbody_total_weight');
			sototvolume = searchRec[0].getValue('custbody_total_vol');

			if (sototweight==null || sototweight=='' || isNaN(sototweight))
			{
				sototweight = 0;
			}

			if (sototvolume==null || sototvolume=='' || isNaN(sototvolume))
			{
				sototvolume = 0;
			}

			var str1 = 'sototweight. = ' + sototweight + '<br>';
			str1 = str1 + 'sototvolume. = ' + sototvolume + '<br>';	
			nlapiLogExecution('DEBUG', 'Weight & Cube before update', str1);

			if(parseFloat(sototweight)>=parseFloat(itemweight))			
				sototweight=parseFloat(sototweight)-parseFloat(itemweight);

			if(parseFloat(sototvolume)>=parseFloat(itemcube))		
				sototvolume=parseFloat(sototvolume)-parseFloat(itemcube);

			var str2 = 'sototweight. = ' + sototweight + '<br>';
			str2 = str2 + 'sototvolume. = ' + sototvolume + '<br>';	
			nlapiLogExecution('DEBUG', 'Weight & Cube After update', str2);

			var fields = new Array();
			var values = new Array();

			fields.push('custbody_total_weight');
			fields.push('custbody_total_vol');

			values.push(parseFloat(sototweight));
			values.push(sototvolume);

			nlapiSubmitField(trantype, searchRec[0].getId(), fields, values);
		}	
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in updateSalesOrderHeader',exp);	
	}

	nlapiLogExecution('DEBUG', 'Out of updateSalesOrderHeader');	
}

function createInventory(binloc,lp,item,itemstatus,packcode,qty,uomlevel,lot,whloc,company,serialNo,expdt)
{
	nlapiLogExecution('DEBUG', 'Into createInventory');

	var str = 'binloc. = ' + binloc + '<br>';
	str = str + 'lp. = ' + lp + '<br>';	
	str = str + 'item. = ' + item + '<br>';
	str = str + 'itemstatus. = ' + itemstatus + '<br>';
	str = str + 'packcode. = ' + packcode + '<br>';
	str = str + 'qty. = ' + qty + '<br>';
	str = str + 'uomlevel. = ' + uomlevel + '<br>';
	str = str + 'lot. = ' + lot + '<br>';
	str = str + 'whloc. = ' + whloc + '<br>';
	str = str + 'company. = ' + company + '<br>';
	str = str + 'serialNo. = ' + serialNo + '<br>';
	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', binloc));
	filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']));

	if(itemstatus!=null && itemstatus!='')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemstatus));

	if(packcode!=null && packcode!='')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', packcode));							

	if(lot!=null && lot!='')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', lot));

	/*if(lp!=null && lp!='')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp));*/

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
	columnsinvt[0].setSort();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
	if(invtsearchresults!=null && invtsearchresults!='' && invtsearchresults.length>0)
	{
		nlapiLogExecution('DEBUG', 'Merging Inventory to the existing record...');

		var qoh=invtsearchresults[0].getValue('custrecord_ebiz_qoh');

		var newqoh = parseFloat(qoh)+parseFloat(qty);

		/*var invtcolumns = new Array();
		var invtvalues = new Array();

		invtcolumns[0] = 'custrecord_ebiz_qoh';
		invtvalues[0] = parseFloat(newqoh);	

		invtcolumns[1] = 'custrecord_ebiz_callinv';
		invtvalues[1] = 'N';	

		invtcolumns[2] = 'custrecord_ebiz_displayfield';
		invtvalues[2] = 'N';	

		nlapiSubmitField('customrecord_ebiznet_createinv', invtsearchresults[0].getId(),invtcolumns, invtvalues);*/

		var scount=1;
		LABL1: for(var j=0;j<scount;j++)
		{	
			nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', j);
			try
			{

				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[0].getId());
				qoh = transaction.getFieldValue('custrecord_ebiz_qoh');
				var newqoh = parseFloat(qoh)+parseFloat(qty);
				transaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(newqoh));
				transaction.setFieldValue('custrecord_ebiz_callinv', "N");						
				transaction.setFieldValue('custrecord_ebiz_displayfield', "N");
				nlapiSubmitRecord(transaction, false, true);
				nlapiLogExecution('DEBUG', 'Cyclecount generated for inv ref ',invtsearchresults[0].getId());
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 

				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				}				 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}




	}
	else
	{

		nlapiLogExecution('DEBUG', 'Creating Inventory record...');

		var newsysLp = GetMaxLPNo('1','1',whloc);

		nlapiLogExecution('DEBUG', 'newsysLp', newsysLp);

		var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');	
		invtRec.setFieldValue('name', newsysLp);
		invtRec.setFieldValue('custrecord_ebiz_inv_binloc', binloc);
		invtRec.setFieldValue('custrecord_ebiz_inv_lp', newsysLp);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku', item);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
		if(packcode!=null && packcode!='')
			invtRec.setFieldValue('custrecord_ebiz_inv_packcode',parseInt(packcode));
		invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(qty).toFixed(4));
		invtRec.setFieldValue('custrecord_inv_ebizsku_no', item);
		invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(qty).toFixed(4));
		invtRec.setFieldValue('custrecord_invttasktype', 10); // Task Type - INVT
		invtRec.setFieldValue('custrecord_wms_inv_status_flag', 19); //Inventory Storage
		invtRec.setFieldValue('custrecord_ebiz_displayfield','N');
		invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
		invtRec.setFieldValue('custrecord_ebiz_inv_loc', whloc); 
		invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');   
		invtRec.setFieldValue('custrecord_ebiz_inv_note1', 'Created by Pick Reversal Process');   
		if(company!=null && company!='')
			invtRec.setFieldValue('custrecord_ebiz_inv_company', company);
		if(uomlevel!=null && uomlevel!='')
			invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);

		if(lot!=null && lot!='')
			invtRec.setFieldText('custrecord_ebiz_inv_lot', lot);

		var filter=new Array();
		if(item!=null&&item!="")
			filter[0]=new nlobjSearchFilter("internalid",null,"anyof",item);

		var column=new Array();
		column[0]=new nlobjSearchColumn("itemid");
		column[1]=new nlobjSearchColumn("description");
		var Itemdescription='';
		var getItem="";
		var searchresitem=nlapiSearchRecord("item",null,filter,column);
		if(searchresitem!=null&&searchresitem!="")
		{
//			getItem=searchresitem[0].getValue("itemid");
			Itemdescription=searchresitem[0].getValue("description");
		}
		nlapiLogExecution("ERROR","Itemdescription",Itemdescription);
		invtRec.setFieldValue('custrecord_ebiz_itemdesc',Itemdescription.substring(0, 50));

		if(lot!="" && lot!=null)
		{

			var filterspor = new Array();
			filterspor.push(new nlobjSearchFilter('name', null, 'is', lot));
			if(whloc!=null && whloc!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whloc));
			if(item!=null && item!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', item));

			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

			var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);

			if(receiptsearchresults==null || receiptsearchresults=='')

			{
				var filterspor1 = new Array();
				filterspor1.push(new nlobjSearchFilter('name', null, 'is', lot));
				//if(whLocation!=null && whLocation!="")
				//filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));
				if(item!=null && item!="")
					filterspor1.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', item));

				var column1=new Array();
				column1[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
				column1[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

				var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor1,column1);

			}
			if(receiptsearchresults!=null)
			{
				var getlotnoid= receiptsearchresults[0].getId();
				nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
				var expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
				var vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
				invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifodate);
				nlapiLogExecution('DEBUG', 'expdate', expdate);
				nlapiLogExecution('DEBUG', 'vfifodate', vfifodate);
			}

		}

		if(company!=null && company!='')
			invtRec.setFieldValue('custrecord_ebiz_inv_company', company);
		if(uomlevel!=null && uomlevel!='')
			invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);

		invtRec.setFieldValue('custrecord_ebiz_serialnumbers', serialNo);

		var invtrecid = nlapiSubmitRecord(invtRec, false, true);
	}

	nlapiLogExecution('DEBUG', 'Out of createInventory');
}

function updateOpenTask(taskid,reversalqty,taskactqty,taskweight,taskcube)
{
	nlapiLogExecution('DEBUG', 'Into updateOpenTask');

	var str = 'taskid. = ' + taskid + '<br>';
	str = str + 'reversalqty. = ' + reversalqty + '<br>';
	str = str + 'taskactqty. = ' + taskactqty + '<br>';	
	str = str + 'taskweight. = ' + taskweight + '<br>';	
	str = str + 'taskcube. = ' + taskcube + '<br>';	

	var taskremqty=parseFloat(taskactqty)-parseFloat(reversalqty);

	str = str + 'taskremqty. = ' + taskremqty + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var fields = new Array();
	var values = new Array();

	fields.push('custrecord_notes');
	fields.push('custrecord_reversalqty');
	fields.push('custrecord_act_qty');
	fields.push('custrecord_total_weight');
	fields.push('custrecord_totalcube');

	values.push('Updated by Pick Reversal process');
	values.push(parseFloat(reversalqty).toFixed(4));
	values.push(parseFloat(taskremqty).toFixed(4));
	values.push(parseFloat(taskweight).toFixed(4));
	values.push(parseFloat(taskcube).toFixed(4));

	if(parseFloat(taskremqty)<1)
	{
		fields.push('custrecord_wms_status_flag');
		values.push('29');
		fields.push('custrecord_ebiz_nsconfirm_ref_no');
		values.push('');
	}

	nlapiSubmitField('customrecord_ebiznet_trn_opentask', taskid, fields, values);

	nlapiLogExecution('DEBUG', 'Out of updateOpenTask');
}

function updateFulfillmentOrder(foid,ordlineno,taskqty)
{
	nlapiLogExecution('DEBUG', 'Into updateFulfillmentOrder');

	var str = 'foid. = ' + foid + '<br>';
	var str = 'ordlineno. = ' + ordlineno + '<br>';
	var str = 'taskqty. = ' + taskqty + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var pickgenqty=0;
	var pickqty=0;
	var linestatusflag;

	var foTransaction = nlapiLoadRecord('customrecord_ebiznet_ordline', foid);

	if(foTransaction!=null && foTransaction!='')
	{
		pickgenqty = foTransaction.getFieldValue('custrecord_pickgen_qty');
		pickqty = foTransaction.getFieldValue('custrecord_pickqty');
		linestatusflag=foTransaction.getFieldValue('custrecord_linestatus_flag');
		var vQty = 0;

		nlapiLogExecution('DEBUG', 'pickqty', pickqty);
		nlapiLogExecution('DEBUG', 'pickgenqty', pickgenqty);
		if (isNaN(pickgenqty))
		{
			pickgenqty = 0;
		}

		if (isNaN(pickqty))
		{
			pickqty = 0;
		}

//		if(parseInt(pickgenqty)== parseInt(pickqty))
//		{
//		foTransaction.setFieldValue('custrecord_pickgen_qty', (parseFloat(pickgenqty)-parseFloat(taskqty)).toFixed(4));
//		}
//		else
//		{
//		foTransaction.setFieldValue('custrecord_pickgen_qty', vQty);
//		}
		if(linestatusflag!='25')
		{
			foTransaction.setFieldValue('custrecord_pickgen_qty', (parseFloat(pickgenqty)-parseFloat(taskqty)).toFixed(4));
			foTransaction.setFieldValue('custrecord_pickqty', (parseFloat(pickqty)-parseFloat(taskqty)).toFixed(4));
		}


		if((parseFloat(pickqty)-parseFloat(taskqty)) <= 0 )
			foTransaction.setFieldValue('custrecord_linestatus_flag', '25');
		else
			foTransaction.setFieldValue('custrecord_linestatus_flag', '11');

		foTransaction.setFieldValue('custrecord_ebiz_wave', '');	
		foTransaction.setFieldValue('custrecord_linenotes1', 'Pick Reversal');

		nlapiSubmitRecord(foTransaction,false,true);
	}

	nlapiLogExecution('DEBUG', 'Out of updateFulfillmentOrder');
}

function updateFIFOdate()
{
	var filterslot = new Array();
	filterslot.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filterslot.push(new nlobjSearchFilter('custrecord_ebizexpirydate', null, 'isnotempty'));
	filterslot.push(new nlobjSearchFilter('custrecord_ebiznotes1', null, 'isempty'));

	var columnslot = new Array();
	columnslot[0] = new nlobjSearchColumn('custrecord_ebizfifodate');
	columnslot[1] = new nlobjSearchColumn('custrecord_ebizexpirydate');
	columnslot[2] = new nlobjSearchColumn('custrecord_ebizsku').setSort();
	columnslot[3] = new nlobjSearchColumn('custrecord_ebizlotbatch').setSort();

	var lotSearchResults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterslot, columnslot);

	for(var i=0;lotSearchResults!=null && i<lotSearchResults.length;i++)
	{
		var sku=lotSearchResults[i].getValue('custrecord_ebizsku');
		var lot=lotSearchResults[i].getId();
		var exprdate=lotSearchResults[i].getValue('custrecord_ebizexpirydate');

		if(exprdate!=null && exprdate!='')
		{

			var filters = new Array();
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', lot));
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', sku));

			var invtSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, null);

			for(var g=0;invtSearchResults!=null && g<invtSearchResults.length;g++)
			{
				var fields = new Array();
				var values = new Array();

				fields.push('custrecord_ebiz_inv_fifo');
				fields.push('custrecord_ebiz_expdate');
				fields.push('custrecord_ebiz_inv_note2');
				fields.push('custrecord_ebiz_callinv');

				values.push(exprdate);
				values.push(exprdate);
				values.push('FIFO date updated with LOT expiry date');
				values.push('N');

				nlapiSubmitField('customrecord_ebiznet_createinv', invtSearchResults[g].getId(), fields, values);

			}
		}

		nlapiSubmitField('customrecord_ebiznet_batch_entry', lotSearchResults[i].getId(), 'custrecord_ebiznotes1', 'INVT Update');
	}	
}

function geteBizItemDimensionsArray(itemidArray)
{
	var searchRec = new Array();
	if(itemidArray!=null && itemidArray!='' && itemidArray.length>0)
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemidArray));
		filter.push(new nlobjSearchFilter('custrecord_ebiznsuom', null, 'noneof', '@NONE@'));
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
		column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
		column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
		column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
		column[4] = new nlobjSearchColumn('custrecord_ebizitemdims') ;
		searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
	}

	return searchRec;

}

function getItemCubeandWeight(item,uomlevel)
{
	nlapiLogExecution('DEBUG', 'Into getItemCubeandWeight');

	var str = 'item. = ' + item + '<br>';
	var str = 'uomlevel. = ' + uomlevel + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var searchRec = new Array();

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', item));
	filter.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', uomlevel));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizweight') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizcube') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizqty') ;

	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	nlapiLogExecution('DEBUG', 'Out of getItemCubeandWeight');
	return searchRec;

}

function getIndividualItemDetails(allLocnGroups, fulfilmentItem,allocationStrategy,pfBinLocationgroupId,allinventorySearchResults)
{
	nlapiLogExecution('DEBUG', 'Into getIndividualItemDetails');

//	var str = 'allLocnGroups. = ' + allLocnGroups + '<br>';
//	str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';	
//	str = str + 'allocationStrategy. = ' + allocationStrategy + '<br>';	
//	str = str + 'pfBinLocationgroupId. = ' + pfBinLocationgroupId + '<br>';	

//	nlapiLogExecution('DEBUG', 'getIndividualItemDetails Parameters', str);

	var inventoryresults = new Array();

	if(allinventorySearchResults!=null && allinventorySearchResults!='')
	{
		//nlapiLogExecution('DEBUG', 'allinventorySearchResults length', allinventorySearchResults.length);

		for(var s = 0; s < allinventorySearchResults.length; s++)
		{
			var invtlocgroupid = allinventorySearchResults[s].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
			var invtitem = allinventorySearchResults[s].getValue('custrecord_ebiz_inv_sku');

			if(invtitem==fulfilmentItem)
			{
				var vRecAdded='F';
				if(allLocnGroups!=null && allLocnGroups!='' && allLocnGroups.length>0)
				{
					for(var t = 0; t < allLocnGroups.length; t++)
					{
						var locgroupid = allLocnGroups[t];

						if(invtlocgroupid==locgroupid)
						{
							inventoryresults.push(allinventorySearchResults[s]);
							vRecAdded='T';
						}
					}
				}

				if((pfBinLocationgroupId!=null && pfBinLocationgroupId == invtlocgroupid && vRecAdded == 'F'))
				{
					inventoryresults.push(allinventorySearchResults[s]);
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getIndividualItemDetails');
	return inventoryresults;
}

function updateInventoryRecordBulk(inventoryRecArr,invtparent){

	nlapiLogExecution('DEBUG', 'Into updateInventoryRecordBulk...New');

	if(inventoryRecArr != null && inventoryRecArr != '' && inventoryRecArr.length>0)
	{
		nlapiLogExecution('DEBUG', 'Inventory Update Parameters New', inventoryRecArr.length);

		for(var i=0;i<inventoryRecArr.length;i++)
		{
			nlapiLogExecution('DEBUG', 'Inventory Update Parameters New Arr Main', inventoryRecArr[i]);
			nlapiLogExecution('DEBUG', 'Inventory Update Parameters New Arr', inventoryRecArr[i][0]);

			var invrecord = inventoryRecArr[i][0];

			var scount=1;
			LABL1: for(var j=0;j<scount;j++)
			{	
				nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', j);
				try
				{
					var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invrecord.getId());
					var alreadyallocqty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
					//case 20125987 start addded condition (null,'')
					if (isNaN(alreadyallocqty) || alreadyallocqty ==''|| alreadyallocqty == null)
					{//case 20125987 end
						alreadyallocqty = 0;
					}
					// case no 20126835ï¿½ start
					var vAllocatingQty=inventoryRecArr[i][1];
					// case no 20126835ï¿½ end
					if (isNaN(vAllocatingQty))
					{
						vAllocatingQty = 0;
					}
					var vTotAllocQty=parseInt(alreadyallocqty)+parseInt(vAllocatingQty);

					//var totalallocqty=parseInt(allocationQuantity)+parseInt(alreadyallocqty);
					inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseInt(vTotAllocQty));
					inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
					inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

					nlapiSubmitRecord(inventoryTransaction,false,true);
				}
				catch(ex)
				{
					var exCode=ex.getCode(); 	 
					if(exCode=='CUSTOM_RECORD_COLLISION')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}


			/*invtparent.selectNewLineItem('recmachcustrecord_ebiz_inv_parent');

			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'id', invrecord.getId());
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','name', invrecord.getValue('name'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_binloc', invrecord.getValue('custrecord_ebiz_inv_binloc'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_lp', invrecord.getValue('custrecord_ebiz_inv_lp'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_sku', invrecord.getValue('custrecord_ebiz_inv_sku'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_sku_status', invrecord.getValue('custrecord_ebiz_inv_sku_status'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_packcode', invrecord.getValue('custrecord_ebiz_inv_packcode'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_qty', invrecord.getValue('custrecord_ebiz_inv_qty'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_inv_ebizsku_no', invrecord.getValue('custrecord_ebiz_inv_sku'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_qoh', invrecord.getValue('custrecord_ebiz_qoh'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_itemdesc', invrecord.getValue('custrecord_ebiz_itemdesc'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_loc', invrecord.getValue('custrecord_ebiz_inv_loc'));
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_invttasktype', invrecord.getValue('custrecord_invttasktype')); // PUTW.
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_wms_inv_status_flag', 19); // STORAGE	
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_callinv', 'N');
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_displayfield', 'N');
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_fifo', invrecord.getValue('custrecord_ebiz_inv_fifo'));	
			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_updated_user_no',  nlapiGetContext().getUser());

			invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_alloc_qty', parseInt(inventoryRecArr[i][1]));

			if(invrecord.getValue('custrecord_ebiz_uomlvl')!=null && invrecord.getValue('custrecord_ebiz_uomlvl')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_uomlvl', invrecord.getValue('custrecord_ebiz_uomlvl'));

			if(invrecord.getValue('custrecord_ebiz_inv_lot')!=null && invrecord.getValue('custrecord_ebiz_inv_lot')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_lot', invrecord.getValue('custrecord_ebiz_inv_lot'));

			if(invrecord.getValue('custrecord_ebiz_inv_company')!=null && invrecord.getValue('custrecord_ebiz_inv_company')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_company', invrecord.getValue('custrecord_ebiz_inv_company'));

			if(invrecord.getValue('custrecord_ebiz_inv_adjusttype')!=null && invrecord.getValue('custrecord_ebiz_inv_adjusttype')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_adjusttype', invrecord.getValue('custrecord_ebiz_inv_adjusttype'));

			if(invrecord.getValue('custrecord_ebiz_model')!=null && invrecord.getValue('custrecord_ebiz_model')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_model', invrecord.getValue('custrecord_ebiz_model'));

			if(invrecord.getValue('custrecord_outboundinvlocgroupid')!=null && invrecord.getValue('custrecord_outboundinvlocgroupid')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_outboundinvlocgroupid', invrecord.getValue('custrecord_outboundinvlocgroupid'));

			if(invrecord.getValue('custrecord_inboundinvlocgroupid')!=null && invrecord.getValue('custrecord_inboundinvlocgroupid')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_inboundinvlocgroupid', invrecord.getValue('custrecord_inboundinvlocgroupid'));

			if(invrecord.getValue('custrecord_ebiz_cycl_count_hldflag')!=null && invrecord.getValue('custrecord_ebiz_cycl_count_hldflag')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_cycl_count_hldflag', invrecord.getValue('custrecord_ebiz_cycl_count_hldflag'));

			if(invrecord.getValue('custrecord_ebiz_invholdflg')!=null && invrecord.getValue('custrecord_ebiz_invholdflg')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_invholdflg', invrecord.getValue('custrecord_ebiz_invholdflg'));

			if(invrecord.getValue('custrecord_ebiz_inv_note1')!=null && invrecord.getValue('custrecord_ebiz_inv_note1')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_note1', invrecord.getValue('custrecord_ebiz_inv_note1'));

			if(invrecord.getValue('custrecord_ebiz_inv_note2')!=null && invrecord.getValue('custrecord_ebiz_inv_note2')!='')
				invtparent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_note2', invrecord.getValue('custrecord_ebiz_inv_note2'));


			invtparent.commitLineItem('recmachcustrecord_ebiz_inv_parent');	*/

			nlapiLogExecution('DEBUG', 'Out of updateInventoryRecordBulk...');
		}
	}
}

/**
 * Remove duplicates from an array
 * @param arrayName
 * @returns {Array}
 */
function removeDuplicateElement(arrayName){
	var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < newArray.length; j++) {
			if (newArray[j] == arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}

//The below function is used to clear allocations from inventory if there are no open picks and replens.
function clearallocations()
{
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_alloc_qty', null, 'greaterthan', 0));	
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));//Storage,Putaway

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');

	var inventorySearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	for(var i = 0; i < inventorySearchResults.length; i++){

		var invlp = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lp');
		var invbinloc = inventorySearchResults[i].getValue('custrecord_ebiz_inv_binloc');
		var invallocqty = inventorySearchResults[i].getValue('custrecord_ebiz_alloc_qty');
		var invtrecid = inventorySearchResults[i].getId();

		var otfilters = new Array();
		otfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3,8]));	//PICK & RPLN
		otfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','20']));
		otfilters.push(new nlobjSearchFilter('custrecord_invref_no', null, 'equalto', invtrecid));
		//otfilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', invbinloc));

		var otcolumns = new Array();
		otcolumns [0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
		otcolumns [1] = new nlobjSearchColumn('custrecord_lpno',null,'group');
		otcolumns [2] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		otcolumns [3] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');

		var otSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, otfilters , otcolumns );

		if(otSearchResults != null && otSearchResults !='')
		{
			for(var j= 0; j< otSearchResults.length; j++){

				var otexpqty = otSearchResults[j].getValue('custrecord_expe_qty',null,'sum');

				if(parseFloat(invallocqty)>parseFloat(otexpqty))
				{
					/*var fields = new Array();
					var values = new Array();

					fields[0] = 'custrecord_ebiz_alloc_qty';
					fields[1] = 'custrecord_ebiz_inv_note2';
					fields[2] = 'custrecord_ebiz_callinv';

					values[0] = parseFloat(otexpqty);
					values[1] = 'Updated Manually on 02/10/2013 as part of clearing invalid allocations';
					values[2] = 'N';

					nlapiSubmitField('customrecord_ebiznet_createinv', invtrecid, fields, values);*/

					var scount=1;
					LABL1: for(var j=0;j<scount;j++)
					{	
						nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', j);
						try
						{
							var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtrecid);
							transaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(otexpqty));
							transaction.setFieldValue('custrecord_ebiz_inv_note2', 'Updated Manually on 02/10/2013 as part of clearing invalid allocations');						
							transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
							nlapiSubmitRecord(transaction, false, true);
							nlapiLogExecution('DEBUG', 'Cyclecount generated for inv ref ',invtrecid);


						}
						catch(ex)
						{
							var exCode='CUSTOM_RECORD_COLLISION'; 

							if (ex instanceof nlobjError) 
							{	
								wmsE=ex.getCode() + '\n' + ex.getDetails();
								exCode=ex.getCode();
							}
							else
							{
								wmsE=ex.toString();
								exCode=ex.toString();
							}				 
							if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
							{ 
								scount=scount+1;
								continue LABL1;
							}
							else break LABL1;
						}
					}



				}
			}

		}
		else
		{
			/*var fields = new Array();
			var values = new Array();

			fields[0] = 'custrecord_ebiz_alloc_qty';
			fields[1] = 'custrecord_ebiz_inv_note2';
			fields[2] = 'custrecord_ebiz_callinv';

			values[0] = parseFloat(0);
			values[1] = 'Updated Manually on 02/10/2013 as part of clearing invalid allocations';
			values[2] = 'N';

			nlapiSubmitField('customrecord_ebiznet_createinv', invtrecid, fields, values);*/

			var scount=1;
			LABL1: for(var j=0;j<scount;j++)
			{	
				nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', j);
				try
				{
					var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtrecid);
					transaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(0));
					transaction.setFieldValue('custrecord_ebiz_inv_note2', 'Updated Manually on 02/10/2013 as part of clearing invalid allocations');						
					transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
					nlapiSubmitRecord(transaction, false, true);
					nlapiLogExecution('DEBUG', 'Cyclecount generated for inv ref ',invtrecid);


				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 

					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					}				 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}


		}
	}

	var s=10;
}

function clearHoldFlags()
{
	var Filter = new Array();
	Filter[0] = new nlobjSearchFilter('custrecord_ebiz_cycl_count_hldflag', null, 'is', 'T');		

	var Column = new Array();
	Column[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	Column[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	Column[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	Column[3] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	Column[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	Column[5] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	Column[6] = new nlobjSearchColumn('custrecord_ebiz_invholdflg');

	var SearchRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, Filter, Column);

	if(SearchRecord != null && SearchRecord != "")
	{

		for( var count = 0; count < SearchRecord.length; count++)
		{
			var vId = SearchRecord[count].getId();

			var values = new Array();
			var fields = new Array();

			fields[0] = 'custrecord_ebiz_cycl_count_hldflag'; 
			values[0] = 'F';

			fields[1] = 'custrecord_ebiz_invholdflg'; 
			values[1] = 'F';

			var updatefields = nlapiSubmitField('customrecord_ebiznet_createinv', vId, fields, values);

		}
	}

	var s=10;
}
function getAvailQtyFromInventory(inventorySearchResults, binLocation, allocatedInv,dowhlocation,fulfilmentItem,
		fulfillmentItemStatus,samelot,latestexpirydt,shelflife){

	nlapiLogExecution('DEBUG','Into getAvailQtyFromInventory');
	var binLocnInvDtls = new Array();
	var matchFound = false;

	nlapiLogExecution('DEBUG','inventorySearchResults',inventorySearchResults);
	if(inventorySearchResults != null && inventorySearchResults.length > 0){
		nlapiLogExecution('DEBUG','inventorySearchResults length',inventorySearchResults.length);
		for(var i = 0; i < inventorySearchResults.length; i++){
			//if(!matchFound){
			invBinLocnId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_binloc');
			var InvWHLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
			var invItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
			var itemStatus = inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku_status');
			var invExprDt = inventorySearchResults[i].getText('custrecord_ebiz_expdate');

			var str = 'dowhlocation. = ' + dowhlocation + '<br>';
			str = str + 'InvWHLocation. = ' + InvWHLocation + '<br>';	
			str = str + 'invBinLocnId. = ' + invBinLocnId + '<br>';
			str = str + 'binLocation. = ' + binLocation + '<br>';
			str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';
			str = str + 'itemStatus. = ' + itemStatus + '<br>';
			str = str + 'invExprDt. = ' + invExprDt + '<br>';
			str = str + 'invItem. = ' + invItem + '<br>';
			str = str + 'samelot. = ' + samelot + '<br>';
			str = str + 'latestexpirydt. = ' + latestexpirydt + '<br>';
			str = str + 'shelflife. = ' + shelflife + '<br>';

			nlapiLogExecution('DEBUG', 'Inventory & FO Details', str);

			if((dowhlocation==InvWHLocation) && (parseInt(invBinLocnId) == parseInt(binLocation)) && (fulfilmentItem==invItem)
					&& (itemStatus==fulfillmentItemStatus)){

				//If Same Lot is 'T', check for the inventory with inventory expiry date 
				//greater than or equal to latest expiry date (i.e the lot which is shipped recently).
				//If Same Lot is 'F', check for the inventory with inventory expiry date 
				//greater than latest expiry date

				var isValidRec = validateShelflife(samelot,latestexpirydt,shelflife,invExprDt);

				if(isValidRec!='F')
				{
					var actualQty = inventorySearchResults[i].getValue('custrecord_ebiz_qoh');
					var inventoryAvailableQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_avl_qty');
					var allocationQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_alloc_qty');
					var packCode = inventorySearchResults[i].getValue('custrecord_ebiz_inv_packcode');					
					var whLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
					var fromLP = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lp');
					var inventoryItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
					var fromLot = inventorySearchResults[i].getText('custrecord_ebiz_inv_lot');
					var expiryDate = inventorySearchResults[i].getValue('custrecord_ebiz_expdate');

					var str = 'Invt QOH. = ' + actualQty + '<br>';
					str = str + 'Alloc Qty. = ' + allocationQuantity + '<br>';	
					str = str + 'Avail Qty. = ' + inventoryAvailableQuantity + '<br>';
					str = str + 'packCode. = ' + packCode + '<br>';
					str = str + 'itemStatus. = ' + itemStatus + '<br>';
					str = str + 'whLocation. = ' + whLocation + '<br>';
					str = str + 'fromLP. = ' + fromLP + '<br>';
					str = str + 'inventoryItem. = ' + inventoryItem + '<br>';
					str = str + 'fromLot. = ' + fromLot + '<br>';
					str = str + 'expiryDate. = ' + expiryDate + '<br>';

					nlapiLogExecution('DEBUG', 'Inventory Details', str);

					var recordId = inventorySearchResults[i].getId();

					// Get allocation already done
					var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,inventoryItem,fromLot);

					nlapiLogExecution('DEBUG', 'alreadyAllocQty', alreadyAllocQty);

					// Adjusting for allocations already done
					inventoryAvailableQuantity = parseInt(inventoryAvailableQuantity) - parseInt(alreadyAllocQty);

					nlapiLogExecution('DEBUG', 'inventoryAvailableQuantity', inventoryAvailableQuantity);

					var currentRow = [i, actualQty, inventoryAvailableQuantity, allocationQuantity, packCode,
					                  itemStatus, whLocation, fromLP, recordId,inventoryItem,fromLot,expiryDate];

					binLocnInvDtls.push(currentRow);

					matchFound = true;
				}
			}				
			//}
		}
	}

	nlapiLogExecution('DEBUG','Out of getAvailQtyFromInventory');
	return binLocnInvDtls;
}

function getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,item,lot){
	nlapiLogExecution('DEBUG', 'Into getAlreadyAllocatedInv',allocatedInv);
	var alreadyAllocQty = 0;
	if(allocatedInv != null && allocatedInv.length > 0){
		for(var i = 0; i < allocatedInv.length; i++){

			if(fromLP == allocatedInv[i][2] && binLocation==allocatedInv[i][0] && item==allocatedInv[i][3] && lot==allocatedInv[i][4])
				alreadyAllocQty = parseFloat(alreadyAllocQty) + parseFloat(allocatedInv[i][1]);
		}
	}
	nlapiLogExecution('DEBUG', 'Out of getAlreadyAllocatedInv');
	return alreadyAllocQty;
}

var _MS_PER_DAY = 1000 * 60 * 60 * 24;

function dateDiffInDays1(currDate, invExprDt) {
	nlapiLogExecution('DEBUG', 'Into dateDiffInDays1');

	var str = 'currDate = ' + currDate + '<br>';
	str = str + 'invExprDt. = ' + invExprDt + '<br>';

	nlapiLogExecution('DEBUG', 'dateDiffInDays parameters',str);

//	Discard the time and time-zone information.
//	var utc1 = Date.UTC(currDate.getFullYear(), currDate.getMonth(), currDate.getDate());
//	var utc2 = Date.UTC(invExprDt.getFullYear(), invExprDt.getMonth(), invExprDt.getDate());

	var currDatearr = currDate.split('/');
	var invExprDtarr = invExprDt.split('/');

	var utc1 = Date.UTC(currDatearr[2] , currDatearr[0], currDatearr[1]);
	var utc2 = Date.UTC(invExprDtarr[2] , invExprDtarr[0], invExprDtarr[1]);

	var dtDiff = Math.floor((utc2 - utc1) / _MS_PER_DAY);

	nlapiLogExecution('DEBUG', 'Out of dateDiffInDays1',dtDiff);

	return dtDiff;
}


function validateShelflife(sameLot,latestExp,shelflife,invtExp)
{
	nlapiLogExecution('DEBUG', 'Into validateShelflife');
	try{
		var str = 'sameLot = ' + sameLot + '<br>';
		str = str + 'latestExp. = ' + latestExp + '<br>';	
		str = str + 'shelflife. = ' + shelflife + '<br>';	
		str = str + 'invtExp. = ' + invtExp + '<br>';	

		nlapiLogExecution('DEBUG', 'validateShelflife parameters',str);
		var isValid='F';

		if(invtExp==null || invtExp=='')
			return 'T';

		if((sameLot!=null && sameLot!='') && (latestExp!=null && latestExp!=''))
		{	
			//if(sameLot=='T' && invtExp>=latestExp)
			if(sameLot=='T' && dateDiffInDays1(latestExp,invtExp)>=0)
			{
				if(shelflife!=null && shelflife!=''&&shelflife!='F')
				{
					if(dateDiffInDays1(DateStamp(),invtExp)>parseFloat(shelflife))
					{
						isValid='T';
					}
				}
				else
				{
					isValid='T';
				}
			}
			//else if(sameLot=='F' && invtExp>latestExp)
			else if(sameLot=='F' && dateDiffInDays1(latestExp,invtExp)>=0)
			{
				if(shelflife!=null && shelflife!='' && shelflife!='F')
				{
					if(dateDiffInDays1(DateStamp(),invtExp)>parseFloat(shelflife))
					{
						isValid='T';
					}
				}
				else
				{
					isValid='T';
				}
			}
		}
		else
		{
			if(shelflife!=null && shelflife!=''&& shelflife!='F')
			{
				if(dateDiffInDays1(DateStamp(),invtExp)>parseFloat(shelflife))
				{
					isValid='T';
				}
			}
			else
			{
				isValid='T';
			}
		}

		return isValid;

		nlapiLogExecution('DEBUG', 'Out of validateShelflife',isValid);
	}
	catch(exp2)
	{
		nlapiLogExecution('DEBUG', 'exp2',exp2);
	}
}


function getZoneLocnGroupsListforRepln(zoneList,DOwhLocation){

	var zoneLocnGroupList = new Array();
	var LocationGroup = new Array();
	var currentRow = new Array();
	// Get the list of replen zones
	if (zoneList == null){
		zoneList = getReplenishmentZoneId();
	}

	var listOfZones = zoneList;
	nlapiLogExecution('DEBUG','List Of Zones',listOfZones);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(listOfZones != null && listOfZones != '')
		filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', listOfZones));
	if(DOwhLocation!=null && DOwhLocation!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', DOwhLocation));



	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_locgroup_no');
	columns[1] = new nlobjSearchColumn('custrecord_zone_seq');
	columns[2] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
	columns[1].setSort();

	var zoneGroups = new nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);

	nlapiLogExecution('DEBUG','Zone Groups',zoneGroups.length);

	if(zoneGroups != null && zoneGroups.length > 0){
		for(var i = 0; i < zoneGroups.length; i++){
			var vLocationgrp=zoneGroups[i].getValue('custrecord_locgroup_no');

			if(LocationGroup.indexOf(vLocationgrp) == -1)
			{
				var vZondId =zoneGroups[i].getValue('custrecordcustrecord_putzoneid');
				currentRow =[vLocationgrp,vZondId];

				zoneLocnGroupList.push(currentRow);
			}
		}
	}

	return zoneLocnGroupList;
}

