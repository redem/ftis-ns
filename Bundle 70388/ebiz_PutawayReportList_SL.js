/***************************************************************************

                                                  eBizNET Solutions Inc 
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_PutawayReportList_SL.js,v $
 *  $Revision: 1.2.4.1.8.2.4.1 $
 *  $Date: 2015/03/17 12:00:19 $
 *  $Author: schepuri $
 *  $Name: t_eBN_2015_1_StdBundle_1_14 $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: ebiz_PutawayReportList_SL.js,v $
 *  Revision 1.2.4.1.8.2.4.1  2015/03/17 12:00:19  schepuri
 *  case# 201412046
 *
 *  Revision 1.2.4.1.8.2  2013/09/11 15:23:51  rmukkera
 *  Case# 20124376
 *
 *
 ****************************************************************************/
function ebiznet_PutawayReport_list_SL(request, response){
	var list = nlapiCreateList('Putaway Report List');
	list.setStyle('normal');
	//  list.setScript(239);    
	var column = list.addColumn('name', 'text', 'PO', 'LEFT');
	column.setURL(nlapiResolveURL('SUITELET', 'customscript_assignputaway', 'customdeploy_ebizassignputaway'));
	column.addParamToURL('custparam_po', 'custrecord_ebiz_cntrl_no', true);
	column.addParamToURL('custparam_povalue', 'name', true);


	// list.addColumn('custrecord_ebiz_cntrl_no', 'text', 'Line#', 'LEFT');
    // case# 201412046 start
	//list.addColumn('custrecord_sku', 'text', 'Item', 'LEFT');// 
	list.addColumn('custrecord_sku_display', 'text', 'Item', 'LEFT');
	// case# 201412046 end
	list.addColumn('custrecord_lpno', 'text', 'LP #', 'LEFT');
	list.addColumn('custrecord_transport_lp', 'text', 'Cart LP #', 'LEFT');


	/*	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['2']);
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['2','6']);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_sku_status');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_lpno');
	columns[5] = new nlobjSearchColumn('custrecord_transport_lp');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);*/

	var searchresults=getOrdersList(0);
	var po = new Object();
	list.addRows(searchresults);
	nlapiLogExecution('DEBUG', 'searchresults.length',searchresults.length);
	nlapiLogExecution('ERROR', 'searchresults.length',searchresults.length);

	/*  
    for (var j = 0; searchresults != null && j < searchresults.length; j++) {

        po["name"] = searchresults[j].getValue('name');
        //po["custrecord_sku"] = searchresults[j].getValue('custrecord_sku');
        po["custrecord_sku"] = searchresults[j].getText('custrecord_sku');
        po["custrecord_lpno"] = searchresults[j].getValue('custrecord_lpno');
        po["custrecord_transport_lp"] = searchresults[j].getValue('custrecord_transport_lp');
        po["custrecord_ebiz_cntrl_no"] = searchresults[j].getValue('custrecord_ebiz_cntrl_no');



        list.addRow(po);
    }*/


	nlapiLogExecution('DEBUG', 'END OF LIST');

	response.writePage(list);

}
var orderListArray=new Array();
function getOrdersList(maxno)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['2']);
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['2','6']);

	if(maxno>0)
	{
		filters[2] = new nlobjSearchFilter('id', null, 'greaterthan', maxno);	
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_sku_status');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_lpno');
	columns[5] = new nlobjSearchColumn('custrecord_transport_lp');
	columns[6] = new nlobjSearchColumn('id').setSort();
	var orderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	nlapiLogExecution('DEBUG', 'orderList', orderList.length);	

	if( orderList!=null && orderList!='')
	{
		if(orderList.length>=1000)
		{ 
			for(var i = 0; i < orderList.length; i++)
			{
				orderListArray.push(orderList[i]); 
			}

			var maxno=orderList[orderList.length-1].getValue('id');
			nlapiLogExecution('ERROR', 'maxno', maxno);	
			if(maxno!=null && maxno!='')
				getOrdersList(maxno);	
		}
		else
		{
			for(var i = 0; i < orderList.length; i++)
			{
				orderListArray.push(orderList[i]); 
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getOrdersList');	
	return orderListArray;
}
