/***************************************************************************
                                                                 
   eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_invoice_syncReport.js,v $
*  $Revision: 1.1.14.1 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_invoice_syncReport.js,v $
*  Revision 1.1.14.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function ebiznet_InventorySync_UI(request, response){
    if (request.getMethod() == 'GET') {
        var form = nlapiCreateForm('Inventory Report');
        
 var sublist = form.addSubList("custpage_items", "list", "Inventory Report");
        // var host = request.getURL().substring(0, (request.getURL().indexOf('.com') + 4));
        
        
        // Creating the sublist.
        // sublist.addField("custpage_serialno", "text", "Sno").setDefaultValue('1');
        sublist.addField("custpage_sku", "text", "Item");
        sublist.addField("custpage_skudesc", "text", "Item Desc");        
        sublist.addField("custpage_qoh_nswms", "text", "QOH(WMS)");
        sublist.addField("custpage_qoh_ns", "text", "QOH(Netsuite)");
        sublist.addField("custpage_variance", "text", "Variance");
        
      
        var filters = new Array();
        filters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku',null,'anyof',[313] );

        var columns = new Array();
              columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku',null,'group'); //Item
             columns[1] = new nlobjSearchColumn('custrecord_ebiz_avl_qty',null,'sum'); 
       columns[2] = new nlobjSearchColumn('quantityavailable','custrecord_ebiz_inv_sku','avg');
       columns[3] = new nlobjSearchColumn('custitem_ebizdescriptionitems','custrecord_ebiz_inv_sku','group'); 


       var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, null, columns  );

       for(var i=0;searchresults!=null&&i<searchresults.length ;i++)
       {
    	   nlapiLogExecution('ERROR', 'searchresults.length',searchresults.length);
       var searchresult=searchresults[i];
       var vitemname = searchresult.getText('custrecord_ebiz_inv_sku',null,'group');
       var vitemdesc = searchresult.getValue('custitem_ebizdescriptionitems','custrecord_ebiz_inv_sku','group');
       var vnswmsqty = searchresult.getValue('custrecord_ebiz_avl_qty',null,'sum');
       var vnettsuiteqty  =searchresult.getValue('quantityavailable','custrecord_ebiz_inv_sku','avg');
       var vvariance=vnettsuiteqty-vnswmsqty;
       nlapiLogExecution('ERROR', 'vitemname',vitemname);
       nlapiLogExecution('ERROR', 'vnswmsqty',vnswmsqty);
       nlapiLogExecution('ERROR', 'vnettsuiteqty',vnettsuiteqty);
       nlapiLogExecution('ERROR', 'vitemdesc',vitemdesc);
       
       form.getSubList('custpage_items').setLineItemValue('custpage_sku', i + 1, vitemname);
       form.getSubList('custpage_items').setLineItemValue('custpage_skudesc', i + 1, vitemdesc);
       form.getSubList('custpage_items').setLineItemValue('custpage_qoh_nswms', i + 1, vnswmsqty);
       form.getSubList('custpage_items').setLineItemValue('custpage_qoh_ns', i + 1, vnettsuiteqty);
       form.getSubList('custpage_items').setLineItemValue('custpage_variance', i + 1, vvariance.toString());
       nlapiLogExecution('ERROR', 'adding to form');
       }
      
       
       response.writePage(form);
    }
    else
    	{
    	
    	}
}
