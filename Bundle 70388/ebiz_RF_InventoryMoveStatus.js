/***************************************************************************
 eBizNET Solutions               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMoveStatus.js,v $
 *     	   $Revision: 1.2.2.9.4.4.4.11.2.2 $
 *     	   $Date: 2015/10/29 06:46:21 $
 *     	   $Author: aanchal $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS	*
 *
 * 
 *
 *****************************************************************************/
function InventoryMoveToNewStatus(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var getNewLP = request.getParameter('custparam_newLP');
		var getMoveQuantity = request.getParameter('custparam_moveqty');
		var getMoveQty = request.getParameter('custparam_moveqty');
		var getBinLocationId = request.getParameter('custparam_imbinlocationid');
		var getBinLocation = request.getParameter('custparam_imbinlocationname');
		var getBatchNo = request.getParameter('custparam_batch');  
		var getItemId = request.getParameter('custparam_imitemid');
		var getItem = request.getParameter('custparam_imitem');
		var getTotQuantity = request.getParameter('custparam_totquantity');
		var getAvailQuantity = request.getParameter('custparam_availquantity');    
		var getUOMId = request.getParameter('custparam_uomid');
		var getUOM = request.getParameter('custparam_uom');
		var getStatus = request.getParameter('custparam_status');
		var getStatusId = request.getParameter('custparam_statusid');
		var getLOTId = request.getParameter('custparam_lotid');
		var getLOTNo = request.getParameter('custparam_lot');  
		var getLPId = request.getParameter('custparam_lpid');
		var getLP = request.getParameter('custparam_lp');  
		var getItemType = request.getParameter('custparam_itemtype');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getInvtRecID=request.getParameter('custparam_invtrecid');
		var totqtymoved=request.getParameter('custparam_totqtymoveed');
		var locationId=request.getParameter('custparam_locationId');//
		var ItemDesc = request.getParameter('custparam_itemdesc');
		nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId'));
		var compId=request.getParameter('custparam_compid');//
		var fifodate=request.getParameter('custparam_fifodate');//

		var	tempBinLocationId=request.getParameter('custparam_hdntmplocation');
		var tempItemId=request.getParameter('custparam_hdntempitem');
		var tempLP=request.getParameter('custparam_hdntemplp');

		nlapiLogExecution('ERROR', 'getInvtRecID ',getInvtRecID);
		nlapiLogExecution('ERROR', 'fifodate ',fifodate);
		var ItemStatus;
		var itemStatusId;
		var itemStatusLoopCount = 0;
		var nextClicked;
		var statusoptionno=0;

		var itemStatusSearchResult=getItemStatusList(locationId);

		if(itemStatusSearchResult!=null && itemStatusSearchResult!='')
		{

			var itemStatusCount = itemStatusSearchResult.length;

			for (var i = 0; itemStatusSearchResult!=null && i < itemStatusSearchResult.length; i++) 
			{		
				itemStatus = itemStatusSearchResult[i].getValue('internalid');				
				nlapiLogExecution('ERROR', 'itemStatus', itemStatus);
				nlapiLogExecution('ERROR', 'getStatus', getStatus);
				nlapiLogExecution('ERROR', 'getStatusId', getStatusId);
				if(itemStatus==getStatusId)
				{
					statusoptionno=parseFloat(i) + 1;
				}
			}
		}

		if (request.getParameter('custparam_count') != null)
		{
			var itemStatusRetrieved = request.getParameter('custparam_count');
			nlapiLogExecution('ERROR', 'itemStatusRetrieved', itemStatusRetrieved);

			nextClicked = request.getParameter('custparam_nextclicked');
			nlapiLogExecution('ERROR', 'nextClicked', nextClicked);

			itemStatusCount = itemStatusCount - itemStatusRetrieved;
			nlapiLogExecution('ERROR', 'itemStatusCount', itemStatusCount);
		}
		else
		{
			var itemStatusRetrieved = 0;
			nlapiLogExecution('ERROR', 'itemStatusRetrieved', itemStatusRetrieved);
		}

		if (itemStatusCount > 10)
		{
			itemStatusLoopCount = 10;
		}
		else
		{
			itemStatusLoopCount = itemStatusCount;
		}


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		if( getLanguage == 'es_ES')
		{

			st0 = "";
			st1 = "TEMA DE ESTADO ";
			st2 = "ENTRAR SELECCI&#211;N:";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "Pr&#243;ximo";
		}
		else
		{
			st0 = "";
			st1 = "ITEM STATUS ";
			st2 = "ENTER SELECTION :";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "NEXT";
		}		

		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript'>";

		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		html = html + "function OnKeyDown_CL() ";
		html = html + " { ";     
		html = html + "         if (";
		html = html + " event.keyCode == 112 || event.keyCode == 113 || event.keyCode == 114 || event.keyCode == 115 || event.keyCode == 116 || event.keyCode == 117 ||";
		html = html + " event.keyCode == 118 || event.keyCode == 119 || event.keyCode == 120 || event.keyCode == 121 || event.keyCode == 122) {";
		html = html + " var arrElements = document.getElementsByTagName('input');";
		html = html + " var keyFound = false;";
		html = html + " for (i = 0; i < arrElements.length; i++) {";
		html = html + " if (arrElements[i].type == 'submit') {";
		html = html + " switch (event.keyCode) {";
		html = html + " case 112:";
		html = html + " if (arrElements[i].value == 'F1')";	//F7 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 113:";
		html = html + " if (arrElements[i].value == 'F2')";		//F8 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 114:";
		html = html + " if (arrElements[i].value == 'F3')";	//F9 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 115:";
		html = html + " if (arrElements[i].value == 'F4')";		//F10 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 116:";
		html = html + " if (arrElements[i].value == 'F5')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 117:";
		html = html + " if (arrElements[i].value == 'F6')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 118:";
		html = html + " if (arrElements[i].value == 'F7')";		//F7 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 119:";
		html = html + " if (arrElements[i].value == 'F8')";		//F8 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 120:";
		html = html + " if (arrElements[i].value == 'F9')";		//F9 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 121:";
		html = html + " if (arrElements[i].value == 'F10')";		//F10 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 122:";
		html = html + " if (arrElements[i].value == 'F11')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " }";
		html = html + " if (keyFound == true) {";        
		html = html + " eval('document._rf_checkin_itemstatus.' + arrElements[i].name + '.click();');";
		html = html + " return false;";
		html = html + " }";
		html = html + " }";
		html = html + " }";
		html = html + " }    ";        
		html = html + "    return true; ";
		html = html + "    }";        
		html = html + " </SCRIPT>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterstatus').focus();";        
		html = html + "</script>";		
		html = html + "</head><body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_itemstatus' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " :<label>" + getStatus + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";        
		html = html + "			</tr>";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' value=" + statusoptionno + ">";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value='" + getBinLocation + "'>";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "					<input type='hidden' name='hdntotqtymoved' value=" + totqtymoved + ">";
		html = html + "					<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "					<input type='hidden' name='hdnLPId' value=" + getLPId + ">";        
		html = html + "					<input type='hidden' name='hdnLOTNo' value=" + getLOTNo + ">";
		html = html + "					<input type='hidden' name='hdnLOTNoId' value=" + getLOTId + ">";
		html = html + "					<input type='hidden' name='hdnStatus' value='" + getStatus + "'>";
		html = html + "					<input type='hidden' name='hdnStatusId' value=" + getStatusId + ">";
		html = html + "					<input type='hidden' name='hdnUOM' value=" + getUOM + ">";
		html = html + "					<input type='hidden' name='hdnUOMId' value=" + getUOMId + ">";
		html = html + "					<input type='hidden' name='hdnTotQty' value=" + getTotQuantity + ">";
		html = html + "					<input type='hidden' name='hdnAvailQty' value=" + getAvailQuantity + ">";
		html = html + "					<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "					<input type='hidden' name='hdnMoveQty' value=" + getMoveQuantity + ">";
		html = html + "					<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "					<input type='hidden' name='hdnInvtRecID' value=" + getInvtRecID + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnlocationId' value=" + locationId + ">";
		html = html + "				    <input type='hidden' name='hdncompId' value=" + compId + ">";
		html = html + "				    <input type='hidden' name='hdnnewstatusId' value=" + newstatusid + ">";
		html = html + "				    <input type='hidden' name='hdnoldstatusoption' value=" + StatusNo + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + ItemDesc + "'>";
		html = html + "				    <input type='hidden' name='hdnfifodate' value='" + fifodate + "'>";
		html = html + "				    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				    <input type='hidden' name='hdntmplocation' value=" + tempBinLocationId + ">";
		html = html + "				    <input type='hidden' name='hdntemplp' value='" + tempLP + "'>";
		html = html + "				    <input type='hidden' name='hdntempitem' value='" + tempItemId + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st4 +  " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		if (itemStatusLoopCount > 10)
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st5 + " <input name='cmdNext' type='submit' value='F8'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}

		nlapiLogExecution('ERROR', 'itemStatusLoopCount', itemStatusLoopCount);

		var StatusNo=itemStatusRetrieved;
		var newstatusid;
		for (var i = itemStatusRetrieved; i < (parseFloat(itemStatusRetrieved) + parseFloat(itemStatusLoopCount)); i++) {
			var itemStatusSearchResults = itemStatusSearchResult[i];

			if (itemStatusSearchResults != null)
			{			
				itemStatus = itemStatusSearchResults.getValue('name');
				itemStatusId = itemStatusSearchResults.getId();	
				nlapiLogExecution('ERROR', 'itemStatus', itemStatus);
				nlapiLogExecution('ERROR', 'getStatus', getStatus);
				if(itemStatus==getStatus)
				{
					StatusNo=parseFloat(i) + 1;
					newstatusid=itemStatusId;
				}
			}
			var value = parseFloat(i) + 1;
			html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";
		}
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterstatus').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}

	else 
	{
		var getNewLPId = "";
		var getActualBeginDate = request.getParameter('hdnActualBeginDate');
		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');
		var oldstatus=request.getParameter('hdnStatusId');
		var getNewStatusoption = request.getParameter('enterstatus');
		var getNewStatus = request.getParameter('hdnnewstatusId');
		var oldstatusoption = request.getParameter('hdnoldstatusoption');
		var invtrecid=-1;
		var CallFlag='N';
		var OldMakeWHFlag='Y';
		var NewMakeWHFlag='Y';
		var vNewWHlocatin;
		var vOldWHlocatin;
		var vNSFlag;
		var UpdateFlag;
		var vNSCallFlag='N';
		var tempflag="F";
		nlapiLogExecution('ERROR', 'old status', oldstatus);
		nlapiLogExecution('ERROR', 'old status option', oldstatusoption);
		nlapiLogExecution('ERROR', 'new status option', getNewStatusoption);
		nlapiLogExecution('ERROR', 'status id', getNewStatus);

		if(oldstatusoption!=getNewStatusoption)
		{
			var itemStatusSearchResult=getItemStatusList(request.getParameter('hdnlocationId'));
			if(itemStatusSearchResult!=null && itemStatusSearchResult!='')
			{
				for(i=0;i<itemStatusSearchResult.length;i++)
				{
					var statusoption=i+1;

					if(statusoption==getNewStatusoption)
					{
						getNewStatus = itemStatusSearchResult[i].getId();	
						tempflag="T";
					}
				}
			}
		}
		nlapiLogExecution('ERROR', 'tempflag', tempflag);
		nlapiLogExecution('ERROR', 'new status', getNewStatus);

		getMoveQuantity = request.getParameter('hdnMoveQty');
		getAvailQuantity = request.getParameter('hdnAvailQty');		

		var IMarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		IMarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', IMarray["custparam_language"]);


		var st6;
		if( getLanguage == 'es_ES')
		{
			st6 = "ESTADO NO V&#193;LIDO";

		}
		else
		{		

			st6 = "INVALID STATUS";


		}

		IMarray["custparam_newLP"] = getNewLP;	

		IMarray["custparam_actualbegindate"] = getActualBeginDate;

		IMarray["custparam_screenno"] = '22STATUS';

		var TimeArray = new Array();
		IMarray["custparam_actualbegintime"] = getActualBeginTime;
		IMarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
		IMarray["custparam_error"] = st6;

		IMarray["custparam_totqtymoveed"] = request.getParameter('hdntotqtymoved');
		IMarray["custparam_imbinlocationid"] = request.getParameter('hdnBinLocationId');
		IMarray["custparam_imbinlocationname"] = request.getParameter('hdnBinLocation');
		IMarray["custparam_imitemid"] = request.getParameter('hdnItemId');
		IMarray["custparam_imitem"] = request.getParameter('hdnItem');
		IMarray["custparam_totquantity"] = request.getParameter('hdnTotQty');
		IMarray["custparam_availquantity"] = request.getParameter('hdnAvailQty');
		IMarray["custparam_moveqty"] = request.getParameter('hdnMoveQty');
		IMarray["custparam_status"] = request.getParameter('hdnStatus');
		IMarray["custparam_statusid"] = request.getParameter('hdnStatusId');
		IMarray["custparam_uom"] = request.getParameter('hdnUOM');
		IMarray["custparam_uomid"] = request.getParameter('hdnUOMId');		
		IMarray["custparam_lot"] = request.getParameter('hdnLOTNo');
		IMarray["custparam_lotid"] = request.getParameter('hdnLOTNoId');
		IMarray["custparam_lp"] = request.getParameter('hdnLP');
		IMarray["custparam_lpid"] = request.getParameter('hdnLPId');
		IMarray["custparam_itemtype"]=request.getParameter('hdnitemType');
		IMarray["custparam_invtrecid"] = request.getParameter('hdnInvtRecID');
		IMarray["custparam_locationId"] = request.getParameter('hdnlocationId');		
		IMarray["custparam_skustatus"]=getNewStatus;
		IMarray["custparam_existingLP"]='';
		IMarray["custparam_itemdesc"] = request.getParameter('hdnItemdesc');
		IMarray["custparam_compid"] = request.getParameter('hdncompId');
		IMarray["custparam_fifodate"] = request.getParameter('hdnfifodate');
		IMarray["custparam_hdntmplocation"] = request.getParameter('hdntmplocation');
		IMarray["custparam_hdntemplp"] = request.getParameter('hdntemplp');
		IMarray["custparam_hdntempitem"] = request.getParameter('hdntempitem');

		nlapiLogExecution('ERROR', 'Compid', request.getParameter('hdncompId'));

		IMarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		nlapiLogExecution('ERROR', 'getNewStatusoption', getNewStatusoption);
		//case# 20127289 starts (getNewStatus is null or undefined checking at if condition)
			/*if((getNewStatusoption==null || getNewStatusoption=='')||(getNewStatus==null || getNewStatus=='' || getNewStatus=='undefined')||tempflag=="F")
		{
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
			return;
			}*/
		//case# 20127289 end
		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') 
		{       		
			response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_qty', 'customdeploy_rf_inventory_move_qty_di', false, IMarray);	   		 
		}
		else 
		{
				if((getNewStatusoption==null || getNewStatusoption=='')||(getNewStatus==null || getNewStatus=='' || getNewStatus=='undefined')||tempflag=="F")
{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
					return;
				}
			var CallFlag='N';
			if(getNewStatus==null ||getNewStatus=='')
			{
				CallFlag='N';
			}
			else if(oldstatus==getNewStatus)
			{
				CallFlag='N';
			}

			else
			{	
				vNSFlag=GetNSCallFlag(oldstatus,getNewStatus);
				if(vNSFlag!= null && vNSFlag!= "" && vNSFlag.length>0)

				{
					CallFlag=vNSFlag;

				}
			}
			if(vNSFlag!=null && vNSFlag!='')
			{
				if(vNSFlag[0]=='N')
					CallFlag='N';
				else if(vNSFlag[0]=='Y') 
				{ 
					OldMakeWHFlag= GetMWFlag(vNSFlag[1]);
					NewMakeWHFlag= GetMWFlag(vNSFlag[2]);
					vNewWHlocatin=vNSFlag[2]; 
					vOldWHlocatin=vNSFlag[1]; 
				}
			}
			if(NewMakeWHFlag!='Y')
			{

				IMarray["custparam_adjustflag"]=CallFlag;
				//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_adjust', 'customdeploy_rf_inventory_move_adjust_di', false, IMarray);
				var itemSubtype = nlapiLookupField('item', request.getParameter('hdnItemId'), ['recordType','custitem_ebizserialin','custitem_ebizserialout']);
				IMarray["custparam_itemtype"] = itemSubtype.recordType;
				nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype);
				if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T'|| itemSubtype.custitem_ebizserialout == 'T') 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_serialscan', 'customdeploy_rf_inventory_serialscan_di', false, IMarray);
					return;
				}
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_complete', 'customdeploy_rf_inventory_move_comple_di', false, IMarray);
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_endloc', 'customdeploy_rf_inventory_move_endloc_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Navigating to LP', 'Success');
			}

		}        
	}
}

function getItemStatusList(whloc)
{
	var itemStatusSearchResult = new Array();
	var itemStatusColumns = new Array();
	var itemStatusFilters = new Array();
	itemStatusFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whloc!=null && whloc!='')
		itemStatusFilters.push(new nlobjSearchFilter( 'custrecord_ebizsiteskus', null, 'anyOf', [whloc]));
	itemStatusColumns[0] = new nlobjSearchColumn('name');
	itemStatusColumns[1] = new nlobjSearchColumn('internalid');
	itemStatusColumns[2] = new nlobjSearchColumn('custrecord_display_sequence');
	itemStatusColumns[2].setSort(false);

	var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);

	return itemStatusSearchResult;

}


function GetNSCallFlag(Status,NewStatus)
{
	nlapiLogExecution('ERROR', 'Into GetNSCallFlag', '');
	nlapiLogExecution('ERROR', 'Status', Status);
	nlapiLogExecution('ERROR', 'New Status', NewStatus);
	var result=new Array();
	var retFlag='N';
	var OldMapLocation;
	var NewMapLocation;
	if(Status!=null && Status!="")
	{
		var OldIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', Status);
		if(OldIStatus!= null && OldIStatus != "")
		{
			OldMapLocation=OldIStatus.getFieldValue('custrecord_item_status_location_map');
			OldIStatus=null;
		} 
	}
	if(NewStatus!=null && NewStatus!="")
	{
		var NewIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', NewStatus);
		if(NewIStatus!= null && NewIStatus != "")
		{
			NewMapLocation=NewIStatus.getFieldValue('custrecord_item_status_location_map');
		} 
	}
	if(OldMapLocation != null && OldMapLocation !="" && NewMapLocation != null && NewMapLocation != "")
	{
		if(OldMapLocation==NewMapLocation)
		{
			retFlag='N';
			result.push('N');
		}
		else
		{
			result.push('Y');
		}
		result.push(OldMapLocation);
		result.push(NewMapLocation);
	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'OldMapLocation', OldMapLocation);
	nlapiLogExecution('ERROR', 'NewMapLocation', NewMapLocation);
	nlapiLogExecution('ERROR', 'Out of GetNSCallFlag', '');
	return result;
}

function GetMWFlag(WHLocation)
{
	nlapiLogExecution('ERROR', 'Into GetMWFlag : WHLocation', WHLocation);
	var retFlag='Y';
	var MWHSiteFlag;

	if(WHLocation!=null && WHLocation!="")
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', WHLocation));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebizwhsite')); 

		var WHOld = nlapiSearchRecord('location', null, filters, columns);

		if(WHOld!= null && WHOld != "" && WHOld.length>0)
		{
			MWHSiteFlag=WHOld[0].getValue('custrecord_ebizwhsite');
			nlapiLogExecution('ERROR', 'MWHSiteFlag', MWHSiteFlag);
			WHOld=null;
		} 
	} 
	if(MWHSiteFlag != null && MWHSiteFlag !="" )
	{
		if(MWHSiteFlag=='F')
			retFlag='N';
		else
			retFlag='Y';

	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'Out of GetMWFlag ', '');
	return retFlag;
}