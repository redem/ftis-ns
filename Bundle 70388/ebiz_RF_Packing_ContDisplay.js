/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_RF_Packing_ContDisplay.js,v $
 *     	   $Revision: 1.1.2.4.4.2.4.4 $
 *     	   $Date: 2014/06/13 12:52:46 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Packing_ContDisplay.js,v $
 * Revision 1.1.2.4.4.2.4.4  2014/06/13 12:52:46  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.4.4.2.4.3  2014/05/30 00:41:03  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.4.4.2.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.4.4.2.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.4.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.4.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.1.2.4  2012/05/17 12:28:11  schepuri
 * CASE201112/CR201113/LOG201121
 * modified CONTAINER label with CARTON
 *
 * Revision 1.1.2.3  2012/04/30 10:10:24  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.1.2.2  2012/03/29 07:01:58  vrgurujala
 * t_NSWMS_LOG201121_89
 *
 * Revision 1.1  2012/03/09 09:30:05  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.1  2011/04/19 14:19:06  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 *
 *****************************************************************************/
function EnterContDisplay(request, response)
{
    if (request.getMethod() == 'GET') 
    {   
    	var getOrderno = request.getParameter('custparam_orderno');
        var getRecordInternalId = request.getParameter('custparam_recordinternalid');
        var getContainerLpNo = request.getParameter('custparam_containerlpno');
        var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
        var getBeginLocationId = request.getParameter('custparam_beginLocation');
        var getItem = request.getParameter('custparam_item');
        var getItemDescription = request.getParameter('custparam_itemdescription');
        var getItemInternalId = request.getParameter('custparam_iteminternalid');
        var getDOLineId = request.getParameter('custparam_dolineid');
        var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
        var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
        var getEnteredLocation = request.getParameter('custparam_endlocation');
        var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
        var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
        var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
        var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var vBatchno = request.getParameter('custparam_batchno');
		var vcontlp = request.getParameter('custparam_contlp');
		var vcontsize = request.getParameter('custparam_contsize');
		var vlineCount = request.getParameter('custparam_linecount');
		var vloopCount = request.getParameter('custparam_loopcount');
		var	vClusterno="";
		var vContLp;
		var vQty;
		var enteriteminfo =request.getParameter('custparam_enteritem');
		var venterqty =  request.getParameter('custparam_enterqty'); 
				
		nlapiLogExecution('DEBUG', 'getOrderno-->', getOrderno);
		nlapiLogExecution('DEBUG', 'enteriteminfo-->', enteriteminfo);	
		 
		var iteminternalid;
 	 	/*var ItemFileter = new Array();
 	 	ItemFileter[0] = new nlobjSearchFilter('name', null, 'is', enteriteminfo);
 	 	var itemSearch = nlapiSearchRecord('item', null, ItemFileter, null);*/
		
		// Changed On 30/4/12 by Suman

		var ItemFilter=new Array();
		ItemFilter.push(new nlobjSearchFilter('nameinternal', null, 'is', enteriteminfo));
		ItemFilter.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		var itemSearch = nlapiSearchRecord('item', null, ItemFilter, null);

		// End of Changes as On 30/4/12
		
 	 	if(itemSearch!=null) 
 	 		iteminternalid = itemSearch[0].getId();
 	 	nlapiLogExecution('DEBUG', 'iteminternalid', iteminternalid);
 	 	
		 		 
		 var SOFilters = new Array();        
         SOFilters[0] = new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderno);
         SOFilters[1] = new nlobjSearchFilter('custrecord_pack_confirmed_date', null, 'isempty', null);                           
         SOFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', iteminternalid);
         
         
         var colsCont = new Array();
		 colsCont[0]=new nlobjSearchColumn('custrecord_container_lp_no');  
		 colsCont[1]=new nlobjSearchColumn('custrecord_act_qty');  
		 colsCont[2]=new nlobjSearchColumn('internalid');  
		 colsCont[2].setSort();
		 
		 var ContSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, colsCont);                
         
		 if (ContSearchResults != null && ContSearchResults.length > 0) 
         {			 
			 nlapiLogExecution('DEBUG', 'ContSearchResults--->', ContSearchResults.length);			 			 
			 vQty =ContSearchResults[0].getValue('custrecord_act_qty');
			 vContLp = ContSearchResults[0].getValue('custrecord_container_lp_no');
         }
		 else
		 {
			 nlapiLogExecution('DEBUG', 'ContSearchResults is null--->');
		 }
		 
			var getLanguage = request.getParameter('custparam_language');	
		    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		    
			var st0,st1,st2,st3,st4;
			if( getLanguage == 'es_ES')
			{
				st0 = "";
				st1 = "CART&#211;N";
				st2 = "ENTER / SCAN CART&#211;N";
				st3 = "ENVIAR";
				st4 = "ANTERIOR";
				
	    	}
			else
			{
				st0 = "";
				st1 = "CARTON";
				st2 = "ENTER/SCAN CARTON:";
				st3 = "SEND";
				st4 = "PREV";
				
				
			}    	
         
		
        var html = "<html><head><title>" + st0 + "</title>";
        html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
        html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
      //  html = html + " document.getElementById('enteritem').focus();";        
        html = html + "</script>";
        html = html + "</head><body>";
        html = html + "	<form name='_rf_cluster_no' method='POST'>";
        html = html + "		<table>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st1 + ": <label>" + vContLp + "</label>";
        html = html + "			</tr>";
        html = html + "			<tr>";        
        html = html + "				<td align = 'left'>" + st2;		
        html = html + "				</td>";
        html = html + "			</tr>";        
        html = html + "			<tr>";
        html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
        html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getOrderno + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		//html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		//html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + vContLp + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + vQty + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterno + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnlinecount' value=" + vlineCount + ">";
		html = html + "				<input type='hidden' name='hdnloopcount' value=" + vloopCount + ">";
		html = html + "				<input type='hidden' name='hdnenterqty' value=" + venterqty + ">";
		html = html + "				<input type='hidden' name='hdnenteriteminfo' value=" + enteriteminfo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT'/>";
        html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";        
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "		 </table>";
        html = html + "	</form>";
      //Case# 20148882 (added Focus Functionality for Textbox)
        html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
        html = html + "</body>";
        html = html + "</html>";
        
        response.write(html);
    }
    else 
    {
        nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
        
        var getEnteredItem = request.getParameter('enteritem');
        nlapiLogExecution('DEBUG', 'Entered Order No', getEnteredItem);      
        
        
        var getWaveNo = request.getParameter('hdnWaveNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getItem = request.getParameter('hdnItem');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		var getItem = request.getParameter('hdnItemName');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');	
		var venteritem = request.getParameter('hdnenteriteminfo');	
        		
        // This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
        // to the previous screen.
        var optedEvent = request.getParameter('cmdPrevious');   
        
        if(getEnteredItem=="")
        {
        	getEnteredItem=getContainerLpNo;
        }
        //hdnenterqty
        
        nlapiLogExecution('DEBUG', 'getWaveNo-->', getWaveNo);		
        
    	var iteminternalid;
 	 	/*var ItemFileter = new Array();
 	 	ItemFileter[0] = new nlobjSearchFilter('name', null, 'is', venteritem);
 	 	var itemSearch = nlapiSearchRecord('item', null, ItemFileter, null);*/
    	
    	// Changed On 30/4/12 by Suman

		var ItemFilter=new Array();
		ItemFilter.push(new nlobjSearchFilter('nameinternal', null, 'is', venteritem));
		ItemFilter.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		var itemSearch = nlapiSearchRecord('item', null, ItemFilter, null);

		// End of Changes as On 30/4/12
		
 	 	if(itemSearch!=null) 
 	 		iteminternalid = itemSearch[0].getId();
 	 	nlapiLogExecution('DEBUG', 'iteminternalid', iteminternalid);
 	 	
 	 	
		 
        var SOarray = new Array();
        var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]);    	
		
		var st9,st10;
		if( getLanguage == 'es_ES')
		{			
			st9 = "LP NO V&#193;LIDO PARA CAJA AGANIST";
			st10 = "ART&#205;CULO INV&#193;LIDO ";
		}
		else
		{			
			st9 = "INVALID CARTON LP AGANIST ORDER";
			st10 = "INVALID ITEM";
		}
        
        var SOLPFilters = new Array();                
        SOLPFilters[0] = new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getWaveNo); //here in getwaveno order no is stored
        SOLPFilters[1] = new nlobjSearchFilter('custrecord_container_lp_no', null,'is',getEnteredItem);
        SOLPFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', iteminternalid);
        
        
        var colsLPCont = new Array();		   
		colsLPCont[0]=new nlobjSearchColumn('custrecord_act_qty'); 
		 
		 var ContLPSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOLPFilters, colsLPCont);                
         
		 if (ContLPSearchResults != null && ContLPSearchResults.length > 0) 
         {			 
			 getExpectedQuantity =ContLPSearchResults[0].getValue('custrecord_act_qty');
         }
		 else
		 {
			 SOarray["custparam_error"] = st9;
             response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
             nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Container Lp for this order');
             return;
		 }
		 
		 	      
        SOarray["custparam_error"] = st10;
		SOarray["custparam_screenno"] = '26A';
        
        SOarray["custparam_orderno"] = getWaveNo;
        SOarray["custparam_recordinternalid"] = getRecordInternalId;
        //SOarray["custparam_containerlpno"] = getContainerLpNo;
        SOarray["custparam_containerlpno"] = getEnteredItem;        
        SOarray["custparam_expectedquantity"] = getExpectedQuantity;
        SOarray["custparam_beginLocation"] = getBeginLocation;
     	SOarray["custparam_enterqty"] = request.getParameter('hdnenterqty');   
        
        
        /*
        if(SOarray["custparam_item"] == null)
        {
        	SOarray["custparam_item"] = getItem;
        }
        else
        {
        	SOarray["custparam_item"] = SOarray["custparam_item"] + "," +  getItem;
        }
        
        if(SOarray["custparam_expectedquantity"] == null)
	    {
	       	SOarray["custparam_expectedquantity"] = getExpectedQuantity;
	    }
	    else
	    {
	       	SOarray["custparam_expectedquantity"] = SOarray["custparam_expectedquantity"] + "," +  getExpectedQuantity;
	    }
	    */
        
        
        SOarray["custparam_item"] = getItem;        
        SOarray["custparam_itemdescription"] = getItemDescription;
        SOarray["custparam_iteminternalid"] = getItemInternalId;
        SOarray["custparam_dolineid"] = getDOLineId;
        SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_batchno"] = vBatchNo;
		SOarray["custparam_linecount"] = request.getParameter('hdnlinecount');
		SOarray["custparam_loopcount"] = request.getParameter('hdnloopcount');
		
        nlapiLogExecution('DEBUG', 'Order LineNo is', getOrderLineNo);
		  
        //	if the previous button 'F7' is clicked, it has to go to the previous screen 
        //  ie., it has to go to accept SO #.
        if (optedEvent == 'F7') {
            //response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			//response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
        	 // response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, SOarray);
            response.sendRedirect('SUITELET', 'customscript_rf_packing_item', 'customdeploy_rf_packing_item_di', false, SOarray);
        }        
        else 
        {
        	
        	response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, SOarray);
			nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			
			/*
            //if (getEnteredLocation != '' && getEnteredLocation == getBeginBinLocation) {
			if (getEnteredItem != '' && getEnteredItem == getItem) 
			{
				var ItemType = nlapiLookupField('item', getItemInternalId, 'recordType');
				//If Lotnumbered item the navigate to batch # scan
				if (ItemType == 'lotnumberedinventoryitem') 
				{					//getOrderLineNo					
					//response.sendRedirect('SUITELET', 'customscript_rf_picking_batch', 'customdeploy_rf_picking_batch_di', false, SOarray);
					//response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, SOarray);
					response.sendRedirect('SUITELET', 'customscript_rf_packing_item', 'customdeploy_rf_packing_item_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
				else 
				{			
					response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
            }
            else 
			{
            	SOarray["custparam_error"] = 'Invalied Item';
                response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
                nlapiLogExecution('DEBUG', 'Error: ', 'Invalied Item');
            }
            */
			
			
        }
    }
}
