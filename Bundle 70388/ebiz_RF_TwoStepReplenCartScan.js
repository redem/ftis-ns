/***************************************************************************
Ã¯Â¿Â½eBizNET Solutions Inc
 ****************************************************************************
 *
 *Ã¯Â¿Â½ $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplenCartScan.js,v $
 *Ã¯Â¿Â½ $Revision: 1.1.2.1.2.1 $
 *Ã¯Â¿Â½ $Date: 2015/11/09 17:02:54 $
 *Ã¯Â¿Â½ $Author: sponnaganti $
 *Ã¯Â¿Â½ $Name: t_WMS_2015_2_StdBundle_1_102 $
 *
 * 	DESCRIPTION
 *Ã¯Â¿Â½ Functionality
 *
 * 	REVISION HISTORY
 *Ã¯Â¿Â½ $Log: ebiz_RF_TwoStepReplenCartScan.js,v $
 *Ã¯Â¿Â½ Revision 1.1.2.1.2.1  2015/11/09 17:02:54  sponnaganti
 *Ã¯Â¿Â½ case# 201413004
 *Ã¯Â¿Â½ 2015.2 issue fix
 *Ã¯Â¿Â½
 *Ã¯Â¿Â½ Revision 1.1.2.1  2015/01/02 15:04:50  skreddy
 *Ã¯Â¿Â½ Case# 201411349
 *Ã¯Â¿Â½ Two step replen process
 *Ã¯Â¿Â½
 *
 ****************************************************************************/

function EnterCartLP(request, response)
{
	if (request.getMethod() == 'GET') 
	{  
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4,st5,st6;
		//20126048
		if( getLanguage == 'es_ES')
		{
			st0 = "ENTRAR / ESCANEAR N&#218;MERO DE CARRO";			
			st1 = "ENVIAR";
			st2 = "ANTERIOR";
		}
		else
		{
			st0 = "ENTER/SCAN CART#";
			st1 = "SEND";
			st2 = "PREV";
		}

		var vReportNo = request.getParameter('custparam_repno');
		var vZoneNo = request.getParameter('custparam_zoneno');
		var vZoneId = request.getParameter('custparam_zoneId');
		var vReplentype = request.getParameter('custparam_replentype');
		var vWhlocation = request.getParameter('custparam_whlocation');

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";       
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+  st0 + ":";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdngetReportno' value=" + vReportNo + ">";	
		html = html + "				<input type='hidden' name='hdngetZoneno' value=" + vZoneNo + ">";	
		html = html + "				<input type='hidden' name='hdngetZoneid' value=" + vZoneId + ">";	
		html = html + "				<input type='hidden' name='hdngetReplentype' value=" + vReplentype + ">";	
		html = html + "				<input type='hidden' name='hdngetWhlocation' value=" + vWhlocation + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+  st1 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+  st2 + " <input name='cmdPrevious' type='submit' value='F7'/>"; 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var vgetItemLP = request.getParameter('enterlp').toUpperCase();
		var varEnteredLP = request.getParameter('enterlp');


		nlapiLogExecution('DEBUG', 'varEnteredLP', varEnteredLP);
		nlapiLogExecution('DEBUG', 'vgetItemLP', vgetItemLP);

		if(varEnteredLP == vgetItemLP)
		{
			varEnteredLP = request.getParameter('enterlp').toUpperCase();
		}
		else
		{
			varEnteredLP = request.getParameter('enterlp');
		}

		var getLanguage = request.getParameter('hdngetLanguage');
		var Reparray = new Array();
		Reparray["custparam_language"] = getLanguage;
		Reparray["custparam_cartlpno"] = varEnteredLP;
		Reparray["custparam_repno"] = request.getParameter('hdngetReportno');
		Reparray["custparam_zoneno"] = request.getParameter('hdngetZoneno');
		Reparray["custparam_zoneId"]=request.getParameter('hdngetZoneid');
		Reparray["custparam_replentype"] = request.getParameter('hdngetReplentype');
		Reparray["custparam_whlocation"] = request.getParameter('hdngetWhlocation');


		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');       

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 

		if (optedEvent == 'F7') {	
			response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenreportno', 'customdeploy_ebiz_twostepreplenreportno', false, Reparray);		}
		else 
		{ 
			if (varEnteredLP != '' && varEnteredLP != null) 
			{
				var lpErrorMessage= MastLPExists(varEnteredLP,Reparray["custparam_whlocation"],vgetItemLP);//check for manual cart lp exists
				if(lpErrorMessage=="")
				{
					var NewCartLPNo=varEnteredLP;
					Reparray["custparam_cartlpno"] = NewCartLPNo;
					nlapiLogExecution('DEBUG', 'NewCartLPNo', NewCartLPNo);	 
					response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
				}
				else
				{
					Reparray["custparam_error"] = lpErrorMessage;
					Reparray["custparam_screenno"] = '2stepCRT';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				}
			}
			else
			{
				Reparray["custparam_error"] = "PLEASE ENTER CART LP#";
				Reparray["custparam_screenno"] = '2stepCRT';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			}
		}
	}
}

/**
 * To check manually generated Cart LP# exists in master LP or not
 * 
 * @param manualShipLP   
 * @returns message with proper error message
 */
function MastLPExists(manualCartLP,whloc,vgetItemLP)
{
	var message="";
	var filters =new Array();
	var vResult=new Array();
	vResult=ebiznet_LPRange_CL_withLPType1(manualCartLP,'2','4',whloc);
	if (vResult[0] == 'Y')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', manualCartLP));

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_cart_closeflag'));
		// To get the data from Master LP based on selection criteria
		searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
		if(searchresults!= null && searchresults.length>0)
		{
			if(searchresults[0].getValue('custrecord_ebiz_cart_closeflag')=='T')
			{
				message="CART LP# IS ALREADY CLOSED, PLEASE ENTER ANOTHER CART LP#";
				return message;//Case #20127806
			}
			else
			{
				return message="";
			}
		}
		else
		{
			vResult=ebiznet_LPRange_CL_withLPType1(manualCartLP,'2','4',whloc,vgetItemLP)// for User Defined  with Cart LPType
			if (vResult[0] == 'Y')
			{
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
				customrecord.setFieldValue('name', manualCartLP);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', manualCartLP);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 4);
				var rec = nlapiSubmitRecord(customrecord, false, true);
				return message;
			}
			else if (vResult[1] == 'O')
			{
				message="CART LP# IS OUT OF RANGE";
				return message;
			}
			else if(vResult[1] == 'I')
			{
				message="PLEASE ENTER VALID CART LP#";
				return message;
			}
			else if(vResult[1] == 'R')
			{
				message="PLEASE GIVE RANGE ALONG WITH LP PREFIX";
				return message;
			}
		}
	}
	else
	{
		return message="INVALID CART LP RANGE";
	}
}
