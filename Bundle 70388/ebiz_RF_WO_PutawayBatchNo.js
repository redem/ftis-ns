/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_PutawayBatchNo.js,v $
 *     	   $Revision: 1.1.2.3.4.5.4.1 $
 *     	   $Date: 2015/11/20 14:52:07 $
 *     	   $Author: skreddy $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_176 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PutawayBatchNo.js,v $
 * Revision 1.1.2.3.4.5.4.1  2015/11/20 14:52:07  skreddy
 * case 201415773
 * 2015.2 issue fix
 *
 * Revision 1.1.2.3.4.5  2014/05/30 00:34:25  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3.4.4  2013/08/21 14:22:59  skreddy
 * Case# 20123285
 *  restict space charater in lot# feild
 *
 * Revision 1.1.2.3.4.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.3.4.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.3.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.8.4.3.4.9.2.1  2013/02/26 13:02:23  skreddy
 *
 *****************************************************************************/

function WOBatchNo(request, response){
	if (request.getMethod() == 'GET') {

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getWONo = request.getParameter('custparam_woname');
		var getWOItem = request.getParameter('custparam_item');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getWOInternalId = request.getParameter('custparam_woid');
		var getWOQtyEntered = request.getParameter('custparam_expectedquantity');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getnoofrecords =request.getParameter('custparam_noofrecords');
		var getwoStatus =request.getParameter('custparam_wostatus');
		var getWOLinePackCode = request.getParameter('custparam_packcode');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getItemType = request.getParameter('custparam_itemtype');

		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');

		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getItemBaseUomQty=request.getParameter('custparam_baseuomqty');
//		var getItemQuantity = request.getParameter('hdnQuantity');
//		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');

		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getLotno = request.getParameter('custparam_batchno');
		/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle ***/
		var getLotExpirydate = request.getParameter('custparam_batchexpirydate');
		/*** up to here ***/
		nlapiLogExecution('ERROR','getWHLocation',getWHLocation);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{

			st0 = "CHECKIN LOTE #";
			st1 = "ART&#205;CULO";	
			st2 = "INGRESAR LOTE#:";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "";
			st6 = "";

		}
		else
		{
			st0 = "CHECKIN  LOT#";
			st1 = "ASSEMBLY ITEM";	
			st2 = "ENTER LOT#:";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "BUILD QTY";
			st6	= "WO#";	
			st7 =" LOT#";

		}		


		var functionkeyHtml = getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>" + st0 + "</title>" ;
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 + ":  <label>" + getWONo + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +" :  <label>" + getWOItem + "</label></td>";
		html = html + "				<input type='hidden' name='hdnWOStatus' value=" + getwoStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnWOItemId' value=" + getFetchedItemId + "></td>";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getWOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnnoofrecords' value=" + getnoofrecords + ">";
		html = html + "				<input type='hidden' name='hdnWOName' value=" + getWONo + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle ***/
		html = html + "				<input type='hidden' name='hdnlotexpirydate' value=" + getLotExpirydate + ">";
		/*** up to here ***/
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnBaseUomQty' value=" + getItemBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnWOQuantityEntered' value=" + parseFloat(getWOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnWOInternalId' value=" + getWOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				<input type='hidden' name='hdnlotno' value=" + getLotno + ">";

		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + ":  <label>" + getWOQtyEntered + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st7 + ":  <label>" + getLotno + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbatch' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		try {
			var POarray = new Array();
			var WOarray = new Array();
			var optedEvent = request.getParameter('cmdPrevious');
			var getBatch = request.getParameter('enterbatch');
			var getLanguage = request.getParameter('hdngetLanguage');
			POarray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);


			var st8,st9;
			if( getLanguage == 'es_ES')
			{

				st8 = "LOT NO V&#193;LIDO #";
				st9 = "LOTE # IS NULL";

			}
			else
			{

				st8 = "INVALID LOT#";
				st9 = " ENTER VALID LOT# ";


			}
			nlapiLogExecution('ERROR', 'optedEvent', optedEvent);
			nlapiLogExecution('ERROR', 'getBatch', getBatch);

			WOarray["custparam_woid"] = request.getParameter('custparam_woid');
			WOarray["custparam_item"] = request.getParameter('custparam_item');
			WOarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
			WOarray["custparam_expectedquantity"] = request.getParameter('hdnWOQuantityEntered');
			WOarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
			WOarray["custparam_noofrecords"] = request.getParameter('hdnnoofrecords');
			WOarray["custparam_woname"] = request.getParameter('hdnWOName');

			WOarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
			WOarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			WOarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			WOarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			WOarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			WOarray["custparam_wostatus"] = request.getParameter('hdnWOStatus');
			WOarray["custparam_packcode"] = request.getParameter('hdnItemPackCode');
			WOarray["custparam_language"] = request.getParameter('hdngetLanguage');
			WOarray["custparam_itemtype"] = request.getParameter('hdnItemType');
			/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle ***/
			WOarray["custparam_batchexpirydate"] = request.getParameter('hdnlotexpirydate');			
			/*** up to here  ***/
			nlapiLogExecution('ERROR','ItemType',WOarray["custparam_itemtype"]);

			/*	POarray["custparam_expectedquantity"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
			POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
			POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
			POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode');//request.getParameter('custparam_polinepackcode');
			POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
			POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
			POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
			POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
			POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');

			//code added on 24 feb 2012 by suman
			//To get the baseuom qty .
			POarray["custparam_baseuomqty"] = request.getParameter('hdnBaseUomQty');
			nlapiLogExecution('ERROR','ITEMINFO[0]',POarray["custparam_itemcube"]);
			nlapiLogExecution('ERROR','ITEMINFO[1]',POarray["custparam_baseuomqty"]);
			//end of code as of 24 feb 2012.

			POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');

			POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
			nlapiLogExecution('ERROR', 'WH Location', POarray["custparam_whlocation"]);
			nlapiLogExecution('ERROR', 'Fetched Item Id', POarray["custparam_fetcheditemid"]);
			 */
			WOarray["custparam_error"] = st8;
			WOarray["custparam_screenno"] = 'WOPutawayGenBatch';

			if (optedEvent == 'F7') {
				nlapiLogExecution('ERROR', 'Work Order batchno F7 Pressed');
				//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_gen', 'customdeploy_ebiz_rf_wo_putaway_gen_di', false, WOarray);
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_qty', 'customdeploy_ebiz_rf_wo_putaway_qty_di', false, WOarray);
				return;
			}
			else {//case 20123285 start
				if(getBatch !=null && getBatch !='')
				{
					var result=ValidateSplCharacter(getBatch,'Lot #');
					nlapiLogExecution('ERROR','result',result);
					if(result == false)
					{
						WOarray["custparam_error"] = 'SPECIAL CHARS NOT ALLOWED IN LOT#';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					}
					else
					{

						var getSearchBatch = getBatchNo(WOarray["custparam_fetcheditemid"],WOarray["custparam_packcode"],	WOarray["custparam_whlocation"],getBatch);

						if(getSearchBatch != null){
							WOarray["custparam_expdate"] = getSearchBatch[0].getValue('custrecord_ebizexpirydate');
							WOarray["custparam_mfgdate"] = getSearchBatch[0].getValue('custrecord_ebizmfgdate');
							WOarray["custparam_bestbeforedate"] = getSearchBatch[0].getValue('custrecord_ebizbestbeforedate');
							WOarray["custparam_fifodate"] = getSearchBatch[0].getValue('custrecord_ebizfifodate');
							WOarray["custparam_lastdate"]= getSearchBatch[0].getValue('custrecord_ebizlastavldate');
							WOarray["custparam_fifocode"] = getSearchBatch[0].getValue('custrecord_ebizfifocode');
							WOarray["custparam_batchno"] = getBatch;
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_loc', 'customdeploy_ebiz_rf_wo_putaway_loc_di', false, WOarray);
						}
						else{
							WOarray["custparam_error"] = st9;
							WOarray["custparam_batchno"] =getBatch ;
							//response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_lotexpdate', 'customdeploy_ebiz_rf_wo_lotexpdate_di', false, WOarray);
						}
					}
				}
				else
				{
					WOarray["custparam_error"] = st9;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					nlapiLogExecution('ERROR', 'batch# is empty');
				}
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
			nlapiLogExecution('ERROR', 'Catch: Location not found');

		}
	}
}

function getBatchNo(item, packCode,location, batch ){
	nlapiLogExecution('ERROR', 'getBatchNo function');
	nlapiLogExecution('ERROR', 'getitem',item);
	nlapiLogExecution('ERROR', 'getpackcode',packCode);
	nlapiLogExecution('ERROR', 'getlocation',location);
	nlapiLogExecution('ERROR', 'getbatch',batch);

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[item]));
	//if(packCode!=null && packCode!='')
	//filter.push(new nlobjSearchFilter('custrecord_ebizpackcode',null,'anyof',[packCode]));
	if(location!=null && location!='')
		filter.push(new nlobjSearchFilter('custrecord_ebizsitebatch',null,'anyof',[location]));
	filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batch));

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
	column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
	column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
	column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
	column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
	column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');

	var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);

	return searchRecord;
}


function ValidateSplCharacter(string,name)
{
	var iChars = "*|,\":<>[]{}`\';()@&$#% ";
	var length=string.length;
	var flag = 'N';
	for(var i=0;i<length;i++)
	{
		if(iChars.indexOf(string.charAt(i))!=-1)
		{
			flag='Y';
			break;
		}
	}
	if(flag == 'Y')
	{
		return false;
	}
	else
	{
		return true;
	}

}
