/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_ClusterPickReport_SL.js,v $
 *     	   $Revision: 1.2.2.4.4.2.4.3 $
 *     	   $Date: 2014/05/26 15:41:48 $
 *     	   $Author: skavuri $
 *     	   $Name: t_eBN_2014_1_StdBundle_3_44 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_ClusterPickReport_SL.js,v $
 * Revision 1.2.2.4.4.2.4.3  2014/05/26 15:41:48  skavuri
 * Case # 20148526 SB Issue Fixed
 *
 * Revision 1.2.2.4.4.2.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.2.2.4.4.2.4.1  2013/02/26 12:55:05  skreddy
 * CASE201112/CR201113/LOG201121
 * merged boombah changes
 *
 * Revision 1.2.2.4.4.2  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.4.4.1  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.2.2.3  2012/03/02 14:24:00  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * cluster pick report
 *
 * Revision 1.3  2012/03/02 14:17:17  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2012/02/24 16:52:10  mbpragada
 * CASE201112/CR201113/LOG201121
 * Cluster pick report new files
 *
 * Revision 1.1  2012/02/24 16:44:30  mbpragada
 * CASE201112/CR201113/LOG201121
 * Cluster pick report new files
 *

 *
 *
 **********************************************************************************************************************/

function GetWaves()
{
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26'])); 

	var columnso = new Array();
	// Case#20148526 starts
	//columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave_no').setSort('true'));
	//columnso.push(new nlobjSearchColumn('name'));
	 columnso.push(new nlobjSearchColumn('internalid').setSort('true'));
		columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
		// Case#20148526 ends

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);
	return searchresults;
}

function GetOrders()
{
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));

	var columnso = new Array();
	// Case# 20148526 starts
	//columnso.push(new nlobjSearchColumn('name').setSort('true'));
	 columnso.push(new nlobjSearchColumn('internalid').setSort('true'));
		columnso.push(new nlobjSearchColumn('name'));
		// Case# 20148526 ends

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);
	return searchresults;
}

function GetClusters()
{
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));  

	var columnso = new Array();
	// Case#20148526 starts
	//columnso.push(new nlobjSearchColumn('custrecord_ebiz_clus_no').setSort('true'));
	 columnso.push(new nlobjSearchColumn('internalid').setSort('true'));
		columnso.push(new nlobjSearchColumn('custrecord_ebiz_clus_no'));
		//Case#20148526 ends

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);
	return searchresults;
}

function ClusterPickReportSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Cluster Pick Report');
		var vQbWave = "";
		form.setScript('customscript_clusterpickreport');

		var WaveField = form.addField('custpage_wave', 'select', 'Wave');
		WaveField.setLayoutType('startrow', 'none');
		WaveField.addSelectOption("", "");

		var DoField = form.addField('custpage_do', 'select', 'Fulfillment Ord#');
		DoField.setLayoutType('startrow', 'none');
		DoField.addSelectOption("", "");

		var ClusterNoField = form.addField('custpage_custerno', 'select', 'Cluster #');
		ClusterNoField.setLayoutType('startrow', 'none');
		ClusterNoField.addSelectOption("", "");

		var searchResultesWave= GetWaves();
		if (searchResultesWave != null && searchResultesWave != "" && searchResultesWave.length>0) {
			for (var i = 0; i < searchResultesWave.length; i++) {
				if(searchResultesWave[i].getValue('custrecord_ebiz_wave_no')!=null && searchResultesWave[i].getValue('custrecord_ebiz_wave_no')!="")
				{
					var res=  form.getField('custpage_wave').getSelectOptions(searchResultesWave[i].getValue('custrecord_ebiz_wave_no'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					WaveField.addSelectOption(searchResultesWave[i].getValue('custrecord_ebiz_wave_no'), searchResultesWave[i].getValue('custrecord_ebiz_wave_no'));
				}
			}
		}

		var searchResultesOrder= GetOrders();
		if (searchResultesOrder != null && searchResultesOrder != "" && searchResultesOrder.length>0) {
			for (var i = 0; i < searchResultesOrder.length; i++) {
				if(searchResultesOrder[i].getValue('name')!=null && searchResultesOrder[i].getValue('name')!="")
				{
					var res=  form.getField('custpage_do').getSelectOptions(searchResultesOrder[i].getValue('name'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					DoField.addSelectOption(searchResultesOrder[i].getValue('name'), searchResultesOrder[i].getValue('name'));
				}
			}
		}

		var searchResultesCluster= GetClusters();
		if (searchResultesCluster != null && searchResultesCluster != "" && searchResultesCluster.length>0) {
			for (var i = 0; i < searchResultesCluster.length; i++) {
				if(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no')!=null && searchResultesCluster[i].getValue('custrecord_ebiz_clus_no')!="")
				{
					var res=  form.getField('custpage_custerno').getSelectOptions(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					ClusterNoField.addSelectOption(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'), searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'));
				}
			}
		}

//		var sublist = form.addSubList("custpage_items", "list", "Cluster Pick");
//		sublist.addField("custpage_tasktype", "text", "TaskType").setDisplayType('hidden');
//		sublist.addField("custpage_waveno", "text", "Wave#");
//		sublist.addField("custpage_sono", "text", "SO #");
//		sublist.addField("custpage_fulfillmentno", "text", "Fulfillment#");
//		sublist.addField("custpage_lineno", "text", "SO Line #");
//		sublist.addField("custpage_itemname", "text", "Item");
//		sublist.addField("custpage_upccode", "text", "UPC Code");
//		sublist.addField("custpage_itemdesc", "text", "Item Description");
//		sublist.addField("custpage_itemstatus", "text", "Item Status");
//		sublist.addField("custpage_packcode", "text", "Pack Code");	
//		sublist.addField("custpage_lot", "text", "LOT/Batch #");
//		sublist.addField("custpage_ordqty", "text", "Qty");
//		sublist.addField("custpage_actbeginloc", "text", "Bin Location");
//		sublist.addField("custpage_lpno", "text", "From LP #");
//		sublist.addField("custpage_container", "text", "Container LP");
//		sublist.addField("custpage_containersize", "text", "Container Size");
//		sublist.addField("custpage_clusternumber", "text", "Cluster #");
//		sublist.addField("custpage_status", "text", "Status").setDisplayType('hidden');	
//		sublist.addField("custpage_remarks", "text", "Remarks");	



		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else //this is the POST block
	{
		var form = nlapiCreateForm('Cluster Pick Report');
		var vQbWave = request.getParameter('custpage_wave');
		var vQbdo = request.getParameter('custpage_do');
		var vQbClusterNo = request.getParameter('custpage_custerno');
		nlapiLogExecution('ERROR', 'vQbdo1', vQbdo);
		nlapiLogExecution('ERROR', 'vQbClusterNo', vQbClusterNo);
		// If wave number is empty, then retrieve the wave number based on the fulfillment order no
		if((vQbWave == null || vQbWave == "") && (vQbdo != null || vQbdo != ""))
		{
			// fetch the wave number
			vQbWave = getWaveNoForFulfillOrder(vQbdo);
		}

		nlapiLogExecution('ERROR', 'vQbWave', vQbWave);
		nlapiLogExecution('ERROR', 'vQbdo2', vQbdo);


		form.setScript('customscript_clusterpickreport');
		form.addButton('custpage_print','Print','PrintClusterPickreport('+vQbWave+')');

		var WaveField = form.addField('custpage_wave', 'select', 'Wave');
		WaveField.setLayoutType('startrow', 'none');

		var DoField = form.addField('custpage_do', 'select', 'Fulfillment Ord#');
		DoField.setLayoutType('startrow', 'none');

		var ClusterNoField = form.addField('custpage_custerno', 'select', 'Cluster #');
		ClusterNoField.setLayoutType('startrow', 'none');

		var cluster = form.addField('custpage_cluster', 'text', 'orderno').setDisplayType('hidden').setDefaultValue(vQbClusterNo);

		WaveField.addSelectOption("", "");
		DoField.addSelectOption("", "");
		ClusterNoField.addSelectOption("", "");



		var searchResultesWave= GetWaves();
		if (searchResultesWave != null && searchResultesWave != "" && searchResultesWave.length>0) {
			for (var i = 0; i < searchResultesWave.length; i++) {
				if(searchResultesWave[i].getValue('custrecord_ebiz_wave_no')!=null && searchResultesWave[i].getValue('custrecord_ebiz_wave_no')!="")
				{
					var res=  form.getField('custpage_wave').getSelectOptions(searchResultesWave[i].getValue('custrecord_ebiz_wave_no'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					WaveField.addSelectOption(searchResultesWave[i].getValue('custrecord_ebiz_wave_no'), searchResultesWave[i].getValue('custrecord_ebiz_wave_no'));
				}
			}
		}

		var searchResultesOrder= GetOrders();
		if (searchResultesOrder != null && searchResultesOrder != "" && searchResultesOrder.length>0) {
			for (var i = 0; i < searchResultesOrder.length; i++) {
				if(searchResultesOrder[i].getValue('name')!=null && searchResultesOrder[i].getValue('name')!="")
				{
					var res=  form.getField('custpage_do').getSelectOptions(searchResultesOrder[i].getValue('name'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					DoField.addSelectOption(searchResultesOrder[i].getValue('name'), searchResultesOrder[i].getValue('name'));
				}
			}
		}
		var searchResultesCluster= GetClusters();
		if (searchResultesCluster != null && searchResultesCluster != "" && searchResultesCluster.length>0) {
			for (var i = 0; i < searchResultesCluster.length; i++) {
				if(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no')!=null && searchResultesCluster[i].getValue('custrecord_ebiz_clus_no')!="")
				{
					var res=  form.getField('custpage_custerno').getSelectOptions(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					ClusterNoField.addSelectOption(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'), searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'));
				}
			}
		}
		WaveField.setDefaultValue(vQbWave);
		DoField.setDefaultValue(vQbdo);
		ClusterNoField.setDefaultValue(vQbClusterNo);
		var sysdate=DateStamp();
		var date=form.addField('custpage_date', 'text', 'Date').setDisplayType('inline');
		date.setDefaultValue(sysdate);


		form.addSubmitButton('Display');

		var sublist = form.addSubList("custpage_items", "list", "Cluster Pick");
		sublist.addField("custpage_tasktype", "text", "TaskType").setDisplayType('hidden');
		sublist.addField("custpage_waveno", "text", "Wave#");
		sublist.addField("custpage_sono", "text", "SO #");
		sublist.addField("custpage_fulfillmentno", "text", "Fulfillment#");
		sublist.addField("custpage_lineno", "text", "SO Line #");
		sublist.addField("custpage_itemname", "text", "Item");
		sublist.addField("custpage_upccode", "text", "UPC Code");
		sublist.addField("custpage_itemdesc", "text", "Item Description");
		sublist.addField("custpage_itemstatus", "text", "Item Status");
		sublist.addField("custpage_packcode", "text", "Pack Code");	
		sublist.addField("custpage_lot", "text", "LOT/Batch #");
		sublist.addField("custpage_ordqty", "text", "Qty");
		sublist.addField("custpage_actbeginloc", "text", "Bin Location");
		sublist.addField("custpage_lpno", "text", "From LP #");
		sublist.addField("custpage_container", "text", "Container LP");
		sublist.addField("custpage_containersize", "text", "Container Size");
		sublist.addField("custpage_clusternumber", "text", "Cluster #");
		sublist.addField("custpage_status", "text", "Status").setDisplayType('hidden');	
		sublist.addField("custpage_remarks", "text", "Remarks");	

		var filters = new Array();

		var j = 0;
		if (vQbWave != "" && vQbWave!=null) {
			filters[j] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vQbWave);
			j = j + 1;
		}
		if (vQbdo != "" && vQbdo !=null) {

			filters[j] = new nlobjSearchFilter('name', null, 'is', vQbdo);
			j = j + 1;
		}			
		if(vQbClusterNo!="" && vQbClusterNo!=null)
		{
			filters[j] = new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vQbClusterNo);
			j = j + 1;
		}
		//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
		filters[j] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']);
		j = j + 1;
		filters[j] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']); // PICK

		var ItemList=new Array();
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[3] = new nlobjSearchColumn('custrecord_tasktype');
		columns[4] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[6] = new nlobjSearchColumn('custrecord_sku');
		columns[7] = new nlobjSearchColumn('custrecord_sku_status');
		columns[8] = new nlobjSearchColumn('custrecord_packcode');
		columns[9] = new nlobjSearchColumn('name');
		columns[10] = new nlobjSearchColumn('custrecord_batch_no');
		columns[11] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[12] = new nlobjSearchColumn('custrecord_container');
		columns[13] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
		columns[14] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[15] = new nlobjSearchColumn('description','custrecord_sku');
		columns[16] = new nlobjSearchColumn('upccode','custrecord_sku');
		columns[17] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[18] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[19] = new nlobjSearchColumn('custrecord_notes');
		columns[14].setSort(false);


		var pfLocnResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		if(pfLocnResults!=null)
		{
			ItemList.push(pfLocnResults);
		}		

		var vline, vitem, vqty, vTaskType, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, 
		vskustatus, vpackcode, vdono,vlotbatch,vClusterNo,vsizeid,vwaveno,vwmsststus,vnotes;
		var upccode="";
		var itemdesc="";			

		//newly added
		nlapiLogExecution('ERROR', 'ItemList.length ', ItemList.length); 
		nlapiLogExecution('ERROR', 'ItemList ', ItemList); 
		if(ItemList != null && ItemList.length > 0){

			var temparraynew=new Array();
			var tempindex=0;
			for(k=0;k<ItemList.length;k++)
			{
				nlapiLogExecution('ERROR', 'ItemList[k]', ItemList[k]); 
				var invtsearchresult = ItemList[k];
				nlapiLogExecution('ERROR', 'invtsearchresult.length ', invtsearchresult.length); 
				for(var j=0;j<invtsearchresult.length;j++)
				{
					temparraynew[tempindex]=invtsearchresult[j];
					tempindex=tempindex+1;
				}
			}
			nlapiLogExecution('ERROR', 'temparraynew.length ', temparraynew.length); 
			var test='';

			if(temparraynew.length>0)
			{

				if(temparraynew.length>25)
				{
					var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('entry');
					pagesize.setDisplaySize(10,10);
					pagesize.setLayoutType('outsidebelow', 'startrow');
					var select= form.addField('custpage_selectpage','select', 'Select Records');	
					select.setLayoutType('outsidebelow', 'startrow');			
					select.setDisplaySize(200,30);
					if (request.getMethod() == 'GET'){
						pagesize.setDefaultValue("25");
						pagesizevalue=25;
					}
					else
					{
						if(request.getParameter('custpage_pagesize')!=null)
						{pagesizevalue= request.getParameter('custpage_pagesize');}
						else
						{pagesizevalue= 25;pagesize.setDefaultValue("25");}
					}


					var len=temparraynew.length/parseFloat(pagesizevalue);
					for(var k=1;k<=Math.ceil(len);k++)
					{

						var from;var to;

						to=parseFloat(k)*parseFloat(pagesizevalue);
						from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

						if(parseFloat(to)>temparraynew.length)
						{
							to=temparraynew.length;
							test=from.toString()+","+to.toString(); 
						}

						var temp=from.toString()+" to "+to.toString();
						var tempto=from.toString()+","+to.toString();
						select.addSelectOption(tempto,temp);

					} 
					if (request.getMethod() == 'POST'){

						if(request.getParameter('custpage_selectpage')!=null ){

							select.setDefaultValue(request.getParameter('custpage_selectpage'));	

						}
						if(request.getParameter('custpage_pagesize')!=null ){

							pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

						}
					}
				}
				else
				{
					pagesizevalue=parseFloat(temparraynew.length);
				}
				var minval=0;var maxval=parseFloat(pagesizevalue);
				if(parseFloat(pagesizevalue)>temparraynew.length)
				{
					maxval=temparraynew.length;
				}
				var selectno=request.getParameter('custpage_selectpage');
				if(selectno!=null )
				{
					var temp= request.getParameter('custpage_selectpage');
					var temparray=temp.split(',');
					nlapiLogExecution('ERROR', 'temparray',temparray.length);
					nlapiLogExecution('ERROR', 'temparray[1]',temparray[1]);
					nlapiLogExecution('ERROR', 'temparray[0]',temparray[0]);
					var diff=parseFloat(temparray[1])-(parseFloat(temparray[0])-1);
					nlapiLogExecution('ERROR', 'diff',diff);

					var pagevalue=request.getParameter('custpage_pagesize');
					nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
					if(pagevalue!=null)
					{
						if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
						{

							var temparray=selectno.split(',');	
							nlapiLogExecution('ERROR', 'temparray.length ', temparray.length);  
							minval=parseFloat(temparray[0])-1;
							nlapiLogExecution('ERROR', 'temparray[0] ', temparray[0]);  
							maxval=parseFloat(temparray[1]);
							nlapiLogExecution('ERROR', 'temparray[1] ', temparray[1]);  
						}
					}
				}





				nlapiLogExecution('ERROR', 'ItemList new', ItemList.length);
				nlapiLogExecution('ERROR', 'temparraynew new ', temparraynew); 
				nlapiLogExecution('ERROR', 'temparraynew length ', temparraynew.length); 
				nlapiLogExecution('ERROR', 'minval new ', minval); 
				nlapiLogExecution('ERROR', 'maxval new ', maxval); 

				var count=1;
				// For all SKUs in pickface locations
				for(var j = minval; j < maxval; j++){
					nlapiLogExecution('ERROR', 'LOOP COUNT11', j);
					var Result = temparraynew[j];
					if(Result !=null && Result !=""){
						lotbatch = Result.getValue('custrecord_batch_no');
						line = Result.getValue('custrecord_line_no');
						item = Result.getValue('custrecord_ebiz_sku_no');
						qty = Result.getValue('custrecord_expe_qty');
						TaskType = Result.getText('custrecord_tasktype');
						Lpno = Result.getValue('custrecord_from_lp_no');
						locationid = Result.getValue('custrecord_actbeginloc');
						ContainerLpNo = Result.getValue('custrecord_container_lp_no');
						location = Result.getText('custrecord_actbeginloc');
						SKU = Result.getText('custrecord_sku');
						skustatus = Result.getText('custrecord_sku_status');
						packcode = Result.getValue('custrecord_packcode');
						dono = Result.getValue('name');
						vClusterNo = Result.getValue('custrecord_ebiz_clus_no');
						vsizeid = Result.getText('custrecord_container');
						itemdesc=Result.getValue('description','custrecord_sku');
						upccode=Result.getValue('upccode','custrecord_sku');
						vwaveno=Result.getValue('custrecord_ebiz_wave_no');
						vwmsststus=Result.getText('custrecord_wms_status_flag');
						vnotes=Result.getValue('custrecord_notes');




						sublist.setLineItemValue('custpage_lineno', count, line);
						sublist.setLineItemValue('custpage_tasktype', count, TaskType);
						sublist.setLineItemValue('custpage_itemname', count, SKU);
						sublist.setLineItemValue('custpage_upccode', count, upccode);
						sublist.setLineItemValue('custpage_itemdesc', count, itemdesc);
						sublist.setLineItemValue('custpage_itemstatus', count, skustatus);
						sublist.setLineItemValue('custpage_packcode', count, packcode);
						sublist.setLineItemValue('custpage_sono', count, dono.split('.')[0]);
						sublist.setLineItemValue('custpage_lot', count, lotbatch);
						sublist.setLineItemValue('custpage_ordqty', count, qty);
						sublist.setLineItemValue('custpage_lpno', count, Lpno);
						sublist.setLineItemValue('custpage_container', count, ContainerLpNo);
						sublist.setLineItemValue('custpage_containersize',count, vsizeid);
						sublist.setLineItemValue('custpage_actbeginloc', count, location);
						sublist.setLineItemValue('custpage_clusternumber', count, vClusterNo);
						sublist.setLineItemValue('custpage_waveno', count, vwaveno);
						sublist.setLineItemValue('custpage_fulfillmentno', count, dono);
						sublist.setLineItemValue('custpage_status',count, vwmsststus);
						sublist.setLineItemValue('custpage_remarks', count, vnotes);						
						count++;
					}
				}
			}
		}
		var tempflag=form.addField('custpage_tempflag','text','tempory flag').setDisplayType('hidden').setDefaultValue('true');
		form.setScript('customscript_ebiz_cancel_fullorder_cl');
		//form.addSubmitButton('Display');

		response.writePage(form);
	}
}

function getWaveNoForFulfillOrder(fulfillOrderNo){
	nlapiLogExecution('ERROR', 'getWaveNoForFulfillOrder', 'Start');
	var waveNo = "";

	if(fulfillOrderNo != null && fulfillOrderNo != ""){
		var filters = new Array();
		filters.push(new nlobjSearchFilter('name', null, 'is', fulfillOrderNo));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); //Pick Task
		//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
		//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');

		var fulfillOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		if(fulfillOrderList != null && fulfillOrderList.length > 0){
			waveNo = fulfillOrderList[0].getValue('custrecord_ebiz_wave_no');
		}
	}

	nlapiLogExecution('ERROR', 'fulfillOrderNo | waveNo', fulfillOrderNo + '|' + waveNo);
	nlapiLogExecution('ERROR', 'getWaveNoForFulfillOrder', 'End');
	return waveNo;
}

function PrintClusterPickreport(ebizwaveno){

	var waveno = nlapiGetFieldValue('custpage_wave');
	var fullfillment = nlapiGetFieldValue('custpage_do');
	//code added from Boombah Prod on 25Feb13 by santosh
	var clusterno = nlapiGetFieldValue('custpage_cluster');
	//up to here
	//alert(clusterno);
	nlapiLogExecution('ERROR', 'into Printreport ebizwaveno',ebizwaveno);
	var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_clusterpickreport_pdf', 'customdeploy_clusterpickreport_pdf_di');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	nlapiLogExecution('ERROR', 'Wave PDF URL',WavePDFURL);					
	WavePDFURL = WavePDFURL + '&custparam_ebiz_wave_no='+ waveno+'&custparam_ebiz_fullfilmentno='+fullfillment+'&custparam_ebiz_clusterno='+clusterno;
	//alert(WavePDFURL);
	nlapiLogExecution('ERROR', 'WavePDFURL',WavePDFURL);

	window.open(WavePDFURL);

}

function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}