/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_BulkPick_StageLoc_SL.js,v $
 *     	   $Revision: 1.1.2.13.2.1 $
 *     	   $Date: 2015/10/13 15:21:42 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_BulkPick_StageLoc_SL.js,v $
 * Revision 1.1.2.13.2.1  2015/10/13 15:21:42  grao
 * 2015.2 Issue Fixes 201414976
 *
 * Revision 1.1.2.13  2014/08/06 15:29:56  skavuri
 * Case# 20149862, 20149863 SB Issue Fixed
 *
 * Revision 1.1.2.12  2014/07/28 15:24:28  skavuri
 * Case# 20149736, 20149731 SB Issue Fixed
 *
 * Revision 1.1.2.11  2014/07/24 15:20:34  skavuri
 * Case # 20149653 Compatibility Issue Fixed
 *
 * Revision 1.1.2.10  2014/06/13 13:34:08  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.9  2014/06/06 07:14:39  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.8  2014/05/30 00:41:00  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.7  2014/05/29 15:42:03  sponnaganti
 * case# 20148627
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.6  2014/03/10 16:23:21  skavuri
 * Case# 20127528 issue fixed
 *
 * Revision 1.1.2.5  2014/03/05 12:02:35  snimmakayala
 * Case #: 20127523
 *
 * Revision 1.1.2.4  2013/12/04 16:18:41  skreddy
 * Case# 20126189
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.1.2.3  2013/08/23 06:13:10  snimmakayala
 * Case# 20124032
 * NLS - UAT ISSUES
 * Quantity is displaying wrongly on RF bulk picking screen for SHIPASIS items.
 *
 * Revision 1.1.2.2  2013/07/19 15:24:13  nneelam
 * Case# 20123528
 * Auto Build Ship.
 *
 * Revision 1.1.2.1  2013/07/19 08:18:22  gkalla
 * Case# CR12US0169
 * Bulk picking CR for Nautilus
 *
 * Revision 1.12.2.42.4.11.2.7  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.12.2.42.4.11.2.6  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 *****************************************************************************/

function PickingStageLocation(request, response){
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	
	
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('DEBUG', 'Into Page Load');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR AL MONTAJE";
			st2 = "N&#218;MERO DE EMPAQUE:";
			st3 = "UBICACI&#211;N:";
			st4 = "INGRESAR / ESCANEAR ETAPA:";
			st5 = "CONF";
		}
		else
		{
			st1 = "GO TO STAGING ";
			st2 = "CARTON NO : ";
			st3 = "LOCATION : ";
			st4 = "ENTER/SCAN STAGE :";
			st5 = "CONF";
		}

		var getWaveno = request.getParameter('custparam_waveno');

		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getItemName = request.getParameter('custparam_itemname');

		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');

		var getSerialNo = request.getParameter('custparam_serialno');

		var whLocation = request.getParameter('custparam_whlocation');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');

		var getItem = '';

		nlapiLogExecution('Debug', 'getItemName', getItemName);
		if(getItemName==null || getItemName=='')
		{
			var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
			var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
			getItem = ItemRec.getFieldValue('itemid');
		}
		else
		{
			getItem=getItemName;
		}

		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');

		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var newcontainerlp = request.getParameter('custparam_newcontainerlp');

		nlapiLogExecution('Debug', 'EntLoc', EntLoc);
		nlapiLogExecution('Debug', 'EntLocRec', EntLocRec);
		nlapiLogExecution('Debug', 'EntLocID', EntLocID);
		nlapiLogExecution('Debug', 'ExceptionFlag', ExceptionFlag);

		/***upto here***/
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');


		var vondemandstage = request.getParameter('custparam_ondemandstage');


		//Code Added for Displaying the Location Name
		var Carrier;
		var Site;
		var Company;
		var stgOutDirection="OUB";
		var carriertype='';
		var wmscarrier='';
		var customizetype='';
		var ordertype='';
		var customer='';

//		nlapiLogExecution('DEBUG', 'getDOLineId',getDOLineId); 
		nlapiLogExecution('DEBUG', 'WaveNo ', getWaveno);
		var vFOSearchDetails=GetFODetails(getWaveno,vZoneId);
		if(vFOSearchDetails!=null && vFOSearchDetails!='')
		{
			var vFOIntId=new Array();
			for(var s=0;s<vFOSearchDetails.length;s++)
			{
				if(vFOSearchDetails[s].getValue('custrecord_ebiz_cntrl_no',null,'group') != null && vFOSearchDetails[s].getValue('custrecord_ebiz_cntrl_no',null,'group') != '')
					vFOIntId.push(vFOSearchDetails[s].getValue('custrecord_ebiz_cntrl_no',null,'group'));
			}

			var columns = new Array();
			var filters = new Array();
			if(vFOIntId != null && vFOIntId != '' && vFOIntId.length>0)
				filters.push(new nlobjSearchFilter('internalid', null, 'is',vFOIntId));

			columns[0] = new nlobjSearchColumn('custrecord_do_wmscarrier');	
			columns[1] = new nlobjSearchColumn('custrecord_do_carrier');				
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_wave');
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_ordline_customizetype');
			columns[4] = new nlobjSearchColumn('custrecord_do_order_type');
			columns[5] = new nlobjSearchColumn('custrecord_do_customer');

			var searchresultsord = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

			if(searchresultsord!=null && searchresultsord!='' && searchresultsord.length>0)
			{
				Carrier = searchresultsord[0].getValue('custrecord_do_carrier');
				customizetype = searchresultsord[0].getValue('custrecord_ebiz_ordline_customizetype');
				ordertype = searchresultsord[0].getValue('custrecord_do_order_type');
				customer = searchresultsord[0].getValue('custrecord_do_customer');
				nlapiLogExecution('DEBUG', 'Carrier',Carrier); 
				nlapiLogExecution('DEBUG', 'customizetype',customizetype); 
				nlapiLogExecution('DEBUG', 'ordertype',ordertype); 
				if(Carrier!=null && Carrier!='')
				{
					var filterscarr = new Array();
					var columnscarr = new Array();

					filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',Carrier));
					filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

					columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
					columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
					columnscarr[2] = new nlobjSearchColumn('id');

					var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
					if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
					{
						carriertype = searchresultscarr[0].getValue('custrecord_carrier_type');
						wmscarrier = searchresultscarr[0].getId();
					}
				}

				if(Carrier==null || Carrier=='')
					wmscarrier=searchresultsord[0].getValue('custrecord_do_wmscarrier');
				if(getWaveno==null || getWaveno=='')
					getWaveno=searchresultsord[0].getValue('custrecord_ebiz_wave');
			}
		}

		nlapiLogExecution('DEBUG', 'carriertype',carriertype); 
		nlapiLogExecution('DEBUG', 'wmscarrier',wmscarrier); 
		nlapiLogExecution('DEBUG', 'WaveNo ', getWaveno);

		var stgLocation = '';
		var FetchBinLocation='';

		var stgLocationresults = GetStageLocationFromSTGM(getWaveno,vZoneId);
		if(stgLocationresults!=null && stgLocationresults!='' && stgLocationresults.length>0)
		{
			stgLocation = stgLocationresults[0].getValue('custrecord_actbeginloc');
			FetchBinLocation = stgLocationresults[0].getText('custrecord_actbeginloc');
		}

		nlapiLogExecution('DEBUG', 'Stage Location from STGM ', FetchBinLocation);
		nlapiLogExecution('DEBUG', 'Stage Location Id from STGM ', stgLocation);

		if(stgLocation==null || stgLocation=='')
		{

			stgLocation = GetPickStageLocation(getItemInternalId, wmscarrier, whLocation, Company,
					stgOutDirection,carriertype,customizetype,null,ordertype,customer);


			nlapiLogExecution('DEBUG', 'After Getting StagingLocation in page load::',stgLocation);
			var FetchBinLocation;
			if(stgLocation!=null && stgLocation!=-1)
			{
				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('name');
				filtersLocGroup.push(new nlobjSearchFilter('internalid', null, 'is',stgLocation));
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				if(searchresultsloc != null && searchresultsloc != '')
					FetchBinLocation=searchresultsloc[0].getValue('name');
			}

			nlapiLogExecution('DEBUG', 'After Getting StagingLocation Name in page load::',FetchBinLocation);
		}
		var getContainerLpNo;
		var name;
		//upto to here

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterstagelocation').focus();";        

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
//		if(pickType!='CL')
//		{
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>"+ st2 +" " + getContainerLpNo;
//		html = html + "				</td>";
//		html = html + "			</tr>";

//		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"</td></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" " + FetchBinLocation;
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnnewContainerLpNo' value=" + newcontainerlp + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";


		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";

		html = html + "				<input type='hidden' name='hdnserialno' value=" + getSerialNo + ">";

		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnstgloc' value=" + stgLocation + ">";

		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwmscarrier' value=" + Carrier + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value='" + getZoneNo + "'>";// Case#20127528
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='vhdnstageloctext' value='" + FetchBinLocation + "'>";
		html = html + "				<input type='hidden' name='vhdncarrier' value=" + wmscarrier + ">";
		html = html + "				<input type='hidden' name='vhdncarriertype' value=" + carriertype + ">";
		html = html + "				<input type='hidden' name='vhdncustomizetype' value=" + customizetype + ">";
		html = html + "				<input type='hidden' name='hdnondemandstage' value=" + vondemandstage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		/*		if(pickType!='CL')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>CARTON NO : " + getContainerLpNo;
			html = html + "				</td>";
			html = html + "			</tr>";

		}*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st4;//ENTER/SCAN STAGE :
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterstagelocation' id='enterstagelocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st5 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>"+ st5 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterstagelocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st6,st7;

		if( getLanguage == 'es_ES')
		{
			st6 = "LOCALIZACI&#211;N ETAPA NO V&#193;LIDO";
			st7 = "OPCI#211;N V&#193;LIDA";

		}
		else
		{
			st6 = "INVALID STAGE LOCATION";
			st7 = "INVALID OPTION";

		}
		nlapiLogExecution('DEBUG', 'Pick Type', request.getParameter('hdnpicktype'));
		var optedEvent = request.getParameter('hdnflag');
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		var WaveNo = request.getParameter('hdnWaveNo');

		var ContainerLPNo = request.getParameter('hdnContainerLpNo');			
		var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var ItemDescription = request.getParameter('hdnItemDescription');
		var ItemInternalId = request.getParameter('hdnItemInternalId');

		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var EndLocation = request.getParameter('hdnEnteredLocation');

		var SerialNo = request.getParameter('hdnserialno');

		var vCarrier = request.getParameter('hdnwmscarrier');
		var vSite;
		var vCompany;
		var stgDirection="OUB";
		var vDoname=request.getParameter('hdnName');

		var vstageLoc=request.getParameter('hdnstgloc');
		var voldcontainer='';
		var vZoneId=request.getParameter('hdnebizzoneno');
		var vhdncarrier=request.getParameter('vhdncarrier');
		var vhdncarriertype=request.getParameter('vhdncarriertype');
		var vhdncustomizetype=request.getParameter('vhdncustomizetype');
		var vondemandstage = request.getParameter('hdnondemandstage');
		var vnewcontainelp=request.getParameter("hdnnewContainerLpNo");
		nlapiLogExecution('DEBUG', 'WaveNo ', WaveNo);
		nlapiLogExecution('DEBUG', 'ContainerLPNo ', ContainerLPNo);
		//nlapiLogExecution('DEBUG', 'vPickType', vPickType);
		nlapiLogExecution('DEBUG', 'vZoneId', vZoneId);
		nlapiLogExecution('DEBUG', 'vondemandstage', vondemandstage);
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');

		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		//nlapiLogExecution('DEBUG', 'SOarray["custparam_waveno"]', SOarray["custparam_waveno"]);
		//nlapiLogExecution('DEBUG', 'SOarray["custparam_recordinternalid"]', SOarray["custparam_recordinternalid"]);
		//nlapiLogExecution('DEBUG', 'SOarray["custparam_containerlpno"]', SOarray["custparam_containerlpno"]);
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');


		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');

		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');
		SOarray["custparam_zoneno"] = request.getParameter('custparam_zoneno');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_newcontainerlp"] = ContainerLPNo;
		var WHLocation = request.getParameter('hdnwhlocation');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		
		if (sessionobj!=context.getUser())
		{
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

		if (optedEvent == 'ENT') 
		{
			//code added on 170812 by suman.
			//To validate user entered Stage Location.
			var stagelocation=request.getParameter('enterstagelocation');
			var hdnstagelocation=request.getParameter('vhdnstageloctext');

			nlapiLogExecution('DEBUG', 'Scanned Stage Location', stagelocation);
			nlapiLogExecution('DEBUG', 'Actual Stage Location', hdnstagelocation);
			if(stagelocation!=hdnstagelocation)
			{
				var resArray=ValidateStageLocation(ItemInternalId,vhdncarrier,WHLocation,vCompany,
						"OUB",vhdncarriertype,vhdncustomizetype,stagelocation);

				var Isvalidestage=resArray[0];
				if(Isvalidestage==true)
				{
					vstageLoc=resArray[1];
				}
				else
				{
					SOarray["custparam_error"] = st6;//'INVALID STAGE LOCATION';
					nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Stage Location');
					SOarray["custparam_screenno"] = 'BULK06';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
			}
			//end of code as on 170812.

			nlapiLogExecution('DEBUG', 'ContainerLPNo (Pack Complete)', ContainerLPNo);
			nlapiLogExecution('DEBUG', 'WaveNo (Pack Complete)', WaveNo);
			var taskfilters = new Array();

			taskfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));//Task Type - PICK
			taskfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','28']));// Pick Confirm and Pack Complete
			if(WaveNo!=null && WaveNo!='')
				taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));	

			if(vZoneId != null && vZoneId != '')
			{	
				nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);
				taskfilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
			}

			var taskcolumns = new Array();
			taskcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
			taskcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			taskcolumns[2] = new nlobjSearchColumn('custrecord_invref_no');
			taskcolumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
			taskcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			taskcolumns[5] = new nlobjSearchColumn('name');
			taskcolumns[6] = new nlobjSearchColumn('custrecord_mast_ebizlp_no');
			taskcolumns[0].setSort();

			var tasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskfilters, taskcolumns);

			if(tasksearchresults!=null)
			{
				for (var i = 0; i < tasksearchresults.length; i++)
				{
					ContainerLPNo = tasksearchresults[i].getValue('custrecord_container_lp_no');
					Item = tasksearchresults[i].getValue('custrecord_ebiz_sku_no');
					var salesorderintrid = tasksearchresults[i].getValue('custrecord_ebiz_order_no');
					vDoname = tasksearchresults[i].getValue('name');
					var ebizmasterLp=tasksearchresults[i].getValue('custrecord_mast_ebizlp_no');
					nlapiLogExecution('DEBUG', 'voldcontainer', voldcontainer);
					nlapiLogExecution('DEBUG', 'ContainerLPNo', ContainerLPNo);
					nlapiLogExecution('DEBUG', 'salesorderintrid', salesorderintrid);
					nlapiLogExecution('DEBUG', 'vondemandstage', vondemandstage);

					if(voldcontainer!=ContainerLPNo)
					{ 	 
						//Case# 20149736 starts (passing salesorderintrid)
						/*CreateSTGMRecord(ContainerLPNo, WaveNo, vCompany,WHLocation,
								Item,stgDirection,vCarrier,vSite,vstageLoc,vZoneId,vondemandstage,ebizmasterLp);*/
						CreateSTGMRecord(ContainerLPNo, WaveNo, vCompany,WHLocation,
								Item,stgDirection,vCarrier,vSite,vstageLoc,vZoneId,vondemandstage,ebizmasterLp,salesorderintrid);
						//Case# 20149736, 20149731 ends
						voldcontainer=ContainerLPNo;
					}
				}
			}	
			/*//AutoBuildShipUnits start
			autobuildshipunits(WaveNo,vZoneId,vnewcontainelp);
			//AutoBuildShipUnits end		
			 */			if(vondemandstage == 'Y')
			 {
				 response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_container', 'customdeploy_rf_bulkpick_container_di', false, null);
			 }
			 else
			 {
				 var vFootPrintFlag='N'; // Put Y to navigate to Dynacraft specific screen for scanning 'No. of foot prints/Pallets'
				 if(vFootPrintFlag != 'Y')
				 {
					 var vOpenTaskRecs=GetOrdDetails(WaveNo,vZoneId);
					 if(vOpenTaskRecs != null && vOpenTaskRecs != '' &&  vOpenTaskRecs.length >  1)
					 {
						 var NextShowLocation=vOpenTaskRecs[0].getValue('custrecord_actbeginloc',null,'group');
						 var NextShowItem=vOpenTaskRecs[0].getValue('custrecord_sku',null,'group');
						 var SOSearchResult=vOpenTaskRecs[0];
						 SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no',null,'group');
						 SOarray["custparam_zoneid"] = vZoneId;
						 SOarray["custparam_zoneno"] = getZoneNo;
						 SOarray["custparam_ebizzoneno"] = vZoneId;

						 SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
						 SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
						 SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
						 SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku',null,'group');
						 SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku',null,'group');
						 SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc',null,'group');
						 SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

						 SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
						 SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id',null,'group');


						 SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container',null,'group');
						 //SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no',null,'group');

						 SOarray["custparam_venterwave"]=WaveNo;
						 SOarray["custparam_venterzone"]=getZoneNo;

						 nlapiLogExecution('Debug', 'Navigating To1', 'Picking Location');
						 response.sendRedirect('SUITELET', 'customscript_ebiz_rf_bulkpick_location', 'customdeploy_ebiz_rf_bulkpick_location', false, SOarray);



							 }
							 else
							 {
								 nlapiLogExecution('Debug', 'Navigating Menu', 'Picking Location');
								 response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_container', 'customdeploy_rf_bulkpick_container_di', false, null);
							 }	
						 }
						 else
						 {
							 nlapiLogExecution('DEBUG', 'Navigating To1', 'Foot print');
							 response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, SOarray);
						 }
					 }
					 nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
				else
				{
					SOarray["custparam_error"] = st7;//'INVALID OPTION';
					nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Option');
					SOarray["custparam_screenno"] = 'BULK06';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				}

			}
			catch (e) 
			{
				nlapiLogExecution('DEBUG', 'exception',e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} 
			finally
			{					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			
			SOarray["custparam_error"] = 'Stage Location is already in Process';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}


function ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo,SerialNo,vBatchno){
	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');

	nlapiLogExecution('DEBUG', "Line Count" + lineCnt);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	/*    
     for (var k = 1; k <= lineCnt; k++)
     {
     var chkVal = request.getLineItemValue('custpage_items', 'custpage_so', k);
     var SONo
     var ItemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
     var itemno = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
     var Lineno = request.getLineItemValue('custpage_items', 'custpage_lineno', k);
     var pickqty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
     var vinvrefno = request.getLineItemValue('custpage_items', 'custpage_invrefno', k);

     var WaveNo = request.getParameter('hdnWaveNo');
     var RecordInternalId = request.getParameter('hdnRecordInternalId');
     var ContainerLPNo = request.getParameter('hdnContainerLpNo');
     var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
     var BeginLocation = request.getParameter('hdnBeginLocationId');
     var Item = request.getParameter('hdnItem');
     var ItemDescription = request.getParameter('hdnItemDescription');
     var ItemInternalId = request.getParameter('hdnItemInternalId');
     var DoLineId = request.getParameter('hdnDOLineId');	//
     var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
     var BeginLocationName = request.getParameter('hdnBeginBinLocation');
     var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
     var EndLocation = getEnteredLocation;

     //        var Recid = request.getLineItemValue('custpage_items', 'custpage_recid', k);
     //        nlapiLogExecution('DEBUG', "Recid " + Recid)
	 */
	//updating allocqty in opentask
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
	OpenTaskTransaction.setFieldValue('custrecord_act_qty', parseFloat(ExpectedQuantity).toFixed(4));
	OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
	OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);

	nlapiLogExecution('DEBUG', 'Done PICK Record Insertion :', 'Success');

	//Updating DO 

	var DOLine = nlapiLoadRecord('customrecord_ebiznet_ordline', DoLineId);
	DOLine.setFieldValue('custrecord_linestatus_flag', '1'); //Statusflag='C'
	DOLine.setFieldValue('custrecord_pickqty', parseFloat(ExpectedQuantity).toFixed(4));
	var SalesOrderInternalId = DOLine.getFieldValue('name');

	try {
		nlapiLogExecution('DEBUG', 'Main Sales Order Internal Id', SalesOrderInternalId);
		var TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');

		var SOLength = TransformRec.getLineItemCount('item');
		nlapiLogExecution('DEBUG', "SO Length", SOLength);

		/*
         for (var j = 0; j < SOLength; j++)
         {
         nlapiLogExecution('DEBUG', "Get Line Value", TransformRec.getLineItemValue('item', 'line', j + 1));
         TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
         }
         TransformRec.setLineItemValue('item', 'itemfulfillment', OrderLineNo, 'T');
         TransformRec.setLineItemValue('item', 'quantity', OrderLineNo, ExpectedQuantity);
         var TransformRecId = nlapiSubmitRecord(TransformRec, true);
		 */
		for (var j = 1; j <= SOLength; j++) {
			nlapiLogExecution('DEBUG', "Get Line Value" + TransformRec.getLineItemValue('item', 'line', j ));

			var item_id = TransformRec.getLineItemValue('item', 'item', j);
			var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);

			if (itemLineNo == OrderLineNo) {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
				TransformRec.setCurrentLineItemValue('item', 'quantity', ExpectedQuantity);
				//TransformRec.setCurrentLineItemValue('item', 'location', 1);
				var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
				var serialsspaces="";
				if (itemSubtype.recordType == 'serializedinventoryitem') {

					arrSerial= SerialNo.split(',');
					nlapiLogExecution('DEBUG', "arrSerial", arrSerial);	
					for (var n = 0; n < arrSerial.length; n++) {
						if (n == 0) {
							serialsspaces = arrSerial[n];
						}
						else
						{
							serialsspaces += " " +arrSerial[n];
						}
					}					
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', serialsspaces);
				}
				//vBatchno
				if (itemSubtype.recordType == 'lotnumberedinventoryitem'){
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', vBatchno+'('+ExpectedQuantity+')');
				}


				TransformRec.commitLineItem('item');
			}
			else {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');
				TransformRec.commitLineItem('item');

			}
			//TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
		}
		//			invtRec.setFieldValue('custrecord_ebiz_inv_loc', '1');//WH Location.

		//			TransformRec.setFieldValue('custbody_ebiz_fulfillmentord',vdono) 
		var TransformRecId = nlapiSubmitRecord(TransformRec, true);
	} 

	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}

	///Updating qty,qoh in inventory
	nlapiLogExecution('DEBUG', 'Invoice Ref No', InvoiceRefNo);
	var InventoryTranaction = nlapiLoadRecord('customrecord_ebiznet_createinv', InvoiceRefNo);

	var InvAllocQty = InventoryTranaction.getFieldValue('custrecord_ebiz_alloc_qty');
	var InvQOH = InventoryTranaction.getFieldValue('custrecord_ebiz_qoh');

	if (isNaN(InvAllocQty)) {
		InvAllocQty = 0;
	}
	InventoryTranaction.setFieldValue('custrecord_ebiz_qoh', (parseFloat(InvQOH) - parseFloat(ExpectedQuantity)).toFixed(4));
	InventoryTranaction.setFieldValue('custrecord_ebiz_alloc_qty', (parseFloat(InvAllocQty) - parseFloat(ExpectedQuantity)).toFixed(4));
	var InventoryTranactionId = nlapiSubmitRecord(InventoryTranaction, false, true);
	OpenTaskTransaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
	nlapiSubmitRecord(OpenTaskTransaction, true);
	nlapiSubmitRecord(DOLine, true);

	//    }

	//    var form = nlapiCreateForm('Pick  Confirmation');
	//    form.addField('custpage_label', 'label', '<h2>Pick Confirmation Successful</h2>');

	//    response.writePage(form);
}

/**
 * 
 * @param vContLp
 * @param vebizWaveNo
 * @param vebizOrdNo
 * @param vDoname
 * @param vCompany
 * @param vlocation
 * @param vdono
 * @param Item
 * @param stgDirection
 * @param vCarrier
 * @param vSite
 */
function CreateSTGMRecord(vContLp, vebizWaveNo,  vCompany,vlocation,
		Item,stgDirection,vCarrier,vSite,stageLocation,vZoneId,vondemandstage,ebizmasterLp,salesorderintrid) { //Case# 20149736 ,20149731

	nlapiLogExecution('DEBUG', 'into CreateSTGMRecord');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,
			'is', '9')); //STATUS.OUTBOUND.PICK_GENERATED('G') 
	if(vebizWaveNo != null && vebizWaveNo != '')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',
				vebizWaveNo));
	if(vContLp!=null && vContLp!='' && vContLp!='null')
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,
				'is', vContLp));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'isempty'));
	}
	if(vZoneId!=null && vZoneId!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null,
				'is', vZoneId));
	/*if(vClusterNo!=null && vClusterNo!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null,
				'is', vClusterNo));*/

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',
			null, filters, columns);
	if (searchresults == null || vondemandstage=='Y') {
		if(stageLocation==null || stageLocation=='')
			stageLocation = GetPickStageLocation(Item, vCarrier, vSite, vCompany,
					stgDirection,null,null,null,null,null);
		var STGMtask = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'stageLocation',stageLocation);
		//nlapiLogExecution('DEBUG', 'Name',vDoname);
		//nlapiLogExecution('DEBUG', 'DO NO',vdono);
		/*if(vDoname==null || vDoname=='')
			vDoname = vdono;*/
		//STGMtask.setFieldValue('name', vDoname);
		//STGMtask.setFieldValue('custrecord_ebiz_concept_po', vebizOrdNo);
		STGMtask.setFieldValue('name', vContLp);
		STGMtask.setFieldValue('custrecord_tasktype', '13');//STATUS.OUTBOUND.ORDERLINE_PARTIALLY_SHIPPED('PS')
		STGMtask.setFieldValue('custrecord_wms_location', vlocation);
		STGMtask.setFieldValue('custrecord_actbeginloc', stageLocation);
		STGMtask.setFieldValue('custrecord_actendloc', stageLocation);
		STGMtask.setFieldValue('custrecord_container_lp_no', vContLp);
		//STGMtask.setFieldValue('custrecord_ebiz_cntrl_no', vdono);
		//STGMtask.setFieldValue('custrecord_ebiz_receipt_no', vdono);
		STGMtask.setFieldValue('custrecord_ebiz_wave_no', vebizWaveNo);
		STGMtask.setFieldValue('custrecordact_begin_date', DateStamp());
		//Case# 20149653 starts
		//STGMtask.setFieldValue('custrecordact_end_date', DateStamp());
		STGMtask.setFieldValue('custrecord_act_end_date', DateStamp());
		//Case# 20149653 ends
		STGMtask.setFieldValue('custrecord_actualendtime', TimeStamp());
		STGMtask.setFieldValue('custrecord_actualbegintime', TimeStamp());
		STGMtask.setFieldValue('custrecord_sku', Item);
		STGMtask.setFieldValue('custrecord_ebiz_sku_no', Item);
		//STGMtask.setFieldValue('custrecord_ebiz_order_no', salesorderintrid);
		STGMtask.setFieldValue('custrecord_ebiz_order_no', salesorderintrid);// Case# 20149653
		STGMtask.setFieldValue('custrecord_mast_ebizlp_no', ebizmasterLp);
		//STGMtask.setFieldValue('custrecord_comp_id', vCompany);
		var currentContext = nlapiGetContext();
		var currentUserID = currentContext.getUser();
		STGMtask.setFieldValue('custrecord_ebizuser', currentUserID);
		var retSubmitLP= nlapiSubmitRecord(STGMtask);
		nlapiLogExecution('DEBUG', 'out of CreateSTGMRecord');

		nlapiLogExecution('DEBUG', 'updating outbound inventory with satge location',stageLocation);
		updateStageInventorywithStageLoc(vContLp,stageLocation);
	}
}


function updateStageInventorywithStageLoc(contlpno,stageLocation)
{
	nlapiLogExecution('DEBUG', 'into updateStageInventorywithStageLoc');
	nlapiLogExecution('DEBUG', 'contlpno',contlpno);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null,'is', '18')); //FLAG.INVENTORY.OUTBOUND('O') 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is', contlpno));
	filters.push(new nlobjSearchFilter('custrecord_invttasktype', null,'is', '3')); ////Task Type - PICK

	var columns = new Array();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null, filters, columns);
	if (invtsearchresults != null && invtsearchresults != '' && invtsearchresults.length > 0) 
	{
		for (var i = 0; i < invtsearchresults.length; i++)
		{
			var invtid=invtsearchresults[i].getId();

			nlapiLogExecution('DEBUG', 'Deleting Outbound Inventory....',invtid);
			nlapiSubmitField('customrecord_ebiznet_createinv', invtid, 'custrecord_ebiz_inv_binloc', stageLocation);

//			var stgmInvtRec = nlapiLoadRecord('customrecord_ebiznet_createinv',invtid);
//			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', stageLocation);
//			var invtrecid = nlapiSubmitRecord(stgmInvtRec, false, true);

			//nlapiLogExecution('DEBUG', 'updateStageInventorywithStageLoc is done',invtrecid);
		}		
	}

	nlapiLogExecution('DEBUG', 'out of updateStageInventorywithStageLoc');
}

/**
 * 
 * @param invtrecid
 * @param vContLp
 * @param Item
 * @param vCarrier
 * @param vSite
 * @param vCompany
 * @param stgDirection
 * @param vqty
 */
function CreateSTGInvtRecord(invtrecid, vContLp, Item, vCarrier, vSite,
		vCompany, stgDirection, vqty,vstageLoc) {
	try {
		nlapiLogExecution('DEBUG', 'into CreateSTGInvtRecord');
		if(vstageLoc==null || vstageLoc=='')
			vstageLoc = GetPickStageLocation(Item, vCarrier, vSite, vCompany,
					stgDirection,null,null,null,null,null);
		nlapiLogExecution('DEBUG', 'vstagLoc', vstageLoc);
		if (vstageLoc != null && vstageLoc != "" && vstageLoc != "-1") {

			var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',
					invtrecid);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', vstageLoc);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
			stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
			stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
			stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
			stgmInvtRec.setFieldValue('custrecord_invttasktype', '3');//STATUS.INBOUND.PUTAWAY_COMPLETE('S')
			stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');			
			nlapiSubmitRecord(stgmInvtRec, false, true);

			nlapiLogExecution('DEBUG', 'out of CreateSTGInvtRecord');
		}
	} catch (e) {
		if (e instanceof nlobjError)
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		else
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}
}


function GetStageLocationFromSTGM(waveno,vZoneId)
{
	nlapiLogExecution('DEBUG', 'Into GetStageLocationFromSTGM');
	nlapiLogExecution('DEBUG', 'zoneId', vZoneId);
	nlapiLogExecution('DEBUG', 'waveno', waveno);

	var SOSearchResults = new Array();

	var SOFilters = new Array();

	if(vZoneId!=null && vZoneId!='')
	{
		nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId)); 
	}
	if(waveno!=null && waveno!='')
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveno));
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [13])); // Task Type - STGM

	var SOColumns = new Array();

	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('internalid'));	
	SOColumns[1].setSort(true);

	SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	nlapiLogExecution('DEBUG', 'Out of GetStageLocationFromSTGM',SOSearchResults);

	return SOSearchResults;	
}


function GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,customizetype,
		StageBinInternalId,ordertype,vcustomer){

	nlapiLogExecution('DEBUG', 'into GetPickStageLocation', Item);
	nlapiLogExecution('DEBUG', 'Item', Item);
	nlapiLogExecution('DEBUG', 'vSite', vSite);
	nlapiLogExecution('DEBUG', 'vCompany', vCompany);
	nlapiLogExecution('DEBUG', 'stgDirection', stgDirection);
	nlapiLogExecution('DEBUG', 'vCarrier', vCarrier);
	nlapiLogExecution('DEBUG', 'vCarrierType', vCarrierType);
	nlapiLogExecution('DEBUG', 'ordertype', ordertype);
	nlapiLogExecution('DEBUG', 'customizetype', customizetype);
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();

	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 = "";
	var ItemInfo3 = "";
	var columns = nlapiLookupField('item', Item, fields);
	if(columns!=null){
		ItemFamily = columns.custitem_item_family;
		ItemGroup = columns.custitem_item_group;
		ItemStatus = columns.custitem_ebizdefskustatus;
		ItemInfo1 = columns.custitem_item_info_1;
		ItemInfo2 = columns.custitem_item_info_2;
		ItemInfo3 = columns.custitem_item_info_3;
	}

	nlapiLogExecution('DEBUG', 'ItemFamily', ItemFamily);
	nlapiLogExecution('DEBUG', 'ItemGroup', ItemGroup);
	nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);
	nlapiLogExecution('DEBUG', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('DEBUG', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('DEBUG', 'ItemInfo3', ItemInfo3);

	nlapiLogExecution('DEBUG', 'Before Variable declaration');

	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}

	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
	if (vCarrier != null && vCarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

	} // code added from Boombah account on 25Feb13 by santosh
	else
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@']));
	// upto here
	if (vCarrierType != null && vCarrierType != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}
	if (ordertype != null && ordertype != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@', ordertype]));

	}
	else
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@']));
	if (customizetype != null && customizetype != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@', customizetype]));

	} // code added from Boombah account on 25Feb13 by santosh
	else
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@']));
	//upto here
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'noneof', ['14']));

	if (vcustomer != null && vcustomer != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_customer', null, 'anyof', ['@NONE@', vcustomer]));
	}
	/*if (StageBinInternalId != null && StageBinInternalId != "")
		//filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
	filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_outboundlocationgroup', 'anyof', StageBinInternalId));*/
	nlapiLogExecution('DEBUG', 'Before Searching of Stageing Rules');
	 filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));//Case# 20149862 ,20149863

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	nlapiLogExecution('DEBUG', 'searchresults',searchresults);
	if (searchresults != null) {
		for (var i = 0; i < Math.min(10, searchresults.length); i++) {
			//GetPickStagLpCount                
			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			var vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');

			nlapiLogExecution('DEBUG', 'Stageing Location Value::', vstgLocation);
			nlapiLogExecution('DEBUG', 'Stageing Location Text::', vstgLocationText);


			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');

			nlapiLogExecution('DEBUG', 'vnoofFootPrints::', vnoofFootPrints);


			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetPickStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}


			nlapiLogExecution('DEBUG', 'final stagelocation::', vstgLocation);

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('DEBUG', 'final stagelocation::', vstgLocation);
			nlapiLogExecution('DEBUG', 'LocGroup::', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				//if (stgDirection = 'INB') { //Case# 20149862 , 20149863
					if (stgDirection == 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}
					filtersLocGroup.push(new nlobjSearchFilter('isinactive', null, 'is', 'F')); // Case#20149862 ,20149863
				//This code is commented on 9th September 2011 by ramana
				/*
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				for (var k = 0; searchresults != null && k < searchresults.length; k++) {
					var vLocid=searchresults[k].getId();
						var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
						var vnoofFootPrints = searchresults[k].getValue('custrecord_nooffootprints');

						//--
						if (vnoofFootPrints != null && vnoofFootPrints != "") {
								if (vLpCnt < vnoofFootPrints) {
									return vLocid;
								}
						}
					else {
						return vLocid;
						}
				}
				 */

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('DEBUG', 'searchresultsloc::', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();
//					var vLocid=searchresultsloc[0].getId();

					nlapiLogExecution('DEBUG', 'vLocid::', vLocid);

					var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');

					//--
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}
			}
		}
	}
	return -1;
}

/**
 * 
 * @param location
 * @param site
 * @param company
 * @returns {Number}
 */
function GetPickStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');

	var outLPCount = 0;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));

	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));

	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');

	return outLPCount;
}

function ValidateStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,vcustomizetype,stagelocation)
{
	try
	{
		nlapiLogExecution('DEBUG','Into ValidateStageLocation',stagelocation);
		nlapiLogExecution('DEBUG','WHLocation',vSite);
		var StageBinInternalIdArray=new Array();
		StageBinInternalIdArray[0]=false;
		var StageBinInternalId=fetchStageInternalId(stagelocation,vSite);
		if(StageBinInternalId!=null&&StageBinInternalId!="")
		{

			//var stagebinloc=GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,vcustomizetype,StageBinInternalId,null);
//			nlapiLogExecution('DEBUG','stagebinloc',stagebinloc);
//			if(stagebinloc!=-1)
//			{
			StageBinInternalIdArray[0]=true;
			StageBinInternalIdArray[1]=StageBinInternalId;
//			}
			/*var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', ['2', '3']));
			filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'noneof', ['14']));
			filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
			if(WHLocation!=null&&WHLocation!="")
				filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', WHLocation]));

			if (vCarrier != null && vCarrier != "") 
				filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

			if (vCarrierType != null && vCarrierType != "")
				filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
			columns[0].setSort();
			columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
			columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
			columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
			columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

			nlapiLogExecution('DEBUG', 'Before Searching of Stageing Rules');


			var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, null);
			if(searchresults!=null&&searchresults!="")
			{
				StageBinInternalIdArray[0]=true;
				StageBinInternalIdArray[1]=StageBinInternalId;
			}*/
		}
		nlapiLogExecution('DEBUG','StageBinInternalIdArray',StageBinInternalIdArray);
		return StageBinInternalIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in ValidateStageLocation',exp);
	}
}

function fetchStageInternalId(stageLoc,vSite)
{
	try
	{
		nlapiLogExecution('DEBUG','Into fetchStageInternalId',stageLoc);
		var FetchBinLocation="";
		var collocGroup = new Array();
		collocGroup[0] = new nlobjSearchColumn('internalid');

		var filtersLocGroup = new Array();
		filtersLocGroup.push(new nlobjSearchFilter('name', null, 'is',stageLoc));
		filtersLocGroup.push(new nlobjSearchFilter('custrecord_ebizlocationtype',null,'anyof',8));//8=Stage
		//case# 20148627 starts
		filtersLocGroup.push(new nlobjSearchFilter('isinactive', null, 'is','F'));
		//case# 20148627 ends
		if(vSite!=null&&vSite!="")
			filtersLocGroup.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null,'anyof',vSite));
		var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
		if(searchresultsloc != null && searchresultsloc != '')
			FetchBinLocation=searchresultsloc[0].getValue('internalid');
		nlapiLogExecution('DEBUG','FetchBinLocation',FetchBinLocation);
		return FetchBinLocation;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in fetchStageInternalId',exp);
	}
}

function GetFODetails(getWaveNo,vZoneId)
{
	var SOFilters = new Array();
	if(getWaveNo != null && getWaveNo != '')
	{	 
		nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
	}

	if(vZoneId != null && vZoneId != '')
	{	
		nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
	}
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [7,8,28]));//pick gen
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pick gen
	var SOColumns = new Array();

	//SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group'));			 
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no',null,'group'));


	SOColumns[0].setSort();


	var SOSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	nlapiLogExecution('DEBUG', 'SOSearchResults1', SOSearchResults1);
	return SOSearchResults1;
}

function GetOrdDetails(getWaveNo,vZoneId)
{
	var SOFilters = new Array();
	if(getWaveNo != null && getWaveNo != '')
	{	 
		nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
	}

	if(vZoneId != null && vZoneId != '')
	{	
		nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
	}
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));//pick gen
	var SOColumns = new Array();

	//SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group'));			 
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty',null,'sum'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc',null,'group'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group')); 
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no',null,'group')); 
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid',null,'group')); 
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container',null,'group'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort(3);
	SOColumns[4].setSort();
	SOColumns[5].setSort();

	var SOSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	nlapiLogExecution('DEBUG', 'SOSearchResults1', SOSearchResults1);
	return SOSearchResults1;
}
//case # 20123528 start
var vSiteId;
var vCompId,vwhlocation="";
function autobuildshipunits(WaveNo,vZoneId,vnewcontainelp){

	nlapiLogExecution('ERROR',"autobuildshipunits ", "Into autobuildshipunits" );
	nlapiLogExecution('ERROR',"vZoneId ", vZoneId );
	nlapiLogExecution('ERROR',"WaveNo ", WaveNo );
	nlapiLogExecution('ERROR',"vnewcontainelp ", vnewcontainelp );
	var shipasisID=getShipAsisId(vZoneId,WaveNo,vnewcontainelp);
	nlapiLogExecution('ERROR',"shipasisID ", shipasisID );
	if(shipasisID!=null&&shipasisID!="")
	{
		var filters = new Array();
		var searchresults =new Array();

		if(WaveNo!="" && WaveNo!=null)
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', WaveNo));
		if(vZoneId!="" && vZoneId!=null)
			filters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is',vZoneId));

		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//Pack complete
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pic
		filters.push(new nlobjSearchFilter('custrecord_container',  null,'isnotempty'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_act_qty');
		columns[3] = new nlobjSearchColumn('custrecord_tasktype');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_ship_lp_no');
		columns[5] = new nlobjSearchColumn('custrecord_actendloc');
		columns[6] = new nlobjSearchColumn('custrecord_sku');
		columns[7] = new nlobjSearchColumn('name');    	
		columns[8] = new nlobjSearchColumn('custrecord_container_lp_no');    	 
		columns[9] = new nlobjSearchColumn('custrecord_sku_status');
		columns[10] = new nlobjSearchColumn('custrecord_packcode');
		columns[11] = new nlobjSearchColumn('custrecord_uom_id');
		columns[12] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[13] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');		
		columns[14] = new nlobjSearchColumn('custrecord_totalcube');
		columns[15] = new nlobjSearchColumn('custrecord_total_weight');
		columns[16] = new nlobjSearchColumn('custrecord_comp_id');
		columns[17] = new nlobjSearchColumn('custrecord_site_id');
		columns[18] = new nlobjSearchColumn('custrecord_batch_no');
		columns[19] = new nlobjSearchColumn('custrecord_wms_location');
		columns[20] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[8].setSort();


		searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);


		if(searchresults!="" && searchresults!=null){
			var WhLoc=searchresults[0].getValue('custrecord_wms_location');
			nlapiLogExecution('ERROR',"WhLoc", WhLoc);
			var vebizOrdNo=searchresults[0].getValue('custrecord_ebiz_order_no');
			var whlocation=searchresults[0].getValue('custrecord_wms_location');

			//start: To get the CarrierType

			var vOrdAutoPackFlag="";
			var vStgAutoPackFlag="";
			var vStgCarrierType="";
			var vCarrierType="";
			var vStgCarrierName="";
			var vorderType='';
			var nsshipmethod='';
			var vcarrier='';
			var vcarrierline='';
			var vcarrierlineID='';
			var vCarrierTypeID='';
			var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vebizOrdNo);
			if(vOrderAutoPackFlag!=null&& vOrderAutoPackFlag.length>0){
				vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
				vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
				vorderType = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
				nsshipmethod = vOrderAutoPackFlag[0].getValue('shipmethod');
			}

			if(vCarrierType==null || vCarrierType=="")
			{
				if(nsshipmethod!=null && nsshipmethod!='')
				{
					var filterscarr = new Array();
					var columnscarr = new Array();

					filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',nsshipmethod));
					filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

					columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
					columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
					columnscarr[2] = new nlobjSearchColumn('id');

					var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
					if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
					{
						vCarrierType = searchresultscarr[0].getText('custrecord_carrier_type');
						vcarrier = searchresultscarr[0].getId();
					}
				}
				else
				{
					var filterscarr = new Array();
					var columnscarr = new Array();
					filterscarr.push(new nlobjSearchFilter('custrecord_lineord', null, 'is',vebizOrdNo));
					filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));
					columnscarr[0] = new nlobjSearchColumn('custrecord_do_carrier');

					var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filterscarr, columnscarr);
					if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
					{					
						vcarrierline=searchresultscarr[0].getText('custrecord_do_carrier');
						vcarrierlineID=searchresultscarr[0].getValue('custrecord_do_carrier');
					}
					nlapiLogExecution('ERROR', 'vcarrierline', vcarrierline);
					if(vcarrierline!=null && vcarrierline!='')
					{
						var filterscarrtype = new Array();
						var columnscarrtype = new Array();
						//filterscarrtype.push(new nlobjSearchFilter('name', null, 'is',vcarrierline));
						filterscarrtype.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'anyof',vcarrierlineID));
						filterscarrtype.push(new nlobjSearchFilter('isinactive', null, 'is','F'));
						columnscarrtype[0] = new nlobjSearchColumn('custrecord_carrier_type');

						var searchresultscarrtype = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarrtype, columnscarrtype);
						if(searchresultscarrtype!=null && searchresultscarrtype!='')
						{
							vCarrierType=searchresultscarrtype[0].getText('custrecord_carrier_type');
							vcarrier = searchresultscarrtype[0].getId();
							vCarrierTypeID = searchresultscarrtype[0].getValue('custrecord_carrier_type');
						}
					}
				}
			}
			nlapiLogExecution('ERROR', 'vCarrierType', vCarrierType);
			nlapiLogExecution('ERROR', 'vcarrier', vcarrier);
			nlapiLogExecution('ERROR', 'vOrdAutoPackFlag', vOrdAutoPackFlag);
			nlapiLogExecution('ERROR', 'vCarrierTypeID', vCarrierTypeID);
			if(vOrdAutoPackFlag!='T' || (vCarrierType=="" || vCarrierType==null)){
				var vStageAutoPackFlag = getAutoPackFlagforStage(whlocation,vcarrier,vorderType,vCarrierTypeID);
				if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
					vStgCarrierType = vStageAutoPackFlag[0][0];
					vStgAutoPackFlag = vStageAutoPackFlag[0][1];
					vStgCarrierName = vStageAutoPackFlag[0][2];
				}
			}

			if(vCarrierType=="" && vStgCarrierType!="")
				vCarrierType=vStgCarrierType;

			nlapiLogExecution('ERROR', 'vCarrierType', vCarrierType);

			if(vCarrierType!='PC'){
				try {
					var uompackflag="";
					var vWhLoc='';
					var now = new Date();		
					var lineCnt = searchresults.length;
					nlapiLogExecution('ERROR','linecnt',lineCnt);


					for (var n = 0; n < lineCnt; n++) {
						vWhLoc=searchresults[n].getValue('custrecord_wms_location');

						nlapiLogExecution('ERROR','vWhLoc',vWhLoc);
						if(vWhLoc!="")
							break;
					}

					var actualDate = DateStamp();
					var actualTime = TimeStamp();
					var sizeId;
					var item;

					var vDIms=getContainerCubeAndTarWeight(sizeId,"");// To get Container weight and volume from container to calculate tar weight and cube

					var TotWeight=0,TotVolume=0,inventoryInout;
					var vTareWeight=0,vTareVol=0;
					if(vDIms.length >0)			{
						inventoryInout=vDIms[2];
						if(vDIms[1] != null && vDIms[1]!="")
							vTareWeight=parseFloat(vDIms[1]);
						if(vDIms[0] != null && vDIms[0]!="")
							vTareVol=parseFloat(vDIms[0]);
					}
					var NewShipLPNo;var voldContainerLp="";	
					nlapiLogExecution('ERROR',"custpage_shiplp ", request.getParameter('custpage_shiplp') );


					nlapiLogExecution('ERROR',"AutoShipLPNo ", NewShipLPNo );

					var picktaskid;
					var LpMasterId;
					var SONo;
					var item;
					var vBoolAnythingChecked=false;
					var searchresultsLP;
					var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record

					for (var k = 0; k < searchresults.length; k++) {

						var vwhlocation=searchresults[k].getValue('custrecord_wms_location');
						var containerlpno=searchresults[k].getValue('custrecord_container_lp_no') ;
						searchresultsLP=GetLPdetails(containerlpno,vwhlocation);



						//	var chkVal = request.getLineItemValue('custpage_items', 'custpage_so', k);
						picktaskid = searchresults[k].getId();
						//request.getLineItemValue('custpage_items', 'custpage_pickid', k);
						LpMasterId = searchresultsLP[0].getId();
						//request.getLineItemValue('custpage_items', 'custpage_lpgid', k);
						uompackflag = searchresults[k].getValue('custrecord_uom_id');
						//request.getLineItemValue('custpage_items', 'custpage_uomid', k);
						SONo = searchresults[k].getValue('custrecord_ebiz_order_no');
						//request.getLineItemValue('custpage_items', 'custpage_orderno', k);
						item = searchresults[k].getValue('custrecord_sku');
						//request.getLineItemValue('custpage_items', 'custpage_skuval', k);
						nlapiLogExecution('ERROR', "uompackflag  " , uompackflag);
						//SONo = request.getLineItemValue('custpage_items', 'custpage_sono', k);

						var vCartWeight = searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_totwght');
						//request.getLineItemValue('custpage_items', 'custpage_cartwt', k);
						var vCartVol = searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_totcube');
						//request.getLineItemValue('custpage_items', 'custpage_cartvol', k);
						var vContainerLp=searchresults[0].getValue('custrecord_container_lp_no');
						//request.getLineItemValue('custpage_items','custpage_cartonid',k);
						nlapiLogExecution('ERROR','voldContainerLp',voldContainerLp);
						nlapiLogExecution('ERROR','vConatierLp',vContainerLp);


						//if (chkVal == 'T') {//If it is checked

						nlapiLogExecution('ERROR', "k1", k);

						//To calculate Total weight and volume
						vBoolAnythingChecked=true;
						//nlapiLogExecution('ERROR','vCartWeight',vCartWeight);
						if(vCartWeight!=null && vCartWeight !="")
							TotWeight+=parseFloat(vCartWeight);
						//nlapiLogExecution('ERROR','TotWeight',TotWeight);
						if(vCartVol!=null && vCartVol !="")
							TotVolume+=parseFloat(vCartVol);

						if(voldContainerLp != vContainerLp)	
						{
							//To calulate auto Buildship# from LP Range
							var AutoShipLPNo=vContainerLp;
							NewShipLPNo=AutoShipLPNo;
						}
						//}

						// To update ShipLP# in opentask
						var fieldNames = new Array(); 
						fieldNames.push('custrecord_wms_status_flag');  	
						fieldNames.push('custrecord_ship_lp_no');
						fieldNames.push('custrecord_container_lp_no');

						var newValues = new Array(); 
						newValues.push('7');
						newValues.push(NewShipLPNo);
						newValues.push(vContainerLp);


						nlapiSubmitField('customrecord_ebiznet_trn_opentask', picktaskid, fieldNames, newValues);

						nlapiLogExecution('ERROR', 'opentask updated ', "");

						// To update ShipLP# in mast LP
						var fieldNames2 = new Array(); 
						fieldNames2.push('custrecord_ebiz_lpmaster_masterlp');  	

						var newValues2 = new Array(); 					 
						newValues2.push(NewShipLPNo);

						nlapiSubmitField('customrecord_ebiznet_master_lp', LpMasterId, fieldNames2, newValues2);

						nlapiLogExecution('ERROR', 'LP Master updated ', ""); 


						if(voldContainerLp != vContainerLp)	
						{
							var blnClosedFlag;

							if(inventoryInout=="1" || inventoryInout =="IN")
							{
								TotWeight+=vTareWeight;
								TotVolume=vTareVol;
							}

							parent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', NewShipLPNo);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', '4');
							if(sizeId != null && sizeId != '')
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', sizeId);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', WaveNo);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_totalcube', parseFloat(TotVolume).toFixed(4));
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', parseFloat(TotWeight).toFixed(4));
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ship_lp_no', NewShipLPNo);
							if(item != null && item != '')
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', item);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date', actualDate);
							if(SONo != null && SONo != '')
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', SONo);

							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '28');


							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', actualTime);
							if(vSiteId != null && vSiteId != '')
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_site_id', vSiteId);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ship_lp_no', NewShipLPNo);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ship_lp_no', NewShipLPNo);
							parent.commitLineItem('recmachcustrecord_ebiz_ot_parent');
							nlapiLogExecution('ERROR', 'Submitting SHIP record', 'TRN_OPENTASK');


							var ResultText=GenerateSucc(vWhLoc,uompackflag);
							nlapiLogExecution('ERROR', 'GenerateSucc result', ResultText);

							parent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent'); 
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', NewShipLPNo);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', NewShipLPNo);
							if(sizeId != null && sizeId != '')
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sizeid', sizeId);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(4));
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', '3');
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', ResultText);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totcube', parseFloat(TotVolume).toFixed(4));
							var status;

							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_wmsstatusflag', '28');

							parent.commitLineItem('recmachcustrecord_ebiz_lp_parent');

							var BuildShipUnitCompletedBoolValue = blnBuildShipUnitCompleted(NewShipLPNo);
							nlapiLogExecution('ERROR', "BuildShipUnitCompletedBoolValue start", BuildShipUnitCompletedBoolValue);

							nlapiLogExecution('ERROR', "k2", k);


						}
						voldContainerLp=vContainerLp;
						nlapiLogExecution('ERROR','NewShipLPNo after',NewShipLPNo);
					}
//					If atleast one item checked need to update checked items in opentask and LP Master
					nlapiSubmitRecord(parent); //submit the parent record, all child records will also be updated
					var context = nlapiGetContext();
					nlapiLogExecution('ERROR','Remaining usage 4',context.getRemainingUsage());


				}
				catch(exp) {
					nlapiLogExecution('ERROR', 'Exception in Buildshipunit ', exp);

				}

			}
		}
	}
	nlapiLogExecution('ERROR',"autobuildshipunits ", "Outof autobuildshipunits" );
}
function getAutoShipLP(vWhLoc)
{
	var AutoShipLPNo="1";	 
	AutoShipLPNo=GetMaxLPNo('1', '3',vWhLoc);// for Auto generated with SHIP LPType
	return AutoShipLPNo;
}
function GetLPdetails(contlp,vwhlocation)
{
	// To get the Container Details from master lp

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
	columns[4] = new nlobjSearchColumn('internalid');
	columns[0].setSort();

	var filters = new Array();
	if(contlp!= null && contlp!="")
	{			
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', contlp));
	}
	if(vwhlocation!= null && vwhlocation!="")
	{			
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', ["@NONE@",vwhlocation]));
	}
	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);

	return SearchResults;


}
function GenerateSucc(vWhLoc,uompackflag)
{
	var finaltext="";
	var duns="";
	var label="",uom="0",ResultText="";
	nlapiLogExecution('ERROR', 'GenerateSucc',vWhLoc);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', 5));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', 1));
	if(vWhLoc!=null && vWhLoc!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', [vWhLoc]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_begin');
	columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_end');
	columns[3] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype');
	columns[4] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype');
	columns[5] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);
//	nlapiLogExecution('ERROR', 'Lp Range Searchresults', searchresults.length);
	if (searchresults !=null && searchresults !="") 
	{nlapiLogExecution('ERROR', 'Lp Range Searchresults', searchresults.length);
//	for (var i = 0; i < searchresults.length; i++) 
//	{
	try 
	{
		var company = searchresults[0].getText('custrecord_ebiznet_lprange_company');	
		var getLPrefix = searchresults[0].getValue('custrecord_ebiznet_lprange_lpprefix');					
		var varBeginLPRange = searchresults[0].getValue('custrecord_ebiznet_lprange_begin');
		var varEndRange = searchresults[0].getValue('custrecord_ebiznet_lprange_end');
		var getLPGenerationTypeValue = searchresults[0].getValue('custrecord_ebiznet_lprange_lpgentype');
		var getLPTypeValue = searchresults[0].getText('custrecord_ebiznet_lprange_lptype');

		var lpMaxValue=GetMaxLPNo('1', '5',vWhLoc);
		nlapiLogExecution('ERROR', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('ERROR', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;
		nlapiLogExecution('ERROR', 'label', label);

		var dunsfilters = new Array();
		dunsfilters[0] = new nlobjSearchFilter('custrecord_company', null, 'is', company);
		var dunscolumns = new Array();
		dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

		var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
		if (dunssearchresults !=null && dunssearchresults != "") 
		{
			duns = dunssearchresults[0].getValue('custrecord_compduns');
			nlapiLogExecution('ERROR', 'duns', duns);
		}

		if(uompackflag == "1" ) 
			uom="0"; 
		else if(uompackflag == "3") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord uom', uom);
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseFloat(checkStr.charAt(i));
			i--;
		}
		//alert(ARL);
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseFloat(checkStr.charAt(i));
			i--;
		}
		// alert(BRL);
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 
			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	//}
	}
	return  ResultText;
}
function blnBuildShipUnitCompleted(newShipLP)
{
	nlapiLogExecution('ERROR', "blnBuildShipUnitCompleted start", newShipLP);
	var vBlnShipCompleted=true;
	var Filers = new Array;
	//The below code is commnted by Satish.N on 06/18/2012 
//	Filers.push(new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', newShipLP)); 

//	var Columns = new Array();
//	Columns[0] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
//	Columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_trailer_no');
//	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, Filers, Columns);

	Filers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', newShipLP)); 

	var Columns = new Array();
	Columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	Columns[1] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filers, Columns);

	var Salesord,closedtasktrailername;
	//if(SOSearchResults != "" || SOSearchResults != null){
	if(SOSearchResults != "" && SOSearchResults != null){
		Salesord=SOSearchResults[0].getValue('custrecord_ebiz_trailer_no');
		closedtasktrailername=SOSearchResults[0].getValue('custrecord_ebiz_trailer_no');
		nlapiLogExecution('ERROR', "Salesordp", Salesord);			 
	}
	if(blnShipComplete(Salesord)){
		nlapiLogExecution('ERROR', "Salesordpshipcompletetrue", '');	
		var Filers = new Array;
		Filers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', Salesord)); 
		Filers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); 
		Filers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', '28')); //pack complete

		var Columns = new Array();
		Columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');

		var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filers, Columns);
		if(ContainerSearchResults == "" || ContainerSearchResults == null){
			nlapiLogExecution('ERROR', "ContainerSearchResults is empty", '');	
			vBlnShipCompleted = true;

		}
		else
		{
			for (var k = 0; k < ContainerSearchResults.length; k++) 
			{
				nlapiLogExecution('ERROR', "ContainerSearchResults",ContainerSearchResults.length );		
				var containerlp = ContainerSearchResults[k].getValue('custrecord_container_lp_no');

			}
			vBlnShipCompleted = false;
		}



	}
	nlapiLogExecution('ERROR', 'vBlnShipCompleted returning value',vBlnShipCompleted);
	return vBlnShipCompleted;
}
function blnShipComplete(vSOInternalId)
{
	var vBlnShipComp=true;
	var filters = new Array();

	filters.push(new nlobjSearchFilter('name', null,
			'is', vSOInternalId)); 


	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
	columns.push(new nlobjSearchColumn('custrecord_pickqty'));
	columns.push(new nlobjSearchColumn('custrecord_shipcomplete'));


	var searchresultsordline = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if(searchresultsordline != null && searchresultsordline != "" && searchresultsordline.length>0)
	{
		nlapiLogExecution('ERROR', 'searchresultsordline.length',searchresultsordline.length);
		for(var v=0;v<searchresultsordline.length;v++)
		{
			var vlinePickQty=0;
			var vlineOrdQty=0;
			if(searchresultsordline[v].getValue('custrecord_ord_qty')!= null && searchresultsordline[v].getValue('custrecord_ord_qty') != "")
				vlineOrdQty=parseFloat(searchresultsordline[v].getValue('custrecord_ord_qty'));

			if(searchresultsordline[v].getValue('custrecord_pickqty') != null && searchresultsordline[v].getValue('custrecord_pickqty') != "")
				vlinePickQty= parseFloat(searchresultsordline[v].getValue('custrecord_pickqty'));

			var vlineShipCompFlag=searchresultsordline[v].getValue('custrecord_shipcomplete');

			nlapiLogExecution('ERROR', 'vlineOrdQty',vlineOrdQty);
			nlapiLogExecution('ERROR', 'vlinePickQty',vlinePickQty);
			nlapiLogExecution('ERROR', 'vlineShipCompFlag',vlineShipCompFlag);

			if(vlineShipCompFlag!= null && vlineShipCompFlag != "" && vlineShipCompFlag =='T' && vlineOrdQty >0)
			{
				if(vlineOrdQty!=vlinePickQty)
				{
					vBlnShipComp=false;

				}
				else
				{
					vBlnShipComp=true;

				}	

			}
			else if(vlineShipCompFlag!= null && vlineShipCompFlag != "" && vlineShipCompFlag =='F' && vlineOrdQty >0)
			{
				vBlnShipComp=false;

			}

		}
	}
	else
		vBlnShipComp=false;
	nlapiLogExecution('ERROR', 'vBlnShipComp',vBlnShipComp);
	return vBlnShipComp;
}
//case # 20123528 end
function getAutoPackFlagforOrdType(vebizOrdNo){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforOrdType eBizOrdNo',vebizOrdNo);
	nlapiLogExecution('ERROR', 'Time Stamp at the start of getAutoPackFlagforOrdType',TimeStampinSec());

	var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
	nlapiLogExecution('ERROR', 'trantype', trantype);
	var searchresults = new Array();

	if(trantype=="salesorder")
	{ 
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
		columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
		columns[2] = new nlobjSearchColumn('custbody_nswmssoordertype');
		columns[3] = new nlobjSearchColumn('shipmethod');

		searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	}
	else		 
	{ 
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
		columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
		columns[2] = new nlobjSearchColumn('custbody_nswmssoordertype');
		columns[3] = new nlobjSearchColumn('shipmethod');

		searchresults = nlapiSearchRecord('transferorder', null, filters, columns);
	}
	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforOrdType', searchresults);
	nlapiLogExecution('ERROR', 'Time Stamp at the end of getAutoPackFlagforOrdType',TimeStampinSec());
	return searchresults;
}

function getAutoPackFlagforStage(whlocation,vcarrier,vorderType,vCarrierTypeID){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforStage');
	nlapiLogExecution('ERROR', 'Time Stamp at the start of getAutoPackFlagforStage',TimeStampinSec());
//	nlapiLogExecution('ERROR', 'whlocation',whlocation);
//	nlapiLogExecution('ERROR', 'vorderType',vorderType);
	var vStgRule = new Array();
	//getStageRule(Item, vCarrier, vSite, vCompany, stgDirection,ordertype)
	nlapiLogExecution('ERROR', 'Time Stamp at the start of getStageRule',TimeStampinSec());
	//vStgRule = getStageRule('', vcarrier, whlocation, '', 'OUB',vorderType);
	vStgRule = getStageRulewithCarrierType('', vcarrier, whlocation, '', 'OUB',vorderType,vCarrierTypeID);
	//nlapiLogExecution('ERROR', 'Stage Rule', vStgRule);
	nlapiLogExecution('ERROR', 'Time Stamp at the end of getStageRule',TimeStampinSec());

	nlapiLogExecution('ERROR', 'Time Stamp at the end of getAutoPackFlagforStage',TimeStampinSec());
	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforStage');
	return vStgRule;
}

function getShipAsisId(vZoneId,WaveNo,vnewcontainelp)
{
	try
	{
		nlapiLogExecution('ERROR', 'getShipAsisId',vZoneId+"/"+WaveNo+"/"+vnewcontainelp);
		var filters = new Array();
		var searchresults;
		var containersizeid="";
		if(WaveNo!="" && WaveNo!=null)
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', WaveNo));
		if(vZoneId!="" && vZoneId!=null)
			filters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is',vZoneId));
		if(vnewcontainelp!="" && vnewcontainelp!=null)
			filters.push(new nlobjSearchFilter('custrecord_mast_ebizlp_no', null, 'is',vnewcontainelp));

		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', [28]));//Picking complete,Pack complete
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pic

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_container');

		searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		nlapiLogExecution("ERROR","searchresults",searchresults);
		if(searchresults!=null&&searchresults!="")
		{
			containersizeid="";
		}
		else
		{
			if(WaveNo!="" && WaveNo!=null)
				filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', WaveNo));
			if(vZoneId!="" && vZoneId!=null)
				filters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is',vZoneId));
			if(vnewcontainelp!="" && vnewcontainelp!=null)
				filters.push(new nlobjSearchFilter('custrecord_mast_ebizlp_no', null, 'is',vnewcontainelp));

			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//Picking complete,Pack complete
			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pic


			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_container');

			searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

			if(searchresults!=null&&searchresults!="")
				containersizeid=searchresults[0].getValue("custrecord_container");
		}
		nlapiLogExecution("ERROR","containersizeid",containersizeid);
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception in getShipAsisId",exp);
	}
}