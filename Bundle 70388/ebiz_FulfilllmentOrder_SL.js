/***************************************************************************
 eBizNET Solutions               
 ***************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_FulfilllmentOrder_SL.js,v $
 *     	   $Revision: 1.42.2.45.4.19.2.74.2.8 $
 *     	   $Date: 2015/12/03 14:20:21 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_FulfilllmentOrder_SL.js,v $
 * Revision 1.42.2.45.4.19.2.74.2.8  2015/12/03 14:20:21  schepuri
 * case# 201414746
 *
 * Revision 1.42.2.45.4.19.2.74.2.7  2015/10/30 10:48:12  rrpulicherla
 * FO changes changes
 *
 * Revision 1.42.2.45.4.19.2.74.2.6  2015/10/27 13:09:07  schepuri
 * case# 201415196
 *
 * Revision 1.42.2.45.4.19.2.74.2.5  2015/10/23 23:19:44  snimmakayala
 * 201415192
 *
 * Revision 1.42.2.45.4.19.2.74.2.4  2015/10/21 08:05:40  deepshikha
 * 2015.2 issue fixes
 * 201414717
 *
 * Revision 1.42.2.45.4.19.2.74.2.3  2015/10/20 07:55:22  grao
 * Lulus issue fixes 201415134
 *
 * Revision 1.42.2.45.4.19.2.74.2.2  2015/09/23 14:57:53  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.42.2.45.4.19.2.74.2.1  2015/09/11 14:09:10  schepuri
 * case# 201414310
 *
 * Revision 1.42.2.45.4.19.2.74  2015/08/26 06:04:31  rrpulicherla
 * Case#201413343
 *
 * Revision 1.42.2.45.4.19.2.73  2015/08/20 20:03:09  sponnaganti
 * Case# 201414075
 * LP SB2 Large order creation with .2 issue fix
 *
 * Revision 1.42.2.45.4.19.2.72  2015/07/06 11:23:18  snimmakayala
 * no message
 *
 * Revision 1.42.2.45.4.19.2.70  2015/06/29 15:08:05  mvarre
 * Case# 201413272
 * Here in this case SO was created but FO's was not created. User has tried to update the sales order through cvs import. In this case we are getting script error
 *
 * Revision 1.42.2.45.4.19.2.69  2015/04/06 12:09:58  gkalla
 * Case# 201412233
 * TF Processing FO large orders issue
 *
 * Revision 1.42.2.45.4.19.2.68  2015/04/06 06:56:33  schepuri
 * case# 201412227
 *
 * Revision 1.42.2.45.4.19.2.67  2015/04/03 13:48:04  schepuri
 * case# 201412227
 *
 * Revision 1.42.2.45.4.19.2.66  2015/03/19 15:27:38  rrpulicherla
 * Case#201411317
 *
 * Revision 1.42.2.45.4.19.2.65  2015/03/16 07:53:31  snimmakayala
 * 201411870
 *
 * Revision 1.42.2.45.4.19.2.64  2015/02/23 17:51:34  rrpulicherla
 * Case#201411317
 *
 * Revision 1.42.2.45.4.19.2.63  2015/02/04 07:29:39  sponnaganti
 * Case# 201411508
 * True Fab outbound process for selected lot in so/to
 *
 * Revision 1.42.2.45.4.19.2.62  2015/01/29 07:52:32  rrpulicherla
 * Case#201411317
 *
 * Revision 1.42.2.45.4.19.2.61  2015/01/23 15:20:23  rrpulicherla
 * Case#201411317
 *
 * Revision 1.42.2.45.4.19.2.60  2014/12/23 06:36:39  spendyala
 * case # 201411292
 *
 * Revision 1.42.2.45.4.19.2.59  2014/12/22 16:44:25  rrpulicherla
 * Case#201411317
 *
 * Revision 1.42.2.45.4.19.2.58  2014/11/27 15:32:17  grao
 * Case# 201411152 DC Dental Issue fixes
 *
 * Revision 1.42.2.45.4.19.2.57  2014/09/02 11:07:08  gkalla
 * case#201410058
 * CSV import issue and pending approval issue
 *
 * Revision 1.42.2.45.4.19.2.56  2014/07/23 23:44:06  gkalla
 * case#1979922
 * DuplicateFO Issue
 *
 * Revision 1.42.2.45.4.19.2.55  2014/07/22 06:59:09  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149587
 *
 * Revision 1.42.2.45.4.19.2.54  2014/07/21 13:09:32  snimmakayala
 * Case: 20149421
 * UOM Conversion issue fix in Schedulers
 *
 * Revision 1.42.2.45.4.19.2.53  2014/07/17 11:05:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * UOM Conversion
 *
 * Revision 1.42.2.45.4.19.2.52  2014/07/04 22:51:32  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149308
 *
 * Revision 1.42.2.45.4.19.2.51  2014/05/29 06:48:28  snimmakayala
 * Case#: 20148595
 *
 * Revision 1.42.2.45.4.19.2.50  2014/05/20 14:35:23  snimmakayala
 * no message
 *
 * Revision 1.42.2.45.4.19.2.49  2014/05/19 15:14:27  sponnaganti
 * case# 20148423
 * Standard Bundle Issue Fix
 *
 * Revision 1.42.2.45.4.19.2.48  2014/05/07 15:17:46  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * FO Creation changes
 *
 * Revision 1.42.2.45.4.19.2.47  2014/04/23 10:06:58  nneelam
 * case#  20148137
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.42.2.45.4.19.2.46  2014/04/23 06:05:44  snimmakayala
 * Case #: 20148132
 *
 * Revision 1.42.2.45.4.19.2.45  2014/04/21 13:59:04  skreddy
 * case # 20127931
 * Dealmed SB  issue fix
 *
 * Revision 1.42.2.45.4.19.2.44  2014/04/18 16:04:54  gkalla
 * case#20126877
 * CSV Import issue fix
 *
 * Revision 1.42.2.45.4.19.2.43  2014/03/07 14:03:30  nneelam
 * case#  20127552
 * Surftech Issue Fix as part of std bundle.
 *
 * Revision 1.42.2.45.4.19.2.42  2014/03/05 12:06:33  snimmakayala
 * Case #: 20127464
 *
 * Revision 1.42.2.45.4.19.2.41  2014/02/12 07:33:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127080
 *
 * Revision 1.42.2.45.4.19.2.40  2014/02/06 14:40:05  skreddy
 * case 20127088
 * Nucourse Prod issue fix
 *
 * Revision 1.42.2.45.4.19.2.39  2014/01/31 15:38:04  gkalla
 * case#20126877   �
 * NuCourse Validations for cvsimport
 *
 * Revision 1.42.2.45.4.19.2.38  2014/01/31 11:34:09  snimmakayala
 * Case# : 20126828
 *
 * Revision 1.42.2.45.4.19.2.37  2014/01/31 11:29:23  snimmakayala
 * Case# : 20126828
 *
 * Revision 1.42.2.45.4.19.2.36  2014/01/20 13:42:12  snimmakayala
 * Case# : 20126712
 *
 * Revision 1.42.2.45.4.19.2.35  2014/01/16 08:39:08  snimmakayala
 * Case# : 20126728
 * # of Lines Validation Issue.
 *
 * Revision 1.42.2.45.4.19.2.34  2014/01/06 15:18:27  gkalla
 * case#20125733
 * Ryonet issue fix for ship complete enabled
 *
 * Revision 1.42.2.45.4.19.2.33  2013/11/21 06:40:14  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to sports hq
 *
 * Revision 1.42.2.45.4.19.2.32  2013/11/14 16:21:42  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20120128
 * FO Creation FIxes
 *
 * Revision 1.42.2.45.4.19.2.31  2013/10/25 20:21:00  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20125337
 *
 * Revision 1.42.2.45.4.19.2.30  2013/10/23 10:17:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related case#20125024 was fixed
 *
 * Revision 1.42.2.45.4.19.2.29  2013/10/16 07:23:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdchanges
 *
 * Revision 1.42.2.45.4.19.2.28  2013/10/11 06:07:52  skreddy
 * Case# 20124822
 * CWD prod issue fix
 *
 * Revision 1.42.2.45.4.19.2.27  2013/09/12 15:42:01  nneelam
 * Case#. 20124380
 * Order Summary Details Issue Fix..
 *
 * Revision 1.42.2.45.4.19.2.26  2013/09/12 15:11:08  gkalla
 * Case# 20120124
 * fulfillment order not creating for back orders issue for Cesium
 *
 * Revision 1.42.2.45.4.19.2.25  2013/08/19 03:04:08  snimmakayala
 * GSUSA PROD ISSUE
 * Case# :201215376
 * Process Large Orders
 *
 * Revision 1.42.2.45.4.19.2.24  2013/08/13 15:22:54  rmukkera
 * Issue Fix for
 * 20123841, 20123840
 *
 * Revision 1.42.2.45.4.19.2.23  2013/08/06 17:02:07  gkalla
 * Case# 20123760
 * Multiple FO creation issue in TSG SB
 *
 * Revision 1.42.2.45.4.19.2.22  2013/08/02 15:46:01  rmukkera
 * Issue fix for
 * FO Creation : System is not creating different FO's
 *
 * Revision 1.42.2.45.4.19.2.21  2013/07/19 12:32:03  rmukkera
 * Issue Fix for
 * Channel Allocation: system is displaying wrong data at WMS location and no data in Actual SO location in FO
 *
 * Revision 1.42.2.45.4.19.2.20  2013/07/15 15:32:15  gkalla
 * Case# 20123370
 * Ryonet Issue
 *
 * Revision 1.42.2.45.4.19.2.19  2013/07/15 11:31:20  snimmakayala
 * GFT UAT ISSUE FIXES
 * Case# : 20123423
 *
 * Revision 1.42.2.45.4.19.2.18  2013/07/10 15:20:35  nneelam
 * Case# 20123302
 * Standard Bundle  Issue Fix.
 *
 * Revision 1.42.2.45.4.19.2.17  2013/06/11 16:01:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Stdbundle issue fixes
 *
 * Revision 1.42.2.45.4.19.2.16  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.42.2.45.4.19.2.15  2013/06/04 15:27:44  rmukkera
 * Issue Fix related to channel allocation
 *
 * Revision 1.42.2.45.4.19.2.14  2013/06/03 15:38:02  rmukkera
 * Channel Allocation linelevel and makeware house site changes
 *
 * Revision 1.42.2.45.4.19.2.13  2013/06/03 07:26:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.42.2.45.4.19.2.12  2013/05/16 13:51:19  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.42.2.45.4.19.2.11  2013/05/01 15:29:08  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.42.2.45.4.19.2.10  2013/05/01 12:44:43  rmukkera
 * Chanel Allocation Related Changes were
 * merged
 *
 * Revision 1.42.2.45.4.19.2.9  2013/04/24 22:16:31  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Standed bundle
 *
 * Revision 1.42.2.45.4.19.2.8  2013/04/05 15:24:50  schepuri
 * standard bundle issue fix
 *
 * Revision 1.42.2.45.4.19.2.7  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.42.2.45.4.19.2.6  2013/04/01 15:40:39  schepuri
 * uom conversion issues
 *
 * Revision 1.42.2.45.4.19.2.5  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.42.2.45.4.19.2.4  2013/03/06 09:48:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet Files merging for Standard bundle
 *
 * Revision 1.42.2.45.4.19.2.3  2013/03/02 12:36:21  gkalla
 * CASE201112/CR201113/LOG201121
 * Merged from Factory mation as part of standard bundle
 *
 * Revision 1.42.2.45.4.19.2.2  2013/02/26 14:47:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Boombah.
 *
 * Revision 1.42.2.45.4.19.2.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.42.2.45.4.19  2013/02/20 15:40:09  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.42.2.45.4.18  2013/02/07 12:55:43  snimmakayala
 * no message
 *
 * Revision 1.42.2.45.4.17  2013/02/05 12:46:38  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.42.2.45.4.16  2013/02/04 09:01:45  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.42.2.45.4.15  2013/02/01 15:22:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.42.2.45.4.14  2013/01/21 07:36:38  spendyala
 * CASE201112/CR201113/LOG201121
 * FO merge issue through schedular is fixed.
 *
 * Revision 1.42.2.45.4.13  2013/01/17 06:03:02  snimmakayala
 * CASE201112/CR201113/LOG2012392
 *
 * .
 *
 * Revision 1.42.2.45.4.12  2013/01/09 15:21:51  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * SO delete restriction.
 * .
 *
 * Revision 1.42.2.45.4.11  2012/12/31 05:51:21  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Bulk Insert in User Event.
 *
 * Revision 1.42.2.45.4.10  2012/12/21 09:05:15  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.42.2.45.4.9  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.42.2.45.4.8  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.42.2.45.4.7  2012/11/12 15:42:48  gkalla
 * CASE201112/CR201113/LOG201121
 * Locking concept for duplicate fulfillments creation
 *
 * Revision 1.42.2.45.4.6  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.42.2.45.4.5  2012/10/30 06:10:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.42.2.45.4.4  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.42.2.45.4.3  2012/10/23 17:08:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.42.2.45.4.2  2012/10/07 23:33:55  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Wave Selection by Order changes.
 *
 * Revision 1.42.2.45  2012/09/11 00:45:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.42.2.44  2012/09/04 20:47:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Exception Handling.
 *
 * Revision 1.42.2.43  2012/09/04 07:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Depending upon the make WHS true showing fulfillment order link.
 *
 * Revision 1.42.2.42  2012/08/29 18:16:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Fulfillment order changes in shedule
 *
 * Revision 1.42.2.41  2012/08/27 13:57:53  mbpragada
 * 20120490
 * Error on clicking Inventory Report link in Sales Order
 *
 * Revision 1.42.2.40  2012/08/27 04:05:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah Issue Fixes
 *
 * Revision 1.42.2.39  2012/08/24 18:45:59  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changesbackup
 *
 * Revision 1.42.2.37  2012/08/22 14:39:44  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Blank Pack Code issue in FO schedule script for Dynacraft.
 *
 * Revision 1.42.2.36  2012/08/22 13:49:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Blank Pack Code issue in FO schedule script for Dynacraft.
 *
 * Revision 1.42.2.35  2012/08/16 09:24:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah UAT Issue Fixes.
 * Cartonization and Customization
 *
 * Revision 1.42.2.34  2012/08/10 15:00:11  gkalla
 * CASE201112/CR201113/LOG201121
 * to merge fulfillments before edit
 *
 * Revision 1.42.2.33  2012/08/07 09:53:18  gkalla
 * CASE201112/CR201113/LOG201121
 * To create FO with last created task with Edit status in scheduler
 *
 * Revision 1.42.2.32  2012/07/31 07:04:05  spendyala
 * CASE201112/CR201113/LOG201121
 * If NSship date is null consider the ebiznetShipdate.
 *
 * Revision 1.42.2.31  2012/07/30 23:21:21  gkalla
 * CASE201112/CR201113/LOG201121
 * NSUOM issue
 *
 * Revision 1.42.2.30  2012/07/23 06:31:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Parent Child
 *
 * Revision 1.42.2.29  2012/07/12 13:31:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * fulfillment order link
 *
 * Revision 1.42.2.28  2012/07/09 06:50:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.42.2.27  2012/07/02 06:31:50  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Fulfillment order creation is resolved.
 *
 * Revision 1.42.2.26  2012/06/02 09:36:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related toFulfillment for TO.
 *
 * Revision 1.42.2.25  2012/05/15 22:52:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Fulfill button diable in TO
 *
 * Revision 1.42.2.24  2012/05/04 22:52:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.42.2.23  2012/05/03 21:17:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.42.2.22  2012/04/20 14:06:46  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.42.2.21  2012/04/20 05:51:40  spendyala
 * CASE201112/CR201113/LOG201121
 * missing parenthesis.
 *
 * Revision 1.42.2.20  2012/04/19 15:05:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Updating the fulfillment order line rec with the order qty.
 *
 * Revision 1.42.2.19  2012/04/13 11:00:05  gkalla
 * CASE201112/CR201113/LOG201121
 * To pass Item status to fulfillment order from sales order/Transfer order
 *
 * Revision 1.42.2.18  2012/04/06 10:13:49  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.42.2.17  2012/03/31 18:15:27  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Code to disable  'fulfill" button
 *
 * Revision 1.42.2.16  2012/03/29 07:01:58  vrgurujala
 * t_NSWMS_LOG201121_89
 *
 * Revision 1.42.2.15  2012/03/26 17:16:27  gkalla
 * CASE201112/CR201113/LOG201121
 * To hide Close Order button
 *
 * Revision 1.42.2.14  2012/03/21 14:58:05  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to soDelete is fixed.
 *
 * Revision 1.42.2.13  2012/03/21 13:42:14  gkalla
 * CASE201112/CR201113/LOG201121
 * To hide Close Order button
 *
 * Revision 1.42.2.12  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.51  2012/03/20 20:15:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Redirect to new wave creation screen
 *
 * Revision 1.50  2012/03/16 07:20:28  spendyala
 * CASE201112/CR201113/LOG201121
 * code merged upto 1.42.2.10 from branch.
 *
 * Revision 1.49  2012/03/14 07:06:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.48  2012/02/20 13:36:46  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Line level WMS Carrier
 *
 * Revision 1.47  2012/02/17 20:21:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.46  2012/02/16 09:54:15  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge of sodelete function
 *
 * Revision 1.45  2012/02/16 01:29:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.44  2012/02/01 15:12:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * ship complete validation
 *
 * Revision 1.43  2012/01/25 07:22:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Item Status by Location
 *
 * Revision 1.42  2012/01/05 14:21:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Consider shipment number at header if no shipment number at line level.
 * similarly for location.
 *
 * Revision 1.41  2011/12/22 18:42:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Shipping
 *
 * Revision 1.40  2011/12/12 13:04:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * spelling corrections in success message
 *
 * Revision 1.39  2011/12/09 22:43:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move changes
 *
 * Revision 1.38  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.37  2011/11/29 10:36:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fixed Type casting issue.
 *
 * Revision 1.36  2011/11/25 10:16:44  rmukkera
 * CASE201112/CR201113/LOG201121
 * TransferOrder Related code changes were done
 *
 * Revision 1.35  2011/11/23 08:43:09  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Schedule script changes to fetch all the lines irrespective of committed qty.
 *
 * Revision 1.34  2011/11/15 19:26:53  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Consider already fufilled qty when calculating variance.
 *
 * Revision 1.33  2011/11/15 15:28:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Consider already fufilled qty when calculating variance.
 *
 * Revision 1.32  2011/11/14 18:59:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * skip the lines which are created with a non warehouse site location.
 *
 * Revision 1.31  2011/11/13 00:16:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Inventory Deletion
 *
 * Revision 1.30  2011/11/11 17:20:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Complete functionality to handle already processed orders.
 *
 * Revision 1.29  2011/11/09 14:48:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Complete Functionality and consider line level location if there is no location at header
 *
 * Revision 1.28  2011/11/09 12:18:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Complete Functionality
 *
 * Revision 1.27  2011/11/03 17:04:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Backorderfulfillment fine tunning.
 *
 * Revision 1.26  2011/11/02 10:25:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Backorderfulfillment changes to retrive the records order by internalid.
 *
 * Revision 1.25  2011/10/27 22:39:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * BackOrderFulfillment fine tuning
 *
 * Revision 1.24  2011/10/24 21:25:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Back Order Fulfillment function
 *
 * Revision 1.23  2011/10/20 08:16:20  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Implemented Default SKU Status and Default Packcode
 *
 * Revision 1.22  2011/10/18 18:17:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Duplicate Fulfillment Order Creation issue fixes
 *
 * Revision 1.21  2011/10/17 10:47:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Container Generation by Order
 *
 * Revision 1.20  2011/10/03 13:33:36  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Carrier Combo added
 *
 * Revision 1.19  2011/09/26 11:32:03  rmukkera
 * CASE201112/CR201113/LOG201121
 * hold flag was changed to create fullfillment order flag at both header and line level
 *
 * Revision 1.18  2011/09/23 14:20:14  rmukkera
 * CASE201112/CR201113/LOG201121
 * Hold Flags conditions were added
 *
 * Revision 1.17  2011/09/20 13:27:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.16  2011/09/19 14:03:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.15  2011/09/19 09:30:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.14  2011/09/19 09:22:20  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.13  2011/09/15 18:57:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.12  2011/09/15 09:55:44  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.11  2011/09/14 18:11:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.10  2011/09/14 15:58:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.9  2011/09/13 07:12:59  spendyala
 * CASE201112/CR201113/LOG201121
 * auto fulfillment fuctionality is added
 *
 * Revision 1.8  2011/07/20 09:01:43  pattili
 * CASE201112/CR201113/LOG201121
 * 1. Changes to Wave Genartion Logic
 * 2. Changes to Pick Report and Wave Pick Report to fetch the items with both the generated and edit status.
 *
 * Revision 1.7  2011/06/17 14:32:41  skota
 * CASE201112/CR201113/LOG201121
 * Fixed the issue of sequence# jumping while creating fulfillment order
 *
 * Revision 1.6  2011/06/10 17:55:04  skota
 * CASE201112/CR201113/LOG201121
 * Not to allow user to create fulfillment order for more than ordered quantity
 *
 * Revision 1.5  2011/06/06 15:18:03  skota
 * CASE201112/CR201113/LOG201121
 * Disable Fulfillment Order link once the order quantity is fulfilled for SO line
 * 
 *
 *****************************************************************************/

function soDeliveryOrderSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		//create the Suitelet form
		var form = nlapiCreateForm('Fulfillment Order');

		form.setScript('customscript_sovalidations');

		var solineno = request.getParameter('custparam_so_line_num');
		var ordtype = request.getParameter('custparam_ordtype');
		var ordqty = request.getParameter('custparam_ordqty');
		var avlqty = request.getParameter('custparam_comtdqty');
		var ItemStatus = request.getParameter('custparamitemstatus');

		nlapiLogExecution('DEBUG', 'solineno', solineno);
		nlapiLogExecution('DEBUG', 'avlqty', avlqty);
		nlapiLogExecution('DEBUG', 'ordqty', ordqty);
		var ordlinesearchresults = getFullFillment(request.getParameter('custparam_soid'));
		if(ordlinesearchresults!=null)
		{

			DeleteFOlinesWithEditStatus(request.getParameter('custparam_soid'));
			ordlinesearchresults = getFullFillment(request.getParameter('custparam_soid'));
			nlapiLogExecution('DEBUG', 'ordlinesearchresults', ordlinesearchresults);
		}
		var fulfillmentqty = 0;

		var lineno= request.getParameter('custparam_so_line_num');
		var vordtype= request.getParameter('custparam_ordtype');
		var vsonum,vsodate,vordpriority,vordtype,vcustomer,vcarrier,vline,vitem,vitemname,vordqty,vavlqty,vbatchno,vWMSCarrier;
		var vlineShipmentNo,vRouteno,vsoline_createfullfilmentorder,vlineLocation,vlineLocationname,vshipcomplete,vWMSLineCarrier;
		var vNSUOM="";
		var vBaseUomQty=1;
		var iteminfo1,iteminfo2,iteminfo3,vlocation,vCompany,vheadShipmentno,vlocationname;
		var vShipDate,vOrderDate;

		var searchresults ;
		var trantype = nlapiLookupField('transaction', request.getParameter('custparam_soid'), 'recordType');
		nlapiLogExecution('DEBUG', 'trantype', trantype);
		if (trantype == 'transferorder') 
		{
			searchresults = nlapiLoadRecord('transferorder', request.getParameter('custparam_soid')); //1020
			vsonum = searchresults.getFieldValue('tranid');
			vsodate = searchresults.getFieldValue('trandate');
			vcustomer ="";
			vcarrier = searchresults.getFieldValue('shipmethod');
			vordpriority="";
			//vordtype ="5";
			vordtype = searchresults.getFieldValue('custbody_nswmssoordertype');
			vlocation = searchresults.getFieldValue('location');
			vlocationname = searchresults.getFieldText('location');
			vWMSCarrier = searchresults.getFieldValue('custbody_salesorder_carrier');			
			vCompany ="";
			vheadShipmentno="";
			vRouteno="";
			vshipcomplete="";
			vShipDate="";
			//vsoline_createfullfilmentorder=searchresults.getFieldValue('custcol_create_fulfillment_order');
		}
		else if(trantype == 'salesorder')
		{
			searchresults = nlapiLoadRecord('salesorder', request.getParameter('custparam_soid')); //1020
			vsonum = searchresults.getFieldValue('tranid');nlapiLogExecution('DEBUG','vsonumId',vsonum);
			vsodate = searchresults.getFieldValue('trandate');
			vcustomer = searchresults.getFieldValue('entity');
			vcarrier = searchresults.getFieldValue('shipmethod');
			vordpriority = searchresults.getFieldValue('custbody_nswmspriority');
			vordtype = searchresults.getFieldValue('custbody_nswmssoordertype');
			vlocation = searchresults.getFieldValue('location');
			vlocationname = searchresults.getFieldText('location');
			vCompany = searchresults.getFieldValue('custbody_nswms_company');
			vheadShipmentno= searchresults.getFieldValue('custbody_shipment_no');
			vRouteno= searchresults.getFieldValue('custbody_nswmssoroute');
			vshipcomplete = searchresults.getFieldValue('shipcomplete');
			vWMSCarrier = searchresults.getFieldValue('custbody_salesorder_carrier');
			vShipDate = searchresults.getFieldValue('shipdate');

			nlapiLogExecution('DEBUG','vShipDate',vShipDate);
			if(vShipDate==null||vShipDate==""||vShipDate=='undefind')
				vShipDate = searchresults.getFieldValue('custbody_nswmspoexpshipdate');
			//	vsoline_createfullfilmentorder=searchresults.getFieldValue('custcol_create_fulfillment_order');
			nlapiLogExecution('DEBUG', 'vheadShipmentno', vheadShipmentno);

		}
		else
		{

		}

		var cnt=searchresults.getLineItemCount('item');
		var j=1;
		var vbqty;      
		nlapiLogExecution('DEBUG', 'SO count', cnt);
		var griditemcount = 0;
		var invnotavailableflag = false;
		var vWMSCarrierFinal = '';
		for (var i = 1; i <= parseFloat(cnt); i++) 
		{		
			nlapiLogExecution('DEBUG', 'Loop count', i);
			vline = searchresults.getLineItemValue('item','line',i);
			vitem=  searchresults.getLineItemValue('item','item',i);	
			if (trantype == 'transferorder') 
			{
				vordqty=searchresults.getLineItemValue('item','quantitycommitted',i);
			}
			else
			{
				vordqty=searchresults.getLineItemValue('item','quantity',i);
			}
			vavlqty=searchresults.getLineItemValue('item','quantitycommitted',i);
			vbatchno= searchresults.getLineItemValue('item','serialnumbers',i);
			//vNSUOM = searchresults.getLineItemValue('item','units',i);
			vNSUOM = searchresults.getLineItemValue('item','units',i);
			vlineShipmentNo= searchresults.getLineItemValue('item','custcol_nswms_shipment_no',i);
			vlinecreatefullfillmentorder= searchresults.getLineItemValue('item','custcol_create_fulfillment_order',i);
			vlineLocation=searchresults.getLineItemValue('item','location',i);
			vlineLocationname=searchresults.getLineItemText('item','location',i);
			vWMSLineCarrier=searchresults.getLineItemValue('item','custcol_salesorder_carrier_linelevel',i);

			if(vWMSLineCarrier != null && vWMSLineCarrier != '')
			{
				vWMSCarrierFinal = vWMSLineCarrier;
			}	
			else if (vWMSCarrier != null && vWMSCarrier != '')
				vWMSCarrierFinal = vWMSCarrier; 
//			else
//			vWMSCarrierFinal = vcarrier; 

			if(vlineLocation!=null && vlineLocation!='')
			{			
				vlocation = vlineLocation;
			}
			if(vlineShipmentNo == null || vlineShipmentNo == "")
			{
				vlineShipmentNo=searchresults.getFieldValue('custbody_shipment_no');
			}
			var mwhsiteflag='F';

			var	islocationAliasExists=CheckLocationAlias(vlocation);
			if(islocationAliasExists!=null && islocationAliasExists!='')
			{
				mwhsiteflag='T';
			}
			else
			{

				if(vlocation!=null && vlocation!='')
				{
					var fields = ['custrecord_ebizwhsite'];

					var locationcolumns = nlapiLookupField('Location', vlocation, fields);
					if(locationcolumns!=null)
						mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
				}
			}
			if(trantype == 'transferorder')
			{
				mwhsiteflag='T';
			}
			if(vitem!=null && vitem!='')
				itemtype = nlapiLookupField('item', vitem, 'recordType');	

			nlapiLogExecution('DEBUG', 'Item', vitem);

			if(itemtype!='descriptionitem' && itemtype!='discountitem' && itemtype!='Payment' && itemtype!='Markup' 
				&& itemtype!='Subtotal'  && itemtype!='otherchargeitem' && itemtype!='serviceitem' && itemtype!='noninventoryitem'
					&& itemtype!='NonInvtPart')
			{

				if(mwhsiteflag=='T' && vitem>0 && vordqty!=null && vordqty!='')
				{

					if(vlinecreatefullfillmentorder=='T' || trantype == 'transferorder'){
						if(vlineShipmentNo==null || vlineShipmentNo=="")
						{
							vlineShipmentNo=vheadShipmentno;
						}			
						nlapiLogExecution('DEBUG', 'vlineShipmentNo', vlineShipmentNo);
						nlapiLogExecution('DEBUG', 'vavlqty=', vavlqty)	;    				
						var itemSubtype = nlapiLookupField('item', vitem, 'recordType');
						nlapiLogExecution('DEBUG', 'citem subtype is =', itemSubtype);

						nlapiLogExecution('DEBUG', 'Item', vitem);
						nlapiLogExecution('DEBUG', 'NS UOM', vNSUOM);
						nlapiLogExecution('DEBUG', 'NS ORD Qty', vordqty);

						var fields = ['custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
						var columns = nlapiLookupField(itemSubtype, vitem, fields, true);
						var skuinfo1 = columns.custitem_item_info_1;
						var skuinfo2 = columns.custitem_item_info_2;
						var skuinfo3 = columns.custitem_item_info_3;
						var skuinfo = skuinfo1 + "," + skuinfo2 + "," + skuinfo3;
						var columnsval = nlapiLookupField(itemSubtype, vitem, fields);
						var skuinfo1val = columnsval.custitem_item_info_1;
						var skuinfo2val = columnsval.custitem_item_info_2;
						var skuinfo3val = columnsval.custitem_item_info_3;
						var skuinfoval = skuinfo1val + "," + skuinfo2val + "," + skuinfo3val;

						fulfillmentqty = 0;
						var veBizUOMQty=1;
						var vBaseUOMQty=1;


						//The below code is for UOM conversion. NS UOM vs eBizNET UOM

						if(vNSUOM!=null && vNSUOM!="")
						{
//							if(vNSUOM=="EA")
//							vNSUOM="Each";
//							else if(vNSUOM=="CA") 
//							vNSUOM="Case";
//							else if(vNSUOM=="PLT") 
//							vNSUOM="Pallet";

							var eBizItemDims=geteBizItemDimensions(vitem);
							nlapiLogExecution('DEBUG', 'eBizItemDims', eBizItemDims);
							if(eBizItemDims!=null&&eBizItemDims.length>0)
							{
								nlapiLogExecution('DEBUG', 'eBizItemDims Length', eBizItemDims.length);
								for(z=0; z < eBizItemDims.length; z++)
								{
									nlapiLogExecution('DEBUG', 'Base UOM Flag', eBizItemDims[z].getValue('custrecord_ebizbaseuom'));
									if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vBaseUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
										nlapiLogExecution('DEBUG', 'vBaseUOMQty', vBaseUOMQty);
									}

									nlapiLogExecution('DEBUG', 'Order Units', searchresults.getLineItemValue('item','units',i));
									nlapiLogExecution('DEBUG', 'SKU Dim Units', eBizItemDims[z].getValue('custrecord_ebiznsuom'));

									if(vNSUOM == eBizItemDims[z].getValue('custrecord_ebiznsuom'))
									{
										nlapiLogExecution('DEBUG', 'NS UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
										veBizUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');	
										nlapiLogExecution('DEBUG', 'veBizUOMQty', veBizUOMQty);
									}
								}
								if(veBizUOMQty==null || veBizUOMQty=='')
								{
									veBizUOMQty=vBaseUOMQty;
								}
								if(veBizUOMQty!=0){
									ordqty = (parseFloat(ordqty)*parseFloat(veBizUOMQty))/parseFloat(vBaseUOMQty);
									vordqty = (parseFloat(vordqty)*parseFloat(veBizUOMQty))/parseFloat(vBaseUOMQty);
									nlapiLogExecution('DEBUG', 'vordqty', vordqty);
								}
							}
						}

						// Upto here

						nlapiLogExecution('DEBUG', 'fulfillmentqty1', fulfillmentqty);

						if (ordlinesearchresults != null )
						{				
							for(l=0; l < ordlinesearchresults.length; l++)
							{
								// if SO Line# and fulfillment ordline matches then calculate (sum up) the quantity for which fulfillment order is already created
								if (vline == ordlinesearchresults[l].getValue('custrecord_ordline')) 
								{
									fulfillmentqty = parseFloat(fulfillmentqty)+ parseFloat(ordlinesearchresults[l].getValue('custrecord_ord_qty'));	
									nlapiLogExecution('DEBUG', 'fulfillmentqty2', fulfillmentqty);
								}
							}
						}

						nlapiLogExecution('DEBUG', 'fulfillmentqty3', fulfillmentqty);

						if(parseFloat(vavlqty)<1) //
						{
							invnotavailableflag = true;
							continue; 
						}
						else if(parseFloat(fulfillmentqty) >= parseFloat(vordqty))
						{
							continue;
						}
						else
						{
							griditemcount++;

							// Add fields to form if atleast one line order quantity is greater than fulfillment qty
							if(griditemcount == 1)
							{
								var soField = form.addField('custpage_so', 'text', 'Sales Order');
								soField.setLayoutType('normal', 'startcol');

								var soNumField = form.addField('custpage_sonum', 'text', 'SO ID');
								soNumField.setDefaultValue(request.getParameter('custparam_soid'));
								soNumField.setLayoutType('normal', 'startrow');
								soNumField.setDisplayType('hidden');

								var doField = form.addField('custpage_do', 'text', 'Fulfillment Order');        
								doField.setLayoutType('normal', 'startrow');

								var trlCustomer = form.addField('custpage_customer', 'select', 'Customer', 'customer');
								trlCustomer.setLayoutType('normal', 'startrow');

								var trlCarrier = form.addField('custpage_carrier', 'select', 'Carrier', 'shipmethod');	
								trlCarrier.setLayoutType('normal', 'startrow');	

								var soDate = form.addField('custpage_sodate', 'text', 'Order Date');
								soDate.setLayoutType('normal', 'startcol');

								var trlPriority = form.addField('custpage_priority', 'select', 'Priority', 'customlist_ebiznet_order_priority');
								trlPriority.setLayoutType('normal', 'startrow');	

								var trlOrderType = form.addField('custpage_ordertype', 'select', 'Order Type', 'customrecord_ebiznet_order_type');
								trlOrderType.setLayoutType('normal', 'startrow');

								var trlcompany = form.addField('custpage_company', 'select', 'Company', 'customrecord_ebiznet_company');
								trlcompany.setLayoutType('normal', 'startrow');	

								//Adding  ItemSubList
								var ItemSubList = form.addSubList("custpage_deliveryord_items", "inlineeditor", "ItemList");
								ItemSubList.addField("custpage_deliveryord_sel", "checkbox","Select").setDefaultValue('T');
								ItemSubList.addField("custpage_deliveryord_line", "text", "Line #");//.setDefaultValue(request.getParameter('custparam_so_line_num'));
								ItemSubList.addField("custpage_deliveryord_sku", "select", "Item","item").setDisplayType('disabled'); 
								ItemSubList.addField("custpage_deliveryord_iteminfo", "text", "Item Info");
								ItemSubList.addField("custpage_deliveryord_iteminfoval", "text", "Item Info").setDisplayType('hidden');                 
								//ItemSubList.addField("custpage_deliveryord_skustatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDefaultValue('1');
//								if(ItemStatus != null && ItemStatus != '')
//								ItemSubList.addField("custpage_deliveryord_skustatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDefaultValue(ItemStatus); 
//								else
//								ItemSubList.addField("custpage_deliveryord_skustatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDefaultValue('1');


								ItemSubList.addField("custpage_deliveryord_skustatus", "text", "Item Status").setDisplayType('disabled');
								ItemSubList.addField("custpage_deliveryord_skustatustext", "text", "Item Status");
								ItemSubList.addField("custpage_deliveryord_lotbatch", "text", "LOT#");
								ItemSubList.addField("custpage_deliveryord_pc", "text", "Pack Code").setDefaultValue('1');
								ItemSubList.addField("custpage_deliveryord_buom", "text", "Base UOM").setDefaultValue('EACH');       
								ItemSubList.addField("custpage_deliveryord_expqty", "text", "Base Ord Qty").setDisplayType('disabled');
								ItemSubList.addField("custpage_deliveryord_rcvngqty", "text", "Fulfillment Qty");	                
								ItemSubList.addField("custpage_deliveryord_fulfillmentcreatedqty", "text", "Fulfillment Created Qty").setDisplayType('hidden');
								ItemSubList.addField("custpage_deliveryord_location", "text", "Location").setDisplayType('hidden');
								ItemSubList.addField("custpage_deliveryord_shipmentno", "text", "Shipment No").setDisplayType('hidden');
								ItemSubList.addField("custpage_deliveryord_route", "text", "Route No").setDisplayType('hidden');	
								ItemSubList.addField("custpage_deliveryord_shipcomplete", "text", "Ship Complete").setDisplayType('hidden');	 
								ItemSubList.addField("custpage_deliveryord_locationname", "text", "Location Name").setDisplayType('hidden');
								ItemSubList.addField("custpage_deliveryord_wmscarrier", "text", "WMS Carrier").setDisplayType('hidden');
								ItemSubList.addField("custpage_deliveryord_shipdate", "text", "Ship Date").setDisplayType('hidden');	

								soField.setDefaultValue(vsonum);
								soDate.setDefaultValue(vsodate);
								trlPriority.setDefaultValue(vordpriority);
								trlOrderType.setDefaultValue(vordtype);
								doField.setDefaultValue(vsonum);

								trlCustomer.setDefaultValue(vcustomer);
								trlCarrier.addSelectOption(vcarrier,searchresults.getFieldText('shipmethod'));
								trlCarrier.setDefaultValue(vcarrier);
								trlcompany.setDefaultValue(vCompany);

								// code to find out number of fulfillment orders created for a given SO
								if (ordlinesearchresults == null)
									doField.setDefaultValue(vsonum+".1");
								else
								{	    		    	
									var fulfillordno = "";
									var fulfillordcnt =0;
									for(l=0; l < ordlinesearchresults.length; l++)
									{  
										if(fulfillordno != ordlinesearchresults[l].getValue('custrecord_lineord'))
										{
											fulfillordno = ordlinesearchresults[l].getValue('custrecord_lineord');
											fulfillordcnt++;
										} 		    		
									}
									fulfillordcnt = fulfillordcnt + 1;
									doField.setDefaultValue(vsonum+"."+fulfillordcnt);
								}

								form.addSubmitButton('Save'); 
							}

							if(vlineLocation!=null && vlineLocation!='')
							{
								vlocation = vlineLocation;
								vlocationname = vlineLocationname;
							}

							var ItemType = nlapiLookupField('item', vitem, 'recordType');            
							if (ItemType == "lotnumberedinventoryitem") 
							{
								var arrbatch = new Array();
								if (vbatchno != null && vbatchno != "") 
								{
									arrbatch = vbatchno.split(String.fromCharCode(5));
								}
								var vallocqty=0;
								for (k = 0; k < arrbatch.length; k++) 
								{
									nlapiLogExecution('DEBUG', 'arrbatch[' + k + ']', arrbatch[k]);
									var batchqty=0;
									var vnewbatchno="";
									if(arrbatch[k]!=null && arrbatch[k]!="")
									{
										var arrrec=arrbatch[k].split('(');						
										if(arrrec.length>1)
										{
											nlapiLogExecution('DEBUG', 'arrrec[0] ', arrrec[0] );
											batchqty =arrrec[1].split(')')[0];
											nlapiLogExecution('DEBUG', 'batchqty ', batchqty );
											vnewbatchno=arrrec[0];
											nlapiLogExecution('DEBUG', 'vnewbatchno ', vnewbatchno );
										}
										else
										{										
											vnewbatchno=arrrec[0];
											batchqty=vavlqty;
											nlapiLogExecution('DEBUG', 'else vnewbatchno ', vnewbatchno );	
										}
									}
									else
									{
										batchqty=vavlqty;	
									}




									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_line', j, vline);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku', j, vitem);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_iteminfo', j, skuinfo);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_iteminfoval', j, skuinfoval);				
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_expqty', j, vordqty);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_rcvngqty', j, batchqty);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_fulfillmentcreatedqty', j, fulfillmentqty);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotbatch', j, vnewbatchno);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_location', j, vlocation);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_shipmentno', j, vlineShipmentNo);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_route', j, vRouteno);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_shipcomplete', j, vshipcomplete);

									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_locationname', j, vlocationname);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_wmscarrier', j, vWMSCarrierFinal);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_shipdate', j, vShipDate);

									vallocqty= parseFloat(vallocqty) + parseFloat(batchqty);

									j = j + 1;					
								}

								nlapiLogExecution('DEBUG', 'vavlqty', vavlqty);
								nlapiLogExecution('DEBUG', 'vallocqty', vallocqty);

								if( parseFloat(vavlqty)-parseFloat(vallocqty) >0)
								{


									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_line', j, vline);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku', j, vitem);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_iteminfo', j, skuinfoval);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_iteminfoval', j, skuinfoval);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_expqty', j, vordqty);
									if(vallocqty>0)
										form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_rcvngqty', j, parseFloat(vavlqty)-parseFloat(vallocqty));
									else
									{								
										//form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_rcvngqty', j, parseFloat(vavlqty)-parseFloat(fulfillmentqty));
										var remqty = parseFloat(vordqty) - parseFloat(fulfillmentqty);
										nlapiLogExecution('DEBUG', 'remqty for lot controlled item', remqty);
										form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_rcvngqty', j, remqty.toString());
									}
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_fulfillmentcreatedqty', j, fulfillmentqty);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotbatch', j, '');
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_location', j, vlocation);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_shipmentno', j, vlineShipmentNo);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_route', j, vRouteno);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_shipcomplete', j, vshipcomplete);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_locationname', j, vlocationname);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_wmscarrier', j, vWMSCarrierFinal);
									form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_shipdate', j, vShipDate);

									vallocqty= parseFloat(vallocqty) + parseFloat(batchqty);

									j = j + 1;
								}
							} //ItemType == "lotnumberedinventoryitem"
							else 
							{	
								nlapiLogExecution('DEBUG', 'vordqty', vordqty);
								nlapiLogExecution('DEBUG', 'fulfillmentqty', fulfillmentqty);

								nlapiLogExecution('DEBUG', 'j=', j);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_line', j, vline);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku', j, vitem);

								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_iteminfo', j, skuinfoval);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_iteminfoval', j, skuinfoval);				
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_expqty', j, vordqty);
								var remqty = parseFloat(vordqty) - parseFloat(fulfillmentqty);
								nlapiLogExecution('DEBUG', 'remqty', remqty);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_rcvngqty', j, remqty.toString());
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_fulfillmentcreatedqty', j, fulfillmentqty);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotbatch', j, vbatchno);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_location', j, vlocation);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_shipmentno', j, vlineShipmentNo);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_route', j, vRouteno);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_shipcomplete', j, vshipcomplete);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_locationname', j, vlocationname);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_wmscarrier', j, vWMSCarrierFinal);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_shipdate', j, vShipDate);
								j = j + 1;
							}
						}
					}// Out of Loop SO COUNT loop
				}
			}
		}  
		nlapiLogExecution('DEBUG', 'Out of Loop cnt=', cnt);

		if(griditemcount == 0 && invnotavailableflag == true)
		{	    	    		
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
			var msgtext = "SO needs Supervisor Approval ";
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+ msgtext + "', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		}
		else if(griditemcount == 0)
		{
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
			var msgtext ="";
			if(trantype == 'transferorder')
			{
				msgtext = "Fulfillment Orders already created for the Transfer Order:"+vsonum;
			}
			else
			{
				msgtext = "Fulfillment Orders already created for the Sales Order:"+vsonum;
			}
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+ msgtext + "', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		}

		response.writePage(form);

	} 
	else //this is the POST block
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		//extract the values from the previous page
		var so = request.getParameter('custpage_so');
		var delord = request.getParameter('custpage_do');
		var poValue = request.getParameter('custpage_po');
		var quan = request.getParameter('custpage_item_quan');
		var lineno = request.getParameter('custpage_line_itemlineno');
		var remQty = request.getParameter('custpage_remainitem_quan');
		var lineCnt = request.getLineItemCount('custpage_deliveryord_items');
		var customer = request.getParameter('custpage_customer');
		var carrier = request.getParameter('custpage_carrier');
		var priority = request.getParameter('custpage_priority');
		var ordtype = request.getParameter('custpage_ordertype');
		var vCompany = request.getParameter('custpage_company');		
		var soid=request.getParameter('custpage_sonum');
		var sodate = request.getParameter('custpage_sodate');

		nlapiLogExecution('DEBUG', 'Into line Count', lineCnt);
		var now = new Date();  

		var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); 

		for (var s = 1; s <= lineCnt; s++) {
			//Retreiving line item details
			var chkd = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sel', s);
			var Item = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', s);
			var ExpQty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_expqty', s);
			var RcvQty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_rcvngqty', s);
			var soline = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_line', s);
			var itemInfo= request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_iteminfo', s);
			var packcode = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_pc', s);
			var UOM = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_buom', s);
			var skustatus = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_skustatus', s);
			var vlotbatch = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_lotbatch', s);
			var vlocation = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', s);
			var itemInfoval= request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_iteminfoval', s);
			var vshipmentno= request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_shipmentno', s);
			var vRouteno= request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_route', s);
			var vshipcomplete = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_shipcomplete', s);
			var vlocationname = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_locationname', s);
			var vWMSCarrier = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_wmscarrier', s);
			var vShipDate = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_shipdate', s);

			nlapiLogExecution('DEBUG','locationname',vlocationname);
			nlapiLogExecution('DEBUG','vWMSCarrier',vWMSCarrier);
			nlapiLogExecution('DEBUG','location',vlocation);
			var actuallocation=null;
			if(vlocation!=null && vlocation!='' && vlocation!='null')
			{
				actuallocation=CheckLocationAlias(vlocation);
			}

			nlapiLogExecution('DEBUG','location',actuallocation);
			var statusbyloc='';

			//The following line of code is only for TNT
			//statusbyloc=getItemStatus(vlocationname);	

			nlapiLogExecution('DEBUG','statusbyloc',statusbyloc);

			if(chkd=='F')
			{ 
				continue;
			}

			var iteminfoarr= new Array();
			iteminfoarr=itemInfoval.split(',');

			var lineordsplit = delord.split('.');
			var backordline = lineordsplit[1];

			parent.selectNewLineItem('recmachcustrecord_ebiz_fo_parent');

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','name', soid);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ns_ord', soid);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_linesku', Item);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', delord);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline', soline);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_backorderno', backordline);


			if(statusbyloc!=null && statusbyloc!='')
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linesku_status', statusbyloc);
			else
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linesku_status', skustatus);

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag', 25);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_priority', priority);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ord_qty', parseFloat(RcvQty).toFixed(5));			
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_type', ordtype);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_customer', customer);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_carrier', carrier);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linepackcode', packcode);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineuom_id', 1);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_batch', vlotbatch);			
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_actuallocation', vlocation);
			if(actuallocation!=null)
			{
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_wms_location', actuallocation);

			}
			else
			{
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_wms_location', vlocation);

			}
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_company', vCompany);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipment_no', vshipmentno);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_route_no', vRouteno);

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipcomplete', vshipcomplete);
			//	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_wmscarrier', vWMSCarrier);
			if(vShipDate!='undefined')
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_shipdate', vShipDate);

			if(sodate!='undefined')
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_orderdate', sodate);

			if(sodate!='undefined')
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_record_linedate', DateStamp());

			if(iteminfoarr.length>0){
				for(var l=0;l<iteminfoarr.length;l++)
				{
					if(iteminfoarr[l]!="" ||iteminfoarr[l]!=null)
					{
						if(l==0)
						{
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo1', iteminfoarr[l]);
						}
						else if(l==1)
						{
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo2', iteminfoarr[l]);
						}
						else
						{
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo3', iteminfoarr[l]);
						}
					}
				}
			}

			parent.commitLineItem('recmachcustrecord_ebiz_fo_parent');	 

//			var rcptrecord = nlapiCreateRecord('customrecord_ebiznet_ordline');
//			nlapiLogExecution('DEBUG', 'Creating DO record', 'TRN_DO');

//			//populating the fields
//			rcptrecord.setFieldValue('name', soid);
//			rcptrecord.setFieldValue('custrecord_ordline', soline);
//			rcptrecord.setFieldValue('custrecord_ebiz_linesku', Item);
//			rcptrecord.setFieldValue('custrecord_ord_qty', parseFloat(RcvQty));
//			rcptrecord.setFieldValue('custrecord_lineord', delord);
//			rcptrecord.setFieldValue('custrecord_ns_ord', soid);
//			rcptrecord.setFieldValue('custrecord_do_order_priority', priority);
//			rcptrecord.setFieldValue('custrecord_do_order_type', ordtype);
//			rcptrecord.setFieldValue('custrecord_do_customer', customer);
//			rcptrecord.setFieldValue('custrecord_do_carrier', carrier);
//			rcptrecord.setFieldValue('custrecord_linestatus_flag', 25);
//			rcptrecord.setFieldValue('custrecord_linepackcode', packcode);
//			rcptrecord.setFieldValue('custrecord_lineuom_id', 1);
//			if(statusbyloc!=null && statusbyloc!='')
//			rcptrecord.setFieldText('custrecord_linesku_status', statusbyloc);
//			else
//			rcptrecord.setFieldValue('custrecord_linesku_status', skustatus);
//			rcptrecord.setFieldValue('custrecord_batch', vlotbatch);
//			rcptrecord.setFieldValue('custrecord_ordline_wms_location', vlocation);	
//			rcptrecord.setFieldValue('custrecord_ordline_company',vCompany);
//			rcptrecord.setFieldValue('custrecord_shipment_no',vshipmentno);
//			rcptrecord.setFieldValue('custrecord_route_no',vRouteno);
//			rcptrecord.setFieldValue('custrecord_shipcomplete',vshipcomplete);
//			rcptrecord.setFieldValue('custrecord_do_wmscarrier', vWMSCarrier);
//			nlapiLogExecution('DEBUG', 'vShipDate', vShipDate);
//			if(vShipDate!='undefined')
//			rcptrecord.setFieldValue('custrecord_ebiz_shipdate',vShipDate);

//			//Adding Item info Details.
//			if(iteminfoarr.length>0){
//			for(var l=0;l<iteminfoarr.length;l++)
//			{
//			if(iteminfoarr[l]!="" ||iteminfoarr[l]!=null)
//			{
//			if(l==0)
//			{
//			rcptrecord.setFieldValue('custrecord_fulfilmentiteminfo1', iteminfoarr[l]);
//			}
//			else if(l==1)
//			{
//			rcptrecord.setFieldValue('custrecord_fulfilmentiteminfo2', iteminfoarr[l]);
//			}
//			else
//			{
//			rcptrecord.setFieldValue('custrecord_fulfilmentiteminfo3', iteminfoarr[l]);
//			}
//			}
//			}
//			}	

//			//commit the record to NetSuite
//			var rcptrecordid = nlapiSubmitRecord(rcptrecord);
//			nlapiLogExecution('DEBUG', 'Done DO Record Insertion ', 'TRN_DO');

			nlapiLogExecution('DEBUG', 'soid ', soid);

//			try
//			{
//			//var trantype = nlapiLookupField('transaction', soid, 'recordType');

//			//if(trantype=='salesorder')
//			//updatesalesorderline(soid,soline,RcvQty,'E');
//			}
//			catch(exp)
//			{
//			nlapiLogExecution('DEBUG', 'exp ', exp);
//			}
			//}
		}//end of For loop lines count.
		//}

		nlapiSubmitRecord(parent);

		var form = nlapiCreateForm('Delivery Order');
		form.addField('custpage_label','label','<H2>DO Added Successfully<H2>');

		response.writePage(form);

		var SOarray = new Array();
		nlapiLogExecution('DEBUG', 'IF'+ delord);
		SOarray ["custpage_qbso"] = delord;
		SOarray ["custpage_item"] = '';
		SOarray ["custpage_consignee"] = '';
		SOarray ["custpage_ordpriority"] = '';
		SOarray ["custpage_ordtype"] = ''; 

		//response.sendRedirect('SUITELET', 'customscript_waveconfirm', 'customdeploy_waveconfirm', false, SOarray );
		//change the path old wavegen to new wave gen screen chnged by rami reddy
		response.sendRedirect('SUITELET', 'customscript_wavecreation', 'customdeploy_wavecreationdi', false, SOarray );

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}    
}

/**
 * It triggers when ever the page is Loaded and the function executes when the page type is 'view' 
 * @param type
 * @param form
 * @param request
 */
function soAddFulfillmentOrderLinkUE(type, form, request)
{
	//	obtain the context object
	var ctx = nlapiGetContext();
	var soid = nlapiGetRecordId();

	//	only execute when the exec context is web browser and in view mode of the PO
	if (ctx.getExecutionContext() == 'userinterface' && type == 'view') 
	{
		//Add the "ebiznet Build Assembly" field
		form.getSubList('item').addField('custpage_build_assembly', 'text', 'Build Assembly', null) ;
		//	form.getSubList('item').addField('custpage_do_link', 'text', 'Fulfillment Order', null);
		form.getSubList('item').addField("custpage_do_link", "url", "Fulfillment Order",null ).setLinkText( "Fulfillment Order");
		//obtain the SO internal ID

		var soname = nlapiGetFieldValue('tranid');
		var Location= nlapiGetFieldValue('location');
		var vitemno= nlapiGetFieldValue('item');
		var shipcomplete = nlapiGetFieldValue('shipcomplete');
		var vmwhsiteflag='F';
		var str = 'soid. = ' + soid + '<br>';
		str = str + 'soname. = ' + soname + '<br>';	
		str = str + 'Location. = ' + Location + '<br>';
		str = str + 'vitemno. = ' + vitemno + '<br>';
		str = str + 'shipcomplete. = ' + shipcomplete + '<br>';

		nlapiLogExecution('DEBUG', 'Log 1', str);
		try
		{
			//To Hide the Close Order button in the UI

			if(Location!=null && Location!='')
			{
				var fields = ['custrecord_ebizwhsite'];

				var locationcolumns = nlapiLookupField('Location', Location, fields);
				if(locationcolumns!=null)
					vmwhsiteflag = locationcolumns.custrecord_ebizwhsite;
			}
			var filter = new Array();
			filter[0]= new nlobjSearchFilter('custrecord_ns_ord', null, 'is', soid); 
			var column = new Array();
			column[0] = new nlobjSearchColumn('custrecord_linestatus_flag');
			var searchRec = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);
			if(searchRec != null && searchRec != '')
			{				
				nlapiLogExecution('DEBUG', 'Hiding Close Button');

				//Get the close button
				var closebutton = form.getButton('closeremaining');  

				if(closebutton!=null)
					closebutton.setVisible(false);
			}

			var filter=new Array();
			filter[0]=new nlobjSearchFilter('internalid',null,'anyof',soid);
			filter[1]=new nlobjSearchFilter('mainline', null, 'is', 'T');
			var column=new Array();
			column[0]=new nlobjSearchColumn('customform');

			var trantype = nlapiLookupField('transaction', soid, 'recordType');
			nlapiLogExecution('DEBUG', 'trantype', trantype);

			if (trantype !=null && trantype == 'salesorder') 
			{
				var rec=nlapiSearchRecord('salesorder',null,filter,column);
				if(rec!=null&&rec!="")
				{
					var vcustomform=rec[0].getText('customform');
					nlapiLogExecution('DEBUG', 'vcustomform', vcustomform);
					if(vcustomform=="eBizNET WMS Sales Order")
					{
						var fulfillbtn= form.getButton('process');  
						if(fulfillbtn!=null)
							fulfillbtn.setDisabled(true);
					}
				}
			}
			else if (trantype !=null && trantype == 'transferorder') 
			{
				var rec=nlapiSearchRecord('transferorder',null,filter,column);
				if(rec!=null&&rec!="")
				{
					var vcustomform=rec[0].getText('customform');
					if(vmwhsiteflag=="T")
					{
						var fulfillbtn= form.getButton('process');  
						if(fulfillbtn!=null)
							fulfillbtn.setDisabled(true);
					}
				}
			}

		}
		catch (exp) 
		{
			nlapiLogExecution('DEBUG', 'Exception in hiding Close button : ', exp);		
		}

		//resolve the generic relative URL for the Suitelet
		var dolinkURL = nlapiResolveURL('SUITELET', 'customscript_ebiznet_deliveryorder', 'customdeploy_deliveryorder');
		var AssemblyLinkURL= nlapiResolveURL('SUITELET', 'customscript_kittoorder', 'customdeploy_kittoorder');
		var SoURL = nlapiResolveURL('SUITELET','customscript_inventoryreport', 'customdeploy1');
		//complete the URL as either for production or sandbox environment
		/*if (ctx.getEnvironment() == 'PRODUCTION') {
			dolinkURL = 'https://system.netsuite.com' + dolinkURL;
			AssemblyLinkURL= 'https://system.netsuite.com' + AssemblyLinkURL;
			SoURL= 'https://system.netsuite.com' + SoURL;
		}
		else 
			if (ctx.getEnvironment() == 'SANDBOX') {
				dolinkURL = 'https://system.sandbox.netsuite.com' + dolinkURL;
				AssemblyLinkURL = 'https://system.sandbox.netsuite.com' + AssemblyLinkURL;
				SoURL = 'https://system.sandbox.netsuite.com' + SoURL;
			}*/
		nlapiLogExecution('DEBUG','GetSublistcount',form.getSubList('item').getLineItemCount());
		nlapiSetFieldValue('custbody_inventoryreport', SoURL);
		var so_CreateFullfillment_flag = nlapiGetFieldValue('custbody_create_fulfillment_order');

		var createfo='T';

//		try
//		{
//		if(trantype == 'salesorder')
//		createfo = fnCreateFo(soid);
//		}
//		catch(exp)
//		{
//		nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
//		createfo='T';
//		}

		nlapiLogExecution('DEBUG','createfo',createfo);

		// The below code is for shipcomplete functionality - 11/09/2011
		if(createfo=='T' && shipcomplete=='T')
		{
			for (var j = 0; j < form.getSubList('item').getLineItemCount(); j++) 
			{
				var ordlineqty = nlapiGetLineItemValue('item', 'quantity', j + 1); 
				var comtdlineqty = nlapiGetLineItemValue('item', 'quantitycommitted', j + 1);
				var fulfildlineqty = nlapiGetLineItemValue('item', 'quantityfulfilled', j + 1);
				var linelocation = nlapiGetLineItemValue('item', 'location', j + 1);
				var vitem = nlapiGetLineItemValue('item', 'item', j + 1);
				nlapiLogExecution('DEBUG', 'Item', vitem);
				var itemtype='';

				if(vitem!=null && vitem!='')
					itemtype = nlapiLookupField('item', vitem, 'recordType');	

				nlapiLogExecution('DEBUG', 'itemtype', itemtype);

				if(itemtype!='descriptionitem' && itemtype!='discountitem' && itemtype!='Payment' && itemtype!='Markup' 
					&& itemtype!='Subtotal'  && itemtype!='otherchargeitem' && itemtype!='serviceitem' && itemtype!='noninventoryitem'
						&& itemtype!='NonInvtPart')
				{				

					if(linelocation!=null && linelocation!='')
						Location=linelocation;

					var mwhsiteflag='F';

					if(Location!=null && Location!='')
					{
						var fields = ['custrecord_ebizwhsite'];

						var locationcolumns = nlapiLookupField('Location', Location, fields);
						if(locationcolumns!=null)
							mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
					}					

					if(fulfildlineqty==null || fulfildlineqty=='' || isNaN(fulfildlineqty))
					{
						fulfildlineqty=0;
					}
					if(comtdlineqty==null || comtdlineqty=='' || isNaN(comtdlineqty))
					{
						comtdlineqty=0;
					}

					var totalcomtdqty=parseFloat(comtdlineqty)+parseFloat(fulfildlineqty);

					if((mwhsiteflag=='T') && (parseFloat(ordlineqty) > parseFloat(totalcomtdqty)))
					{
						createfo = 'F';
						continue;
					}
				}
			}
		}		
		//upto here

		//if(so_CreateFullfillment_flag=='T' && createfo=='T')
		if(createfo=='T')
		{
			//loop through the item sublist
			for (var i = 0; i < form.getSubList('item').getLineItemCount(); i++) 
			{
				//This  link is for Assembly Build.
				var totalQty=0;
				var ordqty = nlapiGetLineItemValue('item', 'quantity', i + 1); 
				var fulfildQty = nlapiGetLineItemValue('item', 'quantityfulfilled', i + 1);
				var comtdQty = nlapiGetLineItemValue('item', 'quantitycommitted', i + 1);
				var salesOrderLineNo = nlapiGetLineItemValue('item', 'line', i + 1);
				nlapiLogExecution('DEBUG', 'ordqty', ordqty);
				nlapiLogExecution('DEBUG', 'fulfildQty', fulfildQty);
				nlapiLogExecution('DEBUG', 'comtdQty', comtdQty);

				if(fulfildQty==null || fulfildQty=='' || isNaN(fulfildQty))
				{
					fulfildQty=0;
				}
				if(comtdQty==null || comtdQty=='' || isNaN(comtdQty))
				{
					comtdQty=0;
				}

				var soLine_CreateFullfillment_flag = nlapiGetLineItemValue('item','custcol_create_fulfillment_order',i+1);
				var soLine_ItemStatus = nlapiGetLineItemValue('item','custcol_ebiznet_item_status',i+1);
				nlapiLogExecution('DEBUG', 'soLine_CreateFullfillment_flag', soLine_CreateFullfillment_flag);
				if (parseFloat(fulfildQty) < parseFloat(ordqty)&&soLine_CreateFullfillment_flag=='T')
				{

					var linkURL;     

					//add the SO ID, item, and line to the URL
					linkURL = dolinkURL + '&custparam_soid=' + soid;
					linkURL = linkURL + '&custparam_sovalue=' + soname;
					linkURL = linkURL + '&custparam_so_line_item=' + nlapiGetLineItemValue('item', 'item', i + 1);
					linkURL = linkURL + '&custparam_so_line_num=' + nlapiGetLineItemValue('item', 'line', i + 1);
					linkURL = linkURL + '&custparam_ordqty=' + ordqty;
					linkURL = linkURL + '&custparam_comtdqty=' + comtdQty;
					linkURL = linkURL + '&custparamitemstatus=' + soLine_ItemStatus;


					totalQty = (parseFloat(ordqty)-parseFloat(comtdQty))-parseFloat(fulfildQty);
					nlapiLogExecution('DEBUG', 'totalQty', totalQty);

					var ABLink;
					ABLink = AssemblyLinkURL + '&custparam_soid=' + soid;
					ABLink = ABLink + '&custparam_sovalue=' + soname;
					ABLink = ABLink + '&custparam_soloc=' + Location;			
					ABLink = ABLink + '&custparam_lneitem=' + nlapiGetLineItemValue('item', 'item', i + 1);
					ABLink = ABLink + '&custparam_lneno=' + nlapiGetLineItemValue('item', 'line', i + 1);
					ABLink = ABLink + '&custparam_lneqty ='+totalQty;

					//log the URL
					nlapiLogExecution('DEBUG', 'DO url', linkURL);

					//populate the URL into the check in field as a hyperlink to the Suitelet
//					var columns = nlapiLookupField('item', nlapiGetLineItemValue('item', 'item', i + 1), 'recordType');
//					var ItemType= columns;

//					if (ItemType == "assemblyitem" || ItemType == "lotnumberedassemblyitem" || ItemType=="serializedassemblyitem" ||  ItemType == "kititem")
//					{
//					form.getSubList('item').setLineItemValue('custpage_build_assembly', i + 1, '<a href="' + ABLink + '">Assembly Build</a>');
//					}
//					//form.getSubList('item').setLineItemValue('custpage_do_link', i + 1, '<a href="' + linkURL + '">Fulfillment Order</a>');
//					nlapiLogExecution('DEBUG','CHKPT',trantype);
//					nlapiLogExecution('DEBUG','vmwhsiteflag',vmwhsiteflag);
//					if (trantype !=null && trantype == 'transferorder'&& vmwhsiteflag=='T')
//					{
//					//form.getSubList('item').setLineItemValue('custpage_do_link', i + 1,linkURL);
//					}
//					else if(trantype== 'salesorder')
//					//form.getSubList('item').setLineItemValue('custpage_do_link', i + 1,linkURL);
//					var linkURL;     

					totalQty = (parseFloat(ordqty)-parseFloat(comtdQty))-parseFloat(fulfildQty);
					nlapiLogExecution('DEBUG', 'totalQty', totalQty);

//					var columns = nlapiLookupField('item', nlapiGetLineItemValue('item', 'item', i + 1), 'recordType');
//					var ItemType= columns;

//					if (ItemType == "assemblyitem") {
//					form.getSubList('item').setLineItemValue('custpage_build_assembly', i + 1, '<a href="' + ABLink + '">Assembly Build</a>');
//					}
					//form.getSubList('item').setLineItemValue('custpage_do_link', i + 1, '<a href="' + linkURL + '">Fulfillment Order</a>');	
					//form.getSubList('item').setLineItemValue('custpage_do_link', i + 1,linkURL);
				}
			}
		}
	}

	else if(ctx.getExecutionContext() == 'userinterface' && type == 'create')
	{

		var soid = nlapiGetFieldValue('createdfrom');
		nlapiLogExecution('DEBUG', 'soid', soid);
		var trantype="";
		if(soid!=null&&soid!="")
			trantype = nlapiLookupField('transaction', soid, 'recordType');
		nlapiLogExecution('DEBUG', 'trantype', trantype);
		if(trantype=='estimate')
		{
			for (var j = 0; j < form.getSubList('item').getLineItemCount(); j++) 
			{
				var ordlineqty = nlapiGetLineItemValue('item', 'quantity', j + 1); 
				var comtdlineqty = nlapiGetLineItemValue('item', 'quantitycommitted', j + 1);
				var fulfildlineqty = nlapiGetLineItemValue('item', 'quantityfulfilled', j + 1);
				var linelocation = nlapiGetLineItemValue('item', 'location', j + 1);
				var vitem = nlapiGetLineItemValue('item', 'item', j + 1);
				nlapiLogExecution('DEBUG', 'Item', vitem);
				var itemtype='';

				if(linelocation!=null && linelocation!='')
					Location=linelocation;

				var mwhsiteflag='F';

				if(Location!=null && Location!='')
				{
					var fields = ['custrecord_ebizwhsite'];

					var locationcolumns = nlapiLookupField('Location', Location, fields);
					if(locationcolumns!=null)
						mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
				}	

				var searchresult = SetItemStatus("SalesOrder", vitem, Location, null);

				if (searchresult != null && searchresult != '') 
				{

					nlapiSetLineItemValue('item', 'custcol_ebiznet_item_status', j+1,searchresult[0]);
					if(searchresult[1] != null && searchresult[1] != '')
						nlapiSetLineItemValue('item', 'custcol_nswmspackcode',j+1,searchresult[1]);
				}


			}
		}
	}

	if(type =='edit')
	{
		var filter = new Array();
		filter[0]= new nlobjSearchFilter('custrecord_ns_ord', null, 'is', soid); 
		filter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'noneof', [25]));
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_linestatus_flag');
		var searchRec = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);
		if(searchRec != null && searchRec != '')
		{				
			nlapiLogExecution('DEBUG', 'Hiding Void Button');

			//Get the close button
			var voidbutton = form.getButton('void');
			if(voidbutton!=null)
				voidbutton.setDisabled(true);


		}
	}

}	

/*It triggers before page is submitted (restrict deleteting the so after wave generation)*/
function soDelete(type, form, request)
{	
	nlapiLogExecution('DEBUG','Into soDelete function',type);

	if(type ==  'delete')
	{
		var status=nlapiGetFieldValue('status');		
		var soId=nlapiGetFieldValue('id');
		nlapiLogExecution('DEBUG','soId ',soId);

		var filter = new Array();
		filter[0]= new nlobjSearchFilter('name', null, 'is', soId);
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_linestatus_flag'); 
		var searchRec = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);

		if(searchRec != null && searchRec != "")
		{
			var SODelStatusFlag=true;
			for(var p=0;p<searchRec.length;p++)
			{
				var vStatus=searchRec[p].getValue("custrecord_linestatus_flag");
				nlapiLogExecution('DEBUG','vStatus ',vStatus);
				if(vStatus != null &&  vStatus != '' && parseInt(vStatus) != 25 )
				{
					SODelStatusFlag=false;
				}	
			}
			nlapiLogExecution('DEBUG','SODelStatusFlag ',SODelStatusFlag);
			if(SODelStatusFlag == true)
			{	
				nlapiLogExecution('DEBUG','searchRec is null so rec can be deleted ',searchRec);
				for ( var s = 0; s < searchRec.length; s++) {
					var Id=nlapiDeleteRecord(searchRec[s].getRecordType(),searchRec[s].getId());
					nlapiLogExecution('DEBUG','Deleted record Id',Id);
				}
			}
			else				 
			{
				nlapiLogExecution('DEBUG','searchRec is not null so rec cannot  be deleted','');
				var ErrorMsg = nlapiCreateError('CannotDelete','Order is being processed in Main Warehouse. Do not delete', true);
				throw ErrorMsg;
			}
		}
	}
//	To validate the order lines if updating by CSVImport/webservices/webstore
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','context.getExecutionContext() ',context.getExecutionContext());
	nlapiLogExecution('DEBUG','context.getExecutionContext() ',context.getExecutionContext() == 'csvimport');
	if((type=='edit' || type=='xedit') && (context.getExecutionContext() == 'webservices' || context.getExecutionContext() == 'webstore'  || context.getExecutionContext() == 'csvimport'))
	{
		var Soid=nlapiGetFieldValue('id');


		nlapiLogExecution('DEBUG','lineItemCount',nlapiGetLineItemCount('item'));
		if(Soid != null && Soid != '')
		{
			var sorec=nlapiLoadRecord('salesorder',Soid);

			for(var i=1;i<=nlapiGetLineItemCount('item');i++)
			{
				var currentLineno=nlapiGetLineItemValue('item','line',i);
				for(var j=1;j<=nlapiGetLineItemCount('item');j++)
				{
					var vSOLineno=nlapiGetLineItemValue('item','line',j);
					nlapiLogExecution('DEBUG','vSOLineno',vSOLineno);
					nlapiLogExecution('DEBUG','currentLineno',currentLineno);
					if(vSOLineno==currentLineno)
					{	 
						var ordersearchResult = getRecordDetails(Soid,currentLineno,'noneof');

						if(ordersearchResult !=null && ordersearchResult !='' && ordersearchResult.length > 0)
						{
							var oldOrderQty=  ordersearchResult[0].getValue('custrecord_ord_qty',null,'sum');
							var oldOrderLineNo=  ordersearchResult[0].getValue('custrecord_ordline',null,'group');
							var oldOrderLocation=  ordersearchResult[0].getValue('custrecord_ordline_wms_location',null,'group');

							nlapiLogExecution('DEBUG','oldOrderLineNo',oldOrderLineNo);

							var NewOrderQty = nlapiGetLineItemValue('item', 'quantity', oldOrderLineNo);
							var NewOrdLocation = nlapiGetLineItemValue('item', 'location', oldOrderLineNo);
							nlapiLogExecution('DEBUG','NewOrderQty',NewOrderQty);
							nlapiLogExecution('DEBUG','NewOrdLocation',NewOrdLocation);
							nlapiLogExecution('DEBUG','oldOrderLocation',oldOrderLocation);
							nlapiLogExecution('DEBUG','oldOrderQty',oldOrderQty);
							nlapiLogExecution('DEBUG','NewOrderQty',NewOrderQty);
							nlapiLogExecution('DEBUG','orderQty',parseFloat(oldOrderQty)> parseFloat(NewOrderQty));
							if(parseFloat(oldOrderQty)> parseFloat(NewOrderQty))
							{ 
								nlapiLogExecution('DEBUG','order is being proceesed,cannot edit','order is being proceesed,cannot edit');
								var ErrorMsg = nlapiCreateError('CannotEdit','order is being proceesed,cannot edit', true);
								throw ErrorMsg;
							}
							if(NewOrdLocation != null && NewOrdLocation != '' && oldOrderLocation != NewOrdLocation)
							{
								nlapiLogExecution('DEBUG','order is being proceesed,cannot edit','order is being proceesed,cannot edit');
								var ErrorMsg = nlapiCreateError('CannotEdit','order is being proceesed,cannot edit', true);
								throw ErrorMsg;
							}
							break;
						}

					}
				} 
			}
		}
	}
}

/**
 * Delete the records which are fufilled and having status E
 * @param soId
 * @param lineNo
 */
function DeleteRecCreated(soId, lineNo, shipcomplete)
{ 
	nlapiLogExecution('DEBUG','Into DeleteRecCreated');

	var str1 = 'soId. = ' + soId + '<br>';
	str1 = str1 + 'lineNo. = ' + lineNo + '<br>';
	str1 = str1 + 'shipcomplete. = ' + shipcomplete + '<br>';

	nlapiLogExecution('DEBUG', 'Function parameters', str1);

	var filter = new Array();
	filter[0]= new nlobjSearchFilter('name', null, 'is', soId);
	filter[1]= new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineNo);
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_linestatus_flag') ;
	column[1] = new nlobjSearchColumn('custrecord_ordline') ;
	column[2] = new nlobjSearchColumn('custrecord_pickgen_qty') ;
	column[3] = new nlobjSearchColumn('custrecord_pickqty') ;

	var searchRec= nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);
	if(searchRec!=null)
		nlapiLogExecution('DEBUG','searchRec ',searchRec.length);
	var Id="";
	var Type="";

	if (searchRec)
	{
		for (var s = 0; s < searchRec.length; s++)
		{
			var searchresult = searchRec[s];
			nlapiLogExecution('DEBUG','searchresult',searchresult);
			Id = searchRec[s].getId();
			Type= searchRec[s].getRecordType();

			var fostatus = searchRec[s].getValue('custrecord_linestatus_flag');
			var pickgenqty = searchRec[s].getValue('custrecord_pickgen_qty');
			var pickqty = searchRec[s].getValue('custrecord_pickqty');

			if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
				pickgenqty=0;

			if(pickqty==null || pickqty=='' || isNaN(pickqty))
				pickqty=0;

			var str = 'fostatus. = ' + fostatus + '<br>';
			str = str + 'pickgenqty. = ' + pickgenqty + '<br>';
			str = str + 'pickqty. = ' + pickqty + '<br>';

			nlapiLogExecution('DEBUG', 'FO Values', str);

			if(fostatus==25 && parseFloat(pickgenqty)==0)
			{
				nlapiDeleteRecord(searchRec[s].getRecordType(),searchRec[s].getId());
				//nlapiLogExecution('DEBUG','Deleted record Id',Id);
			}
			else
			{
				nlapiSubmitField('customrecord_ebiznet_ordline',Id,'custrecord_shipcomplete',shipcomplete);
			}
		}
	}
}

function DeleteFOlinesWithEditStatus(soId)
{
	var filter = new Array();
	filter[0]= new nlobjSearchFilter('name', null, 'is', soId);
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_linestatus_flag'); 
	var searchRec = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);

	if(searchRec != null && searchRec != "")
	{

		for(var p=0;p<searchRec.length;p++)
		{
			var vStatus=searchRec[p].getValue("custrecord_linestatus_flag");
			nlapiLogExecution('DEBUG','vStatus ',vStatus);
			if(vStatus != null &&  vStatus != '' && parseInt(vStatus) == 25 )
			{
				nlapiDeleteRecord(searchRec[p].getRecordType(),searchRec[p].getId());
			}	
		}
	}

}
/**
 * Delete the records which are fufilled and having status E
 * @param soId
 * @param lineNo
 */
function DeleteOpenFOlines(soId, linesarray, shipcomplete)
{ 
	nlapiLogExecution('DEBUG','Into DeleteOpenFOlines');

	var str = 'soId. = ' + soId + '<br>';
	str = str + 'linesarray. = ' + linesarray + '<br>';
	str = str + 'shipcomplete. = ' + shipcomplete + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var filter = new Array();
	filter[0]= new nlobjSearchFilter('custrecord_ns_ord', null, 'is', soId);

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_linestatus_flag') ;
	column[1] = new nlobjSearchColumn('custrecord_ordline').setSort();
	column[2] = new nlobjSearchColumn('custrecord_pickgen_qty') ;
	column[3] = new nlobjSearchColumn('custrecord_pickqty') ;

	var searchRec= nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);
	if(searchRec!=null)
		nlapiLogExecution('DEBUG','searchRec ',searchRec.length);

	var Id="";
	var Type="";

	if (searchRec)
	{
		nlapiLogExecution('DEBUG','linesarray.length',linesarray.length);

		for (var x = 0; x < linesarray.length; x++)
		{
			var solineno = linesarray[x][0];

			//nlapiLogExecution('DEBUG','solineno',solineno);

			for (var s = 0; s < searchRec.length; s++)
			{
				var folineno = searchRec[s].getValue('custrecord_ordline');

				//nlapiLogExecution('DEBUG','folineno',folineno);

				if(solineno == folineno)
				{
					Id = searchRec[s].getId();
					Type= searchRec[s].getRecordType();

					var fostatus = searchRec[s].getValue('custrecord_linestatus_flag');
					var pickgenqty = searchRec[s].getValue('custrecord_pickgen_qty');
					var pickqty = searchRec[s].getValue('custrecord_pickqty');

					if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
						pickgenqty=0;

					if(pickqty==null || pickqty=='' || isNaN(pickqty))
						pickqty=0;

					var str1 = 'fostatus. = ' + fostatus + '<br>';
					str1 = str1 + 'pickgenqty. = ' + pickgenqty + '<br>';
					str1 = str1 + 'pickqty. = ' + pickqty + '<br>';
					str1 = str1 + 'solineno. = ' + solineno + '<br>';
					str1 = str1 + 'folineno. = ' + folineno + '<br>';

					nlapiLogExecution('DEBUG', 'FO Values', str1);

					if(fostatus==25 && parseFloat(pickgenqty)==0)
					{
						nlapiDeleteRecord(searchRec[s].getRecordType(),searchRec[s].getId());
						//nlapiLogExecution('DEBUG','Deleted record Id',Id);
					}
					else
					{
						nlapiSubmitField('customrecord_ebiznet_ordline',Id,'custrecord_shipcomplete',shipcomplete);
					}

					break;
				}
			}
		}
	}

	nlapiLogExecution('DEBUG','Out of DeleteOpenFOlines');
}


/**
 *  Generate the fulfillment order number based on the existing fulfillment orders
 *  	by concatenating the sales order number with the maximum value of the fulfillment order number.
 * @param salesOrderId
 * @returns {String}
 */
function fulfillmentOrderDetails(salesOrderId,salesordername)
{
	var salesOrderArray = new Array();
	var searchFulfillment =  getFullFillment(salesOrderId);
	//nlapiLogExecution('DEBUG','searchFulfillment :', searchFulfillment);

	//If searchfulfillment is null it indicates Its a New SO. 
	if(searchFulfillment == null){
		var fulfillmentNumber=salesordername +".1";
	}
	else{
		for(var i=0; i< searchFulfillment.length;i++)
		{
			var fullfillmentlineord = searchFulfillment[i].getValue('custrecord_lineord');
			var lineordsplit = fullfillmentlineord.split('.');
			salesOrderArray[i] = lineordsplit[1];
		}
		//MaxValue returns the max fullfillment order number
		var maximumValue = MaxValue(salesOrderArray);

		//adding 1 to the max fullfillment order number to get the new fullfillment order no.
		var fulfillmentValue = parseFloat(maximumValue) + parseFloat(1);
		nlapiLogExecution('DEBUG','fulfillment Value :', fulfillmentValue);

		//concatinating the fulfillmentvalue to salesorderid to get new fullfillment no.
		var fulfillmentNumber =  salesordername + "." + fulfillmentValue;
		//nlapiLogExecution('DEBUG','fulfillmentNumber :', fulfillmentNumber);
	}
	return fulfillmentNumber;
}

/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
function fulfillmentOrderDetailsforSchedulescript(salesOrderId,salesordername,solocation,solineshipmethod,solineshipto)
{/***Upto here ***/
	var fulfillmentNumber =  '';
	var salesOrderArray = new Array();
	var searchFulfillment =  getFullFillment(salesOrderId);
	//nlapiLogExecution('DEBUG','searchFulfillment :', searchFulfillment);

	//If searchfulfillment is null it indicates Its a New SO. 
	if(searchFulfillment == null){
		var fulfillmentNumber=salesordername +".1";
	}
	else{
		for(var i=0; i< searchFulfillment.length;i++)
		{
			var fullfillmentlineord = searchFulfillment[i].getValue('custrecord_lineord');
			var folinestatus = searchFulfillment[i].getValue('custrecord_linestatus_flag');
			/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
			var folocation = searchFulfillment[i].getValue('custrecord_ordline_wms_location'); //
			var foshipmethod = searchFulfillment[i].getValue('custrecord_do_carrier'); //
			var folineshipto = searchFulfillment[i].getValue('custrecord_ebiz_ship_attention'); //
			var lineordsplit = fullfillmentlineord.split('.');
			salesOrderArray[i] = lineordsplit[1];
			/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
			if(folineshipto==null || folineshipto=='')
				folineshipto=-1;
			if(solineshipto==null || solineshipto=='' || solineshipto=='undefined')
				solineshipto=-1;
			nlapiLogExecution('DEBUG','folineshipto : solineshipto : solineshipmethod : foshipmethod : solocation : folocation : folinestatus', folineshipto +" : "+ solineshipto +" : "+ solineshipmethod +" : "+ foshipmethod +" : "+ solocation +" : "+ folocation +" : "+ folinestatus);
			if(folinestatus==25 && solocation==folocation && solineshipmethod==foshipmethod && folineshipto==solineshipto)
			{
				fulfillmentNumber=fullfillmentlineord;
				return fulfillmentNumber;
			}			
		}
		//MaxValue returns the max fullfillment order number
		var maximumValue = MaxValue(salesOrderArray);

		//adding 1 to the max fullfillment order number to get the new fullfillment order no.
		var fulfillmentValue = parseInt(maximumValue) + parseInt(1);
		nlapiLogExecution('DEBUG','fulfillment Value :', fulfillmentValue);

		//concatinating the fulfillmentvalue to salesorderid to get new fullfillment no.
		fulfillmentNumber =  salesordername + "." + fulfillmentValue;
		//nlapiLogExecution('DEBUG','fulfillmentNumber :', fulfillmentNumber);
	}
	return fulfillmentNumber;
}

/**
 * searching for records in fullfillment order line
 * @param soId
 * @returns ordlinesearchresults
 */

function getFullFillment(soId)
{	
	var ordlinefilters = new Array();
	ordlinefilters[0] = new nlobjSearchFilter('name', null, 'is', soId);
	var ordlinecolumns = new Array();  
	ordlinecolumns[0] = new nlobjSearchColumn('custrecord_lineord').setSort();        
	ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ordline');
	ordlinecolumns[2] = new nlobjSearchColumn('custrecord_ord_qty'); // fulfillment ordered quantity
	ordlinecolumns[3] = new nlobjSearchColumn('custrecord_linestatus_flag'); //
	ordlinecolumns[4] = new nlobjSearchColumn('custrecord_ordline_wms_location'); //
	ordlinecolumns[5] = new nlobjSearchColumn('custrecord_do_carrier'); //
	ordlinecolumns[6] = new nlobjSearchColumn('custrecord_ebiz_ship_attention'); //

	var ordlinesearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);
	return ordlinesearchresults;
}

function getFullFillmentbyline(soId,lineno)
{
	nlapiLogExecution('DEBUG','Into  getFullFillmentbyline');
	nlapiLogExecution('DEBUG','soId',soId);
	nlapiLogExecution('DEBUG','lineno',lineno);

	var ordlinefilters = new Array();
	ordlinefilters[0] = new nlobjSearchFilter('name', null, 'is', soId);
	ordlinefilters[1] = new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineno);
	ordlinefilters[2] = new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [25]);//EDIT

	var ordlinecolumns = new Array();  
	ordlinecolumns[0] = new nlobjSearchColumn('custrecord_lineord').setSort();        
	ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ordline');
	ordlinecolumns[2] = new nlobjSearchColumn('custrecord_ord_qty'); // fulfillment ordered quantity
	ordlinecolumns[3] = new nlobjSearchColumn('custrecord_linestatus_flag'); //

	var ordlinesearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);

	nlapiLogExecution('DEBUG','Out of  getFullFillmentbyline');

	return ordlinesearchresults;
}

/**
 * Getting the maximum value for the fulfillment order.
 * @param array
 * @returns maximumNumber
 */
function MaxValue(array)
{
	var maximumNumber = array[0];
	for (var i = 0; i < array.length; i++) 
	{
		if (array[i] > maximumNumber) 
		{
			maximumNumber = array[i];
		}
	}
	return maximumNumber;
}

/**
 * To get the sum of all the order qty of the particular line no and SO, 
 * from fullfillment order table
 * @param soid
 * @param lineno
 * @returns Totalqty
 */
function getFulfilmentqty(soid,lineno)
{
	var vqty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', soid.toString());
	filters[1] = new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineno);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	var searchresultsRcpts = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	if (searchresultsRcpts != null) 
	{
		var val = searchresultsRcpts[0];
		vqty=val.getValue('custrecord_ord_qty',null, 'sum');
		//nlapiLogExecution('DEBUG','vqtysum',vqty);
		if(vqty==null)
			return 0;
		else
			return vqty;
	}
}


function getFulfilmentqtyforallLines(soid)
{
	nlapiLogExecution('DEBUG','Into getFulfilmentqtyforallLines');

	var searchresultsRcpts = new Array();

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', soid.toString());

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_ordline',null,'group');

	var searchresultsRcpts = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	nlapiLogExecution('DEBUG','Out of getFulfilmentqtyforallLines');

	return searchresultsRcpts;
}

function getFulfilmentqtybyLine(soid, salesOrderLineNo,alllinesfoqty)
{
	nlapiLogExecution('DEBUG','Into getFulfilmentqtybyLine');

	var foqty=0;

	if(alllinesfoqty!=null && alllinesfoqty!='' && alllinesfoqty.length>0)
	{
		for (var j = 0; j < alllinesfoqty.length; j++) 
		{
			var foline = alllinesfoqty[j].getValue('custrecord_ordline',null,'group');

			if(salesOrderLineNo==foline)
			{
				foqty=alllinesfoqty[j].getValue('custrecord_ord_qty',null, 'sum');
			}
		}
	}

	nlapiLogExecution('DEBUG','Out of getFulfilmentqtybyLine',foqty);
	return foqty;
}

/**
 * Read the string and split with comma seperator then
 * delete the records which are removed from SO 
 * @param soId
 */
function deleteEditedLine(soId)
{
	//reading the string from transaction body field
	var getAllLineNo = nlapiGetFieldValue('custbody_ebiz_lines_deleted');
	var shipcomplete = nlapiGetFieldValue('shipcomplete');
	nlapiLogExecution('DEBUG','getAllLineNo',getAllLineNo);

	var eachLineNo=new Array();

	//spliting at comma separator
	eachLineNo=getAllLineNo.split(',');
	nlapiLogExecution('DEBUG','ARRAYLENGTH',eachLineNo.length);

	for(var i=0;i<eachLineNo.length-1;i++)
	{
		//nlapiLogExecution('DEBUG','eachLineNo',eachLineNo[i]);
		//passing the lineNo which is deleted from SO to the function.
		DeleteRecCreated(soId, eachLineNo[i],shipcomplete);
	}

}

/**
 * 
 * @param itemid
 */
function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	if(itemid!=null && itemid!='')
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filter.push(new nlobjSearchFilter('custrecord_ebiznsuom', null, 'noneof', '@NONE@'));
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
		column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
		column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
		column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
		searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
	}

	return searchRec;
}


/**
 * It triggers after page is submitted 
 * @param type
 * @param form
 * @param request
 */
function AutoAdjustFulfillmentOrders(type, form, request)
{

	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Into AutoAdjustFulfillmentOrders',type);
	nlapiLogExecution('DEBUG','Remaining usage at the start of AutoAdjustFulfillmentOrders ',context.getRemainingUsage());
	nlapiLogExecution('DEBUG','Time Stamp at the start of AutoAdjustFulfillmentOrders ',TimeStampinSec());

	try
	{
		//if(type !=  'delete' && context.getExecutionContext() != 'webstore')
		if(type !=  'delete')
		{
			var fulfillmentOrdersAgainstLocationAlias=new Array();
			var LocationAliasArray= getLocationAliases();
			var create_fulfillment_order = nlapiGetFieldValue('custbody_create_fulfillment_order');

			var soid = nlapiGetRecordId();
			nlapiLogExecution('DEBUG','soid',soid);
			var trantype = nlapiLookupField('transaction', soid, 'recordType');
			var searchresults = nlapiLoadRecord(trantype, soid); 
			var soname = searchresults.getFieldValue('tranid');
			var shipcomplete = searchresults.getFieldValue('shipcomplete');
			var location = searchresults.getFieldValue('location');
			var getinstdata = searchresults.getFieldValue('custbody_nswmspoinstructions');
			nlapiLogExecution('DEBUG','getinstdata',getinstdata);

			/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh for FO creation based on linelevel shipmethod***/
			var HeadShipmethod = searchresults.getFieldValue('shipmethod');
			var ItemLineShipping = searchresults.getFieldValue('ismultishipto');
			/***Upto here***/
			var toLocation="";
			if(trantype=="transferorder")
				toLocation= searchresults.getFieldValue('transferlocation');

			var str = 'soname. = ' + soname + '<br>';
			str = str + 'soid. = ' + soid + '<br>';
			str = str + 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';	
			str = str + 'ItemLineShipping. = ' + ItemLineShipping + '<br>';	
			str = str + 'shipcomplete. = ' + shipcomplete + '<br>';	
			str = str + 'location. = ' + location + '<br>';	
			str = str + 'HeadShipmethod. = ' + HeadShipmethod + '<br>';	
			str = str + 'toLocation. = ' + toLocation + '<br>';	

			nlapiLogExecution('Debug', 'Header parameters', str);
			
			nlapiLogExecution('Debug', 'type', type);
			nlapiLogExecution('Debug', 'context.getExecutionContext()', context.getExecutionContext());

			//if(type=='create' && context.getExecutionContext() == 'webservices')
			if(type=='create' && (context.getExecutionContext() == 'webservices'|| context.getExecutionContext() == 'webstore'|| context.getExecutionContext() == 'userevent'))
			{
				for(var i =0; i < nlapiGetLineItemCount('item'); i++)
				{
					var LineItem = nlapiGetLineItemValue('item', 'item', i + 1);
					var LineLocation = nlapiGetLineItemValue('item', 'location', i + 1);
					var LineItemStatus = nlapiGetLineItemValue('item', 'custcol_ebiznet_item_status', i + 1);

					var vTempLocation=LineLocation;
					if(LineLocation == null || LineLocation == '')
						vTempLocation=location;

					var str = 'LineItem. = ' + LineItem + '<br>';
					str = str + 'LineLocation. = ' + LineLocation + '<br>';
					str = str + 'LineItemStatus. = ' + LineItemStatus + '<br>';	
					str = str + 'vTempLocation. = ' + vTempLocation + '<br>';	

					nlapiLogExecution('Debug', 'Line parameters', str);

					if(LineItem != null && LineItem != '' && vTempLocation != null && vTempLocation != '' && (LineItemStatus == null || LineItemStatus==''))
					{	
						if(trantype=='transferorder')
							var searchresult = SetItemStatus("TransferOrder", LineItem, vTempLocation, toLocation);
						else
							var searchresult = SetItemStatus(trantype, LineItem, vTempLocation, null);
						nlapiLogExecution('ERROR','searchresult',searchresult);
						if (searchresult != null && searchresult != '') 
						{
							//nlapiSetLineItemValue('item', 'custcol_ebiznet_item_status', i+1, searchresult[0]);

							searchresults.setLineItemValue('item','custcol_ebiznet_item_status',i+1,searchresult[0]);
							if(searchresult[1] != null && searchresult[1] != '')
							{
								//nlapiSetLineItemValue('item', 'custcol_nswmspackcode',i+1, searchresult[1]);
								searchresults.setLineItemValue('item','custcol_nswmspackcode',i+1,searchresult[1]);
							}
						}
					}

				}
				nlapiSubmitRecord(searchresults, true);	
			}

			try
			{
				var solinearray = new Array();
				//delete the record which are having the same SO,Lineno and with status E
				for(var i =0; i < nlapiGetLineItemCount('item'); i++)
				{
					var salesOrderLineNo = nlapiGetLineItemValue('item', 'line', i + 1);
					var qtyEntered = nlapiGetLineItemValue('item', 'quantity', i + 1);
					var currow = [salesOrderLineNo,qtyEntered];
					solinearray.push(currow);
				}

				if(solinearray != null && solinearray!='' && solinearray.length>0)
				{
					DeleteOpenFOlines(soid, solinearray,shipcomplete);
				}
			}
			catch(exp)
			{
				nlapiLogExecution("ERROR","Exception in Deleting record whose status is E",exp);
			}

			if(create_fulfillment_order != null && create_fulfillment_order == 'T' )
			{	
				try
				{
					// To lock sales order
					if(soname != null && soname != '')
					{	
						nlapiLogExecution('DEBUG','soname before Locking',soname);
						nlapiLogExecution('DEBUG','trantype before Locking',trantype);
						var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
						fulfillRec.setFieldValue('name', soname);					
						fulfillRec.setFieldValue('custrecord_ebiznet_trantype', trantype);
						fulfillRec.setFieldValue('externalid', soname);
						fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by UE');
						nlapiSubmitRecord(fulfillRec,false,true );
						nlapiLogExecution('DEBUG','Locked successfully');
					}
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
					var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
					throw wmsE;
				}
				try
				{

					if(type == 'edit' || type=='create')  
					{	
						var nooflines = nlapiGetLineItemCount('item');

						nlapiLogExecution('DEBUG','nooflines',nooflines);

						if(nooflines>100)
						{
							var committedlinescount = getCommittedlinescount(soid);

							nlapiLogExecution('DEBUG','committedlinescount',committedlinescount);

							if(committedlinescount>100)
							{
								var ordsize = nlapiGetFieldValue('custbody_ebiz_ord_size');

								nlapiLogExecution('DEBUG','ordsize',ordsize);

								if(ordsize!='L')
								{
									nlapiSubmitField('salesorder',soid,'custbody_ebiz_ord_size','L');
								}

								return;
							}
						}

						// Delete the fulfillment order lines for those sales order lines deleted by 
						//user by clicking the button 'Remove' on SO screen
						var getAllLineNo=nlapiGetFieldValue('custbody_ebiz_lines_deleted');
						var shipcomplete = nlapiGetFieldValue('shipcomplete');
						if(getAllLineNo != null)
							deleteEditedLine(soid);

						/*var solinearray = new Array();
						//delete the record which are having the same SO,Lineno and with status E
						for(var i =0; i < nlapiGetLineItemCount('item'); i++)
						{
							var salesOrderLineNo = nlapiGetLineItemValue('item', 'line', i + 1);
							var qtyEntered = nlapiGetLineItemValue('item', 'quantity', i + 1);

							var currow = [salesOrderLineNo,qtyEntered];

							solinearray.push(currow);

//							if(salesOrderLineNo != null)
//							DeleteRecCreated(soid, salesOrderLineNo,shipcomplete);

//							nlapiLogExecution('DEBUG','qtyEntered',qtyEntered);
//							updateFulfillmentOrderByQtyEntered(salesOrderLineNo,soid,qtyEntered);
						}

						if(solinearray != null && solinearray!='' && solinearray.length>0)
						{
							DeleteOpenFOlines(soid, solinearray,shipcomplete);
							//updateFulfillmentOrderByQtyEntered(solinearray,soid);
						}*/
					}

					var cnt=searchresults.getLineItemCount('item');
					var createfo='T';
					//Case# 20124404� Start 

					try
					{
						if(trantype == 'salesorder')
							createfo = fnCreateFo(soid);
					}
					catch(exp)
					{
						nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
						createfo='T';
					}

					nlapiLogExecution('DEBUG','createfo',createfo);
					//Case# 20124404� end 
					// The below code is for shipcomplete functionality - 11/09/2011
					if(createfo=='T' && shipcomplete=='T')
					{
						for (var j = 1; j <= parseFloat(cnt); j++) 
						{	
							var commtdqty = searchresults.getLineItemValue('item','quantitycommitted',j);
							var orderqty = searchresults.getLineItemValue('item', 'quantity', j); 
							var fulfildQty = searchresults.getLineItemValue('item', 'quantityfulfilled', j);
							var linelocation = searchresults.getLineItemValue('item', 'location', j);
							var itemtype = searchresults.getLineItemValue('item', 'itemtype', j);
							var isLineclosed = searchresults.getLineItemValue('item', 'isclosed', j);

							if(linelocation!=null && linelocation!='')
								location=linelocation;

							var mwhsiteflag='F';
							var	islocationAliasExists=CheckLocationAlias(location);
							if(islocationAliasExists!=null && islocationAliasExists!='')
							{
								mwhsiteflag='T';
							}
							else
							{
								if(location!=null && location!='')
								{
									var fields = ['custrecord_ebizwhsite'];

									var locationcolumns = nlapiLookupField('Location', location, fields);
									if(locationcolumns!=null)
										mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
								}
							}

							if(fulfildQty==null || fulfildQty=='' || isNaN(fulfildQty))
							{
								fulfildQty=0;
							}
							if(commtdqty==null || commtdqty=='' || isNaN(commtdqty))
							{
								commtdqty=0;
							}

							var totalcommtdqty=parseFloat(commtdqty)+parseFloat(fulfildQty);

							nlapiLogExecution('DEBUG','itemtype',itemtype);

							if(itemtype!='Description' && itemtype!='Discount' && itemtype!='Payment' && itemtype!='Markup' 
								&& itemtype!='Subtotal' && itemtype!='OthCharge' && itemtype!='Service' && itemtype!='noninventoryitem'
									&& itemtype!='NonInvtPart')
							{
								//case 20127066 : added new Lineclosed condition to If condition 
								if((mwhsiteflag=='T') && (parseFloat(orderqty) > parseFloat(totalcommtdqty)) && (isLineclosed != 'T'))
								{
									createfo = 'F';
									continue;
								}
							}
						}
					}
					//upto here
					//alert(createfo);
					if(createfo=='T')
					{
						var alllinesfoqty = getFulfilmentqtyforallLines(soid);
						var foparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); 
						var custTypearray = new Array();
						var solocationarray = new Array();
						var FOShipmethodArr=new Array();
						var FOShipToArr=new Array();
						for (var t = 1; t <= parseFloat(cnt); t++) 
						{
							var customizationtype = searchresults.getLineItemValue('item','custcol_customizationcode',t);
							if(customizationtype==null || customizationtype=='')
								customizationtype=-1;

							custTypearray.push(customizationtype);
							/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh for FO creation based on linelevel shipmethod***/
							var vShipMethod=HeadShipmethod;
							var vLineShipmethod = searchresults.getLineItemValue('item','shipmethod',t);
//							nlapiLogExecution('DEBUG','vLineShipmethod',vLineShipmethod);
//							nlapiLogExecution('DEBUG','vLineShipmethod',searchresults.getLineItemValue('item','shipvia',t));
//							nlapiLogExecution('DEBUG','vHeadShipmethod',searchresults.getFieldValue('shipvia'));
							if(vLineShipmethod==null || vLineShipmethod=='')
								vLineShipmethod=vShipMethod;
							if(ItemLineShipping=='T')
								vShipMethod=vLineShipmethod;

							if(vShipMethod==null || vShipMethod=='')
							{
								vShipMethod=-1;								
							}

							FOShipmethodArr.push(vShipMethod);

							/***Upto here ***/
							var solinelocation = searchresults.getLineItemValue('item', 'location', t);
							if(solinelocation == null || solinelocation =='')
							{
								solinelocation = location;
							}	

							if(solinelocation!=null && solinelocation!='')
								solocationarray.push(solinelocation);
							var solineshipto = searchresults.getLineItemValue('item', 'shipaddress', t);

							if(solineshipto==null || solineshipto=='')
								solineshipto=-1;

							FOShipToArr.push(solineshipto);
						}

						nlapiLogExecution('DEBUG','custTypearray',custTypearray);
						custTypearray=removeDuplicateElement(custTypearray);
						//nlapiLogExecution('DEBUG','distinct custTypearray',custTypearray);

						//nlapiLogExecution('DEBUG','solocationarray',solocationarray);
						solocationarray=removeDuplicateElement(solocationarray);
						//nlapiLogExecution('DEBUG','distinct solocationarray',solocationarray);

						/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh for FO creation based on linelevel shipmethod***/
						//nlapiLogExecution('DEBUG','FOShipmethodArr',FOShipmethodArr);

						//case 20123302 start
						if(FOShipmethodArr!=undefined && FOShipmethodArr.length>0 ) 
							FOShipmethodArr=removeDuplicateElement(FOShipmethodArr);
						//nlapiLogExecution('DEBUG','distinct FOShipmethodArr',FOShipmethodArr);
						/***Upto here***/

						//nlapiLogExecution('ERROR','FOShipToArr',FOShipToArr);
						FOShipToArr=removeDuplicateElement(FOShipToArr);
						//nlapiLogExecution('ERROR','distinct FOShipToArr',FOShipToArr);

						var str = 'custTypearray. = ' + custTypearray + '<br>';
						str = str + 'solocationarray. = ' + solocationarray + '<br>';
						str = str + 'FOShipmethodArr. = ' + FOShipmethodArr + '<br>';	
						str = str + 'FOShipToArr. = ' + FOShipToArr + '<br>';	

						nlapiLogExecution('Debug', 'Array parameters', str);

						var focreated=0;

						for (var h = 0; h <solocationarray.length; h++) 
						{

							var solocation = solocationarray[h];
							/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh for FO creation based on linelevel shipmethod***/
							for (var s = 0; s <custTypearray.length; s++) 
							{
								var customizetype = custTypearray[s];

								for(var z=0;z<FOShipmethodArr.length;z++)
								{
									var FOShipmethod = FOShipmethodArr[z];
									for(var f=0;f<FOShipToArr.length;f++)
									{

										var oldfulFillmentOrdNo='-1';
										var fullFillmentorderno='-1';	
										var FOShipTo = FOShipToArr[f];

										for (var i = 1; i <= parseFloat(cnt); i++) 
										{
											var linecustomizetype = searchresults.getLineItemValue('item','custcol_customizationcode',i);

											var linecustomizetype = searchresults.getLineItemValue('item','custcol_customizationcode',i);

											var lineShipMethod = searchresults.getLineItemValue('item','shipmethod',i);

											var lineShipTo = searchresults.getLineItemValue('item','shipaddress',i);



											/*var salesOrderLineformsg = searchresults.getLineItemValue('item','line',i);


											nlapiLogExecution("ERROR","salesOrderLineformsg",salesOrderLineformsg);
											 */
											var getlineinstdata = searchresults.getLineItemValue('item','custcol_wms_rfinstructions',i);



											nlapiLogExecution("ERROR","getlineinstdata",getlineinstdata);



											if(linecustomizetype==null || linecustomizetype=='')
												linecustomizetype=-1;

											var linelocation = searchresults.getLineItemValue('item', 'location', i);
											if(linelocation != null && linelocation !='')
											{
												location = linelocation;
											}	
											if(lineShipTo==null || lineShipTo=='')
												lineShipTo=-1;

											var vTestShipmethod=HeadShipmethod;
											if(ItemLineShipping=='T' && lineShipMethod !=  null && lineShipMethod != '')
												vTestShipmethod=lineShipMethod;

											if(vTestShipmethod==null || vTestShipmethod=='')
												vTestShipmethod=-1;

											var str1 = 'solocation. = ' + solocation + '<br>';
											str1 = str1 + 'location. = ' + location + '<br>';
											str1 = str1 + 'Shipmethod. = ' + vTestShipmethod + '<br>';
											str1 = str1 + 'ShipTo. = ' + lineShipTo + '<br>';
											str1 = str1 + 'i. = ' + i + '<br>';
											nlapiLogExecution('DEBUG', 'Location Values', str1);

											if(solocation==location)
											{										
												var str2 = 'customizetype. = ' + customizetype + '<br>';
												str2 = str2 + 'linecustomizetype. = ' + linecustomizetype + '<br>';

												nlapiLogExecution('DEBUG', 'Cutomize Type Values', str2);

												if(customizetype==linecustomizetype)
												{
													var str3 = 'FOShipmethod. = ' + FOShipmethod + '<br>';
													str3 = str3 + 'vTestShipmethod. = ' + vTestShipmethod + '<br>';

													nlapiLogExecution('DEBUG', 'Shipmethod Type Values', str3);
													if(FOShipmethod==vTestShipmethod)
													{	
														var str3 = 'FOShipTo. = ' + FOShipTo + '<br>';
														str3 = str3 + 'lineShipTo. = ' + lineShipTo + '<br>';
														nlapiLogExecution('ERROR', 'Ship To Values', str3);
														if(FOShipTo==lineShipTo)
														{	
															var variance=0;
															var soLine_Hold_flag = searchresults.getLineItemValue('item','custcol_create_fulfillment_order',i);				
															var linecommtdqty = searchresults.getLineItemValue('item','quantitycommitted',i);
															var lineorderqty = searchresults.getLineItemValue('item', 'quantity', i); 
															var linefulfildQty = searchresults.getLineItemValue('item', 'quantityfulfilled', i);
															var vNSUOM = searchresults.getLineItemValue('item','units', i);
															var vLineItem = searchresults.getLineItemValue('item','item', i);
															if(lineorderqty==null || lineorderqty=='' || isNaN(lineorderqty))
															{
																lineorderqty=0;
															}
															if(linefulfildQty==null || linefulfildQty=='' || isNaN(linefulfildQty))
															{
																linefulfildQty=0;
															}
															var linelocation = searchresults.getLineItemValue('item', 'location', i);
															if(linelocation != null && linelocation !='')
															{
																location = linelocation;
															}	

															var mwhsiteflag='F';
															var mwhsiteflag='F';
															var	islocationAliasExists=CheckLocationAlias(location);
															if(islocationAliasExists!=null && islocationAliasExists!='')
															{
																mwhsiteflag='T';
															}
															else
															{
																nlapiLogExecution('DEBUG','location',location);
																if(location!=null && location!='')
																{
																	var fields = ['custrecord_ebizwhsite'];

																	var locationcolumns = nlapiLookupField('Location', location, fields);
																	if(locationcolumns!=null)
																		mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
																}
															}

															var str3 = 'mwhsiteflag. = ' + mwhsiteflag + '<br>';
															str3 = str3 + 'lineorderqty. = ' + lineorderqty + '<br>';
															str3 = str3 + 'linefulfildQty. = ' + linefulfildQty + '<br>';
															str3 = str3 + 'soLine_Hold_flag. = ' + soLine_Hold_flag + '<br>';

															nlapiLogExecution('ERROR', 'Qty Values', str3);

															if(mwhsiteflag=='T' && (parseFloat(lineorderqty)> parseFloat(linefulfildQty)))
															{
																nlapiLogExecution('DEBUG','soLine_Hold_flag',soLine_Hold_flag);

																if(soLine_Hold_flag=='T')
																{
																	var salesOrderLineNo = searchresults.getLineItemValue('item','line',i);
																	var commetedQty = searchresults.getLineItemValue('item','quantitycommitted',i);
																	if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
																	{
																		commetedQty=0;
																	}

																	nlapiLogExecution('DEBUG','commetedQty',parseFloat(commetedQty));

																	//var totalQuantity = getFulfilmentqty(soid, salesOrderLineNo);
																	var totalQuantity = getFulfilmentqtybyLine(soid, salesOrderLineNo,alllinesfoqty);
																	if(totalQuantity == null || totalQuantity =='')
																		totalQuantity = 0;
																	nlapiLogExecution('DEBUG','totalQuantity before NS UOM',totalQuantity);		
																	nlapiLogExecution('DEBUG','vNSUOM',vNSUOM);															
																	if(vNSUOM != null && vNSUOM != '')
																	{
																		totalQuantity=fnConvertToBaseUOM(vNSUOM,vLineItem,totalQuantity);
																	}
																	if(totalQuantity == null || totalQuantity =='')
																		totalQuantity = 0;
																	nlapiLogExecution('DEBUG','totalQuantity',totalQuantity);
																	if(totalQuantity == 0)
																		variance=commetedQty;
																	else
																	{
																		var shippedqty = searchresults.getLineItemValue('item', 'quantityfulfilled', i);
																		if(shippedqty==null || shippedqty=='' || isNaN(shippedqty))
																		{
																			shippedqty=0;
																		}
																		var totalcommtdqty = parseFloat(commetedQty)+parseFloat(shippedqty);

																		if(parseFloat(totalQuantity) < parseFloat(totalcommtdqty))
																		{
																			variance=parseFloat(totalcommtdqty)-parseFloat(totalQuantity);
																		}
																		/*** The below code is merged from Factory Mation production account on March-01-2013 by Ganesh K as part of Standard bundle. Purpose is to generate DO based on qty picked directly by NS and eBiz FO qty***/
																		nlapiLogExecution('DEBUG','variance1',variance);

																		if(parseFloat(variance) > 0)
																		{
																			//-------------------------------------------------Change by Ganesh on 16th Jan 2013
																			if((parseFloat(variance)+ parseFloat(shippedqty)  + parseFloat(totalQuantity))>parseFloat(totalcommtdqty))
																			{
																				var totalInTransitFOQuantity = getFulfilmentqtyInProgNew(soid, salesOrderLineNo);
																				nlapiLogExecution('DEBUG','totalInTransitFOQuantity',totalInTransitFOQuantity);
																				variance = parseFloat(totalcommtdqty) - parseFloat(shippedqty) - parseFloat(totalInTransitFOQuantity);
																				//variance = parseFloat(totalcomtdqty) - parseFloat(shippedqty);

																			}
																		}

																	}
																	nlapiLogExecution('DEBUG','variance2',variance);
																	/***Upto here***/
																	if(variance > 0)
																	{	
																		var fullfilorderids='';

																		var str3 = 'fullFillmentorderno. = ' + fullFillmentorderno + '<br>';
																		str3 = str3 + 'oldfulFillmentOrdNo. = ' + oldfulFillmentOrdNo + '<br>';
																		str3 = str3 + 'focreated. = ' + focreated + '<br>';

																		nlapiLogExecution('ERROR', 'FO Values', str3);

																		if(fullFillmentorderno!=oldfulFillmentOrdNo || fullFillmentorderno=='-1')
																		{
																			if(fullfilorderids == null || fullfilorderids == '')
																			{

																				//generate new fullfillment order no.
																				if(focreated==0)
																				{
																					fullFillmentorderno=fulfillmentOrderDetails(soid,soname);
																					var lineordsplit = fullFillmentorderno.split('.');
																					focreated = parseFloat(lineordsplit[1])+parseFloat(1);

																				}
																				else
																				{
																					var aliasFulfillmentorder=null;
																					nlapiLogExecution('DEBUG','LocationAliasArray.length',LocationAliasArray.length);
																					for(var n2=0;n2<LocationAliasArray.length;n2++)
																					{
																						var tlocation2=LocationAliasArray[n2][0];//This is location at Location Alas custum record
																						nlapiLogExecution('DEBUG','tlocation2',tlocation2);
																						if(location==tlocation2)
																						{

																							nlapiLogExecution('DEBUG','location',location);
																							var taliasLocation2=LocationAliasArray[n2][1];
																							nlapiLogExecution('DEBUG','taliasLocation2',taliasLocation2);

																							if(fulfillmentOrdersAgainstLocationAlias.length > 0)
																							{
																								for(var n=0;n<fulfillmentOrdersAgainstLocationAlias.length;n++)
																								{
																									var aiasLocation=fulfillmentOrdersAgainstLocationAlias[n][0];
																									var aiasshipmethod=fulfillmentOrdersAgainstLocationAlias[n][2];
																									if(aiasshipmethod==null || aiasshipmethod=='')
																										aiasshipmethod=-1;
																									var aiasshipto=fulfillmentOrdersAgainstLocationAlias[n][3];
																									if(aiasshipto==null || aiasshipto=='')
																										aiasshipto=-1;

																									var str3 = 'aiasLocation. = ' + aiasLocation + '<br>';
																									str3 = str3 + 'aiasshipmethod. = ' + aiasshipmethod + '<br>';
																									str3 = str3 + 'vTestShipmethod. = ' + vTestShipmethod + '<br>';
																									str3 = str3 + 'aiasshipto. = ' + aiasshipto + '<br>';
																									str3 = str3 + 'lineShipTo. = ' + lineShipTo + '<br>';

																									nlapiLogExecution('ERROR', 'Alias Values', str3);

																									if(taliasLocation2==aiasLocation && aiasshipmethod==vTestShipmethod
																											&& aiasshipto==lineShipTo)
																									{
																										aliasFulfillmentorder=fulfillmentOrdersAgainstLocationAlias[n][1];
																										nlapiLogExecution('DEBUG','aliasFulfillmentorder',aliasFulfillmentorder);
																									}
																								}
																							}
																						}
																					}
																					//var nextvalue = parseFloat(focreated)+parseFloat(1);
																					nlapiLogExecution('DEBUG','aliasFulfillmentorder',aliasFulfillmentorder);
																					if(aliasFulfillmentorder==null)
																					{
																						fullFillmentorderno=soname+'.'+focreated;
																						focreated=parseInt(focreated)+parseInt(1);
																					}
																					else
																					{
																						fullFillmentorderno=aliasFulfillmentorder;
																					}
																				}
																				oldfulFillmentOrdNo=fullFillmentorderno;
																				fullfilorderids = fullFillmentorderno;
																				nlapiLogExecution('DEBUG','fullfilorderids',fullfilorderids);
																				nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);
																			}
																			else
																			{
																				nlapiLogExecution('DEBUG','fullfilorderids',fullfilorderids);
																				var lineordsplit = fullfilorderids.split('.');
																				var nextvalue = parseFloat(lineordsplit[1])+parseFloat(1);
																				fullFillmentorderno=lineordsplit[0]+'.'+nextvalue;
																				oldfulFillmentOrdNo=fullFillmentorderno;
																				fullfilorderids = fullFillmentorderno;
																				nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);
																			}
																		}
//																		case  20123646  end

																		//createFulfillmentOrder(i, variance, fullFillmentorderno,searchresults);
																		createFulfillmentOrderBulk(i, variance, fullFillmentorderno,searchresults,foparent,fulfillmentOrdersAgainstLocationAlias,getinstdata,getlineinstdata);
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					nlapiSubmitRecord(foparent);
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG','exception in auto fulfillment creation',e.message);
				}
				finally
				{
					nlapiLogExecution('DEBUG','soname before unlocking',soname);
					nlapiLogExecution('DEBUG','trantype before unlocking',trantype);

					if(soname != null && soname != '')
					{	
						var ExternalIdFilters = new Array();
						ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
						ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', trantype));
						var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
						if(searchresultsExt != null && searchresultsExt != '')
						{
							nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
							nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
							nlapiLogExecution('DEBUG','Unlocked successfully');
						}	
					}
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in AutoAdjustFulfillmentOrders',exp);
	}
	nlapiLogExecution('DEBUG','Remaining usage at the end of AutoAdjustFulfillmentOrders ',context.getRemainingUsage());
}

function fnConvertToBaseUOM(vNSUOM,itemId,totalQuantity)
{
	var variance=0;
	var veBizUOMQty=0;
	var vBaseUOMQty=0;
	if(vNSUOM!=null && vNSUOM!="")
	{
		nlapiLogExecution('DEBUG', 'itemId', itemId);
		var eBizItemDims=geteBizItemDimensions(itemId);
		nlapiLogExecution('DEBUG', 'eBizItemDims', eBizItemDims);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				nlapiLogExecution('DEBUG', 'Base UOM Flag', eBizItemDims[z].getValue('custrecord_ebizbaseuom'));
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					vBaseUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					nlapiLogExecution('DEBUG', 'vBaseUOMQty', vBaseUOMQty);
				}

				nlapiLogExecution('DEBUG', 'SKU Dim UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
				//if(vNSUOM == eBizItemDims[z].getText('custrecord_ebiznsuom'))
				if(vNSUOM == eBizItemDims[z].getValue('custrecord_ebiznsuom'))
				{					
					veBizUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					nlapiLogExecution('DEBUG', 'veBizUOMQty', veBizUOMQty);
					vUOMId = eBizItemDims[z].getValue('custrecord_ebizuomskudim');
					nlapiLogExecution('DEBUG', 'vUOMId', vUOMId);
				}
			}
			nlapiLogExecution('DEBUG', 'veBizUOMQty', veBizUOMQty);
			nlapiLogExecution('DEBUG', 'variance', variance);
			nlapiLogExecution('DEBUG', 'vBaseUOMQty', vBaseUOMQty);

			if(veBizUOMQty!=0)
				variance = parseFloat(totalQuantity)*parseFloat(vBaseUOMQty)/parseFloat(veBizUOMQty);
			else
				variance=totalQuantity;
			nlapiLogExecution('DEBUG', 'vordqty', variance);

		}
		else
		{
			variance=totalQuantity;
		}

		return variance;
	}

}
function updateFulfillmentOrderByQtyEntered(solines,soid)
{
	nlapiLogExecution('DEBUG','Into updateFulfillmentOrderByQtyEntered');

	var str = 'soid. = ' + soid + '<br>';
	str = str + 'solines. = ' + solines + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);	

	try
	{		
		var filter= new Array();
		filter[0] = new nlobjSearchFilter('custrecord_ns_ord', null, 'is', soid);
		//filter[1]= new nlobjSearchFilter('custrecord_ordline', null, 'equalto', salesOrderLineNo);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_pickgen_qty');
		columns[1] = new nlobjSearchColumn('custrecord_ord_qty');
		columns[2] = new nlobjSearchColumn('custrecord_ordline');

		columns[2].setSort();

		var searchRecords = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, columns);
		if(searchRecords!=null && searchRecords!="")
		{
			for ( var x = 0; x < solines.length; x++)
			{
				var qty = solines[x][1];
				var soline = solines[x][0];

				nlapiLogExecution('DEBUG','soline',soline);

				for ( var count = 0; count < searchRecords.length; count++)
				{
					var foline = searchRecords[count].getValue('custrecord_ordline');
					var PickGenQty = searchRecords[count].getValue('custrecord_pickgen_qty');
					var Foordqty = searchRecords[count].getValue('custrecord_ord_qty');

					if(soline == foline)
					{
						if(PickGenQty==null||PickGenQty=="")
							PickGenQty=0;

						var str = 'soline. = ' + soline + '<br>';
						str = str + 'foline. = ' + foline + '<br>';
						str = str + 'solineqty. = ' + qty + '<br>';
						str = str + 'FO Ordqty. = ' + Foordqty + '<br>';
						str = str + 'FO PickGenQty. = ' + PickGenQty + '<br>';

						nlapiLogExecution('DEBUG', 'Line & Qty Details', str);	

						var recId=searchRecords[count].getId();

						if(parseFloat(PickGenQty)<parseFloat(qty))
						{
							if(parseFloat(PickGenQty)> 0 && parseFloat(PickGenQty)<parseFloat(Foordqty))
							{
								nlapiLogExecution('DEBUG','CHKp1');
								var Id=nlapiSubmitField('customrecord_ebiznet_ordline', recId, 'custrecord_ord_qty',parseFloat(PickGenQty).toFixed(5));
								qty=parseFloat(qty)-parseFloat(PickGenQty);
								nlapiLogExecution('DEBUG','ID',Id);
								nlapiLogExecution('DEBUG','qty after deduction',qty);
							}
							else
							{
								nlapiLogExecution('DEBUG','CHKp2');
								qty=parseFloat(qty)-parseFloat(Foordqty);
								nlapiLogExecution('DEBUG','qty',qty);
							}
						}
						else
						{
							nlapiLogExecution('DEBUG','CHKp3');
							var Id=nlapiSubmitField('customrecord_ebiznet_ordline', recId, 'custrecord_ord_qty',parseFloat(qty).toFixed(5));
							nlapiLogExecution('DEBUG','Id',Id);
							qty=parseFloat(qty)-parseFloat(PickGenQty);
						}
					}
					/***Upto here ***/
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in updateFulfillmentOrderByQtyEntered',exp);
	}
}

/**
 * Creating fullfillment order when the create_fulfillment_order is checked
 * @param form
 * @returns
 */
function createFulfillmentOrderBulk(lineno,variance,fullFillmentorderno,searchresults,foparent,
		fulfillmentOrdersAgainstLocationAlias,getinstdata,getlineinstdata)
{
	var salesOrderNo = nlapiGetFieldValue('tranid');	
	var salesOrderInternalId = nlapiGetRecordId();	
	var salesOrderDate = nlapiGetFieldValue('trandate');
	var customer = nlapiGetFieldValue('entity');
	var priority = nlapiGetFieldValue('custbody_nswmspriority');
	var orderType = nlapiGetFieldValue('custbody_nswmssoordertype');
	var shipCarrierMethod = nlapiGetFieldValue('shipmethod');
	var wmsCarrier = nlapiGetFieldValue('custbody_salesorder_carrier');
	var wmsCarrierLineLevel = searchresults.getLineItemValue('item','custcol_salesorder_carrier_linelevel',lineno);
	var shipmethodLineLevel = searchresults.getLineItemValue('item','shipmethod',lineno);
	var company = nlapiGetFieldValue('custbody_nswms_company');
	var route = nlapiGetFieldValue('custbody_nswmssoroute');
	var location=nlapiGetFieldValue('location');
	var shipcomplete = nlapiGetFieldValue('shipcomplete');
	var locationname=nlapiGetFieldText('location');
	var itemlevelshipping=nlapiGetFieldValue('ismultishipto');
	var vShipDate = nlapiGetFieldValue('shipdate');				
	if(vShipDate==null||vShipDate==""||vShipDate=='undefined')
		vShipDate = nlapiGetFieldValue('custbody_nswmspoexpshipdate');

	nlapiLogExecution('DEBUG','customer',customer);

	if(customer==null || customer=='')
	{
		customer = searchresults.getFieldValue('entity');
		priority = searchresults.getFieldValue('custbody_nswmspriority');
		orderType = searchresults.getFieldValue('custbody_nswmssoordertype');		
		salesOrderDate = searchresults.getFieldValue('trandate');	
		shipCarrierMethod = searchresults.getFieldValue('shipmethod');
	}	

	var lineordsplit = fullFillmentorderno.split('.');
	var backordline = lineordsplit[1];
	var customizationtype = searchresults.getLineItemValue('item','custcol_customizationcode',lineno);
	var linelocation=searchresults.getLineItemValue('item','location',lineno);
	var linelocationname=searchresults.getLineItemText('item','location',lineno);
	if(linelocation!=null && linelocation!='')
	{
		location=linelocation;
		locationname = linelocationname;
	}
	nlapiLogExecution('DEBUG','location',location);
	var actuallocation=null;
	if(location!=null && location!='' && location!='null')
	{
		actuallocation=CheckLocationAlias(location);
	}

	nlapiLogExecution('DEBUG','actuallocation',actuallocation);

	var vshipmethod = shipCarrierMethod;
	if(shipmethodLineLevel!=null && shipmethodLineLevel!='')
		vshipmethod = shipmethodLineLevel;

	nlapiLogExecution('DEBUG','vshipmethod',vshipmethod);

	//The below code is commented by Satish.N on 14NOV2013
	/*
	nlapiLogExecution('DEBUG','lineordsplit[1]',lineordsplit[1]);
	var actuallocation=null;
	var LocationAliasFOnumber='';
	if(location!=null && location!='' && location!='null')
	{
		actuallocation=CheckLocationAlias(location);
		//case 20124822 start : for channel allocation 
		nlapiLogExecution('DEBUG','into location alias',lineordsplit[1]);
		var nextvalue = parseFloat(lineordsplit[1])+parseFloat(1);
		LocationAliasFOnumber=lineordsplit[0]+'.'+nextvalue;
		//end of case 20124822
	}

	nlapiLogExecution('DEBUG','location',actuallocation);
	nlapiLogExecution('DEBUG','LocationAliasFOnumber',LocationAliasFOnumber);
	 */
	var salesOrderLineNo =searchresults.getLineItemValue('item','line',lineno);	
	var itemId =searchresults.getLineItemValue('item','item',lineno);
	var Shipment = searchresults.getLineItemValue('item','custcol_nswms_shipment_no',lineno);
	if(Shipment==null || Shipment=='')
		Shipment=searchresults.getFieldValue('custbody_shipment_no');
	var itemStatus =searchresults.getLineItemValue('item','custcol_ebiznet_item_status',lineno);
	var itemPackCode =searchresults.getLineItemValue('item','custcol_nswmspackcode',lineno);
	var defpackcode = '';
	//var shipattention = searchresults.getLineItemValue('item','shipaddress_display',lineno);
	var shipattention = searchresults.getLineItemValue('item','shipaddress',lineno);	
	//code added for getting inventory detail from order and updating into FO
	var vinvtdetaillot="";
	var vinvtdetailqty="";
	var BatchID="";
	//searchresults.selectLineItem('item',salesOrderLineNo);
	searchresults.selectLineItem('item',lineno);
	// case# 201412227
	var vAdvBinManagement = '';
	var ctx = nlapiGetContext();
	if(ctx != null && ctx != '')
	{
		if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
			vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
	}

	nlapiLogExecution('DEBUG', 'vAdvBinManagement', vAdvBinManagement);

	if(vAdvBinManagement==true)
	{
		var invDetailSubrecord = searchresults.viewCurrentLineItemSubrecord('item', 'inventorydetail');
		if (invDetailSubrecord!="" && invDetailSubrecord!=null)
		{
			invDetailSubrecord.selectLineItem('inventoryassignment', 1);
			//var soAssignedlot=invDetailSubrecord.getCurrentLineItemValue('inventoryassignment','issueinventorynumber');
			vinvtdetaillot=invDetailSubrecord.getCurrentLineItemText('inventoryassignment','issueinventorynumber');
			vinvtdetailqty=invDetailSubrecord.getCurrentLineItemValue('inventoryassignment','quantity');
		}
		if(vinvtdetaillot!=null && vinvtdetaillot!='')
		{
			var filter=new Array();
			if(itemId!=null&&itemId!="")
				filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemId));
			filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',vinvtdetaillot));
			var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filter,null);
			if(rec!=null&&rec!="")
			{
				BatchID=rec[0].getId();

			}
		}
	}
	var str = 'salesOrderNo. = ' + salesOrderNo + '<br>';
	str = str + 'salesOrderInternalId. = ' + salesOrderInternalId + '<br>';	
	str = str + 'wmsCarrierLineLevel. = ' + wmsCarrierLineLevel + '<br>';
	str = str + 'wmsCarrier. = ' + wmsCarrier + '<br>';
	str = str + 'lineno. = ' + lineno + '<br>';
	str = str + 'itemPackCode. = ' + itemPackCode + '<br>';
	str = str + 'itemStatus. = ' + itemStatus + '<br>';
	str = str + 'Shipment. = ' + Shipment + '<br>';
	str = str + 'locationname. = ' + locationname + '<br>';
	str = str + 'vShipDate. = ' + vShipDate + '<br>';
	str = str + 'shipCarrierMethod. = ' + shipCarrierMethod + '<br>';
	str = str + 'shipmethodLineLevel. = ' + shipmethodLineLevel + '<br>';
	str = str + 'salesOrderDate. = ' + salesOrderDate + '<br>';
	str = str + 'fullFillmentorderno. = ' + fullFillmentorderno + '<br>';
	str = str + 'shipattention. = ' + shipattention + '<br>';
	//The below code is commented by Satish.N on 14NOV2013
//	str = str + 'LocationAliasFOnumber. = ' + LocationAliasFOnumber + '<br>';
	str = str + 'customer. = ' + customer + '<br>';
	str = str + 'priority. = ' + priority + '<br>';
	str = str + 'orderType. = ' + orderType + '<br>';
	str = str + 'vinvtdetaillot. = ' + vinvtdetaillot + '<br>';
	str = str + 'vinvtdetailqty. = ' + vinvtdetailqty + '<br>';
	str = str + 'BatchID. = ' + BatchID + '<br>';
	nlapiLogExecution('DEBUG', 'Input Parameters', str);

	var statusbyloc='';


	// Sales Order Item Uom
	var baseUom = searchresults.getLineItemValue('item','custcol_nswmssobaseuom',lineno);
	//var vNSUOM = searchresults.getLineItemText('item','units', lineno);
	var vNSUOM = searchresults.getLineItemValue('item','units', lineno);
	nlapiLogExecution('DEBUG', 'Order UOM', vNSUOM);

	var veBizUOMQty=1;
	var vBaseUOMQty=1;
	var vUOMId='';

	if(vNSUOM!=null && vNSUOM!="")
	{
		var eBizItemDims=geteBizItemDimensions(itemId);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				nlapiLogExecution('DEBUG', 'Base UOM Flag', eBizItemDims[z].getValue('custrecord_ebizbaseuom'));
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					vBaseUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					nlapiLogExecution('DEBUG', 'vBaseUOMQty', vBaseUOMQty);
				}

				nlapiLogExecution('DEBUG', 'SKU Dim UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
				//if(vNSUOM == eBizItemDims[z].getText('custrecord_ebiznsuom'))
				if(vNSUOM == eBizItemDims[z].getValue('custrecord_ebiznsuom'))
				{					
					veBizUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					nlapiLogExecution('DEBUG', 'veBizUOMQty', veBizUOMQty);
					vUOMId = eBizItemDims[z].getValue('custrecord_ebizuomskudim');
					nlapiLogExecution('DEBUG', 'vUOMId', vUOMId);
				}
			}
			nlapiLogExecution('DEBUG', 'veBizUOMQty', veBizUOMQty);
			nlapiLogExecution('DEBUG', 'variance', variance);
			nlapiLogExecution('DEBUG', 'vBaseUOMQty', vBaseUOMQty);
			if(vBaseUOMQty==0)
				vBaseUOMQty=1;

			if(veBizUOMQty!=0)
				variance = parseFloat(variance)*parseFloat(veBizUOMQty)/parseFloat(vBaseUOMQty);
			nlapiLogExecution('DEBUG', 'vordqty', variance);
		}
	}

	var itemSubtype = nlapiLookupField('item', itemId, 'recordType');
	var fields = ['custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var columnsval = nlapiLookupField(itemSubtype, itemId, fields,true);
	var skuinfo1val = columnsval.custitem_item_info_1;
	var skuinfo2val = columnsval.custitem_item_info_2;
	var skuinfo3val = columnsval.custitem_item_info_3;


	foparent.selectNewLineItem('recmachcustrecord_ebiz_fo_parent');

	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','name', salesOrderInternalId);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ns_ord', parseInt(salesOrderInternalId));
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline', parseInt(salesOrderLineNo));
	if(priority!=null && priority!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_priority', priority);
	if(orderType!=null && orderType!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_type', orderType);
	if(customer!=null && customer!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_customer', customer);
	if(shipmethodLineLevel!=null && shipmethodLineLevel!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_carrier', shipmethodLineLevel);
	else if(shipCarrierMethod!=null && shipCarrierMethod!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_carrier', shipCarrierMethod);
	if(wmsCarrierLineLevel != null && wmsCarrierLineLevel != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_wmscarrier', wmsCarrierLineLevel);
//	else if (wmsCarrier!=null && wmsCarrier!='')
//	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_wmscarrier', wmsCarrier);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag', 25);
	if(route != null && route != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_route_no', route);

	//The below code is commented by Satish.N on 14NOV2013
	//foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', fullFillmentorderno);
	if(company != null && company != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_company', company);
	if(vShipDate!='undefined')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_shipdate', vShipDate);
	if(salesOrderDate!='undefined')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_orderdate', salesOrderDate);

	//case # 20124380 Start
	if(salesOrderDate!='undefined')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_record_linedate', DateStamp());
	//case # 20124380 End

	if(customizationtype!=null && customizationtype!="" && customizationtype!='undefined')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_ordline_customizetype', customizationtype);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_actuallocation', location);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', fullFillmentorderno);
	if(actuallocation!=null)
	{
		var currRow=[actuallocation,fullFillmentorderno,vshipmethod,shipattention];
		fulfillmentOrdersAgainstLocationAlias.push(currRow);
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_wms_location', actuallocation);

		//The below code is commented by Satish.N on 14NOV2013
		//foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', LocationAliasFOnumber);
	}
	else
	{

		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_wms_location', location);
		//The below code is commented by Satish.N on 14NOV2013
		//foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', fullFillmentorderno);
	}
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipcomplete', shipcomplete);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_backorderno', backordline);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline', salesOrderLineNo);
	if(itemId != null && itemId != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_linesku', itemId);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipment_no', Shipment);

	//*********************************************************************************************************//

	var statusbyloc='';

	// Sales Order Item Status
	var itemStatus =searchresults.getLineItemValue('item','custcol_ebiznet_item_status',lineno);
	nlapiLogExecution('DEBUG','itemStatus',itemStatus);		

	if( (itemStatus==null || itemStatus=='') || (itemPackCode==null || itemPackCode==''))
	{

		var fields = ['custitem_ebizoutbounddefskustatus','custitem_ebizdefpackcode'];
		var columns= nlapiLookupField('item',itemId,fields);
		if(columns!=null)
		{
			if(itemStatus==null || itemStatus=='')
				itemStatus = columns.custitem_ebizoutbounddefskustatus;
			defpackcode = columns.custitem_ebizdefpackcode;
		}

		if(itemStatus==null || itemStatus=='')
		{
			if(actuallocation!=null && actuallocation!='')		
				itemStatus = getDefaultSKUStatus(actuallocation);
			else
				itemStatus = getDefaultSKUStatus(location);
		}
	}

	if(itemPackCode==null || itemPackCode=='')
	{
		itemPackCode=defpackcode;
	}

	nlapiLogExecution('DEBUG','itemPackCode',itemPackCode);
	nlapiLogExecution('DEBUG','itemStatus',itemStatus);


	//*********************************************************************************************************//


	if(itemStatus != null && itemStatus != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linesku_status', itemStatus);
	if(itemPackCode != null && itemPackCode != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linepackcode', itemPackCode);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ord_qty', parseFloat(variance));
	if(vUOMId != null && vUOMId != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineuom_id', vUOMId);
	if(skuinfo1val != null && skuinfo1val != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo1', skuinfo1val);
	if(skuinfo2val != null && skuinfo2val != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo2', skuinfo2val);
	if(skuinfo3val != null && skuinfo3val != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo3', skuinfo3val);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linenotes2', 'Created by User Event');

	if(shipattention!=null && shipattention!='')
	{
		var shipattentiontext = searchresults.getLineItemText('item','shipaddress',lineno);	
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_ship_attention', shipattentiontext);
	}
	if(vinvtdetaillot != null && vinvtdetaillot != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_lot_number', vinvtdetaillot);
	if(vinvtdetailqty != null && vinvtdetailqty != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_lotnumber_qty', vinvtdetailqty);
	if(BatchID != null && BatchID != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_lotno_internalid', BatchID);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_wms_soinstructions', getinstdata);

	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_wms_soiteminstructions', getlineinstdata);

	foparent.commitLineItem('recmachcustrecord_ebiz_fo_parent');

}

function backOrderFulfillment(type)
{
	nlapiLogExecution('DEBUG','Into  backOrderFulfillment','');
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());

	var vConfig=nlapiLoadConfiguration('companyfeatures');
	var UOMruleValue=vConfig.getFieldValue('unitsofmeasure');
	nlapiLogExecution('ERROR','UOMruleValue',UOMruleValue);

	//if ( type != 'scheduled') return; 

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
	filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0

	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
	//filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
	filters.push(new nlobjSearchFilter('custbody_ebiz_ord_size', null, 'isnot', 'L')); // To process less than 100 line orders only

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');	
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
	columns[3] = new nlobjSearchColumn('quantitycommitted');
	columns[4] = new nlobjSearchColumn('quantity');
	columns[5] = new nlobjSearchColumn('trandate');
	columns[6] = new nlobjSearchColumn('entity');
	columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
	columns[8] = new nlobjSearchColumn('shipmethod');
	columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
	columns[10] = new nlobjSearchColumn('custbody_nswms_company');
	columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
	columns[12] = new nlobjSearchColumn('location');
	columns[13] = new nlobjSearchColumn('item');
	columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
	columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
	columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');

	columns[17] = new nlobjSearchColumn('tranid');
	columns[18] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
	columns[19] = new nlobjSearchColumn('quantityshiprecv');
	columns[20] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
	columns[21] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
	columns[22] = new nlobjSearchColumn('custcol_customizationcode');
	columns[23] = new nlobjSearchColumn('custbody_nswmspoexpshipdate');
	columns[24] = new nlobjSearchColumn('custbody_nswmssoordertype');
	columns[25] = new nlobjSearchColumn('custbody_shipment_no');
	columns[26] = new nlobjSearchColumn('shipdate');
	columns[27] = new nlobjSearchColumn('custcol_nswms_shipment_no');
	//columns[28] = new nlobjSearchColumn('quantityuom');// case# 201415196
	columns[28] = new nlobjSearchColumn('custcol_create_fulfillment_order');
	columns[29] = new nlobjSearchColumn('linesequencenumber');
	if(UOMruleValue == 'T')
	{
		columns[30] = new nlobjSearchColumn('unit');
		columns[31] = new nlobjSearchColumn('quantityuom');

	}

	//true - Discending false - Ascending
	columns[0].setSort(true);
	columns[1].setSort();

	var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);
	nlapiLogExecution('DEBUG','Remaining Usage 2',context.getRemainingUsage());
	if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
	{	
		var createfo = 'T'; //Added 08-Nov-2011
		var sonumber = 0; //Added 08-Nov-2011
		var oldsoname='';
		var fullFillmentorderno='';
		var fonumberarr = new Array();
		var createfo = 'T'; //Added 08-Nov-2011
		var vcustomer = '';
		nlapiLogExecution('ERROR','salesOrderLineDetails.length ',salesOrderLineDetails.length);
		//Begin To check the number of order lines if length is greate than 1000 
		var vLastOrderSO;
		if(salesOrderLineDetails.length>=1000)
		{	
			vLastOrderSO=salesOrderLineDetails[salesOrderLineDetails.length-1].getValue('tranid');
			nlapiLogExecution('ERROR','vLastOrderSO ',vLastOrderSO);
		}
		//End

		nlapiLogExecution('DEBUG','salesOrderLineDetails.length ',salesOrderLineDetails.length);
		for(var i=0;i<salesOrderLineDetails.length;i++)
		{
			nlapiLogExecution('DEBUG','# Loop',i);
			vinternalid=salesOrderLineDetails[i].getValue('internalid');
			nlapiLogExecution('DEBUG','vinternalid',vinternalid);

			var soname = salesOrderLineDetails[i].getValue('tranid');
			//Begin To check the number of order lines if length is greate than 1000 
			//Here if order changed then getting current order line items count and if all the order lines are not in the search results list then breaking the scheulder
			vProceedFO='T';
			if(vLastOrderSO == soname && i != 0 && salesOrderLineDetails[i].getValue('tranid') != salesOrderLineDetails[i-1].getValue('tranid') )
			{
				var vOrdDets=getOrdLineCount(vinternalid);
				//var vOrdDets=nlapiLoadRecord('salesorder',vinternalid);
				if(vOrdDets != null && vOrdDets != '')
				{
					var SOLastOrdCount = vOrdDets[0].getValue('internalid',null,'count');
					nlapiLogExecution('DEBUG','SOLastOrdCount ',SOLastOrdCount);
					nlapiLogExecution('DEBUG','SOLastOrdCount ',SOLastOrdCount);
					nlapiLogExecution('DEBUG','salesOrderLineDetails.length-1 ',parseInt(salesOrderLineDetails.length)-1);
					if((parseInt(salesOrderLineDetails.length)-i) != SOLastOrdCount)					 
					{
						vProceedFO='F';
						nlapiLogExecution('DEBUG','vProceedFO ',vProceedFO);
						break;
					}	

				}	
			}
			//End
			var solocation = salesOrderLineDetails[i].getValue('location');
			var orderqty = salesOrderLineDetails[i].getValue('quantity');
			var create_fulfillment_order= salesOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
			var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
			var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
			var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
			var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');
			var customizationcode = salesOrderLineDetails[i].getValue('custcol_customizationcode');
			var uomconvqty = '';
			var unit = '';
			if(UOMruleValue == 'T')
			{
				unit = salesOrderLineDetails[i].getValue('unit');
				uomconvqty = salesOrderLineDetails[i].getValue('quantityuom');
			}

			/***Below code is merged from Lexjet Production by Ganesh K on 04th Mar 2013***/
			var solineshipmethod=salesOrderLineDetails[i].getValue('shipmethod');
			var solineCreateFulfillFlag = salesOrderLineDetails[i].getValue('custcol_create_fulfillment_order');
			var lineseqno = salesOrderLineDetails[i].getValue('linesequencenumber');
			/***Upto here ***/
			var conversionrate = 1;
			if(unit!=null && unit!='' && uomconvqty!=null && uomconvqty!='')
			{
				conversionrate = parseFloat(orderqty)/parseFloat(uomconvqty);
			}


			var str = 'SO Name. = ' + soname + '<br>';
			str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
			str = str + 'SO Location. = ' + solocation + '<br>';
			str = str + 'Customization Code. = ' + customizationcode + '<br>';
			str = str + 'Order Qty. = ' + orderqty + '<br>';
			str = str + 'Unit. = ' + unit + '<br>';
			str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
			str = str + 'UOM Conversion Rate. = ' + conversionrate + '<br>';
			str = str + 'Commetted Qty. = ' + commetedQty + '<br>';			
			str = str + 'Shipped Qty. = ' + shippedqty + '<br>';
			str = str + 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
			str = str + 'shpcompleteflag. = ' + shpcompleteflag + '<br>';
			str = str + 'solineCreateFulfillFlag. = ' + solineCreateFulfillFlag + '<br>';

			nlapiLogExecution('DEBUG', 'SO Line Details1', str);		

			if(customizationcode==null || customizationcode=='')
				customizationcode=-1;

			if(uomconvqty==null || uomconvqty=='' || isNaN(uomconvqty))
				uomconvqty=1;

			if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
				commetedQty=0;

			if(shippedqty==null || shippedqty=='' || isNaN(shippedqty))
				shippedqty=0;

			if(unit!=null && unit!='')
			{
				commetedQty = parseFloat(commetedQty)/parseFloat(conversionrate);
				shippedqty = parseFloat(shippedqty)/parseFloat(conversionrate);
			}

			// Assuming search results will return the data in the descending order of SO#
			var mwhsiteflag='F';
			if(soname != null)
			{
				var	islocationAliasExists=CheckLocationAlias(solocation);
				if(islocationAliasExists!=null && islocationAliasExists!='')
				{
					mwhsiteflag='T';
				}
				else
				{
					if(solocation!=null && solocation!='')
					{
						var fields = ['custrecord_ebizwhsite'];

						var locationcolumns = nlapiLookupField('Location', solocation, fields);
						if(locationcolumns!=null)
							mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
					}
				}


				if(sonumber != soname)
				{
					sonumber = soname;

					try
					{
						vcustomer = nlapiLookupField('salesorder', vinternalid, 'entity');	
						createfo = fnCreateFo(vinternalid);
					}
					catch(exp)
					{
						nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
						createfo='T';
					}

					nlapiLogExecution('DEBUG','createfo',createfo);

					if(shpcompleteflag != null && shpcompleteflag == 'T'  && createfo=='T')
					{
						try
						{
							createfo = fnShipcompleteFo(vinternalid,salesOrderLineDetails,sonumber);
						}
						catch(exp)
						{
							nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
							createfo='T';
						}

						nlapiLogExecution('DEBUG','Ship Complete Create FO',createfo);
					}							
				}				
			}			

			if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T' && mwhsiteflag=='T' && solineCreateFulfillFlag=='T')
			{
				try
				{
					if(soname != null && soname != '')
					{	
						//To lock the sales order
						nlapiLogExecution('DEBUG','soname before Locking',soname);
						nlapiLogExecution('DEBUG','trantype before Locking','salesorder');
						var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
						fulfillRec.setFieldValue('name', soname);					
						fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'salesorder');
						fulfillRec.setFieldValue('externalid', soname);
						fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by Scheduler');
						nlapiSubmitRecord(fulfillRec,false,true );
						nlapiLogExecution('DEBUG','Locked successfully');
					}
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
					nlapiLogExecution('DEBUG', 'DEBUG', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.');
					//var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
					//throw wmsE;
					continue;
				}
				try
				{
					var variance=0;
					//DeleteRecCreated(salesOrderLineDetails[i].getId(), salesOrderLineNo);
					//var totalQuantity = getFulfilmentqtyNew(salesOrderLineDetails[i].getId(), salesOrderLineNo);
					var totalQuantity = getFulfilmentqty(salesOrderLineDetails[i].getId(), salesOrderLineNo);
					if(totalQuantity != null && totalQuantity != '' && totalQuantity >0 && conversionrate != null && conversionrate !='' && conversionrate>0)
						totalQuantity= parseFloat(totalQuantity)/ parseFloat(conversionrate);
					nlapiLogExecution('DEBUG','Remaining Usage 3',context.getRemainingUsage());
					var str = 'SO Name. = ' + soname + '<br>';
					str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
					str = str + 'SO Location. = ' + solocation + '<br>';
					str = str + 'Customization Code. = ' + customizationcode + '<br>';
					str = str + 'Order Qty. = ' + orderqty + '<br>';
					str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
					str = str + 'Commetted Qty. = ' + commetedQty + '<br>';
					str = str + 'Created FO Qty. = ' + totalQuantity + '<br>';
					str = str + 'Shipped Qty. = ' + shippedqty + '<br>';

					nlapiLogExecution('DEBUG', 'SO Line Details', str);		
					var totalcomtdqty=parseFloat(commetedQty)+parseFloat(shippedqty);
					//totalcomtdqty= parseFloat(totalcomtdqty)* parseFloat(conversionrate);
					if(totalQuantity == 0)
						variance=commetedQty;
					else
					{
						if(parseFloat(totalQuantity) < parseFloat(totalcomtdqty))
						{
							variance=parseFloat(totalcomtdqty)-parseFloat(totalQuantity);
						}
					}

					nlapiLogExecution('DEBUG','Variance1',variance);

					if(variance > 0)
					{
						//-------------------------------------------------Change by Ganesh on 16th Jan 2013
						if((parseFloat(variance)+ parseFloat(shippedqty) + parseFloat(totalQuantity))>parseFloat(totalcomtdqty))
						{
							var totalInTransitFOQuantity = getFulfilmentqtyInProgNew(salesOrderLineDetails[i].getId(), salesOrderLineNo);
							nlapiLogExecution('DEBUG','totalInTransitFOQuantity',totalInTransitFOQuantity);
							variance = parseFloat(totalcomtdqty) - parseFloat(shippedqty) - parseFloat(totalInTransitFOQuantity);
							//variance = parseFloat(totalcomtdqty) - parseFloat(shippedqty);

						}
						//-----------------------------------------------------------------------
					}	/*//Temporary Fix
				if((parseFloat(variance)+ parseFloat(shippedqty))>parseFloat(totalcomtdqty))
				{
					variance = parseFloat(variance) - (parseFloat(totalcomtdqty)-parseFloat(variance));
					//New scenario
					variance = parseFloat(variance) - parseFloat(shippedqty);
				}*/
					//

					nlapiLogExecution('DEBUG','Variance2',variance);

					if(variance > 0)
					{
						if(oldsoname!=soname)
						{
							//generate new fullfillment order no.
							//fullFillmentorderno=fulfillmentOrderDetails(salesOrderLineDetails[i].getId(),soname);
							/***Below code is merged from Lexjet production by Ganesh on 4th Mar 2013***/
							fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod);
							oldsoname=soname;
							var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod];

							fonumberarr.push(curRow);
						}
						else
						{
							if(fonumberarr!=null && fonumberarr!='' && fonumberarr.length>0)
							{
								var fonumberfound = 'F';
								for(var t=0;t<fonumberarr.length;t++)
								{
									if(fonumberarr[t][0]==soname && fonumberarr[t][2]==customizationcode && fonumberarr[t][3]==solocation && fonumberarr[t][4]==solineshipmethod )
									{
										fullFillmentorderno=fonumberarr[t][1];
										fonumberfound='T';
									}
								}
								if(fonumberfound=='F')
								{
									//fullFillmentorderno=fulfillmentOrderDetails(salesOrderLineDetails[i].getId(),soname);
									fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod);
									var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod];
									fonumberarr.push(curRow);
									/***upto here ***/
								}
							}
						}
						nlapiLogExecution('DEBUG','Remaining Usage 4',context.getRemainingUsage());
						nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);

//						if(salesOrderLineNo != null && fullFillmentorderno!= null && fullFillmentorderno != '')
//						{	
//						var DeletedOrdQty=DeleteRecCreatedNew(salesOrderLineDetails[i].getId(), salesOrderLineNo,fullFillmentorderno);
//						if(DeletedOrdQty != null && DeletedOrdQty != '' && parseFloat(DeletedOrdQty)!=0)
//						variance=parseFloat(DeletedOrdQty)+parseFloat(variance);
//						nlapiLogExecution('DEBUG','variance New',variance);
//						}	
//						if(fullFillmentorderno == null || fullFillmentorderno == '')
//						{	
//						fullFillmentorderno=fulfillmentOrderDetails(salesOrderLineDetails[i].getId(),soname);
//						}
						nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);	
						nlapiLogExecution('DEBUG','Remaining Usage 4',context.getRemainingUsage());	

						if(fullFillmentorderno!=null && fullFillmentorderno!='')
						{
							var fulillmentarray = getFullFillmentbyline(salesOrderLineDetails[i].getId(),salesOrderLineNo);
							if(fulillmentarray!=null && fulillmentarray!='' && fulillmentarray.length>0)
							{
								nlapiLogExecution('DEBUG','fulillmentarray length',fulillmentarray.length);	

								var existordqty = fulillmentarray[0].getValue('custrecord_ord_qty');
								var fointernalid = fulillmentarray[0].getId();
								var totalordqty = parseFloat(existordqty)+parseFloat(variance);

								nlapiLogExecution('DEBUG','existordqty',existordqty);	
								nlapiLogExecution('DEBUG','variance',variance);
								nlapiLogExecution('DEBUG','totalordqty',totalordqty);

								nlapiLogExecution('DEBUG','Updating Fulfillment Order Line...');
								nlapiSubmitField('customrecord_ebiznet_ordline', fointernalid, 'custrecord_ord_qty', totalordqty);
							}
							else
							{
								createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,salesOrderLineDetails[i],soname,salesOrderLineDetails[i].getId(),vcustomer,lineseqno);
							}					

						}
						nlapiLogExecution('DEBUG','Remaining Usage 5',context.getRemainingUsage());							
					}
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG','exception in FO scheduler',e.message);
				}
				finally
				{
					//To unlock the sales order
					nlapiLogExecution('DEBUG','soname before unlocking',soname);
					nlapiLogExecution('DEBUG','trantype before unlocking','salesorder');

					if(soname != null && soname != '')
					{	
						var ExternalIdFilters = new Array();
						ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
						ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'salesorder'));
						var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
						if(searchresultsExt != null && searchresultsExt != '')
						{
							nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
							nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
							nlapiLogExecution('DEBUG','Unlocked successfully');
						}	
					}
				}
				//Begin
				//if order changed then Checking the number lines of current order and mulitpliying with 50 + buffer units 500, then we are breaking the scheduler
				if((i+1 != salesOrderLineDetails.length) && salesOrderLineDetails[i].getValue('tranid') != salesOrderLineDetails[i+1].getValue('tranid') )
				{
					var vOrdLineCount=getOrdLineCountFrmForLoop(salesOrderLineDetails,i+1);			 
					if(vOrdLineCount != null && vOrdLineCount != '')
					{

						nlapiLogExecution('ERROR','vOrdLineCount ',vOrdLineCount);
						var vAppxUnitsPerLine=50;
						nlapiLogExecution('ERROR','context.getRemainingUsage() ',context.getRemainingUsage());
						if(parseInt((parseInt(vOrdLineCount)*parseInt(vAppxUnitsPerLine)))+500 >=context.getRemainingUsage())					 
						{
							nlapiLogExecution('ERROR','vinternalid',vinternalid);
							updateSeqNo('FO',vinternalid);
							nlapiLogExecution('ERROR','break');
							vtempflag=true;
							break;
						}	

					}	
				}
				/*//Update seq no in wave table if the remaining use is less than 500
			if(context.getRemainingUsage()<500)
			{
				break;
			}*/
			}
			nlapiLogExecution('DEBUG','salesOrderLineDetails.length2 ',salesOrderLineDetails.length);

		}

		nlapiLogExecution('DEBUG','Remaining Usage at the end ',context.getRemainingUsage());	
		nlapiLogExecution('DEBUG','Out of  backOrderFulfillment','');

	}
}

function backOrderFulfillmentnew(type)
{
	nlapiLogExecution('DEBUG','Into  backOrderFulfillmentnew','');
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());

	var vConfig=nlapiLoadConfiguration('companyfeatures');
	var UOMruleValue=vConfig.getFieldValue('unitsofmeasure');

	nlapiLogExecution('DEBUG','UOMruleValue',UOMruleValue);

	if ( type != 'scheduled') return; 
	var vintrid=0;
	var vinternalid=0;

	//get the seq no from wave record.
	vintrid = GetMaxTransactionNo('FO');
	nlapiLogExecution('DEBUG','internalid in search filter',vintrid);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
	filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0

	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
	//filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
	filters.push(new nlobjSearchFilter('custbody_ebiz_ord_size', null, 'isnot', 'L')); // To process less than 100 line orders only
	if(parseFloat(vintrid)>1)
	{
		nlapiLogExecution('DEBUG','In side internalid in search filter',vintrid);
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'lessthanorequalto', vintrid));
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');	
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
	columns[3] = new nlobjSearchColumn('quantitycommitted');
	columns[4] = new nlobjSearchColumn('quantity');
	columns[5] = new nlobjSearchColumn('trandate');
	columns[6] = new nlobjSearchColumn('entity');
	columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
	columns[8] = new nlobjSearchColumn('shipmethod');
	columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
	columns[10] = new nlobjSearchColumn('custbody_nswms_company');
	columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
	columns[12] = new nlobjSearchColumn('location');
	columns[13] = new nlobjSearchColumn('item');
	columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
	columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
	columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');	
	columns[17] = new nlobjSearchColumn('tranid');
	columns[18] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
	columns[19] = new nlobjSearchColumn('quantityshiprecv');
	columns[20] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
	columns[21] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
	columns[22] = new nlobjSearchColumn('custcol_customizationcode');
	columns[23] = new nlobjSearchColumn('custbody_nswmspoexpshipdate');
	columns[24] = new nlobjSearchColumn('custbody_nswmssoordertype');
	columns[25] = new nlobjSearchColumn('custbody_shipment_no');
	columns[26] = new nlobjSearchColumn('shipdate');
	columns[27] = new nlobjSearchColumn('custcol_nswms_shipment_no');
	//columns[28] = new nlobjSearchColumn('quantityuom');
	columns[28] = new nlobjSearchColumn('shipattention');
	columns[29] = new nlobjSearchColumn('custcol_create_fulfillment_order');
	columns[30] = new nlobjSearchColumn('linesequencenumber');
	if(UOMruleValue == 'T')
	{
		columns[31] = new nlobjSearchColumn('unit');
		columns[32] = new nlobjSearchColumn('quantityuom');
	}


	//true - Discending false - Ascending
	columns[0].setSort(true);
	columns[1].setSort();

	var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);
	nlapiLogExecution('DEBUG','Remaining Usage 2',context.getRemainingUsage());
	if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
	{	

		var sonumber = 0; //Added 08-Nov-2011
		var oldsoname='';
		var fullFillmentorderno='';
		var fonumberarr = new Array();
		var vtempflag=false;
		var createfo = 'T'; //Added 08-Nov-2011
		var vcustomer = '';
		nlapiLogExecution('ERROR','salesOrderLineDetails.length ',salesOrderLineDetails.length);
		//Begin To check the number of order lines if length is greate than 1000 
		var vLastOrderSO;
		if(salesOrderLineDetails.length>=1000)
		{	
			vLastOrderSO=salesOrderLineDetails[salesOrderLineDetails.length-1].getValue('tranid');
			nlapiLogExecution('ERROR','vLastOrderSO ',vLastOrderSO);
		}
		//End

		nlapiLogExecution('DEBUG','salesOrderLineDetails.length ',salesOrderLineDetails.length);
		for(var i=0;i<salesOrderLineDetails.length;i++)
		{			
			var mwhsiteflag='F';
			vinternalid=salesOrderLineDetails[i].getValue('internalid');
			var soname = salesOrderLineDetails[i].getValue('tranid');	
			//Begin To check the number of order lines if length is greate than 1000 
			//Here if order changed then getting current order line items count and if all the order lines are not in the search results list then breaking the scheulder
			vProceedFO='T';
			if(vLastOrderSO == soname && i != 0 && salesOrderLineDetails[i].getValue('tranid') != salesOrderLineDetails[i-1].getValue('tranid') )
			{
				var vOrdDets=getOrdLineCount(vinternalid);
				//var vOrdDets=nlapiLoadRecord('salesorder',vinternalid);
				if(vOrdDets != null && vOrdDets != '')
				{
					nlapiLogExecution('ERROR','vLastOrderSO ',vLastOrderSO);
					var SOLastOrdCount = vOrdDets[0].getValue('internalid',null,'count');
					nlapiLogExecution('ERROR','SOLastOrdCount ',SOLastOrdCount);
					nlapiLogExecution('ERROR','salesOrderLineDetails.length-1 ',parseInt(salesOrderLineDetails.length)-1);
					if((parseInt(salesOrderLineDetails.length)-i) != SOLastOrdCount)
					{
						vProceedFO='F';
						nlapiLogExecution('ERROR','vProceedFO ',vProceedFO);
						break;
					}	

				}	
			}
			//End
			var solocation = salesOrderLineDetails[i].getValue('location');
			var orderqty = salesOrderLineDetails[i].getValue('quantity');
			var create_fulfillment_order= salesOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
			var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
			var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
			var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
			var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');
			var customizationcode = salesOrderLineDetails[i].getValue('custcol_customizationcode');
			var uomconvqty = '';
			var unit = '';
			if(UOMruleValue == 'T')
			{
				unit = salesOrderLineDetails[i].getValue('unit');
				uomconvqty = salesOrderLineDetails[i].getValue('quantityuom');
			}

			/***Below code is merged from Lexjet Production by Ganesh K on 4th Mar 2013***/
			var solineshipmethod = salesOrderLineDetails[i].getValue('shipmethod');
			/***upto here***/
			var solineshipto = salesOrderLineDetails[i].getValue('shipattention');
			var solineCreateFulfillFlag = salesOrderLineDetails[i].getValue('custcol_create_fulfillment_order');
			var lineseqno = salesOrderLineDetails[i].getValue('linesequencenumber');
			var conversionrate = 1;
			if(unit!=null && unit!=''&& uomconvqty!=null && uomconvqty!='')
			{
				conversionrate = parseFloat(orderqty)/parseFloat(uomconvqty);
			}


			var str = '# Loop. = ' + i + '<br>';
			str = str + 'vinternalid. = ' + vinternalid + '<br>';
			str = str + 'SO Name. = ' + soname + '<br>';
			str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
			str = str + 'SO Location. = ' + solocation + '<br>';
			str = str + 'Customization Code. = ' + customizationcode + '<br>';
			str = str + 'Order Qty. = ' + orderqty + '<br>';
			str = str + 'Unit. = ' + unit + '<br>';
			str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
			str = str + 'UOM Conversion Rate. = ' + conversionrate + '<br>';
			str = str + 'Commetted Qty. = ' + commetedQty + '<br>';			
			str = str + 'Shipped Qty. = ' + shippedqty + '<br>';
			str = str + 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
			str = str + 'shpcompleteflag. = ' + shpcompleteflag + '<br>';
			str = str + 'Ship Method. = ' + solineshipmethod + '<br>';
			str = str + 'Ship To. = ' + solineshipto + '<br>';
			str = str + 'solineCreateFulfillFlag. = ' + solineCreateFulfillFlag + '<br>';


			nlapiLogExecution('DEBUG', 'SO Line Details1', str);		

			if(customizationcode==null || customizationcode=='')
				customizationcode=-1;

			if(solineshipto==null || solineshipto=='')
				solineshipto=-1;

			if(uomconvqty==null || uomconvqty=='' || isNaN(uomconvqty))
				uomconvqty=1;

			if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
				commetedQty=0;

			if(shippedqty==null || shippedqty=='' || isNaN(shippedqty))
				shippedqty=0;

			if(unit!=null && unit!='')
			{
				commetedQty = parseFloat(commetedQty)/parseFloat(conversionrate);
				shippedqty = parseFloat(shippedqty)/parseFloat(conversionrate);
			}

			// Assuming search results will return the data in the descending order of SO#

			if(soname != null)
			{
				var	islocationAliasExists=CheckLocationAlias(solocation);
				if(islocationAliasExists!=null && islocationAliasExists!='')
				{
					mwhsiteflag='T';
				}
				else
				{
					if(solocation!=null && solocation!='')
					{
						var fields = ['custrecord_ebizwhsite'];

						var locationcolumns = nlapiLookupField('Location', solocation, fields);
						if(locationcolumns!=null)
							mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
					}
				}


				if(sonumber != soname)
				{
					sonumber = soname;

					try
					{
						vcustomer = nlapiLookupField('salesorder', vinternalid, 'entity');	
						createfo = fnCreateFo(vinternalid);
					}
					catch(exp)
					{
						nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
						createfo='T';
					}

					nlapiLogExecution('DEBUG','createfo',createfo);

					if(shpcompleteflag != null && shpcompleteflag == 'T'  && createfo=='T')
					{
						try
						{
							createfo = fnShipcompleteFo(vinternalid,salesOrderLineDetails,sonumber);
						}
						catch(exp)
						{
							nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
							createfo='T';
						}

						nlapiLogExecution('DEBUG','Ship Complete Create FO',createfo);
					}							
				}				
			}

			var str = 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
			str = str + 'createfo. = ' + createfo + '<br>';
			str = str + 'mwhsiteflag. = ' + mwhsiteflag + '<br>';

			nlapiLogExecution('DEBUG', 'Flag Details1', str);		

			if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T' && mwhsiteflag=='T' && solineCreateFulfillFlag=='T')
			{
				try
				{
					if(soname != null && soname != '')
					{	
						//To lock the sales order
						nlapiLogExecution('DEBUG','soname before Locking',soname);
						nlapiLogExecution('DEBUG','trantype before Locking','salesorder');
						var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
						fulfillRec.setFieldValue('name', soname);					
						fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'salesorder');
						fulfillRec.setFieldValue('externalid', soname);
						fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by Scheduler');
						nlapiSubmitRecord(fulfillRec,false,true );
						nlapiLogExecution('DEBUG','Locked successfully');
					}
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
					nlapiLogExecution('DEBUG', 'DEBUG', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.');
					//var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
					//throw wmsE;
					continue;
				}
				try
				{
					var variance=0;
					var totalQuantity = getFulfilmentqty(salesOrderLineDetails[i].getId(), salesOrderLineNo);
					if(totalQuantity != null && totalQuantity != '' && totalQuantity >0 && conversionrate != null && conversionrate !='' && conversionrate>0)
						totalQuantity= parseFloat(totalQuantity)/ parseFloat(conversionrate);
					nlapiLogExecution('DEBUG','Remaining Usage 3',context.getRemainingUsage());
					var str = 'SO Name. = ' + soname + '<br>';
					str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
					str = str + 'SO Location. = ' + solocation + '<br>';
					str = str + 'Customization Code. = ' + customizationcode + '<br>';
					str = str + 'Order Qty. = ' + orderqty + '<br>';
					str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
					str = str + 'Commetted Qty. = ' + commetedQty + '<br>';
					str = str + 'Created FO Qty. = ' + totalQuantity + '<br>';
					str = str + 'Shipped Qty. = ' + shippedqty + '<br>';
					str = str + 'Ship Method. = ' + solineshipmethod + '<br>';
					str = str + 'Ship To. = ' + solineshipto + '<br>';

					nlapiLogExecution('DEBUG', 'SO Line Details', str);		
					var totalcomtdqty=parseFloat(commetedQty)+parseFloat(shippedqty);
					//totalcomtdqty= parseFloat(totalcomtdqty)* parseFloat(conversionrate);
					if(totalQuantity == 0)
						variance=commetedQty;
					else
					{
						if(parseFloat(totalQuantity) < parseFloat(totalcomtdqty))
						{
							variance=parseFloat(totalcomtdqty)-parseFloat(totalQuantity);
						}
					}

					nlapiLogExecution('DEBUG','Variance1',variance);

					if(variance > 0)
					{
						//-------------------------------------------------Change by Ganesh on 16th Jan 2013
						if((parseFloat(variance)+ parseFloat(shippedqty)  + parseFloat(totalQuantity))>parseFloat(totalcomtdqty))
						{
							var totalInTransitFOQuantity = getFulfilmentqtyInProgNew(salesOrderLineDetails[i].getId(), salesOrderLineNo);
							nlapiLogExecution('DEBUG', 'Out of getFulfilmentqtyInProgNew - totalInTransitFOQuantity',totalInTransitFOQuantity);
							variance = parseFloat(totalcomtdqty) - parseFloat(shippedqty) - parseFloat(totalInTransitFOQuantity);

						}
						//-----------------------------------------------------------------------
					}	

					nlapiLogExecution('DEBUG','Variance2',variance);

					if(variance > 0)
					{
						if(oldsoname!=soname)
						{
							//generate new fullfillment order no.
							/***Below code is merged from Lexjet Production by Ganesh K on 4th Mar 2013***/
							//fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
							fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod,solineshipto);
							oldsoname=soname;
							var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod,solineshipto];
							fonumberarr.push(curRow);
						}
						else
						{
							if(fonumberarr!=null && fonumberarr!='' && fonumberarr.length>0)
							{
								var fonumberfound = 'F';
								for(var t=0;t<fonumberarr.length;t++)
								{
									if(fonumberarr[t][0]==soname && fonumberarr[t][2]==customizationcode && fonumberarr[t][3]==solocation 
											&& fonumberarr[t][4]==solineshipmethod && fonumberarr[t][5]==solineshipto)
									{
										fullFillmentorderno=fonumberarr[t][1];
										fonumberfound='T';
									}
								}
								if(fonumberfound=='F')
								{
									//fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
									fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod,solineshipto);
									var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod,solineshipto];
									/***upto here***/
									fonumberarr.push(curRow);
								}
							}
						}
						nlapiLogExecution('DEBUG','Remaining Usage 4',context.getRemainingUsage());
						nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);

						if(fullFillmentorderno!=null && fullFillmentorderno!='')
						{
							var fulillmentarray = getFullFillmentbyline(salesOrderLineDetails[i].getId(),salesOrderLineNo);
							if(fulillmentarray!=null && fulillmentarray!='' && fulillmentarray.length>0)
							{
								nlapiLogExecution('DEBUG','fulillmentarray length',fulillmentarray.length);	

								var existordqty = fulillmentarray[0].getValue('custrecord_ord_qty');
								var fointernalid = fulillmentarray[0].getId();
								var totalordqty = parseFloat(existordqty)+parseFloat(variance);

								var str = 'existordqty. = ' + existordqty + '<br>';
								str = str + 'totalordqty. = ' + totalordqty + '<br>';	
								str = str + 'variance. = ' + variance + '<br>';

								nlapiLogExecution('DEBUG', 'Qty Details', str);		

								nlapiLogExecution('DEBUG','Updating Fulfillment Order Line...');
								nlapiSubmitField('customrecord_ebiznet_ordline', fointernalid, 'custrecord_ord_qty', totalordqty);
							}
							else
							{
								createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,salesOrderLineDetails[i],soname,salesOrderLineDetails[i].getId(),vcustomer,lineseqno);
							}					

						}
						nlapiLogExecution('DEBUG','Remaining Usage 5',context.getRemainingUsage());							
					}
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG','exception in FO scheduler',e.message);
				}
				finally
				{
					//To unlock the sales order
					nlapiLogExecution('DEBUG','soname before unlocking',soname);
					nlapiLogExecution('DEBUG','trantype before unlocking','salesorder');

					if(soname != null && soname != '')
					{	
						var ExternalIdFilters = new Array();
						ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
						ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'salesorder'));
						var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
						if(searchresultsExt != null && searchresultsExt != '')
						{
							nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
							nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
							nlapiLogExecution('DEBUG','Unlocked successfully');
						}	
					}
				}
			}			
			//Update seq no in wave table if the remaining use is less than 500
			nlapiLogExecution('DEBUG','Remaining Usage',context.getRemainingUsage());
			var soname = salesOrderLineDetails[i].getValue('tranid');
			//Begin
			//if order changed then Checking the number lines of current order and mulitpliying with 50 + buffer units 500, then we are breaking the scheduler 
			if((i+1 != salesOrderLineDetails.length) && salesOrderLineDetails[i].getValue('tranid') != salesOrderLineDetails[i+1].getValue('tranid') )
			{
				var vOrdLineCount=getOrdLineCountFrmForLoop(salesOrderLineDetails,i+1);

				if(vOrdLineCount != null && vOrdLineCount != '')
				{

					nlapiLogExecution('ERROR','vOrdLineCount ',vOrdLineCount);
					var vAppxUnitsPerLine=50;
					nlapiLogExecution('ERROR','context.getRemainingUsage() ',context.getRemainingUsage());
					nlapiLogExecution('ERROR','context.getRemainingUsage() ',(parseInt(vOrdLineCount)*parseInt(vAppxUnitsPerLine)));
					nlapiLogExecution('ERROR','context.getRemainingUsage() ',parseInt((parseInt(vOrdLineCount)*parseInt(vAppxUnitsPerLine))));
					nlapiLogExecution('ERROR','context.getRemainingUsage() ',parseInt((parseInt(vOrdLineCount)*parseInt(vAppxUnitsPerLine)))+500);
					if(parseInt((parseInt(vOrdLineCount)*parseInt(vAppxUnitsPerLine)))+500 >=context.getRemainingUsage())					 
					{
						nlapiLogExecution('ERROR','vinternalid',vinternalid);
						updateSeqNo('FO',vinternalid);
						nlapiLogExecution('ERROR','break');
						vtempflag=true;
						break;
					}	

				}	
			}
			//End
			/*if(context.getRemainingUsage()<500)
			{				
				nlapiLogExecution('ERROR','vinternalid',vinternalid);
				updateSeqNo('FO',vinternalid);
				nlapiLogExecution('ERROR','break');
				vtempflag=true;
				break;
			}*/
		}
		nlapiLogExecution('DEBUG','salesOrderLineDetails.length2 ',salesOrderLineDetails.length);

		//Reset the seq no if all the sale orders are processed.
		if(salesOrderLineDetails.length<1000&&vtempflag!=true)
		{
			updateSeqNo('FO',0);
		}
		else
		{
			nlapiLogExecution('DEBUG','vinternalid',vinternalid);
			updateSeqNo('FO',vinternalid);			
		}
	}

	nlapiLogExecution('DEBUG','Remaining Usage at the end ',context.getRemainingUsage());	
	nlapiLogExecution('DEBUG','Out of  backOrderFulfillmentnew','');
}

function createbackorderFulfillmentOrdernew(lineno,variance,fullFillmentorderno,searchresults,tranid,salesOrderId,vcustomer,lineseqno,getinstdata,getlineinstdata)
{
	try
	{
		nlapiLogExecution('DEBUG','Into createbackorderFulfillmentOrdernew','');

		var str = 'salesOrderId. = ' + salesOrderId + '<br>';	
		str = str + 'tranid. = ' + tranid + '<br>';
		str = str + 'lineno. = ' + lineno + '<br>';
		str = str + 'variance. = ' + variance + '<br>';
		str = str + 'fullFillmentorderno. = ' + fullFillmentorderno + '<br>';
		str = str + 'vcustomer from header. = ' + vcustomer + '<br>';


		nlapiLogExecution('DEBUG', 'Function Parameters', str);
		nlapiLogExecution('DEBUG', 'lineseqno', lineseqno);
		nlapiLogExecution('DEBUG', 'getinstdata in cbackfulfil', getinstdata);
		nlapiLogExecution('DEBUG', 'getlineinstdata in cbackfulfil', getlineinstdata);
		nlapiLogExecution('DEBUG', 'customer from line', searchresults.getValue('entity'));

		var salesOrderNo = tranid;	
		var salesOrderInternalId = salesOrderId;

		var salesOrderDate =searchresults.getValue('trandate');
		var customer = searchresults.getValue('entity');
		var priority = searchresults.getValue('custbody_nswmspriority');
		var orderType = searchresults.getValue('custbody_nswmssoordertype');
		nlapiLogExecution('DEBUG','orderType in',orderType);
		var shipCarrierMethod = searchresults.getValue('shipmethod');	
		var company = searchresults.getValue('custbody_nswms_company');
		var route = searchresults.getValue('custbody_nswmssoroute');
		var location=searchresults.getValue('location');
		var shipcomplete = searchresults.getValue('shipcomplete');

		var vShipDate = getShipDate(salesOrderId);

		//var vShipDate = searchresults.getValue('shipdate');
		nlapiLogExecution('DEBUG','vShipDate',vShipDate);
		if(vShipDate==null||vShipDate==""||vShipDate=='undefined')
			vShipDate = searchresults.getValue('custbody_nswmspoexpshipdate');
		var customizationcode = searchresults.getValue('custcol_customizationcode');
		var vShippingNo = searchresults.getValue('custbody_shipment_no');
		nlapiLogExecution('DEBUG','vShippingNo in',vShippingNo);
		var locationname=searchresults.getText('location');
		var vOrdType = searchresults.getValue('custbody_nswmssoordertype');
		var shipattention = searchresults.getValue('shipattention');

//		nlapiLogExecution('DEBUG','locationname',locationname);
//		var linelocation = searchresults.getLineItemValue('item','location',lineno);
//		if(linelocation!=null && linelocation!='' && linelocation!='null')
//		{
//		location=linelocation;
//		}
		var statusbyloc='';
		var actuallocation=null;
		if(location!=null && location!='' && location!='null')
		{
			actuallocation=CheckLocationAlias(location);
		}
		else
		{
			if(salesOrderInternalId!=null && salesOrderInternalId!='' && salesOrderInternalId!='')
			{
				var SoRecord = nlapiLoadRecord('salesorder', salesOrderInternalId);
				location = SoRecord.getFieldValue('location');				
				actuallocation=CheckLocationAlias(location);
			}
		}

		nlapiLogExecution('DEBUG','location',actuallocation);

		//The following line of code is only for TNT
		//statusbyloc=getItemStatus(locationname);	

		nlapiLogExecution('DEBUG','statusbyloc',statusbyloc);

		var lineordsplit = fullFillmentorderno.split('.');
		var backordline = lineordsplit[1];

		//*********header information************
		var orderLineRecord = nlapiCreateRecord('customrecord_ebiznet_ordline');
		orderLineRecord.setFieldValue('name', salesOrderInternalId);
		orderLineRecord.setFieldValue('custrecord_ns_ord', parseFloat(salesOrderInternalId));
		//orderLineRecord.setFieldValue('custrecord_ordline',parseFloat(salesOrderLineNo));
		orderLineRecord.setFieldValue('custrecord_do_order_priority', priority);
		orderLineRecord.setFieldValue('custrecord_do_order_type', orderType);
		nlapiLogExecution('DEBUG','vcustomer new',vcustomer);
		orderLineRecord.setFieldValue('custrecord_do_customer', vcustomer);//case# 201415991
		orderLineRecord.setFieldValue('custrecord_do_carrier', shipCarrierMethod);	
		orderLineRecord.setFieldValue('custrecord_linestatus_flag', 25);
		orderLineRecord.setFieldValue('custrecord_route_no',route);
		orderLineRecord.setFieldValue('custrecord_lineord', fullFillmentorderno);
		orderLineRecord.setFieldValue('custrecord_ebiz_backorderno', backordline);
		orderLineRecord.setFieldValue('custrecord_ordline_company',company);
		if(actuallocation!=null)
		{
			orderLineRecord.setFieldValue('custrecord_ordline_wms_location',actuallocation);
		}
		else
		{
			orderLineRecord.setFieldValue('custrecord_ordline_wms_location',location);
		}
		orderLineRecord.setFieldValue('custrecord_actuallocation',location);
		orderLineRecord.setFieldValue('custrecord_record_linetime',TimeStamp());
		orderLineRecord.setFieldValue('custrecord_record_linedate',DateStamp());
		orderLineRecord.setFieldValue('custrecord_shipcomplete',shipcomplete);
		orderLineRecord.setFieldValue('custrecord_shipment_no',vShippingNo);
		if(vShipDate!='undefined')
			orderLineRecord.setFieldValue('custrecord_ebiz_shipdate',vShipDate);

		if(salesOrderDate!='undefined')
			orderLineRecord.setFieldValue('custrecord_ebiz_orderdate',salesOrderDate);

		if(customizationcode!=null && customizationcode!='' && customizationcode!=-1)
			orderLineRecord.setFieldValue('custrecord_ebiz_ordline_customizetype',customizationcode);

		if(shipattention!=null && shipattention!='' && shipattention!=-1)
			orderLineRecord.setFieldValue('custrecord_ebiz_ship_attention',shipattention);

		//*******Item/Line information*************

		// Shipment
		var Shipment = searchresults.getValue('custcol_nswms_shipment_no');
		nlapiLogExecution('DEBUG','Shipment before1',Shipment);
		if(Shipment==null || Shipment=='')
			Shipment=searchresults.getValue('custbody_shipment_no');

		nlapiLogExecution('DEBUG','Shipment before2',Shipment);
		orderLineRecord.setFieldValue('custrecord_shipment_no', Shipment);
		nlapiLogExecution('DEBUG','Shipment',Shipment);

		//Sales Order Line #
		var salesOrderLineNo =searchresults.getValue('line');
		orderLineRecord.setFieldValue('custrecord_ordline', salesOrderLineNo);

		// Sales Order Item Id
		var itemId =searchresults.getValue('item');
		orderLineRecord.setFieldValue('custrecord_ebiz_linesku', itemId);

		//********************************************************************************************************//

		var itemStatus =searchresults.getValue('custcol_ebiznet_item_status');
		var itemPackCode =searchresults.getValue('custcol_nswmspackcode');
		nlapiLogExecution('DEBUG','itemPackCode',itemPackCode);

		var defpackcode = '';

		if( (itemStatus==null || itemStatus=='') || (itemPackCode==null || itemPackCode==''))
		{
			var fields = ['custitem_ebizoutbounddefskustatus','custitem_ebizdefpackcode'];
			var columns= nlapiLookupField('item',itemId,fields);
			if(columns!=null)
			{
				if(itemStatus==null || itemStatus=='')
					itemStatus = columns.custitem_ebizoutbounddefskustatus;
				defpackcode = columns.custitem_ebizdefpackcode;
			}
		}

		if(itemStatus==null || itemStatus=='')
		{
			if(actuallocation!=null && actuallocation!='')		
				itemStatus = getDefaultSKUStatus(actuallocation);
			else
				itemStatus = getDefaultSKUStatus(location);
		}

		nlapiLogExecution('DEBUG','itemStatus',itemStatus);

		if(itemStatus!=null && itemStatus!='')
		{
			orderLineRecord.setFieldValue('custrecord_linesku_status', itemStatus);
		}

		// Sales Order Item Pack Code

		if(itemPackCode==null || itemPackCode=='')
		{
			itemPackCode=defpackcode;
		}
		nlapiLogExecution('DEBUG','itemPackCode',itemPackCode);
		orderLineRecord.setFieldValue('custrecord_linepackcode', itemPackCode);


		//********************************************************************************************************//

		/*// Sales Order Item Uom
		var baseUom = searchresults.getValue('custcol_nswmssobaseuom');
		if(baseUom != null && baseUom != '')
			orderLineRecord.setFieldValue('custrecord_lineuom_id', parseFloat(baseUom));*/

		var vNSUOM = searchresults.getValue('unit');

		var veBizUOMQty=1;
		var vBaseUOMQty=1;

		if(vNSUOM!=null && vNSUOM!="")
		{
			var eBizItemDims=geteBizItemDimensions(itemId);
			if(eBizItemDims!=null&&eBizItemDims.length>0)
			{
				for(z=0; z < eBizItemDims.length; z++)
				{
					if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
					{
						vBaseUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					}

					if((vNSUOM == eBizItemDims[z].getValue('custrecord_ebiznsuom')) ||
							(vNSUOM == eBizItemDims[z].getText('custrecord_ebiznsuom')))
					{
						veBizUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					}
				}

				if(veBizUOMQty==null || veBizUOMQty=='')
				{
					veBizUOMQty=vBaseUOMQty;
				}

				if(veBizUOMQty!=0)
					variance = parseFloat(variance)*parseFloat(veBizUOMQty)/parseFloat(vBaseUOMQty);
				nlapiLogExecution('DEBUG', 'variance', variance);
			}
		}

		// Sales Order Item Committed Quantity
		orderLineRecord.setFieldValue('custrecord_ord_qty', parseFloat(variance).toFixed(5));
		// Item Info for a particular Item
		var itemSubtype = nlapiLookupField('item', itemId, 'recordType');
		var fields = ['custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
		var columnsval = nlapiLookupField(itemSubtype, itemId, fields,true);// case# 201414310
		var skuinfo1val = columnsval.custitem_item_info_1;
		var skuinfo2val = columnsval.custitem_item_info_2;
		var skuinfo3val = columnsval.custitem_item_info_3;

		orderLineRecord.setFieldValue('custrecord_fulfilmentiteminfo1',skuinfo1val);
		orderLineRecord.setFieldValue('custrecord_fulfilmentiteminfo2',skuinfo2val);
		orderLineRecord.setFieldValue('custrecord_fulfilmentiteminfo3',skuinfo3val);
		orderLineRecord.setFieldValue('custrecord_linenotes2','Created by Schedule Script');
		orderLineRecord.setFieldValue('custrecord_wms_soinstructions', getinstdata);
		orderLineRecord.setFieldValue('custrecord_wms_soiteminstructions', getlineinstdata);


		if(vOrdType != null && vOrdType != '')			
			orderLineRecord.setFieldValue('custrecord_do_order_type',vOrdType);

		//code added for getting inventory detail from order and updating into FO
		var vinvtdetaillot="";
		var vinvtdetailqty="";
		var BatchID="";

		try
		{
			var vAdvBinManagement = '';
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}
			var SoinvtRecord = nlapiLoadRecord('salesorder', salesOrderInternalId);
			//SoinvtRecord.selectLineItem('item',salesOrderLineNo);
			SoinvtRecord.selectLineItem('item',lineseqno);
			//SoinvtRecord.selectLineItem('item',lineno);
			if(vAdvBinManagement==true)
			{
				var invDetailSubrecord = SoinvtRecord.viewCurrentLineItemSubrecord('item', 'inventorydetail');
				if (invDetailSubrecord!="" && invDetailSubrecord!=null)
				{
					invDetailSubrecord.selectLineItem('inventoryassignment', 1);
					//var soAssignedlot=invDetailSubrecord.getCurrentLineItemValue('inventoryassignment','issueinventorynumber');
					vinvtdetaillot=invDetailSubrecord.getCurrentLineItemText('inventoryassignment','issueinventorynumber');
					vinvtdetailqty=invDetailSubrecord.getCurrentLineItemValue('inventoryassignment','quantity');
				}
				if(vinvtdetaillot!=null && vinvtdetaillot!='')
				{
					var filter=new Array();
					if(itemId!=null&&itemId!="")
						filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemId));
					filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',vinvtdetaillot));
					var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filter,null);
					if(rec!=null&&rec!="")
					{
						BatchID=rec[0].getId();

					}
				}
			}
			var str1 = 'vinvtdetaillot. = ' + vinvtdetaillot + '<br>';	
			str1 = str1 + 'vinvtdetailqty. = ' + vinvtdetailqty + '<br>';
			str1 = str1 + 'BatchID. = ' + BatchID + '<br>';




			nlapiLogExecution('DEBUG', 'Inventory Detail Parameters', str1);

			if(vinvtdetaillot != null && vinvtdetaillot != "")
				orderLineRecord.setFieldValue('custrecord_ebiz_lot_number', vinvtdetaillot);
			if(vinvtdetailqty != null && vinvtdetailqty != "")
				orderLineRecord.setFieldValue('custrecord_ebiz_lotnumber_qty', vinvtdetailqty);
			if(BatchID != null && BatchID != "")
				orderLineRecord.setFieldValue('custrecord_ebiz_lotno_internalid', BatchID);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in wmsCarrier : ', exp);		
		}

		try
		{
			//var wmsCarrier = nlapiGetFieldValue('custbody_salesorder_carrier');
			var wmsCarrier = searchresults.getValue('custbody_salesorder_carrier');
			var wmscarrierlinelevel = searchresults.getValue('custcol_salesorder_carrier_linelevel');
			nlapiLogExecution('DEBUG', 'wmscarrier', wmsCarrier);
			nlapiLogExecution('DEBUG', 'wmscarrierlinelevel', wmscarrierlinelevel);

			if (wmscarrierlinelevel != null && wmscarrierlinelevel != "")
			{

				orderLineRecord.setFieldValue('custrecord_do_wmscarrier', wmscarrierlinelevel);
			}
			/*	else if (wmsCarrier != null && wmsCarrier != "")
			{

				orderLineRecord.setFieldValue('custrecord_do_wmscarrier', wmsCarrier);
			} */

		}
		catch(exp) {
			nlapiLogExecution('DEBUG', 'Exception in wmsCarrier : ', exp);		
		}


		//submit the record 
		if(parseFloat(variance) != 0 ){
			var recordId = nlapiSubmitRecord(orderLineRecord);
			//updatesalesorderline(salesOrderInternalId,salesOrderLineNo,variance,'E');
			nlapiLogExecution('DEBUG','recordstatus','recordsubmited'+lineno);
		}		
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in createbackorderFulfillmentOrdernew : ', exp);		
	}

	nlapiLogExecution('DEBUG','Out of createbackorderFulfillmentOrdernew','');
}

/**
 * This function will update sales order line with the qty for which FO is created.
 * @param introrderid
 * @param ordlineno
 */

function updatesalesorderline(introrderid,ordlineno,taskqty,status)
{
	nlapiLogExecution('DEBUG', 'Into updatesalesorderline');
	nlapiLogExecution('DEBUG', 'SO Id',introrderid);
	nlapiLogExecution('DEBUG', 'SO Line No',ordlineno);
	nlapiLogExecution('DEBUG', 'SO Line Qty',taskqty);
	nlapiLogExecution('DEBUG', 'Status',status);

	var oldqty = 0;
	var totalqty = 0;

	var TransformRecord = nlapiLoadRecord('salesorder', introrderid);

	if(status=='E')
	{
		oldqty = TransformRecord.getLineItemValue('item', 'custcol_ebizreleaseqty',ordlineno);

		if(oldqty==null || oldqty=='' || isNaN(oldqty))
			oldqty=0;

		totalqty=parseFloat(taskqty)+parseFloat(oldqty);

		nlapiLogExecution('DEBUG', 'SO Line Total Qty',totalqty);
		nlapiLogExecution('DEBUG', 'oldqty',oldqty);
		nlapiLogExecution('DEBUG', 'lineno',TransformRecord.getLineItemValue('item', 'line',ordlineno));
		TransformRecord.setLineItemValue('item', 'custcol_ebizreleaseqty',ordlineno,parseFloat(totalqty));
	}
	else if(status=='G')
	{
		oldqty = TransformRecord.getLineItemValue('item', 'custcol_ebizpickgenqty',ordlineno);

		if(oldqty==null || oldqty=='' || isNaN(oldqty))
			oldqty=0;

		totalqty=parseFloat(taskqty)+parseFloat(oldqty);

		nlapiLogExecution('DEBUG', 'SO Line Total Qty',totalqty);

		TransformRecord.setLineItemValue('item', 'custcol_ebizpickgenqty',ordlineno,parseFloat(totalqty));
	}
	else if(status=='C')
	{
		oldqty = TransformRecord.getLineItemValue('item', 'custcol_ebizpickedqty',ordlineno);

		if(oldqty==null || oldqty=='' || isNaN(oldqty))
			oldqty=0;

		totalqty=parseFloat(taskqty)+parseFloat(oldqty);

		nlapiLogExecution('DEBUG', 'SO Line Total Qty',totalqty);

		TransformRecord.setLineItemValue('item', 'custcol_ebizpickedqty',ordlineno,parseFloat(totalqty));
	}
	else if(status=='S')
	{
		oldqty = TransformRecord.getLineItemValue('item', 'custcol_ebizshippedqty',ordlineno);

		if(oldqty==null || oldqty=='' || isNaN(oldqty))
			oldqty=0;

		totalqty=parseFloat(taskqty)+parseFloat(oldqty);

		nlapiLogExecution('DEBUG', 'SO Line Total Qty',totalqty);

		TransformRecord.setLineItemValue('item', 'custcol_ebizshippedqty',ordlineno,parseFloat(totalqty));
	}
	nlapiLogExecution('DEBUG', 'Befor submit');
	nlapiSubmitRecord(TransformRecord, true);	

	nlapiLogExecution('DEBUG', 'Out of updatesalesorderline');
}

function updateSeqNo(transType,seqno){
	nlapiLogExecution('DEBUG', 'Into updateSeqNo', seqno);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_seqno');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_trantype', null, 'is', transType);

	// Search in ebiznet_wave custom record for all rows for the given transaction type
	var results = nlapiSearchRecord('customrecord_ebiznet_wave', null, filters, columns);
	if(results != null){
		for (var t = 0; t < results.length; t++) {
			nlapiSubmitField('customrecord_ebiznet_wave', results[t].getId(), 'custrecord_seqno', parseFloat(seqno));
		}
	}

	nlapiLogExecution('DEBUG', 'Out of updateSeqNo', seqno);
}

function backOrderFulfillmentforTO(type)
{
	nlapiLogExecution('DEBUG','Into  backOrderFulfillmentforTO','');
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());

	var vConfig=nlapiLoadConfiguration('companyfeatures');
	var UOMruleValue=vConfig.getFieldValue('unitsofmeasure');

	nlapiLogExecution('DEBUG','UOMruleValue',UOMruleValue);

	if ( type != 'scheduled') return; 
	var vintrid=0;
	var vinternalid=0;

	//get the seq no from wave record.

	nlapiLogExecution('DEBUG','internalid in search filter',vintrid);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
	//filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0

	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['TrnfrOrd:B','TrnfrOrd:D','TrnfrOrd:E']));
	filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
	//filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
	filters.push(new nlobjSearchFilter('transactionlinetype', null, 'anyof', 'ITEM'));
	if(parseInt(vintrid)>1)
	{
		nlapiLogExecution('DEBUG','In side internalid in search filter',vintrid);
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'lessthanorequalto', vintrid));
	}

	//filters.push(new nlobjSearchFilter('internalid', null, 'is', 871126));//To avoid tax lines.

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');	
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
	columns[3] = new nlobjSearchColumn('quantitycommitted');
	columns[4]=new nlobjSearchColumn('formulanumeric');
	columns[4].setFormula('ABS({quantity})');
	columns[5] = new nlobjSearchColumn('trandate');
	columns[6] = new nlobjSearchColumn('entity');
	columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
	columns[8] = new nlobjSearchColumn('shipmethod');
	columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
	columns[10] = new nlobjSearchColumn('custbody_nswms_company');
	columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
	columns[12] = new nlobjSearchColumn('location');
	columns[13] = new nlobjSearchColumn('item');
	columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
	columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
	columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');	
	columns[17] = new nlobjSearchColumn('tranid');
	columns[18] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
	columns[19] = new nlobjSearchColumn('quantityshiprecv');
	columns[20] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
	columns[21] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
	columns[22] = new nlobjSearchColumn('custbody_shipment_no');
	columns[23] = new nlobjSearchColumn('custbody_nswmssoordertype');	
	if(UOMruleValue =='T')
	{
		columns[24] = new nlobjSearchColumn('unit');

	}
	//true - Discending false - Ascending
	columns[0].setSort(true);
	columns[1].setSort();

	var salesOrderLineDetails = new nlapiSearchRecord('transferorder', null, filters, columns);
	nlapiLogExecution('DEBUG','Remaining Usage 2',context.getRemainingUsage());
	if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
	{	

		var sonumber = 0; //Added 08-Nov-2011
		var oldsoname='';
		var fullFillmentorderno='';
		var fonumberarr = new Array();
		var createfo = 'T'; //Added 08-Nov-2011
		var vcustomer = '';
		nlapiLogExecution('DEBUG','salesOrderLineDetails.length ',salesOrderLineDetails.length);
		for(var i=0;i<salesOrderLineDetails.length;i++)
		{	

			vinternalid=salesOrderLineDetails[i].getValue('internalid');
			var soname = salesOrderLineDetails[i].getValue('tranid');			
			var orderqty = salesOrderLineDetails[i].getValue('formulanumeric');
			if(orderqty < 0)
				orderqty=orderqty*(-1);
			var create_fulfillment_order= salesOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
			var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
			var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
			if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
				commetedQty=0;

			var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
			var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');

			var str = '# Loop. = ' + i + '<br>';				
			str = str + 'vinternalid. = ' + vinternalid + '<br>';
			str = str + 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
			str = str + 'shpcompleteflag. = ' + shpcompleteflag + '<br>';

			nlapiLogExecution('DEBUG', 'SO Details', str);

			// Assuming search results will return the data in the descending order of SO#
			var mwhsiteflag='F';
			if(soname != null)
			{
				if(sonumber != soname)
				{
					sonumber = soname;
					vcustomer = nlapiLookupField('salesorder', vinternalid, 'entity');
					createfo = 'T';
					nlapiLogExecution('DEBUG','SO Number',sonumber);

					if(shpcompleteflag != null && shpcompleteflag == 'T')
					{						
						for(var j=0;j<salesOrderLineDetails.length;j++)
						{
							if(sonumber == salesOrderLineDetails[j].getValue('tranid'))
							{
								var itemtype='';
								var solineordqty = salesOrderLineDetails[j].getValue('formulanumeric');
								if(solineordqty < 0)
									solineordqty=solineordqty*(-1);
								var solinecmmtdqty = salesOrderLineDetails[j].getValue('quantitycommitted');
								var lineitem=salesOrderLineDetails[j].getValue('item');
								var shipmethod=salesOrderLineDetails[j].getValue('shipmethod');

								var linelocation=salesOrderLineDetails[j].getValue('location');
								if(linelocation!=null && linelocation!='')
									location=linelocation;

//								var mwhsiteflag='F';
								var	islocationAliasExists=CheckLocationAlias(location);
								if(islocationAliasExists!=null && islocationAliasExists!='')
								{
									mwhsiteflag='T';
								}
								else
								{
									if(location!=null && location!='')
									{
										var fields = ['custrecord_ebizwhsite'];

										var locationcolumns = nlapiLookupField('Location', location, fields);
										if(locationcolumns!=null)
											mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
									}
								}

								if(solinecmmtdqty==null || solinecmmtdqty=='' || isNaN(solinecmmtdqty))
									solinecmmtdqty=0;

								var solinefulfildqty = salesOrderLineDetails[j].getValue('quantityshiprecv');

								if(solinefulfildqty == null || solinefulfildqty == '' || isNaN(solinefulfildqty))
									solinefulfildqty = 0;

								var totalcomtdqty=parseFloat(solinecmmtdqty)+parseFloat(solinefulfildqty);

								if(lineitem!=null && lineitem!='')
									itemtype = nlapiLookupField('item', lineitem, 'recordType');	

								nlapiLogExecution('DEBUG','itemtype',itemtype);

								if(itemtype!='descriptionitem' && itemtype!='discountitem' && itemtype!='Payment' && itemtype!='Markup' 
									&& itemtype!='Subtotal'  && itemtype!='otherchargeitem' && itemtype!='serviceitem' && itemtype!='noninventoryitem'
										&& itemtype!='NonInvtPart')
								{
									if(parseFloat(solineordqty) > parseFloat(totalcomtdqty))
									{
										createfo = 'F';
										continue;
									}
								}
							}

						}

						nlapiLogExecution('DEBUG','Create FO',createfo);
					}						
				}				
			}

			var str = 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';				
			str = str + 'createfo. = ' + createfo + '<br>';

			nlapiLogExecution('DEBUG', 'SO Flag Details', str);


			if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T' && mwhsiteflag=='T')
			{
				var variance=0;

				var totalQuantity = getFulfilmentqty(salesOrderLineDetails[i].getId(), salesOrderLineNo);
				nlapiLogExecution('DEBUG','Remaining Usage 3',context.getRemainingUsage());

				var str = 'SO Name. = ' + soname + '<br>';				
				str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';
				str = str + 'Order Qty. = ' + orderqty + '<br>';
				str = str + 'Commetted Qty. = ' + commetedQty + '<br>';
				str = str + 'Created FO Qty. = ' + totalQuantity + '<br>';
				str = str + 'Shipped Qty. = ' + shippedqty + '<br>';

				nlapiLogExecution('DEBUG', 'SO & FO Qty Details', str);


				var totalcomtdqty=parseFloat(commetedQty)+parseFloat(shippedqty);

				if(totalQuantity == 0)
					variance=commetedQty;
				else
				{
					if(parseFloat(totalQuantity) < parseFloat(totalcomtdqty))
					{
						variance=parseFloat(totalcomtdqty)-parseFloat(totalQuantity);
					}
				}

				nlapiLogExecution('DEBUG','Variance',variance);

				if(variance > 0)
				{
					if(oldsoname!=soname)
					{
						//generate new fullfillment order no.
						fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
						oldsoname=soname;
						var curRow = [soname,fullFillmentorderno];
						fonumberarr.push(curRow);
					}
					else
					{
						if(fonumberarr!=null && fonumberarr!='' && fonumberarr.length>0)
						{
							var fonumberfound = 'F';
							for(var t=0;t<fonumberarr.length;t++)
							{
								if(fonumberarr[t][0]==soname)
								{
									fullFillmentorderno=fonumberarr[t][1];
									fonumberfound='T';
								}
							}
							if(fonumberfound=='F')
							{
								fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
								var curRow = [soname,fullFillmentorderno];
								fonumberarr.push(curRow);
							}
						}
					}
					nlapiLogExecution('DEBUG','Remaining Usage 4',context.getRemainingUsage());

					nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);

					if(fullFillmentorderno!= null && fullFillmentorderno != '')
					{
						var fulillmentarray = getFullFillmentbyline(salesOrderLineDetails[i].getId(),salesOrderLineNo);
						if(fulillmentarray!=null && fulillmentarray!='' && fulillmentarray.length>0)
						{
							nlapiLogExecution('DEBUG','fulillmentarray length',fulillmentarray.length);	

							var existordqty = fulillmentarray[0].getValue('custrecord_ord_qty');
							var fointernalid = fulillmentarray[0].getId();
							var totalordqty = parseFloat(existordqty)+parseFloat(variance);

							var str = 'existordqty. = ' + existordqty + '<br>';				
							str = str + 'totalordqty. = ' + totalordqty + '<br>';
							str = str + 'variance. = ' + variance + '<br>';

							nlapiLogExecution('DEBUG', 'FO Qty Details', str);

							nlapiSubmitField('customrecord_ebiznet_ordline', fointernalid, 'custrecord_ord_qty', totalordqty);
						}
						else
						{
							createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,salesOrderLineDetails[i],soname,salesOrderLineDetails[i].getId(),vcustomer);
						}
					}

					nlapiLogExecution('DEBUG','Remaining Usage 5',context.getRemainingUsage());							
				}
			}			
			//Update seq no in wave table if the remaining use is less than 500
			if(context.getRemainingUsage()<500)
			{				
				nlapiLogExecution('DEBUG','vinternalid',vinternalid);			
				nlapiLogExecution('DEBUG','break');
				break;
			}
		}
		nlapiLogExecution('DEBUG','salesOrderLineDetails.length2 ',salesOrderLineDetails.length);

	}

	nlapiLogExecution('DEBUG','Remaining Usage at the end ',context.getRemainingUsage());	

	nlapiLogExecution('DEBUG','Out of  backOrderFulfillmentforTO','');
}

/**
 *  Generate the fulfillment order number based on the existing fulfillment orders
 *  	by concatenating the sales order number with the maximum value of the fulfillment order number.
 * @param salesOrderId
 * @returns {String}
 */
function fulfillmentOrderDetailsNew(salesOrderId,salesordername,SOLine,customizationcode)
{
	nlapiLogExecution('DEBUG','Into fulfillmentOrderDetailsNew','');

	var str = 'salesOrderId. = ' + salesOrderId + '<br>';				
	str = str + 'salesordername. = ' + salesordername + '<br>';
	str = str + 'SOLine. = ' + SOLine + '<br>';
	str = str + 'customizationcode. = ' + customizationcode + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var salesOrderArray = new Array();
	var ordlinefilters = new Array();
	ordlinefilters.push(new nlobjSearchFilter('name', null, 'is', salesOrderId));
	ordlinefilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [25]));
	if(parseFloat(SOLine)!= 0)
		ordlinefilters.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', SOLine));

	if(customizationcode!= null && customizationcode!='' && customizationcode!=-1)
		ordlinefilters.push( new nlobjSearchFilter('custrecord_ebiz_ordline_customizetype', null, 'is', customizationcode));

	var ordlinecolumns = new Array();  
	ordlinecolumns[0] = new nlobjSearchColumn('custrecord_lineord').setSort();        
	ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ordline');
	ordlinecolumns[2] = new nlobjSearchColumn('custrecord_ord_qty'); // fulfillment ordered quantity
	var searchFulfillment = nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);

	//nlapiLogExecution('DEBUG','searchFulfillment :', searchFulfillment);

	//If searchfulfillment is null it indicates Its a New SO. 
	if(searchFulfillment == null){
		var fulfillmentNumber=null;
	}
	else{
		for(var i=0; i< searchFulfillment.length;i++)
		{
			var fullfillmentlineord = searchFulfillment[i].getValue('custrecord_lineord');
			var lineordsplit = fullfillmentlineord.split('.');
			salesOrderArray[i] = lineordsplit[1];
		}
		//MaxValue returns the max fullfillment order number
		var maximumValue = MaxValue(salesOrderArray);

		//max fullfillment order number to get the new fullfillment order no.
		var fulfillmentValue = parseFloat(maximumValue);
		nlapiLogExecution('DEBUG','fulfillment Value :', fulfillmentValue);

		//concatinating the fulfillmentvalue to salesorderid to get new fullfillment no.
		var fulfillmentNumber =  salesordername + "." + fulfillmentValue;
		//nlapiLogExecution('DEBUG','fulfillmentNumber :', fulfillmentNumber);
	}

	nlapiLogExecution('DEBUG','Out of fulfillmentOrderDetailsNew','');
	return fulfillmentNumber;
}

/**
 * Delete the records which are fufilled and having status E
 * @param soId
 * @param lineNo
 */
function DeleteRecCreatedNew(soId, lineNo,foid)
{ 
	nlapiLogExecution('DEBUG','Into DeleteRecCreatedNew');
	var str = 'soid. = ' + soid + '<br>';				
	str = str + 'lineno. = ' + lineno + '<br>';
	str = str + 'foid. = ' + foid + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var filter = new Array();
	filter.push(new nlobjSearchFilter('name', null, 'is', soId));
	filter.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineNo));
	if(foid != null && foid != '')
		filter.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', foid));

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_linestatus_flag') ;
	column[1] = new nlobjSearchColumn('custrecord_ord_qty') ;

	var searchRec= nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);
	if(searchRec!=null)
		nlapiLogExecution('DEBUG','searchRec ',searchRec.length);
	var Id="";
	var Type="";
	if (searchRec)
	{
		for (var s = 0; s < searchRec.length; s++)
		{
			var searchresult = searchRec[s];
			nlapiLogExecution('DEBUG','searchresult',searchresult);
			Id = searchRec[s].getId();
			Type= searchRec[s].getRecordType();
			nlapiLogExecution('DEBUG','FLAG',searchRec[s].getValue('custrecord_linestatus_flag'));
			if(searchRec[s].getValue('custrecord_linestatus_flag')==25)
			{
				nlapiDeleteRecord(searchRec[s].getRecordType(),searchRec[s].getId());
				nlapiLogExecution('DEBUG','vDelQty',searchRec[s].getValue('custrecord_ord_qty'));
				var vDelQty=searchRec[s].getValue('custrecord_ord_qty');
				nlapiLogExecution('DEBUG','Deleted record Id',Id);
				return vDelQty;
			}
			else
			{

			}
		}
	}
	return '';
}


/**
 * To get the sum of all the order qty of the particular line no and SO, 
 * from fullfillment order table
 * @param soid
 * @param lineno
 * @returns Totalqty
 */
function getFulfilmentqtyNew(soid,lineno)
{
	nlapiLogExecution('DEBUG', 'Into getFulfilmentqtyNew');

	var str = 'soid. = ' + soid + '<br>';				
	str = str + 'lineno. = ' + lineno + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var vqty=0;
	var filters = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', soid.toString()));
	filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineno));
	filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'noneof', [25]));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	var searchresultsRcpts = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	if (searchresultsRcpts != null) 
	{
		var val = searchresultsRcpts[0];
		vqty=val.getValue('custrecord_ord_qty',null, 'sum');
		//nlapiLogExecution('DEBUG','vqtysum',vqty);
		if(vqty==null)
			return 0;
		else
			return vqty;
	}
}


function getFulfilmentqtyInProgNew(soid,lineno)
{
	nlapiLogExecution('DEBUG', 'Into getFulfilmentqtyInProgNew');

	var str = 'soid. = ' + soid + '<br>';				
	str = str + 'lineno. = ' + lineno + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var vqty=0;
	var filters = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', soid.toString()));
	filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineno));
	filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'noneof', [25]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_ship_qty',null,'sum');
	var searchresultsRcpts = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if (searchresultsRcpts != null) 
	{
		var val = searchresultsRcpts[0];
		vqty=val.getValue('custrecord_ord_qty',null, 'sum');
		var vShipqty=val.getValue('custrecord_ship_qty',null, 'sum');
		//nlapiLogExecution('DEBUG','vqtysum',vqty);
		if(vqty==null || vqty=='' )
			vqty= 0;
		if(vShipqty==null || vShipqty=='')
			vShipqty= 0;
		var vDiff=parseFloat(vqty)-parseFloat(vShipqty);
		if(parseFloat(vDiff)<0)
		{	
			vDiff=0;
			return vDiff;
		}
		else
			return vDiff;
	}
	else
		return 0;
}

function CreateFO_SaveSearch(type)
{
	nlapiLogExecution('DEBUG','Into  CreateFO_SaveSearch','');
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());
	//if ( type != 'scheduled') return; 
	var vintrid=0;
	var vinternalid=0;

	var vConfig=nlapiLoadConfiguration('companyfeatures');
	var UOMruleValue=vConfig.getFieldValue('unitsofmeasure');

	nlapiLogExecution('DEBUG','UOMruleValue',UOMruleValue);

	var vInternalIdArray=GetAllInternalID_SavedSearch();

	if(vInternalIdArray!=null&&vInternalIdArray!="")
	{		
		var filters = new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
		filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0

		// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
		filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
		//filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
		filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
		filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
		filters.push(new nlobjSearchFilter('custbody_ebiz_ord_size', null, 'isnot', 'L')); // To process less than 100 line orders only
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', vInternalIdArray));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');	
		columns[1] = new nlobjSearchColumn('line');
		columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
		columns[3] = new nlobjSearchColumn('quantitycommitted');
		columns[4] = new nlobjSearchColumn('quantity');
		columns[5] = new nlobjSearchColumn('trandate');
		columns[6] = new nlobjSearchColumn('entity');
		columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
		columns[8] = new nlobjSearchColumn('shipmethod');
		columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
		columns[10] = new nlobjSearchColumn('custbody_nswms_company');
		columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
		columns[12] = new nlobjSearchColumn('location');
		columns[13] = new nlobjSearchColumn('item');
		columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
		columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
		columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');		
		columns[17] = new nlobjSearchColumn('tranid');
		columns[18] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
		columns[19] = new nlobjSearchColumn('quantityshiprecv');
		columns[20] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
		columns[21] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
		columns[22] = new nlobjSearchColumn('custcol_customizationcode');
		columns[23] = new nlobjSearchColumn('custbody_nswmspoexpshipdate');
		columns[24] = new nlobjSearchColumn('custbody_nswmssoordertype');
		columns[25] = new nlobjSearchColumn('custbody_shipment_no');
		columns[26] = new nlobjSearchColumn('shipdate');
		columns[27] = new nlobjSearchColumn('custcol_nswms_shipment_no');
		//	columns[28] = new nlobjSearchColumn('quantityuom');
		columns[28] = new nlobjSearchColumn('shipattention');
		columns[29] = new nlobjSearchColumn('custcol_create_fulfillment_order');
		columns[30] = new nlobjSearchColumn('linesequencenumber');
		columns[31] = new nlobjSearchColumn('custbody_nswmspoinstructions');
		columns[32] = new nlobjSearchColumn('custcol_wms_rfinstructions');		
		if(UOMruleValue=='T')
		{
			columns[33] = new nlobjSearchColumn('unit');
			columns[34] = new nlobjSearchColumn('quantityuom');
		}
		//true - Discending false - Ascending
		columns[0].setSort(true);
		columns[1].setSort();

		var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);
		nlapiLogExecution('DEBUG','Remaining Usage 2',context.getRemainingUsage());
		if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
		{	

			var sonumber = 0; //Added 08-Nov-2011
			var oldsoname='';
			var fullFillmentorderno='';
			var fonumberarr = new Array();
			var vtempflag=false;
			var createfo = 'T'; //Added 08-Nov-2011
			var vcustomer = '';

			nlapiLogExecution('DEBUG','salesOrderLineDetails.length ',salesOrderLineDetails.length);
			for(var i=0;i<salesOrderLineDetails.length;i++)
			{			
				var mwhsiteflag='F';
				vinternalid=salesOrderLineDetails[i].getValue('internalid');
				var soname = salesOrderLineDetails[i].getValue('tranid');	
				var solocation = salesOrderLineDetails[i].getValue('location');
				var orderqty = salesOrderLineDetails[i].getValue('quantity');
				var create_fulfillment_order= salesOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
				var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
				var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
				var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
				var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');
				var customizationcode = salesOrderLineDetails[i].getValue('custcol_customizationcode');
				var uomconvqty = '';
				var unit = '';
				if(UOMruleValue=='T')
				{
					unit = salesOrderLineDetails[i].getValue('unit');
					uomconvqty = salesOrderLineDetails[i].getValue('quantityuom');
				}

				/***Below code is merged from Lexjet Production by Ganesh K on 4th Mar 2013***/
				var solineshipmethod = salesOrderLineDetails[i].getValue('shipmethod');
				var solineCreateFulfillFlag = salesOrderLineDetails[i].getValue('custcol_create_fulfillment_order');
				var lineseqno=salesOrderLineDetails[i].getValue('linesequencenumber');
				/***upto here***/
				var solineshipto = salesOrderLineDetails[i].getValue('shipattention');
				var getlineinstdata = salesOrderLineDetails[i].getValue('custcol_wms_rfinstructions');
				nlapiLogExecution('DEBUG','getlineinstdata in fo_saved',getlineinstdata);

				var getinstdata = salesOrderLineDetails[i].getValue('custbody_nswmspoinstructions');
				nlapiLogExecution('DEBUG','getinstdata in fo_saved',getinstdata);

				var conversionrate = 1;
				if(unit!=null && unit!='' && uomconvqty!=null && uomconvqty!='')
				{
					conversionrate = parseFloat(orderqty)/parseFloat(uomconvqty);
				}


				var str = '# Loop. = ' + i + '<br>';
				str = str + 'vinternalid. = ' + vinternalid + '<br>';
				str = str + 'SO Name. = ' + soname + '<br>';
				str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
				str = str + 'SO Location. = ' + solocation + '<br>';
				str = str + 'Customization Code. = ' + customizationcode + '<br>';
				str = str + 'Order Qty. = ' + orderqty + '<br>';
				str = str + 'Unit. = ' + unit + '<br>';
				str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
				str = str + 'UOM Conversion Rate. = ' + conversionrate + '<br>';
				str = str + 'Commetted Qty. = ' + commetedQty + '<br>';			
				str = str + 'Shipped Qty. = ' + shippedqty + '<br>';
				str = str + 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
				str = str + 'Ship Method. = ' + solineshipmethod + '<br>';
				str = str + 'Ship To. = ' + solineshipto + '<br>';
				str = str + 'solineCreateFulfillFlag. = ' + solineCreateFulfillFlag + '<br>';
				nlapiLogExecution('DEBUG', 'SO Line Details1', str);		

				if(customizationcode==null || customizationcode=='')
					customizationcode=-1;

				if(solineshipto==null || solineshipto=='')
					solineshipto=-1;

				if(uomconvqty==null || uomconvqty=='' || isNaN(uomconvqty))
					uomconvqty=1;

				if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
					commetedQty=0;

				if(shippedqty==null || shippedqty=='' || isNaN(shippedqty))
					shippedqty=0;

				if(unit!=null && unit!='')
				{
					commetedQty = parseFloat(commetedQty)/parseFloat(conversionrate);
					shippedqty = parseFloat(shippedqty)/parseFloat(conversionrate);
				}

				// Assuming search results will return the data in the descending order of SO#

				if(soname != null)
				{
					var	islocationAliasExists=CheckLocationAlias(solocation);
					if(islocationAliasExists!=null && islocationAliasExists!='')
					{
						mwhsiteflag='T';
					}
					else
					{
						if(solocation!=null && solocation!='')
						{
							var fields = ['custrecord_ebizwhsite'];

							var locationcolumns = nlapiLookupField('Location', solocation, fields);
							if(locationcolumns!=null)
								mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
						}
					}


					if(sonumber != soname)
					{
						sonumber = soname;

						try
						{
							vcustomer = nlapiLookupField('salesorder', vinternalid, 'entity');
							createfo = fnCreateFo(vinternalid);
						}
						catch(exp)
						{
							nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
							createfo='T';
						}

						nlapiLogExecution('DEBUG','createfo',createfo);

						if(shpcompleteflag != null && shpcompleteflag == 'T'  && createfo=='T')
						{
							try
							{
								createfo = fnShipcompleteFo(vinternalid,salesOrderLineDetails,sonumber);
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
								createfo='T';
							}

							nlapiLogExecution('DEBUG','Create FO',createfo);
						}						
					}				
				}

				var str = 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
				str = str + 'createfo. = ' + createfo + '<br>';
				str = str + 'mwhsiteflag. = ' + mwhsiteflag + '<br>';

				nlapiLogExecution('DEBUG', 'Flag Details1', str);		

				if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T' && mwhsiteflag=='T')
				{
					try
					{
						if(soname != null && soname != '')
						{	
							//To lock the sales order
							nlapiLogExecution('DEBUG','soname before Locking',soname);
							nlapiLogExecution('DEBUG','trantype before Locking','salesorder');
							var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
							fulfillRec.setFieldValue('name', soname);					
							fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'salesorder');
							fulfillRec.setFieldValue('externalid', soname);
							fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by Scheduler');
							nlapiSubmitRecord(fulfillRec,false,true );
							nlapiLogExecution('DEBUG','Locked successfully');
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
						nlapiLogExecution('DEBUG', 'DEBUG', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.');
						//var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
						//throw wmsE;
						continue;
					}
					try
					{
						var variance=0;
						var totalQuantity = getFulfilmentqty(salesOrderLineDetails[i].getId(), salesOrderLineNo);
						nlapiLogExecution('DEBUG','Remaining Usage 3',context.getRemainingUsage());
						var str = 'SO Name. = ' + soname + '<br>';
						str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
						str = str + 'SO Location. = ' + solocation + '<br>';
						str = str + 'Customization Code. = ' + customizationcode + '<br>';
						str = str + 'Order Qty. = ' + orderqty + '<br>';
						str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
						str = str + 'Commetted Qty. = ' + commetedQty + '<br>';
						str = str + 'Created FO Qty. = ' + totalQuantity + '<br>';
						str = str + 'Shipped Qty. = ' + shippedqty + '<br>';
						str = str + 'Ship Method. = ' + solineshipmethod + '<br>';
						str = str + 'Ship To. = ' + solineshipto + '<br>';
						str = str + 'solineCreateFulfillFlag. = ' + solineCreateFulfillFlag + '<br>';
						nlapiLogExecution('DEBUG', 'SO Line Details', str);	
						if(solineCreateFulfillFlag != null && solineCreateFulfillFlag == 'T')
						{
							var totalcomtdqty=parseFloat(commetedQty)+parseFloat(shippedqty);

							if(totalQuantity == 0)
								variance=commetedQty;
							else
							{
								if(parseFloat(totalQuantity) < parseFloat(totalcomtdqty))
								{
									variance=parseFloat(totalcomtdqty)-parseFloat(totalQuantity);
								}
							}

							nlapiLogExecution('DEBUG','Variance1',variance);

							if(variance > 0)
							{
								//-------------------------------------------------Change by Ganesh on 16th Jan 2013
								if((parseFloat(variance)+ parseFloat(shippedqty)  + parseFloat(totalQuantity))>parseFloat(totalcomtdqty))
								{
									var totalInTransitFOQuantity = getFulfilmentqtyInProgNew(salesOrderLineDetails[i].getId(), salesOrderLineNo);
									nlapiLogExecution('DEBUG', 'Out of getFulfilmentqtyInProgNew - totalInTransitFOQuantity',totalInTransitFOQuantity);
									variance = parseFloat(totalcomtdqty) - parseFloat(shippedqty) - parseFloat(totalInTransitFOQuantity);

								}
								//-----------------------------------------------------------------------
							}	

							nlapiLogExecution('DEBUG','Variance2',variance);

							if(variance > 0)
							{
								if(oldsoname!=soname)
								{
									//generate new fullfillment order no.
									/***Below code is merged from Lexjet Production by Ganesh K on 4th Mar 2013***/
									//fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
									fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod,solineshipto);
									oldsoname=soname;
									var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod,solineshipto];
									fonumberarr.push(curRow);
								}
								else
								{
									if(fonumberarr!=null && fonumberarr!='' && fonumberarr.length>0)
									{
										var fonumberfound = 'F';
										for(var t=0;t<fonumberarr.length;t++)
										{
											if(fonumberarr[t][0]==soname && fonumberarr[t][2]==customizationcode && fonumberarr[t][3]==solocation 
													&& fonumberarr[t][4]==solineshipmethod && fonumberarr[t][5]==solineshipto)
											{
												fullFillmentorderno=fonumberarr[t][1];
												fonumberfound='T';
											}
										}
										if(fonumberfound=='F')
										{
											//fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
											fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod,solineshipto);
											var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod,solineshipto];
											/***upto here***/
											fonumberarr.push(curRow);
										}
									}
								}
								nlapiLogExecution('DEBUG','Remaining Usage 4',context.getRemainingUsage());
								nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);

								if(fullFillmentorderno!=null && fullFillmentorderno!='')
								{
									var fulillmentarray = getFullFillmentbyline(salesOrderLineDetails[i].getId(),salesOrderLineNo);
									if(fulillmentarray!=null && fulillmentarray!='' && fulillmentarray.length>0)
									{
										nlapiLogExecution('DEBUG','fulillmentarray length',fulillmentarray.length);	

										var existordqty = fulillmentarray[0].getValue('custrecord_ord_qty');
										var fointernalid = fulillmentarray[0].getId();
										var totalordqty = parseFloat(existordqty)+parseFloat(variance);

										var str = 'existordqty. = ' + existordqty + '<br>';
										str = str + 'totalordqty. = ' + totalordqty + '<br>';	
										str = str + 'variance. = ' + variance + '<br>';

										nlapiLogExecution('DEBUG', 'Qty Details', str);		

										nlapiLogExecution('DEBUG','Updating Fulfillment Order Line...');
										nlapiSubmitField('customrecord_ebiznet_ordline', fointernalid, 'custrecord_ord_qty', totalordqty);
									}
									else
									{
										createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,salesOrderLineDetails[i],soname,salesOrderLineDetails[i].getId(),vcustomer,lineseqno,getinstdata,getlineinstdata);
									}					

								}
								nlapiLogExecution('DEBUG','Remaining Usage 5',context.getRemainingUsage());							
							}
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG','exception in FO scheduler',e.message);
					}
					finally
					{
						//To unlock the sales order
						nlapiLogExecution('DEBUG','soname before unlocking',soname);
						nlapiLogExecution('DEBUG','trantype before unlocking','salesorder');

						if(soname != null && soname != '')
						{	
							var ExternalIdFilters = new Array();
							ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
							ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'salesorder'));
							var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
							if(searchresultsExt != null && searchresultsExt != '')
							{
								nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
								nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
								nlapiLogExecution('DEBUG','Unlocked successfully');
							}	
						}
					}
				}			
				//Update seq no in wave table if the remaining use is less than 500
				nlapiLogExecution('DEBUG','Remaining Usage',context.getRemainingUsage());
				if(context.getRemainingUsage()<500)
				{
					nlapiLogExecution('DEBUG','break');					
					break;
				}
			}			
		}
	}
	nlapiLogExecution('DEBUG','Remaining Usage at the end ',context.getRemainingUsage());	
	nlapiLogExecution('DEBUG','Out of  CreateFO_SaveSearch','');
}



function GetAllInternalID_SavedSearch()
{
	try
	{
		nlapiLogExecution('DEBUG','Into GetAllInternalID_SavedSearch');
//		var resultSet=nlapiSearchRecord('Transaction','customsearch_focreation_savedsearch',null,null);
		var resultSet=nlapiSearchRecord('Transaction','customsearch_missing_fo_qtys',null,null);
		var SOInternalIdArray=new Array();
		if(resultSet!=null&&resultSet!="")
		{
			for(var i=0;i<resultSet.length;i++)
			{
				//SOInternalIdArray.push(resultSet[i].getValue('internalid',null,'group'));
				Soid=resultSet[i].getValue('internalid',null,'group');
				nlapiLogExecution('DEBUG','Soid',Soid);
				try
				{
					createfo = fnCreateFo(Soid);
				}
				catch(exp)
				{
					nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
					createfo='T';
				}
				nlapiLogExecution('DEBUG','createfo',createfo);
				if(createfo=='T')
				{
					SOInternalIdArray.push(Soid);
				}

			}
		}
		nlapiLogExecution('DEBUG','SOInternalIdArray',SOInternalIdArray);
		return SOInternalIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetAllInternalID_SavedSearch',exp);
	}
}

function getCommittedlinescount(soid)
{
	var committedlinescount=0;

	nlapiLogExecution('ERROR','Into getCommittedlinescount',soid);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));	
	filters.push(new nlobjSearchFilter('quantitycommitted', null, 'greaterthan', 0));
	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('custbody_hold_release_status', null, 'noneof', [3]));
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', [soid]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');	
	columns[1] = new nlobjSearchColumn('line');

	var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);

	if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
	{
		committedlinescount = salesOrderLineDetails.length;
	}

	nlapiLogExecution('ERROR','Out of getCommittedlinescount',committedlinescount);	

	return committedlinescount;
}

function CheckLocationAlias(location)
{
	var OrginalLocation=null;
	var filters= new Array();
	if(location!=null && location!='' && location!='null')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_loc_location',null,'anyof',location));
		var columns=new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiz_loc_aliaslocation'));
		resultSet=nlapiSearchRecord('customrecord_ebiz_locationalias',null,filters,columns);
		if(resultSet!=null && resultSet!='' && resultSet.length>0)
		{
			OrginalLocation=resultSet[0].getValue('custrecord_ebiz_loc_aliaslocation');
		}
	}
	return OrginalLocation;

}

function releaseOrders()
{
	nlapiLogExecution('DEBUG','Into  releaseOrders','');
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());

	var soids = context.getSetting('SCRIPT', 'custscript_sointrids');
	var ebizwaveno = context.getSetting('SCRIPT', 'custscript_waveno');

	var vConfig=nlapiLoadConfiguration('companyfeatures');
	var UOMruleValue=vConfig.getFieldValue('unitsofmeasure');

	var vInternalIdArray=new Array();

	var vInternalIdArray=soids.toString().split(',');

	nlapiLogExecution('DEBUG','SO Internal Ids',vInternalIdArray);
	nlapiLogExecution('DEBUG','Wave #',ebizwaveno);

	var isfocreated='F';

	if(vInternalIdArray!=null&&vInternalIdArray!="")
	{
		var vintrid=0;
		var vinternalid=0;

		var filters = new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
		filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0
		// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
		filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
		filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
		filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', vInternalIdArray));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');	
		columns[1] = new nlobjSearchColumn('line');
		columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
		columns[3] = new nlobjSearchColumn('quantitycommitted');
		columns[4] = new nlobjSearchColumn('quantity');
		columns[5] = new nlobjSearchColumn('trandate');
		columns[6] = new nlobjSearchColumn('entity');
		columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
		columns[8] = new nlobjSearchColumn('shipmethod');
		columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
		columns[10] = new nlobjSearchColumn('custbody_nswms_company');
		columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
		columns[12] = new nlobjSearchColumn('location');
		columns[13] = new nlobjSearchColumn('item');
		columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
		columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
		columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');

		columns[17] = new nlobjSearchColumn('tranid');
		columns[18] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
		columns[19] = new nlobjSearchColumn('quantityshiprecv');
		columns[20] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
		columns[21] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
		columns[22] = new nlobjSearchColumn('custbody_nswmssoordertype');
		columns[23] = new nlobjSearchColumn('custbody_nswmspoexpshipdate');
		columns[24] = new nlobjSearchColumn('linesequencenumber');
		if(UOMruleValue == 'T')
		{
			columns[25] = new nlobjSearchColumn('unit');
		}

		//true - Discending false - Ascending
		columns[0].setSort(true);
		columns[1].setSort();

		var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);
		nlapiLogExecution('DEBUG','Remaining Usage 2',context.getRemainingUsage());
		if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
		{	
			var createfo = 'T'; //Added 08-Nov-2011
			var sonumber = 0; //Added 08-Nov-2011
			var oldsoname='';
			var fullFillmentorderno='';
			var fonumberarr = new Array();
			var vcustomer ='';

			nlapiLogExecution('DEBUG','salesOrderLineDetails.length ',salesOrderLineDetails.length);
			for(var i=0;i<salesOrderLineDetails.length;i++)
			{
				nlapiLogExecution('DEBUG','# Loop',i);
				vinternalid=salesOrderLineDetails[i].getValue('internalid');
				nlapiLogExecution('DEBUG','vinternalid',vinternalid);

				isfocreated='T';

				var soname = salesOrderLineDetails[i].getValue('tranid');			
				var orderqty = salesOrderLineDetails[i].getValue('quantity');
				var create_fulfillment_order= salesOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
				var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
				var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
				if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
					commetedQty=0;

				var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
				var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');
				var solocation = salesOrderLineDetails[i].getValue('location');
				var solineshipmethod=salesOrderLineDetails[i].getValue('shipmethod');
				var lineseqno=salesOrderLineDetails[i].getValue('linesequencenumber');
				// Assuming search results will return the data in the descending order of SO#

				if(soname != null)
				{
					if(sonumber != soname)
					{
						sonumber = soname;
						try
						{
							vcustomer = nlapiLookupField('salesorder', vinternalid, 'entity');
							createfo = fnCreateFo(vinternalid);
						}
						catch(exp)
						{
							nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
							createfo='T';
						}

						if(shpcompleteflag != null && shpcompleteflag == 'T'   && createfo=='T')
						{						
							for(var j=0;j<salesOrderLineDetails.length;j++)
							{
								if(sonumber == salesOrderLineDetails[j].getValue('tranid'))
								{
									var itemtype='';
									var solineordqty = salesOrderLineDetails[j].getValue('quantity');
									var solinecmmtdqty = salesOrderLineDetails[j].getValue('quantitycommitted');
									var lineitem=salesOrderLineDetails[j].getValue('item');
									var shipmethod=salesOrderLineDetails[j].getValue('shipmethod');
									if(solinecmmtdqty==null || solinecmmtdqty=='' || isNaN(solinecmmtdqty))
										solinecmmtdqty=0;

									var solinefulfildqty = salesOrderLineDetails[j].getValue('quantityshiprecv');

									if(solinefulfildqty == null || solinefulfildqty == '' || isNaN(solinefulfildqty))
										solinefulfildqty = 0;

									var totalcomtdqty=parseInt(solinecmmtdqty)+parseInt(solinefulfildqty);

									if(lineitem!=null && lineitem!='')
										itemtype = nlapiLookupField('item', lineitem, 'recordType');	

									nlapiLogExecution('DEBUG','itemtype',itemtype);

									if(itemtype!='descriptionitem' && itemtype!='discountitem' && itemtype!='Payment' && itemtype!='Markup' 
										&& itemtype!='Subtotal'  && itemtype!='otherchargeitem' && itemtype!='serviceitem' && itemtype!='noninventoryitem'
											&& itemtype!='NonInvtPart')
									{
										if(parseInt(solineordqty) > parseInt(totalcomtdqty))
										{
											createfo = 'F';
											continue;
										}
									}
								}

							}

							nlapiLogExecution('DEBUG','Create FO',createfo);
						}						
					}				
				}


				nlapiLogExecution('DEBUG','create_fulfillment_order',create_fulfillment_order);
				nlapiLogExecution('DEBUG','createfo',createfo);
				if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T')
				{
					try
					{
						if(soname != null && soname != '')
						{	
							//To lock the sales order
							//nlapiLogExecution('DEBUG','soname before Locking',soname);
							//nlapiLogExecution('DEBUG','trantype before Locking','salesorder');
							var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
							fulfillRec.setFieldValue('name', soname);					
							fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'salesorder');
							fulfillRec.setFieldValue('externalid', soname);
							fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by Scheduler');
							nlapiSubmitRecord(fulfillRec,false,true );
							nlapiLogExecution('DEBUG','Locked successfully');
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
						nlapiLogExecution('DEBUG', 'DEBUG', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.');
						//var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
						//throw wmsE;
						continue;
					}
					try
					{
						var variance=0;
						//DeleteRecCreated(salesOrderLineDetails[i].getId(), salesOrderLineNo);
						var totalQuantity = getFulfilmentqty(salesOrderLineDetails[i].getId(), salesOrderLineNo);
						nlapiLogExecution('DEBUG','Remaining Usage 3',context.getRemainingUsage());
						nlapiLogExecution('DEBUG','SO Name',soname);
						nlapiLogExecution('DEBUG','SO Line',salesOrderLineNo);
						nlapiLogExecution('DEBUG','Order Qty',orderqty);
						nlapiLogExecution('DEBUG','Commetted Qty',commetedQty);
						nlapiLogExecution('DEBUG','Created FO Qty',totalQuantity);
						nlapiLogExecution('DEBUG','Shipped Qty',shippedqty);
						var totalcomtdqty=parseInt(commetedQty)+parseInt(shippedqty);

						if(totalQuantity == 0)
							variance=commetedQty;
						else
						{
							if(parseInt(totalQuantity) < parseInt(totalcomtdqty))
							{
								variance=parseInt(totalcomtdqty)-parseInt(totalQuantity);
							}
						}

						nlapiLogExecution('DEBUG','Variance',variance);

						if(variance > 0)
						{

							if(oldsoname!=soname)
							{
								//generate new fullfillment order no.
								fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod);
								oldsoname=soname;
								var curRow = [soname,fullFillmentorderno,solocation,solineshipmethod];
								fonumberarr.push(curRow);
							}
							else
							{
								if(fonumberarr!=null && fonumberarr!='' && fonumberarr.length>0)
								{
									var fonumberfound = 'F';
									for(var t=0;t<fonumberarr.length;t++)
									{
										if(fonumberarr[t][0]==soname&&fonumberarr[t][2]==solocation&& fonumberarr[t][3]==solineshipmethod)
										{
											fullFillmentorderno=fonumberarr[t][1];
											fonumberfound='T';
										}
									}
									if(fonumberfound=='F')
									{
										fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
										var curRow = [soname,fullFillmentorderno,solocation,solineshipmethod];
										fonumberarr.push(curRow);
									}
								}
							}
							nlapiLogExecution('DEBUG','Remaining Usage 4',context.getRemainingUsage());
							nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);

							if(fullFillmentorderno!=null && fullFillmentorderno!='')
							{
								var fulillmentarray = getFullFillmentbyline(salesOrderLineDetails[i].getId(),salesOrderLineNo);
								if(fulillmentarray!=null && fulillmentarray!='' && fulillmentarray.length>0)
								{
									nlapiLogExecution('DEBUG','fulillmentarray length',fulillmentarray.length);	

									var existordqty = fulillmentarray[0].getValue('custrecord_ord_qty');
									var fointernalid = fulillmentarray[0].getId();
									var totalordqty = parseFloat(existordqty)+parseFloat(variance);

									nlapiLogExecution('DEBUG','existordqty',existordqty);	
									nlapiLogExecution('DEBUG','variance',variance);
									nlapiLogExecution('DEBUG','totalordqty',totalordqty);

									nlapiLogExecution('DEBUG','Updating Fulfillment Order Line...');
									nlapiSubmitField('customrecord_ebiznet_ordline', fointernalid, 'custrecord_ord_qty', totalordqty);
								}
								else
								{
									createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,salesOrderLineDetails[i],soname,salesOrderLineDetails[i].getId(),vcustomer,lineseqno);
								}					

							}
							nlapiLogExecution('DEBUG','Remaining Usage 5',context.getRemainingUsage());							
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG','Exception in FO Scheduler',e.message);	
					}
					finally{
						//To unlock the sales order
						nlapiLogExecution('DEBUG','soname before unlocking',soname);
						nlapiLogExecution('DEBUG','trantype before unlocking','salesorder');

						if(soname != null && soname != '')
						{	
							var ExternalIdFilters = new Array();
							ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
							ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'salesorder'));
							var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
							if(searchresultsExt != null && searchresultsExt != '')
							{
								nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
								nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
								nlapiLogExecution('DEBUG','Unlocked successfully');
							}	
						}
					}
				}			
				//Update seq no in wave table if the remaining use is less than 500
				if(context.getRemainingUsage()<500)
				{	
					isfocreated='F';
					var param = new Array();
					param['custscript_sointrids'] = soids;
					param['custscript_waveno'] = ebizwaveno;

					nlapiScheduleScript('customscript_ebiz_release_orders_sch', null,param);

					nlapiLogExecution('DEBUG', 'Invoking Schedule Script Ends', TimeStampinSec());
					break;
				}
			}
			nlapiLogExecution('DEBUG','salesOrderLineDetails.length2 ',salesOrderLineDetails.length);

			if(isfocreated=='T')
			{
				updateWave(ebizwaveno,vInternalIdArray);
				var param = new Array();
				param['custscript_ebizwaveno'] = ebizwaveno;
				nlapiScheduleScript('customscript_ebiz_pickgen_scheduler', null,param);
			}
		}

		nlapiLogExecution('DEBUG','Remaining Usage at the end ',context.getRemainingUsage());			
	}
	nlapiLogExecution('DEBUG','Out of  releaseOrders','');
}

function updateWave(ebizwaveno,vInternalIdArray)
{
	nlapiLogExecution('DEBUG','Into updateWave',ebizwaveno);
	nlapiLogExecution('DEBUG','vInternalIdArray',vInternalIdArray);

	var fofilters = new Array();
	var focolumns = new Array();

	fofilters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', vInternalIdArray));
	fofilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [25]));

	var folineresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, fofilters, focolumns);
	if(folineresults!=null && folineresults!='')
	{
		for (var i = 0; i < folineresults.length; i++){

			var recordId = folineresults[i].getId();

			var fieldNames = new Array(); 
			fieldNames.push('custrecord_linestatus_flag');  
			fieldNames.push('custrecord_ebiz_wave');
			fieldNames.push('custrecord_ebiz_fowavedate');

			var newValues = new Array(); 
			newValues.push(15);
			newValues.push(parseInt(ebizwaveno));
			newValues.push(DateStamp());

			nlapiSubmitField('customrecord_ebiznet_ordline', recordId, fieldNames, newValues);

		}
	}
	nlapiLogExecution('DEBUG','Out of updateWave',ebizwaveno);

}

function CreateFO1s_SavedSearch(type)
{
	nlapiLogExecution('ERROR','Into  CreateFO1s_SavedSearch','');
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());


	var vConfig=nlapiLoadConfiguration('companyfeatures');
	var UOMruleValue=vConfig.getFieldValue('unitsofmeasure');
	nlapiLogExecution('ERROR','UOMruleValue',UOMruleValue);

	var vInternalIdArray=GetAllInternalIDs();

	nlapiLogExecution('ERROR','vInternalIdArray',vInternalIdArray);

	if(vInternalIdArray!=null&&vInternalIdArray!="")
	{		
		var filters = new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
		filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0

		// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
		filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
		//filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
		filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
		filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
		filters.push(new nlobjSearchFilter('custbody_ebiz_ord_size', null, 'isnot', 'L')); // To process less than 100 line orders only
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', vInternalIdArray));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');	
		columns[1] = new nlobjSearchColumn('line');
		columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
		columns[3] = new nlobjSearchColumn('quantitycommitted');
		columns[4] = new nlobjSearchColumn('quantity');
		columns[5] = new nlobjSearchColumn('trandate');
		columns[6] = new nlobjSearchColumn('entity');
		columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
		columns[8] = new nlobjSearchColumn('shipmethod');
		columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
		columns[10] = new nlobjSearchColumn('custbody_nswms_company');
		columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
		columns[12] = new nlobjSearchColumn('location');
		columns[13] = new nlobjSearchColumn('item');
		columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
		columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
		columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');

		columns[17] = new nlobjSearchColumn('tranid');
		columns[18] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
		columns[19] = new nlobjSearchColumn('quantityshiprecv');
		columns[20] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
		columns[21] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
		columns[22] = new nlobjSearchColumn('custcol_customizationcode');
		columns[23] = new nlobjSearchColumn('custbody_nswmspoexpshipdate');
		columns[24] = new nlobjSearchColumn('custbody_nswmssoordertype');
		columns[25] = new nlobjSearchColumn('custbody_shipment_no');
		columns[26] = new nlobjSearchColumn('shipdate');
		columns[27] = new nlobjSearchColumn('custcol_nswms_shipment_no');
		//	columns[28] = new nlobjSearchColumn('quantityuom');
		columns[28] = new nlobjSearchColumn('shipattention');
		columns[29] = new nlobjSearchColumn('custcol_create_fulfillment_order');
		columns[30] = new nlobjSearchColumn('linesequencenumber');
		columns[31] = new nlobjSearchColumn('custbody_nswmspoinstructions');
		columns[32] = new nlobjSearchColumn('custcol_wms_rfinstructions');
		if(UOMruleValue == 'T')
		{
			columns[33] = new nlobjSearchColumn('unit');
			columns[34] = new nlobjSearchColumn('quantityuom');
		}		
		//true - Discending false - Ascending
		columns[0].setSort(true);
		columns[1].setSort();

		var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);
		nlapiLogExecution('DEBUG','Remaining Usage 2',context.getRemainingUsage());
		if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
		{	

			var sonumber = 0; //Added 08-Nov-2011
			var oldsoname='';
			var fullFillmentorderno='';
			var fonumberarr = new Array();
			var vtempflag=false;
			var createfo = 'T'; //Added 08-Nov-2011
			var vcustomer = '';

			nlapiLogExecution('DEBUG','salesOrderLineDetails.length ',salesOrderLineDetails.length);
			for(var i=0;i<salesOrderLineDetails.length;i++)
			{			
				var mwhsiteflag='F';
				vinternalid=salesOrderLineDetails[i].getValue('internalid');
				var soname = salesOrderLineDetails[i].getValue('tranid');	
				var solocation = salesOrderLineDetails[i].getValue('location');
				var orderqty = salesOrderLineDetails[i].getValue('quantity');
				var create_fulfillment_order= salesOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
				var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
				var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
				var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
				var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');
				var customizationcode = salesOrderLineDetails[i].getValue('custcol_customizationcode');
				var uomconvqty = '';
				var unit = '';
				if(UOMruleValue == 'T')
				{
					unit = salesOrderLineDetails[i].getValue('unit');
					uomconvqty = salesOrderLineDetails[i].getValue('quantityuom');
				}

				/***Below code is merged from Lexjet Production by Ganesh K on 4th Mar 2013***/
				var solineshipmethod = salesOrderLineDetails[i].getValue('shipmethod');
				var solineCreateFulfillFlag = salesOrderLineDetails[i].getValue('custcol_create_fulfillment_order');
				var getinstdata = salesOrderLineDetails[i].getValue('custbody_nswmspoinstructions');
				nlapiLogExecution('DEBUG','getinstdata in fo_saved1',getinstdata);

				var getlineinstdata = salesOrderLineDetails[i].getValue('custcol_wms_rfinstructions');
				nlapiLogExecution('DEBUG','getlineinstdata in fo_saved1',getlineinstdata);

				var linesquno = salesOrderLineDetails[i].getValue('linesequencenumber');
				/***upto here***/
				var solineshipto = salesOrderLineDetails[i].getValue('shipattention');
				var conversionrate = 1;
				if(unit!=null && unit!='' && uomconvqty!=null && uomconvqty!='')
				{
					conversionrate = parseFloat(orderqty)/parseFloat(uomconvqty);
				}


				var str = '# Loop. = ' + i + '<br>';
				str = str + 'vinternalid. = ' + vinternalid + '<br>';
				str = str + 'SO Name. = ' + soname + '<br>';
				str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
				str = str + 'SO Location. = ' + solocation + '<br>';
				str = str + 'Customization Code. = ' + customizationcode + '<br>';
				str = str + 'Order Qty. = ' + orderqty + '<br>';
				str = str + 'Unit. = ' + unit + '<br>';
				str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
				str = str + 'UOM Conversion Rate. = ' + conversionrate + '<br>';
				str = str + 'Commetted Qty. = ' + commetedQty + '<br>';			
				str = str + 'Shipped Qty. = ' + shippedqty + '<br>';
				str = str + 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
				str = str + 'Ship Method. = ' + solineshipmethod + '<br>';
				str = str + 'Ship To. = ' + solineshipto + '<br>';
				str = str + 'solineCreateFulfillFlag. = ' + solineCreateFulfillFlag + '<br>';
				nlapiLogExecution('DEBUG', 'SO Line Details1', str);		

				if(customizationcode==null || customizationcode=='')
					customizationcode=-1;

				if(solineshipto==null || solineshipto=='')
					solineshipto=-1;

				if(uomconvqty==null || uomconvqty=='' || isNaN(uomconvqty))
					uomconvqty=1;

				if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
					commetedQty=0;

				if(shippedqty==null || shippedqty=='' || isNaN(shippedqty))
					shippedqty=0;

				if(unit!=null && unit!='')
				{
					commetedQty = parseFloat(commetedQty)/parseFloat(conversionrate);
					shippedqty = parseFloat(shippedqty)/parseFloat(conversionrate);
				}

				// Assuming search results will return the data in the descending order of SO#

				if(soname != null)
				{
					var	islocationAliasExists=CheckLocationAlias(solocation);
					if(islocationAliasExists!=null && islocationAliasExists!='')
					{
						mwhsiteflag='T';
					}
					else
					{
						if(solocation!=null && solocation!='')
						{
							var fields = ['custrecord_ebizwhsite'];

							var locationcolumns = nlapiLookupField('Location', solocation, fields);
							if(locationcolumns!=null)
								mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
						}
					}


					if(sonumber != soname)
					{
						sonumber = soname;

						try
						{
							vcustomer = nlapiLookupField('salesorder', vinternalid, 'entity');
							createfo = fnCreateFo(vinternalid);
						}
						catch(exp)
						{
							nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
							createfo='T';
						}

						nlapiLogExecution('DEBUG','createfo',createfo);

						if(shpcompleteflag != null && shpcompleteflag == 'T'  && createfo=='T')
						{	


							try
							{
								createfo = fnShipcompleteFo(vinternalid,salesOrderLineDetails,sonumber);
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
								createfo='T';
							}



							nlapiLogExecution('DEBUG','Create FO',createfo);
						}						
					}				
				}

				var str = 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
				str = str + 'createfo. = ' + createfo + '<br>';
				str = str + 'mwhsiteflag. = ' + mwhsiteflag + '<br>';

				nlapiLogExecution('DEBUG', 'Flag Details1', str);		

				if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T' && mwhsiteflag=='T')
				{
					try
					{
						if(soname != null && soname != '')
						{	
							//To lock the sales order
							nlapiLogExecution('DEBUG','soname before Locking',soname);
							nlapiLogExecution('DEBUG','trantype before Locking','salesorder');
							var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
							fulfillRec.setFieldValue('name', soname);					
							fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'salesorder');
							fulfillRec.setFieldValue('externalid', soname);
							fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by Scheduler');
							nlapiSubmitRecord(fulfillRec,false,true );
							nlapiLogExecution('DEBUG','Locked successfully');
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
						nlapiLogExecution('DEBUG', 'DEBUG', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.');
						//var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
						//throw wmsE;
						continue;
					}
					try
					{
						var variance=0;
						var totalQuantity = getFulfilmentqty(salesOrderLineDetails[i].getId(), salesOrderLineNo);
						nlapiLogExecution('DEBUG','Remaining Usage 3',context.getRemainingUsage());
						var str = 'SO Name. = ' + soname + '<br>';
						str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
						str = str + 'SO Location. = ' + solocation + '<br>';
						str = str + 'Customization Code. = ' + customizationcode + '<br>';
						str = str + 'Order Qty. = ' + orderqty + '<br>';
						str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
						str = str + 'Commetted Qty. = ' + commetedQty + '<br>';
						str = str + 'Created FO Qty. = ' + totalQuantity + '<br>';
						str = str + 'Shipped Qty. = ' + shippedqty + '<br>';
						str = str + 'Ship Method. = ' + solineshipmethod + '<br>';
						str = str + 'Ship To. = ' + solineshipto + '<br>';
						str = str + 'solineCreateFulfillFlag. = ' + solineCreateFulfillFlag + '<br>';
						nlapiLogExecution('DEBUG', 'SO Line Details', str);		
						if(solineCreateFulfillFlag != null && solineCreateFulfillFlag == 'T')
						{							
							var totalcomtdqty=parseFloat(commetedQty)+parseFloat(shippedqty);

							if(totalQuantity == 0)
								variance=commetedQty;
							else
							{
								if(parseFloat(totalQuantity) < parseFloat(totalcomtdqty))
								{
									variance=parseFloat(totalcomtdqty)-parseFloat(totalQuantity);
								}
							}

							nlapiLogExecution('DEBUG','Variance1',variance);

							if(variance > 0)
							{
								//-------------------------------------------------Change by Ganesh on 16th Jan 2013
								if((parseFloat(variance)+ parseFloat(shippedqty)  + parseFloat(totalQuantity))>parseFloat(totalcomtdqty))
								{
									var totalInTransitFOQuantity = getFulfilmentqtyInProgNew(salesOrderLineDetails[i].getId(), salesOrderLineNo);
									nlapiLogExecution('DEBUG', 'Out of getFulfilmentqtyInProgNew - totalInTransitFOQuantity',totalInTransitFOQuantity);
									variance = parseFloat(totalcomtdqty) - parseFloat(shippedqty) - parseFloat(totalInTransitFOQuantity);

								}
								//-----------------------------------------------------------------------
							}	

							nlapiLogExecution('DEBUG','Variance2',variance);

							if(variance > 0)
							{
								if(oldsoname!=soname)
								{
									//generate new fullfillment order no.
									/***Below code is merged from Lexjet Production by Ganesh K on 4th Mar 2013***/
									//fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
									fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod,solineshipto);
									oldsoname=soname;
									var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod,solineshipto];
									fonumberarr.push(curRow);
								}
								else
								{
									if(fonumberarr!=null && fonumberarr!='' && fonumberarr.length>0)
									{
										var fonumberfound = 'F';
										for(var t=0;t<fonumberarr.length;t++)
										{
											if(fonumberarr[t][0]==soname && fonumberarr[t][2]==customizationcode && fonumberarr[t][3]==solocation 
													&& fonumberarr[t][4]==solineshipmethod && fonumberarr[t][5]==solineshipto)
											{
												fullFillmentorderno=fonumberarr[t][1];
												fonumberfound='T';
											}
										}
										if(fonumberfound=='F')
										{
											//fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
											fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod,solineshipto);
											var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod,solineshipto];
											/***upto here***/
											fonumberarr.push(curRow);
										}
									}
								}
								nlapiLogExecution('DEBUG','Remaining Usage 4',context.getRemainingUsage());
								nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);

								if(fullFillmentorderno!=null && fullFillmentorderno!='')
								{
									var fulillmentarray = getFullFillmentbyline(salesOrderLineDetails[i].getId(),salesOrderLineNo);
									if(fulillmentarray!=null && fulillmentarray!='' && fulillmentarray.length>0)
									{
										nlapiLogExecution('DEBUG','fulillmentarray length',fulillmentarray.length);	

										var existordqty = fulillmentarray[0].getValue('custrecord_ord_qty');
										var fointernalid = fulillmentarray[0].getId();
										var totalordqty = parseFloat(existordqty)+parseFloat(variance);

										var str = 'existordqty. = ' + existordqty + '<br>';
										str = str + 'totalordqty. = ' + totalordqty + '<br>';	
										str = str + 'variance. = ' + variance + '<br>';

										nlapiLogExecution('DEBUG', 'Qty Details', str);		

										nlapiLogExecution('DEBUG','Updating Fulfillment Order Line...');
										nlapiSubmitField('customrecord_ebiznet_ordline', fointernalid, 'custrecord_ord_qty', totalordqty);
									}
									else
									{
										createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,salesOrderLineDetails[i],soname,salesOrderLineDetails[i].getId(),vcustomer,linesquno,getinstdata,getlineinstdata);
									}					

								}
								nlapiLogExecution('DEBUG','Remaining Usage 5',context.getRemainingUsage());							
							}
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG','exception in FO scheduler',e.message);
					}
					finally
					{
						//To unlock the sales order
						nlapiLogExecution('DEBUG','soname before unlocking',soname);
						nlapiLogExecution('DEBUG','trantype before unlocking','salesorder');

						if(soname != null && soname != '')
						{	
							var ExternalIdFilters = new Array();
							ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
							ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'salesorder'));
							var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
							if(searchresultsExt != null && searchresultsExt != '')
							{
								nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
								nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
								nlapiLogExecution('DEBUG','Unlocked successfully');
							}	
						}
					}
				}			
				//Update seq no in wave table if the remaining use is less than 500
				nlapiLogExecution('DEBUG','Remaining Usage',context.getRemainingUsage());
				if(context.getRemainingUsage()<500)
				{
					nlapiLogExecution('DEBUG','break');					
					break;
				}
			}			
		}
	}
	nlapiLogExecution('ERROR','Out of  CreateFO1s_SavedSearch','');
}

function GetAllInternalIDs()
{
	try
	{
		nlapiLogExecution('ERROR','Into GetAllInternalIDs');
		var resultSet=nlapiSearchRecord('Transaction','customsearch_ebn_salesorder_fo_summary',null,null);
		var SOInternalIdArray=new Array();
		if(resultSet!=null&&resultSet!="")
		{
			nlapiLogExecution('ERROR','resultSet length',resultSet.length);
			for(var i=0;i<resultSet.length;i++)
			{
				SOInternalIdArray.push(resultSet[i].getValue('internalid'));
			}
		}
		nlapiLogExecution('ERROR','SOInternalIdArray',SOInternalIdArray);
		return SOInternalIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetAllInternalIDs',exp);
	}
}

function processLargeOrders(type)
{
	nlapiLogExecution('DEBUG','Into  processLargeOrders','');
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());
	//if ( type != 'scheduled') return; 
	var vintrid=0;
	var vinternalid=0;

	var vConfig=nlapiLoadConfiguration('companyfeatures');
	var UOMruleValue=vConfig.getFieldValue('unitsofmeasure');
	nlapiLogExecution('ERROR','UOMruleValue',UOMruleValue);

	//var vInternalIdArray=GetAllInternalIDs();
	var vInternalIdArray=GetAllInternalID_SavedSearch();
	if(vInternalIdArray!=null&&vInternalIdArray!="")
	{
		nlapiLogExecution('DEBUG','Into  backOrderFulfillment','');
		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());
		//if ( type != 'scheduled') return; 

		var filters = new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
		filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0
		// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
		filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
		filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', vInternalIdArray));
		filters.push(new nlobjSearchFilter('custbody_ebiz_ord_size', null, 'is', 'L'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('tranid');	
		columns[1] = new nlobjSearchColumn('line');
		columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
		columns[3] = new nlobjSearchColumn('quantitycommitted');
		columns[4] = new nlobjSearchColumn('quantity');
		columns[5] = new nlobjSearchColumn('trandate');
		columns[6] = new nlobjSearchColumn('entity');
		columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
		columns[8] = new nlobjSearchColumn('shipmethod');
		columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
		columns[10] = new nlobjSearchColumn('custbody_nswms_company');
		columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
		columns[12] = new nlobjSearchColumn('location');
		columns[13] = new nlobjSearchColumn('item');
		columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
		columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
		columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');		
		columns[17] = new nlobjSearchColumn('internalid');
		columns[18] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
		columns[19] = new nlobjSearchColumn('quantityshiprecv');
		columns[20] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
		columns[21] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
		columns[22] = new nlobjSearchColumn('linesequencenumber');		
		if(UOMruleValue=='T')
		{
			columns[23] = new nlobjSearchColumn('unit');

		}
		//true - Discending false - Ascending
		columns[0].setSort(true);
		columns[1].setSort();

		var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);
		nlapiLogExecution('DEBUG','Remaining Usage 2',context.getRemainingUsage());
		if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
		{	
			var createfo = 'T'; //Added 08-Nov-2011
			var sonumber = 0; //Added 08-Nov-2011
			var oldsoname='';
			var fullFillmentorderno='';
			var fonumberarr = new Array();
			var vcustomer ='';

			nlapiLogExecution('DEBUG','salesOrderLineDetails.length ',salesOrderLineDetails.length);
			for(var i=0;i<salesOrderLineDetails.length;i++)
			{
				nlapiLogExecution('DEBUG','# Loop',i);
				vinternalid=salesOrderLineDetails[i].getValue('internalid');
				nlapiLogExecution('DEBUG','vinternalid',vinternalid);

				var soname = salesOrderLineDetails[i].getValue('tranid');			
				var orderqty = salesOrderLineDetails[i].getValue('quantity');
				var create_fulfillment_order= salesOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
				var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
				var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
				if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
					commetedQty=0;

				var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
				var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');
				var solocation = salesOrderLineDetails[i].getValue('location');
				var solineshipmethod=salesOrderLineDetails[i].getValue('shipmethod');
				var linesquno = salesOrderLineDetails[i].getValue('linesequencenumber');
				var solineshipto = salesOrderLineDetails[i].getValue('shipattention');
				// Assuming search results will return the data in the descending order of SO#

				if(soname != null)
				{
					if(sonumber != soname)
					{
						sonumber = soname;

						try
						{
							vcustomer = nlapiLookupField('salesorder', vinternalid, 'entity');	
							nlapiLogExecution('DEBUG','vcustomer in process large order',vcustomer);
							createfo = fnCreateFo(vinternalid);
						}
						catch(exp)
						{
							nlapiLogExecution('ERROR','Exception in fnCreateFo',exp);
							createfo='T';
						}

						nlapiLogExecution('DEBUG','createfo',createfo);

						if(shpcompleteflag != null && shpcompleteflag == 'T'   && createfo=='T')
						{						
							for(var j=0;j<salesOrderLineDetails.length;j++)
							{
								if(sonumber == salesOrderLineDetails[j].getValue('tranid'))
								{
									var solineordqty = salesOrderLineDetails[j].getValue('quantity');
									var solinecmmtdqty = salesOrderLineDetails[j].getValue('quantitycommitted');
									var lineitem=salesOrderLineDetails[j].getValue('item');
									var shipmethod=salesOrderLineDetails[j].getValue('shipmethod');
									if(solinecmmtdqty==null || solinecmmtdqty=='' || isNaN(solinecmmtdqty))
										solinecmmtdqty=0;

									var solinefulfildqty = salesOrderLineDetails[j].getValue('quantityshiprecv');

									if(solinefulfildqty == null || solinefulfildqty == '' || isNaN(solinefulfildqty))
										solinefulfildqty = 0;

									var totalcomtdqty=parseInt(solinecmmtdqty)+parseInt(solinefulfildqty);
									if(lineitem!=shipmethod)//Temporary fix to avoid unnecessary lines
									{
										if(parseInt(solineordqty) > parseInt(totalcomtdqty))
										{
											createfo = 'F';
											continue;
										}
									}
								}

							}

							nlapiLogExecution('DEBUG','Create FO',createfo);
						}						
					}				
				}

				if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T')
				{
					try
					{
						if(soname != null && soname != '')
						{	
							//To lock the sales order
							nlapiLogExecution('DEBUG','soname before Locking',soname);
							nlapiLogExecution('DEBUG','trantype before Locking','salesorder');
							var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
							fulfillRec.setFieldValue('name', soname);					
							fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'salesorder');
							fulfillRec.setFieldValue('externalid', soname);
							fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by Scheduler');
							nlapiSubmitRecord(fulfillRec,false,true );
							nlapiLogExecution('DEBUG','Locked successfully');
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
						nlapiLogExecution('DEBUG', 'DEBUG', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.');
						//var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
						//throw wmsE;
						continue;
					}
					try
					{
						var variance=0;
						//DeleteRecCreated(salesOrderLineDetails[i].getId(), salesOrderLineNo);
						var totalQuantity = getFulfilmentqty(salesOrderLineDetails[i].getId(), salesOrderLineNo);
						nlapiLogExecution('DEBUG','Remaining Usage 3',context.getRemainingUsage());
						nlapiLogExecution('DEBUG','SO Name',soname);
						nlapiLogExecution('DEBUG','SO Line',salesOrderLineNo);
						nlapiLogExecution('DEBUG','Order Qty',orderqty);
						nlapiLogExecution('DEBUG','Commetted Qty',commetedQty);
						nlapiLogExecution('DEBUG','Created FO Qty',totalQuantity);
						nlapiLogExecution('DEBUG','Shipped Qty',shippedqty);
						var totalcomtdqty=parseInt(commetedQty)+parseInt(shippedqty);

						if(totalQuantity == 0)
							variance=commetedQty;
						else
						{
							if(parseInt(totalQuantity) < parseInt(totalcomtdqty))
							{
								variance=parseInt(totalcomtdqty)-parseInt(totalQuantity);
							}
						}

						nlapiLogExecution('DEBUG','Variance',variance);

						if(variance > 0)
						{

							if(oldsoname!=soname)
							{
								//generate new fullfillment order no.
								fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod,solineshipto);
								oldsoname=soname;
								var curRow = [soname,fullFillmentorderno,solocation,solineshipmethod];
								fonumberarr.push(curRow);
							}
							else
							{
								if(fonumberarr!=null && fonumberarr!='' && fonumberarr.length>0)
								{
									var fonumberfound = 'F';
									for(var t=0;t<fonumberarr.length;t++)
									{
										if(fonumberarr[t][0]==soname&&fonumberarr[t][2]==solocation&& fonumberarr[t][3]==solineshipmethod)
										{
											fullFillmentorderno=fonumberarr[t][1];
											fonumberfound='T';
										}
									}
									if(fonumberfound=='F')
									{
										fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod,solineshipto);
										var curRow = [soname,fullFillmentorderno,solocation,solineshipmethod];
										fonumberarr.push(curRow);
									}
								}
							}
							nlapiLogExecution('DEBUG','Remaining Usage 4',context.getRemainingUsage());
							nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);

							if(fullFillmentorderno!=null && fullFillmentorderno!='')
							{
								var fulillmentarray = getFullFillmentbyline(salesOrderLineDetails[i].getId(),salesOrderLineNo);
								if(fulillmentarray!=null && fulillmentarray!='' && fulillmentarray.length>0)
								{
									nlapiLogExecution('DEBUG','fulillmentarray length',fulillmentarray.length);	

									var existordqty = fulillmentarray[0].getValue('custrecord_ord_qty');
									var fointernalid = fulillmentarray[0].getId();
									var totalordqty = parseFloat(existordqty)+parseFloat(variance);

									nlapiLogExecution('DEBUG','existordqty',existordqty);	
									nlapiLogExecution('DEBUG','variance',variance);
									nlapiLogExecution('DEBUG','totalordqty',totalordqty);

									nlapiLogExecution('DEBUG','Updating Fulfillment Order Line...');
									nlapiSubmitField('customrecord_ebiznet_ordline', fointernalid, 'custrecord_ord_qty', totalordqty);
								}
								else
								{
									nlapiLogExecution('DEBUG','vcustomer before calling function',vcustomer);
									createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,salesOrderLineDetails[i],soname,salesOrderLineDetails[i].getId(),vcustomer,linesquno);
								}					

							}
							nlapiLogExecution('DEBUG','Remaining Usage 5',context.getRemainingUsage());							
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG','Exception in FO Scheduler',e.message);	
					}
					finally{
						//To unlock the sales order
						nlapiLogExecution('DEBUG','soname before unlocking',soname);
						nlapiLogExecution('DEBUG','trantype before unlocking','salesorder');

						if(soname != null && soname != '')
						{	
							var ExternalIdFilters = new Array();
							ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
							ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'salesorder'));
							var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
							if(searchresultsExt != null && searchresultsExt != '')
							{
								nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
								nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
								nlapiLogExecution('DEBUG','Unlocked successfully');
							}	
						}
					}
				}			
				//Update seq no in wave table if the remaining use is less than 500
				if(context.getRemainingUsage()<500)
				{	
					break;
				}
			}
			nlapiLogExecution('DEBUG','salesOrderLineDetails.length2 ',salesOrderLineDetails.length);

		}

		nlapiLogExecution('DEBUG','Remaining Usage at the end ',context.getRemainingUsage());	

		nlapiLogExecution('DEBUG','Out of  processLargeOrders','');
	}
}


function setPriority(vsoid,vshipmethod)
{
	nlapiLogExecution('DEBUG','Into  setPriority',vshipmethod);
	var vOrderPriority=3;
	if(vshipmethod!=null && vshipmethod!='')
	{
		if(vshipmethod.indexOf('Day')!=-1 || vshipmethod=='EMPO - USPS Express Mail')
		{
			vOrderPriority=1;

			var fields = new Array();
			var values = new Array();

			fields.push('custbody_nswmspriority');

			values.push(vOrderPriority);

			nlapiSubmitField('salesorder', vsoid, fields, values);	
		}
	}

	nlapiLogExecution('DEBUG','Out of  setPriority',vOrderPriority);
	return vOrderPriority;
}

//To get the linenumbers count of an order
function getOrdLineCount(ordIntId)
{
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
	filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0

	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
	if(ordIntId != null && ordIntId != '')
	{
		nlapiLogExecution('ERROR','In side internalid in search filter',ordIntId);
		filters.push(new nlobjSearchFilter('internalid', null, 'is', ordIntId));
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid',null,'count');	

	var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);
	return salesOrderLineDetails;
}

//To get line items count of an order from for loop
function getOrdLineCountFrmForLoop(salesOrderLineDetails,currentLineNumber)
{
	var vLineCount=0;
	for(var s=currentLineNumber;s<salesOrderLineDetails.length;s++)
	{
		if((s+1 != salesOrderLineDetails.length) &&  salesOrderLineDetails[s].getValue('tranid') == salesOrderLineDetails[s+1].getValue('tranid') )
		{
			vLineCount=vLineCount+1;
		}
		else
		{
			vLineCount=vLineCount+1;
			break;
		}	

	}	
	nlapiLogExecution('ERROR','vLineCount',vLineCount);
	return vLineCount;
}

/**
 * searching for records
 * @param soId
 * @param lineNo
 * @returns
 */
function getRecordDetails(soId,lineNo,condn){
	var filter= new Array();
	nlapiLogExecution('DEBUG','soId ',soId);
	nlapiLogExecution('DEBUG','lineNo ',lineNo);
	filter.push(new nlobjSearchFilter('name', null, 'is', soId));
	filter.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineNo));
	filter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, condn, [25]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_ordline',null,'group');
	columns[2] = new nlobjSearchColumn('name',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_ordline_wms_location',null,'group');
	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, columns);
	return searchRecords;
}

function getLocationAliases()
{
	var locationAliasesArray=new Array();
	var columns=new Array();
	columns.push(new nlobjSearchColumn('custrecord_ebiz_loc_aliaslocation'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_loc_location'));
	resultSet=nlapiSearchRecord('customrecord_ebiz_locationalias',null,null,columns);
	if(resultSet!=null && resultSet!='' && resultSet.length>0)
	{
		for(var k3=0;k3<resultSet.length;k3++)
		{
			var CurrRow=[resultSet[k3].getValue('custrecord_ebiz_loc_location'),resultSet[k3].getValue('custrecord_ebiz_loc_aliaslocation')];
			locationAliasesArray.push(CurrRow);
		}
	}

	return locationAliasesArray;	
}

function getDefaultSKUStatus(vlocation)
{
	nlapiLogExecution('DEBUG','Into getDefaultSKUStatus',vlocation);
	var filters = new Array();
	var columns = new Array();
	var defSKUStatus='';

	filters.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', [vlocation]));
	filters.push(new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	columns.push(new nlobjSearchColumn('name'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filters, columns);
	if(searchresults!=null && searchresults!='')
	{
		defSKUStatus=searchresults[0].getId();
	}

	nlapiLogExecution('DEBUG','Out of getDefaultSKUStatus',defSKUStatus);
	return defSKUStatus;
}

function getShipDate(vsalesorderid)
{
	nlapiLogExecution('DEBUG','Into getShipDate',vsalesorderid);

	var vsoshipdate='';

	var filters = new Array();
	var columns = new Array();

	filters.push(new nlobjSearchFilter('internalid', null, 'is', vsalesorderid));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	columns.push(new nlobjSearchColumn('shipdate'));

	var searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	if(searchresults!=null && searchresults!='')
	{
		nlapiLogExecution('DEBUG','searchresults length',searchresults.length);
		vsoshipdate=searchresults[0].getValue('shipdate');
	}

	nlapiLogExecution('DEBUG','Out of getShipDate',vsoshipdate);
	return vsoshipdate;

}


function fnShipcompleteFo(vinternalid,salesOrderLineDetails,sonumber)
{

	nlapiLogExecution('DEBUG','Into Ship Complete Vaidation',sonumber);

	var createfo='T';

	var filtersshipcomplete = new Array();
	filtersshipcomplete.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
	filtersshipcomplete.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));	// Order Quantity > 0							
	filtersshipcomplete.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));							
	filtersshipcomplete.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	filtersshipcomplete.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
	//filtersshipcomplete.push(new nlobjSearchFilter('custbody_ebiz_ord_size', null, 'isnot', 'L')); // To process less than 100 line orders only
	filtersshipcomplete.push(new nlobjSearchFilter('internalid', null, 'is', vinternalid));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');	
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
	columns[3] = new nlobjSearchColumn('quantitycommitted');
	columns[4] = new nlobjSearchColumn('quantity');
	columns[5] = new nlobjSearchColumn('trandate');
	columns[6] = new nlobjSearchColumn('shipmethod');
	columns[7] = new nlobjSearchColumn('location');
	columns[8] = new nlobjSearchColumn('item');
	columns[9] = new nlobjSearchColumn('tranid');
	columns[10] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
	columns[11] = new nlobjSearchColumn('quantityshiprecv');

	//true - Discending false - Ascending
	columns[0].setSort(true);
	columns[1].setSort();

	var salesOrderLineDetailsshipcomplete = new nlapiSearchRecord('salesorder', null, filtersshipcomplete, columns);


	if(salesOrderLineDetailsshipcomplete!=null && salesOrderLineDetailsshipcomplete!='')
	{

		for(var z=0;z<salesOrderLineDetailsshipcomplete.length;z++)
		{
			var itemtype='';
			var solineordqty = salesOrderLineDetailsshipcomplete[z].getValue('quantity');
			var solinecmmtdqty = salesOrderLineDetailsshipcomplete[z].getValue('quantitycommitted');
			var lineitem=salesOrderLineDetailsshipcomplete[z].getValue('item');
			var shipmethod=salesOrderLineDetailsshipcomplete[z].getValue('shipmethod');

			var linelocation=salesOrderLineDetailsshipcomplete[z].getValue('location');
			if(linelocation!=null && linelocation!='')
				location=linelocation;

			if(solinecmmtdqty==null || solinecmmtdqty=='' || isNaN(solinecmmtdqty))
				solinecmmtdqty=0;

			var solinefulfildqty = salesOrderLineDetailsshipcomplete[z].getValue('quantityshiprecv');

			if(solinefulfildqty == null || solinefulfildqty == '' || isNaN(solinefulfildqty))
				solinefulfildqty = 0;

			var totalcomtdqty=parseFloat(solinecmmtdqty)+parseFloat(solinefulfildqty);

			if(lineitem!=null && lineitem!='')
				itemtype = nlapiLookupField('item', lineitem, 'recordType');	

			nlapiLogExecution('DEBUG','itemtype',itemtype);

			if(itemtype!='descriptionitem' && itemtype!='discountitem' && itemtype!='Payment' && itemtype!='Markup' 
				&& itemtype!='Subtotal'  && itemtype!='otherchargeitem' && itemtype!='serviceitem' && itemtype!='noninventoryitem'
					&& itemtype!='NonInvtPart')
			{
				if(parseFloat(solineordqty) > parseFloat(totalcomtdqty))
				{
					createfo = 'F';
					continue;
				}
			}

		}

		if(createfo=='T')
		{
			var shipcompletelines=salesOrderLineDetailsshipcomplete.length;
			var totalshipcompltelength=0;
			nlapiLogExecution('DEBUG','sonumber',sonumber);
			nlapiLogExecution('DEBUG','shipcompletelines',shipcompletelines);
			for(var j=0;j<salesOrderLineDetails.length;j++)
			{
				if(sonumber == salesOrderLineDetails[j].getValue('tranid'))
				{
					totalshipcompltelength++;
				}
			}
			nlapiLogExecution('DEBUG','totalshipcompltelength',totalshipcompltelength);
			if(shipcompletelines==totalshipcompltelength)
			{
				createfo = 'T';
			}
			else
				createfo = 'F';

		}

	}

	nlapiLogExecution('DEBUG','Out of Ship Complete Vaidation',createfo);
	return createfo;
}