//profilerInitialize("DepartTrailer");
/*
eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Schedulers/Attic/ebiz_DepartTrailer_Schd.js,v $
 *     	   $Revision: 1.1.2.1.4.1 $
 *     	   $Date: 2015/09/23 14:58:08 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_13 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_DepartTrailer_Schd.js,v $
 * Revision 1.1.2.1.4.1  2015/09/23 14:58:08  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.2.1  2014/05/22 22:14:25  snimmakayala
 * Case #: 20148508
 *
 * Revision 1.2.2.21.4.3  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.2.2.21.4.2  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.21.4.1  2012/10/30 06:10:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.2.2.21  2012/09/04 16:44:15  gkalla
 * CASE201112/CR201113/LOG201121
 * Actual end date reset to today date
 *
 * Revision 1.2.2.20  2012/09/04 07:26:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Showing improper message after trailer depart.
 *
 * Revision 1.2.2.19  2012/09/03 19:09:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fisk Production Issue Fixes
 *
 * Revision 1.2.2.18  2012/08/30 02:00:48  gkalla
 * CASE201112/CR201113/LOG201121
 * Updating sysdate to actual end date of opentask issue
 *
 * Revision 1.2.2.17  2012/08/23 13:42:17  mbpragada
 * 20120490
 * commneted joins in search cloumns
 *
 * Revision 1.2.2.16  2012/07/30 16:47:03  gkalla
 * CASE201112/CR201113/LOG201121
 * Populating actual carrieTrailer
 *
 * Revision 1.2.2.15  2012/07/23 06:30:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Dynacraft UAT issue fixes
 *
 * Revision 1.2.2.14  2012/07/13 00:33:52  gkalla
 * CASE201112/CR201113/LOG201121
 * Shipmanifest issue fixed
 *
 * Revision 1.2.2.13  2012/07/10 23:59:23  gkalla
 * CASE201112/CR201113/LOG201121
 * Added shipment# and trailer# whenever getting success
 *
 * Revision 1.2.2.12  2012/07/04 14:50:47  gkalla
 * CASE201112/CR201113/LOG201121
 * JAE specific changes
 *
 * Revision 1.2.2.11  2012/07/04 07:50:29  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned
 *
 * Revision 1.2.2.9  2012/06/12 13:22:29  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.2.2.8  2012/06/11 22:25:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.2.2.7  2012/06/07 13:23:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Update shipqty in fulfillment
 *
 * Revision 1.2.2.5  2012/04/20 14:01:18  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.2.2.4  2012/04/12 21:46:16  spendyala
 * CASE201112/CR201113/LOG201121
 * Showing meaningful data instead of showing incomplete data.
 *
 * Revision 1.2.2.3  2012/04/11 11:10:55  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.2.2.2  2012/04/06 14:03:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.2.2.1  2012/02/08 14:24:13  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing  related to updateopentask with status flag shipped and actualenddate
 *
 * Revision 1.2  2011/12/12 08:41:22  rgore
 * CASE201112/CR201113/LOG201121
 * Sanitized error message for spelling and grammar.
 * - Ratnakar
 * 12 Dec 2011
 *
 * Revision 1.1  2011/11/23 11:15:41  schepuri
 * CASE201112/CR201113/LOG201121
 * splitting load and depart trailer functionality
 *
 *
 *****************************************************************************/
/**
 * This is the main function which performs the complete Trailer Depart process.
 * @param request
 * @param response
 */
function DepartTrailerSchd(type){


	nlapiLogExecution('DEBUG','Into  PrintAutoPacking_SCHD','');

	var context = nlapiGetContext();  
	var trailerno='';

	trailerno = context.getSetting('SCRIPT', 'custscript_trailer');



	var pronoresearch = nlapiLoadRecord('customrecord_ebiznet_trailer', trailerno); 

	var trailerName=pronoresearch.getFieldValue('name');
	var currentRecord;
	var resultsetShipLP,resultsetSoNo;

	nlapiLogExecution('DEBUG','trailerName',trailerName);

	var filters = specifyLoadTrailerFilters(trailerName);
	var columns = specifyLoadTrailerColumns();

	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(SearchResults != "" && SearchResults != null){
		nlapiLogExecution('DEBUG', "SearchResults is not empty", SearchResults.length);


		var closeTaskDetails = new Array();		
		var distinctOrders = new Array();
		var TranIdArr = new Array();
		var SOCarrierArr = new Array();
		var newcurrentRow = new Array();
		var lineCnt = SearchResults.length;			
		var prevordno='';
		var prevShipLp='';
		var totalweight=0;

		var pronoresearch = nlapiLoadRecord('customrecord_ebiznet_trailer', trailerno); 
		var PronId = pronoresearch.getFieldValue('custrecord_ebizpro');
		var Actdept=pronoresearch.getFieldValue('custrecord_ebizdepartdate');
		var sealno = pronoresearch.getFieldValue('custrecord_ebizseal');
		var Tripno=pronoresearch.getFieldValue('custrecord_ebizvehicle');
		var trailerName=pronoresearch.getFieldValue('name');
		var vCarrierFrTrailer=pronoresearch.getFieldText('custrecord_ebizcarrierid');
		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);
		nlapiLogExecution('DEBUG', 'Remaining Usage at end 1', nlapiGetContext().getRemainingUsage());
		nlapiLogExecution('DEBUG', 'vCarrierFrTrailer', vCarrierFrTrailer);


		var vMainWave='';
		for (var i = 0; i < SearchResults.length; i++) {
			var currentRecord=SearchResults[i];
			var serialno= parseInt(i)+1;		
			var ordernumber= currentRecord.getValue('name');
			var wavenumber = currentRecord.getValue('custrecord_ebiz_wave_no');
			var shiplp = currentRecord.getValue('custrecord_ship_lp_no');
			var recordId = currentRecord.getId();
			var salesordno = currentRecord.getValue('custrecord_ebiz_order_no');
			var itemname= currentRecord.getText('custrecord_sku');
			var avlqty= currentRecord.getValue('custrecord_act_qty');		
			var itemNo= currentRecord.getValue('custrecord_sku');
			var ebizDono= currentRecord.getValue('custrecord_ebiz_cntrl_no');
			var itemFulfillmentInternalId = currentRecord.getValue('custrecord_ebiz_nsconfirm_ref_no');
			var taskweight = currentRecord.getValue('custrecord_total_weight');
			var taskcube = currentRecord.getValue('custrecord_totalcube');
			var fulfillmentOrderInternalId = currentRecord.getValue('custrecord_ebiz_cntrl_no');
			var containerlp= currentRecord.getValue('custrecord_container_lp_no');
			var begindate= '';//request.getLineItemValue('custpage_departtrailer','custpage_begindate',k);
			var actbegintime= '';//request.getLineItemValue('custpage_departtrailer','custpage_actbegintime',k);
			var vZoneId=currentRecord.getValue('custrecord_ebiz_zoneid');
			var vEbizSKU=currentRecord.getValue('custrecord_ebiz_sku_no');
			var nsConfNo=currentRecord.getValue('custrecord_ebiz_nsconfirm_ref_no');

			var tasktype=currentRecord.getValue('custrecord_tasktype');
			var zoneid=currentRecord.getValue('custrecord_ebizzone_no');
			var location=currentRecord.getValue('custrecord_wms_location');
			var packcode=currentRecord.getValue('custrecord_packcode');
			var lineno=currentRecord.getValue('custrecord_line_no');
			var ExpQty=currentRecord.getValue('custrecord_expe_qty');
			var Controlno=currentRecord.getValue('custrecord_ebiz_cntrl_no');
			var ActQty=currentRecord.getValue('custrecord_act_qty');
			var ActBegDate=currentRecord.getValue('custrecordact_begin_date');
			var ActBigLoc=currentRecord.getValue('custrecord_actbeginloc');
			var BegTime=currentRecord.getValue('custrecord_actualbegintime');

			var EndDate=currentRecord.getValue('custrecord_act_end_date');
			var EndLoc=currentRecord.getValue('custrecord_actendloc');
			var EndTime=currentRecord.getValue('custrecord_actualendtime');
			var vTranId=currentRecord.getValue('tranid','custrecord_ebiz_order_no');
			var vSOCarrier=currentRecord.getText('custbody_salesorder_carrier','custrecord_ebiz_order_no');
			//var EndDate=DateStamp();
			nlapiLogExecution('DEBUG','tasktype',tasktype);
			nlapiLogExecution('DEBUG','vTranId',vTranId);
			nlapiLogExecution('DEBUG','vSOCarrier',vSOCarrier);
			if(wavenumber != null && wavenumber != '')
				vMainWave=wavenumber;
			var currentContext = nlapiGetContext();
			var currentRow = [salesordno,taskweight,ordernumber];

			var newcurrentRow = [recordId,itemFulfillmentInternalId,serialno,ordernumber,wavenumber,shiplp,salesordno,itemname,avlqty,itemNo,ebizDono
			                     ,taskweight,fulfillmentOrderInternalId,containerlp,begindate,actbegintime,taskweight,taskcube,vZoneId,vEbizSKU,currentContext.getUser()
			                     ,tasktype,zoneid,location,packcode,lineno,ExpQty,Controlno,ActQty,ActBegDate,ActBigLoc
			                     ,BegTime,EndDate,EndLoc,EndTime,nsConfNo];

			closeTaskDetails.push(currentRow);			
			if(i==0)
			{
				sorderno=salesordno;
			}
			nlapiLogExecution('DEBUG','prevordno',prevordno);
			nlapiLogExecution('DEBUG','salesordno',salesordno);

			/*In opentask updating wms status flag from trailer loaded to shipped and actual end date is updated */
			updateOpenTask(newcurrentRow,newParent);


			if(prevShipLp!=shiplp)
			{
				updateShipTask(shiplp);
				prevShipLp=shiplp;
			}

			if(prevordno!=salesordno)
			{
				distinctOrders.push(salesordno);
				TranIdArr.push(vTranId);
				SOCarrierArr.push(vSOCarrier);
				prevordno=salesordno;
			}
		}
		var textFields = ['entity', 'tranid'];
		columns = nlapiLookupField('salesorder',sorderno,textFields);
		var customerID= columns.entity;
		var salesorderID=columns.tranid;
		nlapiLogExecution('DEBUG', 'salesorderID',salesorderID);
		nlapiLogExecution('DEBUG', 'customerID',customerID);
		var asnflagchecked=nlapiLookupField('customer',customerID,'custentity_ebiz_asn_required');
		nlapiLogExecution('DEBUG', 'asnflagchecked',asnflagchecked);

		if(asnflagchecked=='T')
		{
			var shipmanifestdetailsarray = new Array();
			shipmanifestdetailsarray.push(new nlobjSearchFilter('custrecord_ship_orderno', null, 'is',salesorderID));

			var shipmanifestdetailscolumnarray = new Array();
			shipmanifestdetailscolumnarray [0] = new nlobjSearchColumn('custrecord_ship_orderno', null, 'group');
			shipmanifestdetailscolumnarray [1] = new nlobjSearchColumn('custrecord_ship_masttrackno', null, 'group');
			shipmanifestdetailscolumnarray [2] = new nlobjSearchColumn('custrecord_ship_carrier', null, 'group');



			var shipmanifestsearchresults  = nlapiSearchRecord('customrecord_ship_manifest', null, shipmanifestdetailsarray, shipmanifestdetailscolumnarray);
			nlapiLogExecution('DEBUG','shipmanifestsearchresults',shipmanifestsearchresults);
			if(shipmanifestsearchresults!=null&&shipmanifestsearchresults!="")
			{
				var trackingno = shipmanifestsearchresults[0].getValue('custrecord_ship_masttrackno', null, 'group');
				nlapiLogExecution('DEBUG', 'trackingno ',trackingno);
				var carrier = shipmanifestsearchresults[0].getValue('custrecord_ship_carrier', null, 'group');
				nlapiLogExecution('DEBUG', 'carrier',carrier);
			}
//			if((trackingno!= "")&&(trackingno!=null)&&(trackingno!='- None -'))
//			{		
//			var trailerrecord = nlapiLoadRecord('customrecord_ebiznet_trailer', trailerno);
//			var prono=trailerrecord.getFieldValue('custrecord_ebizpro');
//			nlapiLogExecution('DEBUG', 'prono',prono);
//			if((prono!= "")&&(prono!=null))
//			{
//			if(prono.indexOf("DYNA") !== -1)
//			{
//			PronId=trackingno;
//			nlapiLogExecution('DEBUG', 'PronId',PronId);
//			trailerrecord.setFieldValue('custrecord_ebizpro', trackingno); 
//			nlapiSubmitRecord(trailerrecord, false, true);
//			nlapiLogExecution('DEBUG', 'intoif','ok');
//			}

//			}

//			}
			var trailerrecord = nlapiLoadRecord('customrecord_ebiznet_trailer', trailerno);
			var prono=trailerrecord.getFieldValue('custrecord_ebizpro');
			nlapiLogExecution('DEBUG', 'prono',prono);
			var pronodyna='F';

			if(carrier=='PC')
			{
				if((trackingno!= "")&&(trackingno!=null)&&(trackingno!='- None -'))
				{
					PronId=trackingno;
					nlapiLogExecution('DEBUG', 'PronId',PronId);
					trailerrecord.setFieldValue('custrecord_ebizpro', trackingno); 
					nlapiSubmitRecord(trailerrecord, false, true);
					nlapiLogExecution('DEBUG', 'intoif','ok');
				}
				else
				{
					if((prono!= "")&&(prono!=null)&& (prono.indexOf("DYNA") != -1))			
					{
						nlapiLogExecution('DEBUG', 'intoelse','else');
						var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'Please enter valid Pro# in trailer record,Trailer Depart: Failed', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						//showInlineMessage(form, 'DEBUG', 'This order not having TrackingNumber,Trailer Depart: Failed', null);
						nlapiLogExecution('DEBUG', 'Aftermsg','Aftermsg');
						response.writePage(form);
						return false;

					}
					if((prono=='')||(prono==null))
					{
						nlapiLogExecution('DEBUG', 'intoelse','else');
						var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'Please enter Pro# in trailer record,Trailer Depart: Failed', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						//showInlineMessage(form, 'DEBUG', 'This order not having TrackingNumber,Trailer Depart: Failed', null);
						nlapiLogExecution('DEBUG', 'Aftermsg','Aftermsg');
						response.writePage(form);
						return false;							
					}
				}
			}
		}

		nlapiSubmitRecord(newParent); 
		nlapiLogExecution('DEBUG', 'Remaining Usage 2', nlapiGetContext().getRemainingUsage());
		//nlapiSubmitField('customrecord_ebiznet_trailer', trailerno, 'custrecord_ebizdepartdate', DateStamp());		
		nlapiLogExecution('DEBUG','closeTaskDetails length',closeTaskDetails.length);
		// To Clear OUB inv and to update closed task with ship status
		//var closeTaskDetails = updateClosedTask(trailerName); // 
		var createShiManifestRecordResult=0;
		//To Create shipmanifest records
		if(distinctOrders != null && distinctOrders != '')
			createShiManifestRecordResult=ProcessShipmanifest(distinctOrders,closeTaskDetails,PronId,Tripno,Actdept,sealno,trailerName,TranIdArr,SOCarrierArr,vCarrierFrTrailer);

		if(createShiManifestRecordResult >0){
			nlapiLogExecution('DEBUG','trailerno',trailerno);
			nlapiSubmitField('customrecord_ebiznet_trailer', trailerno, 'custrecord_ebizdepartdate', DateStamp());
			//showInlineMessage(form, 'Confirmation', 'Trailer ' +Tripno+ ' Departed Successfully', '');
			nlapiLogExecution('DEBUG','vMainWave',vMainWave);
			//To get Shipment# from Fulfillment order
			var Filers = new Array;			 
			Filers.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto', vMainWave)); 
			Filers.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'isnotempty'));
			var Columns = new Array();
			Columns[0] = new nlobjSearchColumn('custrecord_shipment_no');

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, Filers, Columns);
			if(SOSearchResults != null && SOSearchResults !='')
			{
				var Shipmentno=SOSearchResults[0].getValue('custrecord_shipment_no');
				nlapiLogExecution('DEBUG','Shipmentno',Shipmentno);
				var msg='Trailer ' +trailerName+ ' and Shipment# ' + Shipmentno + ' Departed Successfully';
				nlapiLogExecution('DEBUG', 'Confirmation', msg);
			}
			else
			{
				var msg='Trailer ' +Tripno+ ' Departed Successfully';
				nlapiLogExecution('DEBUG', 'Confirmation', msg);
			}



		}
		else{
			var msg='Trailer Depart: Failed';
			nlapiLogExecution('DEBUG', 'Confirmation', msg);

		}

		nlapiLogExecution('DEBUG', 'Remaining Usage at end', nlapiGetContext().getRemainingUsage());

	}

}






function updateShipTask(shiplp)
{
	nlapiLogExecution('DEBUG', "Into updateShipTask:shiplp", shiplp);

	var shipTaskFilers = new Array();
	var shipTaskColumns = new Array();

	if(shiplp!=null && shiplp!='')
	{
		shipTaskFilers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shiplp));
		shipTaskFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4])); 	//SHIP Task
		shipTaskFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [10])); // LOADED

		var openTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, shipTaskFilers, shipTaskColumns);
		for (var i = 0; openTaskSearchResults!=null && i < openTaskSearchResults.length; i++) {
			var shipTaskId = openTaskSearchResults[i].getId();

			nlapiLogExecution('DEBUG', "updateOpenTask:TaskId", shipTaskId);

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', shipTaskId, 'custrecord_wms_status_flag', '14');
		}
	}

	nlapiLogExecution('DEBUG', "Out of updateShipTask");
}


function updateOpenTask(openTaskDetails,newParent)
{	
	nlapiLogExecution('DEBUG', "into updateOpenTask:TaskId", openTaskDetails);
	if(openTaskDetails!=null && openTaskDetails!='' && openTaskDetails.length>0)
	{
		nlapiLogExecution('DEBUG', "openTaskDetails[32]", openTaskDetails[32]);

		newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', openTaskDetails[0]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '14');			
		//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date',DateStamp());
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype',openTaskDetails[21]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku',openTaskDetails[9]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',openTaskDetails[14]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime',openTaskDetails[15]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight',openTaskDetails[16]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_totalcube',openTaskDetails[17]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no',openTaskDetails[13]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ship_lp_no',openTaskDetails[5]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid',openTaskDetails[18]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no',openTaskDetails[19]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no',openTaskDetails[4]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no',openTaskDetails[20]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name',openTaskDetails[3]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no',openTaskDetails[6]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizzone_no',openTaskDetails[22]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location',openTaskDetails[23]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode',openTaskDetails[24]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no',openTaskDetails[25]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty',openTaskDetails[26]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no',openTaskDetails[27]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty',openTaskDetails[28]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',openTaskDetails[29]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc',openTaskDetails[30]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime',openTaskDetails[31]);
		nlapiLogExecution('DEBUG', "openTaskDetails[32]", openTaskDetails[32]);
		if(openTaskDetails[32] != null && openTaskDetails[32] != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date',openTaskDetails[32]);
		else
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date',DateStamp());
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc',openTaskDetails[33]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime',openTaskDetails[34]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_nsconfirm_ref_no',openTaskDetails[35]);

		newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');		
	}
}
/*
 *  This function is to specify the Load Trailer filters.
 * 	This function returns an array of filter criteria specified. 
 * @param waveno
 * @param fulfillmentorder
 * @param shiplp
 * @returns {Array}
 */
function specifyLoadTrailerFilters(trailername){

	nlapiLogExecution('DEBUG','Into specifyLoadTrailerFilters','');
	var filters = new Array();


	if(trailername != "" && trailername!=null){
		nlapiLogExecution('DEBUG','Inside trailername',trailername);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailername));			
	}
	// Add filter criteria for WMS Status Flag;
	// Search for 'TRAILER LOADED' ;
	filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [10]));

	// Add filter criteria for Task type;
	// Search for 'SHIP'/4 ;
	// Search for 'PICK'/3 ;
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	nlapiLogExecution('DEBUG','Out of specifyLoadTrailerFilters','');
	return filters;
}

/**
 *  This function is to specify the Load Trailer columns.
 * 	This returns an array of columns to be fetched from the search record. 
 * @returns {Array}
 */
function specifyLoadTrailerColumns(){
	var columns = new Array();
	columns.push(new nlobjSearchColumn('name'));
	columns.push(new nlobjSearchColumn('custrecord_ship_lp_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
	columns.push(new nlobjSearchColumn('custrecord_sku'));
	columns.push(new nlobjSearchColumn('custrecord_act_qty'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	columns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no'));
	columns.push(new nlobjSearchColumn('custrecord_total_weight'));
	columns.push(new nlobjSearchColumn('custrecordact_begin_date'));
	columns.push(new nlobjSearchColumn('custrecord_actualbegintime'));
	columns.push(new nlobjSearchColumn('custrecord_totalcube'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	//columns.push(new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	columns.push(new nlobjSearchColumn('custrecord_wms_location'));
	columns.push(new nlobjSearchColumn('custrecord_tasktype'));
	columns.push(new nlobjSearchColumn('custrecord_packcode'));
	columns.push(new nlobjSearchColumn('custrecord_line_no'));
	columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	columns.push(new nlobjSearchColumn('custrecord_act_qty'));
	columns.push(new nlobjSearchColumn('custrecordact_begin_date'));
	columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	columns.push(new nlobjSearchColumn('custrecord_actualbegintime'));
	columns.push(new nlobjSearchColumn('custrecord_act_end_date'));
	columns.push(new nlobjSearchColumn('custrecord_actendloc'));
	columns.push(new nlobjSearchColumn('custrecord_actualendtime'));	 
	//columns.push(new nlobjSearchColumn('tranid','custrecord_ebiz_order_no'));
	//columns.push(new nlobjSearchColumn('custbody_salesorder_carrier','custrecord_ebiz_order_no'));


	columns[0].setSort();

	return columns;
}


function createShiManifestRecord(PronId,Tripno,Actdept,sealno,trailerName,soname,carrier,weight,parent,sonum)
{	
	var string="";
	if(carrier !=null && carrier !=""){
		string = carrier+"-" ;
	}
	if(Tripno !=null && Tripno !=""){
		string += Tripno+"-" ;
	}
	if(trailerName !=null && trailerName !=""){
		string += trailerName+"-" ;
	}
	if(Actdept !=null && Actdept !=""){
		string += Actdept+"-" ;
	}
	if(sealno !=null && sealno !=""){
		string += sealno ;
	}
	var lp=string;

	parent.selectNewLineItem('recmachcustrecord_ebiz_sm_parent');	 

	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','name', soname);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_orderno', soname);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_trackno', PronId);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_contlp', lp);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_carrier', 'LTL');
	if(weight==0)
		weight=0.1;
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_actwght', weight);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_charges', 0);	

	parent.commitLineItem('recmachcustrecord_ebiz_sm_parent');
	nlapiLogExecution('DEBUG', 'Shipmanifest Created', soname);


}

function createShiManifestRecordOld(PronId,Tripno,Actdept,sealno,trailerName,sono,carrier,weight,soname,vCarrier,vFONo) {
	nlapiLogExecution('DEBUG', "Into createShiManifestRecordOld", 'success');

	nlapiLogExecution('DEBUG', "PronId", PronId);
	nlapiLogExecution('DEBUG', "Tripno", Tripno);
	nlapiLogExecution('DEBUG', "Actdept", Actdept);
	nlapiLogExecution('DEBUG', "sealno", sealno);
	nlapiLogExecution('DEBUG', "trailerName", trailerName);
	nlapiLogExecution('DEBUG', "soname", soname);
	nlapiLogExecution('DEBUG', "sono", sono);
	nlapiLogExecution('DEBUG', "carrier", carrier);
	nlapiLogExecution('DEBUG', "weight", weight);
	nlapiLogExecution('DEBUG', "vCarrier", vCarrier);
	nlapiLogExecution('DEBUG', "vFONo", vFONo);
	var string="";
	if(carrier !=null && carrier !=""){
		string = carrier+"-" ;
	}
	if(Tripno !=null && Tripno !=""){
		string += Tripno+"-" ;
	}
	if(trailerName !=null && trailerName !=""){
		string += trailerName+"-" ;
	}
	if(Actdept !=null && Actdept !=""){
		string += Actdept+"-" ;
	}
	if(sealno !=null && sealno !=""){
		string += sealno ;
	}
	var lp=string;

	var ShiManifestRecord = nlapiCreateRecord('customrecord_ship_manifest');

	ShiManifestRecord.setFieldValue('name', sono);
	ShiManifestRecord.setFieldValue('custrecord_ship_orderno', soname);
	ShiManifestRecord.setFieldValue('custrecord_ship_trackno', PronId);
	ShiManifestRecord.setFieldValue('custrecord_ship_contlp', lp);
	ShiManifestRecord.setFieldValue('custrecord_ship_carrier', 'LTL');
	//ShiManifestRecord.setFieldValue('custrecord_ship_pkgwght', weight);
	if(weight==0)
		weight=0.1;
	ShiManifestRecord.setFieldValue('custrecord_ship_actwght', parseFloat(weight).toFixed(5));
	ShiManifestRecord.setFieldValue('custrecord_ship_charges', 0);
	ShiManifestRecord.setFieldValue('custrecord_ship_order', sono);
	ShiManifestRecord.setFieldValue('custrecord_ship_act_carrier', vCarrier);
	ShiManifestRecord.setFieldValue('custrecord_ship_ref3', vFONo);
	nlapiLogExecution('DEBUG', "Before createShiManifestRecord", '');
	var recid = nlapiSubmitRecord(ShiManifestRecord);
	nlapiLogExecution('DEBUG', "Out of createShiManifestRecordOld", 'success');

}
/**
 * This function is used to update the Status in closed task record.
 *  
 * @param trailerName
 */
function updateClosedTask(trailerName)
{
	nlapiLogExecution('DEBUG', "updateClosedTask:trailerName", trailerName);
	var closedTaskId;
	var closedTaskFilers = new Array();
	var closeTaskDetails = new Array();

	// Add filter criteria for Task type ;
	// Search for 'PICK';
	var vDistinctOrders=new Array();
	var vPrevOrd;
	closedTaskFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
	closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerName)); 	 

	var closedTaskColumns = new Array();
	closedTaskColumns[0] = new nlobjSearchColumn('name');
	closedTaskColumns[1] = new nlobjSearchColumn('custrecord_ebiz_lpno');
	closedTaskColumns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	closedTaskColumns[2].setSort();
	var closedTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, closedTaskFilers, closedTaskColumns);
	if(closedTaskSearchResults!=null && closedTaskSearchResults!='')
	{
		for (var i = 0; i < closedTaskSearchResults.length; i++) {
			closedTaskId = closedTaskSearchResults[i].getId();
			var vcontainerlp = closedTaskSearchResults[i].getValue('custrecord_ebiz_lpno');

			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', closedTaskId);

			var quantityPicked = transaction.getFieldValue('custrecord_act_qty');
			var fulfillmentOrderInternalId = transaction.getFieldValue('custrecord_ebiz_cntrl_no');
			var fulfillmentOrder = transaction.getFieldValue('name');
			var waveNumber = transaction.getFieldValue('custrecord_ebiz_wave_no');
			var itemId = transaction.getFieldValue('custrecord_sku');
			var closeTaskRecordId = closedTaskSearchResults[i].getId();
			var itemFulfillmentInternalId = transaction.getFieldValue('custrecord_ebiz_nsconfirm_ref_no');//3369
			//var totalweight = transaction.getFieldValue('custrecord_ebiztask_total_weight');//3369
			var ItemOrd = transaction.getFieldValue('custrecord_ebiz_order_no');//Order#
			transaction.setFieldValue('custrecord_wms_status_flag', 14);//added by shylaja on nov 21
			nlapiSubmitRecord(transaction, true);

			var currentRow = [i, closeTaskRecordId, quantityPicked, fulfillmentOrder, 
			                  fulfillmentOrderInternalId, itemId, waveNumber, itemFulfillmentInternalId];

			closeTaskDetails.push(currentRow);

			clearOutboundInventory(vcontainerlp);

			if(vPrevOrd != ItemOrd)
			{	
				vDistinctOrders.push(ItemOrd);
				vPrevOrd == ItemOrd;
			} 	
			else
			{

			}

		}
		// To update Ship status in fulfillment ord line
		if(closeTaskDetails != null && closeTaskDetails != '' && closeTaskDetails.length>0)
		{
			updateFulfillment(closeTaskDetails);
		}	
	}


	return vDistinctOrders;
}

function clearOutboundInventory(vebizcontainerlp)
{
	nlapiLogExecution('DEBUG', "Into  clearOutboundInventory" , vebizcontainerlp);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is',vebizcontainerlp));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['18']));

	var columns= new Array();

	var inventorysearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filters ,columns);
	if(inventorysearchresults!=null)
	{
		for (var i = 0; inventorysearchresults != null && i < inventorysearchresults.length; i++) 
		{
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', inventorysearchresults[i].getId());
			nlapiLogExecution('DEBUG', 'Deleted Outbound Inventory Record : ', id);	
		}
	}

	nlapiLogExecution('DEBUG', "Out of  clearOutboundInventory" , vebizcontainerlp);	
}

/**
 * This function is used to update the status flag to SHIPPED and updating ship quantity in Fulfillment 
 * 	Order Line.
 * The close task details array consists of 
 * 	0 = i, 1 = closeTaskRecordId, 2 = quantityPicked, 3 = fulfillmentOrder,	4 = fulfillmentOrderInternalId, 
 * 	5= itemId, 6 = waveNumber, 7 = item fulfillment Id
 * @param fulfillmetIds
 * @param trailerName
 */
function updateFulfillment(closeTaskDetails){
	for (var i = 0; i < closeTaskDetails.length; i++) {
		var fulfillmentOrderInternalId = closeTaskDetails[i][4];
		var shipQuantity = closeTaskDetails[i][2];
		if(fulfillmentOrderInternalId!=null && fulfillmentOrderInternalId!="")
		{
			//set the status flag as STATUS.OUTBOUND.SHIPPED

			/*var transaction = nlapiLoadRecord('customrecord_ebiznet_ordline', fulfillmentOrderInternalId);			
			transaction.setFieldValue('custrecord_linestatus_flag', 14);
			transaction.setFieldValue('custrecord_ship_qty', shipQuantity);
			nlapiSubmitRecord(transaction, true);*/

			var fieldNames = new Array(); 
			fieldNames.push('custrecord_linestatus_flag');  	
			fieldNames.push('custrecord_ship_qty'); 
			nlapiLogExecution('DEBUG', "shipQuantity" , shipQuantity);
			var newValues = new Array(); 
			newValues.push('14');	
			newValues.push(shipQuantity);		

			nlapiSubmitField('customrecord_ebiznet_ordline', fulfillmentOrderInternalId, fieldNames, newValues);
		}
	}	
}

/*
 *
 * To process shipmanifest records
 * @param ClosedTaskDetails
 * @param PronId
 * @param Tripno
 * @param Actdept
 * @param sealno
 * @param trailerName
 * @returns {Number}
 */
function ProcessShipmanifest(distinctorders,ClosedTaskDetails,PronId,Tripno,Actdept,sealno,trailerName,TranIdArr,
		SOCarrierArr,vCarrierFrTrailer)
{	
	nlapiLogExecution('DEBUG', "Into ProcessShipmanifest");	

	var count=0;
	var ordarrray = new Array();
	var distinctordFilers = new Array(); 
	var salesOrderNumber='';


	if(distinctorders != null && distinctorders!=""){
		nlapiLogExecution('DEBUG', "distinctordSearchResults", distinctorders.length);	

		for (var i = 0; i < distinctorders.length; i++){

			var Salesord=distinctorders[i];
			nlapiLogExecution('DEBUG', "Salesordp else", Salesord);
			count +=1;

			var trantypeso = nlapiLookupField('transaction', distinctorders[i], 'recordType');
			//Script added on 15th Feb 2013 by suman
			//To over come time execution process LoadRecord is replaced with search criteria.

			var searchfilter=new Array();
			searchfilter[0]=new nlobjSearchFilter("internalid",null,"anyof",distinctorders[i]);
			searchfilter[1]=new nlobjSearchFilter("mainline",null,"is","T");

			var searchColumn=new Array();
			searchColumn[0]=new nlobjSearchColumn("tranid");
			searchColumn[1]=new nlobjSearchColumn("custbody_salesorder_carrier");

			var salesorderheadresearch=nlapiSearchRecord(trantypeso,null,searchfilter,searchColumn);

			var soname = salesorderheadresearch[0].getValue("tranid");
			var carrier=salesorderheadresearch[0].getText("custbody_salesorder_carrier");
			/*var salesorderheadresearch = nlapiLoadRecord(trantypeso, distinctorders[i]); 
			var soname = salesorderheadresearch.getFieldValue('tranid');
			var carrier=salesorderheadresearch.getFieldText('custbody_salesorder_carrier');*/
			//end of code as of 15th Feb 2013
			nlapiLogExecution('DEBUG', "after salesorder", Salesord);
			var weight=getweight(Salesord,ClosedTaskDetails);
			var vFONo=getFoNo(Salesord,ClosedTaskDetails);
			var createShiManifestRecordResult = createShiManifestRecordOld(PronId,Tripno,Actdept,sealno,trailerName,
					Salesord,carrier,weight,soname,vCarrierFrTrailer,vFONo);


		}
	}

	nlapiLogExecution('DEBUG', "record count", count);
	return count;
}

function getFoNo(SalesordId,ClosedTaskDetails)
{
	nlapiLogExecution('DEBUG', "Into getFoNo", SalesordId);
	var fono='';

	if(ClosedTaskDetails!=null && ClosedTaskDetails!='')
	{
		for (var i = 0; i < ClosedTaskDetails.length; i++) {
			var ordno = ClosedTaskDetails[i][0];
			var fono = ClosedTaskDetails[i][2];

			if(ordno==SalesordId)
				return fono;
		}
	}

	nlapiLogExecution('DEBUG', "Out of getFoNo", fono);
	return fono;
}

function getweight(SalesordId,ClosedTaskDetails)
{
	nlapiLogExecution('DEBUG', "Into getweight", SalesordId);
	var totweight=0;

	if(ClosedTaskDetails!=null && ClosedTaskDetails!='')
	{
		for (var i = 0; i < ClosedTaskDetails.length; i++) {
			var ordno = ClosedTaskDetails[i][0];
			var weight = ClosedTaskDetails[i][1];
			if(weight==null || weight=='' || isNaN(weight))
				weight=0;
			if(ordno==SalesordId)
				totweight+=parseFloat(weight);
		}
	}

	nlapiLogExecution('DEBUG', "Out of getweight", totweight);
	return totweight;
}

function getweightold(SalesordId)
{
	nlapiLogExecution('DEBUG', "getweight", SalesordId);
	var weight;
	var closedTaskFilers = new Array(); 

	closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'is', SalesordId)); 

	var closedTaskColumns = new Array();
	closedTaskColumns[0] = new nlobjSearchColumn('custrecord_ebiztask_total_weight');

	var closedTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, closedTaskFilers, closedTaskColumns);
	if(closedTaskSearchResults!=null && closedTaskSearchResults!='')
	{
		for (var i = 0; i < closedTaskSearchResults.length; i++) {
			weight = closedTaskSearchResults[0].getValue('custrecord_ebiztask_total_weight');
		}
	}
	return weight;
}
