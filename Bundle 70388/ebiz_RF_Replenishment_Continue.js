/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/ebiz_RF_Replenishment_Continue.js,v $
 *     	   $Revision: 1.6.4.2.4.2.4.7 $
 *     	   $Date: 2014/06/13 09:39:46 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Replenishment_Continue.js,v $
 * Revision 1.6.4.2.4.2.4.7  2014/06/13 09:39:46  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.6.4.2.4.2.4.6  2014/06/06 07:24:46  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.6.4.2.4.2.4.5  2014/05/30 00:34:22  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.6.4.2.4.2.4.4  2013/11/14 15:41:55  skreddy
 * Case# 20125719 & 20125658
 * Afosa SB issue fix
 *
 * Revision 1.6.4.2.4.2.4.3  2013/05/14 14:16:37  schepuri
 * RF Replen confirm based on filters
 *
 * Revision 1.6.4.2.4.2.4.2  2013/04/17 16:02:35  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.6.4.2.4.2.4.1  2013/03/05 14:46:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.6.4.2.4.2  2012/09/26 12:37:08  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.6.4.2.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.6.4.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.6.4.1  2012/02/21 13:25:41  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.7  2012/02/21 11:51:13  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */
function Replenishment_Continue(request, response){
	if (request.getMethod() == 'GET') {
		var vClustno = request.getParameter('custparam_clustno');
		var reportNo = request.getParameter('custparam_repno');		

		var whlocation = request.getParameter('custparam_whlocation');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		
		var getreportNo=request.getParameter('custparam_enterrepno');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');

		nlapiLogExecution('ERROR', 'entereditem', getitem);
		nlapiLogExecution('ERROR', 'entereditemid', getitemid);
		nlapiLogExecution('ERROR', 'enteredloc', getloc);
		nlapiLogExecution('ERROR', 'enteredlocid', getlocid);		
		nlapiLogExecution('ERROR', 'taskpriority', taskpriority);
		nlapiLogExecution('ERROR', 'getreportNo', getreportNo);

//		var actEndLocationId = request.getParameter('custparam_reppicklocno');
//		var actEndLocation = request.getParameter('custparam_reppickloc');


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{

			st0 = "REPOSICI&#211;N DE CONTINUAR";
			st1 = "CONTINUAR ";
			st2 ="Reposici&#243;n Confirmado &#233;xito"

		}
		else
		{
			st0 = "REPLENISHMENT CONTINUE";
			st1 = "CONTINUE ";
			st2 = "Replenishment Confirmed Successfully"


		}		

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('cmdPrevious').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<input type='hidden' name='hdnclustno' value=" + vClustno + ">";
		html = html + "				<input type='hidden' name='hdnrepno' value=" + reportNo + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
		html = html + "				<td align = 'left'>"+st2+"</td>";
		html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
			html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
		
		html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
		html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
		html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
		html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
		html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " <input name='cmdPrevious' id='cmdPrevious' type='submit' value='F6'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdPrevious').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		//hdnclustno
		var vClustNo = request.getParameter('hdnclustno');
		var repno = request.getParameter('hdnrepno');	
		var whLocation = request.getParameter('hdnwhlocation');

		var optedEvent = request.getParameter('cmdPrevious');

		var Reparray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		Reparray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);


		var st2;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{

			st2 = "ENTRADA NO V&#193;LIDA";
		}
		else
		{

			st2 = "INVALID ENTRY";

		}

		Reparray["custparam_repno"] =repno;
		Reparray["custparam_whlocation"] = whLocation;
		Reparray["custparam_error"] = st2;
		Reparray["custparam_screenno"] = 'R5';
		
		Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
		Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
		Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
		Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
		Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
		Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
		
		var getitemid = request.getParameter('hdngetitemid');
		var getlocid = request.getParameter('hdngetlocid');
		var taskpriority = request.getParameter('hdntaskpriority');
		var taskpriority = request.getParameter('hdntaskpriority');
		var getreportNo = request.getParameter('hdngetreportno');
		var replenType = request.getParameter('hdnReplenType');
		

		if (optedEvent == 'F6') {
			if (vClustNo != null && vClustNo != "") {
				var Repfilters = new Array();
				Repfilters[0] = new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClustNo);
				Repfilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
				Repfilters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]);

				var RepColumns = new Array();
				RepColumns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
				RepColumns[1] = new nlobjSearchColumn('custrecord_sku');
				RepColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
				RepColumns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Repfilters, RepColumns);

				if (searchresults != null && searchresults.length > 0) {
					nlapiLogExecution('ERROR', 'Into searchresults');
					Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
					Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku');
					Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
					Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
					Reparray["custparam_repid"] = searchresults[0].getId();
					Reparray["custparam_clustno"] = vClustNo;						

					Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc');
					nlapiLogExecution('ERROR','beginLocationId', Reparray["custparam_repbeglocId"]);

					response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
					return;
				}
				else
				{
					updateStageTasks();
					response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false);		
				}
			}
			if (repno != null && repno != "") {	        	
				//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
				var searchresults =getReplenTasks(repno,getitemid,getlocid,taskpriority,getreportNo,replenType);

				if (searchresults != null && searchresults.length > 0) {
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

					if (Reparray["custparam_repno"] != null) {
						Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
						Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku');
						Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
						Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
						Reparray["custparam_repid"] = searchresults[0].getId();
						Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc');
						Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc');

						Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc');
						nlapiLogExecution('ERROR','beginLocationId', Reparray["custparam_repbeglocId"]);

						response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
						return;
					}
					else {
						updateStageTasks();
						response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false,Reparray);                		 
						return;	
					}
				}
				else {	            	
					updateStageTasks();
					nlapiLogExecution('DEBUG', 'REPLENISHMENT Continue F6 Pressed');
					response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false,Reparray);

//					nlapiLogExecution('ERROR', 'getIntransitReplenTasks');
//					var searchresults = getIntransitReplenTasks(repno);
//					nlapiLogExecution('ERROR', 'After getIntransitReplenTasks');

//					if (searchresults != null && searchresults.length > 0) {
//					nlapiLogExecution('ERROR', 'In getIntransitReplenTasks');	
//					Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
//					Reparray["custparam_repsku"] = searchresults[0].getValue('custrecord_sku');
//					Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
//					Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
//					Reparray["custparam_repid"] = searchresults[0].getId();
//					Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc');
//					Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc');

//					response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
//					return;
//					}
//					else
//					{
//					updateStageTasks();
//					response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false);
//					return;
				}			
			}
			else{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				nlapiLogExecution('ERROR', 'Invalid Report #', repno);
			}
		}
	}
}

/**
 * 
 * @param reportNo
 * @returns
 */
function getReplenTasks(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	var filters = new Array();
	/*filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);*/
	
	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);
	
		
	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));
		
	}
	
	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
		
	}

	//if(getBinLocationId!=null && getBinLocationId!='')
	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
		
	}

	//if(getPriority!=null && getPriority!='')
	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
		
	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_actendloc');    

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

/**
 * 
 * @param reportNo
 * @returns
 */
function getIntransitReplenTasks(reportNo)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [24]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_actendloc');    
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;    

}

/**
 * 
 * @param skuNo
 * @returns
 */
function getPickFaceLoc(skuNo)
{
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skuNo);
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_pickbinloc');

	var searchresult = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filter, column);
	return searchresult;
}

/**
 * 
 */
function updateStageTasks()
{
	nlapiLogExecution('ERROR','Into Update Stage Tasks');

	try {
		var taskFilters = new Array();
		taskFilters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [24]);
		taskFilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is','8');

		var taskColumns = new Array();
		taskColumns[0] = new nlobjSearchColumn('custrecord_wms_status_flag');
		taskColumns[1] = new nlobjSearchColumn('custrecord_invref_no');

		var taskSearch = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);

		if (taskSearch != null)
		{
			for (var i=0; i< taskSearch.length; i++)
			{	
				var taskId = taskSearch[i].getId();
				nlapiLogExecution('ERROR','taskId',taskId);
				var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', taskId);
				transaction.setFieldValue('custrecord_wms_status_flag', 20);

				var taskResult = nlapiSubmitRecord(transaction);
			}
		}
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed', e);
	}
}