/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_Packing_SL.js,v $
<<<<<<< ebiz_Packing_SL.js
<<<<<<< ebiz_Packing_SL.js
 *     	   $Revision: 1.1.2.4.4.3.4.44.2.10 $
 *     	   $Date: 2015/12/04 15:15:01 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.1.2.4.4.3.4.44.2.10 $
 *     	   $Date: 2015/12/04 15:15:01 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.1.2.4.4.3.4.44.2.9
=======
<<<<<<< ebiz_Packing_SL.js
 *     	   $Revision: 1.1.2.4.4.3.4.44.2.10 $
 *     	   $Date: 2015/12/04 15:15:01 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.1.2.4.4.3.4.44.2.10 $
 *     	   $Date: 2015/12/04 15:15:01 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.1.2.4.4.3.4.44.2.9
>>>>>>> 1.1.2.4.4.3.4.41
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Packing_SL.js,v $
 * Revision 1.1.2.4.4.3.4.44.2.10  2015/12/04 15:15:01  grao
 * 2015.2 Issue Fixes 201415441
 *
 * Revision 1.1.2.4.4.3.4.44.2.9  2015/12/04 08:42:20  aanchal
 * 2015.2 Issue fix
 * 201416009
 *
 * Revision 1.1.2.4.4.3.4.44.2.8  2015/11/28 18:00:12  rrpulicherla
 * Lovely skin issue fixes
 *
 * Revision 1.1.2.4.4.3.4.44.2.7  2015/11/10 16:51:03  snimmakayala
 * 201414260
 *
 * Revision 1.1.2.4.4.3.4.44.2.6  2015/11/09 17:36:39  mpragada
 * case# 201413537
 * UCC # not updating in LP Master
 *
 * Revision 1.1.2.4.4.3.4.44.2.5  2015/11/09 15:44:21  mpragada
 * case# 201413537
 * UCC # not updating in LP Master
 *
 * Revision 1.1.2.4.4.3.4.44.2.3  2015/11/05 17:42:21  grao
 * 2015.2 Issue Fixes 201413012
 *
 * Revision 1.1.2.4.4.3.4.44.2.2  2015/11/05 07:53:08  deepshikha
 * 2015.2 issue fixes
 * 201415366
 *
 * Revision 1.1.2.4.4.3.4.44.2.1  2015/09/23 14:57:56  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.2.4.4.3.4.44  2015/08/07 10:19:07  schepuri
 * case# 201413894
 *
 * Revision 1.1.2.4.4.3.4.43  2015/07/23 15:34:40  grao
 * 2015.2   issue fixes  201413031
 *
 * Revision 1.1.2.4.4.3.4.42  2015/07/13 15:35:50  grao
 * 2015.2 ssue fixes  201413031:
 *
 * Revision 1.1.2.4.4.3.4.41  2015/07/13 15:31:13  skreddy
 * Case# 201413462
 * Briggs SB issue fix
 *
 * Revision 1.1.2.4.4.3.4.40  2015/05/14 07:15:18  schepuri
 * case# 201412734
 *
 * Revision 1.1.2.4.4.3.4.39  2015/05/12 14:15:52  schepuri
 * case# 201412731
 *
 * Revision 1.1.2.4.4.3.4.38  2015/05/08 09:35:44  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.4.4.3.4.37  2015/05/07 14:38:02  skreddy
 * Case# 201412597
 * Packlist Printing after Packing completed
 *
 * Revision 1.1.2.4.4.3.4.36  2014/12/24 13:34:32  schepuri
 * issue# 201411217
 *
 * Revision 1.1.2.4.4.3.4.35  2014/09/19 07:41:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Changes
 *
 * Revision 1.1.2.4.4.3.4.34  2014/08/14 10:00:40  sponnaganti
 * case# 20149845
 * stnd bundle issue fix
 *
 * Revision 1.1.2.4.4.3.4.33  2014/08/07 15:32:33  sponnaganti
 * case# 20149859 20149860
 * Stnd Bundle issue fix
 *
 * Revision 1.1.2.4.4.3.4.32  2014/07/30 15:22:45  grao
 * Case#: 20149750  Standard bundel  issue fixes
 *
 * Revision 1.1.2.4.4.3.4.31  2014/07/15 16:28:45  sponnaganti
 * Case# 20149407
 * Stnd Bundle issue fix
 *
 * Revision 1.1.2.4.4.3.4.30  2014/06/10 13:17:06  rmukkera
 * Case # 20148832
 * VRA functionality added
 *
 * Revision 1.1.2.4.4.3.4.29  2014/05/16 13:21:44  sponnaganti
 * case# 20148424
 * standard BUndle issue fix
 *
 * Revision 1.1.2.4.4.3.4.28  2014/04/22 09:15:53  nneelam
 * case#  20148112
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.4.4.3.4.27  2014/04/21 15:50:38  nneelam
 * case#  20148059�
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.4.4.3.4.26  2014/03/19 07:05:12  gkalla
 * case#20127139,20127525
 * MHP added batch number and nuCourse issue fix
 *
 * Revision 1.1.2.4.4.3.4.25  2014/03/13 14:20:30  nneelam
 * case#  20127573
 * nuCourse Fix.
 *
 * Revision 1.1.2.4.4.3.4.24  2014/02/24 16:19:23  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdbundleissues
 *
 * Revision 1.1.2.4.4.3.4.23  2014/02/18 06:09:40  gkalla
 * case#20127139
 * NuCourse double picks issue
 *
 * Revision 1.1.2.4.4.3.4.22  2014/02/13 14:52:27  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Paking issue fixes
 *
 * Revision 1.1.2.4.4.3.4.21  2014/02/12 07:37:01  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127138
 *
 * Revision 1.1.2.4.4.3.4.20  2014/02/07 15:48:50  nneelam
 * case#  20126956
 * std bundle issue fix
 *
 * Revision 1.1.2.4.4.3.4.19  2014/02/07 07:22:02  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127096
 *
 * Revision 1.1.2.4.4.3.4.18  2014/01/31 11:29:25  snimmakayala
 * Case# : 20126828
 *
 * Revision 1.1.2.4.4.3.4.17  2014/01/30 15:24:07  rmukkera
 * Case # 20127010
 *
 * Revision 1.1.2.4.4.3.4.16  2014/01/10 16:02:37  skreddy
 * case#20126716
 * Nucourse Prod issue
 *
 * Revision 1.1.2.4.4.3.4.15  2014/01/06 13:33:05  snimmakayala
 * Case# : 20125731
 * Productivity Report Related Changes
 *
 * Revision 1.1.2.4.4.3.4.14  2013/12/19 16:23:22  gkalla
 * case#20126401
 * Standard bundle issue
 *
 * Revision 1.1.2.4.4.3.4.13  2013/12/12 16:42:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * prodissue fixes
 *
 * Revision 1.1.2.4.4.3.4.12  2013/11/29 10:22:03  snimmakayala
 * Case# : 20125973
 * MHP UAT Fixes.
 *
 * Revision 1.1.2.4.4.3.4.11  2013/11/09 01:19:21  gkalla
 * case#20124991
 * file Cannot read property "0" issue
 *
 * Revision 1.1.2.4.4.3.4.10  2013/10/22 15:37:40  rmukkera
 * Case # 20125186
 *
 * Revision 1.1.2.4.4.3.4.9  2013/09/30 16:38:53  gkalla
 * Case# 20124654
 * Packing by fulfillment order level issue for Cesium
 *
 * Revision 1.1.2.4.4.3.4.8  2013/08/27 14:50:56  skreddy
 * Case#: 20124077
 * DJN issue fix
 *
 * Revision 1.1.2.4.4.3.4.7  2013/08/26 15:32:59  rmukkera
 * Case#  20123991
 *
 * Revision 1.1.2.4.4.3.4.6  2013/08/20 15:33:04  rmukkera
 * Issue Fix related to 20123934�
 *
 * Revision 1.1.2.4.4.3.4.5  2013/07/02 13:31:29  rrpulicherla
 * Case# 20123269
 * Packing issues
 *
 * Revision 1.1.2.4.4.3.4.4  2013/06/20 14:57:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * FM Multi location issues
 *
 * Revision 1.1.2.4.4.3.4.3  2013/06/19 06:53:50  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue related to validating location is active or not.
 *
 * Revision 1.1.2.4.4.3.4.2  2013/04/18 13:50:30  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.1.2.4.4.3.4.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.1.2.4.4.3  2012/12/11 10:24:02  dtummala
 * LOG2012392
 * Packing screen modifications for DJN
 *
 * Revision 1.1.2.4  2012/08/22 15:52:16  gkalla
 * CASE201112/CR201113/LOG201121
 * Packing for TO and orders filling for morethan 1000
 *
 * Revision 1.1.2.3  2012/07/31 10:56:30  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue related to Auto Assign LP
 *
 * Revision 1.1.2.2  2012/06/15 15:46:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Packing
 *
 * Revision 1.1.2.1  2012/06/14 17:34:28  gkalla
 * CASE201112/CR201113/LOG201121
 * GUI Packing
 *
 * Revision 1.7.2.2  2012/04/20 09:35:32  vrgurujala
 * CASE201112/CR201113/LOG201121
 * t_NSWMS_LOG201121_120
 *
 * Revision 1.7.2.1  2012/04/12 21:44:55  spendyala
 * CASE201112/CR201113/LOG201121
 * Showing meaningful data instead of showing incomplete data.
 *
 * Revision 1.7  2011/12/22 18:42:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Shipping
 *
 * Revision 1.6  2011/09/12 07:04:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/08/27 06:48:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Manifest related fixes.
 *
 * Revision 1.4  2011/08/25 16:06:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Manifest related fixes.
 *
 * Revision 1.3  2011/08/24 15:24:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pack Location is added to the sublist and PACK task creation.
 *
 * Revision 1.2  2011/08/23 13:30:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Comments and Logs added
 *
 * Revision 1.1  2011/08/23 12:54:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Assign Pack Station - New Screen developed
 * 
 *****************************************************************************/

function ebiz_AssignPackStation_SL(request, response)
{
	if (request.getMethod() == 'GET'){
		// Call method to render the Packing criteria
		showAssignPackStationCriteria(request,response);
		var printername=request.getParameter('printerName');
		nlapiLogExecution('ERROR', 'IntoPrinterName',printername);
	} 
	else{
		if (request.getParameter('custpage_orderno') != null){
			nlapiLogExecution('ERROR', 'Displaying fulfillment order search results');

			var form = nlapiCreateForm('Packing');
			//Case# 20124405� start
			var vConfig=nlapiLoadConfiguration('accountingpreferences');
			var ruleValue='';var DaysOverDue='';
			if(vConfig != null && vConfig != '')
			{
				DaysOverDue=vConfig.getFieldValue('CREDLIMDAYS');
				ruleValue=vConfig.getFieldValue('CUSTCREDLIMHANDLING');
			}

			var Printer=request.getParameter('custpage_printername');
			nlapiLogExecution('ERROR', 'printername  first else',Printer);

			var daysoverduefield= form.addField('custpage_daysoverdue', 'text', '').setDisplayType('hidden');
			var rulevaluefield= form.addField('custpage_rulevalue', 'text', '').setDisplayType('hidden');
			daysoverduefield.setDefaultValue(DaysOverDue);
			rulevaluefield.setDefaultValue(ruleValue);
			//case# 20124405� end

			form.setScript('customscript_ebiz_packing_cl');
			// Call method to display the selected fulfillment order candidates
			displayPossibleFulfillmentOrders(request, response, form);
		}
		else{
			nlapiLogExecution('ERROR', 'Perform Packing for Selected Containers');

			var form = nlapiCreateForm('Packing');
			//Case# 20124405� start
			var vConfig=nlapiLoadConfiguration('accountingpreferences');
			var ruleValue='';var DaysOverDue='';
			if(vConfig != null && vConfig != '')
			{
				DaysOverDue=vConfig.getFieldValue('CREDLIMDAYS');
				ruleValue=vConfig.getFieldValue('CUSTCREDLIMHANDLING');
			}

			var Printer=request.getParameter('custpage_tempwarehouse');
			//nlapiLogExecution('ERROR', 'printername SecondElse',Printer);

			var daysoverduefield= form.addField('custpage_daysoverdue', 'text', '').setDisplayType('hidden');
			var rulevaluefield= form.addField('custpage_rulevalue', 'text', '').setDisplayType('hidden');
			daysoverduefield.setDefaultValue(DaysOverDue);
			rulevaluefield.setDefaultValue(ruleValue);
			//case# 20124405� end
			form.setScript('customscript_ebiz_packing_cl');
			processSelectedContainers(request, response, form);
		}
		response.writePage(form);	
	}
}


/**
 * This method will create a ship manifest record
 * @param vOrdNo
 * @param containerlp
 */
function CreateShipManifestRecord(vOrdNo,containerlp,compid,siteid,weight)
{
	try {
		nlapiLogExecution('ERROR','into CreateShipManifestRecord');
		nlapiLogExecution('ERROR','vOrdNo',vOrdNo);
		nlapiLogExecution('ERROR','containerlp',containerlp); 
		nlapiLogExecution('ERROR','Nweight', weight);
		if (vOrdNo != null && vOrdNo != "") { 
			var vCarrierType=getCarrierType(vOrdNo);
			if(vCarrierType == null || vCarrierType == '')
				vCarrierType='PC';
			CreateShippingManifestRecord(vOrdNo,containerlp,vCarrierType,null,weight);			
		}

	}
	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
	}
}


/**
 * Function to render the search criteria to display all the possible containers
 * that can be packed based on the provided search criteria
 * 
 * @param request
 * @param response
 */
function showAssignPackStationCriteria(request, response){
	var form = nlapiCreateForm('Packing');
	//form.setScript('customscript_ebiz_packing_cl');
	// Fullfillment order number in search criteria
	var selectSO = form.addField('custpage_orderno', 'text', 'Order #');
	var temphtmlstring= form.addField('custpage_temphtmlstring', 'longtext', 'temphtmlstring').setDisplayType('hidden');
	//temphtmlstring.setDefaultValue();

	var tempimgurl= form.addField('custpage_tempimgurl', 'text', 'tempimgurl').setDisplayType('hidden');
	//tempimgurl.setDefaultValue();

	var temphtmlstring= form.addField('custpage_tempnewcontlp', 'text', 'tempnewcontlp').setDisplayType('hidden');
	//temphtmlstring.setDefaultValue(newContainerLpNo);

	var workstation=request.getParameter('printerName');
	var selectprintername=form.addField('custpage_printername', 'select', 'Printer Name').setMandatory(true);
//	selectprintername.addSelectOption("","");//case# 201412734

	var Carton = form.addField('custpage_containerlp', 'text', 'CARTON #');
	// Retrieve all Work Stations
	var printernameList = getAllprinternames();

	// Add all Work Stations to workstation Field
	addAllPrinterNamesToField(form,selectprintername, printernameList);
//	nlapiLogExecution('ERROR', 'WorkStation After Save',workstation);
	if(workstation!=null && workstation!='')
	{	
		selectprintername.setDefaultValue(workstation);	
	}



//	//Retrieve all printers
//	var printernameList = getAllprinternames();
//	addAllPrinterNamesToField(form,selectprintername, printernameList);
//	var printername=request.getParameter('custpage_printername');
////	form.addField('custpage_vprintername','text','printervalue');
////	nlapiLogExecution('ERROR', 'Printername After Save',printername);
//	if(printername!=null && printername!='')
//	{	
//	selectprintername.setDefaultValue(printername);	
//	}
	//selectSO.addSelectOption("","");

	// Retrieve all sales orders
	//var salesOrderList = getAllSalesOrders(-1);

	// Add all sales orders to SO Field
	//addAllSalesOrdersToField(form,selectSO, salesOrderList);
	var OrderType = form.addField('custpage_ordertype', 'select', 'Transaction Type').setMandatory(true);
//	OrderType.addSelectOption("","");//case# 201412734
	OrderType.addSelectOption("salesorder","SO");
	OrderType.addSelectOption("transferorder","TO");
	OrderType.addSelectOption("vendorreturnauthorization","VRA");
	if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
	{
		OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
	}
	var button = form.addSubmitButton('Display');
	response.writePage(form);
}
//function to retrieve the printer names
function getAllprinternames(){

	var colsWorkstation = new Array();
//	colsSO[0] = new nlobjSearchColumn('name');
	colsWorkstation[0] = new nlobjSearchColumn('custrecord_printer_printername',null, 'group');
	var searchresults = nlapiSearchRecord('customrecord_printer_preferences', null, null, colsWorkstation);

	return searchresults;
}
//Add printer name
function addAllPrinterNamesToField(form,selectprintername, printernameList){
	if(printernameList!=null && printernameList!='')
	{
		//nlapiLogExecution('ERROR','printernameLength',printernameList.length);
		if(printernameList != null && printernameList.length > 0){
			for (var i = 0; i < printernameList.length; i++) 
			{			
				selectprintername.addSelectOption(printernameList[i].getValue('custrecord_printer_printername',null,'group'), printernameList[i].getValue('custrecord_printer_printername',null,'group'));
			}
		}
	}
}
/**
 * Function to retrieve sales orders
 * @returns
 */
/*function getAllSalesOrders(){
	var filtersSO = new Array();		
	filtersSO.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)		
	filtersSO.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	filtersSO.push(new nlobjSearchFilter('recordType', 'custrecord_ebiz_order_no', 'is', 'salesorder'));
	var colsSO = new Array();
	//colsSO[0] = new nlobjSearchColumn('name');
	colsSO[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	colsSO[0].setSort(true);
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersSO, colsSO);

	return searchresults;
}*/
var vOrdArr=new Array();
function getAllSalesOrders(maxno){
	var filtersSO = new Array();		
	filtersSO.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)		
	filtersSO.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	//filtersSO.push(new nlobjSearchFilter('recordType', 'custrecord_ebiz_order_no', 'is', 'salesorder'));
	if(maxno!=-1)
	{
		filtersSO.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
	}

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('id');	 
	columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columnsinvt[1].setSort(true);

	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersSO,columnsinvt);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {


		vOrdArr.push(customerSearchResults[i]);
	}

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		getAllSalesOrders(maxno);	
	}
	return vOrdArr;
}

/**
 * Function to add all sales orders in the list passed to the passed field
 * 
 * @param selectSO
 * @param salesOrderList
 */
function addAllSalesOrdersToField(form,selectSO, salesOrderList){
	if(salesOrderList != null && salesOrderList.length > 0){
		for (var i = 0; i < salesOrderList.length; i++) 
		{
			var res=  form.getField('custpage_orderno').getSelectOptions(salesOrderList[i].getText('custrecord_ebiz_order_no'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			selectSO.addSelectOption(salesOrderList[i].getValue('custrecord_ebiz_order_no'), salesOrderList[i].getText('custrecord_ebiz_order_no'));
		}
	}
}

/**
 * 
 * @param request
 * @param response
 * @param form
 */
function displayPossibleFulfillmentOrders(request, response, form){
	var vSo = request.getParameter('custpage_orderno');
	var ordertype = request.getParameter('custpage_ordertype');
	//var ebizContlp=request.getParameter('custpage_ebizcontlp');
	var ebizContlp=request.getParameter('custpage_containerlp');
	nlapiLogExecution('ERROR','vSo',vSo);
	var Printer=request.getParameter('custpage_printername');
	//	nlapiLogExecution('ERROR','workstationNameInFirstFUnctionIF',Printer);
	var tempprinter= form.addField('custpage_tempwarehouse', 'text', 'tempwarehouse').setDisplayType('hidden');
	//var tempprinter= form.addField('custpage_tempwarehouse', 'text', 'tempwarehouse');
	tempprinter.setDefaultValue(Printer);
	if(ebizContlp !=null && ebizContlp !='')
	{
		var vResult=CheckCortonNo(ebizContlp,'');
		nlapiLogExecution('DEBUG', 'vResult', vResult);
		if(vResult==true)
		{

			var CartonFilters = new Array();
			CartonFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ebizContlp));
			CartonFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//pick Confirm
			CartonFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));


			var SOColumns = new Array();
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
			SOColumns.push(new nlobjSearchColumn('name'));
			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, CartonFilters, SOColumns);
			if(SOSearchResults != null && SOSearchResults != '' && SOSearchResults.length>0)
			{
				nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length); 
				var Orderarray = SOSearchResults[0].getValue('name');	
				if(Orderarray !=null && Orderarray !='')
				{					
					var vOrder = Orderarray.split('.');
					vSo =vOrder[0];
				}
			}
			else
			{
				showInlineMessage(form, 'Error', 'Invalid Carton #', null);
				form.addButton('custpage_generatesearch','New Order','backtogeneratesearch()');
				return;

			}

		}
		else
		{
			showInlineMessage(form, 'Error', 'Invalid Carton #', null);
			form.addButton('custpage_generatesearch','New Order','backtogeneratesearch()');
			return;
		}
	}
	nlapiLogExecution('DEBUG', 'vSo', vSo); 
	var ordernomain=GetSOInternalId(vSo,ordertype);
	nlapiLogExecution('ERROR','orderno',ordernomain);
	var printername=request.getParameter('custpage_printername');
	//case# 20149407 starts
	var RoleLocation=getRoledBasedLocation();
	var FULFILLMENTATORDERLEVEL = getSystemRuleValue('IF001',RoleLocation);
	nlapiLogExecution('ERROR','FULFILLMENTATORDERLEVEL',FULFILLMENTATORDERLEVEL);
	//case# 20149407 ends
	if(ordernomain!=null && ordernomain!='' && ordernomain != -1){
		var vopenpicksexist="";
		if(FULFILLMENTATORDERLEVEL=='Y')
			vopenpicksexist = IsOpenPicksExist(ordernomain);
		nlapiLogExecution('ERROR','vopenpicksexist',vopenpicksexist);
		if(vopenpicksexist!='Y')
		{
			var salesOrderList = getSalesOrderlist(ordernomain,ebizContlp);

			var ordersFound = false;

			nlapiLogExecution('ERROR','salesOrderList',salesOrderList);
			if(salesOrderList != null && salesOrderList.length > 0){
				var trantype = nlapiLookupField('transaction', ordernomain, 'recordType');
				var vOrderMainText=ordernomain;
				var location;
				if(trantype=="salesorder")
				{
					var rec= nlapiLoadRecord('salesorder', ordernomain);
					if(rec != null && rec != '')
					{
						vOrderMainText=rec.getFieldValue('tranid');
						location=rec.getFieldValue('location');
					}
				}
				else if(trantype=="transferorder")
				{
					var rec= nlapiLoadRecord('transferorder', ordernomain);
					if(rec != null && rec != ''){
						vOrderMainText=rec.getFieldValue('tranid');
						location=rec.getFieldValue('location');
					}
				}
				//case# 20149859 20149860 starts(For Displaying VRA Number at Order # and scan item issue)
				else 
				{
					var rec= nlapiLoadRecord('vendorreturnauthorization', ordernomain);
					if(rec != null && rec != ''){
						vOrderMainText=rec.getFieldValue('tranid');
						location=rec.getFieldValue('location');
					}
				}
				//case# 20149859 20149860  ends
				// Creating a sublist and adding fields to the sublist
				addFieldsToSublist(form);
				nlapiLogExecution('ERROR','location',location);
				var message=form.addField('custpage_message', 'text', '').setLayoutType('outsideabove').setDisplayType('inline').setLayoutType('midrow');

				var button = form.addSubmitButton('Pack/Close Carton');
				form.addButton('custpage_autoassignlp','Auto Assign LP','AutoAssignLP()');
				form.addButton('custpage_generatesearch','New Order','backtogeneratesearch()');

				var cartongroup=form.addFieldGroup("cartoninfo", "Carton Details");

				var vOrdField=form.addField('custpage_ordhead', 'text', 'Order #',null,'cartoninfo').setDisplayType('inline').setDefaultValue(vOrderMainText);
				var vprintername=form.addField('custpage_ordprintername', 'text', 'printername',null,'cartoninfo').setDisplayType('hidden').setDefaultValue(printername);
				form.addField('custpage_contlp', 'text', 'Carton #',null,'cartoninfo').setMandatory(true);

				var OrderType = form.addField('custpage_ordertype', 'select', 'Transaction Type').setMandatory(true);
				//OrderType.addSelectOption("","");//case# 201412734
				OrderType.addSelectOption("salesorder","SO");
				OrderType.addSelectOption("transferorder","TO");
				OrderType.addSelectOption("vendorreturnauthorization","VRA");
				if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
				{
					OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
				}
				form.addField('custpage_weight', 'text', 'Carton Weight',null,'cartoninfo').setMandatory(true).setDefaultValue('0.1');
				var vfoField=form.addField('custpage_fonumber', 'text', 'FO #',null,'cartoninfo').setDisplayType('hidden').setDefaultValue(vSo);

				var contsize=form.addField('custpage_cartonsize', 'select', 'Carton Size',null,'cartoninfo').setMandatory(true);
				contsize.addSelectOption("", "");
				cartongroup.setShowBorder(true);


				var filtersloc = new Array();
				var vRoleLocation=getRoledBasedLocation();
				nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);

				if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
				{
					filtersloc.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'is', vRoleLocation));
				}

				filtersloc.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

				var columnsloc = new Array();
				columnsloc.push(new nlobjSearchColumn('name'));

				var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersloc, columnsloc);

				if (searchlocresults != null && searchlocresults!='')
				{

					nlapiLogExecution('ERROR','test1',searchlocresults.length);
					for (var i = 0; i < searchlocresults.length; i++) 
					{
						var res = form.getField('custpage_cartonsize').getSelectOptions(searchlocresults[i].getValue('name'), 'is');
						if (res != null) 
						{
							if (res.length > 0)	                
								continue;	                
						}
						contsize.addSelectOption(searchlocresults[i].getId(), searchlocresults[i].getValue('name'));
					}
				}




				nlapiLogExecution('ERROR',' ordernomain test1',ordernomain);
				nlapiLogExecution('ERROR',' ebizContlp test1',ebizContlp);
				var Filters = new Array();
				if(ordernomain != null && ordernomain != '')
					Filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', ordernomain));
				if(ebizContlp != null && ebizContlp != '')
					Filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ebizContlp));
				Filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//pick Confirm
				Filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));


				var SOColumns = new Array();
				SOColumns.push(new nlobjSearchColumn('custrecord_container'));
				SOColumns.push(new nlobjSearchColumn('name'));
				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filters, SOColumns);
				var vcontsize ='';
				if(SOSearchResults != null && SOSearchResults != '' && SOSearchResults.length>0)
				{
					vcontsize=SOSearchResults[0].getValue('custrecord_container');
				}
				nlapiLogExecution('ERROR',' default vcontsize',vcontsize);


				if(vcontsize!='' && vcontsize!=null)
				{
					contsize.setDefaultValue(vcontsize);	
				}




				/*if(request.getParameter('custpage_cartonsize')!='' && request.getParameter('custpage_cartonsize')!=null)
				{
					contsize.setDefaultValue(request.getParameter('custpage_cartonsize'));	
				}*/

				//vScanQtyFld.setLayoutType('startrow');
				var vAddBtn=form.addButton('custpage_addpack','Add Items','AddPackNew()');	
				//form.addField('custpage_message', 'label', 'abc');

				var itemgroup=form.addFieldGroup("iteminfo", "Add Item");

				var vBulk=form.addField('custpage_packtype', 'radio', 'By Bulk Qty', 'bb','iteminfo');

				form.addField('custpage_packtype', 'radio','By Item','bi');
				vBulk.setDefaultValue('bi');

				var vScanItemFld=form.addField('custpage_scanitem', 'text', 'Scan Item');
				var vScanQtyFld=form.addField('custpage_scanqty', 'text', 'Pack Qty');
				var test=form.addField('custpage_test', 'label', '');
				itemgroup.setShowBorder(true);

				form.addField('custpage_repeat', 'text', 'repeat').setDisplayType('hidden').setDefaultValue('0');		
				form.addField('custpage_ebizso', 'text', 'soid').setDisplayType('hidden').setDefaultValue(ordernomain);
				form.addField('custpage_lpcreate', 'text', 'repeat').setDisplayType('hidden');		

				var vitemimage=form.addField('custpage_image', 'inlinehtml', 'Item Image');
				vitemimage.setDefaultValue('image');		
				//vitemimage.setLayoutType('normal', 'startrow');

				// Get containers to pack based on search criteria defined

				var orderno, containerno,ebizorderno,ebizcntrlno,siteid,compid,containersize,totweight,skuno,qty,skutext,vLineno;	
				for (var i = 0; i < salesOrderList.length; i++){
					orderno = salesOrderList[i].getValue('name');
					var waveno=salesOrderList[i].getValue('custrecord_ebiz_wave_no');
					containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
					ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
					ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
					compid = salesOrderList[i].getValue('custrecord_comp_id');
					siteid = salesOrderList[i].getValue('custrecord_wms_location');
					containersize = salesOrderList[i].getValue('custrecord_container');
					totweight = salesOrderList[i].getValue('custrecord_total_weight');
					skuno = salesOrderList[i].getValue('custrecord_sku');
					skutext = salesOrderList[i].getText('custrecord_sku');
					qty = salesOrderList[i].getValue('custrecord_act_qty');
					var totCube = salesOrderList[i].getValue('custrecord_totalcube');
					//Added on 18th April 2012
					var vBatch=salesOrderList[i].getValue('custrecord_batch_no');
					nlapiLogExecution('ERROR', 'Site Value in Packing', siteid);
					vLineno = salesOrderList[i].getValue('custrecord_line_no');
					var parentitem = salesOrderList[i].getValue('custrecord_parent_sku_no');

//					case # 20127573 added upccode.
					//case# 20148424 starts
					//var poItemFields = ['displayname','upccode'];
					var poItemFields = ['salesdescription','upccode'];
					var poItemColumns = nlapiLookupField('item', skuno, poItemFields);
					var ItemDescription = poItemColumns.salesdescription;
					var upccode = poItemColumns.upccode;
					//upto to here on 18th aprril
					//case# 20148424 ends
					form.getSubList('custpage_items').setLineItemValue('custpage_order', i + 1, orderno);
					form.getSubList('custpage_items').setLineItemValue('custpage_wave', i + 1, waveno);
					form.getSubList('custpage_items').setLineItemValue('custpage_container', i + 1, containerno);
					form.getSubList('custpage_items').setLineItemValue('custpage_internalid', i + 1, salesOrderList[i].getId());
					form.getSubList('custpage_items').setLineItemValue('custpage_ebizordno', i + 1, ebizorderno);
					form.getSubList('custpage_items').setLineItemValue('custpage_ebizcntrlno', i + 1, ebizcntrlno);
					form.getSubList('custpage_items').setLineItemValue('custpage_compid', i + 1, compid);
					form.getSubList('custpage_items').setLineItemValue('custpage_siteid', i + 1, siteid);
					form.getSubList('custpage_items').setLineItemValue('custpage_containersize', i + 1, containersize);
					form.getSubList('custpage_items').setLineItemValue('custpage_totalweight', i + 1, totweight);
					form.getSubList('custpage_items').setLineItemValue('custpage_ebizskuno', i + 1, skuno);
					form.getSubList('custpage_items').setLineItemValue('custpage_ebizsku', i + 1, skutext);
					form.getSubList('custpage_items').setLineItemValue('custpage_ebizupccode', i + 1, upccode);
					if(ItemDescription!=null && ItemDescription!='')
						form.getSubList('custpage_items').setLineItemValue('custpage_ebizskudesc', i + 1, ItemDescription);
					//form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, qty);
					form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i + 1, qty);
					form.getSubList('custpage_items').setLineItemValue('custpage_pickqty', i + 1, qty);
					form.getSubList('custpage_items').setLineItemValue('custpage_totcube', i + 1, totCube);
					form.getSubList('custpage_items').setLineItemValue('custpage_batch', i + 1, vBatch);
					form.getSubList('custpage_items').setLineItemValue('custpage_parentsku', i + 1,parentitem);
					form.getSubList('custpage_items').setLineItemValue('custpage_ordlineno', i + 1, vLineno);					

					ordersFound = true;
				}
			}

			if(!ordersFound){
				nlapiLogExecution('ERROR', 'No containers to pack', '0');
				//var form = nlapiCreateForm('Packing');
				//showInlineMessage(form, 'Confirmation', 'No containers to pack', null);
				showInlineMessage(form, 'Confirmation', 'No containers to pack', null);
				form.addButton('custpage_generatesearch','New Order','backtogeneratesearch()');
			}
		}
		else
		{
			showInlineMessage(form, 'Error', 'PICKS ARE PENDING FOR THIS ORDER. PLEASE CONTINUE WITH PICKING.', null);
			form.addButton('custpage_generatesearch','New Order','backtogeneratesearch()');
		}
	}
	else
	{
		showInlineMessage(form, 'Error', 'Invalid Order #', null);
		form.addButton('custpage_generatesearch','New Order','backtogeneratesearch()');
	}
}


function addFieldsToSublist(form){

	//var sublist = form.addSubList("custpage_items", "inlineeditor",	"Packing");
	var sublist = form.addSubList("custpage_items", "list", "Packing");

	//sublist.addMarkAllButtons();
	//sublist.addField("custpage_select", "checkbox", "Assign").setDisplayType('hidden');
	sublist.addField("custpage_order", "text", "Order #").setDisplayType('inline');
	sublist.addField("custpage_wave", "text", "Wave #").setDisplayType('inline');
	sublist.addField("custpage_container", "text", "Container #").setDisplayType('inline');	
	sublist.addField("custpage_ebizsku", "text", "Item").setDisplayType('inline');
	sublist.addField("custpage_ebizupccode", "text", "UPC Code").setDisplayType('inline');
	sublist.addField("custpage_ebizskudesc", "textarea", "Item Desc").setDisplayType('inline');
	sublist.addField("custpage_pickqty", "text", "Pick Qty").setDisplayType('inline');
	sublist.addField("custpage_batch", "text", "Lot#").setDisplayType('inline');
	sublist.addField("custpage_qty", "text", "Pack Qty").setDisplayType('entry').setDefaultValue(0);
	sublist.addField("custpage_actqty", "text", "ActQty").setDisplayType('hidden');
	sublist.addField("custpage_internalid", "text", "Internal Id").setDisplayType('hidden');
	sublist.addField("custpage_ebizordno", "text", "FF Ebiz ORD #").setDisplayType('hidden');
	sublist.addField("custpage_ebizcntrlno", "text", "FF Ebiz Cntrl #").setDisplayType('hidden');
	sublist.addField("custpage_compid", "text", "Comp Id").setDisplayType('hidden');
	sublist.addField("custpage_siteid", "text", "Site Id").setDisplayType('hidden');
	sublist.addField("custpage_containersize", "text", "Container Size").setDisplayType('hidden');
	sublist.addField("custpage_totalweight", "text", "Total Weight").setDisplayType('hidden');
	sublist.addField("custpage_ebizskuno", "text", "SKU").setDisplayType('hidden');
	sublist.addField("custpage_totcube", "text", "cube").setDisplayType('hidden');
	/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/	
	sublist.addField("custpage_ordlineno", "text", "LineNo").setDisplayType('hidden');
	sublist.addField("custpage_parentsku", "text", "Parent SKU").setDisplayType('hidden');
	sublist.addField("custpage_hiddenqty", "text", "hiddenPack Qty").setDisplayType('hidden');
	/* Up to here */ 

}

/**
 * 
 * @param orderno
 */
function getSalesOrderlist(orderno,ebizContlp){
	var filters = new Array();
	nlapiLogExecution('ERROR','orderno in getSalesOrderlist',orderno);
	nlapiLogExecution('DEBUG','ebizContlp',ebizContlp);

	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);

	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));
	}

	if(orderno!="" && orderno!=null){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', orderno));
	}
	//case 20126716 start added Container LP filter
	if((ebizContlp!=null)&&(ebizContlp!=''))
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ebizContlp));
	}
	//case 20126716 end
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_line_no');
	columns[2] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[5] = new nlobjSearchColumn('custrecord_comp_id');
	columns[6] = new nlobjSearchColumn('custrecord_site_id');
	columns[7] = new nlobjSearchColumn('custrecord_container');
	columns[8] = new nlobjSearchColumn('custrecord_total_weight');
	columns[9] = new nlobjSearchColumn('custrecord_sku');
	columns[10] = new nlobjSearchColumn('custrecord_act_qty');
	columns[11] = new nlobjSearchColumn('custrecord_wms_location');
	columns[12] = new nlobjSearchColumn('custrecord_totalcube');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/	
	columns[14] = new nlobjSearchColumn('custrecord_parent_sku_no');
	columns[15] = new nlobjSearchColumn('custrecord_batch_no');

	/* Up to here */ 
	columns[0].setSort();	
	columns[1].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	return searchresults;
}

function processSelectedContainers(request, response, form){

	//Start of Dinesh COde for getting sales order ID
	//custpage_orderno
	var vSo = request.getParameter('custpage_ordhead');
	var printername=request.getParameter('custpage_ordprintername');
	/*var printername=request.getParameter('custpage_printername');*/
	//nlapiLogExecution('ERROR','IntoContainerPrinterName',printername);
	//nlapiLogExecution('ERROR','vSo',vSo);
	nlapiLogExecution('ERROR', 'Time Stamp before GetSOInternalId',TimeStampinSec());
	var ordertype = request.getParameter('custpage_ordertype');
	var ebizContlp=request.getParameter('custpage_containerlp');
	if(ebizContlp !=null && ebizContlp !='')
	{
		var CartonFilters = new Array();
		CartonFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ebizContlp));
		CartonFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//pick Confirm
		CartonFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));


		var SOColumns = new Array();
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
		SOColumns.push(new nlobjSearchColumn('name'));
		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, CartonFilters, SOColumns);
		if(SOSearchResults != null && SOSearchResults != '' && SOSearchResults.length>0)
		{
			nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length); 
			var Orderarray = SOSearchResults[0].getValue('name');	
			if(Orderarray !=null && Orderarray !='')
			{					
				var vOrder = Orderarray.split('.');
				vSo =vOrder[0];
			}
		}

	}
	var ordernomain=GetSOInternalId(vSo,ordertype);
	nlapiLogExecution('ERROR','orderno',ordernomain);
	var vsiteid="";
	var ebizorderno;
	var OTContainerlpno;
	if(ordernomain != null && ordernomain != '' && ordernomain != -1){
		var salesOrderList = getSalesOrderlist(ordernomain);
		if(salesOrderList != null && salesOrderList!="")
		{
			ebizorderno = salesOrderList[0].getValue('custrecord_ebiz_order_no');
			vsiteid=salesOrderList[0].getValue('custrecord_wms_location');
			getFONO=salesOrderList[0].getValue('name');
			OTContainerlpno =salesOrderList[0].getValue('custrecord_container_lp_no'); 

		}
		else
			ebizorderno=ordernomain;

	}

	//Start of Dinesh COde for getting sales order ID
	var lineCount = request.getLineItemCount('custpage_items');
	nlapiLogExecution('ERROR','linecount', lineCount);
	var isRowSelected="";
	var Container="";
	var internalId="";
	var ffordno="";
	var lineNo="";
	var stageLoc="";
	var ebizordno="";
	var ebizcntrlno="";

	var compid="";
	var containersize="";
	var oldcontainer="";	
	var oldFFOrdNo="";
	var itemno='';
	var NewContLP = request.getParameter('custpage_contlp');
	var NewSizeId = request.getParameter('custpage_cartonsize');
	var NewWeight = request.getParameter('custpage_weight');
	var SalesOrdNo = request.getParameter('custpage_ordhead');
	var eBizSO = request.getParameter('custpage_ebizso');
	//var vsiteid = request.getParameter('custpage_siteid');
	var Autolp=request.getParameter('custpage_lpcreate');
	var foOrderNo = request.getParameter('custpage_fonumber');
	var tempflag="F";
	var Msg="";
	nlapiLogExecution('ERROR','eBizSO', eBizSO);
	nlapiLogExecution('ERROR','NewContLP', NewContLP);
	nlapiLogExecution('ERROR','vsiteid', vsiteid);
	nlapiLogExecution('ERROR','Autolp', Autolp);
	nlapiLogExecution('ERROR','OTContainerlpno', OTContainerlpno);

	if(NewContLP == null || NewContLP == '')
		newContainerLpNo=GetMaxLPNo('1', '2',vsiteid);
	else
	{
		if(OTContainerlpno == NewContLP)
		{
			newContainerLpNo = NewContLP;
		}
		else if(Autolp!='AUTO' && Autolp!='') // case# 201413894
		{
			var LPReturnValue = ebiznet_LPRange_CL_withLPType(NewContLP.replace(/\s+$/,""), '2','2',vsiteid);//'2'UserDefiend,'2'PICK
			nlapiLogExecution('ERROR','LPReturnValue', LPReturnValue);
			if(LPReturnValue==null || LPReturnValue=='')
			{
				//showInlineMessage(form, 'DEBUG', 'ContainerLP  ' + NewContLP + '  Out of range', "");
				Msg='ContainerLP  ' + NewContLP + '  Out of range';
				tempflag="T";
				//return;
			}
		}



		if(tempflag=="F")
		{
			var vOpenTaskRecs=fnValidateEnteredContLP(NewContLP,eBizSO,vsiteid);
			nlapiLogExecution('ERROR','vOpenTaskRecs', vOpenTaskRecs);
			if(vOpenTaskRecs != null && vOpenTaskRecs != '')
			{
				nlapiLogExecution('DEBUG','ContLP already exists for other order');
				//showInlineMessage(form, 'DEBUG', 'ContainerLP  ' + NewContLP + '  Already exists', "");
				Msg='ContainerLP  ' + NewContLP + '  Already exists';
				tempflag="T";
				//return;
			}
			else
			{
				var vOpenTaskRecsL2=fnValidateEnteredContLPLevel2(NewContLP,eBizSO,vsiteid);
				if(vOpenTaskRecsL2 != null && vOpenTaskRecsL2 != '')
				{
					nlapiLogExecution('DEBUG','ContLP already packed to this order');
					//showInlineMessage(form, 'DEBUG', 'ContainerLP  ' + NewContLP + '  Already exists', "");
					Msg='ContainerLP  ' + NewContLP + '  Already exists';
					tempflag="T";
					//return;
				}

			}
			if(tempflag == 'F')
			{	
				CreateLP(NewContLP,NewSizeId,NewWeight);
				newContainerLpNo=NewContLP;
			}
		}
	}
	nlapiLogExecution('DEBUG','tempflag/Msg', tempflag+"/"+Msg);
	if(tempflag=="T")
	{
		showInlineMessage(form, 'DEBUG', Msg, "");
		return true;
	}
	else
	{
		var FirstTaskIndex=0;
		var currentUserID = getCurrentUser();
		var vCartTotWeight=0;
		var vCartTotCube=0;	
		var vLastIndex=1;
		for(var s = 1; s <= lineCount; s++){
			//isRowSelected= request.getLineItemValue('custpage_items', 'custpage_select', k);
			var vNewPackQtyTemp= request.getLineItemValue('custpage_items', 'custpage_qty', s);

			var isRowSelectedTmp='F';
			if(vNewPackQtyTemp != null && vNewPackQtyTemp != '' && parseFloat(vNewPackQtyTemp) > 0)
			{	
				isRowSelectedTmp='T';
				vLastIndex=s;
			}
		}
		var vOldFO;
		var foarr = new Array();
		var LineIndexarr = new Array();
		var vOrdArrToUpdate=new Array();
		for(var k = 1; k <= lineCount; k++){
			//isRowSelected= request.getLineItemValue('custpage_items', 'custpage_select', k);
			var vNewPackQty= request.getLineItemValue('custpage_items', 'custpage_qty', k);

			isRowSelected='F';
			if(vNewPackQty != null && vNewPackQty != '' && parseFloat(vNewPackQty) > 0)
				isRowSelected='T';
			nlapiLogExecution('ERROR','vNewPackQty', vNewPackQty);
			nlapiLogExecution('ERROR','Record Selected?', isRowSelected);
			if(isRowSelected == "T"){
				FirstTaskIndex=k;
				Container = request.getLineItemValue('custpage_items', 'custpage_container', k);
				internalId= request.getLineItemValue('custpage_items', 'custpage_internalid', k);
				ffordno = request.getLineItemValue('custpage_items', 'custpage_order', k);
				//stageLoc = request.getLineItemValue('custpage_items', 'custpage_packlocation', k);
				ebizordno = request.getLineItemValue('custpage_items', 'custpage_ebizordno', k);
				ebizcntrlno = request.getLineItemValue('custpage_items', 'custpage_ebizcntrlno', k);					
				compid = request.getLineItemValue('custpage_items', 'custpage_compid', k);
				siteid = request.getLineItemValue('custpage_items', 'custpage_siteid', k);
				containersize = request.getLineItemValue('custpage_items', 'custpage_containersize', k);
				itemno = request.getLineItemValue('custpage_items', 'custpage_ebizskuno', k);
				var qty = request.getLineItemValue('custpage_items', 'custpage_qty', k);
				var actqty = request.getLineItemValue('custpage_items', 'custpage_actqty', k);
				var taskCube = request.getLineItemValue('custpage_items', 'custpage_totcube', k);
				var taskWeight = request.getLineItemValue('custpage_items', 'custpage_totalweight', k);
				lineNo = request.getLineItemValue('custpage_items', 'custpage_ordlineno', k);

				foarr.push(ffordno);
				LineIndexarr.push(k);
				nlapiLogExecution('ERROR','entered qty', qty);
				nlapiLogExecution('ERROR','actqty', actqty);
				nlapiLogExecution('ERROR','internalId', internalId);
				nlapiLogExecution('ERROR','vLastIndex', vLastIndex);
				var vDeviceUpdFlag='F';
				if(k+1<=parseInt(vLastIndex) )
				{	
					var NextFFordno = request.getLineItemValue('custpage_items', 'custpage_order', k+1);
					if(NextFFordno != ffordno)
						vDeviceUpdFlag='T';
				}
				else if(k==vLastIndex)
					vDeviceUpdFlag='T';
				nlapiLogExecution('ERROR','vDeviceUpdFlag', vDeviceUpdFlag);
				if(qty!=null && qty !='' && parseFloat(qty) == parseFloat(actqty))
				{
					if(taskWeight != null && taskWeight != '')
						vCartTotWeight= parseFloat(vCartTotWeight)+parseFloat(taskWeight);
					if(taskCube != null && taskCube != '')
						vCartTotCube= parseFloat(vCartTotCube)+parseFloat(taskCube);

					var blnUpdateSTGMTask=true;
					if(vOrdArrToUpdate.indexOf(ebizordno)!=-1)
					{	
						blnUpdateSTGMTask=false;
					}
					else
					{	
						vOrdArrToUpdate.push(ebizordno);
					}

					//updateStatusInOpenTaskNew(internalId,28,null,newContainerLpNo,NewSizeId,qty,currentUserID,null,vDeviceUpdFlag,Container,itemno,ebizordno,actqty,ffordno,lineNo);//28 - PACK Complete
					updateStatusInOpenTaskNew(internalId,28,NewWeight,newContainerLpNo,NewSizeId,qty,currentUserID,null,vDeviceUpdFlag,Container,itemno,
							ebizordno,actqty,ffordno,lineNo,blnUpdateSTGMTask);//28 - PACK Complete
				}	
				else if(qty!=null && qty !='' && parseFloat(qty) != parseFloat(actqty))
				{
					var vEachCube=0;
					var vEachWeight=0;
					if(taskCube != null && taskCube != '')
						vEachCube=parseFloat(taskCube) / parseFloat(actqty);
					if(taskWeight != null && taskWeight != '')
						vEachWeight=parseFloat(taskWeight) / parseFloat(actqty);



					nlapiLogExecution('ERROR','Into Diff');
					var vDiffQty=parseFloat(actqty) - parseFloat(qty);

					nlapiLogExecution('ERROR','Creating open task for remaining');
					//var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',internalId);

					var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',internalId);		    	
					createopentaskrec.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
					nlapiLogExecution('ERROR','OleContLP',createopentaskrec.getFieldValue('custrecord_container_lp_no'));
					//createopentaskrec.setFieldValue('custrecord_container_lp_no', newContainerLpNo);
					createopentaskrec.setFieldValue('custrecord_container', NewSizeId);	
					createopentaskrec.setFieldValue('custrecord_act_qty', parseFloat(vDiffQty).toFixed(5));
					createopentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(vDiffQty).toFixed(5));
					createopentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
					createopentaskrec.setFieldValue('custrecord_ebizuser', currentUserID);
					createopentaskrec.setFieldValue('custrecord_totalcube',(parseFloat(vEachCube)* parseFloat(vDiffQty)).toFixed(5));		
					createopentaskrec.setFieldValue('custrecord_total_weight',(parseFloat(vEachWeight)* parseFloat(vDiffQty)).toFixed(5));
					nlapiLogExecution('ERROR','ffordno',ffordno);
					createopentaskrec.setFieldValue('name',ffordno);

					//createopentaskrec.setFieldValue('custrecord_total_weight',parseFloat(varContainerTareWeight)+parseFloat(weightinfo2));	
					var vUpdWeight=parseFloat(vEachWeight)* parseFloat(qty);
					var vUpdCube=parseFloat(vEachCube)* parseFloat(qty);
					if(taskWeight != null && taskWeight != '')
						vCartTotWeight= parseFloat(vCartTotWeight)+parseFloat(vUpdWeight);
					if(taskCube != null && taskCube != '')
						vCartTotCube= parseFloat(vCartTotCube)+parseFloat(vUpdCube);

					nlapiSubmitRecord(createopentaskrec, false, true);

					var blnUpdateSTGMTask=true;
					if(vOrdArrToUpdate.indexOf(ebizordno)!=-1)
					{	
						blnUpdateSTGMTask=false;
					}
					else
					{	
						vOrdArrToUpdate.push(ebizordno);
					}

					updateStatusInOpenTaskNew(internalId,28,vUpdWeight,newContainerLpNo,NewSizeId,qty,currentUserID,vUpdCube,vDeviceUpdFlag,Container,itemno,
							ebizordno,actqty,ffordno,lineNo,blnUpdateSTGMTask);//28 - PACK Complete
					//updateStatusInOpenTaskNew(internalId,28,NewWeight,newContainerLpNo,NewSizeId,actqty);//28 - PACK Complete

				}
				//CreatePACKTask(compid,siteid,ebizordno,ffordno,newContainerLpNo,14,stageLoc,stageLoc,28,ebizcntrlno,itemno);//14 - Task Type - PACK

			}

		}
		var Htmlparameters = new Array(); 
		if(FirstTaskIndex != null && FirstTaskIndex != '' && FirstTaskIndex != 0)
		{	
			nlapiLogExecution('ERROR','foarr',foarr);
			//foarr = removeDuplicateElement(foarr);
			var wavenumber="";
			nlapiLogExecution('ERROR','foarr',foarr);
			var trantype = nlapiLookupField('transaction', ebizorderno, 'recordType');
			var salesorderdetails =nlapiLoadRecord(trantype, ebizorderno);
			nlapiLogExecution('ERROR', 'Time Stamp before CreatePackListCheckbyWmsStatusFlag',TimeStampinSec());
			Htmlparameters=CreatePackListCheckbyWmsStatusFlag(newContainerLpNo,getFONO,printername,wavenumber,salesorderdetails,trantype);
			nlapiLogExecution('ERROR', 'Time Stamp after CreatePackListCheckbyWmsStatusFlag',TimeStampinSec());
			nlapiLogExecution('ERROR', 'Htmlparameters',Htmlparameters);
			if(NewWeight == null || NewWeight == '')
			{
				var tareWeightCube=getContainerCubeAndTarWeight(NewSizeId,null);
				if( tareWeightCube != null && tareWeightCube != '' && tareWeightCube.length>2)
				{
					if(tareWeightCube[1] != null && tareWeightCube[1] != '')
					{
						NewWeight= parseFloat(vCartTotWeight)+parseFloat(tareWeightCube[1]);

					}
					if(tareWeightCube[0] != null && tareWeightCube[0] != '')
					{
						vCartTotCube= parseFloat(vCartTotCube)+parseFloat(tareWeightCube[0]);
					}
				}

			}	
			var vPackedFO=new Array();
			for (var t = 0; t < foarr.length; t++) 
			{
				var fono = foarr[t];
				if(vPackedFO.indexOf(fono)==-1)
				{	
					vPackedFO.push(fono);
					nlapiLogExecution('ERROR', 'Creating Pack Task',fono);
					var vFirstTaskIntId=request.getLineItemValue('custpage_items', 'custpage_internalid', LineIndexarr[t]);
					var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',vFirstTaskIntId);

					AssignPackStationrec.setFieldValue('name', fono); //14 stands for pack
					AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
					AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
					AssignPackStationrec.setFieldValue('custrecord_container_lp_no', newContainerLpNo);
					AssignPackStationrec.setFieldValue('custrecord_sku', null);
					AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
					AssignPackStationrec.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
					AssignPackStationrec.setFieldValue('custrecord_ebizuser', currentUserID);
					AssignPackStationrec.setFieldValue('custrecord_taskassignedto', currentUserID);
					AssignPackStationrec.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', "");
					AssignPackStationrec.setFieldValue('custrecord_device_upload_flag', "");
					AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', DateStamp());
					nlapiSubmitRecord(AssignPackStationrec, false, true);
				}
			}
			updateStatusInLPMasterNew(newContainerLpNo,NewWeight,vCartTotCube,28);

			//CreateShipManifestRecord(eBizSO,newContainerLpNo,NewSizeId,request.getLineItemValue('custpage_items', 'custpage_compid', FirstTaskIndex),request.getLineItemValue('custpage_items', 'custpage_siteid', FirstTaskIndex),NewWeight);
		}

		//Start of Dinesh COde for Getting the Order is Intrenational or Domestick

		//var shipmethod = nlapiLookupField('salesorder', ebizorderno, 'shipmethod');
		var shipmethod = nlapiLookupField(ordertype, ebizorderno, 'shipmethod');
		//var shipmethod=nlapiGetFieldValue('shipmethod');
		nlapiLogExecution('ERROR', 'ShipMethod', shipmethod);
		var filters = new Array();
		// taskType = PUTW or RPLN
		if(shipmethod!=null && shipmethod!='')
			filters.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'anyof', shipmethod));	
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_carrier_international');
		var CarrierServiceResults = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filters, columns);
		var internationalflag=null;
		if(CarrierServiceResults!=null && CarrierServiceResults!='')
		{
			internationalflag=CarrierServiceResults[0].getValue('custrecord_carrier_international');
			if(internationalflag!=null && internationalflag!='')
			{
				if(internationalflag=='T')
				{
					showInlineMessage(form, 'Confirmation', 'Order  ' + SalesOrdNo + '  Packed successfully. Do not forget to include International documents with shipment.', "");

				}
				else if (internationalflag=='F')
				{
					showInlineMessage(form, 'Confirmation', 'Order  ' + SalesOrdNo + '  Packed successfully', "");

				}
			}
		}

		//End of Dinesh COde for Getting the Order is Intrenational or Domestick
		//showInlineMessage(form, 'Confirmation', 'Order  ' + SalesOrdNo + '  Packed successfully', "");
		//nlapiLogExecution('ERROR','tempsalesorder','tempsalesorder');
		var tempsalesorder= form.addField('custpage_tempsalesorder', 'text', 'tempwaresalesorder').setDisplayType('hidden');
		tempsalesorder.setDefaultValue(foOrderNo);
		//nlapiLogExecution('ERROR','Aftertempsalesorder','Aftertempsalesorder');
		form.addButton('custpage_generatesearch','New Order','backtogeneratesearch()');


		//start of adding Print button for packlist printing
		var htmlstring="";
		//htmlstring=Htmlparameters["strVar"];
		//htmlstring=gethtmlstringbyLp(newContainerLpNo);
		nlapiLogExecution('ERROR', 'Htmlstring is:',htmlstring);
		if(htmlstring!=null && htmlstring!="")
		{
			htmlstring=htmlstring.replace('headerimage',imageurl);
		}

		var temphtmlstring= form.addField('custpage_temphtmlstring', 'longtext', 'temphtmlstring').setDisplayType('hidden');
		temphtmlstring.setDefaultValue(htmlstring);

		var tempimgurl= form.addField('custpage_tempimgurl', 'text', 'tempimgurl').setDisplayType('hidden');
		//tempimgurl.setDefaultValue(imageurl);

		var temphtmlstring= form.addField('custpage_tempnewcontlp', 'text', 'tempnewcontlp').setDisplayType('hidden');
		temphtmlstring.setDefaultValue(newContainerLpNo);

		nlapiLogExecution('ERROR', 'Befor Print packlist Button:','Befor Print packlist Button:');
		form.addButton('custpage_print','Print Packlist','PrintPacklist()');

		//showInlineHtml(form, ' ', htmlstring, "");

		//End of adding Print button for packlist printing



		nlapiLogExecution('ERROR', 'Time Stamp 21',TimeStampinSec());
		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/
		var tempwarehouse=request.getParameter('custpage_tempwarehouse');
		//nlapiLogExecution('ERROR', 'Endof SecondElse', tempwarehouse);
		var tempprintersecond= form.addField('custpage_tempwarehousesecond', 'text', 'tempwarehousesecond').setDisplayType('hidden');
		tempprintersecond.setDefaultValue(tempwarehouse);
		var tempwarehouseSecond=request.getParameter('custpage_tempwarehousesecond');
		//nlapiLogExecution('ERROR', 'Endof SecondElse tempwarehouseSecond', tempwarehouseSecond);
//		var searchfilter=new Array();
//		searchfilter.push(new nlobjSearchFilter('custrecord_container_lp_no',null,'is',newContainerLpNo));
//		searchfilter.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',null,'isnotempty'));

//		var searchcolumn=new Array();
//		searchcolumn[0]=new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');

//		var searchrec=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,searchfilter,searchcolumn);

//		var ItemFulfillmentRecId="";
//		if(searchrec!=null&&searchrec!="")
//		ItemFulfillmentRecId=searchrec[0].getValue('custrecord_ebiz_nsconfirm_ref_no');
//		nlapiLogExecution('ERROR','ItemFulfillmentRecId',ItemFulfillmentRecId);
//		var POarray = new Array();
//		if(ItemFulfillmentRecId==null||ItemFulfillmentRecId=="")
//		nlapiSetRedirectURL('RECORD', 'ItemFulfillment', null, false, POarray);
//		else
//		nlapiSetRedirectURL('RECORD', 'ItemFulfillment', ItemFulfillmentRecId, false, POarray);
		/* Up to here */ 
	}
}

//start of getting the Html string by ContainerLp
function gethtmlstringbyLp(newContainerLpNo)
{
	var searchfilter=new Array();
	searchfilter.push(new nlobjSearchFilter('custrecord_label_lp',null,'is',newContainerLpNo));
	nlapiLogExecution('ERROR', 'Packlist ContainerLp', newContainerLpNo);

	var searchcolumn=new Array();
	searchcolumn[0]=new nlobjSearchColumn('custrecord_labeldata');

	var Labelprintingsearchrec=nlapiSearchRecord('customrecord_labelprinting',null,searchfilter,searchcolumn);
	nlapiLogExecution('ERROR', 'Labelprintingsearchrec', Labelprintingsearchrec);
	var htmlstring="";
	if(Labelprintingsearchrec!=null && Labelprintingsearchrec!="")
		htmlstring=Labelprintingsearchrec[0].getValue('custrecord_labeldata');
	return htmlstring;

}
//End of getting the Html string by ContainerLp

function CreateLP(varEnteredLP,containerInternalId,varEnteredweight)
{

	//nlapiLogExecution('ERROR', 'getItem', ItemInternalId);

	var contRec = nlapiLoadRecord('customrecord_ebiznet_container', containerInternalId);
	var vTareWeight=0;
	var ContainerCube;
	if(contRec != null && contRec != '')
	{
		vTareWeight=contRec.getFieldValue('custrecord_cubecontainer');
		ContainerCube=contRec.getFieldValue('custrecord_cubecontainer');
	}


	nlapiLogExecution('ERROR', 'ContainerSizeInternalId', containerInternalId);				
	//Insert LP Record
	CreateMasterLPRecord(varEnteredLP,containerInternalId,ContainerCube,varEnteredweight);

}

function CreateMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,EnteredWeight)
{
	//case # 20126956,start Getting Rolebased location and max lpno.
	var ResultText='';
	var vRoleLocation=getRoledBasedLocation();
	//ResultText=GetMaxLPNo('1','5',vRoleLocation);
	//case# 201413537 starts
	ResultText=GenerateLable('0',vRoleLocation);
	//case# 201413537 end
	nlapiLogExecution('ERROR', 'ResultText',ResultText);
	//End
	var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
	MastLP.setFieldValue('name', vContLpNo);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
	if(containerInternalId!="" && containerInternalId!=null)
	{
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
	}	

	if(EnteredWeight!="" && EnteredWeight!=null)
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(EnteredWeight).toFixed(5));
	if(ContainerCube!="" && ContainerCube!=null)
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(5));
	if(ResultText!="" && ResultText!=null)
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);

	var retktoval = nlapiSubmitRecord(MastLP);
	nlapiLogExecution('ERROR', 'Master LP Insertion','Success');

}

/**
 * This method will update the status to pack complete in open task for all containers
 * @param recordId
 * @param statusflag
 */
function updateStatusInOpenTaskNew(recordId, statusflag,TotWeight,ContLP,SizeId,qty,currentUserID,TotCube,vDeviceUpdFlag,Container,itemno,
		ebizordno,actqty,ffordno,lineNo,blnUpdateSTGMTask){
	nlapiLogExecution('ERROR', 'In to updateStatusInOpenTask function');
	nlapiLogExecution('ERROR', 'Time Stamp at the start of updateStatusInOpenTask',TimeStampinSec());

	var fieldNames = new Array(); 
	fieldNames.push('custrecord_wms_status_flag');  
	fieldNames.push('custrecord_pack_confirmed_date');
	if(TotWeight != null && TotWeight != '')
		fieldNames.push('custrecord_total_weight');
	fieldNames.push('custrecord_container_lp_no');
	fieldNames.push('custrecord_act_qty');
	fieldNames.push('custrecord_expe_qty');	
	fieldNames.push('custrecord_upd_ebiz_user_no');
	fieldNames.push('custrecord_device_upload_flag');
	if(TotCube != null && TotCube != '')
		fieldNames.push('custrecord_totalcube');
	if(SizeId != null && SizeId !='')
		fieldNames.push('custrecord_container');

	var newValues = new Array(); 
	newValues.push(statusflag);
	newValues.push(DateStamp());
	if(TotWeight != null && TotWeight != '')
		newValues.push(parseFloat(TotWeight).toFixed(5));
	newValues.push(ContLP);
	newValues.push(parseFloat(qty).toFixed(5));
	newValues.push(parseFloat(qty).toFixed(5));
	newValues.push(parseFloat(currentUserID));
	newValues.push(vDeviceUpdFlag);
	if(TotCube != null && TotCube != '')
		newValues.push(parseFloat(TotCube).toFixed(5));
	if(SizeId != null && SizeId !='')
		newValues.push(SizeId);

	nlapiSubmitField('customrecord_ebiznet_trn_opentask', recordId, fieldNames, newValues);

	// case# 201411241

	var str = 'Container. = ' + Container + '<br>';
	str = str + 'itemno. = ' + itemno + '<br>';	
	str = str + 'ffordno. = ' + ffordno + '<br>';	
	str = str + 'lineNo. = ' + lineNo + '<br>';
	str = str + 'ContLP. = ' + ContLP + '<br>';	
	str = str + 'blnUpdateSTGMTask. = ' + blnUpdateSTGMTask + '<br>';	

	nlapiLogExecution('ERROR', 'updateStatusInOpenTaskNew Param to updateserialentry', str);
	var filters = new Array();
	//filters.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', contlp));
	filters.push(new nlobjSearchFilter('custrecord_serialcontlpno', null, 'is', Container));
	filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itemno));
	//filters.push(new nlobjSearchFilter('custrecord_serialebizsono', null, 'is', ordid));
	filters.push(new nlobjSearchFilter('custrecord_serialsono', null, 'is', ffordno));
	filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null, 'is', lineNo));
	filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	var serialEntryResults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
	if(serialEntryResults!=null && serialEntryResults!='' && serialEntryResults!='null')
	{

		for(var k1=0;k1<serialEntryResults.length;k1++)
		{

			var SerialEntryRec=nlapiLoadRecord('customrecord_ebiznetserialentry',serialEntryResults[k1].getId());
			if(SerialEntryRec!=null && SerialEntryRec!='' && SerialEntryRec!='null')
			{

				SerialEntryRec.setFieldValue('custrecord_serialcontlpno',ContLP);//updating with new contlp
				nlapiSubmitRecord(SerialEntryRec);
			}

		}
	}

	nlapiLogExecution('ERROR', 'itemno',itemno);
	if(Container !=null && Container!='' && itemno !=null && itemno!='')
	{

		nlapiLogExecution('ERROR', 'actqty at invt',actqty);
		nlapiLogExecution('ERROR', 'qty at invt',qty);

		var filtersinvt = new Array();
		var columnsinvt = new Array();
		columnsinvt[0]=new nlobjSearchColumn('custrecord_ebiz_qoh');
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', Container));
		filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18]));
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemno));
		if(actqty!=null&&actqty!="")
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'equalto', actqty));


		var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

		if(searchresults!=null && searchresults!='')
		{
			//for (var i = 0; searchresults != null && i < searchresults.length; i++) 
			//{
			var invtqoh=searchresults[0].getValue('custrecord_ebiz_qoh');
			nlapiLogExecution('ERROR', 'qty',qty);
			nlapiLogExecution('ERROR', 'invtqoh',invtqoh);
			if(parseFloat(qty)!=parseFloat(invtqoh))
			{
				var invtremqty=parseFloat(invtqoh)-parseFloat(qty);
				nlapiLogExecution('ERROR', 'invtremqty',invtremqty);
				var createremstagerecord =  nlapiCopyRecord('customrecord_ebiznet_createinv',searchresults[0].getId());		    	
				createremstagerecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(invtremqty));
				createremstagerecord.setFieldValue('custrecord_ebiz_avl_qty', parseFloat(invtremqty));
				var recidnew = nlapiSubmitRecord(createremstagerecord, false, true);
			}
			var fieldinvt = new Array(); 
			fieldinvt.push('custrecord_ebiz_inv_lp');  
			fieldinvt.push('custrecord_ebiz_qoh');  
			fieldinvt.push('custrecord_ebiz_avl_qty');  

			var newValuesinvt = new Array(); 
			newValuesinvt.push(ContLP);
			newValuesinvt.push(qty);
			newValuesinvt.push(qty);

			nlapiSubmitField('customrecord_ebiznet_createinv', searchresults[0].getId(), fieldinvt, newValuesinvt);

			//}

		}
	}
	if(ebizordno!=null && ebizordno !='' && blnUpdateSTGMTask==true)
	{
		var packfilters = new Array();
		packfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',ebizordno));
		packfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['13','14']));//13 - STGM, 14 - PACK

		var packcolumns= new Array();
		packcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
		packcolumns[1] = new nlobjSearchColumn('custrecord_tasktype');

		var packtasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,packfilters ,packcolumns);
		if(packtasksearchresults !=null && packtasksearchresults !='')
		{
			nlapiLogExecution('ERROR', 'packtasksearchresults length2',packtasksearchresults.length);

			for (var k = 0; k < packtasksearchresults.length; k++) 
			{

				var vTaskType = packtasksearchresults[k].getValue('custrecord_tasktype');
				var vCartonNo = packtasksearchresults[k].getValue('custrecord_container_lp_no');
				var vTaskId = packtasksearchresults[k].getId();

				if(vTaskType=='13')
				{
					var fieldStgm= new Array(); 
					fieldStgm.push('custrecord_container_lp_no');  

					var newValuesStgm = new Array(); 
					newValuesStgm.push(ContLP);

					nlapiSubmitField('customrecord_ebiznet_trn_opentask', vTaskId, fieldStgm, newValuesStgm);
					nlapiLogExecution('ERROR', 'STGM Updated');
				}

				if(vTaskType=='14' && vCartonNo==Container)
				{
					var fieldPACK= new Array(); 
					fieldPACK.push('custrecord_device_upload_flag');  

					var newValuesPACK = new Array(); 
					newValuesPACK.push('T');

					nlapiSubmitField('customrecord_ebiznet_trn_opentask', vTaskId, fieldPACK, newValuesPACK);
					nlapiLogExecution('ERROR', 'PACK Updated');
				}
			}
		}
	}	




//	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);
//	transaction.setFieldValue('custrecord_wms_status_flag', statusflag);
//	transaction.setFieldValue('custrecord_pack_confirmed_date', DateStamp());
//	nlapiSubmitRecord(transaction, false, true);
//	nlapiLogExecution('ERROR', 'Pack Complete - Status Flag Updated in open task',recordId);

	nlapiLogExecution('ERROR', 'Out of updateStatusInOpenTask function');
}

function getParentOrder(orderno){
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', orderno));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ns_ord');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	return searchresults;
}

function getCarrierType(ebizOrd)
{
	var vOrdAutoPackFlag="";
	var vStgAutoPackFlag="";
	var vStgCarrierType="";
	var vCarrierType="";
	var vStgCarrierName="";
	var vCarrier="";
	var vordtype='';
	var whlocation='';
	var vOrderAutoPackFlag = getAutoPackFlagforOrdType(ebizOrd);
	if(vOrderAutoPackFlag!=null && vOrderAutoPackFlag.length>0){
		vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
		vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
		vCarrier = vOrderAutoPackFlag[0].getText('custbody_salesorder_carrier');
		vordtype = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
		whlocation = vOrderAutoPackFlag[0].getValue('location');
		nlapiLogExecution('ERROR', 'Order Type',vordtype);
		nlapiLogExecution('ERROR', 'WH Location',whlocation);
		nlapiLogExecution('ERROR', 'AutoPackFlag @ Order Type',vOrdAutoPackFlag);
		nlapiLogExecution('ERROR', 'Carrier Type @ Order Type',vCarrierType);
		nlapiLogExecution('ERROR', 'Carrier @ Order Type',vCarrier);
	}

	if(vOrdAutoPackFlag!='T' || (vCarrier=="" ||vCarrier==null)){
		var vStageAutoPackFlag = getAutoPackFlagforStage(whlocation,vordtype);
		if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
			vStgCarrierType = vStageAutoPackFlag[0][0];
			vStgAutoPackFlag = vStageAutoPackFlag[0][1];  
			vStgCarrierName = vStageAutoPackFlag[0][2];
			nlapiLogExecution('ERROR', 'AutoPackFlag(Stage)',vStgAutoPackFlag);
			nlapiLogExecution('ERROR', 'Carrier Type(Stage)',vStgCarrierType);
			nlapiLogExecution('ERROR', 'Carrier(Stage)',vStgCarrierName);
		}
	}

	if((vCarrier=="" || vCarrier==null) && (vStgCarrierName!="" && vStgCarrierName!=null))
		vCarrier=vStgCarrierName;

	if((vCarrierType=="" || vCarrierType==null) && (vStgCarrierType!="" && vStgCarrierType!=null))
		vCarrierType=vStgCarrierType;

	nlapiLogExecution('ERROR', 'vCarrierType', vCarrierType);
	return vCarrierType;
}
function getAutoPackFlagforOrdType(vebizOrdNo){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforOrdType eBizOrdNo',vebizOrdNo);
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
	columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforOrdType', searchresults);
	return searchresults;

}
function getAutoPackFlagforStage(){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforStage');
	var vStgRule = new Array();
	vStgRule = getStageRule('', '', '', '', 'OUB');
	nlapiLogExecution('ERROR', 'Stage Rule', vStgRule);

	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforStage');
	return vStgRule;
}
function updateStatusInLPMasterNew(ContainerLPNo,Totweightupdate,expectedCube,status)
{
	nlapiLogExecution('ERROR', 'Updating in LP Master');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
	var filtersmlp = new Array();
	filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ContainerLPNo);
	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp,columns);
	if (SrchRecord != null && SrchRecord.length > 0) {

		var lpRecid= SrchRecord[0].getId();
		var fields=new Array();
		fields[0]='custrecord_ebiz_lpmaster_totwght';
		fields[1]='custrecord_ebiz_lpmaster_totcube';
		fields[2]='custrecord_ebiz_lpmaster_wmsstatusflag';
		var lpweight=SrchRecord[0].getValue('custrecord_ebiz_lpmaster_totwght');
		var lpcube=SrchRecord[0].getValue('custrecord_ebiz_lpmaster_totcube');
		var Values=new Array();
		Values[0]=parseFloat(Totweightupdate);
		nlapiLogExecution('ERROR', 'Totweightupdate',Totweightupdate);
		Values[1]=parseInt(lpcube)+parseInt(expectedCube);
		Values[2]=status;

		var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_master_lp',lpRecid,fields,Values);

	}	
}

function GetSOInternalId(SOText,ordertype)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',SOText);
	nlapiLogExecution('ERROR','Into GetSOInternalId (ordertype)',ordertype);
	var ActualSoID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));

	var columns=new Array();
	columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord(ordertype,null,filter,columns);
	/*if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}*/
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualSoID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualSoID);

	return ActualSoID;
}


function IsOpenPicksExist(vebizOrdNo)
{
	nlapiLogExecution('ERROR','Into IsOpenPicksExist (Input)',vebizOrdNo);

	var vopenpick='N';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no',null,'anyof',vebizOrdNo));
	filter.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',[3]));
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',[9]));


	var columns=new Array();
	columns.push(new nlobjSearchColumn('name'));

	var searchrec=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);
	if(searchrec!=null && searchrec!='' &&searchrec.length>0)
	{
		vopenpick='Y';
	}

	nlapiLogExecution('ERROR','Out of IsOpenPicksExist (Output)',vopenpick);

	return vopenpick;
}


function fnValidateEnteredContLP(EnteredContLP,SalesOrderInternalId,vsite)
{
	nlapiLogExecution('Error', 'Into fnValidateEnteredContLP',EnteredContLP);	
	nlapiLogExecution('Error', 'SalesOrderInternalId',SalesOrderInternalId);	
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));

	if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  
	if(vsite!=null && vsite!='')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vsite)); 
	//Case # 20127010� Start
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); 
	//Case # 20127010� End
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');



	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(salesOrderList==null || salesOrderList=='')
	{
		nlapiLogExecution('Error', 'test1','test1');
		var filters1=new Array();
		filters1.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_contlp_no', null, 'is', EnteredContLP));
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
			filters1.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  
		if(vsite!=null && vsite!='')
			filters1.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location', null, 'anyof', vsite));  
		//Case # 20127010� Start
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', ['3'])); 
		//Case # 20127010� End

		var columns1 = new Array();
		columns1[0] = new nlobjSearchColumn('name');



		var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters1, columns1);
	}
	nlapiLogExecution('Error', 'salesOrderList',salesOrderList);
	return salesOrderList;
}
function fnValidateEnteredContLPLevel2(EnteredContLP,SalesOrderInternalId,vsite)
{
	nlapiLogExecution('DEBUG', 'Into fnValidateEnteredContLP',EnteredContLP);	
	nlapiLogExecution('DEBUG', 'SalesOrderInternalId',SalesOrderInternalId);	
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [14]));
	if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));  
	if(vsite!=null && vsite!='')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vsite));  
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');

	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(salesOrderList==null || salesOrderList=='')
	{
		nlapiLogExecution('DEBUG', 'test1','test1');
		var filters1=new Array();
		filters1.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_contlp_no', null, 'is', EnteredContLP));
		filters1.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [14]));
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
			filters1.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', SalesOrderInternalId));  
		if(vsite!=null && vsite!='')
			filters1.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location', null, 'anyof', vsite));  
		var columns1 = new Array();
		columns1[0] = new nlobjSearchColumn('name');

		var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters1, columns1);
	}
	nlapiLogExecution('DEBUG', 'salesOrderList',salesOrderList);
	return salesOrderList;
}

function CreatePackListCheckbyWmsStatusFlag(getCartonLPNo,getFONO,printername,wavenumber,salesorderdetails,trantype)
{
	nlapiLogExecution('ERROR', 'Time Stamp at the start of CreatePackListCheckbyWmsStatusFlag',TimeStampinSec());
	//nlapiLogExecution('ERROR', 'CreatePackList wavenumber',wavenumber);
	//nlapiLogExecution('ERROR', 'getFONO',getFONO);
	//nlapiLogExecution('ERROR', 'printername',printername);
	//nlapiLogExecution('ERROR', 'getCartonLPNo',getCartonLPNo);
	/*var printername=request.getParameter('custpage_printername');
	nlapiLogExecution('ERROR','IntoPrinterName',printername);*/

	try
	{
		var openTaskFilters1 = new Array();
		var openTaskColumns = new Array();
		openTaskColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		openTaskColumns[1] = new nlobjSearchColumn('name');//
		openTaskColumns[2] = new nlobjSearchColumn('custrecord_wms_location');
		openTaskColumns[3] = new nlobjSearchColumn('custrecord_wms_status_flag');

//		if(getCartonLPNo!=null && getCartonLPNo!='')
//		{

//		openTaskFilters1.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getCartonLPNo));
//		}
		if(getFONO!=null && getFONO!='')
		{
			openTaskFilters1.push( new nlobjSearchFilter('name', null, 'is', getFONO));
		}
		if((wavenumber!=null)&&(wavenumber!=''))
		{
			openTaskFilters1.push( new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',wavenumber)); 
		}
		openTaskFilters1.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK and PACK
		openTaskFilters1.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'noneof', [26,30,29]));
		openTaskFilters1.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0')); 
		openTaskFilters1.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0.0'));
		var openTaskSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openTaskFilters1, openTaskColumns);
		var HtmlparametersatPacking = new Array();	
		if(openTaskSearchResults1!=null && openTaskSearchResults1!='' && openTaskSearchResults1.length>0)   
		{
			nlapiLogExecution('ERROR', 'openSearchResults1', openTaskSearchResults1.length);

			var vPickConfCount=0;
			var vPackCompCount=0;
			var vebizOrdNo=openTaskSearchResults1[0].getValue('custrecord_ebiz_order_no');
			var fulfillmentnumber=openTaskSearchResults1[0].getValue('name');
			var whlocation=openTaskSearchResults1[0].getValue('custrecord_wms_location');

			for(var s=0;s<openTaskSearchResults1.length;s++)
			{
				var vebizOrdStatus=openTaskSearchResults1[s].getValue('custrecord_wms_status_flag');

				if(vebizOrdStatus!='28')
					vPickConfCount=parseInt(vPickConfCount)+1;
				else if(vebizOrdStatus=='28')
					vPackCompCount=parseInt(vPackCompCount)+1;

			}
			//nlapiLogExecution('ERROR', 'vebizOrdNo', vebizOrdNo);
			//nlapiLogExecution('ERROR', 'fulfillmentnumber', fulfillmentnumber);
			//nlapiLogExecution('ERROR', 'whlocation', whlocation);
			if(parseInt(vPackCompCount)==0 && parseInt(vPickConfCount)==0 )
			{
				//POarray["custparam_error"] =  "No pending picks with pick confirmed status.";
				nlapiLogExecution('ERROR', 'There is no Pick confirm and pack complete status tasks');
				//response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}
			else if(parseInt(vPackCompCount)>0 && parseInt(vPickConfCount)<=0 )
			{
				nlapiLogExecution('ERROR', 'All are Packcomplete');

			}		

			else 
			{
				nlapiLogExecution('ERROR', 'All are in  PickTasks');

			}

			nlapiLogExecution('ERROR', 'printername',printername);
			if((printername==null)||printername=='')
			{
				printername="";
			}

			nlapiLogExecution('ERROR', 'vebizOrdNo', vebizOrdNo);


			var location=null;
			var locationId=whlocation;
			var locationRecord=null;
			var location=whlocation;
			if(salesorderdetails!=null && salesorderdetails!='')
			{


				if(location!=null && location!='')
				{
					locationRecord = nlapiLoadRecord('location', location);
				}
			}
			var ismultilineship=salesorderdetails.getFieldValue('ismultishipto');
			nlapiLogExecution('ERROR', 'ismultilineship ',ismultilineship);
			var customerentityrecord;
			if(ismultilineship=='T')
			{
				var entity=salesorderdetails.getFieldValue('entity');
				if(entity != "" && entity != null)
				{
					try
					{
						customerentityrecord = nlapiLoadRecord('customer', entity);
					}
					catch(e)
					{
						nlapiLogExecution('ERROR', 'Exception in Loading Customer',e);
						customerentityrecord='';
					}
				}
			}
			var  opentaskarray = new Array();
			opentaskarray["vQbWave"]= wavenumber;
			opentaskarray["VOrderNo"]=getFONO;
			var locationId=whlocation;
			opentaskarray["locationId"]=locationId;
			opentaskarray["vSalesInternalId"]=vebizOrdNo;
			//var printername="";
			opentaskarray["ismultilineship"]=ismultilineship;
			opentaskarray["printername"]=printername;
			opentaskarray["trantype"]=trantype;
			HtmlparametersatPacking=createpacklisthtml(opentaskarray,vebizOrdNo,getFONO,printername,trantype,salesorderdetails,locationRecord,wavenumber);
		}
		return HtmlparametersatPacking;

	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'CreatePackListException', exp);

	}

	nlapiLogExecution('ERROR', 'Time Stamp at the end of CreatePackListCheckbyWmsStatusFlag',TimeStampinSec());
}
function CheckCortonNo(vCarton,whloc)
{
	try
	{
		nlapiLogExecution('Debug', 'vCarton', vCarton);
		var filters = new Array();
		//filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', '1');
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', vCarton));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'anyof', [2]));
		if(whloc!=null && whloc!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', [whloc]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters,null);
		if(searchresults !=null && searchresults!='')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	catch(e)
	{
		nlapiLogExecution('Debug', 'exception in vaild carton', e);
		return false;
	}
}


function getSystemRuleValue(RuleId,loc)
{
	nlapiLogExecution('Debug', 'Into getSystemRuleValue... ', RuleId);
	nlapiLogExecution('Debug', 'loc', loc);
	var systemrulevalue='';

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', RuleId);
		filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

		/*var locarray ;
		locarray.push('@NONE@');
		//case# 20148056 starts
		if(loc != null && loc != '')
		{
			locarray.push(loc);
			filters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof',locarray));
		}*/
		//case# 20148056 ends

		if(loc!=null && loc!='')
		{
			loc.push('@NONE@');
			nlapiLogExecution('Debug', 'Into loc', loc);

			filters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof',loc));
		}
		else
		{
			nlapiLogExecution('Debug', 'Into none');

			filters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof','@NONE@'));
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			nlapiLogExecution('Debug', 'Into searchresults ', searchresults);

			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				nlapiLogExecution('Debug', 'Into searchresults new', searchresults);

				systemrulevalue = searchresults[0].getValue('custrecord_ebizrulevalue');
				return systemrulevalue;
			}
			else
				return systemrulevalue;
		}
		else
			return systemrulevalue;
	}
	catch (exp) 
	{
		nlapiLogExecution('Debug', 'Exception in GetSystemRules: ', exp);
		return systemrulevalue;
	}	
}
//case# 201413537 starts
function GenerateLable(uompackflag,location){

	nlapiLogExecution('Debug', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	//added by mahesh


	try 
	{	
		var lpMaxValue=GetMaxLPNo('1', '5',location);
		nlapiLogExecution('Debug', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('Debug', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;

		//to get company id

		duns=GetCompanyDUNSnumber('');

		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('Debug', 'uom', uom);
		nlapiLogExecution('Debug', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('Debug', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseInt(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseInt(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('Debug', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('Debug', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	return ResultText;
}
//case# 201413537 end