/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/UserEvents/ebiz_ActualenddateUpdate_UE.js,v $
 *     	   $Revision: 1.27.2.42.4.12.2.95.2.9 $
 *     	   $Date: 2015/11/14 18:30:43 $
 *     	   $Author: svanama $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_ActualenddateUpdate_UE.js,v $
 * Revision 1.27.2.42.4.12.2.95.2.9  2015/11/14 18:30:43  svanama
 * Case # 201413537 and 201413908
 *
 * Revision 1.27.2.42.4.12.2.95.2.8  2015/11/14 17:57:10  svanama
 * Case # 201413908 and Case # 201413908
 *
 * Revision 1.27.2.42.4.12.2.95.2.7  2015/11/12 16:44:09  rmalraj
 * case # 201413329,After doing packing through UI, for FIMS shipping method system is not displaying order details in external label details SO#SLS00000950
 *
 * Revision 1.27.2.42.4.12.2.95.2.6  2015/11/12 15:15:13  grao
 * 2015.2 Issue Fixes 201415574
 *
 * Revision 1.27.2.42.4.12.2.95.2.5  2015/11/05 22:29:55  rrpulicherla
 * 2015.2 issues
 *
 * Revision 1.27.2.42.4.12.2.95.2.4  2015/10/26 14:39:52  grao
 *  Briggs Sb issue fixes 201414142
 *
 * Revision 1.27.2.42.4.12.2.95.2.3  2015/10/20 10:40:59  grao
 * Briggs issue fixes 201414142
 *
 * Revision 1.27.2.42.4.12.2.95.2.2  2015/09/23 14:57:33  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.27.2.42.4.12.2.95.2.1  2015/09/22 23:31:57  snimmakayala
 * 201414495
 *
 * Revision 1.27.2.42.4.12.2.95  2015/08/11 09:33:14  schepuri
 * case# 201413921
 *
 * Revision 1.27.2.42.4.12.2.94  2015/06/16 14:43:07  rmalraj
 * 201413139
 * FIMS (FedEx International Mail Service) label generation functionality , when carrier id is FIMS , FIMS label functionality is invoked for KRM
 *
 * Revision 1.27.2.42.4.12.2.93  2015/05/13 13:20:33  schepuri
 * case# 201412741
 * For Dynamic Packslip printing
 *
 * Revision 1.27.2.42.4.12.2.92  2015/04/29 13:52:55  skreddy
 * Case# 20147994
 * Stnadard bundle issue fix
 *
 * Revision 1.27.2.42.4.12.2.91  2015/04/27 13:28:30  schepuri
 * case# 201412501
 *
 * Revision 1.27.2.42.4.12.2.90  2015/04/22 15:29:19  skreddy
 * Case# 201412437
 * standard bundle issue fix
 *
 * Revision 1.27.2.42.4.12.2.89  2015/03/27 14:54:34  snimmakayala
 * Case#: 201412007
 * Itemfulfillment fixes for Kit/package Items
 *
 * Revision 1.27.2.42.4.12.2.88  2015/03/27 14:39:09  snimmakayala
 * Case#: 201412007
 * Itemfulfillment fixes for Kit/package Items
 *
 * Revision 1.27.2.42.4.12.2.87  2015/03/23 15:01:44  snimmakayala
 * no message
 *
 * Revision 1.27.2.42.4.12.2.86  2015/02/03 14:48:45  rrpulicherla
 * Case#201411317
 *
 * Revision 1.27.2.42.4.12.2.85  2015/01/30 20:07:46  gkalla
 * Case# 201411486
 * IF posting issue parseFloat
 *
 * Revision 1.27.2.42.4.12.2.84  2015/01/22 07:33:28  skreddy
 * Case# 201411324
 * CT Prod issue fix
 *
 * Revision 1.27.2.42.4.12.2.83  2014/12/26 13:57:07  schepuri
 * issue# 201410740,201410742
 *
 * Revision 1.27.2.42.4.12.2.82  2014/12/24 07:04:56  schepuri
 * issue# 201411179
 *
 * Revision 1.27.2.42.4.12.2.81  2014/12/15 06:56:27  schepuri
 * 201411179
 *
 * Revision 1.27.2.42.4.12.2.80  2014/09/30 15:08:48  sponnaganti
 * case# 20147994
 * stnd bundle issue fix
 *
 * Revision 1.27.2.42.4.12.2.79  2014/08/12 14:45:04  snimmakayala
 * no message
 *
 * Revision 1.27.2.42.4.12.2.78  2014/08/08 14:53:39  snimmakayala
 * Case: 20148073
 *
 * Revision 1.27.2.42.4.12.2.77  2014/08/01 15:14:23  grao
 * Case#: 20148146   Standard bundel  issue fixes
 *
 * Revision 1.27.2.42.4.12.2.76  2014/07/24 12:53:46  snimmakayala
 * no message
 *
 * Revision 1.27.2.42.4.12.2.75  2014/07/16 18:00:54  snimmakayala
 * case# 20148932 20148978 20148804 20148367 20148146
 * Stnd Bundle issue fix
 *
 * Revision 1.27.2.42.4.12.2.74  2014/07/16 17:59:16  snimmakayala
 * case# 20148932 20148978 20148804 20148367 20148146
 * Stnd Bundle issue fix
 *
 * Revision 1.27.2.42.4.12.2.73  2014/07/16 15:22:52  sponnaganti
 * case# 20148932 20148978 20148804 20148367 20148146
 * Stnd Bundle issue fix
 *
 * Revision 1.27.2.42.4.12.2.72  2014/07/09 10:31:20  snimmakayala
 * Case: 20148073
 * Carousal integration
 *
 * Revision 1.27.2.42.4.12.2.71  2014/06/27 15:50:46  skreddy
 * case # 20148553
 * Nucourse prod  issue fix
 *
 * Revision 1.27.2.42.4.12.2.70  2014/06/23 14:41:34  snimmakayala
 * 20148940
 *
 * Revision 1.27.2.42.4.12.2.69  2014/06/19 15:49:31  sponnaganti
 * case# 20148736
 * Stnd Bundle issue fixes
 *
 * Revision 1.27.2.42.4.12.2.68  2014/06/10 13:14:52  rmukkera
 * Case # 20148832
 * VRA functionality added
 *
 * Revision 1.27.2.42.4.12.2.67  2014/05/29 15:32:44  sponnaganti
 * case# 20148056
 * Stnd Bundle Issue fix
 *
 * Revision 1.27.2.42.4.12.2.66  2014/05/27 07:06:24  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201219774
 *
 * Revision 1.27.2.42.4.12.2.65  2014/04/21 14:00:00  skreddy
 * case # 20148009
 * Dealmed SB  issue fix
 *
 * Revision 1.27.2.42.4.12.2.64  2014/04/07 06:54:22  nneelam
 * case#  20140052
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.27.2.42.4.12.2.63  2014/03/27 16:14:24  skavuri
 * Case # 20127174 issue fixed
 *
 * Revision 1.27.2.42.4.12.2.62  2014/03/14 14:54:07  rmukkera
 * Case # 20127191
 *
 * Revision 1.27.2.42.4.12.2.61  2014/03/07 15:51:35  skreddy
 * case 20127609 &20127594
 * SportsHQ and demo account isue fixs
 *
 * Revision 1.27.2.42.4.12.2.60  2014/03/04 13:00:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case# 20127476
 *
 * Revision 1.27.2.42.4.12.2.59  2014/03/04 08:57:57  rmukkera
 * Case # 20127191
 *
 * Revision 1.27.2.42.4.12.2.58  2014/02/25 15:37:46  nneelam
 * case#  20127191
 * Standard Bundle Issue Fix.
 *
 * Revision 1.27.2.42.4.12.2.57  2014/02/21 14:43:14  rmukkera
 * Case# 20127191
 *
 * Revision 1.27.2.42.4.12.2.56  2014/02/19 12:55:13  snimmakayala
 * Case #: 20125419
 * Auto Ship and Trailer building process for EDI Orders.
 *
 * Revision 1.27.2.42.4.12.2.55  2014/02/18 15:32:03  sponnaganti
 * case# 20127164
 * (FULFILLMENTATORDERLEVEL=='F' is changed to FULFILLMENTATORDERLEVEL!='Y')
 *
 * Revision 1.27.2.42.4.12.2.54  2014/02/04 15:11:44  grao
 * Case# 20126922 related issue fixes in TSG issue fixes
 *
 * Revision 1.27.2.42.4.12.2.53  2014/01/31 06:52:00  nneelam
 * case#  20126922
 * std bundle issue fix
 *
 * Revision 1.27.2.42.4.12.2.52  2014/01/24 14:51:38  rmukkera
 * Case # 20126922
 *
 * Revision 1.27.2.42.4.12.2.51  2014/01/22 09:18:40  snimmakayala
 * Case# : 20126828
 * IF failures for NuCourse.
 *
 * Revision 1.27.2.42.4.12.2.50  2014/01/17 22:57:06  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20125911
 *
 * Revision 1.27.2.42.4.12.2.49  2014/01/07 15:35:34  rmukkera
 * Case # 20126486
 *
 * Revision 1.27.2.42.4.12.2.48  2013/12/26 15:20:38  rmukkera
 * Case # 20126491,20126405ï¿½
 *
 * Revision 1.27.2.42.4.12.2.47  2013/12/23 16:38:05  gkalla
 * case#20126404
 * Standard bundle issue
 *
 * Revision 1.27.2.42.4.12.2.46  2013/12/20 07:43:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20125911
 *
 * Revision 1.27.2.42.4.12.2.45  2013/12/19 16:21:37  gkalla
 * case#20126401
 * Standard bundle issue
 *
 * Revision 1.27.2.42.4.12.2.44  2013/12/02 13:56:26  gkalla
 * case#20125683
 * Ryonet item fulfillment failure issue. blank space in contlp number
 *
 * Revision 1.27.2.42.4.12.2.43  2013/11/22 08:41:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201217424
 *
 * Revision 1.27.2.42.4.12.2.42  2013/11/18 13:34:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Ryonet issue fix
 *
 * Revision 1.27.2.42.4.12.2.41  2013/10/16 15:40:23  rmukkera
 * Case# 20124622
 *
 * Revision 1.27.2.42.4.12.2.40  2013/10/16 14:31:40  schepuri
 * shipmanifest record not created
 *
 * Revision 1.27.2.42.4.12.2.39  2013/10/03 09:08:08  schepuri
 * item fulfillment is not created
 *
 * Revision 1.27.2.42.4.12.2.38  2013/09/30 15:55:39  rmukkera
 * Case# 20124696
 *
 * Revision 1.27.2.42.4.12.2.37  2013/09/05 16:55:32  gkalla
 * Case# 20124303
 * Itemfulfillment not posting issue for Afosa
 *
 * Revision 1.27.2.42.4.12.2.36  2013/09/02 15:39:01  grao
 * Issue fixes related to the 20124238
 *
 * Revision 1.27.2.42.4.12.2.35  2013/08/23 15:33:55  grao
 * SB Issue Fixes  20124014
 *
 * Revision 1.27.2.42.4.12.2.34  2013/08/22 07:16:55  snimmakayala
 * Case# 20123982
 * NLS - UAT ISSUES(IF FAILURES FOR KITS)
 *
 * Revision 1.27.2.42.4.12.2.33  2013/08/07 17:36:06  grao
 * Case#:201214994, Getting ebizOrderno. related issues fixes
 *
 * Revision 1.27.2.42.4.12.2.32  2013/08/06 03:48:12  snimmakayala
 * Case# 201214549
 * Performance tuning
 *
 * Revision 1.27.2.42.4.12.2.31  2013/08/05 19:32:07  spendyala
 * CASE201112/CR201113/LOG201121
 * Shipmanifest record not created issue resolved.
 *
 * Revision 1.27.2.42.4.12.2.30  2013/08/05 16:22:10  gkalla
 * Case# 20123718
 * Item fulfillment posting issue
 *
 * Revision 1.27.2.42.4.12.2.29  2013/07/19 21:40:17  svanama
 * Case# 20123475
 * Shipmanifest record creation issue resolved
 *
 * Revision 1.27.2.42.4.12.2.28  2013/07/01 10:38:49  snimmakayala
 * Case# 20123239
 * GSUSA :: Issue Fixes
 *
 * Revision 1.27.2.42.4.12.2.27  2013/06/28 16:26:21  skreddy
 * Case# 20123225
 * Monobind SB issue fix
 *
 * Revision 1.27.2.42.4.12.2.26  2013/06/28 15:55:01  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fix related to epicuren kit/package item
 *
 * Revision 1.27.2.42.4.12.2.25  2013/06/26 16:32:31  gkalla
 * Case# 20123022
 * Standard bundle Issue Fix
 *
 * Revision 1.27.2.42.4.12.2.24  2013/06/26 15:00:35  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 201213655
 *
 * Revision 1.27.2.42.4.12.2.23  2013/06/18 14:12:21  mbpragada
 * 20120490
 *
 * Revision 1.27.2.42.4.12.2.22  2013/06/18 13:33:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * nls issue fixes
 *
 * Revision 1.27.2.42.4.12.2.21  2013/06/03 07:26:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.20  2013/05/13 15:08:06  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.27.2.42.4.12.2.19  2013/05/13 11:15:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.18  2013/05/07 15:37:53  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.27.2.42.4.12.2.17  2013/04/30 18:43:00  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.16  2013/04/29 15:41:00  spendyala
 * CASE201112/CR201113/LOG2012392
 * Storing Error log in open task field.
 *
 * Revision 1.27.2.42.4.12.2.15  2013/04/25 15:26:51  skreddy
 * CASE201112/CR201113/LOG201121
 * issue rellated to autopacking
 *
 * Revision 1.27.2.42.4.12.2.14  2013/04/24 22:16:05  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Standed bundle
 *
 * Revision 1.27.2.42.4.12.2.13  2013/04/19 15:45:01  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.27.2.42.4.12.2.12  2013/04/16 14:55:56  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.11  2013/04/10 05:37:39  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.10  2013/04/03 17:11:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Standed bundle
 *
 * Revision 1.27.2.42.4.12.2.9  2013/04/02 16:02:08  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * TSg Issue fixes
 *
 * Revision 1.27.2.42.4.12.2.8  2013/04/01 21:26:22  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.7  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.6  2013/03/22 11:45:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.5  2013/03/18 06:45:56  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.4  2013/03/15 15:00:13  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.27.2.42.4.12.2.3  2013/03/12 12:42:44  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Monobind.
 *
 * Revision 1.27.2.42.4.12.2.2  2013/03/06 09:48:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet Files merging for Standard bundle
 *
 * Revision 1.27.2.42.4.12.2.1  2013/03/02 12:36:21  gkalla
 * CASE201112/CR201113/LOG201121
 * Merged from Factory mation as part of standard bundle
 *
 * Revision 1.27.2.42.4.12  2013/02/16 12:11:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.27.2.42.4.11  2013/02/01 07:56:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Linelevelfulfillment changes
 *
 * Revision 1.27.2.42.4.10  2013/01/09 12:03:44  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * transferorder fulfillment issue
 *
 * Revision 1.27.2.42.4.9  2013/01/02 15:42:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Double Click restriction.
 * .
 *
 * Revision 1.27.2.42.4.8  2012/12/31 05:52:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Restrict Packing for Closed and Paymeny Hold Orders.
 *
 * Revision 1.27.2.42.4.7  2012/12/21 09:29:26  mbpragada
 * 20120490
 *
 * Revision 1.27.2.42.4.6  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.27.2.42.4.5  2012/12/12 15:59:42  gkalla
 * CASE201112/CR201113/LOG201121
 * Actual Bin management enhancement
 *
 * Revision 1.27.2.42.4.4  2012/12/11 12:19:05  svanama
 * CASE201112/CR201113/LOG201121
 * this code changes for monobind pick and ship labels
 *
 * Revision 1.27.2.42.4.3  2012/11/03 01:34:55  gkalla
 * CASE201112/CR201113/LOG201121
 * Item fulfillment failure issue.
 *
 * Revision 1.27.2.42.4.2  2012/11/01 14:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.27.2.42.4.1  2012/10/03 11:49:12  schepuri
 * CASE201112/CR201113/LOG201121
 * itemfulfillment confimation failing
 *
 * Revision 1.27.2.42  2012/09/14 10:31:53  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * kitto order changs
 *
 * Revision 1.27.2.41  2012/09/11 00:45:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.27.2.40  2012/09/07 03:47:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.27.2.39  2012/09/04 20:47:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Exception Handling.
 *
 * Revision 1.27.2.38  2012/08/30 01:59:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fisk Production Issue Fixes
 *
 * Revision 1.27.2.37  2012/08/27 04:04:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Auto Packlist for FISK
 * NS Case # : 20120739
 *
 * Revision 1.27.2.36  2012/08/25 20:56:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Item Fulfillment Issue for FISK.
 * Case # 20120719
 *
 * Revision 1.27.2.35  2012/08/24 18:42:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changesbackup
 *
 * Revision 1.27.2.33  2012/08/24 12:47:53  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Packing List Issue for Boombah.Packing list will be generated if all the tasks in the FO are pack completed.
 *
 * Revision 1.27.2.32  2012/08/17 13:21:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.27.2.31  2012/08/10 18:24:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.27.2.30  2012/08/08 17:23:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Rounding the totalcube value to 2 decimal when updating sales order
 *
 * Revision 1.27.2.29  2012/08/08 15:20:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Kit to order
 *
 * Revision 1.27.2.28  2012/08/07 12:50:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Issue Fix of case#20120652 to restrinct zero qty shipping
 *
 * Revision 1.27.2.27  2012/08/07 09:49:35  gkalla
 * CASE201112/CR201113/LOG201121
 * Issue Fix of case#20120652 to restrinct zero qty shipping
 *
 * Revision 1.27.2.26  2012/07/30 23:25:45  gkalla
 * CASE201112/CR201113/LOG201121
 * NSUOM issue
 *
 * Revision 1.27.2.25  2012/07/26 06:02:04  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.27.2.24  2012/07/19 12:30:12  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * issue while load record
 *
 * Revision 1.27.2.23  2012/07/17 13:03:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stg Rule determination by Carrier Type.
 *
 * Revision 1.27.2.22  2012/07/10 23:10:35  gkalla
 * CASE201112/CR201113/LOG201121
 * Alert Mails
 *
 * Revision 1.27.2.21  2012/07/04 08:07:30  gkalla
 * CASE201112/CR201113/LOG201121
 * Auto pack flag checking issue fix.
 *
 * Revision 1.27.2.20  2012/07/03 21:54:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.27.2.19  2012/07/02 12:50:41  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.27.2.18  2012/06/18 07:04:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.27.2.15  2012/05/15 05:28:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.27.2.14  2012/05/04 22:52:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.27.2.13  2012/04/27 11:44:10  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.27.2.12  2012/04/20 15:26:53  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Zone picking
 *
 * Revision 1.27.2.11  2012/04/20 00:00:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Set lotno value to null after every iteration
 *
 * Revision 1.27.2.10  2012/04/19 16:12:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking and SKIP
 *
 * Revision 1.40  2012/04/19 15:57:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking and SKIP
 *
 * Revision 1.39  2012/04/05 15:27:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multiple Route Shippings.
 *
 * Revision 1.38  2012/04/03 15:14:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.37  2012/03/09 07:16:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.36  2012/02/20 13:36:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Line level WMS Carrier
 *
 * Revision 1.35  2012/02/17 13:59:21  rmukkera
 * CASE201112/CR201113/LOG201121
 * added code for updating the sales order header column Total Weight
 *
 * Revision 1.34  2012/02/02 11:26:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stage Rule by Order Type
 *
 * Revision 1.33  2012/01/30 07:05:05  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.32  2012/01/28 00:09:03  svanama
 * CASE201112/CR201113/LOG201121
 * comment the code  generatePickLabel  and shiplabel
 *
 * Revision 1.31  2012/01/18 17:06:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned
 *
 * Revision 1.30  2012/01/18 14:48:58  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned
 *
 * Revision 1.29  2012/01/17 15:04:04  gkalla
 * CASE201112/CR201113/LOG201121
 * For Pack task creation in Pick confirmation
 *
 * Revision 1.28  2012/01/12 18:05:41  gkalla
 * CASE201112/CR201113/LOG201121
 * For Fine tuning
 *
 * Revision 1.27  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.26  2011/11/28 07:02:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Confirm changes
 *
 * Revision 1.25  2011/11/25 08:54:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default LOT
 *
 * Revision 1.23  2011/11/24 12:28:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Exception.
 *
 * Revision 1.22  2011/11/21 13:18:02  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default Lot Functionality. (Item as Lot)
 *
 * Revision 1.21  2011/11/15 16:05:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * set display flag to 'N' when updating inventory
 *
 * Revision 1.20  2011/11/04 11:03:04  svanama
 * CASE201112/CR201113/LOG201121
 * InsertExceptionLog  is added in try catch block
 *
 * Revision 1.19  2011/10/04 16:06:19  gkalla
 * CASE201112/CR201113/LOG201121
 * For Lot# entry updation
 *
 * Revision 1.18  2011/10/04 12:11:54  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/10/03 09:30:49  jrnarsupalli
 * CASE201112/CR201113/LOG201121
 * RM Comments: Corrected CMLOG number in the previous version.
 *
 * Revision 1.16  2011/09/30 07:09:10  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Label printing related changes done by Siva Kumar.V
 *
 * Revision 1.12  2011/09/26 08:44:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/09/24 08:46:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/09/23 13:21:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/09/23 07:24:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Actual End Date Filter is commented
 *
 * Revision 1.8  2011/09/22 15:42:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Logs Added
 *
 * Revision 1.7  2011/09/21 14:13:05  rmukkera
 * CASE201112/CR201113/LOG201121
 * code  modification in calling transform record
 *
 * Revision 1.6  2011/09/20 13:27:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/09/19 10:28:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.4  2011/09/16 07:28:58  rmukkera
 * CASE201112/CR201113/LOG201121
 * source code changes included both open and close tasks
 * added cvs header
 *

 **************************************************************************** */



function AfterActualEndDateUpdateUE(type){

	nlapiLogExecution('Debug', 'Time Stamp at the start of AfterActualEndDateUpdateUE',TimeStampinSec());

	var vsalesOrderId="";
	var vcontainerLp="";
	var vebizOrdNo;
	var userId="";
	var DataRecCompId;
	var DataRecSiteId;
	var DataRecEbizOrdNo;
	var DataRecOrdNo;
	var DataRecContainer;
	var DataRecPackStation;
	var DataRecEbizControlNo;
	var DataRecItem;
	var oldtasktype;
	var DataRecWaveno;

	userId = nlapiGetUser();
	try{
		var newRecord = nlapiGetNewRecord();
		vebizOrdNo= newRecord.getFieldValue('name');
		var recid=newRecord.getId();
		var taskType=newRecord.getFieldValue('custrecord_tasktype');//
		var wmsstatusflag=newRecord.getFieldValue('custrecord_wms_status_flag');
		var nsconfirmationno=newRecord.getFieldValue('custrecord_ebiz_nsconfirm_ref_no');
		var isItLastPick=newRecord.getFieldValue('custrecord_device_upload_flag');
		var itemloc=newRecord.getFieldValue('custrecord_wms_location');
		var vWMSCarrier=newRecord.getFieldValue('custrecord_ebizwmscarrier');
		vcontainerLp=newRecord.getFieldValue('custrecord_container_lp_no');		
		vsalesOrderId=newRecord.getFieldValue('custrecord_ebiz_order_no');
		var kitflag=newRecord.getFieldValue('custrecord_kit_exception_flag');
		var stagelocation=newRecord.getFieldValue('custrecord_actbeginloc');
		/***Below code is merged from Lexjet Production by Ganesh on 5th March 2013***/
		var wavenumber=newRecord.getFieldValue('custrecord_ebiz_wave_no');
		/***Upto here ***/
		var fointrid = newRecord.getFieldValue('custrecord_ebiz_cntrl_no');
		var vhostid = newRecord.getFieldValue('custrecord_hostid');
		var vInvRefNo = newRecord.getFieldValue('custrecord_invref_no');
		var vActPickQty = newRecord.getFieldValue('custrecord_act_qty');
		var vExpPickQty = newRecord.getFieldValue('custrecord_expe_qty');
		var vProcessFlag = newRecord.getFieldValue('custrecord_ebiz_processflag');
		var vBeginLoc = newRecord.getFieldValue('custrecord_actbeginloc');
		var vEndLoc = newRecord.getFieldValue('custrecord_actendloc');
		var vshipasisflag = newRecord.getFieldValue('custrecord_ebiz_shipasis');
		var solineno = newRecord.getFieldValue('custrecord_line_no');
		var BulkPickFlag = newRecord.getFieldValue('custrecord_bulk_pick_flag');
		var masterlpno = newRecord.getFieldValue('custrecord_mast_ebizlp_no');
		var vwmsloc = newRecord.getFieldValue('custrecord_wms_location');
		var vsku = newRecord.getFieldValue('custrecord_sku');
		var vskuname = newRecord.getFieldText('custrecord_sku');
		var vuomlevel = newRecord.getFieldValue('custrecord_uom_level');
		var containerSize=newRecord.getFieldText('custrecord_container');

		var str = 'type. = ' + type + '<br>';
		str = str + 'recid. = ' + recid + '<br>';
		str = str + 'taskType. = ' + taskType + '<br>';	
		str = str + 'vsalesOrderId. = ' + vsalesOrderId + '<br>';	
		str = str + 'vebizOrdNo. = ' + vebizOrdNo + '<br>';	
		str = str + 'nsconfirmationno. = ' + nsconfirmationno + '<br>';
		str = str + 'wmsstatusflag. = ' + wmsstatusflag + '<br>';
		str = str + 'isItLastPick. = ' + isItLastPick + '<br>';
		str = str + 'userId. = ' + userId + '<br>';
		str = str + 'vhostid. = ' + vhostid + '<br>';
		str = str + 'vInvRefNo. = ' + vInvRefNo + '<br>';
		str = str + 'vActPickQty. = ' + vActPickQty + '<br>';
		str = str + 'vExpPickQty. = ' + vExpPickQty + '<br>';
		str = str + 'vProcessFlag. = ' + vProcessFlag + '<br>';
		str = str + 'vBeginLoc. = ' + vBeginLoc + '<br>';
		str = str + 'vEndLoc. = ' + vEndLoc + '<br>';
		str = str + 'vshipasisflag. = ' + vshipasisflag + '<br>';
		str = str + 'solineno. = ' + solineno + '<br>';
		str = str + 'containerSize. = ' + containerSize + '<br>';

		nlapiLogExecution('Debug', 'Log in iteration1', str);

		//Itemfulfillment posting at what task (PICK or PACK)
		//var fulfillmenttask = getSystemRuleValue('IF002');

		if(type =='xedit' && wmsstatusflag!=2)
		{
			var opentaskrec= nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
			if(opentaskrec!=null && opentaskrec!='')
			{
				vebizOrdNo=opentaskrec.getFieldValue('name');
				taskType=opentaskrec.getFieldValue('custrecord_tasktype');
				wmsstatusflag=opentaskrec.getFieldValue('custrecord_wms_status_flag');
				nsconfirmationno=opentaskrec.getFieldValue('custrecord_ebiz_nsconfirm_ref_no');
				isItLastPick=opentaskrec.getFieldValue('custrecord_device_upload_flag');
				itemloc=opentaskrec.getFieldValue('custrecord_wms_location');
				vWMSCarrier=opentaskrec.getFieldValue('custrecord_ebizwmscarrier');
				vcontainerLp=opentaskrec.getFieldValue('custrecord_container_lp_no');
				vsalesOrderId=opentaskrec.getFieldValue('custrecord_ebiz_order_no');
				wavenumber=opentaskrec.getFieldValue('custrecord_ebiz_wave_no');
				fointrid = opentaskrec.getFieldValue('custrecord_ebiz_cntrl_no');
				vhostid = opentaskrec.getFieldValue('custrecord_hostid');
				vInvRefNo = opentaskrec.getFieldValue('custrecord_invref_no');
				vActPickQty = opentaskrec.getFieldValue('custrecord_act_qty');
				vExpPickQty = opentaskrec.getFieldValue('custrecord_expe_qty');
				vProcessFlag = opentaskrec.getFieldValue('custrecord_ebiz_processflag');
				vBeginLoc = opentaskrec.getFieldValue('custrecord_actbeginloc');
				vEndLoc = opentaskrec.getFieldValue('custrecord_actendloc');
				vshipasisflag = opentaskrec.getFieldValue('custrecord_ebiz_shipasis');
				solineno = opentaskrec.getFieldValue('custrecord_line_no');
				BulkPickFlag = opentaskrec.getFieldValue('custrecord_bulk_pick_flag');
				masterlpno = opentaskrec.getFieldValue('custrecord_mast_ebizlp_no');
				vwmsloc = opentaskrec.getFieldValue('custrecord_wms_location');
				vsku = opentaskrec.getFieldValue('custrecord_sku');
				vuomlevel = opentaskrec.getFieldValue('custrecord_uom_level');
				containerSize=opentaskrec.getFieldText('custrecord_container');
			}

		}

		//case# 20148056 starts
		var fulfillmenttask = getSystemRuleValue('IF002',itemloc);
		//case# 20148056 ends

		var str = 'type. = ' + type + '<br>';
		str = str + 'taskType. = ' + taskType + '<br>';	
		str = str + 'recid. = ' + recid + '<br>';
		str = str + 'vsalesOrderId. = ' + vsalesOrderId + '<br>';	
		str = str + 'vebizOrdNo. = ' + vebizOrdNo + '<br>';	
		str = str + 'nsconfirmationno. = ' + nsconfirmationno + '<br>';
		str = str + 'wmsstatusflag. = ' + wmsstatusflag + '<br>';
		str = str + 'isItLastPick. = ' + isItLastPick + '<br>';
		str = str + 'userId. = ' + userId + '<br>';
		str = str + 'vcontainerLp. = ' + vcontainerLp + '<br>';
		str = str + 'kitflag. = ' + kitflag + '<br>';
		str = str + 'wavenumber. = ' + wavenumber + '<br>';
		str = str + 'fointrid. = ' + fointrid + '<br>';
		str = str + 'vhostid. = ' + vhostid + '<br>';
		str = str + 'vInvRefNo. = ' + vInvRefNo + '<br>';
		str = str + 'vActPickQty. = ' + vActPickQty + '<br>';
		str = str + 'vExpPickQty. = ' + vExpPickQty + '<br>';
		str = str + 'vProcessFlag. = ' + vProcessFlag + '<br>';
		str = str + 'vBeginLoc. = ' + vBeginLoc + '<br>';
		str = str + 'vEndLoc. = ' + vEndLoc + '<br>';
		str = str + 'containerSize. = ' + containerSize + '<br>';

		nlapiLogExecution('Debug', 'Log in iteration2', str);
		if(vcontainerLp!="" && vcontainerLp!=null)
		{
			var contlp=vcontainerLp.indexOf(" ");
			nlapiLogExecution('DEBUG', 'contlp', contlp);
			if(contlp!='-1')
			{
				vcontainerLp=vcontainerLp.trim();
			}

		}
		//if(fointrid!=null && fointrid!='')
		if((vebizOrdNo == null || vebizOrdNo == '') && fointrid!=null && fointrid!='')
		{
			var foFields = ['custrecord_lineord'];
			var foColumns = nlapiLookupField('customrecord_ebiznet_ordline', fointrid, foFields);
			if(foColumns!=null && foColumns!='')
			{

				vebizOrdNo   = foColumns.custrecord_lineord;
			}

			nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
		}

		if(vcontainerLp!="" && vcontainerLp!=null)
		{
			var contlp=vcontainerLp.indexOf(" ");
			nlapiLogExecution('DEBUG', 'contlp', contlp);
			if(contlp=='-1')
			{
				vcontainerLp=vcontainerLp.trim();
			}

		}

		try{
			//These methods are using for Generate LabelPrinting 
			if((type =='edit' || type =='xedit') && taskType == 3 && (vcontainerLp!=null && vcontainerLp !="") && wmsstatusflag == 9)
			{ 
				var internalid=newRecord.getId();
				if(vsalesOrderId==null || vsalesOrderId=='')
					vsalesOrderId=newRecord.getFieldValue('custrecord_ebiz_order_no');
				vcontainerLp=newRecord.getFieldValue('custrecord_container_lp_no');

				str = 'vcontainerLp. = ' + vcontainerLp + '<br>';
				str = str + 'vsalesOrderId. = ' + vsalesOrderId + '<br>';	
				str = str + 'internalid. = ' + internalid + '<br>';

				nlapiLogExecution('Debug', 'Log in generatePickLabel', str);

				if(vcontainerLp!="" && vcontainerLp!=null)
				{
					//generatePickLabel(vcontainerLp,vsalesOrderId,internalid);
					//generateShipLabel(vcontainerLp,vsalesOrderId,internalid);
				}

			}
		}
		catch(exp) {
			nlapiLogExecution('Debug', 'Exception in GenrateLabel (salesorder) : ', exp);		
		}

		//The below code is for REMSTAR integration.

		//For Picking Process
		if((type =='edit' || type =='xedit') && taskType == 3 && wmsstatusflag == 8 && (vhostid!=null && vhostid!='' && vhostid!='null' && vhostid.toUpperCase()=='REMSTAR')
				&& vProcessFlag!='T')
		{

			deleteAllocations(vInvRefNo,vActPickQty,vcontainerLp,vExpPickQty,vsalesOrderId,itemloc);
			UpdateRFFulfillOrdLine(fointrid,vActPickQty);
			nlapiSubmitField('customrecord_ebiznet_trn_opentask', recid,'custrecord_ebiz_processflag', 'T');
			if(vEndLoc==null || vEndLoc=='')
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', recid,'custrecord_actendloc', vBeginLoc);
			isItLastPick = getLastPickTask(vcontainerLp,vsalesOrderId);
		}

		//For Putaway Process
		if((type =='edit' || type =='xedit') && taskType == 2 && wmsstatusflag == 3 && (vhostid!=null && vhostid!='' && vhostid!='null' && vhostid.toUpperCase()=='REMSTAR')
				&& vProcessFlag!='T')
		{
			nlapiLogExecution('Debug', 'In to PUTW Records', 'done');
			try{

				var newPUTWRecord = nlapiGetNewRecord();
				var vebizOrdNo= newPUTWRecord.getFieldValue('name');
				var taskType=newPUTWRecord.getFieldValue('custrecord_tasktype');//
				var wmsstatusflag=newPUTWRecord.getFieldValue('custrecord_wms_status_flag');
				var nsconfirmationno=newPUTWRecord.getFieldValue('custrecord_ebiz_nsconfirm_ref_no');
				var isItLastPick=newPUTWRecord.getFieldValue('custrecord_device_upload_flag');
				var itemloc=newPUTWRecord.getFieldValue('custrecord_wms_location');
				var vWMSCarrier=newPUTWRecord.getFieldValue('custrecord_ebizwmscarrier');
				var vLineNum = newPUTWRecord.getFieldValue('custrecord_line_no');
				var itemid = newPUTWRecord.getFieldValue('custrecord_sku');
				var itemdesc = newPUTWRecord.getFieldValue('custrecord_skudesc');
				var itemstatus = newPUTWRecord.getFieldValue('custrecord_sku_status');
				var itempackcode = newPUTWRecord.getFieldValue('custrecord_packcode');
				var quantity = newPUTWRecord.getFieldValue('custrecord_expe_qty');
				var ActBeginLocationId = newPUTWRecord.getFieldValue('custrecord_actbeginloc');
				var binlocationid = newPUTWRecord.getFieldValue('custrecord_actbeginloc');
				var ActEndlocationid = newPUTWRecord.getFieldValue('custrecord_actendloc');
				var invtlp = newPUTWRecord.getFieldValue('custrecord_lpno');
				var ActQuantity = newPUTWRecord.getFieldValue('custrecord_act_qty');
				var RecordId =  newPUTWRecord.getId();
				var batchno = newPUTWRecord.getFieldValue('custrecord_batch_no');
				var whloc = newPUTWRecord.getFieldValue('custrecord_batch_no');
				var methodid = newPUTWRecord.getFieldValue('custrecord_ebizmethod_no');
				var vsalesOrderId = newPUTWRecord.getFieldValue('custrecord_ebiz_order_no');

				var trantype = nlapiLookupField('transaction', vsalesOrderId, 'recordType');

				var getScannedSerialNoinQtyexception='';
				var cntryid = '';

				var qtyexceptionFlag = false;
				var LocationExceptionFlag = false;

				//checking for Qty exception
				if(parseFloat(quantity) != parseFloat(ActQuantity))
					qtyexceptionFlag = true;

				//checking for location exception
				if(parseFloat(ActBeginLocationId) != parseFloat(ActEndlocationid))
					LocationExceptionFlag = true;

				var NewGenLP = '';

				var str = 'type. = ' + type + '<br>';
				str = str + 'recid. = ' + RecordId + '<br>';
				str = str + 'vsalesOrderId. = ' + vsalesOrderId + '<br>';	
				str = str + 'vLineNum. = ' + vLineNum + '<br>';	
				str = str + 'itemid. = ' + itemid + '<br>';
				str = str + 'quantity. = ' + quantity + '<br>';
				str = str + 'ActQuantity. = ' + ActQuantity + '<br>';
				str = str + 'trantype. = ' + trantype + '<br>';
				str = str + 'methodid. = ' + methodid + '<br>';
				str = str + 'whloc. = ' + whloc + '<br>';
				str = str + 'ActBeginLocationId. = ' + ActBeginLocationId + '<br>';
				str = str + 'ActEndlocationid. = ' + ActEndlocationid + '<br>';
				str = str + 'qtyexceptionFlag. = ' + qtyexceptionFlag + '<br>';
				str = str + 'LocationExceptionFlag. = ' + LocationExceptionFlag + '<br>';

				nlapiLogExecution('Debug', 'Log for PUTW Records', str);


				// Posting Item Receipt and Create inventory
				var vputwrecid = rf_confirmputaway(vsalesOrderId, vLineNum, itemid, itemdesc, itemstatus, itempackcode, quantity, binlocationid, 
						ActBeginLocationId, invtlp, ActEndlocationid, ActQuantity, RecordId,trantype,batchno,whloc,methodid,getScannedSerialNoinQtyexception,NewGenLP,qtyexceptionFlag,cntryid);

				nlapiLogExecution('Debug', 'vputwrecid', vputwrecid);
				nlapiLogExecution('Debug', 'vputwrecid[0]', vputwrecid[0]);
				if(vputwrecid!=null && vputwrecid!='' && vputwrecid[0]!=-1)
				{
					// Update transaction Order line details
					TrnLineUpdation(trantype, 'PUTW', vebizOrdNo,vsalesOrderId, vLineNum,itemid, null, quantity,"",itemstatus);

					//Binlocation Remaining Cube Updation
					var Remqty=0;
					if(itemid !=null && itemid !='' )
					{
						var itemDimensions = getSKUCubeAndWeight(itemid,"");
						var itemCube = itemDimensions[0];
						var getBaseUomQty= itemDimensions[1];
						Remqty=parseFloat(quantity)-parseFloat(ActQuantity);

						var str1 = 'itemCube. = ' + itemCube + '<br>';
						str1 = str1 + 'getBaseUomQty. = ' + getBaseUomQty + '<br>';
						str1 = str1 + 'Remqty. = ' + Remqty + '<br>';	
						nlapiLogExecution('DEBUG', 'Total Item Cube ', str1 );	

						if(LocationExceptionFlag == true)
						{
							//update remainingCube for new binlocation
							nlapiLogExecution('DEBUG', 'Location Exception ', 'done');
							var TotalItemCube=CubeCapacity(quantity,getBaseUomQty,itemCube);
							nlapiLogExecution('DEBUG', 'TotalItemCube ', TotalItemCube);
							nlapiLogExecution('DEBUG', 'ActEndlocationid ', ActEndlocationid);
							var binLocationRemainingCube = GeteLocCube(ActEndlocationid);

							var remainingCube = parseFloat(binLocationRemainingCube) - parseFloat(TotalItemCube);
							UpdateLocCube(ActEndlocationid, parseFloat(remainingCube));
							nlapiLogExecution('DEBUG', 'remainingCube ', remainingCube );

							//update remainingCube for Old binlocation
							var oldBinLocationId = ActBeginLocationId;
							var oldBinLocationRemainingCube =  GeteLocCube(oldBinLocationId);
							var actualRemainingCube = parseFloat(oldBinLocationRemainingCube) + parseFloat(TotalItemCube);

							nlapiLogExecution('DEBUG', 'oldBinLocationId ', oldBinLocationId );
							nlapiLogExecution('DEBUG', 'oldBinLocationRemainingCube ', oldBinLocationRemainingCube );
							nlapiLogExecution('DEBUG', 'actualRemainingCube ', actualRemainingCube );

							UpdateLocCube(oldBinLocationId, parseFloat(actualRemainingCube));

						}
						else
						{
							var TotalItemCube = parseFloat(itemCube) * parseFloat(Remqty);
							nlapiLogExecution('DEBUG', 'TotalItemCube ', TotalItemCube);

							var binLocationRemainingCube = GeteLocCube(ActBeginLocationId);
							nlapiLogExecution('DEBUG', 'binLocationRemainingCube ', binLocationRemainingCube );
							var remainingCube = parseFloat(binLocationRemainingCube) + parseFloat(TotalItemCube);
							UpdateLocCube(ActBeginLocationId, parseFloat(remainingCube));

							nlapiLogExecution('DEBUG', 'remainingCube ', remainingCube );
						}
					}

					// Delete Inbound CHKN Task
					DeleteInvtRecCreatedforCHKNTask(vsalesOrderId, invtlp);
				}

			}
			catch(exp2)
			{
				nlapiLogExecution('Debug', 'Exception in Putaway Record ', exp2);
			}


		}
		// For RPLN Process
		if((type =='edit' || type =='xedit') && taskType == 8 && wmsstatusflag == 19 && (vhostid!=null && vhostid!='' && vhostid!='null' && vhostid.toUpperCase()=='REMSTAR')
				&& vProcessFlag!='T')
		{
			try{
				nlapiLogExecution("ERROR", "In to RPLN Process" ,"sucess");

				var transaction1 = nlapiGetNewRecord();
				//	nlapiLogExecution("ERROR", "afteropentask Id" ,recid);
				//	var transaction1 = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);

				var varsitelocation = transaction1.getFieldValue('custrecord_site_id');
				//transaction1.setFieldValue('custrecord_actualendtime', TimeStamp());
				//transaction1.setFieldValue('custrecord_act_end_date', DateStamp());
				//transaction1.setFieldValue('custrecord_tasktype', 8);
				//transaction1.setFieldValue('custrecord_wms_status_flag','19');

				var varsku=transaction1.getFieldValue('custrecord_ebiz_sku_no');
				var ebizTaskno=transaction1.getFieldValue('custrecord_ebiz_task_no');
				var invtLpno=transaction1.getFieldValue('custrecord_lpno');
				var FromLpno=transaction1.getFieldValue('custrecord_from_lp_no');
				var whLocation=transaction1.getFieldValue('custrecord_wms_location');
				var binLocation=transaction1.getFieldValue('custrecord_actendloc');
				var beginLocation = transaction1.getFieldValue('custrecord_actbeginloc');
				var reportno = transaction1.getFieldValue('name');
				var itemStatus=transaction1.getFieldValue('custrecord_sku_status');
				//var itemStatus = getDefaultItemStatus();
				var accountNumber = getAccountNumber(whLocation);
				var varqty = transaction1.getFieldValue('custrecord_act_qty');
				var batchno = transaction1.getFieldValue('custrecord_batch_no');

				var ExpQty = transaction1.getFieldValue('custrecord_expe_qty');
				var InvtRef = transaction1.getFieldValue('custrecord_invref_no');
				nlapiLogExecution("ERROR", "ExpQty Id" ,ExpQty);

				var str = 'type. = ' + type + '<br>';
				str = str + 'recid. = ' + recid + '<br>';
				str = str + 'reportno. = ' + reportno + '<br>';	
				str = str + 'binLocation. = ' + binLocation + '<br>';	
				str = str + 'beginLocation. = ' + beginLocation + '<br>';
				str = str + 'itemStatus. = ' + itemStatus + '<br>';
				str = str + 'Actvarqty. = ' + varqty + '<br>';
				str = str + 'batchno. = ' + batchno + '<br>';
				str = str + 'ebizTaskno. = ' + ebizTaskno + '<br>';
				str = str + 'InvtRef. = ' + InvtRef + '<br>';
				str = str + 'ExpQty. = ' + ExpQty + '<br>';
				str = str + 'accountNumber. = ' + accountNumber + '<br>';

				nlapiLogExecution('Debug', 'Log for RPLN Records', str);

				var stageLocation=GetStageLocation(varsku, "", whLocation, "", "BOTH");

				nlapiLogExecution('Debug', 'stageLocation', stageLocation);
				if(stageLocation == -1){

					var bulkLocationInventoryResults=getRecord(varsku, beginLocation, FromLpno,itemStatus);
					//var bulkLocationInventoryResults=getRecord(varsku, beginLocation, invtLpno);
					if(bulkLocationInventoryResults != null)
						updateBulkLocation(bulkLocationInventoryResults, varsku, beginLocation, varqty, itemStatus);

					var invtresults=getRecord(varsku, binLocation,invtLpno,itemStatus);

					updateOrCreatePickfaceInventory(invtresults, varsku, varqty, invtLpno, binLocation, 
							itemStatus, accountNumber,whLocation,batchno);

				}
				else
				{
					nlapiLogExecution('Debug', 'into else part', stageLocation);
					// Update inventory for the begin location
					var bulkLocationInventoryResults=getRecord(varsku, beginLocation, FromLpno,itemStatus);
					//var bulkLocationInventoryResults=getRecord(varsku, beginLocation, invtLpno);
					if(bulkLocationInventoryResults != null)
						updateBulkLocation(bulkLocationInventoryResults, varsku, beginLocation, varqty, itemStatus);

					var invRefNo=transaction1.getFieldValue('custrecord_invref_no');                           
					//var result = nlapiSubmitRecord(transaction);

					nlapiLogExecution('ERROR','Inventory Ref No', invRefNo);
					//var invtresults=getRecord(varsku, binLocation,FromLpno);
					var invtresults=getRecord(varsku, binLocation,invtLpno,itemStatus,batchno);

					if(invRefNo != null && invRefNo != "")
					{
						//Update Inventory in pickface bin location
						updateOrCreatePickfaceInventory(invtresults, varsku, varqty, invtLpno, binLocation, 
								itemStatus, accountNumber,whLocation,batchno);
						var LocationType = nlapiLookupField('customrecord_ebiznet_location', beginLocation, ['custrecord_ebizlocationtype']);

						if (LocationType.custrecord_ebizlocationtype == '8' )
						{
							DeleteInventoryRecord(invRefNo);
						}
					}


				}
				//nlapiSubmitRecord(transaction1, true);
				if(invtresults != null && invtresults != "")
				{
					var columns = nlapiLookupField('customrecord_ebiznet_createinv', invtresults[0].getId(),['custrecord_ebiz_inv_lp']);
					if(columns.custrecord_ebiz_inv_lp != null && columns.custrecord_ebiz_inv_lp != "")
					{        						
						invtLpno = columns.custrecord_ebiz_inv_lp;	
						nlapiLogExecution('ERROR','columns.custrecord_ebiz_inv_lp', columns.custrecord_ebiz_inv_lp);
					}            		
				} 
				CreateLPrecord(invtLpno);
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', recid,'custrecord_ebiz_processflag', 'T');
				if(varsku != null && varsku !='')
				{
					var fields = ['recordType'];
					var columns = nlapiLookupField('item', varsku, fields);
					var ItemType = columns.recordType;
					nlapiLogExecution('ERROR','ItemType',ItemType);

					if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
					{
						AdjustSerialNumbers(varqty,varsku,FromLpno,invtLpno,binLocation);
					}
				}
			}
			catch(exp3)
			{
				nlapiLogExecution('Debug', 'Exception in RPLN Confirm Process', exp3);
			}

		}

		//Upto here.

		var str = 'type. = ' + type + '<br>';
		str = str + 'recid. = ' + recid + '<br>';
		str = str + 'taskType. = ' + taskType + '<br>';	
		str = str + 'wmsstatusflag. = ' + wmsstatusflag + '<br>';	
		str = str + 'isItLastPick. = ' + isItLastPick + '<br>';
		str = str + 'kitflag. = ' + kitflag + '<br>';

		nlapiLogExecution('Debug', 'Log in iteration2', str);
		// For last pick task codes is moved to here
		if ((type =='edit' || type =='xedit') && taskType == 3 && wmsstatusflag==8 && isItLastPick == 'T' && kitflag!='T')
		{
			if(vsalesOrderId==null || vsalesOrderId=='')
				vsalesOrderId=newRecord.getFieldValue('custrecord_ebiz_order_no');
			var getkitflag='F';
			try
			{
				getkitflag=getKitflaginopentask(vsalesOrderId);
			}
			catch(exp)
			{
				nlapiLogExecution('Debug', 'Exception in getKitflaginopentask ', exp);	
			}
			nlapiLogExecution('Debug', 'getkitflag ', getkitflag);	

			if(getkitflag!='T')
			{
				var sostatus='';
				var paymentstatus='';

				var sostatusdet = new Array();
				//var trantype = nlapiLookupField('transaction', vsalesOrderId, 'recordType');

				sostatusdet = isOrderClosedAndGetDets(vsalesOrderId);
				if(sostatusdet!=null && sostatusdet!='' && sostatusdet.length>0)
				{
					sostatus=sostatusdet[0].getValue('status');
					paymentstatus=sostatusdet[0].getValue('paymenteventresult');
				}

				var str = 'sostatus. = ' + sostatus + '<br>';
				str += 'paymentstatus. = ' + paymentstatus + '<br>';

				nlapiLogExecution('Debug', 'SO Status Log', str);

				if(sostatus=='closed' || paymentstatus=='HOLD')
				{
					var functionality = vebizOrdNo+" - + Packing";

					if(sostatus=='closed')
					{						
						var msg='Order is already closed in Netsuite.So Packing is not allowed in eBizNET';
						InsertExceptionLog('SalesOrder',2, functionality, msg, vebizOrdNo, vcontainerLp,'','','', userId);
					}
					if(paymentstatus=='HOLD')
					{
						var msg='A payment hold has been placed on this Sales Order.So Packing is not allowed in eBizNET';
						InsertExceptionLog('SalesOrder',2, functionality, msg, vebizOrdNo, vcontainerLp,'','','', userId);
					}
				}
				else
				{
					var trantypeso = nlapiLookupField('transaction', vsalesOrderId, 'recordType');
					if(trantypeso!='workorder')
					{
						var vpackcomplete = AutoPacking(vsalesOrderId,itemloc,sostatusdet,vebizOrdNo,containerSize,vcontainerLp,vshipasisflag,vhostid);
						nlapiLogExecution('Debug', 'vpackcomplete', vpackcomplete);
						if(vpackcomplete=='T')
						{
							wmsstatusflag=28;
							taskType=14;
						}
					}
				}
			}

//			try
//			{
//			var printername=getStageLocationPrinter(vsalesOrderId);  
//			CreatePackListCheckbyWmsStatusFlag(vsalesOrderId,vcontainerLp,vebizOrdNo,printername);
//			}
//			catch(exp)
//			{
//			nlapiLogExecution('Debug', 'Exception in Create Packlist ', exp);	
//			}
		}	


		if ((type =='edit' || type =='xedit') && taskType == 14 && wmsstatusflag==28 && (isItLastPick == 'T' || vshipasisflag=='T')  && kitflag!='T')
		{
			if(vshipasisflag=='T')
			{
				nlapiLogExecution('Debug', 'Time Stamp at the start of Ship As Is Changes',TimeStampinSec());

				var vOrdAutoPackFlag="";
				var vCarrierType="";
				var vorderType="";	
				var nsshipmethod="";
				var vcarrier='';
				var vcarrierline='';
				var vcarrierlineID='';
				var vCarrierTypeID='';

				//************Ship Manifest Related data**********************************//

				var containersearchresults = getSKUDims(vsku,vuomlevel);

				var trantype = nlapiLookupField('transaction', vsalesOrderId, 'recordType');
				var freightterms ="";
				var otherrefnum="";
				var servicelevelvalue='';
				var  searchresults;
				var vCustShipCountry;
				var sorec= nlapiLoadRecord(trantype, vsalesOrderId);
				var opentaskordersearchresult=getOpenTaskDetails(vsalesOrderId,vcontainerLp);

				var entity=sorec.getFieldValue('entity');
				var shiptocountry=sorec.getFieldValue('shipcountry');
				var shipmethod=sorec.getFieldText('shipmethod');

				var customerid
				if(entity != "" && entity != null)
				{
					var fields = ['entityid','resalenumber','shipcountry'];
					var entitycolumns = nlapiLookupField('customer',entity,fields);
					customerid = entitycolumns.entityid;
					//nlapiLogExecution('Debug', 'start of resaleno','start of resaleno');
					var resaleno=entitycolumns.resalenumber;
					//nlapiLogExecution('Debug', 'End of resaleno',resaleno);
					vCustShipCountry=entitycolumns.shipcountry;
				}

				//************Ship Manifest Related data**********************************//

				var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vsalesOrderId);
				if(vOrderAutoPackFlag!=null&& vOrderAutoPackFlag.length>0){
					vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
					vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
					vorderType = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
					nsshipmethod = vOrderAutoPackFlag[0].getValue('shipmethod');
				}

				if(vCarrierType==null || vCarrierType=="")
				{
					if(nsshipmethod!=null && nsshipmethod!='')
					{
						var filterscarr = new Array();
						var columnscarr = new Array();

						filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',nsshipmethod));
						filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

						columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
						columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
						columnscarr[2] = new nlobjSearchColumn('id');

						var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
						if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
						{
							vCarrierType = searchresultscarr[0].getText('custrecord_carrier_type');
							vcarrier = searchresultscarr[0].getId();
						}
					}
					else
					{
						var filterscarr = new Array();
						var columnscarr = new Array();
						filterscarr.push(new nlobjSearchFilter('custrecord_lineord', null, 'is',vebizOrdNo));
						filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));
						columnscarr[0] = new nlobjSearchColumn('custrecord_do_carrier');

						var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filterscarr, columnscarr);
						if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
						{					
							vcarrierline=searchresultscarr[0].getText('custrecord_do_carrier');
							vcarrierlineID=searchresultscarr[0].getValue('custrecord_do_carrier');
						}

						if(vcarrierline!=null && vcarrierline!='')
						{
							var filterscarrtype = new Array();
							var columnscarrtype = new Array();
							filterscarrtype.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'anyof',vcarrierlineID));
							filterscarrtype.push(new nlobjSearchFilter('isinactive', null, 'is','F'));
							columnscarrtype[0] = new nlobjSearchColumn('custrecord_carrier_type');

							var searchresultscarrtype = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarrtype, columnscarrtype);
							if(searchresultscarrtype!=null && searchresultscarrtype!='')
							{
								vCarrierType=searchresultscarrtype[0].getText('custrecord_carrier_type');
								vcarrier = searchresultscarrtype[0].getId();
								vCarrierTypeID = searchresultscarrtype[0].getValue('custrecord_carrier_type');
							}
						}
					}
				}

				var smparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); 
				nlapiLogExecution('Debug', 'Time Stamp after creating Ship Manifet parent',TimeStampinSec());

				var uccarr = GetMaxLPNoBulk(1, 5,vwmsloc,vactqty);
				var cartonarr = GetMaxLPNoBulk(1, 2,vwmsloc,vactqty);
				var uccprefix = uccarr[0];
				var uccmax = uccarr[1]
				var cartonprefix = cartonarr[0];
				var cartonmax = cartonarr[1];
				var duns=GetCompanyDUNSnumber('');
				var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); 
				nlapiLogExecution('Debug', 'Time Stamp after creating LP parent',TimeStampinSec());


				nlapiLogExecution('Debug', 'vCarrierType', vCarrierType);
				nlapiLogExecution('Debug', 'vactqty', vactqty);
				for ( var z = 1; z < vactqty; z++) 
				{
					var uccmax = parseInt(uccmax)+1;

					var ucclp = uccprefix+uccmax.toString();

					var cartonmax = parseInt(cartonmax)+1;

					var cartonlp = cartonprefix+cartonmax.toString();

					var uccno = GenerateUCCLabelCode('1',duns,ucclp)
					var custweight="";
					if((containersearchresults!=null)&&(containersearchresults!=""))
					{
						custweight=containersearchresults [0].getValue('custrecord_ebizweight');	
					}
					if(custweight == null || custweight == '' || parseFloat(custweight) == 0)
					{
						custweight='0.0001';
					}else
					{
						custweight=parseFloat(custweight).toFixed(4)
					}
					parent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');	 
					parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', cartonlp);
					parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', cartonlp);
					parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_masterlp', masterlpno);
					if(vwmsloc!=null && vwmsloc!='')
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_site', vwmsloc);
					parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', 2);
					parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', uccno);
					parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_wmsstatusflag', '28');
					parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_item', vsku);
					parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_controlno', vebizOrdNo);

					parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totwght', custweight);
					parent.commitLineItem('recmachcustrecord_ebiz_lp_parent');



					if(vCarrierType=='PC')
					{
						createshipmanifestbulk(trantype,sorec,opentaskordersearchresult,entitycolumns,vsalesOrderId,containersearchresults,
								cartonlp,solineno,smparent);
					}

				}
				nlapiSubmitRecord(parent); 
				nlapiSubmitRecord(smparent); 

				//AutoBuildshipunits(vebizOrdNo,masterlpno,wavenumber,vsalesOrderId);

				nlapiLogExecution('Debug', 'Time Stamp at the end of Ship As Is Changes',TimeStampinSec());
				return;
			}
		}

		if ((type =='edit' || type =='xedit' || type=='create') && taskType == 14 && wmsstatusflag==28)//&& && isItLastPick == 'T' (nsconfirmationno == null || nsconfirmationno == '' || nsconfirmationno == ' ')) 
		{ 
			var str = 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
			str = str + 'vWMSCarrier. = ' + vWMSCarrier + '<br>';
			str = str + 'vsalesOrderId. = ' + vsalesOrderId + '<br>';
			str = str + 'vcontainerLp. = ' + vcontainerLp + '<br>';

			nlapiLogExecution('Debug', 'Log in Edit', str);

			var ismultilineship='';

			if(vebizOrdNo!='') 
			{	
				var trantypeso = nlapiLookupField('transaction', vsalesOrderId, 'recordType');
				nlapiLogExecution('Debug', 'trantypeso', trantypeso);
				var salesorderrec= nlapiLoadRecord(trantypeso, vsalesOrderId);
				if(salesorderrec!=null && salesorderrec!='')
					ismultilineship=salesorderrec.getFieldValue('ismultishipto');

				nlapiLogExecution('Debug', 'Item Line Shipping', ismultilineship);
				var confirmLotToNS='Y';
				confirmLotToNS=GetConfirmLotToNS(itemloc);
				nlapiLogExecution('Debug', 'confirmLotToNS', confirmLotToNS);

				//packlist generation code				
				
				if((nsconfirmationno==null || nsconfirmationno=='' || nsconfirmationno == ' ')  && (trantypeso!="vendorreturnauthorization" ))
				{
					
					try
					{
						var printername="";	
						//var printername=getStageLocationPrinter(vsalesOrderId);  
						CreatePackListCheckbyWmsStatusFlag(vsalesOrderId,vcontainerLp,vebizOrdNo,printername,wavenumber,salesorderrec,trantypeso);
					}
					catch(exp)
					{
						nlapiLogExecution('Debug', 'Exception in Create Packlist ', exp);	
					}
					//nlapiLogExecution('Debug', 'vcontainerLp1', vcontainerLp);
					if(ismultilineship!='T')
					{
						//nlapiLogExecution('Debug', 'vcontainerLp2', vcontainerLp);
						/***Wave number passing code is merged from Lexjet production by Ganesh on 5th Mar 2013***/
						nlapiLogExecution('Debug', 'Time Stamp at the start of createItemFulfillment',TimeStampinSec());
						var vTransferRecId=createItemFulfillment(vsalesOrderId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsconfirmationno,vcontainerLp,wavenumber,itemloc,fulfillmenttask,recid);
						nlapiLogExecution('Debug', 'Time Stamp at the end of createItemFulfillment',TimeStampinSec());
					}
					else
					{
						var vTransferRecId=createItemFulfillmentMultiLineShipping(vsalesOrderId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsconfirmationno,vcontainerLp,wavenumber,fulfillmenttask,itemloc,recid);
					}

					if(vTransferRecId != null && vTransferRecId != '')
					{
						var vAutoShipRule=getAutoShipRule(vsalesOrderId);
					}	

				}
			}

		}
		else
		{
			nlapiLogExecution('Debug', 'In Else');
			if(vsalesOrderId !='' && vsalesOrderId!=null && vsalesOrderId !='null')
			{
				var trantypeso = nlapiLookupField('transaction', vsalesOrderId, 'recordType');
				nlapiLogExecution('Debug', 'trantypeso', trantypeso);

				if(trantypeso == "vendorreturnauthorization")
				{
					nlapiLogExecution('Debug', 'solineno new log', solineno);
					
					var salesorderrec= nlapiLoadRecord(trantypeso, vsalesOrderId);
					//vsalesOrderLineno=newRecord.getFieldValue('custrecord_line_no');
					//salesorderrec.setLineItemValue('item','custcol_transactionstatusflag',vsalesOrderLineno,8);
					if(solineno != null && solineno != '')
						salesorderrec.setLineItemValue('item','custcol_transactionstatusflag',solineno,8);
					nlapiSubmitRecord(salesorderrec, true);	
				}
			}
		}
	}
	catch(e) {
		nlapiLogExecution('Debug', 'vebizOrdNoexp', vebizOrdNo);

		nlapiLogExecution('Debug', 'userId', userId);
		var exceptionname='SalesOrder';
		var functionality=vebizOrdNo+' - ' +'Item Fulfillment';
		var trantype=2;
		nlapiLogExecution('Debug', 'DetailsError', functionality);	
		nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('Debug', 'vebizOrdNo351', vebizOrdNo);
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var exceptiondetails=e;
		if ( e instanceof nlobjError )
		{
			nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
			exceptiondetails=e.getCode() + '\n' + e.getDetails();
		}
		else
		{
			exceptiondetails=e.toString();
			nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
		}

		UpdateOpenTaskWithExceptionValue(vsalesOrderId,vebizOrdNo,vcontainerLp,exceptiondetails);
		InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,vsalesOrderId);
		nlapiLogExecution('Debug', 'Exception in AfterActualEndDateUpdateUE : ', e);	
	}

	if((type=='create')||(type =='edit') || (type =='xedit'))
	{		
		try
		{			
			var newRecord = nlapiGetNewRecord();			
			var task_Type=newRecord.getFieldValue('custrecord_tasktype');//		
			var wms_statusflag=newRecord.getFieldValue('custrecord_wms_status_flag');		
			var PoID=newRecord.getFieldValue('custrecord_ebiz_cntrl_no');		
			var lineId=newRecord.getFieldValue('custrecord_line_no');
			var vsalesOrderId=newRecord.getFieldValue('custrecord_ebiz_order_no');
			var vcontainerLp=newRecord.getFieldValue('custrecord_container_lp_no');
			var vebizOrdNo=newRecord.getFieldValue('custrecord_ebiz_cntrl_no');
			var foname=newRecord.getFieldValue('name');
			var wavenumber=newRecord.getFieldValue('custrecord_ebiz_wave_no');
			var itemid=newRecord.getFieldValue('custrecord_sku');
			var expqty=newRecord.getFieldValue('custrecord_expe_qty');
			var wmslocation=newRecord.getFieldValue('custrecord_wms_location');
			var str = 'type. = ' + type + '<br>';
			str = str + 'task_Type. = ' + task_Type + '<br>';
			str = str + 'wms_statusflag. = ' + wms_statusflag + '<br>';	
			str = str + 'PoID. = ' + PoID + '<br>';	
			str = str + 'lineId. = ' + lineId + '<br>';
			str = str + 'vsalesOrderId. = ' + vsalesOrderId + '<br>';
			str = str + 'vcontainerLp. = ' + vcontainerLp + '<br>';
			str = str + 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
			str = str + 'foname. = ' + foname + '<br>';
			str = str + 'wmslocation. = ' + wmslocation + '<br>';

			nlapiLogExecution('Debug', 'Log in Create', str);

			if ((type=='create') ||(type =='edit'))
			{
				if((task_Type == 2 || task_Type==1) && (wms_statusflag==6 || wms_statusflag==2||wms_statusflag==1))
				{				
					//var TransformRecord = nlapiLoadRecord('purchaseorder', PoID);
					/*	var trantype = nlapiLookupField('transaction', PoID, 'recordType');
					var TransformRecord = nlapiLoadRecord(trantype, PoID);
					TransformRecord.setLineItemValue('item', 'custcol_transactionstatusflag',lineId,wmsstatusflag);
					nlapiSubmitRecord(TransformRecord, true);*/	
				}
				// Code added for DCD customer for generate Item labels
				nlapiLogExecution('Debug', 'Log in Label type', type);
				if ((type == 'create')) {
				var filters=new Array();
				var columns=new Array();

				filters.push(new nlobjSearchFilter('custrecord_ebizruletype', null, 'is',"Item Label"));
				filters.push(new nlobjSearchFilter('custrecord_ebizrulevalue', null, 'is',"Y"));


				if ((task_Type == 2) && (wms_statusflag == 2)) {
					var systemIntegrationRulevalue=getIntegrationSystemRuleValue("Receiving-ItemLabel", "");
					if(systemIntegrationRulevalue!=null && systemIntegrationRulevalue!="")
					{
						var systemrulevalue = systemIntegrationRulevalue[0];
						var systemruletype = systemIntegrationRulevalue[1];

						nlapiLogExecution('Debug', 'Generate ItemLabel Function for systemrulevalue + " " + systemruletype', systemrulevalue + " " + systemruletype);
						if (systemruletype == "BartenderFormat") 
						{

							if (systemrulevalue == "Standard")
							{
								generateitemlabel(expqty, itemid, foname);
							}
							else if (systemrulevalue == "Custom") 
							{
								//Add one of the Custom library file and use this function for reference
								try { 
									generatecustomitemlabel(newRecord);
								}
								catch (exp) {

									nlapiLogExecution('Debug', 'Exception in Custom   Item function ', exp);
								}
							}
						}
						if (systemruletype == "ZebraFormat")
						{

							if (systemrulevalue == "Standard") {
								generateZebraitemlabel(expqty, itemid, newRecord);
							}
							else if (systemrulevalue == "Custom") 
							{
								//Add one of the Custom library file and use this function for reference
								try {
									generateZebraCustomitemlabel(newRecord);

								}
								catch (exp) {

									nlapiLogExecution('Debug', 'Exception in Custom Zebra Item function ', exp);
								}
							}

						}

					}
					var systemIntegrationPalletRulevalue=getIntegrationSystemRuleValue("Receiving-PalletLabel", "");

					if(systemIntegrationPalletRulevalue!=null && systemIntegrationPalletRulevalue!="")
					{
						var systempalletrulevalue = systemIntegrationPalletRulevalue[0];
						var systempalletruletype = systemIntegrationPalletRulevalue[1];
						nlapiLogExecution('Debug', 'Generate ItemLabel Function for systemrulevalue + " " + systemruletype', systempalletrulevalue + " " + systempalletruletype);
						if (systempalletruletype == "BartenderFormat") 
						{

							if (systempalletrulevalue == "Standard")
							{
								genarateBartenderPalletLabel(newRecord);
							}
							else if(systempalletrulevalue == "Custom")
							{
								//Add one of the Custom library file and use this function for reference
								try
								{
									//generateCustomBartenderPalletLabel(newRecord);
									//This Pallet Function but Using as Item Label
									 generatecustomitemlabel(lineId, itemid, PoID,wmslocation,newRecord);
								}
								catch(exp)
								{
									nlapiLogExecution('Debug', 'Exception in Custom Bartender Pallet function ', exp);
								}
							}
						}
						if (systempalletruletype == "ZebraFormat") 
						{

							if (systempalletrulevalue == "Standard")
							{
								generateZebraPalletLabel(newRecord);
							}
							else if(systempalletrulevalue == "Custom")
							{

								try{
									generateCustomZebraPalletLabel(newRecord);
								}catch(exp){nlapiLogExecution('Debug', 'Exception in Custom Zebra Pallet function ', exp);}



							}
						}



					}

				}
				}

			}

			if(task_Type == 13)
			{
//				nlapiLogExecution('Debug', 'Into STGM Task...');

//				try
//				{
//				var printername=getStagingPrinter(stagelocation,vsalesOrderId);
//				//var printername=getStageLocationPrinter(vsalesOrderId);  
//				//nlapiLogExecution('Debug', 'printername',printername);

//				//CreatePackListCheckbyWmsStatusFlag(vsalesOrderId,vcontainerLp,foname,printername,wavenumber);
//				}
//				catch(exp)
//				{
//				nlapiLogExecution('Debug', 'Exception in Create Packlist ', exp);	
//				}
			}

			if(type=='edit'||type=="xedit")
			{
//				if(task_Type == 2 && wms_statusflag==3)			
//				{
//				nlapiLogExecution('Debug', 'from test : ', '');	
//				//var TransformRecord = nlapiLoadRecord('purchaseorder', PoID);
//				var trantype = nlapiLookupField('transaction', PoID, 'recordType');
//				var TransformRecord = nlapiLoadRecord(trantype, PoID);

//				TransformRecord.setLineItemValue('item', 'custcol_transactionstatusflag',lineId,wmsstatusflag);

//				nlapiSubmitRecord(TransformRecord, true);	
//				}
			}	

			if(task_Type==4)
			{
				var vAutoShipRule=getAutoShipRule(vsalesOrderId);
			}

		}
		catch(exp)
		{
			nlapiLogExecution('Debug', 'Exception in Create', exp);
		}
	}

	nlapiLogExecution('Debug', 'Time Stamp at the end of AfterActualEndDateUpdateUE',TimeStampinSec());
}

function getStagingPrinter(stagelocation,vsalesOrderId)
{
	nlapiLogExecution('Debug', 'Time Stamp at the start of getStagingPrinter',TimeStampinSec());	

	var trantype = nlapiLookupField('transaction', vsalesOrderId, 'recordType');	
	var shipmethod= nlapiLookupField(trantype, vsalesOrderId, 'shipmethod');

	nlapiLogExecution('Debug', 'Time Stamp at the end of nlapiLookupField',TimeStampinSec());

	var printername;
	try
	{
		var stgDirectiontype="OUB";
		if (stgDirectiontype== 'OUB') {
			stgDirection= "2";
		}
		else
		{
			stgDirection= "3";
		}
		var filterscarr = new Array();
		var columnscarr = new Array();
		var carriertype;
		var wmscarrier;
		if(shipmethod!=null && shipmethod!='')
			filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'anyof',shipmethod));
		filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

		columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');    
		columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');    
		columnscarr[2] = new nlobjSearchColumn('id');

		var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
		if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
		{
			carriertype = searchresultscarr[0].getValue('custrecord_carrier_type');
			wmscarrier = searchresultscarr[0].getId();
		}

		nlapiLogExecution('Debug', 'Stgwmscarrier', wmscarrier);
		var filters= new Array();
		filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null,'noneof',['14']));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if (wmscarrier != null && wmscarrier != "") 
		{
			filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@',wmscarrier]));

		}

		filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', [stagelocation]));
		filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [stgDirection, '3']));
		var columns = new Array();
		columns[0]=new nlobjSearchColumn('custrecord_stg_printername');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
		if(searchresults!=null)
		{
			for(var i=0;i<searchresults.length;i++)
			{

				printername=searchresults[i].getValue('custrecord_stg_printername');
			}
		}
		nlapiLogExecution('Debug', 'printername', printername);
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'printerException', exp);
	}

	nlapiLogExecution('Debug', 'Time Stamp at the end of getStagingPrinter',TimeStampinSec());

	return printername;
}

function getStageLocationPrinter(vsalesOrderId)
{
	var trantype = nlapiLookupField('transaction', vsalesOrderId, 'recordType');
	var salesorderdetails =nlapiLoadRecord(trantype, vsalesOrderId);
	var locationId =salesorderdetails.getFieldValue('location');
	var shipmethod=salesorderdetails.getFieldValue('shipmethod');
	var shipcompanyid=salesorderdetails.getFieldValue('custbody_nswms_company');
	var printername;
	var stgDirectiontype="OUB";
	if (stgDirectiontype== 'OUB') {
		stgDirection= "2";
	}
	else
	{
		stgDirection= "3";
	}
	var filterscarr = new Array();
	var columnscarr = new Array();
	var carriertype;
	var wmscarrier;
	if(shipmethod!=null && shipmethod!='')
		filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'anyof',shipmethod));
	filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

	columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');    
	columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');    
	columnscarr[2] = new nlobjSearchColumn('id');

	var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
	if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
	{
		carriertype = searchresultscarr[0].getValue('custrecord_carrier_type');
		wmscarrier = searchresultscarr[0].getId();
	}
	nlapiLogExecution('Debug', 'carriertype', carriertype);
	nlapiLogExecution('Debug', 'wmscarrier', wmscarrier);
	//var stagelocatioin=GetPickStageLocation("",wmscarrier,locationId,shipcompanyid,stgDirection,carriertype);
	//nlapiLogExecution('Debug', 'stagelocatioin', stagelocatioin);
	//return stagelocatioin;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null,'noneof',['14']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if((locationId!=null)||(locationId!=null))
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof',locationId));
	}
	if((shipcompanyid!=null)||(shipcompanyid!=null))
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof',shipcompanyid));
	}
	if (wmscarrier != null && wmscarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', [wmscarrier]));

	}
	if (carriertype != null && carriertype != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', carriertype]));

	}
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [stgDirection, '3']));
	var columns = new Array();
	columns[0]=new nlobjSearchColumn('custrecord_stg_printername');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if(searchresults!=null)
	{
		for(var i=0;i<searchresults.length;i++)
		{
			printername=searchresults[i].getValue('custrecord_stg_printername');
		}
	}
	nlapiLogExecution('Debug', 'printername', printername);

	return printername;
}

/**
 * 
 * @param itemid
 */
function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('custrecord_ebiznsuom', null, 'noneof', '@NONE@'));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}

/**
 * 
 * @param itemidArray
 */
function geteBizItemDimensionsArray(itemidArray)
{
	var searchRec = new Array();
	if(itemidArray!=null && itemidArray!='' && itemidArray.length>0)
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemidArray));
		filter.push(new nlobjSearchFilter('custrecord_ebiznsuom', null, 'noneof', '@NONE@'));
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
		column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
		column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
		column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
		column[4] = new nlobjSearchColumn('custrecord_ebizitemdims') ;
		searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
	}

	return searchRec;

}

function AutoPacking(vebizOrdNo,whlocation,sostatusdet,foebizOrdNo,pcontainersize,vcontainerLp,pshipasis,vhostid){	
	nlapiLogExecution('Debug', 'into AutoPacking', TimeStampinSec());

	var str = 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
	str = str + 'whlocation. = ' + whlocation + '<br>';	
	str = str + 'sostatusdet. = ' + sostatusdet + '<br>';	
	str = str + 'foebizOrdNo. = ' + foebizOrdNo + '<br>';
	str = str + 'vcontainerLp. = ' + vcontainerLp + '<br>';	
	str = str + 'vhostid. = ' + vhostid + '<br>';	
	str = str + 'pshipasis. = ' + pshipasis + '<br>';	
	str = str + 'pcontainersize. = ' + pcontainersize + '<br>';	

	nlapiLogExecution('DEBUG', 'AutoPacking Parameters', str);

	var packcomplete='F';
	if(vebizOrdNo!=null && vebizOrdNo!='')
	{
		var vOrdAutoPackFlag="";
		var vStgAutoPackFlag="";
		var vStgCarrierType="";
		var vCarrierType="";
		var vStgCarrierName="";
		var vorderType='';
		var nsshipmethod='';
		var vcarrier='';
		var vcarrierline='';
		var vcarrierlineID='';
		var vCarrierTypeID='';
		var trantype='';
		//var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vebizOrdNo);
		var vOrderAutoPackFlag = sostatusdet;
		if(vOrderAutoPackFlag!=null&& vOrderAutoPackFlag.length>0){
			vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
			vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
			vorderType = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
			nsshipmethod = vOrderAutoPackFlag[0].getValue('shipmethod');
			trantype = vOrderAutoPackFlag[0].getValue('recordType');

			var str = 'vorderType. = ' + vorderType + '<br>';
			str = str + 'vOrdAutoPackFlag. = ' + vOrdAutoPackFlag + '<br>';	
			str = str + 'vCarrierType. = ' + vCarrierType + '<br>';	
			str = str + 'nsshipmethod. = ' + nsshipmethod + '<br>';	
			str = str + 'trantype. = ' + trantype + '<br>';	

			nlapiLogExecution('DEBUG', 'Order Type Parameters', str);

		}

		if(vCarrierType==null || vCarrierType=="")
		{
			if(nsshipmethod!=null && nsshipmethod!='')
			{
				var filterscarr = new Array();
				var columnscarr = new Array();

				filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',nsshipmethod));
				filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

				columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
				columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
				columnscarr[2] = new nlobjSearchColumn('id');

				var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
				if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
				{
					vCarrierType = searchresultscarr[0].getText('custrecord_carrier_type');
					vcarrier = searchresultscarr[0].getId();
				}
			}
			else
			{
				var filterscarr = new Array();
				var columnscarr = new Array();
				filterscarr.push(new nlobjSearchFilter('custrecord_lineord', null, 'is',foebizOrdNo));
				filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));
				columnscarr[0] = new nlobjSearchColumn('custrecord_do_carrier');

				var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filterscarr, columnscarr);
				if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
				{					
					vcarrierline=searchresultscarr[0].getText('custrecord_do_carrier');
					vcarrierlineID=searchresultscarr[0].getValue('custrecord_do_carrier');
				}
				nlapiLogExecution('Debug', 'vcarrierline', vcarrierline);
				if(vcarrierline!=null && vcarrierline!='')
				{
					var filterscarrtype = new Array();
					var columnscarrtype = new Array();
					//filterscarrtype.push(new nlobjSearchFilter('name', null, 'is',vcarrierline));
					filterscarrtype.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'anyof',vcarrierlineID));
					filterscarrtype.push(new nlobjSearchFilter('isinactive', null, 'is','F'));
					columnscarrtype[0] = new nlobjSearchColumn('custrecord_carrier_type');

					var searchresultscarrtype = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarrtype, columnscarrtype);
					if(searchresultscarrtype!=null && searchresultscarrtype!='')
					{
						vCarrierType=searchresultscarrtype[0].getText('custrecord_carrier_type');
						vcarrier = searchresultscarrtype[0].getId();
						vCarrierTypeID = searchresultscarrtype[0].getValue('custrecord_carrier_type');
					}
				}
			}
		}

		var str = 'vCarrierType. = ' + vCarrierType + '<br>';
		str = str + 'vcarrier. = ' + vcarrier + '<br>';	
		str = str + 'vOrdAutoPackFlag. = ' + vOrdAutoPackFlag + '<br>';	
		str = str + 'vCarrierTypeID. = ' + vCarrierTypeID + '<br>';	

		nlapiLogExecution('DEBUG', 'Carrier Parameters', str);

		if(vOrdAutoPackFlag!='T' || (vCarrierType=="" || vCarrierType==null)){
			var vStageAutoPackFlag = getAutoPackFlagforStage(whlocation,vcarrier,vorderType,vCarrierTypeID);
			if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
				vStgCarrierType = vStageAutoPackFlag[0][0];
				vStgAutoPackFlag = vStageAutoPackFlag[0][1];
				vStgCarrierName = vStageAutoPackFlag[0][2];
			}
		}

		if(vCarrierType=="" && vStgCarrierType!="")
			vCarrierType=vStgCarrierType;

		nlapiLogExecution('Debug', 'vCarrierType', vCarrierType);

		if(vOrdAutoPackFlag=='T' || vStgAutoPackFlag=='T' || pcontainersize=="SHIPASIS" || pshipasis =='T' ||
				(vhostid!=null && vhostid!='' && vhostid!='null' && vhostid.toUpperCase()=='REMSTAR')){
			var context = nlapiGetContext();
			nlapiLogExecution('Debug','Remaining usage at the start of UpdatesInOpenTask ',context.getRemainingUsage());
			UpdatesInOpenTask(vebizOrdNo,vCarrierType,vStgCarrierName,vcontainerLp,foebizOrdNo);
			packcomplete='T';
		}
	}
	nlapiLogExecution('Debug', 'out of AutoPacking',TimeStampinSec());
	return packcomplete;	
}

function AutoPackingAfterfulfillment(vebizOrdNo,whlocation,foebizOrdNo,FULFILLMENTATORDERLEVEL,vcontainerLp){	
	nlapiLogExecution('Debug', 'into AutoPackingAfterfulfillment', TimeStampinSec());

	var str = 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
	str = str + 'whlocation. = ' + whlocation + '<br>';	
	str = str + 'foebizOrdNo. = ' + foebizOrdNo + '<br>';
	str = str + 'FULFILLMENTATORDERLEVEL. = ' + FULFILLMENTATORDERLEVEL + '<br>';	
	str = str + 'vcontainerLp. = ' + vcontainerLp + '<br>';

	nlapiLogExecution('DEBUG', 'AutoPackingAfterfulfillment Parameters', str);

	var packcomplete;
	if(vebizOrdNo!=null && vebizOrdNo!='')
	{
		nlapiLogExecution('Debug', 'into AutoPacking vebizOrdNo is not null', vebizOrdNo);
		var vOrdAutoPackFlag="";
		var vStgAutoPackFlag="";
		var vStgCarrierType="";
		var vCarrierType="";
		var vStgCarrierName="";
		var vorderType='';
		var nsshipmethod='';
		var vcarrier='';
		var vcarrierline='';
		var vcarrierlineID='';
		var vCarrierTypeID='';
		var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vebizOrdNo);
		if(vOrderAutoPackFlag!=null&& vOrderAutoPackFlag.length>0){
			vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
			vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
			vorderType = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
			nsshipmethod = vOrderAutoPackFlag[0].getValue('shipmethod');
		}

		if(vCarrierType==null || vCarrierType=="")
		{
			if(nsshipmethod!=null && nsshipmethod!='')
			{
				var filterscarr = new Array();
				var columnscarr = new Array();

				filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',nsshipmethod));
				filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

				columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
				columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
				columnscarr[2] = new nlobjSearchColumn('id');

				var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
				if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
				{
					vCarrierType = searchresultscarr[0].getText('custrecord_carrier_type');
					vcarrier = searchresultscarr[0].getId();
				}
			}
			else
			{
				var filterscarr = new Array();
				var columnscarr = new Array();
				filterscarr.push(new nlobjSearchFilter('custrecord_lineord', null, 'is',foebizOrdNo));
				filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));
				columnscarr[0] = new nlobjSearchColumn('custrecord_do_carrier');

				var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filterscarr, columnscarr);
				if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
				{					
					vcarrierline=searchresultscarr[0].getText('custrecord_do_carrier');
					vcarrierlineID=searchresultscarr[0].getValue('custrecord_do_carrier');
				}
				nlapiLogExecution('Debug', 'vcarrierline', vcarrierline);
				if(vcarrierline!=null && vcarrierline!='')
				{
					var filterscarrtype = new Array();
					var columnscarrtype = new Array();
					//filterscarrtype.push(new nlobjSearchFilter('name', null, 'is',vcarrierline));
					filterscarrtype.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'anyof',vcarrierlineID));
					filterscarrtype.push(new nlobjSearchFilter('isinactive', null, 'is','F'));
					columnscarrtype[0] = new nlobjSearchColumn('custrecord_carrier_type');

					var searchresultscarrtype = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarrtype, columnscarrtype);
					if(searchresultscarrtype!=null && searchresultscarrtype!='')
					{
						vCarrierType=searchresultscarrtype[0].getText('custrecord_carrier_type');
						vcarrier = searchresultscarrtype[0].getId();
						vCarrierTypeID = searchresultscarrtype[0].getValue('custrecord_carrier_type');
					}
				}
			}
		}

		if(vOrdAutoPackFlag!='T' || (vCarrierType=="" || vCarrierType==null)){
			var vStageAutoPackFlag = getAutoPackFlagforStage(whlocation,vcarrier,vorderType,vCarrierTypeID);
			if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
				vStgCarrierType = vStageAutoPackFlag[0][0];
				vStgAutoPackFlag = vStageAutoPackFlag[0][1];
				vStgCarrierName = vStageAutoPackFlag[0][2];
			}
		}

		if(vCarrierType=="" && vStgCarrierType!="")
			vCarrierType=vStgCarrierType;

		nlapiLogExecution('Debug', 'vCarrierType', vCarrierType);

		//if(vOrdAutoPackFlag=='T' || vStgAutoPackFlag=='T')
		//{
		var context = nlapiGetContext();
		nlapiLogExecution('Debug','Remaining usage at the start of AutoPackingAfterfulfillment UpdatesInOpenTask ',context.getRemainingUsage());
		//UpdatesInOpenTask(vebizOrdNo,vCarrierType,vStgCarrierName);
		UpdatesInOpenTaskAfterfulfillment(vebizOrdNo,vCarrierType,vStgCarrierName,FULFILLMENTATORDERLEVEL,vcontainerLp);
		//}
	}
	nlapiLogExecution('Debug', 'out of AutoPackingAfterfulfillment',TimeStampinSec());
	return packcomplete;	
}

function UpdatesInOpenTaskAfterfulfillment(vebizOrdNo,vCarrierType,CarrieerName,FULFILLMENTATORDERLEVEL,vcontainerLp){
	nlapiLogExecution('Debug', 'into UpdatesInOpenTaskAfterfulfillment', TimeStampinSec());

	var str = 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
	str = str + 'vCarrierType. = ' + vCarrierType + '<br>';	
	str = str + 'CarrieerName. = ' + CarrieerName + '<br>';
	str = str + 'FULFILLMENTATORDERLEVEL. = ' + FULFILLMENTATORDERLEVEL + '<br>';	
	str = str + 'vcontainerLp. = ' + vcontainerLp + '<br>';

	nlapiLogExecution('DEBUG', 'UpdatesInOpenTaskAfterfulfillment Parameters', str);

	var packstation = getPackStation();
	var DataRecArr=new Array();
	//var containerlpArray=new Array();
	var ebizorderno;
	var filters = new Array();

	if(vebizOrdNo!=""){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vebizOrdNo));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',null,'isnotempty')); // NSconf# is not empty
	if((FULFILLMENTATORDERLEVEL!='Y') && (vcontainerLp!='undefined') && (vcontainerLp!=null) && (vcontainerLp!=""))
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vcontainerLp));//Task Type - PICK

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_comp_id');
	columns[5] = new nlobjSearchColumn('custrecord_site_id');
	columns[6] = new nlobjSearchColumn('custrecord_container');
	columns[7] = new nlobjSearchColumn('custrecord_total_weight');
	columns[8] = new nlobjSearchColumn('custrecord_sku');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no');

	//columns[0].setSort();
	columns[1].setSort();
	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	if(salesOrderList != null && salesOrderList.length > 0){
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item,waveno;	
		for (var i = 0; i < salesOrderList.length; i++){
			var context = nlapiGetContext();
			//nlapiLogExecution('Debug','Remaining usage in the for loop starting ',context.getRemainingUsage());
			orderno = salesOrderList[i].getValue('name');
			containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
			ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
			ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
			compid = salesOrderList[i].getValue('custrecord_comp_id');
			siteid = salesOrderList[i].getValue('custrecord_site_id');
			containersize = salesOrderList[i].getValue('custrecord_container');
			totweight = salesOrderList[i].getValue('custrecord_total_weight');
			item = salesOrderList[i].getValue('custrecord_sku');//modified on dec 12
			waveno = salesOrderList[i].getValue('custrecord_ebiz_wave_no');
			internalid = salesOrderList[i].getId();
			DataRecArr.push(new DataRec(compid,siteid,ebizorderno,orderno,containerno,packstation,ebizcntrlno,item,waveno));			
		} 

		if(DataRecArr.length>0)
		{
			nlapiLogExecution('Debug', 'DataRecArr.length', DataRecArr.length);
			var oldcontainer="";
			for (var k = 0; k < DataRecArr.length; k++)
			{	
				var DataRecord=DataRecArr[k];
				var containerlpno =  DataRecord.DataRecContainer;
				//nlapiLogExecution('Debug', 'Before WaveNo getting', 'Before WaveNo getting');
				var ShipmanifestWave =  DataRecord.DataRecWaveno;
				//nlapiLogExecution('Debug', 'AFter WaveNo getting', ShipmanifestWave);

				var str = 'oldcontainer. = ' + oldcontainer + '<br>';
				str = str + 'containerlpno. = ' + containerlpno + '<br>';	

				nlapiLogExecution('Debug', 'Old & New Containers', str);

				if(oldcontainer!=containerlpno)
				{
					if(vCarrierType=="PC")
					{	
						CreateShippingManifestRecord(ebizorderno,containerlpno,CarrieerName,ShipmanifestWave);
					}

					var context = nlapiGetContext();
					//nlapiLogExecution('Debug','Remaining usage after Ship manifest ',context.getRemainingUsage());
					oldcontainer = containerlpno;
				}
			}
		}
	}

	nlapiLogExecution('Debug', 'out of UpdatesInOpenTask function', vebizOrdNo);
}

function getAutoPackFlagforStage(whlocation,vcarrier,vorderType,vCarrierTypeID){
	nlapiLogExecution('Debug', 'into getAutoPackFlagforStage',TimeStampinSec());

	var vStgRule = new Array();

	vStgRule = getStageRulewithCarrierType('', vcarrier, whlocation, '', 'OUB',vorderType,vCarrierTypeID);

	nlapiLogExecution('Debug', 'out of getAutoPackFlagforStage',TimeStampinSec());
	return vStgRule;
}

function getAutoPackFlagforOrdType(vebizOrdNo){
	nlapiLogExecution('Debug', 'into getAutoPackFlagforOrdType',TimeStampinSec());

	nlapiLogExecution('Debug', 'vebizOrdNo',vebizOrdNo);

	var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
	nlapiLogExecution('Debug', 'trantype', trantype);
	var searchresults = new Array();

	if(trantype=="salesorder")
	{ 
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
		columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
		columns[2] = new nlobjSearchColumn('custbody_nswmssoordertype');
		columns[3] = new nlobjSearchColumn('shipmethod');
		columns[4] = new nlobjSearchColumn('entity');
		columns[5] = new nlobjSearchColumn('custbody_nswmspriority');

		searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	}
	else		 
	{ 
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
		columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
		columns[2] = new nlobjSearchColumn('custbody_nswmssoordertype');
		columns[3] = new nlobjSearchColumn('shipmethod');

		searchresults = nlapiSearchRecord('transferorder', null, filters, columns);
	}
	nlapiLogExecution('Debug', 'out of getAutoPackFlagforOrdType', TimeStampinSec());
	return searchresults;
}

function UpdatesInOpenTask(vebizOrdNo,vCarrierType,CarrieerName,vcontainerLp,foebizOrdNo){
	nlapiLogExecution('Debug', 'into UpdatesInOpenTask', TimeStampinSec());

	var str = 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
	str = str + 'vCarrierType. = ' + vCarrierType + '<br>';	
	str = str + 'CarrieerName. = ' + CarrieerName + '<br>';
	str = str + 'foebizOrdNo. = ' + foebizOrdNo + '<br>';	
	str = str + 'vcontainerLp. = ' + vcontainerLp + '<br>';

	nlapiLogExecution('DEBUG', 'UpdatesInOpenTask Parameters', str);

	var context = nlapiGetContext();
	var packstation = getPackStation();
	var DataRecArr=new Array();
	var picktaskarr = new Array();
	//var containerlpArray=new Array();
	var ebizorderno;
	var filters = new Array();

	if(vebizOrdNo!=""){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vebizOrdNo));
	}

	if(foebizOrdNo!=""){
		filters.push( new nlobjSearchFilter('name', null, 'is', foebizOrdNo));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	/*** Below change is merged from Factory mation production on March 1st 2013 by Ganesh K***/
	filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
	filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0.0'));
	/***Upto here***/
	//Below code is commented because of if two pick tasks are picked by diiferent user defined container Lps then system is creating only one pack task for last container
	/*if(vcontainerLp!=null && vcontainerLp!='' && vcontainerLp!='null' && vcontainerLp!="undefined")
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vcontainerLp));//Task Type - PICK*/
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_comp_id');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_container');
	columns[7] = new nlobjSearchColumn('custrecord_total_weight');
	columns[8] = new nlobjSearchColumn('custrecord_sku');
	columns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[10] = new nlobjSearchColumn('custrecord_actendloc');
	columns[11] = new nlobjSearchColumn('custrecord_batch_no');
	columns[12] = new nlobjSearchColumn('custrecord_act_qty');
	columns[13] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[14] = new nlobjSearchColumn('custrecord_uom_level');
	columns[15] = new nlobjSearchColumn('custrecord_ebizuser');
	columns[16] = new nlobjSearchColumn('custrecordact_begin_date');
	columns[17] = new nlobjSearchColumn('custrecord_actualbegintime');
	columns[18] = new nlobjSearchColumn('custrecord_packcode');
	columns[19] = new nlobjSearchColumn('custrecord_lpno');
	columns[20] = new nlobjSearchColumn('custrecord_line_no');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[22] = new nlobjSearchColumn('custrecord_ebiz_customer');


	//columns[0].setSort();
	columns[1].setSort();
	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	if(salesOrderList != null && salesOrderList.length > 0){
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item,waveno,
		beginloc,endloc,lot,actqty,expqty,uomlevel,empid,begindate,begintime,packcode,lp,lineno,customer;	
		for (var i = 0; i < salesOrderList.length; i++){

			orderno = salesOrderList[i].getValue('name');
			containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
			ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
			ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
			compid = salesOrderList[i].getValue('custrecord_comp_id');
			siteid = salesOrderList[i].getValue('custrecord_wms_location');
			containersize = salesOrderList[i].getValue('custrecord_container');
			totweight = salesOrderList[i].getValue('custrecord_total_weight');
			item = salesOrderList[i].getValue('custrecord_sku');
			beginloc = salesOrderList[i].getValue('custrecord_actbeginloc');
			endloc = salesOrderList[i].getValue('custrecord_actendloc');
			lot = salesOrderList[i].getValue('custrecord_batch_no');
			actqty = salesOrderList[i].getValue('custrecord_act_qty');
			expqty = salesOrderList[i].getValue('custrecord_expe_qty');
			uomlevel = salesOrderList[i].getValue('custrecord_uom_level');
			empid = salesOrderList[i].getValue('custrecord_ebizuser');
			internalid = salesOrderList[i].getId();
			begindate=salesOrderList[i].getValue('custrecordact_begin_date');
			begintime=salesOrderList[i].getValue('custrecord_actualbegintime');
			packcode=salesOrderList[i].getValue('custrecord_packcode');
			lp=salesOrderList[i].getValue('custrecord_lpno');
			lineno=salesOrderList[i].getValue('custrecord_line_no');
			waveno = salesOrderList[i].getValue('custrecord_ebiz_wave_no');
			customer=salesOrderList[i].getValue('custrecord_ebiz_customer');

			DataRecArr.push(new DataRec(compid,siteid,ebizorderno,orderno,containerno,packstation,ebizcntrlno,item,waveno,totweight,containersize,customer));


			var currRow = [beginloc,endloc,lot,actqty,expqty,siteid,uomlevel,empid,internalid,
			               context.getUser(),containerno,ebizorderno,ebizcntrlno,begindate,begintime,
			               item,packcode,lp,lineno,compid,containersize,totweight,orderno,waveno];

			picktaskarr.push(currRow);

			updateStatusInOpenTask(internalid,28);//28 - Status Flag - Outbound Pack Complete
		} 

		if(DataRecArr.length>0)
		{
			CreatePACKTaskBulk(DataRecArr);	
		}

		if(DataRecArr.length>0)
		{
			nlapiLogExecution('Debug', 'DataRecArr.length', DataRecArr.length);
			var oldcontainer="";
			for (var k = 0; k < DataRecArr.length; k++)
			{	
				var DataRecord=DataRecArr[k];
				var containerlpno =  DataRecord.DataRecContainer;

				if(oldcontainer!=containerlpno)
				{
					nlapiLogExecution('Debug', 'Time Stamp at the start of updateStatusInLPMaster',TimeStampinSec());
					updateStatusInLPMaster(containerno,28);//28 - Status Flag - Outbound Pack Complete
					nlapiLogExecution('Debug', 'Time Stamp at the end of updateStatusInLPMaster',TimeStampinSec());

					oldcontainer = containerlpno;	
				}
			}
		}
	}

	nlapiLogExecution('Debug', 'out of UpdatesInOpenTask function', TimeStampinSec());
}

function updateOpentaskBulk(picktaskarr)
{
	nlapiLogExecution('Debug', 'Time Stamp at the start of Update Opentask Bulk',TimeStampinSec());

//	var currRow = [0-beginloc,1-endloc,2-lot,3-actqty,4-expqty,5-siteid,6-uomlevel,7-empid,8-picktaskintid,
//	9-ebizUser,10-containerno,11-ebizorderno,12-ebizcntrlno,13-begindate,14-begintime,
//	15-item,16-packcode,17-lp,18-lineno,19-compid,20-containersize,21-totweight,22-orderno,23-waveno];

	if(picktaskarr!=null && picktaskarr!='' && picktaskarr.length>0)
	{
		nlapiLogExecution('Debug', 'picktaskarr length',picktaskarr.length);

		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);

		for (var s = 0; s < picktaskarr.length; s++) {

			newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', picktaskarr[s][8]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', picktaskarr[s][22]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', picktaskarr[s][23]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', picktaskarr[s][1]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', picktaskarr[s][2]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', 28);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', picktaskarr[s][5]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_level', picktaskarr[s][6]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', parseFloat(picktaskarr[s][3]).toFixed(5));
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', picktaskarr[s][9]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', picktaskarr[s][0]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',picktaskarr[s][13]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', picktaskarr[s][14]);

			if (picktaskarr[s][9] != null && picktaskarr[s][9] != "") 
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', picktaskarr[s][9]);
			else
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', picktaskarr[s][7]);

			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', picktaskarr[s][12]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', 3);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', parseFloat(picktaskarr[s][4]).toFixed(5));
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', picktaskarr[s][15]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', picktaskarr[s][15]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', picktaskarr[s][16]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', picktaskarr[s][17]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', picktaskarr[s][11]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', picktaskarr[s][18]);

			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', picktaskarr[s][19]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', picktaskarr[s][10]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', picktaskarr[s][20]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', parseFloat(picktaskarr[s][21]).toFixed(5));
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_pack_confirmed_date', DateStamp());

			newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');
		}

		nlapiSubmitRecord(newParent); 
	}

	nlapiLogExecution('Debug', 'Time Stamp at the end of Update Opentask Bulk',TimeStampinSec());
}

/**
 * To create DataRec for multidimentional array
 *  
 */
function DataRec(vCompId, vSiteId, vEbizOrdNo,vOrdNo,vContainer,vPackStation,vEbizControlNo,vItem,waveno,totweight,containersize,customer) 
{
	if(vCompId != null && vCompId != '')
		this.DataRecCompId = vCompId;
	else
		this.DataRecCompId=null;

	if(vSiteId != null && vSiteId != '')
		this.DataRecSiteId = vSiteId;
	else
		this.DataRecSiteId=null;
	this.DataRecEbizOrdNo = vEbizOrdNo;
	this.DataRecOrdNo = vOrdNo;
	this.DataRecContainer = vContainer;
	this.DataRecPackStation = vPackStation;
	this.DataRecEbizControlNo = vEbizControlNo;
	this.DataRecItem = vItem;	
	this.DataRecWaveno = waveno;
	this.DataRectotalweight = totweight;
	this.DataRecContainerSizeId = containersize;
	this.DataRecCustomer = customer;
}	


function createItemFulfillment(SalesOrderInternalId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsrefno,vcontainerLp,wavenumber,
		itemloc,fulfillmenttask,packtaskid,LoadSalesOrderRec)
{	
	nlapiLogExecution('Debug', 'Into createItemFulfillment',TimeStampinSec());

	var str4 = 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str4 = str4 + 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
	str4 = str4 + 'vWMSCarrier. = ' + vWMSCarrier + '<br>';
	str4 = str4 + 'confirmLotToNS. = ' + confirmLotToNS + '<br>';	
	str4 = str4 + 'nsrefno. = ' + nsrefno + '<br>';
	str4 = str4 + 'vcontainerLp. = ' + vcontainerLp + '<br>';
	str4 = str4 + 'wavenumber. = ' + wavenumber + '<br>';
	str4 = str4 + 'itemloc. = ' + itemloc + '<br>';
	str4 = str4 + 'fulfillmenttask. = ' + fulfillmenttask + '<br>';
	str4 = str4 + 'packtaskid. = ' + packtaskid + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str4);

	var userId = nlapiGetUser();
	//Post Itemfulfillment at Order level? (Y or N)
	//case# 20148056 starts
	var FULFILLMENTATORDERLEVEL = getSystemRuleValue('IF001',itemloc);
	//case# 20148056 ends

	var lineArray = new Array();
	var filterLineArray = new Array();
	var filterItemArray =new Array(); // For distinct item ids
	//var AddTotalArray = new Array();
	var itemListArray= new Array();
	var ItemArray= new Array();
	var filter= new Array();
	var TransformRecId = null;

	if(SalesOrderInternalId != null && SalesOrderInternalId != '')
		filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 

	filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); //PICK

	if(fulfillmenttask=='PICK')
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['9']));//9 //Picks generated
	else
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8','9']));//9 //Picks generated or Picks confirmed

	if(vWMSCarrier != null && vWMSCarrier != '')
		filter.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier

	if(FULFILLMENTATORDERLEVEL!='Y')
	{
		nlapiLogExecution('Debug', 'FULFILLMENTATORDERLEVEL inside',FULFILLMENTATORDERLEVEL);
		nlapiLogExecution('Debug', 'vcontainerLp inside',vcontainerLp);
		filter.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
	}
	else
	{
		filter.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
	}

	filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
	filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0.0'));
	filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

	var columns= new Array(); 

	columns[0] = new nlobjSearchColumn('custrecord_act_qty');
	columns[1] = new nlobjSearchColumn('custrecord_line_no');
	columns[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[3]= new nlobjSearchColumn('custrecord_act_end_date');

	var opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);

	nlapiLogExecution('Debug', 'opentaskordersearchresult',opentaskordersearchresult);

	if(opentaskordersearchresult==null || opentaskordersearchresult=='') // To make sure there is not records exists with Picks generated stage for the order
	{
		nlapiLogExecution('Debug', 'No Open Records. All are Pack Confirmaed');
		var QuilfiedItemArray=new Array();
		var QuilfiedItemLineArray=new Array();
		var QuilfiedItemLineKitArray=new Array();
		var filters= new Array();					

		if(SalesOrderInternalId != null && SalesOrderInternalId != '')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 

		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
		if(fulfillmenttask=='PICK')
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8']));//8 //Picks confirmed
		else
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));//28//Pack Complete
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
		/*** Below change merged from Factory mation production account on 1st March 2013 by Ganesh ***/
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 

		filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		/***Upto here***/
		if(vWMSCarrier != null && vWMSCarrier != '')
			filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier

		if(FULFILLMENTATORDERLEVEL!='Y')
		{
			nlapiLogExecution('Debug', 'vcontainerLp inside if',vcontainerLp);
			filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
		}
		else
		{
			filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		}

		var column =new Array(); 

		column[0] = new nlobjSearchColumn('custrecord_act_qty');
		column[1] = new nlobjSearchColumn('custrecord_line_no');
		column[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
		column[3]= new nlobjSearchColumn('custrecord_act_end_date');
		column[4]= new nlobjSearchColumn('internalid');
		column[5]= new nlobjSearchColumn('name');
		column[6]= new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		column[7]= new nlobjSearchColumn('custrecord_ebiz_order_no');
		column[8]= new nlobjSearchColumn('custrecord_serial_no');
		column[9]= new nlobjSearchColumn('custrecord_batch_no');					
		column[10]= new nlobjSearchColumn('custrecord_total_weight');
		column[11]= new nlobjSearchColumn('custrecord_totalcube');
		column[12]= new nlobjSearchColumn('custrecord_parent_sku_no');
		column[13]= new nlobjSearchColumn('custrecord_sku');
		column[1].setSort();
		column[9].setSort();

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

		var DoLineId="";
		var totalWeight=0.0;var totalcube=0.0;
		if(searchresult !=null && searchresult !='') // SearchResult contains all the open task records of task type PICK and status flag PACK COMPLETE
		{
			nlapiLogExecution('Debug', 'searchresult length',searchresult.length);
			var sum =0;
			var actqty="";
			var linenumber="";
			var skuno="";
			var vBatchno="";
			var vserialno="";

			if(parseInt(searchresult.length)>50)
			{
				nlapiLogExecution('Debug', 'Invoking Schedule Script Starts',searchresult.length);

				var param = new Array();
				param['custscript_soorderinternalid'] = SalesOrderInternalId;
				param['custscript_ebizordno'] = vebizOrdNo;
				param['custscript_wmscarrier'] = vWMSCarrier;
				param['custscript_confirmtons'] = confirmLotToNS;
				param['custscript_ebizwavenumber'] = wavenumber;
				param['custscript_containerlp'] = vcontainerLp;
				param['custscript_itemloc'] = itemloc;
				param['custscript_fulfillmenttask'] = fulfillmenttask;
				param['custscript_ismultilineship'] = 'F';
				param['custscript_recid'] = '';

				nlapiScheduleScript('customscript_ebiz_itemfulfillment_sch', null,param);


			}			
			else
			{

				DoLineId=searchresult[0].getValue('custrecord_ebiz_cntrl_no');

				nlapiLogExecution('Debug', 'searchresult.length',searchresult.length);

				for (l = 0; l < searchresult.length; l++) 
				{  
					if(SalesOrderInternalId==null || SalesOrderInternalId=='')
						SalesOrderInternalId=searchresult[l].getValue('custrecord_ebiz_order_no');	

					itemListArray[l] = new Array();

					actqty=searchresult[l].getValue('custrecord_act_qty');
					linenumber=searchresult[l].getValue('custrecord_line_no');
					skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
					/***Below code has been merged from Factory mation prodcution by Ganesh on 1st March 2013***/					
					var parentskuno=searchresult[l].getValue('custrecord_parent_sku_no');
					var skuname=searchresult[l].getText('custrecord_parent_sku_no');
					var skuvalue=searchresult[l].getValue('custrecord_ebiz_sku_no');
					var skutext=searchresult[l].getText('custrecord_sku');
					/***Upto here***/
					vBatchno = searchresult[l].getValue('custrecord_batch_no');
					vserialno = searchresult[l].getValue('custrecord_serial_no');
					if(searchresult[l].getValue('custrecord_total_weight')!=null && searchresult[l].getValue('custrecord_total_weight')!='')
					{
						totalWeight=totalWeight+parseFloat(searchresult[l].getValue('custrecord_total_weight'));
					}
					if(searchresult[l].getValue('custrecord_totalcube')!=null && searchresult[l].getValue('custrecord_totalcube')!='')
					{
						totalcube=totalcube+parseFloat(searchresult[l].getValue('custrecord_totalcube'));
					}
					itemListArray[l][0]=actqty;
					itemListArray[l][1]=skuno;
					itemListArray[l][2]=linenumber;
					itemListArray[l][3]=vBatchno;
					itemListArray[l][4]=vserialno;
					/***Below code has been merged from factory mation production by Ganesh on 1st March 2013***/
					itemListArray[l][5]=skuname;
					itemListArray[l][6]=parentskuno;
					itemListArray[l][7]=skuvalue;
					itemListArray[l][8]=skutext;
					/***Upto here ***/
					ItemArray.push(skuno);
					lineArray.push(linenumber);
				}

				// The below logic is to get all the distinct line nos to filterLineArray . This contails distinct lines nos for the order.
				label:for(var i=0; i<lineArray.length;i++ )
				{  
					for(var j=0; j<filterLineArray.length;j++ )
					{
						if(filterLineArray [j]==lineArray [i]) 
							continue label;
					}
					filterLineArray [filterLineArray .length] = lineArray [i];
				}

				filterLineArray.sort(function(a,b){return a - b;});

				// The below logic is to get all the distinct Item ids filterItemArray . This contains distinct Item ids for the order.
				label2:for(var m=0; m<ItemArray.length;m++ )
				{  
					for(var p=0; p<filterItemArray.length;p++ )
					{
						if(filterItemArray[p]==ItemArray[m]) 
							continue label2;
					}
					filterItemArray[filterItemArray.length] = ItemArray[m];
				}

				// To fetch Item dims from Item Array
				var vItemDimsArr = geteBizItemDimensionsArray(filterItemArray);
				if(vItemDimsArr !=null && vItemDimsArr != "")
				{
					nlapiLogExecution('Debug', 'vItemDimsArr.length', vItemDimsArr.length);
				}	

				var TransformRec;

				var filters= new Array();

				filters.push(new nlobjSearchFilter('internalid', null,'is', SalesOrderInternalId));
				filters.push(new nlobjSearchFilter('mainline',null,'is','T'));

				var column =new Array(); 

				column[0] = new nlobjSearchColumn('custbody_nswmssoordertype');
				column[1] = new nlobjSearchColumn('custbody_total_weight');

				var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
				nlapiLogExecution('Debug', 'trantype', trantype);
				var vAdvBinManagement=false;

				var ctx = nlapiGetContext();
				if(ctx != null && ctx != '')
				{
					if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
						vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
				}  
				nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);
				if(trantype=="salesorder")
				{
					var orderTypeSearchResult = nlapiSearchRecord('salesorder',null,filters,column);
					if(orderTypeSearchResult!=null && orderTypeSearchResult.length>0)
					{
						try
						{
							if(orderTypeSearchResult[0].getValue('custbody_total_weight')!=null && orderTypeSearchResult[0].getValue('custbody_total_weight')!='') 					
							{
								totalWeight=totalWeight+parseFloat(orderTypeSearchResult[0].getValue('custbody_total_weight'));
							}

							nlapiLogExecution('Debug', 'Time Stamp at the start of TransformRecord',TimeStampinSec());
							TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');
							nlapiLogExecution('Debug', 'Time Stamp at the end of TransformRecord',TimeStampinSec());
						}
						catch(e) {


							var exceptionname='SalesOrder';
							var functionality='Creating Item Fulfillment';
							var trantype=2;
							var reference2="";
							var reference3="";
							var reference4 ="";
							var reference5 ="";
							var alertType=1;//1 for exception and 2 for report
							var exceptiondetails=e;
							if ( e instanceof nlobjError )
							{
								nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
								exceptiondetails=e.getCode() + '\n' + e.getDetails();
							}
							else
							{
								exceptiondetails=e.toString();
								nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
							}
							errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
							UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
							InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
							nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		
						}
					}
				}
				else if(trantype=="vendorreturnauthorization")
				{
					try
					{
						nlapiLogExecution('Debug', 'from to', 'from to');
						TransformRec = nlapiTransformRecord('vendorreturnauthorization', SalesOrderInternalId, 'itemfulfillment');
					}

					catch(e) {

						var exceptionname='vendorreturnauthorization';
						var functionality='Creating Item Fulfillment';
						var trantype=2;
						var reference2="";
						var reference3="";
						var reference4 ="";
						var reference5 ="";
						var alertType=1;//1 for exception and 2 for report
						var exceptiondetails=e;
						if ( e instanceof nlobjError )
						{
							nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
							exceptiondetails=e.getCode() + '\n' + e.getDetails();
						}
						else
						{
							exceptiondetails=e.toString();
							nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
						}
						errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
						InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
						UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
						nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		

					}
				}
				else
				{
					try
					{
						nlapiLogExecution('Debug', 'from to', 'from to');
						TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment');
					}

					catch(e) {

						var exceptionname='TransferOrder';
						var functionality='Creating Item Fulfillment';
						var trantype=2;
						var reference2="";
						var reference3="";
						var reference4 ="";
						var reference5 ="";
						var alertType=1;//1 for exception and 2 for report
						var exceptiondetails=e;
						if ( e instanceof nlobjError )
						{
							nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
							exceptiondetails=e.getCode() + '\n' + e.getDetails();
						}
						else
						{
							exceptiondetails=e.toString();
							nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
						}
						errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
						InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
						UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
						nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		
					}
				}

				var itemnoarr = new Array();
				for(var q=0; q<filterLineArray.length;q++ ) // This array contains all disctinct lines no for the order
				{
					if(TransformRec!=null)
					{
						var SOItemLength = TransformRec.getLineItemCount('item');
						for(var vcount=1;vcount<=SOItemLength;vcount++)
						{
							var itemLinevalue = TransformRec.getLineItemValue('item', 'line', vcount);
							if(itemLinevalue==filterLineArray[q])
							{		
								var vitemno = TransformRec.getLineItemValue('item', 'item', vcount);
								itemnoarr.push(vitemno);
								break;
							}
						}
					}
				}

				var memberitemsarr = new Array();
				if(itemnoarr!=null && itemnoarr!='')
				{
					memberitemsarr = getMemberItemsDetails(itemnoarr);
				}
				var IsBoolean=new Array();
				var vTobeConfItemCount=0;

				for(var i=0; i<filterLineArray.length;i++ ) // This array contains all disctinct lines no for the order
				{ 
					var context = nlapiGetContext();
					nlapiLogExecution('Debug','Remaining usage at the start of Item fulfillment line# ' + i,context.getRemainingUsage());

					var actuvalqty="";
					var skunumber="";
					var linenumber="";
					var totalqty=0;	
					var ItemType="";
					var batchno="";
					var serialno="";var serialout="";
					var filterlinenumber=filterLineArray[i];
					//By Ganesh for batch# entry
					var vPrevbatch;
					var vPrevLine;
					var vLotNo;
					var VLotNo1;
					var vLotQty=0;
					var vLotTotQty=0;
					var boolfound = false;
					var serialsspaces = "";
					var itemname ='';
					var itemvalue;
					var vLotonly;
					var IsLineClosed="F";
					var KitIsPicked='F';
					if(TransformRec!=null)
					{
						//Case # 20126922ï¿½Start
						if(LoadSalesOrderRec !=null && LoadSalesOrderRec!='')
						{
							IsLineClosed = LoadSalesOrderRec.getLineItemValue('item', 'isclosed', filterlinenumber);
						}
						itemname = TransformRec.getLineItemText('item', 'item', i+1);
						itemvalue = TransformRec.getLineItemValue('item', 'item', i+1);
						/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
						for(var y=0; y<itemListArray.length;y++) // This array contails all the data for the order
						{ 
							var vlinenumber =itemListArray[y][2];						
							if(filterlinenumber==vlinenumber)
							{
								itemvalue =itemListArray[y][6];
								itemname =itemListArray[y][5];
								if(itemvalue==null || itemvalue=='')
									itemvalue=itemListArray[y][7];
								itemname=itemListArray[y][8];
							}
						}	
					}
					var str = 'itemvalue. = ' + itemvalue + '<br>';
					str = str + 'itemname. = ' + itemname + '<br>';	
					str = str + 'IsLineClosed. = ' + IsLineClosed + '<br>';	

					nlapiLogExecution('DEBUG', 'Item Parameters', str);//

					/***upto here ***/			
					if(itemname!=null && itemname!=''&&IsLineClosed!="T")
					{
						itemname=itemname.replace(/ /g,"-");

						var itemTypesku = nlapiLookupField('item', itemvalue, 'recordType');

						if(itemTypesku=='kititem')
						{
							//Code added on 5th Sep 2013 by Suman.
							//case# 20127164 starts (FULFILLMENTATORDERLEVEL=='F' is changed to FULFILLMENTATORDERLEVEL!='Y')
							if(FULFILLMENTATORDERLEVEL!='Y')
								KitIsPicked=ISKitItemcompletlyPicked(itemvalue,vebizOrdNo,vWMSCarrier);
							//nlapiLogExecution("ERROR","KitIsPicked",KitIsPicked);

							if(KitIsPicked=="true"||FULFILLMENTATORDERLEVEL=='Y')
							{
								//case# 20127164 end
								//End of code as of 5th Sep.
								/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
								var vKitItemArr=new Array();
								var vKitItemQtyArr=new Array();
								var vKitMemberItemQtyArr=new Array();
								var vKitItemBatch=new Array();
								/***upto here ***/
								for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
								{ 
									linenumber =itemListArray[n][2];


									if(memberitemsarr!=null && memberitemsarr!='')
									{
										//nlapiLogExecution('Debug', 'memberitemsarr length', memberitemsarr.length);

										for(var w=0; w<memberitemsarr.length;w++) 
										{
											fulfilmentItem = memberitemsarr[w].getValue('memberitem');
											memberitemqty = memberitemsarr[w].getValue('memberquantity');

											if((filterlinenumber==linenumber && itemListArray[n][1]==fulfilmentItem))
											{
												QuilfiedItemArray.push(itemvalue);
												QuilfiedItemLineArray.push(linenumber);
												QuilfiedItemLineKitArray.push(linenumber);

												skunumber=itemListArray[n][1];

												var columns;
												batchno =itemListArray[n][3];

												if(serialno=="")
												{
													serialno =itemListArray[n][4];}
												else
												{ 
													serialno =serialno+","+itemListArray[n][4];
												}

												if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
												{
													vPrevbatch=itemname;
													itemListArray[n][3] = itemname;
													vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
												}

												if(vAdvBinManagement==true)
												{
													if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem")
													{
														nlapiLogExecution('Debug', 'Using vAdvBinManagement2',vAdvBinManagement);
														/*var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
														compSubRecord.selectNewLineItem('inventoryassignment');*/
														var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
														if(compSubRecord !=null && compSubRecord!='')
														{
															compSubRecord.selectLineItem('inventoryassignment', 1);
														}
														else
														{
															compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
															compSubRecord.selectNewLineItem('inventoryassignment');
														}
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', itemListArray[n][0]);
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][4]);

														compSubRecord.commitLineItem('inventoryassignment');
														compSubRecord.commit();
													}
												}
												/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
												if (boolfound) {
													if(vKitItemArr.indexOf(fulfilmentItem)!= -1)
													{
														vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]= parseFloat(vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]) + parseFloat(itemListArray[n][0]);

													}
													else
													{	
														vKitItemArr.push(fulfilmentItem);
														vKitItemQtyArr.push(itemListArray[n][0]);
														vKitMemberItemQtyArr.push(memberitemqty);
														vKitItemBatch.push(itemListArray[n][3]);
													}
													totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);
													/***Upto here***/

													vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
													serialsspaces += " " + itemListArray[n][4];

													if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
													{
														if(itemListArray[n][0] != null && itemListArray[n][0] != "")
															vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
													}
													else
													{ 
														if(vLotNo!=null && vLotNo != "")
															vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
														else
															vLotNo = vPrevbatch +"(" + vLotQty + ")";



														if(itemListArray[n][0] != null && itemListArray[n][0] != "")
															vLotQty=parseFloat(itemListArray[n][0]);
														vPrevbatch=itemListArray[n][3];
														vPrevLine=linenumber;
														nlapiLogExecution('Debug', 'vPrevLine',vPrevLine);
//														if(vAdvBinManagement)
//														{
//														try
//														{
//														if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//														{
//														nlapiLogExecution('Debug', 'Using vAdvBinManagement3',vAdvBinManagement);
//														/*var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//														compSubRecord.selectNewLineItem('inventoryassignment');*/

//														var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
//														nlapiLogExecution('Debug', 'compSubRecord1',compSubRecord);
//														if(compSubRecord !=null && compSubRecord!='')
//														{
//														nlapiLogExecution('Debug', 'compSubRecord1','compSubRecord1');
//														compSubRecord.selectLineItem('inventoryassignment', vPrevLine);
//														}
//														else
//														{
//														nlapiLogExecution('Debug', 'compSubRecord2','compSubRecord2');
//														compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//														compSubRecord.selectNewLineItem('inventoryassignment');
//														}
//														nlapiLogExecution('Debug', 'vLotQty',vLotQty);
//														nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);
//														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
//														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

//														compSubRecord.commitLineItem('inventoryassignment');
//														compSubRecord.commit();
//														//New code end

//														}
//														}
//														catch(exp2)
//														{
//														nlapiLogExecution('Debug', 'Exception in Advance Bin Management',exp2);
//														}
//														}
													}

												} else {
													if(skunumber!= null && skunumber != "")
													{
														//nlapiLogExecution('Debug', 'skunumber', skunumber);
														columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
														ItemType = columns.recordType;
														//nlapiLogExecution('Debug', 'ItemType in else', ItemType);
													}
													/***Below code is merged from Factory mation production account on 2nd Mar 2013 by Ganesh***/
													vKitItemArr.push(fulfilmentItem);
													vKitItemQtyArr.push(itemListArray[n][0]);
													vKitMemberItemQtyArr.push(memberitemqty);
													vKitItemBatch.push(itemListArray[n][3]);
													totalqty = parseFloat(itemListArray[n][0]);
													//nlapiLogExecution('Debug', 'totalqty10', totalqty);
													//totalqty = Math.floor(parseFloat(itemListArray[n][0])/memberitemqty);
													/***Upto here***/
													vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
													serialsspaces = itemListArray[n][4];
													if(itemListArray[n][0] != null && itemListArray[n][0] != "")
														vLotQty=parseFloat(itemListArray[n][0]);
													vPrevbatch=itemListArray[n][3];
													vPrevLine=linenumber;
													nlapiLogExecution('Debug', 'vPrevLine',vPrevLine);
//													if(vAdvBinManagement)
//													{
//													try
//													{
//													if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//													{
//													nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
//													/*var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//													compSubRecord.selectNewLineItem('inventoryassignment');*/

//													var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
//													nlapiLogExecution('Debug', 'compSubRecord',compSubRecord);
//													if(compSubRecord !=null && compSubRecord!='')
//													{
//													nlapiLogExecution('Debug', 'test1','test1');
//													compSubRecord.selectLineItem('inventoryassignment', vPrevLine);
//													}
//													else
//													{
//													nlapiLogExecution('Debug', 'test2','test2');
//													compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//													compSubRecord.selectNewLineItem('inventoryassignment');
//													}
//													nlapiLogExecution('Debug', 'vLotQty',vLotQty);
//													nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);
//													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
//													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

//													compSubRecord.commitLineItem('inventoryassignment');
//													compSubRecord.commit();
//													//New code end

//													}
//													}
//													catch(exp2)
//													{
//													nlapiLogExecution('Debug', 'Exception in Advance Bin Management',exp2);
//													}
//													}
												}
												boolfound = true;
												IsBoolean.push("T");
											}
										}
										/***Below code is merged from Factory mation production on 2nd Mar 2013 by Ganesh  as part of standard bundle***/
									}
								}
							}
							if(boolfound==true)
							{
								if(vKitItemQtyArr != null && vKitItemQtyArr != '' && vKitItemQtyArr.length>0)
								{
									totalqty=Math.floor(parseFloat(vKitItemQtyArr[0])/parseFloat(vKitMemberItemQtyArr[0]));
									//nlapiLogExecution('Debug', 'totalqty11', totalqty);
								}	
							}/***Upto here***/
						}
						else
						{
							for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
							{ 
								linenumber =itemListArray[n][2];

								if((filterlinenumber==linenumber))
								{
									QuilfiedItemArray.push(itemvalue);
									QuilfiedItemLineArray.push(linenumber);
									skunumber=itemListArray[n][1];

									var columns;
									batchno =itemListArray[n][3];

									if(serialno=="")
									{
										serialno =itemListArray[n][4];}
									else
									{ 
										serialno =serialno+","+itemListArray[n][4];
									}

									if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
									{
										vPrevbatch=itemname;
										itemListArray[n][3] = itemname;
										vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
									}

									columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
									ItemType = columns.recordType;

									if (boolfound) {
										totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);

										vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
										serialsspaces += " " + itemListArray[n][4];

										if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
										{
											if(itemListArray[n][0] != null && itemListArray[n][0] != "")
												vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
										}
										else
										{ 
											if(vLotNo!=null && vLotNo != "")
												vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
											else
												vLotNo = vPrevbatch +"(" + vLotQty + ")";
											/***Below code is merged form Lexjet production by Ganesh  on 5th Mar 2013***/
//											if(vAdvBinManagement)
//											{
//											if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//											{

//											nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);
//											TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment');
//											nlapiLogExecution('Debug', 'Using vAdvBinManagement5',vAdvBinManagement);
//											nlapiLogExecution('Debug', 'test1','test1');
//											nlapiLogExecution('Debug', 'TransformRec',TransformRec);

//											var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//											nlapiLogExecution('Debug', 'test2','test2')
//											compSubRecord.selectNewLineItem('inventoryassignment');
//											nlapiLogExecution('Debug', 'Using vLotQty',vLotQty);
//											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
//											nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);
//											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

//											compSubRecord.commitLineItem('inventoryassignment');
//											compSubRecord.commit();
//											nlapiLogExecution('Debug', 'End','END');
//											//New code end

//											}
//											}
											/***Upto here***/
											if(itemListArray[n][0] != null && itemListArray[n][0] != "")
												vLotQty=parseFloat(itemListArray[n][0]);
											vPrevbatch=itemListArray[n][3];
											vPrevLine=linenumber;
										}

									} else {
										if(skunumber!= null && skunumber != "")
										{
											columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
											ItemType = columns.recordType;
										}
										totalqty = parseFloat(itemListArray[n][0]);
										vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
										serialsspaces = itemListArray[n][4];

										if(itemListArray[n][0] != null && itemListArray[n][0] != "")
											vLotQty=parseFloat(itemListArray[n][0]);
										vPrevbatch=itemListArray[n][3];
										vPrevLine=linenumber;
									}
									boolfound = true;
									IsBoolean.push("T");
								}
							}
						}

						var SOLength=0;
						var itemIndex=0;

						if(TransformRec!=null)
						{
							SOLength = TransformRec.getLineItemCount('item');  // To get the no of lines from item fulfillment record
							nlapiLogExecution('Debug', "SO Length", SOLength);    

							var SOWMSCarrier = TransformRec.getFieldValue('custbody_salesorder_carrier');
							for (var j = 1; j <= SOLength; j++) {

								var item_id = TransformRec.getLineItemValue('item', 'item', j);
								// To get Item Type
								//Below code commented for governance issue and this is code is not using
								//var columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
								var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
								var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);
								if(trantype=="transferorder"){
									itemLineNo=parseFloat(itemLineNo)-1;
								}
								var NSUOM = TransformRec.getLineItemValue('item', 'units', j); 
								if (itemLineNo == filterlinenumber) {
									itemIndex=j;  
								}
							}
						}
						if(itemIndex!=0 && totalqty!=null && totalqty!='')
						{
							var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
							var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
							var itemLineNo = TransformRec.getLineItemValue('item', 'line', itemIndex);
							if(itemname!=null && itemname!='')
								itemname=itemname.replace(/ /g,"-");
							var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
							var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);

							var vparentitemtype = nlapiLookupField('item', item_id, 'recordType');

							var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);

							var veBizUOMQty=1;
							var vBaseUOMQty=1;
							if(NSOrdUOM!=null && NSOrdUOM!="")
							{
								if(vItemDimsArr!=null && vItemDimsArr.length>0)
								{
									for(z=0; z < vItemDimsArr.length; z++)
									{
										if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
										{	
											if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
											{
												vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
												nlapiLogExecution('Debug', 'vBaseUOMQty', vBaseUOMQty);
											}

											if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
											{
												nlapiLogExecution('Debug', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
												veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
												nlapiLogExecution('Debug', 'veBizUOMQty', veBizUOMQty);
											}
										}
									}
									if(veBizUOMQty==null || veBizUOMQty=='')
									{
										veBizUOMQty=vBaseUOMQty;
									}
									if(veBizUOMQty!=0)
									{ //Case# 20123228 start: to accept decimal qty
										//totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
										totalqty = (parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty);
										//Case# 20123228 end:
									}
									nlapiLogExecution('Debug', 'totalqty', totalqty);

									vLotQty =totalqty;
								}
							}

							if (boolfound) {
								nlapiLogExecution('Debug', 'confirmLotToNS',confirmLotToNS);
								if(confirmLotToNS=='N')
									vPrevbatch=itemname;				
								nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);


								if(vLotNo!=null && vLotNo != "")
									vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
								else
									vLotNo = vPrevbatch +"(" + vLotQty + ")";
								//Case # 20126405   start
								TransformRec.selectLineItem('item', itemIndex);
								TransformRec.setCurrentLineItemValue('item', 'quantity', totalqty);
								//Case # 20126405   End
								if(vAdvBinManagement)
								{
									try
									{
										nlapiLogExecution('Debug', 'Into Advance Bin Management lot item', ItemType);

										if (vparentitemtype == "lotnumberedinventoryitem" || vparentitemtype == "lotnumberedassemblyitem")
										{
											/***Below code is merged from Lexjet production on 5th march by Ganesh ***/
											if(itemListArray!=null && itemListArray!='')
											{
												nlapiLogExecution('Debug', 'itemListArray Length', itemListArray.length);

												//var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');//
												//var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
												var partialbatch;
												var totallotqty=0;
												var subrecordcount=0;
												var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
												var vinvtdetailsLineCount=1;
												for(var i1=0;i1<itemListArray.length;i1++)
												{										
													var vNextBatch="";
													if(i1+1 <itemListArray.length)
														vNextBatch=itemListArray[i1+1][3];
													else
														vNextBatch=itemListArray[i1][3];
													var opentaskitemid = itemListArray[i1][6];

													nlapiLogExecution('Debug', 'opentaskitemid', opentaskitemid);
													nlapiLogExecution('Debug', 'soitemid', item_id);
													nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
													nlapiLogExecution('Debug', 'Locallinenumber', Locallinenumber);
													var Locallinenumber =itemListArray[i1][2];
													if(opentaskitemid==item_id && filterlinenumber == Locallinenumber )
													{
														var itemTypesku = nlapiLookupField('item', item_id, 'recordType');
														if(itemTypesku=='kititem')
														{	
															for(var k1=0;k1<vKitItemArr.length;k1++)
															{
																if((compSubRecord !=null && compSubRecord!='') )
																{
																	nlapiLogExecution('EMERGENCY', 'inside','edit');
																	subrecordcount=compSubRecord.getLineItemCount('inventoryassignment');
																	//compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
																	//compSubRecord.selectNewLineItem('inventoryassignment');
																	nlapiLogExecution('EMERGENCY', 'vinvtdetailsLineCount',vinvtdetailsLineCount);
																	nlapiLogExecution('EMERGENCY', 'subrecordcount',subrecordcount);
																	if(parseInt(vinvtdetailsLineCount)>=parseInt(subrecordcount))
																	{
																		compSubRecord.selectNewLineItem('inventoryassignment');
																	}
																	else
																	{
																		compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
																	}
																}
																else if(compSubRecord ==null || compSubRecord=='')
																{
																	nlapiLogExecution('EMERGENCY', 'inside2','create');
																	compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
																	compSubRecord.selectNewLineItem('inventoryassignment');
																}
																else if(compSubRecord !=null && compSubRecord!='')
																{
																	nlapiLogExecution('EMERGENCY', 'inside3','create new sub line');
																	subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
																	if(parseInt(vinvtdetailsLineCount)>parseInt(subrecordcount))
																		compSubRecord.selectNewLineItem('inventoryassignment');
																	else
																		compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
																}
																nlapiLogExecution('EMERGENCY', 'qty', vKitItemQtyArr[k1]);
																nlapiLogExecution('EMERGENCY', 'batch', vKitItemBatch[k1]);
																compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vKitItemQtyArr[k1]);

																compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vKitItemBatch[k1]);

																compSubRecord.commitLineItem('inventoryassignment');
															}
														}
														else
														{
															nlapiLogExecution('EMERGENCY', 'item_id', item_id);
															nlapiLogExecution('EMERGENCY', 'filterlinenumber', filterlinenumber);
															/*var subrecordtype='edit';
														var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
														if(compSubRecord !=null && compSubRecord!='')
														{
															//compSubRecord.selectLineItem('inventoryassignment', 1);
														}
														else
														{
															compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
															//compSubRecord.selectNewLineItem('inventoryassignment');
														}
															 */
															if(itemListArray[i1][0] != null && itemListArray[i1][0] != "")
																vLotQty=parseFloat(itemListArray[i1][0]);
															vPrevbatch=itemListArray[i1][3];
															nlapiLogExecution('EMERGENCY', 'partialbatch', partialbatch);
															nlapiLogExecution('EMERGENCY', 'vPrevbatch', vPrevbatch);
															if(partialbatch!=vPrevbatch)
															{
																totallotqty=vLotQty;
																partialbatch=itemListArray[i1][3];
															}
															else if(partialbatch==vPrevbatch)
															{
																totallotqty=parseFloat(totallotqty)+parseFloat(vLotQty);
															}
															else
															{
																totallotqty=vLotQty;
																//partialbatch=itemListArray[i1][3];
																//totallotqty=parseFloat(totallotqty)+parseFloat(vLotQty);
															}

//															if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//															{
//															nlapiLogExecution('Debug', 'Using vAdvBinManagement1',vLotQty);
//															nlapiLogExecution('Debug', 'Using vPrevbatch',vPrevbatch);


															//compSubRecord.selectNewLineItem('inventoryassignment');
															//compSubRecord.selectLineItem('inventoryassignment', 1);

															/*if(subrecordtype='edit')
														{
															var indx=parseInt(i1)+1;
															compSubRecord.selectLineItem('inventoryassignment', 1);
														}
														else
														{
															compSubRecord.selectNewLineItem('inventoryassignment');
														}*/
															nlapiLogExecution('EMERGENCY', 'vinvtdetailsLineCount',vinvtdetailsLineCount);

															//var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
															if((compSubRecord !=null && compSubRecord!='') && (partialbatch==vPrevbatch))
															{
																nlapiLogExecution('EMERGENCY', 'inside','edit');
																subrecordcount=compSubRecord.getLineItemCount('inventoryassignment');
																//compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
																//compSubRecord.selectNewLineItem('inventoryassignment');
																nlapiLogExecution('EMERGENCY', 'vinvtdetailsLineCount',vinvtdetailsLineCount);
																nlapiLogExecution('EMERGENCY', 'subrecordcount',subrecordcount);
																if(parseInt(vinvtdetailsLineCount)>parseInt(subrecordcount))
																{
																	compSubRecord.selectNewLineItem('inventoryassignment');
																}
																else
																{
																	compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
																}

															}
															else if(compSubRecord ==null || compSubRecord=='')
															{
																nlapiLogExecution('EMERGENCY', 'inside2','create');
																compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
																compSubRecord.selectNewLineItem('inventoryassignment');
															}
															else if(compSubRecord !=null && compSubRecord!='')
															{
																nlapiLogExecution('EMERGENCY', 'inside3','create new sub line');
																subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
																if(parseInt(vinvtdetailsLineCount)>parseInt(subrecordcount))
																	compSubRecord.selectNewLineItem('inventoryassignment');
																else
																	compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
															}

															if(NSOrdUOM!=null && NSOrdUOM!="")
															{
																var vUOMlotqty=0;
																nlapiLogExecution('Debug', 'veBizUOMQty',veBizUOMQty);
																nlapiLogExecution('Debug', 'vBaseUOMQty',vBaseUOMQty);
																if(veBizUOMQty!=0 && vBaseUOMQty!=0)
																{ 
																	//vUOMlotqty = (parseFloat(vLotQty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty);
																	vUOMlotqty = (parseFloat(totallotqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty);
																	nlapiLogExecution('Debug', 'vUOMlotqty',vUOMlotqty);
																	if(vUOMlotqty !=null && vUOMlotqty!='' && vUOMlotqty !=0)
																	{																
																		compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vUOMlotqty);
																	}
																}
															}
															else
															{
																nlapiLogExecution('EMERGENCY', 'vLotQty',vLotQty);
																nlapiLogExecution('EMERGENCY', 'vPrevbatch',vPrevbatch);	
																nlapiLogExecution('EMERGENCY', 'totallotqty',totallotqty);
																compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', totallotqty);

															}
															compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);
														}

														compSubRecord.commitLineItem('inventoryassignment');
														//	compSubRecord.commit();
														nlapiLogExecution('EMERGENCY', 'after','submit');
														if(vNextBatch !=vPrevbatch)
															vinvtdetailsLineCount++;
													}
												}

												compSubRecord.commit();
											}
											/***upto here***/
											//New code end
										}

									}
									catch(exp2)
									{
										nlapiLogExecution('Debug', 'Error in Advance Bin Management',exp2);
									}
								}

								if(vAdvBinManagement)
								{
									//case # 20140052  start.....if condition was closed improperly.
									if (vparentitemtype == "serializedinventoryitem" || vparentitemtype == "serializedassemblyitem")
									{
										var filters= new Array();	
										filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
										filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
										filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
										//filters.push(new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'I'));
										var column =new Array(); 

										column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

										var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
										if(searchresultser!=null && searchresultser!='')
										{
											var subrecordtype= 'edit';
//											var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
											var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
											if(compSubRecord ==null || compSubRecord == '' || compSubRecord == 'null')
											{												
												compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
												subrecordtype='insert';
											}
											//	for ( var b = 0; b < searchresultser.length; b++ ) {//case # 20127174? 
											for ( var b = 0; b < totalqty; b++ ) {
												serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
												var seriid ;
												var vfilters= new Array();				

												vfilters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
												vfilters.push(new nlobjSearchFilter('custrecord_serialnumber', null,'is', serialsspaces));
												vfilters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
												filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
												var vcolumn =new Array(); 
												vcolumn[0] = new nlobjSearchColumn('custrecord_serialstatus');							
												var vsearchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,vfilters,vcolumn);
												if(vsearchresultser!=null && vsearchresultser!='')
												{
													seriid = vsearchresultser[0].getId();
												}

												var trnserial = nlapiLoadRecord('customrecord_ebiznetserialentry', seriid);
												trnserial.setFieldValue('custrecord_serialstatus', 'I');	
												var retval  =nlapiSubmitRecord(trnserial, true);

//												nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
//												nlapiLogExecution('Debug', 'Using totalqty',totalqty);
//												nlapiLogExecution('Debug', 'Using serialsspaces',serialsspaces);

												//compSubRecord.selectNewLineItem('inventoryassignment');
												if(subrecordtype =='edit')
												{
													var indx=parseInt(b)+1;
													compSubRecord.selectLineItem('inventoryassignment', indx);
												}
												else
												{
													compSubRecord.selectNewLineItem('inventoryassignment');
												}
												//Case # 20126405 Start
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
												//Case # 20126405 End
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serialsspaces);

												compSubRecord.commitLineItem('inventoryassignment');
											}
											compSubRecord.commit();
										}	
									}
								}
								TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
								nlapiLogExecution('EMERGENCY', 'totalqty', totalqty);
								TransformRec.setCurrentLineItemValue('item', 'quantity', parseFloat(totalqty).toFixed(5));
								//nlapiLogExecution('Debug', 'itemloc2', itemloc2);
								TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

								//nlapiLogExecution('Debug', 'ItemType', ItemType);
								if(!vAdvBinManagement)
								{	
									if (vparentitemtype == "lotnumberedinventoryitem") {
										//nlapiLogExecution('Debug', 'vLotNo', vLotNo);									
										TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
										//nlapiLogExecution('Debug', 'vBatchno', vBatchno);
									}
									else if(vparentitemtype == "lotnumberedassemblyitem") 
									{
										TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
										//nlapiLogExecution('Debug', 'vLotNo', vLotNo);
									}
									else if (vparentitemtype == "serializedinventoryitem" || vparentitemtype == "serializedassemblyitem") {
										var serialsspaces = "";
										if(serialsspaces==null || serialsspaces=='')
										{
											var filters= new Array();				
											//nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
											filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
											filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
											filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
											var column =new Array(); 

											column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

											var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
											if(searchresultser!=null && searchresultser!='')
											{
												for ( var b = 0; b < searchresultser.length; b++ ) {
													if(serialsspaces==null || serialsspaces=='')
													{
														serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
													}
													else
													{
														serialsspaces += " " + searchresultser[b].getValue('custrecord_serialnumber');
													}
												}
											}									
											//nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);
										}
										//nlapiLogExecution('Debug', 'before serialsspaces', serialsspaces);
										TransformRec.setCurrentLineItemValue('item','serialnumbers', serialsspaces);
									}
								}

								vLotNo="";
							}
							TransformRec.commitLineItem('item');

							//The below code is written by Satish.N on 03/23/2015 to post the itemfulfillment for Kit/Package items
							//if the components are lot controlled items and advance bin management is enabled.

							var str = 'vparentitemtype. = ' + vparentitemtype + '<br>';
							str = str + 'KitIsPicked. = ' + KitIsPicked + '<br>';	
							str = str + 'FULFILLMENTATORDERLEVEL. = ' + FULFILLMENTATORDERLEVEL + '<br>';	

							nlapiLogExecution('DEBUG', 'Parent Item Parameters', str);

							KitIsPicked='true';

							if((vparentitemtype=='kititem') && (KitIsPicked=="true"||FULFILLMENTATORDERLEVEL=='Y'))
							{								
								var vitemIndex=0;

								var vmemlotqtyarr = new Array();


								for(var p=0; p<itemListArray.length;p++) // This array contails all the data for the order
								{
									var lotfound='F';

									var vmainitem = itemListArray[p][6];
									var vcompItem=itemListArray[p][1];
									var vcompQty=itemListArray[p][0];
									var vcompLot=itemListArray[p][3];

									if(vmainitem==item_id)
									{
										var vcomparr=[vcompLot,vcompQty,vcompItem,vmainitem];

										nlapiLogExecution('DEBUG', 'vmemlotqtyarr', vmemlotqtyarr);

										if(vmemlotqtyarr!=null || vmemlotqtyarr!='')
										{
											for(var p2=0; p2<vmemlotqtyarr.length;p2++) // This array contails all the data for the order
											{
												if(vcompLot == vmemlotqtyarr[p2][0])
												{
													var vPrevLotQty=vmemlotqtyarr[p2][1];
													nlapiLogExecution('DEBUG','vPrevLotQty',vPrevLotQty);
													var totalLotQuantity = parseFloat(vPrevLotQty) + parseFloat(vcompQty);
													nlapiLogExecution('DEBUG','totalLotQuantity Exists',totalLotQuantity);
													vmemlotqtyarr[p2][1]=totalLotQuantity;

													lotfound='T';
												}											
											}
											if(lotfound=='F')
											{
												vmemlotqtyarr.push(vcomparr);
											}
										}
										else
										{
											vmemlotqtyarr.push(vcomparr);
										}
									}
								}

								nlapiLogExecution('DEBUG', 'vmemlotqtyarr final', vmemlotqtyarr);


								var membercomparr = getMemberItemsDetails(item_id);
								if(membercomparr!=null && membercomparr!='')
								{
									//for(var h=0; h<membercomparr.length;h++) // This array contails all the data for the order
									for(var h=0; h<vmemlotqtyarr.length;h++) // This array contails all the data for the order
									{
										var vparentitem = vmemlotqtyarr[h][3];
										var vmemberItem=vmemlotqtyarr[h][2];
										var vmemberQty=vmemlotqtyarr[h][1];
										var vmemberLot=vmemlotqtyarr[h][0];

										if(vparentitem==item_id)
										{
											for (var g = 1; g <= SOLength; g++) {

												var vitem_id = TransformRec.getLineItemValue('item', 'item', g);
												var vitemrec = TransformRec.getLineItemValue('item', 'itemreceive', g);
												var vitemLineNo = TransformRec.getLineItemValue('item', 'line', g);
												var parentlineno= TransformRec.getLineItemValue('item', 'kitmemberof', g);

												var str = 'parentlineno. = ' + parentlineno + '<br>';
												str = str + 'filterlinenumber. = ' + filterlinenumber + '<br>';	
												str = str + 'vmemberItem. = ' + vmemberItem + '<br>';	
												str = str + 'vitem_id. = ' + vitem_id + '<br>';	

												nlapiLogExecution('DEBUG', 'Child Item Parameters', str);//


												if (parentlineno == filterlinenumber && vmemberItem == vitem_id) {
													vitemIndex=g; 
													break;
												}
											}

											var str = 'vitemIndex. = ' + vitemIndex + '<br>';
											str = str + 'vmemberQty. = ' + vmemberQty + '<br>';	
											str = str + 'vmemberLot. = ' + vmemberLot + '<br>';	
											str = str + 'itemloc2. = ' + itemloc2 + '<br>';	

											nlapiLogExecution('DEBUG', 'Child Item Lot Parameters', str);//

											if(vitemIndex!=0 && vmemberQty!=null && vmemberQty!='')
											{
												TransformRec.selectLineItem('item', vitemIndex);
												TransformRec.setCurrentLineItemValue('item', 'quantity', vmemberQty);	
												TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

												if(vAdvBinManagement)
												{
													var childItemType=nlapiLookupField('item', vitem_id, 'recordType');

													if (childItemType == "lotnumberedinventoryitem" || childItemType == "lotnumberedassemblyitem")
													{												

														var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');		
														compSubRecord.selectNewLineItem('inventoryassignment');
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vmemberQty);																
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vmemberLot);										
														compSubRecord.commitLineItem('inventoryassignment');
														compSubRecord.commit();
													}
												}
												else
												{
													if (childItemType == "lotnumberedinventoryitem" || childItemType == "lotnumberedassemblyitem")
													{
														TransformRec.setCurrentLineItemValue('item','serialnumbers', vmemberLot);
													}
												}

												TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');

												TransformRec.commitLineItem('item');

											}
										}
									}
								}
							}

							/////////////////////////////////////////
						}
						else
						{
							nlapiLogExecution('Debug', 'itemIndex in else', itemIndex);


							if(itemIndex != 0)
							{
								TransformRec.selectLineItem('item', itemIndex);


								TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');


								TransformRec.commitLineItem('item');
							}
						}
					}
				}
				//Case# 20124622 Start//Issue Fix for the so having lines with different site then itemfulfillment was failing if we generate the wave for single site
				var SOLengthnew;
				if(TransformRec!=null && TransformRec!='')
					SOLengthnew = TransformRec.getLineItemCount('item');

				if(parseInt(filterLineArray.length)!=parseInt(SOLengthnew))
				{
					for(var k1=1;k1<=SOLengthnew;k1++)
					{
						var isLinenoComitted=false;
						var linenum= TransformRec.getLineItemValue('item', 'line', k1);
						for(var k2=0;k2<filterLineArray.length;k2++)
						{
							var filterlineno=filterLineArray[k2];
							if(parseInt(linenum)==parseInt(filterlineno))
							{
								isLinenoComitted=true;
								break;
							}
						}
						if(isLinenoComitted==false)
						{
							nlapiLogExecution('Debug', 'itemIndex in', k1);
							TransformRec.selectLineItem('item', k1);

							TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

							TransformRec.commitLineItem('item');
						}
					}
				}
				//Case# 20124622 End

				//nlapiLogExecution('Debug', 'itemloc', itemloc);
				nlapiLogExecution('ERROR', 'Before Submit', IsBoolean);
				nlapiLogExecution('ERROR', 'ISBoolean.indexOf(T)', IsBoolean.indexOf("T"));
				if(parseInt(IsBoolean.indexOf("T"))!=parseInt(-1))
				{
					nlapiLogExecution('ERROR', "INTOIF");
					boolfound=true;
				}
				nlapiLogExecution('ERROR', 'boolfound', boolfound);
				nlapiLogExecution('ERROR', 'TransformRec', TransformRec);

				if(TransformRec!=null && boolfound==true)
				{
					nlapiLogExecution('Debug', 'Time Stamp at the start of nlapiSubmitRecord',TimeStampinSec());
					try
					{
						TransformRecId = nlapiSubmitRecord(TransformRec, true);
						nlapiLogExecution('Debug', 'Time Stamp at the end of nlapiSubmitRecord',TimeStampinSec());
					}
					catch(e)
					{
						var exceptionname='SalesOrder';
						var functionality='Creating Item Fulfillment';
						var trantype=2;
						var reference2="";
						var reference3="";
						var reference4 ="";
						var reference5 ="";
						var alertType=1;//1 for exception and 2 for report
						var exceptiondetails=e;
						if ( e instanceof nlobjError )
						{
							nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
							exceptiondetails=e.getCode() + '\n' + e.getDetails();
						}
						else
						{
							exceptiondetails=e.toString();
							nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
						}
						InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);

						var ErrorCode=e.getCode();
						var vErrorMsg=e.message;
						UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
					}
					nlapiLogExecution('ERROR', 'After Submit', TransformRecId);

					/*for ( var i = 0; i < searchresult.length; i++ ) {
					//nlapiLogExecution('ERROR', 'searchresult[i].getId()', searchresult[i].getId());
					//nlapiLogExecution('ERROR', 'TransformRecId', TransformRecId);
					nlapiSubmitField(searchresult[i].getRecordType(), searchresult[i].getId(), 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
				}*/
					//Code added on 5th Sep 2013 by suman.
					nlapiLogExecution('ERROR', 'QuilfiedItemArray',QuilfiedItemArray);
					nlapiLogExecution('ERROR', 'QuilfiedItemLineArray',QuilfiedItemLineArray);
					if(QuilfiedItemArray!=null&&QuilfiedItemArray!=""&&QuilfiedItemArray.length!=0)
					{
						var QuilfiedItemArray=removeDuplicateElementOld(QuilfiedItemArray);
						var QuilfiedItemLineArray=removeDuplicateElementOld(QuilfiedItemLineArray);
						var QuilfiedItemLineKitArray=removeDuplicateElementOld(QuilfiedItemLineKitArray);
						UpdateAllOpenTaskRecordsWithNSID(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineArray,vcontainerLp,QuilfiedItemLineKitArray,FULFILLMENTATORDERLEVEL);
					}
					//End of code as of 5th Sep 2013.

					if(packtaskid!=null && packtaskid!='')
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', packtaskid, 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);

					if(TransformRecId !=null && TransformRecId !='')
						AutoPackingAfterfulfillment(SalesOrderInternalId,itemloc,vebizOrdNo,FULFILLMENTATORDERLEVEL,vcontainerLp);

					if(trantype=="salesorder" && TransformRecId !=null && TransformRecId !='' && totalWeight != null && totalWeight != '' && parseFloat(totalWeight) != 0 && totalWeight!=0.0 )
					{
						totalcube = roundNumber(totalcube, 2);

						var fields = new Array();
						var values = new Array();

						fields.push('custbody_total_weight');
						fields.push('custbody_total_vol');

						values.push(parseInt(totalWeight));
						values.push(parseFloat(totalcube));

						nlapiSubmitField('salesorder', SalesOrderInternalId, fields, values);	

						/***Below code is merged from Lexjet production on 5th march by Ganesh and commented ***/
						//UpdateShipmanifestCustomrecord(vebizOrdNo,wavenumber,TransformRecId);
						/***upto here***/

					}
				}
			}
		}
	}	
	return TransformRecId;
	nlapiLogExecution('Debug', 'Time Stamp at the end of createItemFulfillment',TimeStampinSec());
}

function getMemberItemsDetails(itemnoarr)
{
	nlapiLogExecution('Debug', 'Into getMemberItemsDetails', itemnoarr);

	var filters = new Array(); 			 
	filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', itemnoarr);	

	var columns1 = new Array(); 
	columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
	columns1[1] = new nlobjSearchColumn( 'memberquantity' );
	columns1[2] = new nlobjSearchColumn( 'itemid' ).setSort();

	var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 

	nlapiLogExecution('Debug', 'Out of getMemberItemsDetails', searchresults);

	return searchresults;

}

/***Below code is merged from Lexjet production on 5th march by Ganesh ***/
function UpdateShipmanifestCustomrecord(vebizOrdNo,wavenumber,TransformRecId)
{
	str = 'Shipmentwavenumber. = ' + wavenumber + '<br>';
	str = str + 'ShipmanifestvebizOrdNo. = ' + vebizOrdNo + '<br>';	
	str = str + 'TransformRecId. = ' + TransformRecId + '<br>';

	nlapiLogExecution('Debug', 'UpdateShipmanifestCustomrecord', str);

	var filters= new Array();					
	filters.push(new nlobjSearchFilter('custrecord_ship_ref3', null,'is', vebizOrdNo));				
	//filters.push(new nlobjSearchFilter('custrecord_ship_nsconf_no',  null,'isempty')); 
//	if(vWMSCarrier != null && vWMSCarrier != '')
//	filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier

	if(wavenumber!=null && wavenumber!='')
	{
		wavenumber=parseInt(wavenumber).toString();
		filters.push(new nlobjSearchFilter('custrecord_ship_ref4', null,'is', wavenumber));	
	}
	/*
	if(vcontainerLp!=null && vcontainerLp!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ship_contlp', null,'is', vcontainerLp));
	} 
	 */
	var columns=new Array();
	columns[0]=new nlobjSearchColumn('custrecord_ship_nsconf_no');
	columns[1]=new nlobjSearchColumn('custrecord_ship_ref5');
	columns[2]=new nlobjSearchColumn('custrecord_ship_void');
	columns[2]=new nlobjSearchColumn('custrecord_ship_ref3');
	columns[3]=new nlobjSearchColumn('custrecord_ship_masttrackno');

	var shipmanifestSearchresult = nlapiSearchRecord('customrecord_ship_manifest',null,filters,columns);

	nlapiLogExecution('Debug', 'TransformRecId', TransformRecId);
	if(shipmanifestSearchresult!=null && shipmanifestSearchresult!='')
	{
		for ( var i = 0; i < shipmanifestSearchresult.length; i++ ) 
		{
			var shipmentinternalid=shipmanifestSearchresult[i].getId();
			nlapiLogExecution('Debug', 'updatefieldId', shipmentinternalid);
//			var UpdateShipmanifestRecord= nlapiLoadRecord('customrecord_ship_manifest', shipmentinternalid);
//			UpdateShipmanifestRecord.setFieldValue('custrecord_ship_nsconf_no',TransformRecId);
//			UpdateShipmanifestRecord.setFieldValue('custrecord_ship_ref3',shipmanifestSearchresult[i].getValue('custrecord_ship_ref3'));
//			UpdateShipmanifestRecord.setFieldValue('custrecord_ship_ref5',shipmanifestSearchresult[i].getValue('custrecord_ship_ref5'));
//			var id = nlapiSubmitRecord(UpdateShipmanifestRecord, true);


			nlapiSubmitField(shipmanifestSearchresult[i].getRecordType(), shipmentinternalid, 'custrecord_ship_nsconf_no', TransformRecId);
			//nlapiLogExecution('Debug', 'id', id);
		}
	}

}
/***upto here***/

function CreatePackListCheckbyWmsStatusFlag(vebizOrdNo,getCartonLPNo,getFONO,printername,wavenumber,salesorderdetails,trantype)
{
	nlapiLogExecution('Debug', 'Time Stamp at the start of CreatePackListCheckbyWmsStatusFlag',TimeStampinSec());
	nlapiLogExecution('Debug', 'salesorderdetails',salesorderdetails);
	if(salesorderdetails!=null && salesorderdetails!='')
	{
		location=salesorderdetails.getFieldValue('location');

		if(location!=null && location!='')
		{
			locationRecord = nlapiLoadRecord('location', location);
		}
	}
	try
	{
		var filterArray = new Array();	 
		if(vebizOrdNo!= null && vebizOrdNo!= '')
		{	 
			filterArray.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vebizOrdNo));
		}
		filterArray.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
		filterArray.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8','9']));
		filterArray.push(new nlobjSearchFilter('name', null,'is', getFONO));
		filterArray.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
		filterArray.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0.0'));
		filterArray.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		var columnsArray = new Array();
		columnsArray[0] = new nlobjSearchColumn('custrecord_act_qty');
		columnsArray[1] = new nlobjSearchColumn('custrecord_line_no');
		columnsArray[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
		var OpentaskPacklsitSearchrecords = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filterArray,columnsArray);
		if(OpentaskPacklsitSearchrecords==null || OpentaskPacklsitSearchrecords=='')
		{
			var  opentaskarray = new Array();
			//nlapiLogExecution('Debug', 'Time Stamp at the start of Packlist',TimeStampinSec());
			//createpacklist(opentaskarray,vebizOrdNo,getFONO,printername,trantype,salesorderdetails,locationRecord,wavenumber);
			//nlapiLogExecution('Debug', 'Time Stamp at the END of Packlist',TimeStampinSec());
			nlapiLogExecution('Debug', 'Time Stamp at the start of GenerateCustumerSpecificSmallLabel',TimeStampinSec());
			GenerateCustumerSpecificSmallLabel(vebizOrdNo,salesorderdetails,trantype,getFONO);
			nlapiLogExecution('Debug', 'Time Stamp at the End of GenerateCustumerSpecificSmallLabel',TimeStampinSec());
		}
		var systemIntegrationRulevalue=getIntegrationSystemRuleValue("SHIP-UCCLabel", "");
		if(systemIntegrationRulevalue!=null && systemIntegrationRulevalue!="")
		{
			var systemrulevalue = systemIntegrationRulevalue[0];
			var systemruletype = systemIntegrationRulevalue[1];

			nlapiLogExecution('Debug', 'Generate UccLabel  Function for systemrulevalue + " " + systemruletype', systemrulevalue + " " + systemruletype);
			if (systemruletype == "BartenderFormat") 
			{

				if (systemrulevalue == "Standard")
				{
					var customAsnrequired="F";
					var CustTemplate="";
					var CustomerId=salesorderdetails.getFieldValue('entity');
					if(CustomerId != "" && CustomerId != null)
					{
						var fields = ['custentity_ebiz_asn_required','custentity_cust_template'];

				var columns = nlapiLookupField('customer',CustomerId,fields);
				customAsnrequired = columns.custentity_ebiz_asn_required;

					}
					nlapiLogExecution('DEBUG', 'customAsnrequired',customAsnrequired);
					if(customAsnrequired=="T")
						GenerateCustomUCCLabel(vebizOrdNo,getCartonLPNo,salesorderdetails,getFONO);
				}
				else if(systemrulevalue == "Custom")
				{
					try
					{

						GenerateCustumerSpecificUccLabel(vebizOrdNo,getCartonLPNo,salesorderdetails,getFONO);
					}
					catch(exp)
					{
						nlapiLogExecution('DEBUG', 'Expection in CustomerSpecific Ucc Label Generation',exp);
					}

				}
			}
			else if (systemruletype == "ZebraFormat") 
			{

				if (systemrulevalue == "Standard")
				{
					var customAsnrequired="F";
					var CustTemplate="";
					var CustomerId=salesorderdetails.getFieldValue('entity');
					if(CustomerId != "" && CustomerId != null)
					{
						var fields = ['custentity_ebiz_asn_required','custentity_cust_template'];

				var columns = nlapiLookupField('customer',CustomerId,fields);
				customAsnrequired = columns.custentity_ebiz_asn_required;

					}
					nlapiLogExecution('DEBUG', 'customAsnrequired',customAsnrequired);
					if(customAsnrequired=="T")
						GenerateZebraUccLabel(vebizOrdNo,getCartonLPNo,salesorderdetails,getFONO);

				}
				else if(systemrulevalue == "Custom")
				{
					try
					{
						GenerateCustomSpecificUccLabel(vebizOrdNo,getCartonLPNo,salesorderdetails,getFONO);
					}
					catch(exp)
					{

						nlapiLogExecution('DEBUG', 'Expection in CustomerSpecific Ucc Label Generation',exp);
					}

				}
			}

		}

		var systemIntegrationRulevalue=getIntegrationSystemRuleValue("SHIP-AddressLabel", "");
		if(systemIntegrationRulevalue!=null && systemIntegrationRulevalue!="")
		{
			var systemrulevalue = systemIntegrationRulevalue[0];
			var systemruletype = systemIntegrationRulevalue[1];
			nlapiLogExecution('Debug', 'Generate AddressLabel  Function for systemrulevalue + " " + systemruletype', systemrulevalue + " " + systemruletype);
			if (systemruletype == "BartenderFormat") 
			{

				if (systemrulevalue == "Standard")
				{
					GenerateZebraAddressLabel(vebizOrdNo,salesorderdetails);
				}
				else if(systemrulevalue == "Custom")
				{
					try
					{

						GenerateCustumerSpecificAddressLabel(vebizOrdNo,getCartonLPNo,salesorderdetails);
					}
					catch(exp)
					{
						nlapiLogExecution('DEBUG', 'Expection in CustomerSpecific Ucc Label Generation',exp);
					}

				}
			}
			else if (systemruletype == "ZebraFormat") 
			{

				if (systemrulevalue == "Standard")
				{
					GenerateZebraAddressLabel(vebizOrdNo,salesorderdetails);
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'CreatePackListException', exp);

	}

	nlapiLogExecution('Debug', 'Time Stamp at the end of CreatePackListCheckbyWmsStatusFlag',TimeStampinSec());
}

/***Below code is merged from Lexjet production on 5th march by Ganesh ***/
function createItemFulfillmentMultiLineShipping(SalesOrderInternalId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsrefno,vcontainerLp,
		wavenumber,fulfillmenttask,itemloc,recid,LoadSalesOrderRec)
{
	nlapiLogExecution('Debug', 'Into createItemFulfillmentMultiLineShipping');

	//Item Fulfillment for Multi Line Shipping

	var shipgroupslist=new Array();
	var itemarray=new Array();
	var TransformRecId = null;

	var columns = new Array();
	var filters = new Array();
	nlapiLogExecution('Debug', 'vebizOrdNo',vebizOrdNo);
	nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);
	nlapiLogExecution('Debug', 'vcontainerLp',vcontainerLp);


	var vRoleLocation=getRoledBasedLocation();


	filters.push(new nlobjSearchFilter('internalid', null, 'is', SalesOrderInternalId));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('shipgroup', null, 'greaterthan', 0));
	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{		
		filters.push(new nlobjSearchFilter('location', null, 'is', vRoleLocation));
	}

	columns[0] = new nlobjSearchColumn('shipgroup');
	columns[1] = new nlobjSearchColumn('item');
	columns[2] = new nlobjSearchColumn('line').setSort();

	var searchresults = nlapiSearchRecord('salesorder', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		// The below logic is to get all the distinct ship groups to shipgroupslist. 
		label:for(var i=0; i<searchresults.length;i++ )
		{  
			var vshipgroup=searchresults[i].getValue('shipgroup');

			if(vshipgroup==null || vshipgroup==''||vshipgroup==0)
				vshipgroup=1;	

			for(var j=0; j<shipgroupslist.length;j++ )
			{
				if(shipgroupslist[j]==vshipgroup) 
					continue label;
			}

			nlapiLogExecution('Debug', 'vshipgroup',vshipgroup);
			shipgroupslist[shipgroupslist.length] = vshipgroup;
		}

	for(var k=0; k<shipgroupslist.length;k++ )
	{
		var shipgroup=shipgroupslist[k];

		nlapiLogExecution('Debug', 'shipgroup',shipgroup);

		for(var l=0; l<searchresults.length;l++ )
		{
			var vshipgroup2=searchresults[l].getValue('shipgroup');

			if(vshipgroup2==null || vshipgroup2==''||vshipgroup2==0)
				vshipgroup2=1;

			if(shipgroup == vshipgroup2)
			{
				nlapiLogExecution('Debug', 'item',searchresults[l].getValue('item'));
				itemarray.push(searchresults[l].getValue('item'));
			}
		}


		//var confirmLotToNS='N';

		var lineArray = new Array();
		var filterLineArray = new Array();
		var filterItemArray =new Array(); // For distinct item ids
		var itemListArray= new Array();
		var ItemArray= new Array();
		var filter= new Array();
		//case# 20148056 starts
		//var FULFILLMENTATORDERLEVEL=getSystemRuleValue('IF001');
		var FULFILLMENTATORDERLEVEL=getSystemRuleValue('IF001',itemloc);
		//case# 20148056 ends
		var QuilfiedItemArray=new Array();
		var QuilfiedItemLineArray=new Array();
		var QuilfiedItemLineKitArray=new Array();

		filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); //PICK

		if(fulfillmenttask=='PICK')
			filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['9']));//9 //Picks generated
		else
			filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8','9']));
		if(vWMSCarrier != null && vWMSCarrier != '')
			filter.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier
		if(itemarray != null && itemarray != '')
			filter.push(new nlobjSearchFilter('custrecord_parent_sku_no', null,'anyof', itemarray));

		if(FULFILLMENTATORDERLEVEL!='Y')
		{
			filter.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
		}
		else
		{
			filter.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		}

		var columns= new Array(); 

		columns[0] = new nlobjSearchColumn('custrecord_act_qty');
		columns[1] = new nlobjSearchColumn('custrecord_line_no');
		columns[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[3]= new nlobjSearchColumn('custrecord_act_end_date');

		var opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);

		nlapiLogExecution('Debug', 'opentaskordersearchresult',opentaskordersearchresult);

		if(opentaskordersearchresult==null) // To make sure there is not records exists with Picks generated stage for the order
		{
			nlapiLogExecution('Debug', 'No Open Records. All are Pack Confirmaed');

			var filters= new Array();	

			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
			if(fulfillmenttask=='PICK')
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8']));//9 //Picks confirmed
			else
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));// PACK COMPLETE

			filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
			/***Below code is merged from Factory  mation production on 2nd March 2013 by Ganesh as part of standard bundle***/
			filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
			filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0.0'));
			/***upto here ***/
			if(FULFILLMENTATORDERLEVEL!='Y')
			{
				filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
			}
			else
			{
				filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
			}

			if(itemarray != null && itemarray != '')
				filters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null,'anyof', itemarray));
			var vRoleLocation=getRoledBasedLocation();
			nlapiLogExecution('Debug', 'vRoleLocation',vRoleLocation);
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

			}

			var column =new Array(); 

			column[0] = new nlobjSearchColumn('custrecord_act_qty');
			column[1] = new nlobjSearchColumn('custrecord_line_no');
			column[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
			column[3]= new nlobjSearchColumn('custrecord_act_end_date');
			column[4]= new nlobjSearchColumn('internalid');
			column[5]= new nlobjSearchColumn('name');
			column[6]= new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			column[7]= new nlobjSearchColumn('custrecord_ebiz_order_no');
			column[8]= new nlobjSearchColumn('custrecord_serial_no');
			column[9]= new nlobjSearchColumn('custrecord_batch_no');					
			column[10]= new nlobjSearchColumn('custrecord_total_weight');
			column[11]= new nlobjSearchColumn('custrecord_totalcube');
			column[12]= new nlobjSearchColumn('custrecord_parent_sku_no');
			column[13]= new nlobjSearchColumn('custrecord_sku');
			column[1].setSort();
			column[9].setSort();

			var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

			var SalesOrderInternalId="";var DoLineId="";
			var totalWeight=0.0;
			var totalcube=0.0;
			if(searchresult !=null) // SearchResult contains all the open task records of task type PICK and status flag PACK COMPLETE
			{
				var sum =0;
				var actqty="";
				var linenumber="";
				var skuno="";
				var vBatchno="";
				var vserialno="";

				if(parseInt(searchresult.length)>50)
				{
					nlapiLogExecution('Debug', 'Invoking Schedule Script Starts',searchresult.length);

					var param = new Array();
					param['custscript_soorderinternalid'] = SalesOrderInternalId;
					param['custscript_ebizordno'] = vebizOrdNo;
					param['custscript_wmscarrier'] = vWMSCarrier;
					param['custscript_confirmtons'] = confirmLotToNS;
					param['custscript_ebizwavenumber'] = wavenumber;
					param['custscript_containerlp'] = vcontainerLp;
					param['custscript_itemloc'] = itemloc;
					param['custscript_fulfillmenttask'] = fulfillmenttask;
					param['custscript_ismultilineship'] = 'T';
					param['custscript_recid'] = '';

					nlapiScheduleScript('customscript_ebiz_itemfulfillment_sch', null,param);


				}
				else
				{
					DoLineId=searchresult[0].getValue('custrecord_ebiz_cntrl_no');

					for (l = 0; l < searchresult.length; l++) 
					{  
						nlapiLogExecution('Debug', 'searchresult.length',searchresult.length);
						SalesOrderInternalId=searchresult[l].getValue('custrecord_ebiz_order_no');						
						itemListArray[l] = new Array();

						actqty=searchresult[l].getValue('custrecord_act_qty');
						linenumber=searchresult[l].getValue('custrecord_line_no');
						skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
						vBatchno = searchresult[l].getValue('custrecord_batch_no');
						vserialno = searchresult[l].getValue('custrecord_serial_no');
						var parentskuno=searchresult[l].getValue('custrecord_parent_sku_no');
						var skuname=searchresult[l].getText('custrecord_parent_sku_no');
						var skuvalue=searchresult[l].getValue('custrecord_ebiz_sku_no');
						var skutext=searchresult[l].getText('custrecord_sku');
						if(searchresult[l].getValue('custrecord_total_weight')!=null && searchresult[l].getValue('custrecord_total_weight')!='')
						{
							totalWeight=totalWeight+parseFloat(searchresult[l].getValue('custrecord_total_weight'));
						}

						if(searchresult[l].getValue('custrecord_totalcube')!=null && searchresult[l].getValue('custrecord_totalcube')!='')
						{
							totalcube=totalcube+parseFloat(searchresult[l].getValue('custrecord_totalcube'));
						}

						itemListArray[l][0]=actqty;
						itemListArray[l][1]=skuno;
						itemListArray[l][2]=linenumber;
						itemListArray[l][3]=vBatchno;
						itemListArray[l][4]=vserialno;
						itemListArray[l][5]=skuname;
						itemListArray[l][6]=parentskuno;
						itemListArray[l][7]=skuvalue;
						itemListArray[l][8]=skutext;
						ItemArray.push(skuno);
						lineArray.push(linenumber);
					}


					// The below logic is to get all the distinct line nos to filterLineArray . This contails distinct lines nos for the order.
					label:for(var i=0; i<lineArray.length;i++ )
					{  
						for(var j=0; j<filterLineArray.length;j++ )
						{
							if(filterLineArray [j]==lineArray [i]) 
								continue label;
						}
						filterLineArray [filterLineArray .length] = lineArray [i];
					}

					filterLineArray.sort(function(a,b){return a - b;});

					// The below logic is to get all the distinct Item ids filterItemArray . This contains distinct Item ids for the order.
					label2:for(var m=0; m<ItemArray.length;m++ )
					{  
						for(var p=0; p<filterItemArray.length;p++ )
						{
							if(filterItemArray[p]==ItemArray[m]) 
								continue label2;
						}
						filterItemArray[filterItemArray.length] = ItemArray[m];
					}


					// To fetch Item dims from Item Array
					var vItemDimsArr = geteBizItemDimensionsArray(filterItemArray);
					if(vItemDimsArr !=null && vItemDimsArr != "")
					{
						nlapiLogExecution('Debug', 'vItemDimsArr.length', vItemDimsArr.length);
					}	

					var TransformRec;

					nlapiLogExecution('Debug', 'SalesOrderInternalId', SalesOrderInternalId);

					var filters= new Array();

					filters.push(new nlobjSearchFilter('internalid', null,'is', SalesOrderInternalId));
					filters.push(new nlobjSearchFilter('mainline',null,'is','T'));

					var column =new Array(); 

					column[0] = new nlobjSearchColumn('custbody_nswmssoordertype');
					//column[1] = new nlobjSearchColumn('custbody_total_weight');

					var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
					nlapiLogExecution('Debug', 'trantype', trantype);

					var vAdvBinManagement=false;

					var ctx = nlapiGetContext();
					if(ctx != null && ctx != '')
					{
						if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
							vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
					}  
					nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);

					if(trantype=="salesorder")
					{
						var orderTypeSearchResult = nlapiSearchRecord('salesorder',null,filters,column);
						if(orderTypeSearchResult!=null && orderTypeSearchResult.length>0)
						{
							try
							{
								//Case #20127191 Start
								/*if(orderTypeSearchResult[0].getValue('custbody_nswmssoordertype')=='5')
							{
								nlapiLogExecution('Debug', 'from soto', 'from soto');
								TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment', { 'shipgroup' : shipgroupslist[k]});
								nlapiLogExecution('Debug', 'Item fulfillment created', 'Success');
							}
							else
							{*/
								//Case #20127191 End
								nlapiLogExecution('Debug', 'from so', 'from so');
								TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment', { 'shipgroup' : shipgroupslist[k]});
								//Case #20127191 Start
								/*}*/
								//Case #20127191 End 
							}
							catch(e) {

								var exceptionname='TransferOrder';
								var functionality=vebizOrdNo+" - " + 'ItemFulFillMent';
								var trantype=2;
								var userId = nlapiGetUser();
								nlapiLogExecution('Debug', 'DetailsError', functionality);	
								nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
								nlapiLogExecution('Debug', 'vebizOrdNo355', vebizOrdNo);
								var reference3="";
								var reference4 ="";
								var reference5 ="";
								var exceptiondetails=e;
								if ( e instanceof nlobjError )
								{
									nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
									exceptiondetails=e.getCode() + '\n' + e.getDetails();
								}
								else
								{
									exceptiondetails=e.toString();
									nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
								}
								UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
								InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
								nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		
							}
						}
					}
					else
					{
						try
						{
							nlapiLogExecution('Debug', 'from to', 'from to');
							TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment', { 'shipgroup' : shipgroupslist[k]});
						}

						catch(e) {

							var exceptionname='TransferOrder';
							var functionality='Item Fulfillment';
							var trantype=2;
							nlapiLogExecution('Debug', 'DetailsError', functionality);	
							nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
							nlapiLogExecution('Debug', 'vebizOrdNo356', vebizOrdNo);
							var reference2="";
							var reference3="";
							var reference4 ="";
							var reference5 ="";
							var alertType='1';
							var exceptiondetails=e;
							if ( e instanceof nlobjError )
							{
								nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
								exceptiondetails=e.getCode() + '\n' + e.getDetails();
							}
							else
							{
								exceptiondetails=e.toString();
								nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
							}
							errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
							UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
							InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
							nlapiLogExecution('Debug', 'Exception in TransformRec (TransferOrder) : ', e);		
						}
					}
					nlapiLogExecution('Debug','TransformRec', TransformRec);
					if(TransformRec!=null && TransformRec!='')
					{
						for(var i=0; i<filterLineArray.length;i++ ) // This array contains all disctinct lines no for the order
						{ 
							var context = nlapiGetContext();
							nlapiLogExecution('Debug','Remaining usage at the start of Item fulfillment line# ' + i,context.getRemainingUsage());

							var actuvalqty="";
							var skunumber="";
							var linenumber="";
							var totalqty=0;	
							var ItemType="";
							var batchno="";
							var serialno="";var serialout="";
							var filterlinenumber=filterLineArray[i];
							//By Ganesh for batch# entry
							var vPrevbatch;
							var vPrevLine;
							var vLotNo;
							var VLotNo1;
							var vLotQty=0;
							var vLotTotQty=0;
							var boolfound = false;
							var serialsspaces = "";
							var IsLineClosed="F";
							//Case # 20126922ï¿½Start
							if(LoadSalesOrderRec !=null && LoadSalesOrderRec!='')
							{
								nlapiLogExecution('ERROR', 'itemname before replace inside', itemname);
								IsLineClosed = LoadSalesOrderRec.getLineItemValue('item', 'isclosed', filterlinenumber);
							}
							//Case # 20126922 end
							nlapiLogExecution('ERROR', 'itemname before replace outside', itemname);
							var itemname = TransformRec.getLineItemText('item', 'item', i+1);
							nlapiLogExecution('ERROR', 'itemname before replace1', itemname);
							var itemvalue = TransformRec.getLineItemValue('item', 'item', i+1);
							nlapiLogExecution('ERROR', 'itemname before replace', itemname);


							for(var y=0; y<itemListArray.length;y++) // This array contails all the data for the order
							{ 
								var vlinenumber =itemListArray[y][2];
								nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
								nlapiLogExecution('Debug', 'vlinenumber', vlinenumber);
								if(filterlinenumber==vlinenumber)
								{
									itemvalue =itemListArray[y][6];
									itemname =itemListArray[y][5];
									if(itemvalue==null || itemvalue=='')
										itemvalue=itemListArray[y][7];
									itemname=itemListArray[y][8];
								}
							}	

							if(itemname!=null && itemname!=''&&IsLineClosed!="T")
							{

								itemname=itemname.replace(/ /g,"-");
								nlapiLogExecution('Debug', 'itemname1', itemname);
								nlapiLogExecution('Debug', 'itemvalue', itemvalue);
								nlapiLogExecution('Debug', 'itemListArray.length', itemListArray.length);
								var itemTypesku = nlapiLookupField('item', itemvalue, 'recordType');
								nlapiLogExecution('Debug', 'itemTypesku', itemTypesku);
								var searchresultsitem = nlapiLoadRecord(itemTypesku, itemvalue);
								var SkuNo=searchresultsitem.getFieldValue('itemid');
								nlapiLogExecution('Debug', 'SkuNo', SkuNo);

								var filters = new Array(); 			 
								filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);		

								var columns1 = new Array(); 
								columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
								columns1[1] = new nlobjSearchColumn( 'memberquantity' );

								var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 
								if(itemTypesku=='kititem')
								{
									//Code added on 5th Sep 2013 by Suman.
									var KitIsPicked=ISKitItemcompletlyPicked(itemvalue,vebizOrdNo,vWMSCarrier);
									nlapiLogExecution("ERROR","KitIsPicked",KitIsPicked);

									if(KitIsPicked=="true")
									{
										//End of code as of 5th Sep.

										for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
										{ 
											linenumber =itemListArray[n][2];



											nlapiLogExecution('Debug', 'linenumber', linenumber);
											nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);

											for(var w=0; w<searchresults.length;w++) 
											{
												fulfilmentItem = searchresults[w].getValue('memberitem');
												memberitemqty = searchresults[w].getValue('memberquantity');

												if((filterlinenumber==linenumber && itemListArray[n][1]==fulfilmentItem))
												{
													QuilfiedItemArray.push(itemvalue);
													QuilfiedItemLineArray.push(linenumber);
													QuilfiedItemLineKitArray.push(linenumber);
													nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
													nlapiLogExecution('Debug', 'totalqty', totalqty);
													skunumber=itemListArray[n][1];
													var columns;

													batchno =itemListArray[n][3];

													if(serialno=="")
													{
														serialno =itemListArray[n][4];}
													else
													{ 
														serialno =serialno+","+itemListArray[n][4];
													}

													if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
													{

														vPrevbatch=itemname;
														itemListArray[n][3] = itemname;
														vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
														nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
													}

													if (boolfound) {
														//totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);
														totalqty = Math.floor(parseFloat(totalqty) + parseFloat(itemListArray[n][0])/memberitemqty);

														vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
														serialsspaces += " " + itemListArray[n][4];


														if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
														{
															if(itemListArray[n][0] != null && itemListArray[n][0] != "")
																vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
														}
														else
														{ 	
															nlapiLogExecution('Debug', 'itemname2', itemname);										

															if(vLotNo!=null && vLotNo != "")
																vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
															else
																vLotNo = vPrevbatch +"(" + vLotQty + ")";

															if(itemListArray[n][0] != null && itemListArray[n][0] != "")
																vLotQty=parseFloat(itemListArray[n][0]);
															vPrevbatch=itemListArray[n][3];
															vPrevLine=linenumber;
														}

													} else {

														if(skunumber!= null && skunumber != "")
														{
															columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
															ItemType = columns.recordType;
															nlapiLogExecution('Debug', 'ItemType in else', ItemType);
														}

														//totalqty = parseFloat(itemListArray[n][0]);
														totalqty = Math.floor(parseFloat(itemListArray[n][0])/memberitemqty);
														vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
														serialsspaces = itemListArray[n][4];

														if(itemListArray[n][0] != null && itemListArray[n][0] != "")
															vLotQty=parseFloat(itemListArray[n][0]);
														vPrevbatch=itemListArray[n][3];
														vPrevLine=linenumber;
													}
													boolfound = true;
												}
											}
										}
									}
								}
								else
								{
									for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
									{ 
										linenumber =itemListArray[n][2];

										nlapiLogExecution('Debug', 'linenumber', linenumber);
										nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);

										if((filterlinenumber==linenumber))
										{
											QuilfiedItemArray.push(itemvalue);
											QuilfiedItemLineArray.push(linenumber);
											nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
											nlapiLogExecution('Debug', 'totalqty', totalqty);
											skunumber=itemListArray[n][1];
											var columns;

											batchno =itemListArray[n][3];

											if(serialno=="")
											{
												serialno =itemListArray[n][4];}
											else
											{ 
												serialno =serialno+","+itemListArray[n][4];
											}

											if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
											{

												vPrevbatch=itemname;
												itemListArray[n][3] = itemname;
												vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
												nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
											}
											var vAdvBinManagement=false;

											var ctx = nlapiGetContext();
											if(ctx != null && ctx != '')
											{
												if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
													vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
											}  
											nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);

											//case # 20127191, getting itemtype
											if(skunumber!= null && skunumber != "")
											{
												columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
												ItemType = columns.recordType;
												nlapiLogExecution('Debug', 'ItemType', ItemType);
											}


											if(vAdvBinManagement)
											{
												//Case # 20127191 Start
												/*	if (ItemType == "serializedinventoryitem")
											{

												nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
												var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', itemListArray[n][0]);
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][4]);

												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();


												var filters= new Array();				
												nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
												filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
												filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
												filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
												var column =new Array(); 

												column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

												var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
												if(searchresultser!=null && searchresultser!='')
												{
													var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
													for ( var b = 0; b < searchresultser.length; b++ ) {

														serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');

														nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
														nlapiLogExecution('Debug', 'Using totalqty',totalqty);
														nlapiLogExecution('Debug', 'Using serialsspaces',serialsspaces);

														compSubRecord.selectNewLineItem('inventoryassignment');
														//Case # 20126405 Start
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
														//Case # 20126405 End
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serialsspaces);

														compSubRecord.commitLineItem('inventoryassignment');


													}
													compSubRecord.commit();
												}





											}*/
												//Case # 20127191 End
//												if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//												{
//												nlapiLogExecution('Debug', 'Using vAdvBinManagement5',vAdvBinManagement);
//												nlapiLogExecution('Debug', 'itemListArray[n][0]',itemListArray[n][0]);
//												nlapiLogExecution('Debug', 'itemListArray[n][3]',itemListArray[n][3]);
//												var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//												nlapiLogExecution('Debug', 'Using vAdvBinManagement6',vAdvBinManagement);
//												compSubRecord.selectNewLineItem('inventoryassignment');
//												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', parseInt(itemListArray[n][0]));
//												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][3]);

//												compSubRecord.commitLineItem('inventoryassignment');
//												nlapiLogExecution('Debug', 'Using vAdvBinManagement7',vAdvBinManagement);
//												compSubRecord.commit();
//												nlapiLogExecution('Debug', 'Using vAdvBinManagement8',vAdvBinManagement);
//												}
											}
											if (boolfound) {
												totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);

												vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
												serialsspaces += " " + itemListArray[n][4];


												if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
												{
													if(itemListArray[n][0] != null && itemListArray[n][0] != "")
														vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
												}
												else
												{ 	
													nlapiLogExecution('Debug', 'itemname2', itemname);										

													if(vLotNo!=null && vLotNo != "")
														vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
													else
														vLotNo = vPrevbatch +"(" + vLotQty + ")";

													if(itemListArray[n][0] != null && itemListArray[n][0] != "")
														vLotQty=parseFloat(itemListArray[n][0]);
													vPrevbatch=itemListArray[n][3];
													vPrevLine=linenumber;
												}

											} else {

												if(skunumber!= null && skunumber != "")
												{
													columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
													ItemType = columns.recordType;
													nlapiLogExecution('Debug', 'ItemType in else', ItemType);
												}

												totalqty = parseFloat(itemListArray[n][0]);
												vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
												serialsspaces = itemListArray[n][4];

												if(itemListArray[n][0] != null && itemListArray[n][0] != "")
													vLotQty=parseFloat(itemListArray[n][0]);
												vPrevbatch=itemListArray[n][3];
												vPrevLine=linenumber;
											}
											boolfound = true;
										}
									}	
								}

								nlapiLogExecution('Debug', 'skuvalue', skunumber);
								nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
								nlapiLogExecution('Debug', 'totalqty', totalqty);
								nlapiLogExecution('Debug', 'batchno', batchno);
								nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);

								var SOLength = TransformRec.getLineItemCount('item');  // To get the no of lines from item fulfillment record
								nlapiLogExecution('Debug', "SO Length", SOLength);    
								var itemIndex=0;
								var SOWMSCarrier = TransformRec.getFieldValue('custbody_salesorder_carrier');
								for (var j = 1; j <= SOLength; j++) {

									var item_id = TransformRec.getLineItemValue('item', 'item', j);
									// To get Item Type
									//Below code commented for governance issue and this is code is not using
									//var columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
									var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
									var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);
									if(trantype=="transferorder"){
										itemLineNo=parseFloat(itemLineNo)-1;
									}
									var NSUOM = TransformRec.getLineItemValue('item', 'units', j);  
									nlapiLogExecution('Debug', "itemIndex", itemIndex); 
									nlapiLogExecution('Debug', "itemLineNo", itemLineNo);
									if (itemLineNo == filterlinenumber) {
										itemIndex=j;    
										nlapiLogExecution('Debug', "itemIndex", itemIndex);
									}
								}
								if(itemIndex!=0)
								{
									var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
									var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
									itemname=itemname.replace(/ /g,"-");
									var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
									var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);
									nlapiLogExecution('Debug', 'itemloc2', itemloc2);

									var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);
									nlapiLogExecution('Debug', 'itemIndex', itemIndex);
									nlapiLogExecution('Debug', 'itemloc2', itemloc2);
									nlapiLogExecution('Debug', 'NSOrdUOM', NSOrdUOM);

									if(NSOrdUOM!=null && NSOrdUOM!="")
									{
										var veBizUOMQty=0;
										var vBaseUOMQty=0;

										if(vItemDimsArr!=null && vItemDimsArr.length>0)
										{
											for(z=0; z < vItemDimsArr.length; z++)
											{
												if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
												{	
													if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
													{
														vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
														nlapiLogExecution('Debug', 'vBaseUOMQty', vBaseUOMQty);
													}

													if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
													{
														nlapiLogExecution('Debug', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
														veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
														nlapiLogExecution('Debug', 'veBizUOMQty', veBizUOMQty);
													}
												}
											}	
											if(veBizUOMQty!=0)
												totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
											nlapiLogExecution('Debug', 'totalqty', totalqty);

											vLotQty =totalqty;
										}
									}

									if (boolfound) {
										if(confirmLotToNS=='N')
											vPrevbatch=itemname;									


										if(vLotNo!=null && vLotNo != "")
											vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
										else
											vLotNo = vPrevbatch +"(" + vLotQty + ")";


										nlapiLogExecution('Debug', 'vLotNo',vLotNo);
										nlapiLogExecution('Debug', 'vLotQty',vLotQty);

										TransformRec.selectLineItem('item', itemIndex);
										TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
										nlapiLogExecution('Debug', 'totalqty', totalqty);
										TransformRec.setCurrentLineItemValue('item', 'quantity', totalqty);
										nlapiLogExecution('Debug', 'itemloc2', itemloc2);
										TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

										nlapiLogExecution('Debug', 'ItemType', ItemType);
										nlapiLogExecution('Debug', 'vAdvBinManagement1', vAdvBinManagement);
										if(vAdvBinManagement)
										{
											if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
											{
												//if(itemListArray!=null && itemListArray!='')
												//{
												//	var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
												//for(var i1=0;i1<itemListArray.length;i1++)
												//{
//												if(itemListArray[i1][0] != null && itemListArray[i1][0] != "")
//												vLotQty=parseInt(itemListArray[i1][0]);
//												vPrevbatch=itemListArray[i1][3];


//												if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//												{
												nlapiLogExecution('Debug', 'totalqty',totalqty);
												nlapiLogExecution('Debug', 'Using vPrevbatch',vLotNo);
												var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');

												//compSubRecord.selectNewLineItem('inventoryassignment');

												//var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
												if(compSubRecord !=null && compSubRecord!='')
												{
													compSubRecord.selectLineItem('inventoryassignment', 1);
												}
												else
												{
													compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
													compSubRecord.selectNewLineItem('inventoryassignment');
												}
												//compSubRecord.selectLineItem('inventoryassignment',1);
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', totalqty);
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();	
											}
											//Case # 20127191 Start
											else if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem")
											{

												/*nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
											var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
											compSubRecord.selectNewLineItem('inventoryassignment');
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', itemListArray[n][0]);
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][4]);

											compSubRecord.commitLineItem('inventoryassignment');
											compSubRecord.commit();*/


												var filters= new Array();				
												nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
												filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
												filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
												filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
												var column =new Array(); 

												column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

												var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
												if(searchresultser!=null && searchresultser!='')
												{
													var subrecordtype= 'edit';
													//	var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
													//var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
													var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
													if(compSubRecord ==null || compSubRecord =='' || compSubRecord == 'null')
													{												
														compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
														subrecordtype='insert';
													}
													for ( var b = 0; b < searchresultser.length; b++ ) {

														serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');

														nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
														nlapiLogExecution('Debug', 'Using totalqty',totalqty);
														nlapiLogExecution('Debug', 'Using serialsspaces',serialsspaces);
														if(subrecordtype=='edit')
														{
															var indx=parseInt(b)+1;
															compSubRecord.selectLineItem('inventoryassignment', indx);
														}
														else
														{

															compSubRecord.selectNewLineItem('inventoryassignment');
														}

														//compSubRecord.selectNewLineItem('inventoryassignment');
														//Case # 20126405 Start
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
														//Case # 20126405 End
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serialsspaces);

														compSubRecord.commitLineItem('inventoryassignment');


													}
													compSubRecord.commit();
												}





											}
											//Case # 20127191 End
										}
										if(!vAdvBinManagement)
										{
											if (ItemType == "lotnumberedinventoryitem") {
												nlapiLogExecution('Debug', 'vLotNo', vLotNo);									
												TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
												nlapiLogExecution('Debug', 'vBatchno', vBatchno);
											}
											else if(ItemType == "lotnumberedassemblyitem") 
											{
												TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
												nlapiLogExecution('Debug', 'vLotNo', vLotNo);
											}
											else if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") {
												var serialsspaces = "";
												if(serialsspaces==null || serialsspaces=='')
												{
													var filters= new Array();				
													nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
													filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
													filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));

													var column =new Array(); 

													column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

													var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
													if(searchresultser!=null && searchresultser!='')
													{
														for ( var b = 0; b < searchresultser.length; b++ ) {
															if(serialsspaces==null || serialsspaces=='')
															{
																serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
															}
															else
															{
																serialsspaces += " " + searchresultser[b].getValue('custrecord_serialnumber');
															}
														}
													}


													nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);
												}
												nlapiLogExecution('Debug', 'before serialsspaces', serialsspaces);
												TransformRec.setCurrentLineItemValue('item','serialnumbers', serialsspaces);
											}
										}
										vLotNo="";
									}
									TransformRec.commitLineItem('item');
								}
							}
						}
//						Case# 20124622 Start//Issue Fix for the so having lines with different site then itemfulfillment was failing if we generate the wave for single site
						var SOLengthnew = TransformRec.getLineItemCount('item');
						if(parseInt(filterLineArray.length)!=parseInt(SOLengthnew))
						{
							for(var k1=1;k1<=SOLengthnew;k1++)
							{
								var isLinenoComitted=false;
								var linenum= TransformRec.getLineItemValue('item', 'line', k1);
								for(var k2=0;k2<filterLineArray.length;k2++)
								{
									var filterlineno=filterLineArray[k2];
									if(parseInt(linenum)==parseInt(filterlineno))
									{
										isLinenoComitted=true;
										break;
									}

								}
								if(isLinenoComitted==false)
								{
									nlapiLogExecution('Debug', 'itemIndex in', k1);
									TransformRec.selectLineItem('item', k1);

									TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

									TransformRec.commitLineItem('item');
								}
							}
						}
						//Case# 20124622 End
						//nlapiLogExecution('Debug', 'itemloc', itemloc);
						nlapiLogExecution('Debug', 'Before Submit', '');
						try
						{
							TransformRecId = nlapiSubmitRecord(TransformRec, true);
						}
						catch(e)
						{
							var exceptionname='SalesOrder';
							var functionality='Creating Item Fulfillment';
							var trantype=2;
							nlapiLogExecution('Debug', 'DetailsError', functionality);	
							nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
							nlapiLogExecution('Debug', 'vebizOrdNo357', vebizOrdNo);
							var reference2="";
							var reference3="";
							var reference4 ="";
							var reference5 ="";
							var alertType=1;//1 for exception and 2 for report
							var exceptiondetails=e;
							if ( e instanceof nlobjError )
							{
								nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
								exceptiondetails=e.getCode() + '\n' + e.getDetails();
							}
							else
							{
								exceptiondetails=e.toString();
								nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
							}
							InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
							UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
							var ErrorCode=e.getCode();
							var vErrorMsg=e.message;
							nlapiLogExecution('Debug', 'Debug', e.getCode());
							nlapiLogExecution('Debug', 'Debug', e.message);
							nlapiLogExecution('Debug', 'Debug', e);
						}
						nlapiLogExecution('Debug', 'After Submit', '');
					}

					//Code added on 5th Sep 2013 by suman.
					if(TransformRecId!=null&&TransformRecId!="")
					{
						if(QuilfiedItemArray!=null&&QuilfiedItemArray!=""&&QuilfiedItemArray.length!=0)
						{
							nlapiLogExecution('Debug', 'QuilfiedItemArray', QuilfiedItemArray);
							var QuilfiedItemArray=removeDuplicateElementOld(QuilfiedItemArray);
							var QuilfiedItemLineArray=removeDuplicateElementOld(QuilfiedItemLineArray);
							var QuilfiedItemLineKitArray=removeDuplicateElementOld(QuilfiedItemLineKitArray);
							UpdateAllOpenTaskRecordsWithNSID(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineArray,vcontainerLp,QuilfiedItemLineKitArray,FULFILLMENTATORDERLEVEL);
						}
					}
					/*for ( var i = 0; i < searchresult.length; i++ ) {
					nlapiLogExecution('Debug', 'searchresult[i].getId()', searchresult[i].getId());
					nlapiLogExecution('Debug', 'TransformRecId', TransformRecId);
					nlapiSubmitField(searchresult[i].getRecordType(), searchresult[i].getId(), 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
				}*/
					//Start Case#: 20123475
					if(TransformRecId !=null && TransformRecId !='')
						AutoPackingAfterfulfillment(SalesOrderInternalId,itemloc,vebizOrdNo,FULFILLMENTATORDERLEVEL,vcontainerLp);
					//End Case#: 20123475	
					//							if(trantype=="salesorder" && TransformRecId !=null && TransformRecId !='' && totalWeight != null && totalWeight != '' && parseFloat(totalWeight) != 0 && totalWeight!=0.0 )
					//							{
					//							nlapiSubmitField('salesorder', SalesOrderInternalId, 'custbody_total_weight', parseFloat(totalWeight));
					//							}
				}
			}
		}
	}



	}
	return TransformRecId;
	nlapiLogExecution('Debug', 'Out of createItemFulfillmentMultiLineShipping');
}

function updateItemFulfillment(SalesOrderInternalId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsconfirmationno,vcontainerLp)
{
	nlapiLogExecution('Debug', 'Into updateItemFulfillment');
	//Item fulfillment update

	var lineArray = new Array();
	var filterLineArray = new Array();
	var filterItemArray =new Array(); // For distinct item ids
	//var AddTotalArray = new Array();
	var itemListArray= new Array();
	var ItemArray= new Array();
	var filter= new Array();

	filter.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
	filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); 			//PICK
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8','9']));//9 //Picks generated or Picks confirmed
	if(vWMSCarrier != null && vWMSCarrier != '')
		filter.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier

	var columns= new Array(); 

	columns[0] = new nlobjSearchColumn('custrecord_act_qty');
	columns[1] = new nlobjSearchColumn('custrecord_line_no');
	columns[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[3]= new nlobjSearchColumn('custrecord_act_end_date');

	var opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);

	nlapiLogExecution('Debug', 'opentaskordersearchresult',opentaskordersearchresult);

	if(opentaskordersearchresult==null) // To make sure there is not records exists with Picks generated stage for the order
	{
		nlapiLogExecution('Debug', 'No Open Records. All are Pack Confirmaed');

		var filters= new Array();					

		filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));// PACK COMPLETE
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'is',nsconfirmationno)); // NS REF no is empty
		if(vWMSCarrier != null && vWMSCarrier != '')
			filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier

		var column =new Array(); 

		column[0] = new nlobjSearchColumn('custrecord_act_qty');
		column[1] = new nlobjSearchColumn('custrecord_line_no');
		column[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
		column[3]= new nlobjSearchColumn('custrecord_act_end_date');
		column[4]= new nlobjSearchColumn('internalid');
		column[5]= new nlobjSearchColumn('name');
		column[6]= new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		column[7]= new nlobjSearchColumn('custrecord_ebiz_order_no');
		column[8]= new nlobjSearchColumn('custrecord_serial_no');
		column[9]= new nlobjSearchColumn('custrecord_batch_no');					
		column[10]= new nlobjSearchColumn('custrecord_total_weight');
		column[1].setSort();
		column[9].setSort();

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

		var SalesOrderInternalId="";var DoLineId="";
		var totalWeight=0.0;var totalcube=0.0;
		if(searchresult !=null) // SearchResult contains all the open task records of task type PICK and status flag PACK COMPLETE
		{
			var sum =0;
			var actqty="";
			var linenumber="";
			var skuno="";
			var vBatchno="";
			var vserialno="";
			var vNotes1='';
			var vFoOrdQty=0;
			var vFoPickQty=0;

			DoLineId=searchresult[0].getValue('custrecord_ebiz_cntrl_no');
			var FulfillOrdLinetransaction = nlapiLoadRecord('customrecord_ebiznet_ordline',DoLineId);
			if(FulfillOrdLinetransaction!=null && FulfillOrdLinetransaction!='')
			{
				vNotes1 = FulfillOrdLinetransaction.getFieldValue('custrecord_linenotes1');
				vFoOrdQty = FulfillOrdLinetransaction.getFieldValue('custrecord_ord_qty');
				vFoPickQty = FulfillOrdLinetransaction.getFieldValue('custrecord_pickqty');
			}

			for (l = 0; l < searchresult.length; l++) 
			{  
				nlapiLogExecution('Debug', 'searchresult.length',searchresult.length);
				SalesOrderInternalId=searchresult[l].getValue('custrecord_ebiz_order_no');						
				itemListArray[l] = new Array();

				actqty=searchresult[l].getValue('custrecord_act_qty');
				linenumber=searchresult[l].getValue('custrecord_line_no');
				skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
				vBatchno = searchresult[l].getValue('custrecord_batch_no');
				vserialno = searchresult[l].getValue('custrecord_serial_no');
				if(searchresult[l].getValue('custrecord_total_weight')!=null && searchresult[l].getValue('custrecord_total_weight')!='')
				{
					totalWeight=totalWeight+parseFloat(searchresult[l].getValue('custrecord_total_weight'));
				}
				if(searchresult[l].getValue('custrecord_total_weight')!=null && searchresult[l].getValue('custrecord_total_weight')!='')
				{
					totalcube=totalcube+parseFloat(searchresult[l].getValue('custrecord_totalcube'));
				}
				itemListArray[l][0]=actqty;
				itemListArray[l][1]=skuno;
				itemListArray[l][2]=linenumber;
				itemListArray[l][3]=vBatchno;
				itemListArray[l][4]=vserialno;
				ItemArray.push(skuno);
				lineArray.push(linenumber);
			}

		}

		// The below logic is to get all the distinct line nos to filterLineArray . This contails distinct lines nos for the order.
		label:for(var i=0; i<lineArray.length;i++ )
		{  
			for(var j=0; j<filterLineArray.length;j++ )
			{
				if(filterLineArray [j]==lineArray [i]) 
					continue label;
			}
			filterLineArray [filterLineArray .length] = lineArray [i];
		}

		filterLineArray.sort(function(a,b){return a - b;});

		// The below logic is to get all the distinct Item ids filterItemArray . This contains distinct Item ids for the order.
		label2:for(var m=0; m<ItemArray.length;m++ )
		{  
			for(var p=0; p<filterItemArray.length;p++ )
			{
				if(filterItemArray[p]==ItemArray[m]) 
					continue label2;
			}
			filterItemArray[filterItemArray.length] = ItemArray[m];
		}



		// To fetch Item dims from Item Array
		var vItemDimsArr = geteBizItemDimensionsArray(filterItemArray);
		if(vItemDimsArr !=null && vItemDimsArr != "")
		{
			nlapiLogExecution('Debug', 'vItemDimsArr.length', vItemDimsArr.length);
		}	

		var TransformRec;

		nlapiLogExecution('Debug', 'SalesOrderInternalId', SalesOrderInternalId);

		var filters= new Array();

		filters.push(new nlobjSearchFilter('internalid', null,'is', SalesOrderInternalId));
		filters.push(new nlobjSearchFilter('mainline',null,'is','T'));

		var column =new Array(); 

		column[0] = new nlobjSearchColumn('custbody_nswmssoordertype');
		column[1] = new nlobjSearchColumn('custbody_total_weight');

		var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
		nlapiLogExecution('Debug', 'trantype', trantype);

		if(trantype=="salesorder")
		{
			var orderTypeSearchResult = nlapiSearchRecord('salesorder',null,filters,column);
			if(orderTypeSearchResult!=null && orderTypeSearchResult.length>0)
			{
				try
				{
					if(orderTypeSearchResult[0].getValue('custbody_total_weight')!=null && orderTypeSearchResult[0].getValue('custbody_total_weight')!='')
					{
						totalWeight=totalWeight+parseFloat(orderTypeSearchResult[0].getValue('custbody_total_weight'));
					}

					if(orderTypeSearchResult[0].getValue('custbody_nswmssoordertype')=='5')
					{
						nlapiLogExecution('Debug', 'from soto', 'from soto');
						TransformRec = nlapiLoadRecord('itemfulfillment', nsconfirmationno);
					}
					else
					{
						nlapiLogExecution('Debug', 'from so', 'from so');
						TransformRec = nlapiLoadRecord('itemfulfillment', nsconfirmationno);

					}
				}
				catch(e) {

					var exceptionname='SalesOrder';
					var functionality='Creating ItemFulFillMent';
					var trantype=2;
					nlapiLogExecution('Debug', 'DetailsError', functionality);	
					nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
					nlapiLogExecution('Debug', 'vebizOrdNo358', vebizOrdNo);
					var reference2="";
					var reference3="";
					var reference4 ="";
					var reference5 ="";
					var alertType=1;//1 for exception and 2 for report
					var exceptiondetails=e;
					if ( e instanceof nlobjError )
					{
						nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
						exceptiondetails=e.getCode() + '\n' + e.getDetails();
					}
					else
					{
						exceptiondetails=e.toString();
						nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
					}
					errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
					UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
					InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
					nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		
				}
			}
		}
		else
		{
			try
			{
				nlapiLogExecution('Debug', 'from to', 'from to');
				TransformRec = nlapiLoadRecord('itemfulfillment', nsconfirmationno);
			}

			catch(e) {

				var exceptionname='TransferOrder';
				var functionality='Creating ItemFulFillMent';
				var trantype=2;
				nlapiLogExecution('Debug', 'DetailsError', functionality);	
				nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
				nlapiLogExecution('Debug', 'vebizOrdNo359', vebizOrdNo);
				var reference2="";
				var reference3="";
				var reference4 ="";
				var reference5 ="";
				var alertType=1;//1 for exception and 2 for report
				var exceptiondetails=e;
				if ( e instanceof nlobjError )
				{
					nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
					exceptiondetails=e.getCode() + '\n' + e.getDetails();
				}
				else
				{
					exceptiondetails=e.toString();
					nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
				}



				errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
				UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
				InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
				nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		
			}
		}
		for(var i=0; i<filterLineArray.length;i++ ) // This array contains all disctinct lines no for the order
		{ 
			var context = nlapiGetContext();
			nlapiLogExecution('Debug','Remaining usage at the start of Item fulfillment line# ' + i,context.getRemainingUsage());

			var actuvalqty="";
			var skunumber="";
			var linenumber="";
			var totalqty=0;	
			var ItemType="";
			var batchno="";
			var serialno="";var serialout="";
			var filterlinenumber=filterLineArray[i];
			//By Ganesh for batch# entry
			var vPrevbatch;
			var vPrevLine;
			var vLotNo;
			var VLotNo1;
			var vLotQty=0;
			var vLotTotQty=0;
			var boolfound = false;
			var serialsspaces = "";
			var itemname ='';
			var itemvalue ='';

			if(TransformRec!=null)
			{
				itemname = TransformRec.getLineItemText('item', 'item', i+1);
				itemvalue = TransformRec.getLineItemValue('item', 'item', i+1);
			}

			if(itemname!=null && itemname!='')
			{
				itemname=itemname.replace(/ /g,"-");
				nlapiLogExecution('Debug', 'itemname1', itemname);
				nlapiLogExecution('Debug', 'itemvalue', itemvalue);
				nlapiLogExecution('Debug', 'itemListArray.length', itemListArray.length);
				var itemTypesku = nlapiLookupField('item', itemvalue, 'recordType');
				nlapiLogExecution('Debug', 'itemTypesku', itemTypesku);
				var searchresultsitem = nlapiLoadRecord(itemTypesku, itemvalue);
				var SkuNo=searchresultsitem.getFieldValue('itemid');
				nlapiLogExecution('Debug', 'SkuNo', SkuNo);

				var filters = new Array(); 			 
				filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);		

				var columns1 = new Array(); 
				columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
				columns1[1] = new nlobjSearchColumn( 'memberquantity' );

				var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 
				if(itemTypesku=='kititem')
				{

					for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
					{ 
						linenumber =itemListArray[n][2];


						nlapiLogExecution('Debug', 'linenumber', linenumber);
						nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);

						for(var w=0; w<searchresults.length;w++) 
						{ 
							//var containerslist=getAllContainersList();
							fulfilmentItem = searchresults[w].getValue('memberitem');
							memberitemqty = searchresults[w].getValue('memberquantity');

							if((filterlinenumber==linenumber && itemListArray[n][1]==fulfilmentItem))
							{
								nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
								nlapiLogExecution('Debug', 'totalqty', totalqty);
								skunumber=itemListArray[n][1];

								var columns;
								batchno =itemListArray[n][3];

								if(serialno=="")
								{
									serialno =itemListArray[n][4];}
								else
								{ 
									serialno =serialno+","+itemListArray[n][4];
								}

								if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
								{

									vPrevbatch=itemname;
									itemListArray[n][3] = itemname;
									vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
									nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
								}

								if (boolfound) {
									//totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);
									totalqty = Math.floor(parseFloat(itemListArray[n][0])/memberitemqty);

									vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
									serialsspaces += " " + itemListArray[n][4];


									if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
									{
										if(itemListArray[n][0] != null && itemListArray[n][0] != "")
											vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
									}
									else
									{ 	
										nlapiLogExecution('Debug', 'itemname2', itemname);										

										if(vLotNo!=null && vLotNo != "")
											vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
										else
											vLotNo = vPrevbatch +"(" + vLotQty + ")";

										if(itemListArray[n][0] != null && itemListArray[n][0] != "")
											vLotQty=parseFloat(itemListArray[n][0]);
										vPrevbatch=itemListArray[n][3];
										vPrevLine=linenumber;
									}

								} else {
									if(skunumber!= null && skunumber != "")
									{
										columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
										ItemType = columns.recordType;
										nlapiLogExecution('Debug', 'ItemType in else', ItemType);
									}
									//totalqty = parseFloat(itemListArray[n][0]);
									totalqty = Math.floor(parseFloat(itemListArray[n][0])/memberitemqty);
									vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
									serialsspaces = itemListArray[n][4];

									if(itemListArray[n][0] != null && itemListArray[n][0] != "")
										vLotQty=parseFloat(itemListArray[n][0]);
									vPrevbatch=itemListArray[n][3];
									vPrevLine=linenumber;
								}
								boolfound = true;
							}
						}
					}
				}
				else
				{
					for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
					{ 
						linenumber =itemListArray[n][2];

						nlapiLogExecution('Debug', 'linenumber', linenumber);
						nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);

						if((filterlinenumber==linenumber))
						{
							nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
							nlapiLogExecution('Debug', 'totalqty', totalqty);
							skunumber=itemListArray[n][1];

							var columns;
							batchno =itemListArray[n][3];

							if(serialno=="")
							{
								serialno =itemListArray[n][4];}
							else
							{ 
								serialno =serialno+","+itemListArray[n][4];
							}

							if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
							{

								vPrevbatch=itemname;
								itemListArray[n][3] = itemname;
								vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
								nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
							}

							if (boolfound) {
								totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);

								vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
								serialsspaces += " " + itemListArray[n][4];


								if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
								{
									if(itemListArray[n][0] != null && itemListArray[n][0] != "")
										vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
								}
								else
								{ 	
									nlapiLogExecution('Debug', 'itemname2', itemname);										

									if(vLotNo!=null && vLotNo != "")
										vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
									else
										vLotNo = vPrevbatch +"(" + vLotQty + ")";

									if(itemListArray[n][0] != null && itemListArray[n][0] != "")
										vLotQty=parseFloat(itemListArray[n][0]);
									vPrevbatch=itemListArray[n][3];
									vPrevLine=linenumber;
								}

							} else {
								if(skunumber!= null && skunumber != "")
								{
									columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
									ItemType = columns.recordType;
									nlapiLogExecution('Debug', 'ItemType in else', ItemType);
								}
								totalqty = parseFloat(itemListArray[n][0]);
								vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
								serialsspaces = itemListArray[n][4];

								if(itemListArray[n][0] != null && itemListArray[n][0] != "")
									vLotQty=parseFloat(itemListArray[n][0]);
								vPrevbatch=itemListArray[n][3];
								vPrevLine=linenumber;
							}
							boolfound = true;
						}
					}
				}

				nlapiLogExecution('Debug', 'skuvalue', skunumber);
				nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
				nlapiLogExecution('Debug', 'totalqty', totalqty);
				nlapiLogExecution('Debug', 'batchno', batchno);
				nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);

				var SOLength = TransformRec.getLineItemCount('item');  // To get the no of lines from item fulfillment record
				nlapiLogExecution('Debug', "SO Length", SOLength);    
				var itemIndex=0;
				var SOWMSCarrier = TransformRec.getFieldValue('custbody_salesorder_carrier');
				for (var j = 1; j <= SOLength; j++) {

					var item_id = TransformRec.getLineItemValue('item', 'item', j);
					// To get Item Type
					//Below code commented for governance issue and this is code is not using
					//var columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
					var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
					var itemLineNo = TransformRec.getLineItemValue('item', 'orderline', j);
					if(trantype=="transferorder"){
						itemLineNo=parseFloat(itemLineNo)-1;
					}
					var NSUOM = TransformRec.getLineItemValue('item', 'units', j);  
					nlapiLogExecution('Debug', "itemIndex", itemIndex); 
					nlapiLogExecution('Debug', "itemLineNo", itemLineNo);
					if (itemLineNo == filterlinenumber) {
						itemIndex=j;    
						nlapiLogExecution('Debug', "itemIndex", itemIndex);
					}
				}
				if(itemIndex!=0)
				{
					var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
					var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
					if(itemname!=null && itemname!='')
						itemname=itemname.replace(/ /g,"-");
					var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
					var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);
					nlapiLogExecution('Debug', 'itemloc2', itemloc2);

					var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);
					nlapiLogExecution('Debug', 'itemIndex', itemIndex);
					nlapiLogExecution('Debug', 'itemloc2', itemloc2);
					nlapiLogExecution('Debug', 'NSOrdUOM', NSOrdUOM);

					if(NSOrdUOM!=null && NSOrdUOM!="")
					{

						var veBizUOMQty=0;
						var vBaseUOMQty=0;

						if(vItemDimsArr!=null && vItemDimsArr.length>0)
						{
							for(z=0; z < vItemDimsArr.length; z++)
							{
								if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
								{	
									if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
										nlapiLogExecution('Debug', 'vBaseUOMQty', vBaseUOMQty);
									}

									if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
									{
										nlapiLogExecution('Debug', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
										veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
										nlapiLogExecution('Debug', 'veBizUOMQty', veBizUOMQty);
									}
								}
							}
							if(veBizUOMQty==null || veBizUOMQty=='')
							{
								veBizUOMQty=vBaseUOMQty;
							}
							if(veBizUOMQty!=0)
								totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
							nlapiLogExecution('Debug', 'totalqty', totalqty);

							vLotQty =totalqty;
						}
					}

					if (boolfound) {

						if(totalqty>0)
						{

							if(confirmLotToNS=='N')
								vPrevbatch=itemname;									


							if(vLotNo!=null && vLotNo != "")
								vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
							else
								vLotNo = vPrevbatch +"(" + vLotQty + ")";


							nlapiLogExecution('Debug', 'vLotNo',vLotNo);
							nlapiLogExecution('Debug', 'vLotQty',vLotQty);
							nlapiLogExecution('Debug', 'totalqty', totalqty);

							TransformRec.selectLineItem('item', itemIndex);

							TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
							TransformRec.setCurrentLineItemValue('item', 'quantity', totalqty);
							nlapiLogExecution('Debug', 'itemloc2', itemloc2);
							TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

							nlapiLogExecution('Debug', 'ItemType', ItemType);

							if (ItemType == "lotnumberedinventoryitem") {
								nlapiLogExecution('Debug', 'vLotNo', vLotNo);									
								TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
								nlapiLogExecution('Debug', 'vBatchno', vBatchno);
							}
							else if(ItemType == "lotnumberedassemblyitem") 
							{
								TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
								nlapiLogExecution('Debug', 'vLotNo', vLotNo);
							}
							else if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") {
								TransformRec.setCurrentLineItemValue('item','serialnumbers', serialsspaces);
							}
							vLotNo="";

							TransformRec.commitLineItem('item');
						}
						else
						{
							nlapiLogExecution('Debug', 'Picked Qty is Zero', totalqty);
							nlapiLogExecution('Debug', 'SOLength', SOLength);

							if(SOLength==1)
							{
								nlapiDeleteRecord('itemfulfillment', nsconfirmationno);
								return;
							}
							else
								TransformRec.setLineItemValue('item','itemreceive',itemIndex,'F');
						}
					}					
				}
				else
				{

					nlapiLogExecution('Debug', 'itemIndex in else', itemIndex);


					if(itemIndex !=0)
					{
						nlapiLogExecution('Debug', 'itemIndex in else', itemIndex);
						TransformRec.selectLineItem('item', itemIndex);
						TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');
						TransformRec.commitLineItem('item');
					}

				}
			}
		}
		//Case# 20124622 Start//Issue Fix for the so having lines with different site then itemfulfillment was failing if we generate the wave for single site

		var SOLengthnew = TransformRec.getLineItemCount('item');
		if(parseInt(filterLineArray.length)!=parseInt(SOLengthnew))
		{
			for(var k1=1;k1<=SOLengthnew;k1++)
			{
				var isLinenoComitted=false;
				var linenum= TransformRec.getLineItemValue('item', 'line', k1);
				for(var k2=0;k2<filterLineArray.length;k2++)
				{
					var filterlineno=filterLineArray[k2];
					if(parseInt(linenum)==parseInt(filterlineno))
					{
						isLinenoComitted=true;
						break;
					}

				}
				if(isLinenoComitted==false)
				{
					nlapiLogExecution('Debug', 'itemIndex in', k1);
					TransformRec.selectLineItem('item', k1);

					TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

					TransformRec.commitLineItem('item');
				}
			}
		}
		//Case# 20124622 End
		nlapiLogExecution('Debug', 'Before Submit', '');

		if(TransformRec!=null)
		{
			var TransformRecId = nlapiSubmitRecord(TransformRec, true);
			nlapiLogExecution('Debug', 'After Submit', '');

			for ( var i = 0; i < searchresult.length; i++ ) {
				nlapiLogExecution('Debug', 'searchresult[i].getId()', searchresult[i].getId());
				nlapiLogExecution('Debug', 'TransformRecId', TransformRecId);
				nlapiSubmitField(searchresult[i].getRecordType(), searchresult[i].getId(), 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
			}

			if(trantype=="salesorder" && TransformRecId !=null && TransformRecId !='' && totalWeight != null && totalWeight != '' && parseFloat(totalWeight) != 0 && totalWeight!=0.0 )
			{
				nlapiSubmitField('salesorder', SalesOrderInternalId, 'custbody_total_weight', parseFloat(totalWeight));
				nlapiLogExecution('Debug', 'after SalesOrderInternalId', SalesOrderInternalId);
				totalcube = roundNumber(totalcube, 2);
				nlapiSubmitField('salesorder', SalesOrderInternalId, 'custbody_total_vol',parseFloat(totalcube));
			}
		}
	}
	nlapiLogExecution('Debug', 'Out of updateItemFulfillment');
}

function createItemFulfillmentforException(tranid,qtylinearray)
{
	nlapiLogExecution('Debug', 'Into createItemFulfillmentforException', trantypeso);
	nlapiLogExecution('Debug', 'tranid', tranid);
	var TransformRecId = '';
	var trantypeso = nlapiLookupField('transaction', tranid, 'recordType');

	nlapiLogExecution('Debug', 'trantypeso', trantypeso);

	var salesorderrec= nlapiLoadRecord(trantypeso, tranid);

	if(salesorderrec!=null && salesorderrec!='')
		ismultilineship=salesorderrec.getFieldValue('ismultishipto');

	nlapiLogExecution('Debug', 'Item Line Shipping', ismultilineship);

	var confirmLotToNS='N';
	//confirmLotToNS=GetConfirmLotToNS(itemloc);

	if(ismultilineship!='T')
	{

		var TransformRec;

		if(trantypeso=="salesorder")
		{
			TransformRec = nlapiTransformRecord('salesorder', tranid, 'itemfulfillment');
		}
		else
		{
			TransformRec = nlapiTransformRecord('transferorder', tranid, 'itemfulfillment');
		}

		nlapiLogExecution('Debug', 'TransformRec', TransformRec);

		for (var i = 0; i <qtylinearray.length; i++) {

			var solinenumber='';
			var qtyCommitted='';
			var boolfound = true;
			var vLotQty=0;
			var vLotTotQty=0;
			var vLotNo='';
			var vPrevbatch='';
			var serialsspaces = "";
			var itemname ='';

			if(TransformRec!=null)
				itemname = TransformRec.getLineItemText('item', 'item', i+1);

			nlapiLogExecution('Debug', 'itemname', itemname);

			if(itemname!=null && itemname!='')
			{

				solinenumber=qtylinearray[i][0];
				qtyCommitted=qtylinearray[i][1];

				nlapiLogExecution('Debug', "solinenumber", solinenumber);
				nlapiLogExecution('Debug', "qtyCommitted", qtyCommitted);

				var itemIndex=0;

				// To get the no of lines from item fulfillment record
				var IFLength = TransformRec.getLineItemCount('item'); 
				nlapiLogExecution('Debug', 'IFLength', IFLength);
				for (var j = 1; j <= IFLength; j++) {

					var item_id = TransformRec.getLineItemValue('item', 'item', j);			
					var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
					var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);
					if(trantypeso=="transferorder"){
						itemLineNo=parseFloat(itemLineNo)-1;
					}
					var NSUOM = TransformRec.getLineItemValue('item', 'units', j);  
					if (itemLineNo == solinenumber) {
						itemIndex=j;    
						nlapiLogExecution('Debug', "itemIndex", itemIndex);
					}

					nlapiLogExecution('Debug', "itemLineNo", itemLineNo);

				}
				if(itemIndex!=0)
				{

					var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
					var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
					var columns;
					var ItemType;

					if(item_id!= null && item_id != "")
					{
						columns = nlapiLookupField('item', item_id, [ 'recordType','custitem_ebizserialout' ]);
						ItemType = columns.recordType;
						nlapiLogExecution('Debug', 'ItemType', ItemType);
					}

					if(itemname!=null && itemname!='')
						itemname=itemname.replace(/ /g,"-");
					var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
					var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);
					nlapiLogExecution('Debug', 'itemloc2', itemloc2);

					var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);
					nlapiLogExecution('Debug', 'itemIndex', itemIndex);
					nlapiLogExecution('Debug', 'itemloc2', itemloc2);
					nlapiLogExecution('Debug', 'NSOrdUOM', NSOrdUOM);

					if(NSOrdUOM!=null && NSOrdUOM!="")
					{
						var veBizUOMQty=0;
						var vBaseUOMQty=0;

						if(vItemDimsArr!=null && vItemDimsArr.length>0)
						{
							for(z=0; z < vItemDimsArr.length; z++)
							{
								if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
								{	
									if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
										nlapiLogExecution('Debug', 'vBaseUOMQty', vBaseUOMQty);
									}

									if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
									{
										nlapiLogExecution('Debug', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
										veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
										nlapiLogExecution('Debug', 'veBizUOMQty', veBizUOMQty);
									}
								}
							}
							if(veBizUOMQty==null || veBizUOMQty=='')
							{
								veBizUOMQty=vBaseUOMQty;
							}
							if(veBizUOMQty!=0)
								qtyCommitted = Math.floor((parseFloat(qtyCommitted)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
							nlapiLogExecution('Debug', 'qtyCommitted', qtyCommitted);

							vLotQty =qtyCommitted;
						}
					}

					if (boolfound) {
						if(confirmLotToNS=='N')
						{
							vPrevbatch=itemname;
						}
						else
						{
							var ItemRec = nlapiLoadRecord(ItemType, item_id);
							if(ItemRec!=null && ItemRec!='')
							{
								var itemlineCount=ItemRec.getLineItemCount('numbers');
								var lotqoh=0;
								var vReqQty = qtyCommitted;

								for (var s = 1; parseFloat(vReqQty)>0 && s <= itemlineCount; s++) {
									lotqoh=ItemRec.getLineItemValue('numbers', 'quantityonhand', s);
									nlapiLogExecution('Debug', 'lotqoh',lotqoh);
									if(parseFloat(lotqoh)>=parseFloat(vReqQty))
									{
										vPrevbatch=ItemRec.getLineItemValue('numbers', 'serialnumber', s);
										vLotQty = vReqQty;
										vReqQty=0;

										nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);
										nlapiLogExecution('Debug', 'vLotQty',vLotQty);

										if(vLotNo!=null && vLotNo != "")
											vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
										else
											vLotNo = vPrevbatch +"(" + vLotQty + ")";
									}
									else
									{
										vPrevbatch=ItemRec.getLineItemValue('numbers', 'serialnumber', s);
										vLotQty = lotqoh;
										vReqQty=parseFloat(vReqQty)-parseFloat(lotqoh);

										nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);
										nlapiLogExecution('Debug', 'vLotQty',vLotQty);

										if(vLotNo!=null && vLotNo != "")
											vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
										else
											vLotNo = vPrevbatch +"(" + vLotQty + ")";
									}
								}

							}
						}

						TransformRec.selectLineItem('item', itemIndex);
						TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
						nlapiLogExecution('Debug', 'qtyCommitted', qtyCommitted);
						TransformRec.setCurrentLineItemValue('item', 'quantity', qtyCommitted);
						nlapiLogExecution('Debug', 'itemloc2', itemloc2);
						TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

						nlapiLogExecution('Debug', 'ItemType', ItemType);

						if (ItemType == "lotnumberedinventoryitem") {

							nlapiLogExecution('Debug', 'vLotNo', vLotNo);									
							TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);							
						}
						else if(ItemType == "lotnumberedassemblyitem") 
						{
							TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
							nlapiLogExecution('Debug', 'vLotNo', vLotNo);
						}
						else if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") {
							TransformRec.setCurrentLineItemValue('item','serialnumbers', serialsspaces);
						}
						vLotNo="";
					}
					TransformRec.commitLineItem('item');

				}
				else
				{
					nlapiLogExecution('Debug', 'itemIndex in else', itemIndex);


					if(itemIndex != 0)
					{
						TransformRec.selectLineItem('item', itemIndex);


						TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');


						TransformRec.commitLineItem('item');
					}


				}
			}
		}
		if(TransformRec!=null)
		{
			TransformRecId = nlapiSubmitRecord(TransformRec, true);
			nlapiLogExecution('Debug', 'After Submit', '');

		}
	}
	nlapiLogExecution('Debug', 'TransformRecId', TransformRecId);
	nlapiLogExecution('Debug', 'Out of createItemFulfillmentforException', trantypeso);
	return TransformRecId;
}

function createNewFulfillmentOrder(SalesOrderInternalId,vebizOrdNo,qtylinearray,IFRefNo)
{	
	nlapiLogExecution('Debug', 'Into createNewFulfillmentOrder');
	var soname = vebizOrdNo.split('.')[0];
	var fullFillmentorderno=fulfillmentOrderDetails(SalesOrderInternalId,soname);
	nlapiLogExecution('Debug', 'fullFillmentorderno',fullFillmentorderno);
	for(var i=0;i<qtylinearray.length;i++)
	{
		var PickExpFulfillOrdLineRec=nlapiCreateRecord('customrecord_ebiznet_ordline');

		PickExpFulfillOrdLineRec.setFieldValue('name', SalesOrderInternalId);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ebiz_linesku', qtylinearray[i][2]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_lineord', fullFillmentorderno);			//new fullfillord
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ordline', qtylinearray[i][0]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ord_qty',parseFloat(qtylinearray[i][1]).toFixed(5));	//vOrdQty2
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linepackcode', qtylinearray[i][3]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linesku_status', qtylinearray[i][4]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linestatus_flag', '25');					//status.outbound.edit
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_lineuom_id', qtylinearray[i][5]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ns_ord', SalesOrderInternalId);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_order_type', qtylinearray[i][6]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_customer', qtylinearray[i][7]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_carrier', qtylinearray[i][8]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ordline_wms_location', qtylinearray[i][9]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ordline_company', qtylinearray[i][10]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_wmscarrier', qtylinearray[i][11]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_order_priority', qtylinearray[i][12]);
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linenotes2', 'Created by Confirm picks qty Exception');
		PickExpFulfillOrdLineRec.setFieldValue('custrecord_nsconfirm_ref_no', IFRefNo);

		var recordId = nlapiSubmitRecord(PickExpFulfillOrdLineRec);
	}

	nlapiLogExecution('Debug', 'Out of createNewFulfillmentOrder');
}

function fulfillmentOrderDetails(salesOrderId,salesordername)
{
	var salesOrderArray = new Array();
	var searchFulfillment =  getFullFillment(salesOrderId);
	//nlapiLogExecution('Debug','searchFulfillment :', searchFulfillment);

	//If searchfulfillment is null it indicates Its a New SO. 
	if(searchFulfillment == null){
		var fulfillmentNumber=salesordername +".1";
	}
	else{
		for(var i=0; i< searchFulfillment.length;i++)
		{
			var fullfillmentlineord = searchFulfillment[i].getValue('custrecord_lineord');
			var lineordsplit = fullfillmentlineord.split('.');
			salesOrderArray[i] = lineordsplit[1];
		}
		//MaxValue returns the max fullfillment order number
		var maximumValue = MaxValue(salesOrderArray);

		//adding 1 to the max fullfillment order number to get the new fullfillment order no.
		var fulfillmentValue = parseFloat(maximumValue) + parseFloat(1);
		nlapiLogExecution('Debug','fulfillment Value :', fulfillmentValue);

		//concatinating the fulfillmentvalue to salesorderid to get new fullfillment no.
		var fulfillmentNumber =  salesordername + "." + fulfillmentValue;
		//nlapiLogExecution('Debug','fulfillmentNumber :', fulfillmentNumber);
	}
	return fulfillmentNumber;
}

function getFullFillment(soId)
{
	var ordlinefilters = new Array();
	ordlinefilters[0] = new nlobjSearchFilter('name', null, 'is', soId);
	var ordlinecolumns = new Array();  
	ordlinecolumns[0] = new nlobjSearchColumn('custrecord_lineord').setSort();        
	ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ordline');
	ordlinecolumns[2] = new nlobjSearchColumn('custrecord_ord_qty'); // fulfillment ordered quantity
	var ordlinesearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);
	return ordlinesearchresults;
}

function MaxValue(array)
{
	var maximumNumber = array[0];
	for (var i = 0; i < array.length; i++) 
	{
		if (array[i] > maximumNumber) 
		{
			maximumNumber = array[i];
		}
	}
	return maximumNumber;
}

function CreatePACKTaskBulk(DataRecArr)
{
	nlapiLogExecution('Debug', 'Into CreatePACKTaskBulk', TimeStampinSec());

	var oldcontainer='';
	var PROSeqNo="";
	var BOLSeqNo="",childBOLSeqNo="";

	var context = nlapiGetContext();

	//DataRecArr.push(new DataRec(compid,siteid,ebizorderno,orderno,containerno,packstation,ebizcntrlno,item));

	//CreatePACKTask(DataRecord.compid,DataRecord.DataRecSiteId,DataRecord.DataRecEbizOrdNo,DataRecord.DataRecOrdNo,
	//DataRecord.DataRecContainer,14,DataRecord.DataRecPackStation,DataRecord.DataRecPackStation,
	//28,DataRecord.DataRecEbizControlNo,DataRecord.DataRecItem);

	var filter_PRO_Wave=new Array();
	filter_PRO_Wave.push(new nlobjSearchFilter('custrecord_trantype',null,'is','PRO'));
	var column_PRO_Wave=new Array();
	column_PRO_Wave[0]=new nlobjSearchColumn('custrecord_seqno');

	var searchrec_PRO_Wave=nlapiSearchRecord('customrecord_ebiznet_wave',null,filter_PRO_Wave,column_PRO_Wave);

	if(searchrec_PRO_Wave!=null&&searchrec_PRO_Wave!="")
	{
		PROSeqNo=searchrec_PRO_Wave[0].getValue('custrecord_seqno');
		var PROIntId=searchrec_PRO_Wave[0].getId();
		PROSeqNo++;
		nlapiLogExecution('Debug', 'PROSeqNo',PROSeqNo);
		nlapiSubmitField('customrecord_ebiznet_wave', PROIntId, 'custrecord_seqno', PROSeqNo);
	}
	else
		nlapiLogExecution('Debug','Exception in fetching PRO Record from WAVE Table','');

	nlapiLogExecution('ERROR','DataRecArr[0].DataRecSiteId',DataRecArr[0].DataRecSiteId);
	nlapiLogExecution('ERROR','DataRecArr[0].DataRecSiteId',DataRecArr[0].DataRectotalweight);




	BOLSeqNo=GenerateLable('Nautilus, Inc.',DataRecArr[0].DataRecSiteId);
	childBOLSeqNo=GenerateLable('Nautilus, Inc.',DataRecArr[0].DataRecSiteId);


	//DataRecArr.push(new DataRec(compid,siteid,ebizorderno,orderno,containerno,packstation,ebizcntrlno,item,totweight));

	var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); 

	for (var k = 0; k < DataRecArr.length; k++)
	{	
		var vWeight=0;
		var DataRecord=DataRecArr[k];
		var containerlpno =  DataRecord.DataRecContainer;
		var SizeId = DataRecord.DataRecContainerSizeId;

		nlapiLogExecution('ERROR','SizeId',SizeId);
		var tarefilters = new Array();
		if(SizeId != "" && SizeId != null && SizeId!='null' )
		{		
			tarefilters.push(new nlobjSearchFilter('internalid', null, 'anyof', SizeId));
		}		
		tarefilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var tarecolumns = new Array();		
		tarecolumns[0] = new nlobjSearchColumn('custrecord_tareweight');		

		var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, tarefilters, tarecolumns);

		nlapiLogExecution('ERROR','ContainerSearchResults',ContainerSearchResults);

		var tareweight = 0;
		if(ContainerSearchResults!=null && ContainerSearchResults!='')
		{
			tareweight = ContainerSearchResults[0].getValue('custrecord_tareweight');
		}


		nlapiLogExecution('ERROR','DataRecArr[0].DataRecSiteId123',DataRecord.DataRectotalweight);
		if(oldcontainer!=containerlpno)
		{

			for (var Z = 0; Z < DataRecArr.length; Z++)
			{
				var vDataRecord=DataRecArr[Z];
				var vContainerlpno =  vDataRecord.DataRecContainer;
				if(vContainerlpno == containerlpno)
				{
					nlapiLogExecution('ERROR','vWeight',vWeight);
					vWeight = parseFloat(vWeight)+parseFloat(vDataRecord.DataRectotalweight);
				}

			}

			nlapiLogExecution('ERROR','tareweight',tareweight);
			nlapiLogExecution('ERROR','vWeight',vWeight);

			var totalweight = parseFloat(tareweight) + parseFloat(vWeight) ; 

			var filtersmlp = new Array();
			filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', containerlpno);

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp,null);

			if (SrchRecord != null && SrchRecord.length > 0) 
			{
				nlapiLogExecution('DEBUG', 'LP FOUND');
				var LprecordId = SrchRecord[0].getId();

				var vtransaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', LprecordId);
				vtransaction.setFieldValue('custrecord_ebiz_lpmaster_totwght', totalweight);
				nlapiSubmitRecord(vtransaction, false, true);
			}



			parent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');	 
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', DataRecord.DataRecOrdNo);
			if(DataRecord.DataRecCompId!=null && DataRecord.DataRecCompId!='')
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', DataRecord.DataRecCompId);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_site_id', DataRecord.DataRecSiteId);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', DataRecord.DataRecEbizControlNo);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', DataRecord.DataRecEbizOrdNo);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', DataRecord.DataRecContainer);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', '14');
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', DataRecord.DataRecPackStation);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', DataRecord.DataRecPackStation);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_customer', DataRecord.DataRecCustomer);

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '28');
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date', DateStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_current_date', DateStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', TimeStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordtime', TimeStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizuser', context.getUser());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', context.getUser());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_pack_confirmed_date', DateStamp());

			nlapiLogExecution('ERROR','totalweight',totalweight);
			//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', DataRecord.DataRectotalweight);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', totalweight);


			if(DataRecord.DataRecItem!=null && DataRecord.DataRecItem!='')
			{
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', DataRecord.DataRecItem);
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', DataRecord.DataRecItem);
			}

			if(BOLSeqNo != null && BOLSeqNo != '')
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_bol', BOLSeqNo.toString());
			if(childBOLSeqNo != null && childBOLSeqNo != '')
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sub_bol', childBOLSeqNo.toString());

			var ProNo="DYNA"+addLeadingZeros(PROSeqNo,7);

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_pro', ProNo.toString());

			parent.commitLineItem('recmachcustrecord_ebiz_ot_parent');

			oldcontainer = containerlpno;

		}
	}

	nlapiSubmitRecord(parent); 

	nlapiLogExecution('Debug', 'Out of CreatePACKTaskBulk', TimeStampinSec());
}

function getKitflaginopentask(vsalesOrderId)
{
	nlapiLogExecution('Debug','vsalesOrderId',vsalesOrderId);
	var kitErrorFlag;
	var filters = new Array();

	//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is' ,vsalesOrderId));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_kit_exception_flag');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		nlapiLogExecution('Debug','searchresults',searchresults.length);
		for(var z=0; z<searchresults.length;z++) 
		{	
			kitErrorFlag=searchresults[z].getValue('custrecord_kit_exception_flag');
			nlapiLogExecution('Debug','kitErrorFlag',kitErrorFlag);
			if(kitErrorFlag=='T')
			{
				nlapiLogExecution('Debug','kitErrorFlag',kitErrorFlag);
				return kitErrorFlag;
			}
		}
	}

}

function UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,ErrorCode)
{
	try
	{
		nlapiLogExecution('Debug',"SalesOrderInternalId",SalesOrderInternalId);
		nlapiLogExecution('Debug',"vebizOrdNo",vebizOrdNo);
		nlapiLogExecution('Debug',"vcontainerLp",vcontainerLp);
		nlapiLogExecution('Debug',"ErrorCode",ErrorCode);

		var filters= new Array();

		if(SalesOrderInternalId != null && SalesOrderInternalId != '')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));

		filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty'));
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0'));
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0.0'));

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
		if(searchresult!=null&&searchresult!="")
		{
			for ( var count = 0; count < searchresult.length; count++)
			{
				var recid=searchresult[count].getId();
				nlapiLogExecution('Debug',"recid",recid);
				nlapiSubmitField("customrecord_ebiznet_trn_opentask",recid,"custrecord_ebiz_error_log",ErrorCode);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('Debug',"Exception in UpdateOpenTaskWithExceptionValue",e);
	}
}

function isOrderClosedAndGetDets(vsalesOrderId)
{
	nlapiLogExecution('Debug', 'Into isOrderClosed',vsalesOrderId);	
	//nlapiLogExecution('Debug', 'Into trantype',trantype);	
	var vstatus = '';
	//var trantype = nlapiLookupField('transaction', vsalesOrderId, 'recordType');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', vsalesOrderId));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');	
	columns[1] = new nlobjSearchColumn('status');	
	columns[2] = new nlobjSearchColumn('paymenteventresult');
	columns[3] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
	columns[4] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	columns[5] = new nlobjSearchColumn('custbody_nswmssoordertype');
	columns[6] = new nlobjSearchColumn('shipmethod');
	columns[7] = new nlobjSearchColumn('recordType');

	var salesOrderDetails = new nlapiSearchRecord('transaction', null, filters, columns);

	nlapiLogExecution('Debug', 'Out of isOrderClosed');

	return salesOrderDetails;
}


function ISKitItemcompletlyPicked(itemvalue,vebizOrdNo,vWMSCarrier)
{
	try
	{
		nlapiLogExecution('ERROR','ISKitItemcompletlyPicked',itemvalue+"/"+vebizOrdNo+"/"+vWMSCarrier);
		var totalRes="";
		var filters= new Array();					
		filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
		//case# 20148736 starts wms_status_flag=28 is commented and wms_status_flag=8,9 is uncommented
//		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));// PACK COMPLETE
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8','9']));//9 //Picks generated or Picks confirmed
		//case# 20148736 ends
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0')); 
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0.0'));
		if(vWMSCarrier != null && vWMSCarrier != '')
			filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier
		if(itemvalue != null && itemvalue != '')
			filters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', itemvalue));

		var column =new Array(); 
		column[0] = new nlobjSearchColumn('custrecord_act_qty');
		column[1] = new nlobjSearchColumn('custrecord_line_no');
		column[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
		column[3]= new nlobjSearchColumn('custrecord_act_end_date');
		column[4]= new nlobjSearchColumn('internalid');
		column[5]= new nlobjSearchColumn('name');
		column[6]= new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		column[7]= new nlobjSearchColumn('custrecord_ebiz_order_no');
		column[8]= new nlobjSearchColumn('custrecord_serial_no');
		column[9]= new nlobjSearchColumn('custrecord_batch_no');					
		column[10]= new nlobjSearchColumn('custrecord_total_weight');
		column[11]= new nlobjSearchColumn('custrecord_totalcube');
		column[12]= new nlobjSearchColumn('custrecord_parent_sku_no');
		column[13]= new nlobjSearchColumn('custrecord_sku');
		column[1].setSort();
		column[9].setSort();

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

		if(searchresult!=null&&searchresult!="")
		{
			totalRes="false";
		}
		else
		{
			totalRes="true";
		}
		return totalRes;
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception in ISKitItemcompletlyPicked",exp);
	}
}

function getAutoShipRule(vsalesOrderId)
{
	nlapiLogExecution('Debug','Into getAutoShipRule');

	var vCustomer,vOrderType,vShipMethod,vOrderPriority,vCarrier;

	var vOrderDetails = getAutoPackFlagforOrdType(vsalesOrderId);	

	if(vOrderDetails!=null && vOrderDetails!='')
	{
		vCustomer = vOrderDetails[0].getValue('entity');
		vOrderType = vOrderDetails[0].getValue('custbody_nswmssoordertype');
		vShipMethod = vOrderDetails[0].getValue('shipmethod');
		vOrderPriority = vOrderDetails[0].getValue('custbody_nswmspriority');
		vCarrier='';

		if(vShipMethod!=null && vShipMethod!='')
		{
			var filterscarr = new Array();
			var columnscarr = new Array();

			filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',vShipMethod));
			filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

			columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
			columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
			columnscarr[2] = new nlobjSearchColumn('id');

			var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
			if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
			{
				vCarrier = searchresultscarr[0].getId();
			}
		}
	}

	var AutoShipDet = getAutoShipStrategies(vCustomer,vOrderType,vOrderPriority,vCarrier);
	if(AutoShipDet!=null && AutoShipDet!='' && AutoShipDet.length>0)
	{
		var vshipruleid=AutoShipDet[7];
		nlapiLogExecution('Debug','vshipruleid',vshipruleid);

		nlapiLogExecution('DEBUG', 'Invoking Schedule Script Starts');	

		var param = new Array();
		param['custscript_sointrid'] = vsalesOrderId;
		param['custscript_shipruleid'] = vshipruleid;
		nlapiScheduleScript('customscript_ebiz_autoshiporders', null,param);
	}
	nlapiLogExecution('Debug','Out of getAutoShipRule');
}

function getAutoShipStrategies(vCustomer,vOrderType,vOrderPriority,vCarrier)
{
	nlapiLogExecution('Debug','Into getAutoShipStrategies');

	var str = 'vCustomer. = ' + vCustomer + '<br>';
	str = str + 'vOrderType. = ' + vOrderType + '<br>';
	str = str + 'vOrderPriority. = ' + vOrderPriority + '<br>';
	str = str + 'vCarrier. = ' + vCarrier + '<br>';

	nlapiLogExecution('Debug', 'Function Parameters', str);

	var arrAutoShip = new Array();

	var filters = new Array();
	var columns = new Array();

	if(vCustomer!=null && vCustomer!='' && vCustomer!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipcustomer', null, 'anyof',['@NONE@',vCustomer]));
	else
		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipcustomer', null, 'anyof',['@NONE@']));

	if(vOrderType!=null && vOrderType!='' && vOrderType!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_ship_ordtype', null, 'anyof',['@NONE@',vOrderType]));
	else
		filters.push(new nlobjSearchFilter('custrecord_ebiz_ship_ordtype', null, 'anyof',['@NONE@']));

	if(vOrderPriority!=null && vOrderPriority!='' && vOrderPriority!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_ship_ord_priority', null, 'anyof',['@NONE@',vOrderPriority]));
	else
		filters.push(new nlobjSearchFilter('custrecord_ebiz_ship_ord_priority', null, 'anyof',['@NONE@']));

	if(vCarrier!=null && vCarrier!=''&& vCarrier!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipcarrier_id', null, 'anyof',['@NONE@',vCarrier]));
	else
		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipcarrier_id', null, 'anyof',['@NONE@']));

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_shiprule_id');	
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_shiprule_name');	
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_auto_buildship_flag');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_ship_units_level');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_auto_trailer_load_flag');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_trailer_level');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_auto_trailer_depart_flag');
//	columns[6] = new nlobjSearchColumn('custrecord_auto_trailer_depart_flag');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_autoship_seq').setSort();// case# 201412501

	var searchresults = nlapiSearchRecord('customrecord_ebiz_autoship_strategies', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		nlapiLogExecution('Debug','# of AutoShipStrategies',searchresults.length);

		for(i=0;i<searchresults.length;i++)
		{
			var vAutoShipUnits = searchresults[i].getValue('custrecord_ebiz_auto_buildship_flag');
			var vShipUnitsLvl = searchresults[i].getValue('custrecord_ebiz_ship_units_level');
			var vAutoLoad = searchresults[i].getValue('custrecord_ebiz_auto_trailer_load_flag');
			var vTrailerLvl = searchresults[i].getValue('custrecord_ebiz_trailer_level');
			var vAutoDepart = searchresults[i].getValue('custrecord_ebiz_auto_trailer_depart_flag');
			//case 20147994  
			//var vAutoDepart = searchresults[i].getValue('custrecord_auto_trailer_depart_flag');
			//case end
			var vShipRuleId = searchresults[i].getValue('custrecord_ebiz_shiprule_id');
			var vShipRuleName = searchresults[i].getValue('custrecord_ebiz_shiprule_name');
			var vShipRuleIntrId = searchresults[i].getId();

			if(vAutoShipUnits == 'T' || vAutoLoad=='T' || vAutoDepart=='T')
			{
				arrAutoShip[0] = vAutoShipUnits;
				arrAutoShip[1] = vShipUnitsLvl;
				arrAutoShip[2] = vAutoLoad;
				arrAutoShip[3] = vTrailerLvl;
				arrAutoShip[4] = vAutoDepart;
				arrAutoShip[5] = vShipRuleId;
				arrAutoShip[6] = vShipRuleName;
				arrAutoShip[7] = vShipRuleIntrId;

				nlapiLogExecution('Debug','Out of getAutoShipStrategies',arrAutoShip);

				return arrAutoShip;
			}
		}
	}

	nlapiLogExecution('Debug','Out of getAutoShipStrategies',arrAutoShip);
	return arrAutoShip;
}

function UpdateAllOpenTaskRecordsWithNSID(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineArray,vcontainerLp,QuilfiedItemLineKitArray,FULFILLMENTATORDERLEVEL)
{
	try
	{
		nlapiLogExecution("ERROR","QuilfiedItemArray",QuilfiedItemArray);
		nlapiLogExecution("ERROR","QuilfiedItemLineArray",QuilfiedItemLineArray);
		nlapiLogExecution("ERROR","QuilfiedItemLineKitArray",QuilfiedItemLineKitArray);
		nlapiLogExecution("ERROR","FULFILLMENTATORDERLEVEL",FULFILLMENTATORDERLEVEL);
		nlapiLogExecution("ERROR","vcontainerLp",vcontainerLp);

		var filters= new Array();					

		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));// PACK COMPLETE
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0')); 
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0.0'));
		if(vWMSCarrier != null && vWMSCarrier != '')
			filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier
		if(QuilfiedItemArray != null && QuilfiedItemArray != '')
			filters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', QuilfiedItemArray));
		if(FULFILLMENTATORDERLEVEL!='Y')
		{
			if(vcontainerLp!=null&&vcontainerLp!="")
				filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
		}
		else
			filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		var column=new Array();
		column[0]=new nlobjSearchColumn("custrecord_line_no");

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

		if(searchresult!=null&&searchresult!="")
		{
			for(vcount=0;vcount<searchresult.length;vcount++)
			{
				var actlineno=searchresult[vcount].getValue("custrecord_line_no");
//				nlapiLogExecution("ERROR","actlineno",actlineno);
				for(vlinecount=0;vlinecount<QuilfiedItemLineArray.length;vlinecount++)
				{
//					nlapiLogExecution("ERROR","QuilfiedItemLineArray[vlinecount]",QuilfiedItemLineArray[vlinecount]);
					if(actlineno==QuilfiedItemLineArray[vlinecount])
					{
						var recid=searchresult[vcount].getId();
						nlapiSubmitField(searchresult[vcount].getRecordType(), recid, 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
					}
				}
			}
			if(QuilfiedItemLineKitArray!=null&&QuilfiedItemLineKitArray!="")
				UpdateAllOpenTaskRecordsWithNSID_KitItem(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineKitArray,vcontainerLp);
		}
		nlapiLogExecution("ERROR","UpdateAllOpenTaskRecordsWithNSID",QuilfiedItemArray+"/"+vebizOrdNo+"/"+vWMSCarrier+"/"+TransformRecId);
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","exception in UpdateAllOpenTaskRecordsWithNSID",exp);
	}
}

function UpdateAllOpenTaskRecordsWithNSID_KitItem(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineArray,vcontainerLp)
{
	try
	{
		nlapiLogExecution("ERROR","QuilfiedItemArray",QuilfiedItemArray);
		nlapiLogExecution("ERROR","QuilfiedItemLineArray",QuilfiedItemLineArray);

		var filters= new Array();					
		filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));// PACK COMPLETE
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0')); 
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0.0'));
		if(vWMSCarrier != null && vWMSCarrier != '')
			filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier
		if(QuilfiedItemArray != null && QuilfiedItemArray != '')
			filters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', QuilfiedItemArray));
		/*if(vcontainerLp!=null&&vcontainerLp!="")
			filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));*/
		var column=new Array();
		column[0]=new nlobjSearchColumn("custrecord_line_no");

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

		if(searchresult!=null&&searchresult!="")
		{
			for(vcount=0;vcount<searchresult.length;vcount++)
			{
				var actlineno=searchresult[vcount].getValue("custrecord_line_no");
//				nlapiLogExecution("ERROR","actlineno",actlineno);
				for(vlinecount=0;vlinecount<QuilfiedItemLineArray.length;vlinecount++)
				{
//					nlapiLogExecution("ERROR","QuilfiedItemLineArray[vlinecount]",QuilfiedItemLineArray[vlinecount]);
					if(actlineno==QuilfiedItemLineArray[vlinecount])
					{
						var recid=searchresult[vcount].getId();
						nlapiSubmitField(searchresult[vcount].getRecordType(), recid, 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
					}
				}
			}
		}
		nlapiLogExecution("ERROR","UpdateAllOpenTaskRecordsWithNSID_KitItem",QuilfiedItemArray+"/"+vebizOrdNo+"/"+vWMSCarrier+"/"+TransformRecId);
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","exception in UpdateAllOpenTaskRecordsWithNSID",exp);
	}
}





//for scheduler
function ItemFulfillmentSchedular(type)
{
	nlapiLogExecution('Debug', 'Scheduler version', type);

	var SalesOrderInternalId='';
	var vebizOrdNo='';
	var vWMSCarrier ='';
	var confirmLotToNS =''
		var nsrefno ='';
	var vcontainerLp ='';
	var wavenumber ='';
	var packtaskid='';
	var itemloc ='';
	var fulfillmenttask ='';
	var packtaskid='';
	var vcontainerLp='';
	try{
		var context = nlapiGetContext(); 
		SalesOrderInternalId = context.getSetting('SCRIPT', 'custscript_soorderinternalid');
		vebizOrdNo = context.getSetting('SCRIPT', 'custscript_ebizordno');
		vWMSCarrier = context.getSetting('SCRIPT', 'custscript_wmscarrier');
		confirmLotToNS = context.getSetting('SCRIPT', 'custscript_confirmtons');
		wavenumber = context.getSetting('SCRIPT', 'custscript_ebizwavenumber');
		vcontainerLp = context.getSetting('SCRIPT', 'custscript_containerlp');
		itemloc = context.getSetting('SCRIPT', 'custscript_itemloc');
		fulfillmenttask = context.getSetting('SCRIPT', 'custscript_fulfillmenttask');
		ismultilineship = context.getSetting('SCRIPT', 'custscript_ismultilineship');
		packtaskid = context.getSetting('SCRIPT', 'custscript_recid');


		var str = 'type. = ' + type + '<br>';
		str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
		str = str + 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
		str = str + 'vWMSCarrier. = ' + vWMSCarrier + '<br>';
		str = str + 'confirmLotToNS. = ' + confirmLotToNS + '<br>';
		str = str + 'wavenumber. = ' + wavenumber + '<br>';
		str = str + 'itemloc. = ' + itemloc + '<br>';
		str = str + 'fulfillmenttask. = ' + fulfillmenttask + '<br>';
		str = str + 'ismultilineship. = ' + ismultilineship + '<br>';
		str = str + 'packtaskid. = ' + packtaskid + '<br>';
		str = str + 'vcontainerLp. = ' + vcontainerLp + '<br>';

		nlapiLogExecution('Debug', 'Parameter Details', str);

		if(type != 'aborted')
		{
			var trantypeso = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
			var salesorderrec= nlapiLoadRecord(trantypeso, SalesOrderInternalId);

			if(ismultilineship!='T')
			{
				createItemFulfillment_SCH(SalesOrderInternalId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsrefno,vcontainerLp,wavenumber,
						itemloc,fulfillmenttask,packtaskid,salesorderrec);


			}
			else
			{
				createItemFulfillmentMultiLineShipping_SCH(SalesOrderInternalId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsrefno,vcontainerLp,
						wavenumber,itemloc,fulfillmenttask,salesorderrec);
			}
		}

	}
	catch(exp)
	{

		nlapiLogExecution('ERROR','exception in ItemFulfillmentSCH',exp);
	}


}



function createItemFulfillment_SCH(SalesOrderInternalId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsrefno,vcontainerLp,wavenumber,
		itemloc,fulfillmenttask,packtaskid,salesorderrec)
{	
	nlapiLogExecution('Debug', 'Into createItemFulfillment_SCH',TimeStampinSec());

	var str4 = 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str4 = str4 + 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
	str4 = str4 + 'vWMSCarrier. = ' + vWMSCarrier + '<br>';
	str4 = str4 + 'confirmLotToNS. = ' + confirmLotToNS + '<br>';	
	str4 = str4 + 'nsrefno. = ' + nsrefno + '<br>';
	str4 = str4 + 'vcontainerLp. = ' + vcontainerLp + '<br>';
	str4 = str4 + 'wavenumber. = ' + wavenumber + '<br>';
	str4 = str4 + 'itemloc. = ' + itemloc + '<br>';
	str4 = str4 + 'fulfillmenttask. = ' + fulfillmenttask + '<br>';
	str4 = str4 + 'packtaskid. = ' + packtaskid + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str4);

	var userId = nlapiGetUser();
	//Post Itemfulfillment at Order level? (Y or N)
	//case# 20148056 starts
	var FULFILLMENTATORDERLEVEL = getSystemRuleValue('IF001',itemloc);
	//case# 20148056 ends

	var lineArray = new Array();
	var filterLineArray = new Array();
	var filterItemArray =new Array(); // For distinct item ids
	//var AddTotalArray = new Array();
	var itemListArray= new Array();
	var ItemArray= new Array();
	var filter= new Array();
	var TransformRecId = null;

	if(SalesOrderInternalId != null && SalesOrderInternalId != '')
		filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 

	filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); //PICK

	if(fulfillmenttask=='PICK')
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['9']));//9 //Picks generated
	else
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8','9']));//9 //Picks generated or Picks confirmed

	if(vWMSCarrier != null && vWMSCarrier != '')
		filter.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier

	if(FULFILLMENTATORDERLEVEL!='Y')
	{
		nlapiLogExecution('Debug', 'FULFILLMENTATORDERLEVEL inside',FULFILLMENTATORDERLEVEL);
		nlapiLogExecution('Debug', 'vcontainerLp inside',vcontainerLp);
		filter.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
	}
	else
	{
		filter.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
	}

	filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
	filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0.0'));
	filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

	var columns= new Array(); 

	columns[0] = new nlobjSearchColumn('custrecord_act_qty');
	columns[1] = new nlobjSearchColumn('custrecord_line_no');
	columns[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[3]= new nlobjSearchColumn('custrecord_act_end_date');

	var opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);

	nlapiLogExecution('Debug', 'opentaskordersearchresult',opentaskordersearchresult);

	if(opentaskordersearchresult==null || opentaskordersearchresult=='') // To make sure there is not records exists with Picks generated stage for the order
	{
		nlapiLogExecution('Debug', 'No Open Records. All are Pack Confirmaed');
		var QuilfiedItemArray=new Array();
		var QuilfiedItemLineArray=new Array();
		var QuilfiedItemLineKitArray=new Array();
		var filters= new Array();					

		if(SalesOrderInternalId != null && SalesOrderInternalId != '')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 

		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
		if(fulfillmenttask=='PICK')
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8']));//8 //Picks confirmed
		else
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));//28//Pack Complete
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
		/*** Below change merged from Factory mation production account on 1st March 2013 by Ganesh ***/
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 

		filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		/***Upto here***/
		if(vWMSCarrier != null && vWMSCarrier != '')
			filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier

		if(FULFILLMENTATORDERLEVEL!='Y')
		{
			nlapiLogExecution('Debug', 'vcontainerLp inside if',vcontainerLp);
			filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
		}
		else
		{
			filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		}

		var column =new Array(); 

		column[0] = new nlobjSearchColumn('custrecord_act_qty');
		column[1] = new nlobjSearchColumn('custrecord_line_no');
		column[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
		column[3]= new nlobjSearchColumn('custrecord_act_end_date');
		column[4]= new nlobjSearchColumn('internalid');
		column[5]= new nlobjSearchColumn('name');
		column[6]= new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		column[7]= new nlobjSearchColumn('custrecord_ebiz_order_no');
		column[8]= new nlobjSearchColumn('custrecord_serial_no');
		column[9]= new nlobjSearchColumn('custrecord_batch_no');					
		column[10]= new nlobjSearchColumn('custrecord_total_weight');
		column[11]= new nlobjSearchColumn('custrecord_totalcube');
		column[12]= new nlobjSearchColumn('custrecord_parent_sku_no');
		column[13]= new nlobjSearchColumn('custrecord_sku');
		column[1].setSort();
		column[9].setSort();

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

		var DoLineId="";
		var totalWeight=0.0;var totalcube=0.0;
		if(searchresult !=null && searchresult !='') // SearchResult contains all the open task records of task type PICK and status flag PACK COMPLETE
		{
			nlapiLogExecution('Debug', 'searchresult length',searchresult.length);
			var sum =0;
			var actqty="";
			var linenumber="";
			var skuno="";
			var vBatchno="";
			var vserialno="";

			DoLineId=searchresult[0].getValue('custrecord_ebiz_cntrl_no');

			nlapiLogExecution('Debug', 'searchresult.length',searchresult.length);

			for (l = 0; l < searchresult.length; l++) 
			{  
				if(SalesOrderInternalId==null || SalesOrderInternalId=='')
					SalesOrderInternalId=searchresult[l].getValue('custrecord_ebiz_order_no');	

				itemListArray[l] = new Array();

				actqty=searchresult[l].getValue('custrecord_act_qty');
				linenumber=searchresult[l].getValue('custrecord_line_no');
				skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
				/***Below code has been merged from Factory mation prodcution by Ganesh on 1st March 2013***/					
				var parentskuno=searchresult[l].getValue('custrecord_parent_sku_no');
				var skuname=searchresult[l].getText('custrecord_parent_sku_no');
				var skuvalue=searchresult[l].getValue('custrecord_ebiz_sku_no');
				var skutext=searchresult[l].getText('custrecord_sku');
				/***Upto here***/
				vBatchno = searchresult[l].getValue('custrecord_batch_no');
				vserialno = searchresult[l].getValue('custrecord_serial_no');
				if(searchresult[l].getValue('custrecord_total_weight')!=null && searchresult[l].getValue('custrecord_total_weight')!='')
				{
					totalWeight=totalWeight+parseFloat(searchresult[l].getValue('custrecord_total_weight'));
				}
				if(searchresult[l].getValue('custrecord_totalcube')!=null && searchresult[l].getValue('custrecord_totalcube')!='')
				{
					totalcube=totalcube+parseFloat(searchresult[l].getValue('custrecord_totalcube'));
				}
				itemListArray[l][0]=actqty;
				itemListArray[l][1]=skuno;
				itemListArray[l][2]=linenumber;
				itemListArray[l][3]=vBatchno;
				itemListArray[l][4]=vserialno;
				/***Below code has been merged from factory mation production by Ganesh on 1st March 2013***/
				itemListArray[l][5]=skuname;
				itemListArray[l][6]=parentskuno;
				itemListArray[l][7]=skuvalue;
				itemListArray[l][8]=skutext;
				/***Upto here ***/
				ItemArray.push(skuno);
				lineArray.push(linenumber);
			}

			// The below logic is to get all the distinct line nos to filterLineArray . This contails distinct lines nos for the order.
			label:for(var i=0; i<lineArray.length;i++ )
			{  
				for(var j=0; j<filterLineArray.length;j++ )
				{
					if(filterLineArray [j]==lineArray [i]) 
						continue label;
				}
				filterLineArray [filterLineArray .length] = lineArray [i];
			}

			filterLineArray.sort(function(a,b){return a - b;});

			// The below logic is to get all the distinct Item ids filterItemArray . This contains distinct Item ids for the order.
			label2:for(var m=0; m<ItemArray.length;m++ )
			{  
				for(var p=0; p<filterItemArray.length;p++ )
				{
					if(filterItemArray[p]==ItemArray[m]) 
						continue label2;
				}
				filterItemArray[filterItemArray.length] = ItemArray[m];
			}

			// To fetch Item dims from Item Array
			var vItemDimsArr = geteBizItemDimensionsArray(filterItemArray);
			if(vItemDimsArr !=null && vItemDimsArr != "")
			{
				nlapiLogExecution('Debug', 'vItemDimsArr.length', vItemDimsArr.length);
			}	

			var TransformRec;

			var filters= new Array();

			filters.push(new nlobjSearchFilter('internalid', null,'is', SalesOrderInternalId));
			filters.push(new nlobjSearchFilter('mainline',null,'is','T'));

			var column =new Array(); 

			column[0] = new nlobjSearchColumn('custbody_nswmssoordertype');
			column[1] = new nlobjSearchColumn('custbody_total_weight');

			var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
			nlapiLogExecution('Debug', 'trantype', trantype);
			var vAdvBinManagement=false;

			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);
			if(trantype=="salesorder")
			{
				var orderTypeSearchResult = nlapiSearchRecord('salesorder',null,filters,column);
				if(orderTypeSearchResult!=null && orderTypeSearchResult.length>0)
				{
					try
					{
						if(orderTypeSearchResult[0].getValue('custbody_total_weight')!=null && orderTypeSearchResult[0].getValue('custbody_total_weight')!='') 					
						{
							totalWeight=totalWeight+parseFloat(orderTypeSearchResult[0].getValue('custbody_total_weight'));
						}

						nlapiLogExecution('Debug', 'Time Stamp at the start of TransformRecord',TimeStampinSec());
						TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');
						nlapiLogExecution('Debug', 'Time Stamp at the end of TransformRecord',TimeStampinSec());
					}
					catch(e) {


						var exceptionname='SalesOrder';
						var functionality='Creating Item Fulfillment';
						var trantype=2;
						var reference2="";
						var reference3="";
						var reference4 ="";
						var reference5 ="";
						var alertType=1;//1 for exception and 2 for report
						var exceptiondetails=e;
						if ( e instanceof nlobjError )
						{
							nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
							exceptiondetails=e.getCode() + '\n' + e.getDetails();
						}
						else
						{
							exceptiondetails=e.toString();
							nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
						}
						errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
						UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
						InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
						nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		
					}
				}
			}
			else if(trantype=="vendorreturnauthorization")
			{
				try
				{
					nlapiLogExecution('Debug', 'from to', 'from to');
					TransformRec = nlapiTransformRecord('vendorreturnauthorization', SalesOrderInternalId, 'itemfulfillment');
				}

				catch(e) {

					var exceptionname='vendorreturnauthorization';
					var functionality='Creating Item Fulfillment';
					var trantype=2;
					var reference2="";
					var reference3="";
					var reference4 ="";
					var reference5 ="";
					var alertType=1;//1 for exception and 2 for report
					var exceptiondetails=e;
					if ( e instanceof nlobjError )
					{
						nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
						exceptiondetails=e.getCode() + '\n' + e.getDetails();
					}
					else
					{
						exceptiondetails=e.toString();
						nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
					}
					errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
					InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
					UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
					nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		

				}
			}
			else
			{
				try
				{
					nlapiLogExecution('Debug', 'from to', 'from to');
					TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment');
				}

				catch(e) {

					var exceptionname='TransferOrder';
					var functionality='Creating Item Fulfillment';
					var trantype=2;
					var reference2="";
					var reference3="";
					var reference4 ="";
					var reference5 ="";
					var alertType=1;//1 for exception and 2 for report
					var exceptiondetails=e;
					if ( e instanceof nlobjError )
					{
						nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
						exceptiondetails=e.getCode() + '\n' + e.getDetails();
					}
					else
					{
						exceptiondetails=e.toString();
						nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
					}
					errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
					InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
					UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
					nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		
				}
			}

			var itemnoarr = new Array();
			for(var q=0; q<filterLineArray.length;q++ ) // This array contains all disctinct lines no for the order
			{
				if(TransformRec!=null)
				{
					var SOItemLength = TransformRec.getLineItemCount('item');
					for(var vcount=1;vcount<=SOItemLength;vcount++)
					{
						var itemLinevalue = TransformRec.getLineItemValue('item', 'line', vcount);
						if(itemLinevalue==filterLineArray[q])
						{		
							var vitemno = TransformRec.getLineItemValue('item', 'item', vcount);
							itemnoarr.push(vitemno);
							break;
						}
					}
				}
			}

			var memberitemsarr = new Array();
			if(itemnoarr!=null && itemnoarr!='')
			{
				memberitemsarr = getMemberItemsDetails(itemnoarr);
			}
			var IsBoolean=new Array();
			var vTobeConfItemCount=0;

			for(var i=0; i<filterLineArray.length;i++ ) // This array contains all disctinct lines no for the order
			{ 
				var context = nlapiGetContext();
				nlapiLogExecution('Debug','Remaining usage at the start of Item fulfillment line# ' + i,context.getRemainingUsage());

				var actuvalqty="";
				var skunumber="";
				var linenumber="";
				var totalqty=0;	
				var ItemType="";
				var batchno="";
				var serialno="";var serialout="";
				var filterlinenumber=filterLineArray[i];
				//By Ganesh for batch# entry
				var vPrevbatch;
				var vPrevLine;
				var vLotNo;
				var VLotNo1;
				var vLotQty=0;
				var vLotTotQty=0;
				var boolfound = false;
				var serialsspaces = "";
				var itemname ='';
				var itemvalue;
				var vLotonly;
				var IsLineClosed="F";
				var KitIsPicked='F';
				if(TransformRec!=null)
				{
					//Case # 20126922ï¿½Start
					if(salesorderrec!=null && salesorderrec!='')// case# 201413921
					{
						IsLineClosed = salesorderrec.getLineItemValue('item', 'isclosed', filterlinenumber);
					}
					itemname = TransformRec.getLineItemText('item', 'item', i+1);
					itemvalue = TransformRec.getLineItemValue('item', 'item', i+1);
					/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
					for(var y=0; y<itemListArray.length;y++) // This array contails all the data for the order
					{ 
						var vlinenumber =itemListArray[y][2];						
						if(filterlinenumber==vlinenumber)
						{
							itemvalue =itemListArray[y][6];
							itemname =itemListArray[y][5];
							if(itemvalue==null || itemvalue=='')
								itemvalue=itemListArray[y][7];
							itemname=itemListArray[y][8];
						}
					}	
				}
				var str = 'itemvalue. = ' + itemvalue + '<br>';
				str = str + 'itemname. = ' + itemname + '<br>';	
				str = str + 'IsLineClosed. = ' + IsLineClosed + '<br>';	

				nlapiLogExecution('DEBUG', 'Item Parameters', str);//

				/***upto here ***/			
				if(itemname!=null && itemname!=''&&IsLineClosed!="T")
				{
					itemname=itemname.replace(/ /g,"-");

					var itemTypesku = nlapiLookupField('item', itemvalue, 'recordType');

					if(itemTypesku=='kititem')
					{
						//Code added on 5th Sep 2013 by Suman.
						//case# 20127164 starts (FULFILLMENTATORDERLEVEL=='F' is changed to FULFILLMENTATORDERLEVEL!='Y')
						if(FULFILLMENTATORDERLEVEL!='Y')
							KitIsPicked=ISKitItemcompletlyPicked(itemvalue,vebizOrdNo,vWMSCarrier);
						//nlapiLogExecution("ERROR","KitIsPicked",KitIsPicked);

						if(KitIsPicked=="true"||FULFILLMENTATORDERLEVEL=='Y')
						{
							//case# 20127164 end
							//End of code as of 5th Sep.
							/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
							var vKitItemArr=new Array();
							var vKitItemQtyArr=new Array();
							var vKitMemberItemQtyArr=new Array();
							var vKitItemBatch=new Array();
							/***upto here ***/
							for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
							{ 
								linenumber =itemListArray[n][2];


								if(memberitemsarr!=null && memberitemsarr!='')
								{
									//nlapiLogExecution('Debug', 'memberitemsarr length', memberitemsarr.length);

									for(var w=0; w<memberitemsarr.length;w++) 
									{
										fulfilmentItem = memberitemsarr[w].getValue('memberitem');
										memberitemqty = memberitemsarr[w].getValue('memberquantity');

										if((filterlinenumber==linenumber && itemListArray[n][1]==fulfilmentItem))
										{
											QuilfiedItemArray.push(itemvalue);
											QuilfiedItemLineArray.push(linenumber);
											QuilfiedItemLineKitArray.push(linenumber);

											skunumber=itemListArray[n][1];

											var columns;
											batchno =itemListArray[n][3];

											if(serialno=="")
											{
												serialno =itemListArray[n][4];}
											else
											{ 
												serialno =serialno+","+itemListArray[n][4];
											}

											if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
											{
												vPrevbatch=itemname;
												itemListArray[n][3] = itemname;
												vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
											}

											if(vAdvBinManagement==true)
											{
												if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem")
												{
													nlapiLogExecution('Debug', 'Using vAdvBinManagement2',vAdvBinManagement);
													/*var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
														compSubRecord.selectNewLineItem('inventoryassignment');*/
													var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
													if(compSubRecord !=null && compSubRecord!='')
													{
														compSubRecord.selectLineItem('inventoryassignment', 1);
													}
													else
													{
														compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
														compSubRecord.selectNewLineItem('inventoryassignment');
													}
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', itemListArray[n][0]);
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][4]);

													compSubRecord.commitLineItem('inventoryassignment');
													compSubRecord.commit();
												}
											}
											/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
											if (boolfound) {
												if(vKitItemArr.indexOf(fulfilmentItem)!= -1)
												{
													vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]= parseFloat(vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]) + parseFloat(itemListArray[n][0]);

												}
												else
												{	
													vKitItemArr.push(fulfilmentItem);
													vKitItemQtyArr.push(itemListArray[n][0]);
													vKitMemberItemQtyArr.push(memberitemqty);
													vKitItemBatch.push(itemListArray[n][3]);
												}
												totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);
												/***Upto here***/

												vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
												serialsspaces += " " + itemListArray[n][4];

												if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
												{
													if(itemListArray[n][0] != null && itemListArray[n][0] != "")
														vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
												}
												else
												{ 
													if(vLotNo!=null && vLotNo != "")
														vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
													else
														vLotNo = vPrevbatch +"(" + vLotQty + ")";



													if(itemListArray[n][0] != null && itemListArray[n][0] != "")
														vLotQty=parseFloat(itemListArray[n][0]);
													vPrevbatch=itemListArray[n][3];
													vPrevLine=linenumber;
													nlapiLogExecution('Debug', 'vPrevLine',vPrevLine);
//													if(vAdvBinManagement)
//													{
//													try
//													{
//													if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//													{
//													nlapiLogExecution('Debug', 'Using vAdvBinManagement3',vAdvBinManagement);
//													/*var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//													compSubRecord.selectNewLineItem('inventoryassignment');*/

//													var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
//													nlapiLogExecution('Debug', 'compSubRecord1',compSubRecord);
//													if(compSubRecord !=null && compSubRecord!='')
//													{
//													nlapiLogExecution('Debug', 'compSubRecord1','compSubRecord1');
//													compSubRecord.selectLineItem('inventoryassignment', vPrevLine);
//													}
//													else
//													{
//													nlapiLogExecution('Debug', 'compSubRecord2','compSubRecord2');
//													compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//													compSubRecord.selectNewLineItem('inventoryassignment');
//													}
//													nlapiLogExecution('Debug', 'vLotQty',vLotQty);
//													nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);
//													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
//													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

//													compSubRecord.commitLineItem('inventoryassignment');
//													compSubRecord.commit();
//													//New code end

//													}
//													}
//													catch(exp2)
//													{
//													nlapiLogExecution('Debug', 'Exception in Advance Bin Management',exp2);
//													}
//													}
												}

											} else {
												if(skunumber!= null && skunumber != "")
												{
													//nlapiLogExecution('Debug', 'skunumber', skunumber);
													columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
													ItemType = columns.recordType;
													//nlapiLogExecution('Debug', 'ItemType in else', ItemType);
												}
												/***Below code is merged from Factory mation production account on 2nd Mar 2013 by Ganesh***/
												vKitItemArr.push(fulfilmentItem);
												vKitItemQtyArr.push(itemListArray[n][0]);
												vKitMemberItemQtyArr.push(memberitemqty);
												vKitItemBatch.push(itemListArray[n][3]);
												totalqty = parseFloat(itemListArray[n][0]);
												//nlapiLogExecution('Debug', 'totalqty10', totalqty);
												//totalqty = Math.floor(parseFloat(itemListArray[n][0])/memberitemqty);
												/***Upto here***/
												vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
												serialsspaces = itemListArray[n][4];
												if(itemListArray[n][0] != null && itemListArray[n][0] != "")
													vLotQty=parseFloat(itemListArray[n][0]);
												vPrevbatch=itemListArray[n][3];
												vPrevLine=linenumber;
												nlapiLogExecution('Debug', 'vPrevLine',vPrevLine);
//												if(vAdvBinManagement)
//												{
//												try
//												{
//												if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//												{
//												nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
//												/*var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//												compSubRecord.selectNewLineItem('inventoryassignment');*/

//												var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
//												nlapiLogExecution('Debug', 'compSubRecord',compSubRecord);
//												if(compSubRecord !=null && compSubRecord!='')
//												{
//												nlapiLogExecution('Debug', 'test1','test1');
//												compSubRecord.selectLineItem('inventoryassignment', vPrevLine);
//												}
//												else
//												{
//												nlapiLogExecution('Debug', 'test2','test2');
//												compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//												compSubRecord.selectNewLineItem('inventoryassignment');
//												}
//												nlapiLogExecution('Debug', 'vLotQty',vLotQty);
//												nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);
//												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
//												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

//												compSubRecord.commitLineItem('inventoryassignment');
//												compSubRecord.commit();
//												//New code end

//												}
//												}
//												catch(exp2)
//												{
//												nlapiLogExecution('Debug', 'Exception in Advance Bin Management',exp2);
//												}
//												}
											}
											boolfound = true;
											IsBoolean.push("T");
										}
									}
									/***Below code is merged from Factory mation production on 2nd Mar 2013 by Ganesh  as part of standard bundle***/
								}
							}
						}
						if(boolfound==true)
						{
							if(vKitItemQtyArr != null && vKitItemQtyArr != '' && vKitItemQtyArr.length>0)
							{
								totalqty=Math.floor(parseFloat(vKitItemQtyArr[0])/parseFloat(vKitMemberItemQtyArr[0]));
								//nlapiLogExecution('Debug', 'totalqty11', totalqty);
							}	
						}/***Upto here***/
					}
					else
					{
						for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
						{ 
							linenumber =itemListArray[n][2];

							if((filterlinenumber==linenumber))
							{
								QuilfiedItemArray.push(itemvalue);
								QuilfiedItemLineArray.push(linenumber);
								skunumber=itemListArray[n][1];

								var columns;
								batchno =itemListArray[n][3];

								if(serialno=="")
								{
									serialno =itemListArray[n][4];}
								else
								{ 
									serialno =serialno+","+itemListArray[n][4];
								}

								if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
								{
									vPrevbatch=itemname;
									itemListArray[n][3] = itemname;
									vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
								}

								columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
								ItemType = columns.recordType;

								if (boolfound) {
									totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);

									vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
									serialsspaces += " " + itemListArray[n][4];

									if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
									{
										if(itemListArray[n][0] != null && itemListArray[n][0] != "")
											vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
									}
									else
									{ 
										if(vLotNo!=null && vLotNo != "")
											vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
										else
											vLotNo = vPrevbatch +"(" + vLotQty + ")";
										/***Below code is merged form Lexjet production by Ganesh  on 5th Mar 2013***/
//										if(vAdvBinManagement)
//										{
//										if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//										{

//										nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);
//										TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment');
//										nlapiLogExecution('Debug', 'Using vAdvBinManagement5',vAdvBinManagement);
//										nlapiLogExecution('Debug', 'test1','test1');
//										nlapiLogExecution('Debug', 'TransformRec',TransformRec);

//										var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//										nlapiLogExecution('Debug', 'test2','test2')
//										compSubRecord.selectNewLineItem('inventoryassignment');
//										nlapiLogExecution('Debug', 'Using vLotQty',vLotQty);
//										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
//										nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);
//										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

//										compSubRecord.commitLineItem('inventoryassignment');
//										compSubRecord.commit();
//										nlapiLogExecution('Debug', 'End','END');
//										//New code end

//										}
//										}
										/***Upto here***/
										if(itemListArray[n][0] != null && itemListArray[n][0] != "")
											vLotQty=parseFloat(itemListArray[n][0]);
										vPrevbatch=itemListArray[n][3];
										vPrevLine=linenumber;
									}

								} else {
									if(skunumber!= null && skunumber != "")
									{
										columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
										ItemType = columns.recordType;
									}
									totalqty = parseFloat(itemListArray[n][0]);
									vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
									serialsspaces = itemListArray[n][4];

									if(itemListArray[n][0] != null && itemListArray[n][0] != "")
										vLotQty=parseFloat(itemListArray[n][0]);
									vPrevbatch=itemListArray[n][3];
									vPrevLine=linenumber;
								}
								boolfound = true;
								IsBoolean.push("T");
							}
						}
					}

					var SOLength=0;
					var itemIndex=0;

					if(TransformRec!=null)
					{
						SOLength = TransformRec.getLineItemCount('item');  // To get the no of lines from item fulfillment record
						nlapiLogExecution('Debug', "SO Length", SOLength);    

						var SOWMSCarrier = TransformRec.getFieldValue('custbody_salesorder_carrier');
						for (var j = 1; j <= SOLength; j++) {

							var item_id = TransformRec.getLineItemValue('item', 'item', j);
							// To get Item Type
							//Below code commented for governance issue and this is code is not using
							//var columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
							var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
							var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);
							if(trantype=="transferorder"){
								itemLineNo=parseFloat(itemLineNo)-1;
							}
							var NSUOM = TransformRec.getLineItemValue('item', 'units', j); 
							if (itemLineNo == filterlinenumber) {
								itemIndex=j;  
							}
						}
					}
					if(itemIndex!=0 && totalqty!=null && totalqty!='')
					{
						var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
						var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
						var itemLineNo = TransformRec.getLineItemValue('item', 'line', itemIndex);
						if(itemname!=null && itemname!='')
							itemname=itemname.replace(/ /g,"-");
						var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
						var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);

						var vparentitemtype = nlapiLookupField('item', item_id, 'recordType');

						var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);

						var veBizUOMQty=1;
						var vBaseUOMQty=1;
						if(NSOrdUOM!=null && NSOrdUOM!="")
						{
							if(vItemDimsArr!=null && vItemDimsArr.length>0)
							{
								for(z=0; z < vItemDimsArr.length; z++)
								{
									if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
									{	
										if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
										{
											vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
											nlapiLogExecution('Debug', 'vBaseUOMQty', vBaseUOMQty);
										}

										if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
										{
											nlapiLogExecution('Debug', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
											veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
											nlapiLogExecution('Debug', 'veBizUOMQty', veBizUOMQty);
										}
									}
								}
								if(veBizUOMQty==null || veBizUOMQty=='')
								{
									veBizUOMQty=vBaseUOMQty;
								}
								if(veBizUOMQty!=0)
								{ //Case# 20123228 start: to accept decimal qty
									//totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
									totalqty = (parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty);
									//Case# 20123228 end:
								}
								nlapiLogExecution('Debug', 'totalqty', totalqty);

								vLotQty =totalqty;
							}
						}

						if (boolfound) {
							nlapiLogExecution('Debug', 'confirmLotToNS',confirmLotToNS);
							if(confirmLotToNS=='N')
								vPrevbatch=itemname;				
							nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);


							if(vLotNo!=null && vLotNo != "")
								vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
							else
								vLotNo = vPrevbatch +"(" + vLotQty + ")";
							//Case # 20126405   start
							TransformRec.selectLineItem('item', itemIndex);
							TransformRec.setCurrentLineItemValue('item', 'quantity', totalqty);
							//Case # 20126405   End
							if(vAdvBinManagement)
							{
								try
								{
									nlapiLogExecution('Debug', 'Into Advance Bin Management lot item', ItemType);

									if (vparentitemtype == "lotnumberedinventoryitem" || vparentitemtype == "lotnumberedassemblyitem")
									{
										/***Below code is merged from Lexjet production on 5th march by Ganesh ***/
										if(itemListArray!=null && itemListArray!='')
										{
											nlapiLogExecution('Debug', 'itemListArray Length', itemListArray.length);

											//var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');//
											//var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
											var partialbatch;
											var totallotqty=0;
											var subrecordcount=0;
											var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
											var vinvtdetailsLineCount=1;
											for(var i1=0;i1<itemListArray.length;i1++)
											{										
												var vNextBatch="";
												if(i1+1 <itemListArray.length)
													vNextBatch=itemListArray[i1+1][3];
												else
													vNextBatch=itemListArray[i1][3];
												var opentaskitemid = itemListArray[i1][6];

												nlapiLogExecution('Debug', 'opentaskitemid', opentaskitemid);
												nlapiLogExecution('Debug', 'soitemid', item_id);
												nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
												nlapiLogExecution('Debug', 'Locallinenumber', Locallinenumber);
												var Locallinenumber =itemListArray[i1][2];
												if(opentaskitemid==item_id && filterlinenumber == Locallinenumber )
												{
													var itemTypesku = nlapiLookupField('item', item_id, 'recordType');
													if(itemTypesku=='kititem')
													{	
														for(var k1=0;k1<vKitItemArr.length;k1++)
														{
															if((compSubRecord !=null && compSubRecord!='') )
															{
																nlapiLogExecution('EMERGENCY', 'inside','edit');
																subrecordcount=compSubRecord.getLineItemCount('inventoryassignment');
																//compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
																//compSubRecord.selectNewLineItem('inventoryassignment');
																nlapiLogExecution('EMERGENCY', 'vinvtdetailsLineCount',vinvtdetailsLineCount);
																nlapiLogExecution('EMERGENCY', 'subrecordcount',subrecordcount);
																if(parseInt(vinvtdetailsLineCount)>=parseInt(subrecordcount))
																{
																	compSubRecord.selectNewLineItem('inventoryassignment');
																}
																else
																{
																	compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
																}
															}
															else if(compSubRecord ==null || compSubRecord=='')
															{
																nlapiLogExecution('EMERGENCY', 'inside2','create');
																compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
																compSubRecord.selectNewLineItem('inventoryassignment');
															}
															else if(compSubRecord !=null && compSubRecord!='')
															{
																nlapiLogExecution('EMERGENCY', 'inside3','create new sub line');
																subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
																if(parseInt(vinvtdetailsLineCount)>parseInt(subrecordcount))
																	compSubRecord.selectNewLineItem('inventoryassignment');
																else
																	compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
															}
															nlapiLogExecution('EMERGENCY', 'qty', vKitItemQtyArr[k1]);
															nlapiLogExecution('EMERGENCY', 'batch', vKitItemBatch[k1]);
															compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vKitItemQtyArr[k1]);

															compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vKitItemBatch[k1]);

															compSubRecord.commitLineItem('inventoryassignment');
														}
													}
													else
													{
														nlapiLogExecution('EMERGENCY', 'item_id', item_id);
														nlapiLogExecution('EMERGENCY', 'filterlinenumber', filterlinenumber);
														/*var subrecordtype='edit';
														var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
														if(compSubRecord !=null && compSubRecord!='')
														{
															//compSubRecord.selectLineItem('inventoryassignment', 1);
														}
														else
														{
															compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
															//compSubRecord.selectNewLineItem('inventoryassignment');
														}
														 */
														if(itemListArray[i1][0] != null && itemListArray[i1][0] != "")
															vLotQty=parseFloat(itemListArray[i1][0]);
														vPrevbatch=itemListArray[i1][3];
														nlapiLogExecution('EMERGENCY', 'partialbatch', partialbatch);
														nlapiLogExecution('EMERGENCY', 'vPrevbatch', vPrevbatch);
														if(partialbatch!=vPrevbatch)
														{
															totallotqty=vLotQty;
															partialbatch=itemListArray[i1][3];
														}
														else if(partialbatch==vPrevbatch)
														{
															totallotqty=parseFloat(totallotqty)+parseFloat(vLotQty);
														}
														else
														{
															totallotqty=vLotQty;
															//partialbatch=itemListArray[i1][3];
															//totallotqty=parseFloat(totallotqty)+parseFloat(vLotQty);
														}

//														if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//														{
//														nlapiLogExecution('Debug', 'Using vAdvBinManagement1',vLotQty);
//														nlapiLogExecution('Debug', 'Using vPrevbatch',vPrevbatch);


														//compSubRecord.selectNewLineItem('inventoryassignment');
														//compSubRecord.selectLineItem('inventoryassignment', 1);

														/*if(subrecordtype='edit')
														{
															var indx=parseInt(i1)+1;
															compSubRecord.selectLineItem('inventoryassignment', 1);
														}
														else
														{
															compSubRecord.selectNewLineItem('inventoryassignment');
														}*/
														nlapiLogExecution('EMERGENCY', 'vinvtdetailsLineCount',vinvtdetailsLineCount);

														//var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
														if((compSubRecord !=null && compSubRecord!='') && (partialbatch==vPrevbatch))
														{
															nlapiLogExecution('EMERGENCY', 'inside','edit');
															subrecordcount=compSubRecord.getLineItemCount('inventoryassignment');
															//compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
															//compSubRecord.selectNewLineItem('inventoryassignment');
															nlapiLogExecution('EMERGENCY', 'vinvtdetailsLineCount',vinvtdetailsLineCount);
															nlapiLogExecution('EMERGENCY', 'subrecordcount',subrecordcount);
															if(parseInt(vinvtdetailsLineCount)>parseInt(subrecordcount))
															{
																compSubRecord.selectNewLineItem('inventoryassignment');
															}
															else
															{
																compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
															}

														}
														else if(compSubRecord ==null || compSubRecord=='')
														{
															nlapiLogExecution('EMERGENCY', 'inside2','create');
															compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
															compSubRecord.selectNewLineItem('inventoryassignment');
														}
														else if(compSubRecord !=null && compSubRecord!='')
														{
															nlapiLogExecution('EMERGENCY', 'inside3','create new sub line');
															subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
															if(parseInt(vinvtdetailsLineCount)>parseInt(subrecordcount))
																compSubRecord.selectNewLineItem('inventoryassignment');
															else
																compSubRecord.selectLineItem('inventoryassignment', vinvtdetailsLineCount);
														}

														if(NSOrdUOM!=null && NSOrdUOM!="")
														{
															var vUOMlotqty=0;
															nlapiLogExecution('Debug', 'veBizUOMQty',veBizUOMQty);
															nlapiLogExecution('Debug', 'vBaseUOMQty',vBaseUOMQty);
															if(veBizUOMQty!=0 && vBaseUOMQty!=0)
															{ 
																//vUOMlotqty = (parseFloat(vLotQty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty);
																vUOMlotqty = (parseFloat(totallotqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty);
																nlapiLogExecution('Debug', 'vUOMlotqty',vUOMlotqty);
																if(vUOMlotqty !=null && vUOMlotqty!='' && vUOMlotqty !=0)
																{																
																	compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vUOMlotqty);
																}
															}
														}
														else
														{
															nlapiLogExecution('EMERGENCY', 'vLotQty',vLotQty);
															nlapiLogExecution('EMERGENCY', 'vPrevbatch',vPrevbatch);	
															nlapiLogExecution('EMERGENCY', 'totallotqty',totallotqty);
															compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', totallotqty);

														}
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);
													}

													compSubRecord.commitLineItem('inventoryassignment');
													//	compSubRecord.commit();
													nlapiLogExecution('EMERGENCY', 'after','submit');
													if(vNextBatch !=vPrevbatch)
														vinvtdetailsLineCount++;
												}
											}

											compSubRecord.commit();
										}
										/***upto here***/
										//New code end
									}

								}
								catch(exp2)
								{
									nlapiLogExecution('Debug', 'Error in Advance Bin Management',exp2);
								}
							}

							if(vAdvBinManagement)
							{
								//case # 20140052  start.....if condition was closed improperly.
								if (vparentitemtype == "serializedinventoryitem" || vparentitemtype == "serializedassemblyitem")
								{
									var filters= new Array();	
									filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
									filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
									filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
									filters.push(new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'I'));
									var column =new Array(); 

									column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

									var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
									if(searchresultser!=null && searchresultser!='')
									{
										var subrecordtype= 'edit';
//										var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
										var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
										if(compSubRecord ==null || compSubRecord == '' || compSubRecord == 'null')
										{												
											compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
											subrecordtype='insert';
										}
										//	for ( var b = 0; b < searchresultser.length; b++ ) {//case # 20127174? 
										for ( var b = 0; b < totalqty; b++ ) {
											serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
											var seriid ;
											var vfilters= new Array();				

											vfilters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
											vfilters.push(new nlobjSearchFilter('custrecord_serialnumber', null,'is', serialsspaces));
											vfilters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
											filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
											var vcolumn =new Array(); 
											vcolumn[0] = new nlobjSearchColumn('custrecord_serialstatus');							
											var vsearchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,vfilters,vcolumn);
											if(vsearchresultser!=null && vsearchresultser!='')
											{
												seriid = vsearchresultser[0].getId();
											}

											var trnserial = nlapiLoadRecord('customrecord_ebiznetserialentry', seriid);
											trnserial.setFieldValue('custrecord_serialstatus', 'I');	
											var retval  =nlapiSubmitRecord(trnserial, true);

//											nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
//											nlapiLogExecution('Debug', 'Using totalqty',totalqty);
//											nlapiLogExecution('Debug', 'Using serialsspaces',serialsspaces);

											//compSubRecord.selectNewLineItem('inventoryassignment');
											if(subrecordtype =='edit')
											{
												var indx=parseInt(b)+1;
												compSubRecord.selectLineItem('inventoryassignment', indx);
											}
											else
											{
												compSubRecord.selectNewLineItem('inventoryassignment');
											}
											//Case # 20126405 Start
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
											//Case # 20126405 End
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serialsspaces);

											compSubRecord.commitLineItem('inventoryassignment');
										}
										compSubRecord.commit();
									}	
								}
							}
							TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
							nlapiLogExecution('EMERGENCY', 'totalqty', totalqty);
							TransformRec.setCurrentLineItemValue('item', 'quantity', parseFloat(totalqty).toFixed(5));
							//nlapiLogExecution('Debug', 'itemloc2', itemloc2);
							TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

							//nlapiLogExecution('Debug', 'ItemType', ItemType);
							if(!vAdvBinManagement)
							{	
								if (vparentitemtype == "lotnumberedinventoryitem") {
									//nlapiLogExecution('Debug', 'vLotNo', vLotNo);									
									TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
									//nlapiLogExecution('Debug', 'vBatchno', vBatchno);
								}
								else if(vparentitemtype == "lotnumberedassemblyitem") 
								{
									TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
									//nlapiLogExecution('Debug', 'vLotNo', vLotNo);
								}
								else if (vparentitemtype == "serializedinventoryitem" || vparentitemtype == "serializedassemblyitem") {
									var serialsspaces = "";
									if(serialsspaces==null || serialsspaces=='')
									{
										var filters= new Array();				
										//nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
										filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
										filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
										filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
										var column =new Array(); 

										column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

										var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
										if(searchresultser!=null && searchresultser!='')
										{
											for ( var b = 0; b < searchresultser.length; b++ ) {
												if(serialsspaces==null || serialsspaces=='')
												{
													serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
												}
												else
												{
													serialsspaces += " " + searchresultser[b].getValue('custrecord_serialnumber');
												}
											}
										}									
										//nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);
									}
									//nlapiLogExecution('Debug', 'before serialsspaces', serialsspaces);
									TransformRec.setCurrentLineItemValue('item','serialnumbers', serialsspaces);
								}
							}

							vLotNo="";
						}
						TransformRec.commitLineItem('item');

						//The below code is written by Satish.N on 03/23/2015 to post the itemfulfillment for Kit/Package items
						//if the components are lot controlled items and advance bin management is enabled.

						var str = 'vparentitemtype. = ' + vparentitemtype + '<br>';
						str = str + 'KitIsPicked. = ' + KitIsPicked + '<br>';	
						str = str + 'FULFILLMENTATORDERLEVEL. = ' + FULFILLMENTATORDERLEVEL + '<br>';	

						nlapiLogExecution('DEBUG', 'Parent Item Parameters', str);

						KitIsPicked='true';

						if((vparentitemtype=='kititem') && (KitIsPicked=="true"||FULFILLMENTATORDERLEVEL=='Y'))
						{								
							var vitemIndex=0;

							var vmemlotqtyarr = new Array();


							for(var p=0; p<itemListArray.length;p++) // This array contails all the data for the order
							{
								var lotfound='F';

								var vmainitem = itemListArray[p][6];
								var vcompItem=itemListArray[p][1];
								var vcompQty=itemListArray[p][0];
								var vcompLot=itemListArray[p][3];

								if(vmainitem==item_id)
								{
									var vcomparr=[vcompLot,vcompQty,vcompItem,vmainitem];

									nlapiLogExecution('DEBUG', 'vmemlotqtyarr', vmemlotqtyarr);

									if(vmemlotqtyarr!=null || vmemlotqtyarr!='')
									{
										for(var p2=0; p2<vmemlotqtyarr.length;p2++) // This array contails all the data for the order
										{
											if(vcompLot == vmemlotqtyarr[p2][0])
											{
												var vPrevLotQty=vmemlotqtyarr[p2][1];
												nlapiLogExecution('DEBUG','vPrevLotQty',vPrevLotQty);
												var totalLotQuantity = parseFloat(vPrevLotQty) + parseFloat(vcompQty);
												nlapiLogExecution('DEBUG','totalLotQuantity Exists',totalLotQuantity);
												vmemlotqtyarr[p2][1]=totalLotQuantity;

												lotfound='T';
											}											
										}
										if(lotfound=='F')
										{
											vmemlotqtyarr.push(vcomparr);
										}
									}
									else
									{
										vmemlotqtyarr.push(vcomparr);
									}
								}
							}

							nlapiLogExecution('DEBUG', 'vmemlotqtyarr final', vmemlotqtyarr);


							var membercomparr = getMemberItemsDetails(item_id);
							if(membercomparr!=null && membercomparr!='')
							{
								//for(var h=0; h<membercomparr.length;h++) // This array contails all the data for the order
								for(var h=0; h<vmemlotqtyarr.length;h++) // This array contails all the data for the order
								{
									var vparentitem = vmemlotqtyarr[h][3];
									var vmemberItem=vmemlotqtyarr[h][2];
									var vmemberQty=vmemlotqtyarr[h][1];
									var vmemberLot=vmemlotqtyarr[h][0];

									if(vparentitem==item_id)
									{
										for (var g = 1; g <= SOLength; g++) {

											var vitem_id = TransformRec.getLineItemValue('item', 'item', g);
											var vitemrec = TransformRec.getLineItemValue('item', 'itemreceive', g);
											var vitemLineNo = TransformRec.getLineItemValue('item', 'line', g);
											var parentlineno= TransformRec.getLineItemValue('item', 'kitmemberof', g);

											var str = 'parentlineno. = ' + parentlineno + '<br>';
											str = str + 'filterlinenumber. = ' + filterlinenumber + '<br>';	
											str = str + 'vmemberItem. = ' + vmemberItem + '<br>';	
											str = str + 'vitem_id. = ' + vitem_id + '<br>';	

											nlapiLogExecution('DEBUG', 'Child Item Parameters', str);//


											if (parentlineno == filterlinenumber && vmemberItem == vitem_id) {
												vitemIndex=g; 
												break;
											}
										}

										var str = 'vitemIndex. = ' + vitemIndex + '<br>';
										str = str + 'vmemberQty. = ' + vmemberQty + '<br>';	
										str = str + 'vmemberLot. = ' + vmemberLot + '<br>';	
										str = str + 'itemloc2. = ' + itemloc2 + '<br>';	

										nlapiLogExecution('DEBUG', 'Child Item Lot Parameters', str);//

										if(vitemIndex!=0 && vmemberQty!=null && vmemberQty!='')
										{
											TransformRec.selectLineItem('item', vitemIndex);
											TransformRec.setCurrentLineItemValue('item', 'quantity', vmemberQty);	
											TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

											if(vAdvBinManagement)
											{
												var childItemType=nlapiLookupField('item', vitem_id, 'recordType');

												if (childItemType == "lotnumberedinventoryitem" || childItemType == "lotnumberedassemblyitem")
												{												

													var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');		
													compSubRecord.selectNewLineItem('inventoryassignment');
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vmemberQty);																
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vmemberLot);										
													compSubRecord.commitLineItem('inventoryassignment');
													compSubRecord.commit();
												}
											}
											else
											{
												if (childItemType == "lotnumberedinventoryitem" || childItemType == "lotnumberedassemblyitem")
												{
													TransformRec.setCurrentLineItemValue('item','serialnumbers', vmemberLot);
												}
											}

											TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');

											TransformRec.commitLineItem('item');

										}
									}
								}
							}
						}

						/////////////////////////////////////////
					}
					else
					{
						nlapiLogExecution('Debug', 'itemIndex in else', itemIndex);


						if(itemIndex != 0)
						{
							TransformRec.selectLineItem('item', itemIndex);


							TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');


							TransformRec.commitLineItem('item');
						}
					}
				}
			}
			//Case# 20124622 Start//Issue Fix for the so having lines with different site then itemfulfillment was failing if we generate the wave for single site
			var SOLengthnew;
			if(TransformRec!=null && TransformRec!='')
				SOLengthnew = TransformRec.getLineItemCount('item');

			if(parseInt(filterLineArray.length)!=parseInt(SOLengthnew))
			{
				for(var k1=1;k1<=SOLengthnew;k1++)
				{
					var isLinenoComitted=false;
					var linenum= TransformRec.getLineItemValue('item', 'line', k1);
					for(var k2=0;k2<filterLineArray.length;k2++)
					{
						var filterlineno=filterLineArray[k2];
						if(parseInt(linenum)==parseInt(filterlineno))
						{
							isLinenoComitted=true;
							break;
						}
					}
					if(isLinenoComitted==false)
					{
						nlapiLogExecution('Debug', 'itemIndex in', k1);
						TransformRec.selectLineItem('item', k1);

						TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

						TransformRec.commitLineItem('item');
					}
				}
			}
			//Case# 20124622 End

			//nlapiLogExecution('Debug', 'itemloc', itemloc);
			nlapiLogExecution('ERROR', 'Before Submit', IsBoolean);
			nlapiLogExecution('ERROR', 'ISBoolean.indexOf(T)', IsBoolean.indexOf("T"));
			if(parseInt(IsBoolean.indexOf("T"))!=parseInt(-1))
			{
				nlapiLogExecution('ERROR', "INTOIF");
				boolfound=true;
			}
			nlapiLogExecution('ERROR', 'boolfound', boolfound);
			nlapiLogExecution('ERROR', 'TransformRec', TransformRec);

			if(TransformRec!=null && boolfound==true)
			{
				nlapiLogExecution('Debug', 'Time Stamp at the start of nlapiSubmitRecord',TimeStampinSec());
				try
				{
					TransformRecId = nlapiSubmitRecord(TransformRec, true);
					nlapiLogExecution('Debug', 'Time Stamp at the end of nlapiSubmitRecord',TimeStampinSec());
				}
				catch(e)
				{
					var exceptionname='SalesOrder';
					var functionality='Creating Item Fulfillment';
					var trantype=2;
					var reference2="";
					var reference3="";
					var reference4 ="";
					var reference5 ="";
					var alertType=1;//1 for exception and 2 for report
					var exceptiondetails=e;
					if ( e instanceof nlobjError )
					{
						nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
						exceptiondetails=e.getCode() + '\n' + e.getDetails();
					}
					else
					{
						exceptiondetails=e.toString();
						nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
					}
					InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);

					var ErrorCode=e.getCode();
					var vErrorMsg=e.message;
					UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
				}
				nlapiLogExecution('ERROR', 'After Submit', TransformRecId);

				/*for ( var i = 0; i < searchresult.length; i++ ) {
					//nlapiLogExecution('ERROR', 'searchresult[i].getId()', searchresult[i].getId());
					//nlapiLogExecution('ERROR', 'TransformRecId', TransformRecId);
					nlapiSubmitField(searchresult[i].getRecordType(), searchresult[i].getId(), 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
				}*/
				//Code added on 5th Sep 2013 by suman.
				nlapiLogExecution('ERROR', 'QuilfiedItemArray',QuilfiedItemArray);
				nlapiLogExecution('ERROR', 'QuilfiedItemLineArray',QuilfiedItemLineArray);
				if(QuilfiedItemArray!=null&&QuilfiedItemArray!=""&&QuilfiedItemArray.length!=0)
				{
					var QuilfiedItemArray=removeDuplicateElementOld(QuilfiedItemArray);
					var QuilfiedItemLineArray=removeDuplicateElementOld(QuilfiedItemLineArray);
					var QuilfiedItemLineKitArray=removeDuplicateElementOld(QuilfiedItemLineKitArray);
					UpdateAllOpenTaskRecordsWithNSID(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineArray,vcontainerLp,QuilfiedItemLineKitArray,FULFILLMENTATORDERLEVEL);
				}
				//End of code as of 5th Sep 2013.

				if(packtaskid!=null && packtaskid!='')
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', packtaskid, 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);

				if(TransformRecId !=null && TransformRecId !='')
					AutoPackingAfterfulfillment(SalesOrderInternalId,itemloc,vebizOrdNo,FULFILLMENTATORDERLEVEL,vcontainerLp);

				if(trantype=="salesorder" && TransformRecId !=null && TransformRecId !='' && totalWeight != null && totalWeight != '' && parseFloat(totalWeight) != 0 && totalWeight!=0.0 )
				{
					totalcube = roundNumber(totalcube, 2);

					var fields = new Array();
					var values = new Array();

					fields.push('custbody_total_weight');
					fields.push('custbody_total_vol');

					values.push(parseInt(totalWeight));
					values.push(parseFloat(totalcube));

					nlapiSubmitField('salesorder', SalesOrderInternalId, fields, values);	

					/***Below code is merged from Lexjet production on 5th march by Ganesh and commented ***/
					//UpdateShipmanifestCustomrecord(vebizOrdNo,wavenumber,TransformRecId);
					/***upto here***/

				}
			}

		}
	}	
	return TransformRecId;
	nlapiLogExecution('Debug', 'Time Stamp at the end of createItemFulfillment_SCH',TimeStampinSec());
}



function createItemFulfillmentMultiLineShipping_SCH(SalesOrderInternalId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsrefno,vcontainerLp,
		wavenumber,fulfillmenttask,itemloc)
{
	nlapiLogExecution('Debug', 'Into createItemFulfillmentMultiLineShipping');

	//Item Fulfillment for Multi Line Shipping

	var shipgroupslist=new Array();
	var itemarray=new Array();

	var columns = new Array();
	var filters = new Array();
	nlapiLogExecution('Debug', 'vebizOrdNo',vebizOrdNo);
	nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);
	nlapiLogExecution('Debug', 'vcontainerLp',vcontainerLp);


	var vRoleLocation=getRoledBasedLocation();


	filters.push(new nlobjSearchFilter('internalid', null, 'is', SalesOrderInternalId));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('shipgroup', null, 'greaterthan', 0));
	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{		
		filters.push(new nlobjSearchFilter('location', null, 'is', vRoleLocation));
	}

	columns[0] = new nlobjSearchColumn('shipgroup');
	columns[1] = new nlobjSearchColumn('item');
	columns[2] = new nlobjSearchColumn('line').setSort();

	var searchresults = nlapiSearchRecord('salesorder', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		// The below logic is to get all the distinct ship groups to shipgroupslist. 
		label:for(var i=0; i<searchresults.length;i++ )
		{  
			var vshipgroup=searchresults[i].getValue('shipgroup');

			if(vshipgroup==null || vshipgroup==''||vshipgroup==0)
				vshipgroup=1;	

			for(var j=0; j<shipgroupslist.length;j++ )
			{
				if(shipgroupslist[j]==vshipgroup) 
					continue label;
			}

			nlapiLogExecution('Debug', 'vshipgroup',vshipgroup);
			shipgroupslist[shipgroupslist.length] = vshipgroup;
		}

	for(var k=0; k<shipgroupslist.length;k++ )
	{
		var shipgroup=shipgroupslist[k];

		nlapiLogExecution('Debug', 'shipgroup',shipgroup);

		for(var l=0; l<searchresults.length;l++ )
		{
			var vshipgroup2=searchresults[l].getValue('shipgroup');

			if(vshipgroup2==null || vshipgroup2==''||vshipgroup2==0)
				vshipgroup2=1;

			if(shipgroup == vshipgroup2)
			{
				nlapiLogExecution('Debug', 'item',searchresults[l].getValue('item'));
				itemarray.push(searchresults[l].getValue('item'));
			}
		}


		//var confirmLotToNS='N';

		var lineArray = new Array();
		var filterLineArray = new Array();
		var filterItemArray =new Array(); // For distinct item ids
		var itemListArray= new Array();
		var ItemArray= new Array();
		var filter= new Array();
		var FULFILLMENTATORDERLEVEL=getSystemRuleValue('IF001');
		var QuilfiedItemArray=new Array();
		var QuilfiedItemLineArray=new Array();
		var QuilfiedItemLineKitArray=new Array();

//		filter.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); //PICK

		if(fulfillmenttask=='PICK')
			filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['9']));//9 //Picks generated
		else
			filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8','9']));
		if(vWMSCarrier != null && vWMSCarrier != '')
			filter.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier
		if(itemarray != null && itemarray != '')
			filter.push(new nlobjSearchFilter('custrecord_parent_sku_no', null,'anyof', itemarray));

		if(FULFILLMENTATORDERLEVEL!='Y')
		{
			filter.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
		}
		else
		{
			filter.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		}

		var columns= new Array(); 

		columns[0] = new nlobjSearchColumn('custrecord_act_qty');
		columns[1] = new nlobjSearchColumn('custrecord_line_no');
		columns[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[3]= new nlobjSearchColumn('custrecord_act_end_date');

		var opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);

		nlapiLogExecution('Debug', 'opentaskordersearchresult',opentaskordersearchresult);

		if(opentaskordersearchresult==null) // To make sure there is not records exists with Picks generated stage for the order
		{
			nlapiLogExecution('Debug', 'No Open Records. All are Pack Confirmaed');

			var filters= new Array();					

			//filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
			if(fulfillmenttask=='PICK')
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8']));//9 //Picks confirmed
			else
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));// PACK COMPLETE

			filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
			/***Below code is merged from Factory  mation production on 2nd March 2013 by Ganesh as part of standard bundle***/
			filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
			filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0.0'));
			/***upto here ***/
			if(FULFILLMENTATORDERLEVEL!='Y')
			{
				filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
			}
			else
			{
				filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
			}

			if(itemarray != null && itemarray != '')
				filters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null,'anyof', itemarray));
			var vRoleLocation=getRoledBasedLocation();
			nlapiLogExecution('Debug', 'vRoleLocation',vRoleLocation);
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

			}

			var column =new Array(); 

			column[0] = new nlobjSearchColumn('custrecord_act_qty');
			column[1] = new nlobjSearchColumn('custrecord_line_no');
			column[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
			column[3]= new nlobjSearchColumn('custrecord_act_end_date');
			column[4]= new nlobjSearchColumn('internalid');
			column[5]= new nlobjSearchColumn('name');
			column[6]= new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			column[7]= new nlobjSearchColumn('custrecord_ebiz_order_no');
			column[8]= new nlobjSearchColumn('custrecord_serial_no');
			column[9]= new nlobjSearchColumn('custrecord_batch_no');					
			column[10]= new nlobjSearchColumn('custrecord_total_weight');
			column[11]= new nlobjSearchColumn('custrecord_totalcube');
			column[12]= new nlobjSearchColumn('custrecord_parent_sku_no');
			column[13]= new nlobjSearchColumn('custrecord_sku');
			column[1].setSort();
			column[9].setSort();

			var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

			var SalesOrderInternalId="";var DoLineId="";
			var totalWeight=0.0;
			var totalcube=0.0;
			if(searchresult !=null) // SearchResult contains all the open task records of task type PICK and status flag PACK COMPLETE
			{
				var sum =0;
				var actqty="";
				var linenumber="";
				var skuno="";
				var vBatchno="";
				var vserialno="";

				DoLineId=searchresult[0].getValue('custrecord_ebiz_cntrl_no');

				for (l = 0; l < searchresult.length; l++) 
				{  
					nlapiLogExecution('Debug', 'searchresult.length',searchresult.length);
					SalesOrderInternalId=searchresult[l].getValue('custrecord_ebiz_order_no');						
					itemListArray[l] = new Array();

					actqty=searchresult[l].getValue('custrecord_act_qty');
					linenumber=searchresult[l].getValue('custrecord_line_no');
					skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
					vBatchno = searchresult[l].getValue('custrecord_batch_no');
					vserialno = searchresult[l].getValue('custrecord_serial_no');
					var parentskuno=searchresult[l].getValue('custrecord_parent_sku_no');
					var skuname=searchresult[l].getText('custrecord_parent_sku_no');
					var skuvalue=searchresult[l].getValue('custrecord_ebiz_sku_no');
					var skutext=searchresult[l].getText('custrecord_sku');
					if(searchresult[l].getValue('custrecord_total_weight')!=null && searchresult[l].getValue('custrecord_total_weight')!='')
					{
						totalWeight=totalWeight+parseFloat(searchresult[l].getValue('custrecord_total_weight'));
					}

					if(searchresult[l].getValue('custrecord_totalcube')!=null && searchresult[l].getValue('custrecord_totalcube')!='')
					{
						totalcube=totalcube+parseFloat(searchresult[l].getValue('custrecord_totalcube'));
					}

					itemListArray[l][0]=actqty;
					itemListArray[l][1]=skuno;
					itemListArray[l][2]=linenumber;
					itemListArray[l][3]=vBatchno;
					itemListArray[l][4]=vserialno;
					itemListArray[l][5]=skuname;
					itemListArray[l][6]=parentskuno;
					itemListArray[l][7]=skuvalue;
					itemListArray[l][8]=skutext;
					ItemArray.push(skuno);
					lineArray.push(linenumber);
				}


				// The below logic is to get all the distinct line nos to filterLineArray . This contails distinct lines nos for the order.
				label:for(var i=0; i<lineArray.length;i++ )
				{  
					for(var j=0; j<filterLineArray.length;j++ )
					{
						if(filterLineArray [j]==lineArray [i]) 
							continue label;
					}
					filterLineArray [filterLineArray .length] = lineArray [i];
				}

				filterLineArray.sort(function(a,b){return a - b;});

				// The below logic is to get all the distinct Item ids filterItemArray . This contains distinct Item ids for the order.
				label2:for(var m=0; m<ItemArray.length;m++ )
				{  
					for(var p=0; p<filterItemArray.length;p++ )
					{
						if(filterItemArray[p]==ItemArray[m]) 
							continue label2;
					}
					filterItemArray[filterItemArray.length] = ItemArray[m];
				}


				// To fetch Item dims from Item Array
				var vItemDimsArr = geteBizItemDimensionsArray(filterItemArray);
				if(vItemDimsArr !=null && vItemDimsArr != "")
				{
					nlapiLogExecution('Debug', 'vItemDimsArr.length', vItemDimsArr.length);
				}	

				var TransformRec;

				nlapiLogExecution('Debug', 'SalesOrderInternalId', SalesOrderInternalId);

				var filters= new Array();

				filters.push(new nlobjSearchFilter('internalid', null,'is', SalesOrderInternalId));
				filters.push(new nlobjSearchFilter('mainline',null,'is','T'));

				var column =new Array(); 

				column[0] = new nlobjSearchColumn('custbody_nswmssoordertype');
				//column[1] = new nlobjSearchColumn('custbody_total_weight');

				var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
				nlapiLogExecution('Debug', 'trantype', trantype);

				var vAdvBinManagement=false;

				var ctx = nlapiGetContext();
				if(ctx != null && ctx != '')
				{
					if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
						vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
				}  
				nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);

				if(trantype=="salesorder")
				{
					var orderTypeSearchResult = nlapiSearchRecord('salesorder',null,filters,column);
					if(orderTypeSearchResult!=null && orderTypeSearchResult.length>0)
					{
						try
						{
							if(orderTypeSearchResult[0].getValue('custbody_nswmssoordertype')=='5')
							{
								nlapiLogExecution('Debug', 'from soto', 'from soto');
								TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment', { 'shipgroup' : shipgroupslist[k]});
								nlapiLogExecution('Debug', 'Item fulfillment created', 'Success');
							}
							else
							{
								nlapiLogExecution('Debug', 'from so', 'from so');
								TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment', { 'shipgroup' : shipgroupslist[k]});

							}
						}
						catch(e) {

							var exceptionname='TransferOrder';
							var functionality=vebizOrdNo+" - " + 'ItemFulFillMent';
							var trantype=2;
							var userId = nlapiGetUser();
							nlapiLogExecution('Debug', 'DetailsError', functionality);	
							nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
							nlapiLogExecution('Debug', 'vebizOrdNo355', vebizOrdNo);
							var reference3="";
							var reference4 ="";
							var reference5 ="";
							var exceptiondetails=e;
							if ( e instanceof nlobjError )
							{
								nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
								exceptiondetails=e.getCode() + '\n' + e.getDetails();
							}
							else
							{
								exceptiondetails=e.toString();
								nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
							}
							UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
							InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
							nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		
						}
					}
				}
				else
				{
					try
					{
						nlapiLogExecution('Debug', 'from to', 'from to');
						TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment', { 'shipgroup' : shipgroupslist[k]});
					}

					catch(e) {

						var exceptionname='TransferOrder';
						var functionality='Item Fulfillment';
						var trantype=2;
						nlapiLogExecution('Debug', 'DetailsError', functionality);	
						nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
						nlapiLogExecution('Debug', 'vebizOrdNo356', vebizOrdNo);
						var reference2="";
						var reference3="";
						var reference4 ="";
						var reference5 ="";
						var alertType='1';
						var exceptiondetails=e;
						if ( e instanceof nlobjError )
						{
							nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
							exceptiondetails=e.getCode() + '\n' + e.getDetails();
						}
						else
						{
							exceptiondetails=e.toString();
							nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
						}
						//errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
						UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
						InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
						nlapiLogExecution('Debug', 'Exception in TransformRec (TransferOrder) : ', e);		
					}
				}
				nlapiLogExecution('Debug','TransformRec', TransformRec);
				if(TransformRec!=null && TransformRec!='')
				{
					for(var i=0; i<filterLineArray.length;i++ ) // This array contains all disctinct lines no for the order
					{ 
						var context = nlapiGetContext();
						nlapiLogExecution('Debug','Remaining usage at the start of Item fulfillment line# ' + i,context.getRemainingUsage());

						var actuvalqty="";
						var skunumber="";
						var linenumber="";
						var totalqty=0;	
						var ItemType="";
						var batchno="";
						var serialno="";var serialout="";
						var filterlinenumber=filterLineArray[i];
						//By Ganesh for batch# entry
						var vPrevbatch;
						var vPrevLine;
						var vLotNo=null;
						var VLotNo1;
						var vLotQty=0;
						var vLotTotQty=0;
						var boolfound = false;
						var serialsspaces = "";
						var itemname = TransformRec.getLineItemText('item', 'item', filterlinenumber);
						var itemvalue = TransformRec.getLineItemValue('item', 'item', filterlinenumber);
						nlapiLogExecution('DEBUG', 'itemname before replace', itemname);


						for(var y=0; y<itemListArray.length;y++) // This array contails all the data for the order
						{ 
							var vlinenumber =itemListArray[y][2];
							nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
							nlapiLogExecution('Debug', 'vlinenumber', vlinenumber);
							if(filterlinenumber==vlinenumber)
							{
								itemvalue =itemListArray[y][6];
								itemname =itemListArray[y][5];
								if(itemvalue==null || itemvalue=='')
									itemvalue=itemListArray[y][7];
								itemname=itemListArray[y][8];
							}
						}	

						if(itemname!=null && itemname!='')
						{
							itemname=itemname.replace(/ /g,"-");
							nlapiLogExecution('Debug', 'itemname1', itemname);
							nlapiLogExecution('Debug', 'itemvalue', itemvalue);
							nlapiLogExecution('Debug', 'itemListArray.length', itemListArray.length);
							var itemTypesku = nlapiLookupField('item', itemvalue, 'recordType');
							nlapiLogExecution('Debug', 'itemTypesku', itemTypesku);
							var searchresultsitem = nlapiLoadRecord(itemTypesku, itemvalue);
							var SkuNo=searchresultsitem.getFieldValue('itemid');
							nlapiLogExecution('Debug', 'SkuNo', SkuNo);

							var filters = new Array(); 			 
							filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);		

							var columns1 = new Array(); 
							columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
							columns1[1] = new nlobjSearchColumn( 'memberquantity' );

							var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 
							if(itemTypesku=='kititem')
							{
								//Code added on 5th Sep 2013 by Suman.
								var KitIsPicked=ISKitItemcompletlyPicked(itemvalue,vebizOrdNo,vWMSCarrier);
								nlapiLogExecution("DEBUG","KitIsPicked",KitIsPicked);

								if(KitIsPicked=="true")
								{
									//End of code as of 5th Sep.

									for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
									{ 
										linenumber =itemListArray[n][2];



										nlapiLogExecution('Debug', 'linenumber', linenumber);
										nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);

										for(var w=0; w<searchresults.length;w++) 
										{
											fulfilmentItem = searchresults[w].getValue('memberitem');
											memberitemqty = searchresults[w].getValue('memberquantity');

											if((filterlinenumber==linenumber && itemListArray[n][1]==fulfilmentItem))
											{
												QuilfiedItemArray.push(itemvalue);
												QuilfiedItemLineArray.push(linenumber);
												QuilfiedItemLineKitArray.push(linenumber);
												nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
												nlapiLogExecution('Debug', 'totalqty', totalqty);
												skunumber=itemListArray[n][1];
												var columns;

												batchno =itemListArray[n][3];

												if(serialno=="")
												{
													serialno =itemListArray[n][4];}
												else
												{ 
													serialno =serialno+","+itemListArray[n][4];
												}

												if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
												{

													vPrevbatch=itemname;
													itemListArray[n][3] = itemname;
													vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
													nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
												}

												if (boolfound) {
													//totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);
													totalqty = Math.floor(parseFloat(totalqty) + parseFloat(itemListArray[n][0])/memberitemqty);

													vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
													serialsspaces += " " + itemListArray[n][4];


													if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
													{
														if(itemListArray[n][0] != null && itemListArray[n][0] != "")
															vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
													}
													else
													{ 	
														nlapiLogExecution('Debug', 'itemname2', itemname);										

														if(vLotNo!=null && vLotNo != "")
															vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
														else
															vLotNo = vPrevbatch +"(" + vLotQty + ")";

														if(itemListArray[n][0] != null && itemListArray[n][0] != "")
															vLotQty=parseFloat(itemListArray[n][0]);
														vPrevbatch=itemListArray[n][3];
														vPrevLine=linenumber;
													}

												} else {

													if(skunumber!= null && skunumber != "")
													{
														columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
														ItemType = columns.recordType;
														nlapiLogExecution('Debug', 'ItemType in else', ItemType);
													}

													//totalqty = parseFloat(itemListArray[n][0]);
													totalqty = Math.floor(parseFloat(itemListArray[n][0])/memberitemqty);
													vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
													serialsspaces = itemListArray[n][4];

													if(itemListArray[n][0] != null && itemListArray[n][0] != "")
														vLotQty=parseFloat(itemListArray[n][0]);
													vPrevbatch=itemListArray[n][3];
													vPrevLine=linenumber;
												}
												boolfound = true;
											}
										}
									}
								}
							}
							else
							{
								for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
								{ 
									linenumber =itemListArray[n][2];

									nlapiLogExecution('Debug', 'linenumber', linenumber);
									nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);

									if((filterlinenumber==linenumber))
									{
										QuilfiedItemArray.push(itemvalue);
										QuilfiedItemLineArray.push(linenumber);
										nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
										nlapiLogExecution('Debug', 'totalqty', totalqty);
										skunumber=itemListArray[n][1];
										var columns;

										batchno =itemListArray[n][3];

										if(serialno=="")
										{
											serialno =itemListArray[n][4];}
										else
										{ 
											serialno =serialno+","+itemListArray[n][4];
										}

										if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
										{

											vPrevbatch=itemname;
											itemListArray[n][3] = itemname;
											vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
											nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
										}

										if(vAdvBinManagement)
										{
											if (ItemType == "serializedinventoryitem")
											{
												nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
												/*var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');*/
												var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
												if(compSubRecord !=null && compSubRecord!='')
												{
													compSubRecord.selectLineItem('inventoryassignment', 1);
												}
												else
												{
													compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
													compSubRecord.selectNewLineItem('inventoryassignment');
												}
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', itemListArray[n][0]);
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][4]);

												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}
//											if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//											{
//											nlapiLogExecution('Debug', 'Using vAdvBinManagement5',vAdvBinManagement);
//											nlapiLogExecution('Debug', 'itemListArray[n][0]',itemListArray[n][0]);
//											nlapiLogExecution('Debug', 'itemListArray[n][3]',itemListArray[n][3]);
//											var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//											nlapiLogExecution('Debug', 'Using vAdvBinManagement6',vAdvBinManagement);
//											compSubRecord.selectNewLineItem('inventoryassignment');
//											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', parseInt(itemListArray[n][0]));
//											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][3]);

//											compSubRecord.commitLineItem('inventoryassignment');
//											nlapiLogExecution('Debug', 'Using vAdvBinManagement7',vAdvBinManagement);
//											compSubRecord.commit();
//											nlapiLogExecution('Debug', 'Using vAdvBinManagement8',vAdvBinManagement);
//											}
										}
										if (boolfound) {
											totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);

											vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
											serialsspaces += " " + itemListArray[n][4];


											if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
											{
												if(itemListArray[n][0] != null && itemListArray[n][0] != "")
													vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
											}
											else
											{ 	
												nlapiLogExecution('Debug', 'itemname2', itemname);										

												if(vLotNo!=null && vLotNo != "")
													vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
												else
													vLotNo = vPrevbatch +"(" + vLotQty + ")";

												if(itemListArray[n][0] != null && itemListArray[n][0] != "")
													vLotQty=parseFloat(itemListArray[n][0]);
												vPrevbatch=itemListArray[n][3];
												vPrevLine=linenumber;
											}

										} else {

											if(skunumber!= null && skunumber != "")
											{
												columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
												ItemType = columns.recordType;
												nlapiLogExecution('Debug', 'ItemType in else', ItemType);
											}

											totalqty = parseFloat(itemListArray[n][0]);
											vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
											serialsspaces = itemListArray[n][4];

											if(itemListArray[n][0] != null && itemListArray[n][0] != "")
												vLotQty=parseFloat(itemListArray[n][0]);
											vPrevbatch=itemListArray[n][3];
											vPrevLine=linenumber;
										}
										boolfound = true;
									}
								}	
							}

							nlapiLogExecution('Debug', 'skuvalue', skunumber);
							nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
							nlapiLogExecution('Debug', 'totalqty', totalqty);
							nlapiLogExecution('Debug', 'batchno', batchno);
							nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);

							var SOLength = TransformRec.getLineItemCount('item');  // To get the no of lines from item fulfillment record
							nlapiLogExecution('Debug', "SO Length", SOLength);    
							var itemIndex=0;
							var SOWMSCarrier = TransformRec.getFieldValue('custbody_salesorder_carrier');
							for (var j = 1; j <= SOLength; j++) {

								var item_id = TransformRec.getLineItemValue('item', 'item', j);
								// To get Item Type
								//Below code commented for governance issue and this is code is not using
								//var columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
								var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
								var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);
								if(trantype=="transferorder"){
									itemLineNo=parseFloat(itemLineNo)-1;
								}
								var NSUOM = TransformRec.getLineItemValue('item', 'units', j);  
								nlapiLogExecution('Debug', "itemIndex", itemIndex); 
								nlapiLogExecution('Debug', "itemLineNo", itemLineNo);
								if (itemLineNo == filterlinenumber) {
									itemIndex=j;    
									nlapiLogExecution('Debug', "itemIndex", itemIndex);
								}
							}
							if(itemIndex!=0)
							{
								var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
								var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
								itemname=itemname.replace(/ /g,"-");
								var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
								var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);
								nlapiLogExecution('Debug', 'itemloc2', itemloc2);

								var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);
								nlapiLogExecution('Debug', 'itemIndex', itemIndex);
								nlapiLogExecution('Debug', 'itemloc2', itemloc2);
								nlapiLogExecution('Debug', 'NSOrdUOM', NSOrdUOM);

								if(NSOrdUOM!=null && NSOrdUOM!="")
								{
									var veBizUOMQty=0;
									var vBaseUOMQty=0;

									if(vItemDimsArr!=null && vItemDimsArr.length>0)
									{
										for(z=0; z < vItemDimsArr.length; z++)
										{
											if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
											{	
												if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
												{
													vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
													nlapiLogExecution('Debug', 'vBaseUOMQty', vBaseUOMQty);
												}

												if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
												{
													nlapiLogExecution('Debug', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
													veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
													nlapiLogExecution('Debug', 'veBizUOMQty', veBizUOMQty);
												}
											}
										}	
										if(veBizUOMQty!=0)
											totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
										nlapiLogExecution('Debug', 'totalqty', totalqty);

										vLotQty =totalqty;
									}
								}

								if (boolfound) {
									if(confirmLotToNS=='N')
										vPrevbatch=itemname;									


									if(vLotNo!=null && vLotNo != "")
										vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
									else
										vLotNo = vPrevbatch +"(" + vLotQty + ")";


									nlapiLogExecution('Debug', 'vLotNo',vLotNo);
									nlapiLogExecution('Debug', 'vLotQty',vLotQty);

									TransformRec.selectLineItem('item', itemIndex);
									TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
									nlapiLogExecution('Debug', 'totalqty', totalqty);
									TransformRec.setCurrentLineItemValue('item', 'quantity', totalqty);
									nlapiLogExecution('Debug', 'itemloc2', itemloc2);
									TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

									nlapiLogExecution('Debug', 'ItemType', ItemType);

									if(vAdvBinManagement)
									{
										if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
										{
											//if(itemListArray!=null && itemListArray!='')
											//{
											//var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
											//var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
											//for(var i1=0;i1<itemListArray.length;i1++)
											//{
//											if(itemListArray[i1][0] != null && itemListArray[i1][0] != "")
//											vLotQty=parseInt(itemListArray[i1][0]);
//											vPrevbatch=itemListArray[i1][3];


//											if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//											{
											nlapiLogExecution('Debug', 'totalqty',totalqty);
											nlapiLogExecution('Debug', 'Using vPrevbatch',vLotNo);
											var compSubRecord = TransformRec.editCurrentLineItemSubrecord('item','inventorydetail');
											if(compSubRecord !=null && compSubRecord!='')
											{
												compSubRecord.selectLineItem('inventoryassignment', 1);
											}
											else
											{
												compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
											}

											//compSubRecord.selectNewLineItem('inventoryassignment');
											//compSubRecord.selectLineItem('inventoryassignment',1);
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', totalqty);
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);
											compSubRecord.commitLineItem('inventoryassignment');
											compSubRecord.commit();	
										}
									}
									if(!vAdvBinManagement)
									{
										if (ItemType == "lotnumberedinventoryitem") {
											nlapiLogExecution('Debug', 'vLotNo', vLotNo);									
											TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
											nlapiLogExecution('Debug', 'vBatchno', vBatchno);
										}
										else if(ItemType == "lotnumberedassemblyitem") 
										{
											TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
											nlapiLogExecution('Debug', 'vLotNo', vLotNo);
										}
										else if (ItemType == "serializedinventoryitem") {
											var serialsspaces = "";
											if(serialsspaces==null || serialsspaces=='')
											{
												var filters= new Array();				
												nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
												filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
												filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));

												var column =new Array(); 

												column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

												var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
												if(searchresultser!=null && searchresultser!='')
												{
													for ( var b = 0; b < searchresultser.length; b++ ) {
														if(serialsspaces==null || serialsspaces=='')
														{
															serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
														}
														else
														{
															serialsspaces += " " + searchresultser[b].getValue('custrecord_serialnumber');
														}
													}
												}


												nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);
											}
											nlapiLogExecution('Debug', 'before serialsspaces', serialsspaces);
											TransformRec.setCurrentLineItemValue('item','serialnumbers', serialsspaces);
										}
									}
									vLotNo="";
								}
								TransformRec.commitLineItem('item');
							}
						}
					}
//					Case# 20124622 Start//Issue Fix for the so having lines with different site then itemfulfillment was failing if we generate the wave for single site
					var SOLengthnew = TransformRec.getLineItemCount('item');
					if(parseInt(filterLineArray.length)!=parseInt(SOLengthnew))
					{
						for(var k1=1;k1<=SOLengthnew;k1++)
						{
							var isLinenoComitted=false;
							var linenum= TransformRec.getLineItemValue('item', 'line', k1);
							for(var k2=0;k2<filterLineArray.length;k2++)
							{
								var filterlineno=filterLineArray[k2];
								if(parseInt(linenum)==parseInt(filterlineno))
								{
									isLinenoComitted=true;
									break;
								}

							}
							if(isLinenoComitted==false)
							{
								nlapiLogExecution('Debug', 'itemIndex in', k1);
								TransformRec.selectLineItem('item', k1);

								TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

								TransformRec.commitLineItem('item');
							}
						}
					}
					//Case# 20124622 End
					//nlapiLogExecution('Debug', 'itemloc', itemloc);
					nlapiLogExecution('Debug', 'Before Submit', '');
					try
					{
						var TransformRecId = nlapiSubmitRecord(TransformRec, true);
					}
					catch(e)
					{
						var exceptionname='SalesOrder';
						var functionality='Creating Item Fulfillment';
						var trantype=2;
						nlapiLogExecution('Debug', 'DetailsError', functionality);	
						nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
						nlapiLogExecution('Debug', 'vebizOrdNo357', vebizOrdNo);
						var reference2="";
						var reference3="";
						var reference4 ="";
						var reference5 ="";
						var alertType=1;//1 for exception and 2 for report
						var exceptiondetails=e;
						if ( e instanceof nlobjError )
						{
							nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
							exceptiondetails=e.getCode() + '\n' + e.getDetails();
						}
						else
						{
							exceptiondetails=e.toString();
							nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
						}
						InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
						UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
						var ErrorCode=e.getCode();
						var vErrorMsg=e.message;
						nlapiLogExecution('Debug', 'Debug', e.getCode());
						nlapiLogExecution('Debug', 'Debug', e.message);
						nlapiLogExecution('Debug', 'Debug', e);
					}
					nlapiLogExecution('Debug', 'After Submit', '');
				}

				//Code added on 5th Sep 2013 by suman.
				if(TransformRecId!=null&&TransformRecId!="")
				{
					if(QuilfiedItemArray!=null&&QuilfiedItemArray!=""&&QuilfiedItemArray.length!=0)
					{
						nlapiLogExecution('Debug', 'QuilfiedItemArray', QuilfiedItemArray);
						var QuilfiedItemArray=removeDuplicateElementOld(QuilfiedItemArray);
						var QuilfiedItemLineArray=removeDuplicateElementOld(QuilfiedItemLineArray);
						var QuilfiedItemLineKitArray=removeDuplicateElementOld(QuilfiedItemLineKitArray);
						UpdateAllOpenTaskRecordsWithNSID(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineArray,vcontainerLp,QuilfiedItemLineKitArray,FULFILLMENTATORDERLEVEL);
					}
				}
				/*for ( var i = 0; i < searchresult.length; i++ ) {
					nlapiLogExecution('Debug', 'searchresult[i].getId()', searchresult[i].getId());
					nlapiLogExecution('Debug', 'TransformRecId', TransformRecId);
					nlapiSubmitField(searchresult[i].getRecordType(), searchresult[i].getId(), 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
				}*/
				//Start Case#: 20123475
				if(TransformRecId !=null && TransformRecId !='')
					AutoPackingAfterfulfillment(SalesOrderInternalId,itemloc,vebizOrdNo,
							FULFILLMENTATORDERLEVEL,vcontainerLp);
				//End Case#: 20123475	
				//							if(trantype=="salesorder" && TransformRecId !=null && TransformRecId !='' && totalWeight != null && totalWeight != '' && parseFloat(totalWeight) != 0 && totalWeight!=0.0 )
				//							{
				//							nlapiSubmitField('salesorder', SalesOrderInternalId, 'custbody_total_weight', parseFloat(totalWeight));
				//							}
			}
		}
	}



	}
	nlapiLogExecution('Debug', 'Out of createItemFulfillmentMultiLineShipping');
}
