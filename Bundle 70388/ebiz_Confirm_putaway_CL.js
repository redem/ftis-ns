/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_Confirm_putaway_CL.js,v $
 *     	   $Revision: 1.4.6.1.4.1.6.1 $
 *     	   $Date: 2015/09/21 10:45:25 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_7 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Confirm_putaway_CL.js,v $
 * Revision 1.4.6.1.4.1.6.1  2015/09/21 10:45:25  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.4.6.1.4.1  2013/09/11 15:23:51  rmukkera
 * Case# 20124376
 *
 * Revision 1.4.6.1  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4  2011/07/21 09:04:33  jrnarsupalli
 * CASE201112/CR201113/LOG201121
 * Added function onDelete
 *
 * Revision 1.3  2011/07/21 06:42:08  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

 //This script is executed when a user clicks on ADD Button at Line Level.
function myGenLocItemAddClick_putaway()
{	
	try {
		//Declaring Variables.
		var LocRec;
		var LocID;
		var RemainCube;
		var LocName;
		var LocIdArray = new Array();
		var RemainCubeArray = new Array();
		var LocNameArray = new Array();
		var ItemCubeCalc = 0.0;
		
		var itemid = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_poskuid');		
		var itemLoc = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_location');			
		var itemQty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_quantity');
		var confirmflag =nlapiGetCurrentLineItemValue('custpage_items', 'custpage_polocgen');
		if (confirmflag == 'T') 
		{
			if (itemLoc != "" && itemLoc != null) //itemLoc != null || 
			{
				var flag = 0;
				var itemrecLoad = nlapiLoadRecord('inventoryitem', itemid);
				var itemCube = itemrecLoad.getFieldValue('custitem_ebiznet_item_cube');
				
				//var itemCube = nlapiGetFieldValue('custpage_line_itemcubevalue');
				var itemCubeCalc = itemQty * itemCube;
				
				var locRecLoad = nlapiLoadRecord('customrecord_ebiznet_location', itemLoc);
				var remainCube = locRecLoad.getFieldValue('custrecord_remainingcube');
				
				if (itemCubeCalc <= remainCube) {
					var remainlinecubeValue = remainCube - itemCubeCalc;
					locRecLoad.setFieldValue('custrecord_remainingcube', parseFloat(remainlinecubeValue).toFixed(5));
					nlapiSubmitRecord(locRecLoad, false, true);
					alert("Bin Location assigned");
					flag = 1;
					
				}
				else {
					alert("Bin location not found");
					flag = 2;
				}
				
			} //End of if block for null Location Validation.
		}
	 }//End of try block.
	catch(err)	
	{
		nlapiLogExecution("DEBUG","Into Catch block of individual lines :" ,err);
	}	
	if (flag==1)
	{
		return true;
	}
	else if(flag==2)
	{
		return false;
	}
	else
	{
		return true;
	}
 }


function onSave()
{
	var lineCnt = nlapiGetLineItemCount('custpage_items');
	var i=0;
	
	for (var s = 1; s <= lineCnt; s++)
	{
		var lineLoc= nlapiGetLineItemValue('custpage_items', 'custpage_location', s);
		var confirmFlag= nlapiGetLineItemValue('custpage_items', 'custpage_polocgen', s);
		if(lineLoc=="" && confirmFlag== "T")
		{
			alert("Please enter values(s)for: Bin Location");
			i=1;
			return false;
		}
		
	}
	if (i == 0) {
		return true;
	}
	
}

function onDelete()
{	alert("Record Cannot Be Deleted.");
	return false;
}
