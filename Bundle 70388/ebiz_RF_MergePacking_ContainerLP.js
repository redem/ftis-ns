/***************************************************************************
�������������������������eBizNET
�������������������� eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_MergePacking_ContainerLP.js,v $
*� $Revision: 1.1.2.6.4.3.4.9 $
*� $Date: 2014/08/28 06:24:41 $
*� $Author: skreddy $
*� $Name: b_WMS_2015_2_StdBundle_Issues $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_MergePacking_ContainerLP.js,v $
*� Revision 1.1.2.6.4.3.4.9  2014/08/28 06:24:41  skreddy
*� case # 201410140
*� DCD SB issue fix
*�
*� Revision 1.1.2.6.4.3.4.8  2014/06/13 13:10:06  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.1.2.6.4.3.4.7  2014/06/06 07:07:49  skavuri
*� Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
*�
*� Revision 1.1.2.6.4.3.4.6  2014/05/30 00:41:02  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.1.2.6.4.3.4.5  2014/04/21 16:05:27  skavuri
*� Case # 20148097 SB Issue fixed
*�
*� Revision 1.1.2.6.4.3.4.4  2014/04/04 15:30:53  skreddy
*� case 20127679
*� mhp sb  issue fix
*�
*� Revision 1.1.2.6.4.3.4.3  2013/07/09 09:43:23  snimmakayala
*� CASE201112/CR201113/LOG2012392
*� GSUSA :: Issue Fixes
*� Case# : 20123239
*�
*� Revision 1.1.2.6.4.3.4.2  2013/06/11 14:30:19  schepuri
*� Error Code Change ERROR to DEBUG
*�
*� Revision 1.1.2.6.4.3.4.1  2013/04/18 07:30:11  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.1.2.6.4.3  2012/11/01 14:55:23  schepuri
*� CASE201112/CR201113/LOG201121
*� Decimal Qty Conversions
*�
*� Revision 1.1.2.6.4.2  2012/09/26 12:28:40  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.1.2.6.4.1  2012/09/24 14:23:04  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi Lnaguage
*�
*� Revision 1.1.2.6  2012/09/03 13:52:07  schepuri
*� CASE201112/CR201113/LOG201121
*� added date stamp
*�
*� Revision 1.1.2.5  2012/05/17 22:52:39  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� Packing
*�
*� Revision 1.1.2.4  2012/05/17 12:15:21  schepuri
*� CASE201112/CR201113/LOG201121
*� modified CONTAINER label with CARTON
*�
*� Revision 1.1.2.3  2012/05/16 23:27:41  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� Merge cortons
*�
*� Revision 1.1.2.2  2012/05/08 09:30:27  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� RF packing
*�
*� Revision 1.1.2.1  2012/04/24 14:48:38  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� Packing
*�
*
****************************************************************************/


/**
 * @param request
 * @param response
 */
function MergePackingContainerLp(request,response)
{
	if (request.getMethod() == 'GET') 
	{   
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6;
		
		if( getLanguage == 'es_ES')
		{
			st1 = "ENTER / SCAN LP CAJA # :";
			st2 = "ENTER / SCAN SIZE:";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "CAJA CERRADO";
			st6 = "CIERRE Y SALIR";
			
			
		}
		else
		{
			st1 = "ENTER/SCAN CARTON LP #: ";
			st2 = "ENTER/SCAN SIZE: ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "CLOSE CARTON";
			st6 = "CLOSE&EXIT";
		
		}
		
		var getOrderno = request.getParameter('custparam_orderno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
		//var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		//var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var vBatchno = request.getParameter('custparam_batchno');
		var vlinecount = request.getParameter('custparam_linecount');
		var vloopcount = request.getParameter('custparam_loopcount'); 
		var enterqty =  request.getParameter('custparam_enteredqty'); 
		var getContlpno=request.getParameter('custparam_contlpno');
		var resultsCount =request.getParameter('custparam_count');

		var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
		getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
		getLPContainerSize= SORec.getFieldText('custrecord_container');

		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
	//	html = html + " document.getElementById('enterlp').focus();";        
		html = html + "</script>";        
		html = html + "</head><body>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getContainerLPNo + "</label>";
		html = html + "				<input type='hidden' name='hdnOrderNo' value=" + getOrderno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLPNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";        
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnlinecount' value=" + vlinecount + ">";
		html = html + "				<input type='hidden' name='hdnloopcount' value=" + vloopcount + ">";
		html = html + "				<input type='hidden' name='hdnenterqty' value=" + enterqty + ">";
		html = html + "				<input type='hidden' name='hdncontlpno' value=" + getContlpno + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getLPContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerLPNo + ">";
		html = html + "				<input type='hidden' name='hdnresultscount' value=" + resultsCount + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getLPContainerSize + "</label>";			
		html = html + "				</td>";
		html = html + "			</tr>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersize' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='ENT' />";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +"<input name='cmdClose' type='submit' value='F8' />";
		html = html + "					"+ st6 +" <input name='cmdExit' type='submit' value='F10'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage =  request.getParameter('hdngetLanguage');
		
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st7;
		
		if( getLanguage == 'es_ES')
		{
			
			st7 = "ORDEN NO V&#193;LIDO #";
			
		}
		else
		{
			
		    st7 = "INVALID ORDER #";
			
		}
		
		
		var varEnteredLP = request.getParameter('enterlp');
		nlapiLogExecution('DEBUG', 'Entered LP', varEnteredLP);

		var varEnteredSize = request.getParameter('entersize');
		nlapiLogExecution('DEBUG', 'Entered Size', varEnteredSize);

		var  varhidcontlp = request.getParameter('hdncontlpno');
		nlapiLogExecution('DEBUG', 'varhidcontlp--->', varhidcontlp);

		var SOarray = new Array();
		
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_error"] = st7;//'INVALID Order #';
		SOarray["custparam_screenno"] = '27A';
		SOarray["custparam_orderno"] = request.getParameter('hdnOrderNo');
		var getOrderNo = request.getParameter('hdnOrderNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray['custparam_linecount'] = request.getParameter('hdnlinecount');
		SOarray['custparam_loopcount'] = request.getParameter('hdnloopcount');
		SOarray['custparam_enteredqty'] = request.getParameter('hdnenterqty');
		SOarray['custparam_count']=request.getParameter('hdnresultscount');
		SOarray['custparam_contlpno']=varhidcontlp;
		
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');
		var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var Item = request.getParameter('hdnItem');
		var ItemDescription = request.getParameter('hdnItemDescription');
		var ItemInternalId = request.getParameter('hdnItemInternalId');
		var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var EndLocation = request.getParameter('hdnEnteredLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var getCortonNo = request.getParameter('hdncontlpno');
		var getresultscount= request.getParameter('hdnresultscount');

		var getExpQty = request.getParameter('hdnenterqty');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');   

		var optedSend = request.getParameter('cmdSend');

		var optedExit = request.getParameter('cmdExit');
		var optedClose = request.getParameter('cmdClose');

		if (varEnteredLP != '' && varEnteredLP != null) 
		{
			if(varEnteredLP==request.getParameter('hdnFetchedContainerLPNo'))
			{
				SOarray["custparam_containerlpno"] = varEnteredLP;
				nlapiLogExecution('DEBUG', 'varEnteredLP', varEnteredLP);
			}
			else if(varEnteredLP != '' && varEnteredLP!=request.getParameter('hdnFetchedContainerLPNo'))
			{
				nlapiLogExecution('DEBUG', 'varEnteredLP', 'userdefined');
				var getEnteredContainerNoPrefix = varEnteredLP.substring(0, 3).toUpperCase();
				var LPReturnValue = ebiznet_LPRange_CL_withLPType(varEnteredLP.replace(/\s+$/,""), '2','2',null);//'2'UserDefiend,'2'PICK
				if(LPReturnValue == true){
					nlapiLogExecution('DEBUG', 'getItem', ItemInternalId);
					var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
					var itemCube = 0;
					var itemWeight=0;						
					if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
						itemCube = (parseFloat(ExpectedQuantity) * parseFloat(arrDims[0]));
						itemWeight = (parseFloat(ExpectedQuantity) * parseFloat(arrDims[1]));//									
					} 
					nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
					nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);
					nlapiLogExecution('DEBUG', 'ContainerSize:ContainerSize', varEnteredSize);		

					
					
					
					
					nlapiLogExecution('DEBUG','ContainerLpNew',request.getParameter('hdnFetchedContainerLPNo'));
					
					var filter=new Array();
					filter[0]=new nlobjSearchFilter('custrecord_ebiz_inv_lp',null,'is',request.getParameter('hdnFetchedContainerLPNo'));

				
					var InvtsearchRec = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filter,null);
					
					if(InvtsearchRec!=null&&InvtsearchRec!="")
					{
						for(var k1 = 0; k1 < InvtsearchRec.length; k1++)
						{
							var InvtRecID=InvtsearchRec[k1].getId();
							nlapiSubmitField('customrecord_ebiznet_createinv',InvtRecID,'custrecord_ebiz_inv_lp',varEnteredLP);
						}
					}

					var ContainerCube;					
					var containerInternalId;
					var ContainerSize;
					if(varEnteredSize=="" || varEnteredSize==null)
					{
						getContainerSize=ContainerSize;
						nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	
						ContainerCube = itemCube;
					}	
					else
					{
						var arrContainerDetails = getContainerCubeAndTarWeight("",varEnteredSize);					
						if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

							ContainerCube =  parseFloat(arrContainerDetails[0]);						
							containerInternalId = arrContainerDetails[3];
							ContainerSize=arrContainerDetails[4];
							TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));// added on 28 nov
						} 
					}
					nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);				
					//Insert LP Record
					CreateRFMasterLPRecord(varEnteredLP,containerInternalId,ContainerCube,itemWeight,varEnteredSize);
				}
				else{
					nlapiLogExecution('DEBUG', 'fasle LPReturnValue', LPReturnValue);
					SOarray["custparam_error"] = "INVALID LP#";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
			}
			else
			{
				var containerLP = GetMaxLPNo('1', '2','');
				//Code If new ContainerLp is given
				var mastlpcustomrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
				mastlpcustomrecord.setFieldValue('name', containerLP);
				mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', containerLP);
				mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 2);//2 stands for PICK
//				mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', varcontintid);
//				mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght',LpTotalWt);        	        	
//				mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_site', wmslocation);  

				nlapiLogExecution('DEBUG', 'Before LP Record calling','Before LP Record calling');
				var LpMastrec = nlapiSubmitRecord(mastlpcustomrecord, false, true);
				nlapiLogExecution('DEBUG','LPMastRec',LpMastrec);
				varEnteredLP=containerLP;

				var filter=new Array();
				filter[0]=new nlobjSearchFilter('custrecord_ebiz_inv_lp',null,'is',request.getParameter('hdnFetchedContainerLPNo'));

				var InvtsearchRec=nlapiSearchRecord('customrecord_ebiznet_createinv',null,filter,null);
				/*if(InvtsearchRec!=null&&InvtsearchRec!="")
				{
					var InvtRecID=InvtsearchRec[0].getId();
					nlapiSubmitField('customrecord_ebiznet_createinv',InvtRecID,'custrecord_ebiz_inv_lp',varEnteredLP);
				}*/
				
				if(InvtsearchRec!=null&&InvtsearchRec!="")
				{
					for(var k2 = 0; k2 < InvtsearchRec.length; k2++)
					{
						var InvtRecID=InvtsearchRec[k2].getId();
						nlapiSubmitField('customrecord_ebiznet_createinv',InvtRecID,'custrecord_ebiz_inv_lp',varEnteredLP);
					}
				}
			}
		}
		else 
		{

			varEnteredLP=request.getParameter('hdnFetchedContainerLPNo');

		}			
		if (varEnteredSize != '' && varEnteredSize != null) 
		{
			SOarray["custparam_contsize"] = varEnteredSize;
			nlapiLogExecution('DEBUG', 'Container Size', varEnteredSize);	 
		}
		else
		{
			varEnteredSize=request.getParameter('hdnContainerSize');
		}


		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.

		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_mergepacking_orderno', 'customdeploy_rf_mergepacking_orderno_di', false, SOarray);
		}

		else if(optedClose == 'F8'||optedExit == 'F10')
		{
			var now = new Date();
			var CurrentDate = DateStamp();
			var SOFilters = new Array();
			if(getOrderNo!=null && getOrderNo!='')
				SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));

			if(getCortonNo!=null && getCortonNo!='')
				SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', getCortonNo));

			SOFilters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));

			SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof', [8]));
		

			var SOColumns = new Array();
			SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
			SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
			SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
			SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
			SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
			SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
			SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
			SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
			SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
			SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
			SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
			SOColumns[14] = new nlobjSearchColumn('name');

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

			for ( var count = 0; count < SOSearchResults.length; count++) 
			{
				var RecID=SOSearchResults[count].getId();
				var ExpQty=SOSearchResults[count].getValue('custrecord_expe_qty');
				var name=SOSearchResults[count].getValue('name');
				var fields=new Array();
				fields[0]='custrecord_wms_status_flag';
				fields[1]='custrecord_act_qty';
				fields[2]='custrecord_act_end_date';
				fields[3]='custrecord_container_lp_no';
				fields[4]='custrecord_from_lp_no';

				var Values=new Array();
				Values[0]=28;
				Values[1]=parseFloat(ExpQty).toFixed(4);
				Values[2]=DateStamp();
				Values[3]=varEnteredLP;
				Values[4]=ContainerLPNo;
				
				var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',RecID,fields,Values);
				nlapiLogExecution('DEBUG','UpdatedRecId',UpdatedRecID);

				//creating the pack task
				if(parseFloat(count)==parseFloat(SOSearchResults.length)-1)
				{
					var varname = SOSearchResults[count].getValue('name');

					var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',RecID);
					createopentaskrec.setFieldValue('name',name);
					createopentaskrec.setFieldValue('custrecord_tasktype',14);//pack task
					createopentaskrec.setFieldValue('custrecord_pack_confirmed_date',CurrentDate);
					createopentaskrec.setFieldValue('custrecord_sku', null);
					createopentaskrec.setFieldValue('custrecord_act_qty', 0);

					var newID=nlapiSubmitRecord(createopentaskrec, false, true);
					nlapiLogExecution('DEBUG','newID',newID);
				}
			}
			response.sendRedirect('SUITELET', 'customscript_rf_mergepacking_orderno', 'customdeploy_rf_mergepacking_orderno_di', false, SOarray);
		}
		else if(optedSend == 'ENT')
		{

			var now = new Date();
			var CurrentDate = DateStamp();
			var SOFilters = new Array();
			if(getOrderNo!=null && getOrderNo!='')
				SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));

			if(getCortonNo!=null && getCortonNo!='')
				SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', getCortonNo));

			SOFilters.push(new nlobjSearchFilter('custrecord_pack_confirmed_date',null, 'isempty', null));

			SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof', [8]));

			var SOColumns = new Array();
			SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
			SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
			SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
			SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
			SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
			SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
			SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
			SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
			SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
			SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
			SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
			SOColumns[14] = new nlobjSearchColumn('name');

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			// Case# 20148097 starts
			if(SOSearchResults!=''&& SOSearchResults!='null'&& SOSearchResults!=null)
				{
			// Case# 20148097 ends
			for ( var count = 0; count < SOSearchResults.length; count++) 
			{
				var RecID=SOSearchResults[count].getId();
				var ExpQty=SOSearchResults[count].getValue('custrecord_expe_qty');
				var fields=new Array();
				fields[0]='custrecord_wms_status_flag';
				fields[1]='custrecord_act_qty';
				fields[2]='custrecord_act_end_date';
				fields[3]='custrecord_container_lp_no';
				fields[4]='custrecord_from_lp_no';
				

				var Values=new Array();
				Values[0]=28;
				Values[1]=parseFloat(ExpQty).toFixed(4);
				Values[2]=DateStamp();
				Values[3]=varEnteredLP;
				Values[4]=ContainerLPNo;

				var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',RecID,fields,Values);
				nlapiLogExecution('DEBUG','UpdatedRecId',UpdatedRecID);
					//creating the pack task
					if(parseFloat(count)==parseFloat(SOSearchResults.length)-1)
					{
						nlapiLogExecution('DEBUG','create Pack task',RecID);
						var varname = SOSearchResults[count].getValue('name');

						var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',RecID);
						createopentaskrec.setFieldValue('name',varname);
						createopentaskrec.setFieldValue('custrecord_tasktype',14);//pack task
						createopentaskrec.setFieldValue('custrecord_pack_confirmed_date',CurrentDate);
						createopentaskrec.setFieldValue('custrecord_sku', null);
						createopentaskrec.setFieldValue('custrecord_act_qty', 0);

						var newID=nlapiSubmitRecord(createopentaskrec, false, true);
						nlapiLogExecution('DEBUG','newID',newID);
					}

			}
			response.sendRedirect('SUITELET', 'customscript_rf_mergepacking_orderno', 'customdeploy_rf_mergepacking_orderno_di', false, SOarray);
				}// if 'SOSearchResults' close Case# 20148097 
		}
	}
}
	
	function CreateRFMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,ItemWeight,getContainerSize)
	{
		var contLPExists = containerRFLPExists(vContLpNo);
		var LPWeightExits = getLPWeight(vContLpNo);
		if(LPWeightExits!=null && LPWeightExits!="")
		{		
			var ContLP=LPWeightExits[0];
			var LPWeight=LPWeightExits[1];
			nlapiLogExecution('DEBUG', 'LPWeight',LPWeight);
		}
		var TotalWeight;	

		if(getContainerSize=="" || getContainerSize==null)
		{
			TotalWeight = ItemWeight;
		}	
		else
		{
			var arrContainerDetails = getContainerCubeAndTarWeight(containerInternalId,"");

			if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) 
			{		
				TotalWeight = (parseFloat(ItemWeight) + parseFloat(arrContainerDetails[1]));
			} 
		}


		if(contLPExists==null){		

			var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
			MastLP.setFieldValue('name', vContLpNo);
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
			if(containerInternalId!="" && containerInternalId!=null)
			{
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
			}	
			nlapiLogExecution('DEBUG', 'TotalWeight',TotalWeight);
			nlapiLogExecution('DEBUG', 'ContainerCube',ContainerCube);
			if(TotalWeight!="" && TotalWeight!=null)
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(4));
			if(ContainerCube!="" && ContainerCube!=null)
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));
			var currentContext = nlapiGetContext();		        		
			var retktoval = nlapiSubmitRecord(MastLP);
			nlapiLogExecution('DEBUG', 'Master LP Insertion','Success');

		}
		else
		{
			var recordId="";

			if(contLPExists!=null){


				for (var i = 0; i < contLPExists.length; i++){
					recordId = contLPExists[i].getId();
					nlapiLogExecution('DEBUG', 'recordId',recordId);		

					var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	
					TotWeight=parseFloat(ItemWeight)+parseFloat(LPWeight);
					nlapiLogExecution('DEBUG', 'itemWeight',ItemWeight);
					nlapiLogExecution('DEBUG', 'LPWeight',LPWeight);
					nlapiLogExecution('DEBUG', 'TotWeight',TotWeight);

					if(TotWeight!="" && TotWeight!=null)
						MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(4));
					if(ContainerCube!="" && ContainerCube!=null)
						MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', ContainerCube);

					var currentContext = nlapiGetContext();	        				
					nlapiSubmitRecord(MastLPUpdate, false, true);        				 
					nlapiLogExecution('DEBUG', 'Master LP Updation','Success');

				}

			}
		}
	}
	
	function containerRFLPExists(lp){
		var filters = new Array();
		nlapiLogExecution('DEBUG', 'lp:',lp); 
		filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
		nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
		columns[0].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
		return searchresults;
	}
	
	function getLPWeight(lp){
		var filters = new Array();
		nlapiLogExecution('DEBUG', 'lp:',lp); 
		filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
		nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
		columns[0].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
		var lpmastArr=new Array();
		nlapiLogExecution('DEBUG', 'Before:',lp); 
		if(searchresults != null && searchresults!= "" && searchresults.length>0)
		{
			lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
			lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));
			nlapiLogExecution('DEBUG', 'lp:',searchresults[0].getValue('custrecord_ebiz_lpmaster_lp')); 
			nlapiLogExecution('DEBUG', 'lpweight:',searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght')); 
		}
		return lpmastArr;
	}