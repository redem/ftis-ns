/***************************************************************************
 eBizNET Solutions Inc                
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_BulkPickingContainer.js,v $
 *     	   $Revision: 1.1.2.6 $
 *     	   $Date: 2014/06/13 13:28:08 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_BulkPickingContainer.js,v $
 * Revision 1.1.2.6  2014/06/13 13:28:08  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.5  2014/06/06 07:17:33  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.4  2014/06/03 15:45:01  skavuri
 * Case# 20148688 SB Issue Fixed
 *
 * Revision 1.1.2.3  2014/05/30 00:41:00  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2  2013/08/23 06:13:10  snimmakayala
 * Case# 20124032
 * NLS - UAT ISSUES
 * Quantity is displaying wrongly on RF bulk picking screen for SHIPASIS items.
 *
 * Revision 1.1.2.1  2013/07/19 08:18:22  gkalla
 * Case# 20123527
 * Bulk picking CR for Nautilus
 *
 * Revision 1.8.2.17.4.8.2.10  2013/06/24 15:45:32  grao
 * CASE201112/CR201113/LOG201121
 * NLS SB Issues fixes
 * Wave Generation - Wave already completed
 * Issue :  While generating picks if system generates failed picks and if we scan that wave# in RF picking system is saying " Wave already completed" . The expected error message is " Invalid Wave#"
 *
 * Revision 1.8.2.17.4.8.2.9  2013/06/19 22:56:06  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 * 
 *****************************************************************************/
function BulkPickingContainer(request, response){
	nlapiLogExecution('DEBUG', 'Into Request', 'Into Request');

	var getLanguage = request.getParameter('custparam_language');
	nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
	var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12;

	if( getLanguage == 'es_ES')
	{
		st1 = "N&#218;MERO DE ONDA";
		st2 = "N&#218;MERO DEL PEDIDO DE VENTA ";
		st3 = "N&#218;MERO DE ZONA "; 
		st4 = "ENVIAR ";
		st5 = "ANTERIOR ";


	}
	else
	{
		st1 = "WAVE NO ";
		st2 = "SALES ORDER NO ";
		st3 = "ZONE NO "; 
		st4 = "SEND ";
		st5 = "PREV ";

	}


	if (request.getMethod() == 'GET') 
	{
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterwaveno').focus();";  

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+ st1;//WAVE NO
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterwaveno' id='enterwaveno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+ st3;//ZONE NO;		 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterzoneno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		  
		html = html + "			<tr>";		 
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//	html = html + "				<td align = 'left'>" + st6 + "<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;cmdPrevious.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>" + st4 + "<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st5 + "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterwaveno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');


		var getLanguage = request.getParameter('hdngetLanguage');

		var st8,st9,st10,st11,st12;

		if( getLanguage == 'es_ES')
		{
			st8 = "OPCI&#211;N V&#193;LIDA ";
			st9 = "VENTAS DE PEDIDO NO V&#193;LIDO # ";
			st10 = "ZONA NO V&#193;LIDO ";
			st11 = "NO V&#193;LIDO WAVE # ";
			st12 = "ORDEN NO V&#193;LIDO # ";
			st13 = "SCAN WAVE # ";
			st14 = "SCAN ZONE # ";
		}
		else
		{
			st8 = "INVALID OPTION ";
			st9 = "INVALID SALES ORDER # ";
			st10 = "INVALID ZONE ";
			st11 = "INVALID WAVE # ";
			st12 = "INVALID ORDER # ";
			st13 = "SCAN WAVE # ";
			st14 = "SCAN ZONE # ";
		}

		var getWaveNo = request.getParameter('enterwaveno');
		//getOrdNo=request.getParameter('enterorderno');
		//var getSONo=request.getParameter('entersono');

		var getZoneNo = request.getParameter('enterzoneno');
		// This variable is to hold the SO# entered.
		var SOarray = new Array();

		SOarray["custparam_error"] = st8;//INVALID OPTION
		SOarray["custparam_screenno"] = 'BULK01';
		var vSkipId=request.getParameter('hdnskipid');
		vSkipId=0;
		SOarray["custparam_skipid"] = vSkipId;
		SOarray["custparam_language"] = getLanguage;

		//nlapiLogExecution('DEBUG', 'Wave #', getWaveNo);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di', false, SOarray);
		}
		else 
		{
			if(getWaveNo == null || getWaveNo == '')
			{
				SOarray["custparam_error"] = st13;//'INVALID ZONE';

				nlapiLogExecution('DEBUG', 'Error: ', 'Zone not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}
			else if(getZoneNo == null || getZoneNo == '')
			{
				SOarray["custparam_error"] = st14;//'INVALID ZONE';

				nlapiLogExecution('DEBUG', 'Error: ', 'Zone not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}

			nlapiLogExecution('DEBUG', 'getWaveNo', getWaveNo);
			//nlapiLogExecution('DEBUG', 'getOrdNo', getOrdNo);
			//nlapiLogExecution('DEBUG', 'getSO', getSONo);
			nlapiLogExecution('DEBUG', 'getZoneNo', getZoneNo);
			var vSOId;

			var vZoneId;

			var SOFilters = new Array();
			if(getWaveNo != null && getWaveNo != '')
			{	 
				nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
			}
			var vZoneId;
			if(getZoneNo != null && getZoneNo != '')
			{	
				vZoneId= vGetgetZoneId(getZoneNo);
				if(vZoneId=='ERROR')
				{
					SOarray["custparam_error"] = st10;//'INVALID ZONE';
					SOarray["custparam_screenno"] = 'BULK01';
					nlapiLogExecution('DEBUG', 'Error: ', 'Zone not found');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;	
				}
				else
				{
					nlapiLogExecution('DEBUG', 'getZoneNo inside if', getZoneNo);
					nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);
					SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
				}
			}
			/*if(getOrdNo != null && getOrdNo != '')
			{
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', getOrdNo));
			}*/	
			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));//pick gen
			var SOColumns = new Array();

			//SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group'));			 
			SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq',null,'group'));
			SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku',null,'group'));
			SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty',null,'sum'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc',null,'group'));		
			SOColumns.push(new nlobjSearchColumn('custrecord_skudesc',null,'group'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group')); 
			SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no',null,'group')); 
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid',null,'group')); 
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group'));
			SOColumns.push(new nlobjSearchColumn('custrecord_wms_location',null,'group'));
			SOColumns.push(new nlobjSearchColumn('custrecord_comp_id',null,'group'));
			//SOColumns.push(new nlobjSearchColumn('custrecord_container',null,'group'));

			SOColumns[0].setSort();
			SOColumns[1].setSort();
			SOColumns[2].setSort();
			SOColumns[3].setSort(3);
			SOColumns[4].setSort();
			SOColumns[5].setSort();

			var SOSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			nlapiLogExecution('DEBUG', 'Wave SearchResults of given Order', SOSearchResults1);
			if (SOSearchResults1 != null && SOSearchResults1 != '') {
				var SOSearchResult=SOSearchResults1[0];
				SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no',null,'group');
				SOarray["custparam_zoneid"] = vZoneId;
				SOarray["custparam_zoneno"] = getZoneNo;
				SOarray["custparam_ebizzoneno"] = vZoneId;
				
				SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
				SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
				SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
				SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku',null,'group');
				SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku',null,'group');
				SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc',null,'group');
				SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

				SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
				SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id',null,'group');


				//SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container',null,'group');
				//SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no',null,'group');

				SOarray["custparam_containersize"] ='';
				SOarray["custparam_recordinternalid"]=SOSearchResult.getId();// Case# 20148688
				SOarray["custparam_venterwave"]=getWaveNo;
				SOarray["custparam_venterzone"]=getZoneNo;
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_bulkpick_location', 'customdeploy_ebiz_rf_bulkpick_location', false, SOarray);
				return;
			}
			else
			{
				SOarray["custparam_error"] = 'INVALID WAVE AND ZONE COMBINATION';
				nlapiLogExecution('DEBUG', 'Error: ', 'INVALID WAVE');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}

		} 
	}
}
 
function vGetgetZoneId(getZoneNo)
{
	if(getZoneNo != null && getZoneNo != '')
	{
		nlapiLogExecution('DEBUG', 'Into Zone Validate', 'Into Zone Validate');
		var ZoneFilters = new Array();
		var ZoneColumns = new Array();
		nlapiLogExecution('DEBUG', 'getZoneNo', getZoneNo);
		ZoneFilters.push(new nlobjSearchFilter('custrecord_putzoneid', null, 'is', getZoneNo));
		ZoneFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		var ZoneSearchResults = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters, ZoneColumns);
		if(ZoneSearchResults != null && ZoneSearchResults != '' && ZoneSearchResults.length>0)
		{
			nlapiLogExecution('DEBUG', 'ZoneSearchResults[0].getId()', ZoneSearchResults[0].getId());
			return ZoneSearchResults[0].getId();
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Into Zone Validate Else', 'Into Zone Validate Else');
			var ZoneFilters1 = new Array();
			var ZoneColumns1 = new Array();
			ZoneFilters1.push(new nlobjSearchFilter('name', null, 'is', getZoneNo));
			ZoneFilters1.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			var ZoneSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters1, ZoneColumns1);
			if(ZoneSearchResults1 != null && ZoneSearchResults1 != '' && ZoneSearchResults1.length>0)
			{
				return ZoneSearchResults1[0].getId();
			}
			else
			{
				return 'ERROR';
				/*SOarray["custparam_error"] = st10;//'INVALID ZONE';
				SOarray["custparam_screenno"] = '12';
				nlapiLogExecution('DEBUG', 'Error: ', 'Zone not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;*/
			}
		}
	} 
}