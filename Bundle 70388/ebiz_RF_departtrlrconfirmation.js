/***************************************************************************
 eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_departtrlrconfirmation.js,v $
 *     	   $Revision: 1.2.4.4.4.5.2.14.2.2 $
 *     	   $Date: 2015/02/03 13:44:57 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_departtrlrconfirmation.js,v $
 * Revision 1.2.4.4.4.5.2.14.2.2  2015/02/03 13:44:57  schepuri
 * issue # 201411500
 *
 * Revision 1.2.4.4.4.5.2.14.2.1  2014/08/18 15:21:35  skavuri
 * Case# 201410026 Std Bundle Issues Fixed
 *
 * Revision 1.2.4.4.4.5.2.14  2014/06/06 07:12:23  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.2.4.4.4.5.2.13  2014/05/30 00:41:01  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.4.4.5.2.12  2014/05/12 15:35:05  skavuri
 * Case # 20147973 SB Issue Fixed
 *
 * Revision 1.2.4.4.4.5.2.11  2014/03/28 15:17:28  skavuri
 * Case # 20127850 issue fixed
 *
 * Revision 1.2.4.4.4.5.2.10  2014/01/17 14:49:21  nneelam
 * case#  20126839
 * Depart Trailer Issue fix in RF
 *
 * Revision 1.2.4.4.4.5.2.9  2014/01/09 14:08:21  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.2.4.4.4.5.2.8  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.2.4.4.4.5.2.7  2013/12/12 07:34:54  nneelam
 * Case# 20126067�
 * Navigation to Main menu if no is clicked..
 *
 * Revision 1.2.4.4.4.5.2.6  2013/12/12 07:31:02  nneelam
 * Case# 20126067�
 * Navigation to seal page if no is clicked..
 *
 * Revision 1.2.4.4.4.5.2.5  2013/10/24 14:31:03  schepuri
 * 20125170
 *
 * Revision 1.2.4.4.4.5.2.4  2013/07/02 15:56:28  rmukkera
 * Case# 20123274
 * 1.During RF trailer load , if we load multiple SHIP LPS, after departing the trailer  only one order ( of  the last SHIP LP) status is changing from pending fulfillment to pending billing. Rest of the SHIP LPS related order status is in pending fulfillment only.- 20123274
 *
 * Revision 1.2.4.4.4.5.2.3  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.4.4.4.5.2.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.4.4.5.2.1  2013/03/22 11:45:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.4.4.4.5  2013/02/21 07:21:47  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2.4.4.4.4  2012/12/24 13:19:21  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for nulls
 *
 * Revision 1.2.4.4.4.3  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.4.4.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.4.4.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.4.4  2012/06/21 10:15:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.2.4.3  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.2  2012/02/22 12:46:20  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2.4.1  2012/02/10 10:49:32  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing  related creating shipmanifest record at carriertype "LTL"
 *
 * Revision 1.2  2011/09/28 13:23:13  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 *
 *****************************************************************************/
function TrailerDepartConfirmationMenu(request, response)
{

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') 
	{
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "&#191;QUIERES SALIR DEL REMOLQUE";
			st2 = "SI";
			st3 = "NO";


		}
		else
		{
			st0 = "";
			st1 = "DO YOU WANT TO DEPART THE TRAILER";
			st2 = "YES";
			st3 = "NO";

		}


		var trailerno = request.getParameter('custparam_trlrno');
		nlapiLogExecution('DEBUG', 'starting of the page trailerno', trailerno);

		var sealno = request.getParameter('custparam_sealno');
		nlapiLogExecution('DEBUG', 'starting of the page sealno', sealno);

		var getshiplp = request.getParameter('custparam_shilp');
		nlapiLogExecution('DEBUG', 'starting of the page getshiplp', getshiplp);

		var functionkeyHtml=getFunctionkeyScript('_rfload'); 
		var html = "<html><head><title>" + st0  +" </title>";  
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends

		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";


		html = html + "</script>";		

		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfload' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				<input type='hidden' name='hdntrlrNo' value='" + trailerno + "'>";
		html = html + "				<input type='hidden' name='hdnsealNo' value=" + sealno + ">";
		html = html + "				<input type='hidden' name='hdnshiplp' value=" + getshiplp + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>"; 
		html = html + "			<tr>";
		//case # 20126839 , Removed the onclick event
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdYes' type='submit' value='F8'/>";
		//End
		html = html + "					" + st3 + " <input name='cmdNo' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');	
		var optedEvent = request.getParameter('cmdPrevious');	
		var trailerName = request.getParameter('hdntrlrNo');
		var sealno = request.getParameter('hdnsealNo');
		var shiplp = request.getParameter('hdnshiplp');
		var deptconfirmarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		deptconfirmarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', deptconfirmarray["custparam_language"]);


		var st5;
		if( getLanguage == 'es_ES')
		{

			st5 = "SALIDA TRAILER: FAILED";

		}
		else
		{

			st5 = "TRAILER DEPART: FAILED";

		}

		deptconfirmarray["custparam_trlrno"]=trailerName;	
		deptconfirmarray["custparam_error"] = st5;
		deptconfirmarray["custparam_screenno"] = '42';
		//case no 20125170� 

		deptconfirmarray["custparam_sealno"]=sealno;	
		deptconfirmarray["custparam_shilp"]=shiplp;	



		var PronId,Actdept,sealno,Tripno,trailerName;
		if (sessionobj!=context.getUser()) {
			try
			{
				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (request.getParameter('cmdNo') == 'F7') {
					//case # 20126067�,navigated to seal screen.
					response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, deptconfirmarray);
					//response.sendRedirect('SUITELET', 'customscript_rf_departtrlrconfirmation', 'customdeploy_rf_deprttrlrconfirmation_di', false, deptconfirmarray);
				}
				else 
				{
					if (request.getParameter('cmdYes') == 'F8') {
						nlapiLogExecution('DEBUG', 'into f8', 'f8');
						var recordId=getrecordinternalidforTrailer(trailerName);
						if(recordId != null && recordId != '')
						{
							nlapiLogExecution('DEBUG', 'Done recordId', recordId);				

							var pronoresearch = nlapiLoadRecord('customrecord_ebiznet_trailer', recordId); 
							PronId = pronoresearch.getFieldValue('custrecord_ebizpro');
							Actdept=pronoresearch.getFieldValue('custrecord_ebizdepartdate');
							sealno = pronoresearch.getFieldValue('custrecord_ebizseal');
							Tripno=pronoresearch.getFieldValue('custrecord_ebizvehicle');
							trailerName=pronoresearch.getFieldValue('name');
						}


						var createShiManifestRecordResult=distinctordcount(shiplp,PronId,Tripno,Actdept,sealno,trailerName);
						if(createShiManifestRecordResult != -1){
							nlapiLogExecution('DEBUG', 'Done shipmanifest customrecord', 'Success');
							var transaction = nlapiLoadRecord('customrecord_ebiznet_trailer', recordId);	
							transaction.setFieldValue('custrecord_ebizseal', sealno);
							transaction.setFieldValue('custrecord_ebizdepartdate', DateStamp());
							var opentaskRecordId = nlapiSubmitRecord(transaction, true);
							// Case # 20127850 starts
							updateShipTask(shiplp);
							// Case # 20127850 ends
							response.sendRedirect('SUITELET', 'customscript_rf_trlrdeparted_success', 'customdeploy_rf_trlrdeparted_success_di', false, deptconfirmarray);
						}
						else{
							nlapiLogExecution('DEBUG', 'shipmanifest customrecord failed ', 'Fail');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, deptconfirmarray);

						}

					}
				}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, deptconfirmarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			deptconfirmarray["custparam_screenno"] = '36';
			deptconfirmarray["custparam_error"] = 'TRAILER ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, deptconfirmarray);
		}

	}
}
function getrecordinternalidforTrailer(trailerName){
	nlapiLogExecution('DEBUG', 'Done trailerName', trailerName);
	var trailerId;
	var trailerFilers = new Array(); 

	trailerFilers.push(new nlobjSearchFilter('name', null, 'is', trailerName.toString()));

	var trailerColumns = new Array();
	trailerColumns[0] = new nlobjSearchColumn('name');

	var trailerSearchResults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, trailerFilers, trailerColumns);

	if(trailerSearchResults != null && trailerSearchResults != '')
	{
		for (var i = 0; i < trailerSearchResults.length; i++) {
			trailerId = trailerSearchResults[0].getId();		 
		}
	}

	return trailerId;
}

function distinctordcount(tshiplp,PronId,Tripno,Actdept,sealno,trailerName)
{
	nlapiLogExecution('ERROR', "distinctordcount", tshiplp);
	var shiplpArray=tshiplp.split(',');

	for(var k=0;k<shiplpArray.length;k++)
	{
		var shiplp=shiplpArray[k];
		var count=1;
		var ordarrray = new Array();
		var distinctordFilers = new Array(); 
		var salesOrderNumber='';
		distinctordFilers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shiplp)); 

		var distinctordColumns = new Array();
		distinctordColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

		var distinctordSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, distinctordFilers, distinctordColumns);

		if(distinctordSearchResults != null && distinctordSearchResults!=""){
			nlapiLogExecution('DEBUG', "distinctordSearchResults", distinctordSearchResults.length);	
			if(distinctordSearchResults.length == 1){
				var Salesord=distinctordSearchResults[0].getValue('custrecord_ebiz_order_no');
				nlapiLogExecution('DEBUG', "Salesordp if", Salesord);			 

				var trantype = nlapiLookupField('transaction', Salesord, 'recordType');	
				nlapiLogExecution('DEBUG', "trantype", trantype);
				var salesorderheadresearch;
				//var salesorderheadresearch = nlapiLoadRecord('salesorder', distinctordSearchResults[i].getValue('custrecord_ebiz_order_no'));
				if(trantype=='transferorder')
					salesorderheadresearch = nlapiLoadRecord('transferorder', Salesord);
				//Case# 201410026 starts
				else if(trantype=='salesorder')
					salesorderheadresearch = nlapiLoadRecord('salesorder', Salesord);
				else
					salesorderheadresearch = nlapiLoadRecord('vendorreturnauthorization', Salesord);
				//Case# 201410026 ends
				nlapiLogExecution('DEBUG', "salesorderheadresearch", salesorderheadresearch);

				var soname = salesorderheadresearch.getFieldValue('tranid');
				var carrier=salesorderheadresearch.getFieldText('custbody_salesorder_carrier');
				nlapiLogExecution('DEBUG', "after salesorder", Salesord);
				var weight=getweight(distinctordSearchResults[0].getValue('custrecord_ebiz_order_no'));
				var createShiManifestRecordResult = createShiManifestRecord(PronId,Tripno,Actdept,sealno,trailerName,Salesord,carrier,weight,soname);
			}
			else{
				for (var i = 0; i < distinctordSearchResults.length; i++){
					if (distinctordSearchResults[i].getValue('custrecord_ebiz_order_no') != null && distinctordSearchResults[i].getValue('custrecord_ebiz_order_no') != '')
					{
						if (salesOrderNumber.indexOf(distinctordSearchResults[i].getValue('custrecord_ebiz_order_no')) == -1 || i==0 ){

							var Salesord=distinctordSearchResults[i].getValue('custrecord_ebiz_order_no');
							nlapiLogExecution('DEBUG', "Salesordp else", Salesord);
							count +=1;

							//var salesorderheadresearch = nlapiLoadRecord('salesorder', distinctordSearchResults[i].getValue('custrecord_ebiz_order_no'));

							var trantype = nlapiLookupField('transaction', Salesord, 'recordType');	
							nlapiLogExecution('DEBUG', "trantype", trantype);
							var salesorderheadresearch;
							if(trantype=='transferorder')
							{
								salesorderheadresearch = nlapiLoadRecord('transferorder', Salesord);
							}
							//Case# 201410026 starts
							else if(trantype=='salesorder')
							{
								salesorderheadresearch = nlapiLoadRecord('salesorder', Salesord);
							}
							else
							{
								salesorderheadresearch = nlapiLoadRecord('vendorreturnauthorization', Salesord);
							}
							//Case# 201410026 ends
							nlapiLogExecution('DEBUG', "salesorderheadresearch else", salesorderheadresearch);

							var soname = salesorderheadresearch.getFieldValue('tranid');
							var carrier=salesorderheadresearch.getFieldText('custbody_salesorder_carrier');
							nlapiLogExecution('DEBUG', "after salesorder", Salesord);
							var weight=getweight(distinctordSearchResults[i].getValue('custrecord_ebiz_order_no'));
							var createShiManifestRecordResult = createShiManifestRecord(PronId,Tripno,Actdept,sealno,trailerName,Salesord,carrier,weight,soname);

						}			
						salesOrderNumber =salesOrderNumber+","+distinctordSearchResults[i].getValue('custrecord_ebiz_order_no');			
					}
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', "record count", count);
	return count;
}

function getweight(SalesordId)
{
	nlapiLogExecution('DEBUG', "getweight", SalesordId);
	var weight;
	var closedTaskFilers = new Array(); 

	closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesordId)); 

	var closedTaskColumns = new Array();
	closedTaskColumns[0] = new nlobjSearchColumn('custrecord_total_weight');

	var closedTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, closedTaskFilers, closedTaskColumns);
	if(closedTaskSearchResults!=null && closedTaskSearchResults!='')
	{
		for (var i = 0; i < closedTaskSearchResults.length; i++) {
			weight = closedTaskSearchResults[0].getValue('custrecord_total_weight');
		}
	}
	return weight;
}

function createShiManifestRecord(PronId,Tripno,Actdept,sealno,trailerName,soname,carrier,weight,sonubertext) {
	nlapiLogExecution('DEBUG', "into createShiManifestRecord", 'success');

	nlapiLogExecution('DEBUG', "PronId", PronId);
	nlapiLogExecution('DEBUG', "Tripno", Tripno);
	nlapiLogExecution('DEBUG', "Actdept", Actdept);
	nlapiLogExecution('DEBUG', "sealno", sealno);
	nlapiLogExecution('DEBUG', "trailerName", trailerName);
	nlapiLogExecution('DEBUG', "soname", soname);
	nlapiLogExecution('DEBUG', "carrier", carrier);
	nlapiLogExecution('DEBUG', "weight", weight);

	var string="";
	if(carrier !=null && carrier !=""){
		string = carrier+"-" ;
	}
	if(Tripno !=null && Tripno !=""){
		string += Tripno+"-" ;
	}
	if(trailerName !=null && trailerName !=""){
		string += trailerName+"-" ;
	}
	if(Actdept !=null && Actdept !=""){
		string += Actdept+"-" ;
	}
	if(sealno !=null && sealno !=""){
		string += sealno ;
	}
	var lp=string;
	var nsconf='';
	var filter = new Array();//
	filter.push(new nlobjSearchFilter('custrecord_ship_contlp',null,'is',lp));
	filter.push(new nlobjSearchFilter('custrecord_ship_order',null,'anyof',soname));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ship_nsconf_no');
	var manifestList= nlapiSearchRecord('customrecord_ship_manifest',null,filter,columns);
	if(manifestList!=null && manifestList!=''){
		nsconf = manifestList[0].getValue('custrecord_ship_nsconf_no');
	}

	var ShiManifestRecord = nlapiCreateRecord('customrecord_ship_manifest');

	ShiManifestRecord.setFieldValue('name', soname);
	ShiManifestRecord.setFieldValue('custrecord_ship_orderno', sonubertext);//case# 201411500,201411059
	//ShiManifestRecord.setFieldValue('custrecord_ship_orderno', soname);
	ShiManifestRecord.setFieldValue('custrecord_ship_trackno', PronId);
	ShiManifestRecord.setFieldValue('custrecord_ship_contlp', lp);
	ShiManifestRecord.setFieldValue('custrecord_ship_carrier', 'LTL');
	//ShiManifestRecord.setFieldValue('custrecord_ship_pkgwght', weight);
	if(weight==null || weight=='')
		weight='0.1';
	ShiManifestRecord.setFieldValue('custrecord_ship_actwght', parseFloat(weight).toFixed(4));
	ShiManifestRecord.setFieldValue('custrecord_ship_charges', 0);
	ShiManifestRecord.setFieldValue('custrecord_ship_void', 'U');
	if(nsconf!='' && nsconf!=null)
		ShiManifestRecord.setFieldValue('custrecord_ship_nsconf_no', nsconf);
	nlapiLogExecution('DEBUG', "Before createShiManifestRecord", '');
	var recid = nlapiSubmitRecord(ShiManifestRecord);
	nlapiLogExecution('DEBUG', "After createShiManifestRecord", 'success');

}
//Case # 20127850
function updateShipTask(shiplp)
{
	nlapiLogExecution('ERROR', "Into updateShipTask:shiplp", shiplp);

	var shipTaskFilers = new Array();
	var shipTaskColumns = new Array();

	if(shiplp!=null && shiplp!='')
	{
		shipTaskFilers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shiplp));
		shipTaskFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4])); 	//SHIP Task
		shipTaskFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [10])); // LOADED

		var openTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, shipTaskFilers, shipTaskColumns);
		for (var i = 0; openTaskSearchResults!=null && i < openTaskSearchResults.length; i++) {
			var shipTaskId = openTaskSearchResults[i].getId();

			nlapiLogExecution('ERROR', "updateOpenTask:TaskId", shipTaskId);

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', shipTaskId, 'custrecord_wms_status_flag', '14');
		}
	}

	nlapiLogExecution('ERROR', "Out of updateShipTask");
}
