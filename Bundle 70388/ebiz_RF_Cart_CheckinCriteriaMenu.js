/***************************************************************************
 ebiz_RF_Cart_CheckinCriteriaMenu.js
��������eBizNET Solutions Inc��������
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_CheckinCriteriaMenu.js,v $
<<<<<<< ebiz_RF_Cart_CheckinCriteriaMenu.js
 *� $Revision: 1.3.2.5.4.7.4.9.2.4 $
 *� $Date: 2014/09/10 15:48:50 $
 *� $Author: sponnaganti $
 *� $Name: t_eBN_2014_2_StdBundle_0_90 $
=======
 *� $Revision: 1.3.2.5.4.7.4.9.2.4 $
 *� $Date: 2014/09/10 15:48:50 $
 *� $Author: sponnaganti $
 *� $Name: t_eBN_2014_2_StdBundle_0_90 $
>>>>>>> 1.3.2.5.4.7.4.9.2.2
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_Cart_CheckinCriteriaMenu.js,v $
 *� Revision 1.3.2.5.4.7.4.9.2.4  2014/09/10 15:48:50  sponnaganti
 *� Case# 20149999
 *� Stnd Bundle issue fixed
 *�
 *� Revision 1.3.2.5.4.7.4.9.2.3  2014/07/28 14:59:41  grao
 *� Case#: 20149606 New 2014.2 Compatibilty issue fixes
 *�
 *� Revision 1.3.2.5.4.7.4.9.2.2  2014/07/28 14:56:46  grao
 *� Case#: 20149606 New 2014.2 Compatibilty issue fixes
 *�
 *� Revision 1.3.2.5.4.7.4.9.2.1  2014/07/11 15:14:00  rmukkera
 *� Case # 20149410
 *�
 *� Revision 1.3.2.5.4.7.4.9  2014/06/23 07:08:10  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *�
 *� Revision 1.3.2.5.4.7.4.8  2014/06/16 15:02:19  rmukkera
 *� Case # 20148708
 *�
 *� Revision 1.3.2.5.4.7.4.4  2013/12/02 08:59:59  schepuri
 *� 20126048
 *�
 *� Revision 1.3.2.5.4.7.4.3  2013/06/11 14:30:40  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.3.2.5.4.7.4.2  2013/04/17 16:04:01  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.3.2.5.4.7  2012/12/24 15:19:27  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Merged code form 2012.2 branch.
 *�
 *� Revision 1.3.2.5.4.6  2012/12/11 14:51:16  schepuri
 *� CASE201112/CR201113/LOG201121
 *� cart checkin criteriamenu issue
 *�
 *� Revision 1.3.2.5.4.5  2012/11/08 14:01:01  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Dafiti Cart Putaway invalid serial,cart,item no issue fix
 *�
 *� Revision 1.3.2.5.4.4  2012/09/27 13:13:26  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.3.2.5.4.3  2012/09/27 10:53:53  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.3.2.5.4.2  2012/09/26 12:43:15  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.3.2.5.4.1  2012/09/21 14:57:16  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.3.2.5  2012/04/20 10:43:12  schepuri
 *� CASE201112/CR201113/LOG201121
 *� changing the Label of Batch #  field to Lot#
 *�
 *� Revision 1.3.2.4  2012/04/17 10:38:39  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� RF Cart To Checkin
 *�
 *� Revision 1.3.2.3  2012/03/16 13:56:23  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Disable-button functionality is been added.
 *�
 *� Revision 1.3.2.2  2012/02/22 12:14:44  schepuri
 *� CASE201112/CR201113/LOG201121
 *� function Key Script code merged
 *�
 *� Revision 1.3  2012/02/16 10:28:15  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Added FunctionkeyScript
 *�
 *� Revision 1.2  2012/02/14 07:09:56  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Merged upto 1.8.2.1.
 *�
 *� Revision 1.1  2012/02/02 08:54:25  rmukkera
 *� CASE201112/CR201113/LOG201121
 *�   cartlp new file
 *�
 *
 ****************************************************************************/



function CheckinCriteriaMenu(request, response){
	if (request.getMethod() == 'GET') 
	{		
		var getOptedField = request.getParameter('custparam_option');
		var trantype = request.getParameter('custparam_trantype');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		var cartlpno = request.getParameter('custparam_cartlpno');


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6,st7,st8;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "CHECK-IN CRITERIOS";
			st1 = "NUEVA ORDEN DE COMPRA / RECIBO";
			st2 = "NUEVO ART&#205;CULO";
			st3 = "NUEVO NO. DE LOTE";
			st4 = "NUEVA CANTIDAD";	
			// 20126048		
			st5 = "NUEVO CARRO";
			st6 = "REGISTRO DE SALIDA";
			st7 = "INGRESAR SELECCI&#211;N";
			st8 = "CONTINUAR";

		}
		else
		{
			st0 = "CHECK-IN CRITERIA";
			st1 = "NEW PO/RECEIPT";
			st2 = "NEW ITEM";
			st3 = "NEW LOT#";
			st4 = "NEW QTY";
			st5 = "NEW CART";
			st6 = "EXIT CHECK-IN";
			st7 = "ENTER SELECTION";
			st8 = "CONTINUE";			

		}		

		var functionkeyHtml=getFunctionkeyScript('_rfcheckincriteriamenu'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfcheckincriteriamenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 1. " + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 2. " + st2;
		html = html + "				</td>";
		html = html + "			</tr> ";
		/*		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 3. NEW LOT#";
		html = html + "				</td>";
		html = html + "			</tr> ";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 3. " + st4;
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "				<td align = 'left'> 4. " + st5;
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			</tr> ";
		html = html + "				<td align = 'left'> 5. " + st6;
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st7;
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";       
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">"; 
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdncartonlp' value=" + cartlpno + ">"; 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st8 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var optedField = request.getParameter('selectoption');

		// This variable is to hold the Quantity entered.
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;
		POarray["custparam_cartlpno"] = request.getParameter('hdncartonlp');
		POarray["custparam_error"] = 'Invalid Option';
		POarray["custparam_screenno"] = 'CartCheckinMenu';

		//	Get the PO# from the previous screen, which is passed as a parameter		
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('custparam_poqtyentered');
		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('custparam_polinepackcode');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		var trantype=request.getParameter('hdntrantype');
		nlapiLogExecution('DEBUG', 'trantype', trantype);
		nlapiLogExecution('DEBUG', 'request.getParameter(custparam_poid)', request.getParameter('custparam_poid'));
		nlapiLogExecution('DEBUG', 'request.getParameter(custparam_pointernalid)', request.getParameter('custparam_pointernalid'));
		var trantype = nlapiLookupField('transaction', request.getParameter('custparam_pointernalid'), 'recordType');

		var ActualBeginDate = DateStamp();
		var ActualBeginTime = TimeStamp();

		nlapiLogExecution('DEBUG', 'ActualBeginDate', ActualBeginDate);
		nlapiLogExecution('DEBUG', 'ActualBeginTime', ActualBeginTime);

		POarray["custparam_actualbegindate"] = ActualBeginDate;
		var TimeArray = new Array();
		TimeArray = ActualBeginTime.split(' ');

		POarray["custparam_actualbegintime"] = TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = TimeArray[1];

		nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_itemquantity"] = request.getParameter('hdnQuantity');
		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/
		POarray["custparam_trantype"] = trantype;
		/* Up to here */ 

		//code added on 13 feb 2012 by suman
		//To Pass the baseuom qty to query string.
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		//end of code as of 13 feb 2012.

		nlapiLogExecution('DEBUG', 'POarray["custparam_itemcube"]', POarray["custparam_itemcube"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_baseuomqty"]', POarray["custparam_baseuomqty"]);
		
		
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', request.getParameter('custparam_pointernalid')));
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', request.getParameter('custparam_lineno')));

		// get filter based on order type
		if(trantype!='transferorder')
		{
			filters.push(getFilterForOrderType(trantype));
		}		

		var columns = new Array();
		columns[0] = getColumnForTransactionType("CHKN");
		columns[1] = getColumnForTransactionType("ASPW");
		columns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');
		

		
		var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);
		
		if(searchResults!=null && searchResults!='')
		{
			POarray['custparam_poqtyentered']= searchResults[0].getValue('custrecord_orderlinedetails_checkin_qty');
			nlapiLogExecution('DEBUG', 'POarray["custparam_poqtyentered"]', POarray["custparam_poqtyentered"]);
		}

		if(trantype=='purchaseorder')
		{

			if (optedField == '1')
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_po', 'customdeploy_ebiz_rf_cartcheckin_po_di', false,POarray);
			}
			else if (optedField == '2') 
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinsku', 'customdeploy_ebiz_rf_cart_checkinsku_di', false,POarray);
			}
			/*	else if (optedField == '3') 
			{
				//response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false,optedField);
			}*/
			else if (optedField == '3') 
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinqty', 'customdeploy_ebiz_rf_cart_checkinqty_di', false,POarray);
			}
			else if (optedField == '4') 
			{
				var filtersmlp = new Array(); 
				filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartlpno'));

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

				if (SrchRecord != null && SrchRecord.length > 0) {
					var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
					rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'T');
					var recid = nlapiSubmitRecord(rec, false, true);
				}
				else {
					nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

				}

				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartscan', 'customdeploy_ebiz_rf_cartscan_di', false, POarray);
			}
			else if (optedField == '5') 
			{
				nlapiLogExecution('DEBUG', 'request.getParameter(custparam_cartlpno)',request.getParameter('custparam_cartlpno'));
				var filtersmlp = new Array(); 
				filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartlpno'));

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

				if (SrchRecord != null && SrchRecord.length > 0) {
					var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
					rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'T');
					var recid = nlapiSubmitRecord(rec, false, true);
				}
				else {
					nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

				}
				response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false,POarray);
			}
			else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}
		}
		else if(trantype=='returnauthorization')
		{
			if (optedField == '1')
			{
				//response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin', 'customdeploy_rf_rma_checkin_di', false,POarray);
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_rma', 'customdeploy_ebiz_rf_cartcheckin_rma_di', false,POarray);
			}
			else if (optedField == '2') 
			{
				//response.sendRedirect('SUITELET', 'customscript_rf_rmacheckin_sku', 'customdeploy_rf_rmacheckin_sku_di', false,POarray);
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinsku', 'customdeploy_ebiz_rf_cart_checkinsku_di', false,POarray);
			}
			/*	else if (optedField == '3') 
			{
				//response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false,optedField);
			}*/
			else if (optedField == '3') 
			{
				//POarray["custparam_poqtyentered"] ="";
				//response.sendRedirect('SUITELET', 'customscript_rf_rma_checkinqty', 'customdeploy_rf_rmacheckin_qty_di', false,POarray);
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinqty', 'customdeploy_ebiz_rf_cart_checkinqty_di', false,POarray);
			}
			/*else if (optedField == '4') 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false,POarray);
			}
			else if (optedField == '5') 
			{
				nlapiLogExecution('DEBUG', 'request.getParameter(custparam_cartlpno)',request.getParameter('custparam_cartlpno'));
				var filtersmlp = new Array(); 
				filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartlpno'));

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

				if (SrchRecord != null && SrchRecord.length > 0) {
					var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
					rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'T');
					var recid = nlapiSubmitRecord(rec, false, true);
				}
				else {
					nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

				}
				response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false,POarray);
			}*/
			else if (optedField == '4') 
			{
				var filtersmlp = new Array(); 
				filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartlpno'));

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

				if (SrchRecord != null && SrchRecord.length > 0) {
					var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
					rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'T');
					var recid = nlapiSubmitRecord(rec, false, true);
				}
				else {
					nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

				}

				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartscan', 'customdeploy_ebiz_rf_cartscan_di', false, POarray);
			}
			else if (optedField == '5') 
			{
				nlapiLogExecution('DEBUG', 'request.getParameter(custparam_cartlpno)',request.getParameter('custparam_cartlpno'));
				var filtersmlp = new Array(); 
				filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartlpno'));

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

				if (SrchRecord != null && SrchRecord.length > 0) {
					var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
					rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'T');
					var recid = nlapiSubmitRecord(rec, false, true);
				}
				else {
					nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

				}
				response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false,POarray);
			}
			else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}

		}
		else
		{
			if (optedField == '1')
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_po', 'customdeploy_ebiz_rf_cartcheckin_po_di', false,POarray);
			}
			else if (optedField == '2') 
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinsku', 'customdeploy_ebiz_rf_cart_checkinsku_di', false,POarray);
			}
			/*else if (optedField == '3') 
			{
				//response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false,optedField);
			}*/
			else if (optedField == '3') 
			{				
				POarray["custparam_poqtyentered"] ="";
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinqty', 'customdeploy_ebiz_rf_cart_checkinqty_di', false,POarray);
			}
			else if (optedField == '4') 
			{
				var filtersmlp = new Array(); 
				filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartlpno'));

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

				if (SrchRecord != null && SrchRecord.length > 0) {
					var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
					rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'T');
					var recid = nlapiSubmitRecord(rec, false, true);
				}
				else {
					nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

				}
				response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false,POarray);
			}
			else if (optedField == '5') 
			{
				nlapiLogExecution('DEBUG', 'request.getParameter(custparam_cartlpno)',request.getParameter('custparam_cartlpno'));
				var filtersmlp = new Array(); 
				filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartlpno'));

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

				if (SrchRecord != null && SrchRecord.length > 0) {
					var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
					rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'T');
					var recid = nlapiSubmitRecord(rec, false, true);
				}
				else {
					nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

				}
				response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false,POarray);
			}
			else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}
		}

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
