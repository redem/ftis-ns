/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/ebiz_inventoryMoveReport_SL.js,v $
 *     	   $Revision: 1.6.2.3.4.2.4.8.2.1 $
 *     	   $Date: 2015/12/02 15:29:18 $
 *     	   $Author: aanchal $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_215 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_inventoryMoveReport_SL.js,v $
 * Revision 1.6.2.3.4.2.4.8.2.1  2015/12/02 15:29:18  aanchal
 * 2015.2 Issue Fix
 * 201415899
 *
 * Revision 1.6.2.3.4.2.4.8  2014/09/24 15:46:39  sponnaganti
 * Case# 201410499
 * Stnd Bundle Issue fix
 *
 * Revision 1.6.2.3.4.2.4.7  2014/08/14 10:04:18  sponnaganti
 * case# 20149950
 * stnd bundle issue fix
 *
 * Revision 1.6.2.3.4.2.4.6  2014/06/12 14:49:26  grao
 * Case#: 20148789  New GUI account issue fixes
 *
 * Revision 1.6.2.3.4.2.4.5  2014/05/06 15:10:18  sponnaganti
 * case# 20148234
 * (LP is not showing issue)
 *
 * Revision 1.6.2.3.4.2.4.4  2013/06/04 06:40:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inventory Changes
 *
 * Revision 1.6.2.3.4.2.4.3  2013/04/04 20:20:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixes
 *
 * Revision 1.6.2.3.4.2.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.6.2.3.4.2.4.1  2013/03/08 14:41:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.6.2.3.4.2  2012/12/18 15:25:48  rmukkera
 * Paging issue fix
 * t_NSWMS_LOG2012392_86
 *
 * Revision 1.6.2.3.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6.2.3  2012/05/14 13:52:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Inventory Report
 *
 * Revision 1.6.2.2  2012/04/20 12:03:54  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.6.2.1  2012/03/06 09:32:25  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to fetching  and displaying the record .
 *
 * Revision 1.6  2011/12/19 10:51:20  spendyala
 * CASE201112/CR201113/LOG201121
 * added vendor part code and store bin location fields to the sublist.
 *
 * Revision 1.5  2011/12/15 10:40:43  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/12/15 07:45:13  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/12/15 05:42:44  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/12/14 18:40:19  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed Deployment ids
 *
 * Revision 1.1  2011/12/14 14:18:00  mbpragada
 * CASE201112/CR201113/LOG201121
 * Inventory Move report  and pdf new files
 *
 
 *****************************************************************************/
var tempInventoryResultsArray=new Array();
function getInventorySearchResults(maxno,request)
{
	var binlocid = request.getParameter('custpage_binlocation');
	nlapiLogExecution('ERROR', 'binLoc Internal ID', binlocid);
	var vfromdate=request.getParameter('custpage_fromdate');
	nlapiLogExecution('ERROR','vfromdate',vfromdate);
	var vtodate=request.getParameter('custpage_todate');
	nlapiLogExecution('ERROR','vtodate',vtodate);
	var vreportno=request.getParameter('custpage_reportno');
	nlapiLogExecution('ERROR','vreportno',vreportno);

	var itemId = request.getParameter('custpage_item');
	var vItemArr=new Array();
	if(itemId != null && itemId != "")
	{
		vItemArr = itemId.split('');
	}
	nlapiLogExecution('ERROR', 'item Internal ID', vItemArr);

	var LPvalue = request.getParameter('custpage_invtlp');
	nlapiLogExecution('ERROR', 'LP value', LPvalue);

	var vStatus = request.getParameter('custpage_itemstatus');
	nlapiLogExecution('ERROR', 'Item Status ', vStatus);

	var i = 0;
	var filtersinv = new Array();

	if (itemId != "" && itemId!=null) {
		filtersinv.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', vItemArr));
		
	}

	if (binlocid!=null && binlocid != "") {
		filtersinv.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', binlocid));
		
	}
	
	if (vreportno != "" && vreportno !=null) {
		filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_report_no', null, 'is', vreportno));
		
	}
	if (LPvalue != "" && LPvalue !=null) {
		filtersinv.push(new nlobjSearchFilter('custrecord_from_lp_no', null, 'is', LPvalue));
		
	}
	if (request.getParameter('custpage_location') != "" && request.getParameter('custpage_location') != null) {
		filtersinv.push(new nlobjSearchFilter('custrecord_wms_location', null, 'is', request.getParameter('custpage_location')));
		
	}
	if (request.getParameter('custpage_company') != "" && request.getParameter('custpage_company') != null) {
		filtersinv.push(new nlobjSearchFilter('custrecord_comp_id', null, 'is', request.getParameter('custpage_company')));
		
	}
	if (vStatus != null && vStatus != "") {
		//case# 20149950 starts(filter condition changed because we are not getting values when we select only item status)
		//filtersinv.push(new nlobjSearchFilter('custrecord_comp_id', null, 'anyof', vStatus));
		filtersinv.push(new nlobjSearchFilter('custrecord_sku_status', null, 'anyof', vStatus));
		//case# 20149950 ends
	}
	if ( (vfromdate != null && vfromdate != "") && (vtodate != null && vtodate != "") ){
		filtersinv.push(new nlobjSearchFilter('custrecord_current_date', null, 'within', vfromdate, vtodate));
		
	}
	filtersinv.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['9','18']));
	
	//filtersinv[i] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',['3','19']);
	if(maxno!=-1)
	{
		filtersinv.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
		
	}


	var columns = new Array();

	columns.push(new nlobjSearchColumn('internalid').setSort('true'));
	columns.push(new nlobjSearchColumn('name'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	columns.push(new nlobjSearchColumn('custrecord_act_qty'));
	columns.push(new nlobjSearchColumn('custrecord_lpno'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpno'));
	columns.push(new nlobjSearchColumn('custrecord_tasktype'));
	columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));;
	columns.push(new nlobjSearchColumn('custrecord_actendloc'));;

	columns.push(new nlobjSearchColumn('custrecord_sku'));
	columns.push(new nlobjSearchColumn('custrecord_skudesc'));
	columns.push(new nlobjSearchColumn('custrecord_batch_no'));
	columns.push(new nlobjSearchColumn('custrecord_sku_status'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_new_sku_status'));
	columns.push(new nlobjSearchColumn('custrecord_packcode'));;		
	columns.push(new nlobjSearchColumn('custrecord_lotnowithquantity'));
	columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_new_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_ot_adjtype'));
	columns.push(new nlobjSearchColumn('custrecord_from_lp_no'));
	columns.push(new nlobjSearchColumn('custrecord_wms_location'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_report_no'));
	//new column field is added
	columns.push(new nlobjSearchColumn('custitemstore_bin_location','custrecord_sku'));
	columns.push(new nlobjSearchColumn('vendorname','custrecord_sku'));
	columns.push(new nlobjSearchColumn('purchasedescription','custrecord_sku'));

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersinv, columns);
	if(invtsearchresults!=null)
	{
		if(invtsearchresults.length>=1000)
		{
			nlapiLogExecution('ERROR', 'InTo More Than 1000 rec');
			var maxno1=invtsearchresults[invtsearchresults.length-1].getId();
			tempInventoryResultsArray.push(invtsearchresults);
			getInventorySearchResults(maxno1,request);
		}
		else
		{
			nlapiLogExecution('ERROR', 'InTo Less Than 1000 rec');
			tempInventoryResultsArray.push(invtsearchresults);
		}
	}
	return tempInventoryResultsArray;
}
function inventoryMoveReportSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		FillForm(request,response);
	} 
	else 
	{ 
		FillForm(request,response);
	}
}
function FillForm(request,response)
{
	
	var pagesizevalue;
	var form = nlapiCreateForm('Inventory Move Report');
	form.setScript('customscript_inv_move_report');
	var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
	if(request.getParameter('custpage_fromdate')!='' && request.getParameter('custpage_fromdate')!=null)
	{
		fromdate.setDefaultValue(request.getParameter('custpage_fromdate'));	
	}
	var todate = form.addField('custpage_todate', 'date', 'To Date');
	if(request.getParameter('custpage_todate')!='' && request.getParameter('custpage_todate')!=null)
	{
		todate.setDefaultValue(request.getParameter('custpage_todate'));	
	}


	var binLocationField = form.addField('custpage_binlocation', 'select', 'Bin Location');
	binLocationField.addSelectOption('', '');
	if(request.getParameter('custpage_binlocation')!='' && request.getParameter('custpage_binlocation')!=null)
	{
		binLocationField.setDefaultValue(request.getParameter('custpage_binlocation'));	
	}
	nlapiLogExecution('ERROR', 'multiselect',request.getParameter('custpage_item'));

	var itemField ='';
	itemField=form.addField('custpage_item', 'multiselect', 'Item','item');
	var reportno = form.addField('custpage_reportno', 'text', 'Report #');
	if(request.getParameter('custpage_reportno')!='' && request.getParameter('custpage_reportno')!=null)
	{
		reportno.setDefaultValue(request.getParameter('custpage_reportno'));	
	}

	if(request.getParameter('custpage_item')!='' && request.getParameter('custpage_item')!=null)
	{
		var itemValue=request.getParameter('custpage_item');
		var itemArray = new Array();
		itemArray = itemValue.split('');
		nlapiLogExecution('ERROR', 'multiselect',itemArray.length);		
	}
	itemField.setDefaultValue(request.getParameter('custpage_item'));	


	var invtlp = form.addField('custpage_invtlp', 'select', 'LP');
	invtlp.addSelectOption('', '');
	//case# 20148234 starts (LP is not showing issue)
	FillLP(form,invtlp,-1);
	//case# 20148234 end
	if(request.getParameter('custpage_invtlp')!='' && request.getParameter('custpage_invtlp')!=null)
	{
		invtlp.setDefaultValue(request.getParameter('custpage_invtlp'));	
	}

	nlapiLogExecution('ERROR', 'Location',request.getParameter('custpage_location'));
	var varLoc = form.addField('custpage_location', 'select', 'Location','location');
	//varLoc.addSelectOption('', '');
	if(request.getParameter('custpage_location')!='' && request.getParameter('custpage_location')!=null)
	{
		varLoc.setDefaultValue(request.getParameter('custpage_location'));	
	}

	var varComp = form.addField('custpage_company', 'select', 'Company');
	varComp.addSelectOption('', '');

	if(request.getParameter('custpage_company')!='' && request.getParameter('custpage_company')!=null)
	{
		varComp.setDefaultValue(request.getParameter('custpage_company'));	
	}

	var vItemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
	vItemStatus.addSelectOption('', '');
	var filtersStatus = new Array();
	filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));


	var columnsstatus = new Array();
	columnsstatus.push(new nlobjSearchColumn('custrecord_statusdescriptionskustatus'));

	var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersStatus, columnsstatus);

	if (searchStatus != null) 
	{
		for (var i = 0; i < searchStatus.length; i++) 
		{
			var res = form.getField('custpage_itemstatus').getSelectOptions(searchStatus[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                    
			}
			vItemStatus.addSelectOption(searchStatus[i].getId(), searchStatus[i].getValue('custrecord_statusdescriptionskustatus'));
		}
	}

	if(request.getParameter('custpage_itemstatus')!='' && request.getParameter('custpage_itemstatus')!=null)
	{
		vItemStatus.setDefaultValue(request.getParameter('custpage_itemstatus'));	
	}


	/*var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, null, new nlobjSearchColumn('custrecord_ebiz_inv_loc').setSort());

	if (searchlocresults != null)
	{
		nlapiLogExecution('ERROR', 'Inventory count', searchlocresults.length);

		for (var i = 0; i < searchlocresults.length; i++) 
		{
			var res = form.getField('custpage_location').getSelectOptions(searchlocresults[i].getText('custrecord_ebiz_inv_loc'), 'is');
			if (res != null) 
			{
				if (res.length > 0)	                
					continue;	                
			}
			varLoc.addSelectOption(searchlocresults[i].getValue('custrecord_ebiz_inv_loc'), searchlocresults[i].getText('custrecord_ebiz_inv_loc'));
		}
	}      
*/
	var searchcompresults = nlapiSearchRecord('customrecord_ebiznet_company', null, new nlobjSearchFilter('isinactive',null,'is','F'), new nlobjSearchColumn('Name').setSort());

	if (searchcompresults != null)
	{
		for (var i = 0; i < searchcompresults.length; i++) 
		{
			var res = form.getField('custpage_company').getSelectOptions(searchcompresults[i].getValue('Name'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                
			}
			if(searchcompresults[i].getValue('Name') != "")
				varComp.addSelectOption(searchcompresults[i].getValue('Name'), searchcompresults[i].getValue('Name'));
		}
	}

	//3 - In bound Storage
	//19 - Inventory Storage
	nlapiLogExecution('ERROR', 'Test1');
	var filtersinv = new Array();
	filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');

	var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);

	if (searchInventory != null) 
	{
		for (var i = 0; i < searchInventory.length; i++) 
		{
			var res = form.getField('custpage_binlocation').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_binloc'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                    
			}
			binLocationField.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_binloc'), searchInventory[i].getText('custrecord_ebiz_inv_binloc'));
		}		

		//case# 20148234 starts (LP is not showing issue)
		
		/*for (var i = 0; i < searchInventory.length; i++) 
		{
			invtlp.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_lp'), searchInventory[i].getValue('custrecord_ebiz_inv_lp'));
		}*/
		//case# 20148234 end
		nlapiLogExecution('ERROR', 'Test2',request.getParameter('custpage_checkfield'));



		if (request.getParameter('custpage_checkfield') == null || request.getParameter('custpage_checkfield') == '') //&& request.getParameter('custpage_checkfield') == "") {
		{  

			form.addField('custpage_checkfield', 'select', 'check').setDisplayType('hidden');

			var sublist = form.addSubList("custpage_invtmovelist", "list", "Inventory Move List");
			sublist.addField("custpage_sno", "text", "SL NO").setDisplayType('disabled');		
			sublist.addField("custpage_reportno", "text", "Report #").setDisplayType('inline');	
			sublist.addField("custpage_invlocation", "text", "Bin Location").setDisplayType('inline');
			sublist.addField("custpage_itemname", "text", "Item", "items").setDisplayType('inline');
			sublist.addField("custpage_itemdesc", "textarea", "Item Description");
			sublist.addField("custpage_vendorpartcode", "text", "Vendor Part Code");
			sublist.addField("custpage_griditemstatus", "text", "Item Status").setDisplayType('inline');
			sublist.addField("custpage_pc", "text", "Pack Code").setDisplayType('inline');
			sublist.addField("custpage_lot", "text", "LOT/Batch #").setDisplayType('inline');
			sublist.addField("custpage_lp", "text", "LP#").setDisplayType('inline');
			sublist.addField("custpage_oldqty", "text", "Old Quantity").setDisplayType('inline');
			sublist.addField("custpage_newqty", "text", "New Quantity").setDisplayType('inline');
			sublist.addField("custpage_itemnewstatus", "text", "New Item Status").setDisplayType('inline');
			sublist.addField("custpage_itemnewloc", "text", "New Bin Location").setDisplayType('inline');			 
			sublist.addField("custpage_itemnewlp", "text", "New LP").setDisplayType('inline');
			sublist.addField("custpage_invadjtype", "text", "Adjustment Type").setDisplayType('inline');	
			sublist.addField("custpage_totqty", "text", "Total Quantity").setDisplayType('hidden');
			sublist.addField("custpage_recid", "text", "RecId").setDisplayType('hidden');
			sublist.addField("custpage_rectype", "text", "RecType").setDisplayType('hidden');
			sublist.addField("custpage_lottext", "text", "LOT").setDisplayType('hidden');
			sublist.addField("custpage_itemid", "text", "itemid").setDisplayType('hidden');
			sublist.addField("custpage_locid", "text", "locid").setDisplayType('hidden');			
			sublist.addField("custpage_oldlp", "text", "LP#").setDisplayType('hidden');
			sublist.addField("custpage_olditemstatus", "text", "ItemStatus").setDisplayType('hidden');
			sublist.addField("custpage_accno", "text", "accountno").setDisplayType('hidden');
			sublist.addField("custpage_siteloc", "text", "siteloc").setDisplayType('hidden');
			sublist.addField("custpage_allocqtyhid", "text", "qtyalloc").setDisplayType('hidden');
			//added fields 
			sublist.addField("custpage_storebinlocation", "text", "Store Bin Location");

			var invtsearchresults=getInventorySearchResults(-1,request);

			if (invtsearchresults != null) 
			{
				var temparraynew=new Array();var tempindex=0;
				for(k=0;k<invtsearchresults.length;k++)
				{
					var invtsearchresult = invtsearchresults[k];

					for(var j=0;j<invtsearchresult.length;j++)
					{
						temparraynew[tempindex]=invtsearchresult[j];
						tempindex=tempindex+1;
					}
				}
				nlapiLogExecution('ERROR', 'temparraynew.length ', temparraynew.length); 
				// paging dropdown  
				var test='';
				if(temparraynew.length>0)
				{
					if(temparraynew.length>25)
					{

						var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('entry');
						pagesize.setDisplaySize(10,10);
						pagesize.setLayoutType('outsidebelow', 'startrow');
						var select= form.addField('custpage_selectpage','select', 'Select Records');	
						select.setLayoutType('outsidebelow', 'startrow');			
						select.setDisplaySize(200,30);

						if (request.getMethod() == 'GET'){

							pagesize.setDefaultValue("25");							
							pagesizevalue=25;
						}
						else
						{
							if(request.getParameter('custpage_pagesize')!=null)
							{pagesizevalue= request.getParameter('custpage_pagesize');}
							else
							{pagesizevalue= 25;pagesize.setDefaultValue("25");}

						}
						form.setScript('customscript_inventoryclientvalidations');
						var len=temparraynew.length/parseFloat(pagesizevalue);
						for(var k=1;k<=Math.ceil(len);k++)
						{

							var from;var to;

							to=parseFloat(k)*parseFloat(pagesizevalue);
							from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

							if(parseFloat(to)>temparraynew.length)
							{
								to=temparraynew.length;
								test=from.toString()+","+to.toString(); 
							}

							var temp=from.toString()+" to "+to.toString();
							var tempto=from.toString()+","+to.toString();
							select.addSelectOption(tempto,temp);
						} 
						if (request.getMethod() == 'POST'){

							if(request.getParameter('custpage_selectpage')!=null ){

								select.setDefaultValue(request.getParameter('custpage_selectpage'));	

							}
							if(request.getParameter('custpage_pagesize')!=null ){

								pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

							}
						}
						else
						{

						}
					}
					else
					{
						pagesizevalue=25;
					}
					var minval=0;var maxval=parseFloat(pagesizevalue);
					if(parseFloat(pagesizevalue)>temparraynew.length)
					{
						maxval=temparraynew.length;
					}
					var selectno=request.getParameter('custpage_selectpage');
					if(selectno!=null )
					{
						var temp= request.getParameter('custpage_selectpage');
						var temparray=temp.split(',');
						nlapiLogExecution('ERROR', 'temparray',temparray.length);

						var diff=parseFloat(temparray[1])-(parseFloat(temparray[0])-1);
						nlapiLogExecution('ERROR', 'diff',diff);

						var pagevalue=request.getParameter('custpage_pagesize');					
						nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
						if(pagevalue!=null)
						{
							if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
							{
								var temparray=selectno.split(',');	
								nlapiLogExecution('ERROR', 'temparray.length ', temparray.length);  
								minval=parseFloat(temparray[0])-1;
								nlapiLogExecution('ERROR', 'temparray[0] ', temparray[0]);  
								maxval=parseFloat(temparray[1]);
								nlapiLogExecution('ERROR', 'temparray[1] ', temparray[1]);  
							}
						}
					}
					nlapiLogExecution('ERROR', 'invtsearchresults.length ', temparraynew.length);
					var index=1;
					nlapiLogExecution('ERROR', 'minval', minval);  

					nlapiLogExecution('ERROR', 'maxval ', maxval); 

					if(maxval > temparraynew.length)
						maxval=temparraynew.length;

					for (var s = minval; s < maxval; s++) 
					{
						try{
						nlapiLogExecution('ERROR', 'count ', s);
						var invtsearchresult = temparraynew[s];

						locationname = invtsearchresult.getText('custrecord_actbeginloc');
						item = invtsearchresult.getValue('custrecord_sku');
						itemname = invtsearchresult.getText('custrecord_sku');
						lp = invtsearchresult.getValue('custrecord_from_lp_no');						 
						lot = invtsearchresult.getValue('custrecord_lotnowithquantity');
						var lotText = invtsearchresult.getText('custrecord_lotnowithquantity');					
						itemstatus = invtsearchresult.getText('custrecord_sku_status');
						packcode = invtsearchresult.getValue('custrecord_packcode');					 
						newitemstatus = invtsearchresult.getText('custrecord_ebiz_new_sku_status');
						newbinloc = invtsearchresult.getText('custrecord_actendloc');
						newqty = invtsearchresult.getValue('custrecord_expe_qty');
						newlp = invtsearchresult.getValue('custrecord_ebiz_new_lp');
						AdjustmentType = invtsearchresult.getText('custrecord_ebiz_ot_adjtype');
						qty = invtsearchresult.getValue('custrecord_act_qty');
						linereportno = invtsearchresult.getValue('custrecord_ebiz_report_no');
						//new fields
						var storbinlocation='';
						var vendornamefield='';
						var itemdesc='';
						storbinlocation=invtsearchresult.getValue('custitemstore_bin_location','custrecord_sku');
						vendornamefield=invtsearchresult.getValue('vendorname','custrecord_sku');
						itemdesc=invtsearchresult.getValue('purchasedescription','custrecord_sku');
//						nlapiLogExecution('ERROR','storbinlocation',storbinlocation);
//						nlapiLogExecution('ERROR','vendornamefield',vendornamefield);
//						nlapiLogExecution('ERROR','itemdesc',itemdesc);

						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_sno',index, (parseFloat(s)+1).toString());
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_invlocation',index, locationname);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemname', index, itemname);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_griditemstatus', index, itemstatus);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_pc', index, packcode);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_lot', index, lot);	
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_lp', index, lp);
						//case# 201410499 not displaying old and new qty correctly
						//form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_oldqty', index, qty);						 
						//form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_newqty', index, newqty);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_oldqty', index, newqty);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_newqty', index, qty);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewstatus', index, newitemstatus);	
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewloc', index, newbinloc);	
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewlp', index, newlp);	
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_invadjtype', index, AdjustmentType);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_reportno', index, linereportno);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_storebinlocation', index, storbinlocation);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_vendorpartcode', index, vendornamefield);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemdesc', index, itemdesc);
						index=index+1;
						}
						catch(exp)
						{
							nlapiLogExecution('ERROR','Exception in Displaying Value',exp);
						}
					}
				}
			}
		}
	}
	else 
	{
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'No Records', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
	}
	form.setScript('customscript_inv_move_report');
	form.addSubmitButton('Display');
	form.addButton('custpage_print','Print','Printreport()');
	response.writePage(form);
}


function Printreport(){

	var fromdate = nlapiGetFieldValue('custpage_fromdate');
	var todate = nlapiGetFieldValue('custpage_todate');
	var lp = nlapiGetFieldValue('custpage_invtlp');	
	var binlocation = nlapiGetFieldValue('custpage_binlocation');
	var item = nlapiGetFieldValue('custpage_item');
	var printreportno = nlapiGetFieldValue('custpage_reportno');	
	var location = nlapiGetFieldValue('custpage_location');
	var company = nlapiGetFieldValue('custpage_company');
	var itemstatus = nlapiGetFieldValue('custpage_itemstatus');


	var InvMovePDFURL = nlapiResolveURL('SUITELET', 'customscript_inv_move_report_pdf', 'customdeploy_inv_move_report_pdf_di');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		InvMovePDFURL = 'https://system.netsuite.com' + InvMovePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			InvMovePDFURL = 'https://system.sandbox.netsuite.com' + InvMovePDFURL;				
		}*/
	nlapiLogExecution('ERROR', 'Wave PDF URL',InvMovePDFURL);					
	InvMovePDFURL = InvMovePDFURL + '&custparam_fromdate='+ fromdate + '&custparam_todate='+ todate + '&custparam_invtlp='+ lp+ '&custparam_binlocation='+ binlocation+ '&custparam_item='+ item+ '&custparam_reportno='+ printreportno+ '&custparam_location='+ location+ '&custparam_company='+ company+ '&custparam_itemstatus='+ itemstatus;	
	window.open(InvMovePDFURL);

}



//case# 20148234 starts (LP is not showing issue)
function FillLP(form,invtlp,maxno)
{
	var filtersinv = new Array();
	filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);
	if(maxno!=-1)
	{
		nlapiLogExecution('ERROR', 'maxno', maxno);
		filtersinv.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[3] = new nlobjSearchColumn('internalid').setSort(true);
	var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);
	nlapiLogExecution('ERROR', 'searchInventory', searchInventory);
	for (var i = 0; i < searchInventory.length; i++) 
	{
		/*var res = form.getField('custpage_invtlp').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_lp'), 'is');
		if (res != null) 
		{
			if (res.length > 0) 
				continue;                    
		}*/
		invtlp.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_lp'), searchInventory[i].getValue('custrecord_ebiz_inv_lp'));
	}
	if(searchInventory!=null && searchInventory.length>=1000)
	{
		var maxno=searchInventory[searchInventory.length-1].getValue('internalid');	
		nlapiLogExecution('ERROR', 'maxno', maxno);
		FillLP(form, invtlp,maxno);	
	}

}
//case# 20148234 end