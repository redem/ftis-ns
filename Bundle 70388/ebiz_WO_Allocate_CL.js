/***************************************************************************
        eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/Attic/ebiz_WO_Allocate_CL.js,v $
 *     	   $Revision: 1.1.2.2.8.1 $
 *     	   $Date: 2015/09/21 14:02:31 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_8 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY


 *****************************************************************************/


/**
 * @author LN
 *@version
 *@date
 *@Description: This is a Client Script acting as a Library function to Work Order Screen
 level LP validation
 */

var eventtype; 
function  myPageInit(type){

	eventtype=type;
	alert("type" + eventtype);
} 
function AllocateQuantity()
{


	nlapiLogExecution('ERROR', 'type', eventtype);
	alert("eventtype" + eventtype);
	var itemcount=nlapiGetLineItemCount('item');
	var assemItem= nlapiGetFieldValue('assemblyitem');
	var vWOId= nlapiGetFieldValue('tranid');
	alert("ItemCount " + itemcount);
	alert("AssembItem " + assemItem);
	if(itemcount>0)
	{
		// Create a Record in Opentask
		var now = new Date();
		//a Date object to be used for a random value
		var now = new Date();
		//now= now.getHours();
		//Getting time in hh:mm tt format.
		var a_p = "";
		var d = new Date();
		var curr_hour = now.getHours();
		if (curr_hour < 12) {
			a_p = "am";
		}
		else {
			a_p = "pm";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}

		var curr_min = now.getMinutes();

		curr_min = curr_min + "";

		if (curr_min.length == 1) {
			curr_min = "0" + curr_min;
		}
		var eBizWaveNo = GetMaxTransactionNo('WAVE');
		for(var s=1;s<=itemcount;s++)
		{

			var varsku = nlapiGetLineItemText('item', 'item', s);	 
			var VRec = nlapiGetLineItemValue('item', 'custcol_ebizwoinventoryref', s);
			var varQty = nlapiGetLineItemValue('item', 'custcol_ebizwoavalqty', s);
			var LineLocation=nlapiGetLineItemValue('item','custcol_ebizwobinloc',s);
			alert("SKU " + varsku);
			alert("varRecid " + VRec);

			var vRecArr=new Array();
			if(VRec!= null && VRec != "")
				vRecArr=VRec.split(',');
			var vQtyArr=new Array();
			if(varQty!= null && varQty != "")
				vQtyArr=varQty.split(',');
			var vRecArrBin=new Array();
			if(LineLocation!= null && LineLocation != "")
				vRecArrBin=LineLocation.split(',');
			for(var k=0;k<vRecArr.length;k++)
			{



				var vLineQty=vQtyArr[k];
				var vLineRec=vRecArr[k];
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vLineRec);
				var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
				var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				nlapiLogExecution('ERROR', 'qty', qty);
				nlapiLogExecution('ERROR', 'allocqty', allocqty);
				nlapiLogExecution('ERROR', 'varqty', vLineQty);
				transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)+ parseFloat(vLineQty)).toFixed(5));



				var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
				customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP 
				//customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
				customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
				customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));

				//customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
				//customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
				//customrecord.setFieldValue('custrecord_expe_qty', varqty); 
				nlapiLogExecution('ERROR','expe qty ',vLineQty);		
				customrecord.setFieldValue('custrecord_expe_qty', parseFloat(vLineQty).toFixed(5));  //Commented and added to resovle decimal issue
				customrecord.setFieldValue('custrecord_act_qty', parseFloat(vLineQty).toFixed(5));
				customrecord.setFieldValue('custrecord_wms_status_flag', 9);	// 14 stands for outbound process
				customrecord.setFieldValue('custrecord_actbeginloc', vRecArrBin[k]);	
				//customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
				//customrecord.setFieldValue('custrecord_lpno', vlp); 
				customrecord.setFieldValue('custrecord_upd_date', now);
				//customrecord.setFieldValue('custrecord_sku', vskuText);
				customrecord.setFieldValue('custrecord_ebiz_sku_no', varsku);
				customrecord.setFieldValue('name', vitem+1);		
				var currentContext = nlapiGetContext();  
				var currentUserID = currentContext.getUser();
				customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
				customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
				customrecord.setFieldValue('custrecord_ebiz_wave_no', eBizWaveNo);
				customrecord.setFieldValue('custrecord_ebiz_order_no', vWOId);



				nlapiSubmitRecord(customrecord);
			}
		}

	} 
	return false;
}
