/***************************************************************************
���������������������������eBizNET
�����������������������eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_GenerateLocation_CL.js,v $
 *� $Revision: 1.2.6.1.4.1.6.1 $
 *� $Date: 2014/07/30 15:29:19 $
 *� $Author: grao $
 *� $Name: t_eBN_2014_2_StdBundle_0_9 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_GenerateLocation_CL.js,v $
 *� Revision 1.2.6.1.4.1.6.1  2014/07/30 15:29:19  grao
 *� Case#: 20148771 �Standard bundel  issue fixes
 *�
 *� Revision 1.2.6.1.4.1  2013/05/21 14:40:28  grao
 *� PO Closed related Validation
 *� Case#:20122721 issues fixes in PMM Prod account.
 *�
 *� Revision 1.2.6.1  2012/11/01 14:55:22  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.2  2011/12/28 14:04:46  schepuri
 *� CASE201112/CR201113/LOG201121
 *� added new fields from itemstatus,to itemstatus
 *�
 *
 ****************************************************************************/
//function Set()
function changePageValues()
{
	var FromStatus= nlapiGetFieldValue('custpage_fromstatus');
	var ToStatus= nlapiGetFieldValue('custpage_tostatus');
	var lineCnt = nlapiGetLineItemCount('custpage_items');
	
	if(parseFloat(lineCnt) > 0)
	{
		var userselection = false;	
		var count=0;
		for (var s = 1; s <= lineCnt; s++)
		{
			var confirmFlag= nlapiGetLineItemValue('custpage_items', 'custpage_chk', s);
			var ItemStatus= nlapiGetLineItemValue('custpage_items', 'custpage_itemstatus', s);

			if (confirmFlag == 'T') 
			{
				userselection = true;
				if(ToStatus != "" && ToStatus != null && FromStatus != "" && FromStatus != null)
				{
					if(FromStatus == ItemStatus)
					{
						//nlapiSetLineItemValue(type, fldnam, linenum, value)
						nlapiSetLineItemValue('custpage_items', 'custpage_itemstatus', s ,ToStatus);
						count=count+1;
						var ItemStatus1= nlapiGetLineItemValue('custpage_items', 'custpage_itemstatus', s);
						//alert('ItemStatus1 ' +ItemStatus1);
					}
				}
			}

		}

		//alert('count ' +count);

		if(userselection == false)
		{
			alert('Please select atleast one row');
			return false;
		}
		if(FromStatus == "" || FromStatus == null )
		{
			alert('please select From Item Status');
			return false;
		}
		if(ToStatus == "" || ToStatus == null )
		{
			alert('please select To Item Status');
			return false;
		}
		if(count == 0)
		{
			alert('Atleast one seleted Item Status should match with From Item Status');
			return false;
		}
		return true;
	}
	else if(parseFloat(lineCnt)==0)
	{
		return false;
	}
	return true;


}


function Display()
{
	//alert('Hi');
	nlapiSetFieldValue('custpage_tempflag','Confirm');
	NLDoMainFormButtonAction("submitter",true);
}

function onSave()
{
	var POlinecount = nlapiGetLineItemCount('custpage_items');	
	var BinFlag="F";
	for (var p = 1; p <=POlinecount; p++) 
	{
		var chkbox  = nlapiGetLineItemValue('custpage_items', 'custpage_chk', p);
		var lineno  = nlapiGetLineItemValue('custpage_items', 'custpage_line', p);
		var getPOid  = nlapiGetLineItemValue('custpage_items', 'custpage_poid', p);
		var vsku  = nlapiGetLineItemValue('custpage_items', 'custpage_qcitemid', p);
		
	//	alert("chkbox." + chkbox);
	//	alert("lineno." + lineno);
	//	alert("poid." + getPOid);
	//	alert("vsku." + vsku);
		
		if (chkbox  == 'T') 
		{
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getPOid));
			filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', vsku));
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]));
			filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'equalto', lineno));
			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]));


			var columns = new Array();
			columns.push(new nlobjSearchColumn('internalid'));			

			// execute the  search, passing null filter and return columns
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

			if(searchresults!=null&&searchresults!="")
			{
				BinFlag="T";
				//alert("BinFlag." + BinFlag);
			}
		}
	}
	if(BinFlag=="T")
	{
		alert('Locations Already Generated.');
		return false;
	}
}