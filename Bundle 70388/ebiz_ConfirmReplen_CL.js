/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/Attic/ebiz_ConfirmReplen_CL.js,v $
 *     	   $Revision: 1.1.2.2.2.10.2.3 $
 *     	   $Date: 2015/11/16 17:12:35 $
 *     	   $Author: deepshikha $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_ConfirmReplen_CL.js,v $
 * Revision 1.1.2.2.2.10.2.3  2015/11/16 17:12:35  deepshikha
 * 2015.2 issues
 * 201415668
 *
 * Revision 1.1.2.2.2.10.2.2  2015/11/14 12:46:20  snimmakayala
 * 201415659
 *
 * Revision 1.1.2.2.2.10.2.1  2015/11/10 15:23:42  schepuri
 * case# 201413692
 *
 * Revision 1.1.2.2.2.10  2015/07/23 13:40:15  schepuri
 * case#201413595
 *
 * Revision 1.1.2.2.2.9  2015/06/22 15:44:28  grao
 * SW issue fixes  201412905
 *
 * Revision 1.1.2.2.2.8  2014/08/04 15:54:37  skavuri
 * Case# 20148543 SB Issue Fixed
 *
 * Revision 1.1.2.2.2.7  2014/06/18 14:55:28  rmukkera
 * Case # 20147983
 *
 * Revision 1.1.2.2.2.6  2014/04/16 06:43:16  skreddy
 * case # 20147970
 * Sonic sb  issue fix
 *
 * Revision 1.1.2.2.2.5  2014/03/27 13:56:57  rmukkera
 * Case # 20127749
 *
 * Revision 1.1.2.2.2.4  2014/03/26 14:38:52  rmukkera
 * Case # 20127749
 *
 * Revision 1.1.2.2.2.3  2014/03/19 15:49:54  skavuri
 * Case # 20127749 issue fixed
 *
 * Revision 1.1.2.2.2.2  2014/03/14 14:28:43  sponnaganti
 * case# 20127698
 * (Remaining cube issue)
 *
 * Revision 1.1.2.2.2.1  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.1.2.2  2013/02/06 01:01:09  kavitha
 * CASE201112/CR201113/LOG201121
 * Serial # functionality - Inventory process
 *
 * Revision 1.1.2.1  2012/12/11 03:47:12  kavitha
 * CASE201112/CR201113/LOG201121
 * Incorporated Serial # functionality
 *
 *
 *****************************************************************************/

function onSave()
{
	var lineCnt = nlapiGetLineItemCount('custpage_replen_items');
	var i=0;
	var itmid = "";
	var qty = "";
	var LP = "";
	var ItemType = "";
	var itemname = "";
	var serialInflg="F";
	var arrLP = new Array();
	var totalqty = 0;
	var AtleastonelinetoSelected="F";
	for (var s = 1; s <= lineCnt; s++)
	{
		var confirmFlag= nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_sel', s);
		if(confirmFlag== "T")
		{
			AtleastonelinetoSelected="T";
			itmid = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_item',s);
			qty = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_rplnqty',s);
			LP = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_fromlp',s);
			//case # 20147983? start
			//LP = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_lp',s);

			ItemType = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_itemtype',s);
			//case # 20147983?(System is not asking for serial numbers while confirming a  serial item replenish task.) end
			//case # 20127749 starts
			var tempqty = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_expqty',s);

			if(qty==null || qty=='' || isNaN(qty) || qty<0)
			{
				alert('Please Enter Quantity/Valid Quantity for Line '+s);
				return false;
			}

			if(parseFloat(qty)> parseFloat(tempqty))
			{
				alert('More Than Replenishment qty is not allowed');
				return false;
			}


			//Case# 20127749 ends

			//case# 20127698 starts (Remaining cube issue)
			var location = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_endloc', s);
			var arrDims = getSKUCubeAndWeight(itmid, 1);
			var itemCube = 0;
			var vTotalCubeValue = 0;
			//alert('itmid '+itmid);
			//alert('qty '+qty);
			//alert('location '+location);
			if((arrDims[0] != "") && (!isNaN(arrDims[0]))) 
			{
				var uomqty = ((parseFloat(qty))/(parseFloat(arrDims[1])));			
				itemCube = ((parseFloat(uomqty)) * (parseFloat(arrDims[0])));
			}
			var vOldRemainingCube = GeteLocCube(location);
			vTotalCubeValue = parseFloat(vOldRemainingCube)-parseFloat(itemCube);
			if(vTotalCubeValue<0)
			{
				alert('Insufficient remaining cube');
				return false;
			}

			//case# 20127698 end
			if(itmid !=null && itmid !='')
			{
				//Case# 20148543 starts
				//var fields = ['recordType', 'custitem_ebizserialin','name'];
				//	var columns = nlapiLookupField('item', itmid, fields);
				//ItemType = columns.recordType;
				//itemname = columns.name;
				nlapiLogExecution('ERROR', 'itmid', itmid);
				var filter=new Array();
				filter[0]=new nlobjSearchFilter("internalid",null,"anyof",itmid);
				var column=new Array();
				column[0]=new nlobjSearchColumn("type");
				column[1]=new nlobjSearchColumn("custitem_ebizserialin");
				column[2]=new nlobjSearchColumn("name");
				var searchres=nlapiSearchRecord("item",null,filter,column);
				if(searchres!=null&&searchres!="")
				{
					//alert("length" + searchres.length);
					//ItemType=searchres[0].recordType;
					//alert("ItemType123 " + searchres[0].recordType);
					itemname=searchres[0].getValue("name");
					serialInflg=searchres[0].getValue("custitem_ebizserialin");
				}
				//Case# 20148543 ends
			}
			//var serialInflg="F";
			//serialInflg = columns.custitem_ebizserialin;
			var Errorflag = "N";

			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
				i++;

				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', itmid);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'A');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					i = 0;
					Errorflag = "Y";
				}

				totalqty = parseInt(totalqty) + parseInt(qty);
				arrLP.push(LP);
			}			
		}		
	}
	if(AtleastonelinetoSelected=="F")
	{
		alert("Please Select atleast one value(s)");
		return false;
	}


	if((i > 0) && (Errorflag == "N"))
	{
		if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
		{			
			LP = "";
			arrLP = removeDuplicateElement(arrLP);
			nlapiLogExecution("ERROR", "arrLP ", arrLP);
			alert("Please select required Serial numbers to move");
			var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumber_adjustment', 'customdeploy_serialnumber_adjustment_di');
			/*var ctx = nlapiGetContext();
			if (ctx.getEnvironment() == 'PRODUCTION') {
				linkURL = 'https://system.na1.netsuite.com' + linkURL;			
			}
			else 
				if (ctx.getEnvironment() == 'SANDBOX') {
					linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
				}*/
			//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_replen_items','custpage_polocationid',p);
			//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_replen_items','custpage_pocompanyid',p);
			linkURL = linkURL + '&custparam_serialskuid=' + itmid;
			linkURL = linkURL + '&custparam_serialsku=' + itemname;
			linkURL = linkURL + '&custparam_serialskulp=' + LP;
			linkURL = linkURL + '&custparam_serialskuqty=' + totalqty;	
			linkURL = linkURL + '&custparam_serialskuarrlp=' + arrLP;
			//linkURL = linkURL + '&custparam_lot=' + BatchNo;
			//linkURL = linkURL + '&custparam_name=' + BatchNo;
			//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_replen_items','custpage_poitemstatus',p);
			//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_replen_items','custpage_popackcode',p);
			window.open(linkURL);
			return false;
		}	
	}
	if (i == 0) {
		return true;
	}
	return true;
}

function removeDuplicateElement(arrayName){
	var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < newArray.length; j++) {
			if (newArray[j] == arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}

function onChange(type, name)
{
	if(name=='custpage_replen_sel')
	{
		/*var confirmFlag= nlapiGetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_sel');
		if(confirmFlag== "T")
		{
			var itmid = nlapiGetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_item');
			var qty = nlapiGetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_expqty');
			//var newqty = nlapiGetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_rplnqty');
			var LP = nlapiGetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_fromlp');

			var fields = ['recordType', 'custitem_ebizserialin','name'];
			var columns = nlapiLookupField('item', itmid, fields);
			var ItemType = columns.recordType;
			var itemname = columns.name;

			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;	

			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
				//if(parseFloat(newqty) != 0 && parseFloat(newqty)<parseFloat(oldqty))
				//{
					alert("Please select required Serial numbers to move");
					var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumber_adjustment', 'customdeploy_serialnumber_adjustment_di');
					var ctx = nlapiGetContext();
					if (ctx.getEnvironment() == 'PRODUCTION') {
						linkURL = 'https://system.na1.netsuite.com' + linkURL;			
					}
					else 
						if (ctx.getEnvironment() == 'SANDBOX') {
							linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
						}
					//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_replen_items','custpage_polocationid',p);
					//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_replen_items','custpage_pocompanyid',p);
					linkURL = linkURL + '&custparam_serialskuid=' + itmid;
					linkURL = linkURL + '&custparam_serialsku=' + itemname;
					linkURL = linkURL + '&custparam_serialskulp=' + LP;
					linkURL = linkURL + '&custparam_serialskuqty=' + qty;	
					//linkURL = linkURL + '&custparam_lot=' + BatchNo;
					//linkURL = linkURL + '&custparam_name=' + BatchNo;
					//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_replen_items','custpage_poitemstatus',p);
					//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_replen_items','custpage_popackcode',p);
					window.open(linkURL);	
				//}
			}
		}*/		
	}
	//case# 20127698 starts (Remaining cube issue)
	//alert(name);
	if(name=="custpage_replen_sel")
	{
		var lineCnt = nlapiGetLineItemCount('custpage_replen_items');
		var itmid = "";
		var qty = "";
		var LP = "";
		var ItemType = "";
		var itemname = "";
		var arrLP = new Array();
		var totalqty = 0;

		for (var s = 1; s <= lineCnt; s++)
		{
			var confirmFlag= nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_sel', s);
			if(confirmFlag== "T")
			{
				itmid = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_item',s);
				qty = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_rplnqty',s);
				LP = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_fromlp',s);

				var location = nlapiGetLineItemValue('custpage_replen_items', 'custpage_replen_endloc', s);
				var arrDims = getSKUCubeAndWeight(itmid, 1);
				var itemCube = 0;
				var vTotalCubeValue = 0;
				//alert('itmid '+itmid);
				//alert('qty '+qty);
				//alert('location '+location);
				if((arrDims[0] != "") && (!isNaN(arrDims[0]))) 
				{
					var uomqty = ((parseFloat(qty))/(parseFloat(arrDims[1])));			
					itemCube = ((parseFloat(uomqty)) * (parseFloat(arrDims[0])));
				}
				var vOldRemainingCube = GeteLocCube(location);
				vTotalCubeValue = parseFloat(vOldRemainingCube)-parseFloat(itemCube);
				if(vTotalCubeValue<0)
				{
					alert('Insufficient remaining cube');
					return false;
				}


			}
		}
	}
	if(name=="custpage_replen_lp")
	{
		var invtLpno='',lp='',whLocation='';


		var confirmFlag= nlapiGetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_sel', s);
		if(confirmFlag== "T")
		{

			invtLpno = nlapiGetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_oldlp',s);
			lp = nlapiGetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_lp', s);
			whLocation = nlapiGetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_whLocation', s);
			if (lp!=invtLpno)
			{
				/*var vgetItemLP=lp.toUpperCase();

				var LPReturnValue = ebiznet_LPRange_CL(lp, '2',whLocation,1,vgetItemLP);

				if (LPReturnValue == true)
				{
					invtLpno=lp;
				} */
				var lpExists='N';
				var filtersmlp = new Array();
				filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', lp);

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

				if (SrchRecord != null && SrchRecord.length > 0) {
					nlapiLogExecution('ERROR', 'LP FOUND');

					lpExists = 'Y';
				}

				if(lpExists =='N')
				{	
					var LPReturnValue = ebiznet_LPRange_CL_withLPType(lp.replace(/\s+$/,""), '2','2',whLocation);//'2'UserDefiend,'2'PICK
					
					if(LPReturnValue==false)
					{	
						alert('Invalid LP Range');
						nlapiSetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_sel','F',false);
						nlapiSetCurrentLineItemValue('custpage_replen_items', 'custpage_replen_lp',invtLpno,false);
						return false;
					}
					
				}
				else
				{
					alert('LP Already Exist');
					return false;
				}

			}
		}


	}
}

