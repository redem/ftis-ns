/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_Putaway.js,v $
 *     	   $Revision: 1.1.2.3.4.16.2.3 $
 *     	   $Date: 2015/11/20 14:52:08 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_Putaway.js,v $
 * Revision 1.1.2.3.4.16.2.3  2015/11/20 14:52:08  skreddy
 * case 201415773
 * 2015.2 issue fix
 *
 * Revision 1.1.2.3.4.16.2.2  2015/11/09 23:27:20  nneelam
 * case# 201415508
 *
 * Revision 1.1.2.3.4.16.2.1  2015/11/06 15:50:09  skreddy
 * case 201415419
 * 2015.2  issue fix
 *
 * Revision 1.1.2.3.4.16  2015/05/20 15:47:24  nneelam
 * case# 201412642
 *
 * Revision 1.1.2.3.4.15  2015/05/12 19:40:49  nneelam
 * case# 201412749
 *
 * Revision 1.1.2.3.4.14  2015/05/11 15:41:10  skreddy
 * Case# 201412642 ,201412628
 * Hilmot  SB issue fix
 *
 * Revision 1.1.2.3.4.13  2014/08/04 09:09:30  nneelam
 * case#  20149371
 * Stanadard Bundle Partial WO Issue Fix.
 *
 * Revision 1.1.2.3.4.12  2014/07/09 12:51:57  rmukkera
 * Case # 20149145
 * jawbone issues applicable to standard also
 *
 * Revision 1.1.2.3.4.11  2014/06/13 08:49:21  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.3.4.10  2014/06/05 14:01:07  rmukkera
 * Case # 20148744
 *
 * Revision 1.1.2.3.4.9  2014/06/03 13:58:51  skreddy
 * case # 20148716
 * PCT prod issue fix
 *
 * Revision 1.1.2.3.4.8  2014/05/30 00:34:24  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3.4.7  2014/02/13 15:09:51  sponnaganti
 * case# 20127056
 * For Keyboard Enter button to work
 *
 * Revision 1.1.2.3.4.6  2014/02/07 15:22:23  skavuri
 * Case # 20127082 Fixed
 *
 * Revision 1.1.2.3.4.5  2014/02/05 15:08:03  sponnaganti
 * case# 20127056
 * For Keyboard Enter button to work
 *
 * Revision 1.1.2.3.4.4  2013/12/20 14:32:29  skreddy
 * case#20126506
 * Pct prod issue fix
 *
 * Revision 1.1.2.3.4.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.3.4.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.3.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.3  2012/12/07 14:28:18  skreddy
 * CASE201112/CR201113/LOG201121
 * Restrict to Approve the WO before confirm
 *
 * Revision 1.1.2.2  2012/11/28 15:08:18  schepuri
 * CASE201112/CR201113/LOG201121
 * throwing error msg plz confirm open pick task issue resolved
 *
 * Revision 1.1.2.1  2012/11/23 09:11:39  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 * Revision 1.32.4.29.4.5  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 *
 *
 *****************************************************************************/
/**
 * @param request
 * @param response
 */
function WOPutaway(request, response){
	if (request.getMethod() == 'GET') {
		nlapiLogExecution('ERROR','inGet');
		//case# 20127056 starts (Now form name is passed correctly in function to work keyboard enter button)
		var functionkeyHtml=getFunctionkeyScript('_ebiz_rf_wo_putaway');
		//case# 20127056
		var html = "<html><head><title>WORKORDER MENU</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterwonumber').focus();";        		 
		html = html + "</script>";       
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_ebiz_rf_wo_putaway' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> WORK ORDER NO";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterwonumber' id='enterwonumber' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterwonumber').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response','Into Response');

		var vWOnumber = request.getParameter('enterwonumber');
		var optedEvent = request.getParameter('cmdPrevious');
		var confirmflag = 'false';


		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di', false, optedEvent);
		}
		else
		{

			var vWOId="";
			var WOFilters = new Array();
			var Columns = new Array();
			var WOarray=new Array();

			WOarray["custparam_error"] = 'INVALID WORK ORDER #';
			WOarray["custparam_screenno"] = 'WOPutawayGen';
			if(vWOnumber != null && vWOnumber !="")
			{
				WOFilters.push(new nlobjSearchFilter('tranid', null, 'is', vWOnumber));
			}
			WOFilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

			var SearchResults = nlapiSearchRecord('workorder', null, WOFilters, Columns);
			if(SearchResults != null && SearchResults != '')
			{
				vWOId=SearchResults[0].getId();
			}	
			else
			{

				nlapiLogExecution('ERROR', 'Error: ', 'Work order Order not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
				return;
			}
			var KitItemCnt='';
			if(vWOId !=null && vWOId!='')
			{
				nlapiLogExecution('ERROR', 'vWOId',vWOId);
				var searchresults = nlapiLoadRecord('workorder', vWOId); //load Workorder
				var WOStatus=searchresults.getFieldValue('status');
				var Qty = searchresults.getFieldValue('quantity'); 	
				var ApproveFlag=searchresults.getFieldValue('custbody_ebiz_wo_qc_status');
				if(WOStatus =='Built') //check workorder is built or not
				{
					nlapiLogExecution('ERROR', 'Error: ', 'Work order Order Status Build',WOStatus);
					WOarray["custparam_error"] = "ALREADY BUILT CONFIRM FOR THIS WORK ORDER";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					return;
				}
				/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle ***/

				else if(ApproveFlag!='2') // check workorder is approved or not
				{ /*** upt o here ***/
					nlapiLogExecution('ERROR', 'Error: ', 'Work order Order Status Build',ApproveFlag);
					WOarray["custparam_error"] = "PLEASE APPROVE THIS WORK ORDER";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					return;
				}
				else
				{
					KitItemCnt = CheckKitItem(vWOId,Qty);
				}
			}
			//nlapiLogExecution('ERROR', 'vWOId',vWOId);
			//var KitItemCnt = CheckKitItem(vWOId);
			nlapiLogExecution('ERROR', 'KitItemCnt',KitItemCnt);

			if(KitItemCnt == 0)
			{
				var OpentaskItemArray = new Array;
				var filterOpentask = new Array();
				filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','14']));	//	Status - Picks Generated
				filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

				if(vWOId != null && vWOId != "")
				{
					filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
				}


				var WOcolumns = new Array();
				/*WOcolumns[0] = new nlobjSearchColumn('custrecord_line_no');
				WOcolumns[1] = new nlobjSearchColumn('custrecord_sku');
				WOcolumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
				WOcolumns[3] = new nlobjSearchColumn('custrecord_lpno');
				WOcolumns[4] = new nlobjSearchColumn('custrecord_batch_no');
				WOcolumns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
				WOcolumns[6] = new nlobjSearchColumn('custrecord_act_qty');   
				WOcolumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				WOcolumns[8] = new nlobjSearchColumn('custrecord_invref_no');
				WOcolumns[9] = new nlobjSearchColumn('custrecord_wms_location');
				WOcolumns[10] = new nlobjSearchColumn('custrecord_comp_id');
				//WOcolumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');				
				WOcolumns[11] = new nlobjSearchColumn('custrecord_ebiz_sku_no');	
				WOcolumns[12] = new nlobjSearchColumn('custrecord_sku_status');	
				WOcolumns[0].setSort();*/

				WOcolumns.push(new nlobjSearchColumn('custrecord_line_no',null,'group'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_sku',null,'group'));
				//WOcolumns.push(new nlobjSearchColumn('custrecord_expe_qty',null,'sum'));
				//WOcolumns.push(new nlobjSearchColumn('custrecord_lpno',null,'group'));
				//WOcolumns.push(new nlobjSearchColumn('custrecord_batch_no',null,'group'));
				//WOcolumns.push(new nlobjSearchColumn('custrecord_actbeginloc',null,'group'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_act_qty',null,'sum'));   
				//WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no',null,'group'));
				//WOcolumns.push(new nlobjSearchColumn('custrecord_invref_no',null,'group'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_wms_location',null,'group'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_comp_id',null,'group'));
				//WOcolumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group');				
				WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group'));	
				WOcolumns.push(new nlobjSearchColumn('custrecord_sku_status',null,'group'));	
				WOcolumns[0].setSort();
				var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);	

				//case 20126506 start : checking for open picks
				 var OpenpicksResults = CheckForOpenPicks(vWOId);
				//case 20126506 end
				if(WOSearchResults!=null && WOSearchResults!='' && WOSearchResults.length>0 && (OpenpicksResults ==null || OpenpicksResults ==''))
				{
					// Create a Record in Opentask
					var now = new Date();
					//a Date object to be used for a random value
					var now = new Date();
					//now= now.getHours();
					//Getting time in hh:mm tt format.
					var a_p = "";
					var d = new Date();
					var curr_hour = now.getHours();
					if (curr_hour < 12) {
						a_p = "am";
					}
					else {
						a_p = "pm";
					}
					if (curr_hour == 0) {
						curr_hour = 12;
					}
					if (curr_hour > 12) {
						curr_hour = curr_hour - 12;
					}

					var curr_min = now.getMinutes();

					curr_min = curr_min + "";

					if (curr_min.length == 1) {
						curr_min = "0" + curr_min;
					}



					for(var i=0; i<WOSearchResults.length ;i++)
					{
						var vlineno = WOSearchResults[i].getValue('custrecord_line_no',null,'group');
						if(OpentaskItemArray.indexOf(vlineno)==-1)
						{
							OpentaskItemArray.push(vlineno);
						}
					}

					nlapiLogExecution('ERROR', 'OpentaskItemArray',OpentaskItemArray.length);
					var OpenTskLineCount = OpentaskItemArray.length;

					var searchresults = nlapiLoadRecord('workorder', vWOId); //load Workorder
					var vLineCount  = searchresults.getLineItemCount('item');   //get itemcount

					nlapiLogExecution('ERROR', 'vLineCount',vLineCount);
					nlapiLogExecution('ERROR', 'WOSearchResults.length',WOSearchResults.length);

					/*var count =0;
					for(var k=1;k<vLineCount;k++)
					{
						var CompItemType = searchresults.getLineItemValue('item', 'itemtype', k);
						if(CompItemType != 'Service' && CompItemType != 'NonInvtPart')
						{
							count=count+1;
						}

					}*/

					//below code is commented beacuse we need to allow process for partial build assembly.
					/*for(var u=1;u<=vLineCount && confirmflag=='false';u++)
					{
						var LineNo =searchresults.getLineItemValue('item','line',u);
						var lineItem = searchresults.getLineItemText('item', 'item', u);
						var lineItemvalue = searchresults.getLineItemValue('item', 'item', u);
						var actqty = searchresults.getLineItemValue('item', 'quantity', u);						
						for(var v=0; v<WOSearchResults.length && confirmflag=='false';v++)
						{

							var LineNoOT =WOSearchResults[v].getValue('custrecord_line_no',null,'group');
							var lineItemOT = WOSearchResults[v].getText('custrecord_sku',null,'group');
							var lineItemvalueOT = WOSearchResults[v].getValue('custrecord_sku',null,'group');
							var actqtyOT = WOSearchResults[v].getValue('custrecord_act_qty',null,'sum');

							var countnew = 0;

							var str = 'LineNo = ' + LineNo + '<br>';
							str = str + 'lineItemvalue = ' + lineItemvalue + '<br>';
							str = str + 'actqty = ' + actqty + '<br>';
							str = str + 'LineNoOT= ' + LineNoOT +'<br>';
							str = str + 'lineItemvalueOT = ' + lineItemvalueOT+ '<br>';
							str = str + 'actqtyOT = ' + actqtyOT+'<br>';
							nlapiLogExecution('ERROR', 'str', str);						

							if(LineNo == LineNoOT && lineItemvalue == lineItemvalueOT)// && actqty == actqtyOT)
							{
								nlapiLogExecution('ERROR', 'str', 'if');
								if(parseInt(actqtyOT) < parseInt(actqty))
								{
									nlapiLogExecution('ERROR', 'in to if actqtyOT',actqtyOT);
									confirmflag = 'true';
									//continue;
									break;
								}
								//Case # 20127082 starts
								//countnew = countnew+1;
								//Case # 20127082 ends
							}
						}

						if(confirmflag == 'true')
						{
							break;
						}

					}
					nlapiLogExecution('ERROR', 'confirmflag',confirmflag);
					if(confirmflag == 'true')
					{
						WOarray["custparam_error"] = "FIRST CONFIRM THE COMPONENT ITEM PICKS";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
						return;
					}
					else*/
					{ 
						var Qty = searchresults.getFieldValue('quantity');   
						
						var filters = new Array();
						filters[0] = new nlobjSearchFilter('internalid', null, 'is', vWOId);
						filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');//ship task


						var columns = new Array();
						columns[0] = new nlobjSearchColumn('quantityshiprecv');
						
						var searchresultsWaveNo = nlapiSearchRecord('workorder', null, filters, columns);
						if(searchresultsWaveNo!=null && searchresultsWaveNo!=''){
							built = searchresultsWaveNo[0].getValue('quantityshiprecv');
						}
						nlapiLogExecution('ERROR','built',built);
						if(built==null || built =="")
							built=0;
						Qty = parseFloat(Qty) - parseFloat(built);
						
						var Itemid=searchresults.getFieldValue('assemblyitem');
						var ItemText=searchresults.getFieldText('assemblyitem');
						var WoCreatedDate=searchresults.getFieldValue('trandate');
						var WoDueDate=searchresults.getFieldValue('custbody_ebiz_woduedate');
						var WoExpcompletionDate=searchresults.getFieldValue('custbody_ebiz_expcompletiondate');
						var ItemQty=searchresults.getFieldValue('quantity');
						var Status=searchresults.getFieldValue('status');
						var ItemLocation=searchresults.getFieldValue('location');
						var WOName=searchresults.getFieldValue('tranid');
						var ItemPackcode=searchresults.getFieldValue('custrecord_ebizpackcodeskudim');

						/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle***/
						var Lotno = searchresults.getFieldValue('custbody_ebiz_lotnumber');

						var ExpiryDate = searchresults.getFieldValue('custbody_ebiz_expirydate');
						/*** up to here ***/


						nlapiLogExecution('ERROR', 'Itemid',Itemid);
						nlapiLogExecution('ERROR', 'WOName',WOName);
						nlapiLogExecution('ERROR', 'ItemText',ItemText);
						nlapiLogExecution('ERROR', 'Qty',Qty);
						nlapiLogExecution('ERROR', 'ItemLocation',ItemLocation);
						nlapiLogExecution('ERROR', 'ItemPackcode',ItemPackcode);

						WOarray["custparam_woid"] =vWOId; 
						WOarray["custparam_fetcheditemid"] =Itemid ;
						WOarray["custparam_item"] = ItemText ;
						WOarray["custparam_expectedquantity"] = Qty;
						WOarray["custparam_whlocation"] = ItemLocation;
						WOarray["custparam_noofrecords"] = vLineCount;
						WOarray["custparam_woname"] = WOName;
						WOarray["custparam_actualbegindate"] = WoCreatedDate;
						WOarray["custparam_wostatus"] = Status;
						WOarray["custparam_packcode"] = ItemPackcode;
						WOarray["custparam_screenno"] = 'WOPutawayGen';
						WOarray["custparam_actualbegintime"] =((curr_hour) + ":" + (curr_min)) ;
						WOarray["custparam_actualbegintimeampm"] = a_p;
						WOarray["custparam_batchno"] = Lotno;
						/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle***/
						WOarray["custparam_batchexpirydate"] = ExpiryDate;
						/*** up to here ***/	

						nlapiLogExecution('ERROR', 'a_p',a_p);

						var getActualBeginTime = request.getParameter('custparam_actualbegintime');
						var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

						//below code is commented beacuse system missing quantity screen for Lot number items.

					/*	//check Assembly Item is LOT# Item or not
						var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
						var columns = nlapiLookupField('item', Itemid, fields);

						var serialInflg="F";
						var batchflag="F";
						var itemSubtype;
						if(columns != null && columns!='')
						{
							itemSubtype = columns.recordType;	
							serialInflg = columns.custitem_ebizserialin;
							batchflag= columns.custitem_ebizbatchlot;

						}

						if (itemSubtype == "lotnumberedinventoryitem" || itemSubtype=="lotnumberedassemblyitem" || batchflag=="T" )
						{
							WOarray["custparam_itemtype"] = "lotnumbereditem" ;
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_batchno', 'customdeploy_ebiz_rf_wo_putaway_batch_di', false, WOarray);
						}
						else*/
					//	{
							WOarray["custparam_itemtype"] = '' ;
							WOarray["custparam_error"] = "";
							//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_loc', 'customdeploy_ebiz_rf_wo_putaway_loc_di', false, WOarray);
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_qty', 'customdeploy_ebiz_rf_wo_putaway_qty_di', false, WOarray);

					//	}
					}


					/*if(parseInt(count) == parseInt(OpenTskLineCount))
					{
						//work order details
						var Qty = searchresults.getFieldValue('quantity');    	     
						var Itemid=searchresults.getFieldValue('assemblyitem');
						var ItemText=searchresults.getFieldText('assemblyitem');
						var WoCreatedDate=searchresults.getFieldValue('trandate');
						var WoDueDate=searchresults.getFieldValue('custbody_ebiz_woduedate');
						var WoExpcompletionDate=searchresults.getFieldValue('custbody_ebiz_expcompletiondate');
						var ItemQty=searchresults.getFieldValue('quantity');
						var Status=searchresults.getFieldValue('status');
						var ItemLocation=searchresults.getFieldValue('location');
						var WOName=searchresults.getFieldValue('tranid');
						var ItemPackcode=searchresults.getFieldValue('custrecord_ebizpackcodeskudim');




						nlapiLogExecution('ERROR', 'Itemid',Itemid);
						nlapiLogExecution('ERROR', 'WOName',WOName);
						nlapiLogExecution('ERROR', 'ItemText',ItemText);
						nlapiLogExecution('ERROR', 'Qty',Qty);
						nlapiLogExecution('ERROR', 'ItemLocation',ItemLocation);
						nlapiLogExecution('ERROR', 'ItemPackcode',ItemPackcode);

						WOarray["custparam_woid"] =vWOId; 
						WOarray["custparam_fetcheditemid"] =Itemid ;
						WOarray["custparam_item"] = ItemText ;
						WOarray["custparam_expectedquantity"] = Qty;
						WOarray["custparam_whlocation"] = ItemLocation;
						WOarray["custparam_noofrecords"] = vLineCount;
						WOarray["custparam_woname"] = WOName;
						WOarray["custparam_actualbegindate"] = WoCreatedDate;
						WOarray["custparam_wostatus"] = Status;
						WOarray["custparam_packcode"] = ItemPackcode;
						WOarray["custparam_screenno"] = 'WOPutawayGen';
						WOarray["custparam_actualbegintime"] =((curr_hour) + ":" + (curr_min)) ;
						WOarray["custparam_actualbegintimeampm"] = a_p;

						nlapiLogExecution('ERROR', 'a_p',a_p);

						var getActualBeginTime = request.getParameter('custparam_actualbegintime');
						var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');


						//check Assembly Item is LOT# Item or not
						var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
						var columns = nlapiLookupField('item', Itemid, fields);

						var serialInflg="F";
						var batchflag="F";
						var itemSubtype;
						if(columns != null && columns!='')
						{
							itemSubtype = columns.recordType;	
							serialInflg = columns.custitem_ebizserialin;
							batchflag= columns.custitem_ebizbatchlot;

						}

						if (itemSubtype == "lotnumberedinventoryitem" || itemSubtype=="lotnumberedassemblyitem" || batchflag=="T" )
						{
							WOarray["custparam_itemtype"] = "lotnumbereditem" ;
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_batchno', 'customdeploy_ebiz_rf_wo_putaway_batch_di', false, WOarray);
						}
						else
						{
							WOarray["custparam_itemtype"] = '' ;
							WOarray["custparam_error"] = "";
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_loc', 'customdeploy_ebiz_rf_wo_putaway_loc_di', false, WOarray);

						}

					}
					else
					{

						WOarray["custparam_error"] = "FIRST CONFIRM THE COMPONENT ITEM PICKS";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);

					}*/
				}
				else
				{

					WOarray["custparam_error"] = " FIRST CONFIRM THE COMPONENT ITEM PICKS";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);

				}
			}
			else
			{

				WOarray["custparam_error"] = "THIS WORK ORDER IS ALREADY PROCESSED/LOCATIONS ARE NOT GENERATED";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);

			}

		}

	}    
}


function CheckKitItem(vWOId,Qty){
	nlapiLogExecution('ERROR', 'vWOId', 'vWOId');
	nlapiLogExecution('ERROR', 'Qty', Qty);
	
	var count=0;
	var filterOpentask = new Array();

	filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2,3]));	//	Status - Location Assigned / Location Not Assigned/Putaway completed
	filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['5']));

	if(vWOId != null && vWOId != "")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
	}
	
	var vColumns = new Array();
	//vColumns.push(new nlobjSearchColumn('custrecord_act_qty',null,'sum'));
	vColumns.push(new nlobjSearchColumn('custrecord_expe_qty',null,'sum'));
	
	var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,vColumns);

	if(WOSearchResults!=null && WOSearchResults !='')
	{ 
		//var BuiltQty=WOSearchResults[0].getValue('custrecord_act_qty',null,'sum');
		//case# 201416964  modified to expected qty because while createaing KTS task we onlu update expected qty at the tome of putaway actual qty is updated 
		var BuiltQty=WOSearchResults[0].getValue('custrecord_expe_qty',null,'sum');// case# 201416964
		nlapiLogExecution('ERROR', 'BuiltQty', BuiltQty);
		if(BuiltQty==null || BuiltQty=='' || BuiltQty=='null')
			BuiltQty = 0;
		if(parseFloat(BuiltQty)< parseFloat(Qty))
		{
			count=0;
			
		}
		else
		{
			count= parseInt(WOSearchResults.length);	 
		}
	}

	return count;

}

//case 20126506 start : checking for open picks
//check for any open picks (Pick Location assign)
function CheckForOpenPicks(vWOId){
	
	nlapiLogExecution('ERROR', 'Check for Open picks ', 'done');
	nlapiLogExecution('ERROR', 'vWOId', 'vWOId');
	
	var OpenPickSearchResults='';
	
	var filterOpentask = new Array();
	filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));	//	Status - Picks Location Assign
	filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

	if(vWOId != null && vWOId != "")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
	}

	var WOcolumns = new Array();
	
	WOcolumns[0] = new nlobjSearchColumn('custrecord_line_no',null,'group');
	WOcolumns[1] = new nlobjSearchColumn('custrecord_sku',null,'group');
	WOcolumns[2] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	WOcolumns[3] = new nlobjSearchColumn('custrecord_act_qty',null,'sum');
	
	OpenPickSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);
	nlapiLogExecution('ERROR', 'OpenPickSearchResults ', OpenPickSearchResults);
	
	return OpenPickSearchResults;

}

//case 20126506 end 




