/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_Packing_ContainerLP.js,v $
 *     	   $Revision: 1.4.4.11.4.3.4.11 $
 *     	   $Date: 2014/09/19 16:33:21 $
 *     	   $Author: sponnaganti $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Packing_ContainerLP.js,v $
 * Revision 1.4.4.11.4.3.4.11  2014/09/19 16:33:21  sponnaganti
 * Case# 201410410 201410411
 * DCD SB Issue fix
 *
 * Revision 1.4.4.11.4.3.4.10  2014/06/13 12:53:45  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.4.11.4.3.4.9  2014/06/06 07:02:34  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.4.4.11.4.3.4.8  2014/05/30 12:28:35  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148276
 *
 * Revision 1.4.4.11.4.3.4.7  2014/05/30 00:41:03  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.11.4.3.4.6  2014/04/28 15:34:31  sponnaganti
 * case# 20148199
 * (for Assign Pack Station Functionality)
 *
 * Revision 1.4.4.11.4.3.4.5  2014/04/23 15:51:17  skavuri
 * Case# 20148124 issue fixed
 *
 * Revision 1.4.4.11.4.3.4.4  2014/04/18 15:43:21  nneelam
 * case#  20127682
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.11.4.3.4.3  2014/02/07 15:48:04  nneelam
 * case#  20126956
 * std bundle issue fix
 *
 * Revision 1.4.4.11.4.3.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.4.11.4.3.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.4.11.4.3  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4.4.11.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.4.4.11.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.4.4.11  2012/09/03 13:52:07  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.4.4.10  2012/08/01 07:16:42  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Routing based on Loc group seq
 *
 * Revision 1.4.4.9  2012/06/02 09:27:46  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to packing resolved.
 *
 * Revision 1.4.4.8  2012/05/18 17:55:41  gkalla
 * CASE201112/CR201113/LOG201121
 * RF Packing
 *
 * Revision 1.4.4.7  2012/05/17 22:52:39  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Packing
 *
 * Revision 1.4.4.6  2012/05/17 12:16:41  schepuri
 * CASE201112/CR201113/LOG201121
 * modified CONTAINER label with CARTON
 *
 * Revision 1.4.4.5  2012/05/08 09:30:27  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF packing
 *
 * Revision 1.4.4.4  2012/04/20 07:35:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.4.4.3  2012/04/17 10:52:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.4.4.2  2012/03/29 07:01:58  vrgurujala
 * t_NSWMS_LOG201121_89
 *
 * Revision 1.6  2012/03/27 05:39:48  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2012/03/09 09:30:05  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/21 07:42:42  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.2  2011/04/21 07:03:02  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.1  2011/04/19 14:19:06  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 *
 *****************************************************************************/
function EnterContainerLP(request, response)
{
	if (request.getMethod() == 'GET') 
	{   
		var getOrderno = request.getParameter('custparam_orderno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
		//var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		//var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var vBatchno = request.getParameter('custparam_batchno');
		var vlinecount = request.getParameter('custparam_linecount');
		var vloopcount = request.getParameter('custparam_loopcount'); 
		var enterqty =  request.getParameter('custparam_enteredqty'); 
		var getContlpno=request.getParameter('custparam_contlpno');
		var name=request.getParameter('custparam_name');
		var resultsCount =request.getParameter('custparam_count');
		var MultipleItemScaned =request.getParameter('custpage_multipleitemscan');
		var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
		getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
		getLPContainerSize= SORec.getFieldText('custrecord_container');
		var getLanguage = request.getParameter('custparam_language');	
		var vloc = request.getParameter('custparam_wmslocation');	
		
		
		
		
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "ENTER / SCAN LP CAJA #";
			st2 = "ENTER / TAMA&#209;O DE ESCANEADO";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "CAJA CERRADO";
			st6 = "CIERRE Y SALIR";
		}
		else
		{
			st0 = "";
			st1 = "ENTER/SCAN CARTON LP #";
			st2 = "ENTER/SCAN SIZE";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "CLOSE CARTON";
			st6 = "CLOSE&EXIT";

		}    	

		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlp').focus();";        
		html = html + "</script>";        
		html = html + "</head><body>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":<label>" + getContainerLPNo + "</label>";
		html = html + "				<input type='hidden' name='hdnOrderNo' value=" + getOrderno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";        
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnlinecount' value=" + vlinecount + ">";
		html = html + "				<input type='hidden' name='hdnloopcount' value=" + vloopcount + ">";
		html = html + "				<input type='hidden' name='hdnenterqty' value=" + enterqty + ">";
		html = html + "				<input type='hidden' name='hdncontlpno' value=" + getContlpno + ">";
		html = html + "				<input type='hidden' name='hdnname' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value='" + getLPContainerSize + "'>";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerLPNo + ">";
		html = html + "				<input type='hidden' name='hdnresultscount' value=" + resultsCount + ">";
		html = html + "				<input type='hidden' name='hdnMultipleItemScaned' value=" + MultipleItemScaned + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdngetvloc' value=" + vloc + ">";
		html = html + "				</td>";
		html = html + "			</tr>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ": <label>" + getLPContainerSize + "</label>";			
		html = html + "				</td>";
		html = html + "			</tr>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersize' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' />";
		html = html + "					" + st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + "<input name='cmdClose' type='submit' value='F8' />";
		html = html + "					" + st6 + " <input name='cmdExit' type='submit' value='F10'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>CLOSE & EXIT <input name='cmdSend' type='submit' value='CE'/>";
//		html = html + "					CLOSE <input name='cmdPrevious' type='submit' value='F7'/>"; 
//		html = html + "					CONTINUE <input name='cmdContinue' type='submit' value='CONT'/>"; 
//		html = html + "				</td>";
//		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var varEnteredLP = request.getParameter('enterlp');
		nlapiLogExecution('DEBUG', 'Entered LP', varEnteredLP);

		var varEnteredSize = request.getParameter('entersize');
		nlapiLogExecution('DEBUG', 'Entered Size', varEnteredSize);

		var  varhidcontlp = request.getParameter('hdncontlpno');
		nlapiLogExecution('DEBUG', 'varhidcontlp--->', varhidcontlp);
		
		var name=request.getParameter('hdnname');

		var SOarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]);    	

		var st9,st10;
		if( getLanguage == 'es_ES')
		{			
			st9 = "ORDEN NO V&#193;LIDO #";
			st10 = "CAJA NO SE REPITE";
		}
		else
		{			
			st9 = "INVALID ORDER #";
			st10 = "CARTON NOT MATCHED";
		}
		SOarray["custparam_error"] = st9;
		SOarray["custparam_screenno"] = '27A';
		SOarray["custparam_orderno"] = request.getParameter('hdnOrderNo');
		var getOrderNo = request.getParameter('hdnOrderNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray['custparam_linecount'] = request.getParameter('hdnlinecount');
		SOarray['custparam_loopcount'] = request.getParameter('hdnloopcount');
		SOarray['custparam_enteredqty'] = request.getParameter('hdnenterqty');
		SOarray['custparam_count']=request.getParameter('hdnresultscount');
		SOarray['custpage_multipleitemscan']=request.getParameter('hdnMultipleItemScaned');
	 	SOarray['custparam_name']=name;
		SOarray["custparam_contlpno"] = varhidcontlp;
		SOarray["custparam_wmslocation"] = request.getParameter('hdngetvloc');
		
		var vloc = request.getParameter('hdngetvloc');
		nlapiLogExecution('DEBUG', 'vloc', vloc);	
		
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');
		var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var Item = request.getParameter('hdnItem');
		var ItemDescription = request.getParameter('hdnItemDescription');
		var ItemInternalId = request.getParameter('hdnItemInternalId');
		var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var EndLocation = request.getParameter('hdnEnteredLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var getCortonNo = request.getParameter('hdncontlpno');
		var getresultscount= request.getParameter('hdnresultscount');

		var getExpQty = request.getParameter('hdnenterqty');
		var closedContainer="F";
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');   

		var optedSend = request.getParameter('cmdSend');

		var optedExit = request.getParameter('cmdExit');
		var optedClose = request.getParameter('cmdClose');
		if(optedClose!=null||optedExit!=null)
			closedContainer="T";

		if (varEnteredLP != '' && varEnteredLP != null) 
		{
			SOarray["custparam_containerlpno"] = varEnteredLP;
			nlapiLogExecution('DEBUG', 'varEnteredLP', varEnteredLP);	 
		}
		var salesorderID="";
		if(RecordInternalId!=null&&RecordInternalId!="")
			var salesorderID=nlapiLookupField("customrecord_ebiznet_trn_opentask",RecordInternalId,"custrecord_ebiz_order_no");
		nlapiLogExecution('DEBUG', 'salesorderID', salesorderID);
		//else if(varEnteredLP != '' && varEnteredLP!=request.getParameter('hdnFetchedContainerLPNo'))
		if(varEnteredLP != '' && varEnteredLP!=request.getParameter('hdnFetchedContainerLPNo'))
		{
			var vOpenTaskRecs=fnValidateEnteredContLP(varEnteredLP,salesorderID,null);
			nlapiLogExecution('Debug', 'Time Stamp at the end of fnValidateEnteredContLP',TimeStampinSec());
			if(vOpenTaskRecs != null && vOpenTaskRecs != '')
			{
				SOarray["custparam_error"] = "INVALID CARTON";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Container is Already available for other other or closed', varEnteredLP);
				return;
			}

			nlapiLogExecution('DEBUG', 'varEnteredLP', 'userdefined');
			var getEnteredContainerNoPrefix = varEnteredLP.substring(0, 3).toUpperCase();
		//	var LPReturnValue = ebiznet_LPRange_CL_withLPType(varEnteredLP.replace(/\s+$/,""), '2','2',null);//'2'UserDefiend,'2'PICK
			var LPReturnValue = ebiznet_LPRange_CL_withLPType(varEnteredLP.replace(/\s+$/,""), '2','2',vloc);
			if(LPReturnValue == true)
			{
				nlapiLogExecution('DEBUG', 'getItem', ItemInternalId);
				var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
				var itemCube = 0;
				var itemWeight=0;						
				if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
					itemCube = (parseFloat(ExpectedQuantity) * parseFloat(arrDims[0]));
					itemWeight = (parseFloat(ExpectedQuantity) * parseFloat(arrDims[1]));//									
				} 
				nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
				nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);
				nlapiLogExecution('DEBUG', 'ContainerSize:ContainerSize', varEnteredSize);		


				var ContainerCube;					
				var containerInternalId;
				var ContainerSize;
				if(varEnteredSize=="" || varEnteredSize==null)
				{
					getContainerSize=ContainerSize;
					nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	
					ContainerCube = itemCube;
				}	
				else
				{
					var arrContainerDetails = getContainerCubeAndTarWeight("",varEnteredSize);					
					if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

						ContainerCube =  parseFloat(arrContainerDetails[0]);						
						containerInternalId = arrContainerDetails[3];
						ContainerSize=arrContainerDetails[4];
						TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));// added on 28 nov
					} 
				}
				nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);				
				//Insert LP Record
				CreateRFMasterLPRecord(varEnteredLP,containerInternalId,ContainerCube,itemWeight,varEnteredSize,closedContainer);
			}
			else
			{
				SOarray["custparam_error"] = "INVALID CARTON";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Container Not Matched', varEnteredLP);
				return;
			}
		}
		else 
		{
			varEnteredLP=request.getParameter('hdnFetchedContainerLPNo');
		}			
		if (varEnteredSize != '' && varEnteredSize != null) 
		{
			SOarray["custparam_contsize"] = varEnteredSize;
			nlapiLogExecution('DEBUG', 'Container Size', varEnteredSize);	 
		}
		else
		{
			varEnteredSize=request.getParameter('hdnContainerSize');
		}
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.

		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, SOarray);
		}
		else if (optedClose == 'F8') 
		{
			nlapiLogExecution('DEBUG', 'CLOSE', 'CLOSE');

			var now = new Date();

			var a_p = "";
			var d = new Date();
			var curr_hour = now.getHours();
			if (curr_hour < 12) {
				a_p = "am";
			}
			else {
				a_p = "pm";
			}
			if (curr_hour == 0) {
				curr_hour = 12;
			}
			if (curr_hour > 12) {
				curr_hour = curr_hour - 12;
			}

			var curr_min = now.getMinutes();

			curr_min = curr_min + "";

			if (curr_min.length == 1) 
			{
				curr_min = "0" + curr_min;
			}

			var CurrentDate = DateStamp();




			//*************************************************************************//

			var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
			var itemCube = 0;
			var itemWeight=0;						
//			if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
//			//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
//			itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
//			itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


//			} 
//			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
//			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);


			var ContainerCube;					
			var containerInternalId;
			var ContainerSize;
			nlapiLogExecution('DEBUG', 'getContainerSize', varEnteredSize);	



			var arrContainerDetails = getContainerCubeAndTarWeight("",varEnteredSize);					
			if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

				ContainerCube =  parseFloat(arrContainerDetails[0]);						
				containerInternalId = arrContainerDetails[3];
				ContainerSize=arrContainerDetails[4];
				TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
				nlapiLogExecution('DEBUG', 'ContainerSize1', ContainerSize);	
			} 
			if(varEnteredSize=="" || varEnteredSize==null)
			{
				varEnteredSize=ContainerSize;
				nlapiLogExecution('DEBUG', 'ContainerSize2', ContainerSize);	
			}

			var varContainerCube;
			var varContainerTareWeight;
			var varContInventory;

			var filtersContiner = new Array(); 
			if(varEnteredSize!=null && varEnteredSize!='')
				filtersContiner.push(new nlobjSearchFilter('name', null, 'is', varEnteredSize));	

			var colsContainer = new Array();
			colsContainer[0]=new nlobjSearchColumn('custrecord_cubecontainer');  
			colsContainer[1]=new nlobjSearchColumn('custrecord_tareweight');  
			colsContainer[2]=new nlobjSearchColumn('custrecord_inventory_in_on');  

			var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersContiner, colsContainer);  
			var varcontintid;
			if(ContainerSearchResults!=null)
			{	
				varcontintid =  ContainerSearchResults[0].getId();
				varContainerCube = ContainerSearchResults[0].getValue('custrecord_cubecontainer');
				varContainerTareWeight = ContainerSearchResults[0].getValue('custrecord_tareweight');
				varContInventory = ContainerSearchResults[0].getValue('custrecord_inventory_in_on');				
			}
			else
			{
				SOarray["custparam_error"] = 'ENTER CARTON SIZE';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Container Not Matched', varEnteredLP);
				return;
			}

			var fetchQty;
			var diffQty;
			var contlpno;
			var wmslocation;
			var cubeinfo1;
			var cubeinfo2;
			var weightinfo1;
			var weightinfo2;
			var opentaskwt;
			var opentasktotalcube;
			var varname;


			var getItemDetails  =  request.getParameter('hdnItem');
			var getQtyDetails   = request.getParameter('hdnExpectedQuantity');

			nlapiLogExecution('DEBUG', 'getItemDetails::', getItemDetails);
			nlapiLogExecution('DEBUG', 'getQtyDetails::', getQtyDetails);

			var eackitem = new Array();
			var eachitemqty = new Array();
			nlapiLogExecution('DEBUG', 'vLoopCount::', vLoopCount);
			var vLoopCount =  request.getParameter('hdnloopcount');

			if(parseFloat(getresultscount)==1) 
			{
				eackitem[0]= getItemDetails;
				eachitemqty[0] = getQtyDetails;
			}
			else
			{		    		  
				eackitem= getItemDetails.split(',');
				eachitemqty = getQtyDetails.split(',');
			}
			nlapiLogExecution('DEBUG', 'eackitem length::', eackitem.length);

			// nlapiLogExecution('DEBUG', 'getQtyDetails::', getQtyDetails);

			var k=0;

			if(RecordInternalId != null && RecordInternalId != '')
			{
				UpdateoutboundInventory(ContainerLPNo,varEnteredLP);
				var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
				if(transaction != null && transaction != '')
				{   


					transaction.setFieldValue('custrecord_container_lp_no', varEnteredLP);
					transaction.setFieldValue('custrecord_totalcube',parseFloat(cubeinfo2).toFixed(4));		
					transaction.setFieldValue('custrecord_total_weight',parseFloat(weightinfo2).toFixed(4));
					nlapiSubmitRecord(transaction, false, true);

					var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',RecordInternalId);	 //for Assign Station purpose
					AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
					AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
					AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
					AssignPackStationrec.setFieldValue('custrecord_sku', null);
					AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
					AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);


					//custrecord_sku', null, 'anyof', iteminternalid);
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
					var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
				}	
			}	


			/*
			for(var i=0;i<eackitem.length;i++)
				// for(var i=0;i<getresultscount;i++)
			{									
				nlapiLogExecution('DEBUG', 'inside for loop', 'inside for loop');
				nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
				nlapiLogExecution('DEBUG', 'varhidcontlp', varhidcontlp);

        		var iteminternalid=SOarray["custparam_iteminternalid"];
        		/*var OpenTaskitemFilters = new Array();	
	    	 	if(getOrderNo!=null && getOrderNo!='')	    	 	
	    	 	OpenTaskitemFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
	    	 	if(varhidcontlp!=null && varhidcontlp!='')	
	    	 		OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varhidcontlp));

	    	 //	OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof',[3]));



				  var OpenTaskitemColumns = new Array();
				    OpenTaskitemColumns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				    OpenTaskitemColumns[1] = new nlobjSearchColumn('custrecord_act_qty');

				    var OpenTaskitemSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskitemFilters, OpenTaskitemColumns);
				if(OpenTaskitemSearchResults!=null && OpenTaskitemSearchResults!='')
				{
					iteminternalid=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_sku_no');
					//getExpQty = OpenTaskitemSearchResults[i].getValue('custrecord_act_qty'); //Assign the Enter Qty here imp
				}
        		getExpQty=eachitemqty[i];
        		nlapiLogExecution('DEBUG', 'iteminternalid', iteminternalid);
        		nlapiLogExecution('DEBUG', 'getExpQty', getExpQty);

        		nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
        		nlapiLogExecution('DEBUG', 'varhidcontlp', varhidcontlp);


//      		var OpenTaskFilters = new Array();	                
//      		OpenTaskFilters[0] = new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo);
//      		OpenTaskFilters[1] = new nlobjSearchFilter('custrecord_container_lp_no', null,'is',varhidcontlp);
//      		OpenTaskFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', iteminternalid);
//      		OpenTaskFilters[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3); //3 stand for pick

        		//OpenTaskFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', eackitem[i]);
        		nlapiLogExecution('DEBUG','getOrderNo',getOrderNo);
        		nlapiLogExecution('DEBUG','varhidcontlp',varhidcontlp);
        		nlapiLogExecution('DEBUG','iteminternalid',iteminternalid);
        		var SOFilters = new Array();
        		if(getOrderNo!=null && getOrderNo!='')
        			SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
        		//if(varhidcontlp!=null && varhidcontlp!='')
        			//SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varhidcontlp));
        		SOFilters.push(new nlobjSearchFilter('custrecord_act_end_date',null, 'isnotempty', null));

        		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
        		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof', [28]));							


        		nlapiLogExecution('DEBUG', 'eackitem[i]::', eackitem[i]);
        		nlapiLogExecution('DEBUG', 'eachitemqty[i]::',eachitemqty[i]);

        		var OpenTaskColumns = new Array();
        		OpenTaskColumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
        		OpenTaskColumns[1] = new nlobjSearchColumn('custrecord_container');
        		OpenTaskColumns[2] = new nlobjSearchColumn('custrecord_totalcube');


        		var OpenTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, OpenTaskColumns);

        		if (OpenTaskSearchResults != null && OpenTaskSearchResults.length > 0) 
        		{
        			//var Recid= OpenTaskSearchResults[0].getId();
        			for(var i=0;i<OpenTaskSearchResults.length;i++)
        			{
        			var Recid=OpenTaskSearchResults[i].getId();
        			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);			    	
        			transaction.setFieldValue('custrecord_container_lp_no', varEnteredLP);//28 stands for packcomplete //Added on 24th

        			nlapiSubmitRecord(transaction, false, true);

        			cubeinfo2 =   (getExpQty * parseFloat(opentasktotalcube)) / fetchQty;
        			nlapiLogExecution('DEBUG', 'cubeinfo2::', cubeinfo2);


        			weightinfo2 = (getExpQty * parseFloat(opentaskwt)) / fetchQty;
        			nlapiLogExecution('DEBUG', 'weightinfo2::', weightinfo2);
        			nlapiLogExecution('DEBUG', 'varContainerTareWeight::', varContainerTareWeight);

        			var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	 //for Assign Station purpose
        			UpdateoutboundInventory(ContainerLPNo,varEnteredLP);

        			nlapiLogExecution('DEBUG', 'Before Deletion of Record::', diffQty);

        			if(diffQty<=0)
        			{
        				var OpenTaskDelFilters = new Array();	                		    		
        				OpenTaskDelFilters[0] = new nlobjSearchFilter('custrecord_container_lp_no', null,'is',varhidcontlp);
        				OpenTaskDelFilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 13); //13 stands for   STGM
        				OpenTaskDelFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', iteminternalid);

        				var OpenTaskDelSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskDelFilters, null);

        				if (OpenTaskDelSearchResults != null && OpenTaskDelSearchResults.length > 0) 
        				{
        					var STGMRecid= OpenTaskDelSearchResults[0].getId();
        					var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', STGMRecid);
        				}

        				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', Recid);		
        				nlapiLogExecution('DEBUG', 'A Record is deleted from Open Task with following internal id Recid::',Recid);
        			}				    					    					   		    		    
        		}
        		}


        		var LpTotalWt;
        		var LpTotalVolume;

        		LpTotalWt = parseFloat(varContainerTareWeight) + parseFloat(weightinfo2);
        		LpTotalVolume = parseFloat(cubeinfo1) + parseFloat(cubeinfo2);

        		if(k==0)
        		{

        			var mastlpcustomrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
        			mastlpcustomrecord.setFieldValue('name', varEnteredLP);
        			mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', varEnteredLP);
        			mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 2);//2 stands for PICK
        			if(varcontintid!=''&&varcontintid!=null)
        				mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', varcontintid);
        			mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght',parseFloat(LpTotalWt).toFixed(4));    
        			//if(wmslocation!=''&&wmslocation!=null)
        				//mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_site', wmslocation);  

        			nlapiLogExecution('DEBUG', 'varContInventory--->', varContInventory);

        			if(parseFloat(varContInventory) == 2) // in case of "ON".
        			{
        				nlapiLogExecution('DEBUG', 'varContInventory:::::', varContInventory);
        				mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(LpTotalVolume).toFixed(4));
        			}
        			else  // in case of "IN" this is Default if no value is selected.
        			{
        				nlapiLogExecution('DEBUG', 'varContInventory:', varContInventory);
        				mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(varContainerCube).toFixed(4));
        			}

        			nlapiLogExecution('DEBUG', 'Before LP Record calling','Before LP Record calling');
        			var rec = nlapiSubmitRecord(mastlpcustomrecord, false, true);
        			nlapiLogExecution('DEBUG', 'After LP Record calling','Before LP Record calling');
        			nlapiLogExecution('DEBUG','varEnteredLP',varEnteredLP);
        			nlapiLogExecution('DEBUG','CurrentDate',CurrentDate);

        			//Code Added for Assign Pack Station Functionality

        			AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
        			AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
        			AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
        			AssignPackStationrec.setFieldValue('custrecord_sku', null);
        			AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
        			AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);


        			//custrecord_sku', null, 'anyof', iteminternalid);
        			nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
        			var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
        			nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');

        		}

        		k++;

			}   	    		    			*/
			nlapiLogExecution('DEBUG', 'loop count', SOarray['custparam_loopcount']);
			nlapiLogExecution('DEBUG', 'line count', SOarray['custparam_linecount']);
			nlapiLogExecution('DEBUG', 'Order', getOrderNo);

			//response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);
			//if(SOarray['custpage_multipleitemscan']=='True')
			//multipleSkuScan(getOrderNo,varhidcontlp,varEnteredLP,varEnteredSize,getresultscount,ContainerLPNo,request);

			var SOarray = new Array();
			SOarray["custparam_error"] = st9;
			SOarray["custparam_screenno"] = '27A';
			SOarray["custparam_orderno"] = request.getParameter('hdnOrderNo');
			SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
			SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
			SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
			SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
			SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
			SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
			SOarray['custparam_linecount'] = '1';
			SOarray['custparam_loopcount'] ='1';

			nlapiLogExecution('DEBUG', 'Before Passing to item from close button', 'Before Passing to item from close button');
			//response.sendRedirect('SUITELET', 'customscript_rf_packing_item', 'customdeploy_rf_packing_item_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, optedEvent);
			nlapiLogExecution('DEBUG', 'After Passing to item from close button', 'After Passing to item from close button');



		} // end of close button //******************************************************************************//        
		else if(optedExit == 'F10')
		{                	


			nlapiLogExecution('DEBUG', 'CLOSE & EXIT', 'CLOSE & EXIT');

			var now = new Date();

			var a_p = "";
			var d = new Date();
			var curr_hour = now.getHours();
			if (curr_hour < 12) {
				a_p = "am";
			}
			else {
				a_p = "pm";
			}
			if (curr_hour == 0) {
				curr_hour = 12;
			}
			if (curr_hour > 12) {
				curr_hour = curr_hour - 12;
			}

			var curr_min = now.getMinutes();

			curr_min = curr_min + "";

			if (curr_min.length == 1) 
			{
				curr_min = "0" + curr_min;
			}

			var CurrentDate = DateStamp();




			//*************************************************************************//

			var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
			var itemCube = 0;
			var itemWeight=0;						
//			if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
//			//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
//			itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
//			itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


//			} 
//			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
//			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);


			var ContainerCube;					
			var containerInternalId;
			var ContainerSize;
			nlapiLogExecution('DEBUG', 'getContainerSize', varEnteredSize);	



			var arrContainerDetails = getContainerCubeAndTarWeight("",varEnteredSize);					
			if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

				ContainerCube =  parseFloat(arrContainerDetails[0]);						
				containerInternalId = arrContainerDetails[3];
				ContainerSize=arrContainerDetails[4];
				TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
				nlapiLogExecution('DEBUG', 'ContainerSize1', ContainerSize);	
			} 
			if(varEnteredSize=="" || varEnteredSize==null)
			{
				varEnteredSize=ContainerSize;
				nlapiLogExecution('DEBUG', 'ContainerSize2', ContainerSize);	
			}

			var varContainerCube;
			var varContainerTareWeight;
			var varContInventory;

			var filtersContiner = new Array();        						
			filtersContiner.push(new nlobjSearchFilter('name', null, 'is', varEnteredSize));	

			var colsContainer = new Array();
			colsContainer[0]=new nlobjSearchColumn('custrecord_cubecontainer');  
			colsContainer[1]=new nlobjSearchColumn('custrecord_tareweight');  
			colsContainer[2]=new nlobjSearchColumn('custrecord_inventory_in_on');  

			var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersContiner, colsContainer);  
			var varcontintid;
			if(ContainerSearchResults!=null)
			{	
				varcontintid =  ContainerSearchResults[0].getId();
				varContainerCube = ContainerSearchResults[0].getValue('custrecord_cubecontainer');
				varContainerTareWeight = ContainerSearchResults[0].getValue('custrecord_tareweight');
				varContInventory = ContainerSearchResults[0].getValue('custrecord_inventory_in_on');				
			}
			else
			{
				SOarray["custparam_error"] = st10;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Container Not Matched', varEnteredLP);
				return;
			}

			var fetchQty;
			var diffQty=0;
			var contlpno;
			var wmslocation;
			var cubeinfo1;
			var cubeinfo2;
			var weightinfo1;
			var weightinfo2;
			var opentaskwt;
			var opentasktotalcube;
			var varname;


			var getItemDetails  =  request.getParameter('hdnItem');
			var getQtyDetails   = request.getParameter('hdnExpectedQuantity');

			nlapiLogExecution('DEBUG', 'getItemDetails::', getItemDetails);
			nlapiLogExecution('DEBUG', 'getQtyDetails::', getQtyDetails);

			var eackitem = new Array();
			var eachitemqty = new Array();

			var vLoopCount =  request.getParameter('hdnloopcount');

			if(parseFloat(getresultscount)==1) 
			{
				eackitem[0]= getItemDetails;
				eachitemqty[0] = getQtyDetails;
			}
			else
			{		    		  
				eackitem= getItemDetails.split(',');
				eachitemqty = getQtyDetails.split(',');
			}
			nlapiLogExecution('DEBUG', 'eackitem length::', eackitem.length);

			// nlapiLogExecution('DEBUG', 'getQtyDetails::', getQtyDetails);
			if(RecordInternalId != null && RecordInternalId != '')
			{
				UpdateoutboundInventory(ContainerLPNo,varEnteredLP);
				var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
				if(transaction != null && transaction != '')
				{   


					transaction.setFieldValue('custrecord_container_lp_no', varEnteredLP);
					transaction.setFieldValue('custrecord_totalcube',parseFloat(cubeinfo2).toFixed(4));		
					transaction.setFieldValue('custrecord_total_weight',parseFloat(weightinfo2).toFixed(4));
					nlapiSubmitRecord(transaction, false, true);

					var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',RecordInternalId);	 //for Assign Station purpose
					AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
					AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
					AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
					AssignPackStationrec.setFieldValue('custrecord_sku', null);
					AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
					AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);


					//custrecord_sku', null, 'anyof', iteminternalid);
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
					var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
				}	
			}	


			var k=0;

			/*//for(var i=0;i<eackitem.length-1;i++)
			for(var i=0;i<getresultscount;i++)
			{									
		    	 	nlapiLogExecution('DEBUG', 'inside for loop', 'inside for loop');

		    		var iteminternalid=SOarray["custparam_iteminternalid"];
		    	 	/*
		    	 	var OpenTaskitemFilters = new Array();
		    	 	nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
		    	 	nlapiLogExecution('DEBUG', 'varhidcontlp', varhidcontlp);
		    	 	if(getOrderNo!=null && getOrderNo!='')	    	 	
		    	 	OpenTaskitemFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
		    	 	if(varhidcontlp!=null && varhidcontlp!='')	
		    	 		OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varhidcontlp));
		    	 	OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_act_end_date',null, 'isnotempty'));

		    	 	var OpenTaskitemColumns = new Array();
					    OpenTaskitemColumns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
					    OpenTaskitemColumns[1] = new nlobjSearchColumn('custrecord_act_qty');

					    var OpenTaskitemSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskitemFilters, OpenTaskitemColumns);
					if(OpenTaskitemSearchResults!=null && OpenTaskitemSearchResults!='')
					{
						iteminternalid=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_sku_no');
						getExpQty = OpenTaskitemSearchResults[i].getValue('custrecord_act_qty'); //Assign the Enter Qty here imp
					}
		    	 	nlapiLogExecution('DEBUG', 'iteminternalid', iteminternalid);



//					var OpenTaskFilters = new Array();	                
//					OpenTaskFilters[0] = new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo);
//					OpenTaskFilters[1] = new nlobjSearchFilter('custrecord_container_lp_no', null,'is',varhidcontlp);
//					OpenTaskFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', iteminternalid);
//					OpenTaskFilters[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3); //3 stand for pick

		    	 	var SOFilters = new Array();
	        		if(getOrderNo!=null && getOrderNo!='')
	        			SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));

		    	 	SOFilters.push(new nlobjSearchFilter('custrecord_act_end_date',null, 'isnotempty', null));

	        		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
	        		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof', [28]));							


	        		nlapiLogExecution('DEBUG', 'eackitem[i]::', eackitem[i]);
	        		nlapiLogExecution('DEBUG', 'eachitemqty[i]::',eachitemqty[i]);

	        		var OpenTaskColumns = new Array();
	        		OpenTaskColumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
	        		OpenTaskColumns[1] = new nlobjSearchColumn('custrecord_container');
	        		OpenTaskColumns[2] = new nlobjSearchColumn('custrecord_totalcube');


	        		var OpenTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, OpenTaskColumns);


				    if (OpenTaskSearchResults != null && OpenTaskSearchResults.length > 0) 
		            {
				    	for(var i=0;i<OpenTaskSearchResults.length;i++)
	        			{
	        			var Recid=OpenTaskSearchResults[i].getId();
	        			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);			    	
	        			transaction.setFieldValue('custrecord_container_lp_no', varEnteredLP);//28 stands for packcomplete //Added on 24th

	        			nlapiSubmitRecord(transaction, false, true);

	        			cubeinfo2 =   (getExpQty * parseFloat(opentasktotalcube)) / fetchQty;
	        			nlapiLogExecution('DEBUG', 'cubeinfo2::', cubeinfo2);


	        			weightinfo2 = (getExpQty * parseFloat(opentaskwt)) / fetchQty;
	        			nlapiLogExecution('DEBUG', 'weightinfo2::', weightinfo2);
	        			nlapiLogExecution('DEBUG', 'varContainerTareWeight::', varContainerTareWeight);

	        			var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	 //for Assign Station purpose
	        			UpdateoutboundInventory(ContainerLPNo,varEnteredLP);

	        			nlapiLogExecution('DEBUG', 'Before Deletion of Record::', diffQty);

	        			if(diffQty<=0)
	        			{
	        				var OpenTaskDelFilters = new Array();	                		    		
	        				OpenTaskDelFilters[0] = new nlobjSearchFilter('custrecord_container_lp_no', null,'is',varhidcontlp);
	        				OpenTaskDelFilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 13); //13 stands for   STGM
	        				OpenTaskDelFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', iteminternalid);

	        				var OpenTaskDelSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskDelFilters, null);

	        				if (OpenTaskDelSearchResults != null && OpenTaskDelSearchResults.length > 0) 
	        				{
	        					var STGMRecid= OpenTaskDelSearchResults[0].getId();
	        					var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', STGMRecid);
	        				}

	        				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', Recid);		
	        				nlapiLogExecution('DEBUG', 'A Record is deleted from Open Task with following internal id Recid::',Recid);
	        			}				    					    					   		    		    
	        		}
				    	//var Recid= OpenTaskSearchResults[0].getId();

		            }


				    var LpTotalWt;
				    var LpTotalVolume;

				    LpTotalWt = parseFloat(varContainerTareWeight) + parseFloat(weightinfo2);
				    LpTotalVolume = parseFloat(cubeinfo1) + parseFloat(cubeinfo2);

				    if(k==0)
				    {

						    var mastlpcustomrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
				        	mastlpcustomrecord.setFieldValue('name', varEnteredLP);
				        	mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', varEnteredLP);
				        	mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 2);//2 stands for PICK
				        	mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', varcontintid);
				        	mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght',parseFloat(LpTotalWt).toFixed(4));        	        	
				        	//mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_site', wmslocation);  

				        	nlapiLogExecution('DEBUG', 'varContInventory--->', varContInventory);

				        	if(parseFloat(varContInventory) == 2) // in case of "ON".
				        	{
				        		nlapiLogExecution('DEBUG', 'varContInventory:::::', varContInventory);
				        		mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(LpTotalVolume).toFixed(4));
				        	}
				        	else  // in case of "IN" this is Default if no value is selected.
				        	{
				        		nlapiLogExecution('DEBUG', 'varContInventory:', varContInventory);
				        		mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(varContainerCube).toFixed(4));
				        	}

				        	nlapiLogExecution('DEBUG', 'Before LP Record calling','Before LP Record calling');
							var rec = nlapiSubmitRecord(mastlpcustomrecord, false, true);
							nlapiLogExecution('DEBUG', 'After LP Record calling','Before LP Record calling');


							 //Code Added for Assign Pack Station Functionality

							AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
							AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
							AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
							AssignPackStationrec.setFieldValue('custrecord_sku', null);
							AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
							AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);


							//custrecord_sku', null, 'anyof', iteminternalid);
							nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
							var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
							nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');

				    }

				    k++;

			}*/   	    		    			
			nlapiLogExecution('DEBUG', 'loop count', SOarray['custparam_loopcount']);
			nlapiLogExecution('DEBUG', 'line count', SOarray['custparam_linecount']);
			nlapiLogExecution('DEBUG', 'Order', getOrderNo);

			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);
			//response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, optedEvent);

		} //end of close & exit functionality  //******************************************************************************//
		else//if(optedSend == 'ENT')
		{        	
			nlapiLogExecution('DEBUG', 'CONTINUE', 'CONTINUE');

			var now = new Date();

			var a_p = "";
			var d = new Date();
			var curr_hour = now.getHours();
			if (curr_hour < 12) {
				a_p = "am";
			}
			else {
				a_p = "pm";
			}
			if (curr_hour == 0) {
				curr_hour = 12;
			}
			if (curr_hour > 12) {
				curr_hour = curr_hour - 12;
			}

			var curr_min = now.getMinutes();

			curr_min = curr_min + "";

			if (curr_min.length == 1) 
			{
				curr_min = "0" + curr_min;
			}

			var CurrentDate = DateStamp();

			var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
			var itemCube = 0;
			var itemWeight=0;						
//			if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
//			//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
//			itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
//			itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


//			} 
//			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
//			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);


			var ContainerCube;					
			var containerInternalId;
			var ContainerSize;
			nlapiLogExecution('DEBUG', 'getContainerSize', varEnteredSize);	



			var arrContainerDetails = getContainerCubeAndTarWeight("",varEnteredSize);					
			if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

				ContainerCube =  parseFloat(arrContainerDetails[0]);						
				containerInternalId = arrContainerDetails[3];
				ContainerSize=arrContainerDetails[4];
				TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
				nlapiLogExecution('DEBUG', 'ContainerSize1', ContainerSize);	
			} 
			if(varEnteredSize=="" || varEnteredSize==null)
			{
				varEnteredSize=ContainerSize;
				nlapiLogExecution('DEBUG', 'ContainerSize2', ContainerSize);	
			}


			//*************************************************************************//

			var varContainerCube;
			var varContainerTareWeight;
			var varContInventory;

			var filtersContiner = new Array();  
			if(varEnteredSize!=null && varEnteredSize!='')
				filtersContiner.push(new nlobjSearchFilter('name', null, 'is', varEnteredSize));	

			var colsContainer = new Array();
			colsContainer[0]=new nlobjSearchColumn('custrecord_cubecontainer');  
			colsContainer[1]=new nlobjSearchColumn('custrecord_tareweight');  
			colsContainer[2]=new nlobjSearchColumn('custrecord_inventory_in_on');  

			var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersContiner, colsContainer);  
			var varcontintid;
			if(ContainerSearchResults!=null)
			{	
				varcontintid =  ContainerSearchResults[0].getId();
				varContainerCube = ContainerSearchResults[0].getValue('custrecord_cubecontainer');
				varContainerTareWeight = ContainerSearchResults[0].getValue('custrecord_tareweight');
				varContInventory = ContainerSearchResults[0].getValue('custrecord_inventory_in_on');				
			}
			else
			{
				SOarray["custparam_error"] = st10;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Container Not Matched', varEnteredLP);
				return;
			}

			var fetchQty;
			var diffQty;
			var contlpno;
			var wmslocation;
			var cubeinfo1;
			var cubeinfo2;
			var weightinfo1;
			var weightinfo2;
			var opentaskwt;
			var opentasktotalcube;
			var varname;


			var getItemDetails  =  request.getParameter('hdnItem');
			var getQtyDetails   = request.getParameter('hdnExpectedQuantity');

			nlapiLogExecution('DEBUG', 'getItemDetails::', getItemDetails);
			nlapiLogExecution('DEBUG', 'getQtyDetails::', getQtyDetails);

			var eackitem = new Array();
			var eachitemqty = new Array();

			var vLoopCount =  request.getParameter('hdnloopcount');

			if(parseFloat(getresultscount)==1) 
			{
				eackitem[0]= getItemDetails;
				eachitemqty[0] = getQtyDetails;
			}
			else
			{		    		  
				eackitem= getItemDetails.split(',');
				eachitemqty = getQtyDetails.split(',');
			}
			nlapiLogExecution('DEBUG', 'eackitem length::', eackitem.length);

			// nlapiLogExecution('DEBUG', 'getQtyDetails::', getQtyDetails);


			if(RecordInternalId != null && RecordInternalId != '')
			{
				UpdateoutboundInventory(ContainerLPNo,varEnteredLP);
				var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
				if(transaction != null && transaction != '')
				{   


					transaction.setFieldValue('custrecord_container_lp_no', varEnteredLP);
					transaction.setFieldValue('custrecord_totalcube',parseFloat(cubeinfo2).toFixed(4));		
					transaction.setFieldValue('custrecord_total_weight',parseFloat(weightinfo2).toFixed(4));
					nlapiSubmitRecord(transaction, false, true);

					var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',RecordInternalId);	 //for Assign Station purpose
					AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
					AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
					AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
					AssignPackStationrec.setFieldValue('custrecord_sku', null);
					AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
					AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);


					//custrecord_sku', null, 'anyof', iteminternalid);
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
					var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
				}	
			}	


			var k=0;

			//for(var i=0;i<eackitem.length-1;i++)
			/* for(var i=0;i<getresultscount;i++)
		{									
	    	 	nlapiLogExecution('DEBUG', 'inside for loop', 'inside for loop');
	    		nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
	    		nlapiLogExecution('DEBUG', 'varhidcontlp', varhidcontlp);

	    		var iteminternalid;
	    	 	var OpenTaskitemFilters = new Array();	
	    	 	if(getOrderNo!=null && getOrderNo!='')	    	 	
	    	 	OpenTaskitemFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
	    	 	if(varhidcontlp!=null && varhidcontlp!='')	
	    	 		OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varhidcontlp));
	    	 	OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_act_end_date',null, 'isnotempty'));



				  var OpenTaskitemColumns = new Array();
				    OpenTaskitemColumns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				    OpenTaskitemColumns[1] = new nlobjSearchColumn('custrecord_act_qty');

				    var OpenTaskitemSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskitemFilters, OpenTaskitemColumns);
				if(OpenTaskitemSearchResults!=null && OpenTaskitemSearchResults!='')
				{
					iteminternalid=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_sku_no');
					getExpQty = OpenTaskitemSearchResults[i].getValue('custrecord_act_qty'); //Assign the Enter Qty here imp
				}
	    	 	nlapiLogExecution('DEBUG', 'iteminternalid', iteminternalid);
	    	 	nlapiLogExecution('DEBUG', 'getExpQty', getExpQty);

	    	 	nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
	    	 	nlapiLogExecution('DEBUG', 'varhidcontlp', varhidcontlp);



				var SOFilters = new Array();
				if(getOrderNo!=null && getOrderNo!='')
					SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
				if(varhidcontlp!=null && varhidcontlp!='')
					SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varhidcontlp));
               if(iteminternalid!=null && iteminternalid!='')
				SOFilters.push(new nlobjSearchFilter('custrecord_sku',null, 'anyof', iteminternalid));
				SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof', [8]));	


				//OpenTaskFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', eackitem[i]);

				//getExpQty = eachitemqty[i]; //Assign the Enter Qty here imp

				nlapiLogExecution('DEBUG', 'eackitem[i]::', eackitem[i]);
				nlapiLogExecution('DEBUG', 'eachitemqty[i]::',eachitemqty[i]);

			    var OpenTaskColumns = new Array();
			    OpenTaskColumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
			    OpenTaskColumns[1] = new nlobjSearchColumn('custrecord_container');
			    OpenTaskColumns[2] = new nlobjSearchColumn('custrecord_totalcube');


			    var OpenTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, OpenTaskColumns);
			    var AssignPackStationrec='';// Case# 20148124    
			    if (OpenTaskSearchResults != null && OpenTaskSearchResults.length > 0) 
	            {
			    	var Recid= OpenTaskSearchResults[0].getId();
			    	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);
			    	//getExpQty
			    	fetchQty =transaction.getFieldValue('custrecord_act_qty');
			    	contlpno = transaction.getFieldValue('custrecord_container_lp_no');
			    	wmslocation = transaction.getFieldValue('custrecord_wms_location');
			    	opentaskwt	= transaction.getFieldValue('custrecord_total_weight');
			    	opentasktotalcube = transaction.getFieldValue('custrecord_totalcube');
			    	varname = transaction.getFieldValue('name');
			    	var Invtrefno=transaction.getFieldValue('custrecord_invref_no');
			    	var OrderNo=transaction.getFieldValue('custrecord_ebiz_order_no');

			    	var skuname = transaction.getFieldValue('custrecord_sku');

			    	nlapiLogExecution('DEBUG', 'skuname::', skuname);

			    	nlapiLogExecution('DEBUG', 'getExpQty::', getExpQty);
			    	nlapiLogExecution('DEBUG', 'fetchQty::', fetchQty);
			    	nlapiLogExecution('DEBUG', 'contlpno::', contlpno);
			    	nlapiLogExecution('DEBUG', 'wmslocation::', wmslocation);

			    	if(parseFloat(fetchQty)>=parseFloat(getExpQty))
			    	{
			    		diffQty = parseFloat(fetchQty) - parseFloat(getExpQty);   
			    	}
			    	nlapiLogExecution('DEBUG', 'diffQty::', diffQty);

			    	cubeinfo1 =   (diffQty * parseFloat(opentasktotalcube)) / fetchQty;
			    	nlapiLogExecution('DEBUG', 'cubeinfo1::', cubeinfo1);


			    	weightinfo1 = (diffQty * parseFloat(opentaskwt)) / fetchQty;
			    	nlapiLogExecution('DEBUG', 'weightinfo1::', weightinfo1);
			    	nlapiLogExecution('DEBUG', 'varContainerTareWeight::', varContainerTareWeight);


			    	//varContainerCube
			    	transaction.setFieldValue('custrecord_act_qty',parseFloat(diffQty).toFixed(4));
			    	transaction.setFieldValue('custrecord_totalcube',parseFloat(cubeinfo1).toFixed(4));	
			    	transaction.setFieldValue('custrecord_total_weight',parseFloat(weightinfo1).toFixed(4));				    	
			    	//transaction.setFieldValue('custrecord_wms_status_flag', '28');//28 stands for packcomplete //Added on 24th

			    	nlapiSubmitRecord(transaction, false, true);

			    	cubeinfo2 =   (getExpQty * parseFloat(opentasktotalcube)) / fetchQty;
			    	nlapiLogExecution('DEBUG', 'cubeinfo2::', cubeinfo2);


			    	weightinfo2 = (getExpQty * parseFloat(opentaskwt)) / fetchQty;
			    	nlapiLogExecution('DEBUG', 'weightinfo2::', weightinfo2);
			    	nlapiLogExecution('DEBUG', 'varContainerTareWeight::', varContainerTareWeight);

			    	UpdateoutboundInventory(ContainerLPNo,varEnteredLP);
			    	// Case# 20148124 starts
			    	//var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	 //for Assign Station purpose
			    	AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	 //for Assign Station purpose
			    	// Case# 20148124 ends
			    	var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);		    	
			    	createopentaskrec.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
			    	//createopentaskrec.setFieldValue('custrecord_wms_status_flag', '28');//28 stands for packcomplete //Added on 24th
			    	createopentaskrec.setFieldValue('custrecord_act_end_date', DateStamp());
			    	createopentaskrec.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
			    	createopentaskrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);
			    	createopentaskrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
			    	createopentaskrec.setFieldValue('custrecord_container', varcontintid);	
			    	createopentaskrec.setFieldValue('custrecord_act_qty', parseFloat(getExpQty).toFixed(4));	
			    	createopentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(getExpQty).toFixed(4));
			    	createopentaskrec.setFieldValue('custrecord_from_lp_no', contlpno);	
			    	createopentaskrec.setFieldValue('custrecord_totalcube',parseFloat(cubeinfo2).toFixed(4));		
			    	createopentaskrec.setFieldValue('custrecord_total_weight',parseFloat(weightinfo2).toFixed(4));
			    	createopentaskrec.setFieldValue('name',varname);

			    	//createopentaskrec.setFieldValue('custrecord_total_weight',parseFloat(varContainerTareWeight)+parseFloat(weightinfo2));	

			    	nlapiSubmitRecord(createopentaskrec, false, true);
			    	//case# 20148199 starts	(for Assign Pack Station Functionality)	    	
			    	nlapiLogExecution('DEBUG', 'Before Deletion of Record::', diffQty);
			    	AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
					AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
					AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
					AssignPackStationrec.setFieldValue('custrecord_sku', null);
					AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
					AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);																	
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
					var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
					//case# 20148199 end
			    	if(diffQty<=0)
			    	{
			    		var OpenTaskDelFilters = new Array();	                		    		
			    		OpenTaskDelFilters[0] = new nlobjSearchFilter('custrecord_container_lp_no', null,'is',varhidcontlp);
			    		OpenTaskDelFilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 13); //13 stands for   STGM
			    		OpenTaskDelFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', iteminternalid);

			    		var OpenTaskDelSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskDelFilters, null);

					    if (OpenTaskDelSearchResults != null && OpenTaskDelSearchResults.length > 0) 
			            {
					    	var STGMRecid= OpenTaskDelSearchResults[0].getId();
					    	var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', STGMRecid);
			            }

			    		var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', Recid);		
			    		nlapiLogExecution('DEBUG', 'A Record is deleted from Open Task with following internal id Recid::',Recid);
			    	}				    					    					   		    		    
	            }


			    var LpTotalWt;
			    var LpTotalVolume;

			    LpTotalWt = parseFloat(varContainerTareWeight) + parseFloat(weightinfo2);
			    LpTotalVolume = parseFloat(cubeinfo1) + parseFloat(cubeinfo2);

			    if(k==0)
			    {

					    var mastlpcustomrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			        	mastlpcustomrecord.setFieldValue('name', varEnteredLP);
			        	mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', varEnteredLP);
			        	mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 2);//2 stands for PICK
			        	mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', varcontintid);
			        	mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght',parseFloat(LpTotalWt).toFixed(4));        	        	
			        	//mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_site', wmslocation);  

			        	nlapiLogExecution('DEBUG', 'varContInventory--->', varContInventory);

			        	if(parseFloat(varContInventory) == 2) // in case of "ON".
			        	{
			        		nlapiLogExecution('DEBUG', 'varContInventory:::::', varContInventory);
			        		mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(LpTotalVolume).toFixed(4));
			        	}
			        	else  // in case of "IN" this is Default if no value is selected.
			        	{
			        		nlapiLogExecution('DEBUG', 'varContInventory:', varContInventory);
			        		mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(varContainerCube).toFixed(4));
			        	}

			        	nlapiLogExecution('DEBUG', 'Before LP Record calling','Before LP Record calling');
						var rec = nlapiSubmitRecord(mastlpcustomrecord, false, true);
						nlapiLogExecution('DEBUG', 'After LP Record calling','Before LP Record calling');

						//case # 20127682
						 //Code Added for Assign Pack Station Functionality
						/*
						AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
						AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
						AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
						AssignPackStationrec.setFieldValue('custrecord_sku', null);
						AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
						AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);																	
						nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
						var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
						nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');


			    }

			    k++;

		}  */ 	    		    			
			nlapiLogExecution('DEBUG', 'loop count', SOarray['custparam_loopcount']);
			nlapiLogExecution('DEBUG', 'line count', SOarray['custparam_linecount']);
			nlapiLogExecution('DEBUG', 'Order', getOrderNo);

			//response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);

			nlapiLogExecution('DEBUG', 'Before Passing to item from close button', 'Before Passing to item from close button');
			//response.sendRedirect('SUITELET', 'customscript_rf_packing_item', 'customdeploy_rf_packing_item_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, optedEvent);
			nlapiLogExecution('DEBUG', 'After Passing to item from close button', 'After Passing to item from close button');
		}//  end of Continue button functionality  //******************************************************************************//
	}// main else part
}

function UpdateoutboundInventory(ContainerLPNo,varEnteredLP)
{
	nlapiLogExecution('DEBUG', 'ContainerLPNo',ContainerLPNo);
	nlapiLogExecution('DEBUG', 'varEnteredLP',varEnteredLP);

	try
	{
		
		var invtfilters=new Array();
		invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp',null,'is',ContainerLPNo));
		
		
		var invtcolumns=new Array();
		invtcolumns[0]=new nlobjSearchColumn('internalid');
		
		var Invtrefno=nlapiSearchRecord('customrecord_ebiznet_createinv',null,invtfilters,invtcolumns);
		if(Invtrefno!=null && Invtrefno!='')
		{
			for(var i=0;i<Invtrefno.length;i++)
			{
				var invtid=Invtrefno[i].getValue('internalid');
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invtid);

				Invttran.setFieldValue('custrecord_ebiz_inv_lp',varEnteredLP);	
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);
			}
		}

	
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}	



}
function CreateRFMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,ItemWeight,getContainerSize,closedContainer)
{
	var contLPExists = containerRFLPExists(vContLpNo);
	var LPWeightExits = getLPWeight(vContLpNo);
	if(LPWeightExits!=null && LPWeightExits!="")
	{		
		var ContLP=LPWeightExits[0];
		var LPWeight=LPWeightExits[1];
		nlapiLogExecution('DEBUG', 'LPWeight',LPWeight);
	}
	var TotalWeight;	

	if(getContainerSize=="" || getContainerSize==null)
	{
		TotalWeight = ItemWeight;
	}	
	else
	{
		var arrContainerDetails = getContainerCubeAndTarWeight(containerInternalId,"");

		if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) 
		{		
			TotalWeight = (parseFloat(ItemWeight) + parseFloat(arrContainerDetails[1]));
		} 
	}

	//case # 20126956,start Getting Rolebased location and max lpno.
	var ResultText='';
	var vRoleLocation=getRoledBasedLocation();
	ResultText=GetMaxLPNo('1','5',vRoleLocation);
	nlapiLogExecution('ERROR', 'ResultText',ResultText);
	//End
	

	if(contLPExists==null){		

		var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
		MastLP.setFieldValue('name', vContLpNo);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
		if(containerInternalId!="" && containerInternalId!=null)
		{
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
		}	
		nlapiLogExecution('DEBUG', 'TotalWeight',TotalWeight);
		nlapiLogExecution('DEBUG', 'ContainerCube',ContainerCube);
		if(TotalWeight!="" && TotalWeight!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(4));
		if(ContainerCube!="" && ContainerCube!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));
		if(ResultText!="" && ResultText!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
		var currentContext = nlapiGetContext();		        		
		if(closedContainer=="T")
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', 28);
		var retktoval = nlapiSubmitRecord(MastLP);
		nlapiLogExecution('DEBUG', 'Master LP Insertion','Success');

	}
	else
	{
		var recordId="";

		if(contLPExists!=null){


			for (var i = 0; i < contLPExists.length; i++){
				recordId = contLPExists[i].getId();
				nlapiLogExecution('DEBUG', 'recordId',recordId);		

				var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	
				TotWeight=parseFloat(ItemWeight)+parseFloat(LPWeight);
				nlapiLogExecution('DEBUG', 'itemWeight',ItemWeight);
				nlapiLogExecution('DEBUG', 'LPWeight',LPWeight);
				nlapiLogExecution('DEBUG', 'TotWeight',TotWeight);

				if(TotWeight!="" && TotWeight!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(4));
				if(ContainerCube!="" && ContainerCube!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));
				if(ResultText!="" && ResultText!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
					if(closedContainer=="T")
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', 28);
				var currentContext = nlapiGetContext();	        				
				nlapiSubmitRecord(MastLPUpdate, false, true);        				 
				nlapiLogExecution('DEBUG', 'Master LP Updation','Success');

			}

		}
	}
}

function containerRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	return searchresults;
}

function getLPWeight(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	var lpmastArr=new Array();
	nlapiLogExecution('DEBUG', 'Before:',lp); 
	if(searchresults != null && searchresults!= "" && searchresults.length>0)
	{
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));
		nlapiLogExecution('DEBUG', 'lp:',searchresults[0].getValue('custrecord_ebiz_lpmaster_lp')); 
		nlapiLogExecution('DEBUG', 'lpweight:',searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght')); 
	}
	return lpmastArr;
}

function multipleSkuScan(getOrderNo,varhidcontlp,varEnteredLP,varEnteredSize,getresultscount,ContainerLPNo,request)
{
	try{
		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_notes',null,'is','T'));
		if(getOrderNo!=null && getOrderNo!='')
			filter.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
		if(varhidcontlp!=null && varhidcontlp!='')
			filter.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varhidcontlp));
		filter.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_sku');
		column[1]=new nlobjSearchColumn('custrecord_act_qty');

		var searchrecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,column);
		if(searchrecord!=null&&searchrecord!="")
		{
			var ItemInternalId;
			var ExpectedQuantity;
			var Item;
			var Recid;
			for(var x=0;x<searchrecord.length;x++)
			{
				ItemInternalId=searchrecord[x].getValue('custrecord_sku');
				ExpectedQuantity=searchrecord[x].getValue('custrecord_act_qty');
				Item=searchrecord[x].getText('custrecord_sku');
				Recid=searchrecord[x].getId();
				nlapiLogExecution('DEBUG', 'CLOSE', 'CLOSE');

				var now = new Date();

				var a_p = "";
				var d = new Date();
				var curr_hour = now.getHours();
				if (curr_hour < 12) {
					a_p = "am";
				}
				else {
					a_p = "pm";
				}
				if (curr_hour == 0) {
					curr_hour = 12;
				}
				if (curr_hour > 12) {
					curr_hour = curr_hour - 12;
				}

				var curr_min = now.getMinutes();

				curr_min = curr_min + "";

				if (curr_min.length == 1) 
				{
					curr_min = "0" + curr_min;
				}

				var CurrentDate = DateStamp();




				//*************************************************************************//

				var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
				var itemCube = 0;
				var itemWeight=0;						
//				if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
//				//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
//				itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
//				itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


//				} 
//				nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
//				nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);


				var ContainerCube;					
				var containerInternalId;
				var ContainerSize;
				nlapiLogExecution('DEBUG', 'getContainerSize', varEnteredSize);	



				var arrContainerDetails = getContainerCubeAndTarWeight("",varEnteredSize);					
				if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

					ContainerCube =  parseFloat(arrContainerDetails[0]);						
					containerInternalId = arrContainerDetails[3];
					ContainerSize=arrContainerDetails[4];
					TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
					nlapiLogExecution('DEBUG', 'ContainerSize1', ContainerSize);	
				} 
				if(varEnteredSize=="" || varEnteredSize==null)
				{
					varEnteredSize=ContainerSize;
					nlapiLogExecution('DEBUG', 'ContainerSize2', ContainerSize);	
				}

				var varContainerCube;
				var varContainerTareWeight;
				var varContInventory;

				var filtersContiner = new Array(); 
				if(varEnteredSize!=null && varEnteredSize!='')
					filtersContiner.push(new nlobjSearchFilter('name', null, 'is', varEnteredSize));	

				var colsContainer = new Array();
				colsContainer[0]=new nlobjSearchColumn('custrecord_cubecontainer');  
				colsContainer[1]=new nlobjSearchColumn('custrecord_tareweight');  
				colsContainer[2]=new nlobjSearchColumn('custrecord_inventory_in_on');  

				var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersContiner, colsContainer);  
				var varcontintid;
				if(ContainerSearchResults!=null)
				{	
					varcontintid =  ContainerSearchResults[0].getId();
					varContainerCube = ContainerSearchResults[0].getValue('custrecord_cubecontainer');
					varContainerTareWeight = ContainerSearchResults[0].getValue('custrecord_tareweight');
					varContInventory = ContainerSearchResults[0].getValue('custrecord_inventory_in_on');				
				}
				/*else
			{
				SOarray["custparam_error"] = 'CARTON Not Matched';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Container Not Matched', varEnteredLP);
				return;
			}*/

				var fetchQty;
				var diffQty;
				var contlpno;
				var wmslocation;
				var cubeinfo1;
				var cubeinfo2;
				var weightinfo1;
				var weightinfo2;
				var opentaskwt;
				var opentasktotalcube;
				var varname;


				var getItemDetails  =  Item;
				//var getQtyDetails   = request.getParameter('hdnExpectedQuantity');
				var getQtyDetails   =ExpectedQuantity;
				nlapiLogExecution('DEBUG', 'getItemDetails::', getItemDetails);
				nlapiLogExecution('DEBUG', 'getQtyDetails::', getQtyDetails);

				var eackitem = new Array();
				var eachitemqty = new Array();
				/*nlapiLogExecution('DEBUG', 'vLoopCount::', vLoopCount);
			var vLoopCount =  request.getParameter('hdnloopcount');*/

				if(parseFloat(getresultscount)==1) 
				{
					eackitem[0]= getItemDetails;
					eachitemqty[0] = getQtyDetails;
				}
				else
				{		    		  
					eackitem= getItemDetails.split(',');
					eachitemqty = getQtyDetails.split(',');
				}
				nlapiLogExecution('DEBUG', 'eackitem length::', eackitem.length);

				// nlapiLogExecution('DEBUG', 'getQtyDetails::', getQtyDetails);

				var k=0;

				for(var i=0;i<eackitem.length;i++)
					// for(var i=0;i<getresultscount;i++)
				{									
					nlapiLogExecution('DEBUG', 'inside for loop', 'inside for loop');
					nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
					nlapiLogExecution('DEBUG', 'varhidcontlp', varhidcontlp);

					var iteminternalid=ItemInternalId;
					/*var OpenTaskitemFilters = new Array();	
	 	if(getOrderNo!=null && getOrderNo!='')	    	 	
	 	OpenTaskitemFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
	 	if(varhidcontlp!=null && varhidcontlp!='')	
	 		OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varhidcontlp));

	 //	OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof',[3]));



		  var OpenTaskitemColumns = new Array();
		    OpenTaskitemColumns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		    OpenTaskitemColumns[1] = new nlobjSearchColumn('custrecord_act_qty');

		    var OpenTaskitemSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskitemFilters, OpenTaskitemColumns);
		if(OpenTaskitemSearchResults!=null && OpenTaskitemSearchResults!='')
		{
			iteminternalid=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_sku_no');
			//getExpQty = OpenTaskitemSearchResults[i].getValue('custrecord_act_qty'); //Assign the Enter Qty here imp
		}*/
					getExpQty=eachitemqty[i];
					nlapiLogExecution('DEBUG', 'iteminternalid', iteminternalid);
					nlapiLogExecution('DEBUG', 'getExpQty', getExpQty);

					nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
					nlapiLogExecution('DEBUG', 'varhidcontlp', varhidcontlp);


//					var OpenTaskFilters = new Array();	                
//					OpenTaskFilters[0] = new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo);
//					OpenTaskFilters[1] = new nlobjSearchFilter('custrecord_container_lp_no', null,'is',varhidcontlp);
//					OpenTaskFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', iteminternalid);
//					OpenTaskFilters[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3); //3 stand for pick

					//OpenTaskFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', eackitem[i]);
					nlapiLogExecution('DEBUG','getOrderNo',getOrderNo);
					nlapiLogExecution('DEBUG','varhidcontlp',varhidcontlp);
					nlapiLogExecution('DEBUG','iteminternalid',iteminternalid);
					var SOFilters = new Array();
					if(getOrderNo!=null && getOrderNo!='')
						SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
					if(varhidcontlp!=null && varhidcontlp!='')
						SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varhidcontlp));

					SOFilters.push(new nlobjSearchFilter('custrecord_sku',null, 'anyof', iteminternalid));
					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
					//SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof', [8]));							


					nlapiLogExecution('DEBUG', 'eackitem[i]::', eackitem[i]);
					nlapiLogExecution('DEBUG', 'eachitemqty[i]::',eachitemqty[i]);

					var OpenTaskColumns = new Array();
					OpenTaskColumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
					OpenTaskColumns[1] = new nlobjSearchColumn('custrecord_container');
					OpenTaskColumns[2] = new nlobjSearchColumn('custrecord_totalcube');


					var OpenTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, OpenTaskColumns);

					if (OpenTaskSearchResults != null && OpenTaskSearchResults.length > 0) 
					{
						//var Recid= OpenTaskSearchResults[0].getId();
//						var Recid=request.getParameter('hdnRecordInternalId');
						nlapiLogExecution('DEBUG','opentaskRecid',Recid);
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);
						//getExpQty
						fetchQty =transaction.getFieldValue('custrecord_expe_qty');
						contlpno = transaction.getFieldValue('custrecord_container_lp_no');
						wmslocation = transaction.getFieldValue('custrecord_wms_location');
						opentaskwt	= transaction.getFieldValue('custrecord_total_weight');
						opentasktotalcube = transaction.getFieldValue('custrecord_totalcube');
						varname = transaction.getFieldValue('name');

						var skuname = transaction.getFieldValue('custrecord_sku');

						nlapiLogExecution('DEBUG', 'skuname::', skuname);

						nlapiLogExecution('DEBUG', 'getExpQty::', getExpQty);
						nlapiLogExecution('DEBUG', 'fetchQty::', fetchQty);
						nlapiLogExecution('DEBUG', 'contlpno::', contlpno);
						nlapiLogExecution('DEBUG', 'wmslocation::', wmslocation);

						if(parseFloat(fetchQty)>=parseFloat(getExpQty))
						{
							diffQty = parseFloat(fetchQty) - parseFloat(getExpQty);   
						}
						nlapiLogExecution('DEBUG', 'diffQty::', diffQty);

						cubeinfo1 =   (diffQty * parseFloat(opentasktotalcube)) / fetchQty;
						nlapiLogExecution('DEBUG', 'cubeinfo1::', cubeinfo1);


						weightinfo1 = (diffQty * parseFloat(opentaskwt)) / fetchQty;
						nlapiLogExecution('DEBUG', 'weightinfo1::', weightinfo1);
						nlapiLogExecution('DEBUG', 'varContainerTareWeight::', varContainerTareWeight);


						//varContainerCube
						transaction.setFieldValue('custrecord_act_qty',parseFloat(diffQty).toFixed(4));
						if(diffQty!=null&&diffQty!="")
							transaction.setFieldValue('custrecord_expe_qty',parseFloat(diffQty).toFixed(4));
						transaction.setFieldValue('custrecord_totalcube',parseFloat(cubeinfo1).toFixed(4));	
						transaction.setFieldValue('custrecord_total_weight',parseFloat(weightinfo1).toFixed(4));				    	
						transaction.setFieldValue('custrecord_wms_status_flag', '8');

						nlapiSubmitRecord(transaction, false, true);

						cubeinfo2 =   (getExpQty * parseFloat(opentasktotalcube)) / fetchQty;
						nlapiLogExecution('DEBUG', 'cubeinfo2::', cubeinfo2);


						weightinfo2 = (getExpQty * parseFloat(opentaskwt)) / fetchQty;
						nlapiLogExecution('DEBUG', 'weightinfo2::', weightinfo2);
						nlapiLogExecution('DEBUG', 'varContainerTareWeight::', varContainerTareWeight);

						var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	 //for Assign Station purpose
						UpdateoutboundInventory(ContainerLPNo,varEnteredLP);
						var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);		    	
						//createopentaskrec.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
						createopentaskrec.setFieldValue('custrecord_wms_status_flag', '28');//28 stands for packcomplete //Added on 24th
						createopentaskrec.setFieldValue('custrecord_act_end_date', DateStamp());
						createopentaskrec.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						createopentaskrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);
						createopentaskrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
						createopentaskrec.setFieldValue('custrecord_container', varcontintid);	
						createopentaskrec.setFieldValue('custrecord_act_qty', parseFloat(getExpQty).toFixed(4));	
						createopentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(getExpQty).toFixed(4));	
						createopentaskrec.setFieldValue('custrecord_from_lp_no', contlpno);	
						createopentaskrec.setFieldValue('custrecord_totalcube',parseFloat(cubeinfo2).toFixed(4));		
						createopentaskrec.setFieldValue('custrecord_total_weight',parseFloat(weightinfo2).toFixed(4));
						createopentaskrec.setFieldValue('name',varname);

						//createopentaskrec.setFieldValue('custrecord_total_weight',parseFloat(varContainerTareWeight)+parseFloat(weightinfo2));	

						nlapiSubmitRecord(createopentaskrec, false, true);
						nlapiLogExecution('DEBUG', 'Before Deletion of Record::', diffQty);

						if(diffQty<=0)
						{
							var OpenTaskDelFilters = new Array();	                		    		
							OpenTaskDelFilters[0] = new nlobjSearchFilter('custrecord_container_lp_no', null,'is',varhidcontlp);
							OpenTaskDelFilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 13); //13 stands for   STGM
							OpenTaskDelFilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', iteminternalid);

							var OpenTaskDelSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskDelFilters, null);

							if (OpenTaskDelSearchResults != null && OpenTaskDelSearchResults.length > 0) 
							{
								var STGMRecid= OpenTaskDelSearchResults[0].getId();
								var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', STGMRecid);
							}

							var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', Recid);		
							nlapiLogExecution('DEBUG', 'A Record is deleted from Open Task with following internal id Recid::',Recid);
						}				    					    					   		    		    
					}


					var LpTotalWt;
					var LpTotalVolume;

					LpTotalWt = parseFloat(varContainerTareWeight) + parseFloat(weightinfo2);
					LpTotalVolume = parseFloat(cubeinfo1) + parseFloat(cubeinfo2);

					if(k==0)
					{

						var mastlpcustomrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
						mastlpcustomrecord.setFieldValue('name', varEnteredLP);
						mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', varEnteredLP);
						mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 2);//2 stands for PICK
						if(varcontintid!=''&&varcontintid!=null)
							mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', varcontintid);
						mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght',parseFloat(LpTotalWt).toFixed(4));    
						//if(wmslocation!=''&&wmslocation!=null)
							//mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_site', wmslocation);  

						nlapiLogExecution('DEBUG', 'varContInventory--->', varContInventory);

						if(parseFloat(varContInventory) == 2) // in case of "ON".
						{
							nlapiLogExecution('DEBUG', 'varContInventory:::::', varContInventory);
							mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(LpTotalVolume).toFixed(4));
						}
						else  // in case of "IN" this is Default if no value is selected.
						{
							nlapiLogExecution('DEBUG', 'varContInventory:', varContInventory);
							mastlpcustomrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(varContainerCube).toFixed(4));
						}

						nlapiLogExecution('DEBUG', 'Before LP Record calling','Before LP Record calling');
						var rec = nlapiSubmitRecord(mastlpcustomrecord, false, true);
						nlapiLogExecution('DEBUG', 'After LP Record calling','Before LP Record calling');
						nlapiLogExecution('DEBUG','varEnteredLP',varEnteredLP);
						nlapiLogExecution('DEBUG','CurrentDate',CurrentDate);

						//Code Added for Assign Pack Station Functionality

						AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
						AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
						AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
						AssignPackStationrec.setFieldValue('custrecord_sku', null);
						AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
						AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);


						//custrecord_sku', null, 'anyof', iteminternalid);
						nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
						var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
						nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');

					}

					k++;

				}   	    		    			
				/*nlapiLogExecution('DEBUG', 'loop count', SOarray['custparam_loopcount']);
			nlapiLogExecution('DEBUG', 'line count', SOarray['custparam_linecount']);
			nlapiLogExecution('DEBUG', 'Order', getOrderNo);*/

				/*	//response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);

	var SOarray = new Array();
	SOarray["custparam_error"] = 'INVALID Order #';
	SOarray["custparam_screenno"] = '27A';
	SOarray["custparam_orderno"] = request.getParameter('hdnOrderNo');
	SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
	SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
	SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
	SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
	SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
	SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
	SOarray['custparam_linecount'] = '1';
	SOarray['custparam_loopcount'] ='1';

	nlapiLogExecution('DEBUG', 'Before Passing to item from close button', 'Before Passing to item from close button');
	//response.sendRedirect('SUITELET', 'customscript_rf_packing_item', 'customdeploy_rf_packing_item_di', false, SOarray);
	response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, optedEvent);
	nlapiLogExecution('DEBUG', 'After Passing to item from close button', 'After Passing to item from close button');*/

			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in multiselect',exp);
	}
}


function fnValidateEnteredContLP(EnteredContLP,SalesOrderInternalId,vsite)
{
	nlapiLogExecution('Debug', 'Into fnValidateEnteredContLP',EnteredContLP);	
	nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);	
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  
	if(vsite!=null && vsite!='')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vsite));  
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');


	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);


	if(salesOrderList == null || salesOrderList == '')
	{

		nlapiLogExecution('Debug', 'Into Closed Task',SalesOrderInternalId);
		var closedTskfilters =new Array();
		//closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_contlp_no', null, 'is', EnteredContLP));
		closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_contlp_no', null, 'is', EnteredContLP));		if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
			closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  
		if(vsite!=null && vsite!='')
			closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location', null, 'anyof', vsite));  
		var ClosedTaskcolumns = new Array();
		ClosedTaskcolumns[0] = new nlobjSearchColumn('name');

		salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, closedTskfilters, ClosedTaskcolumns);

	}
	if(salesOrderList == null || salesOrderList == '')
	{
		var filtersmlp = new Array();
		filtersmlp.push(new nlobjSearchFilter('name', null, 'is', EnteredContLP));
		if(vsite!=null&&vsite!="")
		filtersmlp.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', vsite));
		filtersmlp.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'anyof', 28));
		var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);
	}
	return salesOrderList;
}