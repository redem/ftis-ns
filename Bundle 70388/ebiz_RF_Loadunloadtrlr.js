/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_Loadunloadtrlr.js,v $
 *     	   $Revision: 1.2.4.2.4.1.4.7 $
 *     	   $Date: 2014/06/13 13:11:00 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Loadunloadtrlr.js,v $
 * Revision 1.2.4.2.4.1.4.7  2014/06/13 13:11:00  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.4.2.4.1.4.6  2014/06/12 15:30:41  skavuri
 * Case # 20148880 (LinkButton functionality added to options in RF Screens)
 *
 * Revision 1.2.4.2.4.1.4.5  2014/06/06 07:10:05  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.2.4.2.4.1.4.4  2014/05/30 00:41:01  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.2.4.1.4.3  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.4.2.4.1.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.2.4.1.4.1  2013/04/03 01:40:50  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.4.2.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.4.2  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.1  2012/02/22 12:48:09  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2  2011/09/28 13:23:13  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 *
 *****************************************************************************/

function LoadunloadMenu(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		var getLanguage = request.getParameter('custparam_language');	
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		if( getLanguage == 'es_ES')
		{
			st0 = "REMOLQUE CARGA / DESCARGA";
			st1 = "CARGANDO";
			st2 = "DESCARGA";
			st3 = "ENTRAR SELECCI&#211;N";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";		
		}
		else
		{
			st0 = "TRAILER LOADING / UNLOADING";
			st1 = "LOADING";
			st2 = "UNLOADING";
			st3 = "ENTER SELECTION";
			st4 = "SEND";
			st5 = "PREV";

		}
		//Case# 20148880 (LinkButton Functionality)
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_rf_loadtrlr', 'customdeploy_rf_loadtrlr_di');
		var linkURL_1 = checkInURL_1; 
		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di');
		var linkURL_2 = checkInURL_2;

		var functionkeyHtml=getFunctionkeyScript('_rfload'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		// html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfload' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1. " + st1;
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		// case# 201417189
		/*	html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 2. " + st2;
		//html = html + "				<td align = 'left'> 2. <a href='" + linkURL_2 + "' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				<td align = 'left'> 2. <a href='' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var optedField = request.getParameter('selectoption');
		var optedEvent = request.getParameter('cmdPrevious');
		var Shiparray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		//loadtrlrarray["custparam_language"] = getLanguage;


		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);
		}
		else 
			if (optedField == '1') {
				response.sendRedirect('SUITELET',  'customscript_rf_loadtrlr', 'customdeploy_rf_loadtrlr_di', false, optedField);
			}
			else 
				if (optedField == '2') {
					//response.sendRedirect('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di', false, optedField);
					response.sendRedirect('SUITELET', 'customscript_rf_loadunloadtrlr', 'customdeploy_rf_loadunloadtrlr_di', false, optedField);// case# 201416241
				}
				else
				{
					Shiparray["custparam_error"]="INVALID OPTION";
					Shiparray["custparam_screenno"] = '36A';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Shiparray);
				}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}