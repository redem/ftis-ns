/***************************************************************************
                                                                 
   eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_PickerPerform_Report_SL.js,v $
*  $Revision: 1.1.2.2 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_PickerPerform_Report_SL.js,v $
*  Revision 1.1.2.2  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/

function PickerPerformanceReport_SL(request,response)
{
	if (request.getMethod() == 'GET')
	{
		var form = nlapiCreateForm('Picker Performance Report');

		//form.setScript('customscript_ebiz_pickerclientvalidation');
		//QB Fields Declaration

		var userid = form.addField('custpage_userid', 'select', 'User','employee');		
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');	
		var tasktype = form.addField('custpage_tasktype', 'select', 'Task Type');
		tasktype.addSelectOption('', '');
		var todate = form.addField('custpage_todate', 'date', 'To Date');

		var filters = new Array();
		//filters[0] = new nlobjSearchFilter('custrecord_ebiz_process_direction', null, 'is', [3]);
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_task_desc');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, null, columns);
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {

			tasktype.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name') );
		}
		
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else
	{
		var form = nlapiCreateForm('Picker Performance Report');
		//form.setScript('customscript_ebiz_pickerclientvalidation');
		//QB Fields Declaration
		
		var vuserid = request.getParameter('custpage_userid');
		var vfromdate = request.getParameter('custpage_fromdate');
		var vtasktype = request.getParameter('custpage_tasktype');
		var vtodate = request.getParameter('custpage_todate');
		
		var userid = form.addField('custpage_userid', 'select', 'User','employee');
		userid.setDefaultValue(vuserid);
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
		fromdate.setDefaultValue(vfromdate);
		var tasktype = form.addField('custpage_tasktype', 'select', 'Task Type');
		tasktype.setDefaultValue(vtasktype);
		var todate = form.addField('custpage_todate', 'date', 'To Date');
		todate.setDefaultValue(vtodate);
		tasktype.addSelectOption('', '');
		
		nlapiLogExecution('ERROR', 'vuserid', vuserid);
		nlapiLogExecution('ERROR', 'vfromdate', vfromdate);
		nlapiLogExecution('ERROR', 'vtasktype', vtasktype);
		nlapiLogExecution('ERROR', 'vtodate', vtodate);
		
		var filters = new Array();	
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');	
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, null, columns);
		
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			tasktype.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name'));
		}	 
		form.addSubmitButton('Display');
		
		var  sublist = form.addSubList('custpage_pickerperformance','list','Picker Performance Report Details'); 
		 
		sublist.addField("custpage_slno", "text", "Sl#");
		sublist.addField('custpage_user','text', 'User');
		sublist.addField('custpage_sopo','text', 'SO/PO #');
		sublist.addField('custpage_tasktype','text', 'Task Type');
		var starttimefld=sublist.addField('custpage_starttime','text', 'Begin Time');
		var endtimefld=sublist.addField('custpage_endtime','text', 'End Time');
		sublist.addField('custpage_duration','text', 'Duration').setDisplayType('hidden');	
		starttimefld.setDisplaySize(20, 20); 
		endtimefld.setDisplaySize(20, 20); 
		
		
		 var Opensearchfilters = new Array(); 

		if ((request.getParameter('custpage_userid') != "" && request.getParameter('custpage_userid') != null)  ) {
			nlapiLogExecution('ERROR', 'vuserid1', vuserid);
			Opensearchfilters.push(new nlobjSearchFilter('custrecord_taskassignedto', null, 'anyof', vuserid));
		} 
		if ((request.getParameter('custpage_tasktype') != "" && request.getParameter('custpage_tasktype') != null)  ) {
			nlapiLogExecution('ERROR', 'vtasktype', vtasktype);
			Opensearchfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', vtasktype));
		} 
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null)  ) {
			nlapiLogExecution('ERROR', 'vfromdate', vfromdate);
			//Opensearchfilters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', vfromdate));
			Opensearchfilters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', vfromdate, vtodate));
		} 
//		if ((request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null)  ) {
//			
//			Opensearchfilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'within', vtodate));
//		} 
		
		var Opencolumns=new Array();
		Opencolumns.push(new nlobjSearchColumn('name'));
		Opencolumns.push(new nlobjSearchColumn('custrecord_taskassignedto'));
		Opencolumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
		Opencolumns.push(new nlobjSearchColumn('custrecord_tasktype'));
		Opencolumns.push(new nlobjSearchColumn('custrecordact_begin_date'));
		Opencolumns.push(new nlobjSearchColumn('custrecord_act_end_date'));
		Opencolumns.push(new nlobjSearchColumn('custrecord_actualbegintime'));
		Opencolumns.push(new nlobjSearchColumn('custrecord_actualendtime'));
				
		
		var OpenTasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Opensearchfilters, Opencolumns);
			
		
		var totalarray=new Array();
		if(OpenTasksearchresults != null && OpenTasksearchresults.length > 0){
			totalarray.push(OpenTasksearchresults);
			setPagingForSublist(totalarray,form);
		}
		
//		 var user='';
//		 var sopo='';
//		 var tasktype='';
//		 var fromdate='';
//		 var todate='';
//		 var duration='';
//		 
//		 if(OpenTasksearchresults != null && OpenTasksearchresults != "")
//		 { 
//			 nlapiLogExecution('ERROR', 'OpenTasksearchresults', OpenTasksearchresults.length);	
//			 for(var i=0;i<OpenTasksearchresults.length;i++)
//			 {
//				 user = OpenTasksearchresults[i].getText("custrecord_taskassignedto");
//				 sopo = OpenTasksearchresults[i].getText("custrecord_ebiz_order_no");
//				 tasktype = OpenTasksearchresults[i].getText("custrecord_tasktype");
//				 fromdate = OpenTasksearchresults[i].getText("custrecordact_begin_date");
//				 todate = OpenTasksearchresults[i].getText("custrecord_act_end_date");
//				// duration=OpenTasksearchresults[z].getValue("custrecordact_begin_date");
//				
//				 nlapiLogExecution('ERROR', 'user', user);
//				 nlapiLogExecution('ERROR', 'sopo', sopo);
//				 nlapiLogExecution('ERROR', 'tasktype', tasktype);
//				 nlapiLogExecution('ERROR', 'fromdate', fromdate);
//				 nlapiLogExecution('ERROR', 'todate', todate);
//				 
//				 
//				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_slno', i + 1, ""+(i + 1));
//				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_user', i + 1, user);
//				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_sopo', i + 1, sopo);
//				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_tasktype', i + 1, tasktype);
//				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_starttime', i + 1, fromdate);
//				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_endtime', i + 1, todate);
//				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_duration', i + 1, duration);			 
//			 }
//
//		 }	
//		

		response.writePage(form);
		}
	}



function setPagingForSublist(orderList,form)
{
	nlapiLogExecution('Error', 'Into setPagingForSublist');
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			//nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];
			nlapiLogExecution('Error', 'ordsearchresult ', ordsearchresult.length);
			if(ordsearchresult!=null)
			{
				//nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';

		if(orderListArray.length>0)
		{

			if(orderListArray.length>50)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("50");
					pagesizevalue=50;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue= 50;
						pagesize.setDefaultValue("50");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

							
				nlapiLogExecution('Error', 'request.getMethod()',request.getMethod());
				
				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}
			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				//nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				//nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{

					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						//nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						//nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						//nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}


			var c=0;
			var minvalue;
			minvalue=minval;
			var index=1;
			nlapiLogExecution('Error', 'maxval ', maxval);
			for(var j = minvalue; j < maxval; j++){		

				var currentOrder = orderListArray[j];				
				var begintime = currentOrder.getValue('custrecord_actualbegintime');
				var endtime =   currentOrder.getValue('custrecord_actualendtime');				 
				nlapiLogExecution('Error', 'begintime ', begintime);
				nlapiLogExecution('Error', 'endtime ', endtime);
				
				/*var vstarttime = new Date();
				vstarttime = begintime;
				var vendtime = new Date();
				vendtime = endtime;
				nlapiLogExecution('Error', 'vstarttime ', vstarttime);
				nlapiLogExecution('Error', 'vendtime', vendtime);*/
				var duration;
				//var duration = vendtime - vstarttime ;
				//duration = (vendtime.getTime()) - (begintime.getTime());
//				var seconds = duration / 1000;
//				var minutes = duration / 1000 / 60;
				//nlapiLogExecution('Error', 'duration', vendtime);
				
				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_slno', index, ""+(j + 1));
				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_user', index, currentOrder.getText('custrecord_taskassignedto'));
				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_sopo', index, currentOrder.getText('custrecord_ebiz_order_no'));
				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_tasktype', index, currentOrder.getText('custrecord_tasktype'));
				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_starttime', index, begintime);
				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_endtime', index, endtime);
				// form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_duration', index, duration + ":" + minutes + ":" + seconds);
				 form.getSubList('custpage_pickerperformance').setLineItemValue('custpage_duration', index, duration);
				
				index=index+1;
			}
		}
	}

	nlapiLogExecution('Error', 'Out of setPagingForSublist');
}


