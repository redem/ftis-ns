/***************************************************************************
	  		   eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_RMACheckin_SL.js,v $
 *     	   $Revision: 1.2.2.6.4.1.4.27.2.1 $
 *     	   $Date: 2015/09/21 11:32:55 $
 *     	   $Author: deepshikha $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * DESCRIPTION
 * REVISION HISTORY
 * $Log: ebiz_RMACheckin_SL.js,v $
 * Revision 1.2.2.6.4.1.4.27.2.1  2015/09/21 11:32:55  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.2.2.6.4.1.4.27  2015/06/11 13:33:56  schepuri
 * case# 201412982
 *
 * Revision 1.2.2.6.4.1.4.26  2015/04/13 09:26:10  skreddy
 * Case# 201412323
 * changed the url path which was hard coded
 *
 * Revision 1.2.2.6.4.1.4.25  2015/04/09 13:54:50  rrpulicherla
 * Case#201412277
 *
 * Revision 1.2.2.6.4.1.4.24  2014/10/29 13:50:00  vmandala
 * Case# 201410441 Stdbundle issue fixed
 *
 * Revision 1.2.2.6.4.1.4.23  2014/10/16 14:14:54  vmandala
 * Case# 201410664 std bundle issue fixed
 *
 * Revision 1.2.2.6.4.1.4.22  2014/10/16 13:57:34  vmandala
 * Case# 201410722 std bundle issue fixed
 *
 * Revision 1.2.2.6.4.1.4.21  2014/09/18 13:25:40  sponnaganti
 * Case# 201410422
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.6.4.1.4.20  2014/08/07 14:39:09  rmukkera
 * Case # 20149873
 *
 * Revision 1.2.2.6.4.1.4.19  2014/06/13 09:55:22  grao
 * Case#: 20148878 New GUI account issue fixes
 *
 * Revision 1.2.2.6.4.1.4.18  2014/06/12 14:27:43  grao
 * Case#: 20148778  New GUI account issue fixes
 *
 * Revision 1.2.2.6.4.1.4.17  2014/05/28 11:15:37  skavuri
 * Case# 20148580 SB Issue Fixed
 *
 * Revision 1.2.2.6.4.1.4.16  2014/05/23 15:39:31  skavuri
 * Case # 20148486 SB Issue Fixed
 *
 * Revision 1.2.2.6.4.1.4.15  2014/05/22 15:40:54  skavuri
 * Case # 20148484 SB Issue Fixed
 *
 * Revision 1.2.2.6.4.1.4.14  2014/05/08 14:52:52  sponnaganti
 * case# 20148326
 * (Put method and put rule updation in open task)
 *
 * Revision 1.2.2.6.4.1.4.13  2014/05/06 15:05:33  skavuri
 * Case# 20148241 SB Issue Fixed
 *
 * Revision 1.2.2.6.4.1.4.12  2014/03/05 15:13:38  sponnaganti
 * case# 20127496
 * (Location filter added)
 *
 * Revision 1.2.2.6.4.1.4.11  2014/02/21 14:57:46  rmukkera
 * Case# 20127203
 *
 * Revision 1.2.2.6.4.1.4.10  2014/02/14 15:29:38  skavuri
 * Case # 20127166 RMA chkn is working fine
 *
 * Revision 1.2.2.6.4.1.4.9  2014/01/17 14:28:32  rmukkera
 * Case # 20126838
 *
 * Revision 1.2.2.6.4.1.4.8  2013/12/27 15:38:25  rmukkera
 * Case # 20126576
 *
 * Revision 1.2.2.6.4.1.4.7  2013/10/24 12:29:16  nneelam
 * Case# 20124767
 * Putaway tasks moving to closed task..
 *
 * Revision 1.2.2.6.4.1.4.6  2013/08/30 15:23:25  nneelam
 * Case#.20124150
 * AutoCheck for Generate Locations.
 *
 * Revision 1.2.2.6.4.1.4.5  2013/06/14 15:42:08  nneelam
 * CASE201112/CR201113/LOG201121
 * RMA Related issue fixes
 *
 * Revision 1.2.2.6.4.1.4.4  2013/05/16 17:31:13  kavitha
 * CASE201112/CR201113/LOG201121
 * Lexjet production Issue fix
 *
 * Revision 1.2.2.6.4.1.4.3  2013/05/10 11:01:22  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.2.2.6.4.1.4.2  2013/04/16 13:15:26  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixes.
 *
 * Revision 1.2.2.6.4.1.4.1  2013/03/01 14:34:59  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.2.2.6.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.6  2012/04/25 15:46:55  spendyala
 * CASE201112/CR201113/LOG201121
 * While Showing Recommended qty POoverage is not considered.
 *
 * Revision 1.2.2.5  2012/04/20 12:59:08  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.2.2.4  2012/04/16 15:00:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Calculating POoverage is moved to general function.
 *
 * Revision 1.2.2.3  2012/04/13 22:21:51  spendyala
 * CASE201112/CR201113/LOG201121
 * Depending upon POoverage Value mentioned in the item master
 * value of poOverage will be calculated accordingly.
 *
 * Revision 1.2.2.2  2012/04/10 16:39:58  gkalla
 * CASE201112/CR201113/LOG201121
 * To fix the issue of While showing Pickface Quantity, it is not considering the Locaiton
 *
 * Revision 1.2.2.1  2012/02/24 13:05:22  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.2  2011/12/15 14:09:48  schepuri
 * CASE201112/CR201113/LOG201121
 * modified field names in customrecord_ebiznet_trn_poreceipt
 *
 * Revision 1.1  2011/09/22 08:36:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 *
 *****************************************************************************/
function getTranIDForPO(tranType, poID) {
	var tranID = null;
	var poRecord = nlapiLoadRecord(tranType, poID);
	if (poRecord != null)
		tranID = poRecord.getFieldValue('tranid');
	return tranID;
}


function addPOFieldAsHRef(form,request) {
	var poField = form.addField('custpage_po', 'select', 'Created From',
	'transaction');
	poField.setDefaultValue(request.getParameter('poID'));
	poField.setDisplayType('inline');
	poField.setLayoutType('startrow');
}

/*
 * Create a HREF for New PO Receipt. This will be next to the receipt drop down.
 */
function addPOReceiptURL(environment, poId, tranId, poLineNum) {
	var poReceiptURL = nlapiResolveURL('SUITELET','customscript_ebiznet_poreceipt', 'customdeploy_poreceipt');

	// Get the FQDN depending on the context environment
	/*var fqdn = getFQDNForHost(environment);

	var ctx = nlapiGetContext();

	if (fqdn == '')
		nlapiLogExecution('ERROR', 'Unable to retrieve FQDN based on context');
	else
		poReceiptURL = getFQDNForHost(ctx.getEnvironment()) + poReceiptURL;*/

	poReceiptURL = poReceiptURL + '&custparam_poid=' + poId;
	poReceiptURL = poReceiptURL + '&custparam_povalue=' + tranId;
	poReceiptURL = poReceiptURL + '&custparam_polinenum=' + poLineNum;

	return poReceiptURL;
}

/*
 * Add receipt field and fill data in drop down NOTE: This does not add the New
 * PO Receipt HREF
 */
function addReceiptFieldToForm(form,request) {
	var poRcptQtyArr = new Array;
	var selectionArr = new Array(); // temporary array to return rcptQty,
	// trailer, bol

	var receipt = form.addField('custpage_poreceipt', 'select', 'Receipt');

	var rcptFilters = new Array();
	rcptFilters[0] = new nlobjSearchFilter('custrecord_ebiz_poreceipt_pono', null, 'anyof', request.getParameter('poID'));
	rcptFilters[1] = new nlobjSearchFilter('custrecord_ebiz_poreceipt_lineno', null, 'is', request.getParameter('poLineNum'));

	var rcptColumns = new Array();
	rcptColumns[0] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptno');
	rcptColumns[1] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptqty');
	rcptColumns[2] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_trailerno');
	rcptColumns[3] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_bol');

	var rcptSearchResults = nlapiSearchRecord(
			'customrecord_ebiznet_trn_poreceipt', null, rcptFilters,
			rcptColumns);

	if (rcptSearchResults != null) {
		var resultCount = rcptSearchResults.length;
		nlapiLogExecution('ERROR', 'rcptSearchResults.length', resultCount);
		for ( var i = 0; i < Math.min(500, resultCount); i++) {
			receipt.addSelectOption(rcptSearchResults[i].getValue('custrecord_ebiz_poreceipt_receiptno'),
					rcptSearchResults[i].getValue('custrecord_ebiz_poreceipt_receiptno'));
			poRcptQtyArr[i] = rcptSearchResults[i].getValue('custrecord_ebiz_poreceipt_receiptqty');
			nlapiLogExecution('ERROR', 'PO Rcpt Qty', poRcptQtyArr[i]);
		}

		receipt.setDefaultValue(rcptSearchResults[resultCount - 1].getValue('custrecord_ebiz_poreceipt_receiptno'));
		selectionArr[0] = poRcptQtyArr[resultCount - 1];
		selectionArr[1] = rcptSearchResults[resultCount - 1].getValue('custrecord_ebiz_poreceipt_trailerno');
		selectionArr[2] = rcptSearchResults[resultCount - 1].getValue('custrecord_ebiz_poreceipt_bol');
	}

	return selectionArr;
}

/*
 * Get total check in quantity
 */
function getTotalCheckinQuantity(poID, poLineNum) {
	// Define search filter to get checkin qty for particular CHKN & PO
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is',
			poID);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', 1);// CHKN
	// task
	// type
	filters[2] = new nlobjSearchFilter('custrecord_line_no', null, 'is',
			poLineNum);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty');

	var searchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',
			null, filters, columns);
	var checkInQty = 0;

	if (searchResults != null) {
		for ( var i = 0; i < searchResults.length; i++) {
			var searchResult = searchResults[i];
			checkInQty = checkInQty
			+ parseFloat(searchResult.getValue('custrecord_expe_qty'));
		}
	}

	nlapiLogExecution('ERROR', 'CheckIn Qty', checkInQty);

	return checkInQty;
}

/*
 * Adding item sublist for checkin
 */
function addItemSublistToForm(form, itemID, poLineNum, poSerialNos, poRcptQty, 
		poItemStatus,poQtyChkn,poOverage,Packcode) {
	//code added on 15/04/13 by suman
	//This is to fetch the item type before loading the item from item master.
	
	var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
	var columns = nlapiLookupField('item', itemID, fields);
	var ItemType;
	var serialInflg="F";		
	var batchflag="F";
	var vExpDate,vBatchNo;

	if(columns != null && columns != '')
	{
		ItemType = columns.recordType;	
		serialInflg = columns.custitem_ebizserialin;
		batchflag= columns.custitem_ebizbatchlot;
	}
	nlapiLogExecution('ERROR', 'CheckIn itemType', ItemType);
	var itemRec = nlapiLoadRecord(ItemType, itemID);
	//end of code as of 15/04/13.
	
	var itemName = itemRec.getFieldValue('itemid');
	var itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
	nlapiLogExecution('ERROR', 'CheckIn itemName', itemName);
	nlapiLogExecution('ERROR', 'request.getParameter(poSerialNos)',
			poSerialNos);

	var itemSubList = form.addSubList("custpage_items", "inlineeditor",
	"ItemList");
	itemSubList.addField("custpage_chkn_pfvalidateflag", "text", "validateflag").setDisplayType('hidden');

	itemSubList.addField("custpage_chkn_line", "text", "Line #")
	.setDefaultValue(poLineNum);
	itemSubList.addField("custpage_chkn_sku", "text", "Item").setDisabled(true)
	.setDefaultValue(itemName);
	itemSubList.addField("custpage_chkn_itemdesc", "text", "Item Desc")
	.setDisabled(true).setDefaultValue(itemDesc);
	itemSubList.addField("custpage_chkn_skustatus", "select", "Item Status",
//			"customrecord_ebiznet_sku_status").setDefaultValue('1');
	"customrecord_ebiznet_sku_status").setDefaultValue(poItemStatus);
	//case 201410722 case start
	itemSubList.addField("custpage_chkn_pc", "text", "Pack Code")
	.setDefaultValue(Packcode);
	//case 201410722 case end
	itemSubList.addField("custpage_chkn_buom", "text", "Base UOM")
	.setDefaultValue('EACH');

	if (poRcptQty != "" && poRcptQty != null)
		itemSubList.addField("custpage_chkn_rcvngqty", "text", "Receive Qty")
		.setDefaultValue(poRcptQty);
	else
		itemSubList.addField("custpage_chkn_rcvngqty", "text", "Receive Qty")
		.setMandatory(true);

	itemSubList.addField("custpage_chkn_lp", "text", "LP(License Plate)")
	.setMandatory(true);
	// Case# 20148484
	//itemSubList.addField("custpage_lotbatch", "text", "LOT#");
	if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
	{
		itemSubList.addField("custpage_lotbatch", "text", "LOT#");;
	}
	else
	{
		itemSubList.addField("custpage_lotbatch", "text", "LOT#").setDisabled(true);
	}
	// Case# 20148484 ends
	itemSubList.addField("custpage_chkn_binlocation", "select", "Bin Location",
	"customrecord_ebiznet_location");

	itemSubList.addField("custpage_iteminternalid", "text", "Item Id")
	.setDisplayType('hidden').setDefaultValue(itemID);
	itemSubList.addField("custpage_serialno", "text", "Item SerialNo")
	.setDisplayType('hidden').setDefaultValue(poSerialNos);
	nlapiLogExecution('ERROR', 'pooverage',	poOverage);
	itemSubList.addField("custpage_pooverage", "text", "PoOverage")
	.setDisplayType('hidden').setDefaultValue(poOverage);

	nlapiLogExecution('ERROR', 'poRcptQty',	parseFloat(poRcptQty));
	nlapiLogExecution('ERROR', 'poOverage',	poOverage);

	itemSubList.addField("custpage_chkn_qty", "text", "CheckIn Qty").setDisplayType('hidden');


}

function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', 3);

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function checkPOOverage(POId,location,company){
	nlapiLogExecution('ERROR','PO Internal Id', POId);
	var poOverage = 0;
	var receiptType;
	if(POId != null && POId != '')
	{
		var poFields = ['custbody_nswmsporeceipttype'];
		var poColumns = nlapiLookupField('transaction', POId, poFields);
		
		if(poColumns != null && poColumns != '')
		{
		receiptType = poColumns.custbody_nswmsporeceipttype;
		nlapiLogExecution('ERROR','here', receiptType);

		}
	}

	
	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		nlapiLogExecution('ERROR','here', receiptType);
		if(receiptColumns != null && receiptColumns != '')
		{
		poOverage = receiptColumns.custrecord_po_overages;
		nlapiLogExecution('ERROR','Out of check PO Overage', poOverage);
		}
	}
	
	return poOverage;
}

/*
 * Process HTTP Get Request
 */
function RMAcheckInGETRequest(request, response) {
	// create the Suitelet form
	var form = nlapiCreateForm('RMA Check-In');

	var ctx = nlapiGetContext();

	/*
	 * Registering client script This script is registered against a client
	 * script. This is invoked whenever the 'ADD' button is selected against any
	 * order line
	 */
	form.setScript('customscript_chknlpvalidation');

	var poID = request.getParameter('poID');
	var poLineNum = request.getParameter('poLineNum');
	var poLineQty = request.getParameter('poLineQty');
	var poQtyChkn = request.getParameter('QtyChkn');
	var poQtyRem = request.getParameter('QtyRem');
	var itemID = request.getParameter('poLineItem');
	var poSerialNos = request.getParameter('poSerialNos');
	var poItemStatus = request.getParameter('poItemStatus');	
	//case 201410722 case start
	var Packcode = request.getParameter('Packcode');
	//case 201410722 case end

	nlapiLogExecution('ERROR', 'quantityRcv', poQtyChkn);
	nlapiLogExecution('ERROR', 'RemQty', poQtyRem);
	nlapiLogExecution('ERROR', 'poID', poID);
	
	var transactionFilters = new Array();
	transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poID);
	transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', itemID);
	transactionFilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', poLineNum);


	var transactionColumns = new Array();
	transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
	transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	transactionColumns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	transactionColumns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');
	transactionColumns[4] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');

	var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);
	
	if(transactionSearchresults != null && transactionSearchresults != "")
	{
		for(var h=0;h<transactionSearchresults.length;h++)
		{
			//var vordQty = transactionSearchresults[h].getValue('custrecord_orderlinedetails_order_qty');
			//Case 201410664 start
			//var vordQty = transactionSearchresults[h].getValue('custrecord_orderlinedetails_order_qty');
			var vordQty = request.getParameter('poLineQty');
			//Case 201410664 end
			var vCheckQty = transactionSearchresults[h].getValue('custrecord_orderlinedetails_checkin_qty');
			poQtyRem=parseFloat(vordQty) - parseFloat(vCheckQty);
		}	
	}	

	// To get PO location and recordType
	var fields = [ 'recordType', 'location', 'custbody_nswms_company' ];
	var columns = nlapiLookupField('transaction', poID, fields);
	var tranType = columns.recordType;
	var location = columns.location;
	var company = columns.custbody_nswms_company;

	var genLocnCheckBox = form.addField('custpage_genloccheck', 'checkbox',
	'Generate Locations');
	//case# 20124150 start
	genLocnCheckBox.setDefaultValue('T');
	//case# 20124150 end

	nlapiLogExecution('ERROR', 'PO Id from Client', poID);

	// Getting Tran Id for this particular id and sending to PO receipt screen.
	var tranID = getTranIDForPO(tranType, poID);
	nlapiLogExecution('ERROR', 'tranID', tranID);

	// add the PO field and make it a hyperlink
	addPOFieldAsHRef(form,request);

	// Added for Location select field
	var poLocation = form.addField('custpage_loc_id', 'select', 'Location',
	'location');
	poLocation.setDefaultValue(location);

	// Adding the receipt dropdown and the new PO receipt URL text
	// poOtherAttributesArr consists of 3 records i.e. poRcptQty, trailerNo, bol
	var poOtherAttributesArr = addReceiptFieldToForm(form,request);
	nlapiLogExecution('ERROR', 'after tranID poOtherAttributesArr[0]', poOtherAttributesArr[0]);
	var poRcptURL = addPOReceiptURL(ctx.getEnvironment(), poID, tranID,
			poLineNum);

	// add the quantity field, make it editable
	var itemQuanField = form.addField('custpage_item_quan', 'integer',
			'Quantity', null);
	itemQuanField.setDefaultValue(poLineQty);
	itemQuanField.setDisplayType('inline');

	// Add the PO Value field and make it hidden
	var poValue = request.getParameter('poValue');
	var poValueField = form.addField('custpage_povalue', 'text', 'Tran Value');
	poValueField.setDefaultValue(poValue);
	poValueField.setDisplayType('hidden');
	//case # 20127166 starts
	// PO internalId
	var poIDpost=form.addField('custpage_poidpost','text','poIDpost').setVisible(false);
	poIDpost.setDefaultValue(poID);
	// case # 20127166 ends
	// Item
	var item = form.addField('custpage_line_item', 'select', 'Item', 'item');
	item.setDefaultValue(itemID);
	item.setDisplayType('inline');

	// Line No.
	var lineNo = form.addField('custpage_line_itemlineno', 'integer', 'Line No');
	lineNo.setDefaultValue(poLineNum);
	lineNo.setDisplayType('inline');

	// Trailer
	var trailer = poOtherAttributesArr[1];

	var trailerNo = form.addField('custpage_trailerno', 'select', 'Trailer',
	'customrecord_ebiznet_trailer');
	if (trailer != null && trailer != "") {
		trailerNo.setDefaultValue(trailer);
		trailerNo.setDisplayType('inline');
	}

	// New PO Receipt URL
	var createNewReqLink = form.addField('custpage_porecipt_link',
			'inlinehtml', null, null, null);
	createNewReqLink.setDefaultValue('<B><A HREF="#" onclick="javascript:window.open('
			+ "'" + poRcptURL + "'" + ');">New RMA Receipt</A></B>');

	// add the remaining quantity field, make it editable
	var remainingItemQtyField = form.addField('custpage_remainitem_quan',
			'integer', 'Remaining Quantity', null).setDisabled(true);
	//remainingItemQtyField.setDefaultValue(request.getParameter('custparam_remainingqty'));
	remainingItemQtyField.setDefaultValue(poQtyRem);

	// add the Check-In Quantity field, make it editable
	var checkinQtyField = form.addField('custpage_checkin_quan', 'integer',
			'Check-In Quantity', null);
	checkinQtyField.setDisabled(true);
	//checkinQtyField.setDefaultValue(getTotalCheckinQuantity(poID, poLineNum));
	checkinQtyField.setDefaultValue(poQtyChkn);

	var transactionTypeField = form.addField('custpage_trantype', 'text',
	'Trantype');
	transactionTypeField.setDefaultValue(tranType);
	transactionTypeField.setDisplayType('hidden');

	// BOL
	var bol = poOtherAttributesArr[2];
	var bolNoField = form.addField('custpage_bolno', 'text', 'BOL');
	if (bol != null && bol != "") {
		bolNoField.setDefaultValue(bol);
		bolNoField.setDisplayType('inline');
	}

	// Company
	var chknCompany = form.addField('custpage_company', 'select', 'Company',
	'customrecord_ebiznet_company');
	chknCompany.setDefaultValue(company);
	chknCompany.setDisplayType('inline');

	// add the Priority Putaway Quantity field
	nlapiLogExecution('ERROR', 'location', location);
	//Case # 20127203 Start
	var priorityPutawayLocnArr = priorityPutaway(itemID, poLineQty,location,poItemStatus);
	//Case # 20127203 End
	var priorityQty = priorityPutawayLocnArr[1];

	var priorityQtyField = form.addField('custpage_priority_quan', 'integer',
			'Pick Face Recommended Quantity', null);
	priorityQtyField.setDisabled(true);
	priorityQtyField.setDefaultValue(priorityQty);

	// add the Recommended Quantity field//shylaja


	var ItemRecommendedQuantity =0;
	//Case # 20126838 Start
	var poOverage=0;//GetPoOverage(itemID,poID,location);
	//Case # 20126838 End		
	//var poOverage = checkPOOverage(getPOInternalId,getWHLocation, null);
	nlapiLogExecution('ERROR','poOverage', poOverage);

	var palletQuantity = fetchPalletQuantity(itemID,location,null);
	if (poOverage == null || poOverage == ""){
		poOverage = 0;	
	}
	//else{
//	ItemRecommendedQuantity = Math.min (parseFloat(poQtyRem) + parseFloat((poOverage * poLineQty)/100), palletQuantity);
	//Case # 20126576 Start
	var hdnItemRecmdQtyWithPOoverage=0;
	hdnItemRecmdQtyWithPOoverage = Math.min (parseFloat(poQtyRem) + parseFloat((poOverage * poLineQty)/100), palletQuantity);
	if(hdnItemRecmdQtyWithPOoverage==null||hdnItemRecmdQtyWithPOoverage==""||hdnItemRecmdQtyWithPOoverage.toString()=="NaN")
	{
		hdnItemRecmdQtyWithPOoverage = 0;
	}
//	ItemRecommendedQuantity = Math.min (parseFloat(poQtyRem), palletQuantity);
	ItemRecommendedQuantity = Math.min (parseFloat(hdnItemRecmdQtyWithPOoverage), palletQuantity);
	//}
	//Case # 20126576 Start
	nlapiLogExecution('ERROR', 'RecommendedQtyField poLineQty', poLineQty);
	nlapiLogExecution('ERROR', 'RecommendedQtyField poOverage', poOverage);
	nlapiLogExecution('ERROR', 'RecommendedQtyField ItemRecommendedQuantity', parseFloat(poLineQty) + parseFloat((poOverage * poLineQty)/100));
	nlapiLogExecution('ERROR', 'RecommendedQtyField least ItemRecommendedQuantity', ItemRecommendedQuantity.toString());
	var RecommendedQtyField = form.addField('custpage_recommended_quan', 'float',
			'Recommended Quantity', null);
	RecommendedQtyField.setDisabled(true);
	RecommendedQtyField.setDefaultValue(ItemRecommendedQuantity.toString());
	
	var ItemRecmdQtyWithPOoverageField = form.addField('custpage_itemrecmd_qty_withpooverage', 'float',
			'Recommended Quantity with poOverage', null);
	ItemRecmdQtyWithPOoverageField.setDisplayType('hidden');
	/*if(hdnItemRecmdQtyWithPOoverage==null||hdnItemRecmdQtyWithPOoverage==""||hdnItemRecmdQtyWithPOoverage.toString()=="NaN")
	{
		hdnItemRecmdQtyWithPOoverage = 0;
	}*/
	nlapiLogExecution('ERROR', 'RecommendedQtyField least ItemRecommendedQuantity', hdnItemRecmdQtyWithPOoverage.toString());
	ItemRecmdQtyWithPOoverageField.setDefaultValue(hdnItemRecmdQtyWithPOoverage.toString());

	// adding item sublist to the form
	addItemSublistToForm(form, itemID, poLineNum, poSerialNos,
			poOtherAttributesArr[0], poItemStatus,poQtyChkn,poOverage,Packcode);

	form.addSubmitButton('Submit');
	form.addPageLink('crosslink', 'Back to PO Check-In', nlapiResolveURL(
			'RECORD', tranType, poID));
	response.writePage(form);
}

/*
 * Get Stage Locn
 */
/*function getStageLocn() {
	var stageLocn = '';
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', null,
			'anyof', [ 29 ]);
	//var locnSearch = nlapiSearchRecord('customrecord_ebiznet_location', null,filters);
	var locnSearch = getStagingLocation();
	if (locnSearch != null)
		stageLocn = locnSearch[0].getId();

	return stageLocn;
}*/


/*
 * Get dock location
 */
/*function getDockLocn() {
	var dockLocn = '';
	var dockLocnFilter = new Array();
	dockLocnFilter[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid',
			null, 'anyof', [ 30 ]);
	//var dockLocnSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockLocnFilter);
	var dockLocnSearchResults = getDockLocation();

	if (dockLocnSearchResults != null)
		dockLocn = dockLocnSearchResults[0].getId();

	return dockLocn;
}*/

/*
 * Getting location attribute
 */
function getLocationAttribute(locnDimArray, attrName){
	var retVal;

	if(attrName == "LOCN_NAME"){
		if(locnDimArray[0] != "")
			retVal = locnDimArray[0];
		else
			retVal = "";
	}else if(attrName == "REMAINING_CUBE"){
		if (!isNaN(locnDimArray[1])) {
			retVal = locnDimArray[1];
		} else {
			retVal = 0;
		}
	}else if(attrName == "LOCN_ID"){
		if (!isNaN(locnDimArray[2])) {
			retVal = locnDimArray[2];
		} else {
			retVal = "";
		}
	}else if(attrName == "OB_LOCN_ID"){
		if (!isNaN(locnDimArray[3])) {
			retVal = locnDimArray[3];
		} else {
			retVal = "";
		}
	} else if (attrName == "OB_LOCN_ID") {
		if (!isNaN(locnDimArray[4])) {
			retVal = locnDimArray[4];
		} else {
			retVal = "";
		}
	} else if (attrName == "IB_LOCN_ID") {
		if (!isNaN(locnDimArray[5])) {
			retVal = locnDimArray[5];
		} else {
			retVal = "";
		}
	} else if (attrName == "PUT_SEQ") {
		if (!isNaN(locnDimArray[6])) {
			retVal = locnDimArray[6];
		} else {
			retVal = "";
		}
	} else if (attrName == "LOCN_REMAINING_CUBE") {
		if (!isNaN(locnDimArray[7])) {
			retVal = locnDimArray[7];
		} else {
			retVal = "";
		}
	}

	return retVal;
}
/*
 * Processing HTTP POST method
 */
function checkInPOSTRequest(request, response) {
	// this is the POST block
	/* To fetch Stage(Out) location */
	var stageLocn = '';
	var dockLocn = '';
	//csae# 20127496 starts
	var t1 = new Date();
	//stageLocn = getStagingLocation();
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'StageLocn Search', getElapsedTimeDuration(t1, t2));

	t1 = new Date();
	dockLocn = getDockLocation();
	t2 = new Date();
	nlapiLogExecution('ERROR', 'DockLocn Search', getElapsedTimeDuration(t1, t2));

	/*
	 * Qc Validation. We check QC flag in Item Master and if it exists bring the
	 * INB-Loc from QC validations record and create CHKN and PUTW records with
	 * status flag as 'N' and redirect to QC validation suitelet.
	 */

	// extract the values from the previous page
	var qcID = request.getParameter('custpage_line_item');
	var qcLineNo = request.getParameter('custpage_line_itemlineno');
	var fields = [ 'recordType', 'custitem_ebizqualitycheck' ];
	var columns = nlapiLookupField('item', qcID, fields);
	var lgItemType = columns.recordType;
	var qcValidate = columns.custitem_ebizqualitycheck;
	var company = request.getParameter('custpage_company');
	var po = request.getParameter('custpage_po');
	var poLocn = request.getParameter('custpage_loc_id');
	var poValue = request.getParameter('custpage_povalue');
	var quantity = request.getParameter('custpage_item_quan');
	var lineNo = request.getParameter('custpage_line_itemlineno');
	var remainingQty = request.getParameter('custpage_remainitem_quan');
	var lineCount = request.getLineItemCount('custpage_items');
	var poRcptNo = request.getParameter('custpage_poreceipt');
	var transactionType = request.getParameter('custpage_trantype');
	var genLocnCheckVal = request.getParameter('custpage_genloccheck');
	nlapiLogExecution('ERROR', 'Generate Location Selection', genLocnCheckVal);
	nlapiLogExecution('ERROR', 'transactionType', transactionType);
	nlapiLogExecution('ERROR', 'poLocn', poLocn);
	stageLocn = getStagingLocation(poLocn);
	//case# 20127496 end
	var checkinLineQty = 0;
	var taskType = "";
	var wmsStatusFlag = "";
	var beginLocn = "";
	var endLocn = "";
	var chkAssignputW="";
	var chkQty=0;
	var vReturnProcess="";

	var vitemId = "";
	var vitemStatusinfo="";

	//Checking the Qty by opening mutiple browsers
	nlapiLogExecution('ERROR', 'lineCount1', lineCount);
	for ( var k = 1; k <= lineCount; k++) {

		chkQty = parseFloat(chkQty) + parseFloat(request.getLineItemValue('custpage_items','custpage_chkn_rcvngqty', k));
		vitemId = request.getLineItemValue('custpage_items','custpage_iteminternalid', k);
	}
	nlapiLogExecution('ERROR', 'po', po);
	//Added for checking qty purpose Added by ramana
	var posearchresults = nlapiLoadRecord('returnauthorization', po); //1020
	nlapiLogExecution('ERROR', 'posearchresults', posearchresults);
	var vponum= posearchresults.getFieldValue('tranid');		
	nlapiLogExecution('ERROR', 'vponum', vponum);
	//alert(vponum);
	var Qtyfilters = new Array();          
	Qtyfilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'is',vponum);
	Qtyfilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto',lineNo);
	Qtyfilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_item', null, 'is',vitemId);

	// Qtyfilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'equalTo',vponum);
	// Qtyfilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalTo',lineNo);

	var Qtycolumns = new Array();
	Qtycolumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');

	var searchQtyresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details',null, Qtyfilters, Qtycolumns);
	var PutwQty = 0;


	if(searchQtyresults!=null)
	{    						  			    	
		PutwQty = searchQtyresults[0].getValue('custrecord_orderlinedetails_checkin_qty');  
	}    				    
	nlapiLogExecution('ERROR', 'vponum', vponum);
	nlapiLogExecution('ERROR', 'lineNo', lineNo);	
	nlapiLogExecution('ERROR', 'PutwQty', PutwQty);				    
	nlapiLogExecution('ERROR', 'chkQty', chkQty);

	var totalchkQty = parseFloat(PutwQty) + parseFloat(chkQty);

	nlapiLogExecution('ERROR', 'totalchkQty', totalchkQty);		
	nlapiLogExecution('ERROR', 'quantity', quantity);		

	//if(parseFloat(PutwQty)>=parseFloat(chkQty))
	/*
	if(parseFloat(totalchkQty)>parseFloat(quantity))
	{			 
		nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
		var form = nlapiCreateForm('Check in');
		nlapiLogExecution('ERROR', 'Form Called', 'form');					  
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'PO Overage is not allowed. Change the quantity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
		nlapiLogExecution('ERROR', 'message', msg);					  
		response.writePage(form);
		nlapiLogExecution('ERROR', 'Write Page to form', 'Written');		
		return;					
	}*/  
	//}

	/*if(vReturnProcess=="CHECK")
		{
			nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
			var form = nlapiCreateForm('Check in');
			nlapiLogExecution('ERROR', 'Form Called', 'form');					  
			var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'PO Overage is not allowed. Change the quantity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
			nlapiLogExecution('ERROR', 'message', msg);					  
			response.writePage(form);
			nlapiLogExecution('ERROR', 'Write Page to form', 'Written');		
			return;	
		}*/
	//upto to here


	//upto to here




	/*
	 * Logic
	 * If QC Validation is not set for the item,
	 *		For all lines
	 * 			Create a CHKIN task in TRN_OPENTASK with begin locn as Stage Locn and End locn as Dock Locn
	 * 			Move record to EBIZTASK
	 * 			If LP has been specified, Create an LP record
	 * 			If Bin Location has been specified,
	 * 				Create a PUTW task in TRN_OPENTASK
	 * 			Else if Bin Location is not specified
	 * 				If user has selected 'Generate Location', then
	 * 					If priority putaway needs to be done,
	 * 						Get priority putaway locations
	 * 						Create PUTW task at priority putaway location
	 * 					End If
	 * 				End If
	 * 			End If
	 * 		End For
	 * End If
	 */

	if (qcValidate == 'F') {
		for ( var s = 1; s <= lineCount; s++) {
			// Retreiving line item details
			var itemName = request.getLineItemValue('custpage_items','custpage_chkn_sku', s);
			var pfqtyvalidateflag = request.getLineItemValue('custpage_items','custpage_chkn_pfvalidateflag', s);
			var itemStatus = request.getLineItemValue('custpage_items','custpage_chkn_skustatus', s);
			var packCode = request.getLineItemValue('custpage_items','custpage_chkn_pc', s);
			var baseUOM = request.getLineItemValue('custpage_items','custpage_chkn_buom', s);
			var lp = request.getLineItemValue('custpage_items','custpage_chkn_lp', s);
			var binLocn = request.getLineItemValue('custpage_items','custpage_chkn_binlocation', s);
			var receiveQty = request.getLineItemValue('custpage_items','custpage_chkn_rcvngqty', s);
			var itemDesc = request.getLineItemValue('custpage_items','custpage_chkn_itemdesc', s);
			var itemId = request.getLineItemValue('custpage_items','custpage_iteminternalid', s);
			var lotBatchNo = request.getLineItemValue('custpage_items','custpage_lotbatch', s);
			var lotWithQty = lotBatchNo + "(" + receiveQty + ")";

			chkQty = receiveQty;

			vitemStatusinfo = itemStatus;



			// Suming the QTY to Send to Trn Tables.
			checkinLineQty = parseFloat(checkinLineQty) + parseFloat(receiveQty);

			// Creating CHKN record even if BIN Location is null or not.
			// Create CHKN task in trn_opentask
			// tasktype = 1(CHKN) and wmsStatusFlag = 1(INBOUND/CHECK-IN)
			// beginLocn = dockLocn and endLocn = stageLocn
			taskType = "1";
			wmsStatusFlag = "1";
			beginLocn = dockLocn;
			endLocn = stageLocn;

			var chknRecID = createRecordLocationNULL(poValue, itemId, quantity, receiveQty, lp, lineNo, 
					packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag,
					qcID, itemDesc, lotBatchNo, poLocn, lgItemType, lotWithQty, company);

			// Move task from TRN_OPENTASK to EBIZTASK
			MoveTaskRecord(chknRecID);

			// Create LP Record
			if (lp != "")
				createLPRecord(lp);

			if (binLocn != null){
				taskType = "2"; // PUTW
				wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
				beginLocn = binLocn;
				endLocn = "";

				// Create PUTW task
				var retPutRecId = createRecordLocationNULL(poValue, itemId, quantity, receiveQty, lp, lineNo, packCode, itemStatus,
						baseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn,
						lgItemType, lotWithQty, company);
			}else{
				if (genLocnCheckVal == 'T') {					
					// Calling Priority putaway function to get priority putaway location
					nlapiLogExecution('ERROR', 'Test', 'Test');
					nlapiLogExecution('ERROR', 'poLocn', poLocn);
					var priorityPutawayLocnArr = priorityPutaway(itemId, receiveQty,poLocn,itemStatus);
					var priorityRemainingQty = priorityPutawayLocnArr[0];
					var priorityQty = priorityPutawayLocnArr[1];
					var priorityLocnID = priorityPutawayLocnArr[2];
					var zoneid = priorityPutawayLocnArr[3];

					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityRemainingQty', priorityRemainingQty);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityQty', priorityQty);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityLocnID', priorityLocnID);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityZoneID', zoneid);

					// If quantity can be putaway'ed on priority, create PUTW task in TRN_OPENTASK
					// set taskType = 2(PUTW) and wmsStatusFlag = 2(INBOUND/LOCATIONS ASSIGNED)
					// beginLocation = priorityLocnID and endLocn = ""
					if(pfqtyvalidateflag != "Y")
					{
						if (priorityQty != 0){
							taskType = "2"; // PUTW
							wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
							beginLocn = priorityLocnID;
							endLocn = "";
							//var systemlp = GetMaxLPNo(1, 1);
							var retPutRecId = CreatePUTWwithLocation(poValue, itemId, quantity, priorityQty, lp, lineNo,
									packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo,
									wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn, lgItemType, lotWithQty, company,zoneid);
						}
					}
					else
					{
						priorityRemainingQty = receiveQty;
					}
					nlapiLogExecution('ERROR', 'priorityRemainingQty', priorityRemainingQty);
					// Check if there is any remaining quantity to be putaway
					if (parseFloat(priorityRemainingQty) != 0) {
						// Item dimensions
						var arrDims = new Array();
						arrDims = getSKUCubeAndWeight(itemId, 1);
						nlapiLogExecution('ERROR', 'arrDims.length', arrDims.length);
						var itemCube = 0;
						if (arrDims.length > 0 && (!isNaN(arrDims[0]))) //BaseUOMItemCube
						{
							var uomqty = ((parseFloat(priorityRemainingQty))/(parseFloat(arrDims[1])));	//BaseUOMQty		
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));//BaseUOMItemCube
							//itemCube = (parseFloat(arrDims[0]) * parseFloat(priorityRemainingQty));
							nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
						} else {

							itemCube = 0;
							nlapiLogExecution('ERROR', 'elsepart:itemCube', itemCube);
						}

						 //case 201410441 start
						//var LoctDims = GetPutawayLocation(itemId, null, itemStatus, null, itemCube, lgItemType,poLocn);
						
						var filters2 = new Array();

						var columns2 = new Array();
						columns2[0] = new nlobjSearchColumn('custitem_item_family');
						columns2[1] = new nlobjSearchColumn('custitem_item_group');
						columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
						columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
						columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
						columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
						columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');
						filters2.push(new nlobjSearchFilter('internalid', null, 'is', itemId));
						var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

						var filters3=new Array();
						filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						var columns3=new Array();
						columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
						columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
						columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
						columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
						columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
						columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
						columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
						columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
						columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
						columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
						columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
						columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
						columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
						columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
						columns3[14] = new nlobjSearchColumn('formulanumeric');
						columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
						columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
						//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
						columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
						// Upto here on 29OCT2013 - Case # 20124515
						columns3[14].setSort();

						var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);

						var LoctDims =   GetPutawayLocationNew(itemId, packCode, itemStatus, baseUOM, parseFloat(itemCube),lgItemType,poLocn,ItemInfoResults,putrulesearchresults,priorityLocnID); // case# 201412982
						   //case 201410441 end
						var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube,Putrule,Putmethod;
						if (LoctDims.length > 0) {
							LocName = getLocationAttribute(LoctDims, "LOCN_NAME");
							RemCube = getLocationAttribute(LoctDims, "REMAINING_CUBE");
							LocId = getLocationAttribute(LoctDims, "LOCN_ID");
							OubLocId = getLocationAttribute(LoctDims, "OB_LOCN_ID");
							PickSeq = getLocationAttribute(LoctDims, "PICK_SEQ");
							InbLocId = getLocationAttribute(LoctDims, "IB_LOCN_ID");
							PutSeq = getLocationAttribute(LoctDims, "PUT_SEQ");
							LocRemCube = getLocationAttribute(LoctDims, "LOCN_REMAINING_CUBE");
							zoneid=LoctDims[8];
							Putmethod=LoctDims[9];
							Putrule=LoctDims[10];
						} else {
							LocName = "";
							LocId = "";
							RemCube = "";
							InbLocId = "";
							OubLocId = "";
							PickSeq = "";
							PutSeq = "";
							zoneid="";
						}

						chkAssignputW  = "Y";
						// Creating Putaway record in TRN_OPENTASK
						taskType = "2"; // PUTW
						wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
						beginLocn = LocId; // Identified location
						endLocn = "";						
						var putwrecordid = CreatePUTWwithLocation(poValue, itemId, quantity, priorityRemainingQty, lp, lineNo,
								packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo,
								wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn, lgItemType, lotWithQty, company,itemCube, zoneid,Putmethod,Putrule);
						if (putwrecordid != "") {
							nlapiLogExecution('ERROR', 'binLocn', binLocn);
							nlapiLogExecution('ERROR', 'RemCube', RemCube);
							if (binLocn == ""  && RemCube >= 0) {
								var retValue = UpdateLocCube(LocId, RemCube);

							}
							// Case#20148241 starts
							nlapiLogExecution('ERROR', ' LocName', LocName);
							nlapiLogExecution('ERROR', 'LocId', LocId);
							if (LocId != ""  && RemCube >= 0) {
								var retValue = UpdateLocCube(LocId, RemCube);
							}
							// Case#20148241 ends
						}
					}// /If priroty remaining QTY is Zero
				} else {
					// create PUTW task in TRN_OPENTASK
					taskType = "2"; // PUTW
					wmsStatusFlag = "6"; // INBOUND/NO LOCATIONS ASSIGNED
					beginLocn = "";
					endLocn = "";
					var retPutRecId = createRecordLocationNULL(poValue, itemId, quantity, receiveQty, lp, lineNo, packCode,
							itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag, qcID, itemDesc,
							lotBatchNo, poLocn, lgItemType, lotWithQty, company);
				}
			}
		}// end of For loop lines count.
	} else if (qcValidate == 'T') {
		nlapiLogExecution('ERROR', 'IF QC validate flag is True', qcValidate);
		nlapiLogExecution('ERROR', 'IF QC validate flag is lineCnt ', lineCount);

		for ( var s = 1; s <= lineCount; s++) {

			// Retreiving line item details
			var ItemName = request.getLineItemValue('custpage_items', 'custpage_chkn_sku', s);
			var ItemStatus = request.getLineItemValue('custpage_items', 'custpage_chkn_skustatus', s);
			var PackCode = request.getLineItemValue('custpage_items', 'custpage_chkn_pc', s);
			var BaseUOM = request.getLineItemValue('custpage_items', 'custpage_chkn_buom', s);
			var LP = request.getLineItemValue('custpage_items', 'custpage_chkn_lp', s);
			var BinLoc = request.getLineItemValue('custpage_items', 'custpage_chkn_binlocation', s);
			var RcvQty = request.getLineItemValue('custpage_items', 'custpage_chkn_rcvngqty', s);
			var ItemDesc = request.getLineItemValue('custpage_items', 'custpage_chkn_itemdesc', s);
			var ItemId = request.getLineItemValue('custpage_items', 'custpage_iteminternalid', s);
			var LotBatch = request.getLineItemValue('custpage_items', 'custpage_lotbatch', s);
			var lotwithqty = LotBatch + "(" + RcvQty + ")";

			vitemStatusinfo = ItemStatus;

			// Suming the QTY to Send to Trn Tables.
			checkinLineQty = parseFloat(checkinLineQty) + parseFloat(RcvQty);

			// Create CHKN task in TRN_OPENTASK
			// taskType = 1(CHKN) and wmsStatusFlag = 1(INBOUND/CHECK-IN)
			// beginLocn = dockLocn and endLocn = stageLocn
			var QcChknRecId = createRecordLocationNULL(poValue, ItemId,
					quantity, RcvQty, LP, lineNo, PackCode, ItemStatus,
					BaseUOM, "1", dockLocn, stageLocn, po, poRcptNo,
					"1", qcID, ItemDesc, LotBatch, poLocn,
					lgItemType, lotwithqty, company);

			// move task to EBIZTASK
			MoveTaskRecord(QcChknRecId);

			// Create LP Record
			if (LP != "") {
				createLPRecord(LP);
			}

			// Create PUTW task in TRN_OPENTASK
			// taskType = 2(PUTW) and wmsStatusFlag = 23(INBOUND/QUALITY CHECK)
			// beginLocn = "" and endLocn = ""
			taskType = "2"; // PUTW
			wmsStatusFlag = "23"; // INBOUND/QUALITY CHECK
			beginLocn = "";
			endLocn = "";
			var QcChknRecId = createRecordLocationNULL(poValue, ItemId, quantity, RcvQty, LP, lineNo, PackCode, ItemStatus,
					BaseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag, qcID, ItemDesc, LotBatch, poLocn,
					lgItemType, lotwithqty, company);
		}

	}
	nlapiLogExecution('ERROR', 'transactionType', transactionType);
	var type = transactionType;// "purchaseorder";
	var putGenQty = 0;
	var putConfQty = 0;
	var ttype = "CHKN";
	var confQty = checkinLineQty;

	nlapiLogExecution('ERROR', 'checkInPOSTRequest:quantity', quantity);
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:confQty', confQty);
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:poValue', poValue);

	nlapiLogExecution('ERROR', 'chkAssignputW Value::', chkAssignputW);

	var tranValue = TrnLineUpdation(type, ttype, poValue, po, qcLineNo, qcID,
			request.getParameter('custpage_item_quan'), confQty,chkAssignputW,vitemStatusinfo);

	var poArray = new Array();
	poArray["custparam_po"] = po;
	poArray["custparam_povalue"] = poValue;






	// Redirecting to QC Validation screen if QC Validate is TRUE
	if (qcValidate == 'T') {
		nlapiLogExecution('ERROR', 'Navigating TO QC Screen');
		poArray["custparam_poqctemid"] = qcID;
		poArray["custparam_poqctemlineno"] = qcLineNo;
		poArray["custparam_company"] = company;

		response.sendRedirect('SUITELET', 'customscript_qc_list',
				'customdeploy_qc_list', false, poArray);
	} else {
		// redirect to the record in view mode(customscript_assignputaway)

		nlapiLogExecution('ERROR', 'Navigating', 'To Assign putaway');
		response.sendRedirect('SUITELET', 'customscript_rma_putawayreport_sl',
				'customdeploy_rma_putawayreport_di', false, poArray);
		var context = nlapiGetContext();
		var usageRemaining = context.getRemainingUsage();
		nlapiLogExecution('ERROR', 'usageRemaining', usageRemaining);
	}








}

/*
 * Main function to process PO Check-in
 */
function RMAcheckInConceptSuitelet(request, response) {
	// obtain the context object
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') {
		// Creating the form for check-in

		RMAcheckInGETRequest(request, response);
	} else {
		checkInPOSTRequest(request, response);
	}// End of else block.
}

/*
 * Create CHKN and PUTW Records .
 */
function createRecordLocationNULL(poValue, itemID, quantity, receiveQty, lp,
		lineNo, packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn,
		po, poReceiptNo, wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn,
		lgItemType, lotWithQty, vCompany) {
	var timeStamp1 = new Date();

	var t3 = new Date();
	var customRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	var t4 = new Date();
	nlapiLogExecution('ERROR', 'createRecordLocationNULL:nlapiCreateRecord|trn_opentask',
			getElapsedTimeDuration(t4, t3));

	// populating the fields
	customRecord.setFieldValue('name', poValue);
	customRecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
	customRecord.setFieldValue('custrecord_ebiz_sku_no', itemID);
	customRecord.setFieldValue('custrecord_act_qty', parseFloat(quantity).toFixed(5));
	customRecord.setFieldValue('custrecord_expe_qty', parseFloat(receiveQty).toFixed(5));
	customRecord.setFieldValue('custrecord_lpno', lp);
	customRecord.setFieldValue('custrecord_ebiz_lpno', lp);
	customRecord.setFieldValue('custrecord_line_no', lineNo);
	customRecord.setFieldValue('custrecord_packcode', packCode);
	customRecord.setFieldValue('custrecord_sku_status', itemStatus);
	customRecord.setFieldValue('custrecord_uom_id', baseUOM);
	customRecord.setFieldValue('custrecord_tasktype', taskType);
	customRecord.setFieldValue('custrecord_actbeginloc', beginLocn);

	// taskType = 1 (CHKN)
	if (taskType == "1") {
		customRecord.setFieldValue('custrecord_actbeginloc', beginLocn);
		customRecord.setFieldValue('custrecord_actendloc', endLocn);
		customRecord.setFieldValue('custrecordact_begin_date', DateStamp());
		customRecord.setFieldValue('custrecord_act_end_date', DateStamp());

		// Adding fields to update time zones.
		customRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
		customRecord.setFieldValue('custrecord_actualendtime', TimeStamp());
		customRecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customRecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
	}

	customRecord.setFieldValue('custrecord_current_date', DateStamp());
	customRecord.setFieldValue('custrecord_upd_date', DateStamp());
	customRecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
	customRecord.setFieldValue('custrecord_ebiz_receipt_no', poReceiptNo);

	// Status flag .
	customRecord.setFieldValue('custrecord_wms_status_flag', wmsStatusFlag);

	// Added for Item name and desc
	customRecord.setFieldValue('custrecord_sku', qcID);
	customRecord.setFieldValue('custrecord_skudesc', itemDesc);

	// LotBatch and batch with Quantity.
	if (lgItemType == "lotnumberedinventoryitem") {
		customRecord.setFieldValue('custrecord_batch_no', lotBatchNo);
		customRecord.setFieldValue('custrecord_lotnowithquantity', lotWithQty);
	}

	customRecord.setFieldValue('custrecord_wms_location', poLocn);
	// Case# 20148486 starts
	var columns = new Array();
	columns.push(new nlobjSearchColumn('name'));
	columns.push(new nlobjSearchColumn('custrecord_ebizfifodate'));
	columns.push(new nlobjSearchColumn('custrecord_ebizexpirydate'));
	var filters= new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'is',poLocn));
	if(lotBatchNo!='' && lotBatchNo!='null' && lotBatchNo!=null)// Case# 20148580
	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is',lotBatchNo));
	var result= nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filters,columns);
	var fifodate='',expdate='';
	if(result!=null && result !='null' && result!='' )
		{
		fifodate=result[0].getValue('custrecord_ebizfifodate');
		expdate=result[0].getValue('custrecord_ebizexpirydate');
		}
	nlapiLogExecution('DEBUG', 'fifodate',fifodate);	
	nlapiLogExecution('DEBUG', 'expdate',expdate);	
	//customRecord.setFieldValue('custrecord_fifodate', DateStamp());
	//case# 201410422 (update only for lot item)
	if (lgItemType == "lotnumberedinventoryitem")
	{
		customRecord.setFieldValue('custrecord_fifodate', fifodate);
		customRecord.setFieldValue('custrecord_expirydate', expdate);// Case# 20148486
	}
	// Case# 20148486 ends
	customRecord.setFieldValue('custrecord_comp_id', vCompany);

	var currentUserID = getCurrentUser();

	customRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
	customRecord.setFieldValue('custrecord_ebizuser', currentUserID);
	customRecord.setFieldValue('custrecord_taskassignedto', currentUserID);
	
	//case 20124767 start
	if(po!=null&&po!="")
		customRecord.setFieldValue('custrecord_ebiz_order_no',po);
    //case end
	
	// commit the record to NetSuite
	t3 = new Date();
	var recordId = nlapiSubmitRecord(customRecord);
	t4 = new Date();
	nlapiLogExecution('ERROR', 'createRecordLocationNULL:nlapiSubmitRecord|trn_opentask',
			getElapsedTimeDuration(t4, t3));

	nlapiLogExecution('ERROR', 'Check-In Record Insertion', 'Success');

	var invtRecordId = createInvtRecord(taskType, po, endLocn, lp, itemID,
			itemStatus, itemDesc, receiveQty, poLocn, lgItemType, packCode,
			vCompany,fifodate,expdate);// Case# 20148486

	var timeStamp2 = new Date();
	nlapiLogExecution('ERROR', 'Elapsed time for createRecordLocationNULL',
			getElapsedTimeDuration(timeStamp1, timeStamp2));
	return recordId;
}

function createInvtRecord(taskType, po, endLocn, lp, itemId, itemStatus, itemDesc,
		receiveQty, poLocn, lgItemType, packCode, vCompany,fifodate,expdate) {// Case# 20148486
	// Creating inventory for taskType = 1 (CHKN)
	if (taskType == "1") {
		try {
			// Creating Inventory Record.
			var t1 = new Date();
			var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
			var t2 = new Date();
			nlapiLogExecution('ERROR', 'createInvtRecord:nlapiCreateRecord|createinv',
					getElapsedTimeDuration(t2, t1)); 


			nlapiLogExecution('ERROR', 'Location',poLocn);                
			var filtersAccNo = new Array();
			// filtersAccNo[0] = new nlobjSearchFilter('custrecord_siteid', null, 'is', 'New Jersey');
			filtersAccNo[0] = new nlobjSearchFilter('custrecord_location', null, 'is', poLocn);
			var colsAcc = new Array();
			colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');  
			var varAccountNo=1;
			var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);   
			if(Accsearchresults!=null ){
				varAccountNo = Accsearchresults[0].getValue('custrecord_accountno');                                
				nlapiLogExecution('ERROR', 'Account ',varAccountNo );
			}
			


			invtRec.setFieldValue('name', po);
			invtRec.setFieldValue('custrecord_ebiz_inv_binloc', endLocn);
			invtRec.setFieldValue('custrecord_ebiz_inv_lp', lp);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemId);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
			invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packCode);
			invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(receiveQty).toFixed(5));

			invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemId);

			invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(receiveQty).toFixed(5));
			invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemDesc);
			invtRec.setFieldValue('custrecord_ebiz_inv_loc', poLocn);

			invtRec.setFieldValue('custrecord_invttasktype', 1); // CHKN..
			invtRec.setFieldValue('custrecord_wms_inv_status_flag', 1); // CHKN
			//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', 1);
		//	invtRec.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);
			//

			// Case# 20148486 starts
			//case# 201410422 (update only for lot item)
			if (lgItemType == "lotnumberedinventoryitem")
			{
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
				invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
			}
			// Case# 20148486 ends
			invtRec.setFieldValue('customrecord_ebiznet_location', endLocn); //Added by ramana on 30th may

			invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
			//

			nlapiLogExecution('ERROR', 'Location Information', endLocn);

			invtRec.setFieldValue('custrecord_ebiz_inv_company', vCompany);

			t1 = new Date();
			var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
			t2 = new Date();
			nlapiLogExecution('ERROR', 'createInvtRecord:nlapiSubmitRecord:createinv',
					getElapsedTimeDuration(t2, t1));

			if (invtRecordId != null) {
				nlapiLogExecution('ERROR', 'Inventory Record Creation', invtRecordId);
			} else {
				nlapiLogExecution('ERROR', 'Inventory Record Creation', 'Failed');
			}
		} catch (error) {
			nlapiLogExecution('ERROR', 'Check for error msg in create invt', error);
		}
	}
}

function createLPRecord(lp) {
	var timestamp1 = new Date();
	var t1 = new Date();
	var lpRecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord:nlapiCreateRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	lpRecord.setFieldValue('name', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);
	t1 = new Date();
	var recid = nlapiSubmitRecord(lpRecord);
	t2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord:nlapiSubmitRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	var timestamp2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord Duration',
			getElapsedTimeDuration(timestamp1, timestamp2));
}
//case# 20127496 starts (Location filter added)
function getStagingLocation(RoleLocation){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	// ACTIVE RECORDS ONLY
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));
	
	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));
	stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', 'null', 'anyof', RoleLocation));
	// Results for inbound staging location
	var Columns = new Array();
	Columns[0]=new nlobjSearchColumn('custrecord_locgrouptype', 'custrecord_inboundlocgroupid');
	
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters,Columns);

	// This search could return more than one inbound staging location.  We are using the first one.
	var id="";
	for(var s = 0; stageLocnResults!=null && s < stageLocnResults.length ; s++)
	{
		var locgrptype = stageLocnResults[s].getText('custrecord_locgrouptype','custrecord_inboundlocgroupid');
		nlapiLogExecution('ERROR', 'locgrptype', locgrptype);
		if(locgrptype =='INBOUND')
		{
			nlapiLogExecution('ERROR', 'test1', locgrptype);
			retInboundStagingLocn = stageLocnResults[0].getId();
		}

		if(locgrptype=='BOTH')
		{
			nlapiLogExecution('ERROR', 'test2', locgrptype);
			id = stageLocnResults[0].getId();
		}
	}

	if(retInboundStagingLocn==null || retInboundStagingLocn=="")
		retInboundStagingLocn=id;

	return retInboundStagingLocn;
}
//case# 20127496 end
function getDockLocation(){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}




function CreatePUTWwithLocation(poValue, ItemId, quan, PriortyQty, LP, lineno,
		PackCode, ItemStatus, BaseUOM, tasktype, beginloc, endloc, po,
		poreceiptno, wmsStsflag, QcID, ItemDesc, LotBatch, poloc, lgItemType,
		lotwithqty, VCompany,itemCube, zoneid,Putmethod,Putrule) {
	var t1 = new Date();
	var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'CreatePUTWwithLocation:nlapiCreateRecord|trn_opentask',
			getElapsedTimeDuration(t2, t1));

	nlapiLogExecution('ERROR', 'Creating PUTW record with Location',
	'TRN_OPENTASK');
	// populating the fields
	putwrecord.setFieldValue('name', poValue);
	putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
	putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
	putwrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(5));
	putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(PriortyQty).toFixed(5));
	putwrecord.setFieldValue('custrecord_lpno', LP);
	putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
	putwrecord.setFieldValue('custrecord_line_no', lineno);
	putwrecord.setFieldValue('custrecord_packcode', PackCode);
	putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
	putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
	putwrecord.setFieldValue('custrecord_tasktype', tasktype);
	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();
	putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
	putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
	putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
	putwrecord.setFieldValue('custrecord_actbeginloc', beginloc);
	putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
	putwrecord.setFieldValue('custrecord_ebizzone_no', zoneid);
	//case# 20148326 starts (Put method and put rule updation in open task)
	putwrecord.setFieldValue('custrecord_ebizmethod_no', Putmethod);
	putwrecord.setFieldValue('custrecord_ebizrule_no', Putrule);
	//case# 20148326 end
	//Addfor total cube	
	putwrecord.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(5));
	//
	nlapiLogExecution('ERROR', 'Total Cube in Checkin', parseFloat(itemCube).toFixed(5));

	// For eBiznet Receipt Number .
	putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

	// month+1 / date / year
	putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

	// Adding fields to update time zones.
	// hour:min:am/pm
	putwrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
	putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());

	// month+1 / date / year
	putwrecord.setFieldValue('custrecord_current_date', DateStamp());
	putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

	// Status flag .
	putwrecord.setFieldValue('custrecord_wms_status_flag', wmsStsflag);

	// Added for Item name and desc
	putwrecord.setFieldValue('custrecord_sku', ItemId);
	putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

	// LotBatch and batch with Quantity.
	if (lgItemType == "lotnumberedinventoryitem") {
		nlapiLogExecution('ERROR', 'lotnumberedinventoryitem PUTW record',
		'LOT-->TRN_OPENTASK');
		putwrecord.setFieldValue('custrecord_batch_no', LotBatch);
		putwrecord.setFieldValue('custrecord_lotnowithquantity', lotwithqty);
	}
	putwrecord.setFieldValue('custrecord_wms_location', poloc);
// Case# 20148486 starts
	var columns = new Array();
	columns.push(new nlobjSearchColumn('name'));
	columns.push(new nlobjSearchColumn('custrecord_ebizfifodate'));
	columns.push(new nlobjSearchColumn('custrecord_ebizexpirydate'));
	var filters= new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'is',poloc));
	if(LotBatch!='' && LotBatch!='null' && LotBatch!=null)// Case# 20148580
	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is',LotBatch));
	var result= nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filters,columns);
	var fifodate='',expdate='';
	if(result != null && result !='null'  && result !='')
		{
		fifodate=result[0].getValue('custrecord_ebizfifodate');
		expdate=result[0].getValue('custrecord_ebizexpirydate');
		}
	nlapiLogExecution('DEBUG', 'fifodate',fifodate);	
	nlapiLogExecution('DEBUG', 'expdate',expdate);
	//case# 201410422 (update only for lot item)
	if (lgItemType == "lotnumberedinventoryitem")
	{
		putwrecord.setFieldValue('custrecord_fifodate', fifodate);
		putwrecord.setFieldValue('custrecord_expirydate', expdate);
	}
	// Case# 20148486 ends
	putwrecord.setFieldValue('custrecord_comp_id', VCompany);
	
	//case 20124767 start
	if(po!=null&&po!="")
		putwrecord.setFieldValue('custrecord_ebiz_order_no',po);
	//case end

	// commit the record to NetSuite
	t2 = new Date();
	var putwrecordid = nlapiSubmitRecord(putwrecord);
	t1 = new Date();
	nlapiLogExecution('ERROR', 'CreatePUTWwithLocation:nlapiSubmitRecord|trn_opentask',
			getElapsedTimeDuration(t2, t1));
}
