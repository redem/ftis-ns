/***************************************************************************
 eBizNET Solutions Inc              
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_PutawayLocation.js,v $
 *     	   $Revision: 1.1.2.1.4.9.2.1 $
 *     	   $Date: 2014/08/04 08:51:29 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PutawayLocation.js,v $
 * Revision 1.1.2.1.4.9.2.1  2014/08/04 08:51:29  nneelam
 * case#  20149371
 * Stanadard Bundle Partial WO Issue Fix.
 *
 * Revision 1.1.2.1.4.9  2014/06/13 08:45:15  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.1.4.8  2014/05/30 00:34:25  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.4.7  2014/04/17 15:52:33  skavuri
 * Case # 20148055 SB issue fixed
 *
 * Revision 1.1.2.1.4.6  2014/02/14 06:23:57  skreddy
 * case 20127124
 * Nucourse SB issue fix
 *
 * Revision 1.1.2.1.4.5  2014/02/05 15:06:02  sponnaganti
 * case# 20127056
 * For Keyboard Enter button to work
 *
 * Revision 1.1.2.1.4.4  2013/06/14 07:05:45  gkalla
 * CASE201112/CR201113/LOG201121
 * PCT Case# 20122979. To select item status while RF WO confirm build
 *
 * Revision 1.1.2.1.4.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.4.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.1.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.1  2012/11/23 09:40:04  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 * Revision 1.10.2.18.4.5  2012/10/11 10:27:04  gkalla

 * 
 *****************************************************************************/
function PutawayLocation(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR A LA UBICACI&#211;N:";
			st2 = "INGRESAR / ESCANEO DE LA UBICACI&#211;N";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "";
			st6 = "";

		}
		else
		{
			st1 = "ASSEMBLY ITEM  ";
			st2 = "ENTER/SCAN LOCATION ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "BUILD QTY ";
			st6 = "WO# ";
			st7 = "LOT#";
		}


		var getWONo = request.getParameter('custparam_woname');
		var getWOItem = request.getParameter('custparam_item');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getWOInternalId = request.getParameter('custparam_woid');
		var getWOQtyEntered = request.getParameter('custparam_expectedquantity');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getnoofrecords =request.getParameter('custparam_noofrecords');
		var getwoStatus =request.getParameter('custparam_wostatus');
		var getWOLinePackCode = request.getParameter('custparam_packcode');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var getexpdate = request.getParameter('custparam_expdate');
		var getmfgdate=request.getParameter('custparam_mfgdate');
		var getbestbeforedate=request.getParameter('custparam_bestbeforedate');
		var getfifodate=request.getParameter('custparam_fifodate');
		var getlastdate=request.getParameter('custparam_lastdate');
		var getbatchno=request.getParameter('custparam_batchno');
		var getItemType = request.getParameter('custparam_itemtype');
		var getFifoCode = request.getParameter('custparam_fifocode');

		nlapiLogExecution('Error', 'getWONo', getWONo);
		nlapiLogExecution('Error', 'getFetchedItemId', getFetchedItemId);
		nlapiLogExecution('Error', 'getItemType', getItemType);



		/*var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchNo = request.getParameter('custparam_batchno');	
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');

		nlapiLogExecution('Error', 'before pickType in loc get', pickType);

		var OrdNo=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var vSOId=request.getParameter('custparam_ebizordno');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('Error', 'getItemInternalId', getItemInternalId);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		nlapiLogExecution('Error', 'before getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Error', 'before getItemInternalId', getItemInternalId);
		if(getFetchedLocation==null || getFetchedLocation == "")
		{
			getFetchedLocation=NextLocation;
		}

		if(getItemInternalId==null || getItemInternalId == "")
		{
			getItemInternalId=NextItemInternalId;
		}

		nlapiLogExecution('Error', 'after getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Error', 'after getItemInternalId', getItemInternalId);
		nlapiLogExecution('Error', 'getBeginLocation ID', getFetchedBeginLocation);
		nlapiLogExecution('Error', 'getBeginLocation', getFetchedLocation);

		nlapiLogExecution('Error', 'NextItemInternalId', NextItemInternalId); */
		//case# 20127056 starts (Now form name is passed correctly in function to work keyboard enter button)
		var functionkeyHtml=getFunctionkeyScript('_rf_putawayloc'); 
		//case# 21012 end
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putawayloc' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 + ":  <label>" + getWONo + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";		

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +" :  <label>" + getWOItem + "</label></td>";
		//html = html + "				<input type='hidden' name='hdntype' value=" + Type + ">";
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getWOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnWOStatus' value=" + getwoStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnWOItemId' value=" + getFetchedItemId + "></td>";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getWOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnnoofrecords' value=" + getnoofrecords + ">";
		html = html + "				<input type='hidden' name='hdnWOName' value=" + getWONo + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnwoqtyenetered' value=" + getWOQtyEntered + ">";
		html = html + "				<input type='hidden' name='hdnBatchno' value=" + getbatchno + ">";
		html = html + "				<input type='hidden' name='hdnexpdate' value=" + getexpdate + ">";
		html = html + "				<input type='hidden' name='hdnmfgdate' value=" + getmfgdate + ">";
		html = html + "				<input type='hidden' name='hdnbestbeforedate' value=" + getbestbeforedate + ">";
		html = html + "				<input type='hidden' name='hdnlastdate' value=" + getlastdate + ">";
		html = html + "				<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				<input type='hidden' name='hdnFifoCode' value=" + getFifoCode + ">";
		html = html + "				<input type='hidden' name='hdnFifodate' value=" + getfifodate + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";

		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + ":  <label>" + getWOQtyEntered + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";

		if(getbatchno != null && getbatchno!= '')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st7 + ":  <label>" + getbatchno + "</label></td>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2;//ENTER/SCAN LOCATION 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					" + st5 + " <input name='cmdOverride' type='submit' value='F11'/>";
		//html = html + "					" + st6 + " <input name='cmdSKIP' type='submit' value='F12'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		var getLanguage = request.getParameter('hdngetLanguage');

		var st7;

		if( getLanguage == 'es_ES')
		{
			st7 = "UBICACI&#211;N NO V&#193;LIDA";

		}
		else
		{
			st7 = "INVALID LOCATION";
		}


		var getBeginLocation='';
		var WOarray = new Array();
		var WOid=request.getParameter('hdnWOid');

		var ItemType = request.getParameter('hdnItemType');

		nlapiLogExecution('Error', 'Into Response', 'Into Response');

		nlapiLogExecution('ERROR', 'WOid', WOid);
		nlapiLogExecution('ERROR', 'ItemType', ItemType);

		var RecCount;


		var getEnteredLocation = request.getParameter('enterlocation');		
		nlapiLogExecution('Error', 'Entered Location', getEnteredLocation);	
		nlapiLogExecution('Error', 'Location', getBeginLocation);	
		nlapiLogExecution('Error', 'Name',  WOarray["name"]);	


		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		WOarray["custparam_woid"] = request.getParameter('custparam_woid');
		WOarray["custparam_item"] = request.getParameter('custparam_item');
		WOarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		WOarray["custparam_expectedquantity"] = request.getParameter('hdnwoqtyenetered');
		WOarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		WOarray["custparam_noofrecords"] = request.getParameter('hdnnoofrecords');
		WOarray["custparam_woname"] = request.getParameter('hdnWOName');

		WOarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		WOarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
		WOarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
		WOarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
		WOarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
		WOarray["custparam_wostatus"] = request.getParameter('hdnWOStatus');
		WOarray["custparam_packcode"] = request.getParameter('hdnItemPackCode');
		WOarray["custparam_language"] = request.getParameter('hdngetLanguage');
		WOarray["custparam_itemtype"] = request.getParameter('hdnItemType');

		WOarray["custparam_expdate"] = request.getParameter('hdnexpdate');
		WOarray["custparam_mfgdate"] = request.getParameter('hdnmfgdate');
		WOarray["custparam_bestbeforedate"] = request.getParameter('hdnbestbeforedate');
		WOarray["custparam_fifodate"] = request.getParameter('hdnFifodate');
		WOarray["custparam_lastdate"]= request.getParameter('hdnlastdate');
		WOarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');
		WOarray["custparam_batchno"] = request.getParameter('hdnBatchno');



		WOarray["custparam_error"] = st7;//'INVALID LOCATION';
		WOarray["custparam_screenno"] = 'WOPutawayGenLoc';

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {	
			if (ItemType == "lotnumbereditem")
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_batchno', 'customdeploy_ebiz_rf_wo_putaway_batch_di', false, WOarray);	
			}
			else
			{
				//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_gen', 'customdeploy_ebiz_rf_wo_putaway_gen_di', false, WOarray);
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_qty', 'customdeploy_ebiz_rf_wo_putaway_qty_di', false, WOarray);
			}
		}
		else {
			//To Remove case sensitive
			if (getEnteredLocation != '' && getEnteredLocation != null) {
				var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getEnteredLocation));


				if (LocationSearch != '' && LocationSearch != null) {
					nlapiLogExecution('Error', 'Length of Location Search', LocationSearch.length);
					var Loctdims=new Array();
					for (var s = 0; s < LocationSearch.length; s++) {
						var getEndLocationInternalId = LocationSearch[s].getId();
						nlapiLogExecution('DEBUG', 'End Location Id', getEndLocationInternalId);
					}
					nlapiLogExecution('DEBUG', 'WOarray["custparam_item"]', WOarray["custparam_item"]);
					nlapiLogExecution('DEBUG', 'WOarray["custparam_expectedquantity"] ', WOarray["custparam_expectedquantity"] );
					nlapiLogExecution('DEBUG', 'WOarray["custparam_fetcheditemid"]', WOarray["custparam_fetcheditemid"]);
					//case 20127123 to check binlocation remaining cube
					var vqty = WOarray["custparam_expectedquantity"];
					var arrdims = fnGetSkucubeandweight(WOarray["custparam_fetcheditemid"], 1);

					var ItemCube = 0;
					if (arrdims !=null && arrdims !='' && arrdims.length > 0) 
					{
						if (!isNaN(arrdims[0])) {
							nlapiLogExecution('ERROR', 'arrdims[0]', arrdims[0]);
							ItemCube = (parseFloat(arrdims[0]) * parseFloat(vqty));
							nlapiLogExecution('ERROR', 'ItemCube', ItemCube);
						}
						else {
							ItemCube = 0;
						}
					}
					else {
						ItemCube = 0;
					}

					var binlocid=getEndLocationInternalId;
					Loctdims = getLocationDetails(binlocid,WOarray["custparam_whlocation"],ItemCube);
					nlapiLogExecution('ERROR', 'Loctdims', Loctdims);
					if (Loctdims != null && Loctdims != '' && Loctdims.length > 0) 
					{

						WOarray["custparam_beginlocationname"] = getEnteredLocation;
						WOarray["custparam_beginlocinternalid"] = getEndLocationInternalId;


						nlapiLogExecution('Error', 'getEnteredLocation',WOarray["custparam_beginlocationname"]);
						nlapiLogExecution('Error', 'getEndLocationInternalId',WOarray["custparam_beginlocinternalid"]);

						/*	var getBeginLocationid=	SOarray["custparam_beginLocation"];
					var wave=request.getParameter('hdnWOid');
					nlapiLogExecution('Error', 'wave', request.getParameter('hdnWOid'));

					WOarray["custparam_remqty"]=0;*/

						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_status', 'customdeploy_ebiz_rf_wo_put_status_dl', false, WOarray);					
						nlapiLogExecution('Error', 'Done customrecord', 'Success');
					}
					else
					{
						nlapiLogExecution('Error', 'Error: ', 'No Sufficient Remaining cube');
						// Case# 20148055
						//WOarray["custparam_error"] = st8;
						WOarray["custparam_error"] = "No Sufficient Remaining cube";
						// Case# 20148055 ends
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);


					}
				}
				else {
					WOarray["custparam_beginlocationname"]='';
					WOarray["custparam_beginlocinternalid"]='';	

					nlapiLogExecution('Error', 'Error: ', 'No Sufficient Remaining Cube For Binlocation');

					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);

				}
			}
			else {
				WOarray["custparam_beginlocationname"]='';
				WOarray["custparam_beginlocinternalid"]='';	
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_status', 'customdeploy_ebiz_rf_wo_put_status_dl', false, WOarray);
			}

		}
	}
}

//case 20127123 to check binlocation remaining cube
function getLocationDetails(binlocid,SiteLocation,ItemCube)
{
	var LocArry = new Array();
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('name');
	columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
	columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
	columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
	columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

	nlapiLogExecution('ERROR', 'binlocid', binlocid);
	nlapiLogExecution('ERROR', 'SiteLocation', SiteLocation);
	nlapiLogExecution('ERROR', 'ItemCube', ItemCube);
	var filters = new Array();
	//filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
	filters.push(new nlobjSearchFilter('internalId', null , 'is', binlocid));
	if(SiteLocation != null && SiteLocation != "")
		filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof',SiteLocation));
	t1 = new Date();

	//var locResults = nlapiLoadRecord('customrecord_ebiznet_createinv', binlocid);
	var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);



	nlapiLogExecution('ERROR', 'locResults', locResults);
	if (locResults != null && locResults != '') {
		nlapiLogExecution('ERROR', 'ifnot ');
		for (var i = 0; i < locResults.length; i++) {
			Location = locResults[i].getValue('name');
			var locationIntId = locResults[i].getId();

			LocRemCube = GeteLocCube(locationIntId);
			nlapiLogExecution('ERROR', 'Location', Location);

			nlapiLogExecution('ERROR', 'locResults[i].getId()', locResults[i].getId());
			OBLocGroup = locResults[i].getValue('custrecord_outboundinvlocgroupid');
			PickSeqNo = locResults[i].getValue('custrecord_startingpickseqno');
			IBLocGroup = locResults[i].getValue('custrecord_inboundinvlocgroupid');
			PutSeqNo = locResults[i].getValue('custrecord_startingputseqno');
			nlapiLogExecution('ERROR', 'LocRemCube', LocRemCube);
			if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
				RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
				location_found = true;

				LocArry[0] = Location;
				LocArry[1] = RemCube;
				LocArry[2] = locationIntId;		//locResults[i].getId();
				LocArry[3] = OBLocGroup;
				LocArry[4] = PickSeqNo;
				LocArry[5] = IBLocGroup;
				LocArry[6] = PutSeqNo;
				LocArry[7] = LocRemCube;
				//LocArry[8] = zoneid;
				nlapiLogExecution('ERROR', 'SA-Location', Location);
				nlapiLogExecution('ERROR', 'Location Internal Id',locationIntId);
				nlapiLogExecution('ERROR', 'Sa-Location Cube', RemCube);
				return LocArry;
			}
		}
	}
}