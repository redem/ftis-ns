/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_CyCBinLoc_Invt.js,v $
*  $Revision: 1.1.2.2 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_CyCBinLoc_Invt.js,v $
*  Revision 1.1.2.2  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function ebiznet_BinLocInventory(request, response){
	if (request.getMethod() == 'GET'){

		var form = nlapiCreateForm('CYC Bin Location Inventory');
		var vbinloc = request.getParameter('custparam_binloc');
		nlapiLogExecution('ERROR','vbinloc',vbinloc);

		var sublist = form.addSubList("custpage_items", "list", "Inventory Report");
		//sublist.addField("custpage_no", "text", "SL NO").setDisplayType('disabled');
		sublist.addField("custpage_sku", "select", "Item", "item").setDisplayType('inline');
		sublist.addField("custpage_whloc", "text", "Location");
		var displayBinLocation = sublist.addField("custpage_actendloc", "text", "Bin Location", 'customrecord_ebiznet_location').setDisplayType('inline');
		sublist.addField("custpage_invtlp", "text", "LP #");
		sublist.addField("custpage_availqty", "text", "Avail Qty");

		var invtfilters = new Array();		
		invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', vbinloc));
		invtfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var invtcolumns = new Array();
		invtcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
		invtcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
		invtcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
		invtcolumns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		invtcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
		invtcolumns[0].setSort();
		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns);
		if(invtsearchresults!=null && invtsearchresults!='')
		{
			nlapiLogExecution('ERROR','invtsearchresults',invtsearchresults.length);
			for(var i=0;i<invtsearchresults.length;i++)
			{				
				var sku = invtsearchresults[i].getValue('custrecord_ebiz_inv_sku');
				var whloc = invtsearchresults[i].getValue('custrecord_ebiz_inv_loc');
				var binloc = invtsearchresults[i].getText('custrecord_ebiz_inv_binloc');
				var invtlp = invtsearchresults[i].getValue('custrecord_ebiz_inv_lp');
				var avlqty = invtsearchresults[i].getValue('custrecord_ebiz_avl_qty');
				
				
				nlapiLogExecution('ERROR','sku',sku);
				nlapiLogExecution('ERROR','whloc',whloc);
				nlapiLogExecution('ERROR','binloc',binloc);
				nlapiLogExecution('ERROR','invtlp',invtlp);
				nlapiLogExecution('ERROR','avlqty',avlqty);
				
				form.getSubList('custpage_items').setLineItemValue('custpage_sku',  i + 1, sku);
				form.getSubList('custpage_items').setLineItemValue('custpage_whloc', i + 1, whloc);
				form.getSubList('custpage_items').setLineItemValue('custpage_actendloc', i + 1, binloc);
				form.getSubList('custpage_items').setLineItemValue('custpage_invtlp', i + 1, invtlp); 				
				form.getSubList('custpage_items').setLineItemValue('custpage_availqty', i + 1, avlqty);
				

			}

		}
		response.writePage(form);
	}
	else
	{
		//post
	}
}