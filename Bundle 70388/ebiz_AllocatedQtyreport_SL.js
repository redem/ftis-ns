/***************************************************************************
 							eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_AllocatedQtyreport_SL.js,v $
 *     	   $Revision: 1.1.2.2.4.3.2.2.2.1 $
 *     	   $Date: 2014/08/29 19:05:15 $
 *     	   $Author: spendyala $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_78 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  
 *
 *****************************************************************************/

function AllocatedQtyReport(request,response)
{
	var ctx = nlapiGetContext();
	if(request.getMethod()=='GET')
	{
		var form = nlapiCreateForm('Allocated Quantity Report');

		var itemField = form.addField('custpage_item', 'multiselect', 'Item','item');
		var binLocnField = form.addField('custpage_binlocation', 'select', 'Bin Location','customrecord_ebiznet_location');
		form.addField("custpage_diffcheck", "checkbox", "Show only Differences").setDefaultValue("T");

		var button = form.addSubmitButton('Display');
		response.writePage(form);

	}
	else
	{
		try
		{
			var result;
			var ItemId=request.getParameter('custpage_item');
			var Binlocation=request.getParameter('custpage_binlocation');
			nlapiLogExecution('ERROR','Binlocation ',Binlocation);
			var form = nlapiCreateForm('Allocated Quantity Report');
			var itemField = form.addField('custpage_item', 'multiselect', 'Item','item');
			itemField .setDefaultValue(ItemId);
			var DiffCheck=request.getParameter('custpage_diffcheck');
			var binLocnField = form.addField('custpage_binlocation', 'select', 'Bin Location','customrecord_ebiznet_location');
			form.addField("custpage_diffcheck", "checkbox", "Show only Differences").setDefaultValue(DiffCheck);

			binLocnField .setDefaultValue(Binlocation);
			var button = form.addSubmitButton('Display');

			var vItemArr = new Array();
			if (ItemId != null && ItemId != "") {
				vItemArr = ItemId.split('');}

			var sublist = form.addSubList("custpage_items", "list", "Allocated Quantity List");
			sublist.addField("custpage_item", "text", "Item#");
			sublist.addField("custpage_lp", "text", "LP#");
			sublist.addField("custpage_binlocation", "text", "BinLocation");	
			sublist.addField("custpage_qoh", "text", "Quantity On Hand");
			sublist.addField("custpage_invtallqty", "text", "Allocated Quantity in Inventory");
			sublist.addField("custpage_openallqty", "text", "Allocated Quantity in OpenTask");

			result=GetSearchRecords(request,vItemArr,-1,Binlocation);
			setPagingForSublist(result,form,DiffCheck);

			response.writePage(form);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception ',exp); 
		}

	}

}


var tempResultsArray=new Array();
/**
 * Function to GetSearch Results  
 * 
 * @param request
 * @param Item
 * @param maxno

 */

function GetSearchRecords(request,vItemArr,maxno,Binlocation)
{ 

	var invfilter= new Array();
	if (vItemArr != null && vItemArr != "")
	{
		invfilter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof',vItemArr ));	 
	}
	if (Binlocation != null && Binlocation != "")
	{
		invfilter.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'is',Binlocation));	 
	}

	invfilter.push(new nlobjSearchFilter('custrecord_ebiz_alloc_qty',null, 'greaterthan', '0'));
	invfilter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag',null, 'anyof', ['3','19']));
	//invfilter.push(new nlobjSearchFilter('custrecord_invttasktype',null, 'anyof', ['2','9','10']));
//	invfilter.push(new nlobjSearchFilter('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc', 'anyof', ['1','2','6','7']));



	if(maxno!=-1)
	{

		invfilter.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}
	var invColumns = new Array();
	invColumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	invColumns[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	invColumns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	invColumns[3] = new nlobjSearchColumn('internalid');
	invColumns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	invColumns[5] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	invColumns[3].setSort();

	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invfilter, invColumns);

	if(SearchResults!=null && SearchResults!='')  
	{
		nlapiLogExecution('ERROR','Searchresults',SearchResults.length);

		if(SearchResults.length>=1000)
		{
			nlapiLogExecution('ERROR','searchresults','more than 1000');
			var maxno1=SearchResults[SearchResults.length-1].getValue(invColumns[3]);
			tempResultsArray.push(SearchResults);
			GetSearchRecords(request,vItemArr,maxno1,Binlocation);

		}
		else
		{
			tempResultsArray.push(SearchResults);
		}
	}

	return tempResultsArray;

}

/**
 * Function to add paging to the Page 
 * 
 * @param form
 * @param currentOrder

 */

function setPagingForSublist(orderList,form,DiffCheck)
{
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			//nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				//nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length);
				nlapiLogExecution('Error', 'ordsearchresult ', ordsearchresult.length);
				for(var j=0;j<ordsearchresult.length;j++)
				{
					//orderListArray[orderListArray.length]=ordsearchresult[j];
					var SearchResults=ordsearchresult[j];


					var LP=SearchResults.getValue('custrecord_ebiz_inv_lp');
					var Item=SearchResults.getText('custrecord_ebiz_inv_sku');
					var ItemId=SearchResults.getValue('custrecord_ebiz_inv_sku');
					var InvtAllQty=SearchResults.getValue('custrecord_ebiz_alloc_qty');
					var BinLoc=SearchResults.getText('custrecord_ebiz_inv_binloc');
					var Qoh=SearchResults.getValue('custrecord_ebiz_qoh');
					var InvRef = SearchResults.getId();
					var vOTQty=GetOTQTY(LP,ItemId,InvRef);
					if(InvtAllQty == null || InvtAllQty == '')
						InvtAllQty=0;
					if(DiffCheck=='T')
					{	
						if(parseFloat(InvtAllQty) != parseFloat(vOTQty))
						{	
							var vOrdDet=[LP,Item,ItemId,InvtAllQty,BinLoc,Qoh,vOTQty,InvRef];
							orderListArray.push(vOrdDet);
						}
					}
					else
					{
						var vOrdDet=[LP,Item,ItemId,InvtAllQty,BinLoc,Qoh,vOTQty,InvRef];
						orderListArray.push(vOrdDet);
					}
				}
			}
		}
		//nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';
		form.setScript('customscript_ebiz_allocatedqtyclient_cl');
		nlapiLogExecution('Error', 'orderListArray ', orderListArray.length);
		if(orderListArray.length>0)
		{

			if(orderListArray.length>25)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records(# of Lines)');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("100");
					pagesizevalue=25;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue=25;
						pagesize.setDefaultValue("25");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseInt(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseInt(k)*parseInt(pagesizevalue);
					from=(parseInt(to)-parseInt(pagesizevalue))+1;

					if(parseInt(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseInt(orderListArray.length);
			}

			var minval=0;var maxval=parseInt(pagesizevalue);
			if(parseInt(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			//nlapiLogExecution('ERROR', 'selectno',selectno);
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseInt(selectedPageArray[1])-(parseInt(selectedPageArray[0])-1);

				var pagevalue=request.getParameter('custpage_pagesize');
				//nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				//nlapiLogExecution('ERROR', 'selectedPageArray[1]',selectedPageArray[1]);
				//nlapiLogExecution('ERROR', 'orderListArray.length',orderListArray.length);
				if(pagevalue!=null&&(parseInt(selectedPageArray[1])<=orderListArray.length))
				{
					nlapiLogExecution('ERROR', 'checkpt','chkpt');
					if(parseInt(diff)==parseInt(pagevalue)|| test==selectno)
					{
						//nlapiLogExecution('ERROR', 'diff',diff);
						var selectedPageArray=selectno.split(',');	
						//nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseInt(selectedPageArray[0])-1;
						//nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseInt(selectedPageArray[1]);
						//nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}

			var c=1;
			var minvalue;
			minvalue=minval;
			var prevVal=null;var currVal=null;
			//nlapiLogExecution('ERROR', 'minvalue ', minvalue);  
			//nlapiLogExecution('ERROR', 'maxval ', maxval); 
			for(var j = minvalue; j < maxval; j++){		
//				nlapiLogExecution('ERROR', 'orderListArray ', orderListArray[j]); 
				var currentOrder = orderListArray[j];
				AddtoSublist(form, currentOrder,c);
				c=c+1;

			}
		}
	}
}


/**
 * Function to Fill Sublist 
 * 
 * @param form
 * @param currentOrder
 * @param i

 */

function AddtoSublist(form, SearchResults, i)
{
	var ctx = nlapiGetContext();
	var ViewLink;

//	var lp=SearchResults[i].getValue('custrecord_ebiz_inv_lp');
	nlapiLogExecution('ERROR','AddtoSublist','AddtoSublist');


	var LP=SearchResults[0];
	var Item=SearchResults[1];
	var ItemId=SearchResults[2];
	var InvtAllQty=SearchResults[3];
	var BinLoc=SearchResults[4];
	var Qoh=SearchResults[5];

	var OpenAllQty=SearchResults[6];
	var OpenInvRefNo=SearchResults[7];
	
	//ViewLink=addURL(ctx.getEnvironment(), ItemId ,LP);
	var ViewLink = nlapiResolveURL('SUITELET','customscript_ebiz_viewallocqtylocation', 'customdeploy_ebiz_viewallocqtylocation_d');
	//nlapiResolveURL('SUITELET','customscript_ebiz_viewallocqtylocation', 'customdeploy_ebiz_viewallocqtylocation_d');
	//nlapiLogExecution('ERROR', 'UrL', Locationurl);

	//nlapiLogExecution('ERROR', 'Wave PDF URL',WavePDFURL);					
	ViewLink = ViewLink + '&custparam_Item=' + ItemId;;
	ViewLink = ViewLink + '&custparam_lp=' + LP;
	ViewLink = ViewLink + '&custparam_InvRef=' + OpenInvRefNo;
	//nlapiLogExecution('ERROR', 'WavePDFURL',WavePDFURL);
//	alert(WavePDFURL);
	//window.open(WavePDFURL);

	form.getSubList('custpage_items').setLineItemValue('custpage_lp', i, LP);
	form.getSubList('custpage_items').setLineItemValue('custpage_item', i, Item);
	form.getSubList('custpage_items').setLineItemValue('custpage_invtallqty', i, InvtAllQty);
	form.getSubList('custpage_items').setLineItemValue('custpage_binlocation', i, BinLoc);
	form.getSubList('custpage_items').setLineItemValue('custpage_qoh', i, Qoh);
	if(OpenAllQty=="0")
	{
		form.getSubList('custpage_items').setLineItemValue('custpage_openallqty', i, OpenAllQty);	
	}
	else
	{
		//nlapiLogExecution('ERROR', 'i', i);
		//nlapiLogExecution('ERROR', 'UrL', ViewLink);
		//form.getSubList('custpage_items').setLineItemValue('custpage_openallqty', i, '<a href="' + ViewLink + '">'+OpenAllQty+'</a>');
		form.getSubList('custpage_items').setLineItemValue('custpage_openallqty', i, "<a href=\"#\" onclick=\"javascript:window.open('" + ViewLink + "');\" >" + OpenAllQty + "</a>");
	}

}









//Generating UrL, to navigate to ViewLocation Page

function addURL(environment,ItemId ,LP) {
	var SoURL;
	SoURL = nlapiResolveURL('SUITELET','customscript_ebiz_viewallocqtylocation', 'customdeploy_ebiz_viewallocqtylocation_d');
	nlapiLogExecution('ERROR', 'Item', ItemId);
	nlapiLogExecution('ERROR', 'LP', LP);
	// Get the FQDN depending on the context environment
	var fqdn = getFQDNForHost(environment);

	var ctx = nlapiGetContext();

	if (fqdn == '')
		nlapiLogExecution('ERROR', 'Unable to retrieve FQDN based on context');
	else
		SoURL = getFQDNForHost(ctx.getEnvironment()) + SoURL;

	SoURL = SoURL + '&custparam_Item=' + ItemId;
	SoURL = SoURL + '&custparam_lp=' + LP;
	nlapiLogExecution('ERROR', 'UrL', SoURL);
	return SoURL;

}

function GetOTQTY(LP,ItemId,InvRef)
{

	var OpenAllQty='';
	nlapiLogExecution('ERROR','AddtoSublist InvRef',InvRef);
	var openfilter= new Array();
	//openfilter.push(new nlobjSearchFilter('custrecord_lpno',null, 'is', LP));
	openfilter.push(new nlobjSearchFilter('custrecord_invref_no',null, 'equalto', InvRef));
	openfilter.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', ['3','8']));
	openfilter.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	openfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof',[19,30]));
	openfilter.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',ItemId));
	//openfilter.push(new nlobjSearchFilter('custrecord_lpno', null, 'isnotempty'));
	
	var columns = new Array();	
	//columns[0] = new nlobjSearchColumn('custrecord_lpno', null, 'group');
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null ,'sum');

	var opentaskResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openfilter, columns);
	if(opentaskResults!=null)
	{	
		nlapiLogExecution('ERROR','opentaskResults',opentaskResults.length);					
		OpenAllQty=opentaskResults[0].getValue('custrecord_expe_qty', null, 'sum')
	}
	else
	{
		OpenAllQty="0";
	}
	return OpenAllQty;


}