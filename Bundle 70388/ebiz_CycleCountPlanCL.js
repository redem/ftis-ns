/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/Attic/ebiz_CycleCountPlanCL.js,v $
 *     	   $Revision: 1.1.4.2.2.2 $
 *     	   $Date: 2015/10/15 07:01:10 $
 *     	   $Author: grao $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_35 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_CycleCountPlanCL.js,v $
 * Revision 1.1.4.2.2.2  2015/10/15 07:01:10  grao
 * 2015.2 Issue Fixes 201415052
 *
 * Revision 1.1.4.2.2.1  2015/10/13 15:22:59  grao
 * 2015.2 Issue Fixes 201414817
 *
 * Revision 1.1.4.2  2014/12/24 12:19:19  snimmakayala
 * Case#: 201411301
 *
 * Revision 1.1.2.1  2014/12/23 12:30:32  snimmakayala
 * Case#: 201411301
 *
 *
 *
 *****************************************************************************/

function fnSaveCycleCountPlan()
{
	var cyccid = nlapiGetFieldValue('id');
	var cycccloseflag = nlapiGetFieldValue('custrecord_cyccplan_close');
	var vbinloc = nlapiGetFieldValue('custrecord_ebiz_cycl_binloc');
	
	
	if(vbinloc!=null && vbinloc!='')
	{

		var vfilters = new Array();
		vfilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['8','9']));	
		if(vbinloc!=null && vbinloc!="" )
				vfilters.push(new nlobjSearchFilter('internalid', null, 'anyof', vbinloc));

		vfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		var vcolumns = new Array();
		vcolumns[0] = new nlobjSearchColumn('custrecord_startingputseqno');		
		var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, vfilters, vcolumns);

		if(locResults!=null && locResults!='')
		{
			alert('Entered location refers to Stage Location');
			return false;
		}

	}

	if(cycccloseflag=='T' && cyccid!=null && cyccid!='')
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', cyccid));
		filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', ['20','31']));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_cycact_beg_loc');
		columns[1] = new nlobjSearchColumn('custrecord_cyclecount_exp_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_cycleact_sku');
		columns[3] = new nlobjSearchColumn('custrecord_cycleexp_qty');
		columns[4] = new nlobjSearchColumn('custrecord_cycle_act_qty');
		columns[5] = new nlobjSearchColumn('custrecord_cyclelpno');
		columns[6] = new nlobjSearchColumn('custrecord_cyclestatus_flag');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);

		if(searchresults!=null && searchresults!='')
		{
			alert('This Plan is not fully resolved. So do not close the plan');
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}