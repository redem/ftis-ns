/***************************************************************************
  eBizNET Solutions
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_Replenishment_SL.js,v $
 *     	   $Revision: 1.12.2.13.4.6.2.30.2.3 $
 *     	   $Date: 2015/12/01 15:31:45 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *****************************************************************************/

/**
 * 
 * @param skuQtyInPickfaceLocnsList
 */
function getPFLocationListForQuery(skuQtyInPickfaceLocnsList){

	var pfLocationList = new Array();
	if(skuQtyInPickfaceLocnsList!=null && skuQtyInPickfaceLocnsList!='')
	{
		for(var i = 0; i < skuQtyInPickfaceLocnsList.length; i++){
			pfLocationList.push(skuQtyInPickfaceLocnsList[i].getValue('custrecord_pickbinloc'));
		}
	}

	if(pfLocationList!=null && pfLocationList!='')
	{
		pfLocationList=removeDuplicateElement(pfLocationList);
	}

	nlapiLogExecution('DEBUG','Out of getPFLocationListForQuery',pfLocationList);

	return pfLocationList;
}

/**
 * 
 * @param skuQtyInPickfaceLocnsList
 * @returns {Array}
 */
function generateSKUAndPFLocnList(skuQtyInPickfaceLocnsList){
	var skuLocnArray = new Array();
	var skuList = new Array();
	var skuCount = 0;
	var pfLocnList = new Array();
	var pfLocnCount = 0;

	var sku = skuQtyInPickfaceLocnsList.getValue('custrecord_pickfacesku');
	var pfLocn = skuQtyInPickfaceLocnsList.getValue('custrecord_pickbinloc');
	skuList[skuCount++]= sku;
	pfLocnList[pfLocnCount++] = pfLocn;
	skuLocnArray = [skuList, pfLocnList];
	return skuLocnArray;
}

/**
 * 
 * @param skuQtyInPickfaceLocnsList
 * @returns {Array}
 */
function getQOHForAllSKUsinPFLocation(skuQtyInPickfaceLocnsList){
	var pfSKUInvList = new Array();
	nlapiLogExecution('DEBUG','skuQtyInPickfaceLocnsList',skuQtyInPickfaceLocnsList);
	var pfLocationList1 = getPFLocationListForQuery(skuQtyInPickfaceLocnsList);
	var pfitems = getPFLocationListForQueryitems(skuQtyInPickfaceLocnsList);
	var pfLocationList = new Array();
	pfLocationList.push(pfLocationList1);

	nlapiLogExecution('DEBUG','pfLocationList new new',pfLocationList);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filters[1] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', pfLocationList1);	
	filters[2] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',  [19]);
	filters[3] = new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0);
	filters[4] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', pfitems);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columns[1].setSort();
	columns[2].setSort(true);

	pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	return pfSKUInvList;
}

/**
 * Function to return the inventory for a particular SKU from a bin location 
 * by looking at a provided list.  This list is generated for all pickface locations.
 * NOTE: WILL ATLEAST RETURN 0
 * 
 * @param qohListForPFLocations
 * @param item
 * @param binLocn
 * @returns {Number}
 */
function getInvForLocnAndSKU(qohListForPFLocations, item, binLocn,site){
	var invForLocnAndSKU = 0;
	nlapiLogExecution('DEBUG','item in new',item);
	nlapiLogExecution('DEBUG','binLocn in new',binLocn);
	nlapiLogExecution('DEBUG','site',site);

	if(qohListForPFLocations != null && qohListForPFLocations != '')
	{
		for(var i = 0; i < qohListForPFLocations.length; i++){

			qohListForPFLocations[i].getValue('custrecord_ebiz_inv_loc');

			if(qohListForPFLocations[i].getValue('custrecord_ebiz_inv_loc') == site && qohListForPFLocations[i].getValue('custrecord_ebiz_inv_sku') == item && qohListForPFLocations[i].getValue('custrecord_ebiz_inv_binloc') == binLocn ){

				if(qohListForPFLocations[i].getValue('custrecord_ebiz_qoh')!='')
				{
					nlapiLogExecution('DEBUG','qoh1',qohListForPFLocations[i].getValue('custrecord_ebiz_qoh'));
					invForLocnAndSKU += parseFloat(qohListForPFLocations[i].getValue('custrecord_ebiz_qoh'));
				}
			}
		}
	}

	return invForLocnAndSKU;
}


function getAllocQtyForLocnAndSKU(qohListForPFLocations, item, binLocn,site){
	var AllocatedForLocnAndSKU = 0;

	if(qohListForPFLocations != null && qohListForPFLocations != '')
	{
		for(var i = 0; i < qohListForPFLocations.length; i++){

			if(qohListForPFLocations[i].getValue('custrecord_ebiz_inv_loc') == site && qohListForPFLocations[i].getValue('custrecord_ebiz_inv_sku') == item && qohListForPFLocations[i].getValue('custrecord_ebiz_inv_binloc') == binLocn ){
				if(qohListForPFLocations[i].getValue('custrecord_ebiz_alloc_qty')!='')
				{
					nlapiLogExecution('DEBUG','allocated1',qohListForPFLocations[i].getValue('custrecord_ebiz_alloc_qty'));
					AllocatedForLocnAndSKU += parseFloat(qohListForPFLocations[i].getValue('custrecord_ebiz_alloc_qty'));
				}
			}
		}
	}

	return AllocatedForLocnAndSKU;
}

function getInvForLocnAndSKUNew(qohListForPFLocations, item, binLocn,site,maxval){
	var invForLocnAndSKU = 0;
	var AllocatedForLocnAndSKU = 0;
	var invtQuantity = new Array();
	if(qohListForPFLocations != null && qohListForPFLocations != '')
	{
		for(var i = 0; i < qohListForPFLocations.length; i++){

			qohListForPFLocations[i].getValue('custrecord_ebiz_inv_loc');

			if(qohListForPFLocations[i].getValue('custrecord_ebiz_inv_loc') == site && qohListForPFLocations[i].getValue('custrecord_ebiz_inv_sku') == item && qohListForPFLocations[i].getValue('custrecord_ebiz_inv_binloc') == binLocn ){

				if(qohListForPFLocations[i].getValue('custrecord_ebiz_qoh')!='')
				{

					invForLocnAndSKU += parseInt(qohListForPFLocations[i].getValue('custrecord_ebiz_qoh'));
				}
				if(qohListForPFLocations[i].getValue('custrecord_ebiz_alloc_qty')!='')
				{

					AllocatedForLocnAndSKU += parseInt(qohListForPFLocations[i].getValue('custrecord_ebiz_alloc_qty'));
				}
			}

		}
		invtQuantity.push(invForLocnAndSKU,AllocatedForLocnAndSKU);
	}

	return invtQuantity;
}

/**
 * 
 * @param replenZoneInvList
 * @param item
 * @param reqdQty
 */
function isEnoughInvAvailableInReplenZone(replenZoneInvList, item, reqdQty,pfBinLocation,PFstatusId){
	var isEnoughAvailable1 = false;
	var isEnoughAvailable = new Array();
	var availQtyInReplenZone = 0;

	for(var i = 0; replenZoneInvList!=null && i < replenZoneInvList.length; i++){
		if(! isEnoughAvailable1){
			var invSKU = replenZoneInvList[i].getValue('custrecord_ebiz_inv_sku');
			var invbinlocation = replenZoneInvList[i].getValue('custrecord_ebiz_inv_binloc');
			var invSKUStatusId = replenZoneInvList[i].getValue('custrecord_ebiz_inv_sku_status');
			// If the items dont match, go to the next record
			if((invSKU != item) || ((invSKUStatusId != PFstatusId)&&( PFstatusId !=null && PFstatusId !='' && PFstatusId !='null')))
				continue;
			if(invbinlocation == pfBinLocation)
				continue;
			// Calculating the available quantity in the replen zone for the item
			if(replenZoneInvList[i].getValue('custrecord_ebiz_qoh')!='' && replenZoneInvList[i].getValue('custrecord_ebiz_qoh')!=null)
				availQtyInReplenZone += parseFloat(replenZoneInvList[i].getValue('custrecord_ebiz_qoh'));


			nlapiLogExecution('DEBUG','availQtyInReplenZone',availQtyInReplenZone);
			if(availQtyInReplenZone >= reqdQty || (availQtyInReplenZone!=null && availQtyInReplenZone!='')){

				isEnoughAvailable[i] = true;
				isEnoughAvailable1 = true;

				continue;
			}
			else
			{
				isEnoughAvailable[i] = false;

				continue;
			}
		}
	}

	return isEnoughAvailable;
}

/**
 * Function is to get the count of lines which have the flag selected.
 */

function getLineCount(){
	var lineCnt = request.getLineItemCount('custpage_replen_items');
	var k = 0;

	for (var x = 1; x <= lineCnt; x++) {
		if (request.getLineItemValue('custpage_replen_items', 'custpage_replen_sel', x) == 'T') {
			k += 1;
		}
	}
	nlapiLogExecution('DEBUG','Line Count',k);
	return k;
}

/**
 * 
 * @param item
 * @returns
 */
function getItemFamilyGroup(item){
	var ItemFamilyGroup = new Array();
	var fields = ['custitem_item_family', 'custitem_item_group'];

	var columns = nlapiLookupField('inventoryitem', item, fields);
	ItemFamilyGroup[0] = columns.custitem_item_family;
	ItemFamilyGroup[1] = columns.custitem_item_group;

	return ItemFamilyGroup;
}

/**
 * Build the string for all the items and zones selected in the list. 
 * This item list has to be passed as a string to search for all bin locations in the inventory
 * NOTE: COULD RETURN NULL
 *  
 * @param replenItemArray
 * @param itemArrayLength
 * @returns List of items as comma separated string
 */
function buildItemListForReplenProcessing(replenItemArray, itemArrayLength){
	var replenItemList = new Array();

	for (var p = 0; p < itemArrayLength; p++){
		replenItemList.push(replenItemArray[p][0]);
	}

	return replenItemList;
}

/**
 * 
 * @param pendingReplenQuantity
 * @param pfLocation
 * @returns {Number}
 */
function getExpectedQtyForPFLocation(pendingReplenQuantity, pfLocation){
	var locationFound = false;
	var expectedQty = 0;

	if(pendingReplenQuantity != null && pendingReplenQuantity.length > 0){
		for(var i = 0; i < pendingReplenQuantity.length; i++){
			if(!locationFound){
				if(pendingReplenQuantity[i][0] == pfLocation){
					locationFound = true;
					expectedQty = pendingReplenQuantity[i][1];
				}
			}
		}
	}

	if(!locationFound)
		expectedQty = -1;

	return expectedQty;
}

/**
 * 
 */


function genReplenGETSuitelet(request, response, whlocation, item,itemfamily,itemgroup)
{
	var start = new Date();

	var form = nlapiCreateForm('Generate Replenishment');
	
	form.setScript('customscript_inventoryclientvalidations');
	var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
	msg.setLayoutType('outsideabove', 'startcol');
	
	item = form.addField('custpage_item', 'multiselect', 'Item','Item');    

	whlocation = form.addField('custpage_location', 'select', 'Location');

	itemfamily = form.addField('custpage_itemfamily', 'select', 'Item Family','customrecord_ebiznet_sku_family');

	itemgroup = form.addField('custpage_itemgroup', 'select', 'Item Group','customrecord_ebiznet_sku_group');

	var inboundlocgroup = form.addField('custpage_inblocgroup', 'select', 'Inbound Location Group','customrecord_ebiznet_loc_group');

	var filtersloc = new Array();
	filtersloc.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));

	var columnsloc = new Array();
	columnsloc.push(new nlobjSearchColumn('name'));

	var searchlocresults = nlapiSearchRecord('Location', null, filtersloc, columnsloc);

	if (searchlocresults != null && searchlocresults!='')
	{

		for (var i = 0; i < searchlocresults.length; i++) 
		{
			var res = form.getField('custpage_location').getSelectOptions(searchlocresults[i].getValue('name'), 'is');
			if (res != null) 
			{
				if (res.length > 0)	                
					continue;	                
			}
			whlocation.addSelectOption(searchlocresults[i].getId(), searchlocresults[i].getValue('name'));
		}
	} 
	if(request.getParameter('custpage_location')!='' && request.getParameter('custpage_location')!=null)
	{
		whlocation.setDefaultValue(request.getParameter('custpage_location'));	
	}

	if(request.getParameter('custpage_itemfamily')!='' && request.getParameter('custpage_itemfamily')!=null)
	{
		itemfamily.setDefaultValue(request.getParameter('custpage_itemfamily'));	
	}

	if(request.getParameter('custpage_itemgroup')!='' && request.getParameter('custpage_itemgroup')!=null)
	{
		itemgroup.setDefaultValue(request.getParameter('custpage_itemgroup'));	
	}

	var inblocgroup='';
	if(request.getParameter('custpage_inblocgroup')!='' && request.getParameter('custpage_inblocgroup')!=null)
	{
		inboundlocgroup.setDefaultValue(request.getParameter('custpage_inblocgroup'));	
		inblocgroup = request.getParameter('custpage_inblocgroup');
	}
	nlapiLogExecution('DEBUG', 'inblocgroup',inblocgroup);	

	if(request.getParameter('custpage_item')!='' && request.getParameter('custpage_item')!=null)
	{
		var itemValue=request.getParameter('custpage_item');
		var itemArray = new Array();
		itemArray = itemValue.split('');
		nlapiLogExecution('DEBUG', 'multiselect',itemArray.length);	
	}
	item.setDefaultValue(request.getParameter('custpage_item'));
	//Adding  ItemSubList
	var itemSubList = form.addSubList("custpage_replen_items", "list", "ItemList");
	itemSubList.addMarkAllButtons();
	itemSubList.addField("custpage_replen_sel", "checkbox", "Select");
	itemSubList.addField("custpage_replen_sku", "select", "Item", "item");
	itemSubList.addField("custpage_replen_location", "select", "Pickface Location", "customrecord_ebiznet_location");

	itemfamily = request.getParameter('custpage_itemfamily');

	itemgroup = request.getParameter('custpage_itemgroup');
	whlocation=request.getParameter('custpage_location');

	ruleid=request.getParameter('custpage_ruleid');

	nlapiLogExecution('DEBUG', 'itemgroup', itemgroup);
	nlapiLogExecution('DEBUG', 'itemfamily', itemfamily);
	nlapiLogExecution('DEBUG', 'whlocation', whlocation);

	itemSubList.addField("custpage_replen_packcode", "text", "PackCode");
	itemSubList.addField("custpage_replen_status", "text", "Status");
	itemSubList.addField("custpage_replen_minqty", "text", "Min Qty");
	itemSubList.addField("custpage_replen_maxqty", "text", "Max Qty");
	itemSubList.addField("custpage_replen_rplnqty", "text", "Replen Qty");
	itemSubList.addField("custpage_replen_roundqty", "text", "Round Qty");
	itemSubList.addField("custpage_replen_qty", "text", "PickFace Loc Qty");
	itemSubList.addField("custpage_replen_allocqty", "text", "PickFace Loc Alloc Qty");
	itemSubList.addField("custpage_replen_availqty", "text", "PickFace Loc Avail Qty");
	itemSubList.addField("custpage_replen_isqtyavailableinwh", "text", "Is Qty Available in WH?");
	itemSubList.addField("custpage_skurecid", "text", "Recid").setDisplayType('hidden');
	itemSubList.addField("custpage_skureplenruleid", "text", "Replen Ruleid").setDisplayType('hidden');
	itemSubList.addField("custpage_whlocation", "text", "Site").setDisplayType('hidden');
	itemSubList.addField("custpage_itemfamily", "text", "itemfamily").setDisplayType('hidden');
	itemSubList.addField("custpage_itemgroup", "text", "itemgroup").setDisplayType('hidden');
	itemSubList.addField("custpage_pickfacefixedlp", "text", "FixedLP").setDisplayType('hidden');
	itemSubList.addField("custpage_replen_status_id", "text", "Status Id").setDisplayType('hidden');
	var hiddenField_selectpage = form.addField('custpage_hiddenfield', 'text', '').setDisplayType('hidden');
	hiddenField_selectpage.setDefaultValue('F');
	var item = request.getParameter('custpage_item');


	var vItemArr=new Array();
	if(item != null && item != "")
	{
		vItemArr = item.split('');
	}
	nlapiLogExecution('DEBUG', 'item Internal ID', item);
	var replenRulesList = getListForAllReplenZones(whlocation);

	var replenZoneList = "";
	var replenZoneTextList = "";
	var replenzonestatuslist="";
	var replenzonestatustextlist="";

	if (replenRulesList != null){
		for (var i = 0; i < replenRulesList.length; i++){
			nlapiLogExecution('DEBUG','replenRulesList.length',replenRulesList.length);
			if (i == 0){
				replenZoneList += replenRulesList[i].getValue('custrecord_replen_strategy_zone');
				replenZoneTextList += replenRulesList[i].getText('custrecord_replen_strategy_zone');
				replenzonestatuslist +=replenRulesList[i].getValue('custrecord_replen_strategy_itemstatus');
				replenzonestatustextlist +=replenRulesList[i].getText('custrecord_replen_strategy_itemstatus');
			} else {
				replenZoneList += ',';
				replenZoneList += replenRulesList[i].getValue('custrecord_replen_strategy_zone');

				replenZoneTextList += ',';
				replenZoneTextList += replenRulesList[i].getText('custrecord_replen_strategy_zone');

				replenzonestatuslist += ',';
				replenzonestatuslist += replenRulesList[i].getValue('custrecord_replen_strategy_itemstatus');

				replenzonestatustextlist += ',';
				replenzonestatustextlist += replenRulesList[i].getText('custrecord_replen_strategy_itemstatus');
			}
		}
	} 
	var itemslist = new Array();
	var inventoryFromReplenZone = getSKUQtyInZone(null,vItemArr,replenzonestatuslist,whlocation,itemfamily,itemgroup,inblocgroup);
	if(inventoryFromReplenZone !=null)
	{
		label:for(k=0;k<inventoryFromReplenZone.length;k++)
		{
			var item = inventoryFromReplenZone[k].getValue('custrecord_ebiz_inv_sku');

			if(itemslist.indexOf(item) == -1)
			{
				itemslist.push(item);
			}			
		}
	}

	if(itemslist=='' || itemslist==null)
		itemslist=vItemArr;

	//var skuQtyInPickfaceLocnsList = getSKUQtyInPickfaceLocnsforReplen(-1,itemslist,whlocation,itemfamily,itemgroup,inblocgroup);
	var serchreplensitems=getopenreplenitems(whlocation);
	var skuQtyInPickfaceLocnsList = getSKUQtyInPickfaceLocnsforReplen(-1,itemslist,whlocation,itemfamily,itemgroup,inblocgroup,serchreplensitems);


	var isQtyAvailinWH = '';
	var enoughAvailArray=new Array();
	//Case# 20149804 starts
//	var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
	//Case# 20149804 ends
	if(skuQtyInPickfaceLocnsList != null && skuQtyInPickfaceLocnsList != ''){
		var vflagtemp=false;
		var temparraynew=new Array();
		var tempindex=0;
		//var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);//Case# 20149381
		nlapiLogExecution('DEBUG', 'skuQtyInPickfaceLocnsList.length', skuQtyInPickfaceLocnsList.length);
		var skuQtyInPickfaceLocns=skuQtyInPickfaceLocnsList;
		var qohListForPFLocations = getQOHForAllSKUsinPFLocation(skuQtyInPickfaceLocns);
		nlapiLogExecution('DEBUG', 'skuQtyInPickfaceLocns.length', skuQtyInPickfaceLocns.length);
		var pendingReplens = pendingReplenQuantity(skuQtyInPickfaceLocns);
		nlapiLogExecution('DEBUG', 'pendingReplens.length', pendingReplens.length);
		for(var k=0;k<skuQtyInPickfaceLocnsList.length;k++)
		{
			

			var Result = skuQtyInPickfaceLocns[k];
			nlapiLogExecution('DEBUG', 'Result Of skuQtyInPickfaceLocns.length', skuQtyInPickfaceLocns.length);

			var skuRecordFromPF = skuQtyInPickfaceLocns[k];

			var item = skuRecordFromPF.getValue('custrecord_pickfacesku');
			var replenQty = skuRecordFromPF.getValue('custrecord_replenqty');
			var maxQty = skuRecordFromPF.getValue('custrecord_maxqty');
			var pfBinLocation = skuRecordFromPF.getValue('custrecord_pickbinloc');
			var minQty = skuRecordFromPF.getValue('custrecord_minqty');
			var replenRuleId = skuRecordFromPF.getValue('custrecord_pickruleid');            
			var site = skuRecordFromPF.getValue('custrecord_pickface_location');
			var pickfacefixedlp = skuRecordFromPF.getText('custrecord_pickface_ebiz_lpno');
			var status=skuRecordFromPF.getText('custrecord_pickface_itemstatus');
				var statusId=skuRecordFromPF.getValue('custrecord_pickface_itemstatus');
			var roundqty = skuRecordFromPF.getValue('custrecord_roundqty');

			// Get QOH at Pickface Location
			var qoh = getInvForLocnAndSKU(qohListForPFLocations, item, pfBinLocation,site);
			
			if(parseFloat(qoh) < parseFloat(maxQty))
			{
				pendingReplens	= getOpenQty(item,pfBinLocation);
			}

			var	openreplensqty=0;
			var	pickqty=0;
			var	putawatqty=0;
			
			var AllocQty = getAllocQtyForLocnAndSKU(qohListForPFLocations, item, pfBinLocation,site);

			nlapiLogExecution('DEBUG', 'qoh', qoh);
			nlapiLogExecution('DEBUG', 'maxQty', maxQty);
			nlapiLogExecution('DEBUG', 'minQty', minQty);
			nlapiLogExecution('DEBUG', 'AllocQty', AllocQty);
nlapiLogExecution('ERROR', 'status', status);
			var AvailQty = parseFloat(qoh) - parseFloat(AllocQty);
			nlapiLogExecution('DEBUG', 'AvailQty', AvailQty);
			
			nlapiLogExecution('DEBUG', 'pendingReplens', pendingReplens);
			if(pendingReplens !=null && pendingReplens!='')
			{
				if(pendingReplens[0][2]!=null && pendingReplens[0][2]!='' && pendingReplens[0][2]!='null' && pendingReplens[0][2]!='unefined')
				{
					openreplensqty = pendingReplens[0][2];
					nlapiLogExecution('DEBUG', 'openreplensqty', openreplensqty);

				}
				if(pendingReplens[0][0]!=null && pendingReplens[0][0]!='' && pendingReplens[0][0]!='null' && pendingReplens[0][0]!='unefined')
				{
					putawatqty = pendingReplens[0][0];
					nlapiLogExecution('DEBUG', 'putawatqty', putawatqty);
				}
				if(pendingReplens[0][2]!=null && pendingReplens[0][1]!='' && pendingReplens[0][1]!='null' && pendingReplens[0][1]!='unefined')
				{
					pickqty = pendingReplens[0][1];
					nlapiLogExecution('DEBUG', 'pickqty', pickqty);
				}
			}

			//var reqdQty = parseFloat(maxQty) - parseFloat(qoh);
			var reqdQty = (parseFloat(maxQty) - (parseFloat(openreplensqty) + parseFloat(qoh)  - parseFloat(pickqty) + parseFloat(putawatqty)));

			var roundreqdqty = Math.floor(parseFloat(reqdQty)/parseFloat(roundqty));
			if(roundreqdqty == null || roundreqdqty == '' || isNaN(roundreqdqty))
				roundreqdqty=0;
			nlapiLogExecution('DEBUG', 'roundreqdqty', roundreqdqty);


			var expQty = getExpectedQtyForPFLocation(pendingReplens, pfBinLocation);
			nlapiLogExecution('DEBUG', 'expQty', expQty);

			//var reqdQty = parseFloat(maxQty) - parseFloat(qoh);

			//if(expQty != -1)
			//	reqdQty = parseFloat(reqdQty) - parseFloat(expQty);

			//case # 20149283 start
			//if(reqdQty > 0 && parseFloat(AvailQty) < parseFloat(minQty)){
			
				//case # 20149283 end
					//var isEnoughAvail = isEnoughInvAvailableInReplenZone(inventoryFromReplenZone, item, reqdQty,statusId);
			
			nlapiLogExecution('DEBUG', 'minQty', minQty);
			// case#201412812
			//if(parseFloat(roundreqdqty) > 0 && parseFloat(AvailQty) <= parseFloat(minQty)){
			if(parseFloat(roundreqdqty) > 0 && parseFloat(AvailQty) < parseFloat(maxQty)){
				var isEnoughAvail = isEnoughInvAvailableInReplenZone(inventoryFromReplenZone, item, reqdQty,pfBinLocation,statusId);

				for(v=0;v<=isEnoughAvail.length;v++)
				{

					if(isEnoughAvail[v]==true)
					{	
						isQtyAvailinWH = isEnoughAvail[v];
						enoughAvailArray[enoughAvailArray.length]=skuQtyInPickfaceLocns[k];
						var itemval = skuQtyInPickfaceLocns[k];

					}
				}


			}		
		}
	}
			var test='';

			if(enoughAvailArray!=null  && enoughAvailArray.length>0)
			{
				var idx = 0;

				var f1 = 0;
				var f2 = 0;
				var f3 = 0;

				t1 = new Date();

				nlapiLogExecution('DEBUG', 'LOOP COUNT', enoughAvailArray.length);

				if(enoughAvailArray.length>25)
				{
					if(vflagtemp==false)
					{
						var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('entry');
						vflagtemp=true;

						pagesize.setDisplaySize(10,10);
						pagesize.setLayoutType('outsidebelow', 'startrow');
						var select= form.addField('custpage_selectpage','select', 'Select Records');	
						select.setLayoutType('outsidebelow', 'startrow');			
						select.setDisplaySize(200,30);
						if (request.getMethod() == 'GET'){
							pagesize.setDefaultValue("25");
							pagesizevalue=25;
						}
						else
						{
							if(request.getParameter('custpage_pagesize')!=null)
							{pagesizevalue= request.getParameter('custpage_pagesize');}
							else
							{pagesizevalue= 25;pagesize.setDefaultValue("25");}
						}
					}
				

					var len=enoughAvailArray.length/parseFloat(pagesizevalue);
					for(var z=1;z<=Math.ceil(len);z++)
					{

						var from;var to;

						to=parseFloat(z)*parseFloat(pagesizevalue);
						from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

						if(parseFloat(to)>enoughAvailArray.length)
						{
							to=enoughAvailArray.length;
							test=from.toString()+","+to.toString(); 
						}

						var temp=from.toString()+" to "+to.toString();
						var tempto=from.toString()+","+to.toString();
						select.addSelectOption(tempto,temp);

					} 
					if (request.getMethod() == 'POST'){

						if(request.getParameter('custpage_selectpage')!=null ){

							select.setDefaultValue(request.getParameter('custpage_selectpage'));	

						}
						if(request.getParameter('custpage_pagesize')!=null ){

							pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

						}
					}
				}
				else
				{
					pagesizevalue=25;
				}
				var minval=0;var maxval=parseFloat(pagesizevalue);
				if(parseFloat(pagesizevalue)>enoughAvailArray.length)
				{
					maxval=enoughAvailArray.length;
				}
				var selectno=request.getParameter('custpage_selectpage');
				if(selectno!=null )
				{
					var temp= request.getParameter('custpage_selectpage');
					var temparray=temp.split(',');

					var diff=parseFloat(temparray[1])-(parseFloat(temparray[0])-1);

					var pagevalue=request.getParameter('custpage_pagesize');

					if(pagevalue!=null)
					{
						if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
						{

							var temparray=selectno.split(',');								 
							minval=parseFloat(temparray[0])-1;							 
							maxval=parseFloat(temparray[1]);

						}
					}
				}

				for(var j = minval; j < maxval; j++){


					var skuRecordFromPF = enoughAvailArray[j];

					var item = skuRecordFromPF.getValue('custrecord_pickfacesku');
					var replenQty = skuRecordFromPF.getValue('custrecord_replenqty');
					var maxQty = skuRecordFromPF.getValue('custrecord_maxqty');
					var pfBinLocation = skuRecordFromPF.getValue('custrecord_pickbinloc');
					var minQty = skuRecordFromPF.getValue('custrecord_minqty');
					var replenRuleId = skuRecordFromPF.getValue('custrecord_pickruleid');            
					var site = skuRecordFromPF.getValue('custrecord_pickface_location');
					var pickfacefixedlp = skuRecordFromPF.getText('custrecord_pickface_ebiz_lpno');
					var status=skuRecordFromPF.getText('custrecord_pickface_itemstatus');
					var statusId=skuRecordFromPF.getValue('custrecord_pickface_itemstatus');
					var roundqty = skuRecordFromPF.getValue('custrecord_roundqty');
					var Quantityresults=getInvForLocnAndSKUNew(qohListForPFLocations, item, pfBinLocation,site,maxval);

					var packcode = skuRecordFromPF.getValue('custrecord_pickfacepackcode');
					var qoh1=0;
					var AllocQty1=0;
					if(Quantityresults!=null && Quantityresults!='')
					{

						qoh1=Quantityresults[0];
						AllocQty1=Quantityresults[1];
					}

					var AvailQty1 = parseInt(qoh1) - parseInt(AllocQty1);					
					nlapiLogExecution('ERROR', 'packcode',packcode);
					itemSubList.setLineItemValue('custpage_replen_sku', idx + 1, item);
					itemSubList.setLineItemValue('custpage_replen_roundqty', idx + 1, roundqty);
					itemSubList.setLineItemValue('custpage_replen_rplnqty', idx + 1, replenQty);
					itemSubList.setLineItemValue('custpage_replen_location', idx + 1, pfBinLocation);
					itemSubList.setLineItemValue('custpage_replen_maxqty', idx + 1, maxQty);
					itemSubList.setLineItemValue('custpage_replen_minqty', idx + 1, minQty);
					itemSubList.setLineItemValue('custpage_replen_qty', idx + 1, qoh1);
					itemSubList.setLineItemValue('custpage_replen_allocqty', idx + 1, AllocQty1);
					itemSubList.setLineItemValue('custpage_replen_availqty', idx + 1, AvailQty1);
					itemSubList.setLineItemValue('custpage_replen_isqtyavailableinwh', idx + 1, isQtyAvailinWH);
					itemSubList.setLineItemValue('custpage_skureplenruleid', idx + 1, replenRuleId);
					itemSubList.setLineItemValue('custpage_whlocation', idx + 1, site);
					itemSubList.setLineItemValue('custpage_pickfacefixedlp', idx + 1, pickfacefixedlp);
					itemSubList.setLineItemValue('custpage_replen_status', idx + 1, status);
					itemSubList.setLineItemValue('custpage_replen_packcode', idx + 1, packcode);
					itemSubList.setLineItemValue('custpage_replen_status_id', idx + 1, statusId);

					idx++;


				}
			}
			else
			{
				nlapiLogExecution('DEBUG', 'no inventory', 'done');
				//var msg= form.addField('custpage_message', 'inlinehtml', null, null, null); //Case# 20149381 
				//msg.setDefaultValue("<div id='div__alert'></div><script>showAlertBox('div__alert', 'ERROR', 'No sufficient inventory available', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");

			}
		//}
	//}


	form.addSubmitButton('Generate Replenishment');
	var end = new Date();
	nlapiLogExecution('DEBUG', 'TOTAL TIME', getElapsedTimeDuration(start, end));
	response.writePage(form);
}


/**
 * 
 * @param request
 * @param response
 */
function genReplenSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Generate Replenishment');
		var varItem=null;

		var location=null; 

		var itemfamily=null ;

		var itemgroup=null; 

		var inblocgroup=null;

		genReplenGETSuitelet(request, response, location, item,itemfamily,itemgroup);
	} 
	else {
		var form = nlapiCreateForm('Generate Replenishment');

		form.setScript('customscript_inventoryclientvalidations');
		
		
		var ItemFamilyGroup = new Array();
		var Reportno = "";
		var inventoryArray = new Array();
		var Pickarray = new Array();

		var lineCount = request.getLineItemCount('custpage_replen_items');
		var availableQty = 0;
		var itemfamily;
		var itemgroup;
		var ruleid;
		nlapiLogExecution('DEBUG','pickfaceDetails',lineCount);
		nlapiLogExecution('DEBUG','itemfam',request.getParameter('custpage_itemfamily'));
		nlapiLogExecution('DEBUG','itemgrp',request.getParameter('custpage_itemgroup'));

		var replenItemArray =  new Array();
		var currentRow = new Array();
		var j = 0;
		var idx = 0;
		var userselection = false;
		if (request.getParameter('custpage_itemfamily') != null && request.getParameter('custpage_itemfamily') != "") {
			itemfamily = request.getParameter('custpage_itemfamily');
		}
		if (request.getParameter('custpage_itemgroup') != null && request.getParameter('custpage_itemgroup') != "") {
			itemgroup = request.getParameter('custpage_itemgroup');
		}

		if (request.getParameter('custpage_ruleid') != null && request.getParameter('custpage_ruleid') != "") {
			ruleid = request.getParameter('custpage_ruleid');
		}

		for(var i = 1; i <= lineCount; i++){
			var selectValue = request.getLineItemValue('custpage_replen_items', 'custpage_replen_sel', i);
			if (selectValue == 'T') 
			{
				userselection = true;	        	
			}

			if (selectValue == 'T') {
				var item = request.getLineItemValue('custpage_replen_items', 'custpage_replen_sku', i);
				var replenQty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_rplnqty', i);
				var replenRule = request.getLineItemValue('custpage_replen_items', 'custpage_skureplenruleid', i);
				var maxQty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_maxqty', i);
				var minQty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_minqty', i);
				var pickfaceQty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_qty', i);
				var whLocation = request.getLineItemValue('custpage_replen_items', 'custpage_whlocation', i);
				var actEndLocation = request.getLineItemValue('custpage_replen_items', 'custpage_replen_location', i);
				var pickfacefixedlp = request.getLineItemValue('custpage_replen_items', 'custpage_pickfacefixedlp', i);
				var status = request.getLineItemValue('custpage_replen_items', 'custpage_replen_status', i);
				var pfroundqty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_roundqty', i);
				var statusId = request.getLineItemValue('custpage_replen_items', 'custpage_replen_status_id', i);

				var putawatqty=0;
				var pickqty=0;
				var openreplensqty=0;
				var Qohinpickface=0;
				nlapiLogExecution('DEBUG','before  ifw',openreplensqty);
				nlapiLogExecution('DEBUG','before  if',parseFloat(openreplensqty));
				var serchreplensqty=getopenreplens(item,actEndLocation,whLocation);
				if(serchreplensqty!=null && serchreplensqty !='')
				{
					for(k=0;k<serchreplensqty.length;k++)
					{
						openreplensqty += serchreplensqty[k].getValue('custrecord_expe_qty');
					}
				}

				var searchQohinpickface=getQOHinPFLocations(item,actEndLocation);
				if(searchQohinpickface!=null && searchQohinpickface!='')
				{
					Qohinpickface = searchQohinpickface[0].getValue('custrecord_ebiz_qoh',null,'sum');
				}
				var searchputawyqty=getOpenputawayqty(item,actEndLocation);
				if(searchputawyqty!=null && searchputawyqty!='')
				{
					putawatqty = searchputawyqty[0].getValue('custrecord_expe_qty',null,'sum');
				}
				var searchpickyqty=getOpenPickqty(item,actEndLocation);
				if(searchpickyqty!=null && searchpickyqty!='')
				{
					pickqty = searchpickyqty[0].getValue('custrecord_expe_qty',null,'sum');
				}

				if(Qohinpickface==null || Qohinpickface=='')
					Qohinpickface=0;

				if(putawatqty==null || putawatqty=='')
					putawatqty=0;

				if(pickqty==null || pickqty=='')
					pickqty=0;

				nlapiLogExecution('DEBUG','putawatqty',putawatqty);
				nlapiLogExecution('DEBUG','pickqty',pickqty);
				nlapiLogExecution('DEBUG','pickfaceQty',pickfaceQty);
				nlapiLogExecution('DEBUG','replenQty',replenQty);
				nlapiLogExecution('DEBUG','openreplensqty',openreplensqty);
				nlapiLogExecution('DEBUG','Qohinpickface',Qohinpickface);
				nlapiLogExecution('DEBUG','maxQty',maxQty);
				nlapiLogExecution('DEBUG','pfroundqty',pfroundqty);
				if(Qohinpickface==null || Qohinpickface=='')
					Qohinpickface=0;

				var remQtyToCreate = parseFloat(maxQty) - parseFloat(Qohinpickface);

				var replenTaskCount = Math.ceil(parseFloat(remQtyToCreate) / parseFloat(replenQty));

				var qtyToFind = parseFloat(replenTaskCount) * parseFloat(replenQty);				

				var tempQty = (parseFloat(maxQty) - (parseFloat(openreplensqty) + parseFloat(Qohinpickface)  - parseFloat(pickqty) + parseFloat(putawatqty)));

				if(pfroundqty==null || pfroundqty=='')
					pfroundqty=1;

				var tempwithRoundQty = Math.floor(parseFloat(tempQty)/parseFloat(pfroundqty));

				if(tempwithRoundQty == null || tempwithRoundQty == '' || isNaN(tempwithRoundQty))
					tempwithRoundQty=0;

				tempQty = parseFloat(pfroundqty)*parseFloat(tempwithRoundQty);

				currentRow = [item, replenQty, replenRule, maxQty, minQty, pickfaceQty, whLocation, 
				              actEndLocation, tempQty, replenTaskCount, pickfacefixedlp,status,statusId,pfroundqty];

				if(idx == 0)
					idx = currentRow.length;

				replenItemArray[j++] = currentRow;
			}
		}
		if(userselection==true)
		{
			var taskCount;
			var zonesArray = new Array();

			var replenRulesList = getListForAllReplenZones();

			var replenZoneList = "";
			var replenZoneTextList = "";
			var replenzonestatuslist="";
			var replenzonestatustextlist="";


			if (replenRulesList != null){
				for (var i = 0; i < replenRulesList.length; i++){
					nlapiLogExecution('DEBUG','replenRulesList.length',replenRulesList.length);
					if (i == 0){
						replenZoneList += replenRulesList[i].getValue('custrecord_replen_strategy_zone');
						replenZoneTextList += replenRulesList[i].getText('custrecord_replen_strategy_zone');
						replenzonestatuslist +=replenRulesList[i].getValue('custrecord_replen_strategy_itemstatus');
						replenzonestatustextlist +=replenRulesList[i].getText('custrecord_replen_strategy_itemstatus');
					} else {
						replenZoneList += ',';
						replenZoneList += replenRulesList[i].getValue('custrecord_replen_strategy_zone');

						replenZoneTextList += ',';
						replenZoneTextList += replenRulesList[i].getText('custrecord_replen_strategy_zone');

						replenzonestatuslist += ',';
						replenzonestatuslist += replenRulesList[i].getValue('custrecord_replen_strategy_itemstatus');

						replenzonestatustextlist += ',';
						replenzonestatustextlist += replenRulesList[i].getText('custrecord_replen_strategy_itemstatus');
					}
				}
			} 

			var itemList = buildItemListForReplenProcessing(replenItemArray, j);

			var locnGroupList = getZoneLocnGroupsList(replenZoneList,whLocation);	
			var ZoneAndLocationGrp =getZoneLocnGroupsListforRepln(replenZoneList,whLocation);
			nlapiLogExecution('DEBUG','ZoneAndLocationGrp',ZoneAndLocationGrp);

			var inventorySearchResults = getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslist, whLocation);
			nlapiLogExecution('DEBUG','inventorySearchResults',inventorySearchResults);

			if (inventorySearchResults != null && inventorySearchResults != ""){


				if (Reportno == null || Reportno == "") {
					Reportno = GetMaxTransactionNo('REPORT');
					Reportno = Reportno.toString();
				}


				var stageLocation='';

				nlapiLogExecution('DEBUG','Reportno',Reportno);
				var taskpriority=5;
				var resultArray =	replenRuleZoneLocation(inventorySearchResults, replenItemArray, Reportno, stageLocation, whLocation,'','',taskpriority,ZoneAndLocationGrp);	
				
				var result=resultArray[0];
				nlapiLogExecution('DEBUG','result',result);

				if(result==true)
				{
					showInlineMessage(form, 'Confirmation', 'Replenishment Report No', Reportno);
					response.writePage(form);
				}
				else
				{

					showInlineMessage(form, 'DEBUG', 'Confirm open replens / Putaway', null);
					response.writePage(form);
				}

			}
			else{

				showInlineMessage(form, 'DEBUG', 'Inventory not available in the replenishment zone(s)', null);
				response.writePage(form);
			}
		}
		else
		{
			nlapiLogExecution('DEBUG','itemfamily',itemfamily);
			nlapiLogExecution('DEBUG','itemgroup',itemgroup);
			genReplenGETSuitelet(request, response, location, item,itemfamily,itemgroup);
		}

	}
}

/**
 * 
 * @param reportno
 * @param vpickmethod
 * @param vpickzone
 * @param wmsstatus
 */
function clusterpicking(reportno, vpickmethod, vpickzone,wmsstatus){
	//Fectching distinct Pick Zone and Pick Method 
	var fields = ['custrecord_replenishment_cluster', 'custrecord_replenishment_noofpicks'];
	var columns = nlapiLookupField('customrecord_ebiznet_replenish_method', vpickmethod, fields);
	var chkcluster = columns.custrecord_replenishment_cluster;
	var vmaxPicks = columns.custrecord_ebiz_pickmethod_maxpicks;
	var vnoofpicks = 0;
	nlapiLogExecution('DEBUG', 'chkcluster', chkcluster);

	if (chkcluster == 'T') {
		var columnsPicktsk = new Array();
		columnsPicktsk[0] = new nlobjSearchColumn('custrecord_ebizzone_no');
		columnsPicktsk[1] = new nlobjSearchColumn('custrecord_ebizmethod_no');
		columnsPicktsk[2] = new nlobjSearchColumn('name');
		columnsPicktsk[2].setSort();

		var filtersPicktsk = new Array();
		filtersPicktsk[0] = new nlobjSearchFilter('name', null, 'is', ebizwaveno);
		filtersPicktsk[1] = new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', reportno);
		filtersPicktsk[2] = new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is', vpickmethod);
		filtersPicktsk[3] = new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is', vpickmethod);
		filtersPicktsk[4] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '8');
		filtersPicktsk[5] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', wmsstatus);

		var prevordNo = "";
		var vclusterNo = "";

		var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPicktsk, columnsPicktsk);
		for (var s = 0; searchresultsinvt != null && s < searchresultsinvt.length; s++) {

			vnoofpicks = vnoofpicks + 1;
			nlapiLogExecution('DEBUG', 'vnoofOrders', vnoofOrders);

			if (vclusterNo == "") {
				vclusterNo = GetMaxClusterNo();
			}
			else {
				if ((vmaxPicks != null && !isNaN(vmaxPicks))) {
					if (vmaxPicks < vnoofpicks) {
						vclusterNo = GetMaxClusterNo();
						vnoofpicks = 1;
					}
				}
			}

			//Updating Cluster No
			var fields = new Array();
			var values = new Array();
			fields[0] = 'custrecord_ebiz_clus_no';
			values[0] = vclusterNo.toString();
			var updatefields = nlapiSubmitField('customrecord_ebiznet_trn_opentask', searchresultsinvt[s].getId(), fields, values);
		}
	}
}

/**
 * 
 * @param itemid
 * @param location
 * @returns
 */
function getRecord(itemid, location){

	var filtersso = new Array();
	var socount = 0;

	filtersso[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemid);
	filtersso[1] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'is', location);
	filtersso[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if (searchresultsLine != null) {
		nlapiLogExecution('DEBUG', 'Record count', searchresultsLine.length);
		socount = searchresultsLine.length;
	}
	return searchresultsLine;
}

/**
 * 
 * @param itemid
 * @param location
 * @returns
 */
function getInventoryRecord(itemid, location){
	var filtersso = new Array();
	var socount = 0;

	filtersso[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemid);
	filtersso[1] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', '[' + location + ']');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if (searchresultsLine != null) {
		socount = searchresultsLine.length;
	}
	return searchresultsLine;
}
function getOpenPickqty(item,actEndLocation)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	filters[3] = new nlobjSearchFilter('custrecord_sku', null, 'anyof',item);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}
function getOpenputawayqty(item,actEndLocation)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	filters[3] = new nlobjSearchFilter('custrecord_sku', null, 'anyof',item);


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

function getopenreplens(fulfilmentItem,pfLocationId,pfWHLoc)
{

	nlapiLogExecution('DEBUG','fulfilmentItem',fulfilmentItem);
	nlapiLogExecution('DEBUG','pfLocationId',pfLocationId);
	nlapiLogExecution('DEBUG','pfWHLoc',pfWHLoc);
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_sku', null,'anyof' ,fulfilmentItem));	
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null,'is' ,pfLocationId));
	if(pfWHLoc != null && pfWHLoc != '')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null,'anyof' ,pfWHLoc));


	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}
function getQOHinPFLocations(itemid,binlocation){

	nlapiLogExecution('DEBUG', 'Into getQOHinPFLocation');
	nlapiLogExecution('DEBUG', 'itemid',itemid);
	nlapiLogExecution('DEBUG', 'binlocation',binlocation);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', binlocation));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemid));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));

	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));	

	var columns = new Array();		
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('DEBUG', 'Out of pfSKUInvList',pfSKUInvList);
	return pfSKUInvList;
}

function getPFLocationListForQueryitems(skuQtyInPickfaceLocnsList){

	var pfitemsList = new Array();
	if(skuQtyInPickfaceLocnsList!=null && skuQtyInPickfaceLocnsList!='')
	{
		for(var i = 0; i < skuQtyInPickfaceLocnsList.length; i++){
			pfitemsList.push(skuQtyInPickfaceLocnsList[i].getValue('custrecord_pickfacesku'));
		}
	}

	if(pfitemsList!=null && pfitemsList!='')
	{
		pfitemsList=removeDuplicateElement(pfitemsList);
	}

	nlapiLogExecution('DEBUG','Out of getPFLocationListForQuery',pfitemsList);

	return pfitemsList;
}
function getopenreplenitems(pfWHLoc)
{


	nlapiLogExecution('ERROR','pfWHLoc',pfWHLoc);
	var filters = new Array();
	var itemarray=new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', controlno); // Remove this from where clause
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8,2])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20,2])); //Cyclecount released
	if(pfWHLoc != null && pfWHLoc != '')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null,'anyof' ,pfWHLoc));


	var columns = new Array();
	//columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');
	columns[0] = new nlobjSearchColumn('custrecord_sku',null ,'group');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!='')
	{
		for (var t = 0; t < searchresults.length; t++){
			itemarray.push(searchresults[t].getValue('custrecord_sku',null ,'group'));
		}
	}
	return itemarray;
}
function getOpenQty(item,actEndLocation)
{
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', actEndLocation);
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['2','3','8']);				
	filters[1] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	filters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof',item);
	filters[3]= new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 9, 20, 21]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_tasktype',null,'group');
	columns[2] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);



	var pendingTaskArray = new Array();
	var pendingopenPutawayTasks =0;
	var pendingopenPickTasks =0;
	var pendingopenReplenTasks=0;

	if(searchresults !=null && searchresults !='')
	{
		for(var m=0;m<searchresults.length;m++)
		{
			var tasktype=searchresults[m].getValue('custrecord_tasktype',null,'group');

			if(tasktype==2 && (actEndLocation == searchresults[m].getValue('custrecord_actbeginloc',null,'group')))
			{
				pendingopenPutawayTasks +=parseFloat(searchresults[m].getValue('custrecord_expe_qty',null,'sum'));
			}
			else if(tasktype==3 && (actEndLocation == searchresults[m].getValue('custrecord_actbeginloc',null,'group')))
			{
				pendingopenPickTasks +=parseFloat(searchresults[m].getValue('custrecord_expe_qty',null,'sum'));
			}
			else if(tasktype==8 && (actEndLocation == searchresults[m].getValue('custrecord_actendloc',null,'group')))
			{
				pendingopenReplenTasks +=parseFloat(searchresults[m].getValue('custrecord_expe_qty',null,'sum'));
			}
			else
			{

			}

		}
	}
	pendingTaskArray[0]=[pendingopenPutawayTasks,pendingopenPickTasks,pendingopenReplenTasks];
	return pendingTaskArray;
}