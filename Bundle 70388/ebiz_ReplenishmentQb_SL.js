/***************************************************************************
 eBizNET Solutions Inc .
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_ReplenishmentQb_SL.js,v $
 *     	   $Revision: 1.3.10.2.4.4 $
 *     	   $Date: 2013/09/11 12:17:20 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *****************************************************************************/

function ebiznet_replenishmentConfirmlist_SL(request, response){
	
	var list = nlapiCreateList('Replenishment List');
	list.setStyle('normal');
	var column = list.addColumn('name', 'text', 'Report#', 'LEFT');
	column.setURL(nlapiResolveURL('SUITELET', 'customscript_confirm_replenishment', 'customdeploy_confirm_replenishment'));
	column.addParamToURL('custparam_reportid', 'name', true);
	list.addColumn('custrecord_sku', 'text', 'Item', 'LEFT');
	list.addColumn('custrecordact_begin_date', 'text', 'Created Date & Time ', 'LEFT');
	list.addColumn('custrecord_expe_qty', 'integer', 'Quantity', 'LEFT');
	

//	var filters = new Array();
//	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);	//	8 = RPLN Task
//	// WMS Status Flag: G(9)/R(20)
//	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9, 20]);

//	var columns = new Array();
//	columns[0] = new nlobjSearchColumn('name');
//	columns[1] = new nlobjSearchColumn('custrecord_sku');
//	columns[2] = new nlobjSearchColumn('custrecord_sku_status');
//	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');

//	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	var searchresults = getConfirmreplenNo('-1');
	if(searchresults != null)
		nlapiLogExecution('ERROR', 'into searchresults length', searchresults.length);

	var replenishment = new Object();
	var dupliarray = new Array();
	var redarray = new Array();
	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		redarray[i] = searchresults[i].getValue('name');
	}

	
	var redarray = removeDuplicateElement(redarray);
	for (var i = 0; i < redarray.length; i++) {
		var TotalExpQty = 0;
		for (var j = 0; searchresults != null && j < searchresults.length; j++) {
			if (redarray[i] == searchresults[j].getValue('name')) {
				replenishment["name"] = searchresults[j].getValue('name');
				replenishment["custrecord_sku"] = searchresults[j].getText('custrecord_sku');
				replenishment["custrecord_sku_status"] = searchresults[j].getText('custrecord_sku_status');
				replenishment["custrecord_ebiz_cntrl_no"] = searchresults[j].getValue('custrecord_ebiz_cntrl_no');
				//replenishment["custrecordact_begin_date"] = searchresults[j].getValue('custrecordact_begin_date');	
				
				replenishment["custrecordact_begin_date"] = searchresults[j].getValue('custrecordact_begin_date') + " " + searchresults[j].getValue('custrecord_actualbegintime');
				
				replenishment["custrecord_expe_qty"] = searchresults[j].getValue('custrecord_expe_qty');
				TotalExpQty = parseInt(TotalExpQty) + parseInt(searchresults[j].getValue('custrecord_expe_qty'));
				replenishment["custrecord_expe_qty"] = parseInt(TotalExpQty);
			}
		}
		list.addRow(replenishment);
	}
	response.writePage(list);
}

function removeDuplicateElement(arrayName){
	var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < newArray.length; j++) {
			if (newArray[j] == arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}



//function genReplenGETSuitelet(request, response)
//{
//	var form = nlapiCreateForm('Generate Replenishment');
//
//	//Adding  ItemSubList
//	var soItem = form.addField('custpage_item', 'select', 'Item','item');
//	soItem.setLayoutType('startrow', 'none'); 		
//
//	var soLocation = form.addField('custpage_location', 'select', 'Location','location');
//	soLocation.setLayoutType('startrow', 'none');    
//	form.addSubmitButton('Generate Replenishment');
//	response.writePage(form);
//}
//
//function genReplenSuitelet(request, response){
//	if (request.getMethod() == 'GET') {
//		//create the Suitelet form
//		genReplenGETSuitelet(request, response); 
//	}
//	else {
//		var itemno = request.getParameter('custpage_item');
//		var whLocation = request.getParameter('custpage_location');		 
//		var replnArray = new Array();
//		replnArray["custpage_item"] = itemno;
//		replnArray["custpage_location"]=whLocation;
//
//		response.sendRedirect('SUITELET', 'customscript_generate_replen_suitlet', 'customdeploy_generate_replen_suitlet_di', false, replnArray);
//	}
//}


var Resdetails;
function getConfirmreplenNo(maxno)
{
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));	//	8 = RPLN Task
//	WMS Status Flag: G(9)/R(20)
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9, 20]));

	if(maxno!=null && maxno!='' && maxno!='-1')
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	else
		Resdetails = new Array();

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_sku_status');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('id');
	columns[5] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[6] = new nlobjSearchColumn('custrecordact_begin_date');
	columns[7] = new nlobjSearchColumn('custrecord_actualbegintime');
	

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if( searchresults!=null && searchresults.length>=1000)
	{ 
		for(var s=0;s<searchresults.length;s++)
		{	
			Resdetails.push(searchresults[s]);
		}
		var maxno=searchresults[searchresults.length-1].getValue('id');
		getConfirmreplenNo(maxno);	
	}
	else
	{
		if( searchresults!=null && searchresults!=''){
			for(var s=0;s<searchresults.length;s++)
			{	
				Resdetails.push(searchresults[s]);
			} 
		}
	}
	nlapiLogExecution('ERROR', 'out Resdetails', Resdetails);
	return Resdetails;
}