/***************************************************************************
	  		   eBizNET
                7HILLS BUSINESS SOLUTION LTD
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Client/Attic/ebiz_Itemreceipt_CL.js,v $
 *     	   $Revision: 1.1.2.3.2.1 $
 *     	   $Date: 2014/12/31 13:22:29 $
 *     	   $Author: spendyala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *$Log: ebiz_Itemreceipt_CL.js,v $
 *Revision 1.1.2.3.2.1  2014/12/31 13:22:29  spendyala
 *Prod issue fixes
 *
 *Revision 1.1.2.3  2014/04/22 16:35:52  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 *Item receipt
 *
 *Revision 1.18.2.15  2012/09/05 14:28:23  schepuri
 *CASE201112/CR201113/LOG201121
 *added date stamp
 *
 *Revision 1.18.2.14  2012/08/30 09:21:36  skreddy
 *CASE201112/CR201113/LOG201121
 *Restricted to changed the item after Wave Generation.
 *
 *Revision 1.18.2.13  2012/08/08 16:56:34  spendyala
 *CASE201112/CR201113/LOG201121
 *Issue related to delete Line while creating new SO is resolved.
 *
 *Revision 1.18.2.12  2012/06/04 14:53:52  spendyala
 *CASE201112/CR201113/LOG201121
 *issue related to parsing the qty.
 *
 *Revision 1.18.2.11  2012/05/25 06:44:58  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 *Subtotaol item validations
 *
 *Revision 1.18.2.10  2012/05/22 07:02:45  rrpulicherla
 *Bypass qty validation
 *
 *Revision 1.18.2.7  2012/04/11 12:28:29  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 *default sku status
 *
 *Revision 1.18.2.6  2012/04/03 14:49:30  spendyala
 *CASE201112/CR201113/LOG201121
 *Issue Related to Delete Fulfillment order is fixed.
 *
 *Revision 1.18.2.5  2012/02/20 15:24:24  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Stable bundle issue fixes
 *
 *Revision 1.18.2.4  2012/02/09 13:06:35  schepuri
 *CASE201112/CR201113/LOG201121
 *issue fixing  related memo textbox validation removed
 *
 *Revision 1.18.2.3  2012/02/07 12:32:38  snimmakayala
 *CASE201112/CR201113/LOG201121
 *UOM Conversion
 *
 *Revision 1.18.2.2  2012/02/02 13:44:21  schepuri
 *CASE201112/CR201113/LOG201121
 *issue fixing related to validation on textbox
 *
 *Revision 1.18.2.1  2012/01/31 13:12:11  schepuri
 *CASE201112/CR201113/LOG201121
 *issue fixing related to validation on textbox
 *
 *Revision 1.18  2011/12/05 14:58:59  snimmakayala
 *CASE201112/CR201113/LOG201121
 *
 *Revision 1.17  2011/11/22 15:25:35  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Code Fine Tunning
 *
 *Revision 1.16  2011/11/22 08:42:55  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Default Lot Functionality. (Item as Lot)
 *
 *Revision 1.15  2011/11/13 00:16:15  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Outbound Inventory Deletion
 *
 *Revision 1.14  2011/11/09 14:47:15  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Ship Complete Functionality
 *
 *Revision 1.13  2011/11/02 19:18:27  gkalla
 *CASE201112/CR201113/LOG201121
 *To populate SKU status and packcode while change the item in SO Line
 *
 *Revision 1.12  2011/11/02 16:17:23  gkalla
 *CASE201112/CR201113/LOG201121
 *To populate SKU status and packcode while change the item in SO Line
 *
 *Revision 1.10  2011/09/30 09:00:33  skdokka
 *CASE201112/CR201113/LOG201121
 *In the onChange function, moved load record in side the if condition
 *
 *Revision 1.9  2011/09/26 11:35:57  rmukkera
 *CASE201112/CR201113/LOG201121
 *hold flag message was deleted
 *
 *Revision 1.8  2011/09/23 14:20:50  rmukkera
 *CASE201112/CR201113/LOG201121
 *Hold Flags condition message added
 *
 *Revision 1.7  2011/09/19 14:03:03  snimmakayala
 *CASE201112/CR201113/LOG201121
 *
 *Revision 1.6  2011/09/09 15:12:14  spendyala
 *Added On Item change  function for auto fulfillment
 *
 *Revision 1.5  2011/09/09 07:08:33  spendyala
 *CASE201112/CR201113/LOG201121
 *functionality added related to auto creation of fulfillment order.
 *
 *Revision 1.4  2011/09/06 22:07:28  skota
 *CASE201112/CR201113/LOG201121
 *
 *Revision 1.3  2011/06/17 14:46:46  skota
 *CASE201112/CR201113/LOG201121
 *code changes in function 'CheckFulfillmentqty' for not allowing the user to enter -ve values etc.,
 *
 *****************************************************************************/

/**
 * To empty the custombody field
 */


var eventtype;
function setDefaultValue(type)
{
	eventtype=type;

}
function onSave()
{
	var Soid=nlapiGetFieldValue('createdfrom');

	var lineCnt = nlapiGetLineItemCount('item');
	var Trantype = nlapiLookupField('transaction', Soid, 'recordType');
	var ItemArray=new Array();
	for (var vcnt = 1; vcnt <= lineCnt; vcnt++) 
	{
		var Itemreceiptcheckval=nlapiGetLineItemValue('item','itemreceive', vcnt);
		if(Itemreceiptcheckval=='T' && Trantype!='returnauthorization')
		{
			ItemArray.push(nlapiGetLineItemValue('item','item',vcnt));
		}
	}

	if(ItemArray!=null&&ItemArray!="")
	{
		var filter=new Array();
		filter[0]=new nlobjSearchFilter("internalid",null,"anyof",ItemArray);

		var column=new Array();
		column[0]=new nlobjSearchColumn("internalid");
		column[1]=new nlobjSearchColumn("type");

		var Itemsearchres=nlapiSearchRecord("item",null,filter,column);
		var oldlocation=null;
		var mwhsiteflag='F';
		for (var s = 1; s <= lineCnt; s++) 
		{
			var Itemreceiptcheckval=nlapiGetLineItemValue('item','itemreceive', s);
			if(Itemreceiptcheckval=='T' && Trantype!='returnauthorization')
			{
				var qty=nlapiGetLineItemValue('item','quantity',s);
				var itemtype=nlapiGetLineItemValue('item','itemtype',s);
				var itemid=nlapiGetLineItemValue('item','item',s);
				var location=nlapiGetLineItemValue('item','location',s);
				var itemtype="";
				for(var itemcnt=0;itemcnt<Itemsearchres.length;itemcnt++)
				{
					if(itemid==Itemsearchres[itemcnt].getValue("internalid"))
					{
						itemtype=Itemsearchres[itemcnt].getValue("type");
						break;
					}
				}
//				var vitemSubtype = nlapiLookupField('item', itemid, ['recordType']);
//				var itemtype=vitemSubtype.recordType;
				if((location!=null && location!='')&&(oldlocation!=location))
				{
					var fields = ['custrecord_ebizwhsite'];

					var locationcolumns = nlapiLookupField('Location', location, fields);
					if(locationcolumns!=null)
						mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
					oldlocation=location;
				}

				// alert('itemtype= ' + itemtype);
				//alert('location= ' + location);
				//alert('Itemreceiptcheckval= ' + Itemreceiptcheckval);
				//alert('eventtype= ' + eventtype);
				//alert('mwhsiteflag= ' + mwhsiteflag);
				if(eventtype=='create' || eventtype=='copy' )
				{
					if((itemtype=='InvtPart') &&  (Itemreceiptcheckval=='T') && (mwhsiteflag=='T') && (Trantype=='purchaseorder' || Trantype=='transferorder'))
					{
						alert("You can't receive Inventory Items Directly in Netsuite" );							
						return false;
					}
					//	else
					//	{
					//return true;
					//	}
				}
			}
		}
	}
	return true;
}

