/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_SerialNumberAdjustment.js,v $
 *     	   $Revision: 1.1.2.2.2.5.2.3 $
 *     	   $Date: 2015/02/17 13:09:29 $
 *     	   $Author: schepuri $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_223 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_SerialNumberAdjustment.js,v $
 * Revision 1.1.2.2.2.5.2.3  2015/02/17 13:09:29  schepuri
 * issue# 201411352,201411373
 *
 * Revision 1.1.2.2.2.5.2.2  2014/09/25 16:05:21  sponnaganti
 * Case# 201410495
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.2.2.5.2.1  2014/08/12 15:39:55  skavuri
 * Case# 20149919 SB Issue Fixed
 *
 * Revision 1.1.2.2.2.5  2014/04/04 14:04:56  skavuri
 * Case # 20127901 issue fixed
 *
 * Revision 1.1.2.2.2.4  2014/03/26 15:44:21  nneelam
 * case#  20127783
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.2.3  2013/06/24 15:33:33  skreddy
 * Case#:20122755
 * to update Serial numbers with new LP
 *
 * Revision 1.1.2.2.2.2  2013/03/19 11:53:20  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.2.2.1  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.2  2013/02/06 01:09:24  kavitha
 * CASE201112/CR201113/LOG201121
 * Serial # functionality - Inventory process
 *
 * Revision 1.1.2.1  2012/12/11 03:29:23  kavitha
 * CASE201112/CR201113/LOG201121
 * Incorporated Serial # functionality
 *
 *
 *****************************************************************************/

function SerialNumberAdjustment(request, response)
{
	if (request.getMethod() == 'GET') 
	{   
		var form = nlapiCreateForm('Serial Numbers list');
		
		var getSKU = request.getParameter('custparam_serialsku');
		nlapiLogExecution('ERROR', 'getSKU', getSKU);
		
		var getSKUId = request.getParameter('custparam_serialskuid');
		nlapiLogExecution('ERROR', 'getSKUId', getSKUId);

		var getLP= request.getParameter('custparam_serialskulp');
		nlapiLogExecution('ERROR', 'getLP', getLP);

		var getQty= request.getParameter('custparam_serialskuqty');
		nlapiLogExecution('ERROR', 'getQty', getQty);

		var getArrLP = new Array();
		getArrLP = request.getParameter('custparam_serialskuarrlp');
		nlapiLogExecution('ERROR', 'getArrLP', getArrLP);
		//case# 201410495 (Getting flag from adjustment screen for not to delete remaining serial numbers)
		var getAdjustFlag= request.getParameter('custparam_serialadjustflag');
		nlapiLogExecution('ERROR', 'getAdjustFlag', getAdjustFlag);
		//createform(form,getSKU,getSKUId,getLP,getQty,[getArrLP]);
		createform(form,getSKU,getSKUId,getLP,getQty,[getArrLP],getAdjustFlag);
		var autobutton = form.addSubmitButton('Submit');
		response.writePage(form);
	}
	else
	{
		try 
		{			
			var lineCount = request.getLineItemCount('custpage_seriallist');
			var form = nlapiCreateForm('Serial Numbers List');			
			
			var LP = request.getParameter('custpage_seriallp');
			var ReqQty = request.getParameter('custpage_serialqty');
			nlapiLogExecution('ERROR', 'LP', LP);
			nlapiLogExecution('ERROR', 'lineCount', lineCount);
			var count =0;

			if(ReqQty) 
			{
				var getSKU = request.getParameter('custpage_serialsku');
				nlapiLogExecution('ERROR', 'getSKU', getSKU);
				
				var getSKUId = request.getParameter('custpage_serialskuid');
				nlapiLogExecution('ERROR', 'getSKUId', getSKUId);

				var getLP = request.getParameter('custpage_seriallp');
				nlapiLogExecution('ERROR', 'getLP', getLP);

				var getQty = request.getParameter('custpage_serialqty');
				nlapiLogExecution('ERROR', 'getQty', getQty);	
				
				var getArrLP = request.getParameter('custpage_serialarrlp');
				nlapiLogExecution('ERROR', 'getArrLP', getArrLP);
			
				var getAdjustFlag = request.getParameter('custpage_serialadjustflag');
				nlapiLogExecution('ERROR', 'getAdjustFlag', getAdjustFlag);
				
				var serialno;
				var InternalID;
				
				for (var k=1; k<=lineCount; k++)
				{
					if(request.getLineItemValue('custpage_seriallist', 'custpage_serialcheckbox', k) == 'T'){
						count++;
					}
				}
				nlapiLogExecution('ERROR', 'count', count);
				nlapiLogExecution('ERROR', 'ReqQty', ReqQty);
				if(parseInt(count) == parseInt(ReqQty))
				{
					for (var k=1; k<=lineCount; k++)
					{				
						if(request.getLineItemValue('custpage_seriallist', 'custpage_serialcheckbox', k) == 'T')
						{
							serialno = request.getLineItemValue('custpage_seriallist', 'custpage_serial_serialno', k);
							InternalID = request.getLineItemValue('custpage_seriallist', 'custpage_serial_internalid', k);
							nlapiLogExecution('ERROR', 'serialno ', serialno);
							nlapiLogExecution('ERROR', 'InternalID ', InternalID);						
							
							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialstatus', 'D');
		
							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);
							//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);								
						}//case # 20127783�
						else if(request.getLineItemValue('custpage_seriallist', 'custpage_serialcheckbox', k) == 'F'){
					//case# 201410495 (Not to delete serial numbers for adjustment)
							if(getAdjustFlag!="T" && getAdjustFlag!= "" && getAdjustFlag!= null)// case # 201411373,201411352
							{
								//Case# 20149919 starts
								InternalID = request.getLineItemValue('custpage_seriallist', 'custpage_serial_internalid', k);
								var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
								//Case# 20149919 ends
							}
						}
						
						if(getArrLP != null && getArrLP != 'undefined' && getArrLP.length > 0 && getArrLP != 'null' && getArrLP != '')
						{
							if(request.getLineItemValue('custpage_seriallist', 'custpage_serialcheckbox', k) == 'T')
							{
								var serialno1 = request.getLineItemValue('custpage_seriallist', 'custpage_serial_serialno', k);
								var InternalID1 = request.getLineItemValue('custpage_seriallist', 'custpage_serial_internalid', k);
								nlapiLogExecution('ERROR', 'serialno ', serialno1);
								nlapiLogExecution('ERROR', 'InternalID ', InternalID1);						
								
								var LoadSerialNumbers1 = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID1);
								LoadSerialNumbers1.setFieldValue('custrecord_serialstatus', 'A');
			
								var recid = nlapiSubmitRecord(LoadSerialNumbers1, false, true);
								nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);								
							}	
						}
					}
					showInlineMessage(form, 'Confirmation', 'Serial Number(s) Selected Successfully', null);
				}
				else
				{
					var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
					msg.setLayoutType('outside','startcol');
					
					nlapiLogExecution('ERROR', 'Please select required number of Serial numbers');
					//createform(form,getSKU,getSKUId,getLP,getQty,[getArrLP]);
					createform(form,getSKU,getSKUId,getLP,getQty,[getArrLP],getAdjustFlag);
					//showInlineMessage(form, 'Error', 'Please select required number of Serial numbers', null);
					msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'Please select required number of Serial numbers', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
					var autobutton = form.addSubmitButton('Submit');
				}
			}			
			response.writePage(form);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception', exp);
		}
	}
}


//function createform(form,getSKU,getSKUId,getLP,getQty,getArrLP)
function createform(form,getSKU,getSKUId,getLP,getQty,getArrLP,getAdjustFlag)
{
	nlapiLogExecution('ERROR', 'createform', null);
	
	var skuField = form.addField('custpage_serialsku', 'text', 'Item :');			
	skuField.setDisplayType('inline');
	skuField.setLayoutType('startrow');
	
	var lpField = form.addField('custpage_seriallp', 'text', 'LP :');
	lpField.setDisplayType('hidden');
	
	var QtyField = form.addField('custpage_serialqty', 'text', 'Quantity :');
	QtyField.setDisplayType('inline');
	
	var skuIDField = form.addField('custpage_serialskuid', 'text', 'Item :');			
	skuIDField.setDisplayType('hidden');
	
	var ArrlpField = form.addField('custpage_serialarrlp', 'text', 'Arr LP :');
	ArrlpField.setDisplayType('hidden');
	
	var AdjustFlagField = form.addField('custpage_serialadjustflag', 'text', 'Adjust Flag :');
	AdjustFlagField.setDisplayType('hidden');
	
	
	if(getSKU!=null || getSKU!="")
	{
		skuField.setDefaultValue(getSKU);
	}
	
	if(getSKUId!=null || getSKUId!="")
	{
		skuIDField.setDefaultValue(getSKUId);
	}
	
	if(getLP!=null || getLP!="")
	{
		lpField.setDefaultValue(getLP);
	}
	
	if(getQty!=null || getQty!="")
	{
		QtyField.setDefaultValue(getQty);
	}
	
	if(getArrLP!=null || getArrLP!="")
	{
		ArrlpField.setDefaultValue(getArrLP);
	}
	
	if(getAdjustFlag!=null || getAdjustFlag!="")
	{
		AdjustFlagField.setDefaultValue(getAdjustFlag);
	}
	
	nlapiLogExecution('ERROR', 'getArrLP create form', getArrLP);
	
	var sublist = form.addSubList("custpage_seriallist", "list", "SerialList");
	sublist.addMarkAllButtons();
	sublist.addField("custpage_serialcheckbox", "checkbox", "Select").setDefaultValue('F');
	sublist.addField("custpage_serialrec_no", "text", "S No").setDisplayType('inline');
	//sublist.addField("custpage_serial_uom", "text", "UOM").setDisplayType('inline');
	sublist.addField("custpage_serial_lp", "text", "LP").setDisplayType('inline');
	sublist.addField("custpage_serial_internalid", "text", "Internal ID").setDisplayType('hidden');
	sublist.addField("custpage_serial_serialno", "text", "Serial #").setDisplayType('inline');
	
	var SerialNo;
	var LPNo;
	var InternalID;
	var k = 1;
	
	if(getLP != null && getLP != '')
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', getSKUId);
		filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is',getLP);
		filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof',[3,19]);
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		columns[1] = new nlobjSearchColumn('custrecord_serialparentid');
		var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             
		
		if(searchResults != null && searchResults != "")
		{ 
			for (var i = 0; i < searchResults.length; i++) 
			{   
				if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
				{
					SerialNo = searchResults[i].getValue('custrecord_serialnumber');
					LPNo = searchResults[i].getValue('custrecord_serialparentid');
					InternalID = searchResults[i].getId();
					form.getSubList('custpage_seriallist').setLineItemValue('custpage_serialrec_no', k, k);
					//form.getSubList('custpage_seriallist').setLineItemValue('custpage_serial_uom', k, 'EACH');
					form.getSubList('custpage_seriallist').setLineItemValue('custpage_serial_lp', k, LPNo);
					form.getSubList('custpage_seriallist').setLineItemValue('custpage_serial_internalid', k, InternalID);
					form.getSubList('custpage_seriallist').setLineItemValue('custpage_serial_serialno', k, SerialNo);	
					k++;
				}
			}
		}	
	}
	else if(getArrLP != null && getArrLP != 'undefined' && getArrLP.length > 0)
	{
		var temp = new Array();
		if(getArrLP.indexOf(',') != -1)
		{
			temp = getArrLP.split(",");
		}
		else
		{
			temp[0] = getArrLP;
		}
		nlapiLogExecution('ERROR', 'temp.length', temp.length);
		for (var x=0; x<=temp.length-1; x++)
		{
			nlapiLogExecution('ERROR', 'temp[x]', temp[x]);
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', getSKUId);
			filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is',temp[x]);
			filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof',[3,19]);
			
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
			columns[1] = new nlobjSearchColumn('custrecord_serialparentid');
			var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             
			
			if(searchResults != null && searchResults != "")
			{ 
				for (var i = 0; i < searchResults.length; i++) 
				{   
					if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
					{
						SerialNo = searchResults[i].getValue('custrecord_serialnumber');
						LPNo = searchResults[i].getValue('custrecord_serialparentid');
						InternalID = searchResults[i].getId();
						form.getSubList('custpage_seriallist').setLineItemValue('custpage_serialrec_no', k, k);
						//form.getSubList('custpage_seriallist').setLineItemValue('custpage_serial_uom', k, 'EACH');
						form.getSubList('custpage_seriallist').setLineItemValue('custpage_serial_lp', k, LPNo);
						form.getSubList('custpage_seriallist').setLineItemValue('custpage_serial_internalid', k, InternalID);
						form.getSubList('custpage_seriallist').setLineItemValue('custpage_serial_serialno', k, SerialNo);	
						k++;
					}
				}
			}	
		}
	}
}