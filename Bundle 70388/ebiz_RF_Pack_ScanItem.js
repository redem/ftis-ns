/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_Pack_ScanItem.js,v $
<<<<<<< ebiz_RF_Pack_ScanItem.js
 *     	   $Revision: 1.1.2.3.4.3.4.21.2.1 $
 *     	   $Date: 2015/11/06 06:16:00 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_84 $
=======
 *     	   $Revision: 1.1.2.3.4.3.4.21.2.1 $
 *     	   $Date: 2015/11/06 06:16:00 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_84 $
>>>>>>> 1.1.2.3.4.3.4.13
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Pack_ScanItem.js,v $
 * Revision 1.1.2.3.4.3.4.21.2.1  2015/11/06 06:16:00  deepshikha
 * 2015.2 issue fixes
 * 201415012
 *
 * Revision 1.1.2.3.4.3.4.21  2015/04/30 13:08:11  schepuri
 * case# 201412474
 *
 * Revision 1.1.2.3.4.3.4.20  2014/06/13 12:56:33  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.3.4.3.4.19  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3.4.3.4.18  2014/05/27 13:57:22  snimmakayala
 * Case#: 20148567
 * RF Packing issue fixes
 *
 * Revision 1.1.2.3.4.3.4.17  2014/04/17 15:46:24  nneelam
 * case#  20148039
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3.4.3.4.16  2014/03/28 08:36:34  grao
 * Case#  20127865 and 20127866  related issue fixes in Sonic  electrox issue fixes
 *
 * Revision 1.1.2.3.4.3.4.15  2014/03/28 08:36:05  grao
 * Case#  20127865 and 20127866  related issue fixes in Sonic  electrox issue fixes
 *
 * Revision 1.1.2.3.4.3.4.14  2014/03/28 08:32:35  grao
 * Case#  20127865 and 20127866  related issue fixes in Sonic  electrox issue fixes
 *
 * Revision 1.1.2.3.4.3.4.13  2014/03/26 15:44:38  nneelam
 * case#  20127805
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3.4.3.4.12  2014/02/06 06:51:40  sponnaganti
 * case# 20127041
 * (if condition added for checking entered item internal id and actitem internal id)
 *
 * Revision 1.1.2.3.4.3.4.11  2014/02/04 15:49:06  sponnaganti
 * case# 20127041
 * (if condition added for checking entered item internal id and actitem internal id)
 *
 * Revision 1.1.2.3.4.3.4.10  2014/02/03 07:51:27  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127036
 *
 * Revision 1.1.2.3.4.3.4.9  2013/11/14 15:42:27  skreddy
 * Case# 20125642
 * Afosa SB issue fix
 *
 * Revision 1.1.2.3.4.3.4.8  2013/11/12 06:40:16  skreddy
 * Case# 20125642
 * Afosa SB issue fix
 *
 * Revision 1.1.2.3.4.3.4.7  2013/10/01 16:07:58  rmukkera
 * Case# 20124698
 *
 * Revision 1.1.2.3.4.3.4.6  2013/08/23 15:10:01  rmukkera
 * Case# 20123973�
 *
 * Revision 1.1.2.3.4.3.4.5  2013/08/12 15:37:44  nneelam
 * Case# 20123865
 * Standard Bundle Issue Fixed..
 *
 * Revision 1.1.2.3.4.3.4.4  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.3.4.3.4.3  2013/05/08 15:07:59  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.1.2.3.4.3.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.3.4.3.4.1  2013/04/04 16:17:47  skreddy
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1.2.3.4.3  2013/01/02 15:42:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Double Click restriction.
 * .
 *
 * Revision 1.1.2.3.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.3.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.1.2.3  2012/06/28 07:55:44  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.1.2.2  2012/06/15 10:08:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.1.2.1  2012/06/06 07:39:03  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.32.4.21  2012/05/14 14:37:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to validating LP
 *
 * Revision 1.32.4.20  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.32.4.19  2012/05/09 15:58:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing Trantype in query string was missing
 *
 * Revision 1.32.4.18  2012/04/27 07:25:07  gkalla
 * CASE201112/CR201113/LOG201121
 * QC check based on rule configuration
 *
 * Revision 1.32.4.17  2012/04/11 22:02:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related navigating to TO Item Status when the user click on Previous button.
 *
 * Revision 1.32.4.16  2012/04/10 22:59:15  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related navigating to previous screen is resolved.
 *
 * Revision 1.32.4.15  2012/03/21 11:08:48  schepuri
 * CASE201112/CR201113/LOG201121
 * Added movetask
 *
 * Revision 1.32.4.14  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.32.4.13  2012/03/06 11:00:05  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.12  2012/03/05 14:42:30  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.11  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.43  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.42  2012/02/21 07:36:44  spendyala
 * CASE201112/CR201113/LOG201121
 * code merged upto 1.32.4.9
 *
 * Revision 1.41  2012/02/14 07:13:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged upto 1.32.4.8.
 *
 * Revision 1.40  2012/02/13 13:39:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.39  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.38  2012/02/10 10:25:15  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.6.
 *
 * Revision 1.37  2012/02/07 07:24:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.4.
 *
 * Revision 1.36  2012/02/01 12:53:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.3.
 *
 * Revision 1.35  2012/02/01 06:30:36  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.32.4.2
 *
 * Revision 1.34  2012/01/19 00:43:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Changes
 *
 * Revision 1.33  2012/01/09 13:11:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Issue if there a space in batch no
 *
 * Revision 1.32  2012/01/06 13:03:09  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing, redirecting to serialno entry
 *
 * Revision 1.31  2012/01/04 20:18:38  gkalla
 * CASE201112/CR201113/LOG201121
 * To add Item to Batch entry
 *
 * Revision 1.30  2012/01/04 15:08:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.29  2011/12/30 14:09:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.28  2011/12/27 23:24:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.27  2011/12/23 23:36:41  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.26  2011/12/23 16:21:51  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.25  2011/12/23 13:24:07  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.24  2011/12/23 13:19:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.23  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.22  2011/12/15 14:10:40  schepuri
 * CASE201112/CR201113/LOG201121
 * modified field names in customrecord_ebiznet_trn_poreceipt
 *
 * Revision 1.21  2011/12/02 13:19:35  schepuri
 * CASE201112/CR201113/LOG201121
 * RF To generate location acc to the location defined in PO
 *
 * Revision 1.20  2011/11/17 08:44:28  spendyala
 * CASE201112/CR201113/LOG201121
 * wms status flag for create inventory is changed to 17 i .e, FLAG INVENTORY INBOUND
 *
 * Revision 1.19  2011/09/29 12:04:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.18  2011/09/28 16:08:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/09/26 19:58:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.16  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.14  2011/09/14 05:27:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/09/12 08:24:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/09/10 11:28:23  schepuri
 * CASE201112/CR201113/LOG201121
 *
 * redirecting to customscript_serial_no
 *
 * Revision 1.11  2011/07/05 12:36:44  schepuri
 * CASE201112/CR201113/LOG201121
 * RF CheckIN changes
 *
 * Revision 1.10  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.9  2011/06/17 07:39:47  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * Confirm putaway changes
 *
 * Revision 1.8  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.6  2011/06/06 07:10:13  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * WBC Issues
 *
 * Revision 1.5  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function Packing_ScanItem(request, response)
{
	if (request.getMethod() == 'GET') 
	{   
		var getOrderno = request.getParameter('custparam_orderno');
		var getContlpno=request.getParameter('custparam_containerno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getActualQty = request.getParameter('custparam_actualquantity');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var vcontlp = request.getParameter('custparam_containerno');
		var vcontsize = request.getParameter('custparam_containersize');
		var vcontsizevalue=request.getParameter('custparam_containersizevalue');	
		var vlineCount = request.getParameter('custparam_linecount');
		var vloopCount = request.getParameter('custparam_loopcount');
		var wmslocation=request.getParameter('custparam_wmslocation');
		var waveno=request.getParameter('custparam_waveno');
		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var Itemdescription='';

		var getItemName = ItemRec.getFieldValue('itemid');
		nlapiLogExecution('DEBUG', 'getItemName', getItemName);
		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('description');
		}
		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('salesdescription');
		}	

		var getLanguage = request.getParameter('custparam_language');	
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		//case 20125642 start: spanish conversion,added "es_AR"
		var st0,st1,st2,st3,st4,st5,st6,st7,st8;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			//case 20125642 end
			st0 = "";
			st1 = "PEDIDO #";
			st2 = "CAJA #";
			st3 = "TAMA&#209;O DE LA CAJA";
			st4 = "ART&#205;CULO";
			st5 = "ENTRAR / SCAN ITEM:";
			st6 = "ENVIAR";
			st7 = "ANTERIOR";
			st8 = "CAJA CERRADO";
		}
		else
		{
			st0 = "";
			st1 = "ORDER#";
			st2 = "CARTON#";
			st3 = "CARTON SIZE";
			st4 = "ITEM";
			st5 = "ENTER/SCAN ITEM:";
			st6 = "SEND";
			st7 = "PREV";
			st8 = "CLOSE CARTON";
		}

		var fetchedcontainerlp =request.getParameter('custparam_fetchedcontainerlp');
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 	
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enteritem').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body  onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getOrderno + "</label>";	
		html = html + "				</td>";
		html = html + "			</tr>";  
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ": <label>" + getContlpno + "</label>";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 +  ": <label>" + vcontsize + "</label>";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + ": <label>" + getItemName + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";        
		html = html + "				<td align = 'left'>" + st5;		
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getOrderno + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContlpno + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnActualQuantity' value=" + getActualQty + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdngetRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnlinecount' value=" + vlineCount + ">";
		html = html + "				<input type='hidden' name='hdnloopcount' value=" + vloopCount + ">";	
		html = html + "				<input type='hidden' name='hdnwmslocation' value=" + wmslocation + ">";		
		html = html + "				<input type='hidden' name='hdngetitemname' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdncontainervalue' value=" + vcontsizevalue + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + fetchedcontainerlp + ">";
		html = html + "				<input type='hidden' name='hdnwaveno' value=" + waveno + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnclosecarton'>";
		html = html + "				<input type='hidden' name='hdnsubmit'>";
		html = html + "				<input type='hidden' name='hdnprevious'>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 +"  <input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnsubmit.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdclosecarton.disabled=true;return false'/>";
		html = html + "				" + st7 + " <input name='cmdPrevious' type='submit' value='F7' onclick='this.form.hdnprevious.value=this.value;this.form.submit();this.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdclosecarton.disabled=true;return false'/>";
		html = html + "				</br>" + st8 + " <input name='cmdclosecarton' type='submit' value='F8' onclick='this.form.hdnclosecarton.value=this.value;this.form.submit();this.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdPrevious.disabled=true;return false'/>";       
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getEnteredItem = request.getParameter('enteritem');
		nlapiLogExecution('DEBUG', 'Entered Order No', getEnteredItem);      

		var getWaveNo = request.getParameter('hdnWaveNo');		
		var getContainerLpNo = request.getParameter('custparam_containerno');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getActualQuantity = request.getParameter('hdnActualQuantity');		
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getOrderNo = request.getParameter('hdnWaveNo');
		var getCortonNo = request.getParameter('hdnFetchedContainerLPNo');		
		var wmslocation= request.getParameter('hdnwmslocation');		
		var getItemName=request.getParameter('hdngetitemname');
		var itemno;

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('hdnprevious');   
		var checkqty = request.getParameter('cmdBulkqty');  
		var closecorton = request.getParameter('hdnclosecarton');
		//var checkqty = request.getParameter('bulkqty');   
		//nlapiLogExecution('DEBUG', 'checkqty', checkqty);

		var SOarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]);    	

		var st9,st10;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{			
			st9 = "ART&#205;CULO INV&#193;LIDO ";
			st10 = "NO CANTIDAD DE IZQUIERDA A PAQUETE PARA ESTE ART&#205;CULO";
		}
		else
		{			
			st9 = "INVALID ITEM";
			st10 = "NO QUANTITY LEFT TO PACK FOR THIS ITEM";
		}

		SOarray["custparam_error"] = st9;
		SOarray["custparam_screenno"] = 'Pack3';

		SOarray["custparam_orderno"] = getWaveNo;		
		SOarray["custparam_containerno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_actualquantity"] = getActualQuantity;
		SOarray["custparam_iteminternalid"] = getItemInternalId;		
		SOarray["custparam_linecount"] = request.getParameter('hdnlinecount');
		SOarray["custparam_loopcount"] = request.getParameter('hdnloopcount');
		SOarray['custparam_wmslocation']=wmslocation;		
		SOarray["custparam_containersize"] = request.getParameter('custparam_containersize');
		SOarray["custparam_containersizevalue"] = request.getParameter('hdncontainervalue');
		SOarray["custparam_fetchedcontainerlp"]=request.getParameter('custparam_fetchedcontainerlp');
		SOarray["custparam_waveno"] = request.getParameter('hdnwaveno');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdngetRecordInternalId'); 
		var checkBulkqty='N';
		if(checkqty!=null && checkqty!='')
		{
			checkBulkqty='Y';
		}
		SOarray['custparam_bulkqty']=checkqty;




		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
	//case 201412474
			if	(request.getParameter('custparam_fetchedcontainerlp')!="" && request.getParameter('custparam_fetchedcontainerlp')!=null)
				SOarray["custparam_containerno"]=request.getParameter('custparam_fetchedcontainerlp');
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scancontainer', 'customdeploy_ebiz_rf_pack_scancontainer', false, SOarray);
		}
		else if(closecorton=='F8')
		{
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
		}
		//else if(checkqty=='F5')
//		{

//		var currItem = validateSKU(getEnteredItem, wmslocation, null);

//		nlapiLogExecution('DEBUG', 'currItem', currItem);
//		if((currItem!='' && currItem!=null))
//		{
//		var filters = new Array();
//		filters.push(new nlobjSearchFilter('nameinternal', null, 'is', currItem));
//		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

//		var columns = new Array();
//		columns[0] = new nlobjSearchColumn('itemid');
//		columns[1] = new nlobjSearchColumn('internalid');
//		columns[0].setSort(true);
//		var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);
//		if(itemSearchResults!=null && itemSearchResults!='')
//		{
//		itemno=itemSearchResults[0].getValue('internalid');
//		}
//		}

//		nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
//		nlapiLogExecution('DEBUG', 'getCortonNo', getCortonNo);
//		nlapiLogExecution('DEBUG', 'itemno', itemno);
//		nlapiLogExecution('DEBUG', 'Into If', 'hi1');
//		var SOFilters = new Array();
//		if(getOrderNo!=null && getOrderNo!='')
//		SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));

//		if(getCortonNo!=null && getCortonNo!='')
//		SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', getCortonNo));

//		if(itemno!=null && itemno!='')
//		SOFilters.push(new nlobjSearchFilter( 'custrecord_ebiz_sku_no',null, 'is', itemno));

//		SOFilters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));

//		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	

//		var SOColumns = new Array();
//		SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
//		SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
//		SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
//		SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
//		SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
//		SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
//		SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
//		SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
//		SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
//		SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
//		SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
//		SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
//		SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
//		SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
//		SOColumns[3].setSort();

//		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);  
//		if (SOSearchResults != null && SOSearchResults.length > 0) 
//		{
//		nlapiLogExecution('DEBUG', 'Test3', 'Test3');
//		nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResults.length);
//		nlapiLogExecution('DEBUG', 'SearchResults of given Order', getOrderNo);
//		var SOSearchResult = SOSearchResults[0];
//		if(SOSearchResults.length > 1)
//		{
//		var SOSearchnextResult = SOSearchResults[1];
//		SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
//		SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
//		nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
//		nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
//		}
//		// SOarray["custparam_waveno"] = getWaveNo;
//		nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResult.getValue('custrecord_lpno'));
//		nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResult.getValue('custrecord_expe_qty'));
//		nlapiLogExecution('DEBUG', 'SearchResults of given Order', getOrderNo);

//		//SOarray["custparam_linecount"] = SOSearchResults.length;
//		SOarray["custparam_orderno"] = getOrderNo;
//		//SOarray["custparam_containerno"] = SOSearchResult.getValue('custrecord_container_lp_no');
//		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();			       
//		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');			       
//		SOarray["custparam_item"] = SOSearchResult.getText('custrecord_sku');
//		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');	
//		//SOarray['custparam_loopcount'] = SOSearchResults.length;
//		SOarray['custparam_iteminternalid'] =  SOSearchResult.getValue('custrecord_sku');

//		}
//		else 
//		{
//		SOarray["custparam_error"] = 'No Line Items To Pack';
//		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
//		nlapiLogExecution('DEBUG', 'Error: ', 'No Line Items To Pack');
//		}
//		//if (getEnteredLocation != '' && getEnteredLocation == getBeginBinLocation) {getItemInternalId

//		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
//		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);

//		var getItemName = ItemRec.getFieldValue('itemid');
//		nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
//		nlapiLogExecution('DEBUG', 'getEnteredItem', getEnteredItem);
//		nlapiLogExecution('DEBUG', 'getItemName', getItemName);

//		if(getEnteredItem != '' )
//		{
//		if (currItem!=null && currItem!='') 
//		{		
//		response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanqty', 'customdeploy_ebiz_rf_pack_scanqty_di', false, SOarray);
//		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

//		}
//		else 
//		{
//		SOarray["custparam_error"] = 'Invalid Item';
//		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
//		nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item');
//		}
//		}
//		else
//		{
//		SOarray["custparam_error"] = 'Invalid Item';
//		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
//		nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item');
//		}



//		}
		else 
		{
			nlapiLogExecution('DEBUG', 'getEnteredItem in ENT', getEnteredItem);
			nlapiLogExecution('DEBUG', 'getItemName', getItemName);


			//case 20123865 . If condition added to check Entered Item and So Item are same.
			//case# 20127041 (if condition commented because when we are scanning upc code if condition is failed)
			//if(getEnteredItem==getItemName){
			nlapiLogExecution('DEBUG', 'getEnteredItem in ENT', getEnteredItem);
			var currItem = validateSKUId(getEnteredItem, wmslocation, null);

			nlapiLogExecution('DEBUG', 'currItem in ENT', currItem);
			nlapiLogExecution('DEBUG', 'currItem[0] in ENT', currItem[0]);
			if(currItem != null && currItem != '' && currItem.length>1)
			{
				//vItemType=vItemArr[0];
				itemno=currItem[1];
			}
			//case# 20127041 (if condition added for checking entered item internal id and actitem internal id) 
			if(itemno==getItemInternalId){
				//case# 20127041 end	
				/*if(currItem!='' && currItem!=null)
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('nameinternal', null, 'is', currItem));
				filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('itemid');
				columns[1] = new nlobjSearchColumn('internalid');
				columns[0].setSort(true);
				var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);
				if(itemSearchResults!=null && itemSearchResults!='')
				{
					itemno=itemSearchResults[0].getValue('internalid');
				}  
			}*/
				nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
				nlapiLogExecution('DEBUG', 'getCortonNo', getCortonNo);
				nlapiLogExecution('DEBUG', 'itemno', itemno);
				nlapiLogExecution('DEBUG', 'Into If', 'hi1');
				var SOFilters = new Array();
				if(getOrderNo!=null && getOrderNo!='')
					SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));

				/*if(getCortonNo!=null && getCortonNo!='')
				SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', getCortonNo));*/

				if(itemno!=null && itemno!='')
					SOFilters.push(new nlobjSearchFilter( 'custrecord_ebiz_sku_no',null, 'is', itemno));

				SOFilters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));
				SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
				SOFilters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'greaterthan', 0));
				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks confirmed)

				var SOColumns = new Array();
				SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
				SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
				SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
				SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
				SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
				SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
				SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
				SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
				SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
				SOColumns[14] = new nlobjSearchColumn('custrecord_act_qty');

				SOColumns[3].setSort();
				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);  
				if (SOSearchResults != null && SOSearchResults.length > 0) 
				{
					nlapiLogExecution('DEBUG', 'Test3', 'Test3');
					nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResults.length);
					nlapiLogExecution('DEBUG', 'SearchResults of given Order', getOrderNo);
					var SOSearchResult = SOSearchResults[0];
					if(SOSearchResults.length > 1)
					{
						var SOSearchnextResult = SOSearchResults[1];
						SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
						SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
						nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
						nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
					}
					// SOarray["custparam_waveno"] = getWaveNo;
					nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResult.getValue('custrecord_lpno'));
					nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResult.getValue('custrecord_expe_qty'));
					nlapiLogExecution('DEBUG', 'SearchResults of given Order', getOrderNo);

					//SOarray["custparam_linecount"] = SOSearchResults.length;
					SOarray["custparam_orderno"] = getOrderNo;
					SOarray["custparam_containerno"] = getContainerLpNo;
					SOarray["custparam_recordinternalid"] = SOSearchResult.getId();	
					//Case # 20124698 Start(System allowed to pack the qty as more than the picked qty through RF packing.while doing the qty exception)
					var actualQty=SOSearchResult.getValue('custrecord_act_qty');
					if(actualQty==null || actualQty=='' ||actualQty=='null')
					{
						actualQty=0;
					}
					var expectedQty=SOSearchResult.getValue('custrecord_expe_qty');
					if(expectedQty==null || expectedQty=='' ||expectedQty=='null')
					{
						expectedQty=0;
					}
					nlapiLogExecution('DEBUG', 'actualQty', actualQty);
					nlapiLogExecution('DEBUG', 'expectedQty', expectedQty);
					if(parseFloat(actualQty)==parseFloat(expectedQty))
					{
						nlapiLogExecution('DEBUG', 'inside', 'if');					
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_actualquantity"] = SOSearchResult.getValue('custrecord_act_qty');

						//SOarray["custparam_linecount"] = SOSearchResults.length;
						SOarray["custparam_orderno"] = getOrderNo;
						//case # 20148039
						//	SOarray["custparam_containerno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();	
						//Case # 20124698 Start(System allowed to pack the qty as more than the picked qty through RF packing.while doing the qty exception)
						var actualQty=SOSearchResult.getValue('custrecord_act_qty');
						if(actualQty==null || actualQty=='' ||actualQty=='null')
						{
							actualQty=0;
						}
						var expectedQty=SOSearchResult.getValue('custrecord_expe_qty');
						if(expectedQty==null || expectedQty=='' ||expectedQty=='null')
						{
							expectedQty=0;
						}
						nlapiLogExecution('DEBUG', 'actualQty', actualQty);
						nlapiLogExecution('DEBUG', 'expectedQty', expectedQty);
						if(parseFloat(actualQty)==parseFloat(expectedQty))
						{	nlapiLogExecution('DEBUG', 'inside', 'if');					
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_actualquantity"] = SOSearchResult.getValue('custrecord_act_qty');
						}
						else
						{
							nlapiLogExecution('DEBUG', 'inside', 'else');	
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_act_qty');
							SOarray["custparam_actualquantity"] = SOSearchResult.getValue('custrecord_act_qty');
						}
						//Case # 20124698 End
						SOarray["custparam_item"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');	
						//SOarray['custparam_loopcount'] = SOSearchResults.length;
						SOarray['custparam_iteminternalid'] =  SOSearchResult.getValue('custrecord_sku');
					}
					else
					{
						nlapiLogExecution('DEBUG', 'inside', 'else');	
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_act_qty');
						SOarray["custparam_actualquantity"] = SOSearchResult.getValue('custrecord_act_qty');
					}
					//Case # 20124698 End
					SOarray["custparam_item"] = SOSearchResult.getText('custrecord_sku');
					SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');	
					//SOarray['custparam_loopcount'] = SOSearchResults.length;
					SOarray['custparam_iteminternalid'] =  SOSearchResult.getValue('custrecord_sku');
				}
				else 
				{
					//SOarray["custparam_error"] = 'No Line Items To Pack';
					SOarray["custparam_error"] = st10;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'No Line Items To Pack');
					return;
				}
				//if (getEnteredLocation != '' && getEnteredLocation == getBeginBinLocation) {
				nlapiLogExecution('DEBUG', 'getEnteredItem', getEnteredItem);
				nlapiLogExecution('DEBUG', 'getItemName', getItemName);
				var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);

				var getItemName = ItemRec.getFieldValue('itemid');
				nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
				//var currItem = validateSKU(getEnteredItem, wmslocation, null);
				if(getEnteredItem != '' )
				{
					if (currItem!=null && currItem!='' && currItem.length>1) 
					{		

						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanqty', 'customdeploy_ebiz_rf_pack_scanqty_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
						return;
					}
					else 
					{
						SOarray["custparam_iteminternalid"] = getItemInternalId;	
						SOarray["custparam_error"] = st9;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Invalidd Item');
						return;
					}
				}
				else
				{
					SOarray["custparam_iteminternalid"] = getItemInternalId;
					SOarray["custparam_error"] = st9;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Invalidd Item');
					return;
				}
			}
			else{
				SOarray["custparam_iteminternalid"] = getItemInternalId;
				SOarray["custparam_error"] = st9;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Invalidd Item');
				return;

				/*		}
			}
			else{
				SOarray["custparam_iteminternalid"] = getItemInternalId;
				SOarray["custparam_error"] = st9;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Invalidd Item');
				return;
				 */
			}
		}
	}
}

/**
 * 
 * @param itemNo
 * @param location
 * @param company
 * @returns
 */
function validateSKU(itemNo, location, company){
	nlapiLogExecution('DEBUG', 'validateSKU', 'Start');

	var inputParams = 'Item = ' + itemNo + '<br>';
	inputParams = inputParams + 'Location = ' + location + '<br>';
	inputParams = inputParams + 'Company = ' + company;
	nlapiLogExecution('DEBUG', 'Input Parameters', inputParams);

	var currItem = eBiz_RF_GetItemForItemNo(itemNo);

	if(currItem == ""){
		currItem = eBiz_RF_GetItemBasedOnUPCCode(itemNo);
	}

	if(currItem == ""){
		currItem = eBiz_RF_GetItemFromSKUDims(itemNo, location);
	}

	var logMsg = "";
	if(currItem == ""){
		logMsg = 'Unable to retrieve item';
		currItem = null;
	} else {
		logMsg = 'Item = ' + currItem;
	}

	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'validateSKU', 'End');
	return currItem;
}
