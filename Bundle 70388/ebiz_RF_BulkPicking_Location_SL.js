/***************************************************************************
 eBizNET Solutions Inc                
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_BulkPicking_Location_SL.js,v $
 *     	   $Revision: 1.1.2.6 $
 *     	   $Date: 2014/06/13 13:29:42 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_BulkPicking_Location_SL.js,v $
 * Revision 1.1.2.6  2014/06/13 13:29:42  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.5  2014/06/03 15:43:42  skavuri
 * Case# 20148688 SB Issue Fixed
 *
 * Revision 1.1.2.4  2014/05/30 00:41:00  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3  2014/03/06 15:28:43  skavuri
 * Case # 20127528 issue fixed
 *
 * Revision 1.1.2.2  2013/08/23 06:13:10  snimmakayala
 * Case# 20124032
 * NLS - UAT ISSUES
 * Quantity is displaying wrongly on RF bulk picking screen for SHIPASIS items.
 *
 * Revision 1.1.2.1  2013/07/19 08:18:22  gkalla
 * Case# 20123527
 * Bulk picking CR for Nautilus
 *
 * Revision 1.10.2.18.4.11.2.9  2013/06/26 15:04:57  snimmakayala
 * Case# : 20123126
 * GSUSA :: Issue Fixes
 * 
 * 
 *****************************************************************************/
function PickingLocation(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Debug', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR A LA UBICACI&#211;N:";
			st2 = "INGRESAR / ESCANEO DE LA UBICACI&#211;N";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "OVERRIDE";
			st6 = "SKIP";
		}
		else
		{
			st1 = "GO TO LOCATION: ";
			st2 = "ENTER/SCAN LOCATION ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "OVERRIDE";
			st6 = "SKIP";
		}


		var getWaveno = request.getParameter('custparam_waveno');
		var getZoneNo = request.getParameter('custparam_zoneno');
		var getZoneId = request.getParameter('custparam_zoneid');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getFetchedLocation=getBeginLocation;

		var getBeginLocationName = request.getParameter('custparam_beginlocationname');

		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');

		var getZoneId=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('Debug', 'getItemInternalId', getItemInternalId);
		nlapiLogExecution('Debug', 'getZoneNo', getZoneNo);
		/*var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');*/
		nlapiLogExecution('Debug', 'before getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Debug', 'before getItemInternalId', getItemInternalId);

		//code added by santosh on 7Aug2012
		var Type='';
		if(request.getParameter('custparam_type') !=null &&  request.getParameter('custparam_type')!="")
		{
			Type=request.getParameter('custparam_type');
		}
		nlapiLogExecution('Debug', 'Type', Type);
		//end of code on 7aug2012
		nlapiLogExecution('Debug', 'after getFetchedLocation', getBeginLocationName);
		nlapiLogExecution('Debug', 'after getItemInternalId', getItemInternalId);
		nlapiLogExecution('Debug', 'getBeginLocation ID', getFetchedBeginLocation);
		nlapiLogExecution('Debug', 'getBeginLocation', getFetchedLocation);
		//nlapiLogExecution('Debug', 'before pickType in loc get', pickType);
		//nlapiLogExecution('Debug', 'NextItemInternalId', NextItemInternalId); 

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlocation').focus();";  

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> Zone# :<label>" + getZoneNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " <label>" + getBeginLocationName + "</label>";
		html = html + "				<input type='hidden' name='hdnZoneNo' value='" + getZoneNo + "'>";//Case# 20127528 getZoneNo must be pass as string for url
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";


		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value='" + getFetchedLocation + "'>";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value='" + getBeginLocationName + "'>";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";

		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";

		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";

		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneId + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2;//ENTER/SCAN LOCATION 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true;this.form.cmdPrevious.disabled=true; this.form.cmdOverride.disabled=true;this.form.cmdSKIP.disabled=true;return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					" + st5 + " <input name='cmdOverride' type='submit' value='F11'/>";
		//html = html + "					" + st6 + " <input name='cmdSKIP' type='submit' value='F12'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('Debug', 'Into Response', 'Into Response');
		var getLanguage = request.getParameter('hdngetLanguage');

		var st7;

		if( getLanguage == 'es_ES')
		{
			st7 = "UBICACI&#211;N NO V&#193;LIDA";

		}
		else
		{
			st7 = "INVALID LOCATION";
		}


		var getBeginLocation='';		
		var getBeginLocationName='';	
		var WaveNo=request.getParameter('hdnWaveNo');

		var ItemId=request.getParameter('hdnItemInternalId');
		var vZoneNo=request.getParameter('hdnZoneNo');
		var vZoneId=request.getParameter('hdnebizzoneno');
		var vSkipId=request.getParameter('hdnskipid');

		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		getBeginLocation = request.getParameter('hdnBeginLocation');
		getBeginLocationName = request.getParameter('hdnBeginLocationName');
		var getItem = request.getParameter('hdnItem');
		var getItemName = request.getParameter('hdnItemName');

		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var OrdNo='';
		var NextShowLocation='';
		var NextShowItemId='';

		var whLocation = request.getParameter('hdnwhlocation');

		var str = 'getBeginLocation. = ' + getBeginLocation + '<br>';
		str = str + 'ItemId. = ' + ItemId + '<br>';	
		str = str + 'WaveNo. = ' + WaveNo + '<br>';		 	
		str = str + 'vZoneId. = ' + vZoneId + '<br>';	
		str = str + 'vZoneNo. = ' + vZoneNo + '<br>';

		nlapiLogExecution('Debug', 'Parameter Values', str);

		var SOarray = new Array();
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		SOarray["custparam_recordinternalid"]= request.getParameter('custparam_recordinternalid');// Case# 20148688
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_screenno"] = 'BULK02';
		if(request.getParameter('cmdOverride')!='F11')
		{

			SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');



			SOarray["custparam_expectedquantity"] = getExpectedQuantity;
			SOarray["custparam_beginLocation"] = getBeginLocation;
			SOarray["custparam_item"] = ItemId;
			SOarray["custparam_itemname"] = getItemName; 
			SOarray["custparam_iteminternalid"] = getItemInternalId; 
			SOarray["custparam_beginLocationname"] = getBeginLocationName;


			SOarray["custparam_whlocation"] = whLocation;


			if(vZoneId!=null && vZoneId!="")
			{
				SOarray["custparam_ebizzoneno"] =  vZoneId;
			}
			else
				SOarray["custparam_ebizzoneno"] = '';
			SOarray["custparam_zoneno"] =  vZoneNo;

		}

		var getEnteredLocation = request.getParameter('enterlocation');		
		nlapiLogExecution('Debug', 'Entered Location', getEnteredLocation);	
		nlapiLogExecution('Debug', 'Location', getBeginLocationName);	

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optskipEvent = request.getParameter('cmdSKIP');


		SOarray["custparam_error"] = st7;//'INVALID LOCATION';
		SOarray["custparam_screenno"] = 'BULK02';

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {	

			SOarray["custparam_venterFO"]=OrdNo;
			SOarray["custparam_venterwave"]=WaveNo;
			SOarray["custparam_venterzone"]=vZoneId;
			/*if(Type=="Wave")
				{ 
					SOarray["custparam_type"] ="Wave";
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusterwave_summary', 'customdeploy_ebiz_rf_clusterwave_summary', false, SOarray);
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
				}*/
			//end of the code on 7Aug2012
			response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_container', 'customdeploy_rf_bulkpick_container_di', false, SOarray);
		}
		/*else if(optskipEvent == 'F12')
			{
				nlapiLogExecution('Debug', 'Entered into SKIP',getRecordInternalId);
				var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
				if(skiptask==null || skiptask=='')
				{
					skiptask=1;
				}
				else
				{
					skiptask=parseInt(skiptask)+1;
				}

				nlapiLogExecution('Debug', 'skiptask',skiptask);

				nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);
				vSkipId= parseFloat(vSkipId) + 1;
				SOarray["custparam_skipid"] = vSkipId;		
				SOarray["custparam_beginLocationname"]=null;
				SOarray["custparam_beginlocationname"]=null;
				SOarray["custparam_beginLocation"]=null;
				SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
			}*/
		else {
			//To Remove case sensitive
			nlapiLogExecution('Debug', 'into else', 'done');	
			nlapiLogExecution('Debug', 'getBeginLocationName.toUpperCase()', getBeginLocationName.toUpperCase());	
			nlapiLogExecution('Debug', 'getEnteredLocation.toUpperCase()', getEnteredLocation.toUpperCase());
			if (getEnteredLocation != '' && getEnteredLocation.toUpperCase() == getBeginLocationName.toUpperCase()) {

				//Updating begin time of the task.

				var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getEnteredLocation));

				nlapiLogExecution('Debug', 'Length of Location Search', LocationSearch.length);
				var getEndLocationInternalId;
				if (LocationSearch != null && LocationSearch != '' && LocationSearch.length != 0) {

					getEndLocationInternalId = LocationSearch[0].getId();
					nlapiLogExecution('Debug', 'End Location Id', getEndLocationInternalId);


					SOarray["custparam_beginlocationname"] = getBeginLocationName;
					SOarray["custparam_beginLocationname"] = getBeginLocationName;
					SOarray["custparam_endlocinternalid"] = getEndLocationInternalId;
					SOarray["custparam_endlocation"] = getEnteredLocation;

					nlapiLogExecution('Debug', 'getBeginLocation',SOarray["custparam_beginLocation"]);
					var getBeginLocationid=	SOarray["custparam_beginLocation"];
					var wave=request.getParameter('hdnWaveNo');

					var invtqty=0;

					var EndLocInternalId=getEndLocationInternalId;
					var ExpectedQuantity=SOarray["custparam_expectedquantity"];

					var WaveNo=wave;

					var getZoneNo=SOarray["custparam_ebizzoneno"];
					var whLocation=SOarray["custparam_whlocation"];
					/*if(IsPickFaceLoc=='T')
						{
							var openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemInternalId);

							if(openinventory!=null && openinventory!='')
							{
								nlapiLogExecution('Debug', 'Pick Face Inventory Length', openinventory.length);

								for(var x=0; x< openinventory.length;x++)
								{
									var vinvtqty=openinventory[0].getValue('custrecord_ebiz_qoh',null,'sum');

									if(vinvtqty==null || vinvtqty=='' || isNaN(vinvtqty))
									{
										vinvtqty=0;
									}
									nlapiLogExecution('Debug', 'vinvtqty',vinvtqty);
									invtqty=parseInt(invtqty)+parseInt(vinvtqty);
								}						
							}
						}
						nlapiLogExecution('Debug', 'invtqty', invtqty);
						if(invtqty==null || invtqty=='' || isNaN(invtqty))
						{
							invtqty=0;
						}

						nlapiLogExecution('Debug', 'invtqty', invtqty);

						if(IsPickFaceLoc=='T' && (parseInt(ExpectedQuantity)>parseInt(invtqty)))
						{
							var openreplns = new Array();
							openreplns=getOpenReplns(ItemInternalId,EndLocInternalId);

							if(openreplns!=null && openreplns!='')
							{
								for(var i=0; i< openreplns.length;i++)
								{
									var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
									var expqty=openreplns[i].getValue('custrecord_expe_qty');
									if(expqty == null || expqty == '')
									{
										expqty=0;
									}	
									nlapiLogExecution('Debug', 'open repln qty',expqty);
									nlapiLogExecution('Debug', 'getQuantity',ExpectedQuantity);

									var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
									if(skiptask==null || skiptask=='')
									{
										skiptask=1;
									}
									else
									{
										skiptask=parseInt(skiptask)+1;
									}

									nlapiLogExecution('Debug', 'skiptask',skiptask);

									nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

									var vPickType = request.getParameter('hdnpicktype');
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');

								}

								var SOarray=new Array();
								SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);
								SOarray["custparam_skipid"] = vSkipId;
								if(NextShowLocation!=null && NextShowLocation!='')
									SOarray["custparam_screenno"] = '11EXP';
								else
									SOarray["custparam_screenno"] = '11LOCEXP';	
								SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS tst';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								return;
							}
							else
							{
								var replengen = generateReplenishment(EndLocInternalId,ItemInternalId,whLocation,WaveNo);
								nlapiLogExecution('Debug', 'replengen',replengen);

								//If repeln generation failed, redirect to item so that user will perform short pick.
								if(replengen!=true)
								{
									nlapiLogExecution('ERROR', 'Replen Generation Failed.Insufficient inventory in Bulk location');
									nlapiLogExecution('ERROR', 'Redirecting to Item scan...');
									response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);					
									return;
								}

								var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
								if(skiptask==null || skiptask=='')
								{
									skiptask=1;
								}
								else
								{
									skiptask=parseInt(skiptask)+1;
								}

								nlapiLogExecution('Debug', 'skiptask',skiptask);

								nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

								var vPickType = request.getParameter('hdnpicktype');
								var SOarray=new Array();

								SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);

								SOarray["custparam_skipid"] = vSkipId;
								if(NextShowLocation!=null && NextShowLocation!='')
									SOarray["custparam_screenno"] = '11EXP';
								else
									SOarray["custparam_screenno"] = '11LOCEXP';	

								if(replengen!=true)
									SOarray["custparam_error"]='Replen Generation Failed.Insufficient inventory in Bulk location';
								else 
									SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';

								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								return;
							}

						}				
						else
						{
							if(IsPickFaceLoc=='T' && (InvoiceRefNo==null || InvoiceRefNo==''))
							{
								if(openinventory!=null && openinventory!='')
								{
									var openinvtrecid = openinventory[0].getId();
									nlapiLogExecution('Debug', 'openinvtrecid', openinvtrecid);

									try
									{
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_invref_no', openinvtrecid);
									}
									catch(exp)
									{
										nlapiLogExecution('Debug', 'Exception in updating inventory reference number', exp);
									}
								}
							}
						}*/
					//End of code specific to GSUSA

					response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_item', 'customdeploy_rf_bulkpick_item', false, SOarray);					
					nlapiLogExecution('Debug', 'Done customrecord', 'Success');
				}
				else {

					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

				}
			}
			/*else if (getEnteredLocation != '' && getEnteredLocation.toUpperCase() != getBeginLocationName.toUpperCase()) {
					nlapiLogExecution('Debug', 'Different Location scanned');
					var vlocationfound='F';
					if (SOSearchResults != null && SOSearchResults.length > 0) {
						for (var l = 0; l < SOSearchResults.length; l++) {

							var actBeginLocation = SOSearchResults[l].getText('custrecord_actbeginloc');

							if((getEnteredLocation.toUpperCase()==actBeginLocation.toUpperCase()) && (vlocationfound=='F')){

								vlocationfound='T';

								nlapiLogExecution('Debug', 'vlocationfound', vlocationfound);

								var pikgenresults=SOSearchResults[l];

								SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
								SOarray["custparam_recordinternalid"] = pikgenresults.getId();
								//SOarray["custparam_containerlpno"] = pikgenresults.getValue('custrecord_lpno');
								SOarray["custparam_containerlpno"] = pikgenresults.getValue('custrecord_container_lp_no');
								SOarray["custparam_expectedquantity"] = pikgenresults.getValue('custrecord_expe_qty');
								SOarray["custparam_beginLocation"] = pikgenresults.getValue('custrecord_actbeginloc');
								SOarray["custparam_item"] = pikgenresults.getValue('custrecord_sku');
								SOarray["custparam_itemname"] = pikgenresults.getText('custrecord_sku');
								SOarray["custparam_itemdescription"] = pikgenresults.getValue('custrecord_skudesc');
								SOarray["custparam_iteminternalid"] = pikgenresults.getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_dolineid"] = pikgenresults.getValue('custrecord_ebiz_cntrl_no');
								SOarray["custparam_invoicerefno"] = pikgenresults.getValue('custrecord_invref_no');
								SOarray["custparam_orderlineno"] = pikgenresults.getValue('custrecord_line_no');
								SOarray["custparam_beginLocationname"] = pikgenresults.getText('custrecord_actbeginloc');
								SOarray["custparam_beginlocationname"] = pikgenresults.getText('custrecord_actbeginloc');
								SOarray["custparam_batchno"] = pikgenresults.getValue('custrecord_batch_no');
								SOarray["custparam_whlocation"] = pikgenresults.getValue('custrecord_wms_location');
								SOarray["custparam_whcompany"] = pikgenresults.getValue('custrecord_comp_id');
								SOarray["custparam_noofrecords"] = SOSearchResults.length;		
								SOarray["name"] =  pikgenresults.getValue('name');
								SOarray["custparam_containersize"] =  pikgenresults.getValue('custrecord_container');
								SOarray["custparam_ebizordno"] =  pikgenresults.getValue('custrecord_ebiz_order_no');
								getBeginLocationName = pikgenresults.getText('custrecord_actbeginloc');

								var vNextRecs=0;

								nlapiLogExecution('Debug', 'Loop # : ', l+1);
								nlapiLogExecution('Debug', 'SOSearchResults Length : ', SOSearchResults.length);

								if(l+1 < SOSearchResults.length)
									vNextRecs=l+1;
								else 
									vNextRecs=0;

								nlapiLogExecution('Debug', 'vNextRecs : ', vNextRecs);

								if(SOSearchResults[vNextRecs].getText('custrecord_actbeginloc') != null && SOSearchResults[vNextRecs].getText('custrecord_actbeginloc') !='')
									SOarray["custparam_nextlocation"] = SOSearchResults[vNextRecs].getText('custrecord_actbeginloc');
								else
									SOarray["custparam_nextlocation"]='';

								if(SOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no') != null && SOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no') != '')	
									SOarray["custparam_nextiteminternalid"] = SOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no');
								else
									SOarray["custparam_nextiteminternalid"]='';

								if(SOSearchResults[vNextRecs].getValue('custrecord_expe_qty') != null && SOSearchResults[vNextRecs].getValue('custrecord_expe_qty') != '')
									SOarray["custparam_nextexpectedquantity"] = SOSearchResults[vNextRecs].getValue('custrecord_expe_qty');
								else
									SOarray["custparam_nextexpectedquantity"]=0;

								nlapiLogExecution('Debug', 'Next Location', SOarray["custparam_nextlocation"]);
								nlapiLogExecution('Debug', 'Next Item Intr Id', SOarray["custparam_nextiteminternalid"]);


								var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getEnteredLocation));

								if (LocationSearch.length != 0) {
									for (var s = 0; s < LocationSearch.length; s++) {
										var getEndLocationInternalId = LocationSearch[s].getId();
										nlapiLogExecution('Debug', 'End Location Id', getEndLocationInternalId);
									}

									SOarray["custparam_beginlocationname"] = getBeginLocationName;
									SOarray["custparam_beginLocationname"] = getBeginLocationName;
									SOarray["custparam_endlocinternalid"] = getEndLocationInternalId;

									//To Remove case sensitive							 
									SOarray["custparam_endlocation"] = actBeginLocation;
									SOarray["custparam_clusterno"] = vClusterNo;

									//Added code specific to GSUSA.
									//Instead of checking weather the item has qty in pick face location and prompting user
									//to confirm replenishment at the end of the transaction .We are checking here only.
									var invtqty=0;

									var IsPickFaceLoc = 'F';
									var ItemInternalId=SOarray["custparam_item"];
									var EndLocInternalId=getEndLocationInternalId;
									var ExpectedQuantity=SOarray["custparam_expectedquantity"];
									var InvoiceRefNo=SOarray["custparam_invoicerefno"];
									var RecordInternalId=SOarray["custparam_recordinternalid"];
									var WaveNo=SOarray["custparam_waveno"];
									var ClusNo=SOarray["custparam_clusterno"];
									var OrdName=SOarray["name"];
									var ebizOrdNo=SOarray["custparam_ebizordno"];
									var getZoneNo=SOarray["custparam_ebizzoneno"];
									var whLocation=SOarray["custparam_whlocation"];
									IsPickFaceLoc = isPickFaceLocation(ItemInternalId,EndLocInternalId);

									nlapiLogExecution('Debug', 'ItemInternalId', ItemInternalId);
									nlapiLogExecution('Debug', 'EndLocInternalId', EndLocInternalId);
									nlapiLogExecution('Debug', 'ExpectedQuantity', ExpectedQuantity);
									nlapiLogExecution('Debug', 'IsPickFaceLoc', IsPickFaceLoc);
									nlapiLogExecution('Debug', 'InvoiceRefNo', InvoiceRefNo);
									nlapiLogExecution('Debug', 'RecordInternalId', RecordInternalId);
									nlapiLogExecution('Debug', 'WaveNo', WaveNo);
									nlapiLogExecution('Debug', 'ClusNo', ClusNo);
									nlapiLogExecution('Debug', 'OrdName', OrdName);
									nlapiLogExecution('Debug', 'ebizOrdNo', ebizOrdNo);
									nlapiLogExecution('Debug', 'whLocation', whLocation);


									if(IsPickFaceLoc=='T')
									{
										var openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemInternalId);

										if(openinventory!=null && openinventory!='')
										{
											nlapiLogExecution('Debug', 'Pick Face Inventory Length', openinventory.length);

											for(var x=0; x< openinventory.length;x++)
											{
												var vinvtqty=openinventory[x].getValue('custrecord_ebiz_qoh');

												if(vinvtqty==null || vinvtqty=='' || isNaN(vinvtqty))
												{
													vinvtqty=0;
												}

												invtqty=parseInt(invtqty)+parseInt(vinvtqty);
											}						
										}
									}

									if(invtqty==null || invtqty=='' || isNaN(invtqty))
									{
										invtqty=0;
									}

									nlapiLogExecution('Debug', 'invtqty', invtqty);

									if(IsPickFaceLoc=='T' && (parseInt(ExpectedQuantity)>parseInt(invtqty)))
									{
										var openreplns = new Array();
										openreplns=getOpenReplns(ItemInternalId,EndLocInternalId);

										if(openreplns!=null && openreplns!='')
										{
											for(var i=0; i< openreplns.length;i++)
											{
												var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
												var expqty=openreplns[i].getValue('custrecord_expe_qty');
												if(expqty == null || expqty == '')
												{
													expqty=0;
												}	
												nlapiLogExecution('Debug', 'open repln qty',expqty);
												nlapiLogExecution('Debug', 'getQuantity',ExpectedQuantity);

												var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
												if(skiptask==null || skiptask=='')
												{
													skiptask=1;
												}
												else
												{
													skiptask=parseInt(skiptask)+1;
												}

												nlapiLogExecution('Debug', 'skiptask',skiptask);

												nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

												var vPickType = request.getParameter('hdnpicktype');
												nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');

											}
											var SOarray=new Array();
											SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);
											SOarray["custparam_skipid"] = vSkipId;
											if(NextShowLocation!=null && NextShowLocation!='')
												SOarray["custparam_screenno"] = '11EXP';
											else
												SOarray["custparam_screenno"] = '11LOCEXP';	
											SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
											return;

										}
										else
										{
											var replengen = generateReplenishment(EndLocInternalId,ItemInternalId,whLocation,WaveNo);
											nlapiLogExecution('Debug', 'replengen',replengen);
											//If repeln generation failed, redirect to item so that user will perform short pick.
											if(replengen!=true)
											{
												nlapiLogExecution('ERROR', 'Replen Generation Failed.Insufficient inventory in Bulk location');
												nlapiLogExecution('ERROR', 'Redirecting to Item scan...');
												response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);					
												return;
											}

											var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
											if(skiptask==null || skiptask=='')
											{
												skiptask=1;
											}
											else
											{
												skiptask=parseInt(skiptask)+1;
											}

											nlapiLogExecution('Debug', 'skiptask',skiptask);

											nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

											var vPickType = request.getParameter('hdnpicktype');
											var SOarray=new Array();

											SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);

											SOarray["custparam_skipid"] = vSkipId;
											if(NextShowLocation!=null && NextShowLocation!='')
												SOarray["custparam_screenno"] = '11EXP';
											else
												SOarray["custparam_screenno"] = '11LOCEXP';	

											if(replengen!=true)
												SOarray["custparam_error"]='Replen Generation Failed.Insufficient inventory in Bulk location';
											else 
												SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';

											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
											return;
										}

									}				
									else
									{
										if(IsPickFaceLoc=='T' && (InvoiceRefNo==null || InvoiceRefNo==''))
										{
											if(openinventory!=null && openinventory!='')
											{
												var openinvtrecid = openinventory[0].getId();
												nlapiLogExecution('Debug', 'openinvtrecid', openinvtrecid);

												try
												{
													nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_invref_no', openinvtrecid);
												}
												catch(exp)
												{
													nlapiLogExecution('Debug', 'Exception in updating inventory reference number', exp);
												}
											}
										}
									}
									//End of code specific to GSUSA
									response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);					
								}						
							}
							//The below code is commented by Satish.N on 08/07/2012
							else if(vlocationfound=='F')
							{
								nlapiLogExecution('Debug', 'Into else vlocationfound', vlocationfound);
								//Upto here.

								var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',SOSearchResults[l].getId(),'custrecord_skiptask');
								if(skiptask==null || skiptask=='')
								{
									skiptask=1;
								}
								else
								{
									skiptask=parseInt(skiptask)+1;
								}

								nlapiLogExecution('Debug', 'skiptask',skiptask);

								nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[l].getId(), 'custrecord_skiptask', skiptask);

							}							
						}
						if(vlocationfound=='F'){
							SOarray["custparam_beginlocationname"]=NextShowLocation;
							SOarray["custparam_beginLocationname"]=NextShowLocation;
							SOarray["custparam_nextlocation"]=NextShowLocation;	
							SOarray["custparam_nextiteminternalid"]=NextShowItemId;

							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						}	
					}
				}*/
			else 
			{
				SOarray["custparam_beginlocationname"]=getBeginLocationName;
				//SOarray["custparam_beginLocationname"]=getBeginLocationName;
				SOarray["custparam_nextlocation"]=NextShowLocation;	
				SOarray["custparam_nextiteminternalid"]=NextShowItemId;
				nlapiLogExecution('Debug', 'NextShowLocation : ', NextShowLocation);
				nlapiLogExecution('Debug', 'Error: ', 'Did not scan the location');
				nlapiLogExecution('Debug', 'NextShowItemId : ', NextShowItemId);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);				
			}
		}		 
	}
}
/*function buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,vZoneId,vPickType)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	var SOColumns = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(WaveNo)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('Debug', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('Debug', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('Debug', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('Debug', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	SOarray["custparam_ebizordno"] = ebizOrdNo;

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('Debug', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('Debug', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('Debug', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('Debug', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_picktype"] = vPickType;
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}*/
