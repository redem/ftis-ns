/***************************************************************************
  eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_RFError.js,v $
 *     	   $Revision: 1.18.2.55.4.27.2.96.2.4 $
 *     	   $Date: 2015/11/24 16:14:16 $
 *     	   $Author: nneelam $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *     	   $Revision: 1.18.2.55.4.27.2.96.2.4 $
 *     	   $Date: 2015/11/24 16:14:16 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_RFError.js,v $
 * Revision 1.18.2.55.4.27.2.96.2.4  2015/11/24 16:14:16  nneelam
 * case# 201415819
 *
 * Revision 1.18.2.55.4.27.2.96.2.3  2015/11/16 10:31:36  skreddy
 * case 201414545
 * 2015.2 issue fix
 *
 * Revision 1.18.2.55.4.27.2.96.2.2  2015/11/14 06:44:20  grao
 * 2015.2 Issue Fixes 201415604
 *
 * Revision 1.18.2.55.4.27.2.96.2.1  2015/11/13 15:42:05  grao
 * 2015.2 Issue Fixes 201415579
 *
 * Revision 1.18.2.55.4.27.2.96  2015/07/20 15:32:48  skreddy
 * Case# 201413561
 * Signwarehouse issue fix
 *
 * Revision 1.18.2.55.4.27.2.95  2015/07/03 15:40:41  skreddy
 * Case# 201412907 & 201412906
 * Signwarehouse SB issue fix
 *
 * Revision 1.18.2.55.4.27.2.94  2015/03/02 15:19:38  snimmakayala
 * 201410541
 *
 * Revision 1.18.2.55.4.27.2.93  2015/02/13 15:00:01  sponnaganti
 * Jawbone Cluster Picking CR Changes
 * CR#  CR14US0323
 *
 * Revision 1.18.2.55.4.27.2.92  2015/01/02 15:04:17  skreddy
 * Case# 201411349
 * Two step replen process
 *
 * Revision 1.18.2.55.4.27.2.80  2014/05/30 00:26:50  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.18.2.55.4.27.2.79  2014/05/21 15:25:56  sponnaganti
 * case# 20148472
 * Stnd Bundle Issue fix
 *
 * Revision 1.18.2.55.4.27.2.78  2014/05/09 15:07:17  sponnaganti
 * case# 20148345
 * (Redirecting to error screen)
 *
 * Revision 1.18.2.55.4.27.2.77  2014/04/29 15:49:24  skavuri
 * Case# 20145821 SB Issue Fixed
 *
 * Revision 1.18.2.55.4.27.2.76  2014/04/25 16:19:27  skavuri
 * Case# 20148136,20148166,20148181 issue fixed
 *
 * Revision 1.18.2.55.4.27.2.75  2014/04/23 15:56:24  skavuri
 * Case# 20148128 issue fixed
 *
 * Revision 1.18.2.55.4.27.2.74  2014/04/23 09:49:37  skavuri
 * Case# 20148136 issue fixed
 *
 * Revision 1.18.2.55.4.27.2.73  2014/04/22 14:58:06  grao
 *  LL Sb RMA CARTChekin enhancement case#:20148087
 *
 * Revision 1.18.2.55.4.27.2.72  2014/04/22 14:57:05  grao
 *  LL Sb RMA CARTChekin enhancement case#:20148087
 *
 * Revision 1.18.2.55.4.27.2.71  2014/04/10 15:50:20  nneelam
 * case#  20144204
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.18.2.55.4.27.2.70  2014/04/03 16:11:52  skavuri
 * Case # 20140003 issue fixed
 *
 * Revision 1.18.2.55.4.27.2.69  2014/03/24 08:14:20  nneelam
 * case#  20127727
 * Standard Bundle Issue Fix.
 *
 * Revision 1.18.2.55.4.27.2.68  2014/03/05 12:02:35  snimmakayala
 * Case #: 20127523
 *
 * Revision 1.18.2.55.4.27.2.67  2014/02/19 12:56:09  snimmakayala
 * Case #: 20125411}
 * Convert the Master UPC Code scanned to Qty.
 *
 * Revision 1.18.2.55.4.27.2.66  2014/02/17 14:47:24  rmukkera
 * Case # 20127182
 *
 * Revision 1.18.2.55.4.27.2.65  2014/01/28 14:09:43  schepuri
 * 20126962
 * Standard Bundle Issue
 *
 * Revision 1.18.2.55.4.27.2.64  2013/12/30 15:34:12  rmukkera
 * Case # 20126584
 *
 * Revision 1.18.2.55.4.27.2.63  2013/12/23 16:15:30  mvarre
 * 20126416
 *
 * Revision 1.18.2.55.4.27.2.62  2013/12/18 15:06:27  rmukkera
 * Case # 20126184
 *
 * Revision 1.18.2.55.4.27.2.61  2013/12/16 11:16:18  rmukkera
 * Case # 20126321�
 *
 * Revision 1.18.2.55.4.27.2.60  2013/12/12 07:35:53  nneelam
 * Case# 20126066�
 * Carton No undefined in RF Pack screen when prev btn is clicked..
 *
 * Revision 1.18.2.55.4.27.2.59  2013/11/14 15:41:55  skreddy
 * Case# 20125719 & 20125658
 * Afosa SB issue fix
 *
 * Revision 1.18.2.55.4.27.2.58  2013/11/14 06:32:04  skreddy
 * Case# 201215000
 * Fast Picking issue fix
 *
 * Revision 1.18.2.55.4.27.2.57  2013/10/18 08:55:26  schepuri
 * 20125009
 *
 * Revision 1.18.2.55.4.27.2.56  2013/10/07 15:37:33  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * MHP Changes
 *
 * Revision 1.18.2.55.4.27.2.55  2013/10/01 08:37:48  grao
 * issue fixes related to the 20124723
 *
 * Revision 1.18.2.55.4.27.2.54  2013/09/30 15:59:30  rmukkera
 * Case#  20124623
 *
 * Revision 1.18.2.55.4.27.2.53  2013/09/17 15:13:26  rmukkera
 * Case# 20124319�
 *
 * Revision 1.18.2.55.4.27.2.52  2013/09/16 09:40:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.18.2.55.4.27.2.51  2013/09/12 15:33:57  nneelam
 * Case#. 20124383���
 * Redirect Link added   Issue Fix..
 *
 * Revision 1.18.2.55.4.27.2.50  2013/09/05 00:04:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Afosa changes
 * case#20123755
 *
 * Revision 1.18.2.55.4.27.2.49  2013/09/03 15:42:12  skreddy
 * Case# 20124195
 * standard bundle issue fix
 *
 * Revision 1.18.2.55.4.27.2.48  2013/08/29 15:03:20  spendyala
 * CASE201112/CR201113/LOG201121
 * Given NEW SKIP option when there is an invt hold flag
 * against the inventory record
 *
 * Revision 1.18.2.55.4.27.2.47  2013/08/29 07:09:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdchanges
 * case#20123755
 *
 * Revision 1.18.2.55.4.27.2.46  2013/08/27 16:34:09  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdchanges
 * case#20123755
 *
 * Revision 1.18.2.55.4.27.2.45  2013/08/23 05:59:00  snimmakayala
 * Case# 20124029
 * NLS - UAT ISSUES(RF BSU IS ACCEPTING ANY SHIP LP IRRESPECTIVE OF SITE)
 *
 * Revision 1.18.2.55.4.27.2.44  2013/08/21 14:22:59  skreddy
 * Case# 20123285
 *  restict space charater in lot# feild
 *
 * Revision 1.18.2.55.4.27.2.43  2013/08/19 16:11:04  rmukkera
 * Case# 20123915 ,20123912
 * Issue Fix
 *
 * Revision 1.18.2.55.4.27.2.42  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.18.2.55.4.27.2.41  2013/08/09 15:23:38  rmukkera
 * standard bundle issue fix
 *
 * Revision 1.18.2.55.4.27.2.40  2013/08/05 16:57:51  skreddy
 * Case# 20123677
 * issue rellated to fifo date
 *
 * Revision 1.18.2.55.4.27.2.39  2013/08/02 15:40:10  rmukkera
 * Case# 20123695
 * Issue fix for
 * RF Cart Putaway :: When we enter the Cart LP screen is displaying invalid Cart LP message.
 *
 * Revision 1.18.2.55.4.27.2.38  2013/07/25 08:54:29  rmukkera
 * landed cost cr changes
 *
 * Revision 1.18.2.55.4.27.2.37  2013/07/19 08:16:21  gkalla
 * Case# 20123527
 * Bulk picking CR for Nautilus
 *
 * Revision 1.18.2.55.4.27.2.36  2013/07/15 17:05:07  skreddy
 * Case# 20123446
 * Item info display CR
 *
 * Revision 1.18.2.55.4.27.2.35  2013/07/08 14:44:28  rrpulicherla
 * Case# 20123315
 * Inventory Move issues
 *
 * Revision 1.18.2.55.4.27.2.34  2013/06/25 14:52:04  mbpragada
 * Case# 20123178
 * Cart put away: System is accepting other sites bin location
 * while doing location exception
 *
 * Revision 1.18.2.55.4.27.2.33  2013/06/14 07:24:49  mbpragada
 * 20120490
 *
 * Revision 1.18.2.55.4.27.2.32  2013/06/14 07:06:44  gkalla
 * CASE201112/CR201113/LOG201121
 * PCT Case# 20122979. To select item status while RF WO confirm build
 *
 * Revision 1.18.2.55.4.27.2.31  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.18.2.55.4.27.2.30  2013/06/10 06:12:36  skreddy
 * CASE201112/CR201113/LOG201121
 * RF order picking for serial Item
 *
 * Revision 1.18.2.55.4.27.2.29  2013/06/04 15:30:16  skreddy
 * CASE201112/CR201113/LOG201121
 * Inventory move changes
 *
 * Revision 1.18.2.55.4.27.2.28  2013/06/04 10:54:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.18.2.55.4.27.2.27  2013/06/04 07:26:55  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Parsing CR for PCT
 *
 * Revision 1.18.2.55.4.27.2.26  2013/05/31 15:17:36  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.18.2.55.4.27.2.25  2013/05/14 14:16:37  schepuri
 * RF Replen confirm based on filters
 *
 * Revision 1.18.2.55.4.27.2.24  2013/05/08 13:31:35  spendyala
 * CASE201112/CR201113/LOG2012392
 * New parameter w.r.t Production QC is added.
 *
 * Revision 1.18.2.55.4.27.2.23  2013/05/07 15:37:53  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.18.2.55.4.27.2.22  2013/05/02 14:24:47  schepuri
 * updating remaning cube issue fix
 *
 * Revision 1.18.2.55.4.27.2.21  2013/05/01 01:10:12  kavitha
 * CASE201112/CR201113/LOG201121
 * TSG Issue fixes
 *
 * Revision 1.18.2.55.4.27.2.20  2013/04/30 15:57:08  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.18.2.55.4.27.2.19  2013/04/24 15:54:26  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.18.2.55.4.27.2.18  2013/04/19 15:44:18  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.18.2.55.4.27.2.17  2013/04/19 15:38:33  skreddy
 * CASE201112/CR201113/LOG201121
 * issue fixes
 *
 * Revision 1.18.2.55.4.27.2.16  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.18.2.55.4.27.2.15  2013/04/16 12:49:55  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixes.
 *
 * Revision 1.18.2.55.4.27.2.14  2013/04/10 15:48:54  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to qty exception in picking
 *
 * Revision 1.18.2.55.4.27.2.13  2013/04/10 05:37:39  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.18.2.55.4.27.2.12  2013/04/04 20:20:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixes
 *
 * Revision 1.18.2.55.4.27.2.11  2013/04/04 15:59:56  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to Location missing in check in
 *
 * Revision 1.18.2.55.4.27.2.10  2013/04/03 01:30:27  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.18.2.55.4.27.2.9  2013/04/02 14:38:18  schepuri
 * tsg sb putaway qty exception
 *
 * Revision 1.18.2.55.4.27.2.8  2013/03/22 11:44:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.18.2.55.4.27.2.7  2013/03/15 15:00:13  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.18.2.55.4.27.2.6  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.18.2.55.4.27.2.5  2013/03/08 14:35:02  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.18.2.55.4.27.2.4  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.18.2.55.4.27.2.3  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.18.2.55.4.27.2.2  2013/02/27 13:06:28  rmukkera
 * monobid production bundle changes were merged to cvs with tag
 * t_eBN_2013_1_StdBundle_2
 *
 * Revision 1.18.2.55.4.27.2.1  2013/02/26 12:56:03  skreddy
 * CASE201112/CR201113/LOG201121
 * merged boombah changes
 *
 * Revision 1.18.2.55.4.27  2013/02/19 15:01:17  dtummala
 * CASE201112/CR201113/LOG201121
 * DJN Modifications in inbound BuildCart and PutawayByCart
 *
 * Revision 1.18.2.55  2012/08/28 15:49:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Added screen for cluster pick qty exception.
 *
 * Revision 1.18.2.54  2012/08/17 08:37:17  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new condition to redirect out bound StageLocation.
 *
 * Revision 1.18.2.53  2012/08/09 07:07:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new condition for Replen Menu.
 *
 * Revision 1.18.2.52  2012/08/01 07:17:31  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Routing based on Loc group seq
 *
 * Revision 1.18.2.51  2012/07/27 12:56:36  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue in close carton
 *
 * Revision 1.18.2.50  2012/07/20 15:28:57  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new Screen for Picking LOT#.
 *
 * Revision 1.18.2.49  2012/07/18 06:22:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Added condition for pick location exception.
 *
 * Revision 1.18.2.48  2012/06/28 08:34:48  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.18.2.47  2012/06/25 14:54:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.18.2.46  2012/06/25 11:32:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.18.2.45  2012/06/22 12:27:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.18.2.44  2012/06/21 10:15:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.18.2.43  2012/06/18 07:34:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.18.2.42  2012/06/15 15:20:47  mbpragada
 * 20120490
 *
 * Revision 1.18.2.41  2012/06/15 10:10:44  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.18.2.40  2012/06/15 07:14:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new error screen for sort scan and zone complete.
 *
 * Revision 1.18.2.39  2012/06/11 13:40:04  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new error screens for Scrap Adjt.
 *
 * Revision 1.18.2.38  2012/06/06 15:27:05  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new error screen for QCProd and scrapqty.
 *
 * Revision 1.18.2.37  2012/06/06 07:43:31  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.18.2.36  2012/06/04 11:47:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * HUBPEN - REPLEN ISSUE FIXES
 *
 * Revision 1.18.2.34  2012/05/31 12:49:33  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.18.2.33  2012/05/28 15:27:43  spendyala
 * CASE201112/CR201113/LOG201121
 * passing parameter to query string  was missing.
 *
 * Revision 1.18.2.32  2012/05/24 15:26:06  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to invalid option entry.
 *
 * Revision 1.18.2.31  2012/05/21 13:07:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Auto Packing
 *
 * Revision 1.18.2.30  2012/05/17 13:29:40  spendyala
 * CASE201112/CR201113/LOG201121
 * error screen for create invt Exipry date and FIFO date .
 *
 * Revision 1.18.2.29  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.18.2.28  2012/05/02 12:36:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Checkin changes
 *
 * Revision 1.18.2.27  2012/04/26 07:10:20  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issue unable to scan the item when we placed two items in a single
 *  location.
 *
 * Revision 1.18.2.26  2012/04/25 15:37:13  spendyala
 * CASE201112/CR201113/LOG201121
 * While Showing Recommended qty POoverage is not considered.
 *
 * Revision 1.18.2.25  2012/04/24 16:11:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.18.2.24  2012/04/24 13:59:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.18.2.23  2012/04/24 13:49:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.18.2.22  2012/04/23 14:02:44  schepuri
 * CASE201112/CR201113/LOG201121
 *  validation on checkin once after checkin already completed
 *
 * Revision 1.18.2.21  2012/04/19 15:00:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.18.2.20  2012/04/19 12:11:36  schepuri
 * CASE201112/CR201113/LOG201121
 * validation on checkin once after checkin already completed
 *
 * Revision 1.18.2.19  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.18.2.18  2012/04/18 13:37:46  schepuri
 * CASE201112/CR201113/LOG201121
 * validating so qty with pick qty
 *
 * Revision 1.18.2.17  2012/04/17 10:38:39  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Cart To Checkin
 *
 * Revision 1.18.2.16  2012/04/11 22:04:19  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related navigation is resolved.
 *
 * Revision 1.18.2.15  2012/04/11 12:45:53  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF PO location Exception
 *
 * Revision 1.18.2.14  2012/04/10 23:07:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Navigation to previous screen issue is resolved.
 *
 * Revision 1.18.2.13  2012/04/06 14:58:01  spendyala
 * CASE201112/CR201113/LOG201121
 * Added soarray[whlocation]
 *
 * Revision 1.18.2.12  2012/03/28 12:52:25  schepuri
 * CASE201112/CR201113/LOG201121
 * validating from entering another loc other than fetched location
 *
 * Revision 1.18.2.11  2012/03/21 15:06:16  spendyala
 * CASE201112/CR201113/LOG201121
 * Added with new fields for cyclecount.
 *
 * Revision 1.18.2.10  2012/03/14 07:24:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Checkin issues for WBC
 *
 * Revision 1.18.2.9  2012/03/12 13:56:17  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing  Record id to the query string.
 *
 * Revision 1.18.2.8  2012/03/12 08:05:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.26  2012/03/12 07:56:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.25  2012/03/09 07:49:06  spendyala
 * CASE201112/CR201113/LOG201121
 * code merged upto 1.18.2.7
 *
 * Revision 1.24  2012/03/09 07:16:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.23  2012/03/02 13:53:51  rmukkera
 * CASE201112/CR201113/LOG201121
 *  Added cart putaway related functionality
 *
 * Revision 1.22  2012/02/13 12:53:45  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.18.2.4
 *
 * Revision 1.21  2012/02/02 09:06:58  rmukkera
 * CASE201112/CR201113/LOG201121
 * checkin and putaway cart file related modifications.
 *
 * Revision 1.20  2012/01/27 07:03:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.19  2012/01/19 16:00:00  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.18  2011/12/30 20:20:17  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/12/30 14:28:22  spendyala
 * CASE201112/CR201113/LOG201121
 * added Error screens for Rf ExpDate-checkin and MfgDate-checkin.
 *
 * Revision 1.16  2011/10/03 22:47:55  snimmakayala
 * CASE201112/CR201113/LOG20112
 * RF Picking Issues
 *
 * Revision 1.15  2011/10/03 14:41:35  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Error Screen added for Serial# wrong entry
 *
 * Revision 1.14  2011/09/30 16:40:59  gkalla
 * CASE201112/CR201113/LOG201121
 * For BOM Report in Work order screen
 *
 * Revision 1.13  2011/09/30 15:15:54  gkalla
 * CASE201112/CR201113/LOG201121
 * For BOM Report in Work order screen
 *
 * Revision 1.12  2011/09/28 12:10:52  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/09/20 12:40:48  mbpragada
 * CASE201112/CR201113/LOG201121
 * Added 2 new  RF screens
 *
 * Revision 1.9  2011/09/14 14:39:02  schepuri
 * CASE201112/CR201113/LOG201121
 * duplicate lps
 *
 * Revision 1.8  2011/08/31 11:10:46  schepuri
 * CASE201112/CR201113/LOG201121
 * Cycle Count to go back to  previous screen
 *
 * Revision 1.7  2011/08/31 09:46:20  schepuri
 * CASE201112/CR201113/LOG201121
 * Cycle Count to go back to  previous screen
 *
 * Revision 1.6  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.5  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.4  2011/04/21 07:03:02  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.3  2011/04/20 13:34:26  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function Error(request, response){
	if (request.getMethod() == 'GET') {
		//	Get the Error Message from the previous screen, which is passed as a parameter		
		getError = request.getParameter('custparam_error');
		nlapiLogExecution('DEBUG', 'Into Request', getError);
		var screenNo=request.getParameter('custparam_screenno');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage',getLanguage);

		var st0,st1,st2;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "LP NO V&#193;LIDA";
			st1 = "ERROR";
			st2 = "CONTINUAR";


		}
		else
		{
			st0 = "INVALID LP";
			st1 = "ERROR";
			st2 = "CONTINUE ";


		}
		var functionkeyHtml=getFunctionkeyScript('_rf_error'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_error' method='POST'>";
		html = html + "		<table>";
		if(screenNo!=null && screenNo!='' && (screenNo=='PL2'||screenNo=='CONFRMInvtMove'))
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><br><label>" + getError + "</label>";
			html = html + "				</td>";
			html = html + "			</tr>";

		}
		else
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>"+st1+": <br><label>" + getError + "</label>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2+" <input name='cmdContinue' type='submit' value='F8'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var optedEvent = request.getParameter('cmdContinue');
		var getError = request.getParameter('custparam_error');
		var getScreenNo = request.getParameter('custparam_screenno');
		nlapiLogExecution('DEBUG', 'Screen No', getScreenNo);

		var POarray = new Array();
		var SOarray = new Array();
		var CIarray = new Array();
		var IMarray = new Array();
		var PAarray = new Array();
		var ShipArray = new Array();
		var Reparray = new Array();
		var CCarray = new Array();//added by shylaja
		var CRTarray = new Array();
		var CRTPutawayarray=new Array();
		var pickQtyExceptionarray = new Array();
		var POarrayget=new Array();
		var packlistArray= new Array();
		var PackArray=new Array();
		var PRODArray=new Array();
		var UCCarray=new Array();
		var UPCArray=new Array();
		var BuildcartArray=new Array();
		var lparray=new Array();	
		PRODArray["custparam_SO"]=request.getParameter('custparam_SO');
		PRODArray["custparam_SOID"]=request.getParameter('custparam_SOID');
		PRODArray["custparam_vcount"]=request.getParameter('custparam_vcount');
		PRODArray["custparam_itemid"]=request.getParameter('custparam_itemid');
		PRODArray["custparam_binlocid"]=request.getParameter('custparam_binlocid');
		PRODArray["custparam_lp"]=request.getParameter('custparam_lp');
		PRODArray["custparam_Item"]=request.getParameter('custparam_Item');
		PRODArray["custparam_option"]=request.getParameter('custparam_option');
		PRODArray["custparam_QCStatus"]=request.getParameter('custparam_QCStatus');
		PRODArray["custparam_tempoption"]=request.getParameter('custparam_tempoption');

		POarrayget["custparam_error"] = request.getParameter('custparam_error');
		POarrayget["custparam_screenno"] = request.getParameter('custparam_screenno');
		POarrayget["custparam_poid"] = request.getParameter('custparam_poid');
		POarrayget["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarrayget["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarrayget["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarrayget["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarrayget["custparam_trantype"] = request.getParameter('custparam_trantype');
		POarrayget["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarrayget["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		POarrayget["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		POarrayget["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		POarrayget["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		POarrayget["custparam_actualbegintime"] = request.getParameter('custparam_actualbegintime');
		POarrayget["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');

		pickQtyExceptionarray["custparam_item"] = request.getParameter('custparam_item');
		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/		
		pickQtyExceptionarray["custparam_itemname"] = request.getParameter('custparam_itemname');
		/* Up to here */ 
		pickQtyExceptionarray["custparam_nextiteminternalid"] = request.getParameter('custparam_nextiteminternalid');
		pickQtyExceptionarray["custparam_picktype"] = request.getParameter('custparam_picktype');
		pickQtyExceptionarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		pickQtyExceptionarray["custparam_enteredQty"] = request.getParameter('custparam_item');
		pickQtyExceptionarray["custparam_enteredReason"] = request.getParameter('custparam_enteredReason');
		pickQtyExceptionarray["custparam_waveno"] = request.getParameter('custparam_waveno');
		pickQtyExceptionarray["custparam_recordinternalid"] = request.getParameter('custparam_recordinternalid');
		pickQtyExceptionarray["custparam_containerlpno"] = request.getParameter('custparam_containerlpno');
		pickQtyExceptionarray["custparam_expectedquantity"] = request.getParameter('custparam_expectedquantity');
		pickQtyExceptionarray["custparam_beginLocation"] = request.getParameter('custparam_beginLocation');
		pickQtyExceptionarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		pickQtyExceptionarray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');
		pickQtyExceptionarray["custparam_dolineid"] = request.getParameter('custparam_dolineid');
		pickQtyExceptionarray["custparam_invoicerefno"] = request.getParameter('custparam_invoicerefno');
		pickQtyExceptionarray["custparam_beginlocationname"] = request.getParameter('custparam_beginlocationname');
		pickQtyExceptionarray["custparam_endlocinternalid"] = request.getParameter('custparam_endlocinternalid');
		pickQtyExceptionarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
		pickQtyExceptionarray["custparam_orderlineno"] = request.getParameter('custparam_orderlineno');
		pickQtyExceptionarray["custparam_clusterno"] = request.getParameter('custparam_clusterno');
		pickQtyExceptionarray["custparam_batchno"] = request.getParameter('custparam_batchno');
		pickQtyExceptionarray["custparam_noofrecords"] = request.getParameter('custparam_noofrecords');
		pickQtyExceptionarray["custparam_nextlocation"] = request.getParameter('custparam_nextlocation');
		pickQtyExceptionarray["name"] = request.getParameter('name');
		pickQtyExceptionarray["custparam_containersize"] = request.getParameter('custparam_containersize');
		pickQtyExceptionarray["custparam_ebizordno"] = request.getParameter('custparam_ebizordno');
		pickQtyExceptionarray["custparam_number"] = request.getParameter('custparam_number');
		pickQtyExceptionarray["custparam_RecType"] = request.getParameter('custparam_RecType');
		pickQtyExceptionarray["custparam_SerOut"] = request.getParameter('custparam_SerOut');
		pickQtyExceptionarray["custparam_SerIn"] = request.getParameter('custparam_SerIn');
		pickQtyExceptionarray["custparam_nextiteminternalid"] = request.getParameter('custparam_nextiteminternalid');
		pickQtyExceptionarray["custparam_skipid"] = request.getParameter('custparam_skipid');
		pickQtyExceptionarray["custparam_autocont"] = request.getParameter('custparam_autocont');
		pickQtyExceptionarray["custparam_newcontainerlp"] = request.getParameter('custparam_newcontainerlp');
		pickQtyExceptionarray["custparam_whcompany"] = request.getParameter('custparam_whcompany');
		pickQtyExceptionarray["custparam_item"] = request.getParameter('custparam_item');
		pickQtyExceptionarray["custparam_item"] = request.getParameter('custparam_item');
		pickQtyExceptionarray["custparam_ebizzoneno"]= request.getParameter('custparam_ebizzoneno');
		pickQtyExceptionarray['custparam_nextrecordid']=request.getParameter('custparam_nextrecordid');
		/* code merged from monobid on feb 27th 2013 by Radhika */
		pickQtyExceptionarray["custparam_Actbatchno"] = request.getParameter('custparam_Actbatchno');
		pickQtyExceptionarray["custparam_Expbatchno"] = request.getParameter('custparam_Expbatchno');
		pickQtyExceptionarray["custparam_remainpickqty"] = request.getParameter('custparam_remainpickqty');
		pickQtyExceptionarray['custparam_itemType']=request.getParameter('custparam_itemType');
		pickQtyExceptionarray["custparam_remainpickqty"] = request.getParameter('hdnremainpickqty');
		pickQtyExceptionarray["custparam_Actbatchno"]=request.getParameter('custparam_Actbatchno');
		pickQtyExceptionarray["custparam_Expbatchno"] = request.getParameter('custparam_Expbatchno');
		pickQtyExceptionarray["custparam_nextbatchno"] = request.getParameter('custparam_nextbatchno');
		pickQtyExceptionarray['custparam_fastpick']=request.getParameter('custparam_fastpick');
		pickQtyExceptionarray['custparam_language']=request.getParameter('custparam_language');
		pickQtyExceptionarray['custparam_serialscanflag']=request.getParameter('custparam_serialscanflag');

		packlistArray["custparam_printername"]=request.getParameter('custparam_printername');
		packlistArray["custparam_cartonno"]=request.getParameter('custparam_cartonno');
		packlistArray["custparam_fono"]=request.getParameter('custparam_fono');
		// From Check-In process
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('custparam_poqtyentered');
		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('custparam_polinepackcode');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');
		POarray["custparam_polineitemstatusValue"] = request.getParameter('custparam_polineitemstatusValue');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		POarray["custparam_actualbegintime"] = request.getParameter('custparam_actualbegintime');
		POarray["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');
		POarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		POarray["custparam_batchno"] = request.getParameter('custparam_batchno');
		POarray["custparam_fifodate"]=request.getParameter('custparam_fifodate');
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		POarray["custparam_remainingCube"] = request.getParameter('custparam_remainingCube');
		POarray["custparam_putmethod"]=request.getParameter('custparam_putmethod');
		POarray["custparam_putrule"] = request.getParameter('custparam_putrule');
		POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		POarray["custparam_countryoforigin"] = request.getParameter('custparam_countryoforigin');
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
		POarray["custparam_shelflife"] = request.getParameter('custparam_shelflife');
		POarray["custparam_captureexpirydate"] = request.getParameter('custparam_captureexpirydate');
		POarray["custparam_capturefifodate"] = request.getParameter('custparam_capturefifodate');
		POarray["custparam_enteredLPNo"] = request.getParameter('custparam_enteredLPNo');
		/* Up to here */ 		
		/* code merged from monobid on feb 27th 2013 by Radhika */		
		POarray["custparam_autolot"] = request.getParameter('custparam_autolot');
		/*** The below code is merged from Lexjet production account on 07th Mar13 by Santosh as part of Standard bundle ***/
		POarray["custparam_expectedquantity"] = request.getParameter('custparam_expectedquantity');
		POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		POarray["custparam_locid"] = request.getParameter('custparam_locid');
		/*** up to here ***/
		POarray["custparam_countryid"] = request.getParameter('custparam_countryid');
		// From Putaway process
		POarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		POarray["custparam_quantity"] = request.getParameter('custparam_quantity');
		POarray["custparam_exceptionquantity"] = request.getParameter('custparam_exceptionquantity');
		POarray["custparam_exceptionQuantityflag"] = request.getParameter('custparam_exceptionQuantityflag');
		POarray["custparam_location"] = request.getParameter('custparam_location');
		POarray["custparam_beginlocation"] =  request.getParameter('custparam_beginlocation');
		POarray["custparam_item"] = request.getParameter('custparam_item');
		POarray["custparam_itemDescription"] = request.getParameter('custparam_itemDescription');
		POarray["custparam_itemid"] = request.getParameter('custparam_itemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_polinenumber"] = request.getParameter('custparam_polinenumber');
		POarray["custparam_poitemstatus"] = request.getParameter('custparam_poitemstatus');
		POarray["custparam_popackcode"] = request.getParameter('custparam_popackcode');
		POarray["custparam_beginlocation"] = request.getParameter('custparam_beginlocation');
		POarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
		POarray["custparam_itemtext"] = request.getParameter('custparam_itemtext');
		POarray["custparam_mfgdate"] = request.getParameter('custparam_mfgdate');
		POarray["custparam_expdate"] = request.getParameter('custparam_expdate');
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		POarray["custparam_recordid"] = request.getParameter('custparam_recordid');
		POarray["custparam_option"] = request.getParameter('custparam_option');
		POarray["custparam_recordcount"] = request.getParameter('custparam_recordcount');
		POarray["custparam_lpNumbersArray"] = request.getParameter('custparam_lpNumbersArray');
		POarray["custparam_skipid"] = request.getParameter('custparam_skipid');
		POarray["custparam_trantype"] = request.getParameter('custparam_trantype');
		POarray["custparam_itemstatus"] = request.getParameter('custparam_itemstatus');
		nlapiLogExecution('DEBUG', 'trantype', POarray["custparam_trantype"]);
		POarray["custparam_uomidtext"] = request.getParameter('custparam_uomidtext');
		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/		
		POarray["custparam_number"] = parseFloat(request.getParameter('custparam_number'));
		/* Up to here */ 		
		POarray["custparam_polineitemlp"]= request.getParameter('custparam_polineitemlp');
		POarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		POarray["custparam_confirmedLpCount"]= request.getParameter('custparam_confirmedLpCount');
		POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		POarray["custparam_locid"] = request.getParameter('custparam_locid');

		POarray["custparam_zone"] = request.getParameter('custparam_zone');
		nlapiLogExecution('DEBUG', 'Zone', POarray["custparam_zone"]);
		POarray["custparam_binlocation"] = request.getParameter('custparam_binlocation');
		nlapiLogExecution('DEBUG', 'binlocation', POarray["custparam_binlocation"]);
		/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle ***/		
		POarray["custparam_woname"] =request.getParameter('custparam_woname');
		POarray["custparam_woid"]=request.getParameter('custparam_woid');
		POarray["hdnBeginlocId"] =request.getParameter('hdnBeginlocId');
		POarray["custparam_batchexpirydate"] = request.getParameter('custparam_batchexpirydate');
		/*** up to here ***/
		POarray["custparam_exceptionqty"] = request.getParameter('custparam_exceptionqty');
		POarray["custparam_qtyexceptionflag"] = request.getParameter('custparam_qtyexceptionflag');
		POarray["custparam_actscanbatchno"] = request.getParameter('custparam_actscanbatchno');
		POarray["custparam_remainingqty"]=request.getParameter('custparam_remainingqty');
		POarray["custparam_language"] = request.getParameter('custparam_language');
		POarray["custparam_number"] = request.getParameter('custparam_number');
		//Case# 201410723/201410724 starts
		POarray["custparam_packcode"] = request.getParameter('custparam_packcode');
		//Case# 201410723/201410724 ends
		//added by surendra
		POarray["custparam_expectedbinsize1"]=request.getParameter('custparam_expectedbinsize1');
		POarray["custparam_expectedbinsize2"]=request.getParameter('custparam_expectedbinsize2');
		POarray["custparam_userAccountId"] = request.getParameter('custparam_userAccountId');
		POarray["custparam_putawayscreen"] = request.getParameter('custparam_putawayscreen');
		POarray["custparam_newlpno"] = request.getParameter('custparam_newlpno');

		//end
		//case # 20148181
		POarray["custparam_screenno"] = request.getParameter('custparam_screenno');
		POarray["custparam_error"] = request.getParameter('custparam_error');
		POarray["custparam_repno"] = request.getParameter('custparam_repno');
		POarray["custparam_zoneno"] = request.getParameter('custparam_zoneno');
		POarray["custparam_cartno"] = request.getParameter('custparam_cartno');
		// From Picking process
		SOarray["custparam_waveno"] = request.getParameter('custparam_waveno');
		nlapiLogExecution('DEBUG', 'custparam_waveno', request.getParameter('custparam_waveno'));
		SOarray["custparam_recordinternalid"] = request.getParameter('custparam_recordinternalid');
		SOarray["custparam_containerlpno"] = request.getParameter('custparam_containerlpno');
		SOarray["custparam_expectedquantity"] = request.getParameter('custparam_expectedquantity');
		SOarray["custparam_beginLocation"] = request.getParameter('custparam_beginLocation');
		SOarray["custparam_item"] = request.getParameter('custparam_item');
		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/				
		SOarray["custparam_itemname"] = request.getParameter('custparam_itemname');
		SOarray["custparam_LP"] = request.getParameter('custparam_LP');
		/* Up to here */ 		
		SOarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');
		SOarray["custparam_dolineid"] = request.getParameter('custparam_dolineid');
		SOarray["custparam_invoicerefno"] = request.getParameter('custparam_invoicerefno');
		//Case# 201410587 starts
		//SOarray["custparam_beginlocationname"] = request.getParameter('custparam_beginLocationname');
		SOarray["custparam_beginlocationname"] = request.getParameter('custparam_beginlocationname');
		//Case# 201410587 ends
		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/		
//		The below line is added by Satish.N on 12/10/2012
		SOarray["custparam_beginLocationname"] = request.getParameter('custparam_beginLocationname');
		/* Up to here */ 
		SOarray["custparam_endlocinternalid"] = request.getParameter('custparam_endlocinternalid');
		SOarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
		SOarray["custparam_orderlineno"] = request.getParameter('custparam_orderlineno');
		SOarray["custparam_nextlocation"]=request.getParameter('custparam_nextlocation');
		SOarray["custparam_picktype"]=request.getParameter('custparam_picktype');
		SOarray["custparam_nooflocrecords"]=request.getParameter('custparam_nooflocrecords');
		SOarray["custparam_nextiteminternalid"]=request.getParameter('custparam_nextiteminternalid');
		SOarray["custparam_nextitem"]=request.getParameter('custparam_nextitem');
		SOarray["name"]=request.getParameter('name');
		SOarray["custparam_clusterno"] = request.getParameter('custparam_clusterno');        
		SOarray["custparam_number"]= request.getParameter('custparam_number');   
		SOarray["custparam_whlocation"]= request.getParameter('custparam_whlocation');   
		SOarray["custparam_skipid"] = request.getParameter('custparam_skipid');
		SOarray["custparam_ebizzoneno"]= request.getParameter('custparam_ebizzoneno');
		SOarray["custparam_nextexpectedquantity"]=request.getParameter('custparam_nextexpectedquantity');
		SOarray['custparam_wmslocation']=request.getParameter('custparam_wmslocation');
		SOarray['custpage_multipleitemscan']=request.getParameter('custpage_multipleitemscan');
		SOarray['custparam_nextrecordid']=request.getParameter('custparam_nextrecordid');
		SOarray["custparam_ebizordno"]=request.getParameter('custparam_ebizordno');
		SOarray["custparam_batchno"]=request.getParameter('custparam_batchno');
		SOarray["custparam_Exceptionflag"]=request.getParameter('custparam_Exceptionflag');
		SOarray["custparam_newcontainerlp"] = request.getParameter('custparam_newcontainerlp');
		/* code merged from monobid on feb 27th 2013 by Radhika */		
		SOarray["custparam_itemstatus"] = request.getParameter('custparam_itemstatus');
		SOarray["custparam_itemType"] = request.getParameter('custparam_itemType');
		SOarray["custparam_Actbatchno"] = request.getParameter('custparam_Actbatchno');
		SOarray["custparam_Expbatchno"] = request.getParameter('custparam_Expbatchno');
		SOarray["custparam_remainpickqty"] = request.getParameter('custparam_remainpickqty');
		SOarray["custparam_nextbatchno"] = request.getParameter('custparam_nextbatchno');
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/             
		SOarray["custparam_locationId"] = request.getParameter('custparam_locationId');
		/* Up to here */ 
		/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle ***/             		
		SOarray["custparam_woid"] = request.getParameter('custparam_woid');
		/*** up to here ***/
		SOarray["custparam_RecType"] = request.getParameter('custparam_RecType');
		SOarray["custparam_detailtask"] = request.getParameter('custparam_detailtask');
		SOarray["custparam_serialscanned"] = request.getParameter('custparam_serialscanned');
		SOarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		SOarray["custparam_LP"] = request.getParameter('custparam_LP');
		SOarray["custparam_enteredqty"] =  request.getParameter('custparam_enteredqty');
		SOarray["custparam_remqty"] =  request.getParameter('custparam_remqty');
		SOarray["custparam_EntLoc"] = request.getParameter('custparam_EntLoc');
		SOarray["custparam_EntLocRec"] = request.getParameter('custparam_EntLocRec');
		SOarray["custparam_EntLocId"] = request.getParameter('custparam_EntLocId');
		SOarray["custparam_Exc"] = request.getParameter('custparam_Exc');
		SOarray["custparam_entLotId"] = request.getParameter('custparam_entLotId');
		SOarray["custparam_SerIn"] = request.getParameter('custparam_SerIn');
		SOarray["custparam_SerOut"] = request.getParameter('custparam_SerOut');
		SOarray["custparam_redirectscreen"] = request.getParameter('custparam_redirectscreen');
		SOarray["custparam_enteredQty"] = request.getParameter('custparam_enteredQty');
		// case 20123446 start
		SOarray["custparam_itemInstructions"] = request.getParameter('custparam_itemInstructions');
		// case 20123446 end
		SOarray["custparam_error"]=request.getParameter('custparam_error');
		SOarray["custparam_zoneno"]=request.getParameter('custparam_zoneno');
		SOarray["custparam_ebizzoneno"]=request.getParameter('custparam_ebizzoneno');
		SOarray["custparam_ondemandstage"] = request.getParameter('custparam_ondemandstage');
		SOarray["custparam_ClosetoStage"]=request.getParameter('custparam_ClosetoStage');
		SOarray["custparam_fetchedcontainerlp"]=request.getParameter('custparam_fetchedcontainerlp');
		SOarray["custparam_fastpick"] = request.getParameter('custparam_fastpick');
		SOarray["custparam_istaskskipped"] = request.getParameter('custparam_istaskskipped');
		SOarray["custparam_remainqty"] = request.getParameter('custparam_remainqty');
		SOarray["custparam_language"] = request.getParameter('custparam_language');
		//case # 20126066� added the below parameter.
		SOarray["custparam_containerno"]=request.getParameter('custparam_containerno');
		SOarray["custparam_foname"]=request.getParameter('custparam_foname');
		SOarray["custparam_venterzone"]=request.getParameter('custparam_venterzone');
		SOarray["custparam_orderno"]=request.getParameter('custparam_orderno');
		SOarray['custparam_serialscanflag']=request.getParameter('custparam_serialscanflag');

		SOarray["custparam_bulkpickflag"]=request.getParameter('custparam_bulkpickflag');
		// From Create Inventory
SOarray["custparam_displayquantity"] = request.getParameter('custparam_displayquantity');
		CIarray["custparam_binlocationid"] = request.getParameter('custparam_binlocationid');
		CIarray["custparam_binlocationname"] = request.getParameter('custparam_binlocationname');
		CIarray["custparam_itemid"] = request.getParameter('custparam_itemid');
		CIarray["custparam_item"] = request.getParameter('custparam_item');
		CIarray["custparam_quantity"] = request.getParameter('custparam_quantity');
		CIarray["custparam_itemstatus"] = request.getParameter('custparam_itemstatus');
		CIarray["custparam_itemstatusid"] = request.getParameter('custparam_itemstatusid');
		CIarray["custparam_packcode"] = request.getParameter('custparam_packcode');
		CIarray["custparam_packcodeid"] = request.getParameter('custparam_packcodeid');
		CIarray["custparam_adjustmenttype"] = request.getParameter('custparam_adjustmenttype');
		CIarray["custparam_adjustmenttypeid"] = request.getParameter('custparam_adjustmenttypeid');
		CIarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		CIarray["custparam_locationId"] = request.getParameter('custparam_locationId');
		CIarray["custparam_itemlp"] = request.getParameter('custparam_itemlp');
		CIarray["custparam_mfgdate"] = request.getParameter('custparam_mfgdate');
		CIarray["custparam_expdate"] = request.getParameter('custparam_expdate');
		CIarray["custparam_bestbeforedate"] = request.getParameter('custparam_bestbeforedate');
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
		CIarray["custparam_shelflife"] = request.getParameter('custparam_shelflife');
		CIarray["custparam_captureexpirydate"] = request.getParameter('custparam_captureexpirydate');
		CIarray["custparam_capturefifodate"] = request.getParameter('custparam_capturefifodate');
		/* Up to here */ 
		/* code merged from monobid on feb 27th 2013 by Radhika */             
		CIarray["custparam_BatchId"] = request.getParameter('custparam_BatchId');
		CIarray["custparam_notes"] = request.getParameter('custparam_notes');
		CIarray["custparam_BatchNo"] = request.getParameter('custparam_BatchNo');
		CIarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		CIarray["custparam_number"] = request.getParameter('custparam_number');
		CIarray["custparam_inventoryId"] =request.getParameter('custparam_inventoryId');

		CIarray["custparam_actscanbatchno"] = request.getParameter('custparam_actscanbatchno');
		CIarray["custparam_language"] = request.getParameter('custparam_language');

		//From Inventory Move
		IMarray["custparam_imbinlocationid"] = request.getParameter('custparam_imbinlocationid');
		IMarray["custparam_imbinlocationname"] = request.getParameter('custparam_imbinlocationname');
		IMarray["custparam_imitemid"] = request.getParameter('custparam_imitemid');
		IMarray["custparam_imitem"] = request.getParameter('custparam_imitem');
		IMarray["custparam_totquantity"] = request.getParameter('custparam_totquantity');
		IMarray["custparam_availquantity"] = request.getParameter('custparam_availquantity');
		IMarray["custparam_moveqty"] = request.getParameter('custparam_moveqty');
		IMarray["custparam_status"] = request.getParameter('custparam_status');
		IMarray["custparam_statusid"] = request.getParameter('custparam_statusid');
		IMarray["custparam_uom"] = request.getParameter('custparam_uom');
		IMarray["custparam_uomid"] = request.getParameter('custparam_uomid');		
		IMarray["custparam_lot"] = request.getParameter('custparam_lot');
		IMarray["custparam_lotid"] = request.getParameter('custparam_lotid');
		IMarray["custparam_lp"] = request.getParameter('custparam_lp');
		IMarray["custparam_lpid"] = request.getParameter('custparam_lpid');
		IMarray["custparam_itemtype"]=request.getParameter('custparam_itemtype');
		IMarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		IMarray["custparam_newLP"] = request.getParameter('custparam_newLP');
		IMarray["custparam_locationId"] = request.getParameter('custparam_locationId');
		IMarray["custparam_newbinlocationid"] = request.getParameter('custparam_newbinlocationid');
		IMarray["custparam_newbinlocationname"] = request.getParameter('custparam_newbinlocationname');
		IMarray["custparam_invtrecid"] = request.getParameter('custparam_invtrecid');
		IMarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
		IMarray["custparam_adjusttype"] = request.getParameter('custparam_adjusttype');
		IMarray["custparam_adjustflag"] = request.getParameter('custparam_adjustflag');
		IMarray["custparam_batch"] = request.getParameter('custparam_batch');
		IMarray["custparam_actualbegintime"] = request.getParameter('custparam_actualbegintime');
		IMarray["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');
		IMarray["custparam_totqtymoveed"] = request.getParameter('custparam_totqtymoveed');
		IMarray["custparam_itemdesc"] = request.getParameter('custparam_itemdesc');
		IMarray["custparam_compid"] = request.getParameter('custparam_compid');
		IMarray["custparam_fifodate"]=request.getParameter('custparam_fifodate');
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/		
		IMarray["custparam_searchtype"] = request.getParameter('custparam_searchtype');
		IMarray["custparam_NewPutMethodID"] = request.getParameter('custparam_NewPutMethodID');


		IMarray["custparam_hdntmplocation"] = request.getParameter('custparam_hdntmplocation');
		IMarray["custparam_hdntempitem"] = request.getParameter('custparam_hdntempitem');
		IMarray["custparam_hdntemplp"] = request.getParameter('custparam_hdntemplp');
		IMarray["custparam_searchimbinlocationid"]=request.getParameter('custparam_searchimbinlocationid');
		IMarray["custparam_searchlp"]=request.getParameter('custparam_searchlp');
		IMarray["custparam_number"] = request.getParameter('custparam_number');
		IMarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		IMarray["custparam_language"] = request.getParameter('custparam_language');

		/* Up to here */ 
		//From Packing
		PAarray["custparam_orderno"] = request.getParameter('custparam_orderno');
		PAarray["custparam_recordinternalid"] = request.getParameter('custparam_recordinternalid');
		PAarray["custparam_containerlpno"] = request.getParameter('custparam_containerlpno');
		PAarray["custparam_expectedquantity"] = request.getParameter('custparam_expectedquantity');
		PAarray["custparam_beginLocation"] = request.getParameter('custparam_beginLocation');
		PAarray["custparam_item"] = request.getParameter('custparam_item');
		PAarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		PAarray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');
		PAarray["custparam_dolineid"] = request.getParameter('custparam_dolineid');
		PAarray["custparam_invoicerefno"] = request.getParameter('custparam_invoicerefno');
		PAarray["custparam_endlocinternalid"] = request.getParameter('custparam_endlocinternalid');        
		PAarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
		PAarray["custparam_beginlocationname"] = request.getParameter('custparam_beginlocationname');
		PAarray["custparam_orderlineno"] = request.getParameter('custparam_orderlineno');
		PAarray["custparam_clusterno"] = request.getParameter('custparam_clusterno');        
		PAarray["custparam_batchno"] = request.getParameter('custparam_batchno');
		PAarray["custparam_contlp"] = request.getParameter('custparam_contlp');
		PAarray["custparam_contsize"] = request.getParameter('custparam_contsize');
		PAarray["custparam_linecount"] = request.getParameter('custparam_linecount');
		PAarray["custparam_loopcount"] = request.getParameter('custparam_loopcount');

		PAarray["custparam_number"]= request.getParameter('custparam_number');   
		PAarray["custparam_name"]= request.getParameter('custparam_name');   		
		PAarray["custparam_contlpno"]= request.getParameter('custparam_contlpno');   
		PAarray["custparam_bulkqty"]= request.getParameter('custparam_bulkqty');   
		PAarray['custparam_wmslocation']=request.getParameter('custparam_wmslocation');
		PAarray["custparam_language"] = request.getParameter('custparam_language');
		//case # 20127727
		PAarray["custparam_count"]=request.getParameter('custparam_count');

		// From CycleCount
		CCarray["custparam_planno"] = request.getParameter('custparam_planno');
		CCarray["custparam_fetchedrecordid"] = request.getParameter('custparam_fetchedrecordid');
		CCarray["custparam_begin_location_id"] = request.getParameter('custparam_begin_location_id');
		CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
		CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
		CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
		CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
		CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');
		CCarray["custparam_expitemno"] = request.getParameter('custparam_expitemno');
		CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
		CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
		CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
		CCarray["custparam_expitemId"] = request.getParameter('custparam_expitemId');
		CCarray["custparam_expiteminternalid"] = request.getParameter('custparam_expiteminternalid');
		CCarray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');
		CCarray["custparam_actiteminternalid"] = request.getParameter('custparam_actiteminternalid');
		CCarray["custparam_actbatch"] = request.getParameter('custparam_actbatch');
		CCarray["custparam_mfgdate"] = request.getParameter('custparam_mfgdate');
		CCarray["custparam_expdate"] = request.getParameter('custparam_expdate');
		CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
		CCarray["custparam_noofrecords"] = request.getParameter('custparam_noofrecords');
		CCarray["custparam_nextlocation"] = request.getParameter('custparam_nextlocation');
		CCarray["custparam_bestbeforedate"] = request.getParameter('custparam_bestbeforedate');
		CCarray["custparam_fifodate"]=request.getParameter('custparam_fifodate');
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
		CCarray["custparam_shelflife"] = request.getParameter('custparam_shelflife');
		CCarray["custparam_captureexpirydate"] = request.getParameter('custparam_captureexpirydate');
		CCarray["custparam_capturefifodate"] = request.getParameter('custparam_capturefifodate');
		CCarray["custparam_siteid"]=request.getParameter('custparam_siteid');
		CCarray["custparam_actitemstatus"]=request.getParameter('custparam_actitemstatus');
		CCarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		CCarray["custparam_number"] = request.getParameter('custparam_number');
		CCarray["custparam_actualqty"] = request.getParameter('custparam_actualqty');
		CCarray["custparam_confmsg"]=request.getParameter('custparam_confmsg');
		CCarray["custparam_serialnoID"]=request.getParameter('custparam_serialnoID');
		/* Up to here */ 
		CCarray["custparam_lpcount"] = request.getParameter('custparam_lpcount');
		CCarray["custparam_language"]=request.getParameter('custparam_language');
		//Case # 20126584 Start
		CCarray["custparam_ruleValue"]=request.getParameter('custparam_ruleValue');
		CCarray["custparam_planitem"]=request.getParameter('custparam_planitem');
		//Case # 20126584 End

		// From cart checkin
		CRTarray["custparam_poid"] = request.getParameter('custparam_poid');
		CRTarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		CRTarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		CRTarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		CRTarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		CRTarray["custparam_poqtyentered"] = request.getParameter('custparam_poqtyentered');
		CRTarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		CRTarray["custparam_polinepackcode"] = request.getParameter('custparam_polinepackcode');
		CRTarray["custparam_polinequantity"] = request.getParameter('custparam_polinequantity');
		CRTarray["custparam_polinequantityreceived"] = request.getParameter('custparam_polinequantityreceived');
		CRTarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');
		CRTarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		CRTarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		CRTarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		CRTarray["custparam_actualbegintime"] = request.getParameter('custparam_actualbegintime');
		CRTarray["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');
		CRTarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		CRTarray["custparam_batchno"] = request.getParameter('custparam_batchno');
		CRTarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		CRTarray["custparam_cartno"] = request.getParameter('custparam_cartno');
		CRTarray["custparam_trantype"] = request.getParameter('custparam_trantype');
		CRTarray["custparam_actscanbatchno"] = request.getParameter('custparam_actscanbatchno');
		CRTarray["custparam_captureexpirydate"] = request.getParameter('custparam_captureexpirydate');
		CRTarray["custparam_capturefifodate"] = request.getParameter('custparam_capturefifodate');
		CRTarray["custparam_shelflife"] = request.getParameter('custparam_shelflife');
		//20123677  start
		CRTarray["custparam_mfgdate"] = request.getParameter('custparam_mfgdate');
		CRTarray["custparam_expdate"] = request.getParameter('custparam_expdate');
		CRTarray["custparam_fifocode"] = request.getParameter('custparam_fifocode');
		CRTarray["custparam_fifodate"] = request.getParameter('custparam_fifodate');
		CRTarray["custparam_lastdate"] = request.getParameter('custparam_lastdate');
		CRTarray["custparam_bestbeforedate"] = request.getParameter('custparam_bestbeforedate');
		CRTarray["custparam_language"] = request.getParameter('custparam_language');

		CRTarray["custparam_rmacartcheckin"] = request.getParameter('custparam_rmacartcheckin');

		//case# 20144204.......Added below parameter.
		CRTarray["custparam_number"] = request.getParameter('custparam_number'); 

		//Case# 201410986 starts
		CRTarray["custparam_packcode"] = request.getParameter('custparam_packcode');
		//Case# 201410986 ends
		//end

		//cartputaway 
		CRTPutawayarray["custparam_confirmedLpCount"]= request.getParameter('custparam_confirmedLpCount');
		CRTPutawayarray["custparam_recordcount"]=request.getParameter('custparam_recordcount');
		CRTPutawayarray["custparam_lpno"]=request.getParameter('custparam_lpno');
		CRTPutawayarray["custparam_pono"]=request.getParameter('custparam_pono');
		CRTPutawayarray["custparam_exceptionQuantityflag"]=request.getParameter('custparam_exceptionQuantityflag');
		CRTPutawayarray["custparam_location"]=request.getParameter('custparam_location');
		CRTPutawayarray["custparam_quantity"]=request.getParameter('custparam_quantity');
		CRTPutawayarray["custparam_option"]=request.getParameter('custparam_option');
		CRTPutawayarray["custparam_lineno"]=request.getParameter('custparam_lineno');
		CRTPutawayarray["custparam_item"]=request.getParameter('custparam_item');
		CRTPutawayarray["custparam_itemtext"]=request.getParameter('custparam_itemtext');
		CRTPutawayarray["custparam_screenno"]=request.getParameter('custparam_screenno');
		CRTPutawayarray["custparam_itempalletquantity"]=request.getParameter('custparam_itempalletquantity');
		CRTPutawayarray["custparam_lpCount"]=request.getParameter('custparam_lpCount');
		CRTPutawayarray["custparam_confirmedLpCount"]= request.getParameter('custparam_confirmedLpCount');
		CRTPutawayarray["custparam_cartno"]=request.getParameter('custparam_cartno');
		CRTPutawayarray["custparam_beginlocation"]=request.getParameter('custparam_beginlocation');
		CRTPutawayarray["custparam_itemDescription"]=request.getParameter('custparam_itemDescription');
		CRTPutawayarray["custparam_exceptionquantity"]=request.getParameter('custparam_exceptionquantity');
		CRTPutawayarray["custparam_recordid"]=request.getParameter('custparam_recordid');
		CRTPutawayarray["custparam_lpNumbersArray"]=request.getParameter('custparam_lpNumbersArray');
		CRTPutawayarray["custparam_whlocation"]=request.getParameter('custparam_whlocation');
		CRTPutawayarray["custparam_language"]=request.getParameter('custparam_language');

//		replenishment

		Reparray["custparam_repno"] = request.getParameter('custparam_repno');
		Reparray["custparam_repbegloc"] = request.getParameter('custparam_repbegloc');
		Reparray["custparam_reppickloc"] = request.getParameter('custparam_reppickloc');
		Reparray["custparam_reppicklocno"] = request.getParameter('custparam_reppicklocno');
		Reparray["custparam_repsku"]= request.getParameter('custparam_repsku');
		Reparray["custparam_repskuno"] = request.getParameter('custparam_repskuno');
		Reparray["custparam_torepexpqty"] = request.getParameter('custparam_torepexpqty');
		Reparray["custparam_repid"] = request.getParameter('custparam_repid');
		Reparray["custparam_clustno"] = request.getParameter('custparam_clustno');
		Reparray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		Reparray["custparam_repbeglocId"] = request.getParameter('custparam_repbeglocId');
		Reparray["custparam_nextlocation"]=request.getParameter('custparam_nextlocation');
		Reparray["custparam_nextqty"]=request.getParameter('custparam_nextqty');
		Reparray["custparam_batchno"]=request.getParameter('custparam_batchno');
		Reparray["custparam_fromlpno"]=request.getParameter('custparam_fromlpno');
		Reparray["custparam_tolpno"]=request.getParameter('custparam_tolpno');
		Reparray["custparam_nextitem"]=request.getParameter('custparam_nextitem');
		Reparray["custparam_nextitemno"]=request.getParameter('custparam_nextitemno');
		Reparray["custparam_repexpqty"] = request.getParameter('custparam_repexpqty');
		Reparray["custparam_noofrecords"] = request.getParameter('custparam_noofrecords');
		Reparray["custparam_serialno"]=request.getParameter('custparam_serialno');
		Reparray["custparam_entertaskpriority"] = request.getParameter('custparam_entertaskpriority');
		Reparray["custparam_enterskuid"] = request.getParameter('custparam_enterskuid');
		Reparray["custparam_entersku"] = request.getParameter('custparam_entersku');
		Reparray["custparam_enterpicklocid"] = request.getParameter('custparam_enterpicklocid');
		Reparray["custparam_enterpickloc"] = request.getParameter('custparam_enterpickloc');
		Reparray["custparam_replentype"] = request.getParameter('custparam_replentype');
		Reparray["custparam_enterrepno"] = request.getParameter('custparam_enterrepno');
		Reparray["custparam_number"] = request.getParameter('custparam_number');
		Reparray["custparam_language"] = request.getParameter('custparam_language');
		Reparray["custparam_enterfromloc"] = request.getParameter('custparam_enterfromloc');
		Reparray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		Reparray["custparam_tolocation"] = request.getParameter('custparam_tolocation');
		Reparray["custparam_batchno"] = request.getParameter('custparam_batchno');
		Reparray["custparam_batchnoId"] = request.getParameter('custparam_batchnoId');
		Reparray["custparam_serialnumber"] = request.getParameter('custparam_serialnumber');
		Reparray["custparam_totalqtyscan"] = request.getParameter('custparam_totalqtyscan');
		Reparray["custparam_zoneno"] = request.getParameter('custparam_zoneno');




		//custparam_noofrecords
		//PackArray
		PackArray["custparam_orderno"] = request.getParameter('custparam_orderno');		
		PackArray["custparam_containerno"] = request.getParameter('custparam_containerno');
		PackArray["custparam_containersize"] = request.getParameter('custparam_containersize');
		PackArray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');
		PackArray["custparam_fetchedcontainerlp"]=request.getParameter('custparam_fetchedcontainerlp');
		PackArray["custparam_recordinternalid"]=request.getParameter('custparam_recordinternalid');
		PackArray["custparam_fetchedcontainerlp"]=request.getParameter('custparam_fetchedcontainerlp');    
		PackArray["custparam_expectedquantity"] = request.getParameter('custparam_expectedquantity');
		PackArray["custparam_nextiteminternalid"] = request.getParameter('custparam_nextiteminternalid');
		PackArray["custparam_enteredqty"] =  request.getParameter('custparam_enteredqty');
		PackArray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');				       
		PackArray["custparam_linecount"] = request.getParameter('custparam_linecount');
		PackArray["custparam_loopcount"] = request.getParameter('custparam_loopcount');			
		PackArray['custparam_wmslocation']=request.getParameter('custparam_wmslocation');			
		PackArray['custparam_bulkqty']=request.getParameter('custparam_bulkqty');
		PackArray['custparam_containersize']=request.getParameter('custparam_containersize');
		PackArray["custparam_number"] = request.getParameter('custparam_number');
		PackArray["custparam_nextlocation"] =request.getParameter('custparam_nextlocation');
		PackArray["custparam_nextiteminternalid"] =request.getParameter('custparam_nextiteminternalid');
		PackArray["custparam_waveno"] =request.getParameter('custparam_waveno');
		PackArray["custparam_numbernew"] =request.getParameter('custparam_numbernew');
		//PackArray["hdnFetchedContainerLPNo"] =request.getParameter('hdnFetchedContainerLPNo');
		PackArray["custparam_actualquantity"] =request.getParameter('custparam_actualquantity');
		PackArray["custparam_language"] = request.getParameter('custparam_language');



		nlapiLogExecution('DEBUG', 'custparam_planno', CCarray["custparam_planno"]);
		nlapiLogExecution('DEBUG', 'custparam_fetchedrecordid', CCarray["custparam_fetchedrecordid"]);
		nlapiLogExecution('DEBUG', 'custparam_begin_location_id', CCarray["custparam_begin_location_id"]);
		nlapiLogExecution('DEBUG', 'custparam_begin_location_name', CCarray["custparam_begin_location_name"]);
		nlapiLogExecution('DEBUG', 'custparam_lpno', CCarray["custparam_lpno"]);
		nlapiLogExecution('DEBUG', 'custparam_packcode', CCarray["custparam_packcode"]);
		nlapiLogExecution('DEBUG', 'custparam_expqty', CCarray["custparam_expqty"]);
		nlapiLogExecution('DEBUG', 'custparam_expitem', CCarray["custparam_expitem"]);
		nlapiLogExecution('DEBUG', 'custparam_expitemdescription', CCarray["custparam_expitemdescription"]);
		nlapiLogExecution('DEBUG', 'custparam_expitemno', CCarray["custparam_expitemno"]);
		nlapiLogExecution('DEBUG', 'custparam_actlp', CCarray["custparam_actlp"]);
		nlapiLogExecution('DEBUG', 'custparam_actpackcode', CCarray["custparam_actpackcode"]);
		nlapiLogExecution('DEBUG', 'custparam_actitem', CCarray["custparam_actitem"]);
		nlapiLogExecution('DEBUG', 'custparam_expiteminternalid', CCarray["custparam_expiteminternalid"]);
		nlapiLogExecution('DEBUG', 'custparam_iteminternalid', CCarray["custparam_iteminternalid"]);
		nlapiLogExecution('DEBUG', 'custparam_actiteminternalid', CCarray["custparam_actiteminternalid"]);

		// For Shipping
		ShipArray["custparam_shiplpno"] = request.getParameter('custparam_shiplpno');
		ShipArray["custparam_itemcount"] = request.getParameter('custparam_itemcount');
		nlapiLogExecution('DEBUG', 'ShipArray["custparam_shiplpno"]', ShipArray["custparam_shiplpno"]);
		ShipArray["custparam_GWgt"] = request.getParameter('custparam_GWgt');
		ShipArray["custparam_GCube"] = request.getParameter('custparam_GCube');
		ShipArray["custparam_trlrno"] = request.getParameter('custparam_trlrno');
		ShipArray["custparam_locationId"] = request.getParameter('custparam_locationId');
		ShipArray["custparam_site_id"] = request.getParameter('custparam_site_id');
		ShipArray["custparam_orderintid"] = request.getParameter('custparam_orderintid');
		ShipArray["custparam_waveno"] = request.getParameter('custparam_waveno');
		ShipArray["custparam_OrderNo"] = request.getParameter('custparam_OrderNo');
		ShipArray["custparam_foname"] = request.getParameter('custparam_foname');

		nlapiLogExecution('DEBUG', 'LP', IMarray["custparam_lp"]);
		nlapiLogExecution('DEBUG', 'LP', IMarray["custparam_lp"]);


		UCCarray["custparam_uccitem"] = request.getParameter('custparam_uccitem');
		UCCarray["custparam_uccebizitem"] = request.getParameter('custparam_uccebizitem');
		UCCarray["custparam_uccpackcode"] = request.getParameter('custparam_uccpackcode');
		UCCarray["custparam_uccactqty"] = request.getParameter('custparam_uccactqty');
		UCCarray["custparam_uccordno"] = request.getParameter('custparam_uccordno');
		UCCarray["custparam_uccebizordno"] = request.getParameter('custparam_uccebizordno');
		UCCarray["custparam_uccordlineno"] = request.getParameter('custparam_uccordlineno');
		UCCarray["custparam_uccwmsstatus"] = request.getParameter('custparam_uccwmsstatus');
		UCCarray["custparam_uccshiplpno"] = request.getParameter('custparam_uccshiplpno');
		UCCarray["custparam_ucclocation"] = request.getParameter('custparam_ucclocation');
		UCCarray["custparam_ucccompany"] = request.getParameter('custparam_ucccompany');
		UCCarray["custparam_uccnsrefno"] = request.getParameter('custparam_uccnsrefno');
		UCCarray["custparam_uccinvrefno"] = request.getParameter('custparam_uccinvrefno');
		UCCarray["custparam_uccfointrid"] = request.getParameter('custparam_uccfointrid');
		UCCarray["custparam_ucctaskintrid"] = request.getParameter('custparam_ucctaskintrid');
		UCCarray["custparam_uccname"] = request.getParameter('custparam_uccname');
		UCCarray["custparam_ucctaskweight"] = request.getParameter('custparam_ucctaskweight');
		UCCarray["custparam_ucctaskcube"] = request.getParameter('custparam_ucctaskcube');
		UCCarray["custparam_uccuomlevel"] = request.getParameter('custparam_uccuomlevel');  
		UCCarray["custparam_ucclprecid"] = request.getParameter('custparam_ucclprecid');
		UCCarray["custparam_uccendlocation"] = request.getParameter('custparam_uccendlocation');
		UCCarray["custparam_uccreversalby"] = request.getParameter('custparam_uccreversalby');
		UCCarray["custparam_ucccontlpno"] = request.getParameter('custparam_ucccontlpno');
		UCCarray["custparam_recnumber"] = request.getParameter('custparam_recnumber');
		//Case #20126184�start
		UCCarray["custparam_uccreversaltype"]=request.getParameter('custparam_uccreversaltype');
		//Case # 20126184� End
		UPCArray["custparam_planno"]=request.getParameter('custparam_planno');
		UPCArray["custparam_sku"]=request.getParameter('custparam_sku');
		UPCArray["custparam_skuno"]=request.getParameter('custparam_skuno');
		UPCArray["custparam_tasktype"]=request.getParameter('custparam_tasktype');
		UPCArray["custparam_location"]=request.getParameter('custparam_location');		
		UPCArray["custparam_actbeginlocationid"] = request.getParameter('custparam_actbeginlocationid');
		UPCArray["custparam_actbeginlocationname"] = request.getParameter('custparam_actbeginlocationname');
		UPCArray["custparam_nextlocation"]=request.getParameter('custparam_nextlocation');


		//lparray["custparam_picklp"]=request.getParameter('custparam_picklp');
		lparray["custparam_language"] = request.getParameter('custparam_language');
		lparray["custparam_serialscanby"] = request.getParameter('custparam_serialscanby');
		lparray["custparam_site"]=request.getParameter('custparam_site');
		lparray["custparam_containerlp"]=request.getParameter('custparam_containerlp');
		lparray["custparam_ucc"]=request.getParameter('custparam_ucc');
		lparray["custparam_tracking"]=request.getParameter('custparam_tracking');
		lparray["custparam_totalqty"] =request.getParameter('custparam_totalqty');
		lparray["custparam_order"]=request.getParameter('custparam_order');
		lparray["custparam_customer"] =request.getParameter('custparam_customer');
		lparray["custparam_itemid"] = request.getParameter('custparam_itemid');
		lparray["custparam_itemtext"] = request.getParameter('custparam_itemtext');
		lparray["custparam_getnumbercount"] =request.getParameter('custparam_getnumbercount');
		lparray["custparam_allitemid"]=request.getParameter('custparam_allitemid');
		lparray["custparam_alreadyscanneditem"]=request.getParameter('custparam_alreadyscanneditem');
		lparray["custparam_itemcount"]=request.getParameter('custparam_itemcount');

		//	if the previous button 'F8' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Check-In PO screen.
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
		nlapiLogExecution('DEBUG', 'getScreenNo', getScreenNo);
		if (optedEvent == 'F8' && getScreenNo == 'MainMenu') {
			// MainMenu
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, POarray);
		}
		else
			if (optedEvent == 'F8' && getScreenNo == 'CheckinMenu') {
				// Checkin Criteria Menu
				response.sendRedirect('SUITELET', 'customscript_rf_checkin_criteria_menu', 'customdeploy_rf_checkin_criteria_menu_di', false, POarray);
			}
			else
				if (optedEvent == 'F8' && getScreenNo == 'Packingmenu') {
					// Packing Criteria Menu
					response.sendRedirect('SUITELET', 'customscript_rf_packing_menu', 'customdeploy_rf_packing_menu_di', false, POarray);
				}
				else
					if (optedEvent == 'F8' && getScreenNo == 'PickMenu') {
						// picking Menu
						response.sendRedirect('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di', false, SOarray);
					}
					else
						if (optedEvent == 'F8' && getScreenNo == '1') {
							// Check-In PO #
							response.sendRedirect('SUITELET', 'customscript_rf_checkin_po', 'customdeploy_rf_checkin_po_di', false, POarray);
						}
						else 
							if (optedEvent == 'F8' && getScreenNo == '1R') {
								// RMA Check-In 
								response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin', 'customdeploy_rf_rma_checkin_di', false, POarray);
							}
							else 
								if (optedEvent == 'F8' && getScreenNo == '1T') {
									// TO Check-In 
									response.sendRedirect('SUITELET', 'customscript_rf_to_checkin', 'customdeploy_rf_to_checkin_di', false, POarray);
								}
								else 
									if (optedEvent == 'F8' && getScreenNo == '2') {
										// Check-In Item
										response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false, POarray);
									}
									else 
										if (optedEvent == 'F8' && getScreenNo == 'cntry') {
											// Check-In Country
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_countryoforigin', 'customdeploy_ebiz_rf_countryoforigin_di', false, POarray);
										}

										else 
											if (optedEvent == 'F8' && getScreenNo == '2R') {
												// RMA Check-In Item
												response.sendRedirect('SUITELET', 'customscript_rf_rmacheckin_sku', 'customdeploy_rf_rmacheckin_sku_di', false, POarray);
											}
											else 
												if (optedEvent == 'F8' && getScreenNo == '2T') {
													// TO Check-In Item
													// case No start 20126962
													// it should redirect to batch entry
													//Case # 20127182�Start
													response.sendRedirect('SUITELET', 'customscript_rf_tocheckin_sku', 'customdeploy_rf_tocheckin_sku_di', false, POarray);
													//response.sendRedirect('SUITELET', 'customscript_rf_checkin_batch_no', 'customdeploy_rf_checkin_batch_no_di', false, POarray);
													//Case # 20127182�End
													// case No end 20126962
												}
												else 
													if (optedEvent == 'F8' && getScreenNo == '3') {
														// Check-In Quantity
														response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false, POarray);
													}


													else 
														if (optedEvent == 'F8' && getScreenNo == '3SKU') {
															// Check-In Item
															response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false, POarrayget);
														}

														else 
															if (optedEvent == 'F8' && getScreenNo == '3R') {
																// RMA Check-In Quantity
																response.sendRedirect('SUITELET', 'customscript_rf_rma_checkinqty', 'customdeploy_rf_rmacheckin_qty_di', false, POarray);
															}

															else 
																if (optedEvent == 'F8' && getScreenNo == '3RSKU') {
																	// RMA Check-In Item
																	response.sendRedirect('SUITELET', 'customscript_rf_rmacheckin_sku', 'customdeploy_rf_rmacheckin_sku_di', false, POarrayget);
																}

																else 
																	if (optedEvent == 'F8' && getScreenNo == '3T') {
																		// TO Check-In Quantity
																		response.sendRedirect('SUITELET', 'customscript_rf_to_checkinqty', 'customdeploy_rf_tocheckin_qty_di', false, POarray);
																	}

																	else 
																		if (optedEvent == 'F8' && getScreenNo == '3TSKU') {
																			// TO Check-In Item
																			response.sendRedirect('SUITELET', 'customscript_rf_tocheckin_sku', 'customdeploy_rf_tocheckin_sku_di', false, POarrayget);
																		}
																		else
																			if (optedEvent == 'F8' && getScreenNo == '3A') {
																				// Check-In Pickface Exception
																				response.sendRedirect('SUITELET', 'customscript_rf_pickface_exception', 'customdeploy_rf_pickface_exception_di', false, POarray);
																			}
																			else
																				if (optedEvent == 'F8' && getScreenNo == '3B') {
																					// Check-In ExpDate Exception
																					response.sendRedirect('SUITELET', 'customscript_rf_checkin_exp_date', 'customdeploy_rf_checkin_exp_date_di', false, POarray);
																				}
																				else
																					if (optedEvent == 'F8' && getScreenNo == '3C') {
																						// Check-In FIFODate Exception
																						response.sendRedirect('SUITELET', 'customscript_rf_checkin_fifo_date', 'customdeploy_rf_checkin_fifo_date_di', false, POarray);
																					}
																					else 
																						if (optedEvent == 'F8' && getScreenNo == '4') {
																							// Check-In Item Status
																							response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
																						}
																						else 
																							if (optedEvent == 'F8' && getScreenNo == '4R') {
																								// RMA Check-In Item Status
																								response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin_item_status', 'customdeploy_rf_rma_checkin_item_status', false, POarray);
																							}
																							else 
																								if (optedEvent == 'F8' && getScreenNo == '4T') {
																									// TO Check-In Item Status
																									response.sendRedirect('SUITELET', 'customscript_rf_to_checkin_item_status', 'customdeploy_rf_to_checkin_item_status_d', false, POarray);
																								}
																								else 
																									if (optedEvent == 'F8' && getScreenNo == '5') {
																										// Check-In LP #
																										response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
																									}
																									else 
																										if (optedEvent == 'F8' && getScreenNo == '5a') {
																											// Check-In Serial Entry Screen #
																											response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no_di', false, POarray);
																										}
																										else 
																											if (optedEvent == 'F8' && getScreenNo == '6') {
																												// Putaway LP #
																												response.sendRedirect('SUITELET', 'customscript_rf_putaway_lp', 'customdeploy_rf_putaway_lp_di', false, POarray);
																											}
																											else 
																												if (optedEvent == 'F8' && getScreenNo == '8') {
																													// Putaway Location
																													response.sendRedirect('SUITELET', 'customscript_rf_putaway_location', 'customdeploy_rf_putaway_location_di', false, POarray);
																												}
																												else 
																													if (optedEvent == 'F8' && getScreenNo == '9') {
																														// Putaway Location Exception
																														response.sendRedirect('SUITELET', 'customscript_rf_putaway_loc_exception', 'customdeploy_rf_putaway_loc_exception_di', false, POarray);
																													}
																													else 
																														if (optedEvent == 'F8' && getScreenNo == '10') {
																															// Putaway Location Exception
																															response.sendRedirect('SUITELET', 'customscript_rf_putaway_qty_exception', 'customdeploy_rf_putaway_qty_exception_di', false, POarray);
																														}
																														else 
																															if (optedEvent == 'F8' && getScreenNo == 'EXBINSIZE') {
																																// Putaway Location Exception
																																response.sendRedirect('SUITELET', 'customscript_rf_expectedbinsize', 'customdeploy_rf_expectedbinsize', false, POarray);
																															}																																																																																																																						
																															else 
																																if (optedEvent == 'F8' && getScreenNo == 'putqtyexpserial') {
																																	// Putaway Location Exception
																																	response.sendRedirect('SUITELET', 'customscript_putawayqtyexp_serial_no', 'customdeploy_putawayqtyexp_serial_no', false, POarray);
																																}

																																else 
																																	if (optedEvent == 'F8' && getScreenNo == '11') {
																																		// Putaway Location Exception
																																		response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirm_loc', 'customdeploy_rf_putaway_confirm_loc_di', false, POarray);
																																	}



																																	else 
																																		if (optedEvent == 'F8' && getScreenNo == '11A') {
																																			// pick qty Exception
																																			response.sendRedirect('SUITELET', 'customscript_rf_pickingqtyexception', 'customdeploy_rf_pickingqtyexception_di', false, pickQtyExceptionarray);
																																		}
		//case # 20124383� Start       
																																		else 
																																			if (optedEvent == 'F8' && getScreenNo == 'InventoryMenu') {

																																				response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, POarray);
																																			}        
		//case # 20124383� Start                                                                                                  //case # 20124383� End

																																			else 
																																				if (optedEvent == 'F8' && getScreenNo == '11EXP') {
																																					// Putaway Location Exception
																																					response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
																																				}
																																				else 
																																					if (optedEvent == 'F8' && getScreenNo == 'REPLENEXP') {
																																						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
																																					}
																																					else 
																																						if (optedEvent == 'F8' && getScreenNo == '12EXP') {
																																							// Putaway Location Exception
																																							response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
																																						}
																																						else 
																																							if (optedEvent == 'F8' && getScreenNo == 'PickLocEXP') {
																																								// Putaway Location Exception
																																								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_loc_exception', 'customdeploy_ebiz_rf_pick_loc_exc_di', false, SOarray);
																																							}
																																							else 
																																								if (optedEvent == 'F8' && getScreenNo == '12') {
																																									// Picking Container
																																									response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
																																								}
																																								else 
																																									if (optedEvent == 'F8' && getScreenNo == '13') {
																																										// Picking Container Number
																																										response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
																																									}
																																									else 
																																										if (optedEvent == 'F8' && getScreenNo == '13NEW') {
																																											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
																																										}		
																																										else 
																																											if (optedEvent == 'F8' && getScreenNo == '14') {
																																												nlapiLogExecution('DEBUG', 'Begin Location', SOarray["custparam_beginlocationname"]);
																																												// Picking Container Number
																																												response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
																																											}
																																											else 
																																												if (optedEvent == 'F8' && getScreenNo == '14PickingBatch') {
																																													nlapiLogExecution('DEBUG', 'Begin Location', SOarray["custparam_beginlocationname"]);
																																													// Picking Container Number
																																													response.sendRedirect('SUITELET', 'customscript_rf_picking_batch', 'customdeploy_rf_picking_batch_di', false, SOarray);
																																												}
																																												else 
																																													if (optedEvent == 'F8' && getScreenNo == '14A') {

																																														// Picking Container Number
																																														response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);
																																													}
																																													else 
																																														if (optedEvent == 'F8' && getScreenNo == 'pickingLocItem') {

																																															// Picking Container Number
																																															response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
																																														}
																																														else 
																																															if (optedEvent == 'F8' && getScreenNo == 'stagelocation') {

																																																// Picking stage Location.
																																																response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
																																															}
																																															else 
																																																if (optedEvent == 'F8' && getScreenNo == '15LOC') {
																																																	// Create Inventory - Location
																																																	response.sendRedirect('SUITELET', 'customscript_rf_location_company', 'customdeploy_rf_location_company_di', false, CIarray);
																																																}
																																																else 
																																																	if (optedEvent == 'F8' && getScreenNo == '15') {
																																																		// Create Inventory - Bin Location
																																																		response.sendRedirect('SUITELET', 'customscript_rf_create_invt_binloc', 'customdeploy_rf_create_invt_binloc_di', false, CIarray);
																																																	}
																																																	else 
																																																		if (optedEvent == 'F8' && getScreenNo == '15A') {
																																																			// Create Inventory - Bin Location
																																																			response.sendRedirect('SUITELET', 'customscript_rf_createinventory_batchno', 'customdeploy_rf_createinventory_batchno', false, CIarray);
																																																		}
																																																		else 
																																																			if (optedEvent == 'F8' && getScreenNo == '15B') {
																																																				// Create Inventory - Bin Location
																																																				response.sendRedirect('SUITELET', 'customscript_rf_create_invt_expdate', 'customdeploy_rf_create_invt_expdate_di', false, CIarray);
																																																			}
																																																			else 
																																																				if (optedEvent == 'F8' && getScreenNo == '15C') {
																																																					// Create Inventory - Bin Location
																																																					response.sendRedirect('SUITELET', 'customscript_rf_create_invt_fifodate', 'customdeploy_rf_create_invt_fifodate_di', false, CIarray);
																																																				}
																																																				else 
																																																					if (optedEvent == 'F8' && getScreenNo == '16') {
																																																						// Create Inventory - Item Status
																																																						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_itemstat', 'customdeploy_rf_create_invt_itemstat_di', false, CIarray);
																																																					}
																																																					else 
																																																						if (optedEvent == 'F8' && getScreenNo == '17') {
																																																							// Create Inventory - LP
																																																							response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
																																																						}

		//added by shylaja for overwrite,merge lp functionality in RF
																																																						else 
																																																							if (optedEvent == 'F8' && getScreenNo == '17A') {
																																																								// Create Inventory - OverWritelLP
																																																								response.sendRedirect('SUITELET', 'customscript_rf_overwrite_lp', 'customdeploy_rf_overwrite_lp_di', false, CIarray);
																																																							}
																																																							else 
																																																								if (optedEvent == 'F8' && getScreenNo == '17B') {
																																																									// Create Inventory - Inventory Merge LP
																																																									response.sendRedirect('SUITELET', 'customscript_rf_inventory_merge_lp', 'customdeploy_rf_inventory_merge_lp_di', false, CIarray);
																																																								}
																																																								else 
																																																									if (optedEvent == 'F8' && getScreenNo == '18') {
																																																										// Inventory Move - LP
																																																										response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_main', 'customdeploy_rf_inventory_move_main_di', false, IMarray);
																																																									}
																																																									else 
																																																										if (optedEvent == 'F8' && getScreenNo == '19') {
																																																											// Inventory Move - Continue
																																																											response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_cont', 'customdeploy_rf_inventory_move_cont_di', false, IMarray);
																																																										}
																																																										else 
																																																											if (optedEvent == 'F8' && getScreenNo == '20') {
																																																												// Inventory Move - Batch
																																																												response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_batchconf', 'customdeploy_rf_inventory_move_batch_di', false, IMarray);
																																																											}
																																																											else 
																																																												if (optedEvent == 'F8' && getScreenNo == '21') {
																																																													nlapiLogExecution('DEBUG', 'Move Qty', 'move qty');
																																																													// Inventory Move - Move Quantity
																																																													response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_qty', 'customdeploy_rf_inventory_move_qty_di', false, IMarray);
																																																												}
																																																												else 
																																																													if (optedEvent == 'F8' && getScreenNo == '23') {
																																																														// Inventory Move - New LP
																																																														response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_lp', 'customdeploy_rf_inventory_move_lp_di', false, IMarray);
																																																													}
																																																													else 
																																																														if (optedEvent == 'F8' && getScreenNo == '22') {
																																																															// Inventory Move - New Location
																																																															response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_endloc', 'customdeploy_rf_inventory_move_endloc_di', false, IMarray);
																																																														}
																																																														else 
																																																															if (optedEvent == 'F8' && getScreenNo == '22STATUS') {
																																																																// Inventory Move - New Location
																																																																response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_status', 'customdeploy_rf_inventory_move_status_di', false, IMarray);
																																																															}
																																																															else 
																																																																if (optedEvent == 'F8' && getScreenNo == '22A') {
																																																																	// Inventory Move - New Location
																																																																	response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_lp', 'customdeploy_rf_inventory_move_lp_di', false, IMarray);
																																																																	//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_endloc', 'customdeploy_rf_inventory_move_endloc_di', false, IMarray);
																																																																}
																																																																else 
																																																																	if (optedEvent == 'F8' && getScreenNo == '22AB') {
																																																																		// Inventory Move - Serial Scan
																																																																		response.sendRedirect('SUITELET', 'customscript_rf_inventory_serialscan', 'customdeploy_rf_inventory_serialscan_di', false, IMarray);																																																						
																																																																	}
																																																																	else 
																																																																		if (optedEvent == 'F8' && getScreenNo == '24') {
																																																																			// Packing - Order
																																																																			response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, PAarray);
																																																																		}
																																																																		else 
																																																																			if (optedEvent == 'F8' && getScreenNo == '24MERGE') {
																																																																				// Packing - Order
																																																																				response.sendRedirect('SUITELET', 'customscript_rf_mergepacking_orderno', 'customdeploy_rf_mergepacking_orderno_di', false, SOarray);
																																																																			}
																																																																			else
																																																																				if (optedEvent == 'F8' && getScreenNo == '25') {
																																																																					// Packing - LP
																																																																					response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, PAarray);
																																																																				}
																																																																				else
																																																																					if (optedEvent == 'F8' && getScreenNo == '26') {
																																																																						// Packing - Item
																																																																						response.sendRedirect('SUITELET', 'customscript_rf_packing_item', 'customdeploy_rf_packing_item_di', false, PAarray);
																																																																					}
																																																																					else
																																																																						if (optedEvent == 'F8' && getScreenNo == '27A') {
																																																																							// Packing - Item
																																																																							response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, PAarray);
																																																																						}
																																																																						else
																																																																							if (optedEvent == 'F8' && getScreenNo == '27') {
																																																																								// Packing - Quantity
																																																																								response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, PAarray);
																																																																							}
																																																																							else//from here added by shylaja.ch for cyclecount
																																																																								if (optedEvent == 'F8' && getScreenNo == '28') {
																																																																									// Cycle Count Plan No
																																																																									response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di', false, CCarray);
																																																																								}
																																																																								else
																																																																									if (optedEvent == 'F8' && getScreenNo == '29') {
																																																																										// Cycle Count Location
																																																																										response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
																																																																									}
																																																																									else
																																																																										if (optedEvent == 'F8' && getScreenNo == '29A') {
																																																																											// Cycle Count Location
																																																																											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cyc_addnewitem', 'customdeploy_ebiz_rf_cyc_addnewitem_di', false, CCarray);

																																																																										}
																																																																										else
																																																																											if (optedEvent == 'F8' && getScreenNo == '30') {
																																																																												// Cycle Count LP
																																																																												response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_lp', 'customdeploy_rf_cyclecount_lp_di', false, CCarray);
																																																																											}
																																																																											else
																																																																												if (optedEvent == 'F8' && getScreenNo == '31') {
																																																																													// Cycle Count SKU
																																																																													response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_sku', 'customdeploy_rf_cyclecount_sku_di', false, CCarray);
																																																																												}
																																																																												else
																																																																													if (optedEvent == 'F8' && getScreenNo == '32') {
																																																																														// Cycle Count Pack Code
																																																																														response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_packcode', 'customdeploy_rf_cyclecount_packcode_di', false, CCarray);
																																																																													}
																																																																													else
																																																																														if (optedEvent == 'F8' && getScreenNo == '33') {
																																																																															// Cycle Count Pack Code
																																																																															response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
																																																																														}
																																																																														else
																																																																															if (optedEvent == 'F8' && getScreenNo == '33B') {
																																																																																// Cycle Count Serial Scan
																																																																																response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_serialscan', 'customdeploy_rf_cyclecount_serialscan_di', false, CCarray);
																																																																															}
																																																																															else
																																																																																if (optedEvent == 'F8' && getScreenNo == '35') {
																																																																																	// Cycle Count Batch
																																																																																	response.sendRedirect('SUITELET', 'customscript_rf_cyclecountbatch', 'customdeploy_rf_cyclecountbatch_di', false, CCarray);
																																																																																}
																																																																																else
																																																																																	if (optedEvent == 'F8' && getScreenNo == '35A') {
																																																																																		// Cycle Count Expected date
																																																																																		response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_exp_date', 'customdeploy_rf_cyclecount_exp_date_di', false, CCarray);
																																																																																	}
																																																																																	else
																																																																																		if (optedEvent == 'F8' && getScreenNo == '35B') {
																																																																																			// Cycle Count FIFO Date
																																																																																			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_fifo_date', 'customdeploy_rf_cyclecount_fifo_date_di', false, CCarray);
																																																																																		}
																																																																																		else
																																																																																			if (optedEvent == 'F8' && getScreenNo == '33C') {
																																																																																				/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/// Cycle Count Item Status
																																																																																				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_itemstatus', 'customdeploy_rf_cyclecount_itemstatus_di', false, CCarray);
																																																																																			}
																																																																																			else/* Up to here */ 
																																																																																				if (optedEvent == 'F8' && getScreenNo == '36') {

																																																																																					response.sendRedirect('SUITELET', 'customscript_rf_loadtrlr', 'customdeploy_rf_loadtrlr_di', false, ShipArray);
																																																																																				}
																																																																																				else
																																																																																					if (optedEvent == 'F8' && getScreenNo == '36A') {

																																																																																						response.sendRedirect('SUITELET', 'customscript_rf_loadunloadtrlr', 'customdeploy_rf_loadunloadtrlr_di', false, ShipArray);
																																																																																					}
																																																																																					else
																																																																																						if (optedEvent == 'F8' && getScreenNo == '37') {

																																																																																							response.sendRedirect('SUITELET', 'customscript_rf_scanlp', 'customdeploy_rf_scanlp_di', false, ShipArray);
																																																																																						}
																																																																																						else
																																																																																							if (optedEvent == 'F8' && getScreenNo == '38') {
																																																																																								// For RF Shipping Ship LP Screen
																																																																																								response.sendRedirect('SUITELET', 'customscript_rf_shipping_shiplp', 'customdeploy_rf_shipping_shiplp_di', false, ShipArray);
																																																																																							}																																						 
																																																																																							else
																																																																																								if (optedEvent == 'F8' && getScreenNo == '39') {
																																																																																									// For Atomatic Ship lp Confirmation screen
																																																																																									response.sendRedirect('SUITELET', 'customscript_rf_shipping_cartonlp', 'customdeploy_rf_shipping_cartonlp_di', false, ShipArray);
																																																																																								}
																																																																																								else

																																																																																									if (optedEvent == 'F8' && getScreenNo == 'RMA1') {
																																																																																										// cartCheck-In PO #
																																																																																										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_rma', 'customdeploy_ebiz_rf_cartcheckin_rma_di', false, POarray);
																																																																																									}
																																																																																									else
																																																																																										if (optedEvent == 'F8' && getScreenNo == 'CRT2') { //case # 20148136
																																																																																											// cartscan #
																																																																																											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartscan', 'customdeploy_ebiz_rf_cartscan_di', false, CRTarray);
																																																																																										}
																																																																																										else	if (optedEvent == 'F8' && getScreenNo == '40') {
																																																																																											// For Atomatic Ship lp Confirmation screen
																																																																																											response.sendRedirect('SUITELET', 'customscript_rf_shipping_sizeid', 'customdeploy_rf_shipping_sizeid_di', false, ShipArray);

																																																																																										}
																																																																																										else
																																																																																											if (optedEvent == 'F8' && getScreenNo == '41') {
																																																																																												// For Serial Number Scanning screen
																																																																																												// Case # 20140003 starts
																																																																																												response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);// Case# 20145821 
																																																																																												//response.sendRedirect('SUITELET', '	customscript_rf_bulkpick_serialscan', 'customdeploy_rf_bulkpick_serialscan_di', false, SOarray);// Case# 20145821 
																																																																																												// Case # 20140003 ends
																																																																																											}
		// Case# 20145821 starts
																																																																																											else
																																																																																												if (optedEvent == 'F8' && getScreenNo == '41BULK') {
																																																																																													nlapiLogExecution('DEBUG', '41BULK into if ', '41BULK');
																																																																																													response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_serialscan', 'customdeploy_rf_bulkpick_serialscan_di', false, SOarray); 
																																																																																												}
		//  case# 20145821 ends
																																																																																												else
																																																																																													if (optedEvent == 'F8' && getScreenNo == '41FAST') {

																																																																																														response.sendRedirect('SUITELET', 'customscript_rf_fastpick_serialscan', 'customdeploy_rf_fastpick_serialscan_di', false, SOarray);
																																																																																													} 
																																																																																													else
																																																																																														if (optedEvent == 'F8' && getScreenNo == '42') {
																																																																																															// For depart
																																																																																															response.sendRedirect('SUITELET', 'customscript_rf_departtrlrconfirmation', 'customdeploy_rf_deprttrlrconfirmation_di', false, ShipArray);
																																																																																														}
		/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
																																																																																														else
																																																																																															if (optedEvent == 'F8' && getScreenNo == 'CartCheckinMenu') {
																																																																																																// Checkin Criteria Menu
																																																																																																response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, POarray);
																																																																																															} /*** up to here ***/
																																																																																															else
																																																																																																if (optedEvent == 'F8' && getScreenNo == 'CRT1') {
																																																																																																	// cartCheck-In PO #
																																																																																																	response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_po', 'customdeploy_ebiz_rf_cartcheckin_po_di', false, CRTarray);
																																																																																																}
																																																																																																else
																																																																																																	if (optedEvent == 'F8' && getScreenNo == 'TR1') {
																																																																																																		// cartCheck-In PO #
																																																																																																		response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_to', 'customdeploy_ebiz_rf_cartcheckin_to_di', false, POarray);
																																																																																																	}
																																																																																																	else
																																																																																																		if (optedEvent == 'F8' && getScreenNo == 'CRT2') {
																																																																																																			// cartscan #
																																																																																																			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartscan', 'customdeploy_ebiz_rf_cartscan_di', false, CRTarray);
																																																																																																		}
																																																																																																		else
																																																																																																			if (optedEvent == 'F8' && getScreenNo == 'CRT3') {
																																																																																																				// cartskuscan
																																																																																																				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinsku', 'customdeploy_ebiz_rf_cart_checkinsku_di', false, CRTarray);
																																																																																																			}
																																																																																																			else
																																																																																																				if (optedEvent == 'F8' && getScreenNo == 'CRT4') {
																																																																																																					// cartqtyscan
																																																																																																					// Case# 20148128 starts
																																																																																																					//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinqty', 'customdeploy_ebiz_rf_cart_checkinqty_di', false, CRTarray);
																																																																																																					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinqty', 'customdeploy_ebiz_rf_cart_checkinqty_di', false, POarray);
																																																																																																					// Case# 20148128 ends
																																																																																																				}


																																																																																																				else
																																																																																																					if (optedEvent == 'F8' && getScreenNo == 'CRT4A') {
																																																																																																						// cartqtyscan
																																																																																																						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinsku', 'customdeploy_ebiz_rf_cart_checkinsku_di', false, POarrayget);
																																																																																																					}



																																																																																																					else
																																																																																																						if (optedEvent == 'F8' && getScreenNo == 'CRT5') {
																																																																																																							// cartqtyscan
																																																																																																							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinitemsts', 'customdeploy_ebiz_rf_cart_checkinitemsts', false, CRTarray);
																																																																																																						}
																																																																																																						else
																																																																																																							if (optedEvent == 'F8' && getScreenNo == 'CRT5P') {
																																																																																																								// cartqtyscan
																																																																																																								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, CRTarray);
																																																																																																							}
																																																																																																							else
																																																																																																								if (optedEvent == 'F8' && getScreenNo == 'CRT6') {
																																																																																																									// cartqtyscan
																																																																																																									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_chkbatchno', 'customdeploy_ebiz_rf_cart_chkbatchno_di', false, CRTarray);
																																																																																																								}
																																																																																																								else
																																																																																																									if (optedEvent == 'F8' && getScreenNo == 'CRT7') {
																																																																																																										// cartqtyscan
																																																																																																										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_chkexpirydate', 'customdeploy_ebiz_rf_cart_chkexpirydate', false, CRTarray);
																																																																																																									}
																																																																																																									else
																																																																																																										if (optedEvent == 'F8' && getScreenNo == 'CRT8') {
																																																																																																											// cartqtyscan
																																																																																																											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_chkinfifodate', 'customdeploy_ebiz_rf_cart_chkinfifodate', false, CRTarray);
																																																																																																										}
																																																																																																										else 
																																																																																																											if (optedEvent == 'F8' && getScreenNo == 'CRT9') {
																																																																																																												// cartqtyscan
																																																																																																												response.sendRedirect('SUITELET', 'customscript_rf_putaway_cartlp', 'customdeploy_rf_putaway_cartlp_di', false, CRTPutawayarray);
																																																																																																											}
																																																																																																											else 
																																																																																																												if (optedEvent == 'F8' && getScreenNo == 'CRT10') {
																																																																																																													// cartqtyscan
																																																																																																													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaysku', 'customdeploy_ebiz_rf_cart_putawaysku_di', false, CRTPutawayarray);
																																																																																																												}
																																																																																																												else 
																																																																																																													if (optedEvent == 'F8' && getScreenNo == 'CRT11') {
																																																																																																														// cartqtyscan
																																																																																																														response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayloc', 'customdeploy_ebiz_rf_cart_putawayloc_di', false, CRTPutawayarray);
																																																																																																													}
																																																																																																													else 
																																																																																																														if (optedEvent == 'F8' && getScreenNo == 'CRT12') {
																																																																																																															// cartqtyscan
																																																																																																															response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaylocexcp', 'customdeploy_ebiz_rf_cart_putawaylocexcp', false, CRTPutawayarray);
																																																																																																														}
																																																																																																														else 
																																																																																																															if (optedEvent == 'F8' && getScreenNo == 'CRT13') {
																																																																																																																// cartqtyscan
																																																																																																																response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayqtyexp', 'customdeploy_ebiz_rf_cart_putawayqtyexp', false, CRTPutawayarray);
																																																																																																															}
																																																																																																															else 
																																																																																																																if (optedEvent == 'F8' && getScreenNo == 'CRT14') {
																																																																																																																	// cartqtyscan
																																																																																																																	response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_pickface_exp', 'customdeploy_ebiz_rf_cart_pickface_exp', false, CRTPutawayarray);
																																																																																																																}
																																																																																																																else 
																																																																																																																	if (optedEvent == 'F8' && getScreenNo == 'CRT16') {
																																																																																																																		// cartqtyscan
																																																																																																																		response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, optedEvent);
																																																																																																																	}
																																																																																																																	else 
																																																																																																																		if (optedEvent == 'F8' && getScreenNo == 'CRT17') {
																																																																																																																			// cartqtyscan
																																																																																																																			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);
																																																																																																																		}
		//case# 20148345 starts 																																																																																																																	
																																																																																																																		else 
																																																																																																																			if (optedEvent == 'F8' && getScreenNo == 'CRTSYSLP') {
																																																																																																																				// cartqtyscan
																																																																																																																				//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartlpconfirm', 'customdeploy_ebiz_rf_cartlpconfirm_di', false, CRTPutawayarray);
																																																																																																																				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartscan', 'customdeploy_ebiz_rf_cartscan_di', false, CRTarray);
																																																																																																																			}
		//case# 20148345 ends																										

																																																																																																																			else
																																																																																																																				if (optedEvent == 'F8' && getScreenNo == 'R1') {
																																																																																																																					// For Serial Number Scanning screen
																																																																																																																					response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false, Reparray);
																																																																																																																				}



																																																																																																																				else
																																																																																																																					if (optedEvent == 'F8' && getScreenNo == 'RPLNMENU') {
																																																																																																																						// For Serial Number Scanning screen
																																																																																																																						response.sendRedirect('SUITELET', 'customscript_rf_replenishmentmenu', 'customdeploy_rf_replenishmentmenu', false, Reparray);
																																																																																																																					}
																																																																																																																					else
																																																																																																																						if (optedEvent == 'F8' && getScreenNo == 'R2') {
																																																																																																																							response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);

																																																																																																																						}
																																																																																																																						else
																																																																																																																							if (optedEvent == 'F8' && getScreenNo == 'R3') {
																																																																																																																								response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);

																																																																																																																							}
																																																																																																																							else
																																																																																																																								if (optedEvent == 'F8' && getScreenNo == 'R4') {
																																																																																																																									response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);

																																																																																																																								}
																																																																																																																								else
																																																																																																																									if (optedEvent == 'F8' && getScreenNo == 'R5') {

																																																																																																																										response.sendRedirect('SUITELET', 'customscript_replen_lp', 'customdeploy_rf_replen_lp_di', false, Reparray);
																																																																																																																									}
																																																																																																																									else
																																																																																																																										if (optedEvent == 'F8' && getScreenNo == 'R6') {

																																																																																																																											response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
																																																																																																																										}
																																																																																																																										else
																																																																																																																											if (optedEvent == 'F8' && getScreenNo == 'R7') {

																																																																																																																												response.sendRedirect('SUITELET', 'customscript_replen_tolp', 'customdeploy_rf_replen_tolp_di', false, Reparray);
																																																																																																																											}
																																																																																																																											/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/					else
																																																																																																																												if (optedEvent == 'F8' && getScreenNo == 'RBATCH') {
																																																																																																																													response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);
																																																																																																																												}/*** up to here ***/
																																																																																																																												else
																																																																																																																													if (optedEvent == 'F8' && getScreenNo == 'CL1') {

																																																																																																																														response.sendRedirect('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di', false, SOarray);
																																																																																																																													}
																																																																																																																													else
																																																																																																																														if (optedEvent == 'F8' && getScreenNo == 'CL2') {

																																																																																																																															response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
																																																																																																																														}
																																																																																																																														else
																																																																																																																															if (optedEvent == 'F8' && getScreenNo == 'CL3') {

																																																																																																																																response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
																																																																																																																															}
																																																																																																																															else
																																																																																																																																if (optedEvent == 'F8' && getScreenNo == 'CL4') {

																																																																																																																																	response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);
																																																																																																																																}
																																																																																																																																else
																																																																																																																																	if (optedEvent == 'F8' && getScreenNo == 'PICKCC1') {

																																																																																																																																		response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
																																																																																																																																	}
																																																																																																																																	else
																																																																																																																																		if (optedEvent == 'F8' && getScreenNo == 'CL4EXP') {

																																																																																																																																			response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
																																																																																																																																		} //code added from Boombah account on 25Feb13 by santosh
																																																																																																																																		else
																																																																																																																																			if (optedEvent == 'F8' && getScreenNo == 'CL4LOCEXP') {
																																																																																																																																				response.sendRedirect('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di', false, SOarray);
																																																																																																																																			} //up to here
																																																																																																																																			else
																																																																																																																																				if (optedEvent == 'F8' && getScreenNo == 'CL5') {

																																																																																																																																					response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_qtyexception', 'customdeploy_rf_cluspicking_qtyexception', false, SOarray);
																																																																																																																																				}

																																																																																																																																				else
																																																																																																																																					if (optedEvent == 'F8' && getScreenNo == 'CLWO1') {

																																																																																																																																						response.sendRedirect('SUITELET', 'customscript_rf_wopicking_cluster_no', 'customdeploy_rf_wopicking_cluster_no', false, SOarray);
																																																																																																																																					}
																																																																																																																																					else
																																																																																																																																						if (optedEvent == 'F8' && getScreenNo == 'CLWO2') {

																																																																																																																																							response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_location', 'customdeploy_rf_wocluspicking_location', false, SOarray);
																																																																																																																																						}
																																																																																																																																						else
																																																																																																																																							if (optedEvent == 'F8' && getScreenNo == 'CLWO3') {

																																																																																																																																								response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
																																																																																																																																							}
																																																																																																																																							else
																																																																																																																																								if (optedEvent == 'F8' && getScreenNo == 'CLWO4') {

																																																																																																																																									response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_dettask', 'customdeploy_rf_wocluspicking_dettask', false, SOarray);
																																																																																																																																								}
																																																																																																																																								else
																																																																																																																																									if (optedEvent == 'F8' && getScreenNo == 'CLWO5') {

																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_stage_loc', 'customdeploy_rf_wocluspicking_stage_loc', false, SOarray);
																																																																																																																																									}

																																																																																																																																									else if (optedEvent == 'F8' && (getScreenNo == 'PL1' || getScreenNo == 'PL2'))
																																																																																																																																									{
																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_printpacklist', 'customdeploy_ebiz_rf_printpacklist', false, packlistArray);
																																																																																																																																									}
																																																																																																																																									else
																																																																																																																																										if (optedEvent == 'F8' && getScreenNo == 'Pack1') {

																																																																																																																																											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanorder', 'customdeploy_ebiz_rf_pack_scanorder_di',false,PackArray);
																																																																																																																																										}
																																																																																																																																										else 
																																																																																																																																											if(optedEvent == 'F8' && getScreenNo == 'Pack2') {
																																																																																																																																												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scancontainer', 'customdeploy_ebiz_rf_pack_scancontainer', false, PackArray);
																																																																																																																																											}

																																																																																																																																											else
																																																																																																																																												if(optedEvent == 'F8' && getScreenNo == 'Pack4') {
																																																																																																																																													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanqty', 'customdeploy_ebiz_rf_pack_scanqty_di', false, PackArray);
																																																																																																																																												}
																																																																																																																																												else
																																																																																																																																													if(optedEvent == 'F8' && getScreenNo == 'Pack3') {
																																																																																																																																														response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanitem', 'customdeploy_ebiz_rf_pack_scanitem_di', false, PackArray);
																																																																																																																																													}
																																																																																																																																													else
																																																																																																																																														if(optedEvent == 'F8' && getScreenNo == 'Pack5') {
																																																																																																																																															response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, PackArray);
																																																																																																																																														}
																																																																																																																																														else
																																																																																																																																															if(optedEvent == 'F8' && getScreenNo == 'Pack6') {
																																																																																																																																																response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanorder', 'customdeploy_ebiz_rf_pack_scanorder_di',false,PackArray);
																																																																																																																																															}																																																																																																																																																													
																																																																																																																																															else

																																																																																																																																																if (optedEvent == 'F8' && getScreenNo == 'ProductionMenu') {

																																																																																																																																																	response.sendRedirect('SUITELET', 'customscript_rf_production_menu', 'customdeploy_rf_production_menu_di', false, PRODArray);
																																																																																																																																																}
																																																																																																																																																else
																																																																																																																																																	if (optedEvent == 'F8' && getScreenNo == 'PRODSO') {

																																																																																																																																																		response.sendRedirect('SUITELET', 'customscript_rf_production_qc_approval', 'customdeploy_rf_production_qc_approv_di', false, PRODArray);
																																																																																																																																																	}
																																																																																																																																																	else
																																																																																																																																																		if (optedEvent == 'F8' && getScreenNo == 'PRODQty') {

																																																																																																																																																			response.sendRedirect('SUITELET', 'customscript_rf_production_qc_qty', 'customdeploy_rf_production_qc_qty_di', false, PRODArray);
																																																																																																																																																		}
																																																																																																																																																		else
																																																																																																																																																			if (optedEvent == 'F8' && getScreenNo == 'ScrapAdjt') {
																																																																																																																																																				response.sendRedirect('SUITELET', 'customscript_rf_scrap_adjustment', 'customdeploy_rf_scrap_adjustment_di', false, null);
																																																																																																																																																			}
																																																																																																																																																			else
																																																																																																																																																				if (optedEvent == 'F8' && getScreenNo == 'ScrapAdjtQty') {

																																																																																																																																																					response.sendRedirect('SUITELET', 'customscript_rf_scrap_adjt_qty', 'customdeploy_rf_scrap_adjt_qty_di', false, PRODArray);
																																																																																																																																																				}
																																																																																																																																																				else
																																																																																																																																																					if (optedEvent == 'F8' && getScreenNo == 'ZoneCompletion') {

																																																																																																																																																						response.sendRedirect('SUITELET', 'customscript_rf_picking_zone_completion', 'customdeploy_rf_picking_zone_comp_di', false, null);
																																																																																																																																																					}
																																																																																																																																																					else
																																																																																																																																																						if (optedEvent == 'F8' && getScreenNo == 'sortscan') {

																																																																																																																																																							response.sendRedirect('SUITELET', 'customscript_rf_picking_sortscan_station', 'customdeploy_rf_picking_sortscan_di', false, null);
																																																																																																																																																						}
																																																																																																																																																						else
																																																																																																																																																							if (optedEvent == 'F8' && getScreenNo == 'UCC1') {

																																																																																																																																																								response.sendRedirect('SUITELET', 'customscript_rf_scanucclabel', 'customdeploy_rf_scanucclabel', false, null);
																																																																																																																																																							}
																																																																																																																																																							else
																																																																																																																																																								if (optedEvent == 'F8' && getScreenNo == 'UCCTASK1') {

																																																																																																																																																									response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalbytask', 'customdeploy_rf_outboundreversalbytask', false, null);
																																																																																																																																																								}
																																																																																																																																																								else
																																																																																																																																																									if (optedEvent == 'F8' && getScreenNo == 'UCCTASK2') {
																																																																																																																																																										//Case # 20126321��Start
																																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalcontinue', 'customdeploy_rf_outboundreversalcontinue', false, UCCarray);
																																																																																																																																																										//Case # 20126321��End
																																																																																																																																																									}
																																																																																																																																																									else
																																																																																																																																																										if (optedEvent == 'F8' && getScreenNo == 'UPC1') {

																																																																																																																																																											response.sendRedirect('SUITELET', 'customscript_rf_upccode_plan_no', 'customdeploy_rf_upccode_plan_no_di', false, UPCArray);
																																																																																																																																																										}
																																																																																																																																																										else
																																																																																																																																																											if (optedEvent == 'F8' && getScreenNo == 'UPC2') {

																																																																																																																																																												response.sendRedirect('SUITELET', 'customscript_rf_upccode_location', 'customdeploy_rf_upccode_location_di', false, UPCArray);
																																																																																																																																																											}
																																																																																																																																																											else
																																																																																																																																																												if (optedEvent == 'F8' && getScreenNo == 'UPC3') {

																																																																																																																																																													response.sendRedirect('SUITELET', 'customscript_rf_upccode_item', 'customdeploy_rf_upccode_item_di', false, UPCArray);
																																																																																																																																																												}
																																																																																																																																																												else
																																																																																																																																																													if (optedEvent == 'F8' && getScreenNo == 'buildcartonLp') {
																																																																																																																																																														// Packing Criteria Menu
																																																																																																																																																														response.sendRedirect('SUITELET', 'customscript_rf_buildcarton_lp', 'customdeploy_rf_buildcarton_lp_di', false, null);
																																																																																																																																																														//response.sendRedirect('SUITELET', 'customscript_rf_packing_menu', 'customdeploy_rf_packing_menu_di', false, POarray);
																																																																																																																																																													}
																																																																																																																																																													else
																																																																																																																																																														if (optedEvent == 'F8' && getScreenNo == 'BuildCartonCart') {
																																																																																																																																																															// Packing Criteria Menu
																																																																																																																																																															response.sendRedirect('SUITELET', 'customscript_rf_buildcarton_cart', 'customdeploy_rf_buildcarton_cart_di', false, POarray);
																																																																																																																																																															//response.sendRedirect('SUITELET', 'customscript_rf_packing_menu', 'customdeploy_rf_packing_menu_di', false, POarray);
																																																																																																																																																														}
																																																																																																																																																														else
																																																																																																																																																															if (optedEvent == 'F8' && getScreenNo == 'Putawaycarton') {
																																																																																																																																																																// Packing Criteria Menu
																																																																																																																																																																response.sendRedirect('SUITELET', 'customscript_rf_cartputawayscanlp', 'customdeploy_rf_cartputawayscanlp', false, POarrayget);
																																																																																																																																																																//response.sendRedirect('SUITELET', 'customscript_rf_packing_menu', 'customdeploy_rf_packing_menu_di', false, POarray);
																																																																																																																																																															}
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
																																																																																																																																																															else
																																																																																																																																																																if (optedEvent == 'F8' && getScreenNo == 'SHIPLOC') {

																																																																																																																																																																	//response.sendRedirect('SUITELET', 'customscriptrf_shiplp_location', 'customdeployrf_shiplp_location_di', false, ShipArray);
																																																																																																																																																																	response.sendRedirect('SUITELET', 'customscript_rf_shiplp_location', 'customdeploy_rf_shiplp_location', false, null);
																																																																																																																																																																}
																																																																																																																																																																else
																																																																																																																																																																	if (optedEvent == 'F8' && getScreenNo == 'INVTSEAR') {
																																																																																																																																																																		// Inventory Search - LP/Location/ITEM 
																																																																																																																																																																		response.sendRedirect('SUITELET', 'customscript_ebiz_rf_inventory_search', 'customdeploy_ebiz_rf_inventory_search_di', false, IMarray);
																																																																																																																																																																	}
																																																																																																																																																																	else
																																																																																																																																																																		if (optedEvent == 'F8' && getScreenNo == 'INVTDTLS') {
																																																																																																																																																																			// Inventory Search Details
																																																																																																																																																																			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_inventory_details', 'customdeploy_ebiz_rf_inventory_details_d', false, IMarray);
																																																																																																																																																																		}
		/* Up to here */ 
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/																																																																																																											/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle*/
																																																																																																																																																																		else //work order picking process
																																																																																																																																																																			if(optedEvent == 'F8' && getScreenNo == 'WOmenu')	
																																																																																																																																																																			{
																																																																																																																																																																				// WorkOrder  Menu
																																																																																																																																																																				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di', false, SOarray);
																																																																																																																																																																			}
																																																																																																																																																																			else
																																																																																																																																																																				if(optedEvent == 'F8' && getScreenNo == 'WOPicking')	//work order
																																																																																																																																																																				{
																																																																																																																																																																					// WorkOrder  Picking menu
																																																																																																																																																																					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking', 'customdeploy_ebiz_rf_wo_picking_di', false, SOarray);
																																																																																																																																																																				}
																																																																																																																																																																				else
																																																																																																																																																																					if(optedEvent == 'F8' && getScreenNo == 'WOLoc')	
																																																																																																																																																																					{
																																																																																																																																																																						// WorkOrder Location Scan
																																																																																																																																																																						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, SOarray);
																																																																																																																																																																					}

																																																																																																																																																																					else
																																																																																																																																																																						if(optedEvent == 'F8' && getScreenNo == 'WOItem')	
																																																																																																																																																																						{
																																																																																																																																																																							// WorkOrder Item Scan
																																																																																																																																																																							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, SOarray);
																																																																																																																																																																						}
																																																																																																																																																																						else
																																																																																																																																																																							if(optedEvent == 'F8' && getScreenNo == 'WOPickingBatch')	
																																																																																																																																																																							{
																																																																																																																																																																								// WorkOrder batch Scan
																																																																																																																																																																								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_batch', 'customdeploy_ebiz_rf_wo_picking_batch_di', false, SOarray);
																																																																																																																																																																							}
																																																																																																																																																																							else
																																																																																																																																																																								if(optedEvent == 'F8' && getScreenNo == 'UpdateSRR')	
																																																																																																																																																																								{
																																																																																																																																																																									// Update Shipment Request Route #
																																																																																																																																																																									response.sendRedirect('SUITELET', 'customscript_rf_shipping_updatesrr', 'customdeploy_rf_shipping_updatesrr_di', false, null);
																																																																																																																																																																								}
																																																																																																																																																																								else
																																																																																																																																																																									if(optedEvent == 'F8' && getScreenNo == 'WOstagelocation')	
																																																																																																																																																																									{
																																																																																																																																																																										// WorkOrder picking staging
																																																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_rf_wo_picking_stage_loc', 'customdeploy_rf_wo_picking_stage_loc_di', false, SOarray);
																																																																																																																																																																									}
																																																																																																																																																																									else
																																																																																																																																																																										if(optedEvent == 'F8' && getScreenNo == 'WOPutScanLp')	
																																																																																																																																																																										{
																																																																																																																																																																											// WO putaway scanLP
																																																																																																																																																																											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_put_lp', 'customdeploy_ebiz_rf_wo_put_lp_di', false, POarray);
																																																																																																																																																																										}
																																																																																																																																																																										else 
																																																																																																																																																																											if(optedEvent == 'F8' && getScreenNo == 'WOPutConfirm')	
																																																																																																																																																																											{
																																																																																																																																																																												// WO putaway Putway confirm
																																																																																																																																																																												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putconfirm_loc', 'customdeploy_ebiz_rf_wo_putconfirm_loc_d', false, POarray);
																																																																																																																																																																											}																																																																						
																																																																																																																																																																											else
																																																																																																																																																																												if(optedEvent == 'F8' && getScreenNo == 'WOPutLocExcep')	
																																																																																																																																																																												{
																																																																																																																																																																													// WO putaway Location Exception
																																																																																																																																																																													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_put_loc_excep', 'customdeploy_ebiz_rf_wo_put_loc_excep_di', false, POarray);
																																																																																																																																																																												}
																																																																																																																																																																												else
																																																																																																																																																																													if(optedEvent == 'F8' && getScreenNo == 'WOPutawayGen')	
																																																																																																																																																																													{
																																																																																																																																																																														// WO Build Assembly WO# scan
																																																																																																																																																																														response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_gen', 'customdeploy_ebiz_rf_wo_putaway_gen_di', false, POarray);
																																																																																																																																																																													}
																																																																																																																																																																													else
																																																																																																																																																																														if(optedEvent == 'F8' && getScreenNo == 'WOPutawayGenQty')	
																																																																																																																																																																														{
																																																																																																																																																																															// WO Build Assembly WO# scan
																																																																																																																																																																															response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_qty', 'customdeploy_ebiz_rf_wo_putaway_qty_di', false, POarray);
																																																																																																																																																																														}
																																																																																																																																																																														else
																																																																																																																																																																															if(optedEvent == 'F8' && getScreenNo == 'WOPutawayGenBatch')	
																																																																																																																																																																															{
																																																																																																																																																																																// WO Build Assembly Batchno scan
																																																																																																																																																																																response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_batchno', 'customdeploy_ebiz_rf_wo_putaway_batch_di', false, POarray);
																																																																																																																																																																															}
																																																																																																																																																																															else
																																																																																																																																																																																if(optedEvent == 'F8' && getScreenNo == 'WOPutawayGenLoc')	
																																																																																																																																																																																{
																																																																																																																																																																																	// WO Build Assembly Location scan
																																																																																																																																																																																	response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_loc', 'customdeploy_ebiz_rf_wo_putaway_loc_di', false, POarray);
																																																																																																																																																																																}
																																																																																																																																																																																else
																																																																																																																																																																																	if(optedEvent == 'F8' && getScreenNo == 'WOPutGenLP')	
																																																																																																																																																																																	{
																																																																																																																																																																																		// WO Build Assembly LP scan
																																																																																																																																																																																		response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_lp', 'customdeploy_ebiz_rf_wo_putaway_lp_di', false, POarray);
																																																																																																																																																																																	}
																																																																																																																																																																																	else
																																																																																																																																																																																		if(optedEvent == 'F8' && getScreenNo == 'WOLotExpDt')
																																																																																																																																																																																		{
																																																																																																																																																																																			// WO Build Assembly LotExpdate
																																																																																																																																																																																			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_lotexpdate', 'customdeploy_ebiz_rf_wo_lotexpdate_di', false, POarray);
																																																																																																																																																																																		}
																																																																																																																																																																																		else
																																																																																																																																																																																			if(optedEvent == 'F8' && getScreenNo == 'WOLotFIFODt')
																																																																																																																																																																																			{
																																																																																																																																																																																				// WO Build Assembly FIFOExpdate
																																																																																																																																																																																				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_lotfifodate', 'customdeploy_ebiz_rf_wo_lotfifodate_di', false, POarray);
																																																																																																																																																																																			}
																																																																																																																																																																																			else
																																																																																																																																																																																				if(optedEvent == 'F8' && getScreenNo == 'WOPickQtyExcep')	
																																																																																																																																																																																				{
																																																																																																																																																																																					// WO Picking Qty exception
																																																																																																																																																																																					response.sendRedirect('SUITELET', 'customscript_ebi_rf_wo_picking_qty_excep', 'customdeploy_ebiz_rf_wo_pick_qty_exp_di', false, SOarray);

																																																																																																																																																																																				}
																																																																																																																																																																																				else 
																																																																																																																																																																																					if(optedEvent == 'F8' && getScreenNo == 'WOPickLocEXP')
																																																																																																																																																																																					{
																																																																																																																																																																																						//WO Picking Location exception
																																																																																																																																																																																						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_loc_excp', 'customdeploy_ebiz_rf_wo_picking_locexp_d', false, SOarray);
																																																																																																																																																																																					}
																																																																																																																																																																																					else
																																																																																																																																																																																						if(optedEvent == 'F8' && getScreenNo == 'WOPickConfirm')
																																																																																																																																																																																						{
																																																																																																																																																																																							//WO Picking Confirmation
																																																																																																																																																																																							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);

																																																																																																																																																																																						}
																																																																																																																																																																																						else
																																																																																																																																																																																							if (optedEvent == 'F8' && getScreenNo == '17C') {
																																																																																																																																																																																								// Rf Create Inventory Serial# Scanning 
																																																																																																																																																																																								response.sendRedirect('SUITELET', 'customscript_rf_create_invt_serialscan', 'customdeploy_rf_create_invt_serialscan_d', false, CIarray);
																																																																																																																																																																																							}
		/*** up to here ***/	
																																																																																																																																																																																							else
																																																																																																																																																																																								if (optedEvent == 'F8' && getScreenNo == 'PickLPExp') {
																																																																																																																																																																																									//  LP Exception in Rf Order Picking  
																																																																																																																																																																																									response.sendRedirect('SUITELET', 'customscript_rf_pickinglpexc', 'customdeploy_rf_pickinglpexc', false, SOarray);
																																																																																																																																																																																								}
																																																																																																																																																																																								else
																																																																																																																																																																																									if (optedEvent == 'F8' && getScreenNo == 'UCC') {
																																																																																																																																																																																										//  LP Exception in Rf Order Picking  
																																																																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_rf_ucclabelsgeneration', 'customdeploy_rf_ucclabelsgeneration_di', false, POarray);
																																																																																																																																																																																									}
																																																																																																																																																																																									else if (optedEvent == 'F8' && getScreenNo == 'CONFRMInvtMove')
																																																																																																																																																																																									{
																																																																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_main', 'customdeploy_rf_inventory_move_main_di', false, optedEvent);
																																																																																																																																																																																									}
																																																																																																																																																																																									else if (optedEvent == 'F8' && getScreenNo == 'PR01')
																																																																																																																																																																																									{
																																																																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_postitemreceipt', 'customdeploy_ebiz_rf_postitemreceipt_di', false, optedEvent);
																																																																																																																																																																																									}
																																																																																																																																																																																									else if(optedEvent == 'F8' && getScreenNo == 'WOPutGenStatus')	
																																																																																																																																																																																									{
																																																																																																																																																																																										// WO Build Assembly Status scan
																																																																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_status', 'customdeploy_ebiz_rf_wo_put_status_dl', false, POarray);
																																																																																																																																																																																									}
																																																																																																																																																																																									else//For Bulk Picking
																																																																																																																																																																																										if (optedEvent == 'F8' && getScreenNo == 'BULK01') {
																																																																																																																																																																																											// For Serial Number Scanning screen

																																																																																																																																																																																											response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_container', 'customdeploy_rf_bulkpick_container_di', false, SOarray);
																																																																																																																																																																																											//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_bulkpick_location', 'customdeploy_ebiz_rf_bulkpick_location', false, SOarray);
																																																																																																																																																																																										}
																																																																																																																																																																																										else
																																																																																																																																																																																											if (optedEvent == 'F8' && getScreenNo == 'BULK02') {
																																																																																																																																																																																												// For Serial Number Scanning screen
																																																																																																																																																																																												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_bulkpick_location', 'customdeploy_ebiz_rf_bulkpick_location', false, SOarray);
																																																																																																																																																																																											}
																																																																																																																																																																																											else
																																																																																																																																																																																												if (optedEvent == 'F8' && getScreenNo == 'BULK03') {
																																																																																																																																																																																													// For Serial Number Scanning screen
																																																																																																																																																																																													response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_item', 'customdeploy_rf_bulkpick_item', false, SOarray);
																																																																																																																																																																																												}
																																																																																																																																																																																												else
																																																																																																																																																																																													if (optedEvent == 'F8' && getScreenNo == 'BULK04') {
																																																																																																																																																																																														// For Serial Number Scanning screen
																																																																																																																																																																																														response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_confirm', 'customdeploy_rf_bulkpick_confirm_di', false, SOarray);
																																																																																																																																																																																													}
																																																																																																																																																																																													else
																																																																																																																																																																																														if (optedEvent == 'F8' && getScreenNo == 'BULK05') {
																																																																																																																																																																																															// For Serial Number Scanning screen
																																																																																																																																																																																															response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_container_no', 'customdeploy_rf_bulkpick_container_no_di', false, SOarray);
																																																																																																																																																																																														}
																																																																																																																																																																																														else
																																																																																																																																																																																															if (optedEvent == 'F8' && getScreenNo == 'BULK06') {
																																																																																																																																																																																																// For Serial Number Scanning screen
																																																																																																																																																																																																response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_stage_loc', 'customdeploy_rf_bulkpick_stage_loc_di', false, SOarray);
																																																																																																																																																																																															}     
																																																																																																																																																																																															else//case 20123285
																																																																																																																																																																																																if (optedEvent == 'F8' && getScreenNo == 'PLT2') {
																																																																																																																																																																																																	// Check-In Lot# screen
																																																																																																																																																																																																	response.sendRedirect('SUITELET', 'customscript_rf_checkin_batch_no', 'customdeploy_rf_checkin_batch_no_di', false, POarray);
																																																																																																																																																																																																}//end
																																																																																																																																																																																																else
																																																																																																																																																																																																	if (optedEvent == 'F8' && getScreenNo == 'SHPMENU') {
																																																																																																																																																																																																		// Shipping main menu
																																																																																																																																																																																																		response.sendRedirect('SUITELET', 'customscript_rf_shipping_menu', 'customdeploy_rf_shipping_menu_di', false, POarray);

																																																																																																																																																																																																	}
																																																																																																																																																																																																	else
																																																																																																																																																																																																		if (optedEvent == 'F8' && getScreenNo == 'OBReversalMenu') {
																																																																																																																																																																																																			//outbound reversal menu
																																																																																																																																																																																																			response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalmenu', 'customdeploy_rf_outboundreversalmenu', false, SOarray);

																																																																																																																																																																																																		}
																																																																																																																																																																																																		else
																																																																																																																																																																																																			if (optedEvent == 'F8' && getScreenNo == 'PICK MENU') {
																																																																																																																																																																																																				//outbound reversal menu
																																																																																																																																																																																																				response.sendRedirect('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di', false, SOarray);

																																																																																																																																																																																																			}
																																																																																																																																																																																																			else
																																																																																																																																																																																																				if (optedEvent == 'F8' && getScreenNo == 'Picklp') {
																																																																																																																																																																																																					// For Serial Number Scanning screen
																																																																																																																																																																																																					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_picklp', 'customdeploy_ebiz_rf_picklp', false, null);
																																																																																																																																																																																																				}  
																																																																																																																																																																																																				else
																																																																																																																																																																																																					if (optedEvent == 'F8' && getScreenNo == 'SerialScan') {
																																																																																																																																																																																																						// For Serial Number Scanning screen
																																																																																																																																																																																																						response.sendRedirect('SUITELET', 'customscript_ebiz_rrf_scanserailucclbls', 'customdeploy_ebiz_rrf_scanserailucclbls', false, lparray);
																																																																																																																																																																																																					}  
																																																																																																																																																																																																					else
																																																																																																																																																																																																						if (optedEvent == 'F8' && getScreenNo == 'SerialScanComplete') {
																																																																																																																																																																																																							// For Serial Number Scanning screen
																																																																																																																																																																																																							response.sendRedirect('SUITELET', 'customscript_rf_shipping_menu', 'customdeploy_rf_shipping_menu_di', false, lparray);
																																																																																																																																																																																																						}
																																																																																																																																																																																																						else
																																																																																																																																																																																																							if (optedEvent == 'F8' && getScreenNo == 'SS01') {
																																																																																																																																																																																																								// For Serial Number Scanning screen
																																																																																																																																																																																																								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_serialscan_menu', 'customdeploy_ebiz_rf_serialscan_menu', false, lparray);
																																																																																																																																																																																																							} 
																																																																																																																																																																																																							else
																																																																																																																																																																																																								if (optedEvent == 'F8' && getScreenNo == 'SS02') {
																																																																																																																																																																																																									// For Serial Number Scanning screen
																																																																																																																																																																																																									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_picklp', 'customdeploy_ebiz_rf_picklp', false, lparray);
																																																																																																																																																																																																								} 
																																																																																																																																																																																																								else
																																																																																																																																																																																																									if (optedEvent == 'F8' && getScreenNo == 'FASTPickLotExp') {
																																																																																																																																																																																																										//Fast Picking
																																																																																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_lot_exp', 'customdeploy_ebiz_rf_pick_lot_exp_di', false, SOarray);

																																																																																																																																																																																																									}  
																																																																																																																																																																																																									else
																																																																																																																																																																																																										if (optedEvent == 'F8' && getScreenNo == 'SS03') {
																																																																																																																																																																																																											// For Serial Number Scanning screen
																																																																																																																																																																																																											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_scanucc_tracking', 'customdeploy_ebiz_rf_scanucc_tracking', false, lparray);
																																																																																																																																																																																																										} 
																																																																																																																																																																																																										else
																																																																																																																																																																																																											if (optedEvent == 'F8' && getScreenNo == 'SS04') {
																																																																																																																																																																																																												// For Serial Number Scanning screen
																																																																																																																																																																																																												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_scan_serialitem', 'customdeploy_ebiz_rf_scan_serialitem', false, lparray);
																																																																																																																																																																																																											} 
																																																																																																																																																																																																											else
																																																																																																																																																																																																												if (optedEvent == 'F8' && getScreenNo == 'SS05') {
																																																																																																																																																																																																													// For Serial Number Scanning screen
																																																																																																																																																																																																													response.sendRedirect('SUITELET', 'customscript_ebiz_rrf_scanserailucclbls', 'customdeploy_ebiz_rrf_scanserailucclbls', false, lparray);
																																																																																																																																																																																																												} 
																																																																																																																																																																																																												else
																																																																																																																																																																																																													if (optedEvent == 'F8' && getScreenNo == '2stepCRT') {
																																																																																																																																																																																																														// For Serial Number Scanning screen
																																																																																																																																																																																																														response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplencartscan', 'customdeploy_ebiz_twostepreplencartscan', false, Reparray);
																																																																																																																																																																																																													} 
																																																																																																																																																																																																													else
																																																																																																																																																																																																														if (optedEvent == 'F8' && getScreenNo == '2stepRptNo') {
																																																																																																																																																																																																															// For Serial Number Scanning screen
																																																																																																																																																																																																															response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenreportno', 'customdeploy_ebiz_twostepreplenreportno', false, Reparray);
																																																																																																																																																																																																														} 
																																																																																																																																																																																																														else
																																																																																																																																																																																																															if (optedEvent == 'F8' && getScreenNo == '2stepMenu') {
																																																																																																																																																																																																																// For Serial Number Scanning screen
																																																																																																																																																																																																																response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenmenu', 'customdeploy_ebiz_twostepreplenmenu', false, Reparray);
																																																																																																																																																																																																															} 
																																																																																																																																																																																																															else
																																																																																																																																																																																																																if (optedEvent == 'F8' && getScreenNo == '2stepRpLoc') {
																																																																																																																																																																																																																	// For Serial Number Scanning screen
																																																																																																																																																																																																																	response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
																																																																																																																																																																																																																} 
																																																																																																																																																																																																																else
																																																																																																																																																																																																																	if (optedEvent == 'F8' && getScreenNo == '2stepRpItm') {
																																																																																																																																																																																																																		// For Serial Number Scanning screen
																																																																																																																																																																																																																		response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenitem', 'customdeploy_ebiz_twostepreplenitem', false, Reparray);
																																																																																																																																																																																																																	} 
																																																																																																																																																																																																																	else
																																																																																																																																																																																																																		if (optedEvent == 'F8' && getScreenNo == '2StepRpQty') {
																																																																																																																																																																																																																			// For Serial Number Scanning screen
																																																																																																																																																																																																																			response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenqty', 'customdeploy_ebiz_twostepreplenqty', false, Reparray);
																																																																																																																																																																																																																			//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_2', 'customdeploy_ebiz_hook_sl_2', false, Reparray);
																																																																																																																																																																																																																		} 
																																																																																																																																																																																																																		else
																																																																																																																																																																																																																			if (optedEvent == 'F8' && getScreenNo == '2Steppw') {
																																																																																																																																																																																																																				response.sendRedirect('SUITELET', 'customscript_ebiz_twostep_putawaycartlp', 'customdeploy_ebiz_twostep_putawaycartlp', false, Reparray);
																																																																																																																																																																																																																			} 
																																																																																																																																																																																																																			else
																																																																																																																																																																																																																				if (optedEvent == 'F8' && getScreenNo == '2Stepcnfpw') {
																																																																																																																																																																																																																					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cnf_opencartlp', 'customdeploy_ebiz_rf_cnf_opencartlp', false, Reparray);
																																																																																																																																																																																																																				} 
																																																																																																																																																																																																																				else
																																																																																																																																																																																																																					if (optedEvent == 'F8' && getScreenNo == '2StepBin') {
																																																																																																																																																																																																																						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_scanloc', 'customdeploy_ebiz_rf_twostp_scanloc', false, Reparray);
																																																																																																																																																																																																																					} 
																																																																																																																																																																																																																					else
																																																																																																																																																																																																																						if (optedEvent == 'F8' && getScreenNo == '2Stepcnfrepln') {
																																																																																																																																																																																																																							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_repenconfirm', 'customdeploy_ebiz_rf_twostp_repenconfirm', false, Reparray);
																																																																																																																																																																																																																							//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_4', 'customdeploy_ebiz_hook_sl_4', false, Reparray);
																																																																																																																																																																																																																						} 
																																																																																																																																																																																																																						else
																																																																																																																																																																																																																							if (optedEvent == 'F8' && getScreenNo == '2stepSTG') {
																																																																																																																																																																																																																								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostep_stageloc', 'customdeploy_ebiz_rf_twostep_stageloc', false, Reparray);
																																																																																																																																																																																																																								//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_3', 'customdeploy_ebiz_hook_sl_3', false, Reparray);
																																																																																																																																																																																																																							} 
																																																																																																																																																																																																																							else
																																																																																																																																																																																																																								if (optedEvent == 'F8' && getScreenNo == '14PickingReplnBatch') {
																																																																																																																																																																																																																									response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplen_batch', 'customdeploy_ebiz_twostepreplen_batch_di', false, Reparray);

																																																																																																																																																																																																																								} 
																																																																																																																																																																																																																								else
																																																																																																																																																																																																																									if (optedEvent == 'F8' && getScreenNo == 'WOPickClusEXP') {
																																																																																																																																																																																																																										// For  WO Cluster picking 
																																																																																																																																																																																																																										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_pick_loc_excep', 'customdeploy_ebiz_rf_wo_pick_loc_exp_di', false, SOarray);
																																																																																																																																																																																																																									}
																																																																																																																																																																																																																									else
																																																																																																																																																																																																																										if (optedEvent == 'F8' && getScreenNo == 'WOclusQtyExp') {
																																																																																																																																																																																																																											// For  WO Cluster picking qty exception 
																																																																																																																																																																																																																											response.sendRedirect('SUITELET', 'customscript_rf_wo_cluspicking_qtyexcep', 'customdeploy_rf_wo_cluspicking_qtyexcp_d', false, SOarray);
																																																																																																																																																																																																																										}		
																																																																																																																																																																																																																										else
																																																																																																																																																																																																																											if (optedEvent == 'F8' && getScreenNo == 'Rserial') {
																																																																																																																																																																																																																												// For  repln serial 
																																																																																																																																																																																																																												response.sendRedirect('SUITELET', 'customscript_rf_replen_serialscan', 'customdeploy_rf_replen_serialscan_di', false, Reparray);
																																																																																																																																																																																																																											}	
																																																																																																																																																																																																																											else
																																																																																																																																																																																																																												if (optedEvent == 'F8' && getScreenNo == '27A') {
																																																																																																																																																																																																																													// For  repln serial 
																																																																																																																																																																																																																													response.sendRedirect('SUITELET', 'customscript_rf_mergepacking_containerlp', 'customdeploy_rf_mergepacking_contlp_di', false, SOarray);
																																																																																																																																																																																																																												}	
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}

