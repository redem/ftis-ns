/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_WaveReleaseQb_SL.js,v $
 *     	   $Revision: 1.1.4.5.2.19.2.1 $
 *     	   $Date: 2015/09/11 14:09:09 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveReleaseQb_SL.js,v $
 * Revision 1.1.4.5.2.19.2.1  2015/09/11 14:09:09  schepuri
 * case# 201414310
 *
 * Revision 1.1.4.5.2.19  2015/09/01 18:11:15  snimmakayala
 * no message
 *
 * Revision 1.1.4.5.2.18  2015/08/12 15:41:17  nneelam
 * case# 201413971
 *
 * Revision 1.1.4.5.2.17  2015/07/28 16:32:26  grao
 * 2015.2   issue fixes  201413054
 *
 * Revision 1.1.4.5.2.16  2015/06/04 06:28:48  schepuri
 * case# 201412665
 *
 * Revision 1.1.4.5.2.15  2015/03/16 07:53:31  snimmakayala
 * 201411870
 *
 * Revision 1.1.4.5.2.14  2014/10/17 13:31:02  skavuri
 * Case# 201410631 Std bundle Issue fixed
 *
 * Revision 1.1.4.5.2.13  2014/09/24 15:59:40  skavuri
 * Case# 201410303 issue fixed
 *
 * Revision 1.1.4.5.2.12  2014/09/18 13:44:10  sponnaganti
 * Case# 201410320
 * TPP SB Issue fix
 *
 * Revision 1.1.4.5.2.11  2014/07/14 15:32:33  sponnaganti
 * Case# 20149447
 * Compatibility Issue fix
 *
 * Revision 1.1.4.5.2.10  2014/06/24 06:07:38  skreddy
 * case # 20148997
 * Sportshq prod  issue fix
 *
 * Revision 1.1.4.5.2.9  2014/05/22 06:48:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148481
 *
 * Revision 1.1.4.5.2.8  2014/05/19 06:29:43  skreddy
 * case # 20148191
 * Sonic SB issue fix
 *
 * Revision 1.1.4.5.2.7  2014/03/19 09:08:44  snimmakayala
 * Case #: 20127688
 *
 * Revision 1.1.4.5.2.6  2013/10/16 07:23:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdchanges
 *
 * Revision 1.1.4.5.2.5  2013/07/18 15:07:02  grao
 * case# 20123474
 * ship via filed showing no values (empty)
 *
 * Revision 1.1.4.5.2.4  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.4.5.2.3  2013/05/21 07:27:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.4.5.2.2  2013/03/22 11:44:32  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.4.5.2.1  2013/03/21 14:18:14  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.4.5  2013/02/16 12:11:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.1.4.4  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.4.3  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.1.4.2  2012/10/07 23:33:55  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Wave Selection by Order changes.
 *
 *
 *
 **********************************************************************************************************************/

function fillfulfillorderField(form, FulfillOrderField,shipmentField,maxno){
	var filtersso = new Array();		
	filtersso.push(new nlobjSearchFilter( 'custrecord_ord_qty', null, 'greaterthan', '0' ));
	//11-Partially Picked, 13-Partially Shipped, 15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filtersso.push(new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [11,13,15,25,26]));
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var vRoleLocation=getRoledBasedLocation();
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', vRoleLocation));

	}
	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('id');	 
	columnsinvt[1] = new nlobjSearchColumn('custrecord_lineord');
	columnsinvt[0].setSort(true);
	columnsinvt[2] = new nlobjSearchColumn('custrecord_shipment_no');

	FulfillOrderField.addSelectOption("", "");
	shipmentField.addSelectOption("","");

	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersso,columnsinvt);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		if(customerSearchResults[i].getValue('custrecord_lineord') != null && customerSearchResults[i].getValue('custrecord_lineord') != "" && 
				customerSearchResults[i].getValue('custrecord_lineord') != " ")
		{
			var resdo = form.getField('custpage_qbso').getSelectOptions(customerSearchResults[i].getValue('custrecord_lineord'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		FulfillOrderField.addSelectOption(customerSearchResults[i].getValue('custrecord_lineord'), customerSearchResults[i].getValue('custrecord_lineord'));
	}
	for (var j = 0; customerSearchResults != null && j < customerSearchResults.length; j++) {

		if(customerSearchResults[j].getValue('custrecord_shipment_no') != null && customerSearchResults[j].getValue('custrecord_shipment_no') != "" && customerSearchResults[j].getValue('custrecord_shipment_no') != " ")
		{
			var resdo2 = form.getField('custpage_qbshipmentno').getSelectOptions(customerSearchResults[j].getValue('custrecord_shipment_no'), 'is');
			if (resdo2 != null && resdo2 != '') {
				if (resdo2.length > 0) {
					continue;
				}
			}
			shipmentField.addSelectOption(customerSearchResults[j].getValue('custrecord_shipment_no'), customerSearchResults[j].getValue('custrecord_shipment_no'));
		}

	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		fillfulfillorderField(form, FulfillOrderField,shipmentField,maxno);	
	}
}

function WaveReleaseQB(request, response){
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('DEBUG', 'Time Stamp at the start of response',TimeStampinSec()); // case# 201416942

		var form = nlapiCreateForm('Wave Creation & Release');

		var soField = form.addField('custpage_qbso', 'select', 'Fulfillment Order #');
		soField.setLayoutType('startrow', 'none');  	
		//var soFieldtext = form.addField('custpage_qbsotext', 'text', 'Fulfillment Order #');
		//soField.setLayoutType('startrow', 'none');  
//		Shipment #

		var Sono = form.addField('custpage_sono', 'text', 'Sales Order #');

		var shipmentField = form.addField('custpage_qbshipmentno', 'select', 'Shipment #');
		shipmentField .setLayoutType('startrow', 'none');
		fillfulfillorderField(form, soField,shipmentField,-1);

		nlapiLogExecution('DEBUG', 'Time Stamp after filling FO and Shipment Combos',TimeStampinSec());

		//Route #
		var RouteField = form.addField('custpage_qbrouteno', 'select', 'Route','customlist_ebiznetroutelov');
		RouteField.setLayoutType('startrow', 'none');

		var soOrdPriority = form.addField('custpage_ordpriority', 'select', 'Order Priority','customlist_ebiznet_order_priority');
		soOrdPriority.setLayoutType('startrow', 'none'); 		

		var soOrdType = form.addField('custpage_ordtype', 'select', 'Order Type','customrecord_ebiznet_order_type');
		soOrdType.setLayoutType('startrow', 'none');

		var Consignee = form.addField('custpage_consignee', 'select', 'Consignee','customer');
		Consignee.setLayoutType('startrow', 'none');

		var itemfamily = form.addField('custpage_itemfamily', 'select', 'Item Family','customrecord_ebiznet_sku_family');

		var itemgroup = form.addField('custpage_itemgroup', 'select', 'Item Group','customrecord_ebiznet_sku_group');

		var item = form.addField('custpage_item', 'select', 'Item','inventoryitem');	

		var iteminfo1= form.addField('custpage_iteminfoone', 'select', 'Item Info1');
		iteminfo1.addSelectOption("","");
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_item_info_1',null,'group');			 
		var filters= new Array();			
		filters.push(new nlobjSearchFilter('custitem_item_info_1', null, 'noneof',['@NONE@']));	
		//Searchng Duplicates
		var result= nlapiSearchRecord('item',null,filters,columns);
		for (var i = 0; result != null && i < result.length; i++) {

			var res=  form.getField('custpage_iteminfoone').getSelectOptions(result[i].getValue('custitem_item_info_1',null,'group'), 'is');
			if (res != null) {						
				if (res.length > 0) {
					continue;
				}
			}

			iteminfo1.addSelectOption(result[i].getValue('custitem_item_info_1',null,'group'),result[i].getText('custitem_item_info_1',null,'group'));
		}

		nlapiLogExecution('DEBUG', 'Time Stamp after filling Item Info1',TimeStampinSec());

		var iteminfo2= form.addField('custpage_iteminfotwo', 'select', 'Item Info2');
		iteminfo2.addSelectOption("","");
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_item_info_2',null,'group');			 
		var filters= new Array();	
		filters.push(new nlobjSearchFilter('custitem_item_info_2', null, 'noneof',['@NONE@']));	
		var result= nlapiSearchRecord('item',null,filters,columns);
		for (var i = 0; result != null && i < result.length; i++) {

			var res=  form.getField('custpage_iteminfotwo').getSelectOptions(result[i].getValue('custitem_item_info_2',null,'group'), 'is');
			if (res != null) {						
				if (res.length > 0) {
					continue;
				}
			}

			iteminfo2.addSelectOption(result[i].getValue('custitem_item_info_2',null,'group'),result[i].getText('custitem_item_info_2',null,'group'));
		}

		nlapiLogExecution('DEBUG', 'Time Stamp after filling Item Info2',TimeStampinSec());

		var iteminfo3= form.addField('custpage_iteminfothree', 'select', 'Item Info3');
		iteminfo3.addSelectOption("","");
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_item_info_3',null,'group');			 
		var filters= new Array();		
		filters.push(new nlobjSearchFilter('custitem_item_info_3', null, 'noneof',['@NONE@']));	
		var result= nlapiSearchRecord('item',null,filters,columns);
		for (var i = 0; result != null && i < result.length; i++) {
			var res=  form.getField('custpage_iteminfothree').getSelectOptions(result[i].getValue('custitem_item_info_3',null,'group'), 'is');
			if (res != null) {						
				if (res.length > 0) {
					continue;
				}
			}
			iteminfo3.addSelectOption(result[i].getValue('custitem_item_info_3',null,'group'),result[i].getText('custitem_item_info_3',null,'group'));
		}

		nlapiLogExecution('DEBUG', 'Time Stamp after filling Item Info3',TimeStampinSec());

		var packcode = form.addField('custpage_packcode', 'select', 'Pack Code','customlist_ebiznet_packcode');	

		var uom = form.addField('custpage_uom', 'select', 'UOM','customlist_ebiznet_uom');
		var Company = form.addField('custpage_company', 'select', 'Company','customrecord_ebiznet_company');

		var Destination = form.addField('custpage_dest', 'select', 'Destination','customlist_nswmsdestinationlov');

		Destination.setLayoutType('startrow', 'none');		

		var shippingcarrier = form.addField('custpage_shippingcarrier', 'select', 'Ship Via(Carrier)');
		shippingcarrier.setLayoutType('startrow', 'none');

		var ShipmethodFilter = new Array();
		var vRoleLocation=getRoledBasedLocation();
		nlapiLogExecution('ERROR', 'vRoleLocation',vRoleLocation );
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			ShipmethodFilter.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', vRoleLocation));

		}
		//added filters because of performance
		ShipmethodFilter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [15,25,26]));
		ShipmethodFilter.push(new nlobjSearchFilter('custrecord_do_carrier', null, 'isnotempty'));
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_do_carrier',null,'group');

		var selectshipmd= nlapiSearchRecord('customrecord_ebiznet_ordline',null,ShipmethodFilter,column);
		shippingcarrier.addSelectOption("","");
		for(var i=0;  selectshipmd != null && i<selectshipmd.length;i++)
		{
			nlapiLogExecution('ERROR', selectshipmd[i].getValue('custrecord_do_carrier',null,'group') + ',' + selectshipmd[i].getText('custrecord_do_carrier',null,'group') );
			shippingcarrier.addSelectOption(selectshipmd[i].getValue('custrecord_do_carrier',null,'group'),selectshipmd[i].getText('custrecord_do_carrier',null,'group'));
		}

		/*		var record = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});

		var methodsField = record.getField('shipmethod');
		var methodOptions = methodsField.getSelectOptions(null, null);
		nlapiLogExecution('DEBUG','methodOptions',methodOptions);

		shippingcarrier.addSelectOption("",""); 

		for (var i = 0; methodOptions != null && i < methodOptions.length; i++) {
			//nlapiLogExecution('DEBUG', methodOptions[i].getId() + ',' + methodOptions[i].getText() );
			shippingcarrier.addSelectOption(methodOptions[i].getId(),methodOptions[i].getText());
		}*/	

		nlapiLogExecution('DEBUG', 'Time Stamp after filling Shipmethod',TimeStampinSec());
		//added by shylaja on 091011 to filter the statuses which are not having allow pick at sku status.
		var itemstatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
		var ItemstatusFilter = new Array();
		var Itemstatuscolumns = new Array();

		ItemstatusFilter[0] = new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T');
		Itemstatuscolumns[0] = new nlobjSearchColumn('custrecord_statusdescriptionskustatus');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemstatusFilter, Itemstatuscolumns);

		itemstatus.addSelectOption("","");
		if (searchresults != null) {	
			for (var i = 0; i < Math.min(500, searchresults.length); i++) {

				var res = form.getField('custpage_itemstatus').getSelectOptions(searchresults[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}
				nlapiLogExecution('DEBUG','res',res);
				//nlapiLogExecution('DEBUG','searchresults[i].getId()',searchresults[i].getId());
				itemstatus.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('custrecord_statusdescriptionskustatus'));
				//nlapiLogExecution('DEBUG','itemstatus',itemstatus);
			}
		}
		nlapiLogExecution('DEBUG','itemstatusnew',itemstatus);

		nlapiLogExecution('DEBUG', 'Time Stamp after filling Item Status',TimeStampinSec());

		var country = form.addField('custpage_country', 'select', 'Country');
		var record = nlapiCreateRecord('location', {recordmode: 'dynamic'});		
		//Case# 201410631 starts
		//var addsubrec= record.createSubrecord('mainaddress');

		//var countryField = addsubrec.getField('country');// Case# 201410303
		var countryField = record.getField('country');
		nlapiLogExecution('DEBUG','countryField---',countryField);
		//Case# 201410631 ends
		if(countryField!=null && countryField!='')
		{
			var countryOptions = countryField.getSelectOptions(null, null);
			//nlapiLogExecution('DEBUG','countryOptions',countryOptions);
			//nlapiLogExecution('DEBUG', countryOptions[0].getId() + ',' + countryOptions[0].getText() );
			country.addSelectOption("","");
			for (var i = 0; countryOptions != null && i < countryOptions.length; i++) {
				//nlapiLogExecution('DEBUG', countryOptions[i].getId() + ',' + countryOptions[i].getText() );
				country.addSelectOption(countryOptions[i].getId(),countryOptions[i].getText());
			}
		}
		//case# 20149447 ends
		nlapiLogExecution('DEBUG', 'Time Stamp after filling Country',TimeStampinSec());

		var state = form.addField('custpage_state', 'select', 'Ship State');
		var sofrightterms = form.addField('custpage_sofrieghtterms', 'select', 'Freight Terms','customlist_nswmsfreighttermslov');
		var addr1 = form.addField('custpage_addr1', 'select', 'Ship Address');		
		var City = form.addField('custpage_city', 'select', 'Ship City');
		
		
		var subs = nlapiGetContext().getFeature('subsidiaries');
		nlapiLogExecution('ERROR', 'subs', subs);
		if(subs != null && subs != '' && subs==true)
		{
			var context=nlapiGetContext();
			var vSubsid=context.getSubsidiary();
			var vRoleid=context.getRole();
			nlapiLogExecution('DEBUG', 'vRoleid', vRoleid);
			var vRoleSubsidArray=new Array();
			if(vRoleid !=null && vRoleid !='')
			{
				var vRolefilters=new Array();

				if(vRoleid != null && vRoleid != '')
					vRolefilters.push(new nlobjSearchFilter('internalid', null, 'anyof', vRoleid));
				vRolefilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F')); 
				var vRoleColumns=new Array();
				vRoleColumns.push(new nlobjSearchColumn('name'));
				vRoleColumns.push(new nlobjSearchColumn('subsidiaries'));
				var vRolesearchresults = nlapiSearchRecord('Role', null, vRolefilters, vRoleColumns);
				if(vRolesearchresults !=null && vRolesearchresults !='' && vRolesearchresults.length > 0)
				{
					for(var i=0;i<vRolesearchresults.length;i++)
					{
						var vnRoleSubsid=vRolesearchresults[i].getValue('subsidiaries');
						vRoleSubsidArray.push(vnRoleSubsid);
					}
				}
			}
			nlapiLogExecution('DEBUG', 'vRoleSubsidArray', vRoleSubsidArray);
			var vEmpRoleLocation=context.getLocation();
			nlapiLogExecution('DEBUG', 'vEmpRoleLocation', vEmpRoleLocation);
		
		}

		state.addSelectOption("","");
		addr1.addSelectOption("","");
		City.addSelectOption("","");
		var columns = new Array();
		columns.push(new nlobjSearchColumn('shipstate'));
		columns.push(new nlobjSearchColumn('shipcity'));
		columns.push(new nlobjSearchColumn('shipaddress'));

		var filters= new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
		if(vRoleSubsidArray !=null && vRoleSubsidArray !='')
			filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', vRoleSubsidArray));
		else if(vSubsid != null && vSubsid != '')
		{
			filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', vSubsid));

		}
		var result= nlapiSearchRecord('salesorder',null,filters,columns);
	//	var shipstate=nlapiSearchRecord('customsearch_wms_fulfillmentstate',null,null,null);
		var shipstate=nlapiSearchRecord('customrecord_ebiznet_ordline','customsearch_wms_fulfillmentstate',null,null);
		

		for (var i = 0; shipstate != null && i < shipstate.length; i++) {
			var soresult=shipstate[i];
			var pfcolumns = soresult.getAllColumns();
			var shipstatevalue=soresult.getValue(pfcolumns[0]);			
			var res=  form.getField('custpage_state').getSelectOptions(shipstatevalue, 'is');
			if (res != null) {				
				if (res.length > 0) {
					continue;
				}
			}		
			state.addSelectOption(shipstatevalue, shipstatevalue);
		}

		nlapiLogExecution('DEBUG', 'Time Stamp after filling State',TimeStampinSec());

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];
			var vshipstate=soresult.getValue('shipstate');
			var vshipcity=soresult.getValue('shipcity');
			var vshipaddr=soresult.getValue('shipaddress');
			if (vshipaddr != null && vshipaddr != '' && vshipaddr != 'null')				
			{
				var res1=  form.getField('custpage_addr1').getSelectOptions(vshipaddr, 'is');
				if (res1 != null) {				
					if (res1.length > 0) {
						continue;
					}
				}		
				addr1.addSelectOption(vshipaddr, vshipaddr);
			}
		}

		nlapiLogExecution('DEBUG', 'Time Stamp after filling Address',TimeStampinSec());

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];			
			var vshipcity=soresult.getValue('shipcity');
			if (vshipcity != null && vshipcity != '' && vshipcity != 'null')
			{

				var res1=  form.getField('custpage_city').getSelectOptions(vshipcity, 'is');
				if (res1 != null) {				
					if (res1.length > 0) {
						continue;
					}
				}		

				City .addSelectOption(vshipcity, vshipcity);
			}
		}

		nlapiLogExecution('DEBUG', 'Time Stamp after filling City',TimeStampinSec());
		//added carrier field By suman
		//var carrier = form.addField('custpage_carrier', 'select', 'Carrier','customrecord_ebiznet_carrier');
		//end of carrier field

		//var soShipdate = form.addField('custpage_soshipdate', 'date', 'Ship Date(<=)').setDefaultValue(DateStamp());
		//var soShipdate = form.addField('custpage_soshipdate', 'date', 'Ship Date(<=)').setDefaultValue(TimeZoneDateStamp());// case# 201412665
		var soShipdate = form.addField('custpage_soshipdate', 'date', 'Ship Date(<=)');
		//var soShiptodate = form.addField('custpage_soshiptodate', 'date', 'Ship Todate');



		var soOrderdate = form.addField('custpage_soorderdate', 'date', 'Order Date : From');
		var soOrdertodate = form.addField('custpage_soordertodate', 'date', 'Order Date : To');
		var WMSflag=form.addField('custpage_wmsstatusflag','select','WMS Status Flag','customrecord_wms_status_flag').setDefaultValue(25);

		var location = form.addField('custpage_site', 'select', 'Location');
		var locationFilter = new Array();
		var locationcolumns = new Array();
		locationFilter.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));
		locationFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		locationcolumns[0] = new nlobjSearchColumn('custrecord_ebizlocationid');

		var locsearchresults = nlapiSearchRecord('location', null, locationFilter, locationcolumns);

		location.addSelectOption("","");
		if (locsearchresults != null) {	
			for (var i = 0; i < Math.min(500, locsearchresults.length); i++) {

				var res = form.getField('custpage_site').getSelectOptions(locsearchresults[i].getValue('custrecord_ebizlocationid'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}

				location.addSelectOption(locsearchresults[i].getId(), locsearchresults[i].getValue('custrecord_ebizlocationid'));

			}
		}

		//code added by santosh 11sep12
		var defvalue="1";
		var Rangefrom = form.addField('custpage_linesfrom', 'text', 'Min. # of Lines').setDefaultValue(defvalue);
		var Rangeto = form.addField('custpage_linesto', 'text', 'Max. # of Lines');//.setDefaultValue(defvalue);
		var Orderselection = form.addField('custpage_orderselection', 'select', 'Order Selection');
		Orderselection.addSelectOption('ALL', 'ALL');
		Orderselection.addSelectOption('BO', 'Back Order');
		Orderselection.addSelectOption('RO', 'Regular Order');

		var TransationType = form.addField('custpage_trantype', 'select', 'Transaction Type');
		TransationType.addSelectOption('0', 'ALL');
		TransationType.addSelectOption('1', 'Sales Order');
		TransationType.addSelectOption('2', 'Transfer Order');

		form.addSubmitButton('Display');
		nlapiLogExecution('DEBUG', 'Time Stamp at the end of response',TimeStampinSec());
		response.writePage(form);
	}
	else //this is the POST block
	{
		var vSo = request.getParameter('custpage_qbso');
		if(vSo=="" ||vSo==null)
		{
			vSo=request.getParameter('custpage_qbsotext');
		}
		var vitem = request.getParameter('custpage_item');
		var vcustomer = request.getParameter('custpage_consignee');
		var vordPriority = request.getParameter('custpage_ordpriority');
		var vordtype = request.getParameter('custpage_ordtype');
		var vitemgroup = request.getParameter('custpage_itemgroup');
		var vitemfamily = request.getParameter('custpage_itemfamily');
		var vpackcode = request.getParameter('custpage_packcode');
		var vuom = request.getParameter('custpage_uom');
		var vitemstatus = request.getParameter('custpage_itemstatus');
		var siteminfo1= request.getParameter('custpage_iteminfoone'); 
		var siteminfo2= request.getParameter('custpage_iteminfotwo');
		var siteminfo3= request.getParameter('custpage_iteminfothree');
		var vCompany= request.getParameter('custpage_company');
		var vShippingCarrier= request.getParameter('custpage_shippingcarrier');
		var vShipCountry= request.getParameter('custpage_country');
		var vShipState= request.getParameter('custpage_state');
		var vShipCity= request.getParameter('custpage_city');
		var vShipAddr1= request.getParameter('custpage_addr1');
		var vShipmentNo= request.getParameter('custpage_qbshipmentno');
		var vRouteno= request.getParameter('custpage_qbrouteno');
		//added By Suman
		//var vCarrier= request.getParameter('custpage_carrier');
		var vShipdate= request.getParameter('custpage_soshipdate');
		//var vShiptodate= request.getParameter('custpage_soshiptodate');

		nlapiLogExecution('DEBUG','siteminfo1',siteminfo1);
		nlapiLogExecution('DEBUG','siteminfo2',siteminfo2);
		nlapiLogExecution('DEBUG','siteminfo3',siteminfo3);
		
		var vsiteminfo1 = '';
		var vsiteminfo2 = '';
		var vsiteminfo3 = '';
		if(siteminfo1!=null && siteminfo1!='')
		{
			var vcolumns = new Array();
			vcolumns[0] = new nlobjSearchColumn('custitem_item_info_1',null,'group');			 
			var vfilters= new Array();			
			vfilters.push(new nlobjSearchFilter('custitem_item_info_1', null, 'anyof',siteminfo1));	

			var viteminfooneresult= nlapiSearchRecord('item',null,vfilters,vcolumns);


			if(viteminfooneresult!=null && viteminfooneresult!='')
			{
				vsiteminfo1= viteminfooneresult[0].getText('custitem_item_info_1',null,'group');
			}

		}
		
		if(siteminfo2!=null && siteminfo2!='')
		{
			var vcolumns1 = new Array();
			vcolumns1[0] = new nlobjSearchColumn('custitem_item_info_2',null,'group');			 
			var vfilters1= new Array();			
			vfilters1.push(new nlobjSearchFilter('custitem_item_info_2', null, 'anyof',siteminfo2));	

			var viteminfooneresult1= nlapiSearchRecord('item',null,vfilters1,vcolumns1);


			if(viteminfooneresult1!=null && viteminfooneresult1!='')
			{
				vsiteminfo2= viteminfooneresult[0].getText('custitem_item_info_2',null,'group');
			}
		}
		
		if(siteminfo3!=null && siteminfo3!='')
		{
			var vcolumns2 = new Array();
			vcolumn2s[0] = new nlobjSearchColumn('custitem_item_info_3',null,'group');			 
			var vfilters2= new Array();			
			vfilters2.push(new nlobjSearchFilter('custitem_item_info_3', null, 'anyof',siteminfo3));	

			var viteminfooneresult2= nlapiSearchRecord('item',null,vfilters2,vcolumns2);


			if(viteminfooneresult2!=null && viteminfooneresult2!='')
			{
				vsiteminfo3= viteminfooneresult[0].getText('custitem_item_info_3',null,'group');
			}
		}
		
		
		nlapiLogExecution('DEBUG','vsiteminfo1',vsiteminfo1);
		nlapiLogExecution('DEBUG','vsiteminfo2',vsiteminfo2);
		nlapiLogExecution('DEBUG','vsiteminfo3',vsiteminfo3);
		

		var vFreightTerms= request.getParameter('custpage_sofrieghtterms');
		var vOrderDate= request.getParameter('custpage_soorderdate');
		var vOrdertodate=request.getParameter('custpage_soordertodate');
		var wmsstatusflag=request.getParameter('custpage_wmsstatusflag');
		//code added by santosh on 11Sep12 
		var vLinesFrom= request.getParameter('custpage_linesfrom');
		var vLinesTo=request.getParameter('custpage_linesto');
		//var vBackOrder = request.getParameter('custpage_backorder');
		//	var vNormalOrder = request.getParameter('custpage_normalorder');
		var vOrderSelection=request.getParameter('custpage_orderselection');
		var sono = request.getParameter('custpage_sono');
		var trantype = request.getParameter('custpage_trantype');
		//var SoId=GetSOInternalId(sono);
		var vSite=request.getParameter('custpage_site');
		//end of the code
		nlapiLogExecution('DEBUG','sono',sono);
		nlapiLogExecution('DEBUG','FreightTerms',vFreightTerms);
		nlapiLogExecution('DEBUG','vShipdate',vShipdate);
		nlapiLogExecution('DEBUG','vOrderDate',vOrderDate);
		nlapiLogExecution('DEBUG','wmsstatusflag',wmsstatusflag);
		var WaveQbparams = new Array();		
		if (vSo!=null &&  vSo != "") {
			WaveQbparams["custpage_qbso"] = vSo;
		}

		if (vitem!=null &&  vitem != "") {
			WaveQbparams["custpage_item"] = vitem;
		}
		if (vcustomer!=null && vcustomer != "") {
			WaveQbparams ["custpage_consignee"] = vcustomer;
		}
		if (vordPriority!=null && vordPriority != "") {
			WaveQbparams ["custpage_ordpriority"] = vordPriority;
		}
		if (vordtype!=null && vordtype != "") {
			WaveQbparams ["custpage_ordtype"] = vordtype;
		}
		if (vitemgroup!=null && vitemgroup != "") {
			WaveQbparams ["custpage_itemgroup"] = vitemgroup;
		}
		if (vitemfamily!=null && vitemfamily != "") {
			WaveQbparams ["custpage_itemfamily"] = vitemfamily;
		}
		if (vpackcode!=null && vpackcode != "") {
			WaveQbparams ["custpage_packcode"] = vpackcode;
		}
		if (vuom!=null && vuom != "") {
			WaveQbparams ["custpage_uom"] = vuom;
		}
		if (vitemstatus!=null && vitemstatus != "") {
			WaveQbparams ["custpage_itemstatus"] = vitemstatus;
		}
// case# 201414310
		if (vsiteminfo1!=null && vsiteminfo1 != "") { 
			WaveQbparams ["custpage_siteminfo1"] = vsiteminfo1;
		}
		if (vsiteminfo2!=null && vsiteminfo2 != "") { 
			WaveQbparams ["custpage_siteminfo2"] = vsiteminfo2;
		}
		if (vsiteminfo3!=null && vsiteminfo3 != "") { 
			WaveQbparams ["custpage_siteminfo3"] = vsiteminfo3;
		}
		if (vCompany!=null && vCompany != "") {
			WaveQbparams ["custpage_company"] = vCompany;
		}
		if (vShippingCarrier!=null && vShippingCarrier != "") {
			WaveQbparams ["custpage_shippingcarrier"] = vShippingCarrier;
		}
		if (vShipCountry!=null && vShipCountry != "") {
			WaveQbparams ["custpage_country"] = vShipCountry;
		}
		if (vShipState!=null && vShipState != "") {
			WaveQbparams ["custpage_state"] = vShipState;
		}
		if (vShipCity!=null && vShipCity != "") {
			WaveQbparams ["custpage_city"] = vShipCity;
		}
		if (vShipAddr1!=null && vShipAddr1 != "") {
			WaveQbparams ["custpage_addr1"] = vShipAddr1;
		}  
		if (vShipmentNo!=null && vShipmentNo != "") {
			WaveQbparams ["custpage_shipmentno"] = vShipmentNo;
		}  
		if (vRouteno!=null && vRouteno != "") {
			WaveQbparams ["custpage_routeno"] = vRouteno;
		}  
		//added Carrier By Suman
//		if (vCarrier!=null && vCarrier != "") {
//		WaveQbparams ["custpage_carrier"] = vCarrier;
//		}

		if (vShipdate!=null && vShipdate != "") {
			WaveQbparams ["custpage_soshipdate"] = vShipdate;
		}
//		if (vShiptodate!=null && vShiptodate != "") {
//		WaveQbparams ["custpage_soshiptodate"] = vShiptodate;
//		}

		if (vFreightTerms!=null && vFreightTerms != "") {
			nlapiLogExecution('DEBUG', 'freightterms',vFreightTerms) ;
			WaveQbparams ["custpage_sofrieghtterms"] = vFreightTerms;
		}
		if (vOrderDate!=null && vOrderDate != "") {
			WaveQbparams ["custpage_soorderdate"] = vOrderDate;
		}
		if (vOrdertodate!=null && vOrdertodate != "") {
			WaveQbparams ["custpage_soordertodate"] = vOrdertodate;
		}

		if (wmsstatusflag!=null && wmsstatusflag != "") {
			WaveQbparams ["custpage_wmsstatusflag"] = wmsstatusflag;
		}

		//code added by santosh 
		if (vLinesFrom!=null && vLinesFrom != "") {
			WaveQbparams ["custpage_linesfrom"] = vLinesFrom;
		}
		if (vLinesTo!=null && vLinesTo != "") {
			WaveQbparams ["custpage_linesto"] = vLinesTo;
		}

		if (vOrderSelection!=null && vOrderSelection != "") {
			WaveQbparams ["custpage_orderselection"] = vOrderSelection;
		}
		if (sono!=null && sono != "") {
			WaveQbparams ["custpage_sono"] = sono;
		}

		if (trantype!=null && trantype != "") {
			WaveQbparams ["custpage_trantype"] = trantype;
		}
//		if (vNormalOrder!=null && vNormalOrder != "") {
//		WaveQbparams ["custpage_normalorder"] = vNormalOrder;
//		}

		if (vSite!=null && vSite != "") {
			WaveQbparams ["custpage_site"] = vSite;
		}

		//end of the code
		response.sendRedirect('SUITELET', 'customscript_ebiz_waverelease', 'customdeploy_ebiz_waverelease', false, WaveQbparams );
		//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_7', 'customdeploy_ebiz_hook_sl_7', false, WaveQbparams );

	}
}

function GetSOInternalId(SOText)
{
	nlapiLogExecution('DEBUG','Into GetSOInternalId (Input)',SOText);

	var ActualSoID='';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));

	var columns=new Array();
	columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord('salesorder',null,filter,columns);
	if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualSoID=searchrec[0].getId();
	}

	nlapiLogExecution('DEBUG','Out of GetSOInternalId (Output)',ActualSoID);

	return ActualSoID;
}
