/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_CountryOfOrigin.js,v $
 *     	   $Revision: 1.1.2.4 $
 *     	   $Date: 2014/06/23 06:57:31 $
 *     	   $Author: skavuri $
 *     	   $Name: t_eBN_2014_1_StdBundle_3_126 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CountryOfOrigin.js,v $
 * Revision 1.1.2.4  2014/06/23 06:57:31  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.1.2.3  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.1  2013/06/04 10:54:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *

 *****************************************************************************/

function CheckInCountryOfOrigin(request, response)
{
	if (request.getMethod() == 'GET') 
	{

		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPONo = request.getParameter('custparam_poid');
		var getOptedField = request.getParameter('custparam_option');
		var getItemCube = request.getParameter('custparam_itemcube');
		var trantype= request.getParameter('custparam_trantype');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getItemBaseUomQty=request.getParameter('custparam_baseuomqty');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var getFetchedItemName=request.getParameter('custparam_fetcheditemname');

		var getwhCompany= request.getParameter('custparam_company');

		//New Start

		var trantype = nlapiLookupField('transaction', getPOInternalId, 'recordType');

		var PORec = nlapiLoadRecord(trantype, getPOInternalId);

		// Fetched the count of lines available in the Purchase Order
		var LineItemCount = PORec.getLineItemCount('item');

		// Loop into fetch the PO Line details for the line# passed.



//		for (var i = 1; i <= LineItemCount; i++) 
//		{
//			var lineno = PORec.getLineItemValue('item', 'line', i);
//			if (lineno == getPOLineNo) 
//			{
//				
//							
//
//			}
//
//		}
		
		for (var i = 1; i <= LineItemCount; i++) {
			var lineno = PORec.getLineItemValue('item', 'line', i);

			if (lineno == getPOLineNo) {


				countryoforigin = PORec.getLineItemText('item', 'custcol_nls_country_of_origin', i);

						countryid = PORec.getLineItemValue('item', 'custcol_nls_country_of_origin', i);


								nlapiLogExecution('DEBUG', 'countryid',countryoforigin);
						nlapiLogExecution('DEBUG', 'countryid',countryid);


			}
		}



		//New End


		//start

//		var POtrantypefilters=new Array();
//		POtrantypefilters.push(new nlobjSearchFilter('tranid',null,'is',getPONo));
//		POtrantypefilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

//		var POtrantypecols=new Array();
//		POtrantypecols[0]=new nlobjSearchColumn('custbody_nls_port_of_origin_po');

//		var PORecinternalids=nlapiSearchRecord('purchaseorder',null,POtrantypefilters,POtrantypecols);

//		var countryoforigin='';
//		var couttryid='';
//		if(PORecinternalids!=null && PORecinternalids!='')
//		{
//		countryoforigin=PORecinternalids[0].getText('custbody_nls_port_of_origin_po');
//		countryid=PORecinternalids[0].getValue('custbody_nls_port_of_origin_po');

//		}

		//end

		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_qty'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enterqty').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_checkin_qty' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>Country Of Origin : <label>" + countryoforigin + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<input type='hidden' name='hdncountryoforigin' value=" + countryoforigin + ">";
		html = html + "				<input type='hidden' name='hdncountryid' value=" + countryid + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + "></td>";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnBaseUomQty' value=" + getItemBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";

		html = html + "				<input type='hidden' name='hdnFetchedItemId' value=" + getFetchedItemId + ">";
		html = html + "				<input type='hidden' name='hdngetPOItem' value=" + getPOItem + ">";
		html = html + "				<input type='hidden' name='hdngetPONo' value=" + getPONo + ">";
		html = html + "				<input type='hidden' name='hdngetPOLineNo' value=" + getPOLineNo + ">";
		html = html + "				<input type='hidden' name='hdngetFetchedItemName' value=" + getFetchedItemName + ">";
		html = html + "				<input type='hidden' name='hdngetwhCompany' value=" + getwhCompany + ">";
		html = html + "				<input type='hidden' name='hdngetPOInternalId' value=" + getPOInternalId + ">";


		html = html + "			<tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entervalue' id='entervalue' type='text' value='"+ countryoforigin +"'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entervalue').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');
		var getOptedField = request.getParameter('hdnOptedField');
		var getActualBeginTime = request.getParameter('hdnActualBeginTime'); 
		var country=request.getParameter('entervalue');

	var cntryfilter=new Array();
		cntryfilter[0]=new nlobjSearchFilter('name', null, 'is', country);
		
		var cntrycolumn = new Array();
		cntrycolumn[0] = new nlobjSearchColumn('custrecord_nls_country_of_origin_code');
		
		var cntrysearchresult = nlapiSearchRecord('customrecord_nls_country_of_origin', null, cntryfilter, cntrycolumn);
		countrycode='';
		if (cntrysearchresult != null && cntrysearchresult.length > 0) {
			countrycode= cntrysearchresult[0].getId();
			
		}

		var POarray = new Array();

		POarray["custparam_itemcube"] = request.getParameter('hdnItemCube');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		POarray["custparam_baseuomqty"] = request.getParameter('hdnBaseUomQty');
		POarray["custparam_poitem"] = request.getParameter('hdngetPOItem');
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
		POarray["custparam_option"] = getOptedField;
		POarray["custparam_actualbegintime"] = getActualBeginTime;
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_countryoforigin"] = country;
		POarray["custparam_countryid"] =countrycode;
		POarray["custparam_error"] = 'INVALID Country';
		POarray["custparam_screenno"] = 'cntry';
		POarray["custparam_fetcheditemname"] = request.getParameter('hdngetFetchedItemName');
		POarray["custparam_company"] = request.getParameter('hdngetwhCompany');
		POarray["custparam_pointernalid"] = request.getParameter('hdngetPOInternalId');
		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') {
			nlapiLogExecution('DEBUG', 'F7', 'F7');
			response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false, POarray);
		}
		else
		{

			nlapiLogExecution('DEBUG', 'elseloop', 'elseloop');
			if (country !=null && country != "")
			{
				nlapiLogExecution('DEBUG', 'checkin', 'checkin');
				response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false, POarray);
			}
			else{
				nlapiLogExecution('DEBUG', 'errorpage', 'errorpage');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);

			}

		}


		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}//end of else loop.

}