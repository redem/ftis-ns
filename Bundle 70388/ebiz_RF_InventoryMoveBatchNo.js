/***************************************************************************
����������������������eBizNET
�����������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMoveBatchNo.js,v $
*� $Revision: 1.3.4.6.4.2.4.5.2.1 $
*� $Date: 2014/11/05 10:31:40 $
*� $Author: sponnaganti $
*� $Name: t_eBN_2014_2_StdBundle_0_158 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_InventoryMoveBatchNo.js,v $
*� Revision 1.3.4.6.4.2.4.5.2.1  2014/11/05 10:31:40  sponnaganti
*� Case# 201410748
*� True Fab SB Issue fixed
*�
*� Revision 1.3.4.6.4.2.4.5  2014/06/13 09:56:46  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.3.4.6.4.2.4.4  2014/06/06 07:34:07  skavuri
*� Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
*�
*� Revision 1.3.4.6.4.2.4.3  2014/05/30 00:34:21  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.3.4.6.4.2.4.2  2013/09/02 15:49:20  skreddy
*� Case# 20124181
*� standard bundle issue fix
*�
*� Revision 1.3.4.6.4.2.4.1  2013/04/17 16:02:35  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.3.4.6.4.2  2012/09/26 12:35:00  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.3.4.6.4.1  2012/09/21 15:01:30  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.3.4.6  2012/04/20 11:14:17  schepuri
*� CASE201112/CR201113/LOG201121
*� changing the Label of Batch #  field to Lot#
*�
*� Revision 1.3.4.5  2012/03/16 14:08:54  spendyala
*� CASE201112/CR201113/LOG201121
*� Disable-button functionality is been added.
*�
*� Revision 1.3.4.4  2012/03/14 14:28:49  gkalla
*� CASE201112/CR201113/LOG201121
*� Not to allow user to scan Batch no
*�
*� Revision 1.3.4.3  2012/02/23 00:27:34  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� supress lp in Invtmove and lp merge based on fifodate
*�
*� Revision 1.3.4.2  2012/02/21 13:24:39  schepuri
*� CASE201112/CR201113/LOG201121
*� function Key Script code merged
*�
*� Revision 1.8  2012/02/21 11:47:40  schepuri
*� CASE201112/CR201113/LOG201121
*� Added FunctionkeyScript
*�
 */
function InventoryMoveBatchConfirm(request, response)
{
	var getBinLocationId = request.getParameter('custparam_imbinlocationid');
	var getBinLocation = request.getParameter('custparam_imbinlocationname');
	var getItemId = request.getParameter('custparam_imitemid');
	var getItem = request.getParameter('custparam_imitem');
	var getTotQuantity = request.getParameter('custparam_totquantity');
	var getAvailQuantity = request.getParameter('custparam_availquantity');    
	var getUOMId = request.getParameter('custparam_uomid');
	var getUOM = request.getParameter('custparam_uom');
	var getStatus = request.getParameter('custparam_status');
	var getStatusId = request.getParameter('custparam_statusid');
	var getLOTId = request.getParameter('custparam_lotid');
	var getLOTNo = request.getParameter('custparam_lot');  
	//var getMoveQuantity = request.getParameter('custparam_moveqty');
	var getLPId = request.getParameter('custparam_lpid');
	var getLP = request.getParameter('custparam_lp');  
	var getItemType = request.getParameter('custparam_itemtype');
	var getActualBeginDate = request.getParameter('custparam_actualbegindate');
	var getActualBeginTime = request.getParameter('custparam_actualbegintime');
	var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
	var locationId=request.getParameter('custparam_locationId');//
	var compId=request.getParameter('custparam_compid');//
	var getInvtRecID=request.getParameter('custparam_invtrecid');
	var ItemDesc = request.getParameter('custparam_itemdesc');
	var fifodate=request.getParameter('custparam_fifodate');//
	nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId'));
	nlapiLogExecution('ERROR', 'getItemType ', getItemType);
	nlapiLogExecution('ERROR', 'getInvtRecID ',getInvtRecID);
//case 20124181 start
	var	tempBinLocationId=request.getParameter('custparam_hdntmplocation');
	var tempItemId=request.getParameter('custparam_hdntempitem');
	var tempLP=request.getParameter('custparam_hdntemplp');
	
	nlapiLogExecution('ERROR', 'tempBinLocationId ',tempBinLocationId);
	nlapiLogExecution('ERROR', 'tempItemId ',tempItemId);
	nlapiLogExecution('ERROR', 'tempLP ',tempLP);
//end

	if (request.getMethod() == 'GET') 
	{	
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "INVENTARIO DE MUDANZA";
			st1 = "LOTE #:";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";
			
		}
		else
		{
			st0 = "INVENTORY MOVE";
			st1 = "LOT # :";
			st2 = "SEND";
			st3 = "PREV";
			
	
		}
		
		
		
		var functionkeyHtml=getFunctionkeyScript('_rf_inventorymove_quantity'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		   //Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends   
		//html = html + " document.getElementById('enterbatchno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventorymove_quantity' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + "<label>" + getLOTNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		/*		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER BATCH# :";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "			<tr>";
		/*html = html + "				<td align = 'left'><input name='enterbatchno' type='text'/>";*/
		html = html + "				<td align = 'left'><input name='enterbatchno'  type='hidden' value=" + getLOTNo + ">";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "					<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "					<input type='hidden' name='hdnLPId' value=" + getLPId + ">";        
		html = html + "					<input type='hidden' name='hdnLOTNo' value=" + getLOTNo + ">";
		html = html + "					<input type='hidden' name='hdnLOTNoId' value=" + getLOTId + ">";
		html = html + "					<input type='hidden' name='hdnStatus' value='" + getStatus + "'>";
		html = html + "					<input type='hidden' name='hdnStatusId' value=" + getStatusId + ">";
		html = html + "					<input type='hidden' name='hdnUOM' value=" + getUOM + ">";
		html = html + "					<input type='hidden' name='hdnUOMId' value=" + getUOMId + ">";
		html = html + "					<input type='hidden' name='hdnTotQty' value=" + getTotQuantity + ">";
		html = html + "					<input type='hidden' name='hdnAvailQty' value=" + getAvailQuantity + ">";      
		html = html + "					<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnlocationId' value=" + locationId + ">";
		html = html + "				    <input type='hidden' name='hdncompId' value=" + compId + ">";
		html = html + "					<input type='hidden' name='hdnInvtRecID' value=" + getInvtRecID + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + ItemDesc + "'>";
		html = html + "				    <input type='hidden' name='hdnfifodate' value='" + fifodate + "'>";
		html = html + "				    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				    <input type='hidden' name='hdntmplocation' value=" + tempBinLocationId + ">";
		html = html + "				    <input type='hidden' name='hdntemplp' value='" + tempLP + "'>";
		html = html + "				    <input type='hidden' name='hdntempitem' value='" + tempItemId + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSearch' id='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSearch').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var getBatchNo = request.getParameter('enterbatchno');
		nlapiLogExecution('ERROR', 'New Batch No', getBatchNo);       

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var Itemtype= request.getParameter('hdnitemType');
		var IMarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		IMarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', IMarray["custparam_language"]);
    	
		
		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			
			st4 = "NO V&#193;LIDA DE LOS LOTES";
			st5 = "BATCH NO ENTRAR";
		}
		else
		{
			
			st4 = "INVALID BATCH NO";
			st5 = "ENTER BATCH NO";
			
		}

		IMarray["custparam_imbinlocationid"] = request.getParameter('hdnBinLocationId');
		IMarray["custparam_imbinlocationname"] = request.getParameter('hdnBinLocation');
		IMarray["custparam_imitemid"] = request.getParameter('hdnItemId');
		IMarray["custparam_imitem"] = request.getParameter('hdnItem');
		IMarray["custparam_totquantity"] = request.getParameter('hdnTotQty');
		IMarray["custparam_availquantity"] = request.getParameter('hdnAvailQty');
		//CIarray["custparam_moveqty"] = request.getParameter('hdnMoveQty');
		IMarray["custparam_status"] = request.getParameter('hdnStatus');
		IMarray["custparam_statusid"] = request.getParameter('hdnStatusId');
		IMarray["custparam_uom"] = request.getParameter('hdnUOM');
		IMarray["custparam_uomid"] = request.getParameter('hdnUOMId');		
		IMarray["custparam_lot"] = request.getParameter('hdnLOTNo');
		IMarray["custparam_lotid"] = request.getParameter('hdnLOTNoId');
		IMarray["custparam_lp"] = request.getParameter('hdnLP');
		IMarray["custparam_lpid"] = request.getParameter('hdnLPId');
		IMarray["custparam_itemtype"]=Itemtype;
		IMarray["custparam_invtrecid"] = request.getParameter('hdnInvtRecID');

		IMarray["custparam_actualbegindate"] = getActualBeginDate;
		IMarray["custparam_locationId"] = request.getParameter('hdnlocationId');
		nlapiLogExecution('ERROR', 'IMarray["custparam_locationId"] ', IMarray["custparam_locationId"]);
		IMarray["custparam_screenno"] = '20';
		IMarray["custparam_itemdesc"] = request.getParameter('hdnItemdesc');
		IMarray["custparam_compid"] = request.getParameter('hdncompId');
		IMarray["custparam_fifodate"] = request.getParameter('hdnfifodate');
		nlapiLogExecution('ERROR', 'Compid', request.getParameter('hdncompId'));

		var TimeArray = new Array();
		IMarray["custparam_actualbegintime"] = getActualBeginTime;
		IMarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
		nlapiLogExecution('ERROR', 'Itemtype', IMarray["custparam_itemtype"]);
//case 20124181 start
		IMarray["custparam_hdntmplocation"] = request.getParameter('hdntmplocation');
		IMarray["custparam_hdntemplp"] = request.getParameter('hdntemplp');
		IMarray["custparam_hdntempitem"] = request.getParameter('hdntempitem');
		//end
		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_cont', 'customdeploy_rf_inventory_move_cont_di', false, IMarray);
		}
		else 
		{
			try 
			{            	

				if (getBatchNo != null && getBatchNo != '') 
				{
					if(getLOTNo != getBatchNo)
					{
						IMarray["custparam_error"] = st4;   	          		    	                  	
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Invalied Batch No', getBatchNo);
						return;
					}
					else
					{
						IMarray["getBatchNo"] = getBatchNo;
						nlapiLogExecution('ERROR', 'Batch No', getBatchNo);
					}
				}
				else 
				{
					IMarray["custparam_error"] = st5;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Batch is not entered', getBatchNo);
					return;
				}

				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_qty', 'customdeploy_rf_inventory_move_qty_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Navigating to LP', 'Success');
			} 
			catch (e) 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}
	}
}
