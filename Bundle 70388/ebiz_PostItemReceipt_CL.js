/***************************************************************************
�eBizNET Solutions Inc 
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Client/Attic/ebiz_PostItemReceipt_CL.js,v $
 *� $Revision: 1.1.2.2.4.2 $
 *� $Date: 2015/10/07 13:16:33 $
 *� $Author: schepuri $
 *� $Name: t_WMS_2015_2_StdBundle_1_24 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_PostItemReceipt_CL.js,v $
 *� Revision 1.1.2.2.4.2  2015/10/07 13:16:33  schepuri
 *� case# 201414894
 *�
 *� Revision 1.1.2.2.4.1  2015/10/06 13:40:30  schepuri
 *� case# 201414876
 *�
 *� Revision 1.1.2.2  2014/06/18 15:42:19  sponnaganti
 *� case# 20148632
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.1.2.1  2013/07/25 08:58:20  rmukkera
 *� landed cost cr new file
 *�
 *� Revision 1.18.2.20.4.6.2.14  2013/06/25 16:00:47  skreddy
 *� CASE# 20123113
 *� Serial Number count equal to Order Qty
 *�
 *� Revision 1.18.2.20.4.6.2.13  2013/06/25 14:50:29  mbpragada
 *� Case# 20123176
 *� System throwing invalid LP range in GUI check-in.
 *�
 *� Revision 1.18.2.20.4.6.2.12  2013/05/20 15:22:23  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.18.2.20.4.6.2.11  2013/05/10 13:57:11  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.18.2.20.4.6.2.10  2013/05/08 15:23:01  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle Issue Fixes
 *�
 *� Revision 1.18.2.20.4.6.2.9  2013/05/08 15:13:34  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.18.2.20.4.6.2.8  2013/05/01 12:44:21  rmukkera
 *� Autobreakdownrelated changes were done
 *�
 *� Revision 1.18.2.20.4.6.2.7  2013/04/17 15:54:21  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue fixes
 *�
 *� Revision 1.18.2.20.4.6.2.6  2013/04/09 08:48:30  grao
 *� CASE201112/CR201113/LOG201121
 *� Issue fixed for set alert for serial numbers
 *�
 *� Revision 1.18.2.20.4.6.2.5  2013/04/03 20:40:48  kavitha
 *� CASE201112/CR201113/LOG2012392
 *� TSG Issue fixes
 *�
 *� Revision 1.18.2.20.4.6.2.4  2013/03/19 12:21:59  schepuri
 *� CASE201112/CR201113/LOG201121
 *� change url path
 *�
 *� Revision 1.18.2.20.4.6.2.3  2013/03/19 11:46:48  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production and UAT issue fixes.
 *�
 *� Revision 1.18.2.20.4.6.2.2  2013/03/05 13:35:46  rmukkera
 *� Merging of lexjet Bundle files to Standard bundle
 *�
 *� Revision 1.18.2.20.4.6.2.1  2013/02/26 12:56:04  skreddy
 *� CASE201112/CR201113/LOG201121
 *� merged boombah changes
 *�
 *� Revision 1.18.2.20.4.6  2013/02/22 06:42:18  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue related to accepting invalid lp Prefix
 *�
 *� Revision 1.18.2.20.4.5  2013/02/07 08:49:54  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Date Validation
 *�
 *� Revision 1.18.2.20.4.4  2012/12/14 07:42:52  spendyala
 *� CASE201112/CR201113/LOG201121
 *� moved from 2012.2 branch
 *�
 *� Revision 1.18.2.20.4.3  2012/11/01 14:55:22  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.18.2.20.4.2  2012/10/03 11:29:53  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Auto Gen Lot No
 *�
 *� Revision 1.18.2.20.4.1  2012/09/26 21:41:39  svanama
 *� CASE201112/CR201113/LOG201121
 *� item label generation code added
 *�
 *� Revision 1.18.2.20  2012/09/14 07:20:47  schepuri
 *� CASE201112/CR201113/LOG201121
 *� checking for null
 *�
 *� Revision 1.18.2.19  2012/08/29 16:19:23  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Changed Prompt message.
 *�
 *� Revision 1.18.2.18  2012/08/23 23:16:10  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Change of sentence which we are prompting at the time of UPC code update.
 *�
 *� Revision 1.18.2.17  2012/08/22 14:48:04  schepuri
 *� no message
 *�
 *� Revision 1.18.2.16  2012/08/21 22:41:33  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Prompting user weather Change in UPC code is updated to item master or not
 *�
 *� Revision 1.18.2.15  2012/08/03 13:20:07  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Validating recommended quantity
 *�
 *� Revision 1.18.2.14  2012/06/19 07:13:59  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to creating same batch# for multiple Sku's.
 *�
 *� Revision 1.18.2.13  2012/06/18 12:59:34  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� validation on location
 *�
 *� Revision 1.18.2.12  2012/05/29 12:45:24  schepuri
 *� CASE201112/CR201113/LOG201121
 *� issue fix for vaidating qty
 *�
 *� Revision 1.18.2.11  2012/05/21 13:04:16  schepuri
 *� CASE201112/CR201113/LOG201121
 *� validation on checkin once checkin already completed
 *�
 *� Revision 1.18.2.10  2012/05/07 13:43:44  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Showing a popup window to scan serialitem# in putaway confirmation screen.
 *�
 *� Revision 1.18.2.9  2012/04/26 07:14:43  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Removing unwanted alert Messages.
 *�
 *� Revision 1.18.2.8  2012/04/25 15:41:15  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Validating entered Batch Item issue is resolved.
 *�
 *� Revision 1.18.2.7  2012/04/20 12:26:36  schepuri
 *� CASE201112/CR201113/LOG201121
 *� changing the Label of Batch #  field to Lot#
 *�
 *� Revision 1.18.2.6  2012/04/06 17:11:24  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Unwanted alert Msg are removed.
 *�
 *� Revision 1.18.2.5  2012/03/30 05:02:25  vrgurujala
 *� t_NSWMS_LOG201121_91
 *�
 *� Revision 1.23  2012/03/30 05:00:16  vrgurujala
 *� CASE201112/CR201113/LOG201121
 *�
 *� Revision 1.22  2012/02/08 12:03:08  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Code Merge
 *�
 *� Revision 1.21  2012/01/30 07:05:05  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *�
 *� Revision 1.20  2012/01/23 10:18:50  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Added SearchColumn to fetch the record with highest UOMLevel from item dimensions.
 *�
 *
 ****************************************************************************/

/**
 * @author LN
 *@version
 *@date
 *@Description: This is a Client Script acting as a Library function to Check-In Screen line
 level LP validation
 */


function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function ValidateLine(type, name)
{	

	var poid = nlapiGetFieldValue('custpage_po');
	
	if(poid!=null && poid!='')
	{

		// case# 201414876
		var vfilters = new Array();
		vfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
		vfilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
		vfilters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
		vfilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
		vfilters.push(new nlobjSearchFilter('tranid','custrecord_ebiz_order_no', 'is', poid));

		var vcolumns=new Array();
		vcolumns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		vcolumns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
		vcolumns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
		vcolumns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
		vcolumns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');

		var openputawaytaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,vfilters,vcolumns);
		

		if(openputawaytaskSearchResults!=null && openputawaytaskSearchResults!='' && openputawaytaskSearchResults.length>0)
		{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2,6]));
		filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isempty'));
		filters.push(new nlobjSearchFilter('tranid','custrecord_ebiz_order_no', 'is', poid));

		var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);


		if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
		{
			/*if(tempVar=='open putaways')
		{*/
			var result=confirm('There are Open Putaway Tasks against this PO, Do you still want to continue?');
			//alert('result'+result);
			if (result==true)
			{

				//nlapiSetFieldValue('custpage_tempnew','yes');
				nlapiSetFieldValue('custpage_tempnew',"yes");
				//alert('custpage_tempnew'+nlapiGetFieldValue('custpage_tempnew'));
				return true;
				
			}
			else
			{
				
				//nlapiSetFieldValue('custpage_tempnew','no');
				nlapiSetFieldValue('custpage_tempnew',"no");
				return true;
			}


			//}
		}
	}
	else
	{
			alert('There are no putaway records for this PO/TO/RMA');// case 201414894
			return false;
	}

}
else
{
	return true;
}

return true;

}
//upto here
