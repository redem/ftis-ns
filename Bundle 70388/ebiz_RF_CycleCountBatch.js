/***************************************************************************
������������������������
���������������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountBatch.js,v $
*� $Revision: 1.6.2.4.4.7.2.4.4.1 $
*� $Date: 2015/11/16 15:35:00 $
*� $Author: rmukkera $
*� $Name: t_WMS_2015_2_StdBundle_1_152 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_CycleCountBatch.js,v $
*� Revision 1.6.2.4.4.7.2.4.4.1  2015/11/16 15:35:00  rmukkera
*� case # 201415115
*�
*� Revision 1.6.2.4.4.7.2.4  2014/06/13 10:15:41  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.6.2.4.4.7.2.3  2014/05/30 00:34:19  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.6.2.4.4.7.2.2  2013/04/17 16:02:37  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.6.2.4.4.7.2.1  2013/03/05 13:35:38  rmukkera
*� Merging of lexjet Bundle files to Standard bundle
*�
*� Revision 1.6.2.4.4.7  2013/02/20 22:30:57  kavitha
*� CASE201112/CR201113/LOG201121
*� Cycle count process - Issue fixes
*�
*� Revision 1.6.2.4.4.6  2013/02/18 06:29:17  skreddy
*� CASE201112/CR201113/LOG201121
*�  RF Lot auto generating FIFO enhancement
*�
*� Revision 1.6.2.4.4.5  2013/02/14 01:35:04  kavitha
*� CASE201112/CR201113/LOG201121
*� Cycle count process - Issue fixes
*�
*� Revision 1.6.2.4.4.4  2013/02/01 07:58:15  rrpulicherla
*� CASE201112/CR201113/LOG201121
*� Cyclecountlatestfixed
*�
*� Revision 1.6.2.3  2012/03/21 15:07:59  spendyala
*� CASE201112/CR201113/LOG201121
*� Driving the user to create batch no .
*�
*� Revision 1.6.2.2  2012/03/16 14:08:54  spendyala
*� CASE201112/CR201113/LOG201121
*� Disable-button functionality is been added.
*�
*� Revision 1.6.2.1  2012/02/21 13:22:23  schepuri
*� CASE201112/CR201113/LOG201121
*� function Key Script code merged
*�
*� Revision 1.6  2011/12/08 07:32:17  spendyala
*� CASE201112/CR201113/LOG201121
*� solved issue related to the actula item  passed by the user.
*�
*
****************************************************************************/

/**
 * @author LN
 *@version
 *@date
 */
function CycleCountBatch(request, response){
	if (request.getMethod() == 'GET') {

		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var getRecordId = request.getParameter('custparam_recordid');
		nlapiLogExecution('ERROR','getRecprdId',getRecordId);
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName =  request.getParameter('custparam_begin_location_name');
		var getPackCode =  request.getParameter('custparam_packcode');
		var getExpectedQuantity =  request.getParameter('custparam_expqty');
		var getExpLPNo = request.getParameter('custparam_lpno');
		var getActLPNo = request.getParameter('custparam_actlp');
		var getExpItem = request.getParameter('custparam_expitem');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');
		var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', getRecordId);

		var getActualItem = request.getParameter('custparam_actitem');
		var itemInternalid = request.getParameter('custparam_expiteminternalid');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var getActualPackCode = request.getParameter('custparam_actpackcode');		
		var planitem=request.getParameter('custparam_planitem');
		var batchNo="";
		if(CCRec!=null)
		{
			batchNo=CCRec.getFieldValue('custrecord_expcycleabatch_no');
			nlapiLogExecution('ERROR','batchNo',batchNo);
		}
		if(batchNo == null)
		{
			batchNo = "";
		}
		
		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountbatch'); 
		var html = "<html><head><title>Cycle Count Batch</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterbatch').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountbatch' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>EXP BATCH/LOT#:</td></tr>";
		html = html + "				<tr><td align = 'left'> <label>" + batchNo + "</label></td></tr>";
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";		
		html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";	
		html = html + "				<input type='hidden' name='hdniteminternalid' value=" + itemInternalid + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN ACTUAL BATCH/LOT#:";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbatch' id='enterbatch' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONT <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false' style='width:40;height:40;'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7' style='width:40;height:40;'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterbatch').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else 
	{
		try {
			var CCarray = new Array();
			var optedEvent = request.getParameter('cmdPrevious');
			var getActbatch = request.getParameter('enterbatch');

			CCarray["custparam_error"] = 'INVALID LOT#';
			CCarray["custparam_screenno"] = '35';
			CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
			CCarray["custparam_planno"] = request.getParameter('custparam_planno');
			CCarray["custparam_begin_location_id"] = request.getParameter('custparam_begin_location_id');
			CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
			CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
			CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
			CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
			CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
			CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
			CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
			CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');

			CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
			CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
			CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
			CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
			CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
			CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');
			CCarray["custparam_actbatch"]='';

			if (optedEvent == 'F7') 
			{
				nlapiLogExecution('DEBUG', 'Cycle Count SKU F7 Pressed');
				//response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_lp', 'customdeploy_rf_cyclecount_lp_di', false, CCarray);
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_packcode', 'customdeploy_rf_cyclecount_packcode_di', false, CCarray);
			}
			else 
			{
				if (getActbatch  != '') 
				{
					var actitem=request.getParameter('custparam_actitem');
					var rec=nlapiLookupField('item', actitem, ['itemid']);
					var actitemText=rec.itemid;
					nlapiLogExecution('ERROR','ACTITEMTEXT',actitemText);
					if(IsValidBatch(getActbatch,actitemText))
					{
						CCarray["custparam_actbatch"] = getActbatch;
						nlapiLogExecution('DEBUG', 'Actual Batch', getActbatch);

						//response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_packcode', 'customdeploy_rf_cyclecount_packcode_di', false, CCarray);
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
					}
					else
					{

						if(actitem!=null && actitem!= '')
						{
							nlapiLogExecution('ERROR','actitem',actitem);
							var poItemFields = ['custitem_ebiz_item_shelf_life','custitem_ebiz_item_cap_expiry_date','custitem_ebiz_item_cap_fifo_date'];
							var rec = nlapiLookupField('item', actitem, poItemFields);
							var itemShelflife = rec.custitem_ebiz_item_shelf_life;
							var CaptureExpirydate= rec.custitem_ebiz_item_cap_expiry_date;
							var CaptureFifodate= rec.custitem_ebiz_item_cap_fifo_date;

							nlapiLogExecution('ERROR','CaptureExpirydate',CaptureExpirydate);
							nlapiLogExecution('ERROR','CaptureExpirydate',CaptureFifodate);
							nlapiLogExecution('ERROR','itemShelflife',itemShelflife);
							CCarray["custparam_actbatch"] = getActbatch;
							CCarray["custparam_shelflife"] = itemShelflife;
							CCarray["custparam_captureexpirydate"] = CaptureExpirydate;
							CCarray["custparam_capturefifodate"] = CaptureFifodate;
							//CCarray["custparam_BatchId"] = batchid;

							var d = new Date();
							var vExpiryDate='';
							if(itemShelflife !=null && itemShelflife!='')
							{

								d.setDate(d.getDate()+parseInt(itemShelflife));
								vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
							}
							else
							{
								vExpiryDate='01/01/2099';										     
							}
							nlapiLogExecution('ERROR', 'vExpiryDate',vExpiryDate);

							if(CaptureExpirydate =='T' ||(CaptureExpirydate =='T' && CaptureFifodate =='T'))
							{				
								response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_exp_date', 'customdeploy_rf_cyclecount_exp_date_di', false, CCarray);
							}
							else
								if(CaptureExpirydate !='T' && CaptureFifodate=='T' )
								{

									CCarray["custparam_fifodate"]='';
									CCarray["custparam_mfgdate"]='';
									CCarray["custparam_bestbeforedate"]='';
									CCarray["custparam_lastdate"]='';
									CCarray["custparam_fifocode"]='';																			
									CCarray["custparam_expdate"]= vExpiryDate;

									response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_fifo_date', 'customdeploy_rf_cyclecount_fifo_date_di', false, CCarray);

								}
								else
									if(CaptureExpirydate =='F' && CaptureFifodate =='F' )
									{

										CCarray["custparam_fifodate"]=DateStamp();
										CCarray["custparam_mfgdate"]='';
										CCarray["custparam_bestbeforedate"]='';
										CCarray["custparam_lastdate"]='';
										CCarray["custparam_fifocode"]='';																					
										CCarray["custparam_expdate"]= vExpiryDate;
										var rec=CreateBatchEntryRec(CCarray);				
										response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
									}

						}	

						//CCarray["custparam_actbatch"] = getActbatch;
						//response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_exp_date', 'customdeploy_rf_cyclecount_exp_date_di', false, CCarray);
					}
					/*{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						nlapiLogExecution('DEBUG', 'INVALID LOT#');						
					}*/
				}
				else 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					nlapiLogExecution('DEBUG', 'LOT# not scanned');
				}
			}
		}
		catch(e)
		{
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			nlapiLogExecution('DEBUG', 'Catch: Cycle Count LOT not found');
		}
	}
}

/*
 * This function validates whether the given batch# exist in eBizNET or not.Returns true or false.
 */
function IsValidBatch(actbatch,actitem)
{
	nlapiLogExecution('ERROR', 'Into IsValidBatch');
	nlapiLogExecution('ERROR', 'Batch',actbatch);
	nlapiLogExecution('ERROR', 'Item',actitem);
	var IsValidBatch=true;
	var columns = new Array();
	var filters = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');

	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', actbatch));
	filters.push(new nlobjSearchFilter('itemid','custrecord_ebizsku', 'is', actitem));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
	if(searchresults != null && searchresults != ''&& searchresults.length>0)
		IsValidBatch=true;
	else
		IsValidBatch=false;
	
	nlapiLogExecution('ERROR', 'IsValidBatch',IsValidBatch);
	
	nlapiLogExecution('ERROR', 'Out of IsValidBatch');

	return IsValidBatch;

}





/**
 * @param CCarray
 */
function CreateBatchEntryRec(CCarray)
{
	try {
		var site="";
		nlapiLogExecution('ERROR', 'in');

		nlapiLogExecution('ERROR', 'BATCH NOT FOUND');
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

		customrecord.setFieldValue('name', CCarray["custparam_actbatch"]);
		customrecord.setFieldValue('custrecord_ebizlotbatch', CCarray["custparam_actbatch"]);
		customrecord.setFieldValue('custrecord_ebizmfgdate', CCarray["custparam_mfgdate"]);
		customrecord.setFieldValue('custrecord_ebizbestbeforedate', CCarray["custparam_bestbeforedate"]);
		customrecord.setFieldValue('custrecord_ebizexpirydate', CCarray["custparam_expdate"]);
		customrecord.setFieldValue('custrecord_ebizfifodate', CCarray["custparam_fifodate"]);
		customrecord.setFieldValue('custrecord_ebizfifocode', CCarray["custparam_fifocode"]);
		customrecord.setFieldValue('custrecord_ebizlastavldate', CCarray["custparam_lastdate"]);
		nlapiLogExecution('ERROR', '1');
		
		//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
		customrecord.setFieldValue('custrecord_ebizsku', CCarray["custparam_actitem"]);
		customrecord.setFieldValue('custrecord_ebizpackcode',CCarray["custparam_packcode"]);


		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', CCarray["custparam_planno"]);

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_cyclesite_id');

		nlapiLogExecution('ERROR', 'Before Search Results Id', 'getSearchResultsId');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, column);
		if(searchresults)
			site=searchresults[0].getValue('custrecord_cyclesite_id');
		if(site!=null&&site!="")
			//customrecord.setFieldValue('custrecord_ebizsitebatch',site);
		var rec = nlapiSubmitRecord(customrecord, false, true);
		nlapiLogExecution('ERROR', 'rec',rec);

	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into BATCH ENTRY Record',e);
	}
}
