/***************************************************************************
�������������������������eBizNET
�������������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_BuildCarton_CART.js,v $
*� $Revision: 1.1.2.1.4.3.4.5 $
*� $Date: 2014/06/13 08:33:20 $
*� $Author: skavuri $
*� $Name: t_NSWMS_2014_1_3_125 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_BuildCarton_CART.js,v $
*� Revision 1.1.2.1.4.3.4.5  2014/06/13 08:33:20  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.1.2.1.4.3.4.4  2014/06/06 06:17:13  skavuri
*� Case# 20148749 (Refresh Functionality ) SB Issue Fixed
*�
*� Revision 1.1.2.1.4.3.4.3  2014/05/30 00:26:46  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.1.2.1.4.3.4.2  2013/06/11 14:30:40  schepuri
*� Error Code Change ERROR to DEBUG
*�
*� Revision 1.1.2.1.4.3.4.1  2013/04/17 16:04:01  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.1.2.1.4.3  2012/09/27 10:53:53  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting multiple language with given Spanish terms
*�
*� Revision 1.1.2.1.4.2  2012/09/26 12:42:29  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.1.2.1.4.1  2012/09/24 10:07:04  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi Lnaguage
*�
*� Revision 1.1.2.1  2012/06/26 06:25:48  spendyala
*� CASE201112/CR201113/LOG201121
*� New Script for Build Carton.
*�
*
****************************************************************************/

function BuildCarton_CART(request,response)
{
	if (request.getMethod() == 'GET') 
	{
		var vPO = request.getParameter('custparam_poid');
		nlapiLogExecution('DEBUG', 'vPO', vPO);
		
		var vItem = request.getParameter('custparam_item');
		nlapiLogExecution('DEBUG', 'vItem', vItem);
		
		var vQty = request.getParameter('custparam_quantity');
		nlapiLogExecution('DEBUG', 'vQty', vQty);
		
		var vInternalId = request.getParameter('custparam_recordid');
		nlapiLogExecution('DEBUG', 'vInternalId', vInternalId);
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "ORDEN DE COMPRA#";
			st2 = "ART&#205;CULO";
			st3 = "CANTIDAD";
			st4 = "INGRESAR / ESCANEAR N&#218;MERO DE PEDIDO";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
			
		}
		else
		{
			st0 = "";
			st1 = "PO#";
			st2 = "ITEM";
			st3 = "QTY";
			st4 = "ENTER/SCAN CART#";
			st5 = "SEND";
			st6 = "PREV";

		}
			
		var functionkeyHtml=getFunctionkeyScript('_rf_buildcarton_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entercart').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_buildcarton_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + vPO + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " : <label>" + vItem + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 +"  : <label>" + vQty + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr><td>";
		html = html + "				<input type='hidden' name='hdnpo' value=" + vPO + ">";
		html = html + "				<input type='hidden' name='hdnitem' value=" + vItem + ">";
		html = html + "				<input type='hidden' name='hdnqty' value=" + vQty + ">";
		html = html + "				<input type='hidden' name='hdninternalid' value=" + vInternalId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " </td></tr>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercart' id='entercart' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercart').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{

		var POarray=new Array();
		
		
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
    	
		
		var st7;
		if( getLanguage == 'es_ES')
		{
			st7 = "INGRESE CARRITO#";
			
		}
		else
		{

			st7 = "ENTER CART#";
			
		}

		var vCart=request.getParameter('entercart');
		nlapiLogExecution('DEBUG','vCart',vCart);
		
		var vPO=request.getParameter('hdnpo');
		nlapiLogExecution('DEBUG','vPO else',vPO);
		var vItem=request.getParameter('hdnitem');
		var vqty=request.getParameter('hdnqty');
		var RecId=request.getParameter('hdninternalid');
	
		
		POarray["custparam_poid"] =  vPO;
		POarray["custparam_item"] =  vItem;
		POarray["custparam_quantity"] =  vqty;
		POarray["custparam_recordid"] =  RecId;
		POarray["custparam_screenno"] = 'BuildCartonCart';
		
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 

		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_buildcarton_lp', 'customdeploy_rf_buildcarton_lp_di', false, POarray);
		}
		else 
		{
			try
			{        
				if (vCart != '' && vCart != null) 
				{
					POarray["custparam_cartlpno"] = vCart;
					lpErrorMessage= MastLPExists(vCart);//check for manual cart lp exists
					if(lpErrorMessage=="")
					{
						var res=UpdateOpenTask(vCart,RecId);
						response.sendRedirect('SUITELET', 'customscript_rf_buildcarton_cnf', 'customdeploy_rf_buildcarton_cnf_di', false, POarray);
					}
					else
					{
						POarray["custparam_error"] = lpErrorMessage;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
				}
				else //To go to Cart LP Confirmation
				{
					POarray["custparam_error"] = st7;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}	 
			}
			catch(exp)
			{
				nlapiLogExecution('DEBUG','Exception in main Else',exp);
			}
		}
	}
}


/**
 * To check manually generated Cart LP# exists in master LP or not
 * 
 * @param manualShipLP   
 * @returns message with proper error message
 */
function MastLPExists(manualCartLP)
{
	var message="";
	var filters =new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', manualCartLP));
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_cart_closeflag', null, 'is', 'T'));
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_cart_closeflag'));
	
	// To get the data from Master LP based on selection criteria
	searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	
	if(searchresults!= null && searchresults.length>0)
	{
		if(searchresults[0].getValue('custrecord_ebiz_cart_closeflag')=='T')
			{
			message="Cart LP# already closed, Please enter another Cart LP#";
			nlapiLogExecution('DEBUG',"message ",message );
			
			}
		return message="";
	}
	else
	{
		if(ebiznet_LPRange_CL_withLPType(manualCartLP,'2','4'))// for User Defined  with Cart LPType
		{
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', manualCartLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', manualCartLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
			return message;
		}
		else
		{
			message="Cart LP# is outof range";
			return message;
		}
	}
}


function UpdateOpenTask(vCart,RecId)
{
	try
	{
		nlapiLogExecution('DEBUG','vCart',vCart);
		var id=nlapiSubmitField('customrecord_ebiznet_trn_opentask',RecId,'custrecord_transport_lp',vCart);
		nlapiLogExecution('DEBUG','Update Opentask Record',id);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in UpdateOpenTask',exp);
	}
}