/***************************************************************************
 eBizNET SOLUTIONS 
 ****************************************************************************/
/*******************************************************************************
 * ***************************************************************************
 * 
 * $Source:
 * /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_RF_WO_PickingClusterNo.js,v $
 * $Revision: 1.1.4.3 $ $Date: 2015/04/24 13:19:07 $ $Author: schepuri $ $Name: t_eBN_2015_1_StdBundle_1_62 $
 * 
 * eBizNET version and checksum stamp. Do not remove. $eBiznet_VER:
 * .............. $eBizNET_SUM: ..... PRAMETERS
 * 
 * 
 * DESCRIPTION
 * 
 * Default Data for Interfaces
 * 
 * NOTES AND WARNINGS
 * 
 * INITATED FROM
 * 
 * 
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PickingClusterNo.js,v $
 * Revision 1.1.4.3  2015/04/24 13:19:07  schepuri
 * case# 201412475
 *
 * Revision 1.1.4.2  2015/03/02 14:46:40  snimmakayala
 * 201410541
 *
 * Revision 1.1.2.1  2015/02/13 14:34:38  sponnaganti
 * Jawbone Cluster Picking CR Changes
 * CR#  CR14US0323
 *
 * Revision 1.3.2.9.4.4.2.5  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.3.2.9.4.4.2.4  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.2.9.4.4.2.3  2013/04/10 15:52:46  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to Invalid Cluster no
 *
 * Revision 1.3.2.9.4.4.2.2  2013/04/03 02:02:02  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.3.2.9.4.4.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.3.2.9.4.4  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.3.2.9.4.3  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.2.9.4.2  2012/10/04 10:28:44  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.3.2.9.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.3.2.9  2012/09/04 20:46:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Incorporate SKIP option in Cluster Picking.
 *
 * Revision 1.3.2.8  2012/08/09 07:30:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Driving user to new screen i.e cluster summary report.
 *
 * Revision 1.3.2.7  2012/08/07 12:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Missing Parameter to query string.
 *
 * Revision 1.3.2.6  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.3.2.5  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.2.4  2012/03/12 08:05:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.8  2012/03/12 07:56:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.7  2012/02/28 01:22:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.6  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.5  2012/02/23 13:08:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.4  2012/02/17 13:29:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 ******************************************************************************/
function WOPickingClusterNo(request, response)
{
	
	var context = nlapiGetContext();
	var sessionobj = null;
	sessionobj = context.getSessionObject('session');
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	var user=context.getUser();	
	context.setSessionObject('session', null);
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	
	if (request.getMethod() == 'GET') 
	{
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4;
		
		if( getLanguage == 'es_ES')
		{
			st1 = "INGRESAR / ESCANEARL N&#218;MERO DE BLOQUE : ";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";
			st4 = "N&#218;MERO DE ZONA "; 
					
		}
		else
		{
			st1 = "ENTER / SCAN CLUSTER # :";
			st2 = "SEND";
			st3 = "PREV";
			st4 = "ZONE NO "; 
			
		}
		
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('enterclusterno').focus();";      
		
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st1;//ENTER/SCAN CLUSTER NO 
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterclusterno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+ st4;//ZONE NO;		 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterzoneno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		
		html = html + "			<tr>";
	//	html = html + "				<td align = 'left'>"+ st2 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
	        html = html + "				<td align = 'left'>"+ st2 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st3 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		
		var getLanguage = request.getParameter('hdngetLanguage');
		
		var st4,st10;
		
		if( getLanguage == 'es_ES')
		{
			
			st4 = "GRUPO DE VALIDEZ #";
			
			
		}
		else
		{
			st4 = "INVALID CLUSTER #";
			st10 = "INVALID ZONE #";
		}
		
		var getClusterNo = request.getParameter('enterclusterno');

		nlapiLogExecution('DEBUG', 'Cluter # ', getClusterNo);

		var getZoneNo = request.getParameter('enterzoneno');
		
		nlapiLogExecution('DEBUG', 'getZoneNo # ', getZoneNo);
		var SOarray = new Array();
		SOarray["custparam_skipid"] = getLanguage;

		SOarray["custparam_error"] = st4;//'INVALID CLUSTER #';
		SOarray["custparam_screenno"] = 'CLWO1';
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');

		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di', false, SOarray);
		}
		else 
		{
			//if((getZoneNo == null || getZoneNo == '') && (getClusterNo == null || getClusterNo == '' ))
			if(getClusterNo == null || getClusterNo == '')// case# 201412475
			{
				//SOarray["custparam_error"] = "PLEASE ENTER CLUSTER#/ZONE NO";//'INVALID ZONE';
				SOarray["custparam_error"] = "PLEASE ENTER CLUSTER#";//cluster number is mandatory along with zone
				SOarray["custparam_screenno"] = 'CLWO1';
				nlapiLogExecution('DEBUG', 'Error: ', 'Zone &cluster not enterd');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}
			var SOFilters = new Array();
			var vZoneId;
			if(getZoneNo != null && getZoneNo != '')
			{	
				vZoneId= vGetgetZoneId(getZoneNo);
				if(vZoneId=='ERROR')
				{
					SOarray["custparam_error"] = st10;//'INVALID ZONE';
					SOarray["custparam_screenno"] = 'CLWO1';
					nlapiLogExecution('DEBUG', 'Error: ', 'Zone not found');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;	
				}
				else
				{
					nlapiLogExecution('DEBUG', 'getZoneNo inside if', getZoneNo);
					nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);
					SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
				}
			}
			if (getClusterNo != null && getClusterNo != '' ) 
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', getClusterNo));
			}

				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
				SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
				SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
				SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
				var SOColumns = new Array();
				SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
				SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
				SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
				SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
				SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
				SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
				SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
				SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');
				SOColumns[11] = new nlobjSearchColumn('custrecord_wms_location');
				SOColumns[12] = new nlobjSearchColumn('custrecord_comp_id');
				SOColumns[13] = new nlobjSearchColumn('name');
				SOColumns[14] = new nlobjSearchColumn('custrecord_container');
				SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				SOColumns[16] = new nlobjSearchColumn('custrecord_container_lp_no');
				SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
				SOColumns[18] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
				SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');
				

				SOColumns[1].setSort();	
				SOColumns[2].setSort();		//	Location Pick Sequence
				//SOColumns[3].setSort();		//	SKU
				//SOColumns[16].setSort();	//	Container LP	

				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

				if (SOSearchResults != null && SOSearchResults.length > 0) {
					
					var vorderno;
					var HoldStatus;
					var vordername;
					//var HoldStatusFlag = 'T';
					var OrdeNoArray = new Array();
					for(var f=0; f < SOSearchResults.length; f++)
					{
						var currrow=[SOSearchResults[f].getValue('custrecord_ebiz_order_no'),SOSearchResults[f].getText('custrecord_ebiz_order_no')];
						OrdeNoArray.push(currrow);

					}
					if(OrdeNoArray != null && OrdeNoArray != '')
					{
						for(var u=0;u<OrdeNoArray.length;u++)
						{
							nlapiLogExecution('DEBUG', 'OrdeNoArray[u][0] before tst',OrdeNoArray[u][0] );
							nlapiLogExecution('DEBUG', 'OrdeNoArray[u][1] before tst',OrdeNoArray[u][1] );
							var trantype="";
							if(OrdeNoArray[u][0]!=null && OrdeNoArray[u][0]!="" && OrdeNoArray[u][0]!='null')
								trantype = nlapiLookupField('transaction', OrdeNoArray[u][0], 'recordType');
							nlapiLogExecution('DEBUG', 'trantype',trantype);
							
							if(trantype !='workorder')
							{
								HoldStatus = fnCreateFo(OrdeNoArray[u][0]);
								nlapiLogExecution('DEBUG', 'HoldStatus before tst',HoldStatus );
							}

							//HoldStatus = 'F';
							nlapiLogExecution('DEBUG', 'OrdeNoArray tst',OrdeNoArray );
							nlapiLogExecution('DEBUG', 'HoldStatus after tst',HoldStatus );

							if(HoldStatus == 'F')
							{
								nlapiLogExecution('DEBUG', 'OrdeNoArray[u][0] after tst',OrdeNoArray[u][0] );
								nlapiLogExecution('DEBUG', 'OrdeNoArray[u][1] after tst',OrdeNoArray[u][1] );
								vordername = OrdeNoArray[u][1];
								break;
							}

						}
					}

					
					if(SOSearchResults.length <= parseFloat(vSkipId))
					{
						vSkipId=0;
					}

					var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];

					SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no');
					SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
					SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
					SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
					SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
					SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
					SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
					SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
					SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
					SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
					SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no');
					SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
					SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
					SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
					SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
					SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
					SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
					SOarray["name"] =  SOSearchResult.getValue('name');
					SOarray["custparam_picktype"] =  'CL';
					SOarray["custparam_nextlocation"] = '';
					SOarray["custparam_nextiteminternalid"] = '';
					SOarray["custparam_nextitem"] = '';
					//code added by santosh on 7Aug2012
					SOarray["custparam_type"] ="Cluster";
					//end of the code on 7Aug2012
					nlapiLogExecution('DEBUG', 'Begin Location Name',SOarray["custparam_beginlocationname"]);
					nlapiLogExecution('DEBUG', 'Redirecting to Location Screen');
					
					if(getZoneNo != null && getZoneNo != '')
						SOarray["custparam_venterzone"] = vZoneId;
					
					nlapiLogExecution('DEBUG', 'zoneId',SOarray["custparam_venterzone"]);
					if(HoldStatus == "F")
					{
						SOarray["custparam_error"] = "Either SO is Closed/Cancelled or Payment is HOLD/Days Over Due/Credit Limit Exceed for  " + vordername;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wocluster_summary', 'customdeploy_ebiz_rf_wocluster_summary', false, SOarray);
						return;
					}
				//	response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
					//code added by santosh on 7Aug2012
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wocluster_summary', 'customdeploy_ebiz_rf_wocluster_summary', false, SOarray);
					//end of the code on 7Aug2012
				}
				else {
					nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

				}
			
			/*else
			{
				nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

			}*/
		}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}


function vGetgetZoneId(getZoneNo)
{
	if(getZoneNo != null && getZoneNo != '')
	{
		nlapiLogExecution('DEBUG', 'Into Zone Validate', 'Into Zone Validate');
		var ZoneFilters = new Array();
		var ZoneColumns = new Array();
		nlapiLogExecution('DEBUG', 'getZoneNo', getZoneNo);
		ZoneFilters.push(new nlobjSearchFilter('custrecord_putzoneid', null, 'is', getZoneNo));
		ZoneFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		var ZoneSearchResults = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters, ZoneColumns);
		if(ZoneSearchResults != null && ZoneSearchResults != '' && ZoneSearchResults.length>0)
		{
			nlapiLogExecution('DEBUG', 'ZoneSearchResults[0].getId()', ZoneSearchResults[0].getId());
			return ZoneSearchResults[0].getId();
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Into Zone Validate Else', 'Into Zone Validate Else');
			var ZoneFilters1 = new Array();
			var ZoneColumns1 = new Array();
			ZoneFilters1.push(new nlobjSearchFilter('name', null, 'is', getZoneNo));
			ZoneFilters1.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			var ZoneSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters1, ZoneColumns1);
			if(ZoneSearchResults1 != null && ZoneSearchResults1 != '' && ZoneSearchResults1.length>0)
			{
				return ZoneSearchResults1[0].getId();
			}
			else
			{
				return 'ERROR';
				/*SOarray["custparam_error"] = st10;//'INVALID ZONE';
				SOarray["custparam_screenno"] = '12';
				nlapiLogExecution('DEBUG', 'Error: ', 'Zone not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;*/
			}
		}
	} 
}