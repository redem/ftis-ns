/***************************************************************************
 eBizNET Solutions               
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Common/Suitelet/GeneralFunctions.js,v $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: GeneralFunctions.js,v $
 * Revision 1.103.2.69.4.25.2.197.2.8  2015/12/02 15:07:47  grao
 * 2015.2 Issue Fixes 201415845
 *
 * Revision 1.103.2.69.4.25.2.197.2.7  2015/11/30 16:14:39  gkalla
 * case# 201415795
 * Not able to process location generation for large orders.
 *
 * Revision 1.103.2.69.4.25.2.197.2.6  2015/11/18 09:59:11  svanama
 * Case # 201415738
 *
 * Revision 1.103.2.69.4.25.2.197.2.5  2015/11/17 16:16:14  svanama
 * Case # 201415732. Ship From Address Population
 *
 * Revision 1.103.2.69.4.25.2.197.2.4  2015/11/14 17:59:18  svanama
 * Case # 201415613
 *
 * Revision 1.103.2.69.4.25.2.197.2.3  2015/11/06 14:12:48  snimmakayala
 * 201415040
 *
 * Revision 1.103.2.69.4.25.2.197.2.2  2015/10/06 10:12:26  gkalla
 * Case# 201414824
 * Added status of pick report same as wave status report. fetching status from scheduler status custom record.
 *
 * Revision 1.103.2.69.4.25.2.197.2.1  2015/09/22 23:31:54  snimmakayala
 * 201414495
 *
 * Revision 1.103.2.69.4.25.2.197  2015/08/21 11:01:42  nneelam
 * case# 201414102
 *
 * Revision 1.103.2.69.4.25.2.196  2015/08/05 15:12:02  grao
 * 2015.2   issue fixes 201413101   and  201413427
 *
 * Revision 1.103.2.69.4.25.2.195  2015/07/30 22:18:08  skreddy
 * Case# 201413656
 * 2015.2  issue fix
 *
 * Revision 1.103.2.69.4.25.2.194  2015/07/28 15:29:13  skreddy
 * Case# 201413628
 * Sighwarehouse SB issue fix
 *
 * Revision 1.103.2.69.4.25.2.193  2015/07/07 13:46:30  schepuri
 * case# 201413380
 *
 * Revision 1.103.2.69.4.25.2.192  2015/07/02 15:22:27  grao
 * SW issue fixes  201413127
 *
 * Revision 1.103.2.69.4.25.2.191  2015/06/29 15:38:16  skreddy
 * Case# 201413188
 * JB Prod issue fix
 *
 * Revision 1.103.2.69.4.25.2.190  2015/06/04 06:29:02  schepuri
 * case# 201412665
 *
 * Revision 1.103.2.69.4.25.2.189  2015/05/25 15:39:23  grao
 * SB issue fixes  201412585
 *
 * Revision 1.103.2.69.4.25.2.188  2015/05/21 13:12:01  schepuri
 * case# 201412858
 *
 * Revision 1.103.2.69.4.25.2.187  2015/05/13 20:27:52  rrpulicherla
 * Case#201412277
 *
 * Revision 1.103.2.69.4.25.2.186  2015/05/12 14:16:11  schepuri
 * case# 201412726
 *
 * Revision 1.103.2.69.4.25.2.185  2015/05/08 15:49:21  nneelam
 * case# 201412644
 *
 * Revision 1.103.2.69.4.25.2.184  2015/05/05 13:26:14  schepuri
 * case# 201412598
 *
 * Revision 1.103.2.69.4.25.2.183  2015/05/04 15:42:35  grao
 * SB issue fixes 201412607 and  Hilmoton 201412519
 *
 * Revision 1.103.2.69.4.25.2.182  2015/05/04 14:08:24  nneelam
 * case# 201412610
 *
 * Revision 1.103.2.69.4.25.2.179  2015/04/27 15:26:53  skreddy
 * Case# 201412463
 * Dynacraft Prod  issue fix
 *
 * Revision 1.103.2.69.4.25.2.178  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.103.2.69.4.25.2.177  2015/04/08 14:47:43  skreddy
 * Case# 201412267
 * JB Prod  issue fix
 *
 * Revision 1.103.2.69.4.25.2.176  2015/04/02 13:31:59  schepuri
 * case# 201412220,201412219
 *
 * Revision 1.103.2.69.4.25.2.175  2015/03/18 15:38:46  sponnaganti
 * Case# 201412069
 * LP SB issue fix
 *
 * Revision 1.103.2.69.4.25.2.174  2015/03/16 10:18:46  skreddy
 * Case# 201412014
 * Lonely Planet SB  issue fix
 *
 * Revision 1.103.2.69.4.25.2.173  2015/03/02 13:53:54  schepuri
 * Issue fix # 201411655
 *
 * Revision 1.103.2.69.4.25.2.172  2015/02/25 13:44:26  schepuri
 * issue fix 201411630,201411627
 *
 * Revision 1.103.2.69.4.25.2.171  2014/12/26 14:22:37  skreddy
 * Case# 201411180
 * CT Prod Issue Fixed
 *
 * Revision 1.103.2.69.4.25.2.170  2014/12/06 02:14:32  gkalla
 * Case# 201411227
 * Dynacraft inventory missing issue,  Intializing the variable
 *
 * Revision 1.103.2.69.4.25.2.169  2014/11/28 15:34:55  skavuri
 * Case# 201411044 Std bundle issue fixed
 *
 * Revision 1.103.2.69.4.25.2.168  2014/11/13 05:26:28  vmandala
 * case#  201411046 Stdbundle issue fixed
 *
 * Revision 1.103.2.69.4.25.2.167  2014/11/05 15:39:27  skavuri
 * Case# 201410949 Std bundle issue fixed
 *
 * Revision 1.103.2.69.4.25.2.166  2014/11/05 09:29:41  sponnaganti
 * Case# 201410688
 * CT SB Issue fixed
 *
 * Revision 1.103.2.69.4.25.2.164  2014/10/29 13:52:47  vmandala
 * Case# 201410770 Stdbundle issue fixed
 *
 * Revision 1.103.2.69.4.25.2.163  2014/10/20 14:36:22  vmandala
 * Case# 201410770 Stdbundle issue fixed
 *
 * Revision 1.103.2.69.4.25.2.162  2014/09/26 16:19:14  sponnaganti
 * Case# 201410548
 * DC Dental SB CR Expected Bin Size
 *
 * Revision 1.103.2.69.4.25.2.161  2014/09/17 15:59:13  sponnaganti
 * Case# 201410406
 * Stnd Bundle Issue fix
 *
 * Revision 1.103.2.69.4.25.2.160  2014/09/16 14:51:42  skreddy
 * case # 201410408/ 201410321
 * JB prod issue fix and TPP SB issue fix
 *
 * Revision 1.103.2.69.4.25.2.159  2014/09/11 15:41:15  skavuri
 * Case# 201410315 Std Bundle issue fixed
 *
 * Revision 1.103.2.69.4.25.2.158  2014/09/10 16:21:59  skavuri
 * Case# 201410279 Std Bundle issue fixed
 *
 * Revision 1.103.2.69.4.25.2.157  2014/09/03 07:38:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201410213
 *
 * Revision 1.103.2.69.4.25.2.156  2014/08/18 15:18:21  skavuri
 * Case# 20149997 , 201410022 Std Bundle Issues Fixed
 *
 * Revision 1.103.2.69.4.25.2.155  2014/08/15 15:38:01  snimmakayala
 * Case: 2014997
 * JAWBONE WO LOCATION GENERATION FIXES
 *
 * Revision 1.103.2.69.4.25.2.154  2014/08/14 09:59:22  sponnaganti
 * case# 20149845
 * stnd bundle issue fix
 *
 * Revision 1.103.2.69.4.25.2.153  2014/08/07 15:31:47  sponnaganti
 * case# 20149865
 * Stnd Bundle issue fix
 *
 * Revision 1.103.2.69.4.25.2.152  2014/07/28 16:23:04  sponnaganti
 * Case# 20149733 20149729
 * Stnd Bundle Issue fix
 *
 * Revision 1.103.2.69.4.25.2.151  2014/07/25 13:40:21  skreddy
 * case # 20149396
 * jawbone SB issue fix
 *
 * Revision 1.103.2.69.4.25.2.150  2014/07/24 12:53:47  snimmakayala
 * no message
 *
 * Revision 1.103.2.69.4.25.2.149  2014/07/23 07:08:09  snimmakayala
 * Case: 20149505
 * FIFO merge changes
 *
 * Revision 1.103.2.69.4.25.2.148  2014/07/22 16:04:09  sponnaganti
 * Case# 20149564
 * Compatibility Issue fix
 *
 * Revision 1.103.2.69.4.25.2.147  2014/07/17 15:24:42  sponnaganti
 * Case# 20148733
 * Stnd Bundle Issue fix
 *
 * Revision 1.103.2.69.4.25.2.146  2014/07/14 15:44:14  sponnaganti
 * Case# 20149380 20148733 20148734 20148550
 * Compatibility and Stnd Bundle Issue fix
 *
 * Revision 1.103.2.69.4.25.2.145  2014/07/14 06:08:53  skreddy
 * case # 20149403
 * Dealmed  issue fix
 *
 * Revision 1.103.2.69.4.25.2.144  2014/07/11 15:47:52  sponnaganti
 * Case# 20149397 20149399
 * Compatibility Issue fix
 *
 * Revision 1.103.2.69.4.25.2.143  2014/07/11 14:41:41  skavuri
 * Case# 20149376 Compatibility Issue Fixed
 *
 * Revision 1.103.2.69.4.25.2.142  2014/07/10 15:25:45  skavuri
 * Case# 20149271, 20149078 Compatibility Issue Fixed
 *
 * Revision 1.103.2.69.4.25.2.141  2014/07/10 15:14:22  skavuri
 * Case# 20149353 Compatibility Issue Fixed
 *
 * Revision 1.103.2.69.4.25.2.140  2014/07/10 07:13:27  skavuri
 * Case# 20149268 Compatibility Issue Fixed
 *
 * Revision 1.103.2.69.4.25.2.139  2014/07/09 10:56:48  snimmakayala
 * Case: 20148073
 * Carousal integration
 *
 * Revision 1.103.2.69.4.25.2.138  2014/07/04 23:03:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149230
 *
 * Revision 1.103.2.69.4.25.2.137  2014/06/25 14:37:24  rmukkera
 * Case # 20149077
 *
 * Revision 1.103.2.69.4.25.2.136  2014/06/23 15:19:21  snimmakayala
 * 20148940
 *
 * Revision 1.103.2.69.4.25.2.135  2014/06/19 08:11:17  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148982
 *
 * Revision 1.103.2.69.4.25.2.134  2014/06/13 15:07:31  grao
 * Case#: 20148314
 * Standard bundle issue fixes
 *
 * Revision 1.103.2.69.4.25.2.133  2014/06/06 12:18:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.103.2.69.4.25.2.132  2014/06/05 15:33:44  nneelam
 * case#  20148727
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.103.2.69.4.25.2.131  2014/06/04 15:05:16  sponnaganti
 * Case# 20148711 20148710
 * Stnd Bundle Issue fix
 *
 * Revision 1.103.2.69.4.25.2.130  2014/05/29 15:35:24  sponnaganti
 * case# 20148056
 * Stnd Bundle Issue fix
 *
 * Revision 1.103.2.69.4.25.2.129  2014/05/27 14:55:23  grao
 * Case#: 20148418  Standard  issue fixes
 *
 * Revision 1.103.2.69.4.25.2.128  2014/05/27 07:36:59  snimmakayala
 * Case#: 20148135
 * Schedule Script Status
 *
 * Revision 1.103.2.69.4.25.2.127  2014/05/26 15:03:08  skreddy
 * case # 20148479/20148476
 * Sonic SB issue fix
 *
 * Revision 1.103.2.69.4.25.2.126  2014/05/23 15:14:33  sponnaganti
 * case# 20148232
 * Stnd Bundle Issue fix
 *
 * Revision 1.103.2.69.4.25.2.125  2014/05/19 15:40:28  skavuri
 * Case # 20148402 issue fixed
 *
 * Revision 1.103.2.69.4.25.2.124  2014/05/19 06:26:39  skreddy
 * case # 20148384
 * boombah prod issue fix
 *
 * Revision 1.103.2.69.4.25.2.123  2014/05/09 16:03:30  skavuri
 * Case # 20148341 SB Issue Fixed
 *
 * Revision 1.103.2.69.4.25.2.122  2014/05/01 06:52:11  rmukkera
 * Case # 20148033,20148043
 *
 * Revision 1.103.2.69.4.25.2.121  2014/04/25 09:56:50  grao
 * Case# 20148168
 * SB iSSUES FIXES�
 *
 * Revision 1.103.2.69.4.25.2.120  2014/04/24 13:17:16  grao
 * Case#: 20148142 Sonic Sb issue fixes
 *
 * Revision 1.103.2.69.4.25.2.119  2014/04/23 15:49:27  skavuri
 * Case# 20148128 issue fixed
 *
 * Revision 1.103.2.69.4.25.2.118  2014/04/23 15:09:08  rmukkera
 * Case # 20148040
 *
 * Revision 1.103.2.69.4.25.2.117  2014/04/21 16:03:25  skavuri
 * Case # 20148102 SB Issue fixed
 *
 * Revision 1.103.2.69.4.25.2.116  2014/04/16 15:32:34  skavuri
 * Case # 20147951 SB issue Fixed
 *
 * Revision 1.103.2.69.4.25.2.115  2014/04/16 06:39:45  skreddy
 * case # 20147985
 * Dealmed sb  issue fix
 *
 * Revision 1.103.2.69.4.25.2.114  2014/04/15 13:09:12  snimmakayala
 * *** empty log message ***
 *
 * Revision 1.103.2.69.4.25.2.113  2014/04/10 17:15:47  skavuri
 * cASE # 20144206 ISSUE FIXED
 *
 * Revision 1.103.2.69.4.25.2.112  2014/04/08 15:50:37  nneelam
 * case#  20141019,  20141270�
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.103.2.69.4.25.2.111  2014/04/07 15:41:29  nneelam
 * case#  20140554,20141019
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.103.2.69.4.25.2.110  2014/04/01 06:35:38  skreddy
 * case 20127772
 * LL prod  issue fix
 *
 * Revision 1.103.2.69.4.25.2.109  2014/03/24 15:07:51  grao
 * Case# 20127804 related issue fixes in Sonic  electrox issue fixes
 *
 * Revision 1.103.2.69.4.25.2.108  2014/03/24 14:38:43  rmukkera
 * Case # 20127751�
 *
 * Revision 1.103.2.69.4.25.2.107  2014/03/20 09:12:34  grao
 * Case# 20127771 related issue fixes in Dealmed  issue fixes
 *
 * Revision 1.103.2.69.4.25.2.106  2014/03/19 07:07:05  gkalla
 * case#201218452
 * TPP Mixed cluster fix
 *
 * Revision 1.103.2.69.4.25.2.105  2014/03/12 06:34:55  skreddy
 * case 20127650 & 20127605
 * Deal med  and NLS SB issue fixs
 *
 * Revision 1.103.2.69.4.25.2.104  2014/03/10 16:03:58  skavuri
 * Case# 20127590 issue fixed
 *
 * Revision 1.103.2.69.4.25.2.103  2014/03/07 10:41:39  rmukkera
 * no message
 *
 * Revision 1.103.2.69.4.25.2.102  2014/02/26 15:21:08  skavuri
 * Case 20127379 working fine
 *
 * Revision 1.103.2.69.4.25.2.101  2014/02/24 15:45:03  sponnaganti
 * case# 20127203
 * (Standard Bundle Issue fixed)
 *
 * Revision 1.103.2.69.4.25.2.100  2014/02/21 15:19:28  rmukkera
 * Case# 20127203
 *
 * Revision 1.103.2.69.4.25.2.99  2014/02/19 23:50:20  grao
 * Updatting total no'of packages and packgeno values in Ship manifest record
 *
 * Revision 1.103.2.69.4.25.2.98  2014/02/19 13:15:08  sponnaganti
 * case# 20126998
 * Standard Bundle Issue (Code added from RYONET SB Acc)
 *
 * Revision 1.103.2.69.4.25.2.97  2014/02/18 12:17:55  snimmakayala
 * Case# : 201218481
 *
 * Revision 1.103.2.69.4.25.2.96  2014/02/14 06:27:01  grao
 * Case# 20127161 related issue fixes in Dealmed  issue fixes
 *
 * Revision 1.103.2.69.4.25.2.95  2014/02/13 15:34:01  grao
 * Case# 20127111 related issue fixes in Standard bundle issue fixes
 *
 * Revision 1.103.2.69.4.25.2.94  2014/02/12 10:23:38  skavuri
 * Case # 20127147 working
 *
 * Revision 1.103.2.69.4.25.2.93  2014/02/12 06:33:27  skavuri
 * Case # 20127118 (Item status is working and it redirects to another page in rf cart checkin)
 *
 * Revision 1.103.2.69.4.25.2.92  2014/02/11 15:45:28  skavuri
 * CAse # 20127118 (now system redirect to next page , not shoeing invalid item status)
 *
 * Revision 1.103.2.69.4.25.2.91  2014/02/11 15:29:56  skreddy
 * case 20127122
 * Standard Bundle issue fix
 *
 * Revision 1.103.2.69.4.25.2.90  2014/02/10 11:17:56  grao
 * Case# 20127110 related issue fixes in Standard bundle issue fixes
 *
 * Revision 1.103.2.69.4.25.2.89  2014/02/06 14:54:50  rmukkera
 * Case # 20127077
 *
 * Revision 1.103.2.69.4.25.2.88  2014/02/05 12:29:12  mbpragada
 * Case# 20123445
 * BOL gen issue fixes
 *
 * Revision 1.103.2.69.4.25.2.87  2014/01/29 06:23:35  skreddy
 * case :20126945
 * PMM Prod issue fix
 *
 * Revision 1.103.2.69.4.25.2.86  2014/01/22 08:51:46  schepuri
 * 20126892, 20126893
 * standard bundle issue fix
 *
 * Revision 1.103.2.69.4.25.2.85  2014/01/21 06:04:20  schepuri
 * 20126892,20126893
 * standard bundle issue fix
 *
 * Revision 1.103.2.69.4.25.2.84  2014/01/20 13:52:50  schepuri
 * 20126855
 * standard bundle issue fix
 *
 * Revision 1.103.2.69.4.25.2.83  2013/12/23 16:40:50  gkalla
 * case#20126404
 * Standard bundle issue
 *
 * Revision 1.103.2.69.4.25.2.82  2013/12/13 15:02:41  rmukkera
 * Case #  20126232
 *
 * Revision 1.103.2.69.4.25.2.81  2013/12/12 07:28:00  nneelam
 * Case# 20125983
 * std bundle issue fix..
 *
 * Revision 1.103.2.69.4.25.2.80  2013/12/05 06:59:20  snimmakayala
 * Case# : 20125973
 * MHP UAT Fixes.
 *
 * Revision 1.103.2.69.4.25.2.79  2013/12/05 06:39:38  snimmakayala
 * Case# : 20125973
 * MHP UAT Fixes.
 *
 * Revision 1.103.2.69.4.25.2.78  2013/12/04 16:16:38  skreddy
 * Case# 20126071
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.103.2.69.4.25.2.77  2013/11/25 15:18:58  grao
 * Case# 20125874 related issue fixes in SB 2014.1
 *
 * Revision 1.103.2.69.4.25.2.76  2013/11/25 15:17:37  grao
 * Case# 20125874 related issue fixes in SB 2014.1
 *
 * Revision 1.103.2.69.4.25.2.75  2013/11/25 06:46:17  vmandala
 * Case# 20125915
 * tek fix (restriction of negative userids)
 *
 * Revision 1.103.2.69.4.25.2.74  2013/11/20 23:28:13  skreddy
 * Case# 20125850
 * Lost and Found CR for MHP
 * issue fix
 *
 * Revision 1.103.2.69.4.25.2.73  2013/11/18 16:09:45  gkalla
 * case#20125758,20125755
 * CWD Picking process slow because of shipmanifest rec creation,UPC Code scanning issue
 *
 * Revision 1.103.2.69.4.25.2.72  2013/11/18 15:57:32  gkalla
 * case#20125758
 * CWD Picking process slow because of shipmanifest rec creation
 *
 * Revision 1.103.2.69.4.25.2.71  2013/11/01 13:41:06  schepuri
 * 20125233
 *
 * Revision 1.103.2.69.4.25.2.70  2013/11/01 13:39:57  rmukkera
 * Case# 20125479
 *
 * Revision 1.103.2.69.4.25.2.69  2013/11/01 12:58:33  rmukkera
 * Case# 20125455
 *
 * Revision 1.103.2.69.4.25.2.68  2013/10/29 13:36:25  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20124515
 * Empty location check during putaway location generation.
 *
 * Revision 1.103.2.69.4.25.2.67  2013/10/29 08:42:35  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20125154
 *
 * Revision 1.103.2.69.4.25.2.66  2013/10/24 15:43:50  nneelam
 * Case# 20125292
 * RF RMA Putaway, Invalid PO Status Error Msg.
 *
 * Revision 1.103.2.69.4.25.2.65  2013/10/23 10:15:59  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related case#20125024,20125023 was fixed
 *
 * Revision 1.103.2.69.4.25.2.64  2013/10/22 16:14:01  gkalla
 * Case# 20125197
 * Sports HQ for role based locations with subsidiaries
 *
 * Revision 1.103.2.69.4.25.2.63  2013/10/22 16:10:40  svanama
 * Case# 20124736
 * When we enable the COD flag at So screen, then system displayed the Cash on delivery as F at ship manifesto record
 *
 * Revision 1.103.2.69.4.25.2.62  2013/10/22 16:02:57  svanama
 * Case# 20124736
 * When we enable the COD flag at So screen, then system displayed the Cash on delivery as F at ship manifesto record
 *
 * Revision 1.103.2.69.4.25.2.61  2013/10/22 15:06:08  skreddy
 * Case# 20124932
 * standard bundle issue fix
 *
 * Revision 1.103.2.69.4.25.2.60  2013/10/18 08:55:00  schepuri
 * rf date formate function based on date setting flag
 *
 * Revision 1.103.2.69.4.25.2.59  2013/10/17 07:02:34  gkalla
 * Case# 20125004
 * Ryonet Timeout issue
 *
 * Revision 1.103.2.69.4.25.2.58  2013/10/15 07:07:23  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20124921
 * Warehouse Activity Report
 *
 * Revision 1.103.2.69.4.25.2.57  2013/10/15 06:30:25  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20124921
 * Warehouse Activity Report
 *
 * Revision 1.103.2.69.4.25.2.56  2013/10/01 14:48:05  grao
 * issue fixes related to the 20124682
 *
 * Revision 1.103.2.69.4.25.2.55  2013/10/01 14:47:00  grao
 * issue fixes related to the 20124682
 *
 * Revision 1.103.2.69.4.25.2.54  2013/09/25 06:53:04  schepuri
 * After Inventory adjustments, Qty is not getting updated in ebiznet and NS
 *
 * Revision 1.103.2.69.4.25.2.53  2013/09/20 08:25:15  svanama
 * Case# 20124381
 * Thirdparty values population in shipmanifest customrecord
 *
 * Revision 1.103.2.69.4.25.2.52  2013/09/19 15:58:04  skreddy
 * Case# 20124407
 * Afosa SB  issue fix
 *
 * Revision 1.103.2.69.4.25.2.51  2013/09/19 15:21:16  rmukkera
 * Case# 20124445
 *
 * Revision 1.103.2.69.4.25.2.50  2013/09/19 10:27:43  mbpragada
 * Case# 20124508
 * BOL and Sub BOL are not moving to closed task
 *
 * Revision 1.103.2.69.4.25.2.49  2013/09/17 16:04:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * standardbundlechanges
 * case#20124221
 *
 * Revision 1.103.2.69.4.25.2.48  2013/09/16 09:40:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.103.2.69.4.25.2.47  2013/09/06 07:35:17  rmukkera
 * Case# 20124243
 *
 * Revision 1.103.2.69.4.25.2.46  2013/09/05 16:56:52  gkalla
 * Case# 20124301
 * Invalid po issue in afosa
 *
 * Revision 1.103.2.69.4.25.2.45  2013/09/02 08:05:29  spendyala
 * case# 20124215
 * issue fixed related to case20124215
 *
 * Revision 1.103.2.69.4.25.2.44  2013/08/21 14:17:24  gkalla
 * Case# 20123942
 * Issue fixed for TSG, Invalid PO Status issue fix
 *
 * Revision 1.103.2.69.4.25.2.43  2013/08/21 02:38:28  snimmakayala
 * Case# 20123983
 * GSUSA - PICKGEN ISSUE
 *
 * Revision 1.103.2.69.4.25.2.42  2013/08/05 14:20:15  spendyala
 * Case# 20123747
 * While getting priority putaway we need to check wheather the binlocation is active or not
 *
 * Revision 1.103.2.69.4.25.2.41  2013/07/19 22:01:45  svanama
 * Case# 20123475
 * Shipmanifest  issues resolved
 *
 * Revision 1.103.2.69.4.25.2.40  2013/07/19 13:43:03  schepuri
 * ryonet sb issue fix
 *
 * Revision 1.103.2.69.4.25.2.39  2013/07/15 11:39:14  snimmakayala
 * Case# 20123430
 * GFT UAT ISSUE
 *
 * Revision 1.103.2.69.4.25.2.38  2013/07/15 10:43:42  mbpragada
 * Case# 20123445
 * BOL number generation issue
 *
 * Revision 1.103.2.69.4.25.2.36  2013/07/10 09:47:41  mbpragada
 * Case# 20123330
 * but lot no is not updating After qty exception
 *
 * Revision 1.103.2.69.4.25.2.35  2013/07/04 18:50:44  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixed against the case#201215876
 *
 * Revision 1.103.2.69.4.25.2.34  2013/07/03 16:12:55  gkalla
 * Case# 20122996
 * PCT Lot# parsing issue
 *
 * Revision 1.103.2.69.4.25.2.33  2013/06/26 15:00:35  snimmakayala
 * Case# : 201213655
 * GSUSA :: Issue Fixes
 *
 * Revision 1.103.2.69.4.25.2.32  2013/06/24 15:41:54  gkalla
 * CASE201112/CR201113/LOG201121
 * fix of PMME case#20123158
 *
 * Revision 1.103.2.69.4.25.2.31  2013/06/20 23:28:50  skreddy
 * CASE201112/CR201113/LOG201121
 * updating Item attribute feild in closed task
 *
 * Revision 1.103.2.69.4.25.2.30  2013/06/20 20:58:17  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of optimistic locking on LP Range
 *
 * Revision 1.103.2.69.4.25.2.29  2013/06/18 14:12:21  mbpragada
 * 20120490
 *
 * Revision 1.103.2.69.4.25.2.28  2013/06/12 11:57:10  svanama
 * CASE201112/CR201113/LOG201121
 * Residnetial Flag and location populaton in Shipmanifest Record
 *
 * Revision 1.103.2.69.4.25.2.27  2013/06/11 14:30:56  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.103.2.69.4.25.2.26  2013/06/05 11:58:52  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.103.2.69.4.25.2.25  2013/06/04 07:26:55  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Parsing CR for PCT
 *
 * Revision 1.103.2.69.4.25.2.24  2013/06/03 14:23:06  schepuri
 * PCT SB  issue
 *
 * Revision 1.103.2.69.4.25.2.23  2013/05/22 11:39:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating PO Status before user perform transaction.
 *
 * Revision 1.103.2.69.4.25.2.22  2013/05/21 07:30:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.103.2.69.4.25.2.21  2013/05/20 15:23:02  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.103.2.69.4.25.2.20  2013/05/15 14:50:14  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.103.2.69.4.25.2.19  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.103.2.69.4.25.2.18  2013/05/14 14:47:07  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.103.2.69.4.25.2.17  2013/05/09 12:49:19  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fix related to Item Status populating in PO.
 *
 * Revision 1.103.2.69.4.25.2.16  2013/05/07 15:13:05  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.103.2.69.4.25.2.15  2013/05/06 15:51:17  skreddy
 * CASE201112/CR201113/LOG201121
 * scanning serial no for Item
 *
 * Revision 1.103.2.69.4.25.2.14  2013/04/29 15:41:13  spendyala
 * CASE201112/CR201113/LOG2012392
 * Storing Error log in open task field.
 *
 * Revision 1.103.2.69.4.25.2.13  2013/04/25 15:26:51  skreddy
 * CASE201112/CR201113/LOG201121
 * issue rellated to autopacking
 *
 * Revision 1.103.2.69.4.25.2.12  2013/04/18 13:48:46  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.103.2.69.4.25.2.11  2013/04/10 05:37:39  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.103.2.69.4.25.2.10  2013/04/04 20:20:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixes
 *
 * Revision 1.103.2.69.4.25.2.9  2013/04/04 16:01:32  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to  update PO status report
 *
 * Revision 1.103.2.69.4.25.2.8  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.103.2.69.4.25.2.7  2013/03/22 11:44:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.103.2.69.4.25.2.6  2013/03/19 11:46:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.103.2.69.4.25.2.5  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.103.2.69.4.25.2.4  2013/03/13 13:57:18  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.103.2.69.4.25.2.3  2013/03/02 12:36:21  gkalla
 * CASE201112/CR201113/LOG201121
 * Merged from Factory mation as part of standard bundle
 *
 * Revision 1.103.2.69.4.25.2.2  2013/02/28 12:38:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Monobind.
 *
 * Revision 1.103.2.69.4.25.2.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.103.2.69.4.25  2013/02/20 23:44:05  kavitha
 * CASE201112/CR201113/LOG201121
 * Gaps fixes in getStageRule,getStageLocation
 *
 * Revision 1.103.2.69.4.24  2013/02/19 00:51:39  gkalla
 * CASE201112/CR201113/LOG201121
 * Issue fixing
 *
 * Revision 1.103.2.69.4.23  2013/02/01 15:22:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.103.2.69.4.22  2013/01/24 14:42:02  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.103.2.69.4.21  2013/01/21 15:51:53  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA-BuildShip Unit Chnages.
 *
 * Revision 1.103.2.69.4.20  2013/01/02 15:42:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Double Click restriction.
 * .
 *
 * Revision 1.103.2.69.4.19  2012/12/31 05:52:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Restrict Packing for Closed and Paymeny Hold Orders.
 *
 * Revision 1.103.2.69.4.18  2012/12/28 16:31:12  skreddy
 * CASE201112/CR201113/LOG201121
 * updated for WO Pick Reversal
 *
 * Revision 1.103.2.69.4.17  2012/12/21 15:32:33  skreddy
 * CASE201112/CR201113/LOG201121
 *  Date validation (eg:Nov31)
 *
 * Revision 1.103.2.69.4.16  2012/12/21 09:05:15  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.103.2.69.4.15  2012/12/17 15:16:14  schepuri
 * CASE201112/CR201113/LOG201121
 * fetching date format from set preferences
 *
 * Revision 1.103.2.69.4.14  2012/12/14 07:42:52  spendyala
 * CASE201112/CR201113/LOG201121
 * moved from 2012.2 branch
 *
 * Revision 1.103.2.69.4.13  2012/12/12 16:00:30  gkalla
 * CASE201112/CR201113/LOG201121
 * Actual Bin management enhancement
 *
 * Revision 1.103.2.69.4.12  2012/12/11 03:35:45  kavitha
 * CASE201112/CR201113/LOG201121
 * Incorporated Serial # functionality
 *
 * Revision 1.103.2.69.4.11  2012/12/05 08:36:59  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.103.2.69.4.10  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.103.2.69.4.9  2012/11/27 17:50:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Serial Capturing file.
 *
 * Revision 1.103.2.69.4.8  2012/11/11 03:40:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * GSUSA UAT issue fixes.
 *
 * Revision 1.103.2.69.4.7  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.103.2.69.4.6  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.103.2.69.4.5  2012/10/26 08:07:54  gkalla
 * CASE201112/CR201113/LOG201121
 * ParseFloat change and cube and weight updation
 *
 * Revision 1.103.2.69.4.4  2012/10/11 14:55:13  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * REPLEN CHANGES
 *
 * Revision 1.103.2.69.4.3  2012/10/03 11:29:36  schepuri
 * CASE201112/CR201113/LOG201121
 * Auto Gen Lot No
 *
 * Revision 1.103.2.69.4.2  2012/10/02 22:45:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.103.2.69.4.1  2012/09/26 22:43:33  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.103.2.69  2012/09/03 13:00:22  schepuri
 * CASE201112/CR201113/LOG201121
 * issue related to fetch putstrategy and modified datestamp function accrding to dateformat settings
 *
 * Revision 1.103.2.68  2012/08/27 04:04:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Auto Packlist for FISK
 * NS Case # : 20120739
 *
 * Revision 1.103.2.67  2012/08/24 18:45:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changesbackup
 *
 * Revision 1.103.2.65  2012/08/17 13:21:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.103.2.64  2012/08/16 13:53:10  spendyala
 * CASE201112/CR201113/LOG201121
 * Added one more search column cirteria with in the function geteBizItemDimensions to fetch item cube.
 *
 * Revision 1.103.2.63  2012/08/16 07:53:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah UAT Issue Fixes.
 * Cartonization and Customization
 *
 * Revision 1.103.2.62  2012/08/06 06:55:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Inventory issues is resolved.
 *
 * Revision 1.103.2.61  2012/07/30 16:43:51  gkalla
 * CASE201112/CR201113/LOG201121
 * Changes of email alerts and replenishment merging
 *
 * Revision 1.103.2.60  2012/07/30 13:52:48  svanama
 * CASE201112/CR201113/LOG201
 * Code changes for populating attention filed in ContactName filed in CreateShippingManifestRecord
 *
 * Revision 1.103.2.59  2012/07/27 13:03:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Passing ord qty in Trnline updation
 *
 * Revision 1.103.2.58  2012/07/26 06:02:04  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.103.2.57  2012/07/18 15:13:19  gkalla
 * CASE201112/CR201113/LOG201121
 * Alert mail issue fixed
 *
 * Revision 1.103.2.56  2012/07/18 06:25:49  spendyala
 * CASE201112/CR201113/LOG201121
 * In Create Ship Manifest Function updating package count and package no.
 *
 * Revision 1.103.2.55  2012/07/13 00:06:53  gkalla
 * CASE201112/CR201113/LOG201121
 * Fi ne tuned Generate putaway location
 *
 * Revision 1.103.2.54  2012/07/10 23:31:02  gkalla
 * CASE201112/CR201113/LOG201121
 * Email alerts
 *
 * Revision 1.103.2.53  2012/07/09 06:51:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.103.2.52  2012/07/04 14:52:43  gkalla
 * CASE201112/CR201113/LOG201121
 * Commented 0.01 value
 *
 * Revision 1.103.2.51  2012/07/04 09:54:48  gkalla
 * CASE201112/CR201113/LOG201121
 * sending pkg value to 0.01 if it sending 0 value to ship manifest
 *
 * Revision 1.103.2.50  2012/07/03 21:53:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Putaway Strategy changes
 *
 * Revision 1.103.2.49  2012/07/02 08:08:57  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issues
 *
 * Revision 1.103.2.48  2012/06/25 22:20:17  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned using child-parent relation
 *
 * Revision 1.103.2.47  2012/06/15 19:06:40  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fatch Subsidiary  dynamically
 *
 * Revision 1.103.2.46  2012/06/15 19:05:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fatch Subsidiary  dynamically
 *
 * Revision 1.103.2.45  2012/06/15 16:33:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.103.2.44  2012/06/15 15:43:38  gkalla
 * CASE201112/CR201113/LOG201121
 * To get dynamic Subsidiary values
 *
 * Revision 1.103.2.43  2012/06/14 06:46:53  gkalla
 * CASE201112/CR201113/LOG201121
 * Invalid item issue fix
 *
 * Revision 1.103.2.42  2012/06/11 14:20:14  svanama
 * CASE201112/CR201113/LOG201
 * fetching servicelevel  by using shipmethod from Carrier Service Level
 *
 * Revision 1.103.2.41  2012/05/31 12:49:33  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.103.2.40  2012/05/25 09:23:00  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.103.2.39  2012/05/14 14:51:02  spendyala
 * CASE201112/CR201113/LOG201121
 * Considering LPtype while searching for LPrange .
 *
 * Revision 1.103.2.38  2012/05/10 23:37:04  gkalla
 * CASE201112/CR201113/LOG201121
 * For matrix item validation
 *
 * Revision 1.103.2.37  2012/05/09 14:42:01  spendyala
 * CASE201112/CR201113/LOG201121
 * Changed searching criteria in the function "Daily"
 *
 * Revision 1.103.2.36  2012/05/09 14:01:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * shipping changes
 *
 * Revision 1.103.2.35  2012/05/09 13:59:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Partial Status Handling
 *
 * Revision 1.103.2.34  2012/05/04 09:03:37  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.103.2.33  2012/05/04 07:15:20  mbpragada
 * CASE201112/CR201113/LOG201121
 * CHANGED ITEM ID TO EXTERNAL ID IN VALIDATE SKU FUNCTION
 *
 * Revision 1.103.2.32  2012/05/03 11:40:01  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed the avg cost field
 *
 * Revision 1.103.2.31  2012/04/30 10:30:42  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.103.2.30  2012/04/27 11:44:10  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.103.2.29  2012/04/26 07:15:54  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issue unable to scan the item when we placed two items in a single
 *  location.
 *
 * Revision 1.103.2.28  2012/04/16 15:03:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Calculating POoverage function is added.
 *
 * Revision 1.103.2.27  2012/04/13 22:28:06  spendyala
 * CASE201112/CR201113/LOG201121
 * while creating transaction order line record for other than PO and SO they are creating multiple records issue was fixed.
 *
 * Revision 1.103.2.26  2012/04/03 15:19:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.103.2.25  2012/03/29 06:30:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * STG Locatoion update issue for outbound inventory records.
 *
 * Revision 1.128  2012/03/29 06:11:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * STG Locatoion update issue for outbound inventory records.
 *
 * Revision 1.127  2012/03/20 18:08:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Open Task to Closed Task movement
 *
 * Revision 1.126  2012/03/14 09:30:51  gkalla
 * CASE201112/CR201113/LOG201121
 * Added disable button functionality
 *
 * Revision 1.125  2012/03/09 09:28:43  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.103.2.22
 *
 * Revision 1.124  2012/03/09 07:16:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.123  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.122  2012/02/27 06:38:43  spendyala
 * CASE201112/CR201113/LOG201121
 * code merged upto 1.103.2.19
 *
 * Revision 1.121  2012/02/17 14:43:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new function addLeadingZeros and meged code upto 1.103.2.17 .
 *
 * Revision 1.120  2012/02/16 00:55:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.119  2012/02/13 23:14:25  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.118  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.117  2012/02/10 09:44:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.116  2012/02/10 08:58:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.115  2012/02/09 14:43:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.114  2012/02/09 09:00:23  rmukkera
 * CASE201112/CR201113/LOG201121
 * IsContLpExist function bug fix
 *
 * Revision 1.113  2012/02/08 15:53:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.112  2012/02/08 09:39:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged files from batch up to 1.103.2.9
 * issue: while searching for empty binloc,
 *  " for loop"  is not termination  eventhough empty binloc is found .
 *  so added break stmt to  for loop to terminate the looping process.
 *
 * Revision 1.111  2012/02/06 22:19:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * cycle count issue fixes
 *
 * Revision 1.110  2012/02/02 11:26:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * putstrategy issue fixes
 *
 * Revision 1.109  2012/02/01 15:20:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation User Event
 *
 * Revision 1.108  2012/02/01 13:35:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged files from batch up to 1.103.2.6 revision.
 *
 * Revision 1.107  2012/01/31 09:19:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged files from batch up to 1.103.2.4 revision.
 *
 * Revision 1.106  2012/01/27 06:55:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Mail Alerts for TO/PO confirmation
 *
 * Revision 1.105  2012/01/17 15:02:31  gkalla
 * CASE201112/CR201113/LOG201121
 * For Pack task creation in Pick confirmation
 *
 * Revision 1.104  2012/01/12 10:58:59  gkalla
 * CASE201112/CR201113/LOG201121
 * Added upc code function without location and company
 *
 * Revision 1.103  2012/01/04 16:01:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge/Mix strategy issue fixes
 *
 * Revision 1.102  2011/12/30 14:26:16  spendyala
 * CASE201112/CR201113/LOG201121
 * changed RFDateFormat function  for RF Expdate validation.
 *
 * Revision 1.101  2011/12/30 00:11:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.100  2011/12/28 23:15:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Putaway changes
 *
 * Revision 1.99  2011/12/27 16:44:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.98  2011/12/26 22:49:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin changes
 *
 * Revision 1.97  2011/12/22 18:42:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Shipping
 *
 * Revision 1.96  2011/12/22 08:14:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UPC Code scan in RF Picking
 *
 * Revision 1.95  2011/12/20 08:15:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Validate LP Range by location(site)
 *
 * Revision 1.94  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.93  2011/12/16 12:30:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.92  2011/12/12 13:04:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * spelling corrections in success message
 *
 * Revision 1.91  2011/12/12 12:42:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Inserting mandatory field 'item'  in open task at PACK tasktype
 *
 * Revision 1.90  2011/12/02 09:38:19  schepuri
 * CASE201112/CR201113/LOG201121
 * To generate location acc to the location defined in PO
 *
 * Revision 1.89  2011/11/30 11:54:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Update sales order line with qty at various levels.
 *
 * Revision 1.88  2011/11/28 11:39:57  svanama
 * CASE201112/CR201113/LOG201121
 * Code changed  in CreateShipManifest
 *
 * Revision 1.87  2011/11/26 14:25:38  svanama
 * CASE201112/CR201113/LOG201121
 * Signaturefiled and CodAmount field in CreateShipManifest
 *
 * Revision 1.86  2011/11/25 10:17:48  rmukkera
 * CASE201112/CR201113/LOG201121
 * TransferOrder Related code changes were done in the createshipmanifest method
 *
 * Revision 1.85  2011/11/23 11:10:25  schepuri
 * CASE201112/CR201113/LOG201121
 * in movetask record function instead of text, value is passed to sku field
 *
 * Revision 1.84  2011/11/22 13:49:58  svanama
 * CASE201112/CR201113/LOG201121
 *  CreateShippingManifestRecord add set void flag and custom5 flag
 *
 * Revision 1.83  2011/11/22 13:03:27  spendyala
 * CASE201112/CR201113/LOG201121
 * added with container size id and pack confirmed date in MoveTaskRecord function
 *
 * Revision 1.82  2011/11/21 13:58:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default Lot Functionality. (Item as Lot)
 *
 * Revision 1.81  2011/11/18 22:57:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Delete record from inventory if QOH becomes 0
 *
 * Revision 1.80  2011/11/17 22:40:56 snimmakayala
 * CASE201112/CR201113/LOG201121
 * Receiving for lotassembledbuilditems.
 *
 * Revision 1.79  2011/11/07 12:03:04  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Exception Handling in Auto Packing and Email Alerts
 *
 * Revision 1.78  2011/11/04 11:35:06  svanama
 * CASE201112/CR201113/LOG201121
 * Modification are changed at SendEmailMessage function
 *
 * Revision 1.77  2011/11/04 11:23:07  svanama
 * CASE201112/CR201113/LOG201121
 * Modified in Move TaskRecord
 *
 * Revision 1.76  2011/11/04 10:58:58  svanama
 * CASE201112/CR201113/LOG201121
 * InsertExceptionLog and SendEmailMessage  Method Added
 *
 * Revision 1.75  2011/11/01 15:36:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Restricting duplicate manifest records for same container lp.
 *
 * Revision 1.74  2011/11/01 10:08:47  rgore
 * CASE201112/CR201113/LOG201121
 * Added function to print remaining governance limit usage in the execution log.
 * - Ratnakar
 * 01 Nov 2011
 *
 * Revision 1.73  2011/10/17 07:16:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.72  2011/10/12 12:20:09  rgore
 * CASE201112/CR201113/LOG201121
 * New utility function to log the start and end time of function call.
 * - Ratnakar
 * 12 Oct 2011
 *
 * Revision 1.71  2011/10/11 08:00:43  skdokka
 * CASE201112/CR201113/LOG201121
 * Made changes to populate consignee name.
 *
 * Revision 1.70  2011/10/09 12:05:54  skdokka
 * CASE201112/CR201113/LOG201121
 * Made changes to populate zip code and service level for TNT
 *
 * Revision 1.69  2011/10/07 10:26:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes to show item description and upccode
 *
 * Revision 1.68  2011/10/04 22:20:41  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Stage Rule Carrier Fetching issue
 *
 * Revision 1.67  2011/10/03 22:47:55  snimmakayala
 * CASE201112/CR201113/LOG20112
 * RF Picking Issues
 *
 * Revision 1.66  2011/10/03 08:56:14  skdokka
 * CASE201112/CR201113/LOG201121
 * Fetching zip code from shipzip filed of sales order.
 *
 * Revision 1.65  2011/09/30 22:44:32  snimmakayala
 * CASE201112/CR201113/LOG20112
 *
 * Revision 1.64  2011/09/30 12:14:10  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Two changes:
 * Avg cost or unit cost if confition issue fixed.
 *
 * Revision 1.63  2011/09/30 11:22:22  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Lot# has been added as extra parameter for inventory posting function.
 * this is enhanced for TNT
 *
 * Revision 1.62  2011/09/28 11:41:20  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.61  2011/09/27 09:42:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.60  2011/09/22 15:41:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.59  2011/09/22 09:47:03  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.58  2011/09/21 18:59:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Fixes
 *
 * Revision 1.57  2011/09/19 11:24:06  rgore
 * CASE201112/CR201113/LOG201121
 * Added method to return the units consumed for Netsuite API called
 * - Ratnakar
 * 19 Sept 2011
 *
 * Revision 1.56  2011/09/16 15:08:04  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.55  2011/09/16 11:07:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.54  2011/09/15 16:33:53  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.53  2011/09/12 07:04:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.52  2011/09/09 15:38:19  vrgurujala
 * no message
 *
 * Revision 1.51  2011/09/08 15:36:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Carrier Determination Related files
 *
 * Revision 1.50  2011/09/07 23:19:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Storage Fix
 *
 * Revision 1.49  2011/09/07 21:49:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Storage Fix
 *
 * Revision 1.48  2011/09/05 12:56:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.
 * Task Split
 *
 * Revision 1.47  2011/09/05 07:55:16  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.46  2011/08/31 11:14:36  schepuri
 * CASE201112/CR201113/LOG201121
 * Inventory Report updating PutGen Qty
 *
 * Revision 1.45  2011/08/31 11:11:19  skota
 *  CASE2011262/CR2011259/LOG2011187
 *  Code changes in MoveTaskRecord function to consider new column custrecord_ebiz_nsconfirm_ref_no value while moving the record from open task to ebiz task
 *
 * Revision 1.44  2011/08/30 14:03:29  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Pick Confirmation Changes
 *
 * Revision 1.43  2011/08/29 13:21:57  snimmakayala
 * CASE2011262/CR2011259/LOG2011187
 *
 * Revision 1.42  2011/08/29 13:00:33  snimmakayala
 * CASE2011262/CR2011259/LOG2011187
 *
 * Revision 1.41  2011/08/29 12:44:59  snimmakayala
 * CASE2011262/CR2011259/LOG2011187
 *
 * Revision 1.40  2011/08/29 12:09:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Manifest related fixes.
 *
 * Revision 1.39  2011/08/29 10:20:36  rgore
 * CASE201112/CR201113/LOG201121
 * Bulk fulfillment order added log messages.
 * General Functions:  Added generic messages for logging array count.
 * - Ratnakar
 * 26 Aug 2011
 *
 * Revision 1.38  2011/08/27 06:48:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Manifest related fixes.
 *
 * Revision 1.37  2011/08/25 15:01:43  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.36  2011/08/25 12:25:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Pick Confirmation Changes
 *
 * Revision 1.35  2011/08/20 07:22:41  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.34  2011/08/11 10:04:42  skota
 * CASE201112/CR201113/LOG201121
 * changes made to return the value from CompareDates function
 *
 * 
 *
 *****************************************************************************/
function TimeStamp(){
	var timestamp;
	var now = new Date();
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = now.getHours();
	var curr_min = now.getMinutes();
	var curr_sec = now.getSeconds();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return timestamp;
}

/**
 * Function to return the current date in mm/dd/yyyy format
 * @author Phani
 * @returns Current system date
 */

function DateSetting()
{
	var ctx = nlapiGetContext();
	var setpreferencesdateformate = ctx.getPreference('DATEFORMAT');

	return setpreferencesdateformate;
}

function DateStamp(){
	var now = new Date();

	var dtsettingFlag = DateSetting();

	nlapiLogExecution('Debug', 'dtsettingFlag', dtsettingFlag);
	if(dtsettingFlag == 'DD/MM/YYYY')
	{
		return ((parseFloat(now.getDate())) + '/' + (parseFloat(now.getMonth()) + 1) + '/' +now.getFullYear());
	}
	else
	{
		return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
	}
	//
}
//case# 201412665
function TimeZoneDateStamp(){
	var now = new convertDate();

	var dtsettingFlag = DateSetting();

	nlapiLogExecution('Debug', 'dtsettingFlag', dtsettingFlag);// case# 201412144
	if(dtsettingFlag == 'DD/MM/YYYY')
	{
		return ((parseFloat(now.getDate())) + '/' + (parseFloat(now.getMonth()) + 1) + '/' +now.getFullYear());
	}
	else
	{
		return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
	}
	//
}

function convertDate(){

	var DS = false;//statically set if true or false

	var date = new Date(); // get current date

	var loadConfig = nlapiLoadConfiguration('userpreferences');
	var getTimeZone = loadConfig.getFieldText('TIMEZONE');

	nlapiLogExecution('ERROR', 'Time Zone', getTimeZone);

	var getOffset = '';
	//getTimeZone != '(GMT) Casablanca' || getTimeZone != '(GMT) Monrovia, Reykjavik' || 
	if(getTimeZone != '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London' && getTimeZone != '(GMT) Casablanca' && getTimeZone != '(GMT) Monrovia, Reykjavik'){

		getOffset = getTimeZone.substring(4, 7);

	}else{

		getOffset = 1; // under 3 timezones above are equal to UTC which is zero difference in hours

	}

	nlapiLogExecution('ERROR', 'Offset', getOffset);

	var UTCDate = date.getTime() + (date.getTimezoneOffset() * 60000); // convert current date into UTC (Coordinated Universal Time)

	nlapiLogExecution('ERROR', 'UTC Date', UTCDate);

	timezoneDate = new Date(UTCDate + (3600000*getOffset)); //create new date object with, subtract if customer timezone is behind UTC and add if ahead



	//--Note: There should be a flag if the current customers timezone is under Daylight saving--//

	if(DS)
	{
		var timezoneDateDayLight = new Date(timezoneDate.getTime() + 60*60000); // add 1 hour customer's timezone is currently under daylight saving

		nlapiLogExecution('ERROR', 'Date with respect to User Time Zone and Daylight Saving', timezoneDate);

		return timezoneDateDayLight;

	}else{

		nlapiLogExecution('ERROR', 'Date with respect to User Time Zone', timezoneDate);

		return timezoneDate;

	}

}
/**
 * Updates the transaction status flag in PO/SO
 * @param ordid
 * @param lineno
 * @param statusflag
 * @param ordtype
 * @returns 
 */
function updateStatus(ordid, lineno, statusflag, ordtype){
	var id;
	try {
		// Load the order record and update status flag
		var tranrecord = nlapiLoadRecord(ordtype, ordid);
		tranrecord.setLineItemValue('item', 'custcol_transactionstatusflag', lineno, statusflag);
		id = nlapiSubmitRecord(tranrecord, true);
	}
	catch(error){
		nlapiLogExecution('Debug', 'updateStatus:update custcol_transactionstatusflag', 'FAILED');
	}

	return id;
}

function GetPutawayLocation(Item, PackCode, ItemStatus, UOM, ItemCube, ItemType,poLocn){
	var context = nlapiGetContext();
	nlapiLogExecution('Debug','Remaining usage at the start',context.getRemainingUsage());
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod,PutMethodName,PutRuleId, MergeFlag = 'F',Mixsku='F',DimentionCheck='F';
	nlapiLogExecution('Debug', 'ItemCube', ItemCube);//0
	nlapiLogExecution('Debug', 'ItemType', ItemType);
	var itemresults;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;

	nlapiLogExecution('Debug', 'Item', Item);
	nlapiLogExecution('Debug', 'ItemFamily', ItemFamily);
	nlapiLogExecution('Debug', 'ItemGroup', ItemGroup);
	nlapiLogExecution('Debug', 'ItemStatus', ItemStatus);
	nlapiLogExecution('Debug', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('Debug', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('Debug', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('Debug', 'putVelocity', putVelocity);
	nlapiLogExecution('Debug', 'poLocn', poLocn);

	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	/*columns[0] = new nlobjSearchColumn('custrecord_sequencenumberpickrule');
	columns[0].setSort();*/
	columns[0] = new nlobjSearchColumn('formulanumeric');
	columns[0].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', ItemStatus]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof',['@NONE@', ItemInfo1]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof',['@NONE@', ItemInfo2]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof',['@NONE@', ItemInfo3]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof', ['@NONE@']));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', Item]));		
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@']));
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_abcvelpickrule', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(poLocn != null && poLocn != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', [poLocn]));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var t1 = new Date();
	var rulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (rulesearchresults != null) {
		nlapiLogExecution('Debug', 'rules count', rulesearchresults.length);
		for (var s = 0; s < rulesearchresults.length; s++) {
			locgroupid = rulesearchresults[s].getValue('custrecord_locationgrouppickrule');
			zoneid = rulesearchresults[s].getValue('custrecord_putawayzonepickrule');
			PutMethod = rulesearchresults[s].getValue('custrecord_putawaymethod');
			PutMethodName = rulesearchresults[s].getText('custrecord_putawaymethod');
			PutRuleId = rulesearchresults[s].getValue('custrecord_ruleidpickrule');
			nlapiLogExecution('Debug', 'Put Rule', PutRuleId);
			nlapiLogExecution('Debug', 'Put Method', PutMethod);
			nlapiLogExecution('Debug', 'Location Group Id from Putaway Rule', locgroupid);

			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = rulesearchresults[s].getValue('custrecord_manual_location_generation');
			nlapiLogExecution('Debug', 'Manual Location Generation', ManualLocationGeneration);

			if (ManualLocationGeneration == 'F') {
				if (locgroupid != 0 && locgroupid != null) {
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

					nlapiLogExecution('Debug', 'locgroupid', locgroupid);

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]));	
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					if(poLocn != null && poLocn != "")
						filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', [poLocn]));


					t1 = new Date();
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:location',
							getElapsedTimeDuration(t1, t2));

					nlapiLogExecution('Debug', 'locResults', locResults);
					if (locResults != null) {
						nlapiLogExecution('Debug', 'ifnot ');
						for (var u = 0; u < locResults.length; u++) {

							Location = locResults[u].getValue('name');
							var locationIntId = locResults[u].getId();

							nlapiLogExecution('Debug', 'Location', Location);
							nlapiLogExecution('Debug', 'Location Id', locationIntId);

							//Commented by satish.N on 04-JAN-2012 as the search results will give the same value.
							//LocRemCube = GeteLocCube(locationIntId);
							LocRemCube = locResults[u].getValue('custrecord_remainingcube');							
							OBLocGroup = locResults[u].getValue('custrecord_outboundinvlocgroupid');
							PickSeqNo  = locResults[u].getValue('custrecord_startingpickseqno');
							IBLocGroup = locResults[u].getValue('custrecord_inboundinvlocgroupid');
							PutSeqNo   = locResults[u].getValue('custrecord_startingputseqno');

							nlapiLogExecution('Debug', 'LocRemCube', LocRemCube);
							nlapiLogExecution('Debug', 'ItemCube', ItemCube);

							if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
								RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
								location_found = true;

								LocArry[0] = Location;
								LocArry[1] = RemCube;
								LocArry[2] = locationIntId;	
								LocArry[3] = OBLocGroup;
								LocArry[4] = PickSeqNo;
								LocArry[5] = IBLocGroup;
								LocArry[6] = PutSeqNo;
								LocArry[7] = LocRemCube;
								LocArry[8] = zoneid;
								LocArry[9] = PutMethodName;
								LocArry[10] = PutRuleId;

								nlapiLogExecution('Debug', 'SA-Location', Location);
								nlapiLogExecution('Debug', 'Location Internal Id',locationIntId);
								nlapiLogExecution('Debug', 'Sa-Location Cube', RemCube);
								return LocArry;
							}

						}
					}
				}
				else {
					nlapiLogExecution('Debug', 'Fetching Location Group from Zone', zoneid);
					var mixarray =  new Array();
					if(PutMethod!=null && PutMethod!='')
						mixarray=GetMergeFlag(PutMethod);
					if(mixarray!=null && mixarray!='' && mixarray.length>0)
					{
						MergeFlag = mixarray[0][0];
						Mixsku = mixarray[0][1];
						DimentionCheck=mixarray[0][2];
					}
					nlapiLogExecution('Debug', 'Merge Flag', MergeFlag);
					nlapiLogExecution('Debug', 'Mix SKU', Mixsku);
					nlapiLogExecution('Debug', 'DimentionCheck', DimentionCheck);
					if (zoneid != null && zoneid != ""){ 
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_zone_seq');
						columns[0].setSort();
						columns[1] = new nlobjSearchColumn('custrecord_locgroup_no');

						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', zoneid));
						filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));
						t1 = new Date();
						var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
						t2 = new Date();
						nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:zone_locgroup',
								getElapsedTimeDuration(t1, t2));

						var RemCube = 0;
						var LocRemCube = 0;

						if (searchresults != null && searchresults != '') 
						{

							for (var i = 0; i < Math.min(30, searchresults.length); i++) {
								locgroupid = searchresults[i].getValue('custrecord_locgroup_no');
								nlapiLogExecution('Debug', 'Fetched Location Group Id from Putaway Zone', locgroupid);

								if (MergeFlag == 'T') {
									nlapiLogExecution('Debug', 'Inside Merge ');
									var locResults;
									nlapiLogExecution('Debug', 'ItemCube', ItemCube);
									nlapiLogExecution('Debug', 'poLocn', poLocn);
									nlapiLogExecution('Debug', 'locgroupid', locgroupid);

									try {
										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[0].setSort();
										columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
										columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
										columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
										columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

										var filters = new Array();
										if(Mixsku!='T')
										{
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
										}

										if(ItemCube == null || ItemCube == '' || isNaN(ItemCube))
											ItemCube = 0;

										if(poLocn!=null && poLocn!='')
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', poLocn));
										filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'is', locgroupid));
										if(ItemCube!=null && ItemCube!='' && parseFloat(ItemCube)>0)
											filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));                                        
										filters.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc',  'is','F'));
										t1 = new Date();
										locResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
										t2 = new Date();
										nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:createinv',
												getElapsedTimeDuration(t1, t2));
									} //try close.
									catch (Error) {
										nlapiLogExecution('Debug', 'Into Error', Error);
									}
									try {
										nlapiLogExecution('Debug', 'Inside Try', 'TRY');

										if (locResults != null && locResults != '') {
											nlapiLogExecution('Debug', 'LocationResults', locResults.length );
											for (var j = 0; j < locResults.length; j++) {
												nlapiLogExecution('Debug', 'locResults[j].getId()', locResults[j].getId());
												Location = locResults[j].getText('custrecord_ebiz_inv_binloc');
												var locationInternalId = locResults[j].getValue('custrecord_ebiz_inv_binloc');

												nlapiLogExecution('Debug', 'Location', Location );
												nlapiLogExecution('Debug', 'Location Id', locationInternalId);

												//LocRemCube = GeteLocCube(locationInternalId);
												LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

												nlapiLogExecution('Debug', 'Loc Rem Cube', LocRemCube);
												nlapiLogExecution('Debug', 'Item Cube', ItemCube);

												//if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
												if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {
													RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

													location_found = true;

													LocArry[0] = Location;
													LocArry[1] = RemCube;
													LocArry[2] = locationInternalId;		//locResults[j].getId();
													LocArry[3] = OBLocGroup;
													LocArry[4] = PickSeqNo;
													LocArry[5] = IBLocGroup;
													LocArry[6] = PutSeqNo;
													LocArry[7] = LocRemCube;
													LocArry[8] = zoneid;
													LocArry[9] = PutMethodName;
													LocArry[10] = PutRuleId;

													nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
													return LocArry;
												}
											}
										}
									} 
									catch (exps) {
										nlapiLogExecution('Debug', 'included exps', exps);
									}

									//Added by satish.N on 04-JAN-2012 to consider open taks while merging.
									if (location_found == false) {

										try{
											nlapiLogExecution('Debug', 'Inside  consider open taks while merging');

											var columns = new Array();
											columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[0].setSort();
											columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
											columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
											columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_actbeginloc');
											columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
											columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[6] = new nlobjSearchColumn('custrecord_sku');
											columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_actbeginloc');

											var filters = new Array();
											if(Mixsku!='T')
											{
												filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', Item));
											}	

											if(ItemCube == null || ItemCube == '' || isNaN(ItemCube))
												ItemCube = 0;

											if(poLocn!=null && poLocn!='')
												filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));
											filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
											filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'is', locgroupid));

											if(ItemCube!=null && ItemCube!='' && parseFloat(ItemCube)>0)
												filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_actbeginloc', 'greaterthanorequalto', ItemCube));

											locResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

											if (locResults != null && locResults != '') {
												for (var j = 0; j < locResults.length; j++) {

													Location = locResults[j].getText('custrecord_actbeginloc');
													var locationInternalId = locResults[j].getValue('custrecord_actbeginloc');

													nlapiLogExecution('Debug', 'Location', Location );
													nlapiLogExecution('Debug', 'Location Id', locationInternalId);

													LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
													OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
													PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
													IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
													PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

													nlapiLogExecution('Debug', 'Loc Rem Cube', LocRemCube);
													nlapiLogExecution('Debug', 'Item Cube', ItemCube);

													//if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
													if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

														location_found = true;

														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locationInternalId;		//locResults[j].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;

														nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
														return LocArry;
													}
												}										
											}
										}
										catch(exps){
											nlapiLogExecution('Debug', 'included exps in opentasks', exps);
										}
									}
								}
								if (location_found == false) {

									//code modified by suman on 27/01/12
									//Instead of searching each and individual binloc in inventory and open task record it seems to have a goverence issue.
									//So I bought all binloc from open task and inventory records depending upon "locgr"Id and placing it in an array so that on comparing this array with the binlocation 
									//will decides weather it is empty or not.

									nlapiLogExecution('Debug', 'Location Not found for Merge Location for Location Group', locgroupid);
									var locResults = GetLocation(locgroupid,ItemCube,poLocn);

									if (locResults != null && locResults != '') {

										var TotalListOfBinLoc=new Array();
										for ( var x = 0; x < locResults.length; x++) 
										{
											TotalListOfBinLoc[x]=locResults[x].getId();

										}
										var emptyloccheck =	binLocationChecknew(TotalListOfBinLoc,poLocn);
										nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

										nlapiLogExecution('Debug', 'locResults.length', locResults.length);
										for (var j = 0; j < Math.min(300, locResults.length); j++) {
											var LocFlag='N';
											var emptyBinLocationId = locResults[j].getId();
											nlapiLogExecution('Debug', 'emptyBinLocationId', emptyBinLocationId);
//											var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//											nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

											for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
											{
//												nlapiLogExecution('Debug', 'emptyloccheck[0][InvBinLocCount]', emptyloccheck[0][InvBinLocCount]);

												if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('Debug', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
											for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
											{
												if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('Debug', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
//											end of code changed 27/01/12 
											nlapiLogExecution('Debug', 'LocFlag', LocFlag);
											nlapiLogExecution('Debug', 'Main Loop ', s);
											nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
											if(LocFlag == 'N'){
												//Changes done by Sarita (index i is changed to j).
												LocRemCube = locResults[j].getValue('custrecord_remainingcube');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno');

												nlapiLogExecution('Debug', 'LocRemCube', LocRemCube);
												nlapiLogExecution('Debug', 'ItemCube', ItemCube);

												//	if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
												if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {
													RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
													location_found = true;
													Location = locResults[j].getValue('name');
													LocArry[0] = Location;
													LocArry[1] = RemCube;
													LocArry[2] = locResults[j].getId();
													LocArry[3] = OBLocGroup;
													LocArry[4] = PickSeqNo;
													LocArry[5] = IBLocGroup;
													LocArry[6] = PutSeqNo;
													LocArry[7] = LocRemCube;
													LocArry[8] = zoneid;
													LocArry[9] = PutMethodName;
													LocArry[10] = PutRuleId;

													nlapiLogExecution('Debug', 'Location', Location);
													nlapiLogExecution('Debug', 'Location Cube', RemCube);
													return LocArry;
													break;
												}
											}
										}
									}
								}
							}
						}//end of searchresult null checking
					}
				}
				nlapiLogExecution('Debug', 'Main Loop ', s);
				nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
			}
		}
	}// end of put rule for loop
	else
	{
		nlapiLogExecution('Debug', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	//  } // end of main if
	return LocArry;
}

/**
 * Determines the location based on putaway strategy and returns the bin location
 * @author Phani
 * @param locgroupid
 * @returns
 */

function ValidateQty(skuID, binLoc){

	var expectedQty = pendingReplenPutawayTasks(skuID, binLoc);
	// Check quantity in inventory
	var inventoryQOH = TotalQuantityinInvt(skuID,binLoc);

	if(inventoryQOH == "" || inventoryQOH == null)
		inventoryQOH=0;

	nlapiLogExecution('Debug', 'priorityPutaway:Expected Quantity', expectedQty);
	nlapiLogExecution('Debug', 'priorityPutaway:Inventory QOH', inventoryQOH);

	// Sum up the total qty that has pending replen tasks and pending
	// moves with qohqty of inventory.
	if (expectedQty =="" || expectedQty == null)
		expectedQty=0;

	var totalAvailableQty = parseFloat(inventoryQOH) + parseFloat(expectedQty);
	nlapiLogExecution('Debug','priorityPutaway:Total Available Qty',totalAvailableQty);
}

function GetAllLocation(vlocgrouparr,ItemCube)
{
	nlapiLogExecution('Debug', 'Into GetAllLocation',vlocgrouparr);

	var searchResults = new Array();

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_remainingcube');
	columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
	columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
	columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
	columns[5] = new nlobjSearchColumn('custrecord_startingputseqno');
	columns[6] = new nlobjSearchColumn('name');		

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', vlocgrouparr));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	if(ItemCube!=null && ItemCube!='' && parseFloat(ItemCube)>0)
		filters.push(new nlobjSearchFilter('custrecord_remainingcube', null, 'greaterthanorequalto', ItemCube));

	searchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

	nlapiLogExecution('Debug', 'Out of GetAllLocation');

	return searchResults;	
}

function GetLocationNew(alllocResults,vzonelocgroupno,ItemCube)
{	
	nlapiLogExecution('Debug', 'Into GetLocationNew');

	var locresults = new Array();

	if(alllocResults!=null && alllocResults!='')
	{
		for (var j = 0; j < alllocResults.length; j++) {

			var inblocgroupno = alllocResults[j].getValue('custrecord_inboundlocgroupid');

			if(vzonelocgroupno==inblocgroupno)
			{
				locresults.push(alllocResults[j]);
			}
		}
	}

	nlapiLogExecution('Debug', 'Out of GetLocationNew');
	return locresults;
}

function GetLocation(locgroupid,ItemCube,poLocn){
	var searchResults;
	try {
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
		columns[0].setSort();
		columns[1] = new nlobjSearchColumn('custrecord_remainingcube');
		columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
		columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
		columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
		columns[5] = new nlobjSearchColumn('custrecord_startingputseqno');
		columns[6] = new nlobjSearchColumn('name');		

		nlapiLogExecution('Debug', 'GetLocation:Before Location Search');
		var filters = new Array();
		if(poLocn!=null && poLocn!='')
			filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', poLocn));
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', locgroupid));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		if(ItemCube!=null && ItemCube!='' && parseFloat(ItemCube)>0)
			filters.push(new nlobjSearchFilter('custrecord_remainingcube', null, 'greaterthanorequalto', ItemCube));

		var t1 = new Date();
		searchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
		var t2 = new Date();
		nlapiLogExecution('Debug', 'nlapiSearchRecord',searchResults);
		nlapiLogExecution('Debug', 'nlapiSearchRecord:GetLocation:location', getElapsedTimeDuration(t1, t2));
		nlapiLogExecution('Debug', 'GetLocation:After Location Search');

	}catch(exception){
		nlapiLogExecution('Debug', 'GetLocation', 'FAILED');
	}
	return searchResults;
}

function binLocationCheck(emptyBinLocationId)
{

	var binLocationCheckResult = 'Y';
	var inventoryColumns = new Array();
	inventoryColumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	inventoryColumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');

	var inventoryFilters = new Array();
	if(emptyBinLocationId!=null && emptyBinLocationId!="")
		inventoryFilters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', emptyBinLocationId);

	inventoryItemSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, inventoryFilters, inventoryColumns );
	nlapiLogExecution('Debug', 'GetLocation:inventoryItemSearchResults',inventoryItemSearchResults);

	var InvtBinLoc=new Array();
	if(inventoryItemSearchResults !=null && inventoryItemSearchResults !="")
	{
		for ( var count = 0; count < inventoryItemSearchResults.length; count++) {
			InvtBinLoc[count]=inventoryItemSearchResults[count].getValue('custrecord_ebiz_inv_binloc');
		}
	}
	else
	{
		InvtBinLoc[0]=-1;
	}

	var taskColumns = new Array();
	taskColumns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	taskColumns[1] = new nlobjSearchColumn('custrecord_sku');

	var taskFilters = new Array();
	if(emptyBinLocationId!=null && emptyBinLocationId!="")
		taskFilters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', emptyBinLocationId);

	var taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);
	nlapiLogExecution('Debug', 'taskSearchResults',taskSearchResults);
	var OpenTaskBinLoc=new Array();
	if(taskSearchResults!=null && taskSearchResults!="")
	{
		for ( var count = 0; count < taskSearchResults.length; count++) 
		{
			OpenTaskBinLoc[count]=taskSearchResults[count].getValue('custrecord_actbeginloc');

		}
	}
	else
	{
		OpenTaskBinLoc[0]=-1;
	}


	var TotalBinLoc=new Array();
	TotalBinLoc[0]=InvtBinLoc;
	TotalBinLoc[1]=OpenTaskBinLoc;
	nlapiLogExecution('Debug', 'TotalBinLoc[0]',TotalBinLoc[0]);
	nlapiLogExecution('Debug', 'TotalBinLoc[1]',TotalBinLoc[1]);
	return TotalBinLoc;
//	if (inventoryItemSearchResults == null)
//	{
//	nlapiLogExecution('Debug', 'GetLocation:inventoryItemSearchResults','here');
//	var taskColumns = new Array();
//	taskColumns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
//	taskColumns[1] = new nlobjSearchColumn('custrecord_sku');

//	var taskFilters = new Array();
//	taskFilters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', emptyBinLocationId);
//	taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);
//	if (taskSearchResults == null){
//	binLocationCheckResult = 'N';
//	}
//	else{
//	binLocationCheckResult = 'Y';
//	}
//	}
//	else
//	{
//	binLocationCheckResult = 'Y';
//	}
//	return binLocationCheckResult;
}

/**
 * Determines whether the putaway method has the merge option selected
 * @author Phani
 * @param putawayMethodId
 * @returns T/F
 */
function GetMergeFlag(putawayMethodId){
	var mergeFlag = 'F';
	var mixsku = 'F';
	var dimentioncheck='F';
	var currentRow = new Array();
	var mixarray =  new Array();

	try {
		var t1 = new Date();
		var searchresults = nlapiLoadRecord('customrecord_ebiznet_putaway_method', putawayMethodId);
		var t2 = new Date();
		nlapiLogExecution('Debug', 'nlapiLoadRecord:GetMergeFlag:putaway_method', getElapsedTimeDuration(t1, t2));

		mergeFlag = searchresults.getFieldValue('custrecord_mergeputaway');
		mixsku = searchresults.getFieldValue('custrecord_mixsku');
		dimentioncheck=searchresults.getFieldValue('custrecord_dimension_putawaymethod');
		currentRow = [mergeFlag, mixsku,dimentioncheck];
		mixarray[0] = currentRow;

	}catch(exception) {
		nlapiLogExecution('Debug', 'GetMergeFlag', exception);
	}

	return mixarray;
}

/**
 * Updates the bin location volume for the specified location
 * @param locnId
 * @param remainingCube
 */
function UpdateLocCube(locnId, remainingCube)
{
	try
	{
		var fields = new Array();
		var values = new Array();
		fields[0] = 'custrecord_remainingcube';
		values[0] = parseFloat(remainingCube).toFixed(4);

		nlapiLogExecution('Debug','Location Internal Id', locnId);

		nlapiLogExecution('Debug','Location Internal IdremainingCube', remainingCube);


		var t1 = new Date();
		nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
		var t2 = new Date();
		nlapiLogExecution('ERROR', 'nlapiSubmitField:UpdateLocCube:location', getElapsedTimeDuration(t1, t2));
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception in UpdateLocCube",exp);
	}
}

/**
 * 
 * @param vskuno
 * @param vUOM
 * @returns {Array}
 */
function fnGetSkucubeandweight(vskuno, vUOM){
	var vCube = 0;
	var vweight = 0;
	var dimarray = new Array();
	if (vUOM == "") {
		vUOM = 1;
	}
	nlapiLogExecution('Debug', 'SKU info', vskuno);
	nlapiLogExecution('Debug', 'vUOM', vUOM);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', vskuno));
	filters.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', vUOM));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebiztareweight');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {
		var searchresult = searchresults[i];
		vCube = searchresult.getValue('custrecord_ebizcube');
		vweight = searchresult.getValue('custrecord_ebiztareweight');
	}
	dimarray[0] = vCube;
	dimarray[1] = vweight;
	nlapiLogExecution('Debug', 'vCube', vCube);
	nlapiLogExecution('Debug', 'vweight', vweight);
	return dimarray;
}

/**
 * 
 * @param skuNo
 * @param uom
 * @returns {dimArray0}
 */
function getSKUCubeAndWeight(skuNo, uom){
	nlapiLogExecution('Debug', 'getSKUCubeAndWeight', 'Start');
	var timestamp1 = new Date();
	var cube = 0;
	var BaseUOMQty = 0;

	var dimArray = new Array();
	if (uom == "")
		uom = 1;

	nlapiLogExecution('Debug', 'getSKUCubeAndWeight:SKU info', skuNo);
	nlapiLogExecution('Debug', 'getSKUCubeAndWeight:UOM', uom);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', skuNo));
	filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var t1 = new Date();
	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiSearchRecord:getSKUCubeAndWeight:skudims',
			getElapsedTimeDuration(t1, t2));

	if(skuDimsSearchResults != null){
		for (var i = 0; i < skuDimsSearchResults.length; i++) {
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	//dimArray["BaseUOMItemCube"] = parseFloat(cube);
	//dimArray["BaseUOMQty"] = BaseUOMQty;
	dimArray[0] = parseFloat(cube);//BaseUOMItemCube
	dimArray[1] = BaseUOMQty;//BaseUOMQty

	var timestamp2 = new Date();
	nlapiLogExecution('Debug', 'getSKUCubeAndWeight Duration', getElapsedTimeDuration(timestamp1, timestamp2));



	nlapiLogExecution('Debug', 'getSKUCubeAndWeight:dimArray["BaseUOMItemCube"]', dimArray[0]);
	nlapiLogExecution('Debug', 'getSKUCubeAndWeight:dimArray["BaseUOMQty"]', dimArray[1]);
	nlapiLogExecution('Debug', 'dimArray.length', dimArray.length);

	return dimArray;
}


function getSKUCubeAndWeightforconfirm(skuNo, uom){
	nlapiLogExecution('Debug', 'getSKUCubeAndWeightforconfirm', 'Start');
	var timestamp1 = new Date();
	var cube = 0;
	//var BaseUOMQty = 0;
	var Weight=0;

	var dimArray = new Array();
	if (uom == "")
		uom = 1;

	nlapiLogExecution('Debug', 'getSKUCubeAndWeightforconfirm:SKU info', skuNo);
	nlapiLogExecution('Debug', 'getSKUCubeAndWeightforconfirm:UOM', uom);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', skuNo));
	filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizweight');

	var t1 = new Date();
	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiSearchRecord:getSKUCubeAndWeightforconfirm:skudims',
			getElapsedTimeDuration(t1, t2));

	if(skuDimsSearchResults != null){
		for (var i = 0; i < skuDimsSearchResults.length; i++) {
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			Weight = skuDim.getValue('custrecord_ebizweight');
		}
	}

	//dimArray["BaseUOMItemCube"] = parseFloat(cube);
	//dimArray["Weight"] = Weight;
	dimArray[0] = parseFloat(cube);//BaseUOMItemCube
	dimArray[1] = parseFloat(Weight);//Weight

	var timestamp2 = new Date();
	nlapiLogExecution('Debug', 'getSKUCubeAndWeight Duration', getElapsedTimeDuration(timestamp1, timestamp2));



	nlapiLogExecution('Debug', 'getSKUCubeAndWeight:dimArray["BaseUOMItemCube"]', dimArray[0]);
	nlapiLogExecution('Debug', 'getSKUCubeAndWeight:dimArray["Weight"]', dimArray[1]);
	nlapiLogExecution('Debug', 'dimArray.length', dimArray.length);

	return dimArray;
}
/**
 * 
 * @param ContainerSizeId
 * @returns Array
 */
function getContainerCubeAndTarWeight(SizeId, container,vSite){ //Case# 20149997 
	nlapiLogExecution('Debug', 'getContainerCubeAndTarWeight', 'Start');
	nlapiLogExecution('Debug', 'SizeId', SizeId);
	nlapiLogExecution('Debug', 'container', container);
	var timestamp1 = new Date();
	var cube = 0;  
	var TarWeight=0;
	var InventoryInOut; 
	var containerInternalId;
	var ContainerName;
	var dimArray = new Array();  



	var filters = new Array();
	if(SizeId != "" && SizeId != null){
		//nlapiLogExecution('Debug', 'getContainerCubeAndTarWeight:SizeId', [SizeId]);  
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', [SizeId]));
	}

	if(container != "" && container != null){
		filters.push(new nlobjSearchFilter('name', null, 'is', container));   
	}
	//Case# 20149997 starts
	if(vSite != "" && vSite != null && vSite != 'null'){
		filters.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', ['@NONE@',vSite]));  
	}
	//Case# 20149997 ends
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_cubecontainer');
	columns[1] = new nlobjSearchColumn('custrecord_tareweight');
	columns[2] = new nlobjSearchColumn('custrecord_inventory_in_on');
	columns[3] = new nlobjSearchColumn('name');

	var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);

	if(ContainerSearchResults != null){
		for (var i = 0; i < ContainerSearchResults.length; i++){
			var ContainerDetails = ContainerSearchResults[i];
			cube = ContainerDetails.getValue('custrecord_cubecontainer');
			TarWeight = ContainerDetails.getValue('custrecord_tareweight');
			InventoryInOut = ContainerDetails.getValue('custrecord_inventory_in_on');
			containerInternalId = ContainerDetails.getId();
			ContainerName=ContainerDetails.getValue('name');
		}
	}
	if(cube != null && cube != '')
		dimArray[0] = parseFloat(cube);//ItemCube
	else
		dimArray[0] = 0;
	if(TarWeight != null && TarWeight != '')
		dimArray[1] = parseFloat(TarWeight);//TarWeight
	else
		dimArray[1] = 0;
	dimArray[2] = InventoryInOut;//TarWeight
	dimArray[3] = containerInternalId;
	dimArray[4] = ContainerName;

	var timestamp2 = new Date();
	nlapiLogExecution('Debug', 'ContainerSearchResults Duration', getElapsedTimeDuration(timestamp1, timestamp2));     

	nlapiLogExecution('Debug', 'ContainerSearchResults:dimArray["ItemCube"]', dimArray[0]);
	nlapiLogExecution('Debug', 'ContainerSearchResults:dimArray["TarWeight"]', dimArray[1]);
	nlapiLogExecution('Debug', 'ContainerSearchResults:dimArray["InventoryInOut"]', dimArray[2]);
	nlapiLogExecution('Debug', 'ContainerSearchResults:dimArray["containerInternalId"]', containerInternalId);
	nlapiLogExecution('Debug', 'dimArray.length', dimArray.length);

	return dimArray;
}
/**
 * 
 * @param LocID
 * @returns {Number}
 */
function GeteLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('Debug', 'GeteLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('Debug', 'GeteLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}

/**
 * 
 * @param lpno
 * @param type
 * @returns {Boolean}
 */
function ebiznet_LPRange_CL(lpno, type,loc,lptype,vgetItemLP){
	var vargetlpno = lpno;
	var vResult = "";
	nlapiLogExecution('Debug', 'vargetlpno.length', vargetlpno.length);
	if (vargetlpno.length > 0) {
		nlapiLogExecution('Debug', 'into the function', vargetlpno.length);
		var filters = new Array();

		//filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', '1');
		filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', type);
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		//Added on 31/01/12 by suman
		if(loc!=null && loc!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', loc));
		//end of code as of 31/01/12.		
		if(lptype!=null && lptype!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lptype]));

		nlapiLogExecution('Debug', 'userdefined type', type);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix'));
		if(searchresults!=null && searchresults.length>0)
		{
			nlapiLogExecution('Debug', 'searchresults.length', searchresults.length);
			for (var i = 0; i < Math.min(50, searchresults.length); i++) {
				try {
					var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');
					nlapiLogExecution('Debug', 'firstgetLPrefix', getLPPrefix);
					var recid = searchresults[i].getId();
					var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recid);

					var varBeginLPRange = transaction.getFieldValue('custrecord_ebiznet_lprange_begin');

					var varEndRange = transaction.getFieldValue('custrecord_ebiznet_lprange_end');

					var getLPGenerationTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lpgentype');
					var getLPTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lptype');

					nlapiLogExecution('Debug', 'getLPGenerationTypeValue', getLPGenerationTypeValue);
					nlapiLogExecution('Debug', 'type', type);
					nlapiLogExecution('Debug', 'getLPTypeValue', getLPTypeValue);

					//if (getLPTypeValue == type) {
					if (getLPGenerationTypeValue == type) {
						
						
						//var getLPrefix = (transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();
						
						var getLPrefix = transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix');
						if(getLPrefix!=null && getLPrefix!='')
						{
							getLPrefix = getLPrefix.toUpperCase();
						}
						
						var LPprefixlen = 0;
						var vLPLen = '';
						var newlplen = '';
						if(getLPrefix !=null && getLPrefix !='')
						{
							nlapiLogExecution('Debug', 'getLPrefix', 'not empty');
							LPprefixlen = getLPrefix.length;
							//case # 20140589
							/*	newlplen = vgetItemLP.substring(0, LPprefixlen);							
							vLPLen = vargetlpno.substring(0, LPprefixlen);
							nlapiLogExecution('Debug', 'newlplen new', newlplen);
							nlapiLogExecution('Debug', 'vLPLen new', vLPLen);
							if(newlplen == vLPLen)
							{
								vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();
								nlapiLogExecution('Debug', 'vLPLen if upper case', vLPLen);
							}
							else
							{*/
							vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();
							//	}

						}
						else
							getLPrefix='';
						//case no 20125233	
						if(vLPLen==lpno)
						{
							//alert('PLs give no along with LpPrefix');
							//return false;
							nlapiLogExecution('Debug', 'else', 'new here');
							vResult = "N";
							break;
						}


						nlapiLogExecution('Debug', 'vLPLen', vLPLen);
						nlapiLogExecution('Debug', 'getLPrefix', getLPrefix);
						if (vLPLen == getLPrefix) {
							var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);
							nlapiLogExecution('Debug', 'varnum', varnum);
							nlapiLogExecution('Debug', 'parseFloat(varnum)', parseFloat(varnum, 10));

							if(vLPLen==lpno)
							{
								vResult = "N";
								break;
							}
							nlapiLogExecution('Debug', 'varnum.length', varnum.length);
							nlapiLogExecution('Debug', 'varEndRange.length', varEndRange.length);
							if(isNaN(varnum)){
								vResult="N";
								break;

							}
							var result=ValidateSplCharacter(varnum,'');
							nlapiLogExecution('ERROR','result',result);
							if(result == false)
							{
								vResult = "N";
								//break;
								break; //Case# 20149376
							}
							if (varnum.length > varEndRange.length) {

								vResult = "N";
								break;
							}
							if ((parseFloat(varnum, 10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum, 10) > parseFloat(varEndRange))) {
								nlapiLogExecution('Debug', 'ifparseFloat(varnum)', parseFloat(varnum));
								nlapiLogExecution('Debug', 'ifparseFloat(varBeginLPRange)', parseFloat(varBeginLPRange));
								nlapiLogExecution('Debug', 'ifparseFloat(varEndRange)', parseFloat(varEndRange));
								vResult = "N";
								nlapiLogExecution('Debug', 'if', vResult);
								break;
							}
							else {
								vResult = "Y";
								nlapiLogExecution('Debug', 'else', vResult);
								break;
							}
						}
						else {
							nlapiLogExecution('Debug', 'else', 'here');
							vResult = "N";
						}
					} //end of if statement
				} 
				catch (err) {
					nlapiLogExecution('Debug', 'err', err);
					
				}
			} //end of for loop
		}
		if (vResult == "Y") {
			nlapiLogExecution('Debug', 'else', 'true');
			return true;
		}
		else {
			nlapiLogExecution('Debug', 'else', 'false');
			return false;
		}
	}
	else {
		return false;
	}
}


/**
 * To check LP range with given LPType, LPgenType and LP
 * @param lpno
 * @param type //lpGentype
 * @param lptype
 * @returns {Boolean}
 */
function ebiznet_LPRange_CL_withLPType(lpno, type ,lptype,whloc){
	var vargetlpno = lpno;
	var vResult = "";
	nlapiLogExecution('Debug', 'lpno', lpno);
	nlapiLogExecution('Debug', 'vargetlpno.length', vargetlpno.length);
	if (vargetlpno.length > 0) {
		//nlapiLogExecution('Debug', 'into the function', vargetlpno.length);
		var filters = new Array();

		//filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', '1');
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'anyof', [type]));
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lptype]));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(whloc!=null && whloc!="" && whloc!='null')// Case# 20148341
			filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', [whloc]));

		nlapiLogExecution('Debug', 'userdefined type', type);

		var columns=new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_begin'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_end'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters,columns);
		//nlapiLogExecution('Debug', 'searchresults.length', searchresults.length);
		if(searchresults!=null && searchresults.length>0)
		{
			nlapiLogExecution('Debug', 'searchresults.length', searchresults.length);


			for (var i = 0; i < searchresults.length; i++) {
				try {
					nlapiLogExecution('Debug', 'searchresults[i].getId()', searchresults[i].getId());
					var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');
					//nlapiLogExecution('Debug', 'firstgetLPrefix', getLPPrefix);
					//var recid = searchresults[i].getId();
					//var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recid);

					var varBeginLPRange = searchresults[i].getValue('custrecord_ebiznet_lprange_begin');

					var varEndRange = searchresults[i].getValue('custrecord_ebiznet_lprange_end');

					var getLPGenerationTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lpgentype');
					var getLPTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lptype');

					//nlapiLogExecution('Debug', 'getLPGenerationTypeValue', getLPGenerationTypeValue);
					//nlapiLogExecution('Debug', 'type', type);
					//nlapiLogExecution('Debug', 'getLPTypeValue', getLPTypeValue);

					//if (getLPTypeValue == type) {
					if (getLPGenerationTypeValue == type) {
						var getLPrefix = (searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();
						var LPprefixlen = getLPrefix.length;
						var vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();

						//nlapiLogExecution('Debug', 'vLPLen', vLPLen);
						//nlapiLogExecution('Debug', 'getLPrefix', getLPrefix);
						if (vLPLen == getLPrefix) {
							var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);
							//nlapiLogExecution('Debug', 'varnum', varnum);
							//nlapiLogExecution('Debug', 'parseFloat(varnum)', parseFloat(varnum, 10));
							if(vLPLen==lpno)
							{
								vResult[0] = "N";
								vResult[1] = "R";
								break;
							}

							//nlapiLogExecution('Debug', 'varnum.length', varnum.length);
							//nlapiLogExecution('Debug', 'varEndRange.length', varEndRange.length);
							if (isNaN(varnum)) {

								vResult = "N";
								//break;
								break; //Case# 201410022 (remove the comment for break)
							}
							//case 20126945 start �: validating special character�
							// case # 20127590 starts
							var result=ValidateSplCharacter(varnum,'');
							nlapiLogExecution('ERROR','result',result);
							if(result == false)
							{
								vResult = "N";
								//break;
								break; //Case# 201410022 (remove the comment for break)
							}
							//case 20126945 end
							if (varnum.length > varEndRange.length) {

								vResult = "N";
								break;
								//break; Case# 20127590
							}
							//	if ((parseFloat(varnum, 10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum, 10) > parseFloat(varEndRange)) ) {
							if ((parseFloat(varnum, 10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum, 10) > parseFloat(varEndRange)) || (isNaN(varnum))) { //Case # 201410022
								//nlapiLogExecution('Debug', 'ifparseFloat(varnum)', parseFloat(varnum));
								//nlapiLogExecution('Debug', 'ifparseFloat(varBeginLPRange)', parseFloat(varBeginLPRange));
								//nlapiLogExecution('Debug', 'ifparseFloat(varEndRange)', parseFloat(varEndRange));
								vResult = "N";
								nlapiLogExecution('Debug', 'if', vResult);
								break;
								//break; // Case# 20127590
							}
							else {
								vResult = "Y";
								nlapiLogExecution('Debug', 'else', vResult);
								break;
							}
						}
						else {
							nlapiLogExecution('Debug', 'else', 'here');
							vResult = "N";
						}
					} //end of if statement
				} 

				catch (err) {
				}
			} //end of for loop
		}
		if (vResult == "Y") {
			nlapiLogExecution('Debug', 'else', 'true');
			return true;
		}
		else {
			nlapiLogExecution('Debug', 'else', 'false');
			return false;
		}
	}
	else {
		return false;
	}
}

//Case 20124932 added for validating invalid cart
function ebiznet_LPRange_CL_withLPType1(lpno, type ,lptype,whloc,vgetItemLP){
	var vargetlpno = lpno;
	var vResult = new Array();
	nlapiLogExecution('Debug', 'lpno', lpno);
	nlapiLogExecution('Debug', 'vargetlpno.length', vargetlpno.length);
	if (vargetlpno.length > 0) {
		//nlapiLogExecution('Debug', 'into the function', vargetlpno.length);
		var filters = new Array();

		//filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', '1');
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'anyof', [type]));
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lptype]));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(whloc!=null && whloc!="" && whloc!='null')
			filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', [whloc]));

		nlapiLogExecution('Debug', 'userdefined type', type);

		var columns=new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_begin'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_end'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters,columns);
		//nlapiLogExecution('Debug', 'searchresults.length', searchresults.length);
		if(searchresults!=null && searchresults.length>0)
		{
			nlapiLogExecution('Debug', 'searchresults.length', searchresults.length);


			for (var i = 0; i < searchresults.length; i++) {
				try {
					nlapiLogExecution('Debug', 'searchresults[i].getId()', searchresults[i].getId());
					var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');
					//nlapiLogExecution('Debug', 'firstgetLPrefix', getLPPrefix);
					//var recid = searchresults[i].getId();
					//var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recid);

					var varBeginLPRange = searchresults[i].getValue('custrecord_ebiznet_lprange_begin');

					var varEndRange = searchresults[i].getValue('custrecord_ebiznet_lprange_end');

					var getLPGenerationTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lpgentype');
					var getLPTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lptype');

					//nlapiLogExecution('Debug', 'getLPGenerationTypeValue', getLPGenerationTypeValue);
					//nlapiLogExecution('Debug', 'type', type);
					//nlapiLogExecution('Debug', 'getLPTypeValue', getLPTypeValue);

					//if (getLPTypeValue == type) {
					if (getLPGenerationTypeValue == type) {
						var getLPrefix = (searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();
						var LPprefixlen = getLPrefix.length;

						//case # 20140554

						/*	var newlplen = vgetItemLP.substring(0, LPprefixlen);						
						var vLPLen = vargetlpno.substring(0, LPprefixlen);						

						if(newlplen == vLPLen)
						{
							vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();
							nlapiLogExecution('Debug', 'vLPLen if upper case', vLPLen);
						}
						else
						{*/
						vLPLen = vargetlpno.substring(0, LPprefixlen);
						//	}


						nlapiLogExecution('Debug', 'vLPLen', vLPLen);
						nlapiLogExecution('Debug', 'getLPrefix', getLPrefix);
						if (vLPLen == getLPrefix) {
							var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);
							//nlapiLogExecution('Debug', 'varnum', varnum);
							//nlapiLogExecution('Debug', 'parseFloat(varnum)', parseFloat(varnum, 10));

							if(vLPLen==lpno)
							{
								vResult[0] = "N";
								vResult[1] = "R";
								break;
							}
							//nlapiLogExecution('Debug', 'varnum.length', varnum.length);
							//nlapiLogExecution('Debug', 'varEndRange.length', varEndRange.length);
							if (isNaN(varnum)) {

								vResult[0] = "N";
								vResult[1] = "O";
								break;
							}
							if (varnum.length > varEndRange.length) {

								vResult[0] = "N";
								vResult[1] = "O";
								break;
							}
							if ((parseFloat(varnum, 10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum, 10) > parseFloat(varEndRange))) {
								//nlapiLogExecution('Debug', 'ifparseFloat(varnum)', parseFloat(varnum));
								//nlapiLogExecution('Debug', 'ifparseFloat(varBeginLPRange)', parseFloat(varBeginLPRange));
								//nlapiLogExecution('Debug', 'ifparseFloat(varEndRange)', parseFloat(varEndRange));
								vResult = "N";
								nlapiLogExecution('Debug', 'if', vResult);
								vResult[1] = "O";
								break;
							}
							else {
								vResult[0] = "Y";
								nlapiLogExecution('Debug', 'else', vResult);
								break;
							}
						}
						else {
							nlapiLogExecution('Debug', 'else', 'here');
							vResult[0] = "N";
							vResult[1] = "I";

						}
					} //end of if statement
				} 

				catch (err) {
				}
			} //end of for loop
		}
		if (vResult[0] == "Y") {
			nlapiLogExecution('Debug', 'else', 'true');
			return vResult;
		}

		else {
			nlapiLogExecution('Debug', 'else', 'false');
			return vResult;
		}
	}
	else {
		return false;
	}
}
//Case 20124932 end 
/**
 * 
 * @param RecordID
 * @returns
 */
function MoveTaskRecord(RecordID,nsRefNo){
	nlapiLogExecution('Debug', 'RecordID', RecordID);
	nlapiLogExecution('Debug', 'nsRefNo', nsRefNo);

	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();

	var timeStamp1 = new Date();

	var t1 = new Date();
	var opentaskrecord = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordID);
	var t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiLoadRecord:MoveTaskRecord:trn_opentask', getElapsedTimeDuration(t1, t2));

	var TranId = opentaskrecord.getFieldValue('name');
	var ActBeginDate = opentaskrecord.getFieldValue('custrecordact_begin_date');
	var ActEndDate = opentaskrecord.getFieldValue('custrecord_act_end_date');
	var ActQty = opentaskrecord.getFieldValue('custrecord_act_qty');
	var BatchNo = opentaskrecord.getFieldValue('custrecord_batch_no');
	var CompId = opentaskrecord.getFieldValue('custrecord_comp_id');
	var ContLPNo = opentaskrecord.getFieldValue('custrecord_container_lp_no');
	nlapiLogExecution('Debug','GenConatierLp',ContLPNo);
	var crossdockflag = opentaskrecord.getFieldValue('custrecord_crossdock_flag');
	var crossdockqty = opentaskrecord.getFieldValue('custrecord_crossdock_qty');
	var currentdate = opentaskrecord.getFieldValue('custrecord_current_date');
	var deviceuploadflag = opentaskrecord.getFieldValue('custrecord_device_upload_flag');
	var ebizappownno = opentaskrecord.getFieldValue('custrecord_ebiz_appowner_no');
	var eBizCntrlNo = opentaskrecord.getFieldValue('custrecord_ebiz_cntrl_no');
	//var eBizContLPNo = opentaskrecord.getFieldValue('custrecord_ebiz_contlp_no');
	var eBizLpNo = opentaskrecord.getFieldValue('custrecord_ebiz_lpno');
	var eBizShipLpNo = opentaskrecord.getFieldValue('custrecord_ebiz_ship_lp_no');
	var Sku = opentaskrecord.getFieldText('custrecord_sku');
	var Skuno = opentaskrecord.getFieldValue('custrecord_sku');
	var SkuDesc = opentaskrecord.getFieldValue('custrecord_skudesc');
	var eBizSkuNo = opentaskrecord.getFieldValue('custrecord_ebiz_sku_no');
	var eBizTaskNo = opentaskrecord.getFieldValue('custrecord_ebiz_task_no');
	var eBizTrailerNo = opentaskrecord.getFieldValue('custrecord_ebiz_trailer_no');
	var eBizWaveNo = opentaskrecord.getFieldValue('custrecord_ebiz_wave_no');
	var ExpeQty = opentaskrecord.getFieldValue('custrecord_expe_qty');
	var LineNo = opentaskrecord.getFieldValue('custrecord_line_no');
	var LPNo = opentaskrecord.getFieldValue('custrecord_lpno');
	var masterLp = opentaskrecord.getFieldValue('custrecord_masterlp');
	var ebizLpNo = opentaskrecord.getFieldValue('custrecord_mast_ebizlp_no');
	var PackCode = opentaskrecord.getFieldValue('custrecord_packcode');
	var ShipLpNo = opentaskrecord.getFieldValue('custrecord_ship_lp_no');
	var SiteId = opentaskrecord.getFieldValue('custrecord_site_id');
	var SkuStatus = opentaskrecord.getFieldValue('custrecord_sku_status');
	var StatusFlag = opentaskrecord.getFieldValue('custrecord_wms_status_flag');
	var TaskType = opentaskrecord.getFieldValue('custrecord_tasktype');
	var TotalCube = opentaskrecord.getFieldValue('custrecord_totalcube');
	var TotalTime = opentaskrecord.getFieldValue('custrecord_total_time');
	var TotalWght = opentaskrecord.getFieldValue('custrecord_total_weight');
	var UOMID = opentaskrecord.getFieldValue('custrecord_uom_id');
	var UpdDate = opentaskrecord.getFieldValue('custrecord_upd_date');
	var UpdUser = opentaskrecord.getFieldValue('custrecord_upd_ebiz_user_no');//	
	var UOMLevel = opentaskrecord.getFieldValue('custrecord_uom_level');
	var eBizRecptNo = opentaskrecord.getFieldValue('custrecord_ebiz_receipt_no');
	var Notes = opentaskrecord.getFieldValue('custrecord_notes');
	var ActBeginLoc = opentaskrecord.getFieldValue('custrecord_actbeginloc');
	var ActEndLoc = opentaskrecord.getFieldValue('custrecord_actendloc');
	var ActBeginTime = opentaskrecord.getFieldValue('custrecord_actualbegintime');
	var ActEndTime = opentaskrecord.getFieldValue('custrecord_actualendtime');
	var RecordTime = opentaskrecord.getFieldValue('custrecord_recordtime');
	var RecordUpdTime = opentaskrecord.getFieldValue('custrecord_recordupdatetime');
	var expirydate = opentaskrecord.getFieldValue('custrecord_expirydate');
	var FifoDate = opentaskrecord.getFieldValue('custrecord_fifodate');
	var modelno = opentaskrecord.getFieldValue('custrecord_modelno');
	var AllocQty = opentaskrecord.getFieldValue('custrecord_ebizallocatedqty');
	var PickQty = opentaskrecord.getFieldValue('custrecord_pick_qty');
	var RefNo = opentaskrecord.getFieldValue('custrecord_invref_no');
	var FromLP = opentaskrecord.getFieldValue('custrecord_from_lp_no');
	var eBizOrdNo = opentaskrecord.getFieldValue('custrecord_ebiz_order_no');
	var wmslocation = opentaskrecord.getFieldValue('custrecord_wms_location');
	var ebizruleno = opentaskrecord.getFieldValue('custrecord_ebizrule_no');
	var ebizmethodno = opentaskrecord.getFieldValue('custrecord_ebizmethod_no');
	var ebizZoneNo = opentaskrecord.getFieldValue('custrecord_ebizzone_no');
	var clusterno = opentaskrecord.getFieldValue('custrecord_ebiz_clus_no');
	var parentSKU = opentaskrecord.getFieldValue('custrecord_parent_sku_no');
	var lastmember = opentaskrecord.getFieldValue('custrecord_last_member_component');
	var KitRefNo = opentaskrecord.getFieldValue('custrecord_kit_refno');
	var serialno = opentaskrecord.getFieldValue('custrecord_serial_no');
	//var lotwithquantity = opentaskrecord.getFieldValue('lotnowithquantity');
	// CASE# 20123330 - Start
	var lotwithquantity = opentaskrecord.getFieldValue('custrecord_lotnowithquantity');
	// CASE# 20123330 - End
	var TranLp = opentaskrecord.getFieldValue('custrecord_transport_lp');
	var empId= opentaskrecord.getFieldValue('custrecord_taskassignedto');
	var User= opentaskrecord.getFieldValue('custrecord_ebizuser');
	var mastShipLP= opentaskrecord.getFieldValue('custrecord_ship_lp_no');
	var nsconfirmRefNo = opentaskrecord.getFieldValue('custrecord_ebiz_nsconfirm_ref_no');
	var packconfirmedDate = opentaskrecord.getFieldValue('custrecord_pack_confirmed_date');
	var containerSizeId = opentaskrecord.getFieldValue('custrecord_container');
	var ItemAttribute = opentaskrecord.getFieldValue('custrecord_ebiz_item_attribute');
	//newly added
	//CASE# -20124508- Start
	var footprint= opentaskrecord.getFieldValue('custrecord_ebizfootprint');
	var openpro= opentaskrecord.getFieldValue('custrecord_pro');
	var bolvalue= opentaskrecord.getFieldValue('custrecord_bol');
	var subbol= opentaskrecord.getFieldValue('custrecord_sub_bol');
	var wmscarrier=opentaskrecord.getFieldValue('custrecord_ebizwmscarrier');
	var zoneid=opentaskrecord.getFieldValue('custrecord_ebiz_zoneid');
	var skip=opentaskrecord.getFieldValue('custrecord_skiptask');
	var taskpriority=opentaskrecord.getFieldValue('custrecord_taskpriority');
	var reversalqty=opentaskrecord.getFieldValue('custrecord_reversalqty');
	var binlocgrpsequence=opentaskrecord.getFieldValue('custrecord_bin_locgroup_seq');
	var expecteditem=opentaskrecord.getFieldValue('custrecord_ebiz_expected_item');
	var kitexceptionflag=opentaskrecord.getFieldValue('custrecord_kit_exception_flag');
	var errorlog=opentaskrecord.getFieldValue('custrecord_ebiz_error_log');
	var zoneid=opentaskrecord.getFieldValue('custrecord_ebiz_zoneid');

	//added fileds from active dev
	var hostid = opentaskrecord.getFieldValue('custrecord_hostid');	
	var footprint=opentaskrecord.getFieldValue('custrecord_ebizfootprint');
	var newlp=opentaskrecord.getFieldValue('custrecord_ebiz_new_lp');	
	var customer=opentaskrecord.getFieldValue('custrecord_ebiz_customer');
	var pacverify=opentaskrecord.getFieldValue('custrecord_pack_verification_flag');
	var timesecond=opentaskrecord.getFieldValue('custrecord_time_second');
	var ActSolocation= opentaskrecord.getFieldValue('custrecord_ebiz_act_solocation');
	var vbulkpickflag = opentaskrecord.getFieldValue('custrecord_bulk_pick_flag');
	var vshipasisflag = opentaskrecord.getFieldValue('custrecord_ebiz_shipasis');
	var vintegrationstatusflag = opentaskrecord.getFieldValue('custrecord_ebiz_integrationstatus');
	var vprocessflag = opentaskrecord.getFieldValue('custrecord_ebiz_processflag');

	//added by surendra
	var Expectedbinsize1=opentaskrecord.getFieldValue('custrecord_ebiz_expbinsize1');
	var Expectedbinsize2=opentaskrecord.getFieldValue('custrecord_ebiz_expbinsize2');
	//end
	t1 = new Date();
	var ebiztaskrecord = nlapiCreateRecord('customrecord_ebiznet_trn_ebiztask');
	t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiCreateRecord:MoveTaskRecord:trn_ebiztask',
			getElapsedTimeDuration(t1, t2));

	nlapiLogExecution('Debug', 'Creating Task Record', 'TRN_EBIZTASK');

	if(Skuno== null || Skuno == '')
		Skuno=parentSKU;

	ebiztaskrecord.setFieldValue('custrecord_ebiz_bulk_pick_flag',vbulkpickflag);
	ebiztaskrecord.setFieldValue('custrecord_ebiz_ctshipasis  ',vshipasisflag);

	//newly added
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebizfootprint',footprint);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_bol',bolvalue);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_pro',openpro);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_sub_bol',subbol);
	//
	//CASE# -20124508- End
	ebiztaskrecord.setFieldValue('name', TranId);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_act_begin_date', ActBeginDate);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_act_end_date', ActEndDate);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_act_qty', parseFloat(ActQty).toFixed(4));
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_batch_no', BatchNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_comp_id', CompId);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_contlp_no', containerSizeId);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_crossdock', crossdockflag);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_crossdock_qty', parseFloat(crossdockqty).toFixed(4));
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_current_date', currentdate);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_device_upload_flag', deviceuploadflag);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_appowner_no', ebizappownno);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_cntrl_no', eBizCntrlNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_contlp_no', ContLPNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_lpno', eBizLpNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_ship_lp_no', eBizShipLpNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_sku', Skuno);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_skudesc', SkuDesc);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_sku_no', eBizSkuNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_task_no', eBizTaskNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_trailer_no', eBizTrailerNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_wave_no', eBizWaveNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_expe_qty', parseFloat(ExpeQty).toFixed(4));
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_line_no', LineNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_lpno', LPNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_masterlp', masterLp);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_packcode', PackCode);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ship_lp_no', ShipLpNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_site_id', SiteId);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_sku_status', SkuStatus);
	//ebiztaskrecord.setFieldValue('custrecord_ebiztask_wms_status_flag', StatusFlag);
	if(TaskType=='3' && StatusFlag!='26' && StatusFlag!='30')
		ebiztaskrecord.setFieldValue('custrecord_ebiztask_wms_status_flag', '14');
	else	
		ebiztaskrecord.setFieldValue('custrecord_ebiztask_wms_status_flag', StatusFlag);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_tasktype', TaskType);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_totalcube', parseFloat(TotalCube).toFixed(4));
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_totaltime', TotalTime);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_total_weight', parseFloat(TotalWght).toFixed(4));
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_uom_id', UOMID);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_upd_date', UpdDate);
	//ebiztaskrecord.setFieldValue('custrecord_ebiztask_upd_ebiz_user_no', currentUserID);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_uom_level', UOMLevel);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_receipt_no', eBizRecptNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_notes', Notes);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_actbeginloc', ActBeginLoc);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_actendloc', ActEndLoc);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_actualbegintime', ActBeginTime);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_actualendtime', ActEndTime);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_recordtime', RecordTime);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_recordupdatetime', RecordUpdTime);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_expirydate', expirydate);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_fifodate', FifoDate);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_modelno', modelno);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebizallocatedqty', parseFloat(AllocQty).toFixed(4));
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_pick_qty', parseFloat(PickQty).toFixed(4));
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_invref_no', RefNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_from_lp_no', FromLP);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_order_no', eBizOrdNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_wms_location', wmslocation);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_rule_no', ebizruleno);//
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_method', ebizmethodno);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_zone_no', ebizZoneNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_cluster_no', clusterno);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_parent_item_no', parentSKU);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_lastmember_component', lastmember);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_kit_reference_no', KitRefNo);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_serial_no', serialno);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_lotno_with_quantity', lotwithquantity);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_transport_lp', TranLp);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebizuser_no', User);	//empId
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_taskassgndid', empId);	//Ship LP No
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebiz_ship_lp_no', mastShipLP);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_mast_ebizlp_no', ebizLpNo);
	//added by surendra
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_expbinsize1', Expectedbinsize1);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_expbinsize2', Expectedbinsize2);
	//end

	// case no start 20126855,20126892,20126893
	if(nsconfirmRefNo != null && nsconfirmRefNo != '')
		ebiztaskrecord.setFieldValue('custrecord_ebiz_nsconf_refno_ebiztask', nsconfirmRefNo);//BY LP
	else if((nsRefNo != null && nsRefNo != '') && (nsconfirmRefNo == null || nsconfirmRefNo == '' ))
		ebiztaskrecord.setFieldValue('custrecord_ebiz_nsconf_refno_ebiztask', nsRefNo);//By PO

	// case no end 20126855,20126892,20126893
	//added packconfirmed date and containersize id by suman
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_pack_confirmed_date', packconfirmedDate);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_container_size_id', containerSizeId);
	ebiztaskrecord.setFieldValue('custrecord_ebiz_closetask_itemattribute', ItemAttribute);

	if(zoneid!=null && zoneid!='')
		ebiztaskrecord.setFieldValue('custrecord_ebiztask_zoneid', zoneid);

	if(ActSolocation!=null && ActSolocation!='')
		ebiztaskrecord.setFieldValue('custrecord_ebiztask_act_solocation', ActSolocation);


	//added fileds from active dev
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_hostid', hostid);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_ebizfootprint', footprint);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_new_lp', newlp);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_customer', customer);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_pack_verify_flag', pacverify);
	ebiztaskrecord.setFieldValue('custrecord_ebiztask_time_second', timesecond);

	ebiztaskrecord.setFieldValue('custrecord_ebiz_ctprocessflag', vprocessflag);
	ebiztaskrecord.setFieldValue('custrecord_ebiz_ctintegrationstatus', vintegrationstatusflag);



	nlapiLogExecution('Debug', 'Submitting Task record', 'TRN_EBIZTASK');
	//commit the record to NetSuite
	t1 = new Date();
	var recid = nlapiSubmitRecord(ebiztaskrecord);
	t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiSubmitRecord:MoveTaskRecord:trn_ebiztask',
			getElapsedTimeDuration(t1, t2));

	nlapiLogExecution('Debug', 'Done CHKN Record Insertion :', 'Success');

	t1 = new Date();
	var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', RecordID);
	t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiDeleteRecord:MoveTaskRecord:trn_opentask',
			getElapsedTimeDuration(t1, t2));

	var timeStamp2 = new Date();
	nlapiLogExecution('Debug', 'MoveTaskRecord Duration', getElapsedTimeDuration(timeStamp1, timeStamp2));

	return recid;
}

/**
 * 
 * @param orderType
 * @returns {String}
 */
function getFilterForOrderType(orderType){
	var retFilter = "";
	if(orderType == 'purchaseorder')
		retFilter = new nlobjSearchFilter('custrecord_orderlinedetails_ord_category', null, 'anyof', [2]);
	else if(orderType == 'salesorder')
		retFilter = new nlobjSearchFilter('custrecord_orderlinedetails_ord_category', null, 'anyof', [3]);
	else if(orderType == 'returnauthorization')
		retFilter = new nlobjSearchFilter('custrecord_orderlinedetails_ord_category', null, 'anyof', [2]);
	else if(orderType == 'transferorder')
		retFilter = new nlobjSearchFilter('custrecord_orderlinedetails_ord_category', null, 'anyof', [2]);

	return retFilter;
}

/**
 * 
 * @param transType
 * @returns {String}
 */
function getColumnForTransactionType(taskType){
	var retColumn = "";

	if(taskType == "CHKN")
		retColumn = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	else if(taskType == "ASPW")
		retColumn = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');
	else if(taskType == "PUTW")
		retColumn = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');
	else if(taskType == "PICKG")
		retColumn = new nlobjSearchColumn('custrecord_orderlinedetails_pickgen_qty');
	else if(taskType == "PICKC")
		retColumn = new nlobjSearchColumn('custrecord_orderlinedetails_pickconf_qty');
	else if(taskType == "SHIP")
		retColumn = new nlobjSearchColumn('custrecord_orderlinedetails_ship_qty');

	return retColumn;
}

/**
 * 
 * @param transType
 * @returns {String}
 */
function getFieldArrayValueForTransType(transType){
	var retFieldType = "";

	if(transType == "CHKN")
		retFieldType = 'custrecord_orderlinedetails_checkin_qty';
	else if(transType == "ASPW")
		retFieldType = 'custrecord_orderlinedetails_putgen_qty';
	else if(transType == "PUTW")
		retFieldType = 'custrecord_orderlinedetails_putconf_qty';	
	else if(transType == "PICKG")
		retFieldType = 'custrecord_orderlinedetails_pickgen_qty';
	else if(transType == "PICKC")
		retFieldType = 'custrecord_orderlinedetails_pickconf_qty';
	else if(transType == "SHIP")
		retFieldType = 'custrecord_orderlinedetails_ship_qty';

	return retFieldType;
}

/**
 * 
 * @param transRecord
 * @param orderType
 */
function setOrderLineDetailsOrderCategory(transRecord, orderType){
	if(orderType == "purchaseorder"||orderType == 'returnauthorization'||orderType == 'transferorder')
		transRecord.setFieldValue('custrecord_orderlinedetails_ord_category', 2);
	else if(orderType == "salesorder")
		transRecord.setFieldValue('custrecord_orderlinedetails_ord_category', 3);
}

/**
 * 
 * @param transRecord
 * @param transType
 * @param confQty
 */
function setOrderLineDetailsOrderQuantity(transRecord, transType, confQty){
	if (transType == "CHKN") {
		transRecord.setFieldValue('custrecord_orderlinedetails_checkin_qty', parseFloat(confQty).toFixed(4)); 
	}else if (transType == "ASPW") {
		transRecord.setFieldValue('custrecord_orderlinedetails_putgen_qty', parseFloat(confQty).toFixed(4));
	}else if (transType == "PUTW") {
		transRecord.setFieldValue('custrecord_orderlinedetails_putconf_qty', parseFloat(confQty).toFixed(4));
	}else if (transType == "PICKG") {
		transRecord.setFieldValue('custrecord_orderlinedetails_pickgen_qty', parseFloat(confQty).toFixed(4));
	}else if (transType == "PICKC") {
		transRecord.setFieldValue('custrecord_orderlinedetails_pickconf_qty', parseFloat(confQty).toFixed(4));
	}else if (transType == "SHIP") {
		transRecord.setFieldValue('custrecord_orderlinedetails_ship_qty', parseFloat(confQty).toFixed(4));
	}
}

/**
 * 
 * @param orderType
 * @param ttype
 * @param poValue
 * @param poid
 * @param lineno
 * @param ItemId
 * @param quan
 * @param confqty
 * @returns {Boolean}
 */

function TrnLineUpdation(orderType, ttype, poValue, poid, lineno, ItemId, quan, confqty,chkAssignputW,ItemStatus,vTrnMgmtLevel){
	nlapiLogExecution('DEBUG','orderType',orderType);
	nlapiLogExecution('DEBUG','ttype',ttype);
	nlapiLogExecution('DEBUG','poValue',poValue);
	nlapiLogExecution('DEBUG','poid',poid);
	nlapiLogExecution('DEBUG','lineno',lineno);
	nlapiLogExecution('DEBUG','ItemId',ItemId);
	nlapiLogExecution('DEBUG','quan',quan);
	nlapiLogExecution('DEBUG','confqty',confqty);
	nlapiLogExecution('DEBUG','chkAssignputW',chkAssignputW);
	nlapiLogExecution('DEBUG','ItemStatus',ItemStatus);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poid));
	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));

	// get filter based on order type
	if(orderType!='transferorder')
	{
		filters.push(getFilterForOrderType(orderType));
	}


	nlapiLogExecution('DEBUG', 'filters[0]', poid);
	nlapiLogExecution('DEBUG', 'filters[1]', lineno);


	nlapiLogExecution('DEBUG', 'ItemStatus status', ItemStatus);

	var columns = new Array();
	columns[0] = getColumnForTransactionType(ttype);
	columns[1] = getColumnForTransactionType("ASPW");
	columns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');
	columns[3] = getColumnForTransactionType("CHKN");

	var itemstatus = 'GOOD';//itemRec.getFieldValue('customrecord_ebiznet_sku_status');
	var uomid = 'EACH';

	var t1 = new Date();
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);
	nlapiLogExecution('DEBUG','chkpt','sucess');
	nlapiLogExecution('DEBUG','searchResults',searchResults);
	var t2 = new Date();
	nlapiLogExecution('DEBUG', 'nlapiSearchRecord:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t1, t2));

	if(searchResults != null){
		var trnId = searchResults[0].getId();
		var lastConfQty = 0;// SearchRecd[0].getValue('custrecord_orderlinedetails_checkin_qty');
		var lastordqty = 0;
		nlapiLogExecution('DEBUG','trnId',trnId);
		var fldArray = new Array();
		fldArray[0] = 'custrecord_orderlinedetails_ebiz_ord_no';
		fldArray[1] = 'custrecord_orderlinedetails_orderline_no';
		fldArray[2] = 'custrecord_orderlinedetails_record_date';
		fldArray[3] = 'custrecord_orderlinedetails_record_time';


		//if(quan == null || quan == '')
		//	quan=0;


		var valArray = new Array();
		valArray[0] = poid;
		valArray[1] = lineno;
		valArray[2] = DateStamp();
		valArray[3] = TimeStamp();
		//if(quan != null || quan == '')
		if(orderType == 'transferorder' && ttype == 'PICKC')
		{
			//fldArray[4] = 'custrecord_orderlinedetails_order_qty';
			fldArray.push('custrecord_orderlinedetails_order_qty');
			//lastordqty = searchResults[0].getValue(fldArray[4]);
			lastordqty = searchResults[0].getValue('custrecord_orderlinedetails_order_qty');
			nlapiLogExecution('DEBUG','lastordqty beforeert',lastordqty);
			if(lastordqty == null || lastordqty == "")
				lastordqty = 0;

			nlapiLogExecution('DEBUG','lastordqty in',lastordqty);
			nlapiLogExecution('DEBUG','quan in ',quan);
			//valArray[4] = (parseFloat(lastordqty) + parseFloat(quan)).toFixed(4);
			valArray.push((parseFloat(lastordqty) + parseFloat(quan)).toFixed(4));
		}
		//fldArray[5] = getFieldArrayValueForTransType(ttype);
		fldArray.push(getFieldArrayValueForTransType(ttype));
		//lastConfQty = searchResults[0].getValue(fldArray[5]);
		lastConfQty = searchResults[0].getValue(getFieldArrayValueForTransType(ttype));
		nlapiLogExecution('DEBUG','lastConfQty is',lastConfQty);//sri
		if(lastConfQty == null || lastConfQty == "")
			lastConfQty = 0;
		/*// Case# 20148402 starts
		if(ttype=="CHKN")
			valArray[5] = parseFloat(lastConfQty);
		else
			valArray[5] = parseFloat(lastConfQty) + parseFloat(confqty);
		// Case# 20148402 ends*/	
		valArray.push(parseFloat(lastConfQty) + parseFloat(confqty));
		if(chkAssignputW=="Y")
		{
			ttype="ASPW";
			//fldArray[6] = getFieldArrayValueForTransType(ttype);
			fldArray.push(getFieldArrayValueForTransType(ttype));
			nlapiLogExecution("ERROR", "getFieldArrayValueForTransType(ttype)", getFieldArrayValueForTransType(ttype));
			//lastConfQty = searchResults[0].getValue("'"+fldArray[5]+"'");
			//lastConfQty = searchResults[0].getValue(fldArray[6]);
			lastConfQty = searchResults[0].getValue(getFieldArrayValueForTransType(ttype));
			nlapiLogExecution("ERROR", "searchResults[0].getValue(getFieldArrayValueForTransType(ttype)) ", searchResults[0].getValue(getFieldArrayValueForTransType(ttype)));
			if(lastConfQty == null || lastConfQty == "")
				lastConfQty = 0;
			//valArray[6] = parseFloat(lastConfQty) + parseFloat(confqty);
			valArray.push(parseFloat(lastConfQty) + parseFloat(confqty));
		}

		nlapiLogExecution("ERROR", "TrnLineUpdation:Submitted Quantity valArray[4]", valArray[4]);//chkin Qty
		nlapiLogExecution("ERROR", "TrnLineUpdation:Submitted Quantity valArray[5]", valArray[5]);//order Qty
		nlapiLogExecution("ERROR", "TrnLineUpdation:Submitted Quantity valArray[6]", valArray[6]);//PutGen Qty
		if(ttype == "PUTW")
		{
			var orderQty = searchResults[0].getValue('custrecord_orderlinedetails_order_qty');
			//var Putgenqty= searchResults[0].getValue('custrecord_orderlinedetails_putgen_qty');
			var ChkQty= searchResults[0].getValue('custrecord_orderlinedetails_checkin_qty');
			var PutConfQty= parseFloat(lastConfQty) + parseFloat(confqty);
			if(ChkQty==null || ChkQty =='' ||isNaN(ChkQty)|| ChkQty<0)
				ChkQty=0;
			//var totalQty = parseFloat(Putgenqty)+ parseFloat(PutConfQty);
			nlapiLogExecution("ERROR", "orderQty", orderQty);
			//nlapiLogExecution("ERROR", "Putgenqty", Putgenqty);
			nlapiLogExecution("ERROR", "PutConfQty", PutConfQty);
			nlapiLogExecution("ERROR", "ChkQty", ChkQty);
			if(parseFloat(ChkQty)> parseFloat(PutConfQty))
			{
				//fldArray[7] = 'custrecord_orderlinedetails_putexcep_qty';
				fldArray.push('custrecord_orderlinedetails_putexcep_qty');
				//valArray[7] = parseFloat(ChkQty)- parseFloat(PutConfQty);
				valArray.push(parseFloat(ChkQty)- parseFloat(PutConfQty));
				nlapiLogExecution("ERROR", "ExcepQty", parseFloat(ChkQty)- parseFloat(PutConfQty));
			}
			else if(parseFloat(ChkQty)== parseFloat(PutConfQty))
			{
				nlapiLogExecution("ERROR", "ChkQty/PutConfQty", 'equal');
				//fldArray[7] = 'custrecord_orderlinedetails_putexcep_qty';
				fldArray.push('custrecord_orderlinedetails_putexcep_qty');
				//valArray[7] = 0;
				valArray.push(0);
				nlapiLogExecution("ERROR", "ExcepQty", '0');
			}

		}

		var t3 = new Date();
		nlapiSubmitField('customrecord_ebiznet_order_line_details', searchResults[0].getId(), fldArray, valArray);
		var t4 = new Date();
		nlapiLogExecution('DEBUG', 'nlapiSubmitField:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t3, t4));
	}else{
		var t5 = new Date();
		var createTranRec = nlapiCreateRecord('customrecord_ebiznet_order_line_details');
		var t6 = new Date();
		nlapiLogExecution('DEBUG', 'nlapiCreateRecord:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t5, t6));
		nlapiLogExecution('DEBUG', 'Item Status', itemstatus);
		nlapiLogExecution('DEBUG', 'UOM ID', uomid);
		nlapiLogExecution('DEBUG', 'PO Value', poValue);
		if(poValue!=''&& poValue!=null)
		{
			nlapiLogExecution('DEBUG', 'PO', poValue);
			createTranRec.setFieldValue('name', poValue);		
			createTranRec.setFieldValue('custrecord_orderlinedetails_order_no', poValue);
		}

		createTranRec.setFieldValue('custrecord_orderlinedetails_ebiz_ord_no', parseFloat(poid));
		createTranRec.setFieldValue('custrecord_orderlinedetails_orderline_no', lineno);
		createTranRec.setFieldValue('custrecord_orderlinedetails_item', ItemId);
//		createTranRec.setFieldValue('custrecord_orderlinedetails_item_status', 1);
		nlapiLogExecution('DEBUG', 'Item Status', ItemStatus);
		if(ItemStatus!=null && ItemStatus!="")
			createTranRec.setFieldText('custrecord_orderlinedetails_item_status', ItemStatus);
		createTranRec.setFieldValue('custrecord_orderlinedetails_uom_id', uomid);
		createTranRec.setFieldValue('custrecord_orderlinedetails_ebiz_sku_no', ItemId);
		createTranRec.setFieldValue('custrecord_orderlinedetails_order_qty', parseFloat(quan).toFixed(4));
		// set order category depending on the order type
		setOrderLineDetailsOrderCategory(createTranRec, orderType);
		setOrderLineDetailsOrderQuantity(createTranRec, ttype, confqty);
		if(chkAssignputW=="Y")
		{
			setOrderLineDetailsOrderQuantity(createTranRec, "ASPW", confqty);
		}

		createTranRec.setFieldValue('custrecord_orderlinedetails_record_date', DateStamp());
		createTranRec.setFieldValue('custrecord_orderlinedetails_record_time', TimeStamp());

		t5 = new Date();
		var tranRecId = nlapiSubmitRecord(createTranRec);
		t6 = new Date();
		nlapiLogExecution('DEBUG', 'nlapiSubmitRecord:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t5, t6));
	}
	return true;
}

/**
 * 
 * @param item
 * @param itemGroup
 * @param itemFamily
 * @returns {Array}
 */
function getPickRulesForItem(item, itemGroup, itemFamily){
	var pickRuleSearchResults = new Array();

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@', item]);
	var k = 1;
	if(itemGroup != null && itemGroup != ""){
		filters[k] = new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', itemGroup]);
		k = k + 1;
	}

	if(itemFamily != null && itemFamily != ""){
		filters[k] = new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', itemFamily]);
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	pickRuleSearchResults = nlobjSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);
	return pickRuleSearchResults;
}

/**
 * 
 * @param pickZone
 * @returns {Array}
 */
function getLocnGroupsForPickZone(pickZone){
	var locnGroupList = new Array();

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', pickZone, null);
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_locgroup_no');

	locnGroupList = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
	return locnGroupList;
}

/**
 * 
 * @param item
 * @param itemfamily
 * @param itemgroup
 * @param avlqty
 * @returns {Array}
 */
function PickStrategieKittoOrder(item, itemfamily, itemgroup, avlqty){

	nlapiLogExecution('Debug', 'into ', 'PickStrategieKittoOrder');
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('Debug', 'item', item);
	nlapiLogExecution('Debug', 'itemgroup', itemgroup);
	nlapiLogExecution('Debug', 'itemfamily', itemfamily);

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@', item]);
	var k = 1;
	if (itemgroup != "" && itemgroup != null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', itemgroup]);
		k = k + 1;
	}
	if (itemfamily != "" && itemfamily != null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', itemfamily]);
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	//Fetching pick rule
	nlapiLogExecution('Debug', 'Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);
	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		var searchresultpick = searchresults[i];
		var vpickzone = searchresultpick.getValue('custrecord_ebizpickzonerul');
		nlapiLogExecution('Debug', 'PickZone', vpickzone);
		var filterszone = new Array();
		filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', vpickzone, null);

		var columnzone = new Array();
		columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');

		var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
		nlapiLogExecution('Debug', 'Loc Group Fetching');
		//fetching loc group
		for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {

			var searchresultzone = searchresultszone[j];
			var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');

			nlapiLogExecution('Debug', 'LocGroup' + searchresultzone.getValue('custrecord_locgroup_no'), searchresultzone.getText('custrecord_locgroup_no'));
			var filtersinvt = new Array();

			filtersinvt[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item);
			filtersinvt[1] = new nlobjSearchFilter('custrecord_outboundinvlocgroupid', null, 'is', vlocgroupno);

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno');
			columnsinvt[5].setSort();

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
				var Recid = searchresult.getId();
				if (isNaN(allocqty)) {
					allocqty = 0;
				}
				if (allocqty == "") {
					allocqty = 0;
				}
				if (parseFloat(actqty) < 0) {
					actqty = 0;
				}
				nlapiLogExecution('Debug', "allocqty: " + allocqty);
				nlapiLogExecution('Debug', "LP: " + LP);

				var remainqty = actqty - allocqty;
				nlapiLogExecution('Debug', 'remainqty' + remainqty);

				var cnfmqty = 0;
				nlapiLogExecution('Debug', 'LOOP BEGIN');

				if (remainqty > 0) {
					nlapiLogExecution('Debug', 'INTO LOOP');
					if ((avlqty - actallocqty) <= remainqty) {
						cnfmqty = avlqty - actallocqty;
						actallocqty = avlqty;
					}
					else {
						cnfmqty = remainqty;
						actallocqty = actallocqty + remainqty;
					}

					if (cnfmqty > 0) {
						//Resultarray
						var invtarray = new Array();
						invtarray[0] = cnfmqty;
						invtarray[1] = vactLocation;
						invtarray[2] = LP;
						invtarray[3] = Recid;
						//invtarray[3] = vpackcode;
						//invtarray[4] = vskustatus;
						//invtarray[5] = vuomid;
						Resultarray.push(invtarray);
					}


				}

				if ((avlqty - actallocqty) == 0) {
					nlapiLogExecution('Debug', 'Into Break:');
					return Resultarray;
				}
			}
		}
	}
	return Resultarray;
}

/**
 * 
 * @param value
 * @returns {String}
 */
/*function RFDateFormat(value){
	if (value) {
		var dateArray=new Array();
		var month = value.substring(0, 2);

		//Code modified on 24Feb 2012 by suman.
		//parseFloat(month,10):- since in java script parseFloat(09) will return NaN so to overcome v have added 10 as its base.
		//so when ever user enter value between 1-9 parseFloat will convert it to base 10 and returns the same what the user have entered. 
		nlapiLogExecution('Debug','month',parseFloat(month,10));
		if(parseFloat(month,10)>0&&parseFloat(month,10)<13)
		{
			var date = value.substring(2, 4);
			nlapiLogExecution('Debug','date',parseFloat(date,10));
			var year = value.substring(4);
			nlapiLogExecution('Debug','year',parseFloat(year,10));
			if(((parseFloat(month,10)==2)||(parseFloat(month,10)==02))&&(parseFloat(year,10)%4==0))
			{
				if((parseFloat(date,10)>0)&&(parseFloat(date,10)<=29))
				{
					var now = new Date();
					var yearPrefix = now.getFullYear().toString();
					yearPrefix = yearPrefix.substring(0, 2);
					var dated=month + "/" + date + "/" + yearPrefix + year;
					dateArray[0]='true';
					dateArray[1]= dated;
					return dateArray;
				}
				else
				{
					nlapiLogExecution('Debug','Debug','In a leap year ,in the month of Feb date:'+date+'will not exist');
					var error=' In the month of Feb date: '+date+' will not exist ;';
					dateArray[0]='false';
					dateArray[1]= error;
					return dateArray;
				}
			}
			else if(((parseFloat(month,10)==2)||(parseFloat(month,10)==02))&&(parseFloat(year,10)%4!=0))
			{
				if((parseFloat(date,10)>0)&&(parseFloat(date,10)<=28))
				{
					var now = new Date();
					var yearPrefix = now.getFullYear().toString();
					yearPrefix = yearPrefix.substring(0, 2);
					var dated=month + "/" + date + "/" + yearPrefix + year;
					dateArray[0]='true';
					dateArray[1]= dated;
					return dateArray;
				}
				else
				{
					nlapiLogExecution('Debug','Debug','In a non leap year,in month of Feb date:'+date+'will not exist');
					var error=' In the month of Feb date: '+date+' will not exist ;';
					dateArray[0]='false';
					dateArray[1]= error;
					return dateArray;
				}


			}
			else if((parseFloat(date,10)>0)&&(parseFloat(date,10)<=31))
			{
				var now = new Date();
				var yearPrefix = now.getFullYear().toString();
				yearPrefix = yearPrefix.substring(0, 2);
				var dated=month + "/" + date + "/" + yearPrefix + year;

				if ((month==4 || month==6 || month==9 || month==11) && parseFloat(date,10)==31) {
					nlapiLogExecution('Debug','Month doesnt have 31 days!',month);
					var error = 'Month doesnt have 31 days';
					dateArray[0]='false';
					dateArray[1]= error;
					return dateArray;
				}
				else
				{			
					dateArray[0]='true';
					dateArray[1]= dated;
					return dateArray;
				}

			}
			else
			{
				nlapiLogExecution('Debug','Debug','In a Year/Month Date:'+date+' doesnt exist ');
				var error=' In a Month, with Date: '+date+' doesnt exist ;';
				dateArray[0]='false';
				dateArray[1]= error;
				return dateArray;

			}

		}
		else
		{
			nlapiLogExecution('Debug','Debug','month:'+month+' enterd doesnt exist ');
			var error=' In a Year of month: '+month+' doesnt exist ;';
			dateArray[0]='false';
			dateArray[1]= error;
			return dateArray;
		}
	}
}*/

function RFDateFormat(value,dtsettingFlag){
	if (value) {
		var dateArray=new Array();


		if(dtsettingFlag != null && dtsettingFlag != '')
		{
			if(dtsettingFlag == 'DD/MM/YYYY')
			{
				var date = value.substring(0, 2);
				var month = value.substring(2, 4);
				var year = value.substring(4);
			}
			else
			{
				var month = value.substring(0, 2);
				var date = value.substring(2, 4);
				var year = value.substring(4);

			}
		}
		else
		{
			var month = value.substring(0, 2);
			var date = value.substring(2, 4);
			var year = value.substring(4);

		}





		//Code modified on 24Feb 2012 by suman.
		//parseFloat(month,10):- since in java script parseFloat(09) will return NaN so to overcome v have added 10 as its base.
		//so when ever user enter value between 1-9 parseFloat will convert it to base 10 and returns the same what the user have entered. 
		nlapiLogExecution('Debug','month',parseFloat(month,10));
		if(parseFloat(month,10)>0&&parseFloat(month,10)<13)
		{
			nlapiLogExecution('Debug','date',parseFloat(date,10));
			nlapiLogExecution('Debug','year',parseFloat(year,10));
			if(((parseFloat(month,10)==2)||(parseFloat(month,10)==02))&&(parseFloat(year,10)%4==0))
			{
				if((parseFloat(date,10)>0)&&(parseFloat(date,10)<=29))
				{
					var now = new Date();
					var yearPrefix = now.getFullYear().toString();
					yearPrefix = yearPrefix.substring(0, 2);
					if(dtsettingFlag == 'DD/MM/YYYY')
						var dated=date + "/" + month + "/" + yearPrefix + year;
					else
						var dated=month + "/" + date + "/" + yearPrefix + year;
					dateArray[0]='true';
					dateArray[1]= dated;
					return dateArray;
				}
				else
				{
					nlapiLogExecution('Debug','Debug','In a leap year ,in the month of Feb date:'+date+'will not exist');
					var error=' In the month of Feb date: '+date+' will not exist ;';
					dateArray[0]='false';
					dateArray[1]= error;
					return dateArray;
				}
			}
			else if(((parseFloat(month,10)==2)||(parseFloat(month,10)==02))&&(parseFloat(year,10)%4!=0))
			{
				if((parseFloat(date,10)>0)&&(parseFloat(date,10)<=28))
				{
					var now = new Date();
					var yearPrefix = now.getFullYear().toString();
					yearPrefix = yearPrefix.substring(0, 2);
					if(dtsettingFlag == 'DD/MM/YYYY')
						var dated=date + "/" + month + "/" + yearPrefix + year;
					else
						var dated=month + "/" + date + "/" + yearPrefix + year;;
						dateArray[0]='true';
						dateArray[1]= dated;
						return dateArray;
				}
				else
				{
					nlapiLogExecution('Debug','Debug','In a non leap year,in month of Feb date:'+date+'will not exist');
					var error=' In the month of Feb date: '+date+' will not exist ;';
					dateArray[0]='false';
					dateArray[1]= error;
					return dateArray;
				}


			}
			else if((parseFloat(date,10)>0)&&(parseFloat(date,10)<=31))
			{
				var now = new Date();
				var yearPrefix = now.getFullYear().toString();
				yearPrefix = yearPrefix.substring(0, 2);
				if(dtsettingFlag == 'DD/MM/YYYY')
					var dated=date + "/" + month + "/" + yearPrefix + year;
				else
					var dated=month + "/" + date + "/" + yearPrefix + year;

				if ((month==4 || month==6 || month==9 || month==11) && parseFloat(date,10)==31) {
					nlapiLogExecution('Debug','Month doesnt have 31 days!',month);
					var error = 'Month doesnt have 31 days';
					dateArray[0]='false';
					dateArray[1]= error;
					return dateArray;
				}
				else
				{			
					dateArray[0]='true';
					dateArray[1]= dated;
					return dateArray;
				}

			}
			else
			{
				nlapiLogExecution('Debug','Debug','In a Year/Month Date:'+date+' doesnt exist ');
				var error=' In a Month, with Date: '+date+' doesnt exist ;';
				dateArray[0]='false';
				dateArray[1]= error;
				return dateArray;

			}

		}
		else
		{
			nlapiLogExecution('Debug','Debug','month:'+month+' enterd doesnt exist ');
			var error=' In a Year of month: '+month+' doesnt exist ;';
			dateArray[0]='false';
			dateArray[1]= error;
			return dateArray;
		}

	}
}

/**
 * Depending on the FIFO policy for an item, determine the FIFO value
 * @param itemType
 * @param itemID
 * @param itemFamilyID
 * @param itemGroupID
 * @param lineNo
 * @param poID
 * @param lotNoId
 * @returns
 */
function FifovalueCheck(itemType, itemID, itemFamilyID, itemGroupID, lineNo, poID, lotNoId)
{
	var fifoval=null;
	nlapiLogExecution('Debug', 'Into FifovalueCheck in General Functions');
	nlapiLogExecution('Debug', 'Item Type',itemType);
	nlapiLogExecution('Debug', 'Item ID',itemID);
	nlapiLogExecution('Debug', 'Item Family',itemFamilyID);
	nlapiLogExecution('Debug', 'Item Group',itemGroupID);
	nlapiLogExecution('Debug', 'Line No',lineNo);
	nlapiLogExecution('Debug', 'PO ID',poID);
	nlapiLogExecution('Debug', 'Lot ID',lotNoId);

	var fifoItemCheck = nlapiLookupField(itemType, itemID, 'custitem_ebizfifopolicy');
	nlapiLogExecution('Debug', 'FIFO @ Item Level', fifoItemCheck);

	if ((fifoItemCheck == "" || fifoItemCheck == null)) 
	{
		// Get FIFO policy for Item Group
		if(itemGroupID!=null && itemGroupID!='')
			fifoItemCheck = nlapiLookupField('customrecord_ebiznet_sku_group', itemGroupID, 'custrecord_fifopolicyskugrp');//Item group.

		nlapiLogExecution('Debug', 'FIFO @ Item Group Level', fifoItemCheck);
		if (fifoItemCheck == null || fifoItemCheck == "")
		{

			if(itemFamilyID!=null && itemFamilyID!='')
				// Get FIFO policy for item family
				fifoItemCheck = nlapiLookupField('customrecord_ebiznet_sku_family', itemFamilyID, 'custrecord_fifopolicy');// Item group.

			nlapiLogExecution('Debug', 'FIFO @ Item Family Level', fifoItemCheck);
//			case# 201411655
			if (fifoItemCheck != null && fifoItemCheck != "") 
			{
				fifoval = ValidateCall(fifoItemCheck, lineNo, poID, lotNoId);
				nlapiLogExecution('Debug', 'Return fifoval ', fifoval);
			}
		}
		else 
		{
			fifoval = ValidateCall(fifoItemCheck, lineNo, poID, lotNoId);
			nlapiLogExecution('Debug', 'Return fifoval ', fifoval);
		}
	}
	else 
	{
		fifoval = ValidateCall(fifoItemCheck, lineNo, poID, lotNoId);
		nlapiLogExecution('Debug', 'Return fifoval ', fifoval);
	}
	return fifoval;
}

/**
 * 
 * @param fifoItemCheck
 * @param putLine
 * @param putPOId
 * @param lotNoId
 * @returns
 */
function ValidateCall(fifoItemCheck, putLine, putPOId, lotNoId){
	var val;
	var expiryDateInLot;
	nlapiLogExecution('Debug', 'ValidateCall', 'Started');
	nlapiLogExecution('Debug', 'lotNoId', lotNoId);

	if (fifoItemCheck == '1') //Daily
		val = Daily(putLine, putPOId, lotNoId);
	else if(fifoItemCheck == '2'){ // EXPIRATION

		if(lotNoId!=null && lotNoId!='')
		{
			// get expiry date for the lot no.
			expiryDateInLot = nlapiLookupField('customrecord_ebiznet_batch_entry', lotNoId, 'custrecord_ebizexpirydate');
		}
		val = expiryDateInLot;
		nlapiLogExecution('Debug', 'ValidateCall:expiryDate', expiryDateInLot);

	}
	else if(fifoItemCheck == '3' || fifoItemCheck == '5') // MONTHLY
		val = firstDateOfYear(putLine,putPOId,expiryDateInLot, fifoItemCheck);
	else
		val = Weekly(putLine, putPOId, lotNoId, expiryDateInLot);

	return val;
}

/**
 * Returns the date corresponding to the sunday when the week began.
 * @param lineNum
 * @param poID
 * @param lotNoID
 * @param dateInLot
 * @returns date for sunday
 */
function Weekly(lineNum, poID, lotNoID, dateInLot){
	if(dateInLot==null || dateInLot=='')
		dateInLot=Daily(lineNum, poID, lotNoID);
	var today = new Date(dateInLot);
	var dayOfWeek = today.getDay();
	var delta = today.getDate() - dayOfWeek;
	var sunday = new Date(today.setDate(delta));
	var date = sunday.getDate();
	var month = sunday.getMonth() + 1;
	var year = sunday.getYear() + 1900;

	var retStr = month + "/" + date + "/" + year;
	nlapiLogExecution('Debug', 'Weekly:Sunday', retStr);
	if(month=="NaN"||date=="NaN"||year=="NaN")
		retStr="";
	return retStr;
}


/**
 * Returns the day from the begin date, if the FIFO Policy is set as daily.
 * @param putLine
 * @param putPOId
 * @param lotNoId
 * @returns day
 */
function Daily(putLine, putPOId, lotNoId){
	nlapiLogExecution('Debug', 'Daily', putLine);
	var day = "";

	try{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'is', putPOId));
		// Case # 20144206 starts
		//filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [1])); //taskType = CHKN
		//filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', [1,17])); // wmsStatusFlag=INBOUND/CHECK-IN
		//case # 20148040 start
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', ['1','2']));
		//case # 20148040 end//taskType = CHKN/2=PUTW
		//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [1,2,17])); // wmsStatusFlag=INBOUND/CHECK-IN/2=Putaway Locations Assigned
		//case # 20148033 start
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', ['1','2','17']));
		//case # 20148033 end // wmsStatusFlag=INBOUND/CHECK-IN/2=Putaway Locations Assigned
		// Case # 20144206 ends
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_line_no', null, 'is', putLine));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiztask_act_begin_date');

		// execute the  search, passing null filter and return columns
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);
		if (searchresults!=null&&searchresults!="")
			day = searchresults[0].getValue('custrecord_ebiztask_act_begin_date');
		else
		{
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', putPOId));
			//filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1])); //taskType = CHKN
			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['1','2'])); //case# 201412219
			//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [1,17])); // wmsStatusFlag=INBOUND/CHECK-IN
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['1','2','17'])); //case# 201412219
			filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', putLine));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecordact_begin_date');

			// execute the  search, passing null filter and return columns
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			if (searchresults!=null&&searchresults!="")
				day = searchresults[0].getValue('custrecordact_begin_date');
		}
	}catch(exps) {
		nlapiLogExecution('Debug', 'Daily function for FIFO policy failed', exps);
	}
	nlapiLogExecution('Debug', 'day',day);
	return day;
}

/**
 * Returns the first of the month for a MONTHLY FIFO policy, otherwise returns 1 Jan of the year
 * @param dateInLot
 * @param fifoItemCheck
 * @returns
 */
function firstDateOfYear(putLine,putPOId,dateInLot, fifoItemCheck){
	var dateOfYear;
	// case# 201411630,201411627
	if(dateInLot==null || dateInLot=='')
		dateInLot=Daily(putLine, putPOId, '');

	var today = new Date(dateInLot);
	var month = today.getMonth() + 1;
	var year = today.getYear();

	if (year < 2000) 
		year = year + 1900;

	if (fifoItemCheck == '3') // MONTHLY
		dateOfYear = month + "/" + "1" + "/" + year;
	else
		dateOfYear = "1" + "/" + "1" + "/" + year;

	nlapiLogExecution('Debug', 'firstDateOfYear:First Date for FIFO Policy', dateOfYear);
	return dateOfYear;
}

/**
 * Remove duplicates from an array
 * @param arrayName
 * @returns {Array}
 */
function removeDuplicateElementOld(arrayName){
	var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < newArray.length; j++) {
			if (newArray[j] == arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}

/**
 * Remove duplicates from an array
 * @param arrayName
 * @returns {Array}
 */
function removeDuplicateElement(arr){
	var dups = {}; 
	return arr.filter(
			function(el) { 
				var hash = el.valueOf(); 
				var isDup = dups[hash]; 
				dups[hash] = true; 
				return !isDup; 
			}
	); 
}

/**
 * Returns the new maximum wave number for wave generation
 * @param transType
 * @returns cluster number
 */
function GetMaxTransactionNo(transType){

	var scount=1;
	LABL1: for(var i=0;i<scount;i++)
	{
		try
		{
			var clusterNo = 1;

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_seqno');

			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_trantype', null, 'is', transType);

			// Search in ebiznet_wave custom record for all rows for the given transaction type
			var results = nlapiSearchRecord('customrecord_ebiznet_wave', null, filters, columns);
			if(results != null){
				for (var t = 0; t < results.length; t++) {
					//clusterNo = results[t].getValue('custrecord_seqno');
					// Increment cluster number
					//clusterNo = parseFloat(clusterNo) + 1;
					var vWaveRec=nlapiLoadRecord('customrecord_ebiznet_wave',results[t].getId());
					clusterNo = vWaveRec.getFieldValue('custrecord_seqno');
					clusterNo = parseInt(clusterNo) + 1;
					vWaveRec.setFieldValue('custrecord_seqno', parseInt(clusterNo));			 
					nlapiSubmitRecord(vWaveRec,false,true);

//					var vWaveRec=nlapiLoadRecord('customrecord_ebiznet_wave',results[t].getId());
//					vWaveRec.setFieldValue('custrecord_seqno', parseInt(clusterNo));			 
//					nlapiSubmitRecord(vWaveRec,false,true);
					nlapiLogExecution('DEBUG', 'Cluster# Updated', clusterNo);
					//nlapiSubmitField('customrecord_ebiznet_wave', results[t].getId(), 'custrecord_seqno', parseFloat(clusterNo));
				}
			}

			nlapiLogExecution('Debug', 'GetMaxTransactionNo:New Cluster No.', clusterNo);
			return parseFloat(clusterNo);
		}
		catch(ex)
		{
			var exCode='CUSTOM_RECORD_COLLISION'; 
			var wmsE='Inventory record being updated by another user. Please try again...';
			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			} 

			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{ 
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;
		}
	}
}

/**
 * Generic function to return max LP number for a given LP Generation Type,LP Type based on NoOfLpsRequired parameter
 * Returns the new LP number based on the maximum LP number for a given LP Generation Type and LP Type
 * @param lpGenerationType
 * @param lpType
 * @param NoOfLpsRequired
 * @returns {String}
 */
function GetMultipleLP(lpGenerationType, lpType, NoOfLpsRequired,poloc)
{
	var maxLP = 1;
	var maxLPPrefix = "";

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpmax');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', lpGenerationType));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', lpType));
	if(poloc!=null && poloc!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', ['@NONE@', poloc]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var results = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);

	if(results != null)
	{
		if(results.length > 1)
		{    		
			nlapiLogExecution('Debug', 'GetMultipleLP:LP Max Query returned more than 1 row');
		}
		else
		{/*
			// Incrementing the max LP number only if the search returns 1 row
			for (var t = 0; t < results.length; t++)
			{
				if (results[t].getValue('custrecord_ebiznet_lprange_lpmax') != null) 
				{
					maxLP = results[t].getValue('custrecord_ebiznet_lprange_lpmax');
					maxLPPrefix = results[t].getValue('custrecord_ebiznet_lprange_lpprefix');   				
				}

				maxLP = parseInt(maxLP) + parseInt(NoOfLpsRequired);
				// update the new max LP in the custom record
				nlapiSubmitField('customrecord_ebiznet_lp_range', results[t].getId(),
						'custrecord_ebiznet_lprange_lpmax', parseInt(maxLP));
			}
		 */


			var scount=1;

			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var vLPRangeId=results[0].getId();
					var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', vLPRangeId);
					if(transaction.getFieldValue('custrecord_ebiznet_lprange_lpmax') != null)
					{	
						maxLP = transaction.getFieldValue('custrecord_ebiznet_lprange_lpmax');
						maxLPPrefix = transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix');
					}
					if(maxLP==null||maxLP==''||isNaN(maxLP))
						maxLP=0;
					maxLP = parseInt(maxLP) + parseInt(NoOfLpsRequired);
					transaction.setFieldValue('custrecord_ebiznet_lprange_lpmax', maxLP);
					nlapiSubmitRecord(transaction, true,true);
					nlapiLogExecution('ERROR', 'LP range updated for ref# ',vLPRangeId);

				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					}					 
					nlapiLogExecution('ERROR', 'Exception in Get Max LP : ', wmsE); 
					nlapiLogExecution('ERROR', 'exCode', exCode); 
					if(exCode=='CUSTOM_RECORD_COLLISION'  || exCode =='RCRD_HAS_BEEN_CHANGED'  || exCode=='UNEXPECTED_ERROR' )
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}
		}
	}

	maxLP = maxLP.toString();
	nlapiLogExecution('Debug', 'GetMaxLPNo:New MaxLP', maxLP);
	return maxLPPrefix + ","+ maxLP;
}

/**
 * Returns the new LP number based on the maximum LP number for a given LP Generation Type and LP Type
 * @param lpGenerationType
 * @param lpType
 * @returns {String}
 */
function GetMaxLPNo(lpGenerationType, lpType,whsite){
	//Case # 20127077� Start
	var maxLP = 0;
	//Case # 20127077� End
	var maxLPPrefix = "";
	// case # 20127118 starts
	// case # 20127118 end
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpmax');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', lpGenerationType));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lpType]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//Case #20127147 Start
	if(whsite!=null && whsite!='')
	{		
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', whsite));

	}
	//Case #20127147 End

	var results = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);

	if(results != null){
		if(results.length > 1){
			alert('More records returned than expected');
			nlapiLogExecution('Debug', 'GetMaxLPNo:LP Max Query returned more than 1 row');
		}else{
			//Commented for optimistiv locking
			/*			// Incrementing the max LP number only if the search returns 1 row
			for (var t = 0; t < results.length; t++){
				if (results[0].getValue('custrecord_ebiznet_lprange_lpmax') != null) {
					maxLP = results[0].getValue('custrecord_ebiznet_lprange_lpmax');
					maxLPPrefix = results[0].getValue('custrecord_ebiznet_lprange_lpprefix'); 


				}

				if(maxLP==null||maxLP==''||isNaN(maxLP))
					maxLP=0;

				maxLP = parseFloat(maxLP) + 1;
				// update the new max LP in the custom record
				nlapiSubmitField('customrecord_ebiznet_lp_range', results[0].getId(),
						'custrecord_ebiznet_lprange_lpmax', parseInt(maxLP));

			}*/
			var scount=1;

			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var vLPRangeId=results[0].getId();
					var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', vLPRangeId);
					if(transaction.getFieldValue('custrecord_ebiznet_lprange_lpmax') != null)
					{	
						maxLP = transaction.getFieldValue('custrecord_ebiznet_lprange_lpmax');

					}
					//Case # 20127077� Start
					maxLPPrefix = transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix');

					if(maxLP==null||maxLP==''||isNaN(maxLP) || maxLP=='null')
						maxLP=0;
					//Case # 20127077� End
					maxLP = parseInt(maxLP) + 1;
					transaction.setFieldValue('custrecord_ebiznet_lprange_lpmax', maxLP);
					nlapiSubmitRecord(transaction, true,true);
					nlapiLogExecution('Debug', 'LP range updated for ref# ',vLPRangeId);

				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					}					 
					nlapiLogExecution('Debug', 'Exception in Get Max LP : ', wmsE); 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}
		}
	}
	// convert maxLP to string
	maxLP = maxLP.toString();
	nlapiLogExecution('Debug', 'GetMaxLPNo:New MaxLP', maxLP);
	//CASE# 20123445    - START
	if(maxLPPrefix == 'null' || maxLPPrefix == null || maxLPPrefix =='')
		maxLPPrefix='';
	//CASE# 20123445   - END
	return maxLPPrefix + maxLP;
}

/**
 * 
 * @param subListID
 * @param checkBoxName
 * @returns {Boolean}
 */
function checkAtleast(subListID, checkBoxName){
	var flag = true;
	var lineNum = nlapiGetLineItemCount(subListID);
	for (var i = 1; i <= lineNum; i++) {
		if (nlapiGetLineItemValue(subListID, checkBoxName, i) == 'T') {
			flag = false;
			break;
		}
	}

	if (flag == true) {
		alert('Please select atleast one line');
		return false;
	}
	else {
		return true;
	}
}

/**
 * Get date from string
 * @param strDate
 * @returns {Date}
 */
function getDateForString(strDate){
	var array = new Array();
	array = strDate.split('/');
	return new Date(array[2], parseFloat(array[0] - 1), array[1]);
}

/**
 * Compare dates
 * @param str1
 * @param str2
 * @returns TRUE:if str1 is before str2 / FALSE: if str1 is after str2
 */
function CompareDates(str1, str2){
	var retVal = false;

	var date1 = getDateForString(str1);
	var date2 = getDateForString(str2);
	if (date1 <= date2)
		retVal = true;
	else
		retVal = false;

	return retVal;
}



//New function to compare two dates
function CompareDates_New(str1, str2){
	var retVal = false;

	var date1 = nlapiStringToDate(str1);
	var date2 = nlapiStringToDate(str2);

	if (date1 <= date2)
		retVal = true;
	else
		retVal = false;

	return retVal;
}
/**
 * This function gets the lines with given status,interval time and the day
 * @param status
 * @param begintime
 * @param endtime
 * @param day
 * @returns {Number}
 */
function getTaskStatus(status, begintime, endtime, day){
	var filters = new Array();
	var count = 0;
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', status));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'within', day));
	filters.push(new nlobjSearchFilter('custrecord_actualendtime', null, 'between', begintime, endtime));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, null);
	if (searchresults != null && searchresults.length > 0) {
		nlapiLogExecution('Debug', 'Task count', searchresults.length);
		count = searchresults.length;
	}
	return count;
}

/**
 * 
 * @param Item
 * @param vCarrier
 * @param vSite
 * @param vCompany
 * @param stgDirection
 * @returns
 */
function GetStageLocation(Item, vCarrier, vSite, vCompany, stgDirection){
	nlapiLogExecution('Debug', 'into GetStageLocation', Item);
	nlapiLogExecution('Debug', 'Item', Item);
	nlapiLogExecution('Debug', 'vSite', vSite);
	nlapiLogExecution('Debug', 'vCompany', vCompany);
	nlapiLogExecution('Debug', 'stgDirection', stgDirection);
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();

	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 ="";
	var ItemInfo3 = "";

	if(Item!=null && Item!="")
	{
		var columns = nlapiLookupField('item', Item, fields);
		if(columns!=null){
			ItemFamily = columns.custitem_item_family;
			ItemGroup = columns.custitem_item_group;
			ItemStatus = columns.custitem_ebizdefskustatus;
			ItemInfo1 = columns.custitem_item_info_1;
			ItemInfo2 = columns.custitem_item_info_2;
			ItemInfo3 = columns.custitem_item_info_3;
		}
	}

	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}

	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
//	if (vCarrier != null && vCarrier != "") {
//	filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

//	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < Math.min(10, searchresults.length); i++) {
			//GetStagLpCount                
			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			//var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');
			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints', 'custrecord_locationstgrule');
			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					//if (vLpCnt < vnoofFootPrints) {//case# 20149865 (Stage location getting -1)
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}
			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}
			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingputseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection == 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}

				filtersLocGroup.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				//This code is commented on 9th September 2011 by ramana
				/*
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				for (var k = 0; searchresults != null && k < searchresults.length; k++) {
					var vLocid=searchresults[k].getId();
						var vLpCnt = GetStagLpCount(vLocid, vSite, vCompany);
						var vnoofFootPrints = searchresults[k].getValue('custrecord_nooffootprints');

						//--
						if (vnoofFootPrints != null && vnoofFootPrints != "") {
								if (vLpCnt < vnoofFootPrints) {
									return vLocid;
								}
						}
					else {
						return vLocid;
						}
				}
				 */

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();
					//var vLocid=searchresultsloc[k].getValue('name');
					var vLpCnt = GetStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');

					//--
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						//if (vLpCnt < vnoofFootPrints) {//case# 20149865 (Stage location getting -1)
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}



			}
		}
	}
	return -1;
}

/**
 * 
 * @param location
 * @param site
 * @param company
 * @returns {Number}
 */
function GetStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');

	var outLPCount = 0;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));

	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));

	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');

	return outLPCount;
}

/**
 * 
 * @param skuID
 * @param orderQty
 * @returns {Array}
 */
//case# 20149733 starts(when remaining cube is lessthan itemcube then system not to generate pick face location)
//function priorityPutaway(skuID, orderQty,location,vitemStatus)
function priorityPutaway(skuID, orderQty,location,vitemStatus,ItemCube){
	nlapiLogExecution('Debug', 'priorityPutaway:Start');
	nlapiLogExecution('Debug','priorityPutaway:Item',skuID);
	nlapiLogExecution('Debug','priorityPutaway:Order Quantity',orderQty);
	nlapiLogExecution('Debug','priorityPutaway:location',location);
	nlapiLogExecution('Debug','Itemstatus of SKU',vitemStatus);
	nlapiLogExecution('Debug','ItemCube',ItemCube);
	var locnQtyArray = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', [skuID]));
	filters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null && location!='')
		filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', [location]));
	//below code is added by suman on 3rd Aug 2013
	filters.push(new nlobjSearchFilter('isinactive','custrecord_pickbinloc', 'is', 'F'));	
	//case# 20149733 starts
	if(ItemCube == null || ItemCube == '' || isNaN(ItemCube))
		ItemCube = 0;

	filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_pickbinloc', 'greaterthanorequalto', ItemCube));
	//case# 20149733 ends
	var newitemstat = new Array();
	newitemstat.push('@NONE@');

	//Case # 20127203 Start
	if(vitemStatus !=null && vitemStatus!='')
	{
		newitemstat.push(vitemStatus);
	}
	//newitemstat.push(vitemStatus);
	nlapiLogExecution('Debug','newitemstat',newitemstat.length);

	if(newitemstat!=null && newitemstat!='' && newitemstat.length >1 )
		filters.push(new nlobjSearchFilter('custrecord_pickface_itemstatus', null, 'anyof',newitemstat));

	//Case # 20127203 End





	//custrecord_pickbinloc
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_pickbinloc');
	columns[1] = new nlobjSearchColumn('custrecord_maxqty');
	columns[2] = new nlobjSearchColumn('custrecord_pickzone');
	columns[3] = new nlobjSearchColumn('custrecord_pickface_itemstatus');

	var pickFaceResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);
	if (pickFaceResults){

		var qtyforAutoBreakdown;
		var qtyToBePlacedinPFLoc;
		var binLoc;
		var zoneid;
		var itemstatus;

		nlapiLogExecution('Debug','Inside pickFaceRes','Notnull');
		var binLoc = pickFaceResults[0].getValue('custrecord_pickbinloc');
		var maxBinQty = pickFaceResults[0].getValue('custrecord_maxqty');
		var zoneid = pickFaceResults[0].getValue('custrecord_pickzone');
		var itemstatus = pickFaceResults[0].getValue('custrecord_pickface_itemstatus');

		// Check the quantity in this Bin Location in create inventory.
		if (binLoc) {
			nlapiLogExecution('Debug','Inside binLoc','Notnull');
			// Get pending putaways
			var expectedQty = pendingReplenPutawayTasks(skuID, binLoc);
			// Check quantity in inventory
			var inventoryQOH = TotalQuantityinInvt(skuID,binLoc,location);

			if(inventoryQOH == "" || inventoryQOH == null)
				inventoryQOH=0;

			nlapiLogExecution('Debug', 'priorityPutaway:Expected Quantity', expectedQty);
			nlapiLogExecution('Debug', 'priorityPutaway:Inventory QOH', inventoryQOH);

			var fulfilQty=0;

			// Sum up the total qty that has pending replen tasks and pending
			// moves with qohqty of inventory.
			if (expectedQty =="" || expectedQty == null)
				expectedQty=0;

			var totalAvailableQty = parseFloat(inventoryQOH) + parseFloat(expectedQty);
			nlapiLogExecution('Debug','priorityPutaway:Total Available Qty',totalAvailableQty);

			// Check this quantiy is less than Max qty in that location.
			// Check if location Qty is greater than Max Bin Loc Qty.
			if (parseFloat(maxBinQty) > parseFloat(totalAvailableQty))
				fulfilQty = parseFloat(maxBinQty) - parseFloat(totalAvailableQty);

			var qtyToBePlacedinPFLoc;
			var locIdofPF;
			var qtyforAutoBreakdown;

			// Check for qty that can fit in that location with ord qty.
			if(parseFloat(fulfilQty) >= parseFloat(orderQty)){
				qtyToBePlacedinPFLoc= parseFloat(orderQty);
				locIdofPF= binLoc;
				qtyforAutoBreakdown= 0;		
			}else{
				qtyToBePlacedinPFLoc= parseFloat(fulfilQty);
				locIdofPF= binLoc;
				qtyforAutoBreakdown= parseFloat(orderQty)-parseFloat(fulfilQty);
			}
		}

		// Assigning Values into Array .
		locnQtyArray[0]= qtyforAutoBreakdown;
		locnQtyArray[1]=qtyToBePlacedinPFLoc;
		locnQtyArray[2]=binLoc;
		locnQtyArray[3]=zoneid;
		locnQtyArray[4]=itemstatus;

		nlapiLogExecution('Debug','qtyforAutoBreakdown',qtyforAutoBreakdown);
		nlapiLogExecution('Debug','qtyToBePlacedinPFLoc',qtyToBePlacedinPFLoc);
		nlapiLogExecution('Debug','binLoc',binLoc);
		nlapiLogExecution('Debug','zoneid',zoneid);
		nlapiLogExecution('Debug','itemstatus',itemstatus);

	}else{
		//Assigning Values into Array .
		locnQtyArray[0]= orderQty;
		locnQtyArray[1]=0;
		locnQtyArray[2]="";
		locnQtyArray[3]="";
		locnQtyArray[4]="";
	}


	return locnQtyArray;
}



function priorityPutawayflag(skuID,location){

	nlapiLogExecution('ERROR','priorityPutaway:Item',skuID);


	var priorityflag;
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', [skuID]));
	filters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null && location!='')
		filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', [location]));

	//custrecord_pickbinloc
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_priorityputaway');

	var pickFaceResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);
	if (pickFaceResults){
		nlapiLogExecution('ERROR','Inside pickFaceRes','Notnull');
		priorityflag = pickFaceResults[0].getValue('custrecord_priorityputaway');

	}

	return priorityflag;
}


function priorityPutawayfixedLP(skuID,location,itemstatus,binlocation){

	nlapiLogExecution('ERROR','priorityPutaway:Item',skuID);
	nlapiLogExecution('ERROR','priorityPutaway:binlocation',binlocation);

	var priorityfixedLP='';
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', [skuID]));
	filters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null && location!='')
		filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', [location]));
	if(binlocation!=null && binlocation!='')
		filters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', [binlocation]));
	if(itemstatus!=null && itemstatus!='')
		filters.push(new nlobjSearchFilter('custrecord_pickface_itemstatus', null, 'anyof', [itemstatus]));

	//custrecord_pickbinloc
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');

	var pickFaceResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);
	if (pickFaceResults){
		nlapiLogExecution('ERROR','Inside pickFaceRes','Notnull');
		priorityfixedLP = pickFaceResults[0].getText('custrecord_pickface_ebiz_lpno');

	}

	return priorityfixedLP;
}

/**
 * 
 * @param skuId
 * @param binLoc
 * @returns
 */
function pendingReplenPutawayTasks(skuId, binLoc){
	try{
		nlapiLogExecution('Debug', 'pendingReplenPutawayTasks:Start', 'Putaway');
		var expectedQty='';
		var filters = new Array();
		// taskType = PUTW or RPLN
		filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2, 8]);

		// wmsStatusFlag = INBOUND/LOCATIONS ASSIGNED or OUTBOUND/PICK GENERATED
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 9])); 
		filters.push(new nlobjSearchFilter('custrecord_expe_qty', null, 'isnotempty'));
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', [skuId]));
		filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', binLoc));
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', ['@NONE@']));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

		var searchOpenTask = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		if(searchOpenTask!=null && searchOpenTask!='')
			expectedQty = searchOpenTask[0].getValue('custrecord_expe_qty', null, 'sum');
		nlapiLogExecution('Debug', 'pendingReplenPutawayTasks:Expected Quantity', expectedQty);
	}catch(exps){
		nlapiLogExecution('Debug', 'pendingReplenPutawayTasks:Exception', exps);
	}

	return expectedQty;
}

/**
 * 
 * @param skuId
 * @param binLoc
 * @returns {String}
 */
function TotalQuantityinInvt(skuId,binLoc,location)
{
	try {
		nlapiLogExecution('Debug', 'Inside TotalQuantityinInvt', 'TotalQuantityinInvt');
		var invLocfilters = new Array();
		invLocfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [binLoc]));
		invLocfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', [skuId]));
		invLocfilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));
		invLocfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', location));

		var invLocCol = new Array();
		invLocCol[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

		var invtRes = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invLocfilters, invLocCol);
		var qohqty = "";
		if (invtRes) {
			nlapiLogExecution('Debug', 'Inside invtRes', 'Not null');
			for (var s = 0; s < invtRes.length; s++) {
				if (qohqty == "") {
					qohqty = parseFloat(invtRes[s].getValue('custrecord_ebiz_qoh'));
				}
				else {
					qohqty = parseFloat(qohqty) + parseFloat(invtRes[s].getValue('custrecord_ebiz_qoh'));
				}
			}
		}
		nlapiLogExecution('Debug', 'Inside TotalQuantityinInvt qohqty', qohqty);
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Inside TotalQuantityinInvt', exp);
	}
	return qohqty;
}

/**
 * Get full qualified domain name to form the URL
 * @param environment
 * @returns fully qualified domain name
 */
function getFQDNForHost(environment){
	var fqdn = '';
	if(environment == 'PRODUCTION')
		fqdn = 'https://system.netsuite.com';
	else if(environment == 'SANDBOX')
		fqdn = 'https://system.sandbox.netsuite.com';

	return fqdn;
}

/**
 * 
 * @param itemNo
 * @param location
 * @param company
 * @returns {String}
 */
function validateSKU(itemNo,location,company)
{
	nlapiLogExecution('Debug', 'validateSKU:Start',itemNo);
	var actItem="";

	/*var invitemfilters = new Array();
	invitemfilters[0] = new nlobjSearchFilter('name',null, 'is',itemNo);
	var invLocCol = new Array();
	invLocCol[0] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);*/

	var sitemno = "";
	var context = nlapiGetContext();
	var userAccountId = context.getCompany();
	if(userAccountId=='1285441')
	{
		sitemno = checkserialritem(itemNo);
	}
	if(sitemno != "")
	{
		itemNo = sitemno;
	}	

	// Changed On 30/4/12 by Suman

	var invitemfilters=new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemNo));
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	var invLocCol = new Array();
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	invLocCol[0] = new nlobjSearchColumn('externalid');


	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);

	// End of Changes as On 30/4/12


	if(invitemRes!=null)
	{
		//actItem=invitemRes[0].getValue('itemid');
		actItem=invitemRes[0].getValue('externalid');

	}
	else
	{					
		var invLocfilters = new Array();
		invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));
		// Changed On 30/4/12 by Suman
		invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12

		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);
		if(invtRes != null)
		{
			//actItem=invtRes[0].getValue('itemid');
			actItem=invtRes[0].getValue('externalid');

		}
		else
		{
			var skuAliasFilters = new Array();
			skuAliasFilters[0] = new nlobjSearchFilter('name', null, 'is', itemNo);	
			skuAliasFilters[1] = new nlobjSearchFilter('custrecord_ebiz_location', null, 'is', location);

			//if(company != null && company != "")
			//	skuAliasFilters[2] = new nlobjSearchFilter('custrecord_ebiz_company', null, 'is', company);

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
				actItem=skuAliasResults[0].getText('custrecord_ebiz_item');
		}			
	}

	return actItem;
}


/**
 * Returns the elapsed duration between timestamp1 and timestamp2
 * @param timestamp1
 * @param timestamp2
 * @returns Elapsed Duration
 */
function getElapsedTimeDuration(timestamp1, timestamp2){
	return (parseFloat(timestamp2.getTime()) - parseFloat(timestamp1.getTime()));
}

/**
 * Returns the current logged in user
 * @returns Current user from Context
 */
function getCurrentUser(){
	var context = nlapiGetContext();
	return context.getUser();
}

/**
 * 
 * @param formid
 * @returns {String}
 */
function getFunctionkeyScript(formid){
	var html=""; 
	html = html + "<SCRIPT LANGUAGE='javascript'>";
	html = html + "function OnKeyDown_CL() ";
	html = html + " { ";     
	html = html + "         if (";
	html = html + " event.keyCode == 112 || event.keyCode == 113 || event.keyCode == 114 || event.keyCode == 115 || event.keyCode == 116 || event.keyCode == 117 ||";
	html = html + " event.keyCode == 118 || event.keyCode == 119 || event.keyCode == 120 || event.keyCode == 121 || event.keyCode == 122 || event.keyCode == 123 || event.keyCode == 13) {";
	html = html + " var arrElements = document.getElementsByTagName('input');";
	html = html + " var keyFound = false;";
	html = html + " for (i = 0; i < arrElements.length; i++) {";
	html = html + " if (arrElements[i].type == 'submit') {";
	html = html + " switch (event.keyCode) {";
	html = html + " case 112:";
	html = html + " if (arrElements[i].value == 'F1')";	//F7 Key														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 113:";
	html = html + " if (arrElements[i].value == 'F2')";		//F8 Key													
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 114:";
	html = html + " if (arrElements[i].value == 'F3')";	//F9 Key														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 115:";
	html = html + " if (arrElements[i].value == 'F4')";		//F10 Key													
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 116:";
	html = html + " if (arrElements[i].value == 'F5')";		//F11 Key";														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 117:";
	html = html + " if (arrElements[i].value == 'F6')";		//F11 Key";														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 118:";
	html = html + " if (arrElements[i].value == 'F7')";		//F7 Key";														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 119:";
	html = html + " if (arrElements[i].value == 'F8')";		//F8 Key";														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 120:";
	html = html + " if (arrElements[i].value == 'F9')";		//F9 Key";														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 121:";
	html = html + " if (arrElements[i].value == 'F10')";		//F10 Key";														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 122:";
	html = html + " if (arrElements[i].value == 'F11')";		//F11 Key";														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 123:";
	html = html + " if (arrElements[i].value == 'F12')";		//F12 Key";														
	html = html + " keyFound = true;";
	html = html + " break;";
	html = html + " case 13:";
	html = html + " if (arrElements[i].value == 'ENT')";		//ENTER KEY -- ATTN"; 
	html = html + " keyFound = true;";	 
	html = html + " break;";
	html = html + " }";
	html = html + " if (keyFound == true && arrElements[i].disabled == false) {";        
	html = html + " eval('document."+formid+".' + arrElements[i].name + '.click();');";
	html = html + " return false;";
	html = html + " }";
	html = html + " }";
	html = html + " }";
	html = html + " }    ";        
	html = html + "    return true; ";
	html = html + "    }";        
	html = html + " </SCRIPT>";
	return html ;
}


/**
 * Generic function to display different types of inline messages with 1 dynamic operational variable/quantity
 * @param messageType
 * @param messageHeader
 * @param messageText
 */
function showInlineMessage(form, messageType, messageText, messageVariable){
	var msg;
	var priority;

	// Create the message field in the form
	msg = form.addField('custpage_message', 'inlinehtml', null, null, null);

	if(messageType == 'Confirmation')
		priority = 'NLAlertDialog.TYPE_LOWEST_PRIORITY';
	else if(messageType == 'ERROR')
		priority = 'NLAlertDialog.TYPE_HIGH_PRIORITY';
	else
		priority = 'NLAlertDialog.TYPE_HIGH_PRIORITY';

	// Set the message value
	if(messageVariable != null)
	{
		if(messageText != null && messageText != "" && messageText !="null")
		{
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', '" +
					messageType + "', '" + messageText + ":" + messageVariable + "', " +
					priority + ",  '100%', null, null, null);</script></div>");
		}
		else
		{
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', '" +
					messageType + "', '" + messageVariable + "', " +
					priority + ",  '100%', null, null, null);</script></div>");
		}
	}
	else
	{

		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', '" +
				messageType + "', '" + messageText + "', " +
				priority + ",  '100%', null, null, null);</script></div>");
	}
}

/**
 * Based on item status(Good,Damage.Repair) it will fetch item status map location from the custom record.
 * 
 */

function getItemStatusMapLoc(itemstatus)
{
	var itemstatusmaplocation;	 
	var filtersItemStatus = new Array();        						
	filtersItemStatus.push(new nlobjSearchFilter('internalid', null, 'is', itemstatus));
	filtersItemStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var colsItemStatus = new Array();
	colsItemStatus[0]=new nlobjSearchColumn('custrecord_item_status_location_map');  
	var ItemStatusSrearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersItemStatus, colsItemStatus);        						        						        						        						
	if(ItemStatusSrearchResults!=null)
		itemstatusmaplocation = ItemStatusSrearchResults[0].getValue('custrecord_item_status_location_map');		
	return itemstatusmaplocation;	
}

/**
 * 
 * Based on Location and item status map location it will fetch the AccountNo
 * @param location
 * @param itemstatusmaploc
 * @returns
 */

function getAccountNo(location,itemstatusmaploc)
{
	var accountno;	
	var filtersAccNo = new Array(); 
	filtersAccNo.push(new nlobjSearchFilter('custrecord_location', null, 'is', location));
	filtersAccNo.push(new nlobjSearchFilter('custrecord_inventorynslocation', null, 'is', itemstatusmaploc));        						
	var colsAcc = new Array();
	colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
	var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);
	if(Accsearchresults!=null)
		accountno = Accsearchresults[0].getValue('custrecord_accountno');	
	return accountno;
}


function getStockAdjustmentAccountNo(loc,tasktype,adjusttype)
{	
	var StAdjustmentAccountNo;
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();

	if(loc!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location',null,'anyof',[loc]));	

	if(tasktype!=null && tasktype!="")
		filterStAccNo.push(new nlobjSearchFilter('custrecord_adjusttasktype',null,'anyof',[tasktype]));

//	if(tasktype==11) //11 is the internalid value of tasktype "ADJT";
//	{	
//	if(adjusttype!=null || adjusttype!="")
//	filterStAccNo.push(new nlobjSearchFilter('custrecord_adjustment_type',null,'anyof',adjusttype));
//	filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));
//	}	

	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');    

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	if(StockAdjustAccResults!=null)
		StAdjustmentAccountNo =  StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount');	

	return StAdjustmentAccountNo;	
}



/**
 * Passing item,item status, qty and location to NS
 * 
 * @param item
 * @param itemstatus
 * @param loc
 * @param qty
 * @param notes
 */
function  InvokeNSInventoryAdjustment(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,date,period)
{
	/*
		var vItemMapLocation =  getItemStatusMapLoc(itemstatus);
		var vAccountNo = getAccountNo(loc,vItemMapLocation);

		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', vAccountNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, vItemMapLocation);
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
		nlapiSubmitRecord(outAdj, false, true);
		nlapiLogExecution('Debug', 'type argument', 'type is create');
	 */

	nlapiLogExecution('Debug', 'Location info ::', loc);
	nlapiLogExecution('Debug', 'Task Type ::', tasktype);
	nlapiLogExecution('Debug', 'Adjustment Type ::', adjusttype);
	nlapiLogExecution('Debug', 'Qty ::', qty);
	nlapiLogExecution('Debug', 'Item ::', item);

	var confirmLotToNS='Y';
	confirmLotToNS=GetConfirmLotToNS(loc);

	var vAccountNo;	
	//Code for fetching account no from stockadjustment custom record	
	vAccountNo = getStockAdjustmentAccountNo(loc,tasktype,adjusttype);			
	nlapiLogExecution('Debug', 'Fetcht Account from stockadjustment', vAccountNo);		
	//upto to here stockadjustment

	var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
	nlapiLogExecution('Debug', 'vItemMapLocation', vItemMapLocation);	

	if(vAccountNo==null || vAccountNo=="")
	{		
		vAccountNo = getAccountNo(loc,vItemMapLocation);
		nlapiLogExecution('Debug', 'Fetcht Account from Inventory AccountNo', vAccountNo);
	}

	var vCost;
	var vAvgCost;
	var vItemname;
	var avgcostlot;

	var filters = new Array();          
	filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
	// Changed On 30/4/12 by Suman
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	//Case # 20149077 Start
	if(loc !=null && loc!='' && loc!='null' && loc!='undefined' && loc > 0)
	{
		filters.push(new nlobjSearchFilter('inventorylocation',null, 'anyof',loc));
	}
	//Case # 20149077 end

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('cost');
	//columns[1] = new nlobjSearchColumn('averagecost');

	//As per email from NS on  02-May-2012  we have changed the code to use 'locationaveragecost' as item unit cost
	//Email from Thad Johnson to Sid
	columns[1] = new nlobjSearchColumn('locationaveragecost');

	columns[2] = new nlobjSearchColumn('itemid');

	//As per recommendation from NS PM Jeff hoffmiester This field 'custitem16' is used as avg cost for Default lot updation in Dec 2011 for TNT
	//columns[3] = new nlobjSearchColumn('custitem16');

	var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
	if (itemdetails !=null) 
	{
		vItemname=itemdetails[0].getValue('itemid');
		vCost = itemdetails[0].getValue('cost');
		nlapiLogExecution('Debug', 'vCost', vCost);
		//vAvgCost = itemdetails[0].getValue('averagecost');
		vAvgCost = itemdetails[0].getValue('locationaveragecost');
		//As per recommendation from NS PM Jeff hoffmiester This field custitem16 is used as avg cost for Default lot updation in Dec 2011 for TNT
		//avgcostlot=itemdetails[0].getValue('custitem16');
		//nlapiLogExecution('Debug', 'avgcostlot', avgcostlot);	
		nlapiLogExecution('Debug', 'Average Cost', vAvgCost);	         
	}		

	var outAdj = nlapiCreateRecord('inventoryadjustment');		
	if(vAccountNo!=null&&vAccountNo!=""&&vAccountNo!="null")
		outAdj.setFieldValue('account', vAccountNo);
	outAdj.setFieldValue('memo', notes);
	outAdj.selectNewLineItem('inventory');
	outAdj.setCurrentLineItemValue('inventory', 'item', item);
	outAdj.setCurrentLineItemValue('inventory', 'location',vItemMapLocation);	
	//Case# 201410949 starts
	//outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby',parseInt(qty));
	outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby',parseFloat(qty));
	//Case# 201410949 ends
	if(date !=null && date!='' && date!='null' && date!='undefined')
	{
		outAdj.setFieldValue('trandate', date);
	}
	if(period !=null && period!='' && period!='null' && period!='undefined')
	{
		outAdj.setFieldValue('postingperiod', period);
	}
	outAdj.setCurrentLineItemValue('inventory', 'unitcost','');		
	/*if(vAvgCost != null &&  vAvgCost != "")	
	{
		nlapiLogExecution('Debug', 'into if cond vAvgCost', vAvgCost);
		outAdj.setCurrentLineItemValue('inventory', 'unitcost',vAvgCost);
	}
	else
	{
		nlapiLogExecution('Debug', 'into else cond.unit cost', vCost);
		outAdj.setCurrentLineItemValue('inventory', 'unitcost', vCost);
	}*/
	//outAdj.setLineItemValue('inventory', 'unitcost', 1, avgcostlot);//avg cost send to inventory
	//outAdj.setFieldValue('subsidiary', 3);

	//This following line will return 'true' for ONE WORLD Account, 'false' for non-ONE WORLD Account.
	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('Debug', 'subs', subs);
	if(subs==true)
	{
		var vSubsidiaryVal=getSubsidiaryNew(loc);
		nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
		if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
	}


	//Added by Sudheer on 093011 to send lot# for inventory posting
	nlapiLogExecution('Debug', 'lot', lot);
	if(lot!=null && lot!='')
	{
		//vItemname=vItemname.replace(" ","-");
		vItemname=vItemname.replace(/ /g,"-");


		var fields = ['recordType', 'custitem_ebizserialin'];
		var columns = nlapiLookupField('item', item, fields);
		var vItemType = columns.recordType;
		nlapiLogExecution('Debug','vItemType',vItemType);

		var serialInflg="F";		
		serialInflg = columns.custitem_ebizserialin;
		nlapiLogExecution('Debug','serialInflg',serialInflg);

		nlapiLogExecution('Debug', 'lot', lot);
		nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
//		For advanced Bin serial Lot management check
		var vAdvBinManagement=false;
		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		}  
		nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);

		if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T") {
			nlapiLogExecution('Debug','lot',lot);
			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				//case 20126071 start : posting serial numbers for AdvanceBinmanagement
				if(lot !=null && lot !='')
				{
					//case # 20126232� start
					var tempQty;

					if(parseFloat(qty)<0)
					{
						tempQty=-1;
					}
					else
					{
						tempQty=1;
					}
					//case # 20126232� end
					var SerialArray = lot.split(',');
					//var SerialArray = lot;
					if(SerialArray !=null && SerialArray !='' && SerialArray.length>0)
					{
						nlapiLogExecution('Debug','SerialArray.length',SerialArray.length);
						var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
						for (var x = 0; x < SerialArray.length; x++)
						{
							nlapiLogExecution('Debug','SerialArray',SerialArray[x]);
							/*if(x==0)
							var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');*/
							compSubRecord.selectNewLineItem('inventoryassignment');
							//case # 20126232� start
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQty);
							//case # 20126232� end
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', SerialArray[x]);

							compSubRecord.commitLineItem('inventoryassignment');

						}
						compSubRecord.commit();
					}
				}
				//case 20126071 end
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
				//outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot);
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers',lot);
			}
		}
		else if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
		{
			if(confirmLotToNS=='N')
				lot=vItemname;
			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
				//outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers',lot + "(" + parseFloat(qty) + ")");
			}
		}
	}
	outAdj.commitLineItem('inventory');
	var id=nlapiSubmitRecord(outAdj, false, true);
	nlapiLogExecution('Debug', 'type argument', 'type is create');
	nlapiLogExecution('Debug', 'type argument id', id);
	return id;
}

/**To get confirm LOT toNS based on system rule
 * 
 * @param Site
 * @returns Y or N
 */
function GetConfirmLotToNS(Site)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'WMSLOT');
		filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('Debug', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}

/**To get Quantity Discrepancy toNS based on system rule
 * 
 * 
 * @returns Y or N
 */
function GetIsQuantityDiscrepancyRequired()
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', 'Is Quantity discrepancey confirmation required?');
		//filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('Debug', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}

function IsLprequiredformove()
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', 'Is LP Scan required for Move?');
		//filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('Debug', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}

function IsLprequiredforreplen()
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', 'Is LP Scan required for Replen?');
		//filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('Debug', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}


function CreateShippingManifestRecordNew(vebizOrdNo,vContainerLp,opentaskordersearchresult,vwaveno)
{
	nlapiLogExecution('Debug', 'into CreateShippingManifestRecord','');
	nlapiLogExecution('Debug', 'vordno',vebizOrdNo);
	nlapiLogExecution('Debug', 'vContainerLp',vContainerLp);
	nlapiLogExecution('Debug', 'vwaveno',vwaveno);
	nlapiLogExecution('Debug', 'opentaskdetails',opentaskordersearchresult);
	var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
	nlapiLogExecution('Debug', 'trantype', trantype);
	var  searchresults;

	if(trantype=="salesorder")
	{
		searchresults =SalesOrderList(vebizOrdNo);
	}
	else
	{
		searchresults =TransferorderList(vebizOrdNo);
	}
	var entity=searchresults[0].getValue('entity');
	var entityrecord ;
	if(entity != "" && entity != null)
	{
		try
		{
			entityrecord = nlapiLoadRecord('customer', entity);
		}
		catch(exp)
		{
			nlapiLogExecution('Debug', 'Exception in Loading Customer',exp);
			entityrecord='';
		}
	}

	var servicelevellbyshipmethod;
	var ServiceLevelId=searchresults[0].getValue('shipmethod');
	if(ServiceLevelId!=null || ServiceLevelId!='')
	{

		servicelevellbyshipmethod=GetSerViceLevelByShipmethod(ServiceLevelId);
	}
	nlapiLogExecution('Debug', 'test1','test1');
	for ( var vcount = 0;  vcount < vContainerLp.length; vcount++) 
	{

		var ShipManifest = nlapiCreateRecord('customrecord_ship_manifest');
		ShipManifest.setFieldValue('custrecord_ship_orderno',searchresults[0].getValue('tranid'));
		ShipManifest.setFieldValue('custrecord_ship_carrier',searchresults[0].getValue('custbody_salesorder_carrier'));
		ShipManifest.setFieldValue('custrecord_ship_city',   searchresults[0].getValue('shipcity'));
		ShipManifest.setFieldValue('custrecord_ship_state',	 searchresults[0].getValue('shipstate'));
		ShipManifest.setFieldValue('custrecord_ship_country',searchresults[0].getValue('shipcountry'));
		ShipManifest.setFieldValue('custrecord_ship_addr1',searchresults[0].getValue('shipaddress1'));

		if(trantype=="salesorder")
		{

			var contactName=searchresults[0].getValue('shipattention');
			var entity=searchresults[0].getText('entity');
			if(contactName!=null && contactName!='')
				contactName=contactName.replace(","," ");

			if(entity!=null && entity!='')
				entity=entity.replace(","," ");			
			nlapiLogExecution('Debug', 'test2','test2');
			ShipManifest.setFieldValue('custrecord_ship_contactname',contactName);					
			ShipManifest.setFieldValue('custrecord_ship_ordertype',searchresults[0].getText('custbody_nswmssoordertype'));
			freightterms=searchresults[0].getText('custbody_nswmsfreightterms');
			otherrefnum=searchresults[0].getValue('otherrefnum');			 
			ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);
			ShipManifest.setFieldValue('custrecord_ship_phone',searchresults[0].getValue('custbody_customer_phone'));
			var saturdaydelivery= searchresults[0].getValue('custbody_nswmssosaturdaydelivery');
			ShipManifest.setFieldValue('custrecord_ship_satflag',saturdaydelivery);
			var cashondelivery= searchresults[0].getValue('custbody_nswmscodflag');
			ShipManifest.setFieldValue('custrecord_ship_codflag',cashondelivery);
			ShipManifest.setFieldValue('custrecord_ship_email',searchresults[0].getValue('email'));
			var locationid=searchresults[0].getValue('location');
			ShipManifest.setFieldValue('custrecord_ship_location',locationid);			
			var shiptotal="0.00";			
			ShipManifest.setFieldValue('custrecord_ship_codflag','F');
			ShipManifest.setFieldValue('custrecord_ship_codamount',shiptotal);			
			ShipManifest.setFieldValue('custrecord_ship_consignee',entity);
			var servicelevelvalue=searchresults[0].getValue('shipmethod');
			ShipManifest.setFieldValue('custrecord_ship_servicelevel',servicelevelvalue);	
			var shipzip=searchresults[0].getValue('shipzip');
			var otherrefnum=searchresults[0].getValue('otherrefnum');		
			ShipManifest.setFieldValue('custrecord_ship_zip',shipzip);
			var companyname= searchresults[0].getText('custbody_nswms_company');
			ShipManifest.setFieldValue('custrecord_ship_company',companyname);
			ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);			
			nlapiLogExecution('Debug', 'test3','test3');

		}
		if(vwaveno!=null && vwaveno!='')
		{
			ShipManifest.setFieldValue('custrecord_ship_ref4',parseInt(vwaveno).toString());
		}

		var address1=searchresults[0].getValue('shipaddress1');
		var address2=searchresults[0].getValue('shipaddress2');

		var zip=searchresults[0].getValue('shipzip');
		var servicelevel=searchresults[0].getText('shipmethod');

		nlapiLogExecution('Debug', 'Zip',zip);
		nlapiLogExecution('Debug', 'Service Level',servicelevel);
		nlapiLogExecution('Debug', 'Zip',zip);
		nlapiLogExecution('Debug', 'City',searchresults[0].getValue('shipcity'));
		nlapiLogExecution('Debug', 'State',searchresults[0].getValue('shipstate'));
		nlapiLogExecution('Debug', 'Zip=',searchresults[0].getValue('shipzip'));
		nlapiLogExecution('Debug', 'Service Level Value',searchresults[0].getText('shipmethod'));

		if(address1!=null && address1!='')
			address1=address1.replace(","," ");
		if(address2!=null && address2!='')
			address2=address2.replace(","," ");
		ShipManifest.setFieldValue('custrecord_ship_order',vebizOrdNo);
		ShipManifest.setFieldValue('custrecord_ship_custom5',"S");	
		ShipManifest.setFieldValue('custrecord_ship_void',"N");

		ShipManifest.setFieldValue('custrecord_ship_ordertype',searchresults[0].getText('custbody_nswmssoordertype'));
		freightterms=searchresults[0].getText('custbody_nswmsfreightterms');
		otherrefnum=searchresults[0].getValue('otherrefnum');			 
		ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);

		ShipManifest.setFieldValue('custrecord_ship_addr1',address1);
		ShipManifest.setFieldValue('custrecord_ship_addr2',address2);


		if(freightterms!="SENDER")
		{

			var thirdpartyacct=entityrecord.getFieldValue('thirdpartyacct');

			if((thirdpartyacct!=null)&&(thirdpartyacct!=''))
			{
				nlapiLogExecution('Debug', 'thirdpartyacct',thirdpartyacct);
				ShipManifest.setFieldValue('custrecord_ship_account',thirdpartyacct);

			}
		}
		nlapiLogExecution('Debug', 'freightterms', freightterms);
		var freightvalue="";
		if(freightterms=="SENDER")
		{
			freightvalue="SHP";
		}
		if(freightterms=="RECEIVER")
		{
			freightvalue="REC";
		}
		if(freightterms=="3RDPARTY")
		{
			freightvalue="TP";
		}
		nlapiLogExecution('Debug', 'freightvalue', freightvalue);
		ShipManifest.setFieldValue('custrecord_ship_paymethod',freightvalue);
		if (entityrecord != null && entityrecord != '')
		{
			var custaddr1 = entityrecord.getFieldValue('shipaddr1');
			var custaddr2 = entityrecord.getFieldValue('shipaddr2');
			var custaddresee = entityrecord.getFieldValue('shipaddressee');
			var custcity = entityrecord.getFieldValue('shipcity');
			var custstate = entityrecord.getFieldValue('shipstate');
			var custzip = entityrecord.getFieldValue('shipzip');	
			ShipManifest.setFieldValue('custrecord_ship_city',  custcity);
			ShipManifest.setFieldValue('custrecord_ship_state',	custstate);
			ShipManifest.setFieldValue('custrecord_ship_addr1',	custaddr1);
			ShipManifest.setFieldValue('custrecord_ship_addr2',	custaddr2);
			ShipManifest.setFieldValue('custrecord_ship_zip',	custzip);
		}

		if((servicelevellbyshipmethod!=null)&&(servicelevellbyshipmethod !='')&&(servicelevellbyshipmethod.length>0))
		{
			nlapiLogExecution('Debug', 'servicelevellbyshipmethod', 'servicelevellbyshipmethod');

			var shipserviceLevel=servicelevellbyshipmethod[0].getValue('custrecord_carrier_service_level'); 
			nlapiLogExecution('Debug', 'shipmethodshipserviceLevel',shipserviceLevel);
			ShipManifest.setFieldValue('custrecord_ship_servicelevel',shipserviceLevel);

		}

		if(trantype=="transferorder")
		{
			nlapiLogExecution('Debug', 'tolocationtstt', trantype);
			var tolocation = searchresults[0].getValue('transferlocation');
			var fromlocation=searchresults[0].getValue('location');
			nlapiLogExecution('Debug', 'tolocation', tolocation);
			var record = nlapiLoadRecord('location', tolocation);
			var shipfromaddress1=record.getFieldValue('addr1');
			var shipfromaddress2=record.getFieldValue('addr2');
			var shipfromcity=record.getFieldValue('city');
			var shipfromstate=record.getFieldValue('state');
			var shipfromzipcode =record.getFieldValue('zip');
			var shipfromcompanyname=record.getFieldValue('addressee');
			var shipfromcountry =record.getFieldValue('country');
			ShipManifest.setFieldValue('custrecord_ship_carrier',searchresults[0].getText('custbody_salesorder_carrier'));
			ShipManifest.setFieldValue('custrecord_ship_city',   shipfromcity);
			ShipManifest.setFieldValue('custrecord_ship_state',	 shipfromstate);
			ShipManifest.setFieldValue('custrecord_ship_country',shipfromcountry);
			ShipManifest.setFieldValue('custrecord_ship_addr1',shipfromaddress1);
			ShipManifest.setFieldValue('custrecord_ship_zip',shipfromzipcode);
			ShipManifest.setFieldValue('custrecord_ship_addr2',shipfromaddress2);
			ShipManifest.setFieldValue('custrecord_ship_location',fromlocation);
			ShipManifest.setFieldValue('custrecord_ship_consignee',shipfromcompanyname);

		}


		if (opentaskordersearchresult != null && opentaskordersearchresult != "")
		{
			nlapiLogExecution('Debug', 'inside opentask search results', opentaskordersearchresult);

			var itemsarr = new Array();

			for (z1 = 0; z1 < opentaskordersearchresult.length; z1++) 
			{
				itemsarr.push(opentaskordersearchresult[z1].getValue('custrecord_sku'));
			}

			var itemdimslist = getAllSKUDims(itemsarr);

			var oldcontainer="";
			for (l = 0; l < opentaskordersearchresult.length; l++) 
			{ 
				nlapiLogExecution('Debug', 'inside opentask', containerid);

				var custlenght="";	
				var custheight="";
				var custwidht="";

				var sku="";
				var ebizskuno="";
				var uomlevel="";
				var containerlpno = opentaskordersearchresult[l].getValue('custrecord_container_lp_no');
				sku = opentaskordersearchresult[l].getText('custrecord_sku');
				ebizskuno = opentaskordersearchresult[l].getValue('custrecord_sku');
				uomlevel = opentaskordersearchresult[l].getValue('custrecord_uom_level');					
				var name= opentaskordersearchresult[l].getValue('name');
				var site=opentaskordersearchresult[l].getValue('custrecord_wms_location');
				ShipManifest.setFieldValue('custrecord_ship_location',site);	
				if(oldcontainer!=containerlpno){
					ShipManifest.setFieldValue('custrecord_ship_ref3',name);
					ShipManifest.setFieldValue('custrecord_ship_contlp',vContainerLp[vcount]);
					ShipManifest.setFieldValue('custrecord_ship_ref5',vContainerLp[vcount]);

					var containerid= opentaskordersearchresult[l].getValue('custrecord_container');
					var containername= opentaskordersearchresult[l].getText('custrecord_container');
					nlapiLogExecution('Debug', 'container id', containerid);
					nlapiLogExecution('Debug', 'container name', containername);
					if(containerid!="")
					{
						if(containername!="SHIPASIS")
						{
							//Lenght, Height,Width fields  in customrecord_ebiznet_container
							var containersearchresults = getContainerDims(containerid);
							if(containersearchresults != null)
							{
								custlenght=containersearchresults [0].getValue('custrecord_length');
								custwidht=containersearchresults [0].getValue('custrecord_widthcontainer');
								custheight=containersearchresults [0].getValue('custrecord_heightcontainer');								
								ShipManifest.setFieldValue('custrecord_ship_length',custlenght);	
								ShipManifest.setFieldValue('custrecord_ship_width',custwidht);
								ShipManifest.setFieldValue('custrecord_ship_height',custheight);
								ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
								ShipManifest.setFieldValue('custrecord_ship_pkgtype',containername);
							} 
						}
						else
						{
							var containersearchresults = getSKUDimsbyUOMlvl(ebizskuno,uomlevel,itemdimslist);									
							if(containersearchresults != null)
							{
								custlenght=containersearchresults[0][0];
								custwidht=containersearchresults[0][1];
								custheight=containersearchresults[0][2];

								ShipManifest.setFieldValue('custrecord_ship_length',custlenght);	
								ShipManifest.setFieldValue('custrecord_ship_width',custwidht);
								ShipManifest.setFieldValue('custrecord_ship_height',custheight);
								ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
								ShipManifest.setFieldValue('custrecord_ship_pkgtype',containername);
								ShipManifest.setFieldValue('custrecord_ship_ref1',sku);
							} 
						}

					}	

					if (containerlpno != null && containerlpno != "") {
						var PackageWeightarr = getTotalWeightNew(containerlpno,site);
						var PackageWeight = 0;
						var pkgno=0;
						var pkgcount=0;
						if(PackageWeightarr!=null && PackageWeightarr!='')
						{
							PackageWeight =  PackageWeightarr[0].getValue('custrecord_ebiz_lpmaster_totwght');

							if(PackageWeight == null || PackageWeight == '' || parseFloat(PackageWeight) == 0)
								PackageWeight='0.0001';
							//case 201410770 start
							ShipManifest.setFieldValue('custrecord_ship_pkgwght',parseFloat(weight).toFixed(4));
							//ShipManifest.setFieldValue('custrecord_ship_pkgwght',PackageWeight);
							// case 201410770 end 



							pkgno = PackageWeightarr[0].getValue('custrecord_ebiz_lpmaster_pkgno');
							pkgcount = PackageWeightarr[0].getValue('custrecord_ebiz_lpmaster_pkgcount');

							nlapiLogExecution('Debug', 'lpmasterpkgno', pkgno);
							nlapiLogExecution('Debug', 'lpmasterpkgcount',pkgcount);

							if(packagecount!=null && packagecount!='' )
								ShipManifest.setFieldValue('custrecord_ship_pkgno',parseInt(packagecount.toString()));
							if(totalpackagecount!=null && totalpackagecount!='' )
								ShipManifest.setFieldValue('custrecord_ship_pkgcount',parseInt(totalpackagecount.toString()));
						}							

					}

					oldcontainer = containerlpno;

					nlapiLogExecution('Debug', 'unexpected error', 'I am success1');

				}					
			}
		}
		else
		{
			nlapiLogExecution('Debug', 'unexpected error', 'I am success2');
		}

		nlapiSubmitRecord(ShipManifest, false, true);

	}


}

function CreateShippingManifestRecord(vebizOrdNo,vContLpNo,vCarrierType,lineno,weight,packagecount,totalpackagecount,vShippingRule) {
	try {

		nlapiLogExecution('Debug', 'into CreateShippingManifestRecord','MainFunction');		
		nlapiLogExecution('Debug', 'Order #',vebizOrdNo);	
		nlapiLogExecution('Debug', 'Container LP #',vContLpNo);	
		nlapiLogExecution('Debug', 'Carrier Type',vCarrierType);
		nlapiLogExecution('Debug', 'lineno',lineno);
		nlapiLogExecution('Debug', 'weight',weight);

		if (vebizOrdNo != null && vebizOrdNo != "") 
		{
			if(IsContLpExist(vContLpNo)!='T')
			{
				var freightterms ="";
				var otherrefnum="";
				var servicelevelvalue='';

				var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
				nlapiLogExecution('Debug', 'trantype', trantype);
				//Get the records in SalesOrder
				nlapiLogExecution('Debug', 'SalesOrderList','');
				var  searchresults;
				var vCustShipCountry;
				var rec= nlapiLoadRecord(trantype, vebizOrdNo);

				/*				if(trantype=="salesorder")
				{
					searchresults =SalesOrderList(vebizOrdNo);
				}

				else if(trantype=="transferorder")
				{
					nlapiLogExecution('Debug', 'SalesOrderList','');

					var filters = new Array();
					filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
					filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('tranid');
					columns[1] = new nlobjSearchColumn('shipcarrier');
					columns[2] = new nlobjSearchColumn('shipaddress1');
					columns[3] = new nlobjSearchColumn('shipaddress2');
					columns[4] = new nlobjSearchColumn('shipcity');
					columns[5] = new nlobjSearchColumn('shipstate');
					columns[6] = new nlobjSearchColumn('shipcountry');
					columns[7] = new nlobjSearchColumn('shipzip');
					columns[8] = new nlobjSearchColumn('shipmethod');
					columns[9] = new nlobjSearchColumn('shipaddressee');
					columns[10] = new nlobjSearchColumn('custbody_salesorder_carrier');
					columns[11] = new nlobjSearchColumn('transferlocation');
					columns[12] = new nlobjSearchColumn('entity');
					columns[13] = new nlobjSearchColumn('custbodyshipping_carrier');

					searchresults = nlapiSearchRecord('transferorder', null, filters, columns);
					nlapiLogExecution('Debug', 'transferorder',searchresults.length);
				}
				nlapiLogExecution('Debug', 'SalesOrderList',searchresults);*/
				//Get the records in customrecord_ebiznet_trn_opentask
				var opentaskordersearchresult=getOpenTaskDetails(vebizOrdNo,vContLpNo);
				nlapiLogExecution('Debug', 'getOpenTaskDetails',opentaskordersearchresult);

				//Code added by Sravan to getch customer default shipping address

				//var entity=searchresults[0].getValue('entity');
				var entity=rec.getFieldValue('entity');
				var shiptocountry=rec.getFieldValue('shipcountry');
				var shipmethod=rec.getFieldText('shipmethod');
				nlapiLogExecution('Debug', 'getOpenTaskDetails entity111',entity);

				//var entityrecord ;
				var customerid;
				var resaleno='';
				if(entity != "" && entity != null)
				{
					//customerid=searchresults[0].getText('entity');
					//case# 20149845 (if condition added for getting values from vendor record if trantype is vendorreturnauthorization)
					if(trantype!="vendorreturnauthorization")
					{
						var fields = ['entityid','resalenumber','shipcountry'];
						var columns = nlapiLookupField('customer',entity,fields);
						customerid = columns.entityid;
						nlapiLogExecution('Debug', 'start of resaleno','start of resaleno');
						//var resaleno=columns.resalenumber;
						resaleno=columns.resalenumber;
						nlapiLogExecution('Debug', 'End of resaleno',resaleno);
						vCustShipCountry=columns.shipcountry;
					}
					else
					{
						var fields = ['entityid','shipcountry'];
						var columns = nlapiLookupField('vendor',entity,fields);
						customerid = columns.entityid;
						vCustShipCountry=columns.shipcountry;
						nlapiLogExecution('Debug', 'customerid in else',customerid);
						nlapiLogExecution('Debug', 'vCustShipCountry in else',vCustShipCountry);
					}
					/*try
					{
						entityrecord = nlapiLoadRecord('customer', entity);
					}
					catch(exp)
					{
						nlapiLogExecution('Debug', 'Exception in Loading Customer',exp);
						entityrecord='';
					}*/
				}
				nlapiLogExecution('Debug', 'getOpenTaskDetails entity112','entity');


				//var customerid=entityrecord.getFieldValue('entityid');
				var ShipManifest = nlapiCreateRecord('customrecord_ship_manifest');
				ShipManifest.setFieldValue('custrecord_ship_orderno',rec.getFieldValue('tranid'));
				ShipManifest.setFieldValue('custrecord_ship_carrier',rec.getFieldValue('custbody_salesorder_carrier'));
				if(resaleno!=null && resaleno!='')
					ShipManifest.setFieldValue('custrecord_ship_custom1',resaleno);
				ShipManifest.setFieldValue('custrecord_ship_city',   rec.getFieldValue('shipcity'));
				ShipManifest.setFieldValue('custrecord_ship_state',	 rec.getFieldValue('shipstate'));

				ShipManifest.setFieldValue('custrecord_ship_custid',customerid);
				ShipManifest.setFieldValue('custrecord_ship_country',rec.getFieldValue('shipcountry'));
				ShipManifest.setFieldValue('custrecord_ship_addr1',rec.getFieldValue('shipaddress1'));

				/*if(shipmethod!= "" && shipmethod!= null)
				{
					if(beginsWith(shipmethod,"Trade"))
					{
						ShipManifest.setFieldValue('custrecord_ship_custom2',"Yes");
					}
					else
					{
						ShipManifest.setFieldValue('custrecord_ship_custom2',"No");
					}
				}*/

				//sales order specific code 
				var islinelevelship="F";
				if(trantype=="salesorder")
				{
					//var contactName=searchresults[0].getText('entity');
					var contactName=rec.getFieldValue('shipattention');
					var entity=rec.getFieldText('entity');
					if(contactName!=null && contactName!='')
						contactName=contactName.replace(","," ");

					if(entity!=null && entity!='')
						entity=entity.replace(","," ");

					ShipManifest.setFieldValue('custrecord_ship_contactname',contactName);					
					ShipManifest.setFieldValue('custrecord_ship_ordertype',rec.getFieldText('custbody_nswmssoordertype'));
					var autoshipflag = getAutoShipFlagBasedOnorderType(rec.getFieldValue('custbody_nswmssoordertype'));


					ShipManifest.setFieldValue('custrecord_ebiz_ship_autoshipping', autoshipflag);

					freightterms=rec.getFieldText('custbody_nswmsfreightterms');
					otherrefnum=rec.getFieldValue('otherrefnum');			 
					ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);
					ShipManifest.setFieldValue('custrecord_ship_phone',rec.getFieldValue('custbody_customer_phone'));
					//ShipManifest.setFieldValue('custrecord_ship_zip',searchresults[0].getValue('shipzip'));
					var saturdaydelivery= rec.getFieldValue('custbody_nswmssosaturdaydelivery');
					ShipManifest.setFieldValue('custrecord_ship_satflag',saturdaydelivery);
					var cashondelivery= rec.getFieldValue('custbody_nswmscodflag');
					ShipManifest.setFieldValue('custrecord_ship_codflag',cashondelivery);
					ShipManifest.setFieldValue('custrecord_ship_email',rec.getFieldValue('email'));




					//var rec= nlapiLoadRecord('salesorder', vebizOrdNo);
					var zipvalue=rec.getFieldValue('shipzip');
					var servicelevelvalue=rec.getFieldText('shipmethod');
					var consignee=rec.getFieldValue('shipaddressee');
					var signaturerequired=rec.getFieldValue('custbody_nswmssignaturerequired');
					ShipManifest.setFieldValue('custrecord_ship_signature_req',signaturerequired);
					var shipcomplete=rec.getFieldValue('shipcomplete');
					var termscondition=rec.getFieldText('terms');
					//Start Case#: 20123475
					islinelevelship=rec.getFieldValue('ismultishipto');
					nlapiLogExecution('Debug', 'islinelevelship',islinelevelship);
					//End Case#: 20123475

					var residentialflag='F';
					residentialflag=rec.getFieldValue('shipisresidential');
					ShipManifest.setFieldValue('custrecord_ship_residential_flag',residentialflag);
					nlapiLogExecution('Debug', 'lineno',lineno);

					if(lineno!=null && lineno!='')
					{
						var shipmethodLineLevel = rec.getLineItemText('item','shipmethod',lineno);


					}

					nlapiLogExecution('Debug', 'shipmethodLineLevel',shipmethodLineLevel);
					nlapiLogExecution('Debug', 'signaturerequired',shipcomplete);
					var shiptotal="0.00";
					//Start Case # 20124736  
					if((shipcomplete=="T")&&(cashondelivery=="T"))
					{

						ShipManifest.setFieldValue('custrecord_ship_codflag','T');
						shiptotal=rec.getFieldValue('subtotal');
						ShipManifest.setFieldValue('custrecord_ship_codamount',shiptotal);
					}
					else
					{
						ShipManifest.setFieldValue('custrecord_ship_codflag','F');
						ShipManifest.setFieldValue('custrecord_ship_codamount',shiptotal);
					}
					//End Case # 20124736  
					nlapiLogExecution('Debug', 'signaturerequired',signaturerequired);
					nlapiLogExecution('Debug', 'zipvalue=',zipvalue);
					nlapiLogExecution('Debug', 'servicelevelvalue=',servicelevelvalue);
					nlapiLogExecution('Debug', 'Consignee=',consignee);

					if(consignee!="" || consignee!=null)
						ShipManifest.setFieldValue('custrecord_ship_consignee',consignee);
					else
						ShipManifest.setFieldValue('custrecord_ship_consignee',entity);


					ShipManifest.setFieldValue('custrecord_ship_servicelevel',servicelevelvalue);	

					if(servicelevelvalue==null || servicelevelvalue=='')
					{					
						ShipManifest.setFieldValue('custrecord_ship_servicelevel',shipmethodLineLevel);	

					}



					ShipManifest.setFieldValue('custrecord_ship_zip',zipvalue);
					//ShipManifest.setFieldValue('custrecord_ship_zip',custzip);
					var companyname= rec.getFieldText('custbody_nswms_company');
					ShipManifest.setFieldValue('custrecord_ship_company',companyname);
					ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
					//if(weight == null || weight == '' || parseFloat(weight) == 0)
					//	weight='0.01';
					ShipManifest.setFieldValue('custrecord_ship_actwght',parseFloat(weight).toFixed(4));


				}

				var address1=rec.getFieldValue('shipaddr1');
				var address2=rec.getFieldValue('shipaddr2');
				//var address1=custaddr1;
				//var address2=custaddr2;


				var zip=rec.getFieldValue('shipzip');
				var servicelevel=rec.getFieldText('shipmethod');

				/*nlapiLogExecution('Debug', 'Zip',zip);
				nlapiLogExecution('Debug', 'Service Level',servicelevel);
				nlapiLogExecution('Debug', 'Zip',zip);
				nlapiLogExecution('Debug', 'City',searchresults[0].getValue('shipcity'));
				nlapiLogExecution('Debug', 'State',searchresults[0].getValue('shipstate'));
				nlapiLogExecution('Debug', 'Zip=',searchresults[0].getValue('shipzip'));
				nlapiLogExecution('Debug', 'Service Level Value',searchresults[0].getText('shipmethod'));
				 */


				if(address1!=null && address1!='')
					address1=address1.replace(","," ");


				if(address2!=null && address2!='')
					address2=address2.replace(","," ");


				ShipManifest.setFieldValue('custrecord_ship_order',vebizOrdNo);
				ShipManifest.setFieldValue('custrecord_ship_custom5',"S");	
				ShipManifest.setFieldValue('custrecord_ship_void',"N");

				ShipManifest.setFieldValue('custrecord_ship_contactname',contactName);					
				ShipManifest.setFieldValue('custrecord_ship_ordertype',rec.getFieldText('custbody_nswmssoordertype'));
				freightterms=rec.getFieldText('custbody_nswmsfreightterms');
				otherrefnum=rec.getFieldValue('otherrefnum');			 
				ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);

				ShipManifest.setFieldValue('custrecord_ship_addr1',address1);
				ShipManifest.setFieldValue('custrecord_ship_addr2',address2);
				nlapiLogExecution('Debug', 'ExpShipdate',DateStamp());
				ShipManifest.setFieldValue('custrecord_ship_expshipdate',DateStamp());
				//Start Case#: 20123475

				//ShipManifest.setFieldValue('custrecord_ship_servicelevel',searchresults[0].getText('custbody_nswmssoservicelevel'));
				//ShipManifest.setFieldValue('custrecord_ship_servicelevel',searchresults[0].getText('shipmethod'));
				//shipmethod 
				//This code is customized code for factory mation, so we have commented that code as part of standard bundle, Commented by Ganesh on 1st Mar 2013
				/*if(freightterms!="SENDER")
				{
					if (entityrecord != null && entityrecord != '')
					{

						var thirdpartyacct=entityrecord.getFieldValue('thirdpartyacct');

						if((thirdpartyacct!=null)&&(thirdpartyacct!=''))
						{
							nlapiLogExecution('Debug', 'thirdpartyacct',thirdpartyacct);
							ShipManifest.setFieldValue('custrecord_ship_account',thirdpartyacct);

						}
					}
				}
				End Case#: 20123475
				Upto here
				if(lineno!=null && lineno!='')
				End factory mation Customization
				 */
				//Start Case#: 20124381
				if(freightterms!="SENDER")
				{

					//var thirdpartyacct=entityrecord.getFieldValue('thirdpartyacct');
					var thirdpartyacct=rec.getFieldValue('custbody_ebiz_thirdpartyacc');
					nlapiLogExecution('ERROR', 'thirdpartyacct',thirdpartyacct);
					if((thirdpartyacct!=null)&&(thirdpartyacct!=''))
					{
						nlapiLogExecution('ERROR', 'thirdpartyacct',thirdpartyacct);
						ShipManifest.setFieldValue('custrecord_ship_account',thirdpartyacct);

					}
				}
				nlapiLogExecution('Debug', 'freightterms', freightterms);
				var freightvalue="";
				if(freightterms=="SENDER")
				{
					freightvalue="SHP";
				}
				if(freightterms=="RECEIVER")
				{
					freightvalue="REC";
				}
				if(freightterms=="3RDPARTY")
				{
					freightvalue="TP";
				}
				nlapiLogExecution('Debug', 'freightvalue', freightvalue);
				ShipManifest.setFieldValue('custrecord_ship_paymethod',freightvalue);

				//End Case#: 20124381

				if(islinelevelship=='T')
				{
					if(entity != null && entity != '')
					{	
						try
						{
							if(trantype!="vendorreturnauthorization")
								entityrecord = nlapiLoadRecord('customer', entity);//Ganesh: We can try to replace with search results Need to think to remove this
							else
								entityrecord = nlapiLoadRecord('vendor', entity);
						}
						catch(exp)
						{
							nlapiLogExecution('Debug', 'Exception in Loading Customer',exp);
							entityrecord='';
						}
						if (entityrecord != null && entityrecord != '')
						{
							var custaddr1 = entityrecord.getFieldValue('shipaddr1');
							var custaddr2 = entityrecord.getFieldValue('shipaddr2');
							var custaddresee = entityrecord.getFieldValue('shipaddressee');
							var custcity = entityrecord.getFieldValue('shipcity');
							var custstate = entityrecord.getFieldValue('shipstate');
							var custzip = entityrecord.getFieldValue('shipzip');
							var custcountry=entityrecord.getFieldValue('shipcountry');

							ShipManifest.setFieldValue('custrecord_ship_country', custcountry);
							ShipManifest.setFieldValue('custrecord_ship_city',  custcity);
							ShipManifest.setFieldValue('custrecord_ship_state',	custstate);
							ShipManifest.setFieldValue('custrecord_ship_addr1',	custaddr1);
							ShipManifest.setFieldValue('custrecord_ship_addr2',	custaddr2);
							ShipManifest.setFieldValue('custrecord_ship_zip',	custzip);
							ShipManifest.setFieldValue('custrecord_ship_consignee',	custaddresee);
						}
					}
				}
				//nlapiLogExecution('Debug', 'hiii', 'hiii');
				nlapiLogExecution('Debug', 'vCarrierType', vCarrierType);
				/*if(vCarrierType!=null && vCarrierType!='')
				{
					var servicelevelList=GetSerViceLevel(vCarrierType);
					if((servicelevelList!=null)&&(servicelevelList !='')&&(servicelevelList.length>0))
					{
						vserlevel=servicelevelList[0].getValue('custrecord_carrier_service_level'); 
						nlapiLogExecution('Debug', 'vserlevel', vserlevel);
						ShipManifest.setFieldValue('custrecord_ship_servicelevel',vserlevel);
					}
				}*/

				var servicelevellbyshipmethod="";
				var ServiceLevelId="";
				ServiceLevelId=rec.getFieldValue('shipmethod');
				nlapiLogExecution('Debug', 'ServiceLevelId',ServiceLevelId);
				if(ServiceLevelId!=null && ServiceLevelId!="")
				{
					servicelevellbyshipmethod=GetSerViceLevelByShipmethod(ServiceLevelId);
				}
				else
				{

					var foorder= opentaskordersearchresult[0].getValue('name');
					nlapiLogExecution('Debug', 'linelevelFoOrder',foorder);
					var site=opentaskordersearchresult[0].getValue('custrecord_wms_location');
					nlapiLogExecution('Debug', 'linelevelsite',site);
					var ServiceLevelId=getlinelevelshipmethod(foorder,site);
					servicelevellbyshipmethod=GetSerViceLevelByShipmethod(ServiceLevelId);
				}
				//End Case#:20123475
				if((servicelevellbyshipmethod!=null)&&(servicelevellbyshipmethod !='')&&(servicelevellbyshipmethod.length>0))
				{
					nlapiLogExecution('Debug', 'servicelevellbyshipmethod', 'servicelevellbyshipmethod');

					var shipserviceLevel=servicelevellbyshipmethod[0].getValue('custrecord_carrier_service_level'); 
					var wmscarriertype=servicelevellbyshipmethod[0].getValue('custrecord_carrier_id'); 
					nlapiLogExecution('Debug', 'shipmethodshipserviceLevel',shipserviceLevel);
					nlapiLogExecution('Debug', 'shipmethodshipserviceLevel',wmscarriertype);
					ShipManifest.setFieldValue('custrecord_ship_servicelevel',shipserviceLevel);
					ShipManifest.setFieldValue('custrecord_ship_carrier',wmscarriertype);

				}


				if(trantype=="transferorder")
				{
					nlapiLogExecution('Debug', 'tolocationtstt', trantype);
					var tolocation = rec.getFieldValue('transferlocation');
					nlapiLogExecution('Debug', 'tolocation', tolocation);

					var record = nlapiLoadRecord('location', tolocation);

					var shipfromaddress1=record.getFieldValue('addr1');
					var shipfromaddress2=record.getFieldValue('addr2');
					var shipfromcity=record.getFieldValue('city');
					var shipfromstate=record.getFieldValue('state');
					var shipfromzipcode =record.getFieldValue('zip');
					var shipfromcompanyname=record.getFieldValue('addressee');
					var shipfromphone=record.getFieldValue('addrphone');
					var shipfromcountry =record.getFieldValue('country');
					var prefixshipmethod=record.getFieldValue('custrecord_preferred_ship_method');
					ShipManifest.setFieldValue('custrecord_ship_carrier',prefixshipmethod);
					ShipManifest.setFieldValue('custrecord_ship_city',shipfromcity);
					ShipManifest.setFieldValue('custrecord_ship_state',shipfromstate);
					ShipManifest.setFieldValue('custrecord_ship_country',shipfromcountry);
					ShipManifest.setFieldValue('custrecord_ship_addr1',shipfromaddress1);
					ShipManifest.setFieldValue('custrecord_ship_zip',shipfromzipcode);
					ShipManifest.setFieldValue('custrecord_ship_addr2',shipfromaddress2);
					ShipManifest.setFieldValue('custrecord_ship_phone',shipfromphone);
					ShipManifest.setFieldValue('custrecord_ship_custid',shipfromcompanyname);
					ShipManifest.setFieldValue('custrecord_ship_contactname',shipfromcompanyname);

				}


				if (opentaskordersearchresult != null && opentaskordersearchresult != "")
				{
					nlapiLogExecution('Debug', 'inside opentask search results', opentaskordersearchresult);

					var oldcontainer="";
					for (l = 0; l < opentaskordersearchresult.length; l++) 
					{ 
						nlapiLogExecution('Debug', 'inside opentask', containerid);

						var custlenght="";	
						var custheight="";
						var custwidht="";

						var sku="";
						var ebizskuno="";
						var uomlevel="";
						var shiplpno="";


						var containerlpno = opentaskordersearchresult[l].getValue('custrecord_container_lp_no');
						var shiplpno=opentaskordersearchresult[l].getValue('custrecord_ship_lp_no');
						sku = opentaskordersearchresult[l].getText('custrecord_sku');
						ebizskuno = opentaskordersearchresult[l].getValue('custrecord_sku');
						uomlevel = opentaskordersearchresult[l].getValue('custrecord_uom_level');					
						var name= opentaskordersearchresult[l].getValue('name');	
						nlapiLogExecution('Debug', 'Before Wave', 'Before Wave');
						var cartonwave = opentaskordersearchresult[l].getValue('custrecord_ebiz_wave_no');
						nlapiLogExecution('Debug', 'After Wave', cartonwave);
						var site=opentaskordersearchresult[l].getValue('custrecord_wms_location');
						ShipManifest.setFieldValue('custrecord_ship_location',site);
						try
						{
							var locrecord = nlapiLoadRecord('location', site);

							var shipfromaddress1=locrecord.getFieldValue('addr1');
							nlapiLogExecution('Debug', 'SalesOrder Location addr1',shipfromaddress1);
							var shipfromaddress2=locrecord.getFieldValue('addr2');
							nlapiLogExecution('Debug', 'SalesOrder Location addr2',shipfromaddress2);
							var shipfromcity=locrecord.getFieldValue('city');
							nlapiLogExecution('Debug', 'SalesOrder Location city',shipfromcity);
							var shipfromstate=locrecord.getFieldValue('state');
							nlapiLogExecution('Debug', 'SalesOrder Location state',shipfromstate);
							var shipfromzipcode =locrecord.getFieldValue('zip');
							var shipfromcompanyname=locrecord.getFieldValue('addressee');
							var shipfromphone=locrecord.getFieldValue('addrphone');
							var shipfromcountry =locrecord.getFieldValue('country');                                        
							ShipManifest.setFieldValue('custrecord_ship_from_city',shipfromcity);
							ShipManifest.setFieldValue('custrecord_ship_from_state',shipfromstate);
							//ShipManifest.setFieldValue('custrecord_ship_country',shipfromcountry);
							ShipManifest.setFieldValue('custrecord_ship_from_addr1',shipfromaddress1);
							ShipManifest.setFieldValue('custrecord_ship_from_zip',shipfromzipcode);
							ShipManifest.setFieldValue('custrecord_ship_from_addr2',shipfromaddress2);
							ShipManifest.setFieldValue('custrecord_ship_from_phone',shipfromphone);
							ShipManifest.setFieldValue('custrecord_ship_from_country',shipfromcountry);
							ShipManifest.setFieldValue('custrecord_ship_from_company',shipfromcompanyname);
						}
						catch(exp)
						{
							nlapiLogExecution('Debug', '  Exception-Location', exp);
						}
						
						
						
						
						if(oldcontainer!=containerlpno){
							ShipManifest.setFieldValue('custrecord_ship_ref3',name);
							ShipManifest.setFieldValue('custrecord_ship_waveno', cartonwave);
							if(vShippingRule=='BuildShip')
							{
								ShipManifest.setFieldValue('custrecord_ship_contlp',shiplpno);
								ShipManifest.setFieldValue('custrecord_ship_ref5',shiplpno);
							}
							else
							{
								ShipManifest.setFieldValue('custrecord_ship_contlp',containerlpno);
								ShipManifest.setFieldValue('custrecord_ship_ref5',containerlpno);
							}

							var containerid= opentaskordersearchresult[l].getValue('custrecord_container');
							var containername= opentaskordersearchresult[l].getText('custrecord_container');
							nlapiLogExecution('Debug', 'container id', containerid);
							nlapiLogExecution('Debug', 'container name', containername);
							if(containerid!="")
							{
								if(containername!="SHIPASIS")
								{
									//Lenght, Height,Width fields  in customrecord_ebiznet_container
									var containersearchresults = getContainerDims(containerid);
									if(containersearchresults != null)
									{
										custlenght=containersearchresults [0].getValue('custrecord_length');
										custwidht=containersearchresults [0].getValue('custrecord_widthcontainer');
										custheight=containersearchresults [0].getValue('custrecord_heightcontainer');								
										ShipManifest.setFieldValue('custrecord_ship_length',parseFloat(custlenght).toFixed(4));	
										ShipManifest.setFieldValue('custrecord_ship_width',parseFloat(custwidht).toFixed(4));
										ShipManifest.setFieldValue('custrecord_ship_height',parseFloat(custheight).toFixed(4));
										ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
										ShipManifest.setFieldValue('custrecord_ship_pkgtype',containername);
									} 
								}
								else
								{
									var containersearchresults = getSKUDims(ebizskuno,uomlevel);
									if(containersearchresults != null)
									{
										custlenght=containersearchresults [0].getValue('custrecord_ebizlength');
										custwidht=containersearchresults [0].getValue('custrecord_ebizwidth');
										custheight=containersearchresults [0].getValue('custrecord_ebizheight');								
										ShipManifest.setFieldValue('custrecord_ship_length',parseFloat(custlenght).toFixed(4));	
										ShipManifest.setFieldValue('custrecord_ship_width',parseFloat(custwidht).toFixed(4));
										ShipManifest.setFieldValue('custrecord_ship_height',parseFloat(custheight).toFixed(4));
										ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
										ShipManifest.setFieldValue('custrecord_ship_pkgtype',containername);
										ShipManifest.setFieldValue('custrecord_ship_ref1',sku);
									} 
								}

							}	

							if (containerlpno != null && containerlpno != "") {
								var PackageWeight = getTotalWeight(containerlpno);
								if(PackageWeight == null || PackageWeight == '' || parseFloat(PackageWeight) == 0)
									PackageWeight='0.0001';
								ShipManifest.setFieldValue('custrecord_ship_pkgwght',parseFloat(PackageWeight).toFixed(4));

								var lpmastersearchrecord =GetTotalPackageValues(containerlpno);
								var pkgno;
								var pkgcount;
								if(lpmastersearchrecord != null && lpmastersearchrecord != '')
								{	
									pkgno=lpmastersearchrecord[0].getValue('custrecord_ebiz_lpmaster_pkgno');
									pkgcount=lpmastersearchrecord[0].getValue('custrecord_ebiz_lpmaster_pkgcount');
								}
								nlapiLogExecution('Debug', 'lpmasterpkgno', pkgno);
								nlapiLogExecution('Debug', 'lpmasterpkgcount',pkgcount);

//								ShipManifest.setFieldValue('custrecord_ship_pkgno',parseFloat(pkgno.toString()));
//								ShipManifest.setFieldValue('custrecord_ship_pkgcount',parseFloat(pkgcount.toString()));

								// Start Code  for getting packagenumber by Fo
								var sequenceNo=getpackagenumberbyfo(name,cartonwave);
								if(sequenceNo!=null && sequenceNo!='')
								{

									ShipManifest.setFieldValue('custrecord_ship_pkgno',sequenceNo.toString());

								}

								var opentaskPackcompletesearchresults=getpackingcompletd(name);
								nlapiLogExecution('Debug', 'opentaskPackcompletesearchresults', opentaskPackcompletesearchresults);
								if(opentaskPackcompletesearchresults!=null && opentaskPackcompletesearchresults!='') {

									nlapiLogExecution('Debug', 'cartonwave', cartonwave);
									var totalpackagessearchresults = getTotalpackagenumberbyfo(vebizOrdNo, name, cartonwave);	
									nlapiLogExecution('Debug', 'totalpackagessearchresults', totalpackagessearchresults);
									var totalpakages=0;
									if(totalpackagessearchresults!=null || totalpackagessearchresults!='')
									{

										totalpakages=totalpackagessearchresults.length;
										ShipManifest.setFieldValue('custrecord_ship_pkgcount',totalpakages.toString());
									}
									else
									{
										totalpakages=1;
										ShipManifest.setFieldValue('custrecord_ship_pkgcount',totalpakages.toString());
									}
								}
								// End Code for getting packagenumber by Fo

//								if(packagecount!=null && packagecount!='' )
//								ShipManifest.setFieldValue('custrecord_ship_pkgno',parseFloat(packagecount.toString()));
//								if(totalpackagecount!=null && totalpackagecount!='' )
//								ShipManifest.setFieldValue('custrecord_ship_pkgcount',parseFloat(totalpackagecount.toString()));

							}

							oldcontainer = containerlpno;

							//nlapiSubmitRecord(ShipManifest, false, true);	
							nlapiLogExecution('Debug', 'unexpected error', 'I am success1');

						}					
					}
				}
				else
				{
					nlapiLogExecution('Debug', 'unexpected error', 'I am success2');
				}
				nlapiSubmitRecord(ShipManifest, false, true);
				try
				{
					nlapiLogExecution('Debug', 'CommodityInternationalShipment', 'CommodityInternationalShipment');
					//CommodityInternationalShipment(vebizOrdNo,vContLpNo,trantype);
					CommodityInternationalShipmentNew(vebizOrdNo,vContLpNo,trantype,rec,vCustShipCountry);
				}
				catch(exp)
				{

					nlapiLogExecution('Debug', 'CommodityshipmentExp',exp);
				}
			}
		}	
	}
	catch (e) {

		InsertExceptionLog('General Functions',2, 'Create Shipping Manifest', e, vebizOrdNo, vContLpNo,vCarrierType,'','', nlapiGetUser());
		if (e instanceof nlobjError)
			nlapiLogExecution('Debug', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		//lapiLogExecution('Debug','SYS ERROR','Hd')
		else
			nlapiLogExecution('Debug', 'unexpected error', e.toString());
		nlapiLogExecution('Debug', 'unexpected error', 'I am unsuccess3');
	}

	nlapiLogExecution('Debug', 'Out of CreateShippingManifestRecord','');		
}


function getAutoShipFlagBasedOnorderType(ordertypeId) {
	nlapiLogExecution('Debug', 'into getAutoShipFlagBasedOnorderType', ordertypeId);
	var autoshipflag = "F";
	if(ordertypeId!=null && ordertypeId!='')
	{
		var ordertypefilterarray = new Array();
		ordertypefilterarray[0] = new nlobjSearchFilter('Internalid', null, 'anyof', [ordertypeId]);
		var ordertypecolumnarray = new Array();
		ordertypecolumnarray[0] = new nlobjSearchColumn('custrecord_ebiz_autoship');

		var ordertypesearchresults = nlapiSearchRecord('customrecord_ebiznet_order_type', null, ordertypefilterarray, ordertypecolumnarray);


		if (ordertypesearchresults != null && ordertypesearchresults!="") {


			autoshipflag = ordertypesearchresults[0].getValue('custrecord_ebiz_autoship');


		}
	}

	return autoshipflag;

}
function getlinelevelshipmethod(foorder,siteid)
{
	nlapiLogExecution('Debug', 'getlinelevelshipmethodinformation', foorder);
	var shipmethodId="";
	var fulfillmentfilters = new Array();
	fulfillmentfilters[0] = new nlobjSearchFilter('custrecord_lineord', null, 'is', foorder);
	if(siteid!=null && siteid!='')
	{
		fulfillmentfilters[1] = new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', [siteid]);
	}
	var fulfillmentcolumns = new Array();
	fulfillmentcolumns[0] = new nlobjSearchColumn('custrecord_do_carrier');
	fulfillmentsearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, fulfillmentfilters, fulfillmentcolumns);
	if(fulfillmentsearchresults!=null)
	{
		shipmethodId=fulfillmentsearchresults[0].getValue('custrecord_do_carrier');
	}
	nlapiLogExecution('Debug', 'getlinelevelshipmethodinformation', shipmethodId);
	return shipmethodId;	

}
function GetTotalPackageValues(vContLpNo)
{
	var filterlpmaster = new Array();
	nlapiLogExecution('Debug', 'vContLpNo',vContLpNo);
	filterlpmaster.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp',null,'is',vContLpNo));
	var columnlpmaster= new Array();
	columnlpmaster[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_pkgcount');
	columnlpmaster[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_pkgno');
	var lpmasterrecordsearch= nlapiSearchRecord('customrecord_ebiznet_master_lp',null,filterlpmaster,columnlpmaster);
	nlapiLogExecution('Debug', 'lpmasterrecordsearch',lpmasterrecordsearch);
	return lpmasterrecordsearch;
}

function IsContLpExist(vContLpNo,site)
{
	nlapiLogExecution('Debug', 'Into IsContLpExist',vContLpNo);	
	var IsContLpExist='F';

	try
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ship_contlp',null,'is',vContLpNo));
		if(site!=null)
		{
			filter.push(new nlobjSearchFilter('custrecord_ship_location',null,'is',site));	
		}
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ship_orderno');
		var manifestList= nlapiSearchRecord('customrecord_ship_manifest',null,filter,columns);
		if(manifestList!=null && manifestList.length>0)
			IsContLpExist='T';		
	}
	catch(e)
	{
		nlapiLogExecution('Debug', 'unexpected error in IsContLpExist');	
	}
	nlapiLogExecution('Debug', 'Out of IsContLpExist',IsContLpExist);	
	return IsContLpExist;
}

function GetSerViceLevelByShipmethod(shipmethodId)
{
	var servicelevelListbyshipmethod='';
	try
	{
		nlapiLogExecution('Debug', 'carrier in Shipmethod',shipmethodId);
		var filter = new Array();
		if(shipmethodId!=null && shipmethodId!='')
			filter.push(new nlobjSearchFilter('custrecord_carrier_nsmethod',null,'is',shipmethodId));
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_carrier_service_level');
		columns[1]=new nlobjSearchColumn('custrecord_carrier_id'); 
		servicelevelListbyshipmethod= nlapiSearchRecord('customrecord_ebiznet_carrier',null,filter,columns);
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'unexpectedbyshipmethod',exp.toString());	
	}
	return servicelevelListbyshipmethod;

}

function GetSerViceLevel(carrier)
{
	nlapiLogExecution('Debug', 'carrier in GetSerViceLevel',carrier);	
	var servicelevelList='';
	try
	{

		var filter = new Array();
		filter.push(new nlobjSearchFilter('name',null,'is',carrier));
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_carrier_service_level');
		servicelevelList= nlapiSearchRecord('customrecord_ebiznet_carrier',null,filter,columns);
	}
	catch(e)
	{
		nlapiLogExecution('Debug', 'unexpected error',e.toString());	
	}
	return servicelevelList;

}

function TransferorderList(vebizOrdNo)
{
	var searchresults = new Array();
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
	filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('shipcarrier');
	columns[2] = new nlobjSearchColumn('shipaddress1');
	columns[3] = new nlobjSearchColumn('shipaddress2');
	columns[4] = new nlobjSearchColumn('shipcity');
	columns[5] = new nlobjSearchColumn('shipstate');
	columns[6] = new nlobjSearchColumn('shipcountry');
	columns[7] = new nlobjSearchColumn('shipzip');
	columns[8] = new nlobjSearchColumn('shipmethod');
	columns[9] = new nlobjSearchColumn('shipaddressee');
	columns[10] = new nlobjSearchColumn('custbody_salesorder_carrier');
	columns[11] = new nlobjSearchColumn('transferlocation');
	columns[12] = new nlobjSearchColumn('entity');
	columns[13] = new nlobjSearchColumn('location');

	searchresults = nlapiSearchRecord('transferorder', null, filters, columns);
	return searchresults;
}
function SalesOrderList(vebizOrdNo) {
	nlapiLogExecution('Debug', 'General Functions', 'In to SalesOrderList');
	var searchresults = new Array();
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
	filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('shipcarrier');
	columns[2] = new nlobjSearchColumn('custbody_nswms_company');
	columns[3] = new nlobjSearchColumn('shipaddress1');
	columns[4] = new nlobjSearchColumn('shipaddress2');
	columns[5] = new nlobjSearchColumn('shipcity');
	columns[6] = new nlobjSearchColumn('shipstate');
	columns[7] = new nlobjSearchColumn('shipcountry');
	columns[8] = new nlobjSearchColumn('custbody_nswmssoordertype');
	columns[9] = new nlobjSearchColumn('entity');
	columns[10] = new nlobjSearchColumn('custbody_nswmsfreightterms');
	columns[11] = new nlobjSearchColumn('custbody_customer_phone');
	columns[12] = new nlobjSearchColumn('shipzip');
	columns[13] = new nlobjSearchColumn('email');
	columns[14] = new nlobjSearchColumn('custbody_nswmssosaturdaydelivery');
	columns[15] = new nlobjSearchColumn('custbody_nswmscodflag');
	columns[16] = new nlobjSearchColumn('shipmethod');
	columns[17] = new nlobjSearchColumn('otherrefnum');
	columns[18] = new nlobjSearchColumn('shipattention');
	columns[19] = new nlobjSearchColumn('custbodyshipping_carrier');
	columns[20] = new nlobjSearchColumn('custbody_ebiz_thirdpartyacc');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	return searchresults;
}


function getOpenTaskDetails(vebizOrdNo,vContLpNo)
{
	nlapiLogExecution('Debug', 'General Functions', 'In to getOpenTaskDetails');
	var filter = new Array();
	var opentaskordersearchresult = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is', vebizOrdNo));
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28,7]));//Status Flag - Outbound Pack Complete)		
	filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	if(vContLpNo != null && vContLpNo != '')
		filter.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vContLpNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_comp_id');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_container');
	columns[3] = new nlobjSearchColumn('custrecord_total_weight');
	columns[4] = new nlobjSearchColumn('custrecord_sku');
	columns[5] = new nlobjSearchColumn('custrecord_uom_level');
	columns[6] = new nlobjSearchColumn('name');
	columns[7] = new nlobjSearchColumn('custrecord_ship_lp_no');
	columns[8] = new nlobjSearchColumn('custrecord_wms_location');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[10] = new nlobjSearchColumn('custrecord_mast_ebizlp_no');
	columns[11] = new nlobjSearchColumn('custrecord_bulk_pick_flag');
	columns[1].setSort();

	opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, columns);
	return  opentaskordersearchresult;
}

function getTotalWeight(containerlpno)
{
	var vWeight="";
	var vVolume="";
	var filtersCont = new Array();
	filtersCont.push(new nlobjSearchFilter('name', null, 'is', containerlpno));

	var columnsCont = new Array();
	columnsCont[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columnsCont[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');

	var searchresultsCont = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersCont, columnsCont);
	if(searchresultsCont){
		vWeight=searchresultsCont[0].getValue('custrecord_ebiz_lpmaster_totwght');
		vVolume = searchresultsCont[0].getValue('custrecord_ebiz_lpmaster_totcube');
	}

	return vWeight;
}

function getContainerDims(containerid)
{
	var filters2 = new Array();
	filters2.push(new nlobjSearchFilter('Internalid', null, 'is', containerid));
	filters2.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns2 = new Array();
	columns2[0] = new nlobjSearchColumn('custrecord_length');
	columns2[1] = new nlobjSearchColumn('custrecord_widthcontainer');
	columns2[2] = new nlobjSearchColumn('custrecord_heightcontainer');
	var containersearchresults = nlapiSearchRecord('customrecord_ebiznet_container', null, filters2, columns2);
	return containersearchresults;
}

function getSKUDims(ebizskuno,uomlevel)
{
	var filters2 = new Array();
	filters2.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ebizskuno));
	filters2.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', uomlevel));
	filters2.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns2 = new Array();
	columns2[0] = new nlobjSearchColumn('custrecord_ebizlength');
	columns2[1] = new nlobjSearchColumn('custrecord_ebizwidth');
	columns2[2] = new nlobjSearchColumn('custrecord_ebizheight');
	var containersearchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters2, columns2);
	return containersearchresults;
}

function getAllSKUDims(itemslist)
{
	var filters2 = new Array();
	filters2.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemslist));
	filters2.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns2 = new Array();
	columns2[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	columns2[1] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	columns2[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');
	columns2[3] = new nlobjSearchColumn('custrecord_ebizqty') ;
	columns2[4] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	columns2[5] = new nlobjSearchColumn('custrecord_ebizlength');
	columns2[6] = new nlobjSearchColumn('custrecord_ebizwidth');
	columns2[7] = new nlobjSearchColumn('custrecord_ebizheight');	
	columns2[8] = new nlobjSearchColumn('custrecord_ebizcube');	

	var containersearchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters2, columns2);
	return containersearchresults;
}

function getSKUDimsbyUOMlvl(ebizskuno,uomlevel,itemdimslist)
{
	var itemdims = new Array();

	if(itemdimslist!=null && itemdimslist!='')
	{
		for (z1 = 0; z1 < itemdimslist.length; z1++) 
		{
			var dimssku = itemdimslist[z1].getValue('custrecord_ebizitemdims');
			var dimuomlevel = itemdimslist[z1].getValue('custrecord_ebizuomlevelskudim');

			if(ebizskuno==dimssku && uomlevel==dimuomlevel)
			{
				var dimslength = itemdimslist[z1].getValue('custrecord_ebizlength');
				var dimswidth = itemdimslist[z1].getValue('custrecord_ebizwidth');
				var dimsheight = itemdimslist[z1].getValue('custrecord_ebizheight');

				var currow = [dimslength,dimswidth,dimsheight];
				itemdims.push(currow);
			}
		}
	}
	return itemdims;
}

function getStageRule(Item, vCarrier, vSite, vCompany, stgDirection,ordertype){
	nlapiLogExecution('Debug', 'into getStageRule');
	nlapiLogExecution('Debug', 'Item', Item);
	nlapiLogExecution('Debug', 'vSite', vSite);
	nlapiLogExecution('Debug', 'vCompany', vCompany);
	nlapiLogExecution('Debug', 'stgDirection', stgDirection);
	nlapiLogExecution('Debug', 'ordertype', ordertype);
	var ItemFamily, ItemGroup, Location;
	var stagelocation="";
	var stagedetails=new Array();
	try
	{


		var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];

		var ItemFamily = "";
		var ItemGroup = "";
		var ItemStatus = "";
		var ItemInfo1 = "";
		var ItemInfo2 ="";
		var ItemInfo3 = "";

		if(Item!=null && Item!="")
		{
			var columns = nlapiLookupField('item', Item, fields);
			nlapiLogExecution('Debug', 'after nlapiLookupField');
			ItemFamily = columns.custitem_item_family;
			ItemGroup = columns.custitem_item_group;
			ItemStatus = columns.custitem_ebizdefskustatus;
			ItemInfo1 = columns.custitem_item_info_1;
			ItemInfo2 = columns.custitem_item_info_2;
			ItemInfo3 = columns.custitem_item_info_3;
		}
		var locgroupid = 0;
		var zoneid = 0;
		var direction = "";
		if (stgDirection == 'INB') {
			direction = "1";
		}
		else 
			if (stgDirection == 'OUB') {
				direction = "2";
			}
			else
			{
				direction = "3";
			}

		var columns = new Array();
		var filters = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
		columns[0].setSort();
		columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
		columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
		columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
		columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');
		columns[5] = new nlobjSearchColumn('name');
		columns[6] = new nlobjSearchColumn('custrecord_carrier_type','custrecord_carrierstgrule');
		columns[7] = new nlobjSearchColumn('custrecord_stgautopackflag');
		columns[8] = new nlobjSearchColumn('custrecord_carrier_name','custrecord_carrierstgrule');
		columns[9] = new nlobjSearchColumn('custrecord_carriertypestgrule');

		if (ItemFamily != null && ItemFamily != "") {
			filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
		}
		if (ItemGroup != null && ItemGroup != "") {
			filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
		}
		if (Item != null && Item != "") {
			filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
		}
		if (vCarrier != null && vCarrier != "") {
			filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

		}
		if (vSite != null && vSite != "") {
			filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

		}
		if (vCompany != null && vCompany != "") {
			filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

		}
		if (ordertype != null && ordertype != "") {
			filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@', ordertype]));

		}
		filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
		filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'noneof', ['14']));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
		if (searchresults != null) {
			for (var i = 0; i < Math.min(10, searchresults.length); i++) {
				//GetStagLpCount                
				var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

				var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');

				var stgrule = searchresults[i].getValue('name');
				var carrier = searchresults[i].getText('custrecord_carrier_type','custrecord_carrierstgrule');
				if(carrier==null || carrier=='')
					var carrier = searchresults[i].getText('custrecord_carriertypestgrule');
				var carriername = searchresults[i].getText('custrecord_carrier_name','custrecord_carrierstgrule');

				var autopackflag = searchresults[i].getValue('custrecord_stgautopackflag');
				var stagedet=new Array();

				var currentRow = [carrier,autopackflag,carriername];

				stagedet[0] = currentRow;

				if (vstgLocation != null && vstgLocation != "") {
					var vLpCnt = GetStagLpCount(vstgLocation, vSite, vCompany);
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (vLpCnt < vnoofFootPrints) {

							return stagedet;
						}
					}
					else {
						return stagedet;
					}
				}
				var LocGroup = "";
				if (stgDirection == 'INB') {
					LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
				}
				else {
					LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
				}
				if (LocGroup != null && LocGroup != "") {

					var collocGroup = new Array();
					var filtersLocGroup = new Array();
					collocGroup[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					collocGroup[0].setSort();
					collocGroup[1] = new nlobjSearchColumn('name');
					collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

					if (stgDirection == 'INB') {
						filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
					}
					else {
						filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
					}

					var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
					for (var k = 0; searchresults != null && k < searchresults.length; k++) {
						var vLocid=searchresults[k].getId();
						var vLpCnt = GetStagLpCount(vLocid, vSite, vCompany);
						var vnoofFootPrints = searchresults[k].getValue('custrecord_nooffootprints');

						//--
						if (vnoofFootPrints != null && vnoofFootPrints != "") {
							if (vLpCnt < vnoofFootPrints) {
								return stagedet;
							}
						}
						else {
							return stagedet;
						}
					}
				}
			}
		}
		nlapiLogExecution('Debug', 'out of getStageRule');
	}
	catch(exp){
		InsertExceptionLog('General Functions',2, 'getStageRule function', exp, '', '','','','', nlapiGetUser());
	}
	return stagedetails;
}

/**
 * This method will update the status to pack complete in open task for all containers
 * @param recordId
 * @param statusflag
 */
function updateStatusInOpenTask(recordId, statusflag){
	nlapiLogExecution('Debug', 'In to updateStatusInOpenTask function');

	var fieldNames = new Array(); 
	fieldNames.push('custrecord_wms_status_flag');  
	fieldNames.push('custrecord_pack_confirmed_date');

	var newValues = new Array(); 
	newValues.push(statusflag);
	newValues.push(DateStamp());

	nlapiSubmitField('customrecord_ebiznet_trn_opentask', recordId, fieldNames, newValues);

//	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);
//	transaction.setFieldValue('custrecord_wms_status_flag', statusflag);
//	transaction.setFieldValue('custrecord_pack_confirmed_date', DateStamp());
//	nlapiSubmitRecord(transaction, false, true);
//	nlapiLogExecution('Debug', 'Pack Complete - Status Flag Updated in open task',recordId);

	nlapiLogExecution('Debug', 'Out of updateStatusInOpenTask function');
}

/**
 * This method will update the status in LP master for a container
 * @param containerlp
 * @param statusflag
 */
function updateStatusInLPMaster(containerlp, statusflag){
	nlapiLogExecution('Debug', 'In to updateStatusInOpenTask function');
	nlapiLogExecution('Debug', 'containerlp in updateStatusInLPMaster',containerlp);
	var recordId="";
	var filtersSO = new Array();
	filtersSO.push(new nlobjSearchFilter('name', null, 'is', containerlp));

	var colsSO = new Array();
	colsSO[0] = new nlobjSearchColumn('name');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersSO, colsSO);
	nlapiLogExecution('Debug', 'searchresults',searchresults);
	if(searchresults){
		for (var i = 0; i < searchresults.length; i++){
			recordId = searchresults[i].getId();
			nlapiLogExecution('Debug', 'recordId',recordId);

			var fieldNames = new Array(); 
			fieldNames.push('custrecord_ebiz_lpmaster_wmsstatusflag');  			

			var newValues = new Array(); 
			newValues.push(statusflag);			

			nlapiSubmitField('customrecord_ebiznet_master_lp', recordId, fieldNames, newValues);

//			var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);
//			transaction.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', statusflag);
//			nlapiSubmitRecord(transaction, false, true);

			nlapiLogExecution('Debug', 'Pack Complete - Status Flag Updated in LP master',containerlp);
		}
	}

	nlapiLogExecution('Debug', 'Out of updateStatusInOpenTask function');
}

/**
 * This Method is used to create a PACK task
 * @param vCompId
 * @param vSiteId
 * @param vOrdNo
 * @param vFFOrdNo
 * @param vContLP
 * @param vTaskType
 * @param vBeginLoc
 * @param vEndLoc
 * @param vStatusFlag
 * @param vEbizCntrlNo
 */

function CreatePACKTask(vCompId,vSiteId,vOrdNo,vFFOrdNo,vContLP,vTaskType,vBeginLoc,vEndLoc,vStatusFlag,vEbizCntrlNo,item) {
	nlapiLogExecution('Debug', 'In to CreatePACKTask function');
	nlapiLogExecution('Debug', 'Company',vCompId);
	nlapiLogExecution('Debug', 'Site',vSiteId);
	nlapiLogExecution('Debug', 'FF Order #',vFFOrdNo);
	nlapiLogExecution('Debug', 'FF Internal Order #',vOrdNo);
	nlapiLogExecution('Debug', 'Container #',vContLP);
	nlapiLogExecution('Debug', 'Begin Location',vBeginLoc);
	nlapiLogExecution('Debug', 'End Location',vEndLoc);
	nlapiLogExecution('Debug', 'Task Type',vTaskType);
	nlapiLogExecution('Debug', 'Status Flag',vStatusFlag);
	nlapiLogExecution('Debug', 'Ebiz Cntrl No',vEbizCntrlNo);
	nlapiLogExecution('Debug', 'item newly added',item);

	//code added on 17Feb 2012 by suman
	//getting the seq# for the records whose tansaction type is bol and pro records from WAVE table.
	var filter_BOL_Wave=new Array();
	filter_BOL_Wave.push(new nlobjSearchFilter('custrecord_trantype',null,'is','BOL'));
	var column_BOL_Wave=new Array();
	column_BOL_Wave[0]=new nlobjSearchColumn('custrecord_seqno');

//	var searchrec_BOL_Wave=nlapiSearchRecord('customrecord_ebiznet_wave',null,filter_BOL_Wave,column_BOL_Wave);
//	var BOLSeqNo="";
//	if(searchrec_BOL_Wave!=null&&searchrec_BOL_Wave!="")
//	{
//	BOLSeqNo=searchrec_BOL_Wave[0].getValue('custrecord_seqno');
//	var  BOLIntId=searchrec_BOL_Wave[0].getId();
//	BOLSeqNo++;
//	nlapiLogExecution('Debug', 'BOLSeqNo',BOLSeqNo);
//	nlapiSubmitField('customrecord_ebiznet_wave', BOLIntId, 'custrecord_seqno', BOLSeqNo);
//	}
//	else
//	nlapiLogExecution('Debug','Exception in fetching BOL Record from WAVE Table','');

	var filter_PRO_Wave=new Array();
	filter_PRO_Wave.push(new nlobjSearchFilter('custrecord_trantype',null,'is','PRO'));
	var column_PRO_Wave=new Array();
	column_PRO_Wave[0]=new nlobjSearchColumn('custrecord_seqno');

	var searchrec_PRO_Wave=nlapiSearchRecord('customrecord_ebiznet_wave',null,filter_PRO_Wave,column_PRO_Wave);
	var PROSeqNo="";
	if(searchrec_PRO_Wave!=null&&searchrec_PRO_Wave!="")
	{
		PROSeqNo=searchrec_PRO_Wave[0].getValue('custrecord_seqno');
		var PROIntId=searchrec_PRO_Wave[0].getId();
		PROSeqNo++;
		nlapiLogExecution('Debug', 'PROSeqNo',PROSeqNo);
		nlapiSubmitField('customrecord_ebiznet_wave', PROIntId, 'custrecord_seqno', PROSeqNo);
	}
	else
		nlapiLogExecution('Debug','Exception in fetching PRO Record from WAVE Table','');
	//end of code as of 17Feb 2012.

	var t1 = new Date();
	var packrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	var t2 = new Date();
	nlapiLogExecution('Debug', 'CreatePack:nlapiCreateRecord|trn_opentask',
			getElapsedTimeDuration(t2, t1));

	nlapiLogExecution('Debug', 'Creating PACK Task','TRN_OPENTASK');

	//Generating Master BOL
	var BOLSeqNo="",childBOLSeqNo="";
	if(vCompId != null && vCompId != '')
	{
		var compsearchresults = nlapiLoadRecord('customrecord_ebiznet_company', vCompId);	 
		companyname = compsearchresults.getFieldValue('custrecord_company');
		nlapiLogExecution('Debug', 'Creating PACK Task companyname',companyname);
		BOLSeqNo=GenerateLable(companyname,vSiteId);	
	}
	BOLSeqNo=GenerateLable('Nautilus, Inc.',vSiteId);
	childBOLSeqNo=GenerateLable('Nautilus, Inc.',vSiteId);

	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();
	// populating the fields
	if(vCompId != null && vCompId != '')
		packrecord.setFieldValue('custrecord_comp_id', vCompId);	 

	if(vSiteId != null && vSiteId != '')
		packrecord.setFieldValue('custrecord_site_id', vSiteId);
	packrecord.setFieldValue('name', vFFOrdNo);
	packrecord.setFieldValue('custrecord_ebiz_cntrl_no', vEbizCntrlNo);
	packrecord.setFieldValue('custrecord_ebiz_order_no', vOrdNo);
	packrecord.setFieldValue('custrecord_container_lp_no', vContLP);
	packrecord.setFieldValue('custrecord_tasktype', vTaskType);
	packrecord.setFieldValue('custrecord_actbeginloc', vBeginLoc);
	packrecord.setFieldValue('custrecord_actendloc', vEndLoc);
	packrecord.setFieldValue('custrecord_wms_status_flag', vStatusFlag);
	packrecord.setFieldValue('custrecordact_begin_date', DateStamp());
	packrecord.setFieldValue('custrecord_act_end_date', DateStamp());
	packrecord.setFieldValue('custrecord_current_date', DateStamp());
	packrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
	packrecord.setFieldValue('custrecord_actualendtime', TimeStamp());
	packrecord.setFieldValue('custrecord_recordtime', TimeStamp());
	packrecord.setFieldValue('custrecord_ebizuser', currentUserID);
	packrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
	packrecord.setFieldValue('custrecord_pack_confirmed_date', DateStamp());

	//Code added on 17Feb 2012 by suman
	//updating BOL and PRO fields
	if(BOLSeqNo != null && BOLSeqNo != '')
		packrecord.setFieldValue('custrecord_bol', BOLSeqNo.toString());
	if(childBOLSeqNo != null && childBOLSeqNo != '')
		packrecord.setFieldValue('custrecord_sub_bol', childBOLSeqNo.toString());
	var ProNo="DYNA"+addLeadingZeros(PROSeqNo,7);
	packrecord.setFieldValue('custrecord_pro', ProNo);

	if(item!=null && item!='')
	{
		packrecord.setFieldValue('custrecord_sku', item);
		packrecord.setFieldValue('custrecord_ebiz_sku_no', item);
	}

	// commit the record to NetSuite
	t2 = new Date();
	var packrecordid = nlapiSubmitRecord(packrecord);
	t1 = new Date();
	nlapiLogExecution('Debug', 'PACK Task Created',
			getElapsedTimeDuration(t2, t1));

	nlapiLogExecution('Debug', 'Out of CreatePACKTask function');
}

function GenerateLable(company,location){	
	nlapiLogExecution('Debug', 'GenerateLable companyname',company);
	//CASE# 20123445    - START
	var finaltext="";
	var duns="";
	var ResultText="";
	//added by mahesh

	nlapiLogExecution('Debug', 'company', company);
	try 
	{	
		var lpMaxValue=GetMaxLPNo('1', '6',location);
		nlapiLogExecution('Debug', 'lpMaxValue', lpMaxValue);

		var dunsfilters = new Array();
		dunsfilters[0] = new nlobjSearchFilter('custrecord_company', null, 'is', company);
		var dunscolumns = new Array();
		dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

		var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
		if (dunssearchresults !=null && dunssearchresults !="") 
		{
			duns = dunssearchresults[0].getValue('custrecord_compduns');
			nlapiLogExecution('Debug', 'duns', duns);
		}

		var dunssubstring = duns.substring(1, duns.length);
		var label='';
		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('ERROR', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="0000000000";
		else if(prefixlength==1)
			label="000000000"+lpMaxValue;
		else if(prefixlength==2)
			label="00000000"+lpMaxValue;
		else if(prefixlength==3)
			label="0000000"+lpMaxValue;
		else if(prefixlength==4)
			label="000000"+lpMaxValue;
		else if(prefixlength==5)
			label="00000"+lpMaxValue;
		else if(prefixlength==6)
			label="0000"+lpMaxValue;
		else if(prefixlength==7)
			label="000"+lpMaxValue;
		else if(prefixlength==8)
			label="00"+lpMaxValue;
		else if(prefixlength==9)
			label="0"+lpMaxValue;
		else if(prefixlength==10)
			label=lpMaxValue;

		//finaltext=dunssubstring+"00000"+lpMaxValue;
		finaltext=dunssubstring+label;
		nlapiLogExecution('ERROR', 'finaltext', finaltext);
		//to get chk digit

		var str1=finaltext;
		var str2='1313131313131313';
		var res1=str1.split("");
		var res2=str2.split("");
		var total=0;
		for(var i=0;i < res1.length;i++)
		{
			var rchkdigit=parseInt(res1[i])+parseInt(res2[i]);
			total=parseInt(total)+parseInt(rchkdigit);
		}
//		var rem=Math.round(total/10);
//		var multiple=rem*10;
		var multiple=parseInt(Math.ceil(total / 10.0))*10;	
		var CheckDigitValue=(multiple-total).toString();	
		nlapiLogExecution('ERROR', 'CheckDigitValue', CheckDigitValue);
		ResultText=finaltext+CheckDigitValue;

	} 
	catch (err) 
	{

	}
	nlapiLogExecution('ERROR', 'GenerateLable ResultText', ResultText);
	return ResultText;
	//CASE# 20123445    - END
}

/**
 * Function to get the packing station
 * @returns {String}
 */

function getPackStation()
{
	nlapiLogExecution('Debug', 'In to getPackStation function');

	var packstation="";
	var filtersPackLoc = new Array();			
	filtersPackLoc.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'anyof', [14]));//Task Type - PACK
	filtersPackLoc.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var colsPackLoc = new Array();
	colsPackLoc[0] = new nlobjSearchColumn('custrecord_locationstgrule');			
	var searchPackLoc = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filtersPackLoc, colsPackLoc);

	if (searchPackLoc != null){
		if(searchPackLoc){
			packstation = searchPackLoc[0].getValue('custrecord_locationstgrule');		
		}
	}
	nlapiLogExecution('Debug', 'Out of getPackStation function');
	return packstation;
}



/**
 * Generic function to log the count for an array
 * 
 * @param messageLevel
 * @param listArray
 */
function logCountMessage(messageLevel, listArray){
	if(listArray != null)
		nlapiLogExecution('Debug', listArray + ' - Count', listArray.length);
	else
		nlapiLogExecution('Debug', listArray + ' - Count', '0');
}

/**
 * Function to return the number of usage units consumed by the Netsuite API call
 * 
 * @param funcName
 * @returns {Number}
 */
function getUnitsConsumed(funcName){
	var units = 0;
	switch(funcName){
	case 'nlapiDeleteFile':
		units = 20;
		break;
	case 'nlapiInitiateWorkflow':
		units = 20;
		break;
	case 'nlapiTriggerWorkflow':
		units = 20;
		break;
	case 'nlapiScheduleScript':
		units = 20;
		break;
	case 'nlapiSubmitConfiguration':
		units = 20;
		break;
	case 'nlapiSubmitFile':
		units = 20;
		break;
	case 'nlapiDeleteRecord':
		units = 4;
		break;
	case 'nlapiSubmitRecord':
		units = 4;
		break;
	case 'nlapiExchangeRate':
		units = 10;
		break;
	case 'nlapiLoadConfiguration':
		units = 10;
		break;
	case 'nlapiLoadFile':
		units = 10;
		break;
	case 'nlapiMergeRecord':
		units = 10;
		break;
	case 'nlapiRequestURL':
		units = 10;
		break;
	case 'nlapiSearchGlobal':
		units = 10;
		break;
	case 'nlapiSearchRecord':
		units = 10;
		break;
	case 'nlapiSendCampaignEmail':
		units = 10;
		break;
	case 'nlapiSendEmail':
		units = 10;
		break;
	case 'nlapiCreateRecord':
		units = 2;
		break;
	case 'nlapiCopyRecord':
		units = 2;
		break;
	case 'nlapiLookupField':
		units = 2;
		break;
	case 'nlapiLoadRecord':
		units = 2;
		break;
	case 'nlapiSubmitField':
		units = 2;
		break;
	case 'nlapiTransformRecord':
		units = 2;
		break;
	}

	return units;
}

function getSubsidiary(){
	var columns = new Array();
	columns.push(new nlobjSearchColumn('acctname'));
	columns.push(new nlobjSearchColumn('acctnumber'));
	columns.push(new nlobjSearchColumn('subsidiary'));
	columns.push(new nlobjSearchColumn('accttype'));
	columns.push(new nlobjSearchColumn('accttype2'));
	columns.push(new nlobjSearchColumn('externalid'));
	columns.push(new nlobjSearchColumn('parent'));

	var accountInfo = nlapiSearchRecord('account', null, null, columns);
	var subsidiary = accountInfo.getValue('subsidiary'); 
	return subsidiary;
}

function logTime(functionName, logInd){
	var time = new Date();
	var timeStr = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
	nlapiLogExecution('Debug', functionName, logInd + ' - ' + timeStr);
}

function getRemainingUsage(){
	var remainingUsage = nlapiGetContext().getRemainingUsage();
	nlapiLogExecution('Debug', 'Remaining Usage', remainingUsage);
}

function InsertExceptionLog(exceptionname,tran, functionality, exptmessage, reference1, reference2,reference3,reference4,reference5, userId)
{ 

	var Exceptionrecord= nlapiCreateRecord('customrecord_exception_log'); 
	Exceptionrecord.setFieldValue('name', exceptionname);
	Exceptionrecord.setFieldValue('custrecord_exceptionlog_trantype',tran);
	Exceptionrecord.setFieldValue('custrecord_exceptionlog_functionality',functionality);  
	Exceptionrecord.setFieldValue('custrecord_exceptionlog_exception',exptmessage);     
	Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference1',reference1);  
	Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference2',reference2);   
	Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference3',reference3); 
	Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference4',reference4); 
	Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference5',reference5); 
	//case 20125915 start
	if(userId!=null&&userId!=""&&parseInt(userId)>0)
		//case 20125915 end
		Exceptionrecord.setFieldValue('custrecord_exceptionlog_user',userId); 
	var tranid = nlapiSubmitRecord(Exceptionrecord,true);
	SendEmailMessage(exptmessage,reference1,reference2,functionality);
}

function SendEmailMessage(exptmessage, ref1, ref2,functionality)
{
	var userId = nlapiGetUser();
	var context = nlapiGetContext();
	nlapiLogExecution('Debug', 'userId', userId);
	var userAccountId = context.getCompany();
	nlapiLogExecution('Debug', 'userAccountId ', userAccountId );
	var rolenumber=3;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('Role', null, 'is', rolenumber);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('email');
	var searchresults = nlapiSearchRecord('Employee', null, filters, columns);
	var email= "";
	var emailappend="";
	for(var i=0;searchresults!=null && i<searchresults.length;i++)
	{
		email =searchresults[i].getValue('email');
		emailappend +=email+";";
	} 
	//nlapiLogExecution('Debug', 'email ', emailappend);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_email_alert_type', null, 'anyof', ['1']));
	filters.push(new nlobjSearchFilter('custrecord_email_trantype', null, 'anyof', ['2']));
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_user_email');
	columns[1] = new nlobjSearchColumn('custrecord_email_option');
	var searchresults = nlapiSearchRecord('customrecord_email_config', null, filters, columns);		 
	var email= "";
	var emailbcc="";
	var emailcc="";
	var emailappend="";
	var emailbccappend= new Array();
	var emailccappend=new Array();
	var count=0;
	var bcccount=0;
	if(searchresults != null && searchresults != '')
	{	
		for(var i=0;i<searchresults.length;i++)
		{
			var emailtext=searchresults[i].getText('custrecord_email_option');
			nlapiLogExecution('Debug','emailtext',emailtext);
			if(emailtext=="BCC")
			{
				emailbccappend[bcccount]=searchresults[i].getValue('custrecord_user_email');
				bcccount++;		 
			}
			else if(emailtext=="CC")
			{
				emailccappend[count]=searchresults[i].getValue('custrecord_user_email');
				count++;		 
			}
			else
			{
				email =searchresults[i].getValue('custrecord_user_email');
				emailappend +=email+";";
			}
		} 
		//emailappend +=email+"satish.nimmakayala@ebizscm.com;";

	}

	emailappend +=email+"satish.nimmakayala@ebizscm.com;";
	var substirngemail= emailappend.substring(0, emailappend.length-1);

	var strContent ="";
	strContent +="Exception  "+exptmessage;
	strContent +="<br/>";
	strContent +="AccountId "+" : "+userAccountId;
	strContent +="<br/>";
	strContent +="Fulfillment Order"+" : "+ref1+"";

	if(ref2!=null && ref2!='')
	{
		strContent +="<br/>";
		strContent +="Container Lp"+" : "+ ref2+"";
	}

	nlapiLogExecution('Debug', 'strContent ', strContent);
	var subject='';
	subject ='Exception  Occured';

	if (functionality!=null && functionality!='')
		subject = functionality;

	if(ref2==null || ref2=='')
		subject=exptmessage;

	if(ref2==null || ref2=='')
	{
		//emailappend="satish.nimmakayala@ebizscm.com;sudheer.allampati@ebizscm.com";

	}
	else
	{
		//emailappend="satish.nimmakayala@ebizscm.com;sravan.dokka@ebizscm.com;sudheer.allampati@ebizscm.com";
	}
	//case 20125915 start
	if( userId!=null && userId!= "" && parseInt(userId)>0 )
		nlapiSendEmail(userId, substirngemail,subject,strContent,emailccappend,emailbccappend,null, null);
	//case end
}

/**
 * This function will update sales order line with the qty for which FO is created.
 * @param introrderid
 * @param ordlineno
 */

function updatesalesorderline(introrderid,ordlineno,taskqty,status)
{
	nlapiLogExecution('Debug', 'Into updatesalesorderline');
	nlapiLogExecution('Debug', 'SO Id',introrderid);
	nlapiLogExecution('Debug', 'SO Line No',ordlineno);
	nlapiLogExecution('Debug', 'SO Line Qty',taskqty);
	nlapiLogExecution('Debug', 'Status',status);

	var oldqty = 0;
	var totalqty = 0;

	var TransformRecord = nlapiLoadRecord('salesorder', introrderid);

	if(status=='E')
	{
		oldqty = TransformRecord.getLineItemValue('item', 'custcol_ebizreleaseqty',ordlineno);

		if(oldqty==null || oldqty=='' || isNaN(oldqty))
			oldqty=0;

		totalqty=parseFloat(taskqty)+parseFloat(oldqty);

		nlapiLogExecution('Debug', 'SO Line Total Qty',totalqty);

		TransformRecord.setLineItemValue('item', 'custcol_ebizreleaseqty',ordlineno,parseFloat(totalqty));
	}
	else if(status=='G')
	{
		oldqty = TransformRecord.getLineItemValue('item', 'custcol_ebizpickgenqty',ordlineno);

		if(oldqty==null || oldqty=='' || isNaN(oldqty))
			oldqty=0;

		totalqty=parseFloat(taskqty)+parseFloat(oldqty);

		nlapiLogExecution('Debug', 'SO Line Total Qty',totalqty);

		TransformRecord.setLineItemValue('item', 'custcol_ebizpickgenqty',ordlineno,parseFloat(totalqty));
	}
	else if(status=='C')
	{
		oldqty = TransformRecord.getLineItemValue('item', 'custcol_ebizpickedqty',ordlineno);

		if(oldqty==null || oldqty=='' || isNaN(oldqty))
			oldqty=0;

		totalqty=parseFloat(taskqty)+parseFloat(oldqty);

		nlapiLogExecution('Debug', 'SO Line Total Qty',totalqty);

		TransformRecord.setLineItemValue('item', 'custcol_ebizpickedqty',ordlineno,parseFloat(totalqty));
	}
	else if(status=='S')
	{
		oldqty = TransformRecord.getLineItemValue('item', 'custcol_ebizshippedqty',ordlineno);

		if(oldqty==null || oldqty=='' || isNaN(oldqty))
			oldqty=0;

		totalqty=parseFloat(taskqty)+parseFloat(oldqty);

		nlapiLogExecution('Debug', 'SO Line Total Qty',totalqty);

		TransformRecord.setLineItemValue('item', 'custcol_ebizshippedqty',ordlineno,parseFloat(totalqty));
	}

	nlapiSubmitRecord(TransformRecord, true);	

	nlapiLogExecution('Debug', 'Out of updatesalesorderline');
}

function validateSKU(itemNo,location,company)
{
	nlapiLogExecution('Debug', 'Into validateSKU',itemNo);

	var actItem="";

	/*var invitemfilters = new Array();
	invitemfilters[0] = new nlobjSearchFilter('name',null, 'is',itemNo);
	var invLocCol = new Array();
	invLocCol[0] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);*/

	var sitemno = "";
	var context = nlapiGetContext();
	var userAccountId = context.getCompany();
	if(userAccountId=='1285441')
	{
		sitemno = checkserialritem(itemNo);
	}
	if(sitemno != "")
	{
		itemNo = sitemno;
	}

	// Changed On 30/4/12 by Suman

	var invitemfilters=new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemNo));
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	var invLocCol = new Array();
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	invLocCol[0] = new nlobjSearchColumn('externalid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);

	// End of Changes as On 30/4/12


	if(invitemRes!=null && invitemRes!='')
	{
		//actItem=invitemRes[0].getValue('itemid');
		actItem=invitemRes[0].getValue('externalid');

	}
	else
	{
		nlapiLogExecution('Debug', 'inSide UPCCODE',itemNo);

		var invLocfilters = new Array();
		invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));
		// Changed On 30/4/12 by Suman
		invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12

		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);

		if(invtRes!=null && invtRes!='')
		{
			//actItem=invtRes[0].getValue('itemid');
			actItem=invtRes[0].getValue('externalid');

		}
		else
		{
			nlapiLogExecution('Debug', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemNo));

			if(location!=null && location!="")					
				skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_location', null, 'anyof', ['@NONE@',location]));

			if(company!=null && company!="")					
				skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_company', null, 'anyof', ['@NONE@',company]));

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
				actItem=skuAliasResults[0].getText('custrecord_ebiz_item');

		}			
	}

	nlapiLogExecution('Debug', 'Out of validateSKU',actItem);

	return actItem;
}

function validateSKU(itemNo)
{
	nlapiLogExecution('Debug', 'Into validateSKU',itemNo);

	var actItem="";

	var sitemno = "";
	var context = nlapiGetContext();
	var userAccountId = context.getCompany();
	if(userAccountId=='1285441')
	{
		sitemno = checkserialritem(itemNo);
	}
	if(sitemno != "")
	{
		itemNo = sitemno;
	}


	var invitemfilters = new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal',null, 'is',itemNo));
	// Changed On 30/4/12 by Suman
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	var invLocCol = new Array();
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	invLocCol[0] = new nlobjSearchColumn('externalid');
	//invLocCol[0] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);
	if(invitemRes!=null && invitemRes!='')
	{
//		actItem=invitemRes[0].getValue('externalid');
		//actItem=invitemRes[0].getValue('itemid');
		actItem=invtRes[0].getId();
	}
	else
	{
		nlapiLogExecution('Debug', 'inSide UPCCODE',itemNo);

		var invLocfilters = new Array();
		invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));
		// Changed On 30/4/12 by Suman
		invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12

		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);

		if(invtRes!=null && invtRes!='')
		{
			//actItem=invtRes[0].getValue('itemid');
//			actItem=invtRes[0].getValue('externalid');
			actItem=invtRes[0].getId();

		}
		else
		{
			nlapiLogExecution('Debug', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemNo));

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
				actItem=skuAliasResults[0].getText('custrecord_ebiz_item');

		}			
	}

	nlapiLogExecution('Debug', 'Out of validateSKU',actItem);

	return actItem;
}

function SendEmailAlertForTO(exptmessage, ref1, ref2)
{
	userId = nlapiGetUser();
	var context = nlapiGetContext();
	nlapiLogExecution('Debug', 'userId', userId);
	var userAccountId = context.getCompany();
	nlapiLogExecution('Debug', 'userAccountId ', userAccountId );
	var rolenumber=3;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('internalid', null, 'is', userId );
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('email');
	var searchresults = nlapiSearchRecord('Employee', null, filters, columns);
	var email= "";
	var emailappend="";
	for(var i=0;i<searchresults.length;i++)
	{
		email =searchresults[i].getValue('email');
		emailappend +=email+";";
	} 
	//nlapiLogExecution('Debug', 'email ', emailappend);	
	var strContent ="";	
	strContent +="AccountId "+" : "+userAccountId;
	strContent +="<br/>";
	strContent +="Purchase/Transfer Order "+" : "+ref1+"";
	strContent +="<br/>";
	strContent +="Error "+" : "+exptmessage;
	//strContent +="<br/>";
	//strContent +="Container Lp"+" : "+ ref2+"";

	nlapiLogExecution('Debug', 'strContent ', strContent);
	var subject ='Item Receipt Failed';
	emailappend+="satish.nimmakayala@ebizscm.com;suneel.kota@ebizscm.com";

	nlapiSendEmail(userId, emailappend,subject,strContent, null, null, null);

}

function getAllContainersList(vlocation)
{
	nlapiLogExecution('Debug', 'into getAllContainersList', '');

	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	if(vlocation!=null && vlocation!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', vlocation));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');
	columns[1] = new nlobjSearchColumn('custrecord_cubecontainer');
	columns[2] = new nlobjSearchColumn('custrecord_maxweight');	
	columns[3] = new nlobjSearchColumn('custrecord_tareweight');
	columns[4] = new nlobjSearchColumn('custrecord_containername');
	columns[5] = new nlobjSearchColumn('custrecord_packfactor');
	columns[6] = new nlobjSearchColumn('custrecord_length');
	columns[7] = new nlobjSearchColumn('custrecord_widthcontainer');
	columns[8] = new nlobjSearchColumn('custrecord_heightcontainer');
	columns[9] = new nlobjSearchColumn('custrecord_ebizsitecontainer');
	

	columns[1].setSort(false);
	columns[2].setSort(false);

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);

	nlapiLogExecution('Debug', 'out of getAllContainersList', '');

	return containerslist;
}

////Case 20124515# Start The below changes done by Satish.N on 29OCT2013 

//The below changes done by Satish.N on 29OCT2013 - Case # 20124515

function binLocationChecknew(emptyBinLocationId,poLocn)
{
	nlapiLogExecution('Debug', 'Into binLocationChecknew',emptyBinLocationId);
	nlapiLogExecution('Debug', 'Into poLocn',poLocn);
	var binLocationCheckResult = 'Y';
	var inventoryColumns = new Array();
	inventoryColumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc',null,'group');
	//inventoryColumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');

	var inventoryFilters = new Array();
	if(emptyBinLocationId!=null && emptyBinLocationId!="")
		inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', emptyBinLocationId));

	if(poLocn!=null && poLocn!='')
		inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', poLocn));

	inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

	inventoryItemSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, inventoryFilters, inventoryColumns );
	nlapiLogExecution('Debug', 'GetLocation:inventoryItemSearchResults',inventoryItemSearchResults);

	var InvtBinLoc=new Array();
	if(inventoryItemSearchResults !=null && inventoryItemSearchResults !="")
	{
		nlapiLogExecution('Debug', 'inventoryItemSearchResults length',inventoryItemSearchResults.length);
		for ( var count = 0; count < inventoryItemSearchResults.length; count++) {
			InvtBinLoc[count]=inventoryItemSearchResults[count].getValue('custrecord_ebiz_inv_binloc',null,'group');
		}
	}
	else
	{
		InvtBinLoc[0]=-1;
	}

	var taskColumns = new Array();
	taskColumns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	//taskColumns[1] = new nlobjSearchColumn('custrecord_sku');

	var taskFilters = new Array();
	taskFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	if(emptyBinLocationId!=null && emptyBinLocationId!="")
		taskFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', emptyBinLocationId));

	if(poLocn!=null && poLocn!='')
		taskFilters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));

	var taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);
	nlapiLogExecution('Debug', 'taskSearchResults',taskSearchResults);
	var OpenTaskBinLoc=new Array();
	if(taskSearchResults!=null && taskSearchResults!="")
	{
		nlapiLogExecution('Debug', 'taskSearchResults length',taskSearchResults.length);
		for ( var count = 0; count < taskSearchResults.length; count++) 
		{
			OpenTaskBinLoc[count]=taskSearchResults[count].getValue('custrecord_actbeginloc',null,'group');

		}
	}
	else
	{
		OpenTaskBinLoc[0]=-1;
	}


	var TotalBinLoc=new Array();
	TotalBinLoc[0]=InvtBinLoc;
	TotalBinLoc[1]=OpenTaskBinLoc;
	nlapiLogExecution('Debug', 'TotalBinLoc[0]',TotalBinLoc[0]);
	nlapiLogExecution('Debug', 'TotalBinLoc[1]',TotalBinLoc[1]);
	nlapiLogExecution('Debug', 'Out of binLocationChecknew',TotalBinLoc);
	return TotalBinLoc;
}


//var taskFilters = new Array();
//taskFilters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', emptyBinLocationId);
//taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);
//if (taskSearchResults == null){
//binLocationCheckResult = 'N';
//}
//else{
//binLocationCheckResult = 'Y';
//}
//}
//else
//{
//binLocationCheckResult = 'Y';
//}
//return binLocationCheckResult;
//}
//end

//Upto here on 29OCT2013 - Case # 20124515


function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	column[5] = new nlobjSearchColumn('custrecord_ebizcube');
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}

function CommodityInternationalShipment(vebizOrdNo,vebizContainerlp,trantype)
{
	nlapiLogExecution("ERROR",'commodityShipment','CommodityShipment');
	//var salesorderrecord= nlapiLoadRecord('salesorder',vebizOrdNo);
	var salesorderrecord= nlapiLoadRecord(trantype,vebizOrdNo);

	/*var fields = [ 'recordType','tranid'];
	var columns = nlapiLookupField('transaction', poID, fields);
	var tranType = columns.recordType;
	var tranid = columns.tranid;*/
	nlapiLogExecution("ERROR",'commodityShipment tst','CommodityShipment');

	if(salesorderrecord != "" && salesorderrecord != null)
	{
		var entityvalue=salesorderrecord.getFieldValue('entity');

		var entityrecord ;
		var shiptocountry;
		if(entityvalue != "" && entityvalue != null)
		{

			entityrecord = nlapiLoadRecord('customer', entityvalue);
		}

		if(entityrecord != "" && entityrecord != null)
		{
			shiptocountry = entityrecord.getFieldValue('shipcountry');
		}

		nlapiLogExecution("ERROR",'commodityShipment',shiptocountry);
		if(shiptocountry!='US')
		{
			var filteropentask = new Array();
			//filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vebizContainerlp));
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [vebizOrdNo]));
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', [vebizContainerlp]));
			filteropentask.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));  
			var columnsopentask= new Array();
			var qtycolumns = new Array();
			columnsopentask[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
			columnsopentask[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
			columnsopentask[2] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');	
			var opentaskrecordsearch = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filteropentask, columnsopentask);
			if(opentaskrecordsearch!=null)
			{
				var itemId,itemName,itemtotalqty,upccode,itemeachweight,itemnumber,itemcountryname,containerlp;
				for(var g=0;g<opentaskrecordsearch.length;g++)
				{
					containerlp=opentaskrecordsearch[g].getValue('custrecord_container_lp_no', null,'group');
					itemId=opentaskrecordsearch[g].getValue('custrecord_sku', null, 'group');
					itemName=opentaskrecordsearch[g].getText('custrecord_sku', null, 'group');
					itemtotalqty=opentaskrecordsearch[g].getValue('custrecord_expe_qty', null, 'sum');
					nlapiLogExecution('Debug','commedityitemId',itemId);
					nlapiLogExecution('Debug','commedtiyitemName',itemName);
					nlapiLogExecution('Debug','commedityqty',itemtotalqty);
					nlapiLogExecution('Debug','containerlp',containerlp);

					var filtersku = new Array();
					filtersku.push(new nlobjSearchFilter('internalid', null,'is',itemId));
					// Changed On 30/4/12 by Suman
					filtersku.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
					// End of Changes as On 30/4/12
					var columnssku =new Array();
					columnssku[0]= new nlobjSearchColumn('upccode');
					columnssku[1]= new nlobjSearchColumn('itemid');
					columnssku[2]= new nlobjSearchColumn('displayname');
					columnssku[3]= new nlobjSearchColumn('countryofmanufacture');
					var searchitemrecord= nlapiSearchRecord('item', null, filtersku,columnssku);
					if(searchitemrecord!=null)
					{
						upccode=searchitemrecord[0].getValue('upccode');
						itemnumber=searchitemrecord[0].getValue('itemid');
						nlapiLogExecution('Debug','commedityupccode',upccode);
						itemdisplayname=searchitemrecord[0].getValue('displayname');
						itemcountryname=searchitemrecord[0].getValue('countryofmanufacture');
						nlapiLogExecution('Debug','commedityupccode',itemdisplayname);
						nlapiLogExecution('Debug','commeditycountrycode',itemcountryname);
					}
					var filteritemdimensions = new Array();
					filteritemdimensions.push(new nlobjSearchFilter('custrecord_ebizitemdims', null,'is', itemId));
					filteritemdimensions.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null,'is', 'EACH'));
					var columnitemdimensions = new Array();
					columnitemdimensions[0]= new nlobjSearchColumn('custrecord_ebizweight');
					var searchitemdimensions = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filteritemdimensions,columnitemdimensions);
					if(searchitemdimensions!=null)
					{
						itemeachweight=searchitemdimensions[0].getValue('custrecord_ebizweight');
						nlapiLogExecution('Debug','commedityitemeachweight',itemeachweight);
					}
					var tranid=salesorderrecord.getFieldValue('tranid');
					var totalLine=salesorderrecord.getLineItemCount('item');
					nlapiLogExecution('Debug', 'lineNum ', totalLine); 
					var itemunits;
					for(var i=1; i<= parseFloat(totalLine); i++)
					{
						var skuname=salesorderrecord.getLineItemText('item','item',i);
						if(itemName==skuname)
						{
							itemunits=salesorderrecord.getLineItemValue('item','rate',i);
						} 
					}
					var totalcummedityweight=parseFloat(itemeachweight)*(itemtotalqty);
					var totalcummedityunit=parseFloat(itemunits)*(itemtotalqty);
					nlapiLogExecution('Debug','commeditytotalcustomvalue',parseFloat(totalcummedityweight).toFixed(4));
					nlapiLogExecution('Debug','commeditytotalcummedityweight',totalcummedityunit);
					var commodityShipment = nlapiCreateRecord('customrecord_commodity_shipmentdetails');
					commodityShipment.setFieldValue('name', tranid); 
					commodityShipment.setFieldValue('custrecord_shipment_qty',parseFloat(itemtotalqty).toFixed(4)); 
					commodityShipment.setFieldValue('custrecord_shipment_unitofmeasure','EA');  
					commodityShipment.setFieldValue('custrecord_shipment_unitvalue',itemunits);     
					commodityShipment.setFieldValue('custrecord_shipment_totalcustomsvalue',totalcummedityunit);
					commodityShipment.setFieldValue('custrecord_shipment_totalcommodityweight',parseFloat(totalcummedityweight).toFixed(4));                                                                                                                                                                    
					commodityShipment.setFieldValue('custrecord_shipment_skuupccode', upccode);
					commodityShipment.setFieldValue('custrecord_shipment_containerlp', containerlp);
					commodityShipment.setFieldValue('custrecord_shipment_orderno', tranid);
					commodityShipment.setFieldValue('custrecord_shipment_countryofmanufacture', itemcountryname);
					commodityShipment.setFieldValue('custrecord_shipment_commoditydescription',itemdisplayname);
					var tranid = nlapiSubmitRecord(commodityShipment);
				}
			}
		}
	}
}

function CommodityInternationalShipmentNew(vebizOrdNo,vebizContainerlp,trantype,salesorderrecord,shiptocountry)
{
	nlapiLogExecution("ERROR",'commodityShipment','CommodityShipment');
	//var salesorderrecord= nlapiLoadRecord('salesorder',vebizOrdNo);
	//var salesorderrecord= nlapiLoadRecord(trantype,vebizOrdNo);

	/*var fields = [ 'recordType','tranid'];
	var columns = nlapiLookupField('transaction', poID, fields);
	var tranType = columns.recordType;
	var tranid = columns.tranid;*/
	nlapiLogExecution("ERROR",'commodityShipment tst','CommodityShipment');

	if(salesorderrecord != "" && salesorderrecord != null)
	{
		var entityvalue=salesorderrecord.getFieldValue('entity');

		/*var entityrecord ;
		var shiptocountry;
		if(entityvalue != "" && entityvalue != null)
		{

			entityrecord = nlapiLoadRecord('customer', entityvalue);
		}

		if(entityrecord != "" && entityrecord != null)
		{
			shiptocountry = entityrecord.getFieldValue('shipcountry');
		}*/

		nlapiLogExecution("ERROR",'commodityShipment',shiptocountry);
		if(shiptocountry!='US')
		{
			var filteropentask = new Array();
			//filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vebizContainerlp));
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [vebizOrdNo]));
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', [vebizContainerlp]));
			filteropentask.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));  
			var columnsopentask= new Array();
			var qtycolumns = new Array();
			columnsopentask[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
			columnsopentask[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
			columnsopentask[2] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');	
			var opentaskrecordsearch = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filteropentask, columnsopentask);
			if(opentaskrecordsearch!=null)
			{
				var itemId,itemName,itemtotalqty,upccode,itemeachweight,itemnumber,itemcountryname,containerlp;
				for(var g=0;g<opentaskrecordsearch.length;g++)
				{
					containerlp=opentaskrecordsearch[g].getValue('custrecord_container_lp_no', null,'group');
					itemId=opentaskrecordsearch[g].getValue('custrecord_sku', null, 'group');
					itemName=opentaskrecordsearch[g].getText('custrecord_sku', null, 'group');
					itemtotalqty=opentaskrecordsearch[g].getValue('custrecord_expe_qty', null, 'sum');
					nlapiLogExecution('Debug','commedityitemId',itemId);
					nlapiLogExecution('Debug','commedtiyitemName',itemName);
					nlapiLogExecution('Debug','commedityqty',itemtotalqty);
					nlapiLogExecution('Debug','containerlp',containerlp);

					var filtersku = new Array();
					filtersku.push(new nlobjSearchFilter('internalid', null,'is',itemId));
					// Changed On 30/4/12 by Suman
					filtersku.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
					// End of Changes as On 30/4/12
					var columnssku =new Array();
					columnssku[0]= new nlobjSearchColumn('upccode');
					columnssku[1]= new nlobjSearchColumn('itemid');
					columnssku[2]= new nlobjSearchColumn('displayname');
					columnssku[3]= new nlobjSearchColumn('countryofmanufacture');
					var searchitemrecord= nlapiSearchRecord('item', null, filtersku,columnssku);
					if(searchitemrecord!=null)
					{
						upccode=searchitemrecord[0].getValue('upccode');
						itemnumber=searchitemrecord[0].getValue('itemid');
						nlapiLogExecution('Debug','commedityupccode',upccode);
						itemdisplayname=searchitemrecord[0].getValue('displayname');
						itemcountryname=searchitemrecord[0].getValue('countryofmanufacture');
						nlapiLogExecution('Debug','commedityupccode',itemdisplayname);
						nlapiLogExecution('Debug','commeditycountrycode',itemcountryname);
					}
					var filteritemdimensions = new Array();
					filteritemdimensions.push(new nlobjSearchFilter('custrecord_ebizitemdims', null,'is', itemId));
					filteritemdimensions.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null,'is', 'EACH'));
					var columnitemdimensions = new Array();
					columnitemdimensions[0]= new nlobjSearchColumn('custrecord_ebizweight');
					var searchitemdimensions = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filteritemdimensions,columnitemdimensions);
					if(searchitemdimensions!=null)
					{
						itemeachweight=searchitemdimensions[0].getValue('custrecord_ebizweight');
						nlapiLogExecution('Debug','commedityitemeachweight',itemeachweight);
					}
					var tranid=salesorderrecord.getFieldValue('tranid');
					var totalLine=salesorderrecord.getLineItemCount('item');
					nlapiLogExecution('Debug', 'lineNum ', totalLine); 
					var itemunits;
					for(var i=1; i<= parseFloat(totalLine); i++)
					{
						var skuname=salesorderrecord.getLineItemText('item','item',i);
						if(itemName==skuname)
						{
							itemunits=salesorderrecord.getLineItemValue('item','rate',i);
						} 
					}
					var totalcummedityweight=parseFloat(itemeachweight)*(itemtotalqty);
					var totalcummedityunit=parseFloat(itemunits)*(itemtotalqty);
					nlapiLogExecution('Debug','commeditytotalcustomvalue',parseFloat(totalcummedityweight).toFixed(4));
					nlapiLogExecution('Debug','commeditytotalcummedityweight',totalcummedityunit);
					var commodityShipment = nlapiCreateRecord('customrecord_commodity_shipmentdetails');
					commodityShipment.setFieldValue('name', tranid); 
					commodityShipment.setFieldValue('custrecord_shipment_qty',parseFloat(itemtotalqty).toFixed(4)); 
					commodityShipment.setFieldValue('custrecord_shipment_unitofmeasure','EA');  
					commodityShipment.setFieldValue('custrecord_shipment_unitvalue',itemunits);     
					commodityShipment.setFieldValue('custrecord_shipment_totalcustomsvalue',totalcummedityunit);
					commodityShipment.setFieldValue('custrecord_shipment_totalcommodityweight',parseFloat(totalcummedityweight).toFixed(4));                                                                                                                                                                    
					commodityShipment.setFieldValue('custrecord_shipment_skuupccode', upccode);
					commodityShipment.setFieldValue('custrecord_shipment_containerlp', containerlp);
					commodityShipment.setFieldValue('custrecord_shipment_orderno', tranid);
					commodityShipment.setFieldValue('custrecord_shipment_countryofmanufacture', itemcountryname);
					commodityShipment.setFieldValue('custrecord_shipment_commoditydescription',itemdisplayname);
					var tranid = nlapiSubmitRecord(commodityShipment);
				}
			}
		}
	}
}

/**
 * @param n
 * @param length
 * @returns
 */
function addLeadingZeros(n, length)
{
	try{
		var str = (n > 0 ? n : -n) + "";
		var zeros = "";
		for (var i = length - str.length; i > 0; i--)
			zeros += "0";
		zeros += str;
		return n >= 0 ? zeros : "-" + zeros;
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR",'Error in addLeadingZeros function',exp);
	}

}

function TimeStampinSec(){
	var timestamp;
	var now = new Date();
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = now.getHours();
	var curr_min = now.getMinutes();
	var curr_sec = now.getSeconds();


	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	if (curr_sec.length == 1)
		curr_sec = "0" + curr_sec;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + ":" + curr_sec + " " + a_p;

	return timestamp;
}


function GetPoOverage(ItemId,POId,WHloc)
{
	var vPOoverageQty=0;
	var poOverage=0;
	var vPOoverageChecked='F';
	var vConfig=nlapiLoadConfiguration('accountingpreferences');
	if(vConfig != null && vConfig != '')
	{
		vPOoverageChecked=vConfig.getFieldValue('OVERRECEIPTS');
	}
	nlapiLogExecution('Debug','vPOoverageChecked', vPOoverageChecked);

	var poItemFields = ['custitem_ebizpo_overage'];
	var poItemColumns = nlapiLookupField('item', ItemId, poItemFields);

	vPOoverageQty   = poItemColumns.custitem_ebizpo_overage;
	nlapiLogExecution('Debug','vPOoverageQty', vPOoverageQty);

	if(vPOoverageChecked =='T')
	{
		if(vPOoverageQty==""||vPOoverageQty==null)
			poOverage = checkPOOverage(POId,WHloc, null);
		else
			poOverage =vPOoverageQty;
	}	
	return poOverage;
}

function checkPOOverage(POId,location,company){
	nlapiLogExecution('Debug','PO Internal Id', POId);
	var poOverage = 0;

	var poFields = ['custbody_nswmsporeceipttype'];
	var poColumns = nlapiLookupField('transaction', POId, poFields);

	var receiptType = poColumns.custbody_nswmsporeceipttype;

	nlapiLogExecution('Debug','here', receiptType);

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		nlapiLogExecution('Debug','here', receiptType);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('Debug','Out of check PO Overage', poOverage);
	return poOverage;
}

function validateSKUId(itemNo)
{
	nlapiLogExecution('Debug', 'Into validateSKU',itemNo);

	var actItem="";


	var sitemno = "";
	var context = nlapiGetContext();
	var userAccountId = context.getCompany();
	if(userAccountId=='1285441')
	{
		sitemno = checkserialritem(itemNo);
	}
	if(sitemno != "")
	{
		itemNo = sitemno;
	}

	var invitemfilters = new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal',null, 'is',itemNo));
	// Changed On 30/4/12 by Suman
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	var invLocCol = new Array();
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	invLocCol[0] = new nlobjSearchColumn('externalid');
	//invLocCol[0] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);
	if(invitemRes!=null && invitemRes!='')
	{
		//actItem=invitemRes[0].getValue('externalid');
		actItem=invitemRes[0].getId();
	}
	else
	{
		nlapiLogExecution('Debug', 'inSide UPCCODE',itemNo);

		var invLocfilters = new Array();
		invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));
		invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);

		if(invtRes!=null && invtRes!='')
		{
			//actItem=invtRes[0].getValue('externalid');
			actItem=invtRes[0].getId();
		}
		else
		{
			nlapiLogExecution('Debug', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemNo));

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
			{
				actItem=skuAliasResults[0].getValue('custrecord_ebiz_item');
				/*var Itype = nlapiLookupField('item', actItem, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, actItem);			
				actItem=	ItemRec.getFieldValue('itemid');*/
			}

		}			
	}

	nlapiLogExecution('Debug', 'Out of validateSKU',actItem);

	return actItem;
}

function getSubsidiaryNew(location)
{
	if(location != null && location !='')
	{	
		var vSubsidiary;
		var locRec= nlapiLoadRecord('location', location);
		if(locRec != null && locRec != '')
			vSubsidiary=locRec.getFieldValue('subsidiary');
		if(vSubsidiary != null && vSubsidiary != '')
			return vSubsidiary;
		else 
			return null;
	}
	else
		return null;

}


/**
 * Updates the bin location volume for the specified location using parent child link
 * @param locnId
 * @param remainingCube
 */
function UpdateLocCubeParent(locnId, remainingCube,parent){
	/*var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_remainingcube';
	values[0] = parseFloat(remainingCube);*/

	nlapiLogExecution('Debug','Location Internal Id', locnId);

	nlapiLogExecution('Debug','Location Internal IdremainingCube', remainingCube);

	parent.selectNewLineItem('recmachcustrecord_ebiz_binloc_parent'); //select a new line from the "Child 1" sublist
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_binloc_parent', 'id', locnId); //add an existing child record
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_binloc_parent', 'custrecord_remainingcube', ParseFloat(remainingCube).toFixed(4)); //add an existing child record
	parent.commitLineItem('recmachcustrecord_ebiz_binloc_parent'); //commit the Child record line

	var t1 = new Date();
	//nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
	var t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiSubmitField:UpdateLocCube:location', getElapsedTimeDuration(t1, t2));
}
/**
 * 
 * @param skuID
 * @param orderQty
 * @returns {Array}
 */
function priorityPutawayNew(skuID, orderQty,location,pickFaceResults){
	nlapiLogExecution('Debug', 'priorityPutaway:Start');
	nlapiLogExecution('Debug','priorityPutaway:Item',skuID);
	nlapiLogExecution('Debug','priorityPutaway:Order Quantity',orderQty);
	var locnQtyArray = new Array();
	if (pickFaceResults != null && pickFaceResults != ''){
		nlapiLogExecution('Debug','Inside pickFaceRes','Notnull');
		nlapiLogExecution('Debug', 'pickFaceResults.length', pickFaceResults.length);


		var binLoc;
		var maxBinQty;
		var zoneid;
		var itemstatus;
		var qtyToBePlacedinPFLoc;
		var qtyforAutoBreakdown;
		for(var k=0;k<pickFaceResults.length;k++)
		{
			if(pickFaceResults[k].getValue('custrecord_pickfacesku') ==  skuID && pickFaceResults[k].getValue('custrecord_pickface_location') == location)
			{
				binLoc = pickFaceResults[k].getValue('custrecord_pickbinloc');
				maxBinQty = pickFaceResults[k].getValue('custrecord_maxqty');
				zoneid = pickFaceResults[k].getValue('custrecord_pickzone');
				itemstatus = pickFaceResults[k].getValue('custrecord_pickface_itemstatus');
			}
		}
		// Check the quantity in this Bin Location in create inventory.
		if (binLoc) {
			nlapiLogExecution('Debug','Inside binLoc','Notnull');
			// Get pending putaways
			var expectedQty = pendingReplenPutawayTasks(skuID, binLoc);
			// Check quantity in inventory
			var inventoryQOH = TotalQuantityinInvt(skuID,binLoc,location); // case# 201412220 

			if(inventoryQOH == "" || inventoryQOH == null)
				inventoryQOH=0;

			nlapiLogExecution('Debug', 'priorityPutaway:Expected Quantity', expectedQty);
			nlapiLogExecution('Debug', 'priorityPutaway:Inventory QOH', inventoryQOH);

			var fulfilQty=0;

			// Sum up the total qty that has pending replen tasks and pending
			// moves with qohqty of inventory.
			if (expectedQty =="" || expectedQty == null)
				expectedQty=0;

			var totalAvailableQty = parseFloat(inventoryQOH) + parseFloat(expectedQty);
			nlapiLogExecution('Debug','priorityPutaway:Total Available Qty',totalAvailableQty);

			// Check this quantiy is less than Max qty in that location.
			// Check if location Qty is greater than Max Bin Loc Qty.
			if (parseFloat(maxBinQty) > parseFloat(totalAvailableQty))
				fulfilQty = parseFloat(maxBinQty) - parseFloat(totalAvailableQty);

			var qtyToBePlacedinPFLoc;
			var locIdofPF;
			var qtyforAutoBreakdown;

			// Check for qty that can fit in that location with ord qty.
			if(parseFloat(fulfilQty) >= parseFloat(orderQty)){
				qtyToBePlacedinPFLoc= parseFloat(orderQty);
				locIdofPF= binLoc;
				qtyforAutoBreakdown= 0;		
			}else{
				qtyToBePlacedinPFLoc= parseFloat(fulfilQty);
				locIdofPF= binLoc;
				qtyforAutoBreakdown= parseFloat(orderQty)-parseFloat(fulfilQty);
			}
		}

		// Assigning Values into Array .
		locnQtyArray[0]= qtyforAutoBreakdown;
		locnQtyArray[1]=qtyToBePlacedinPFLoc;
		locnQtyArray[2]=binLoc;
		locnQtyArray[3]=zoneid;
		locnQtyArray[4]=itemstatus;

		nlapiLogExecution('Debug','qtyforAutoBreakdown',qtyforAutoBreakdown);
		nlapiLogExecution('Debug','qtyToBePlacedinPFLoc',qtyToBePlacedinPFLoc);
		nlapiLogExecution('Debug','binLoc',binLoc);
		nlapiLogExecution('Debug','zoneid',zoneid);
		nlapiLogExecution('Debug','itemstatus',itemstatus);

	}else{
		//Assigning Values into Array .
		locnQtyArray[0]= orderQty;
		locnQtyArray[1]=0;
		locnQtyArray[2]="";
		locnQtyArray[3]="";
		locnQtyArray[4]="";
	}


	return locnQtyArray;
}

/*function GetPutawayLocationNew(Item, PackCode, ItemStatus, UOM, ItemCube, ItemType,poLocn,ItemInfoArr,putrulesearchresults){
	var context = nlapiGetContext();
	nlapiLogExecution('Debug','Remaining usage at the start',context.getRemainingUsage());
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod,PutMethodName,PutRuleId, MergeFlag = 'F',Mixsku='F';
	nlapiLogExecution('Debug', 'ItemCube', ItemCube);//0
	nlapiLogExecution('Debug', 'ItemType', ItemType);
	var itemresults;



	var ItemFamily;
	var ItemGroup;
	var ItemInfo1;
	var ItemInfo2;
	var ItemInfo3;
	var putVelocity;
	if(ItemInfoArr!= null && ItemInfoArr != '' )
	{
		nlapiLogExecution('Debug', 'ItemInfoArr.length', ItemInfoArr.length);
		for(var p=0;p<ItemInfoArr.length;p++)
		{
			if(ItemInfoArr[p].getId()==Item)
			{
				ItemFamily = ItemInfoArr[p].getValue('custitem_item_family');
				ItemGroup = ItemInfoArr[p].getValue('custitem_item_group');
				ItemInfo1 = ItemInfoArr[p].getValue('custitem_item_info_1');
				ItemInfo2 = ItemInfoArr[p].getValue('custitem_item_info_2');
				ItemInfo3 = ItemInfoArr[p].getValue('custitem_item_info_3');
				putVelocity=ItemInfoArr[p].getValue('custitem_ebizabcvelitem');
			}	
		}	
	}	


	nlapiLogExecution('Debug', 'Item', Item);
	nlapiLogExecution('Debug', 'ItemFamily', ItemFamily);
	nlapiLogExecution('Debug', 'ItemGroup', ItemGroup);
	nlapiLogExecution('Debug', 'ItemStatus', ItemStatus);
	nlapiLogExecution('Debug', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('Debug', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('Debug', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('Debug', 'putVelocity', putVelocity);
	nlapiLogExecution('Debug', 'poLocn', poLocn);

	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenumberpickrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';
	 var vPutRules=new Array();
	if(putrulesearchresults != null && putrulesearchresults != '' && putrulesearchresults.length>0)
	{
		nlapiLogExecution('Debug', 'putrulesearchresults.length', putrulesearchresults.length);
		for(var j = 0; j < putrulesearchresults.length; j++){
			nlapiLogExecution('Debug', 'putrulesearchresults[j]', putrulesearchresults[j]);
				if((putrulesearchresults[j].getValue('custrecord_skufamilypickrule')==null||putrulesearchresults[j].getValue('custrecord_skufamilypickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skufamilypickrule')!= null && putrulesearchresults[j].getValue('custrecord_skufamilypickrule') != '' && putrulesearchresults[j].getValue('custrecord_skufamilypickrule') ==ItemFamily))	
				{
					if((putrulesearchresults[j].getValue('custrecord_skugrouppickrule')==null||putrulesearchresults[j].getValue('custrecord_skugrouppickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skugrouppickrule')!= null && putrulesearchresults[j].getValue('custrecord_skugrouppickrule') != '' && putrulesearchresults[j].getValue('custrecord_skugrouppickrule') ==ItemGroup))	
					{
						if((putrulesearchresults[j].getValue('custrecord_skustatuspickrule')==null||putrulesearchresults[j].getValue('custrecord_skustatuspickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skustatuspickrule')!= null && putrulesearchresults[j].getValue('custrecord_skustatuspickrule') != '' && putrulesearchresults[j].getValue('custrecord_skustatuspickrule') ==ItemStatus))	
						{
							if((putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule')==null||putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule')!= null && putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule') != '' && putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule') ==ItemInfo1))	
							{
								if((putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule')==null||putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule')!= null && putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule') != '' && putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule') ==ItemInfo2))	
								{
									if((putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule')==null||putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule')!= null && putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule') != '' && putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule') ==ItemInfo3))	
									{
										if((putrulesearchresults[j].getValue('custrecord_skupickrule')==null||putrulesearchresults[j].getValue('custrecord_skupickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skupickrule')!= null && putrulesearchresults[j].getValue('custrecord_skupickrule') != '' && putrulesearchresults[j].getValue('custrecord_skupickrule') ==Item))	
										{
											if((putrulesearchresults[j].getValue('custrecord_abcvelpickrule')==null||putrulesearchresults[j].getValue('custrecord_abcvelpickrule')=='')||(putrulesearchresults[j].getValue('custrecord_abcvelpickrule')!= null && putrulesearchresults[j].getValue('custrecord_abcvelpickrule') != '' && putrulesearchresults[j].getValue('custrecord_abcvelpickrule') ==putVelocity))	
											{
												if((putrulesearchresults[j].getValue('custrecord_ebizsitepickput')==null||putrulesearchresults[j].getValue('custrecord_ebizsitepickput')=='')||(putrulesearchresults[j].getValue('custrecord_ebizsitepickput')!= null && putrulesearchresults[j].getValue('custrecord_ebizsitepickput') != '' && putrulesearchresults[j].getValue('custrecord_ebizsitepickput') ==poLocn))	
												{
													vPutRules.push(j);
													nlapiLogExecution('Debug', 'j', j);
													nlapiLogExecution('Debug', 'putrulesearchresults[j].getId', putrulesearchresults[j].getId());
												}	
											}	
										}	
									}	
								}	
							}	
						}	
					}	
				}	
			}
		}
	 	var t1 = new Date();
 	var t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (vPutRules != null) {
		nlapiLogExecution('Debug', 'rules count', vPutRules.length);
		nlapiLogExecution('Debug', 'vPutRules', vPutRules);
		for (var s = 0; s < vPutRules.length; s++) {
			nlapiLogExecution('Debug', 'vPutRules[s]', vPutRules[s]);
			nlapiLogExecution('Debug', 'Id', putrulesearchresults[vPutRules[s]].getId());
			nlapiLogExecution('Debug', 'Put Rule', putrulesearchresults[vPutRules[s]].getValue('custrecord_ruleidpickrule'));
			nlapiLogExecution('Debug', 'Put Method', putrulesearchresults[vPutRules[s]].getValue('custrecord_putawaymethod'));
			nlapiLogExecution('Debug', 'Location Group Id from Putaway Rule', putrulesearchresults[vPutRules[s]].getValue('custrecord_locationgrouppickrule'));
			locgroupid = putrulesearchresults[vPutRules[s]].getValue('custrecord_locationgrouppickrule');
			zoneid = putrulesearchresults[vPutRules[s]].getValue('custrecord_putawayzonepickrule');
			PutMethod = putrulesearchresults[vPutRules[s]].getValue('custrecord_putawaymethod');
			PutMethodName = putrulesearchresults[vPutRules[s]].getText('custrecord_putawaymethod');
			PutRuleId = putrulesearchresults[vPutRules[s]].getValue('custrecord_ruleidpickrule');
			nlapiLogExecution('Debug', 'Put Rule', PutRuleId);
			nlapiLogExecution('Debug', 'Put Method', PutMethod);
			nlapiLogExecution('Debug', 'Location Group Id from Putaway Rule', locgroupid);

			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = putrulesearchresults[vPutRules[s]].getValue('custrecord_manual_location_generation');
			nlapiLogExecution('Debug', 'Manual Location Generation', ManualLocationGeneration);

			if (ManualLocationGeneration == 'F') {
				if (locgroupid != 0 && locgroupid != null) {
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

					nlapiLogExecution('Debug', 'locgroupid', locgroupid);

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]));	
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					if(poLocn != null && poLocn != "")
						filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', [poLocn]));


					t1 = new Date();
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:location',
							getElapsedTimeDuration(t1, t2));

					nlapiLogExecution('Debug', 'locResults', locResults);
					if (locResults != null) {
						nlapiLogExecution('Debug', 'ifnot ');
						for (var u = 0; u < locResults.length; u++) {

							Location = locResults[u].getValue('name');
							var locationIntId = locResults[u].getId();

							nlapiLogExecution('Debug', 'Location', Location);
							nlapiLogExecution('Debug', 'Location Id', locationIntId);

							//Commented by satish.N on 04-JAN-2012 as the search results will give the same value.
							//LocRemCube = GeteLocCube(locationIntId);
							LocRemCube = locResults[u].getValue('custrecord_remainingcube');							
							OBLocGroup = locResults[u].getValue('custrecord_outboundinvlocgroupid');
							PickSeqNo  = locResults[u].getValue('custrecord_startingpickseqno');
							IBLocGroup = locResults[u].getValue('custrecord_inboundinvlocgroupid');
							PutSeqNo   = locResults[u].getValue('custrecord_startingputseqno');

							nlapiLogExecution('Debug', 'LocRemCube', LocRemCube);
							nlapiLogExecution('Debug', 'ItemCube', ItemCube);

							if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
								RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
								location_found = true;

								LocArry[0] = Location;
								LocArry[1] = RemCube;
								LocArry[2] = locationIntId;	
								LocArry[3] = OBLocGroup;
								LocArry[4] = PickSeqNo;
								LocArry[5] = IBLocGroup;
								LocArry[6] = PutSeqNo;
								LocArry[7] = LocRemCube;
								LocArry[8] = zoneid;
								LocArry[9] = PutMethodName;
								LocArry[10] = PutRuleId;

								nlapiLogExecution('Debug', 'SA-Location', Location);
								nlapiLogExecution('Debug', 'Location Internal Id',locationIntId);
								nlapiLogExecution('Debug', 'Sa-Location Cube', RemCube);
								return LocArry;
							}

						}
					}
				}
				else {
					nlapiLogExecution('Debug', 'Fetching Location Group from Zone', zoneid);
					var mixarray =  new Array();
					if(PutMethod!=null && PutMethod!='')
						mixarray=GetMergeFlag(PutMethod);
					if(mixarray!=null && mixarray!='' && mixarray.length>0)
					{
						MergeFlag = mixarray[0][0];
						Mixsku = mixarray[0][1];
					}
					nlapiLogExecution('Debug', 'Merge Flag', MergeFlag);
					nlapiLogExecution('Debug', 'Mix SKU', Mixsku);

					if (zoneid != null && zoneid != ""){ 
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_locgroup_no');
						columns[0].setSort();
						columns[1] = new nlobjSearchColumn('custrecord_locgroup_no');

						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', zoneid));
						filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));
						t1 = new Date();
						var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
						t2 = new Date();
						nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:zone_locgroup',
								getElapsedTimeDuration(t1, t2));

						var RemCube = 0;
						var LocRemCube = 0;

						if (searchresults != null && searchresults != '') 
						{

							for (var i = 0; i < Math.min(30, searchresults.length); i++) {
								locgroupid = searchresults[i].getValue('custrecord_locgroup_no');
								nlapiLogExecution('Debug', 'Fetched Location Group Id from Putaway Zone', locgroupid);

								if (MergeFlag == 'T') {
									nlapiLogExecution('Debug', 'Inside Merge ');
									var locResults;
									nlapiLogExecution('Debug', 'ItemCube', ItemCube);
									nlapiLogExecution('Debug', 'poLocn', poLocn);
									nlapiLogExecution('Debug', 'locgroupid', locgroupid);

									try {
										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[0].setSort();
										columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
										columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
										columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
										columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

										var filters = new Array();
										if(Mixsku!='T')
										{
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
										}

										if(poLocn!=null && poLocn!='')
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [poLocn]));
										filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'is', locgroupid));
										filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));                                        

										t1 = new Date();
										locResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
										t2 = new Date();
										nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:createinv',
												getElapsedTimeDuration(t1, t2));
									} //try close.
									catch (Error) {
										nlapiLogExecution('Debug', 'Into Error', Error);
									}
									try {
										nlapiLogExecution('Debug', 'Inside Try', 'TRY');

										if (locResults != null && locResults != '') {
											nlapiLogExecution('Debug', 'LocationResults', locResults.length );
											for (var j = 0; j < locResults.length; j++) {
												nlapiLogExecution('Debug', 'locResults[j].getId()', locResults[j].getId());
												Location = locResults[j].getText('custrecord_ebiz_inv_binloc');
												var locationInternalId = locResults[j].getValue('custrecord_ebiz_inv_binloc');

												nlapiLogExecution('Debug', 'Location', Location );
												nlapiLogExecution('Debug', 'Location Id', locationInternalId);

												//LocRemCube = GeteLocCube(locationInternalId);
												LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

												nlapiLogExecution('Debug', 'Loc Rem Cube', LocRemCube);
												nlapiLogExecution('Debug', 'Item Cube', ItemCube);

												if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

													RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

													location_found = true;

													LocArry[0] = Location;
													LocArry[1] = RemCube;
													LocArry[2] = locationInternalId;		//locResults[j].getId();
													LocArry[3] = OBLocGroup;
													LocArry[4] = PickSeqNo;
													LocArry[5] = IBLocGroup;
													LocArry[6] = PutSeqNo;
													LocArry[7] = LocRemCube;
													LocArry[8] = zoneid;
													LocArry[9] = PutMethodName;
													LocArry[10] = PutRuleId;

													nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
													return LocArry;
												}
											}
										}
									} 
									catch (exps) {
										nlapiLogExecution('Debug', 'included exps', exps);
									}

									//Added by satish.N on 04-JAN-2012 to consider open taks while merging.
									if (location_found == false) {

										try{
											nlapiLogExecution('Debug', 'Inside  consider open taks while merging');

											var columns = new Array();
											columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[0].setSort();
											columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
											columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
											columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_actbeginloc');
											columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
											columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[6] = new nlobjSearchColumn('custrecord_sku');
											columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_actbeginloc');

											var filters = new Array();
											if(Mixsku!='T')
											{
												filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', Item));
											}	
											if(poLocn!=null && poLocn!='')
												filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));
											filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
											filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'is', locgroupid));
											filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_actbeginloc', 'greaterthanorequalto', ItemCube));

											locResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

											if (locResults != null && locResults != '') {
												for (var j = 0; j < locResults.length; j++) {

													Location = locResults[j].getText('custrecord_actbeginloc');
													var locationInternalId = locResults[j].getValue('custrecord_actbeginloc');

													nlapiLogExecution('Debug', 'Location', Location );
													nlapiLogExecution('Debug', 'Location Id', locationInternalId);

													LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
													OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
													PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
													IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
													PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

													nlapiLogExecution('Debug', 'Loc Rem Cube', LocRemCube);
													nlapiLogExecution('Debug', 'Item Cube', ItemCube);

													if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

														location_found = true;

														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locationInternalId;		//locResults[j].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;

														nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
														return LocArry;
													}
												}										
											}
										}
										catch(exps){
											nlapiLogExecution('Debug', 'included exps in opentasks', exps);
										}
									}
								}
								if (location_found == false) {

									//code modified by suman on 27/01/12
									//Instead of searching each and individual binloc in inventory and open task record it seems to have a goverence issue.
									//So I bought all binloc from open task and inventory records depending upon "locgr"Id and placing it in an array so that on comparing this array with the binlocation 
									//will decides weather it is empty or not.

									nlapiLogExecution('Debug', 'Location Not found for Merge Location for Location Group', locgroupid);
									var locResults = GetLocation(locgroupid,ItemCube);

									if (locResults != null && locResults != '') {

										var TotalListOfBinLoc=new Array();
										for ( var x = 0; x < locResults.length; x++) 
										{
											TotalListOfBinLoc[x]=locResults[x].getId();

										}
										var emptyloccheck =	binLocationChecknew(TotalListOfBinLoc);
										nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

										nlapiLogExecution('Debug', 'locResults.length', locResults.length);
										for (var j = 0; j < Math.min(300, locResults.length); j++) {
											var LocFlag='N';
											var emptyBinLocationId = locResults[j].getId();
											nlapiLogExecution('Debug', 'emptyBinLocationId', emptyBinLocationId);
//											var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//											nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

											for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
											{
//												nlapiLogExecution('Debug', 'emptyloccheck[0][InvBinLocCount]', emptyloccheck[0][InvBinLocCount]);

												if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('Debug', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
											for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
											{
												if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('Debug', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
//											end of code changed 27/01/12 
											nlapiLogExecution('Debug', 'LocFlag', LocFlag);
											nlapiLogExecution('Debug', 'Main Loop ', s);
											nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
											if(LocFlag == 'N'){
												//Changes done by Sarita (index i is changed to j).
												LocRemCube = locResults[j].getValue('custrecord_remainingcube');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno');

												nlapiLogExecution('Debug', 'LocRemCube', LocRemCube);
												nlapiLogExecution('Debug', 'ItemCube', ItemCube);

												if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
													RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
													location_found = true;
													Location = locResults[j].getValue('name');
													LocArry[0] = Location;
													LocArry[1] = RemCube;
													LocArry[2] = locResults[j].getId();
													LocArry[3] = OBLocGroup;
													LocArry[4] = PickSeqNo;
													LocArry[5] = IBLocGroup;
													LocArry[6] = PutSeqNo;
													LocArry[7] = LocRemCube;
													LocArry[8] = zoneid;
													LocArry[9] = PutMethodName;
													LocArry[10] = PutRuleId;

													nlapiLogExecution('Debug', 'Location', Location);
													nlapiLogExecution('Debug', 'Location Cube', RemCube);
													return LocArry;
													break;
												}
											}
										}
									}
								}
							}
						}//end of searchresult null checking
					}
				}
				nlapiLogExecution('Debug', 'Main Loop ', s);
				nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
			}
		}
	}// end of put rule for loop
	else
	{
		nlapiLogExecution('Debug', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	//  } // end of main if
	return LocArry;
}*/

function GetPutawayLocationNew(Item, PackCode, ItemStatus, UOM, ItemCube, ItemType,poLocn,ItemInfoArr,putrulesearchresults,
		vItemLocId){
	var context = nlapiGetContext();
	nlapiLogExecution('Debug','Remaining usage at the start',context.getRemainingUsage());
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod,PutMethodName,PutRuleId,PutBinLoc;
	var MergeFlag = 'F',Mixsku='F',DimentionCheck='F',EmptyBinLoc='F';

	var itemresults;

	if(ItemCube == null || ItemCube == '' || isNaN(ItemCube))
		ItemCube = 0;

	var ItemFamily;
	var ItemGroup;
	var ItemInfo1;
	var ItemInfo2;
	var ItemInfo3;
	var putVelocity;
	if(ItemInfoArr!= null && ItemInfoArr != '' )
	{
		nlapiLogExecution('Debug', 'ItemInfoArr.length', ItemInfoArr.length);
		for(var p=0;p<ItemInfoArr.length;p++)
		{
			if(ItemInfoArr[p].getId()==Item)
			{
				ItemFamily = ItemInfoArr[p].getValue('custitem_item_family');
				ItemGroup = ItemInfoArr[p].getValue('custitem_item_group');
				ItemInfo1 = ItemInfoArr[p].getValue('custitem_item_info_1');
				ItemInfo2 = ItemInfoArr[p].getValue('custitem_item_info_2');
				ItemInfo3 = ItemInfoArr[p].getValue('custitem_item_info_3');
				putVelocity=ItemInfoArr[p].getValue('custitem_ebizabcvelitem');
			}	
		}	
	}	

	var str = 'Item. = ' + Item + '<br>';				
	str = str + 'ItemFamily. = ' + ItemFamily + '<br>';
	str = str + 'ItemGroup. = ' + ItemGroup + '<br>';
	str = str + 'ItemStatus. = ' + ItemStatus + '<br>';
	str = str + 'ItemInfo1. = ' + ItemInfo1 + '<br>';
	str = str + 'ItemInfo2. = ' + ItemInfo2 + '<br>';
	str = str + 'ItemInfo3. = ' + ItemInfo3 + '<br>';
	str = str + 'putVelocity. = ' + putVelocity + '<br>';
	str = str + 'poLocn. = ' + poLocn + '<br>';
	str = str + 'vItemLocId. = ' + vItemLocId + '<br>';
	str = str + 'ItemType. = ' + ItemType + '<br>';
	str = str + 'ItemCube. = ' + ItemCube + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);


	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenumberpickrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	columns[6] = new nlobjSearchColumn('custrecord_locationpickrule');
	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';
	var vPutRules=new Array();
	if(putrulesearchresults != null && putrulesearchresults != '' && putrulesearchresults.length>0)
	{
		nlapiLogExecution('Debug', 'putrulesearchresults.length', putrulesearchresults.length);
		for(var j = 0; j < putrulesearchresults.length; j++){
			nlapiLogExecution('Debug', 'putrulesearchresults[j]', putrulesearchresults[j]);
			if((putrulesearchresults[j].getValue('custrecord_skufamilypickrule')==null||putrulesearchresults[j].getValue('custrecord_skufamilypickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skufamilypickrule')!= null && putrulesearchresults[j].getValue('custrecord_skufamilypickrule') != '' && putrulesearchresults[j].getValue('custrecord_skufamilypickrule') ==ItemFamily))	
			{ 
				//nlapiLogExecution('Debug', 'ItemFamily', putrulesearchresults[j].getValue('custrecord_skufamilypickrule'));

				if((putrulesearchresults[j].getValue('custrecord_skugrouppickrule')==null||putrulesearchresults[j].getValue('custrecord_skugrouppickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skugrouppickrule')!= null && putrulesearchresults[j].getValue('custrecord_skugrouppickrule') != '' && putrulesearchresults[j].getValue('custrecord_skugrouppickrule') ==ItemGroup))	
				{
					//nlapiLogExecution('Debug', 'ItemGroup', putrulesearchresults[j].getValue('custrecord_skugrouppickrule'));

					if((putrulesearchresults[j].getValue('custrecord_skustatuspickrule')==null||putrulesearchresults[j].getValue('custrecord_skustatuspickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skustatuspickrule')!= null && putrulesearchresults[j].getValue('custrecord_skustatuspickrule') != '' && putrulesearchresults[j].getValue('custrecord_skustatuspickrule') ==ItemStatus))	
					{
						//nlapiLogExecution('Debug', 'ItemStatus', putrulesearchresults[j].getValue('custrecord_skustatuspickrule'));

						if((putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule')==null||putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule')!= null && putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule') != '' && putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule') ==ItemInfo1))	
						{
							//nlapiLogExecution('Debug', 'Info1', putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule'));

							if((putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule')==null||putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule')!= null && putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule') != '' && putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule') ==ItemInfo2))	
							{
								//nlapiLogExecution('Debug', 'Info2', putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule'));

								if((putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule')==null||putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule')!= null && putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule') != '' && putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule') ==ItemInfo3))	
								{
									//nlapiLogExecution('Debug', 'Info3',putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule'));

									if((putrulesearchresults[j].getValue('custrecord_skupickrule')==null||putrulesearchresults[j].getValue('custrecord_skupickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skupickrule')!= null && putrulesearchresults[j].getValue('custrecord_skupickrule') != '' && putrulesearchresults[j].getValue('custrecord_skupickrule') ==Item))	
									{
										//nlapiLogExecution('Debug', 'ITEM',  putrulesearchresults[j].getValue('custrecord_skupickrule'));

										if((putrulesearchresults[j].getValue('custrecord_abcvelpickrule')==null||putrulesearchresults[j].getValue('custrecord_abcvelpickrule')=='')||(putrulesearchresults[j].getValue('custrecord_abcvelpickrule')!= null && putrulesearchresults[j].getValue('custrecord_abcvelpickrule') != '' && putrulesearchresults[j].getValue('custrecord_abcvelpickrule') ==putVelocity))	
										{
											//nlapiLogExecution('Debug', 'ABC Vel', putrulesearchresults[j].getValue('custrecord_abcvelpickrule'));

											if((putrulesearchresults[j].getValue('custrecord_ebizsitepickput')==null||putrulesearchresults[j].getValue('custrecord_ebizsitepickput')=='')||(putrulesearchresults[j].getValue('custrecord_ebizsitepickput')!= null && putrulesearchresults[j].getValue('custrecord_ebizsitepickput') != '' && putrulesearchresults[j].getValue('custrecord_ebizsitepickput') ==poLocn))	
											{
												//nlapiLogExecution('Debug', 'putrulesearchresults[j].getValue(custrecord_ebizsitepickput)', putrulesearchresults[j].getValue('custrecord_ebizsitepickput'));
												if((putrulesearchresults[j].getValue('custrecord_locationpickrule')==null||putrulesearchresults[j].getValue('custrecord_locationpickrule')=='')||(putrulesearchresults[j].getValue('custrecord_locationpickrule')!= null && putrulesearchresults[j].getValue('custrecord_locationpickrule') != ''))// && putrulesearchresults[j].getValue('custrecord_locationpickrule') ==poLocn))	
												{

													nlapiLogExecution('Debug', 'putrulesearchresults[j].getValue(custrecord_locationpickrule)', putrulesearchresults[j].getValue('custrecord_locationpickrule'));
													vPutRules.push(j);
													nlapiLogExecution('Debug', 'j', j);
													nlapiLogExecution('Debug', 'putrulesearchresults[j].getId', putrulesearchresults[j].getId());
												}	
											}
										}	
									}	
								}	
							}	
						}	
					}	
				}	
			}	
		}
	}
	var t1 = new Date();
	var t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (vPutRules != null) {
		nlapiLogExecution('Debug', 'rules count', vPutRules.length);
		nlapiLogExecution('Debug', 'vPutRules', vPutRules);

		var putzonearr = new Array();
		var putBINLocGrarr = new Array();
		var zonesearchresults = new Array();
		var vlocgrouparr = new Array();

		for (var h = 0; h < vPutRules.length; h++) {
			if(putrulesearchresults[vPutRules[h]].getValue('custrecord_putawayzonepickrule')!=null 
					&& putrulesearchresults[vPutRules[h]].getValue('custrecord_putawayzonepickrule')!='')
			{
				putzonearr.push(putrulesearchresults[vPutRules[h]].getValue('custrecord_putawayzonepickrule'));
			}
			if(putrulesearchresults[vPutRules[h]].getValue('custrecord_locationgrouppickrule')!=null 
					&& putrulesearchresults[vPutRules[h]].getValue('custrecord_locationgrouppickrule')!='')
			{
				putBINLocGrarr.push(putrulesearchresults[vPutRules[h]].getValue('custrecord_locationgrouppickrule'));
			}

		}

		if(putzonearr!=null && putzonearr!='' && putzonearr.length>0)
		{
			nlapiLogExecution('Debug', 'putzonearr', putzonearr);
			var columnszone = new Array();
			columnszone[0] = new nlobjSearchColumn('custrecord_zone_seq');
			columnszone[0].setSort();
			columnszone[1] = new nlobjSearchColumn('custrecord_locgroup_no');
			columnszone[2] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');

			var filterszone = new Array();
			filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', putzonearr));
			filterszone.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));

			zonesearchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnszone);
		}

		if((zonesearchresults!=null && zonesearchresults!='' && zonesearchresults.length>0)||
				(putBINLocGrarr!=null&&putBINLocGrarr!=""&&putBINLocGrarr.length>0)){

			//nlapiLogExecution('Debug', 'zonesearchresults length', zonesearchresults.length);
			//case# 201412858
			for (var b = 0; zonesearchresults!=null && b < zonesearchresults.length; b++) {
				if(zonesearchresults[b].getValue('custrecord_locgroup_no')!=null && zonesearchresults[b].getValue('custrecord_locgroup_no')!='')
				{
					vlocgrouparr.push(parseFloat(zonesearchresults[b].getValue('custrecord_locgroup_no')));				
				}

			}
			for (var c = 0; c < putBINLocGrarr.length; c++) {
				vlocgrouparr.push(putBINLocGrarr[c]);				
			}
			//nlapiLogExecution('Debug', 'ItemCube', ItemCube);
			//nlapiLogExecution('Debug', 'poLocn', poLocn);
			nlapiLogExecution('Debug', 'vlocgrouparr', vlocgrouparr);

			var alllocResults = GetAllLocation(vlocgrouparr,ItemCube);

			var invtlocResults;
			var openputwResults;

			try {
				var columnsinvt = new Array();
				columnsinvt[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
				columnsinvt[0].setSort();
				columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
				columnsinvt[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
				columnsinvt[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
				columnsinvt[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
				columnsinvt[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
				columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
				columnsinvt[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

				var filtersinvt = new Array();

				if(poLocn!=null && poLocn!='')
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [poLocn]));
				//filtersinvt.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', vlocgrouparr));
				filtersinvt.push(new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', vlocgrouparr));


				if(ItemCube == null || ItemCube == '' || isNaN(ItemCube))
					ItemCube = 0;

				filtersinvt.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));

				filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
				filtersinvt.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc',  'is','F'));
				//case# 20149729 starts (below condition for Inventory move process)
				if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', vItemLocId));
				//case# 20149729 ends
				t1 = new Date();
				invtlocResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);				
				t2 = new Date();
				nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:createinv',getElapsedTimeDuration(t1, t2));

				if(invtlocResults!=null && invtlocResults!='')
					nlapiLogExecution('Debug', 'invtlocResults length', invtlocResults.length);
			} //try close.
			catch (Error) {
				nlapiLogExecution('Debug', 'Into Error', Error);
			}

			var columnsputw = new Array();
			columnsputw[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
			columnsputw[0].setSort();
			columnsputw[1] = new nlobjSearchColumn('custrecord_actbeginloc');
			columnsputw[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
			columnsputw[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_actbeginloc');
			columnsputw[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
			columnsputw[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
			columnsputw[6] = new nlobjSearchColumn('custrecord_sku');
			columnsputw[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_actbeginloc');

			var filtersputw = new Array();

			if(poLocn!=null && poLocn!='')
				filtersputw.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));
			filtersputw.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
			filtersputw.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'anyof', vlocgrouparr));
			// open 2 -PUTW, 7-CYCC,9-MOVE,8-RPLN
			filtersputw.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2,7,9,8]));
			if(ItemCube!=null && ItemCube!='' && parseFloat(ItemCube)>0)
				filtersputw.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_actbeginloc', 'greaterthanorequalto', ItemCube));
			//case# 20127650 added filter to check actbinloc is active or not
			filtersputw.push(new nlobjSearchFilter('isinactive', 'custrecord_actbeginloc',  'is','F'));
			//case# 20149729 starts (below condition for Inventory move process)
			if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
				filtersputw.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', vItemLocId));
			//case# 20149729 ends

			openputwResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersputw, columnsputw);

			if(openputwResults!=null && openputwResults!='')
				nlapiLogExecution('Debug', 'openputwResults length', openputwResults.length);
		}

		for (var s = 0; s < vPutRules.length; s++) {
//			nlapiLogExecution('Debug', 'vPutRules[s]', vPutRules[s]);
//			nlapiLogExecution('Debug', 'Id', putrulesearchresults[vPutRules[s]].getId());
//			nlapiLogExecution('Debug', 'Put Rule', putrulesearchresults[vPutRules[s]].getValue('custrecord_ruleidpickrule'));
//			nlapiLogExecution('Debug', 'Put Method', putrulesearchresults[vPutRules[s]].getValue('custrecord_putawaymethod'));
//			nlapiLogExecution('Debug', 'Location Group Id from Putaway Rule', putrulesearchresults[vPutRules[s]].getValue('custrecord_locationgrouppickrule'));

			locgroupid = putrulesearchresults[vPutRules[s]].getValue('custrecord_locationgrouppickrule');
			zoneid = putrulesearchresults[vPutRules[s]].getValue('custrecord_putawayzonepickrule');
			PutMethod = putrulesearchresults[vPutRules[s]].getValue('custrecord_putawaymethod');
			PutMethodName = putrulesearchresults[vPutRules[s]].getText('custrecord_putawaymethod');
			PutRuleId = putrulesearchresults[vPutRules[s]].getValue('custrecord_ruleidpickrule');
			PutBinLoc = putrulesearchresults[vPutRules[s]].getValue('custrecord_locationpickrule');
			EmptyBinLoc = putrulesearchresults[vPutRules[s]].getValue('custrecord_emptylocation','custrecord_putawaymethod');
			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = putrulesearchresults[vPutRules[s]].getValue('custrecord_manual_location_generation');

//			nlapiLogExecution('Debug', 'Put Rule PutBinLoc', PutBinLoc);
//			nlapiLogExecution('Debug', 'Put Rule', PutRuleId);
//			nlapiLogExecution('Debug', 'Put Method', PutMethod);
//			nlapiLogExecution('Debug', 'Location Group Id from Putaway Rule', locgroupid);			
//			nlapiLogExecution('Debug', 'Manual Location Generation', ManualLocationGeneration);
//			nlapiLogExecution('Debug', 'EmptyBinLoc', EmptyBinLoc);

			var mixarray =  new Array();
			if(PutMethod!=null && PutMethod!='')
				mixarray=GetMergeFlag(PutMethod);
			if(mixarray!=null && mixarray!='' && mixarray.length>0)
			{
				MergeFlag = mixarray[0][0];
				Mixsku = mixarray[0][1];
				DimentionCheck=mixarray[0][2];
			}

			var str = 'PutRuleId. = ' + PutRuleId + '<br>';				
			str = str + 'MergeFlag. = ' + MergeFlag + '<br>';
			str = str + 'Mixsku. = ' + Mixsku + '<br>';
			str = str + 'EmptyBinLoc. = ' + EmptyBinLoc + '<br>';			

			nlapiLogExecution('DEBUG', 'Put Rule Attributes', str);

			if (ManualLocationGeneration == 'F') {
				//if (locgroupid != 0 && locgroupid != null) {
				if ((locgroupid != 0 && locgroupid != null) || (PutBinLoc != "" && PutBinLoc != null)) {
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

					nlapiLogExecution('Debug', 'locgroupid', locgroupid);

					var filters = new Array();
					if(locgroupid != null && locgroupid != "")
						filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]));	
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					if(poLocn != null && poLocn != "")
						filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', [poLocn]));

					if(PutBinLoc != null && PutBinLoc != "")
						filters.push(new nlobjSearchFilter('internalid', null, 'is', PutBinLoc));

					//below condition for Inventory move process
					if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
						filters.push(new nlobjSearchFilter('internalid', null, 'noneof', vItemLocId));
					//case# 20149564 starts(If remaining cube is lessthan itemcube updating wiyh negative values)
					if(ItemCube!=null && ItemCube!='' && parseFloat(ItemCube)>0)
						filters.push(new nlobjSearchFilter('custrecord_remainingcube', null, 'greaterthanorequalto', ItemCube));
					//case# 20149564 ends
					t1 = new Date();
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:location',
							getElapsedTimeDuration(t1, t2));


					if (locResults != null) {

						nlapiLogExecution('Debug', 'locResults length', locResults.length);

						var vbinlocarr = new Array();
						for (var xx = 0; xx < locResults.length; xx++) {
							vbinlocarr.push(locResults[xx].getId());
						}

						if(EmptyBinLoc=='T')
						{
							var emptyloccheck =	binLocationChecknew(vbinlocarr,poLocn);
						}

						for (var u = 0; u < locResults.length; u++) {
							var vBinLocationId = locResults[u].getId();
							//nlapiLogExecution('Debug', 'vBinLocationId',vBinLocationId);
							if (MergeFlag == 'T') {
								nlapiLogExecution('Debug', 'Inside Merge ');

								if (invtlocResults != null && invtlocResults != '') {
									nlapiLogExecution('Debug', 'LocationResults', invtlocResults.length );
									for (var k1 = 0; k1 < invtlocResults.length; k1++) {

										var invtBinloction = invtlocResults[k1].getValue('custrecord_ebiz_inv_binloc');
										var invtlocgroupid = invtlocResults[k1].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										var invtitem = invtlocResults[k1].getValue('custrecord_ebiz_inv_sku');

										var str = 'invtBinloction. = ' + invtBinloction + '<br>';				
										str = str + 'vBinLocationId. = ' + vBinLocationId + '<br>';
										str = str + 'Mixsku. = ' + Mixsku + '<br>';
										str = str + 'invtitem. = ' + invtitem + '<br>';		
										str = str + 'Item. = ' + Item + '<br>';	

										nlapiLogExecution('DEBUG', 'Mix Attributes', str);

										if(vBinLocationId == invtBinloction)
										{
											if(Mixsku=='T' || (Mixsku!='T' && invtitem==Item) )
											{
												nlapiLogExecution('Debug', 'invtlocResults[j].getId()', invtlocResults[k1].getId());
												Location = invtlocResults[k1].getText('custrecord_ebiz_inv_binloc');
												var locationInternalId = invtlocResults[k1].getValue('custrecord_ebiz_inv_binloc');

												LocRemCube = invtlocResults[k1].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
												OBLocGroup = invtlocResults[k1].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PickSeqNo = invtlocResults[k1].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
												IBLocGroup = invtlocResults[k1].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PutSeqNo = invtlocResults[k1].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

												var str = 'Location. = ' + Location + '<br>';				
												str = str + 'locationInternalId. = ' + locationInternalId + '<br>';
												str = str + 'LocRemCube. = ' + LocRemCube + '<br>';
												str = str + 'ItemCube. = ' + ItemCube + '<br>';	

												nlapiLogExecution('DEBUG', 'Location Attributes', str);

												if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

													RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

													location_found = true;

													LocArry[0] = Location;
													LocArry[1] = RemCube;
													LocArry[2] = locationInternalId;
													LocArry[3] = OBLocGroup;
													LocArry[4] = PickSeqNo;
													LocArry[5] = IBLocGroup;
													LocArry[6] = PutSeqNo;
													LocArry[7] = LocRemCube;
													LocArry[8] = zoneid;
													LocArry[9] = PutMethodName;
													LocArry[10] = PutRuleId;

													nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
													return LocArry;
												}
											}
										}
									}
								}
								if (location_found == false) {

									try{
										nlapiLogExecution('Debug', 'Inside  consider open taks while merging when binlocationgroup');
										nlapiLogExecution('Debug','Remaining usage before considering Open task',context.getRemainingUsage());
										nlapiLogExecution('Debug', 'vBinLocationId',vBinLocationId);

										if (openputwResults != null && openputwResults != '') {
											nlapiLogExecution('Debug', 'openputwResults length', openputwResults.length );
											for (var k2 = 0; k2 < openputwResults.length; k2++) {

												var tasklocgroupid = openputwResults[k2].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
												var taskitem = openputwResults[k2].getValue('custrecord_sku');
												var taskActbinlocation = openputwResults[k2].getValue('custrecord_actbeginloc');

												var str = 'vBinLocationId. = ' + vBinLocationId + '<br>';				
												str = str + 'taskActbinlocation. = ' + taskActbinlocation + '<br>';
												str = str + 'Mixsku. = ' + Mixsku + '<br>';
												str = str + 'taskitem. = ' + taskitem + '<br>';		
												str = str + 'Item. = ' + Item + '<br>';	

												nlapiLogExecution('DEBUG', 'Mix Attributes', str);

												if(taskActbinlocation == vBinLocationId)
												{
													if(Mixsku=='T' || (Mixsku!='T' && taskitem==Item) )
													{
														Location = openputwResults[k2].getText('custrecord_actbeginloc');
														var locationInternalId = openputwResults[k2].getValue('custrecord_actbeginloc');

														LocRemCube = openputwResults[k2].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
														OBLocGroup = openputwResults[k2].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
														PickSeqNo = openputwResults[k2].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
														IBLocGroup = openputwResults[k2].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
														PutSeqNo = openputwResults[k2].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

														var str = 'Location. = ' + Location + '<br>';				
														str = str + 'locationInternalId. = ' + locationInternalId + '<br>';
														str = str + 'LocRemCube. = ' + LocRemCube + '<br>';
														str = str + 'ItemCube. = ' + ItemCube + '<br>';	

														nlapiLogExecution('DEBUG', 'Location Attributes', str);

														if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

															RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

															location_found = true;

															LocArry[0] = Location;
															LocArry[1] = RemCube;
															LocArry[2] = locationInternalId;
															LocArry[3] = OBLocGroup;
															LocArry[4] = PickSeqNo;
															LocArry[5] = IBLocGroup;
															LocArry[6] = PutSeqNo;
															LocArry[7] = LocRemCube;
															LocArry[8] = zoneid;
															LocArry[9] = PutMethodName;
															LocArry[10] = PutRuleId;

															nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
															return LocArry;
														}
													}
												}
											}										
										}
									}
									catch(exps){
										nlapiLogExecution('Debug', 'included exps in opentasks', exps);
									}
								}
							}
							if(EmptyBinLoc=='T' && location_found == false)
							{
								//The below changes done by Satish.N on 29OCT2013 
								//*****************************************************************//
								// Case 20124515# Start
								nlapiLogExecution('Debug', 'Inside empty location check');

								nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

								//for (var j3 = 0; j3 < locResults.length; j3++) {
								var LocFlag='N';
								var emptyBinLocationId = locResults[u].getId();
								nlapiLogExecution('Debug', 'emptyBinLocationId', emptyBinLocationId);

								for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
								{
									if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
									{
										//nlapiLogExecution('Debug', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
										LocFlag='Y';
										break;
									}
								}
								for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
								{
									if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
									{
										//nlapiLogExecution('Debug', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
										LocFlag='Y';
										break;
									}
								}

								nlapiLogExecution('Debug', 'LocFlag', LocFlag);
								nlapiLogExecution('Debug', 'Main Loop ', s);
								nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
								if(LocFlag == 'N'){										
									LocRemCube = locResults[u].getValue('custrecord_remainingcube');
									OBLocGroup = locResults[u].getValue('custrecord_outboundlocgroupid');
									PickSeqNo = locResults[u].getValue('custrecord_startingpickseqno');
									IBLocGroup = locResults[u].getValue('custrecord_inboundlocgroupid');
									PutSeqNo = locResults[u].getValue('custrecord_startingputseqno');

									nlapiLogExecution('Debug', 'LocRemCube', LocRemCube);
									nlapiLogExecution('Debug', 'ItemCube', ItemCube);

									if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {
										RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
										location_found = true;
										Location = locResults[u].getValue('name');
										LocArry[0] = Location;
										LocArry[1] = RemCube;
										LocArry[2] = locResults[u].getId();
										LocArry[3] = OBLocGroup;
										LocArry[4] = PickSeqNo;
										LocArry[5] = IBLocGroup;
										LocArry[6] = PutSeqNo;
										LocArry[7] = LocRemCube;
										LocArry[8] = zoneid;
										LocArry[9] = PutMethodName;
										LocArry[10] = PutRuleId;

										nlapiLogExecution('Debug', 'Location', Location);
										nlapiLogExecution('Debug', 'Location Cube', RemCube);
										return LocArry;
									}
								}

								if(MergeFlag=="T")
								{
									nlapiLogExecution('Debug', 'Inside Merge ');

									if (invtlocResults != null && invtlocResults != '') {
										nlapiLogExecution('Debug', 'LocationResults', invtlocResults.length );
										for (var j1 = 0; j1 < invtlocResults.length; j1++) {

											var invtlocgroupid = invtlocResults[j1].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
											var invtitem = invtlocResults[j1].getValue('custrecord_ebiz_inv_sku');

											if(invtlocgroupid == locgroupid)
											{
												nlapiLogExecution('Debug', 'Mixsku',Mixsku);
												nlapiLogExecution('Debug', 'invtitem',invtitem);
												nlapiLogExecution('Debug', 'Item',Item);

												if(Mixsku=='T' || (Mixsku!='T' && invtitem==Item) )
												{
													nlapiLogExecution('Debug', 'invtlocResults[j].getId()', invtlocResults[j1].getId());
													Location = invtlocResults[j1].getText('custrecord_ebiz_inv_binloc');
													var locationInternalId = invtlocResults[j1].getValue('custrecord_ebiz_inv_binloc');

													nlapiLogExecution('Debug', 'Location', Location );
													nlapiLogExecution('Debug', 'Location Id', locationInternalId);

													//LocRemCube = GeteLocCube(locationInternalId);
													LocRemCube = invtlocResults[j1].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
													OBLocGroup = invtlocResults[j1].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
													PickSeqNo = invtlocResults[j1].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
													IBLocGroup = invtlocResults[j1].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
													PutSeqNo = invtlocResults[j1].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

													nlapiLogExecution('Debug', 'Loc Rem Cube', LocRemCube);
													nlapiLogExecution('Debug', 'Item Cube', ItemCube);

													if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

														location_found = true;

														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locationInternalId;
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;

														nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
														return LocArry;
													}
												}
											}
										}
									}
									if (location_found == false) {

										try{
											nlapiLogExecution('Debug', 'Inside  consider open taks while merging');
											nlapiLogExecution('Debug','Remaining usage before considering open taks while merging',context.getRemainingUsage());
											nlapiLogExecution('Debug', 'vzonelocgroupno',vzonelocgroupno);

											if (openputwResults != null && openputwResults != '') {
												nlapiLogExecution('Debug', 'openputwResults length', openputwResults.length );
												for (var j2 = 0; j2 < openputwResults.length; j2++) {

													var tasklocgroupid = openputwResults[j2].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
													var taskitem = openputwResults[j2].getValue('custrecord_sku');

													if(tasklocgroupid == vzonelocgroupno)
													{	
														nlapiLogExecution('Debug', 'Mixsku',Mixsku);
														nlapiLogExecution('Debug', 'taskitem',taskitem);
														nlapiLogExecution('Debug', 'Item',Item);

														if(Mixsku=='T' || (Mixsku!='T' && taskitem==Item) )
														{
															Location = openputwResults[j2].getText('custrecord_actbeginloc');
															var locationInternalId = openputwResults[j2].getValue('custrecord_actbeginloc');

															nlapiLogExecution('Debug', 'Location', Location );
															nlapiLogExecution('Debug', 'Location Id', locationInternalId);

															LocRemCube = openputwResults[j2].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
															OBLocGroup = openputwResults[j2].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
															PickSeqNo = openputwResults[j2].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
															IBLocGroup = openputwResults[j2].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
															PutSeqNo = openputwResults[j2].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

															nlapiLogExecution('Debug', 'Loc Rem Cube', LocRemCube);
															nlapiLogExecution('Debug', 'Item Cube', ItemCube);

															if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

																RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

																location_found = true;

																LocArry[0] = Location;
																LocArry[1] = RemCube;
																LocArry[2] = locationInternalId;
																LocArry[3] = OBLocGroup;
																LocArry[4] = PickSeqNo;
																LocArry[5] = IBLocGroup;
																LocArry[6] = PutSeqNo;
																LocArry[7] = LocRemCube;
																LocArry[8] = zoneid;
																LocArry[9] = PutMethodName;
																LocArry[10] = PutRuleId;

																nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
																return LocArry;
															}
														}
													}
												}										
											}
										}
										catch(exps){
											nlapiLogExecution('Debug', 'included exps in opentasks', exps);
										}
									}
								}
								//}
								//*****************************************************************//

							}//end
							//case# 20148733 starts (commented beacause when first iteration binlocid is not matching with invbinlocid and opentaskbinlocid then it is taking first bin location)
							/*	else
							{
								Location = locResults[u].getValue('name');
								var locationIntId = locResults[u].getId();

								nlapiLogExecution('Debug', 'Location', Location);
								nlapiLogExecution('Debug', 'Location Id', locationIntId);

								//Commented by satish.N on 04-JAN-2012 as the search results will give the same value.
								//LocRemCube = GeteLocCube(locationIntId);
								LocRemCube = locResults[u].getValue('custrecord_remainingcube');							
								OBLocGroup = locResults[u].getValue('custrecord_outboundinvlocgroupid');
								PickSeqNo  = locResults[u].getValue('custrecord_startingpickseqno');
								IBLocGroup = locResults[u].getValue('custrecord_inboundinvlocgroupid');
								PutSeqNo   = locResults[u].getValue('custrecord_startingputseqno');

								nlapiLogExecution('Debug', 'LocRemCube', LocRemCube);
								nlapiLogExecution('Debug', 'ItemCube', ItemCube);

								if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
									RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
									location_found = true;

									LocArry[0] = Location;
									LocArry[1] = RemCube;
									LocArry[2] = locationIntId;	
									LocArry[3] = OBLocGroup;
									LocArry[4] = PickSeqNo;
									LocArry[5] = IBLocGroup;
									LocArry[6] = PutSeqNo;
									LocArry[7] = LocRemCube;
									LocArry[8] = zoneid;
									LocArry[9] = PutMethodName;
									LocArry[10] = PutRuleId;

									nlapiLogExecution('Debug', 'SA-Location', Location);
									nlapiLogExecution('Debug', 'Location Internal Id',locationIntId);
									nlapiLogExecution('Debug', 'Sa-Location Cube', RemCube);
									return LocArry;
								}
							}*/
							//case# 20148733 ends
						}
					}
				}
				else {
					nlapiLogExecution('Debug','Remaining usage before fetching zone location groups ',context.getRemainingUsage());
					nlapiLogExecution('Debug', 'Fetching Location Group from Zone', zoneid);

					if (zoneid != null && zoneid != ""){

						var RemCube = 0;
						var LocRemCube = 0;

						if(zonesearchresults!=null && zonesearchresults!='' && zonesearchresults.length>0){

							for (var b1 = 0; b1 < zonesearchresults.length; b1++) {

								if(zoneid==zonesearchresults[b1].getValue('custrecordcustrecord_putzoneid'))
								{
									var vzonelocgroupno=zonesearchresults[b1].getValue('custrecord_locgroup_no');
									//nlapiLogExecution('Debug', 'Fetched Location Group Id from Putaway Zone', vzonelocgroupno);

									if (MergeFlag == 'T') {
										nlapiLogExecution('Debug', 'Inside Merge ');

										if (invtlocResults != null && invtlocResults != '') {
											nlapiLogExecution('Debug', 'LocationResults', invtlocResults.length );
											var columnsinvt2 = new Array();
											columnsinvt2[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
											columnsinvt2[0].setSort();
											columnsinvt2[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
											columnsinvt2[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
											columnsinvt2[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
											columnsinvt2[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
											columnsinvt2[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
											columnsinvt2[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
											columnsinvt2[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

											var filtersinvt2 = new Array();

											if(poLocn!=null && poLocn!='')
												filtersinvt2.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [poLocn]));
											//filtersinvt.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', vlocgrouparr));
											filtersinvt2.push(new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', vlocgrouparr));


											if(ItemCube == null || ItemCube == '' || isNaN(ItemCube))
												ItemCube = 0;

											filtersinvt2.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));

											filtersinvt2.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
											filtersinvt2.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc',  'is','F'));
											//case# 20149729 starts (below condition for Inventory move process)
											if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
												filtersinvt2.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', vItemLocId));
											if(Item!=null && Item!="" && Item !='null' && Mixsku!='T')
												filtersinvt2.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', Item));
											if(vzonelocgroupno!=null && vzonelocgroupno!="" && vzonelocgroupno !='null')
												filtersinvt2.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', vzonelocgroupno));
											//case# 20149729 ends
											 
											var invtlocResults2 = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt2, columnsinvt2);
											if(invtlocResults2 != null && invtlocResults2 != '')
												{
											for (var j1 = 0; j1 < invtlocResults2.length; j1++) {

												var invtlocgroupid = invtlocResults2[j1].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												var invtitem = invtlocResults2[j1].getValue('custrecord_ebiz_inv_sku');

												var str = 'invtlocgroupid. = ' + invtlocgroupid + '<br>';				
												str = str + 'vzonelocgroupno. = ' + vzonelocgroupno + '<br>';
												str = str + 'Mixsku. = ' + Mixsku + '<br>';
												str = str + 'invtitem. = ' + invtitem + '<br>';		
												str = str + 'Item. = ' + Item + '<br>';	

												nlapiLogExecution('DEBUG', 'Mix Attributes', str);

												if(invtlocgroupid == vzonelocgroupno)
												{
													if(Mixsku=='T' || (Mixsku!='T' && invtitem==Item) )
													{
														//nlapiLogExecution('Debug', 'invtlocResults[j].getId()', invtlocResults[j1].getId());
														Location = invtlocResults2[j1].getText('custrecord_ebiz_inv_binloc');
														var locationInternalId = invtlocResults2[j1].getValue('custrecord_ebiz_inv_binloc');

														LocRemCube = invtlocResults2[j1].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
														OBLocGroup = invtlocResults2[j1].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
														PickSeqNo = invtlocResults2[j1].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
														IBLocGroup = invtlocResults2[j1].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
														PutSeqNo = invtlocResults2[j1].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

														var str = 'Location. = ' + Location + '<br>';				
														str = str + 'locationInternalId. = ' + locationInternalId + '<br>';
														str = str + 'LocRemCube. = ' + LocRemCube + '<br>';
														str = str + 'ItemCube. = ' + ItemCube + '<br>';	
														str = str + 'DimentionCheck. = ' + DimentionCheck + '<br>';	

														nlapiLogExecution('DEBUG', 'Location Attributes', str);

														if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

															RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

															location_found = true;

															LocArry[0] = Location;
															LocArry[1] = RemCube;
															LocArry[2] = locationInternalId;
															LocArry[3] = OBLocGroup;
															LocArry[4] = PickSeqNo;
															LocArry[5] = IBLocGroup;
															LocArry[6] = PutSeqNo;
															LocArry[7] = LocRemCube;
															LocArry[8] = zoneid;
															LocArry[9] = PutMethodName;
															LocArry[10] = PutRuleId;

															nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
															return LocArry;
														}
													}
												}
											}
												}
										}
										if (location_found == false) {

											try{
												nlapiLogExecution('Debug', 'Inside  consider open taks while merging');
												nlapiLogExecution('Debug','Remaining usage before considering open taks while merging',context.getRemainingUsage());
												nlapiLogExecution('Debug', 'vzonelocgroupno',vzonelocgroupno);

												if (openputwResults != null && openputwResults != '') {
													nlapiLogExecution('Debug', 'openputwResults length', openputwResults.length );
													for (var j2 = 0; j2 < openputwResults.length; j2++) {

														var tasklocgroupid = openputwResults[j2].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
														var taskitem = openputwResults[j2].getValue('custrecord_sku');

														var str = 'tasklocgroupid. = ' + tasklocgroupid + '<br>';				
														str = str + 'vzonelocgroupno. = ' + vzonelocgroupno + '<br>';
														str = str + 'Mixsku. = ' + Mixsku + '<br>';
														str = str + 'taskitem. = ' + taskitem + '<br>';		
														str = str + 'Item. = ' + Item + '<br>';	

														nlapiLogExecution('DEBUG', 'Mix Attributes', str);

														if(tasklocgroupid == vzonelocgroupno)
														{
															if(Mixsku=='T' || (Mixsku!='T' && taskitem==Item) )
															{
																Location = openputwResults[j2].getText('custrecord_actbeginloc');
																var locationInternalId = openputwResults[j2].getValue('custrecord_actbeginloc');
																LocRemCube = openputwResults[j2].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
																OBLocGroup = openputwResults[j2].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
																PickSeqNo = openputwResults[j2].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
																IBLocGroup = openputwResults[j2].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
																PutSeqNo = openputwResults[j2].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

																var str = 'Location. = ' + Location + '<br>';				
																str = str + 'locationInternalId. = ' + locationInternalId + '<br>';
																str = str + 'LocRemCube. = ' + LocRemCube + '<br>';
																str = str + 'ItemCube. = ' + ItemCube + '<br>';	
																str = str + 'DimentionCheck. = ' + DimentionCheck + '<br>';	

																nlapiLogExecution('DEBUG', 'Location Attributes', str);

																if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

																	RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

																	location_found = true;

																	LocArry[0] = Location;
																	LocArry[1] = RemCube;
																	LocArry[2] = locationInternalId;
																	LocArry[3] = OBLocGroup;
																	LocArry[4] = PickSeqNo;
																	LocArry[5] = IBLocGroup;
																	LocArry[6] = PutSeqNo;
																	LocArry[7] = LocRemCube;
																	LocArry[8] = zoneid;
																	LocArry[9] = PutMethodName;
																	LocArry[10] = PutRuleId;

																	nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
																	return LocArry;
																}
															}
														}
													}										
												}
											}
											catch(exps){
												nlapiLogExecution('Debug', 'included exps in opentasks', exps);
											}
										}
									}

									//if (location_found == false) {
									if (EmptyBinLoc  == 'T' && location_found == false) {

										//code modified by suman on 27/01/12
										//Instead of searching each and individual binloc in inventory and open task record it seems to have a goverence issue.
										//So I bought all binloc from open task and inventory records depending upon "locgr"Id and placing it in an array so that on comparing this array with the binlocation 
										//will decides weather it is empty or not.

										nlapiLogExecution('Debug', 'Location Not found for Merge Location for Location Group', vzonelocgroupno);
										//var locResults = GetLocation(vzonelocgroupno,ItemCube);

										var locResults = GetLocationNew(alllocResults,vzonelocgroupno,ItemCube);

										if (locResults != null && locResults != '') {

											var TotalListOfBinLoc=new Array();
											for ( var x = 0; x < locResults.length; x++) 
											{
												TotalListOfBinLoc[x]=locResults[x].getId();

											}
											var emptyloccheck =	binLocationChecknew(TotalListOfBinLoc,poLocn);
											nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

											nlapiLogExecution('Debug', 'locResults.length', locResults.length);
											for (var j3 = 0; j3 < locResults.length; j3++) {
												var LocFlag='N';
												var emptyBinLocationId = locResults[j3].getId();
												nlapiLogExecution('Debug', 'emptyBinLocationId', emptyBinLocationId);
//												var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//												nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

												for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
												{
//													nlapiLogExecution('Debug', 'emptyloccheck[0][InvBinLocCount]', emptyloccheck[0][InvBinLocCount]);

													if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
													{
														nlapiLogExecution('Debug', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
														LocFlag='Y';
														break;
													}
												}
												for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
												{
													if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
													{
														nlapiLogExecution('Debug', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
														LocFlag='Y';
														break;
													}
												}
//												end of code changed 27/01/12 
												nlapiLogExecution('Debug', 'LocFlag', LocFlag);
												nlapiLogExecution('Debug', 'Main Loop ', s);
												nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
												if(LocFlag == 'N'){
													//Changes done by Sarita (index i is changed to j).
													LocRemCube = locResults[j3].getValue('custrecord_remainingcube');
													OBLocGroup = locResults[j3].getValue('custrecord_outboundlocgroupid');
													PickSeqNo = locResults[j3].getValue('custrecord_startingpickseqno');
													IBLocGroup = locResults[j3].getValue('custrecord_inboundlocgroupid');
													PutSeqNo = locResults[j3].getValue('custrecord_startingputseqno');

													nlapiLogExecution('Debug', 'LocRemCube', LocRemCube);
													nlapiLogExecution('Debug', 'ItemCube', ItemCube);

													if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {
														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
														location_found = true;
														Location = locResults[j3].getValue('name');
														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locResults[j3].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;

														nlapiLogExecution('Debug', 'Location', Location);
														nlapiLogExecution('Debug', 'Location Cube', RemCube);
														return LocArry;
														break;
													}
												}
											}
										}
									}
								}
							}
						}
					}//Zone Id
				}//location group else
			}//Manual Location is not false
		}//vputrules loop
		nlapiLogExecution('Debug', 'Main Loop ', s);
		nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
	}// Putrules not null
	else
	{
		nlapiLogExecution('Debug', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	return LocArry;
}// end


//To Validate and return Item internal id and also returns sku alias type or not
function validateSKUId(itemNo,location,company)
{
	nlapiLogExecution('Debug', 'Into validateSKU',itemNo);

	var actItem=new Array();


	var sitemno = "";
	var context = nlapiGetContext();
	var userAccountId = context.getCompany();
	if(userAccountId=='1285441')
	{
		sitemno = checkserialritem(itemNo);
	}
	if(sitemno != "")
	{
		itemNo = sitemno;
	}

	var invitemfilters = new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal',null, 'is',itemNo));
	// Changed On 30/4/12 by Suman
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	var invLocCol = new Array();
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	invLocCol[0] = new nlobjSearchColumn('externalid');
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	invLocCol[1] = new nlobjSearchColumn('itemid');
	actItem[0]='F';
	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);
	if(invitemRes!=null && invitemRes!='')
	{

		//actItem=invitemRes[0].getValue('externalid');

		actItem[1]=invitemRes[0].getId();

	}
	else
	{

		nlapiLogExecution('Debug', 'inSide UPCCODE',itemNo);

		var invLocfilters = new Array();
		invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));
		invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);

		if(invtRes!=null && invtRes!='')
		{

			//actItem=invtRes[0].getValue('externalid');
			actItem[1]=invtRes[0].getId();
			actItem[2]=invtRes[0].getValue('itemid');
		}
		else
		{

			nlapiLogExecution('Debug', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			if(itemNo!=null && itemNo!='')
				skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemNo));
			if(location!=null && location!='')
				skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_location', null, 'is', ["@NONE@",location]));

			//if(company != null && company != "")
			//	skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_company', null, 'is', company));

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
			{

				actItem[0]='T';
				actItem[1]=skuAliasResults[0].getValue('custrecord_ebiz_item');
				actItem[2]=skuAliasResults[0].getText('custrecord_ebiz_item'); // Case# 20149268
				/*var Itype = nlapiLookupField('item', actItem, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, actItem);			
				actItem=	ItemRec.getFieldValue('itemid');*/
			}

		}			
	} 
	return actItem;
}

function getLPlist(lptype,whloc)
{
	var searchresults = new Array();
	var filtersSO = new Array();

	if(lptype!=null && lptype!='')
		filtersSO.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'anyof', ['@NONE@',lptype]));
	if(whloc!=null && whloc!='')
		filtersSO.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', ['@NONE@',whloc]));

	filtersSO.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'noneof', [28]));
	filtersSO.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsSO = new Array();
	colsSO[0] = new nlobjSearchColumn('name');
	colsSO[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	colsSO[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_masterlp');
	colsSO[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_site');
	colsSO[4] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_company');
	colsSO[5] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lptype');
	colsSO[6] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	colsSO[7] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	colsSO[8] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
	colsSO[9] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag');
	colsSO[10] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
	colsSO[11] = new nlobjSearchColumn('custrecord_ebiz_cart_closeflag');
	colsSO[12] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_item');
	colsSO[13] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_pkgcount');
	colsSO[14] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_pkgno');
	colsSO[15] = new nlobjSearchColumn('internalid').setSort(true);

	searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersSO, colsSO);
	return searchresults;

}

/**
 * 
 * @param trantype //1 is for Inbound,2 for outbound
 * @param msg //exception msg
 * @param Ord //Order#
 * @param exceptionname// like TransferOrder,PurchaseOrder,SalesOrder
 * @param functionality//Situaltion of exception like 'Creating Itemfulfillment' or 'updating Itemfulfillment
 * @param alertType//1 is for Exception and 2 for report
 * @param reference2//extra parameters
 * @param reference3
 * @param reference4
 * @param reference5
 */
function errSendMailByProcess(vprocesstrantype,msg,OrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5)
{
	try
	{
		var Ord;
		nlapiLogExecution('Debug','Sending mail','MailSending');
		nlapiLogExecution('Debug','vtrantype',vprocesstrantype);
		nlapiLogExecution('Debug','msg',msg);
		nlapiLogExecution('Debug','OrdNo',OrdNo);
		nlapiLogExecution('Debug','reference2',reference2);

		var userId;
		userId = nlapiGetUser();
		/*var transaction = nlapiLoadRecord('Employee', userId);
	var variable=transaction.getFieldValue('email');
	var username=variable.split('@')[0];*/

		var subject="";

		//nlapiLogExecution('Debug','subject',subject);
		var context = nlapiGetContext();

		var userAccountId = context.getCompany();
		var strContent ="";
		var strSubject="";

		/*var trantype = nlapiLookupField('transaction', OrdNo, 'recordType');
		searchresults = nlapiLoadRecord(trantype, OrdNo); //1020
		var Ord = searchresults.getFieldValue('tranid');*/

		//var Ord= nlapiLoadRecord(trantype, OrdNo);
		var Ord=OrdNo;
		strSubject=functionality + ' failed for ' + exceptionname + ' ' + Ord;
		if(reference2 != null && reference2 != '')
			strSubject=strSubject + ' Container LP#: ' + reference2;
		nlapiLogExecution('Debug','subject',strSubject);
		strContent +="AccountId :"+userAccountId;
		strContent +="<br/>";
		strContent +=strSubject;
		strContent +="<br/>";
		strContent +=msg;
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_email_alert_type', null, 'anyof', alertType));
		filters.push(new nlobjSearchFilter('custrecord_email_trantype', null, 'anyof', vprocesstrantype));
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_user_email');
		columns[1] = new nlobjSearchColumn('custrecord_email_option');
		var searchresults = nlapiSearchRecord('customrecord_email_config', null, filters, columns);		 
		var email= "";
		var emailbcc="";
		var emailcc="";
		var emailappend="";
		var emailbccappend= new Array();
		var emailccappend=new Array();
		var count=0;
		var bcccount=0;
		if(searchresults != null && searchresults != '')
		{	
			for(var i=0;i<searchresults.length;i++)
			{
				var emailtext=searchresults[i].getText('custrecord_email_option');
				nlapiLogExecution('Debug','emailtext',emailtext);
				if(emailtext=="BCC")
				{
					emailbccappend[bcccount]=searchresults[i].getValue('custrecord_user_email');
					bcccount++;		 
				}
				else if(emailtext=="CC")
				{
					emailccappend[count]=searchresults[i].getValue('custrecord_user_email');
					count++;		 
				}
				else
				{
					email =searchresults[i].getValue('custrecord_user_email');
					emailappend +=email+";";
				}
			} 
			emailappend +=email+"satish.nimmakayala@ebizscm.com;";
			var substirngemail= emailappend.substring(0, emailappend.length-1);
			if(userId != null && userId != '' && userId > 0)
				nlapiSendEmail(userId,substirngemail,strSubject,strContent,emailccappend,emailbccappend,null,null);
		}
		var Exceptionrecord= nlapiCreateRecord('customrecord_exception_log'); 
		Exceptionrecord.setFieldValue('name', exceptionname);
		Exceptionrecord.setFieldValue('custrecord_exceptionlog_trantype',vprocesstrantype);
		Exceptionrecord.setFieldValue('custrecord_exceptionlog_functionality',functionality);  
		Exceptionrecord.setFieldValue('custrecord_exceptionlog_exception',strSubject);     
		Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference1',Ord);  
		Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference2',reference2);   
		Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference3',reference3); 
		Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference4',reference4); 
		Exceptionrecord.setFieldValue('custrecord_exceptionlog_reference5',reference5); 
		if(userId != null && userId != '' && userId > 0)
			Exceptionrecord.setFieldValue('custrecord_exceptionlog_user',userId); 
		var tranid = nlapiSubmitRecord(Exceptionrecord,true);	
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in errSendMailByProcess',exp);
	}
}
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}


function CreateShippingManifestRecordforShipLP(vebizOrdNo,vContLpNo,vCarrierType,lineno,weight,packagecount,totalpackagecount,
		vShippingRule,vshiplp) {
	try {

		nlapiLogExecution('Debug', 'Into CreateShippingManifestRecordforShipLP','');		

		var str = 'Order #. = ' + vebizOrdNo + '<br>';
		str = str + 'Container LP #. = ' + vContLpNo + '<br>';	
		str = str + 'Carrier Type. = ' + vCarrierType + '<br>';
		str = str + 'Line No. = ' + lineno + '<br>';
		str = str + 'Weight. = ' + weight + '<br>';
		str = str + 'Shipping Rule. = ' + vShippingRule + '<br>';
		str = str + 'Ship Lp. = ' + vshiplp + '<br>';

		nlapiLogExecution('Debug', 'Function Parameters', str);

		if (vebizOrdNo != null && vebizOrdNo != "") 
		{
			if(IsContLpExist(vshiplp)!='T')
			{
				var freightterms ="";
				var otherrefnum="";

				var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
				nlapiLogExecution('Debug', 'trantype', trantype);
				var  searchresults;
				if(trantype=="salesorder")
				{
					searchresults =SalesOrderList(vebizOrdNo);
				}

				else if(trantype=="transferorder")
				{
					nlapiLogExecution('Debug', 'SalesOrderList','');

					var filters = new Array();
					filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
					filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('tranid');
					columns[1] = new nlobjSearchColumn('shipcarrier');
					columns[2] = new nlobjSearchColumn('shipaddress1');
					columns[3] = new nlobjSearchColumn('shipaddress2');
					columns[4] = new nlobjSearchColumn('shipcity');
					columns[5] = new nlobjSearchColumn('shipstate');
					columns[6] = new nlobjSearchColumn('shipcountry');
					columns[7] = new nlobjSearchColumn('shipzip');
					columns[8] = new nlobjSearchColumn('shipmethod');
					columns[9] = new nlobjSearchColumn('shipaddressee');
					columns[10] = new nlobjSearchColumn('custbody_salesorder_carrier');
					columns[11] = new nlobjSearchColumn('transferlocation');
					columns[12] = new nlobjSearchColumn('entity');

					searchresults = nlapiSearchRecord('transferorder', null, filters, columns);
					nlapiLogExecution('Debug', 'transferorder',searchresults.length);
				}
				nlapiLogExecution('Debug', 'SalesOrderList',searchresults);
				//Get the records in customrecord_ebiznet_trn_opentask
				var opentaskordersearchresult=getOpenTaskDetails(vebizOrdNo,vContLpNo);
				nlapiLogExecution('Debug', 'getOpenTaskDetails',opentaskordersearchresult);

				//Code added by Sravan to getch customer default shipping address

				var entity=searchresults[0].getValue('entity');
				nlapiLogExecution('Debug', 'getOpenTaskDetails entity111',entity);

				var entityrecord ;
				if(entity != "" && entity != null)
				{
					var fields = ['entityid'];
					var columns = nlapiLookupField('customer',entity,fields);
					var customerid = columns.entityid;

					try
					{
						entityrecord = nlapiLoadRecord('customer', entity);
					}
					catch(exp)
					{
						nlapiLogExecution('Debug', 'Exception in Loading Customer',exp);
						entityrecord='';
					}
				}

				var ShipManifest = nlapiCreateRecord('customrecord_ship_manifest');
				ShipManifest.setFieldValue('custrecord_ship_orderno',searchresults[0].getValue('tranid'));
				ShipManifest.setFieldValue('custrecord_ship_carrier',searchresults[0].getValue('custbody_salesorder_carrier'));
				ShipManifest.setFieldValue('custrecord_ship_city',   searchresults[0].getValue('shipcity'));
				ShipManifest.setFieldValue('custrecord_ship_state',	 searchresults[0].getValue('shipstate'));
				ShipManifest.setFieldValue('custrecord_ship_custid',customerid);
				ShipManifest.setFieldValue('custrecord_ship_country',searchresults[0].getValue('shipcountry'));
				ShipManifest.setFieldValue('custrecord_ship_addr1',searchresults[0].getValue('shipaddress1'));

				//sales order specific code 
				if(trantype=="salesorder")
				{
					var contactName=searchresults[0].getValue('shipattention');
					var entity=searchresults[0].getText('entity');
					if(contactName!=null && contactName!='')
						contactName=contactName.replace(","," ");

					if(entity!=null && entity!='')
						entity=entity.replace(","," ");

					ShipManifest.setFieldValue('custrecord_ship_contactname',contactName);					
					ShipManifest.setFieldValue('custrecord_ship_ordertype',searchresults[0].getText('custbody_nswmssoordertype'));
					freightterms=searchresults[0].getText('custbody_nswmsfreightterms');
					otherrefnum=searchresults[0].getValue('otherrefnum');			 
					ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);
					ShipManifest.setFieldValue('custrecord_ship_phone',searchresults[0].getValue('custbody_customer_phone'));
					var saturdaydelivery= searchresults[0].getValue('custbody_nswmssosaturdaydelivery');
					ShipManifest.setFieldValue('custrecord_ship_satflag',saturdaydelivery);
					var cashondelivery= searchresults[0].getValue('custbody_nswmscodflag');
					ShipManifest.setFieldValue('custrecord_ship_codflag',cashondelivery);
					ShipManifest.setFieldValue('custrecord_ship_email',searchresults[0].getValue('email'));

					var rec= nlapiLoadRecord('salesorder', vebizOrdNo);
					var zipvalue=rec.getFieldValue('shipzip');
					var servicelevelvalue=rec.getFieldText('shipmethod');
					var consignee=rec.getFieldValue('shipaddressee');
					var signaturerequired=rec.getFieldValue('custbody_nswmssignaturerequired');
					ShipManifest.setFieldValue('custrecord_ship_signature_req',signaturerequired);
					var shipcomplete=rec.getFieldValue('shipcomplete');
					var termscondition=rec.getFieldText('terms');

					var shipmethodLineLevel = '';
					if(lineno!=null && lineno!='')
					{
						shipmethodLineLevel = rec.getLineItemText('item','shipmethod',lineno);
					}

					nlapiLogExecution('Debug', 'shipmethodLineLevel',shipmethodLineLevel);
					nlapiLogExecution('Debug', 'signaturerequired',shipcomplete);
					var shiptotal="0.00";
					if((shipcomplete=="T")&&(termscondition=="C.O.D."))
					{
						ShipManifest.setFieldValue('custrecord_ship_codflag','T');
						shiptotal=rec.getFieldValue('subtotal');
						ShipManifest.setFieldValue('custrecord_ship_codamount',shiptotal);
					}
					else
					{
						ShipManifest.setFieldValue('custrecord_ship_codflag','F');
						ShipManifest.setFieldValue('custrecord_ship_codamount',shiptotal);
					}
					nlapiLogExecution('Debug', 'signaturerequired',signaturerequired);
					nlapiLogExecution('Debug', 'zipvalue=',zipvalue);
					nlapiLogExecution('Debug', 'servicelevelvalue=',servicelevelvalue);
					nlapiLogExecution('Debug', 'Consignee=',consignee);

					if(consignee!="" || consignee!=null)
						ShipManifest.setFieldValue('custrecord_ship_consignee',consignee);
					else
						ShipManifest.setFieldValue('custrecord_ship_consignee',entity);

					ShipManifest.setFieldValue('custrecord_ship_servicelevel',servicelevelvalue);	

					if(servicelevelvalue==null || servicelevelvalue=='')
					{					
						ShipManifest.setFieldValue('custrecord_ship_servicelevel',shipmethodLineLevel);	

					}

					ShipManifest.setFieldValue('custrecord_ship_zip',zipvalue);
					var companyname= searchresults[0].getText('custbody_nswms_company');
					ShipManifest.setFieldValue('custrecord_ship_company',companyname);
					ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
					ShipManifest.setFieldValue('custrecord_ship_actwght',weight);
				}

				var address1=searchresults[0].getValue('shipaddress1');
				var address2=searchresults[0].getValue('shipaddress2');
				var zip=searchresults[0].getValue('shipzip');
				var servicelevel=searchresults[0].getText('shipmethod');

				nlapiLogExecution('Debug', 'Zip',zip);
				nlapiLogExecution('Debug', 'Service Level',servicelevel);
				nlapiLogExecution('Debug', 'Zip',zip);
				nlapiLogExecution('Debug', 'City',searchresults[0].getValue('shipcity'));
				nlapiLogExecution('Debug', 'State',searchresults[0].getValue('shipstate'));
				nlapiLogExecution('Debug', 'Zip=',searchresults[0].getValue('shipzip'));
				nlapiLogExecution('Debug', 'Service Level Value',searchresults[0].getText('shipmethod'));

				if(address1!=null && address1!='')
					address1=address1.replace(","," ");

				if(address2!=null && address2!='')
					address2=address2.replace(","," ");

				ShipManifest.setFieldValue('custrecord_ship_order',vebizOrdNo);
				ShipManifest.setFieldValue('custrecord_ship_custom5',"S");	
				ShipManifest.setFieldValue('custrecord_ship_void',"N");
				ShipManifest.setFieldValue('custrecord_ship_contactname',contactName);					
				ShipManifest.setFieldValue('custrecord_ship_ordertype',searchresults[0].getText('custbody_nswmssoordertype'));
				freightterms=searchresults[0].getText('custbody_nswmsfreightterms');
				otherrefnum=searchresults[0].getValue('otherrefnum');			 
				ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);
				ShipManifest.setFieldValue('custrecord_ship_addr1',address1);
				ShipManifest.setFieldValue('custrecord_ship_addr2',address2);

				if(lineno!=null && lineno!='')
				{
					if (entityrecord != null && entityrecord != '')
					{
						var custaddr1 = entityrecord.getFieldValue('shipaddr1');
						var custaddr2 = entityrecord.getFieldValue('shipaddr2');
						var custaddresee = entityrecord.getFieldValue('shipaddressee');
						var custcity = entityrecord.getFieldValue('shipcity');
						var custstate = entityrecord.getFieldValue('shipstate');
						var custzip = entityrecord.getFieldValue('shipzip');	
						ShipManifest.setFieldValue('custrecord_ship_city',  custcity);
						ShipManifest.setFieldValue('custrecord_ship_state',	custstate);
						ShipManifest.setFieldValue('custrecord_ship_addr1',	custaddr1);
						ShipManifest.setFieldValue('custrecord_ship_addr2',	custaddr2);
						ShipManifest.setFieldValue('custrecord_ship_zip',	custzip);
					}
				}

				var servicelevelList=GetSerViceLevel(vCarrierType);
				if((servicelevelList!=null)&&(servicelevelList !='')&&(servicelevelList.length>0))
				{
					vserlevel=servicelevelList[0].getValue('custrecord_carrier_service_level'); 
					nlapiLogExecution('Debug', 'vserlevel', vserlevel);
					ShipManifest.setFieldValue('custrecord_ship_servicelevel',vserlevel);
				}

				var servicelevellbyshipmethod;
				var ServiceLevelId=searchresults[0].getValue('shipmethod');
				if(ServiceLevelId!=null || ServiceLevelId!='')
				{

					servicelevellbyshipmethod=GetSerViceLevelByShipmethod(ServiceLevelId);
				}

				if((servicelevellbyshipmethod!=null)&&(servicelevellbyshipmethod !='')&&(servicelevellbyshipmethod.length>0))
				{
					nlapiLogExecution('Debug', 'servicelevellbyshipmethod', 'servicelevellbyshipmethod');

					var shipserviceLevel=servicelevellbyshipmethod[0].getValue('custrecord_carrier_service_level'); 
					var wmscarriertype=servicelevellbyshipmethod[0].getValue('custrecord_carrier_id'); 
					nlapiLogExecution('Debug', 'shipmethodshipserviceLevel',shipserviceLevel);
					nlapiLogExecution('Debug', 'wmscarriertype',wmscarriertype);
					ShipManifest.setFieldValue('custrecord_ship_servicelevel',shipserviceLevel);
					ShipManifest.setFieldValue('custrecord_ship_carrier',wmscarriertype);

				}

				if(trantype=="transferorder")
				{
					nlapiLogExecution('Debug', 'tolocationtstt', trantype);
					var tolocation = searchresults[0].getValue('transferlocation');
					nlapiLogExecution('Debug', 'tolocation', tolocation);

					var record = nlapiLoadRecord('location', tolocation);
					var shipfromaddress1=record.getFieldValue('addr1');
					var shipfromaddress2=record.getFieldValue('addr2');
					var shipfromcity=record.getFieldValue('city');
					var shipfromstate=record.getFieldValue('state');
					var shipfromzipcode =record.getFieldValue('zip');
					var shipfromcompanyname=record.getFieldValue('addressee');
					var shipfromphone=record.getFieldValue('addrphone');
					var shipfromcountry =record.getFieldValue('country');
					var prefixshipmethod=record.getFieldValue('custrecord_preferred_ship_method');
					ShipManifest.setFieldValue('custrecord_ship_carrier',prefixshipmethod);
					ShipManifest.setFieldValue('custrecord_ship_city',shipfromcity);
					ShipManifest.setFieldValue('custrecord_ship_state',shipfromstate);
					ShipManifest.setFieldValue('custrecord_ship_country',shipfromcountry);
					ShipManifest.setFieldValue('custrecord_ship_addr1',shipfromaddress1);
					ShipManifest.setFieldValue('custrecord_ship_zip',shipfromzipcode);
					ShipManifest.setFieldValue('custrecord_ship_addr2',shipfromaddress2);
					ShipManifest.setFieldValue('custrecord_ship_phone',shipfromphone);
					ShipManifest.setFieldValue('custrecord_ship_custid',shipfromcompanyname);
					ShipManifest.setFieldValue('custrecord_ship_contactname',shipfromcompanyname);

				}


				if (opentaskordersearchresult != null && opentaskordersearchresult != "")
				{
					nlapiLogExecution('Debug', 'inside opentask search results', opentaskordersearchresult);

					var oldcontainer="";
					for (l = 0; l < opentaskordersearchresult.length; l++) 
					{ 
						nlapiLogExecution('Debug', 'inside opentask', containerid);

						var custlenght="";	
						var custheight="";
						var custwidht="";
						var sku="";
						var ebizskuno="";
						var uomlevel="";
						var shiplpno="";						

						var containerlpno = opentaskordersearchresult[l].getValue('custrecord_container_lp_no');
						var shiplpno=opentaskordersearchresult[l].getValue('custrecord_ship_lp_no');
						sku = opentaskordersearchresult[l].getText('custrecord_sku');
						ebizskuno = opentaskordersearchresult[l].getValue('custrecord_sku');
						uomlevel = opentaskordersearchresult[l].getValue('custrecord_uom_level');					
						var name= opentaskordersearchresult[l].getValue('name');	
						var vwaveno = opentaskordersearchresult[l].getValue('custrecord_ebiz_wave_no');
						ShipManifest.setFieldValue('custrecord_ship_waveno',vwaveno);

						if(oldcontainer!=containerlpno){
							ShipManifest.setFieldValue('custrecord_ship_ref3',name);
							if(vShippingRule=='BuildShip')
							{
								ShipManifest.setFieldValue('custrecord_ship_contlp',shiplpno);
								ShipManifest.setFieldValue('custrecord_ship_ref5',shiplpno);
							}
							else
							{
								ShipManifest.setFieldValue('custrecord_ship_contlp',containerlpno);
								ShipManifest.setFieldValue('custrecord_ship_ref5',containerlpno);
							}

							var containerid= opentaskordersearchresult[l].getValue('custrecord_container');
							var containername= opentaskordersearchresult[l].getText('custrecord_container');
							nlapiLogExecution('Debug', 'container id', containerid);
							nlapiLogExecution('Debug', 'container name', containername);

							if(containerid!="")
							{
								if(containername!="SHIPASIS")
								{
									//Lenght, Height,Width fields  in customrecord_ebiznet_container
									var containersearchresults = getContainerDims(containerid);
									if(containersearchresults != null)
									{
										custlenght=containersearchresults [0].getValue('custrecord_length');
										custwidht=containersearchresults [0].getValue('custrecord_widthcontainer');
										custheight=containersearchresults [0].getValue('custrecord_heightcontainer');								
										ShipManifest.setFieldValue('custrecord_ship_length',custlenght);	
										ShipManifest.setFieldValue('custrecord_ship_width',custwidht);
										ShipManifest.setFieldValue('custrecord_ship_height',custheight);
										ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
										ShipManifest.setFieldValue('custrecord_ship_pkgtype',containername);
									} 
								}
								else
								{
									var containersearchresults = getSKUDims(ebizskuno,uomlevel);
									if(containersearchresults != null)
									{
										custlenght=containersearchresults [0].getValue('custrecord_ebizlength');
										custwidht=containersearchresults [0].getValue('custrecord_ebizwidth');
										custheight=containersearchresults [0].getValue('custrecord_ebizheight');								
										ShipManifest.setFieldValue('custrecord_ship_length',custlenght);	
										ShipManifest.setFieldValue('custrecord_ship_width',custwidht);
										ShipManifest.setFieldValue('custrecord_ship_height',custheight);
										ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
										ShipManifest.setFieldValue('custrecord_ship_pkgtype',containername);
										ShipManifest.setFieldValue('custrecord_ship_ref1',sku);
									} 
								}
							}	

							if (containerlpno != null && containerlpno != "") {
								var PackageWeight = getTotalWeight(containerlpno);
								if(PackageWeight == null || PackageWeight == '' || parseFloat(PackageWeight) == 0)
									PackageWeight='0.0001';
								ShipManifest.setFieldValue('custrecord_ship_pkgwght',PackageWeight);

								var lpmastersearchrecord =GetTotalPackageValues(containerlpno);
								var pkgno;
								var pkgcount;

								if(lpmastersearchrecord != null && lpmastersearchrecord != '')
								{	
									pkgno=lpmastersearchrecord[0].getValue('custrecord_ebiz_lpmaster_pkgno');
									pkgcount=lpmastersearchrecord[0].getValue('custrecord_ebiz_lpmaster_pkgcount');
								}
								nlapiLogExecution('Debug', 'lpmasterpkgno', pkgno);
								nlapiLogExecution('Debug', 'lpmasterpkgcount',pkgcount);

								if(packagecount!=null && packagecount!='' )
									ShipManifest.setFieldValue('custrecord_ship_pkgno',parseInt(packagecount.toString()));
								if(totalpackagecount!=null && totalpackagecount!='' )
									ShipManifest.setFieldValue('custrecord_ship_pkgcount',parseInt(totalpackagecount.toString()));

							}

							oldcontainer = containerlpno;

							ShipManifest.setFieldValue('custrecord_ship_pkgtype','SHIP');

							//nlapiSubmitRecord(ShipManifest, false, true);	
							nlapiLogExecution('Debug', 'unexpected error', 'I am success1');

						}					
					}
				}
				else
				{
					nlapiLogExecution('Debug', 'unexpected error', 'I am success2');
				}
				var shipmanifestid = nlapiSubmitRecord(ShipManifest, false, true);

				nlapiLogExecution('Debug', 'shipmanifestid', shipmanifestid);

				if(shipmanifestid!=null && shipmanifestid!='')
					updateMasterCarton(vContLpNo,vshiplp);

				try
				{
					nlapiLogExecution('Debug', 'CommodityInternationalShipment', 'CommodityInternationalShipment');
					CommodityInternationalShipment(vebizOrdNo,vContLpNo,trantype);
				}
				catch(exp)
				{

					nlapiLogExecution('Debug', 'CommodityshipmentExp',exp);
				}
			}
			else
			{
				updateMasterCarton(vContLpNo,vshiplp);
			}
		}	
	}
	catch (e) {

		InsertExceptionLog('General Functions',2, 'Create Shipping Manifest', e, vebizOrdNo, vContLpNo,vCarrierType,'','', nlapiGetUser());
		if (e instanceof nlobjError)
			nlapiLogExecution('Debug', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		//lapiLogExecution('Debug','SYS ERROR','Hd')
		else
			nlapiLogExecution('Debug', 'unexpected error', e.toString());
		nlapiLogExecution('Debug', 'unexpected error', 'I am unsuccess3');
	}

	nlapiLogExecution('Debug', 'Out of CreateShippingManifestRecordforShipLP','');		
}

function GetInboundStageLocation(vSite, vCompany, stgDirection){

	nlapiLogExecution('Debug', 'Into GetInboundStageLocation');

	var str = 'vSite. = ' + vSite + '<br>';
	str = str + 'vCompany. = ' + vCompany + '<br>';	
	str = str + 'stgDirection. = ' + stgDirection + '<br>';

	nlapiLogExecution('Debug', 'GetInboundStageLocation Parameters', str);

	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}


	var columns = new Array();
	var filters = new Array();

	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}

	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < searchresults.length; i++) {

			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			var vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');

			var str = 'Stage Location ID. = ' + vstgLocation + '<br>';
			str = str + 'Stage Location Text. = ' + vstgLocationText + '<br>';	

			nlapiLogExecution('Debug', 'Stage Location Details', str);

			if (vstgLocation != null && vstgLocation != "") {
				nlapiLogExecution('Debug', 'Out of GetInboundStageLocation',vstgLocation);
				return vstgLocation;
			}

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('Debug', 'LocGroup ', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				filtersLocGroup.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('Debug', 'searchresultsloc ', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {

					var vLocid=searchresultsloc[0].getId();
					nlapiLogExecution('Debug', 'Out of GetInboundStageLocation',vLocid);
					return vLocid;

				}				
			}
		}
	}

	nlapiLogExecution('Debug', 'Out of GetInboundStageLocation');

}


function updateMasterCarton(vContLpNo,vShipLpNo)
{
	nlapiLogExecution('Debug', 'Into updateMasterCarton',vContLpNo);	

	try
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ship_contlp',null,'is',vContLpNo));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ship_contlp');
		var manifestList= nlapiSearchRecord('customrecord_ship_manifest',null,filter,columns);
		if(manifestList!=null && manifestList!='' && manifestList.length>0)
		{
			for (l = 0; l < manifestList.length; l++) 
			{ 
				nlapiSubmitField('customrecord_ship_manifest',manifestList[l].getId(),'custrecord_ship_mastercarton',vShipLpNo);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('Debug', 'unexpected error in updateMasterCarton');	
	}
	nlapiLogExecution('Debug', 'Out of updateMasterCarton');	
}


function getSystemRule(RuleValue)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', RuleValue);
		//filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('Debug', 'Exception in GetSystemRules: ', exp);
		return 'Y';
	}	
}

function AutolotNumber(itemId)
{ 
	var vResultArray=new Array();
	var arrayMonth = new Array('A','B','C','D','E','F','G','H','I','J','K','L'); 	 
	nlapiLogExecution('Debug','Item ID = '+ itemId);
	var count = 0;
	var serialNumber=1;
	var now = new Date();
	var vMonth=parseFloat(now.getMonth())+1;
	var vYear=parseFloat(now.getFullYear());
	var Filters = new Array();
	Filters.push(new nlobjSearchFilter('custrecord_ebiz_lotseq_month', null, 'equalto', parseFloat(vMonth)));
	Filters.push(new nlobjSearchFilter('custrecord_ebiz_lotseq_year', null, 'equalto', parseFloat(vYear)));


	var Columns = new Array();
	Columns[0] = new nlobjSearchColumn('custrecord_ebiz_builds_for_month');


	var SearchResults = nlapiSearchRecord('customrecord_ebiz_lotsequence', null, Filters, Columns);
	if(SearchResults != null && SearchResults != '')
	{
		var vId=SearchResults[0].getId();
		serialNumber=SearchResults[0].getValue('custrecord_ebiz_builds_for_month');
	}
	else
		serialNumber=1;
	//var serialNumber = nlapiLookupField('customrecord_celigo_lot_sequence', '1', 'custrecord_num_builds_for_month', null);
	serialNumber = (serialNumber*1) + (count*1);
	serialNumber = serialNumber + '';
	var itemLife=36;
	try{
		itemLife = nlapiLookupField('item', itemId, 'custitem_item_life', null);
	}
	catch(exps){
		nlapiLogExecution('Debug', 'itemLife', exps);
	}
	if(serialNumber.length==1)
		serialNumber = '0'+serialNumber; 
	nlapiLogExecution('Debug','Serial number  = ' + serialNumber); 
	nlapiLogExecution('Debug','life time  = ' + itemLife);
	if(itemLife == null || itemLife == '')
		itemLife=36;
	if((itemLife != null)&&(itemLife !='')){
		var currentDate = new Date(); 
		var currentYear = currentDate.getFullYear().toString().substring(2);
		//var lotNumber    = serialNumber + arrayMonth[(currentDate.getMonth()*1)] + currentYear;
		var lotNumberSequence  =  serialNumber ;
		var lotNumberMonthYear  =  arrayMonth[(currentDate.getMonth()*1)] + currentYear;
		//nlapiLogExecution('Debug','lotNumber = ' + lotNumber);
		nlapiLogExecution('Debug','lotNumberSequence = ' + lotNumberSequence);
		nlapiLogExecution('Debug','lotNumberMonthYear = ' + lotNumberMonthYear);
		//itemReceiptRecord.setLineItemValue('item','serialnumbers',i,lotNumber);

		var d = nlapiAddMonths(currentDate, (itemLife*1));
		var year = d.getFullYear().toString().substring(2);
		nlapiLogExecution('Debug','expiration date = ' + d);
		//itemReceiptRecord.setLineItemValue('item','expirationdate',i,nlapiDateToString(d)); 
		count++;
		vResultArray.push(lotNumberMonthYear);
		vResultArray.push(nlapiDateToString(d));
		vResultArray.push(lotNumberSequence);
		return vResultArray; 
	} 
}

function fnUpdateBuildSequence(Counter)
{
	nlapiLogExecution('Debug','Into fnUpdateBuildSequence');
	if(Counter== null || Counter== '')
		Counter=1;
	var now = new Date();
	var vMonth=parseFloat(now.getMonth())+1;
	var vYear=parseFloat(now.getFullYear());
	nlapiLogExecution('Debug','vMonth',vMonth);
	nlapiLogExecution('Debug','vYear',vYear);
	var Filters = new Array();
	Filters.push(new nlobjSearchFilter('custrecord_ebiz_lotseq_month', null, 'equalto', parseFloat(vMonth)));
	Filters.push(new nlobjSearchFilter('custrecord_ebiz_lotseq_year', null, 'equalto', parseFloat(vYear)));


	var Columns = new Array();
	Columns[0] = new nlobjSearchColumn('custrecord_ebiz_builds_for_month');


	var SearchResults = nlapiSearchRecord('customrecord_ebiz_lotsequence', null, Filters, Columns);
	if(SearchResults != null && SearchResults != '')
	{
		nlapiLogExecution('Debug','SearchResults',SearchResults.length);
		var vId=SearchResults[0].getId();
		var vBuildCount=SearchResults[0].getValue('custrecord_ebiz_builds_for_month');

		var tranrecord = nlapiLoadRecord('customrecord_ebiz_lotsequence', vId);
		tranrecord.setFieldValue('custrecord_ebiz_builds_for_month',parseFloat(vBuildCount)+ parseFloat(Counter));
		var id = nlapiSubmitRecord(tranrecord, true);

	}	
	else
	{
		var orderLineRecord = nlapiCreateRecord('customrecord_ebiz_lotsequence');
		orderLineRecord.setFieldValue('name', vMonth + '-' + vYear);
		orderLineRecord.setFieldValue('custrecord_ebiz_lotseq_month', vMonth);
		orderLineRecord.setFieldValue('custrecord_ebiz_builds_for_month', (1 + parseFloat(Counter)));
		orderLineRecord.setFieldValue('custrecord_ebiz_lotseq_year',vYear);
		var recordId = nlapiSubmitRecord(orderLineRecord);
	}	
	nlapiLogExecution('Debug','Lot sequence updated');

}

/**
 * @param serialItem
 * @param serialnoscanned
 * @returns
 */
function SerialNoIdentification(serialItem,serialnoscanned)
{
	try
	{
		nlapiLogExecution('Debug','Into SerialNoIdentification',serialItem);
		nlapiLogExecution('Debug','serialnoscanned',serialnoscanned);
		var ISParsingReq='F';
		var ParsingFormula=0;
		var ParsingType;
		var OriginalSerialNo;
		var filter=new Array();
		filter.push(new nlobjSearchFilter('internalid',null,'anyof',serialItem));
		filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custitem_ebiz_serialparsing_required');
		column[1]=new nlobjSearchColumn('custitem_ebiz_serialparsing_formula');
		column[2]=new nlobjSearchColumn('custitem_ebiz_trim_serial_to');

		var searchrec=nlapiSearchRecord('item',null,filter,column);

		if(searchrec!=null&&searchrec!="")
		{
			ISParsingReq=searchrec[0].getValue('custitem_ebiz_serialparsing_required');
			if(ISParsingReq=='T')
			{
				ParsingFormula=searchrec[0].getValue('custitem_ebiz_serialparsing_formula');
				ParsingType=searchrec[0].getValue('custitem_ebiz_trim_serial_to');
				if(ParsingType==1)//LEFT Trim
				{
					OriginalSerialNo=serialnoscanned.substring(0,ParsingFormula);
				}
				else if(ParsingType==2)//RIGHT Trim
				{
					OriginalSerialNo=serialnoscanned.substring(serialnoscanned.length-ParsingFormula);
				}
				else if(ParsingType==3)//LEFT(RIGHT) Trim
				{
					var res=ParsingFormula.split('(');
					var leftlen=res[0];					
					var rightlen=res[1].split(')')[0];

					OriginalSerialNo=serialnoscanned.substring(serialnoscanned.length-rightlen);
					OriginalSerialNo=OriginalSerialNo.substring(0,leftlen);
				}
				else if(ParsingType==4)//RIGHT(LEFT) Trim
				{

					var res=ParsingFormula.split('(');
					var rightlen=res[0];					
					var leftlen=res[1].split(')')[0];
					OriginalSerialNo=serialnoscanned.substring(0,leftlen);
					OriginalSerialNo=OriginalSerialNo.substring(OriginalSerialNo.length-rightlen);
				}
			}
			else
				OriginalSerialNo=serialnoscanned;
		}

		nlapiLogExecution('Debug','OriginalSerialNo',OriginalSerialNo);
		return OriginalSerialNo;
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in SerialNoIdentification',exp);
	}
}


function isOrderClosed(vsalesOrderId)
{
	nlapiLogExecution('Debug', 'Into isOrderClosed',vsalesOrderId);	

	var vstatus = '';
	var trantype = nlapiLookupField('transaction', vsalesOrderId, 'recordType');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', vsalesOrderId));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');	
	columns[1] = new nlobjSearchColumn('status');	
	columns[2] = new nlobjSearchColumn('paymenteventresult');
	columns[3] = new nlobjSearchColumn('shipcity');

	var salesOrderDetails = new nlapiSearchRecord(trantype, null, filters, columns);

	nlapiLogExecution('Debug', 'Out of isOrderClosed');

	return salesOrderDetails;
}


function WOInvReversal(woid,CancelAlloc)
{ 
	nlapiLogExecution('Debug', 'vWOId', woid);
	nlapiLogExecution('Debug', 'CancelAlloc', CancelAlloc);
	var vResults=new Array();
	var filters = new Array(); 
	nlapiLogExecution('Debug', 'into Block vWOId', woid);
	if (woid != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', '8'));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));//nsconfirm# is empty
	} 
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_invref_no');
	columns[1] = new nlobjSearchColumn('custrecord_act_qty');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[4] = new nlobjSearchColumn('custrecord_batch_no');
	columns[5] = new nlobjSearchColumn('custrecord_lpno');
	columns[6] = new nlobjSearchColumn('custrecord_sku_status');
	columns[7] = new nlobjSearchColumn('custrecord_packcode');
	columns[8] = new nlobjSearchColumn('custrecord_uom_level');
	columns[9] = new nlobjSearchColumn('custrecord_batch_no');
	columns[10] = new nlobjSearchColumn('custrecord_wms_location');
	columns[11] = new nlobjSearchColumn('custrecord_comp_id');
	columns[12] = new nlobjSearchColumn('name');
	columns[13] = new nlobjSearchColumn('custrecord_actendloc');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != "" && searchresults.length>0)
	{ 
		nlapiLogExecution('Debug', 'ItemCount', searchresults.length);
		for(var p=0;p<searchresults.length;p++)
		{
			var vInvRefNoOpen=searchresults[p].getValue('custrecord_invref_no');
			var vActQty=searchresults[p].getValue('custrecord_act_qty');
			var binloc=searchresults[p].getValue('custrecord_actbeginloc');
			var vActQty=searchresults[p].getValue('custrecord_act_qty');
			var lp=searchresults[p].getValue('custrecord_lpno');
			var item=searchresults[p].getValue('custrecord_sku');
			var itemstatus=searchresults[p].getValue('custrecord_sku_status');
			var packcode=searchresults[p].getValue('custrecord_packcode');
			var uomlevel=searchresults[p].getValue('custrecord_uom_level');
			var lot = searchresults[p].getValue('custrecord_batch_no');
			var whloc = searchresults[p].getValue('custrecord_wms_location');
			var company =searchresults[p].getValue('custrecord_comp_id');
			var WOname =searchresults[p].getValue('name');
			var vActendLocation =searchresults[p].getValue('custrecord_actendloc');

			nlapiLogExecution('Debug', 'vInvRefNoOpen', vInvRefNoOpen);
			nlapiLogExecution('Debug', 'vActQty', vActQty);
			if(vInvRefNoOpen != null && vInvRefNoOpen != '' && vActQty!= null && vActQty != '' )	
			{     
				var scount=1;
				var transaction='-1';
				var Invtrecid='';
				LABL1: for(var i=0;i<scount;i++)
				{	
					nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
					try
					{
						try
						{
							transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvRefNoOpen);
						}
						catch(exp)
						{
							transaction='-1';
							nlapiLogExecution('DEBUG', 'Exception in Loading Invt Record',exp);	
						}

						if(transaction!=null && transaction!='' && transaction!='-1')
						{
							var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
							var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
							var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
							var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
							var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');
							if(allocqty == null || allocqty == '')
								allocqty=0;
							nlapiLogExecution('Debug', 'qty', qty);
							nlapiLogExecution('Debug', 'allocqty', allocqty);
							nlapiLogExecution('Debug', 'CancelAlloc', CancelAlloc);
							nlapiLogExecution('Debug', 'vActQty', vActQty);
							//nlapiLogExecution('Debug', 'varqty', vLineQty);
							if(CancelAlloc==false)
							{	
								nlapiLogExecution('Debug', 'vActQty1', vActQty);
								transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)+ parseFloat(vActQty)).toFixed(4));
							}
							transaction.setFieldValue('custrecord_ebiz_qoh',(parseFloat(qty)+ parseFloat(vActQty)).toFixed(4));
							nlapiSubmitRecord(transaction, false, true);
							//Need to update status in open task
							nlapiLogExecution('Debug', 'In to updateStatusInOpenTask function');
						}
						else if(transaction ==  null || transaction == '' || transaction =='-1')
						{
							Invtrecid=createInventoryforWOReversal(vActendLocation,lp,item,itemstatus,packcode,vActQty,uomlevel,lot,whloc,company,null,null,WOname);
						}

					}
					catch(ex)
					{
						var exCode='CUSTOM_RECORD_COLLISION'; 
						var wmsE='Inventory record being updated by another user. Please try again...';
						if (ex instanceof nlobjError) 
						{	
							wmsE=ex.getCode() + '\n' + ex.getDetails();
							exCode=ex.getCode();
						}
						else
						{
							wmsE=ex.toString();
							exCode=ex.toString();
						}			
						nlapiLogExecution('ERROR', 'Exception in Pick Reversal : ', wmsE); 
						if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
						{ 
							scount=scount+1;
							continue LABL1;
						}
						else break LABL1;
					}
				}
				if(CancelAlloc==true)
				{     
					var fieldNames = new Array(); 
					fieldNames.push('custrecord_wms_status_flag');                 
					fieldNames.push('custrecord_notes');
					var newValues = new Array(); 
					newValues.push('29');                     
					newValues.push('Updated by Pick Reversal process');
				}
				else
				{
					var fieldNames = new Array(); 
					fieldNames.push('custrecord_wms_status_flag');  
					fieldNames.push('custrecord_act_end_date');
					fieldNames.push('custrecord_actendloc');   
					fieldNames.push('custrecord_act_qty');
					fieldNames.push('custrecord_notes');
					fieldNames.push('custrecord_actbeginloc');
					if(Invtrecid !=null && Invtrecid !='')
						fieldNames.push('custrecord_invref_no');

					var newValues = new Array(); 
					newValues.push('9');
					newValues.push('');
					newValues.push('');
					newValues.push('');
					newValues.push('Updated by Pick Reversal process');                           
					newValues.push(vActendLocation);
					if(Invtrecid !=null && Invtrecid !='')
						newValues.push(Invtrecid);
				}
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', searchresults[p].getId(), fieldNames, newValues);
			}
		}
		//Remove stage location inventory from create inventory
		var Ifilters = new Array();
		Ifilters[0] = new nlobjSearchFilter('custrecord_ebiz_transaction_no', null, 'anyof', woid);
		Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18,17,36]);
		var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
		if(serchInvtRec != null && serchInvtRec != '')
		{
			for(var j=0;j<serchInvtRec.length;j++)
			{
				nlapiDeleteRecord('customrecord_ebiznet_createinv',serchInvtRec[j].getId());
			}     
		}     
		if(CancelAlloc==true)
		{     
			var wosearchresultsitem = nlapiLoadRecord('workorder', woid);
			var itemcount= wosearchresultsitem.getLineItemCount('item');
			if(itemcount>0)
			{
				//var eBizWaveNo = GetMaxTransactionNo('WAVE');
				for(var s=1;s<=itemcount;s++)
				{  
					nlapiLogExecution('Debug', 'into Clear', s);
					wosearchresultsitem.setLineItemValue('item', 'custcol_ebizwoinventoryref', s,'');
					wosearchresultsitem.setLineItemValue('item', 'custcol_ebizwoavalqty', s,''); 
					wosearchresultsitem.setLineItemValue('item','custcol_locationsitemreceipt',s,'');   
					wosearchresultsitem.setLineItemValue('item','serialnumbers',s,''); 
					wosearchresultsitem.setLineItemValue('item','custcol_ebizwolotinfo',s,'');
					wosearchresultsitem.setLineItemValue('item','custcol_ebizwobinloc',s,'');
					wosearchresultsitem.setLineItemValue('item','custcol_ebizwobinlocvalue',s,'');
					wosearchresultsitem.setLineItemValue('item','custcol_ebizwolotlineno',s,'');
					wosearchresultsitem.setLineItemValue('item','custcol_ebiz_lp',s,'');
					wosearchresultsitem.setFieldValue('custbody_ebiz_update_confirm','T');
					nlapiLogExecution('Debug', 'Custom columns data cleared'); 
				}
				var CommittedId = nlapiSubmitRecord(wosearchresultsitem, true);
				nlapiLogExecution('Debug', 'CommittedId ', CommittedId);
			}
		}
		vResults[0]='S';
		vResults[1] = 'WO Pick Reversal Successful';
	}
	else
	{     
		vResults[0]='E';
		vResults[1] = 'Bin locations are not allocated to this work order';
		//showInlineMessage(form, 'Debug', 'Bin locations are not allocated to this work order', '');
		//response.writePage(form);
	}
	return vResults;
}
function getIntegrationSystemRuleValue(RuleId, loc) {

    nlapiLogExecution('Debug', 'Into Integration systemRuleValue... ', RuleId);
    nlapiLogExecution('Debug', 'loc', loc);
    var systemIntegrationRulevalue = new Array();
    try {
        var integrationfilterarray = new Array();
        integrationfilterarray[0] = new nlobjSearchFilter('name', null, 'is', RuleId);
        integrationfilterarray[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
        var locarray = new Array();
        locarray.push('@NONE@');

        if (loc != null && loc != '') {
            locarray.push(loc);
            filters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', locarray));
        }

        var integrationcolumns = new Array();
        integrationcolumns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
        integrationcolumns[1] = new nlobjSearchColumn('custrecord_ebizruletype');
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, integrationfilterarray, integrationcolumns);
        if (searchresults != null && searchresults != '') {
            var rulevalue = searchresults[0].getValue('custrecord_ebizrulevalue');
            var systemruletype = searchresults[0].getValue('custrecord_ebizruletype');
            systemIntegrationRulevalue[0] = rulevalue;
            systemIntegrationRulevalue[1] = systemruletype;
        } 

    }
    catch (exp) {
        nlapiLogExecution('Debug', 'Exception in GetSystemRules: ', exp);
        return systemIntegrationRulevalue;
    }
    return systemIntegrationRulevalue;
}
function getSystemRuleValue(RuleId,loc)
{
	nlapiLogExecution('Debug', 'Into getSystemRuleValue... ', RuleId);
	nlapiLogExecution('Debug', 'loc', loc);
	var systemrulevalue='';

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', RuleId);
		filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

		var locarray = new Array();
		locarray.push('@NONE@');
		//case# 20148056 starts
		if(loc != null && loc != '')
		{
			locarray.push(loc);
			filters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', locarray));
		}
		//case# 20148056 ends

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				systemrulevalue = searchresults[0].getValue('custrecord_ebizrulevalue');
				return systemrulevalue;
			}
			else
				return systemrulevalue;
		}
		else
			return systemrulevalue;
	}
	catch (exp) 
	{
		nlapiLogExecution('Debug', 'Exception in GetSystemRules: ', exp);
		return systemrulevalue;
	}	
}


function getRoledBasedLocationNew()
{

	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('ERROR', 'subs', subs);
	if(subs != null && subs != '' && subs==true)
	{
		var context=nlapiGetContext();
		var vSubsid=context.getSubsidiary();
		var vEmpRoleLocation=context.getLocation();
		nlapiLogExecution('Debug', 'vEmpRoleLocation', vEmpRoleLocation);
		var filters=new Array();
		if(vSubsid != null && vSubsid != '')
			filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', vSubsid));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_whlocationtype', null, 'anyof', 1));
		filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));		
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F')); 
		if(vEmpRoleLocation !=null && vEmpRoleLocation !='' && vEmpRoleLocation !=0)
			filters.push(new nlobjSearchFilter('internalid', null, 'is', vEmpRoleLocation));

		var searchresults = nlapiSearchRecord('location', null, filters, null);
		if(searchresults != null && searchresults !='')
		{
			var vRoleLocation=new Array();
			for(var k=0;k<searchresults.length;k++)
			{
				//vRoleLocation.push(searchresults[k].getId()); // case# 201412534
				vRoleLocation.push(parseInt(searchresults[k].getId()));
			}
			return vRoleLocation;
		}
		else
			return null;
	}
	else
	{
		/*var vMultiSite=true;
		var context = nlapiGetContext();
		var vRoleLocation=context.getLocation();
		if(vMultiSite==true)
		{
			return vRoleLocation;

		}	
		else
			return null;*/

		var vMultiSite=true;
		var context = nlapiGetContext();
		//var vRoleLocation = new Array();
		var vRoleLocation = null;//new Array();//case# 201412726
		if(context.getLocation() != null && context.getLocation() != '')
		{
			vRoleLocation = new Array();
			vRoleLocation.push(parseInt(context.getLocation()));
		}
		//vRoleLocation.push(context.getLocation());
		if(vMultiSite==true)
		{
			nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
			return vRoleLocation;


		}	
		else
			return null;
	}




}


function getRoledBasedLocation()
{
	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('Debug', 'subs', subs);
	if(subs != null && subs != '' && subs==true)
	{
		var context=nlapiGetContext();
		var vSubsid=context.getSubsidiary();
		nlapiLogExecution('Debug', 'vSubsid', vSubsid);
		var vEmpRoleLocation=context.getLocation();
		nlapiLogExecution('Debug', 'vEmpRoleLocation', vEmpRoleLocation);
		var filters=new Array();
		if(vSubsid != null && vSubsid != '')
			filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', vSubsid));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_whlocationtype', null, 'anyof', 1));
		filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));		
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F')); 
		if(vEmpRoleLocation !=null && vEmpRoleLocation !='' && vEmpRoleLocation!=0)
			filters.push(new nlobjSearchFilter('internalid', null, 'is', vEmpRoleLocation)); 
		var searchresults = nlapiSearchRecord('location', null, filters, null);
		if(searchresults != null && searchresults !='')
		{
			var vRoleLocation=new Array();
			for(var k=0;k<searchresults.length;k++)
			{
				vRoleLocation.push(parseInt(searchresults[k].getId()));
			}
			return vRoleLocation;
		}
		else
			return null;
	}
	else
	{
		var vMultiSite=true;
		var context = nlapiGetContext();
		//case# 20149397 20149399 starts
		//var vRoleLocation=context.getLocation();
		//var vRoleLocation = new Array();
		//vRoleLocation.push(parseInt(context.getLocation()));
		//case# 20149397 20149399 ends

		var vRoleLocation = null;//new Array();//case# 201412726
		if(context.getLocation() != null && context.getLocation() != '')
		{
			vRoleLocation = new Array();
			vRoleLocation.push(parseInt(context.getLocation()));
		}
		//vRoleLocation.push(context.getLocation());
		if(vMultiSite==true)
		{
			return vRoleLocation;

		}	
		else
			return null;
	}
}

function SetItemStatus(OrderType,seleteditem,fromLocation,toLocation)
{
	try
	{
		nlapiLogExecution("ERROR","OrderType",OrderType.toLowerCase());
		//alert(OrderType);
		var IsSiteMWH='F';
		var resultantArray=new Array();

		if(OrderType.toLowerCase()=="salesorder")
		{
			if(fromLocation!=null&&fromLocation!="")
			{
				if(seleteditem!=null&&seleteditem!="")
				{
					var ItemStatusFromItemMaster=GetItemStatusFromItemMaster(seleteditem);
					//alert('ItemStatusFromItemMaster' + ItemStatusFromItemMaster);
					var skuStatus = ItemStatusFromItemMaster[0];
					var ouboundskuStatus = ItemStatusFromItemMaster[1];
					var packCode=  ItemStatusFromItemMaster[2];
					if((skuStatus!=null&&skuStatus!="")||(ouboundskuStatus!=null&&ouboundskuStatus!=""))
					{
						if(ouboundskuStatus!=null&&ouboundskuStatus!="")
						{
							resultantArray.push(ouboundskuStatus);
							resultantArray.push(packCode);
							return resultantArray;
							//alert(resultantArray);
						}
						else
						{
							resultantArray.push(skuStatus);
							resultantArray.push(packCode);
							return resultantArray;
						}
					}
					else
					{

						IsSiteMWH=IsMWHSiteTrue(fromLocation);
						//alert('IsSiteMWH' + IsSiteMWH);						
						if(IsSiteMWH=='T')
						{
							var ItemStatusdetail=GetItemStatusAgainstSite(fromLocation);
							//alert('ItemStatusdetail' + ItemStatusdetail);
							if(ItemStatusdetail != null && ItemStatusdetail != '')
								resultantArray.push(ItemStatusdetail[0]);
							else
								resultantArray.push(null);
							//alert(ItemStatusdetail[0]);
							resultantArray.push(packCode);
							return resultantArray;
						}
					}
				}
			}
		}
		else if(OrderType.toLowerCase()=="transferorder")
		{
			if(fromLocation!=null&&fromLocation!="")
			{
				if(seleteditem!=null&&seleteditem!="")
				{
					var ItemStatusFromItemMaster=GetItemStatusFromItemMaster(seleteditem);

					var skuStatus = ItemStatusFromItemMaster[0];
					var ouboundskuStatus = ItemStatusFromItemMaster[1];
					var packCode=  ItemStatusFromItemMaster[2]; 
					if((skuStatus!=null&&skuStatus!="")||(ouboundskuStatus!=null&&ouboundskuStatus!=""))
					{
						if(ouboundskuStatus!=null&&ouboundskuStatus!="")
						{
							resultantArray.push(ouboundskuStatus);
							resultantArray.push(packCode);
							return resultantArray;
						}
						else
						{
							resultantArray.push(skuStatus);
							resultantArray.push(packCode);
							return resultantArray;
						}
					}
					else
					{
						IsSiteMWH=IsMWHSiteTrue(fromLocation);

						if(IsSiteMWH=='T')
						{
							var ItemStatusdetail=GetItemStatusAgainstSite(fromLocation);
							resultantArray.push(ItemStatusdetail[0]);
							resultantArray.push(packCode);
							return resultantArray;
						}
						else
						{
							IsSiteMWH=IsMWHSiteTrue(toLocation);

							if(IsSiteMWH=='T')
							{
								var ItemStatusdetail=GetItemStatusAgainstSite(toLocation);
								resultantArray.push(ItemStatusdetail[0]);
								resultantArray.push(packCode);
								return resultantArray;
							}
						}
					}
				}
			}
		}
		else if(OrderType.toLowerCase()=="purchaseorder")
		{
			if(fromLocation!=null&&fromLocation!="")
			{
				if(seleteditem!=null&&seleteditem!="")
				{
					var ItemStatusFromItemMaster=GetItemStatusFromItemMaster(seleteditem);

					var skuStatus = ItemStatusFromItemMaster[0];
					var ouboundskuStatus = ItemStatusFromItemMaster[1];
					var packCode=  ItemStatusFromItemMaster[2]; 
					if(skuStatus!=null&&skuStatus!="")
					{
						resultantArray.push(skuStatus);
						resultantArray.push(packCode);
						return resultantArray;
					}
					else
					{
						IsSiteMWH=IsMWHSiteTrue(fromLocation);

						if(IsSiteMWH=='T')
						{
							var ItemStatusdetail=GetItemStatusAgainstSite(fromLocation);
							resultantArray.push(ItemStatusdetail[0]);
							resultantArray.push(packCode);
							return resultantArray;
						}
					}
				}
			}
		}
		//case # 20127379 starts
		else if(OrderType.toLowerCase()=="returnauthorization")
		{

			if(fromLocation!=null&&fromLocation!="")
			{
				if(seleteditem!=null&&seleteditem!="")
				{
					var ItemStatusFromItemMaster=GetItemStatusFromItemMaster(seleteditem);
					//alert('ItemStatusFromItemMaster' + ItemStatusFromItemMaster);
					var skuStatus = ItemStatusFromItemMaster[0];
					var ouboundskuStatus = ItemStatusFromItemMaster[1];
					var packCode=  ItemStatusFromItemMaster[2];
					if((skuStatus!=null&&skuStatus!="")||(ouboundskuStatus!=null&&ouboundskuStatus!=""))
					{
						if(ouboundskuStatus!=null&&ouboundskuStatus!="")
						{
							resultantArray.push(ouboundskuStatus);
							resultantArray.push(packCode);
							return resultantArray;
							//alert(resultantArray);
						}
						else
						{
							resultantArray.push(skuStatus);
							resultantArray.push(packCode);
							return resultantArray;
						}
					}
					else
					{

						IsSiteMWH=IsMWHSiteTrue(fromLocation);
						//alert('IsSiteMWH' + IsSiteMWH);						
						if(IsSiteMWH=='T')
						{
							var ItemStatusdetail=GetItemStatusAgainstSite(fromLocation);
							//alert('ItemStatusdetail' + ItemStatusdetail);
							if(ItemStatusdetail != null && ItemStatusdetail != '')
								resultantArray.push(ItemStatusdetail[0]);
							else
								resultantArray.push(null);
							//alert(ItemStatusdetail[0]);
							resultantArray.push(packCode);
							return resultantArray;
						}
					}
				}
			}

		}
		// case 20127379 ends
		nlapiLogExecution("ERROR","resultantArray",resultantArray);
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in SetItemStatus of Generalfunction',exp);
	}
}

function getCreditExceedList()
{ 	 
	var CrExceedCustSearch = nlapiSearchRecord('customer', 'customsearch_ebiz_customer_overdue', null, null);
	return CrExceedCustSearch;
}
function getDueDaysList()
{  
	var CrDueCustSearch = nlapiSearchRecord('customer', 'customsearch_ebiz_customer_due_days', null, null);
	return CrDueCustSearch;
}




function getStageRulewithCarrierType(Item, vCarrier, vSite, vCompany, stgDirection,ordertype,vCarrierType){
	nlapiLogExecution('Debug', 'into getStageRule');
	nlapiLogExecution('Debug', 'Item', Item);
	nlapiLogExecution('Debug', 'vSite', vSite);
	nlapiLogExecution('Debug', 'vCompany', vCompany);
	nlapiLogExecution('Debug', 'stgDirection', stgDirection);
	nlapiLogExecution('Debug', 'ordertype', ordertype);
	nlapiLogExecution('Debug', 'vCarrier',vCarrier);
	nlapiLogExecution('Debug', 'vCarrierType',vCarrierType);
	var ItemFamily, ItemGroup, Location;
	var stagelocation="";
	var stagedetails=new Array();
	try
	{


		var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];

		var ItemFamily = "";
		var ItemGroup = "";
		var ItemStatus = "";
		var ItemInfo1 = "";
		var ItemInfo2 ="";
		var ItemInfo3 = "";

		if(Item!=null && Item!="")
		{
			var columns = nlapiLookupField('item', Item, fields);
			nlapiLogExecution('Debug', 'after nlapiLookupField');
			ItemFamily = columns.custitem_item_family;
			ItemGroup = columns.custitem_item_group;
			ItemStatus = columns.custitem_ebizdefskustatus;
			ItemInfo1 = columns.custitem_item_info_1;
			ItemInfo2 = columns.custitem_item_info_2;
			ItemInfo3 = columns.custitem_item_info_3;
		}
		var locgroupid = 0;
		var zoneid = 0;
		var direction = "";
		if (stgDirection == 'INB') {
			direction = "1";
		}
		else 
			if (stgDirection == 'OUB') {
				direction = "2";
			}
			else
			{
				direction = "3";
			}

		var columns = new Array();
		var filters = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
		columns[0].setSort();
		columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
		columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
		columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
		columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');
		columns[5] = new nlobjSearchColumn('name');
		columns[6] = new nlobjSearchColumn('custrecord_carrier_type','custrecord_carrierstgrule');
		columns[7] = new nlobjSearchColumn('custrecord_stgautopackflag');
		columns[8] = new nlobjSearchColumn('custrecord_carrier_name','custrecord_carrierstgrule');
		columns[9] = new nlobjSearchColumn('custrecord_carriertypestgrule');

		if (ItemFamily != null && ItemFamily != "") {
			filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
		}
		if (ItemGroup != null && ItemGroup != "") {
			filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
		}
		if (Item != null && Item != "") {
			filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
		}
		if (vCarrier != null && vCarrier != "") {
			filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

		}
		if (vCarrierType != null && vCarrierType != "") {
			filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

		}
		if (vSite != null && vSite != "") {
			filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

		}
		if (vCompany != null && vCompany != "") {
			filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

		}
		if (ordertype != null && ordertype != "") {
			filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@', ordertype]));

		}
		filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
		filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'noneof', ['14']));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
		if (searchresults != null) {
			for (var i = 0; i < Math.min(10, searchresults.length); i++) {
				//GetStagLpCount                
				var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

				var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');

				var stgrule = searchresults[i].getValue('name');
				var carrier = searchresults[i].getText('custrecord_carrier_type','custrecord_carrierstgrule');
				if(carrier==null || carrier=='')
					var carrier = searchresults[i].getText('custrecord_carriertypestgrule');
				var carriername = searchresults[i].getText('custrecord_carrier_name','custrecord_carrierstgrule');

				var autopackflag = searchresults[i].getValue('custrecord_stgautopackflag');
				var stagedet=new Array();

				var currentRow = [carrier,autopackflag,carriername];

				stagedet[0] = currentRow;

				if (vstgLocation != null && vstgLocation != "") {
					var vLpCnt = GetStagLpCount(vstgLocation, vSite, vCompany);
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (vLpCnt < vnoofFootPrints) {

							return stagedet;
						}
					}
					else {
						return stagedet;
					}
				}
				var LocGroup = "";
				if (stgDirection == 'INB') {
					LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
				}
				else {
					LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
				}
				if (LocGroup != null && LocGroup != "") {

					var collocGroup = new Array();
					var filtersLocGroup = new Array();
					collocGroup[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					collocGroup[0].setSort();
					collocGroup[1] = new nlobjSearchColumn('name');
					collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

					if (stgDirection == 'INB') {
						filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
					}
					else {
						filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
					}

					var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
					for (var k = 0; searchresults != null && k < searchresults.length; k++) {
						var vLocid=searchresults[k].getId();
						var vLpCnt = GetStagLpCount(vLocid, vSite, vCompany);
						var vnoofFootPrints = searchresults[k].getValue('custrecord_nooffootprints');

						//--
						if (vnoofFootPrints != null && vnoofFootPrints != "") {
							if (vLpCnt < vnoofFootPrints) {
								return stagedet;
							}
						}
						else {
							return stagedet;
						}
					}
				}
			}
		}
		nlapiLogExecution('Debug', 'out of getStageRule');
	}
	catch(exp){
		InsertExceptionLog('General Functions',2, 'getStageRule function', exp, '', '','','','', nlapiGetUser());
	}
	return stagedetails;
}

function checkserialritem(itemNo)
{

	nlapiLogExecution('Debug', 'checkserialritem', 'Start');
	nlapiLogExecution('Debug', 'Input Item No', itemNo);

	var currItem = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_nls_sns_serial_number', null, 'is', itemNo));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_nls_sns_item_no');

	var itemSearchResults = nlapiSearchRecord('customrecord_nls_serial_no_status', null, filters, columns);

	//nlapiLogExecution('Debug', 'result Item No', itemSearchResults[0].getValue('custrecord_nls_sns_item_no'));

	if(itemSearchResults != null)
		currItem = itemSearchResults[0].getText('custrecord_nls_sns_item_no');


	var logMsg = 'Item = ' + currItem;
	nlapiLogExecution('Debug', 'Item Retrieved', logMsg);
	nlapiLogExecution('Debug', 'checkserialritem', 'End');

	return currItem;
}


function GetItemStatusAgainstSite(location)
{
	try
	{
		//alert('location' + location);
		var getFetchedItemStatusId;
		var getFetchedItemStatus;
		var getFetchedSiteLocation;
		var getStatusArray=new Array();
		var ItemStatusFilters = new Array();
		ItemStatusFilters.push(new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T'));
		ItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', location));

		var ItemStatusColumns = new Array();
		ItemStatusColumns.push(new nlobjSearchColumn('internalid'));
		ItemStatusColumns.push(new nlobjSearchColumn('name'));
		ItemStatusColumns.push(new nlobjSearchColumn('custrecord_ebizsiteskus'));

		var ItemStatusResults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemStatusFilters, ItemStatusColumns);
		if (ItemStatusResults != null) 
		{

			//alert('ItemStatusResults' + ItemStatusResults);
			var ItemStatusResult = ItemStatusResults[0];
			//alert('ItemStatusResults[0]' + ItemStatusResults[0]);
			getFetchedItemStatusId = ItemStatusResult.getId();
			//alert('ItemStatusResult.getId()' + ItemStatusResult.getId());
			getFetchedItemStatus = ItemStatusResult.getValue('name');
			getFetchedSiteLocation = ItemStatusResult.getValue('custrecord_ebizsiteskus');
			getStatusArray.push(getFetchedItemStatusId);
			getStatusArray.push(getFetchedItemStatus);
			getStatusArray.push(getFetchedSiteLocation);
			//alert('getFetchedItemStatusId' + getFetchedItemStatusId);
			nlapiLogExecution('Debug','getStatusArray',getStatusArray);
		}

		return getStatusArray;
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in GetItemStatusAgainstSite of Generalfuncation ',exp);
	}
}



function IsMWHSiteTrue(location)
{
	try
	{
		var mwhsiteflag='F';
		var fields = ['custrecord_ebizwhsite'];

		var locationcolumns = nlapiLookupField('location', location, fields);
		if(locationcolumns!=null)
			mwhsiteflag = locationcolumns.custrecord_ebizwhsite;

		nlapiLogExecution('Debug','mwhsiteflag',mwhsiteflag);
		return mwhsiteflag;
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in IsMWHSiteTrue of Generalfuncation ',exp);
	}

}


function GetItemStatusFromItemMaster(Itemid)
{
	try
	{
		var fields = ['custitem_ebizdefpackcode','custitem_ebizdefskustatus','custitem_ebizoutbounddefskustatus'];
		var columns = nlapiLookupField('item', Itemid, fields);
		var skuStatus = columns.custitem_ebizdefskustatus;
		var ouboundskuStatus = columns.custitem_ebizoutbounddefskustatus;
		var packCode= columns.custitem_ebizdefpackcode;

		var ItemStatusFromItemMasterArray=new Array();
		ItemStatusFromItemMasterArray.push(skuStatus);
		ItemStatusFromItemMasterArray.push(ouboundskuStatus);
		ItemStatusFromItemMasterArray.push(packCode);

		nlapiLogExecution('Debug','ItemStatusFromItemMasterArray',ItemStatusFromItemMasterArray);
		return ItemStatusFromItemMasterArray;

	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in GetItemStatusFromItemMaster of Generalfuncation ',exp);
	}
}

function CheckOrderStatus(Orderid,trantype)
{
	try
	{
		nlapiLogExecution("ERROR","CheckOrderStatus");
		nlapiLogExecution("ERROR","Orderid",Orderid);
		nlapiLogExecution("ERROR","trantype",trantype);
		var CheckStatus="F";

		if(Orderid!=null&&Orderid!="")
		{
			var Orderrec= nlapiLoadRecord(trantype, Orderid);
			if(Orderrec!=null && Orderrec!='')
			{
				var OrdStatus = Orderrec.getFieldValue('status');

				nlapiLogExecution("ERROR","OrdStatus",OrdStatus);
				var newOrdstatus = "Facturaci&#243;n pendiente/parcialmente recibido";
				//newOrdstatus = newOrdstatus.toString();
				nlapiLogExecution("ERROR","newOrdstatus",newOrdstatus);
				if((OrdStatus == newOrdstatus).toString())
					nlapiLogExecution("ERROR","here",newOrdstatus);


				if(trantype=="purchaseorder")
				{					
					if(OrdStatus=="Pending Receipt"||OrdStatus=="Pending Billing/Partially Received"||OrdStatus=="Approved by Supervisor/Pending Receipt" ||OrdStatus=="Partially Received" || OrdStatus == "Recibo pendiente" || OrdStatus == "En espera de facturaci�n / Parcialmente Recibido" || OrdStatus == 'Aprobado por el supervisor/Recibo pendiente' || (OrdStatus == newOrdstatus).toString())
					{
						if(OrdStatus=="Pending Receipt"||OrdStatus=="Pending Billing/Partially Received"||OrdStatus=="Approved by Supervisor/Pending Receipt" ||OrdStatus=="Partially Received" || OrdStatus == "Recibo pendiente" || OrdStatus == "En espera de facturaci�n / Parcialmente Recibido" || OrdStatus == 'Aprobado por el supervisor/Recibo pendiente')
							CheckStatus="T";
					}
					//Case # 20125292, Moved closing bracket for purchaseorder trantype from below where it is covering all the conditions.
				}
				//End
				else if(trantype=="transferorder")
				{
					if(OrdStatus=="Pending Receipt"||OrdStatus=="Pending Receipt/Partially Fulfilled" ||OrdStatus=="Partially Received" || OrdStatus == 'Recibo pendiente' || OrdStatus == "En espera de facturaci�n / Parcialmente Recibido")
						CheckStatus="T";
				}
				else if(trantype=="returnauthorization")
				{
					if(OrdStatus=="Pending Receipt"||OrdStatus=="Pending Refund/Partially Received" ||OrdStatus=="Partially Received"  || OrdStatus == 'Recibo pendiente' || 
							OrdStatus == "En espera de facturaci�n / Parcialmente Recibido" || (OrdStatus == newOrdstatus).toString())
						CheckStatus="T";
				}

			}
			nlapiLogExecution("ERROR","CheckStatus",CheckStatus);
			return CheckStatus;
		}
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","CheckOrderStatus",exp);
	}
}


//to get Lot results from Batch format
function GetLotParsingResults(Itemid,vendor,ScannedBatch)
{
	var getBatch;
	try
	{

		nlapiLogExecution("ERROR","Itemid",Itemid);
		nlapiLogExecution("ERROR","vendor",vendor);
		nlapiLogExecution("ERROR","ScannedBatch",ScannedBatch);

		var StartBtye;
		var StartBtye;
		var Direction;

		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_barcode_process_item',null,'anyof',Itemid));
		//filter.push(new nlobjSearchFilter('custrecord_ebiz_barcode_process_vendor',null,'anyof',vendor));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_barcode_process_vendor',null,'anyof',['@NONE@',vendor]));
		filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizbeginbyte');
		column[1] = new nlobjSearchColumn('custrecord_ebizendbyte');
		column[2] = new nlobjSearchColumn('custrecord_ebiz_barcode_pars_direction');
		column[3] = new nlobjSearchColumn('custrecord_ebiz_barcode_process_vendor');
		column[3].setSort(false);

		//column[3] = new nlobjSearchColumn('custrecord_ebiz_barcode_pars_direction');

		var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_format', null, filter, column);
		if(searchRecord!=null && searchRecord!='')
		{
			StartBtye = searchRecord[0].getValue('custrecord_ebizbeginbyte');
			EndBtye = searchRecord[0].getValue('custrecord_ebizendbyte');
			Direction = searchRecord[0].getValue('custrecord_ebiz_barcode_pars_direction');

			nlapiLogExecution("ERROR","StartBtye",StartBtye);
			nlapiLogExecution("ERROR","EndBtye",EndBtye);
			nlapiLogExecution("ERROR","Direction",Direction);
			nlapiLogExecution("ERROR","Direction",Direction);


			var totallength=ScannedBatch.length;
			nlapiLogExecution("ERROR","totallength",totallength);

			if(Direction ==1)//Left to Right
			{
				getBatch=ScannedBatch.substring(StartBtye-1,EndBtye);
			}
			else if(Direction ==2)//Left to Right
			{
				getBatch=ScannedBatch.substring((totallength-EndBtye),totallength);
				nlapiLogExecution("ERROR","(totallength-EndBtye)",(totallength-EndBtye));
			}
			else    //none of 
				getBatch=ScannedBatch;
		}
		else
		{
			getBatch=ScannedBatch;
		}
		nlapiLogExecution("ERROR","getBatch",getBatch);


	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in GetLotParsingResults ',exp);
	}

	return getBatch;
}

function clearItemAllocations(ActPickQty,contlpno,expPickQty,remQty,SalesOrderInternalId,itemno,binlocation)
{
	nlapiLogExecution('Debug','Into clearItemAllocations');

	var str = 'CARTON LP. = ' + contlpno + '<br>';	
	str = str + 'ActPickQty. = ' + ActPickQty + '<br>';	
	str = str + 'expPickQty. = ' + expPickQty + '<br>';	
	str = str + 'remQty. = ' + remQty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	str = str + 'itemno. = ' + itemno + '<br>';	
	str = str + 'binlocation. = ' + binlocation + '<br>';	
	nlapiLogExecution('Debug', 'Parameter Values', str);

	var invtintrid = 0;

	var invtfilters = new Array(); 			 
	invtfilters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemno);
	invtfilters[1] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', binlocation);
	invtfilters[2] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]);
	invtfilters[3] = new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthanorequalto', parseInt(expPickQty));

	var invtcolumns1 = new Array(); 
	invtcolumns1[0] = new nlobjSearchColumn( 'custrecord_ebiz_inv_sku' ); 			
	invtcolumns1[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_ebiz_inv_binloc');
	invtcolumns1[2] = new nlobjSearchColumn( 'custrecord_ebiz_qoh' );
	invtcolumns1[3] = new nlobjSearchColumn( 'custrecord_ebiz_inv_binloc' );
	invtcolumns1[1].setSort();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns1); 

	nlapiLogExecution('Debug', 'invtsearchresults', invtsearchresults);

	if(invtsearchresults!=null && invtsearchresults!='')
	{
		invtintrid = invtsearchresults[0].getId();
	}
	else
	{
		var invtfilters = new Array(); 			 
		invtfilters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemno);
		invtfilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]);
		invtfilters[2] = new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthanorequalto', parseInt(expPickQty));

		var invtcolumns1 = new Array(); 
		invtcolumns1[0] = new nlobjSearchColumn( 'custrecord_ebiz_inv_sku' ); 			
		invtcolumns1[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_ebiz_inv_binloc');
		invtcolumns1[2] = new nlobjSearchColumn( 'custrecord_ebiz_qoh' );
		invtcolumns1[3] = new nlobjSearchColumn( 'custrecord_ebiz_inv_binloc' );
		invtcolumns1[1].setSort();

		var invtsearchresults2 = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns1); 

		nlapiLogExecution('Debug', 'invtsearchresults2', invtsearchresults2);

		if(invtsearchresults2!=null && invtsearchresults2!='')
		{
			invtintrid = invtsearchresults2[0].getId();
		}
	}

	nlapiLogExecution('Debug', 'invtintrid', invtintrid);

	if(invtintrid!=null && invtintrid!='' && invtintrid!=0)
	{
		var scount=1;
		var invtrecid="";
		var newqoh = 0;

		LABL1: for(var i=0;i<scount;i++)
		{
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);

			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invtintrid);
				nlapiLogExecution('Debug', 'Invttran', Invttran);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
				var Itemname = Invttran.getFieldText('custrecord_ebiz_inv_sku');

				var remainqty=0;


				if(parseInt(expPickQty) != parseInt(ActPickQty))
				{

					if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
					{
						newqoh = parseInt(InvQOH) - parseInt(expPickQty);
					}
					else
					{
						newqoh = parseInt(Invallocqty) + parseInt(remQty);
					}
				}
				else
				{
					newqoh = parseInt(InvQOH) - parseInt(ActPickQty);
				}

				nlapiLogExecution('Debug', 'newqoh',newqoh);
				nlapiLogExecution('Debug', 'Itemname',Itemname);

				Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh));  
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

				if(invtrecid!=null && invtrecid!='' && parseFloat(ActPickQty)>0)
					CreateSTGInvtRecord(invtrecid, contlpno,ActPickQty,SalesOrderInternalId);

				if((parseInt(newqoh)) <= 0)
				{		
					nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invtrecid);
					var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invtrecid);				
				}


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
	}

	nlapiLogExecution('Debug','Out of clearItemAllocations');
}

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,SalesOrderInternalId) 
{
	nlapiLogExecution('Debug', 'Into CreateSTGInvtRecord');
	var str = 'Inv Ref No. = ' + invtrecid + '<br>';
	str = str + 'CARTON LP. = ' + vContLp + '<br>';	
	str = str + 'Qty. = ' + vqty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	nlapiLogExecution('Debug', 'STG INVT Details', str);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', vqty);
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', vqty);
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');	
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note1', '');	
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note2', '');	
		// Temporarily hard coded by Satish.N
		//stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', 7218);	
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', SalesOrderInternalId);

		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('Debug', 'Out of CreateSTGInvtRecord');
}

var _MS_PER_DAY = 1000 * 60 * 60 * 24;

//a and b are javascript Date objects
function dateDiffInDays(a, b) {
//	Discard the time and time-zone information.
	var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

	return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}


function addDays()
{
	var now = new Date();

	var curdate = ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());

	var newdate = nlapiAddDays(new Date(curdate), 90);
}

function getLatestExpiry(customer,item,site)
{
	nlapiLogExecution('Debug', 'Into getLatestExpiry');

	var str = 'customer. = ' + customer + '<br>';
	str = str + 'item. = ' + item + '<br>';	
	str = str + 'site. = ' + site + '<br>';	

	nlapiLogExecution('Debug', 'Function Parameters', str);

	var lotfilters = new Array(); 			 
	lotfilters.push(new nlobjSearchFilter('custrecord_ebiz_lot_customer', null, 'anyof', customer));
	lotfilters.push(new nlobjSearchFilter('custrecord_ebiz_lot_item', null, 'anyof', item));
	lotfilters.push(new nlobjSearchFilter('custrecord_ebiz_lot_whlocation', null, 'anyof', site));

	var lotcolumns = new Array(); 
	lotcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_lotno'); 			
	lotcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_lot_expiry');
	lotcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_lot_pickdate');
	lotcolumns[1].setSort(true);

	var customerlotdet = nlapiSearchRecord('customrecord_ebiz_customer_lot', null, lotfilters, lotcolumns); 

	nlapiLogExecution('Debug', 'Out of getLatestExpiry',customerlotdet);

	return customerlotdet;
}
//to get Stock Adjustment Details

function getStockAdjustmentDetails(loc,tasktype,adjusttype)
{	
	var StockAdjustAccResults=null;
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();

	if(loc!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location',null,'anyof',[loc]));	

	if(tasktype!=null && tasktype!="")
		filterStAccNo.push(new nlobjSearchFilter('custrecord_adjusttasktype',null,'anyof',[tasktype]));

//	if(tasktype==11) //11 is the internalid value of tasktype "ADJT";
//	{	
//	if(adjusttype!=null || adjusttype!="")
//	filterStAccNo.push(new nlobjSearchFilter('custrecord_adjustment_type',null,'anyof',adjusttype));
//	filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));
//	}	

	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');  
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_adjtype_trantype');  
	colsStAccNo[2] = new nlobjSearchColumn('custrecord_ebiz_adjtype_mapnslocation');  
	colsStAccNo[3] = new nlobjSearchColumn('custrecord_ebiz_adtype_movewmsbinloc');  


	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	if(StockAdjustAccResults!=null && StockAdjustAccResults!='' && StockAdjustAccResults.length>0)
	{
		nlapiLogExecution('ERROR', 'StockAdjustAccResults',StockAdjustAccResults.length);

	}

	return StockAdjustAccResults;	
}


function SystemRuleForStockAdjustment(vSite)
{

	try{

		var ResultArray = new Array();
		var rulevalue='';

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is', 'Adjust Inventory for Short Picks?');
		filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

		var locationarr = new Array();
		locationarr.push('@NONE@');
		if(vSite!=null && vSite!='')
		{
			locationarr.push(vSite);
			filters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', locationarr));
		}
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');
		columns[1].setSort();

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			rulevalue = searchresults[0].getValue('custrecord_ebizrulevalue');
			if(rulevalue=='PICKTASKLEVEL')
			{
				var vmessage="STND";
				ResultArray[0]='Y';				
				ResultArray[1]= vmessage;
				return ResultArray;
			}
			else if(rulevalue=='BINLEVEL')
			{
				var vmessage="ADJUSTBINLOC";
				ResultArray[0]='Y';				
				ResultArray[1]= vmessage;
				return ResultArray;
			}
			else
			{
				var vmessage="NOADJUST";
				ResultArray[0]='Y';				
				ResultArray[1]= vmessage;
				return ResultArray;
			}

		}
		else
		{
			var vmessage="STND";
			ResultArray[0]='Y';				
			ResultArray[1]= vmessage;
			return ResultArray;
		}
	}
	catch (exp) 
	{
		nlapiLogExecution('ERROR', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		var vmessage="STND";
		ResultArray[0]='Y';				
		ResultArray[1]= vmessage;
		return ResultArray;
	}	
}



function updateBin()
{

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_note2', null, 'is', 'D');
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);	

	if(searchresults!=null && searchresults!='')
	{
		for(i=0;i<searchresults.length;i++)
		{
			var vbinloc=searchresults[i].getText('custrecord_ebiz_inv_binloc');
			var vloc=searchresults[i].getValue('custrecord_ebiz_inv_loc');
			var vinvtid = searchresults[i].getId();

			var bfilters = new Array();
			bfilters[0] = new nlobjSearchFilter('name', null, 'is', vbinloc);
			bfilters[1] = new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', vloc);

			var bcolumns = new Array();
			bcolumns[0] = new nlobjSearchColumn('name');

			var bsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, bfilters, bcolumns);	

			if(bsearchresults!=null && bsearchresults!='')
			{
				var vbinid = bsearchresults[0].getId();

				var fields = new Array();
				var values = new Array();

				fields.push('custrecord_ebiz_inv_note2');
				fields.push('custrecord_ebiz_inv_binloc');

				values.push('');
				values.push(vbinid);

				nlapiSubmitField('customrecord_ebiznet_createinv', vinvtid, fields, values);
			}			

		}

	}
}


function ValidateSplCharacter(string,name)
{
	try
	{
		var iChars = "*|,\":<>[]{}`\';()@&$#% ";
		var length=string.length;
		var flag = 'N';
		for(var i=0;i<length;i++)
		{
			if(iChars.indexOf(string.charAt(i))!=-1)
			{
				flag='Y';
				break;
			}
		}
		if(flag == 'Y')
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	catch(e)
	{
		nlapiLogExecution("ERROR","ValidateSplCharacter",e);
		return false;
	}
}
function getpackagenumberbyfo(fonumber)
{
	// start of  get the PackageNo From Shipmanifest
	var sequenceNo='';
	nlapiLogExecution('Debug','into Calculate NoOf Packages','into Calculate NoOf Packages');
	nlapiLogExecution('Debug','fonumber',fonumber);
	var filtersShipmanifest = new Array();		
	filtersShipmanifest .push(new nlobjSearchFilter('custrecord_ship_ref3', null, 'is', fonumber));
	//filtersShipmanifest .push(new nlobjSearchFilter('custrecord_ship_pkgcount', null, 'isnotempty'));

	var colsShipmanifest = new Array();		
	//colsShipmanifest [0] = new nlobjSearchColumn('custrecord_ship_pkgcount',null, 'group');	
	colsShipmanifest [0] = new nlobjSearchColumn('custrecord_ship_pkgno',null, 'Max');



	var Workstationsearchresults = nlapiSearchRecord('customrecord_ship_manifest', null, filtersShipmanifest , colsShipmanifest);
	if(Workstationsearchresults!=null && Workstationsearchresults!='')
	{
		//noofpackages=Workstationsearchresults[0].getValue('custrecord_ship_pkgcount',null,'group');
		sequenceNo=Workstationsearchresults[0].getValue('custrecord_ship_pkgno',null, 'Max');
	}
	if(sequenceNo!=null && sequenceNo!='')
	{
		nlapiLogExecution('Debug','SequenceNoisNotEmpty',sequenceNo);
		sequenceNo=parseInt(sequenceNo)+1;
	}
	else
	{
		nlapiLogExecution('Debug','SequenceNoisEmpty',sequenceNo);
		sequenceNo=1;	   
	}
	nlapiLogExecution('Debug','sequenceNo',sequenceNo);
	return sequenceNo;


	// End of  get the PackageNo From Shipmanifest
}

//Check whether Packing is completed ot not
function getpackingcompletd(fonumber)
{
	var deviceuploadflag='F';
	nlapiLogExecution('Debug','into CheckPackingCompletedorNot','into CheckPackingCompletedorNot');
	var filtersopentask = new Array();		
	//filtersShipmanifest .push(new nlobjSearchFilter('custrecord_ship_orderno', null, 'is', SalesOrdNo));
	filtersopentask .push(new nlobjSearchFilter('name', null, 'is', fonumber));
	filtersopentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filtersopentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));
	//filtersShipmanifest .push(new nlobjSearchFilter('custrecord_ship_pkgcount', null, 'isnotempty'));

	var colsopentask = new Array();		
	//colsShipmanifest [0] = new nlobjSearchColumn('custrecord_ship_pkgcount');	
	colsopentask [0] = new nlobjSearchColumn('custrecord_device_upload_flag');


	var opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersopentask , colsopentask);


//	if(opentasksearchresults!=null && opentasksearchresults !='')
//	{
//	deviceuploadflag=opentasksearchresults[0].getValue('custrecord_device_upload_flag');
//	}
	return opentasksearchresults;

}

//Count of ContainerLp for Totalpackages
function getTotalpackagenumberbyfo(vebizOrdNo, name, cartonwave)
{


	nlapiLogExecution('Debug','into CheckTotalNoOfPackages','into CheckTotalNoOfPackages');
	nlapiLogExecution('Debug','vebizOrdNo',vebizOrdNo);
	var filtersopentask = new Array();		
	//filtersShipmanifest .push(new nlobjSearchFilter('custrecord_ship_orderno', null, 'is', SalesOrdNo));
	filtersopentask .push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [vebizOrdNo]));
	//filtersShipmanifest .push(new nlobjSearchFilter('custrecord_ship_pkgcount', null, 'isnotempty'));

	var colsopentask = new Array();		
	//colsShipmanifest [0] = new nlobjSearchColumn('custrecord_ship_pkgcount');	
	colsopentask [0] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');


	var opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersopentask , colsopentask);



	return opentasksearchresults;

}


function IsBatchNoValid(BeginLocationId,ItemId,getExpectedQuantity,BatchId)
{
	try
	{
		nlapiLogExecution('DEBUG','BeginLocationId',BeginLocationId);
		nlapiLogExecution('DEBUG','ItemId',ItemId);
		nlapiLogExecution('DEBUG','getExpectedQuantity',getExpectedQuantity);
		//nlapiLogExecution('DEBUG','getItemStatusId',getItemStatusId);
		nlapiLogExecution('DEBUG','BatchId',BatchId);

		var summaryArray=new Array();
		var tempflag='F';
		var recid="";
		var Lp="";
		var ItemStatus="";
		var Packcode="";
		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null,'anyof',BeginLocationId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null,'anyof',ItemId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot',null,'anyof',BatchId));

//		if(getItemStatusId !=null && getItemStatusId !=''){
//		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof',getItemStatusId));
//		}

		filter.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty',null, 'greaterthan', getExpectedQuantity));
		filter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));

		var Columns = new Array();
		Columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		Columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
		Columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');

		var searchrecord=nlapiSearchRecord('customrecord_ebiznet_createinv',null,filter,Columns);

		if(searchrecord!=null&&searchrecord!="")
		{
			tempflag='T';
			recid=searchrecord[0].getId();
			Lp=searchrecord[0].getValue('custrecord_ebiz_inv_lp');
			ItemStatus=searchrecord[0].getValue('custrecord_ebiz_inv_sku_status');
			Packcode=searchrecord[0].getValue('custrecord_ebiz_inv_packcode');

			nlapiLogExecution('DEBUG','recid',recid);
			nlapiLogExecution('DEBUG','Lp',Lp);
			nlapiLogExecution('DEBUG','ItemStatus',ItemStatus);
			nlapiLogExecution('DEBUG','Packcode',Packcode);
		}
		summaryArray.push(tempflag);
		summaryArray.push(recid);
		summaryArray.push(Lp);
		summaryArray.push(Packcode);
		summaryArray.push(ItemStatus);
		nlapiLogExecution('DEBUG','summaryArray',summaryArray);
		return summaryArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in IsBatchNoValid',exp);
	}
}

function GetBatchId(itemid,getEnteredbatch)
{
	try
	{
		nlapiLogExecution('DEBUG','itemid',itemid);
		nlapiLogExecution('DEBUG','getEnteredbatch',getEnteredbatch);

		var globalArray=new Array();
		var BatchID;
		var flag='F';
		var filter=new Array();
		if(itemid!=null&&itemid!="")
			filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid));
		filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',getEnteredbatch));
		var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filter,null);
		if(rec!=null&&rec!="")
		{
			BatchID=rec[0].getId();
			flag='T';
		}
		globalArray.push(BatchID);
		globalArray.push(flag);
		nlapiLogExecution('DEBUG','globalArray',globalArray);
		return globalArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetBatchId',exp);
	}
}


function GetSystemRuleForPostItemReceiptby(pwhlocation) //Case# 20149154
{
	nlapiLogExecution('ERROR','pwhlocation',pwhlocation);
	try
	{
		var rulevalue='';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Post Item Receipt by'));
		//filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',pwhlocation]));

		// case no start 20126968
		var vRoleLocation=getRoledBasedLocation();
		var resloc=new Array();
		resloc.push("@NONE@");

		nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
		//case# 20149824
		if(pwhlocation!=null && pwhlocation!='')
		{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',pwhlocation]));
		}
		else if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			for(var count=0;count<vRoleLocation.length;count++)
				resloc.push(vRoleLocation[count]);
			filter.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', resloc));
		}
		// Case# 20149154 starts
		/*else
			{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',pwhlocation]));
			}*/
		// Case# 20149154 ends
		// case no end 20126968
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		column[1]=new nlobjSearchColumn('custrecord_ebizsite');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);
		var siteval = '';
		if(searchresult!=null && searchresult!="")
		{
			for(var k = 0;k<searchresult.length;k++)
			{
				siteval = searchresult[k].getValue('custrecord_ebizsite');

				nlapiLogExecution('ERROR','siteval',siteval);

				if(pwhlocation!=null && pwhlocation!='')
				{
					if(pwhlocation == siteval)
					{
						rulevalue=searchresult[k].getValue('custrecord_ebizrulevalue');
						break;
					}
				}
			}

			if(rulevalue==null || rulevalue=='')
			{
				for(var z = 0;z<searchresult.length;z++)
				{
					siteval = searchresult[z].getValue('custrecord_ebizsite');

					if(siteval==null || siteval=='')
					{
						rulevalue=searchresult[z].getValue('custrecord_ebizrulevalue');
						break;
					}
				}

			}
		}

		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		if(rulevalue!=null && rulevalue!='undefined' && rulevalue!='null' && rulevalue!='')
		{
			if(rulevalue.trim()!='LP' && rulevalue.trim()!='PO')
			{
				rulevalue='LP';
			}
		}
		else
		{
			rulevalue='LP';
		}
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}

//case # 20141019....Added below function.
function IsCustomScreenRequired(vScreen)
{
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', vScreen));	
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));	

	columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
	columns[1] = new nlobjSearchColumn('custrecord_ebizrule_scriptid');
	columns[2] = new nlobjSearchColumn('custrecord_ebizrule_deployid');

	var ruleDetails = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);

	return ruleDetails;

}



//this function returns sum of actqty from OT and CT 
function GetTotalReceivedQty(poInternalId,itemId,lineno)
{
	try
	{

		nlapiLogExecution('ERROR','poInternalId',poInternalId);
		nlapiLogExecution('ERROR','itemId',itemId);
		nlapiLogExecution('ERROR','lineno',lineno);
		var TotalQty=0;
		var OpentaskActQty=0;
		var ClosedtaskActQty=0;
		var OpenTaskFilters = new Array();
		OpenTaskFilters[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poInternalId);
		OpenTaskFilters[1] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', itemId);
		OpenTaskFilters[2] = new nlobjSearchFilter('custrecord_line_no', null, 'equalto', lineno);
		OpenTaskFilters[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
		OpenTaskFilters[4] = new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isnotempty');
		OpenTaskFilters[5] = new nlobjSearchFilter('custrecord_wms_status_flag',  null,'noneof',[32]);

		var OpenTaskColumns = new Array();
		OpenTaskColumns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		OpenTaskColumns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
		OpenTaskColumns[2] = new nlobjSearchColumn('custrecord_act_qty', null, 'sum');


		var OpenTaskSearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskFilters, OpenTaskColumns);

		if (OpenTaskSearchresults != null && OpenTaskSearchresults !=''&& OpenTaskSearchresults.length > 0)
		{
			OpentaskActQty = OpenTaskSearchresults[0].getValue('custrecord_act_qty', null, 'sum');

			//if(OpentaskActQty==null || OpentaskActQty =='' || isNAN(OpentaskActQty))
			if(OpentaskActQty==null || OpentaskActQty =='' || isNaN(OpentaskActQty))// Case# 20149354
				OpentaskActQty=0;

			nlapiLogExecution('ERROR','OpentaskActQty',OpentaskActQty);
			TotalQty = TotalQty+ parseFloat(OpentaskActQty);
		}

		nlapiLogExecution('ERROR','TotalQty',TotalQty);


		var ClosedTaskFilters = new Array();
		ClosedTaskFilters[0] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', poInternalId);
		ClosedTaskFilters[1] = new nlobjSearchFilter('custrecord_ebiztask_sku', null, 'anyof', itemId);
		ClosedTaskFilters[2] = new nlobjSearchFilter('custrecord_ebiztask_line_no', null, 'equalto', lineno);
		ClosedTaskFilters[3] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [2]);
		ClosedTaskFilters[4] = new nlobjSearchFilter('custrecord_ebiz_nsconf_refno_ebiztask',  null,'isnotempty');
		ClosedTaskFilters[5] = new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag',  null,'noneof',[32]);

		var ClosedTaskColumns = new Array();
		ClosedTaskColumns[0]=new nlobjSearchColumn('custrecord_ebiztask_sku',null,'group');
		ClosedTaskColumns[1]=new nlobjSearchColumn('custrecord_ebiztask_line_no',null,'group');
		ClosedTaskColumns[2] = new nlobjSearchColumn('custrecord_ebiztask_act_qty', null, 'sum');


		var CloedTaskSearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, ClosedTaskFilters, ClosedTaskColumns);

		if (CloedTaskSearchresults != null && CloedTaskSearchresults !=''&& CloedTaskSearchresults.length > 0)
		{
			ClosedtaskActQty = CloedTaskSearchresults[0].getValue('custrecord_ebiztask_act_qty', null, 'sum');

			//if(ClosedtaskActQty==null || ClosedtaskActQty =='' || isNAN(ClosedtaskActQty))
			if(ClosedtaskActQty==null || ClosedtaskActQty =='' || isNaN(ClosedtaskActQty)) // Case# 20149354
				ClosedtaskActQty=0;

			nlapiLogExecution('ERROR','ClosedtaskActQty',ClosedtaskActQty);
			TotalQty = TotalQty+ parseFloat(ClosedtaskActQty);
		}
		nlapiLogExecution('ERROR','TotalQty',TotalQty);

		return TotalQty;
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','Exception in GetReceived Qty',e);
	}
}

function updateScheduleScriptStatus(processname,curuserId,status,transactionrefno,trantype,notes)
{
	nlapiLogExecution('DEBUG','Into updateScheduleScriptStatus',status);

	var str = 'processname. = ' + processname + '<br>';
	str = str + 'curuserId. = ' + curuserId + '<br>';	
	str = str + 'transactionrefno. = ' + transactionrefno + '<br>';	
	str = str + 'trantype. = ' + trantype + '<br>';	
	str = str + 'notes. = ' + notes + '<br>';	

	nlapiLogExecution('Debug', 'Function Parameters', str);

	if(status=='Submitted')
	{
		var datetime = DateStamp() +" "+ getClockTime();   
		var schedulestatus = nlapiCreateRecord('customrecord_ebiz_schedulescripts_status');
		schedulestatus.setFieldValue('name',processname);
		schedulestatus.setFieldValue('custrecord_ebiz_processname',processname);
		schedulestatus.setFieldValue('custrecord_ebiz_processstatus',status);
		schedulestatus.setFieldValue('custrecord_ebiz_initiateddatetime',datetime);
		schedulestatus.setFieldValue('custrecord_ebiz_processtranrefno',transactionrefno);
		if(curuserId!=null && curuserId!='')
			schedulestatus.setFieldValue('custrecord_ebiz_processinitiatedby',curuserId);
		if(trantype!=null && trantype!='')
			schedulestatus.setFieldValue('custrecord_ebiz_process_trantype',trantype);
		var tranid = nlapiSubmitRecord(schedulestatus);
	}
	else if(status=='In Progress') 
	{
		var filter=new Array();
		if(curuserId!=null && curuserId!='')
			filter.push(new nlobjSearchFilter('custrecord_ebiz_processinitiatedby',null,'anyof',curuserId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processstatus',null,'is','Submitted'));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processname',null,'is',processname));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processtranrefno',null,'is',parseFloat(transactionrefno)));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_processname');
		column[1]=new nlobjSearchColumn('id').setSort();

		var searchresult=nlapiSearchRecord('customrecord_ebiz_schedulescripts_status',null,filter,column);
		if(searchresult!=null && searchresult!='')
		{
			var vid=searchresult[0].getId();
			nlapiLogExecution('DEBUG','vid in In Progress',vid);
			var fields = new Array();
			var values = new Array();

			fields[0] = 'custrecord_ebiz_processstatus';
			fields[1] = 'custrecord_ebiz_processbegindate';
			fields[2] = 'custrecord_ebiz_processbegintime';

			values[0] = status;
			values[1] = DateStamp();
			values[2] = TimeStamp();

			nlapiSubmitField('customrecord_ebiz_schedulescripts_status', vid, fields, values);
		}

	}
	else if(status=='Completed') 
	{
		var filter=new Array();
		if(curuserId!=null && curuserId!='')
			filter.push(new nlobjSearchFilter('custrecord_ebiz_processinitiatedby',null,'anyof',curuserId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processstatus',null,'is','In Progress'));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processname',null,'is',processname));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processtranrefno',null,'is',parseFloat(transactionrefno)));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_processname');
		column[1]=new nlobjSearchColumn('id').setSort();

		var searchresult=nlapiSearchRecord('customrecord_ebiz_schedulescripts_status',null,filter,column);
		if(searchresult!=null && searchresult!='')
		{
			var vid=searchresult[0].getId();
			nlapiLogExecution('DEBUG','vid in In Completed',vid);
			var fields = new Array();
			var values = new Array();

			fields[0] = 'custrecord_ebiz_processstatus';
			fields[1] = 'custrecord_ebiz_processenddate';
			fields[2] = 'custrecord_ebiz_processendtime';
			fields[3] = 'custrecord_ebiz_processnotes';

			values[0] = status;
			values[1] = DateStamp();
			values[2] = TimeStamp();
			values[3] = notes;

			nlapiSubmitField('customrecord_ebiz_schedulescripts_status', vid, fields, values);
		}
		else
		{
			var datetime = DateStamp() +" "+ getClockTime();   
			var schedulestatus = nlapiCreateRecord('customrecord_ebiz_schedulescripts_status');
			schedulestatus.setFieldValue('name',processname);
			schedulestatus.setFieldValue('custrecord_ebiz_processname',processname);
			schedulestatus.setFieldValue('custrecord_ebiz_processstatus',status);
			schedulestatus.setFieldValue('custrecord_ebiz_initiateddatetime',datetime);
			schedulestatus.setFieldValue('custrecord_ebiz_processtranrefno',transactionrefno);
			if(curuserId!=null && curuserId!='')
				schedulestatus.setFieldValue('custrecord_ebiz_processinitiatedby',curuserId);
			if(trantype!=null && trantype!='')
				schedulestatus.setFieldValue('custrecord_ebiz_process_trantype',trantype);
			var tranid = nlapiSubmitRecord(schedulestatus);
		}	
	}
}

function getClockTime(){
	var now    = new Date();
	var hour   = now.getHours();
	var minute = now.getMinutes();
	var second = now.getSeconds();
	var ap = "AM";
	if (hour   > 11) { ap = "PM";             }
	if (hour   > 12) { hour = hour - 12;      }
	if (hour   == 0) { hour = 12;             }
	if (hour   < 10) { hour   = "0" + hour;   }
	if (minute < 10) { minute = "0" + minute; }
	if (second < 10) { second = "0" + second; }
	var timeString = hour + ':' + minute + ':' + second + " " + ap;
	return timeString;
}

function fetchCaseQuantity(itemId, location, company)
{
	nlapiLogExecution('DEBUG','itemId',itemId);
	nlapiLogExecution('DEBUG','location in fetchCaseQuantity',location);
	var itemCaseQuantity = 0;

	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
//	case 20123368
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', 2));//2=Case
//	end
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 	
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true); 	

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null && itemSearchResults != '')
	{
		itemCaseQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('DEBUG','Item Case Quantity',itemCaseQuantity);
	}
	return itemCaseQuantity;
}

function getQuantityBreakdown(caseQty,orderQty)
{
	nlapiLogExecution('DEBUG','Into  getQuantityBreakdown',orderQty);
	var vnoofcases = 0;
	var vcasestr = ' Case';
	var veachstr = ' Each';

	if(parseFloat(caseQty)>0)
		vnoofcases = Math.floor(parseFloat(orderQty)/parseFloat(caseQty));

	vnoofeaches = parseFloat(orderQty) - (vnoofcases * caseQty);

	if(parseFloat(vnoofcases)>1)
		vcasestr = ' Cases';

	if(parseFloat(vnoofeaches)>1)
		veachstr = ' Eaches';

	var qtybreakup = vnoofcases.toString() +vcasestr+ " + "+vnoofeaches.toString()+veachstr;

	nlapiLogExecution('DEBUG','Out of  getQuantityBreakdown',qtybreakup);

	return qtybreakup;

}

function eBiz_RF_GetItemForItemIdWithArr(itemId,location){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemForItemIdWithArr', 'Start');

	var itemRecordArr = new Array();

	if(itemId != null && itemId != ""){
		var logMsg = 'ItemId = |' + itemId + '|';
		nlapiLogExecution('ERROR', 'Input Parameters', logMsg);
		nlapiLogExecution('ERROR', 'location', location);
		//alert('itemId'+itemId);
		//alert('location'+location);
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', itemId));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(location!=null && location!='')
			filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('externalid');
		columns[0].setSort(true);
		//alert('itemId11'+itemId);
		var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);
		if(itemSearchResults != null && itemSearchResults.length > 0){
			for(var i=0;i<itemSearchResults.length;i++)
			{	
				var itemInternalId = itemSearchResults[i].getId();
				nlapiLogExecution('ERROR', 'Internal Id', itemInternalId);
				itemRecordArr.push(itemInternalId);
				//var itemcolumns = nlapiLookupField('item', itemInternalId, [ 'recordType']);
				//var itemType = itemcolumns.recordType;
				//alert('itemInternalId'+itemInternalId);
				//nlapiLogExecution('ERROR', 'itemType', itemType);

				//itemRecord = nlapiLoadRecord(itemType, itemInternalId);
			}
		}
		else
		{
			nlapiLogExecution('ERROR', 'Inside UPC', location);

			var invLocfilters = new Array();
			var invLocCol = new Array();
			invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemId));
			invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

			var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);
			if(invtRes != null && invtRes.length > 0){
				for(var i=0;i<invtRes.length;i++)
				{	
					var itemInternalId = invtRes[i].getId();
					nlapiLogExecution('ERROR', 'Internal Id', itemInternalId);
					itemRecordArr.push(itemInternalId);			
				}
			}
			else
			{
				nlapiLogExecution('Debug', 'inSide SKUALIAS',itemId);

				var skuAliasFilters = new Array();
				skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemId));

				if(location!=null && location!="")					
					skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_location', null, 'anyof', ['@NONE@',location]));

				var skuAliasCols = new Array();
				skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

				var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

				if(skuAliasResults != null)
				{
					var actItem=skuAliasResults[0].getValue('custrecord_ebiz_item');
					itemRecordArr.push(actItem);
				}
			}
		}
	}

	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemForItemId', 'End');
	return itemRecordArr;
}


function eBiz_RF_GetItemForItemIdWithArrNew(itemId,location){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemForItemIdWithArr', 'Start');

	var itemRecordArr = new Array();

	if(itemId != null && itemId != ""){
		var logMsg = 'ItemId = |' + itemId + '|';
		nlapiLogExecution('ERROR', 'Input Parameters', logMsg);
		nlapiLogExecution('ERROR', 'location', location);
		//alert('itemId'+itemId);
		//alert('location'+location);
		var filters = new Array();
		filters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemId));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(location!=null && location!='')
			filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));
		filters.push(new nlobjSearchFilter('type', null, 'anyof', ['InvtPart','Assembly','Kit']));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('itemid');
		columns[0].setSort(true);
		//alert('itemId11'+itemId);
		var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);
		if(itemSearchResults != null && itemSearchResults.length > 0){
			for(var i=0;i<itemSearchResults.length;i++)
			{	
				var itemInternalId = itemSearchResults[i].getId();
				nlapiLogExecution('ERROR', 'Internal Id', itemInternalId);
				itemRecordArr.push(itemInternalId);
				//var itemcolumns = nlapiLookupField('item', itemInternalId, [ 'recordType']);
				//var itemType = itemcolumns.recordType;
				//alert('itemInternalId'+itemInternalId);
				//nlapiLogExecution('ERROR', 'itemType', itemType);

				//itemRecord = nlapiLoadRecord(itemType, itemInternalId);
			}
		}
		else
		{
			nlapiLogExecution('ERROR', 'Inside UPC', location);

			var invLocfilters = new Array();
			var invLocCol = new Array();
			invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemId));
			invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

			var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);
			if(invtRes != null && invtRes.length > 0){
				for(var i=0;i<invtRes.length;i++)
				{	
					var itemInternalId = invtRes[i].getId();
					nlapiLogExecution('ERROR', 'Internal Id', itemInternalId);
					itemRecordArr.push(itemInternalId);			
				}
			}
			else
			{
				nlapiLogExecution('Debug', 'inSide SKUALIAS',itemId);

				var skuAliasFilters = new Array();
				skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemId));

				if(location!=null && location!="")					
					skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_location', null, 'anyof', ['@NONE@',location]));

				var skuAliasCols = new Array();
				skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

				var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

				if(skuAliasResults != null)
				{
					var actItem=skuAliasResults[0].getValue('custrecord_ebiz_item');
					itemRecordArr.push(actItem);
				}
			}
		}
	}

	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemForItemId', 'End');
	return itemRecordArr;
}
//Case# 201410279
function GetPutawayLocationRFandWO(Item, PackCode, ItemStatus, UOM, ItemCube, ItemType,poLocn,ItemInfoArr,putrulesearchresults,vItemLocId){
	var context = nlapiGetContext();
	nlapiLogExecution('Debug','Into GetPutawayLocationRFandWO',context.getRemainingUsage());
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod,PutMethodName,PutRuleId,PutBinLoc;
	var MergeFlag = 'F',Mixsku='F',DimentionCheck='F',EmptyBinLoc='F';
	var itemresults;

	if(ItemCube == null || ItemCube == '' || isNaN(ItemCube))
		ItemCube = 0;

	var ItemFamily;
	var ItemGroup;
	var ItemInfo1;
	var ItemInfo2;
	var ItemInfo3;
	var putVelocity;
	if(ItemInfoArr!= null && ItemInfoArr != '' )
	{
		nlapiLogExecution('Debug', 'ItemInfoArr.length', ItemInfoArr.length);
		for(var p=0;p<ItemInfoArr.length;p++)
		{
			if(ItemInfoArr[p].getId()==Item)
			{
				ItemFamily = ItemInfoArr[p].getValue('custitem_item_family');
				ItemGroup = ItemInfoArr[p].getValue('custitem_item_group');
				ItemInfo1 = ItemInfoArr[p].getValue('custitem_item_info_1');
				ItemInfo2 = ItemInfoArr[p].getValue('custitem_item_info_2');
				ItemInfo3 = ItemInfoArr[p].getValue('custitem_item_info_3');
				putVelocity=ItemInfoArr[p].getValue('custitem_ebizabcvelitem');
			}	
		}	
	}

	var str = 'ItemCube.' + ItemCube + '<br>';
	str = str + 'ItemType.' + ItemType + '<br>';
	str = str + 'poLocn.' + poLocn + '<br>';
	str = str + 'Item.' + Item + '<br>';
	str = str + 'ItemFamily.' + ItemFamily + '<br>';
	str = str + 'ItemGroup.' + ItemGroup + '<br>';
	str = str + 'ItemStatus.' + ItemStatus + '<br>';
	str = str + 'ItemInfo1.' + ItemInfo1 + '<br>';
	str = str + 'ItemInfo2.' + ItemInfo2 + '<br>';
	str = str + 'ItemInfo3.' + ItemInfo3 + '<br>';
	str = str + 'putVelocity.' + putVelocity + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;


	var filters3=new Array();
	filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters3.push(new nlobjSearchFilter('custrecord_ebiz_wo_flag', null, 'is', 'T'));

	if(ItemFamily != null && ItemFamily != '')
		filters3.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', [ItemFamily,'@NONE@']));
	if(ItemGroup != null && ItemGroup != '')
		filters3.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', [ItemGroup,'@NONE@']));
	if(ItemStatus != null && ItemStatus != '')
		filters3.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', [ItemStatus,'@NONE@']));
	if(ItemInfo1 != null && ItemInfo1 != '')
		filters3.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof', [ItemInfo1,'@NONE@']));
	if(ItemInfo2 != null && ItemInfo2 != '')
		filters3.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof', [ItemInfo2,'@NONE@']));
	if(ItemInfo3 != null && ItemInfo3 != '')
		filters3.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof', [ItemInfo3,'@NONE@']));
	if(Item != null && Item != '')
		filters3.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', [Item,'@NONE@']));
	if(putVelocity != null && putVelocity != '')
		filters3.push(new nlobjSearchFilter('custrecord_abcvelpickrule', null, 'anyof', [putVelocity,'@NONE@']));
	if(poLocn != null && poLocn != '')
		filters3.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', [poLocn,'@NONE@']));

	var columns3=new Array();
	columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
	columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
	columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
	columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
	columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
	columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
	columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
	columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
	columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
	columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
	columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns3[14] = new nlobjSearchColumn('formulanumeric');
	columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns3[14].setSort();



	var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);

	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenumberpickrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	columns[6] = new nlobjSearchColumn('custrecord_locationpickrule');
	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';
	var vPutRules=new Array();
	/*if(putrulesearchresults != null && putrulesearchresults != '' && putrulesearchresults.length>0)
	{
		nlapiLogExecution('Debug', 'putrulesearchresults.length', putrulesearchresults.length);
		for(var j = 0; j < putrulesearchresults.length; j++){
			nlapiLogExecution('Debug', 'putrulesearchresults[j]', putrulesearchresults[j]);
			if((putrulesearchresults[j].getValue('custrecord_skufamilypickrule')==null||putrulesearchresults[j].getValue('custrecord_skufamilypickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skufamilypickrule')!= null && putrulesearchresults[j].getValue('custrecord_skufamilypickrule') != '' && putrulesearchresults[j].getValue('custrecord_skufamilypickrule') ==ItemFamily))	
			{ 
				nlapiLogExecution('Debug', 'ItemFamily', putrulesearchresults[j].getValue('custrecord_skufamilypickrule'));

				if((putrulesearchresults[j].getValue('custrecord_skugrouppickrule')==null||putrulesearchresults[j].getValue('custrecord_skugrouppickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skugrouppickrule')!= null && putrulesearchresults[j].getValue('custrecord_skugrouppickrule') != '' && putrulesearchresults[j].getValue('custrecord_skugrouppickrule') ==ItemGroup))	
				{
					nlapiLogExecution('Debug', 'ItemGroup', putrulesearchresults[j].getValue('custrecord_skugrouppickrule'));

					if((putrulesearchresults[j].getValue('custrecord_skustatuspickrule')==null||putrulesearchresults[j].getValue('custrecord_skustatuspickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skustatuspickrule')!= null && putrulesearchresults[j].getValue('custrecord_skustatuspickrule') != '' && putrulesearchresults[j].getValue('custrecord_skustatuspickrule') ==ItemStatus))	
					{
						nlapiLogExecution('Debug', 'ItemStatus', putrulesearchresults[j].getValue('custrecord_skustatuspickrule'));

						if((putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule')==null||putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule')!= null && putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule') != '' && putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule') ==ItemInfo1))	
						{
							nlapiLogExecution('Debug', 'Info1', putrulesearchresults[j].getValue('custrecord_skuinfo1pickrule'));

							if((putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule')==null||putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule')!= null && putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule') != '' && putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule') ==ItemInfo2))	
							{
								nlapiLogExecution('Debug', 'Info2', putrulesearchresults[j].getValue('custrecord_skuinfo2pickrule'));

								if((putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule')==null||putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule')!= null && putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule') != '' && putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule') ==ItemInfo3))	
								{
									nlapiLogExecution('Debug', 'Info3',putrulesearchresults[j].getValue('custrecord_skuinfo3pickrule'));

									if((putrulesearchresults[j].getValue('custrecord_skupickrule')==null||putrulesearchresults[j].getValue('custrecord_skupickrule')=='')||(putrulesearchresults[j].getValue('custrecord_skupickrule')!= null && putrulesearchresults[j].getValue('custrecord_skupickrule') != '' && putrulesearchresults[j].getValue('custrecord_skupickrule') ==Item))	
									{
										nlapiLogExecution('Debug', 'ITEM',  putrulesearchresults[j].getValue('custrecord_skupickrule'));

										if((putrulesearchresults[j].getValue('custrecord_abcvelpickrule')==null||putrulesearchresults[j].getValue('custrecord_abcvelpickrule')=='')||(putrulesearchresults[j].getValue('custrecord_abcvelpickrule')!= null && putrulesearchresults[j].getValue('custrecord_abcvelpickrule') != '' && putrulesearchresults[j].getValue('custrecord_abcvelpickrule') ==putVelocity))	
										{
											nlapiLogExecution('Debug', 'ABC Vel', putrulesearchresults[j].getValue('custrecord_abcvelpickrule'));

											if((putrulesearchresults[j].getValue('custrecord_ebizsitepickput')==null||putrulesearchresults[j].getValue('custrecord_ebizsitepickput')=='')||(putrulesearchresults[j].getValue('custrecord_ebizsitepickput')!= null && putrulesearchresults[j].getValue('custrecord_ebizsitepickput') != '' && putrulesearchresults[j].getValue('custrecord_ebizsitepickput') ==poLocn))	
											{
												nlapiLogExecution('Debug', 'putrulesearchresults[j].getValue(custrecord_ebizsitepickput)', putrulesearchresults[j].getValue('custrecord_ebizsitepickput'));
												if((putrulesearchresults[j].getValue('custrecord_locationpickrule')==null||putrulesearchresults[j].getValue('custrecord_locationpickrule')=='')||(putrulesearchresults[j].getValue('custrecord_locationpickrule')!= null && putrulesearchresults[j].getValue('custrecord_locationpickrule') != ''))// && putrulesearchresults[j].getValue('custrecord_locationpickrule') ==poLocn))	
												{

													nlapiLogExecution('Debug', 'putrulesearchresults[j].getValue(custrecord_locationpickrule)', putrulesearchresults[j].getValue('custrecord_locationpickrule'));
													vPutRules.push(j);
													nlapiLogExecution('Debug', 'j', j);
													nlapiLogExecution('Debug', 'putrulesearchresults[j].getId', putrulesearchresults[j].getId());
												}	
											}
										}	
									}	
								}	
							}	
						}	
					}	
				}	
			}	
		}
	}*/
	var t1 = new Date();
	var t2 = new Date();
	nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (putrulesearchresults != null && putrulesearchresults != '') {
		nlapiLogExecution('Debug', 'rules count', putrulesearchresults.length);
		nlapiLogExecution('Debug', 'vPutRules', putrulesearchresults);

		var putzonearr = new Array();
		var zonesearchresults = new Array();
		var vlocgrouparr = new Array();

		for (var h = 0; h < putrulesearchresults.length; h++) {
			if(putrulesearchresults[h].getValue('custrecord_putawayzonepickrule')!=null 
					&& putrulesearchresults[h].getValue('custrecord_putawayzonepickrule')!='')
			{
				putzonearr.push(putrulesearchresults[h].getValue('custrecord_putawayzonepickrule'));
			}
		}

		if(putzonearr!=null && putzonearr!='' && putzonearr.length>0)
		{
			nlapiLogExecution('Debug', 'putzonearr', putzonearr);
			var columnszone = new Array();
			columnszone[0] = new nlobjSearchColumn('custrecord_zone_seq');
			columnszone[0].setSort();
			columnszone[1] = new nlobjSearchColumn('custrecord_locgroup_no');
			columnszone[2] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');

			var filterszone = new Array();
			filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', putzonearr));
			filterszone.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));

			zonesearchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnszone);
		}

		if(zonesearchresults!=null && zonesearchresults!='' && zonesearchresults.length>0){

			nlapiLogExecution('Debug', 'zonesearchresults length', zonesearchresults.length);

			for (var b = 0; b < zonesearchresults.length; b++) {
				vlocgrouparr.push(zonesearchresults[b].getValue('custrecord_locgroup_no'));				
			}

			nlapiLogExecution('Debug', 'vlocgrouparr', vlocgrouparr);

			var alllocResults = GetAllLocation(vlocgrouparr,ItemCube);


		}

		for (var s = 0; s < putrulesearchresults.length; s++) {

			var str = 'putrulesearchresults[s].' + putrulesearchresults[s] + '<br>';
			str = str + 'putrulesearchresults[s].getId().' + putrulesearchresults[s].getId() + '<br>';
			str = str + 'Put Rule.' + putrulesearchresults[s].getValue('custrecord_ruleidpickrule') + '<br>';
			str = str + 'Put Method.' + putrulesearchresults[s].getValue('custrecord_putawaymethod') + '<br>';
			str = str + 'Location Group.' + putrulesearchresults[s].getValue('custrecord_locationgrouppickrule') + '<br>';
			str = str + 'EmptyBinLoc.' + putrulesearchresults[s].getValue('custrecord_emptylocation','custrecord_putawaymethod') + '<br>';

			nlapiLogExecution('DEBUG', 'Put Rule Parameters', str);

			locgroupid = putrulesearchresults[s].getValue('custrecord_locationgrouppickrule');
			zoneid = putrulesearchresults[s].getValue('custrecord_putawayzonepickrule');
			PutMethod = putrulesearchresults[s].getValue('custrecord_putawaymethod');
			PutMethodName = putrulesearchresults[s].getText('custrecord_putawaymethod');
			PutRuleId = putrulesearchresults[s].getValue('custrecord_ruleidpickrule');
			PutBinLoc = putrulesearchresults[s].getValue('custrecord_locationpickrule');
			EmptyBinLoc = putrulesearchresults[s].getValue('custrecord_emptylocation','custrecord_putawaymethod');
			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = putrulesearchresults[s].getValue('custrecord_manual_location_generation');

			var mixarray =  new Array();
			if(PutMethod!=null && PutMethod!='')
				mixarray=GetMergeFlag(PutMethod);
			if(mixarray!=null && mixarray!='' && mixarray.length>0)
			{
				MergeFlag = mixarray[0][0];
				Mixsku = mixarray[0][1];
				DimentionCheck=mixarray[0][2];
			}

			var str = 'MergeFlag' + MergeFlag + '<br>';
			str = str + 'Mixsku.' + Mixsku + '<br>';

			nlapiLogExecution('DEBUG', 'MergeFlag', str);

			var invtlocResults;
			var openputwResults;

			try {
				var columnsinvt = new Array();
				columnsinvt[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
				columnsinvt[0].setSort();
				columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
				columnsinvt[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
				columnsinvt[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
				columnsinvt[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
				columnsinvt[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
				columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
				columnsinvt[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

				var filtersinvt = new Array();

				if(poLocn!=null && poLocn!='')
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [poLocn]));
				//filtersinvt.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', vlocgrouparr));
				filtersinvt.push(new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', vlocgrouparr));


				if(ItemCube == null || ItemCube == '' || isNaN(ItemCube))
					ItemCube = 0;

				filtersinvt.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));

				filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
				filtersinvt.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc',  'is','F'));

				if(Mixsku == 'F' && Item != null && Item != '')
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', Item));

				//case# 20149729 starts (below condition for Inventory move process)
				if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', vItemLocId));
				//case# 20149729 ends
				t1 = new Date();
				invtlocResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);				
				t2 = new Date();
				nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:createinv',getElapsedTimeDuration(t1, t2));

				if(invtlocResults!=null && invtlocResults!='')
					nlapiLogExecution('Debug', 'invtlocResults length', invtlocResults.length);
			} //try close.
			catch (Error) {
				nlapiLogExecution('Debug', 'Into Error', Error);
			}



			if (ManualLocationGeneration == 'F') {
				//if (locgroupid != 0 && locgroupid != null) {
				if ((locgroupid != 0 && locgroupid != null) || (PutBinLoc != "" && PutBinLoc != null)) {
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

					nlapiLogExecution('Debug', 'locgroupid', locgroupid);

					var filters = new Array();
					if(locgroupid != null && locgroupid != "")
						filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]));	
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					if(poLocn != null && poLocn != "")
						filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', [poLocn]));

					if(PutBinLoc != null && PutBinLoc != "")
						filters.push(new nlobjSearchFilter('internalid', null, 'is', PutBinLoc));

					//below condition for Inventory move process
					if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
						filters.push(new nlobjSearchFilter('internalid', null, 'noneof', vItemLocId));


					t1 = new Date();
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('Debug', 'nlapiSearchRecord:GetPutawayLocation:location',
							getElapsedTimeDuration(t1, t2));


					if (locResults != null) {

						nlapiLogExecution('Debug', 'locResults length', locResults.length);

						var vbinlocarr = new Array();
						for (var xx = 0; xx < locResults.length; xx++) {
							vbinlocarr.push(locResults[xx].getId());
						}

						if(EmptyBinLoc=='T')
						{
							var emptyloccheck =	binLocationChecknew(vbinlocarr,poLocn);
						}

						for (var u = 0; u < locResults.length; u++) {

							if(EmptyBinLoc=='T')
							{
								//The below changes done by Satish.N on 29OCT2013 
								//*****************************************************************//
								// Case 20124515# Start
								nlapiLogExecution('Debug', 'Inside empty location check');

								nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

								//for (var j3 = 0; j3 < locResults.length; j3++) {
								var LocFlag='N';
								var emptyBinLocationId = locResults[u].getId();
								nlapiLogExecution('Debug', 'emptyBinLocationId', emptyBinLocationId);

								for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
								{
									if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
									{
										//nlapiLogExecution('Debug', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
										LocFlag='Y';
										break;
									}
								}
								for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
								{
									if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
									{
										//nlapiLogExecution('Debug', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
										LocFlag='Y';
										break;
									}
								}

								nlapiLogExecution('Debug', 'LocFlag', LocFlag);
								nlapiLogExecution('Debug', 'Main Loop ', s);
								nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
								if(LocFlag == 'N'){										
									LocRemCube = locResults[u].getValue('custrecord_remainingcube');
									OBLocGroup = locResults[u].getValue('custrecord_outboundlocgroupid');
									PickSeqNo = locResults[u].getValue('custrecord_startingpickseqno');
									IBLocGroup = locResults[u].getValue('custrecord_inboundlocgroupid');
									PutSeqNo = locResults[u].getValue('custrecord_startingputseqno');

									var str = 'Location' + locResults[u].getValue('name') + '<br>';
									str = str + 'LocRemCube.' + LocRemCube + '<br>';
									str = str + 'ItemCube.' + ItemCube + '<br>';

									nlapiLogExecution('DEBUG', 'Bin Location Det', str);

									if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {
										RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
										location_found = true;
										Location = locResults[u].getValue('name');
										LocArry[0] = Location;
										LocArry[1] = RemCube;
										LocArry[2] = locResults[u].getId();
										LocArry[3] = OBLocGroup;
										LocArry[4] = PickSeqNo;
										LocArry[5] = IBLocGroup;
										LocArry[6] = PutSeqNo;
										LocArry[7] = LocRemCube;
										LocArry[8] = zoneid;
										LocArry[9] = PutMethodName;
										LocArry[10] = PutRuleId;

										return LocArry;
									}
								}
								//}
								//*****************************************************************//

							}//end
							else
							{
								Location = locResults[u].getValue('name');
								var locationIntId = locResults[u].getId();

								//Commented by satish.N on 04-JAN-2012 as the search results will give the same value.
								//LocRemCube = GeteLocCube(locationIntId);
								LocRemCube = locResults[u].getValue('custrecord_remainingcube');							
								OBLocGroup = locResults[u].getValue('custrecord_outboundinvlocgroupid');
								PickSeqNo  = locResults[u].getValue('custrecord_startingpickseqno');
								IBLocGroup = locResults[u].getValue('custrecord_inboundinvlocgroupid');
								PutSeqNo   = locResults[u].getValue('custrecord_startingputseqno');

								var str = 'Location' + Location + '<br>';
								str = str + 'LocRemCube.' + LocRemCube + '<br>';
								str = str + 'ItemCube.' + ItemCube + '<br>';
								str = str + 'locationIntId.' + locationIntId + '<br>';

								nlapiLogExecution('DEBUG', 'Bin Location Det 2', str);

								if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
									RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
									location_found = true;

									LocArry[0] = Location;
									LocArry[1] = RemCube;
									LocArry[2] = locationIntId;	
									LocArry[3] = OBLocGroup;
									LocArry[4] = PickSeqNo;
									LocArry[5] = IBLocGroup;
									LocArry[6] = PutSeqNo;
									LocArry[7] = LocRemCube;
									LocArry[8] = zoneid;
									LocArry[9] = PutMethodName;
									LocArry[10] = PutRuleId;

									return LocArry;
								}
							}
						}
					}
				}
				else {
					nlapiLogExecution('Debug','Remaining usage before fetching zone location groups ',context.getRemainingUsage());
					nlapiLogExecution('Debug', 'Fetching Location Group from Zone', zoneid);

					if (zoneid != null && zoneid != ""){

						var RemCube = 0;
						var LocRemCube = 0;

						if(zonesearchresults!=null && zonesearchresults!='' && zonesearchresults.length>0){

							for (var b1 = 0; b1 < zonesearchresults.length; b1++) {

								if(zoneid==zonesearchresults[b1].getValue('custrecordcustrecord_putzoneid'))
								{
									var vzonelocgroupno=zonesearchresults[b1].getValue('custrecord_locgroup_no');
									nlapiLogExecution('Debug', 'Fetched Location Group Id from Putaway Zone', vzonelocgroupno);

									if (MergeFlag == 'T') {
										nlapiLogExecution('Debug', 'Inside Merge ');

										if (invtlocResults != null && invtlocResults != '') {
											nlapiLogExecution('Debug', 'LocationResults', invtlocResults.length );
											for (var j1 = 0; j1 < invtlocResults.length; j1++) {

												var invtlocgroupid = invtlocResults[j1].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												var invtitem = invtlocResults[j1].getValue('custrecord_ebiz_inv_sku');

												if(invtlocgroupid == vzonelocgroupno)
												{
													nlapiLogExecution('Debug', 'Mixsku',Mixsku);
													nlapiLogExecution('Debug', 'invtitem',invtitem);
													nlapiLogExecution('Debug', 'Item',Item);

													if(Mixsku=='T' || (Mixsku!='T' && invtitem==Item) )
													{
														nlapiLogExecution('Debug', 'invtlocResults[j].getId()', invtlocResults[j1].getId());
														Location = invtlocResults[j1].getText('custrecord_ebiz_inv_binloc');
														var locationInternalId = invtlocResults[j1].getValue('custrecord_ebiz_inv_binloc');

														nlapiLogExecution('Debug', 'Location', Location );
														nlapiLogExecution('Debug', 'Location Id', locationInternalId);

														//LocRemCube = GeteLocCube(locationInternalId);
														LocRemCube = invtlocResults[j1].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
														OBLocGroup = invtlocResults[j1].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
														PickSeqNo = invtlocResults[j1].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
														IBLocGroup = invtlocResults[j1].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
														PutSeqNo = invtlocResults[j1].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

														nlapiLogExecution('Debug', 'Loc Rem Cube', LocRemCube);
														nlapiLogExecution('Debug', 'Item Cube', ItemCube);

														if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

															RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

															location_found = true;

															LocArry[0] = Location;
															LocArry[1] = RemCube;
															LocArry[2] = locationInternalId;
															LocArry[3] = OBLocGroup;
															LocArry[4] = PickSeqNo;
															LocArry[5] = IBLocGroup;
															LocArry[6] = PutSeqNo;
															LocArry[7] = LocRemCube;
															LocArry[8] = zoneid;
															LocArry[9] = PutMethodName;
															LocArry[10] = PutRuleId;

															nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
															return LocArry;
														}
													}
												}
											}
										}
										if (location_found == false) {

											try{
												nlapiLogExecution('Debug', 'Inside  consider open taks while merging');
												nlapiLogExecution('Debug','Remaining usage before considering open taks while merging',context.getRemainingUsage());
												nlapiLogExecution('Debug', 'vzonelocgroupno',vzonelocgroupno);
												var columnsputw = new Array();
												columnsputw[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
												columnsputw[0].setSort();
												columnsputw[1] = new nlobjSearchColumn('custrecord_actbeginloc');
												columnsputw[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
												columnsputw[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_actbeginloc');
												columnsputw[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
												columnsputw[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
												columnsputw[6] = new nlobjSearchColumn('custrecord_sku');
												columnsputw[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_actbeginloc');

												var filtersputw = new Array();

												if(poLocn!=null && poLocn!='')
													filtersputw.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));
												filtersputw.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
												filtersputw.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'anyof', vlocgrouparr));
												// open 2 -PUTW, 7-CYCC,9-MOVE,8-RPLN
												filtersputw.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2,7,9,8]));
												if(ItemCube!=null && ItemCube!='' && parseFloat(ItemCube)>0)
													filtersputw.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_actbeginloc', 'greaterthanorequalto', ItemCube));
												//case# 20127650 added filter to check actbinloc is active or not
												filtersputw.push(new nlobjSearchFilter('isinactive', 'custrecord_actbeginloc',  'is','F'));
												//case# 20149729 starts (below condition for Inventory move process)
												if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
													filtersputw.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', vItemLocId));
												//case# 20149729 ends
												if(Mixsku == 'F' && Item != null && Item != '')
													filtersputw.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', Item));
												openputwResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersputw, columnsputw);



												if (openputwResults != null && openputwResults != '') {
													nlapiLogExecution('Debug', 'openputwResults length', openputwResults.length );
													for (var j2 = 0; j2 < openputwResults.length; j2++) {

														var tasklocgroupid = openputwResults[j2].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
														var taskitem = openputwResults[j2].getValue('custrecord_sku');

														if(tasklocgroupid == vzonelocgroupno)
														{	
															nlapiLogExecution('Debug', 'Mixsku',Mixsku);
															nlapiLogExecution('Debug', 'taskitem',taskitem);
															nlapiLogExecution('Debug', 'Item',Item);

															if(Mixsku=='T' || (Mixsku!='T' && taskitem==Item) )
															{
																Location = openputwResults[j2].getText('custrecord_actbeginloc');
																var locationInternalId = openputwResults[j2].getValue('custrecord_actbeginloc');

																nlapiLogExecution('Debug', 'Location', Location );
																nlapiLogExecution('Debug', 'Location Id', locationInternalId);

																LocRemCube = openputwResults[j2].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
																OBLocGroup = openputwResults[j2].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
																PickSeqNo = openputwResults[j2].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
																IBLocGroup = openputwResults[j2].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
																PutSeqNo = openputwResults[j2].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

																nlapiLogExecution('Debug', 'Loc Rem Cube', LocRemCube);
																nlapiLogExecution('Debug', 'Item Cube', ItemCube);

																if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

																	RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

																	location_found = true;

																	LocArry[0] = Location;
																	LocArry[1] = RemCube;
																	LocArry[2] = locationInternalId;
																	LocArry[3] = OBLocGroup;
																	LocArry[4] = PickSeqNo;
																	LocArry[5] = IBLocGroup;
																	LocArry[6] = PutSeqNo;
																	LocArry[7] = LocRemCube;
																	LocArry[8] = zoneid;
																	LocArry[9] = PutMethodName;
																	LocArry[10] = PutRuleId;

																	nlapiLogExecution('Debug', 'after changes Location Cube', RemCube);
																	return LocArry;
																}
															}
														}
													}										
												}
											}
											catch(exps){
												nlapiLogExecution('Debug', 'included exps in opentasks', exps);
											}
										}
									}

									if (location_found == false) {

										//code modified by suman on 27/01/12
										//Instead of searching each and individual binloc in inventory and open task record it seems to have a goverence issue.
										//So I bought all binloc from open task and inventory records depending upon "locgr"Id and placing it in an array so that on comparing this array with the binlocation 
										//will decides weather it is empty or not.

										nlapiLogExecution('Debug', 'Location Not found for Merge Location for Location Group', vzonelocgroupno);
										//var locResults = GetLocation(vzonelocgroupno,ItemCube);

										var locResults = GetLocationNew(alllocResults,vzonelocgroupno,ItemCube);

										if (locResults != null && locResults != '') {

											var TotalListOfBinLoc=new Array();
											for ( var x = 0; x < locResults.length; x++) 
											{
												TotalListOfBinLoc[x]=locResults[x].getId();

											}
											var emptyloccheck =	binLocationChecknew(TotalListOfBinLoc,poLocn);
											nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

											nlapiLogExecution('Debug', 'locResults.length', locResults.length);
											for (var j3 = 0; j3 < locResults.length; j3++) {
												var LocFlag='N';
												var emptyBinLocationId = locResults[j3].getId();
												nlapiLogExecution('Debug', 'emptyBinLocationId', emptyBinLocationId);
//												var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//												nlapiLogExecution('Debug', 'emptyloccheck', emptyloccheck);

												for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
												{
//													nlapiLogExecution('Debug', 'emptyloccheck[0][InvBinLocCount]', emptyloccheck[0][InvBinLocCount]);

													if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
													{
														nlapiLogExecution('Debug', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
														LocFlag='Y';
														break;
													}
												}
												for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
												{
													if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
													{
														nlapiLogExecution('Debug', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
														LocFlag='Y';
														break;
													}
												}
//												end of code changed 27/01/12 
												nlapiLogExecution('Debug', 'LocFlag', LocFlag);
												nlapiLogExecution('Debug', 'Main Loop ', s);
												nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
												if(LocFlag == 'N'){
													//Changes done by Sarita (index i is changed to j).
													LocRemCube = locResults[j3].getValue('custrecord_remainingcube');
													OBLocGroup = locResults[j3].getValue('custrecord_outboundlocgroupid');
													PickSeqNo = locResults[j3].getValue('custrecord_startingpickseqno');
													IBLocGroup = locResults[j3].getValue('custrecord_inboundlocgroupid');
													PutSeqNo = locResults[j3].getValue('custrecord_startingputseqno');

													nlapiLogExecution('Debug', 'LocRemCube', LocRemCube);
													nlapiLogExecution('Debug', 'ItemCube', ItemCube);

													if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {
														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
														location_found = true;
														Location = locResults[j3].getValue('name');
														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locResults[j3].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;

														nlapiLogExecution('Debug', 'Location', Location);
														nlapiLogExecution('Debug', 'Location Cube', RemCube);
														return LocArry;
														break;
													}
												}
											}
										}
									}
								}
							}
						}
					}//Zone Id
				}//location group else
			}//Manual Location is not false
		}//vputrules loop
		nlapiLogExecution('Debug', 'Main Loop ', s);
		nlapiLogExecution('Debug','Remaining usage ',context.getRemainingUsage());
	}// Putrules not null
	else
	{
		nlapiLogExecution('Debug', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	return LocArry;
}// end
function createInventoryforWOReversal(binloc,lp,item,itemstatus,packcode,qty,uomlevel,lot,whloc,company,serialNo,expdt,WOname)
{
	nlapiLogExecution('DEBUG', 'Into createInventory');

	var str = 'binloc. = ' + binloc + '<br>';
	str = str + 'lp. = ' + lp + '<br>';	
	str = str + 'item. = ' + item + '<br>';
	str = str + 'itemstatus. = ' + itemstatus + '<br>';
	str = str + 'packcode. = ' + packcode + '<br>';
	str = str + 'qty. = ' + qty + '<br>';
	str = str + 'uomlevel. = ' + uomlevel + '<br>';
	str = str + 'lot. = ' + lot + '<br>';
	str = str + 'whloc. = ' + whloc + '<br>';
	str = str + 'company. = ' + company + '<br>';
	str = str + 'serialNo. = ' + serialNo + '<br>';
	str = str + 'WOname. = ' + WOname + '<br>';
	nlapiLogExecution('DEBUG', 'Function Parameters', str);
	var invtrecid='';

	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', binloc));
	filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']));

	if(itemstatus!=null && itemstatus!='')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemstatus));

	if(packcode!=null && packcode!='')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', packcode));							

	if(lot!=null && lot!='')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', lot));

	if(lp!=null && lp!='')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp));

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
	columnsinvt[0].setSort();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
	if(invtsearchresults!=null && invtsearchresults!='' && invtsearchresults.length>0)
	{
		nlapiLogExecution('DEBUG', 'Merging Inventory to the existing record...');

		var qoh=invtsearchresults[0].getValue('custrecord_ebiz_qoh');

		var newqoh = parseFloat(qoh)+parseFloat(qty);

		/*var invtcolumns = new Array();
		var invtvalues = new Array();

		invtcolumns[0] = 'custrecord_ebiz_qoh';
		invtvalues[0] = parseFloat(newqoh);	

		invtcolumns[1] = 'custrecord_ebiz_callinv';
		invtvalues[1] = 'N';	

		invtcolumns[2] = 'custrecord_ebiz_displayfield';
		invtvalues[2] = 'N';	

		nlapiSubmitField('customrecord_ebiznet_createinv', invtsearchresults[0].getId(),invtcolumns, invtvalues);*/

		var scount=1;
		LABL1: for(var j=0;j<scount;j++)
		{	
			nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', j);
			try
			{

				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[0].getId());
				qoh = transaction.getFieldValue('custrecord_ebiz_qoh');
				var newqoh = parseFloat(qoh)+parseFloat(qty);
				transaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(newqoh));
				transaction.setFieldValue('custrecord_ebiz_callinv', "N");						
				transaction.setFieldValue('custrecord_ebiz_displayfield', "N");
				invtrecid=nlapiSubmitRecord(transaction, false, true);
				nlapiLogExecution('DEBUG', 'Cyclecount generated for inv ref ',invtsearchresults[0].getId());
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 

				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				}				 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}




	}
	else
	{

		nlapiLogExecution('DEBUG', 'Creating Inventory record...');

		var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');	
		invtRec.setFieldValue('name', lp);
		invtRec.setFieldValue('custrecord_ebiz_inv_binloc', binloc);
		invtRec.setFieldValue('custrecord_ebiz_inv_lp', lp);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku', item);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
		if(packcode!=null && packcode!='')
			invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packcode);
		invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(qty).toFixed(4));
		invtRec.setFieldValue('custrecord_inv_ebizsku_no', item);
		invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(qty).toFixed(4));
		invtRec.setFieldValue('custrecord_invttasktype', 10); // Task Type - INVT
		invtRec.setFieldValue('custrecord_wms_inv_status_flag', 19); //Inventory Storage
		invtRec.setFieldValue('custrecord_ebiz_displayfield','N');
		invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
		invtRec.setFieldValue('custrecord_ebiz_inv_loc', whloc); 
		invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');   
		invtRec.setFieldValue('custrecord_ebiz_inv_note1', 'Created by WO Pick ReversalProcess for WO#'+WOname);   
		if(company!=null && company!='')
			invtRec.setFieldValue('custrecord_ebiz_inv_company', company);
		if(uomlevel!=null && uomlevel!='')
			invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);

		if(lot!=null && lot!='')
			invtRec.setFieldText('custrecord_ebiz_inv_lot', lot);

		var filter=new Array();
		if(item!=null&&item!="")
			filter[0]=new nlobjSearchFilter("internalid",null,"anyof",item);

		var column=new Array();
		column[0]=new nlobjSearchColumn("itemid");
		column[1]=new nlobjSearchColumn("description");
		var Itemdescription='';
		var getItem="";
		var searchresitem=nlapiSearchRecord("item",null,filter,column);
		if(searchresitem!=null&&searchresitem!="")
		{
//			getItem=searchresitem[0].getValue("itemid");
			Itemdescription=searchresitem[0].getValue("description");
		}
		nlapiLogExecution("ERROR","Itemdescription",Itemdescription);
		invtRec.setFieldValue('custrecord_ebiz_itemdesc',Itemdescription.substring(0, 50));

		if(lot!="" && lot!=null)
		{

			var filterspor = new Array();
			filterspor.push(new nlobjSearchFilter('name', null, 'is', lot));
			if(whloc!=null && whloc!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whloc));
			if(item!=null && item!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', item));

			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

			var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);

			if(receiptsearchresults==null || receiptsearchresults=='')

			{
				var filterspor1 = new Array();
				filterspor1.push(new nlobjSearchFilter('name', null, 'is', lot));
				//if(whLocation!=null && whLocation!="")
				//filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));
				if(item!=null && item!="")
					filterspor1.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', item));

				var column1=new Array();
				column1[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
				column1[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

				var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor1,column1);

			}
			if(receiptsearchresults!=null)
			{
				var getlotnoid= receiptsearchresults[0].getId();
				nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
				var expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
				var vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
				invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifodate);
				nlapiLogExecution('DEBUG', 'expdate', expdate);
				nlapiLogExecution('DEBUG', 'vfifodate', vfifodate);
			}

		}

		if(company!=null && company!='')
			invtRec.setFieldValue('custrecord_ebiz_inv_company', company);
		if(uomlevel!=null && uomlevel!='')
			invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);

		invtRec.setFieldValue('custrecord_ebiz_serialnumbers', serialNo);

		invtrecid = nlapiSubmitRecord(invtRec, false, true);
	}

	nlapiLogExecution('DEBUG', 'Out of createInventory');
	return invtrecid;
}

//case 201411046 
function CheckItemDimens(itemId, location,poItemPackcode)
{
	nlapiLogExecution('DEBUG', 'CheckItemDimens-itemId ',itemId);
	nlapiLogExecution('DEBUG', 'CheckItemDimens-location ',location);
	nlapiLogExecution('DEBUG', 'CheckItemDimens-poItemPackcode ',poItemPackcode);
	var itemSearchResults='';
	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//case 201410846
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));


	if(poItemPackcode!=null && poItemPackcode!='' && poItemPackcode!='null')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',poItemPackcode));

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 	
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true); 	

	itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);
	return itemSearchResults;
}

//Case# 201411044 starts

function WOFifovalueCheck(itemType, itemID, itemFamilyID, itemGroupID, lineNo, poID, lotNoId)
{
	var fifoval=null;
	nlapiLogExecution('Debug', 'Into WOFifovalueCheck in General Functions');
	nlapiLogExecution('Debug', 'Item Type',itemType);
	nlapiLogExecution('Debug', 'Item ID',itemID);
	nlapiLogExecution('Debug', 'Item Family',itemFamilyID);
	nlapiLogExecution('Debug', 'Item Group',itemGroupID);
	nlapiLogExecution('Debug', 'Line No',lineNo);
	nlapiLogExecution('Debug', 'PO ID',poID);
	nlapiLogExecution('Debug', 'Lot ID',lotNoId);

	var fifoItemCheck = nlapiLookupField(itemType, itemID, 'custitem_ebizfifopolicy');
	nlapiLogExecution('Debug', 'FIFO @ Item Level', fifoItemCheck);

	if ((fifoItemCheck == "" || fifoItemCheck == null)) 
	{
		// Get FIFO policy for Item Group
		if(itemGroupID!=null && itemGroupID!='')
			fifoItemCheck = nlapiLookupField('customrecord_ebiznet_sku_group', itemGroupID, 'custrecord_fifopolicyskugrp');//Item group.

		nlapiLogExecution('Debug', 'FIFO @ Item Group Level', fifoItemCheck);
		if (fifoItemCheck == null || fifoItemCheck == "")
		{

			if(itemFamilyID!=null && itemFamilyID!='')
				// Get FIFO policy for item family
				fifoItemCheck = nlapiLookupField('customrecord_ebiznet_sku_family', itemFamilyID, 'custrecord_fifopolicy');// Item group.

			nlapiLogExecution('Debug', 'FIFO @ Item Family Level', fifoItemCheck);
			if (fifoItemCheck != null && fifoItemCheck != "") // case# 201417021
			{
				fifoval = WOValidateCall(fifoItemCheck, lineNo, poID, lotNoId);
				nlapiLogExecution('Debug', 'Return fifoval ', fifoval);
			}
		}
		else 
		{
			fifoval = WOValidateCall(fifoItemCheck, lineNo, poID, lotNoId);
			nlapiLogExecution('Debug', 'Return fifoval ', fifoval);
		}
	}
	else 
	{
		fifoval = WOValidateCall(fifoItemCheck, lineNo, poID, lotNoId);
		nlapiLogExecution('Debug', 'Return fifoval ', fifoval);
	}
	return fifoval;
}



function WOValidateCall(fifoItemCheck, putLine, putPOId, lotNoId){
	var val;
	var expiryDateInLot='';
	nlapiLogExecution('Debug', 'ValidateCall', 'Started');
	nlapiLogExecution('Debug', 'lotNoId', lotNoId);

	var vdate = WO_Daily(putLine, putPOId, lotNoId);
	if (fifoItemCheck == '1') //Daily
		val = WO_Daily(putLine, putPOId, lotNoId);
	else if(fifoItemCheck == '2'){ // EXPIRATION

		if(lotNoId!=null && lotNoId!='')
		{
			// get expiry date for the lot no.
			expiryDateInLot = nlapiLookupField('customrecord_ebiznet_batch_entry', lotNoId, 'custrecord_ebizexpirydate');
		}
		val = expiryDateInLot;
		nlapiLogExecution('Debug', 'ValidateCall:expiryDate', expiryDateInLot);

	}
	else if(fifoItemCheck == '3' || fifoItemCheck == '5') // MONTHLY
		val = firstDateOfYear(putLine,putPOId,vdate, fifoItemCheck);
	else
		val = Weekly(putLine, putPOId, lotNoId, vdate);

	return val;
}




function WO_Daily(putLine, putPOId, lotNoId){
	nlapiLogExecution('Debug', 'WO_Daily', putLine);
	var day = "";

	day=DateStamp();

	nlapiLogExecution('Debug', 'day',day);
	return day;
}
//Case# 201411044 ends
//updating allocated qty if location exception perform while wo picking.
function UpdateAllocateQtyForWOPickreversal(binloc,lp,item,itemstatus,packcode,qty,uomlevel,lot,whloc,company,serialNo,expdt,WOname){

	try
	{
		nlapiLogExecution('DEBUG', 'into Updating Allocated quantity');

		var str = 'binloc. = ' + binloc + '<br>';
		str = str + 'lp. = ' + lp + '<br>';	
		str = str + 'item. = ' + item + '<br>';
		str = str + 'itemstatus. = ' + itemstatus + '<br>';
		str = str + 'packcode. = ' + packcode + '<br>';
		str = str + 'qty. = ' + qty + '<br>';
		str = str + 'uomlevel. = ' + uomlevel + '<br>';
		str = str + 'lot. = ' + lot + '<br>';
		str = str + 'whloc. = ' + whloc + '<br>';
		str = str + 'company. = ' + company + '<br>';
		str = str + 'serialNo. = ' + serialNo + '<br>';
		str = str + 'WOname. = ' + WOname + '<br>';
		nlapiLogExecution('DEBUG', 'Function Parameters', str);

		var Invtrecid='';
		var filtersinvt = new Array();

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', binloc));
		filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']));

		if(itemstatus!=null && itemstatus!='')
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemstatus));

		if(packcode!=null && packcode!='')
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', packcode));							

		if(lot!=null && lot!='')
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', lot));

		if(lp!=null && lp!='')
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp));

		var columnsinvt = new Array();
		columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh'); 
		columnsinvt[0].setSort();

		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
		if(invtsearchresults!=null && invtsearchresults!='' && invtsearchresults.length>0)
		{
			var scount=1;
			LABL1: for(var j=0;j<scount;j++)
			{	
				nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', j);
				try
				{

					var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[0].getId());
					var qoh = transaction.getFieldValue('custrecord_ebiz_qoh');
					var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
					if (isNaN(allocqty)) {
						allocqty = 0;
					}
					if (allocqty == "") {
						allocqty = 0;
					}
					if (parseFloat(allocqty) < 0) {
						allocqty = 0;
					}
					var newallocateqty = parseFloat(allocqty)+parseFloat(qty);
					transaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(newallocateqty));
					transaction.setFieldValue('custrecord_ebiz_callinv', "N");						
					transaction.setFieldValue('custrecord_ebiz_displayfield', "N");
					invtrecid=nlapiSubmitRecord(transaction, false, true);
					nlapiLogExecution('DEBUG', 'inv ref ',invtrecid);
				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 

					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					}				 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}

		}
		nlapiLogExecution('DEBUG', 'Out of into Updating Allocated quantity');
		return invtrecid;

	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'exception in updating allocated qty ',exp);
	}

}
//This function is used in post item receipt porcess
//updating the serial# status to 'P' for used serial numbers in IR posting,
function UpdateSerialNumbersStatus(SerialArray)
{
	nlapiLogExecution('ERROR','SerialArray',SerialArray);

	try
	{
		for( var z=0; z<SerialArray.length;z++)
		{
			var fields = new Array();
			var values = new Array();
			fields[0] = 'custrecord_serialstatus';
			values[0] = 'P';
			nlapiLogExecution('ERROR','SerialArray[z]',SerialArray[z]);
			nlapiSubmitField('customrecord_ebiznetserialentry', SerialArray[z], fields, values);

		}
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception in UpdateSerialNumbersStatus",exp);
	}
}
function GetMaxLPNoBulk(lpGenerationType, lpType,whsite,nooflps){
	var maxLP = 1;
	var maxLPPrefix = "";

	var maxlparr = new Array();

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpmax');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', lpGenerationType));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lpType]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', ['@NONE@', whsite]));

	var results = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);

	if(results != null){
		if(results.length > 1){
			alert('More records returned than expected');
			nlapiLogExecution('Debug', 'GetMaxLPNo:LP Max Query returned more than 1 row');
		}else{
			//Commented for optimistiv locking
			/*			// Incrementing the max LP number only if the search returns 1 row
			for (var t = 0; t < results.length; t++){
				if (results[0].getValue('custrecord_ebiznet_lprange_lpmax') != null) {
					maxLP = results[0].getValue('custrecord_ebiznet_lprange_lpmax');
					maxLPPrefix = results[0].getValue('custrecord_ebiznet_lprange_lpprefix'); 


				}

				if(maxLP==null||maxLP==''||isNaN(maxLP))
					maxLP=0;

				maxLP = parseFloat(maxLP) + 1;
				// update the new max LP in the custom record
				nlapiSubmitField('customrecord_ebiznet_lp_range', results[0].getId(),
						'custrecord_ebiznet_lprange_lpmax', parseInt(maxLP));

			}*/
			var scount=1;

			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var vLPRangeId=results[0].getId();
					var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', vLPRangeId);
					//if(transaction.getFieldValue('custrecord_ebiznet_lprange_lpmax') != null)
					//{	
					maxLP = transaction.getFieldValue('custrecord_ebiznet_lprange_lpmax');
					maxLPPrefix = transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix');
					//}

					nlapiLogExecution('Debug', 'maxLP ',maxLP);
					if(maxLP==null||maxLP==''||isNaN(maxLP))
						maxLP=0;
					maxLP = parseInt(maxLP) + 1;
					var vnewmaxlp = parseInt(maxLP) + parseInt(nooflps);
					transaction.setFieldValue('custrecord_ebiznet_lprange_lpmax', vnewmaxlp);

					nlapiSubmitRecord(transaction, true,true);
					nlapiLogExecution('Debug', 'LP range updated for ref# ',vLPRangeId);

				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					}					 
					nlapiLogExecution('Debug', 'Exception in Get Max LP : ', wmsE); 
					if(exCode=='CUSTOM_RECORD_COLLISION')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}
		}
	}
	// convert maxLP to string
	maxLP = maxLP.toString();
	nlapiLogExecution('Debug', 'GetMaxLPNo:New MaxLP', maxLP);
	//CASE# 20123445    - START
	if(maxLPPrefix == 'null' || maxLPPrefix == null || maxLPPrefix =='')
		maxLPPrefix='';
	//CASE# 20123445   - END

	maxlparr[0]=maxLPPrefix;
	maxlparr[1]=maxLP;

	return maxlparr;//maxLPPrefix + maxLP;
}

function GetCompanyDUNSnumber(company)
{
	var dunsfilters = new Array();
	var duns='';

	if(company!=null && company!='')
		dunsfilters.push(new nlobjSearchFilter('internalid', null, 'is', company));

	dunsfilters.push(new nlobjSearchFilter('custrecord_compduns', null, 'isnotempty'));
	dunsfilters.push(new nlobjSearchFilter('isinactive', null,'is', 'F'));

	var dunscolumns = new Array();
	dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

	var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
	if (dunssearchresults !=null && dunssearchresults !="") 
	{
		duns = dunssearchresults[0].getValue('custrecord_compduns');
		nlapiLogExecution('DEBUG', 'duns', duns);
	}
	return duns;
}

function GenerateUCCLabelCode(uompackflag,duns,lpMaxValue)
{

	nlapiLogExecution('DEBUG', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var label="",uom="",ResultText="";
	//added by mahesh

	try 
	{	

		nlapiLogExecution('DEBUG', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('DEBUG', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label=lpMaxValue;


		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('DEBUG', 'uom', uom);
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord finaltext', finaltext);

		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseInt(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseInt(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText=finaltext+CheckDigitValue.toString();
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	return ResultText;
}

function createshipmanifestbulk(trantype,sorec,opentaskordersearchresult,entitycolumns,salesordintrid,containersearchresults,
		containerlpno,solineno,smparent)
{
	nlapiLogExecution('Debug', 'Into createshipmanifestbulk', containerlpno);
	var freightterms ="";
	var otherrefnum="";
	var servicelevelvalue='';
	var  searchresults;
	var vCustShipCountry;
	var resaleno;
	var lineno=solineno;
	var islinelevelship="F";
	var vShippingRule='';

	var entity=sorec.getFieldValue('entity');
	var shiptocountry=sorec.getFieldValue('shipcountry');
	var shipmethod=sorec.getFieldText('shipmethod');

	var customerid
	if(entity != "" && entity != null)
	{
		customerid = entitycolumns.entityid;
		nlapiLogExecution('Debug', 'start of resaleno','start of resaleno');
		resaleno=entitycolumns.resalenumber;		
		vCustShipCountry=entitycolumns.shipcountry;
	}

	var zipvalue=sorec.getFieldValue('shipzip');
	var servicelevelvalue=sorec.getFieldText('shipmethod');
	var consignee=sorec.getFieldValue('shipaddressee');
	var shipcomplete=sorec.getFieldValue('shipcomplete');
	var termscondition=sorec.getFieldText('terms');	
	islinelevelship=sorec.getFieldValue('ismultishipto');
	nlapiLogExecution('Debug', 'islinelevelship',islinelevelship);	
	nlapiLogExecution('Debug', 'lineno',lineno);

	smparent.selectNewLineItem('recmachcustrecord_ebiz_sm_parent');	 
	smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_orderno', sorec.getFieldValue('tranid'));
	smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_custom1', resaleno);

	if(trantype=="salesorder")
	{		
		var contactName=sorec.getFieldValue('shipattention');
		var entity=sorec.getFieldText('entity');
		if(contactName!=null && contactName!='')
			contactName=contactName.replace(","," ");

		if(entity!=null && entity!='')
			entity=entity.replace(","," ");


		otherrefnum=sorec.getFieldValue('otherrefnum');	
		freightterms = sorec.getFieldText('custbody_nswmsfreightterms');
		var cashondelivery= sorec.getFieldValue('custbody_nswmscodflag');
		var address1=sorec.getFieldValue('shipaddr1');
		var address2=sorec.getFieldValue('shipaddr2');
		var zip=sorec.getFieldValue('shipzip');
		var servicelevel=sorec.getFieldText('shipmethod');

		if(address1!=null && address1!='')
			address1=address1.replace(","," ");

		if(address2!=null && address2!='')
			address2=address2.replace(","," ");

		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_custid', customerid);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_carrier', sorec.getFieldValue('custbody_salesorder_carrier'));
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_contactname', contactName);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_ordertype', sorec.getFieldText('custbody_nswmssoordertype'));
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_phone', sorec.getFieldValue('custbody_customer_phone'));
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_satflag',sorec.getFieldValue('custbody_nswmssosaturdaydelivery'));
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_email', sorec.getFieldValue('email'));
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_signature_req', sorec.getFieldValue('custbody_nswmssignaturerequired'));
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_residential_flag', sorec.getFieldValue('shipisresidential'));

		if(islinelevelship!='T')
		{
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_zip', zipvalue);
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_city', sorec.getFieldValue('shipcity'));
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_state', sorec.getFieldValue('shipstate'));
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_country', sorec.getFieldValue('shipcountry'));
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_addr1', sorec.getFieldValue('shipaddress1'));
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_addr2', address2);

			if(consignee!="" && consignee!=null)
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_consignee', consignee);
			else
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_consignee', entity);

		}

		if(lineno!=null && lineno!='')
		{
			var shipmethodLineLevel = sorec.getLineItemText('item','shipmethod',lineno);
		}

		var shiptotal="0.00";

		if((shipcomplete=="T")&&(cashondelivery=="T"))
		{
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_codflag', 'T');
			shiptotal=sorec.getFieldValue('subtotal');
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_codamount', shiptotal);
		}
		else
		{
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_codflag', 'F');
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_codamount', shiptotal);
		}			


		if(servicelevelvalue==null || servicelevelvalue=='')
		{	
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_servicelevel', shipmethodLineLevel);
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_carrier', shipmethodLineLevel);
		}
		else
		{
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_servicelevel', servicelevelvalue);
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_carrier', servicelevelvalue);
		}

		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_company', sorec.getFieldText('custbody_nswms_company'));
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_ref2', otherrefnum);
		//smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_actwght', 0.01);

	}

	smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_order', salesordintrid);
	smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_custom5', "S");
	smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_void', "N");


	if(freightterms!="SENDER")
	{
		var thirdpartyacct=sorec.getFieldValue('custbody_ebiz_thirdpartyacc');		
		if((thirdpartyacct!=null)&&(thirdpartyacct!=''))
		{
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_account', thirdpartyacct);
		}
	}

	var freightvalue="";
	if(freightterms=="SENDER")
	{
		freightvalue="SHP";
	}
	if(freightterms=="RECEIVER")
	{
		freightvalue="REC";
	}
	if(freightterms=="3RDPARTY")
	{
		freightvalue="TP";
	}

	smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_paymethod', freightvalue);

	if(islinelevelship=='T')
	{
		var SOitem = sorec.getLineItemCount('item'); 
		for (var k  = 1; k <= 1; k++) {
			var soshipinternalid = sorec.getLineItemValue('item','shipaddress',1);
		}

		var SOLength = sorec.getLineItemCount('iladdrbook'); 

		for (var j = 1; j <= SOLength; j++) {
			var Shipaddrinternalid= sorec.getLineItemValue('iladdrbook', 'iladdrinternalid',j);

			if (Shipaddrinternalid == soshipinternalid)
			{
				var Shipaddr= sorec.getLineItemValue('iladdrbook', 'iladdrshipaddr', j);
				var custaddr1= sorec.getLineItemValue('iladdrbook', 'iladdrshipaddr1', j);
				var custaddr2= sorec.getLineItemValue('iladdrbook', 'iladdrshipaddr2', j);
				var custaddresee =sorec.getLineItemValue('iladdrbook', 'iladdrshipaddressee', j);
				var custcity= sorec.getLineItemValue('iladdrbook', 'iladdrshipcity', j);
				var custcountry= sorec.getLineItemValue('iladdrbook', 'iladdrshipcountry', j);
				var custstate= sorec.getLineItemValue('iladdrbook', 'iladdrshipstate', j);
				var custzip= sorec.getLineItemValue('iladdrbook', 'iladdrshipzip', j);

				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_zip', custzip);
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_city', custcity);
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_state', custstate);
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_country', custcountry);
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_addr1', custaddr1);
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_addr2', custaddr2);
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_consignee', custaddresee);
			}
		}	
	}

	if(trantype=="transferorder")
	{
		var tolocation = sorec.getFieldValue('transferlocation');
		var record = nlapiLoadRecord('location', tolocation);

		var shipfromaddress1=record.getFieldValue('addr1');
		var shipfromaddress2=record.getFieldValue('addr2');
		var shipfromcity=record.getFieldValue('city');
		var shipfromstate=record.getFieldValue('state');
		var shipfromzipcode =record.getFieldValue('zip');
		var shipfromcompanyname=record.getFieldValue('addressee');
		var shipfromphone=record.getFieldValue('addrphone');
		var shipfromcountry =record.getFieldValue('country');
		var prefixshipmethod=record.getFieldValue('custrecord_preferred_ship_method');


		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_carrier', prefixshipmethod);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_city', shipfromcity);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_state', shipfromstate);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_country', shipfromcountry);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_addr1', shipfromaddress1);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_zip', shipfromzipcode);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_addr2', shipfromaddress2);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_phone', shipfromphone);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_custid', shipfromcompanyname);
		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_contactname', shipfromcompanyname);

	}


	nlapiLogExecution('Debug', 'inside opentask search results', opentaskordersearchresult);

	var oldcontainer="";
	for (l = 0; l < opentaskordersearchresult.length; l++) 
	{ 
		nlapiLogExecution('Debug', 'inside opentask', containerid);

		var custlenght="";	
		var custheight="";
		var custwidht="";

		var sku="";
		var ebizskuno="";
		var uomlevel="";
		var shiplpno="";

		var shiplpno=opentaskordersearchresult[l].getValue('custrecord_ship_lp_no');
		sku = opentaskordersearchresult[l].getText('custrecord_sku');
		ebizskuno = opentaskordersearchresult[l].getValue('custrecord_sku');
		uomlevel = opentaskordersearchresult[l].getValue('custrecord_uom_level');					
		var name= opentaskordersearchresult[l].getValue('name');	
		var site=opentaskordersearchresult[l].getValue('custrecord_wms_location');

		smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_location', site);

		var oldcontainer="";
		if(oldcontainer!=containerlpno){
			smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_ref3', name);
			if(vShippingRule=='BuildShip')
			{				
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_contlp', shiplpno);
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_ref5', shiplpno);
			}
			else
			{
				var bulkpickflag=opentaskordersearchresult[l].getValue('custrecord_bulk_pick_flag');
				if(bulkpickflag=="T")
				{
					var mastercontainerlp=opentaskordersearchresult[l].getValue('custrecord_mast_ebizlp_no');

					smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_contlp', containerlpno);
					smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_ref5', containerlpno);
					smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_ref4', mastercontainerlp);
				}
				else
				{
					var mastercontainerlp=opentaskordersearchresult[l].getValue('custrecord_mast_ebizlp_no');

					smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_contlp', containerlpno);
					smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_ref5', containerlpno);
					smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_ref4', mastercontainerlp);
				}
			}

			var containerid= opentaskordersearchresult[l].getValue('custrecord_container');
			var containername= opentaskordersearchresult[l].getText('custrecord_container');
			nlapiLogExecution('Debug', 'container id', containerid);
			nlapiLogExecution('Debug', 'container name', containername);


			if(containersearchresults != null)
			{
				var custlenght=containersearchresults [0].getValue('custrecord_ebizlength');
				var custwidht=containersearchresults [0].getValue('custrecord_ebizwidth');
				var custheight=containersearchresults [0].getValue('custrecord_ebizheight');	
				var custweight=containersearchresults [0].getValue('custrecord_ebizweight');	

				if(custweight == null || custweight == '' || parseFloat(custweight) == 0)
					custweight='0.0001';		

				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_length', parseFloat(custlenght).toFixed(4));
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_width', parseFloat(custwidht).toFixed(4));
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_height', parseFloat(custheight).toFixed(4));
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_pkgtype', 'SHIPASIS');
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_ref1', sku);
				smparent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_pkgwght', parseFloat(custweight).toFixed(4));
			} 


			oldcontainer = containerlpno;			

		}					
	}

	smparent.commitLineItem('recmachcustrecord_ebiz_sm_parent');

	nlapiLogExecution('Debug', 'Out of createshipmanifestbulk', containerlpno);
}

function getLotExpdate(vbatchno,itemid)
{
	try{
		nlapiLogExecution('ERROR', 'vbatchno', vbatchno);
		nlapiLogExecution('ERROR', 'itemid', itemid);
		var expdate='';
		var filterspor = new Array();
		filterspor.push(new nlobjSearchFilter('name', null, 'is', vbatchno));
		if(itemid!=null && itemid!="")
			filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
		column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

		var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
		if(receiptsearchresults!=null)
		{
			var getlotnoid= receiptsearchresults[0].getId();
			nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
			expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');

		}
		nlapiLogExecution('ERROR', 'expdate', expdate);
		return expdate;
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'expection in getLotExpdate', e);
	}

}
