/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_StockadjustmentType_CL.js,v $
 *     	   $Revision: 1.4.14.1 $
 *     	   $Date: 2013/11/20 23:28:13 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_StockadjustmentType_CL.js,v $
 * Revision 1.4.14.1  2013/11/20 23:28:13  skreddy
 * Case# 20125850
 * Lost and Found CR for MHP
 * issue fix
 *
 * Revision 1.4  2011/09/20 12:28:50  mbpragada
 * CASE201112/CR201113/LOG201121
 * Added New validations for stock adjustment types
 *
 * Revision 1.3  2011/09/19 12:59:03  mbpragada
 * CASE201112/CR201113/LOG201121
 * Added New validations for stock adjustment types
 *
 * Revision 1.2  2011/09/19 11:03:10  mbpragada
 * CASE201112/CR201113/LOG201121
 * Added New validations for stock adjustment types
 *
 * Revision 1.1  2011/09/19 05:59:44  mbpragada
 * CASE201112/CR201113/LOG201121
 * Added validations for stock adjustment types
 *
 *
 *****************************************************************************/
/**
 * This function is used to check the below mentioned 2 cases in stock adjustment type
 * 1. One site has unique number of  name fields. 
 * 2. One task type in one site has only one default flag
 */
function fnCheckStockAdjustmentEntry(type)
{

	var tasktype = nlapiGetFieldValue('custrecord_adjusttasktype');
	var glaccount = nlapiGetFieldValue('custrecord_ebiz_mapto_ns_glaccount');	 
	var location	= nlapiGetFieldValue('custrecord_ebiz_adjtype_location');
	var adjsttype	= nlapiGetFieldValue('custrecord_adjustment_type');
	var defaultflag	= nlapiGetFieldValue('custrecord_adjustdefaultflag');
	var name	= nlapiGetFieldValue('name');
	var id = nlapiGetFieldValue('id');
	nlapiLogExecution('ERROR', 'tasktype: ', tasktype);
	nlapiLogExecution('ERROR', 'glaccount: ', glaccount);
	nlapiLogExecution('ERROR', 'location: ', location);
	nlapiLogExecution('ERROR', 'id: ', id);
	nlapiLogExecution('ERROR', 'adjsttype: ', adjsttype);
	nlapiLogExecution('ERROR', 'name: ', name);
	nlapiLogExecution('ERROR', 'defaultflag: ', defaultflag);
	if(tasktype != "" && tasktype != null && glaccount != "" && glaccount != null && location != ""  && location != null)
	{
		var filters = new Array();		

		filters.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'anyof',[location]));		
		filters.push(new nlobjSearchFilter('name', null,'is',name));
		if (id != null && id != "") 
		{			
			filters.push(new nlobjSearchFilter('internalid', null, 'noneof', id));
		}		 
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');	

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filters,columns);		
		if(searchresults != null && searchresults !="")
		{
			if(searchresults.length > 0){
				alert("Stock Adjustment Type already exists.");
				return false;
			}
		}
		else
		{
			if(defaultflag=='T'){
				var filters1 = new Array();		
				nlapiLogExecution('ERROR', 'into else filters: ', 'sucess');
				filters1.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'anyof',[location]));
				filters1.push(new nlobjSearchFilter('custrecord_adjusttasktype', null,'anyof',[tasktype]));
				filters1.push(new nlobjSearchFilter('custrecord_adjustdefaultflag', null,'is','T'));
				if (id != null && id != "") 
				{
					filters1.push(new nlobjSearchFilter('internalid', null, 'noneof', id));
				}		 
				var columns1 = new Array();			 
				columns1[0] = new nlobjSearchColumn('custrecord_adjustdefaultflag');

				var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filters1,columns1);	
				if(searchresults1 != null && searchresults1 !="")
				{
					alert("Default Stock Adjustment Type for this task type is already exists.");
					return false;				

				}
				else
				{return true;}
			}
			else
			{
				return true;
			}

		}

	}
	else
	{
		return true;  
	}
}


function Onchange(type, name)
{
	
	if (name == 'custrecord_ebiz_adjtype_trantype') {
//alert ('into tansction type');
		var TransTypeValue = nlapiGetFieldValue('custrecord_ebiz_adjtype_trantype');
		//alert(TransTypeValue);

		if(TransTypeValue == "2")
		{
			//alert ('if Tasnscvalue is 2');
			nlapiDisableField("custrecord_ebiz_mapto_ns_glaccount",true);
			nlapiDisableField("custrecord_ebiz_adjtype_mapnslocation",false);
		}
		else
		{
			//alert ('if Tasnscvalue is 1');
			nlapiDisableField("custrecord_ebiz_mapto_ns_glaccount",false);
			nlapiDisableField("custrecord_ebiz_adjtype_mapnslocation",true);
		}

	}
}

