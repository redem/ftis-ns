/***************************************************************************
  		   eBizNET Solutions Inc               
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_OutboundReversal.js,v $
 *     	   $Revision: 1.1.2.5.4.4.4.4 $
 *     	   $Date: 2014/06/13 13:02:44 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_OutboundReversal.js,v $
 * Revision 1.1.2.5.4.4.4.4  2014/06/13 13:02:44  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.5.4.4.4.3  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.5.4.4.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.5.4.4.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.5.4.4  2012/12/17 23:00:13  kavitha
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal Process Issue fixes
 *
 * Revision 1.1.2.5.4.3  2012/12/06 07:16:22  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Update Stage Inventory.
 *
 * Revision 1.1.2.5.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.5.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.5  2012/07/12 14:20:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default Focus issues.
 *
 * Revision 1.1.2.4  2012/06/25 14:54:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.1.2.3  2012/06/25 11:32:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.1.2.2  2012/06/22 12:27:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.1.2.1  2012/06/21 10:17:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 *
 *****************************************************************************/

function OutboundReversal(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		
		
		var st1,st2,st3,st4;
		
		if( getLanguage == 'es_ES')
		{
			st1 = "INVERSI&#211;N DE ALEJAMIENTO";
			st2 = "ENTER / SCAN UCC LABEL";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			
			
			
		}
		else
		{
			st1 = "OUTBOUND REVERSAL";
			st2 = "ENTER/SCAN UCC LABEL";
			st3 = "SEND";
			st4 = "PREV";
			
			
		}
		
		var functionkeyHtml=getFunctionkeyScript('_rfpickingmenu'); 
		var html = "<html><head><title>"+ st1 +"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterucc').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfpickingmenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterucc' id='enterucc' type='text'/>";
		html = html + "				</td>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='F8' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterucc').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');
		
		var getLanguage =  request.getParameter('hdngetLanguage');
nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		
		
		var st5;
		
		if( getLanguage == 'es_ES')
		{
			st5 = "V&#193;LIDA LA ETIQUETA UCC.";
			
			
		}
		else
		{
			st5 = "INVALID UCC LABEL.";
			
			
		}
		var ucclabel = request.getParameter('enterucc');
		var optedEvent = request.getParameter('cmdPrevious');

		nlapiLogExecution('DEBUG', 'ucclabel',ucclabel);
		nlapiLogExecution('DEBUG', 'optedEvent',optedEvent);
		
		var UCCarray=new Array();
		UCCarray["custparam_language"] = getLanguage;
		UCCarray["custparam_screenno"] = 'UCC1';

		if (request.getParameter('cmdPrevious') == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalmenu', 'customdeploy_rf_outboundreversalmenu', false, UCCarray);
		}
		else{

			if(ucclabel!=null && ucclabel!='')
			{
				var containerlp='';
				var item='';
				var lprecid='';

				var uccFilters = new Array();
				var uccColumns = new Array();

				uccFilters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_sscc', null, 'is', ucclabel));
				uccFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				uccColumns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_masterlp');
				uccColumns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_item');

				var uccSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, uccFilters, uccColumns);
				if(uccSearchResults != null && uccSearchResults != '')
				{
					containerlp = uccSearchResults[0].getValue('custrecord_ebiz_lpmaster_masterlp');
					item = uccSearchResults[0].getValue('custrecord_ebiz_lpmaster_item');
					lprecid = uccSearchResults[0].getId();

					nlapiLogExecution('DEBUG', 'containerlp',containerlp);
					nlapiLogExecution('DEBUG', 'item',item);
					nlapiLogExecution('DEBUG', 'lprecid',lprecid);

					var taskFilters = new Array();
					var taskColumns = new Array();

					taskFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlp));
					// 3 - PICK
					taskFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); 
					// 7 - BUILD SHIP UNIT 	8 - PICK CONFIRMED	28 - PACK COMPLETE
					taskFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [7,8,28]));

					taskColumns[0] = new nlobjSearchColumn('custrecord_sku');
					taskColumns[1] = new nlobjSearchColumn('custrecord_packcode');
					taskColumns[2] = new nlobjSearchColumn('custrecord_act_qty');
					taskColumns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					taskColumns[4] = new nlobjSearchColumn('custrecord_line_no');
					taskColumns[5] = new nlobjSearchColumn('custrecord_wms_status_flag');
					taskColumns[6] = new nlobjSearchColumn('custrecord_ebiz_ship_lp_no');
					taskColumns[7] = new nlobjSearchColumn('custrecord_wms_location');
					taskColumns[8] = new nlobjSearchColumn('custrecord_comp_id');
					taskColumns[9] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
					taskColumns[10] = new nlobjSearchColumn('custrecord_invref_no');
					taskColumns[11] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
					taskColumns[12] = new nlobjSearchColumn('name');
					taskColumns[13] = new nlobjSearchColumn('custrecord_total_weight');
					taskColumns[14] = new nlobjSearchColumn('custrecord_totalcube');
					taskColumns[15] = new nlobjSearchColumn('custrecord_uom_level');
					taskColumns[16] = new nlobjSearchColumn('custrecord_actendloc');
					taskColumns[17] = new nlobjSearchColumn('custrecord_container_lp_no');
					
					var taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);

					if(taskSearchResults != null && taskSearchResults != '')
					{
						for ( var k = 0; k < taskSearchResults.length; k++) {

							var itemname=taskSearchResults[k].getText('custrecord_sku');
							nlapiLogExecution('DEBUG', 'itemname',itemname);
							
							if(item==itemname)
							{
								UCCarray["custparam_uccitem"] = item;
								UCCarray["custparam_uccebizitem"] = taskSearchResults[k].getValue('custrecord_sku');
								UCCarray["custparam_uccpackcode"] = taskSearchResults[k].getValue('custrecord_packcode');
								UCCarray["custparam_uccactqty"] = taskSearchResults[k].getValue('custrecord_act_qty');
								UCCarray["custparam_uccordno"] = taskSearchResults[k].getText('custrecord_ebiz_order_no');
								UCCarray["custparam_uccebizordno"] = taskSearchResults[k].getValue('custrecord_ebiz_order_no');
								UCCarray["custparam_uccordlineno"] = taskSearchResults[k].getValue('custrecord_line_no');
								UCCarray["custparam_uccwmsstatus"] = taskSearchResults[k].getValue('custrecord_wms_status_flag');
								UCCarray["custparam_uccshiplpno"] = taskSearchResults[k].getValue('custrecord_ebiz_ship_lp_no');
								UCCarray["custparam_ucclocation"] = taskSearchResults[k].getValue('custrecord_wms_location');
								UCCarray["custparam_ucccompany"] = taskSearchResults[k].getValue('custrecord_comp_id');
								UCCarray["custparam_uccnsrefno"] = taskSearchResults[k].getValue('custrecord_ebiz_nsconfirm_ref_no');
								UCCarray["custparam_uccinvrefno"] = taskSearchResults[k].getValue('custrecord_invref_no');
								UCCarray["custparam_uccfointrid"] = taskSearchResults[k].getValue('custrecord_ebiz_cntrl_no');
								UCCarray["custparam_uccname"] = taskSearchResults[k].getValue('name');
								UCCarray["custparam_ucctaskweight"] = taskSearchResults[k].getValue('custrecord_total_weight');
								UCCarray["custparam_ucctaskcube"] = taskSearchResults[k].getValue('custrecord_totalcube');
								UCCarray["custparam_uccuomlevel"] = taskSearchResults[k].getValue('custrecord_uom_level');
								UCCarray["custparam_ucctaskintrid"] = taskSearchResults[k].getId();
								UCCarray["custparam_ucclprecid"] = lprecid;
								UCCarray["custparam_uccendlocation"] = taskSearchResults[k].getText('custrecord_actendloc');
								UCCarray["custparam_ucccartonno"] = taskSearchResults[k].getValue('custrecord_container_lp_no');
								
								response.sendRedirect('SUITELET', 'customscript_rf_confirmreversal', 'customdeploy_rf_confirmreversal', false, UCCarray);	

							}
						}
					}
					else
					{
						UCCarray["custparam_error"] = st5;//"Invalid UCC Label";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);						
					}
				}
				else
				{					
					UCCarray["custparam_error"] = st5;//"Invalid UCC Label";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
				}

			}
			else
			{
				UCCarray["custparam_error"] = st5;//"Invalid UCC Label";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
			}			
		}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}