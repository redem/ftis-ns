/**
 * @author LN
 *@version
 *@date
 */
function QCValidateFieldChanged(type, name){
    if ((name == 'custrecord_qc_type') && (nlapiGetFieldText('custrecord_qc_type') == "Boolean")) {
        nlapiDisableField('custrecord_qc_value', true);
        nlapiDisableField('custrecord_min_value', true);
        nlapiDisableField('custrecord_max_value', true);
    }
    else 
        if ((name == 'custrecord_qc_type') && (nlapiGetFieldText('custrecord_qc_type') == "Value")) {
            nlapiDisableField('custrecord_qc_value', false);
            nlapiDisableField('custrecord_min_value', true);
            nlapiDisableField('custrecord_max_value', true);
        }
        else 
            if ((name == 'custrecord_qc_type') && (nlapiGetFieldText('custrecord_qc_type') == "Range")) {
                nlapiDisableField('custrecord_qc_value', true);
                nlapiDisableField('custrecord_min_value', false);
                nlapiDisableField('custrecord_max_value', false);
            }
            else {
                nlapiDisableField('custrecord_qc_value', false);
                nlapiDisableField('custrecord_min_value', false);
                nlapiDisableField('custrecord_max_value', false);
            }
}
