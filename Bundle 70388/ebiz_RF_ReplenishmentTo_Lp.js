/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_ReplenishmentTo_Lp.js,v $
 *     	   $Revision: 1.1.2.14.4.12.2.28.2.2 $
 *     	   $Date: 2015/11/17 10:27:49 $
 *     	   $Author: gkalla $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_ReplenishmentTo_Lp.js,v $
 * Revision 1.1.2.14.4.12.2.28.2.2  2015/11/17 10:27:49  gkalla
 * case# 201415703
 * Quantity is not updating for pick face quantity
 *
 * Revision 1.1.2.14.4.12.2.28.2.1  2015/11/12 15:03:31  skreddy
 * case 201415504
 * Boombah SB  issue fix
 *
 * Revision 1.1.2.14.4.12.2.28  2015/07/13 13:24:36  schepuri
 * case# 201413379
 *
 * Revision 1.1.2.14.4.12.2.27  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.14.4.12.2.26  2014/11/19 14:52:43  schepuri
 * case#  201411078,201411048
 *
 * Revision 1.1.2.14.4.12.2.25  2014/11/12 13:27:14  sponnaganti
 * Case# 201410853 and 201410855
 * TPP SB Issue fix
 *
 * Revision 1.1.2.14.4.12.2.24  2014/09/02 15:34:28  sponnaganti
 * case# 201410019
 * stnd bundle issue fix
 *
 * Revision 1.1.2.14.4.12.2.23  2014/06/13 09:30:04  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.14.4.12.2.22  2014/03/04 12:36:55  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127481
 *
 * Revision 1.1.2.14.4.12.2.21  2014/01/20 10:29:10  rrpulicherla
 * no message
 *
 * Revision 1.1.2.14.4.12.2.20  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.14.4.12.2.19  2013/12/13 14:00:53  schepuri
 * 20126105
 *
 * Revision 1.1.2.14.4.12.2.18  2013/11/20 20:45:48  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20125767
 * Replen Exceptions
 *
 * Revision 1.1.2.14.4.12.2.17  2013/11/15 13:02:16  rmukkera
 * no message
 *
 * Revision 1.1.2.14.4.12.2.16  2013/11/14 15:41:55  skreddy
 * Case# 20125719 & 20125658
 * Afosa SB issue fix
 *
 * Revision 1.1.2.14.4.12.2.15  2013/10/22 16:05:31  gkalla
 * Case# 20125192
 * Dynacraft RF Replen Confirm issue
 *
 * Revision 1.1.2.14.4.12.2.14  2013/09/03 15:58:08  rmukkera
 * Case# 20123992
 *
 * Revision 1.1.2.14.4.12.2.13  2013/08/27 16:34:09  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdchanges
 * case#20123755
 *
 * Revision 1.1.2.14  2012/08/31 07:10:31  spendyala
 * CASE201112/CR201113/LOG201121
 * RF Replenishment Issue resolved.
 *
 * Revision 1.1.2.13  2012/08/27 16:28:52  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * fiskreplenissues
 *
 * Revision 1.1.2.12  2012/08/26 15:41:17  spendyala
 * CASE201112/CR201113/LOG201121
 * Replenishment issue Inv is not decrementing from from LP
 *
 * Revision 1.1.2.11  2012/08/24 16:09:57  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Replen confirm
 *
 * Revision 1.1.2.10  2012/08/17 22:55:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *  RF Replen
 *
 * Revision 1.1.2.9  2012/08/09 07:05:31  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Round turn in scanning other Items.
 *
 * Revision 1.1.2.8  2012/08/06 06:51:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Item Description.
 *
 * Revision 1.1.2.7  2012/07/12 14:47:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.1.2.6  2012/06/11 22:25:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.1.2.5  2012/06/04 11:47:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * HUBPEN - REPLEN ISSUE FIXES
 *
 * Revision 1.1.2.4  2012/05/07 12:01:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.1.2.3  2012/03/22 08:09:08  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Replenishment
 *
 * Revision 1.1.2.1  2012/02/07 12:40:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Replen Issue Fixes
 *
 * Revision 1.3  2012/01/17 13:53:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.2  2012/01/09 13:15:23  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.1  2012/01/05 17:24:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.6  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *****************************************************************************/
function Replenishmentto_LP(request, response){


	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	
	if (request.getMethod() == 'GET') {
		var getNo = request.getParameter('custparam_repno');
		var getBegLoc = request.getParameter('custparam_repbegloc');
		var getPickLoc = request.getParameter('custparam_reppickloc');
		var getPickLocNo = request.getParameter('custparam_reppicklocno');
		var getSku = request.getParameter('custparam_repsku');
		var getSkuNo = request.getParameter('custparam_repskuno');
		var getExpQty = request.getParameter('custparam_torepexpqty');
		var getid = request.getParameter('custparam_repid');
		var vClustno = request.getParameter('custparam_clustno');
		var whlocation = request.getParameter('custparam_whlocation');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var nextlocation=request.getParameter('custparam_nextlocation');
		var nextqty=request.getParameter('custparam_nextqty');
		var batchno=request.getParameter('custparam_batchno');
		var fromlpno=request.getParameter('custparam_fromlpno');
		var tolpno=request.getParameter('custparam_tolpno');
		var nextitem=request.getParameter('custparam_nextitem');
		var nextitemno=request.getParameter('custparam_nextitemno');
		var taskpriority = request.getParameter('custparam_entertaskpriority');
		var RecordCount=request.getParameter('custparam_noofrecords');

		var getreportNo=request.getParameter('custparam_enterrepno');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');
		var remainingreplenqty = request.getParameter('custparam_remainingreplenqty');
		var getLanguage = request.getParameter('custparam_language');
		var getNumber = request.getParameter('custparam_number');

		var beginLocId = request.getParameter('custparam_repbeglocId');
		
		var enterfromloc = request.getParameter('custparam_enterfromloc');
		nlapiLogExecution('ERROR', 'enterfromloc', enterfromloc);
		
		var html=gettoLPScan(getNo,getBegLoc,getSku,getExpQty,getPickLoc,getPickLocNo, getid,vClustno,whlocation,beginLocId,fromlpno,tolpno,batchno,nextqty,nextitem,nextitemno,getSkuNo,
				nextlocation,taskpriority,RecordCount,replenType,getitem,getitemid,getloc,getlocid,getreportNo,remainingreplenqty,getLanguage,getNumber,enterfromloc);        
		response.write(html);
	}
	else
	{
		try {
			var Reparray = new Array();

			var optedEvent = request.getParameter('cmdPrevious');

			var lp = request.getParameter('enterreplentolp');		
			var hdnBeginLocation = request.getParameter('hdnBeginLocation');
			var hdnrepLpno=request.getParameter('hdnfromLpno');	
			var hdnreptoLpno=request.getParameter('hdntoLpno');	
			var clusterNo = request.getParameter('hdnclustno');		
			var RecordCount = request.getParameter('hdnRecCount');			
			var ExpQty=request.getParameter('hdnExpQty');
			var ReportNo=request.getParameter('hdnNo');
			var recid=request.getParameter('hdnid');
			var vClustNo = request.getParameter('hdnclustno');
			var batchno = request.getParameter('hdnbatchno');
			var whlocation = request.getParameter('hdnwhlocation');
			var sku = request.getParameter('hdnSku');
			var skuno = request.getParameter('hdnskuno');
			var binlocid= request.getParameter('hdnPickLocNo');
			

			var getLanguage = request.getParameter('hdngetLanguage');
			Reparray["custparam_language"] = getLanguage;

			if(request.getParameter('hdngetNumber')!= null && request.getParameter('hdngetNumber') != "")
				getNumber = request.getParameter('hdngetNumber');
			else
				getNumber=0;

			var st0,st1,st2;
			if( getLanguage == 'es_ES' || getLanguage =='es_AR')
			{
				st0 = "LP NO V&#193;LIDA";
				st1 = "LP no coinciden";
				st2 = "LP ES NULO";


			}
			else
			{
				st0 = "INVALID LP";
				st1 = "LP DO NOT MATCH";
				st2 = "LP IS NULL ";


			}

			Reparray["custparam_repno"] = request.getParameter('hdnNo');
			Reparray["custparam_repbegloc"] = request.getParameter('hdnnextloc');
			Reparray["custparam_repsku"] = request.getParameter('hdnSku');
			//Reparray["custparam_repexpqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnExpQty');//case# 201410853
			Reparray["custparam_repid"] = request.getParameter('hdnid');
			Reparray["custparam_repskuno"] = request.getParameter('hdnskuno');
			Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
			Reparray["custparam_batchno"] = request.getParameter('hdnbatchno');
			Reparray["custparam_error"] = st0;//'INVALID LP';
			Reparray["custparam_screenno"] = 'R2';
			Reparray["custparam_clustno"] = clusterNo;	
			Reparray["custparam_number"] = request.getParameter('hdngetNumber');
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnactendlocationid');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnactendlocation');
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnPickLocNo');
			nlapiLogExecution('Debug', 'now hdnPickLocNo is ', request.getParameter('hdnPickLocNo'));
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
			Reparray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
			Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
			Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
			Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');			
			Reparray["custparam_screenno"] = 'R7';
			Reparray["custparam_clustno"] = vClustNo;
			Reparray["custparam_nextitem"] = request.getParameter('hdnnitem');
			Reparray["custparam_nextitemno"] = request.getParameter('hdnnextitemno');			
			Reparray["custparam_reppickloc"] = request.getParameter('hdnPickLoc');
			Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');

			Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
			Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
			Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
			Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
			Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
			Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
			Reparray["custparam_remainingreplenqty"] = request.getParameter('hdnremainingreplenqty');

			Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');
			nlapiLogExecution('ERROR','request.getParameter(hdnenterfromloc)', request.getParameter('hdnenterfromloc'));
			var getitemid = request.getParameter('hdngetitemid');
			var getlocid = request.getParameter('hdngetlocid');
			var taskpriority = request.getParameter('hdntaskpriority');
			var taskpriority = request.getParameter('hdntaskpriority');
			var getreportNo = request.getParameter('hdngetreportno');
			var replenType = request.getParameter('hdnReplenType');	

			var expQty= request.getParameter('hdnExpQty');
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');

			var beginLocationId = request.getParameter('hdnbeginLocId');
			nlapiLogExecution('Debug','beginLocationId', Reparray["custparam_repbeglocId"]);
			nlapiLogExecution('Debug','nextitem', Reparray["custparam_nextitem"]);
			
			if(lp==null || lp=='')
			{
			Reparray["custparam_error"] = st2;//'LP IS NULL';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			nlapiLogExecution('Debug', 'Location is null');
			}
			//lp=hdnreptoLpno;

			if (sessionobj!=context.getUser()) {
				try
				{
					if(sessionobj==null || sessionobj=='')
					{
						sessionobj=context.getUser();
						context.setSessionObject('session', sessionobj); 
					}
					if (optedEvent == 'F7') {
						nlapiLogExecution('Debug', 'REPLENISHMENT LOCATION F7 Pressed');						
						response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);

					}
					else {
						if (lp != '') {
							if (lp == hdnreptoLpno) {


								var searchresults =getReplenTasks(ReportNo,skuno,request.getParameter('hdnPickLocNo'),getitemid,getlocid,taskpriority,getreportNo,replenType);
								nlapiLogExecution('Debug', 'searchresults if lp is same', searchresults.length);
								for (var s = 0; s < searchresults.length; s++) 
								{

									var Rid =searchresults[s].getId();
									var expectedqty =searchresults[s].getValue('custrecord_expe_qty');
									var qty =searchresults[s].getValue('custrecord_act_qty');
									var batch=searchresults[s].getValue('custrecord_batch_no');
									var skuno=searchresults[s].getValue('custrecord_ebiz_sku_no');
									var beginlocation =searchresults[s].getValue('custrecord_actbeginloc');
									var itemStatus =searchresults[s].getValue('custrecord_sku_status');
									var endloc =searchresults[s].getValue('custrecord_actendloc');
									var remainingreplenqty = searchresults[s].getValue('custrecord_reversalqty');

									try {

										var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask',Rid);								
										transaction.setFieldValue('custrecord_actendloc', endloc);
										transaction.setFieldValue('custrecord_act_end_date', DateStamp());
										transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
										transaction.setFieldValue('custrecord_tasktype', 8);
										transaction.setFieldValue('custrecord_wms_status_flag', 19);
										transaction.setFieldValue('custrecord_act_qty', qty);
										var varsku=transaction.getFieldValue('custrecord_ebiz_sku_no');
										var ebizTaskno=transaction.getFieldValue('custrecord_ebiz_task_no');
										var invtLpno=transaction.getFieldValue('custrecord_lpno');
										var whLocation=transaction.getFieldValue('custrecord_wms_location');
										var binLocation=transaction.getFieldValue('custrecord_actendloc');
										var beginLocation = transaction.getFieldValue('custrecord_actbeginloc');
										var fromLPno=transaction.getFieldValue('custrecord_from_lp_no');
										var itemstatus=transaction.getFieldValue('custrecord_sku_status');
										var fromInvRef=transaction.getFieldValue('custrecord_invref_no');

										var accountNumber = getAccountNumber(request.getParameter('hdnwhlocation'));

										var stageLocation=GetStageLocation(varsku, "", whLocation, "", "BOTH");
										nlapiLogExecution('Debug', 'stageLocation', stageLocation);
										if(stageLocation == -1){

											if(fromInvRef == null || fromInvRef == '')
											{
												var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno,itemstatus,'BULK');

												nlapiLogExecution('Debug','bulkLocationInventoryResults', bulkLocationInventoryResults);
												if(bulkLocationInventoryResults != null && bulkLocationInventoryResults != '')
													//fromInvRef=bulkLocationInventoryResults[0].getid();
													fromInvRef=bulkLocationInventoryResults[0].getId();
											}

											if(fromInvRef != null && fromInvRef != '')
												updateBulkLocation(fromInvRef, varsku, beginlocation, qty, itemStatus,expectedqty,whLocation,remainingreplenqty);

											var invtresults=getRecord(varsku, binLocation,invtLpno,itemstatus,'PF');
											var vGetFifoDate="";
											if(fromInvRef!=null && fromInvRef != '')
												vGetFifoDate=GetFiFoDate(bulkLocationInventoryResults);
											else
												vGetFifoDate=DateStamp();
											updateOrCreatePickfaceInventory(invtresults, varsku, qty, invtLpno, binLocation, 
													itemStatus, accountNumber,batch,whlocation,skuno);
											nlapiSubmitRecord(transaction, true);
										}
										else
										{

											if(fromInvRef == null || fromInvRef == '')
											{
												var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno,itemstatus,'BULK');
												if(bulkLocationInventoryResults != null && bulkLocationInventoryResults != '')
													fromInvRef=bulkLocationInventoryResults[0].getId();
											}

											if(fromInvRef != null && fromInvRef != '')
												updateBulkLocation(fromInvRef, varsku, beginlocation, qty, itemStatus,expectedqty,whLocation,remainingreplenqty);

											var vGetFifoDate="";
											if(fromInvRef!=null && fromInvRef != '')
												//vGetFifoDate=GetFiFoDate(fromInvRef);
												vGetFifoDate=GetFiFoDate(bulkLocationInventoryResults);
											else
												vGetFifoDate=DateStamp();

												var invRefNo=transaction.getFieldValue('custrecord_invref_no');                           
											var result = nlapiSubmitRecord(transaction);


											var invtresults=getRecord(varsku, endloc,invtLpno,itemstatus,'PF');

											nlapiLogExecution('Debug','invtresults', invtresults);
											nlapiLogExecution('Debug','invRefNo1', invRefNo);

											if(invRefNo != null && invRefNo != "")
											{

												updateOrCreatePickfaceInventory(invtresults, varsku, qty, invtLpno, binLocation, 
														itemStatus, accountNumber,batch,whlocation,skuno,vGetFifoDate);
												nlapiLogExecution('Debug', 'beginLocation', beginLocation);
												var LocationType = nlapiLookupField('customrecord_ebiznet_location', beginLocation, ['custrecord_ebizlocationtype']);

												if (LocationType.custrecord_ebizlocationtype == '8' )
												{
													DeleteInventoryRecord(invRefNo);
												}
											}
										}

									}
									catch (e) {
										nlapiLogExecution('Debug', 'Updating OPEN TASK Record failed', e);
									}
								}
								var Endlocationsearchresults =getendReplenTasks(ReportNo,skuno,getitemid,getlocid,taskpriority,getreportNo,replenType);
								if(Endlocationsearchresults!=null && Endlocationsearchresults!='')
								{
									Reparray["custparam_repno"]= Endlocationsearchresults[0].getValue('name');
									Reparray["custparam_repbegloc"] = Endlocationsearchresults[0].getText('custrecord_actbeginloc');
									Reparray["custparam_repbeglocId"] = Endlocationsearchresults[0].getValue('custrecord_actbeginloc');
									Reparray["custparam_repsku"] = Endlocationsearchresults[0].getText('custrecord_sku');
									Reparray["custparam_repexpqty"] = Endlocationsearchresults[0].getValue('custrecord_expe_qty');
									Reparray["custparam_repskuno"] = Endlocationsearchresults[0].getValue('custrecord_ebiz_sku_no');
									Reparray["custparam_repid"] = Endlocationsearchresults[0].getId();
									Reparray["custparam_whlocation"] = Endlocationsearchresults[0].getValue('custrecord_wms_location');
									Reparray["custparam_reppicklocno"] = Endlocationsearchresults[0].getValue('custrecord_actendloc');
									Reparray["custparam_reppickloc"] = Endlocationsearchresults[0].getText('custrecord_actendloc');
									Reparray["custparam_fromlpno"] = Endlocationsearchresults[0].getValue('custrecord_from_lp_no');
									Reparray["custparam_tolpno"] = Endlocationsearchresults[0].getValue('custrecord_lpno');
									response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
								}
								else
								{
									var opentaskcount=0;									
									opentaskcount=getOpenTasksCount(ReportNo,getitemid,getlocid,taskpriority,getreportNo,replenType);
									nlapiLogExecution('Debug', 'opentaskcount', opentaskcount);
									if(opentaskcount >= 1)
									{										
										Reparray["custparam_noofrecords"]=opentaskcount;										
										response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
									}
									else
									{
										nlapiLogExecution('Debug', 'Test2', 'Test2');
										response.sendRedirect('SUITELET', 'customscript_rf_replen_cont', 'customdeploy_rf_replen_cont_di', false, Reparray);
									}
								}


							}
							else {

								nlapiLogExecution('Debug', 'User Scanned a diifferent LP');

								var isValidLP='F';

								isValidLP = validateLP(lp,skuno,binlocid,whlocation);

								if(isValidLP!='F')
								{
									createMasterLPRecord(lp,whlocation);

									var searchresults =getReplenTasks(ReportNo,skuno,request.getParameter('hdnPickLocNo'),getitemid,getlocid,taskpriority,getreportNo,replenType);
									nlapiLogExecution('Debug', 'searchresults', searchresults);
									for (var s = 0; searchresults!=null && s < searchresults.length; s++) 
									{

										var Rid =searchresults[s].getId();
										var expectedqty =searchresults[s].getValue('custrecord_expe_qty');
										var qty =searchresults[s].getValue('custrecord_act_qty');
										var batch=searchresults[s].getValue('custrecord_batch_no');
										var skuno=searchresults[s].getValue('custrecord_ebiz_sku_no');
										var beginlocation =searchresults[s].getValue('custrecord_actbeginloc');
										var itemStatus =searchresults[s].getValue('custrecord_sku_status');
										var endloc =searchresults[s].getValue('custrecord_actendloc');
										var remainingreplenqty = searchresults[s].getValue('custrecord_reversalqty');

										try {

											var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask',Rid);

											transaction.setFieldValue('custrecord_actendloc', endloc);
											transaction.setFieldValue('custrecord_act_end_date', DateStamp());
											transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
											transaction.setFieldValue('custrecord_tasktype', 8);
											transaction.setFieldValue('custrecord_wms_status_flag', 19);
											transaction.setFieldValue('custrecord_act_qty', qty);
											transaction.setFieldValue('custrecord_ebiz_new_lp', lp);

											var varsku=transaction.getFieldValue('custrecord_ebiz_sku_no');
											var ebizTaskno=transaction.getFieldValue('custrecord_ebiz_task_no');
											var invtLpno=transaction.getFieldValue('custrecord_lpno');
											var whLocation=transaction.getFieldValue('custrecord_wms_location');
											var binLocation=transaction.getFieldValue('custrecord_actendloc');
											var beginLocation = transaction.getFieldValue('custrecord_actbeginloc');
											var fromLPno=transaction.getFieldValue('custrecord_from_lp_no');
											var itemstatus=transaction.getFieldValue('custrecord_sku_status');
											var fromInvRef=transaction.getFieldValue('custrecord_invref_no');									
											var accountNumber = getAccountNumber(request.getParameter('hdnwhlocation'));

											var stageLocation=GetStageLocation(varsku, "", whLocation, "", "BOTH");
											if(stageLocation == -1){

												if(fromInvRef == null || fromInvRef == '')
												{
													var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno,itemstatus,'BULK');
													if(bulkLocationInventoryResults != null && bulkLocationInventoryResults != '')
														fromInvRef = bulkLocationInventoryResults[0].getId();
												}

												nlapiLogExecution('Debug','bulkLocationInventoryResults', bulkLocationInventoryResults);
												if(fromInvRef != null && fromInvRef != '')
													updateBulkLocation(fromInvRef, varsku, beginlocation, qty, itemStatus,expectedqty,whLocation,remainingreplenqty);


												var invtresults=getRecord(varsku, binLocation,invtLpno,itemstatus,'PF');

												nlapiLogExecution('Debug','invtresults', invtresults);

												var vGetFifoDate="";
												if(fromInvRef!=null && fromInvRef != '')
													vGetFifoDate=GetFiFoDate(bulkLocationInventoryResults);
												else
													vGetFifoDate=DateStamp();

												nlapiLogExecution('Debug','vGetFifoDate', vGetFifoDate);
												updateOrCreatePickfaceInventory(invtresults, varsku, qty, lp, binLocation, 
														itemStatus, accountNumber,batch,whlocation,skuno,vGetFifoDate);
												nlapiSubmitRecord(transaction, true);
											}
											else
											{
												nlapiLogExecution('Debug','in two stage', 'in two stage');

												if(fromInvRef == null || fromInvRef == '')
												{
													var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno,itemstatus,'BULK');

													nlapiLogExecution('Debug','bulkLocationInventoryResults', bulkLocationInventoryResults);
													if(bulkLocationInventoryResults != null && bulkLocationInventoryResults != '')
														fromInvRef=bulkLocationInventoryResults[0].getId();
												}
												if(fromInvRef != null && fromInvRef != '')
													updateBulkLocation(fromInvRef, varsku, beginlocation, qty, itemStatus,expectedqty,whLocation,remainingreplenqty);

												var vGetFifoDate="";
												if(fromInvRef!=null && fromInvRef != '')
												{
													//vGetFifoDate=GetFiFoDate(fromInvRef);
													vGetFifoDate=GetFiFoDate(bulkLocationInventoryResults);
													nlapiLogExecution('ERROR','vGetFifoDate', vGetFifoDate);
												}
												else
													vGetFifoDate=DateStamp();										

												var invRefNo=transaction.getFieldValue('custrecord_invref_no');                           
												var result = nlapiSubmitRecord(transaction);
												var invtresults=getRecord(varsku, endloc,lp,itemstatus,'PF');

												if(invRefNo != null && invRefNo != "")
												{											
													updateOrCreatePickfaceInventory(invtresults, varsku, qty, lp, binLocation, 
															itemStatus, accountNumber,batch,whlocation,skuno,vGetFifoDate);
													nlapiLogExecution('Debug', 'beginLocation', beginLocation);
													var LocationType = nlapiLookupField('customrecord_ebiznet_location', beginLocation, ['custrecord_ebizlocationtype']);
													nlapiLogExecution('Debug', 'LocationType', LocationType);
													if (LocationType.custrecord_ebizlocationtype == '8' )
													{
														DeleteInventoryRecord(invRefNo);
													}
												}
											}

										}
										catch (e) {
											nlapiLogExecution('Debug', 'Updating OPEN TASK Record failed', e);
										}
									}
									var Endlocationsearchresults =getendReplenTasks(ReportNo,skuno,getitemid,getlocid,taskpriority,getreportNo,replenType);
									if(Endlocationsearchresults!=null && Endlocationsearchresults!='')
									{
										Reparray["custparam_repno"]= Endlocationsearchresults[0].getValue('name');
										Reparray["custparam_repbegloc"] = Endlocationsearchresults[0].getText('custrecord_actbeginloc');
										Reparray["custparam_repbeglocId"] = Endlocationsearchresults[0].getValue('custrecord_actbeginloc');
										Reparray["custparam_repsku"] = Endlocationsearchresults[0].getText('custrecord_sku');
										Reparray["custparam_repexpqty"] = Endlocationsearchresults[0].getValue('custrecord_expe_qty');
										Reparray["custparam_repskuno"] = Endlocationsearchresults[0].getValue('custrecord_ebiz_sku_no');
										Reparray["custparam_repid"] = Endlocationsearchresults[0].getId();
										Reparray["custparam_whlocation"] = Endlocationsearchresults[0].getValue('custrecord_wms_location');
										Reparray["custparam_reppicklocno"] = Endlocationsearchresults[0].getValue('custrecord_actendloc');
										Reparray["custparam_reppickloc"] = Endlocationsearchresults[0].getText('custrecord_actendloc');
										Reparray["custparam_fromlpno"] = Endlocationsearchresults[0].getValue('custrecord_from_lp_no');
										Reparray["custparam_tolpno"] = Endlocationsearchresults[0].getValue('custrecord_lpno');
										response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
									}
									else
									{
										var opentaskcount=0;									
										opentaskcount=getOpenTasksCount(ReportNo,getitemid,getlocid,taskpriority,getreportNo,replenType);
										nlapiLogExecution('Debug', 'opentaskcount', opentaskcount);

										if(opentaskcount >= 1)
										{
											nlapiLogExecution('Debug', 'Test1', 'Test1');

											response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
										}
										else
										{
											nlapiLogExecution('Debug', 'Test2', 'Test2');
											response.sendRedirect('SUITELET', 'customscript_rf_replen_cont', 'customdeploy_rf_replen_cont_di', false, Reparray);
										}
									}

								}
								else
								{
									Reparray["custparam_error"] =st1; //'LP DO NOT MATCH';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
									nlapiLogExecution('Debug', 'Location not Matched');
								}
							}
						}
						else {
							Reparray["custparam_error"] = st2;//'LP IS NULL';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
							nlapiLogExecution('Debug', 'Location is null');
						}
					}
				}
				catch (e)  {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				} finally {					
					context.setSessionObject('session', null);
					nlapiLogExecution('DEBUG', 'finally','block');

				}
			}
			else
			{
				Reparray["custparam_screenno"] = 'R1';
				Reparray["custparam_error"] = 'LP ALREADY IN PROCESS';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			}
		}
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			nlapiLogExecution('Debug', 'Catch: LP not found');
		}
	}
}

function createMasterLPRecord(lp,whlocation)
{
	nlapiLogExecution('Debug', 'Into createMasterLPRecord');

	var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
	MastLP.setFieldValue('name', lp);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lptype', 1);
	if(whlocation!=null && whlocation!='')
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_site', whlocation);

	var retktoval = nlapiSubmitRecord(MastLP);

	nlapiLogExecution('Debug', 'Out of createMasterLPRecord');
}

function validateLP(lp,skuno,binlocid,whlocation)
{
	nlapiLogExecution('Debug', 'Into validateLP');

	var str = 'lp. = ' + lp + '<br>';
	str = str + 'skuno. = ' + skuno + '<br>';	
	str = str + 'binlocid. = ' + binlocid + '<br>';
	str = str + 'whlocation. = ' + whlocation + '<br>';

	nlapiLogExecution('Debug', 'Function Parameters', str);

	var isValidLp='F';
	var ebizlpno='-1';

	ebizlpno=geteBizLPNo(lp,whlocation);
	nlapiLogExecution('Debug', 'ebizlpno',ebizlpno);
	if(ebizlpno!=null && ebizlpno!='' && ebizlpno!='-1')
	{
		var isLPExist='F';

		isLPExist = isLPExistinStorage(lp,binlocid,whlocation);
		nlapiLogExecution('Debug', 'isLPExist',isLPExist);
		if(isLPExist=='T')
		{
			isValidLp='F';
		}
		else
		{
			isValidLp='T';
		}

	}
	else
	{
		var getEnteredLPPrefix = lp.substring(0, 3).toUpperCase();
		nlapiLogExecution('Debug', 'getEnteredLPPrefix',getEnteredLPPrefix);
		var LPReturnValue = ebiznet_LPRange_CL_withLPType(lp.replace(/\s+$/,""), '2','1',whlocation);//'2'UserDefiend,'1'PALT
		nlapiLogExecution('Debug', 'LPReturnValue',LPReturnValue);
		if(LPReturnValue == true)
		{
			isValidLp='T';
		}
		else
		{
			isValidLp='F';
		}
	}

	nlapiLogExecution('Debug', 'Out of validateLP');

	return isValidLp;
}

function isLPExistinStorage(lp,binlocid,whlocation)
{
	nlapiLogExecution('Debug', 'Into isLPExistinStorage');

	var isLPExist='F';
	var filtersso = new Array();

	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', [binlocid]));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp));

	if(whlocation!=null && whlocation!="")
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [whlocation]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var searchresultsInvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsInvt != null && searchresultsInvt != '' && searchresultsInvt.length>0)
		isLPExist='T';

	nlapiLogExecution('Debug', 'Out of isLPExistinStorage');

	return isLPExist;
}

function geteBizLPNo(lp,whlocation)
{
	nlapiLogExecution('Debug', 'Into geteBizLPNo');

	var ebizlpno='-1';
	var filters = new Array();
	var columns = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));

	if(whlocation!=null && whlocation!="")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', ['@NONE@',whlocation]));

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');

	var lpmasterrecordsearch= nlapiSearchRecord('customrecord_ebiznet_master_lp',null,filters,columns);
	if(lpmasterrecordsearch!=null && lpmasterrecordsearch!='' && lpmasterrecordsearch.length>0)
		ebizlpno=lpmasterrecordsearch[0].getId();

	nlapiLogExecution('Debug', 'Out of geteBizLPNo');

	return ebizlpno;		
}

function getReplenTasks(reportNo,sku,endlocation,getitemid,getlocid,taskpriority,getreportNo,replenType)
{

	var filters = new Array();

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}
	//if(getBinLocationId!=null && getBinLocationId!='')
	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isnotempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_batch_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_sku_status');
	columns[5] = new nlobjSearchColumn('custrecord_act_qty');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_reversalqty');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	nlapiLogExecution('Debug', 'searchresults', searchresults.length);
	return searchresults;
}


function getendReplenTasks(ReportNo,skuno,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('Debug', 'ReportNo', ReportNo);
	nlapiLogExecution('Debug', 'ReportNo', skuno);
	var openreccount=0;
	var filters = new Array();

	var filters = new Array();

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}

	//if(getBinLocationId!=null && getBinLocationId!='')
	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

	}

	//if(getPriority!=null && getPriority!='')
	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isnotempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_batch_no');

	columns[1].setSort(false);
	columns[2].setSort(false);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!=''&& searchresults.length>0)
		openreccount=searchresults.length;		


	return searchresults;

}

function getOpenTasksCount(ReportNo,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('Debug', 'ReportNo', ReportNo);
	var openreccount=0;

	var filters = new Array();

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}

	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	}

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_batch_no');

	columns[1].setSort(false);
	columns[2].setSort(false);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!=''&& searchresults.length>0)
		openreccount=searchresults.length;		


	return openreccount;

}

/**
 * 
 * @param bulkLocationInventoryResults
 * @param expQty
 * @param itemStatus
 */

function updateBulkLocation(bulkLocationInventoryRecId, itemID, beginLocationId, actQty, itemStatus,expectedqty,
		whLocation,remainingreplenqty)
{
	nlapiLogExecution('Debug', "Into updateBulkLocation");		
	var str = 'expectedqty.' + expectedqty + '<br>';
	str = str + 'actQty.' + actQty + '<br>';	
	str = str + 'remainingreplenqty.' + remainingreplenqty + '<br>';	

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	if(bulkLocationInventoryRecId != null)
	{	

		if (expectedqty == null || isNaN(expectedqty) || expectedqty == "") 
		{
			expectedqty = 0;
		}

		if (actQty == null || isNaN(actQty) || actQty == "") 
		{
			actQty = 0;
		}

		var discrepancyqty =0;
		var QOHQty=0;
		var calculatedQOHQty=0;
		var vnotes1="This is from replen qty Exception";
		var tasktype=11; //11 is the internalid value of tasktype "ADJT"; 
		var vAdjustType1=getAdjustmentType(whLocation);

		var scount=1;
		var newqoh=0;
		var vNewAllocQty=0;

		LABL1: for(var i=0;i<scount;i++)
		{	
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', bulkLocationInventoryRecId);
				var inventoryQty = transaction.getFieldValue('custrecord_ebiz_inv_qty');
				var inventoryAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				var inventoryQOHQty= transaction.getFieldValue('custrecord_ebiz_qoh');

				if (inventoryQty == null || isNaN(inventoryQty) || inventoryQty == "") 
				{
					inventoryQty = 0;
				}
				if (inventoryAllocQty == null || isNaN(inventoryAllocQty) || inventoryAllocQty == "") 
				{
					inventoryAllocQty = 0;
				}
				if (inventoryQOHQty == null || isNaN(inventoryQOHQty) || inventoryQOHQty == "") 
				{
					inventoryQOHQty = 0;
				}

				vNewAllocQty = parseInt(inventoryAllocQty)-parseInt(expectedqty);

				if(parseInt(expectedqty) != parseInt(actQty))
				{
					if(remainingreplenqty==null || remainingreplenqty=='null'  || remainingreplenqty=='' || isNaN(remainingreplenqty))
					{
						newqoh = parseInt(inventoryQOHQty) - parseInt(actQty);
					}
					else
					{
						newqoh = parseInt(vNewAllocQty) + parseInt(remainingreplenqty);
					}
				}
				else
				{
					newqoh = parseInt(inventoryQOHQty) - parseInt(actQty);
				}


				calculatedQOHQty= parseInt(inventoryQOHQty)- parseInt(actQty);

				if(remainingreplenqty!=null && remainingreplenqty!='null' && remainingreplenqty!='')
				{
					discrepancyqty = (parseInt(actQty)+parseInt(remainingreplenqty)+parseInt(vNewAllocQty))-parseInt(inventoryQOHQty);
				}


				var str = 'inventoryAllocQty.' + inventoryAllocQty + '<br>';
				str = str + 'vNewAllocQty.' + vNewAllocQty + '<br>';	
				str = str + 'inventoryQOHQty.' + inventoryQOHQty + '<br>';	
				str = str + 'newqoh. ' + newqoh + '<br>';					
				str = str + 'remainingreplenqty. ' + remainingreplenqty + '<br>';	
				str = str + 'discrepancyqty. ' + discrepancyqty + '<br>';	
				str = str + 'calculatedQOHQty. ' + calculatedQOHQty + '<br>';	

				nlapiLogExecution('Debug', 'Qty Details', str);

				if(parseInt(newqoh)<=0)
					newqoh=0;

				if(parseInt(vNewAllocQty)<=0)
					vNewAllocQty=0;

				transaction.setFieldValue('custrecord_ebiz_alloc_qty',vNewAllocQty);
				transaction.setFieldValue('custrecord_ebiz_qoh',newqoh);
				transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
				transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

				nlapiSubmitRecord(transaction);
				nlapiLogExecution('Debug', 'Replen Confirmed for inv ref ',bulkLocationInventoryRecId);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

		if(parseInt(expectedqty) != parseInt(actQty))
		{
			if(discrepancyqty != 0)
			{
				var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(itemID,itemStatus,whLocation,parseInt(discrepancyqty),
						vnotes1,tasktype,vAdjustType1,'');
				nlapiLogExecution('Debug', 'netsuiteadjustId', netsuiteadjustId);
			}
		}

		nlapiLogExecution('Debug', 'beginLocationId', beginLocationId);
		//	var retValue =  LocationCubeUpdation(itemID, beginLocationId, actQty, 'P');	//cse# 201411048

		if(parseInt(newqoh)<=0)
		{
			nlapiLogExecution('Debug', 'Deleting ZERO qty invetory record...',bulkLocationInventoryRecId);
			nlapiDeleteRecord('customrecord_ebiznet_createinv', bulkLocationInventoryRecId);
		}

	}

	nlapiLogExecution('Debug', "Out of updateBulkLocation");		
}

function LocationCubeUpdation(itemID, beginLocationId, expQty, optype)
{
	var arrDims = getSKUCubeAndWeight(itemID, 1);
	var itemCube = 0;
	var vTotalCubeValue = 0;
	//if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"])))
	if (arrDims[0] != "" && (!isNaN(arrDims[0])))
	{
		var uomqty = ((parseFloat(expQty))/(parseFloat(arrDims[1])));			
		itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));
		nlapiLogExecution('Debug', 'itemCube', itemCube);
	} 

	var vOldRemainingCube = GeteLocCube(beginLocationId);
	nlapiLogExecution('Debug', 'vOldRemainingCube', vOldRemainingCube);

	if(optype == "P")
		vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
	else
	{
		//case# 201410019 starts (bin location remaining cube updating with negative values)
		//vTotalCubeValue = parseFloat(itemCube) - parseFloat(vOldRemainingCube);
		vTotalCubeValue = parseFloat(vOldRemainingCube) - parseFloat(itemCube);
		//case# 201410019 ends
	}

	nlapiLogExecution('Debug', 'vOldLocationRemainingCube', vTotalCubeValue);

	var retValue =  UpdateLocCube(beginLocationId,vTotalCubeValue);
	nlapiLogExecution('Debug', 'LocCubeUpdation', retValue);

	return retValue; 
}


/**
 * 
 * @param invtresults
 */
function updateOrCreatePickfaceInventory(invtresults, varsku, expQty, invtLpno, binLocation, 
		itemStatus, accountNumber,batchno,whlocation,skuno,GetFifoDate){

	nlapiLogExecution('Debug', "Into updateOrCreatePickfaceInventory");	

	nlapiLogExecution('Debug', 'Time Stamp at the start of updateOrCreatePickfaceInventory',TimeStampinSec());

	nlapiLogExecution('Debug', "invtresults",invtresults);	

	if(invtresults != null)
	{
		nlapiLogExecution('Debug', "invtresults length",invtresults.length);	

		nlapiLogExecution('Debug', "Inventory Exists", invtresults[0].getId());	

		var scount=1;
		LABL1: for(var i=0;i<scount;i++)
		{	
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);

			try
			{
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtresults[0].getId());								
				var invtqty = transaction.getFieldValue('custrecord_ebiz_qoh');	
				var pfallocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				var fifioDtExist = transaction.getFieldValue('custrecord_ebiz_inv_fifo');// case# 201413379
				var lotnoExistforitem = transaction.getFieldValue('custrecord_ebiz_inv_lot');
				var vMainQty = transaction.getFieldValue('custrecord_ebiz_inv_qty');
				if (invtqty == null || isNaN(invtqty) || invtqty == "") {
					invtqty = 0;
				}

				if (pfallocqty == null || isNaN(pfallocqty) || pfallocqty == "") {
					pfallocqty = 0;
				}
				if (vMainQty == null || isNaN(vMainQty) || vMainQty == "") {
					vMainQty = 0;
				}
				invtqty = parseInt(invtqty) + parseInt(expQty);						
				//transaction.setFieldValue('custrecord_ebiz_inv_sku_status',itemStatus);	
				transaction.setFieldValue('custrecord_ebiz_qoh',invtqty);
				transaction.setFieldValue('custrecord_ebiz_inv_qty',vMainQty);
				transaction.setFieldValue('custrecord_ebiz_callinv','N');	
				transaction.setFieldValue('custrecord_ebiz_displayfield','N');	
				if((lotnoExistforitem == null || lotnoExistforitem == '')&& (fifioDtExist == null || fifioDtExist == ''))
					transaction.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
				nlapiLogExecution('Debug', "Quantity Assigned ", invtqty);						
				nlapiSubmitRecord(transaction);	
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
		updateInvRefforOpenPickTasks(varsku,binLocation,invtresults[0].getId(),pfallocqty,invtqty);
	}
	else
	{
		nlapiLogExecution('Debug', "Inventory Not Exists", varsku);					
		var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
		invrecord.setFieldValue('name', varsku+1);					
		invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
		invrecord.setFieldValue('custrecord_ebiz_inv_note1','Replenished');
		invrecord.setFieldValue('custrecord_ebiz_qoh', expQty);
		invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		
		invrecord.setFieldValue('custrecord_ebiz_inv_lp', invtLpno);		
		invrecord.setFieldValue('custrecord_ebiz_inv_binloc', binLocation);	
		invrecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
		invrecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
		invrecord.setFieldValue('custrecord_ebiz_inv_qty', expQty);
		if(accountNumber!=null && accountNumber!='')
			invrecord.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);

		invrecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
		invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
		invrecord.setFieldValue('custrecord_invttasktype', 8);
		invrecord.setFieldValue('custrecord_inv_ebizsku_no', skuno);
		nlapiLogExecution('Debug', "GetFifoDate", GetFifoDate);	
		/*if(GetFifoDate==null || GetFifoDate=='')
			GetFifoDate=DateStamp();
		invrecord.setFieldValue('custrecord_ebiz_inv_fifo', GetFifoDate);*/
	//invrecord.setFieldValue('custrecord_ebiz_expdate', GetFifoDate);
// case# 201411078
		var ItemTypeRec = nlapiLookupField('item', varsku, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);				
		var batchflag = ItemTypeRec.custitem_ebizbatchlot;
		var ItemType = ItemTypeRec.recordType;
		
		
		if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
		{

			//invrecord.setFieldValue('custrecord_ebiz_expdate', GetFifoDate);

			var expdate='';
			var filterspor = new Array();
			filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
			
			if(varsku!=null && varsku!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', varsku));

			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

			var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
			if(receiptsearchresults!=null)
			{
				getlotnoid= receiptsearchresults[0].getId();
				nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
				expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
				GetFifoDate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				nlapiLogExecution('DEBUG', 'expdate', expdate);
				nlapiLogExecution('DEBUG', 'GetFifoDate', GetFifoDate);
				if(expdate!=null && expdate!='')
					invrecord.setFieldValue('custrecord_ebiz_expdate', expdate);
				if(GetFifoDate!=null && GetFifoDate!='')
					invrecord.setFieldValue('custrecord_ebiz_inv_fifo', GetFifoDate);
			}
			else
			{
				GetFifoDate=DateStamp();
				expdate=DateStamp();
				if(expdate!=null && expdate!='')
					invrecord.setFieldValue('custrecord_ebiz_expdate', expdate);
				if(GetFifoDate!=null && GetFifoDate!='')
					invrecord.setFieldValue('custrecord_ebiz_inv_fifo', GetFifoDate);
			}
		
		
		}

		if(batchno!=null && batchno!='')
			invrecord.setFieldText('custrecord_ebiz_inv_lot', batchno);
		invrecord.setFieldValue('custrecord_ebiz_inv_loc', whlocation);

		var invtrefno = nlapiSubmitRecord(invrecord);

		updateInvRefforOpenPickTasks(varsku,binLocation,invtrefno,0,expQty);
	}

	//	var retValue =  LocationCubeUpdation(varsku, binLocation, expQty, 'M');// case#201411048

	nlapiLogExecution('Debug', 'Time Stamp at the end of updateOrCreatePickfaceInventory',TimeStampinSec());
	nlapiLogExecution('Debug', 'Out of updateOrCreatePickfaceInventory LocCubeUpdation', 'Success');
}

function updateInvRefforOpenPickTasks(itemno,binlocation,invrefno,allocqty,qoh)
{
	nlapiLogExecution('Debug', 'Into  updateInvRefforOpenPickTasks');

	var str = 'itemno. = ' + itemno + '<br>';
	str = str + 'binlocation. = ' + binlocation + '<br>';	
	str = str + 'invrefno. = ' + invrefno + '<br>';
	str = str + 'allocqty. = ' + allocqty + '<br>';
	str = str + 'qoh. = ' + qoh + '<br>';

	var totalpickqty = 0;

	nlapiLogExecution('Debug', 'Parameter Details', str);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', binlocation));
	filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', itemno));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));	 // TASK TYPE - PICK	
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	 // STATUS - PICKS GENERATED
	filters.push(new nlobjSearchFilter('custrecord_invref_no', null, 'isempty'));

	var columns = new Array();

	columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_skiptask'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		for (var s = 0; s < searchresults.length; s++) {

			var taskrecid = searchresults[s].getId();
			var pickqty = searchresults[s].getValue('custrecord_expe_qty');

			if(pickqty==null || pickqty=='' || isNaN(pickqty))
			{
				pickqty=0;
			}

			totalpickqty = parseFloat(totalpickqty)+parseFloat(pickqty);

			if(taskrecid!=null && taskrecid!='')
			{
				nlapiLogExecution('Debug', 'Updating PICK task with inventory reference number...',taskrecid);
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', taskrecid, 'custrecord_invref_no', invrefno);
			}
		}

		nlapiLogExecution('Debug', 'totalpickqty',totalpickqty);

		var totalallocqty = parseFloat(totalpickqty)+parseFloat(allocqty);

		nlapiLogExecution('Debug', 'totalallocqty',totalallocqty);

		if(totalallocqty==null || totalallocqty=='' || isNaN(totalallocqty))
		{
			totalallocqty=0;
		}

		nlapiLogExecution('Debug', 'Updating Inventory Record with allocate qty...',invrefno);
		//nlapiSubmitField('customrecord_ebiznet_createinv', invrefno, 'custrecord_ebiz_alloc_qty', totalallocqty);

		var scount=1;
		LABL1: for(var i=0;i<scount;i++)
		{	
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);			
				var inventoryAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');


				if (inventoryAllocQty == null || isNaN(inventoryAllocQty) || inventoryAllocQty == "") 
				{
					inventoryAllocQty = 0;
				}

				var totalallocqty = parseFloat(totalpickqty)+parseFloat(inventoryAllocQty);

				var str = 'totalallocqty.' + totalallocqty + '<br>';
				nlapiLogExecution('Debug', 'Qty Details', str);

				transaction.setFieldValue('custrecord_ebiz_alloc_qty',totalallocqty);			
				transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
				transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

				nlapiSubmitRecord(transaction);		
				nlapiLogExecution('Debug', 'Allocation Qty updated');
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
	}

	nlapiLogExecution('Debug', 'Out of  updateInvRefforOpenPickTasks');
}

/**
 * This is to update the status of the scanned location from 'R' to 'T'
 * @param recId
 */
function updateScannedLoc(recId)
{
	try {
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
		transaction.setFieldValue('custrecord_wms_status_flag', 24);
		var result = nlapiSubmitRecord(transaction);
	}
	catch (e) {
		nlapiLogExecution('Debug', 'Updating OPEN TASK Record failed', e);
	}
}

/**
 * 
 * @param itemid
 * @param location
 * @param invtLPNo
 * @returns
 */
function getRecord(itemid,location, invtLPNo,itemstatus,loctype)
{
	nlapiLogExecution('Debug', 'Into getRecord');
	var str = 'itemid. = ' + itemid + '<br>';
	str = str + 'location. = ' + location + '<br>';	
	str = str + 'invtLPNo. = ' + invtLPNo + '<br>';	
	str = str + 'itemstatus. = ' + itemstatus + '<br>';	
	str = str + 'loctype. = ' + loctype + '<br>';	
	nlapiLogExecution('Debug', 'Parameter Values', str);

	var filtersso = new Array();
	var socount=0;		

	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', [itemid]));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));
	filtersso.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
	if((loctype == 'BULK') && (invtLPNo!=null && invtLPNo!=''))
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', invtLPNo));
	if(itemstatus!=null && itemstatus!='')
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', itemstatus));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsLine != null)
	{
		nlapiLogExecution('Debug', 'Inventory count', searchresultsLine.length);
		socount=searchresultsLine.length; 
	} 
	else
	{ 
		nlapiLogExecution('Debug', 'inside else if results a', 'null');
	}       
	return searchresultsLine;
}

/**
 * 
 * @param recId
 * @param invrecord
 */
function updateStagTask(recId,invrecord)
{
	try {
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
		transaction.setFieldValue('custrecord_wms_status_flag', 24);		//20);
		transaction.setFieldValue('custrecord_invref_no', invrecord);
		var result = nlapiSubmitRecord(transaction);
	} 
	catch (e) {
		nlapiLogExecution('Debug', 'Updating OPEN TASK Record failed', e);
	}
}

/**
 * 
 * @param vitem
 * @param varsku
 * @param varqty
 * @param lp
 * @param whLocation
 * @param binLocation
 * @param itemStatus
 * @param accountNumber
 * @returns
 */
function createstagInvtRecord(vitem, varsku, varqty, lp, whLocation, binLocation, itemStatus, accountNumber)
{
	nlapiLogExecution('Debug','Inside InvtRecCreatedfor ','Function');
	var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	invrecord.setFieldValue('name', vitem+1);
	invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku);
	invrecord.setFieldValue('custrecord_ebiz_inv_note1','Replenished');
	invrecord.setFieldValue('custrecord_ebiz_qoh', varqty);
	invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');
	invrecord.setFieldValue('custrecord_ebiz_inv_lp', lp);
	invrecord.setFieldValue('custrecord_ebiz_inv_binloc', binLocation);
	invrecord.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
	invrecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
	invrecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
	invrecord.setFieldValue('custrecord_ebiz_inv_qty', varqty);
	//invrecord.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);

	invrecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
	invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
	invrecord.setFieldValue('custrecord_invttasktype', 8);

	var retval=nlapiSubmitRecord(invrecord);
	return retval;
}

/**
 * 
 */
function DeleteInventoryRecord(invRefNo)
{
	nlapiLogExecution('Debug','Inside DeleteInvtRecCreatedforCHKNTask ','Function');
	var Ifilters = new Array();
	Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
	Ifilters.push(new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [8]));
	Ifilters.push(new nlobjSearchFilter('internalid', null, 'is', invRefNo));

	var invtId = "";
	var invtType = "";
	var searchInventory= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (searchInventory){
		for (var s = 0; s < searchInventory.length; s++) {
			invtId = searchInventory[s].getId();
			invtType= searchInventory[s].getRecordType();
			nlapiDeleteRecord(searchInventory[s].getRecordType(),searchInventory[s].getId());
			nlapiLogExecution('Debug','Inventory record deleted ',invtId);
		}
	}
}

/**
 * 
 */
function UpdateInventory()
{
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
	transaction.setFieldValue('custrecord_wms_status_flag', 23);

	var result = nlapiSubmitRecord(transaction);
}

/**
 * 
 * Below is the search criteria to fetch the default item status from the item status master
 */

function getDefaultItemStatus(){

	var filtersitemstatus = new Array();
	filtersitemstatus[0] = new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T');

	var searchresultsitemstatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersitemstatus, null);
	if(searchresultsitemstatus != null && searchresultsitemstatus.length > 0)
	{
		var itemStatus = searchresultsitemstatus[0].getId();
		nlapiLogExecution('Debug', "searchresultsitemstatus[0].getId()", itemStatus);
	}
	return itemStatus;
}

/**
 * 
 * @param whLocation
 * @returns
 */
function getAccountNumber(whLocation){
	var filtersAccount = new Array();
	filtersAccount[0] = new nlobjSearchFilter('custrecord_location', null, 'is', whLocation);

	var columnsAccount = new Array();
	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);              

	if (accountSearchResults != null){
		var accountNumber = accountSearchResults[0].getValue('custrecord_accountno');                                
		nlapiLogExecution('Debug', 'Account #',accountNumber);
	}

	return accountNumber;
}

function gettoLPScan(getNo,getBegLoc,getSku,getExpQty,getPickLoc,getPickLocNo, getid,vClustno,whlocation,beginLocId,fromlpno,tolpno,batchno,nextqty,nextitem,nextitemno,getSkuNo,nextlocation,
		taskpriority,RecordCount,replenType,getitem,getitemid,getloc,getlocid,getreportNo,remainingreplenqty,getLanguage,getNumber,enterfromloc)
{
	if(getExpQty==null)
	{
		getExpQty=nextqty;
	}
	var st0,st1,st2,st3,st4;
	if( getLanguage == 'es_ES' || getLanguage =='es_AR')
	{
		st0 = "LP REPOSICI&#211;N";
		st1 = "LP";
		st2 = "ENTER / SCAN LP :";
		st3 = "ENVIAR";
		st4 = "ANTERIOR";
	}
	else
	{
		st0 = "REPLENISHMENT LP";
		st1 = "LP:";
		st2 = "ENTER/SCAN  LP: ";
		st3 = "SEND";
		st4 = "PREV";


	}


	var functionkeyHtml=getFunctionkeyScript('_rf_replenishmentTo_LP');
	var html = "<html><head><title>"+st0+"</title>";
	html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
	//Case# 20148749 Refresh Functionality starts
	html = html + "var version = navigator.appVersion;";
	html = html + "document.onkeydown = function (e) {";
	html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
	html = html + "if ((version.indexOf('MSIE') != -1)) { ";
	html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
	html = html + "else {if (keycode == 116)return false;}";
	html = html + "};";
	//Case# 20148749 Refresh Functionality ends
	html = html + "nextPage = new String(history.forward());";          
	html = html + "if (nextPage == 'undefined')";     
	html = html + "{}";     
	html = html + "else";     
	html = html + "{  location.href = window.history.forward();"; 
	html = html + "} ";

	//html = html + " document.getElementById('enterreplentolp').focus();";   

	html = html + "function stopRKey(evt) { ";
	html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
	html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
	html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
	html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
	html = html + "	  alert('System Processing, Please wait...');";
	html = html + "	  return false;}} ";
	html = html + "	} ";

	html = html + "	document.onkeypress = stopRKey; ";

	html = html + "</script>";
	html = html +functionkeyHtml;
	html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
	html = html + "<body>";
	html = html + "	<form name='_rf_replenishmentTo_LP' method='POST'>";
	html = html + "		<table>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st1+":  <label>" + tolpno + "</label></td>";
	html = html + "				<input type='hidden' name='hdnNo' value=" + getNo + ">";
	html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBegLoc + ">";
	html = html + "				<input type='hidden' name='hdnSku' value=" + getSku + ">";
	html = html + "				<input type='hidden' name='hdnExpQty' value=" + getExpQty + ">";
	html = html + "				<input type='hidden' name='hdnPickLoc' value=" + getPickLoc + ">";
	html = html + "				<input type='hidden' name='hdnPickLocNo' value=" + getPickLocNo + ">";
	html = html + "				<input type='hidden' name='hdnid' value=" + getid + ">";
	html = html + "				<input type='hidden' name='hdnclustno' value=" + vClustno + ">";
	html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
	html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
	html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
	html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
	html = html + "				<input type='hidden' name='hdnbatchno' value=" + batchno + ">";
	html = html + "				<input type='hidden' name='hdnnitem' value=" + nextitem + ">";
	html = html + "				<input type='hidden' name='hdnnextitemno' value=" + nextitemno + ">";
	html = html + "				<input type='hidden' name='hdnskuno' value=" + getSkuNo + ">";
	html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
	html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
	html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
	html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
	html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
	html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
	html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
	html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
	html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
	html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
	html = html + "				<input type='hidden' name='hdnremainingreplenqty' value=" + remainingreplenqty + ">";
	html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
	html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
	html = html + "				<input type='hidden' name='hdnenterfromloc' value=" + enterfromloc + ">";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>"+st2;
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input name='enterreplentolp' id='enterreplentolp' type='text' value='"+tolpno+"'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>"+st3+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
	html = html + "					"+st4+" <input name='cmdPrevious' type='submit' value='F7'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "		 </table>";
	html = html + "	</form>";
	//Case# 20148882 (added Focus Functionality for Textbox)
	html = html + "<script type='text/javascript'>document.getElementById('enterreplentolp').focus();</script>";
	html = html + "</body>";
	html = html + "</html>";
	return html;
}


function GetFiFoDate(SearchRec)
{
	try
	{
		var Fifodate='';
		if( SearchRec[0].getId() !=null && SearchRec[0].getId() !='')
		{
			 Fifodate=nlapiLookupField('customrecord_ebiznet_createinv',SearchRec[0].getId(),'custrecord_ebiz_inv_fifo');
		}
		return Fifodate;
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in GetFiFoDate',exp);	
	}
}
function getAdjustmentType(vWMSLocation1)
{
	var vAdjustType1='';
	var filters = new Array();		
	if(vWMSLocation1!=null && vWMSLocation1!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'anyof',[vWMSLocation1]));		
	filters.push(new nlobjSearchFilter('custrecord_adjusttasktype', null,'is','11'));//ADJT
	filters.push(new nlobjSearchFilter('custrecord_adjustdefaultflag', null,'is','T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');	

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filters,columns);		

	if(searchresults != null && searchresults !="")
	{
		if(searchresults.length > 0){
			vAdjustType1 =searchresults[0].getId();
		}
	}

	return vAdjustType1;
}
function  getIdInvokeNSInventoryAdjustment(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot)
{

	nlapiLogExecution('Debug', 'Location info::', loc);
	nlapiLogExecution('Debug', 'Task Type::', tasktype);
	nlapiLogExecution('Debug', 'Adjustment Type::', adjusttype);
	nlapiLogExecution('Debug', 'qty::', qty);

	var vAccountNo;	
	//Code for fetching account no from stockadjustment custom record	
	vAccountNo = getStockAdjustmentAccountNo(loc,tasktype,adjusttype);			
	nlapiLogExecution('Debug', 'Fetcht Account from stockadjustment', vAccountNo);		
	//upto to here stockadjustment

	var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
	nlapiLogExecution('Debug', 'vItemMapLocation', vItemMapLocation);	

	if(vAccountNo==null || vAccountNo=="")
	{		
		vAccountNo = getAccountNo(loc,vItemMapLocation);
		nlapiLogExecution('Debug', 'Fetcht Account from Inventory AccountNo', vAccountNo);
	}

	var vCost;
	var vAvgCost;

	var filters = new Array();          
	filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
	//Changed on 30/4/12by suman
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	//End of changes as on 30/4/12.
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('cost');
	columns[1] = new nlobjSearchColumn('averagecost');

	var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
	if (itemdetails !=null) 
	{
		vCost = itemdetails[0].getValue('cost');
		nlapiLogExecution('Debug', 'vCost', vCost);
		vAvgCost = itemdetails[0].getValue('averagecost');
		nlapiLogExecution('Debug', 'Average Cost', vAvgCost);	         
	}		

	var outAdj = nlapiCreateRecord('inventoryadjustment');			
	outAdj.setFieldValue('account', vAccountNo);
	outAdj.setFieldValue('memo', notes);
	outAdj.insertLineItem('inventory', 1);
	outAdj.setLineItemValue('inventory', 'item', 1, item);
	outAdj.setLineItemValue('inventory', 'location', 1, vItemMapLocation);	
	outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseInt(qty));
	if(vAvgCost != null &&  vAvgCost != "")	
	{
		nlapiLogExecution('Debug', 'into if cond vAvgCost', vAvgCost);
		outAdj.setLineItemValue('inventory', 'unitcost', 1, vAvgCost);
	}
	else
	{
		nlapiLogExecution('Debug', 'into else cond.unit cost', vCost);
		outAdj.setLineItemValue('inventory', 'unitcost', 1, vCost);
	}

	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('Debug', 'subs', subs);
	if(subs==true)
	{
		var vSubsidiaryVal=getSubsidiaryNew(loc);
		if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
		nlapiLogExecution('Debug', 'vSubsidiaryVal', vSubsidiaryVal);
	}

	if(lot!=null && lot!='')
	{
		if(parseInt(qty)<0)
			qty=parseInt(qty)*(-1);

		nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
		outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseInt(qty) + ")");
	}

	var internalid = nlapiSubmitRecord(outAdj, true, true);
	nlapiLogExecution('Debug', 'type argument', 'type is create');
	return internalid;
}
