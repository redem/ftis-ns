/***************************************************************************
	  	 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_task_management_SL.js,v $
 *     	   $Revision: 1.12.2.7.4.1.4.10.2.3 $
 *     	   $Date: 2015/12/03 15:49:59 $
 *     	   $Author: deepshikha $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *****************************************************************************/
/**
 * @param request
 * @param response
 */
function ebizTaskmanagement(request, response){

	if (request.getMethod() == 'GET') {
		//create the Suitelet form
		var form = nlapiCreateForm('Inbound Task-Management');

		//add the Task Type field and make it a hyperlink

		var tasktype = form.addField('custpage_tasktype', 'multiselect', 'Task Type');

		//new nlobjSearchFilter('mainline', null, 'is', 'T')
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_process_direction', null, 'is', [1]));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_task_desc');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, filters, columns);
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {

			tasktype.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name') + ' - ' + searchresults[i].getValue('custrecord_ebiz_task_desc'));
		}

		var taskstatus = form.addField('custpage_taskstatus', 'select', 'Task Status');
		taskstatus.addSelectOption('ALL', 'ALL');
		taskstatus.addSelectOption('C', 'COMPLETE');
		taskstatus.addSelectOption('I', 'INCOMPLETE');
		//taskstatus.addSelectOption('S', 'SUSPEND');


		var priority = form.addField('custpage_priority', 'select', 'Priority').setDisplayType('hidden');
		priority.addSelectOption('', '');
		priority.addSelectOption('HIGH', 'HIGH');
		priority.addSelectOption('LOW', 'LOW');

		//Get distinct po's and its IDs into dropdown  
		/*var orderpo = form.addField('custpage_orderpo', 'select', 'PO/TO');
		orderpo.addSelectOption('', '');
		
		try{
			orderpofillmethod(form,orderpo,-1);
		}
		catch(exp){
			nlapiLogExecution('ERROR','Exception in Main else',exp);
		}*/
		
		
		var OrderType = form.addField('custpage_ordertype', 'select', 'Order Type').setMandatory(true);
		OrderType.addSelectOption("","");
		OrderType.addSelectOption("purchaseorder","PO");
		OrderType.addSelectOption("transferorder","TO");
		OrderType.addSelectOption("returnauthorization","RMA");
		if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
		{
			OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
		}
		var selectPO = form.addField('custpage_orderpo', 'text', 'PO/TO/RMA#');
		

	
		
		

		/*var filterspo2 = new Array();
		filterspo2.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var searchresults2 = nlapiSearchRecord('transferorder', null, filterspo2, new nlobjSearchColumn('tranid').setSort());
*/
		
		/*try{
			ordertofillmethod(form,orderpo,-1);
		}
		catch(e){
			nlapiLogExecution('ERROR','Exception in second else',e);
		}*/
		
		
		
	/*	if (searchresults2 != null){
			for (var i = 0; i < searchresults2.length; i++){
				orderpo.addSelectOption(searchresults2[i].getId(), searchresults2[i].getValue('tranid'));
			}
		}*/
		
		
	/*	  if (searchresults2 != null){
	        	for (var i = 0; i < searchresults2.length; i++) {
	        		var res = form.getField('custpage_orderpo').getSelectOptions(searchresults2[i].getValue('tranid'), 'is');
	        		if (res != null) {
	        			nlapiLogExecution('ERROR', 'res.length', res.length + searchresults2[i].getValue('tranid'));
	        			if (res.length > 0) {
	        				continue;
	        			}
	        		}
	        		orderpo.addSelectOption(searchresults2[i].getId(), searchresults2[i].getValue('tranid'));
	        	}
	        }*/
		
		
		
		
		

		/*       
        var filterspo = new Array();
        filterspo[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'isnot', '');
        filterspo[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1, 2]);

        var colspo = new Array();
        colspo[0]=new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
        colspo[0].setSort();        
        colspo[1]=new nlobjSearchColumn('name');
        colspo[1].setSort();

        var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterspo, colspo);

        if (searchresults != null){
        	for (var i = 0; i < searchresults.length; i++) {
        		var res = form.getField('custpage_orderpo').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_cntrl_no'), 'is');
        		if (res != null) {
        			nlapiLogExecution('ERROR', 'res.length', res.length + searchresults[i].getValue('custrecord_ebiz_cntrl_no'));
        			if (res.length > 0) {
        				continue;
        			}
        		}

        		orderpo.addSelectOption(searchresults[i].getValue('custrecord_ebiz_cntrl_no'), searchresults[i].getValue('name'));
        	}
        }

		//Fetching Record from Closed task

		var filterspo1 = new Array();
        filterspo1[0] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'isnot', '');
        filterspo1[1] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [1, 2]);
        var colspo1 = new Array();
        colspo1[0]=new nlobjSearchColumn('custrecord_ebiztask_ebiz_cntrl_no');
        colspo1[0].setSort(true);        
        colspo1[1]=new nlobjSearchColumn('name');
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filterspo1, colspo1);

        if (searchresults != null){
        	for (var i = 0; i < searchresults.length; i++) {
        		var res = form.getField('custpage_orderpo').getSelectOptions(searchresults[i].getValue('custrecord_ebiztask_ebiz_cntrl_no'), 'is');
        		if (res != null) {
        			nlapiLogExecution('ERROR', 'res.length', res.length + searchresults[i].getValue('custrecord_ebiztask_ebiz_cntrl_no'));
        			if (res.length > 0) {
        				continue;
        			}
        		}
        		orderpo.addSelectOption(searchresults[i].getValue('custrecord_ebiztask_ebiz_cntrl_no'), searchresults[i].getValue('name'));
        	}
        }
		 */		
		var sku = form.addField('custpage_sku', 'select', 'ITEM', 'item');

		var lotbatch = form.addField('custpage_lotbatch', 'select', 'LOT#');
		lotbatch.addSelectOption('', '');

		var filterslot = new Array();
		filterslot.push(new nlobjSearchFilter('custrecord_batch_no', null, 'isnot', ''));
		filterslot.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1, 2]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslot, new nlobjSearchColumn('custrecord_batch_no'));
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			var res = form.getField('custpage_lotbatch').getSelectOptions(searchresults[i].getValue('custpage_lotbatch'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			lotbatch.addSelectOption(searchresults[i].getValue('custrecord_batch_no'), searchresults[i].getValue('custrecord_batch_no'));
		}

		var filterslot = new Array();
		filterslot.push(new nlobjSearchFilter('custrecord_ebiztask_batch_no', null, 'isnot', ''));
		filterslot.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [1, 2]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filterslot, new nlobjSearchColumn('custrecord_ebiztask_batch_no'));
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			if (searchresults[i].getValue('custrecord_ebiztask_batch_no') != null) {
				var res = form.getField('custpage_lotbatch').getSelectOptions(searchresults[i].getValue('custrecord_ebiztask_batch_no'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}
				lotbatch.addSelectOption(searchresults[i].getValue('custrecord_ebiztask_batch_no'), searchresults[i].getValue('custrecord_ebiztask_batch_no'));
			}
		}

		//var beginlocation = form.addField('custpage_beginlocation', 'select', 'Begin Location', 'customrecord_ebiznet_location');
		//var endlocation = form.addField('custpage_endlocation', 'select', 'End Location', 'customrecord_ebiznet_location');

		var beginlocation = form.addField('custpage_beginlocation', 'select', 'Begin Location');
		beginlocation.addSelectOption('', '');
		var filtersBeginLoc = new Array();
		//filtersBeginLoc.push(new nlobjSearchFilter('custrecord_ebiztask_batch_no', null, 'isnot', ''));
		filtersBeginLoc.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'isnotempty'));
		filtersBeginLoc.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1,2]));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
		
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersBeginLoc, columns);
		nlapiLogExecution('ERROR', 'searchresults Length:', searchresults.length);
		//var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersBeginLoc, new nlobjSearchColumn('custrecord_actbeginloc'));
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			if (searchresults[i].getValue('custrecord_actbeginloc') != null && searchresults[i].getValue('custrecord_actbeginloc') != '') {
				var res = form.getField('custpage_beginlocation').getSelectOptions(searchresults[i].getText('custrecord_actbeginloc'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}
				beginlocation.addSelectOption(searchresults[i].getText('custrecord_actbeginloc'), searchresults[i].getText('custrecord_actbeginloc'));
			}
		}


		var endlocation = form.addField('custpage_endlocation', 'select', 'End Location');
		endlocation.addSelectOption('', '');
		var filtersEndLoc = new Array();
		//filtersBeginLoc.push(new nlobjSearchFilter('custrecord_ebiztask_batch_no', null, 'isnot', ''));
		filtersEndLoc.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1, 2]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersEndLoc, new nlobjSearchColumn('custrecord_actendloc'));
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
		// case no 
			if (searchresults[i].getValue('custrecord_actendloc') != null && searchresults[i].getValue('custrecord_actendloc') != '') {
				var res = form.getField('custpage_endlocation').getSelectOptions(searchresults[i].getText('custrecord_actendloc'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}
				endlocation.addSelectOption(searchresults[i].getText('custrecord_actendloc'), searchresults[i].getText('custrecord_actendloc'));
			}
		}


		var lp = form.addField('custpage_lp', 'select', 'LP','customrecord_ebiznet_master_lp');
		//lp.addSelectOption('', '');
		/*
		var filterslp = new Array();
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'isnot', ''));
		filterslp.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1, 2]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslp, new nlobjSearchColumn('custrecord_lpno').setSort());

		if(searchresults != null){
			for (var i = 0; i < searchresults.length; i++) {
				var res = form.getField('custpage_lp').getSelectOptions(searchresults[i].getValue('custrecord_lpno'), 'is');
				if (res != null) {
					//nlapiLogExecution('ERROR', 'res.length1', res.length + searchresults[i].getValue('custrecord_lpno'));
					if (res.length > 0) {
						continue;
					}
				}
				lp.addSelectOption(searchresults[i].getValue('custrecord_lpno'), searchresults[i].getValue('custrecord_lpno'));
			}
		}

		var filterslp = new Array();
		filterslp.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'isnot', ''));
		filterslp.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [1, 2]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filterslp, new nlobjSearchColumn('custrecord_ebiztask_lpno'));

		if (searchresults != null){
			for (var i = 0; i < searchresults.length; i++) {
				var res = form.getField('custpage_lp').getSelectOptions(searchresults[i].getValue('custrecord_ebiztask_lpno'), 'is');
				if (res != null) {
					//nlapiLogExecution('ERROR', 'res.length2', res.length + searchresults[i].getValue('custrecord_ebiztask_lpno'));
					if (res.length > 0) {
						continue;
					}
				}
				lp.addSelectOption(searchresults[i].getValue('custrecord_ebiztask_lpno'), searchresults[i].getValue('custrecord_ebiztask_lpno'));
			}
		}
*/
		var employee = form.addField('custpage_employee', 'select', 'Employee', 'employee');

		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
		var todate = form.addField('custpage_todate', 'date', 'To Date');

		var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');
		//trailer.setLayoutType('startrow', 'none');
		var Location = form.addField('custpage_location', 'select', 'Location', 'location');
		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else {
		var form = nlapiCreateForm("Inbound Task Management");
		//form.addPageLink('crosslink', 'Go Back', 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=54&deploy=2');
		var linkURL = nlapiResolveURL('SUITELET', 'customscript_task_management', 'customdeploy_task_management_di');
     		   form.addPageLink('crosslink', 'Go Back',linkURL);
		var tasktype = form.addField('custpage_tasktype', 'multiselect', 'Task Type');
		
		
		var ordtype=request.getParameter('custpage_ordertype');
		
		var poname = request.getParameter('custpage_orderpo');
		
		nlapiLogExecution('ERROR', 'poname', poname);
		var poid;
		var poidarray;
		if(poname!=null && poname!='')
		{
			poid=GetPOInternalId(poname,ordtype);
                        nlapiLogExecution('ERROR', 'poid', poid);
		}
		else
		{
			poidarray=GetPOInternalIdArray(ordtype);
			nlapiLogExecution('ERROR', 'poidarray', poidarray);
		}
		
		
                 
		//new nlobjSearchFilter('mainline', null, 'is', 'T')
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_process_direction', null, 'is', [1]));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_task_desc');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, filters, columns);
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {

			tasktype.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name') + ' - ' + searchresults[i].getValue('custrecord_ebiz_task_desc'));
		}

		var taskstatus = form.addField('custpage_taskstatus', 'select', 'Task Status');
		taskstatus.addSelectOption('ALL', 'ALL');
		taskstatus.addSelectOption('C', 'COMPLETE');
		taskstatus.addSelectOption('I', 'INCOMPLETE');
		//taskstatus.addSelectOption('S', 'SUSPEND');



		var priority = form.addField('custpage_priority', 'select', 'Priority').setDisplayType('hidden');
		priority.addSelectOption('', '');
		priority.addSelectOption('HIGH', 'HIGH');
		priority.addSelectOption('LOW', 'LOW');

		//Get distinct po's and its IDs into dropdown  

		
		
		var OrderType = form.addField('custpage_ordertype', 'select', 'Order Type').setMandatory(true);
		OrderType.addSelectOption("","");
		OrderType.addSelectOption("purchaseorder","PO");
		OrderType.addSelectOption("transferorder","TO");
		OrderType.addSelectOption("returnauthorization","RMA");
		if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
		{
			OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
		}
		var selectPO = form.addField('custpage_orderpo', 'text', 'PO/TO/RMA#');
		
		/*var orderpo = form.addField('custpage_orderpo', 'select', 'PO/TO');
		orderpo.addSelectOption('', '');

		var filterspo = new Array();
		filterspo.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var searchresults = nlapiSearchRecord('purchaseorder', null, filterspo, new nlobjSearchColumn('tranid').setSort());

		if (searchresults != null){
			for (var i = 0; i < searchresults.length; i++){
				orderpo.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('tranid'));
			}
		}

		var filterspo2 = new Array();
		filterspo2.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var searchresults2 = nlapiSearchRecord('transferorder', null, filterspo2, new nlobjSearchColumn('tranid').setSort());

		if (searchresults2 != null){
			for (var i = 0; i < searchresults2.length; i++){
				orderpo.addSelectOption(searchresults2[i].getId(), searchresults2[i].getValue('tranid'));
			}
		}*/

		/*
        var orderpo = form.addField('custpage_orderpo', 'select', 'PO');
        orderpo.addSelectOption('', '');
        var filterspo = new Array();
        filterspo[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'isnot', '');
        filterspo[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1, 2]);
        var colspo = new Array();
        colspo[0]=new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
        colspo[0].setSort(true);        
        colspo[1]=new nlobjSearchColumn('name');
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterspo, colspo);
        for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
            var res = form.getField('custpage_orderpo').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_cntrl_no'), 'is');
            if (res != null) {
                nlapiLogExecution('ERROR', 'res.length', res.length + searchresults[i].getValue('custrecord_ebiz_cntrl_no'));
                if (res.length > 0) {
                    continue;
                }
            }
            orderpo.addSelectOption(searchresults[i].getValue('custrecord_ebiz_cntrl_no'), searchresults[i].getValue('name'));
        }

		//Fetching Record from Closed task

		var filterspo1 = new Array();
        filterspo1[0] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'isnot', '');
        filterspo1[1] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [1, 2]);
        var colspo1 = new Array();
        colspo1[0]=new nlobjSearchColumn('custrecord_ebiztask_ebiz_cntrl_no');
        colspo1[0].setSort(true);        
        colspo1[1]=new nlobjSearchColumn('name');
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filterspo1, colspo1);
        for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
            var res = form.getField('custpage_orderpo').getSelectOptions(searchresults[i].getValue('custrecord_ebiztask_ebiz_cntrl_no'), 'is');
            if (res != null) {
                nlapiLogExecution('ERROR', 'res.length', res.length + searchresults[i].getValue('custrecord_ebiztask_ebiz_cntrl_no'));
                if (res.length > 0) {
                    continue;
                }
            }
            orderpo.addSelectOption(searchresults[i].getValue('custrecord_ebiztask_ebiz_cntrl_no'), searchresults[i].getValue('name'));
        }
		 */

		var sku = form.addField('custpage_sku', 'select', 'ITEM', 'item');
		//sku.setLayoutType('endrow');
		//priority.addSelectOption('H','HIGH');  

		var lotbatch = form.addField('custpage_lotbatch', 'select', 'LOT#');
		lotbatch.addSelectOption('', '');

		var filterslot = new Array();
		filterslot.push(new nlobjSearchFilter('custrecord_batch_no', null, 'isnot', ''));
		filterslot.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1, 2]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslot, new nlobjSearchColumn('custrecord_batch_no'));
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			var res = form.getField('custpage_lotbatch').getSelectOptions(searchresults[i].getValue('custpage_lotbatch'), 'is');
			if (res != null) {
				// nlapiLogExecution('ERROR', 'res.length', res.length + searchresults[i].getValue('custpage_lotbatch'));
				if (res.length > 0) {
					continue;
				}
			}
			lotbatch.addSelectOption(searchresults[i].getValue('custrecord_batch_no'), searchresults[i].getValue('custrecord_batch_no'));
		}

		var filterslot = new Array();
		filterslot.push(new nlobjSearchFilter('custrecord_ebiztask_batch_no', null, 'isnot', ''));
		filterslot.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [1, 2]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filterslot, new nlobjSearchColumn('custrecord_ebiztask_batch_no'));
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			if (searchresults[i].getValue('custrecord_ebiztask_batch_no') != null) {
				var res = form.getField('custpage_lotbatch').getSelectOptions(searchresults[i].getValue('custrecord_ebiztask_batch_no'), 'is');
				if (res != null) {
					// nlapiLogExecution('ERROR', 'res.length', res.length + searchresults[i].getValue('custpage_lotbatch'));
					if (res.length > 0) {
						continue;
					}
				}
				lotbatch.addSelectOption(searchresults[i].getValue('custrecord_ebiztask_batch_no'), searchresults[i].getValue('custrecord_ebiztask_batch_no'));
			}
		}

		var beginlocation = form.addField('custpage_beginlocation', 'select', 'Begin Location', 'customrecord_ebiznet_location');

		var endlocation = form.addField('custpage_endlocation', 'select', 'End Location', 'customrecord_ebiznet_location');

		var lp = form.addField('custpage_lp', 'select', 'LP');
		lp.addSelectOption('', '');
		var filterslp = new Array();
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'isnot', ''));
		filterslp.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1, 2]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslp, new nlobjSearchColumn('custrecord_lpno'));
		if(searchresults!=null && searchresults!='')
		{
			for (var i = 0; i < Math.min(500, searchresults.length); i++) {
				var res = form.getField('custpage_lp').getSelectOptions(searchresults[i].getValue('custrecord_lpno'), 'is');
				if (res != null) {
					//nlapiLogExecution('ERROR', 'res.length3', res.length + searchresults[i].getValue('custrecord_lpno'));
					if (res.length > 0) {
						continue;
					}
				}
				lp.addSelectOption(searchresults[i].getValue('custrecord_lpno'), searchresults[i].getValue('custrecord_lpno'));
			}
		}

		var filterslp = new Array();
		filterslp.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'isnot', ''));
		filterslp.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [1, 2]));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filterslp, new nlobjSearchColumn('custrecord_ebiztask_lpno'));
		if(searchresults!=null && searchresults!='')
		{
			for (var i = 0; i < Math.min(500, searchresults.length); i++) {
				var res = form.getField('custpage_lp').getSelectOptions(searchresults[i].getValue('custrecord_ebiztask_lpno'), 'is');
				if (res != null) {
					// nlapiLogExecution('ERROR', 'res.length4', res.length + searchresults[i].getValue('custrecord_ebiztask_lpno'));
					if (res.length > 0) {
						continue;
					}
				}
				lp.addSelectOption(searchresults[i].getValue('custrecord_ebiztask_lpno'), searchresults[i].getValue('custrecord_ebiztask_lpno'));
			}
		}

		var employee = form.addField('custpage_employee', 'select', 'Employee', 'employee');

		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
		var todate = form.addField('custpage_todate', 'date', 'To Date');

		var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');
		var Location = form.addField('custpage_location', 'select', 'Location', 'location');

		tasktype.setDefaultValue(request.getParameter('custpage_tasktype'));        
		taskstatus.setDefaultValue(request.getParameter('custpage_taskstatus'));        
		priority.setDefaultValue(request.getParameter('custpage_priority'));        
		selectPO.setDefaultValue(request.getParameter('custpage_orderpo'));        
		sku.setDefaultValue(request.getParameter('custpage_sku'));        
		lotbatch.setDefaultValue(request.getParameter('custpage_lotbatch'));       
		beginlocation.setDefaultValue(request.getParameter('custpage_beginlocation'));        
		endlocation.setDefaultValue(request.getParameter('custpage_endlocation'));       
		lp.setDefaultValue(request.getParameter('custpage_lp'));       
		employee.setDefaultValue(request.getParameter('custpage_employee'));
		fromdate.setDefaultValue(request.getParameter('custpage_fromdate'));
		todate.setDefaultValue(request.getParameter('custpage_todate'));
		trailer.setDefaultValue(request.getParameter('custpage_trailer'));
		Location.setDefaultValue(request.getParameter('custpage_location'));

		form.addSubmitButton('Display');

		//create a sublist to display values.
		var sublist = form.addSubList("custpage_results", "list", "Result List");
		sublist.addField("custpage_slno", "text", "Sl No");
		sublist.addField("custpage_taskno", "text", "Ref #");
		sublist.addField("custpage_tasktype", "text", "Task Type");
		sublist.addField("custpage_taskpriority", "text", "Task Priority");
		sublist.addField("custpage_status", "text", "Status");
		sublist.addField("custpage_order", "text", "Order#");
		sublist.addField("custpage_qty", "text", "Qty");
		sublist.addField("custpage_begintime", "text", "Begin Time");
		sublist.addField("custpage_endtime", "text", "End Time");
		sublist.addField("custpage_beginlocation", "select", "Begin Location", "customrecord_ebiznet_location").setDisplayType('inline');
		sublist.addField("custpage_endlocation", "select", "End Location", "customrecord_ebiznet_location").setDisplayType('inline');
		sublist.addField("custpage_putzone", "text", "Put Zone");
		// sublist.addField("custpage_refno", "text", "Ref #");
		sublist.addField("custpage_item", "select", "Item", "item").setDisplayType('inline');
		sublist.addField("custpage_lotbat", "text", "LOT#");
		sublist.addField("custpage_lp", "text", "LP");
		sublist.addField("custpage_user", "text", "User");
		sublist.addField("custpage_trailer", "text", "Trailer#");

		// define search filters
		var value = request.getParameter('custpage_tasktype');
		nlapiLogExecution('ERROR', 'Value:', value);
		var taskarray = new Array();
		taskarray = value.split('');
		for (var p = 0; p < taskarray.length; p++) {
			nlapiLogExecution('ERROR', 'TaskArray:', taskarray[p]);
		}
		var opentaskserchresultcount=0;
		var vStatusId=request.getParameter('custpage_taskstatus');
		try {
			var i = 0;
			var filters = new Array();
			var param = request.getParameter('custpage_sku');
			nlapiLogExecution('ERROR', 'Length:', taskarray.length);

			if (taskarray.length != 0 && value != '') {
				filters[i] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', taskarray);
				nlapiLogExecution('ERROR', 'filters[i]', taskarray);
				i++;
			}
			if (request.getParameter('custpage_taskstatus') != "") {

				nlapiLogExecution('ERROR', 'Inside Task Status', request.getParameter('custpage_taskstatus'));
				if(request.getParameter('custpage_taskstatus') == 'C')
					{
				 filters[i] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty');
					}
				else
					{
					 filters[i] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
					}
				i++;
			}
			if (poid != null && poid!= '') {
				nlapiLogExecution('ERROR', 'Inside PO', poid);
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', poid);
				i++;
			}
			
			if (poidarray != null && poidarray!= '') {
				nlapiLogExecution('ERROR', 'Inside poidarray', poidarray);
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'anyof', poidarray);
				i++;
			}			
			
			
			if (request.getParameter('custpage_sku') != "") {
				nlapiLogExecution('ERROR', 'Inside SKU Param', request.getParameter('custpage_sku'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', request.getParameter('custpage_sku'));
				i++;
			}

			if (request.getParameter('custpage_lotbatch') != "") {
				nlapiLogExecution('ERROR', 'Inside Lot/Batch', request.getParameter('custpage_lotbatch'));
				filters[i] = new nlobjSearchFilter('custrecord_batch_no', null, 'is', request.getParameter('custpage_lotbatch'));
				i++;
			}

			if (request.getParameter('custpage_beginlocation') != "") {
				nlapiLogExecution('ERROR', 'Inside Begin Loc', request.getParameter('custpage_beginlocation'));
				filters[i] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', request.getParameter('custpage_beginlocation'));
				i++;
			}
			if (request.getParameter('custpage_endlocation') != "") {
				nlapiLogExecution('ERROR', 'Inside End Loc', request.getParameter('custpage_endlocation'));
				filters[i] = new nlobjSearchFilter('custrecord_actendloc', null, 'is', request.getParameter('custpage_endlocation'));
				i++;
			}
			if (request.getParameter('custpage_lp') != "") {
				nlapiLogExecution('ERROR', 'Inside LP', request.getParameter('custpage_lp'));
				filters[i] = new nlobjSearchFilter('custrecord_lpno', null, 'is', request.getParameter('custpage_lp'));
				i++;
			}
			if (request.getParameter('custpage_employee') != "") {
				nlapiLogExecution('ERROR', 'Inside LP', request.getParameter('custpage_employee'));
				filters[i] = new nlobjSearchFilter('custrecord_taskassignedto', null, 'is', request.getParameter('custpage_employee'));
				i++;
			}
			if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {
				nlapiLogExecution('ERROR', 'fromdate', request.getParameter('custpage_fromdate'));
				nlapiLogExecution('ERROR', 'todate', request.getParameter('custpage_todate'));

				filters[i] = new nlobjSearchFilter('custrecordact_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate') );
				i++;
			} 
			if (request.getParameter('custpage_trailer') != "") {
				var ebizTrailer=request.getParameter('custpage_trailer');
				var tailerRec = nlapiLoadRecord('customrecord_ebiznet_trailer', ebizTrailer);
				nlapiLogExecution('ERROR', 'Inside Open task Trailer', tailerRec.getFieldValue('name'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', tailerRec.getFieldValue('name'));
				i++;
			}
			if (request.getParameter('custpage_location') != "" && request.getParameter('custpage_location') != null) 
			{
				nlapiLogExecution('ERROR', 'location', request.getParameter('custpage_location'));

				filters[i] = new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custpage_location'));
				i++;
			}

			filters.push(new nlobjSearchFilter('custrecord_ebiz_process_direction', 'custrecord_tasktype', 'is', [1]));
            filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', [32]));

			// return opportunity sales rep, customer custom field, and customer ID
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_tasktype');
			//columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpno');
			columns[1] = new nlobjSearchColumn('custrecord_lpno');
			columns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[3] = new nlobjSearchColumn('custrecord_actendloc');
			columns[4] = new nlobjSearchColumn('custrecordact_begin_date');
			columns[5] = new nlobjSearchColumn('custrecord_act_end_date');
			columns[6] = new nlobjSearchColumn('custrecord_actualbegintime');
			columns[7] = new nlobjSearchColumn('custrecord_actualendtime');
			columns[8] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[9] = new nlobjSearchColumn('custrecord_ebiz_task_no');
			columns[10] = new nlobjSearchColumn('custrecord_batch_no');
			columns[11] = new nlobjSearchColumn('custrecord_ebizuser');
			columns[12] = new nlobjSearchColumn('custrecord_upd_ebiz_user_no');
			columns[13] = new nlobjSearchColumn('custrecord_taskassignedto');
			columns[14] = new nlobjSearchColumn('custrecord_ebizzone_no');
			columns[15] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
			columns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			columns[17] = new nlobjSearchColumn('custrecord_act_qty');
			columns[18] = new nlobjSearchColumn('custrecord_expe_qty');

			// execute the  search, passing null filter and return columns
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

			if(searchresults !=null)
			{
				nlapiLogExecution('ERROR', 'open task searchresults', searchresults.length);
				opentaskserchresultcount=searchresults.length;
			}

		} 
		catch (error) {
			nlapiLogExecution('ERROR', 'Into Catch Block', error);
		}

		var tasktype, lp, beginloc, endloc, begindte, enddate, begintime, endtime, itemname, dstatus, taskno, lotbat,vempname,vempname1,taskassigned,putzone;
		var ZoneName = "";
		var vPONo='';
		var vQty;

		try {
			var intcnt=0;
                         nlapiLogExecution('ERROR','priorityId',priorityId);
                         nlapiLogExecution('ERROR', 'custrecord_ebizzone_no value', putzone);
			/*Searching records from custom record and dynamically adding to the sublist lines */
			for (var i = 0; searchresults != null && i < searchresults.length; i++) {
				//intcnt=i;
				var searchresult = searchresults[i];
				tasktype = searchresult.getText('custrecord_tasktype');
				//lp = searchresult.getValue('custrecord_ebiz_lpno');
				lp = searchresult.getValue('custrecord_lpno');
				beginloc = searchresult.getValue('custrecord_actbeginloc');
				endloc = searchresult.getValue('custrecord_actendloc');
				begindte = searchresult.getValue('custrecordact_begin_date');
				enddate = searchresult.getValue('custrecord_act_end_date');
				begintime = searchresult.getValue('custrecord_actualbegintime');
				endtime = searchresult.getValue('custrecord_actualendtime');
				itemname = searchresult.getValue('custrecord_ebiz_sku_no');
				taskno = searchresult.getValue('custrecord_ebiz_task_no');
				lotbat = searchresult.getValue('custrecord_batch_no');
				vempname = searchresult.getText('custrecord_ebizuser');			
				vempname1= searchresult.getText('custrecord_upd_ebiz_user_no');
				taskassigned= searchresult.getText('custrecord_taskassignedto');
				putzone= searchresult.getValue('custrecord_ebizzone_no');
				vPONo= searchresult.getText('custrecord_ebiz_order_no');
				vQty= searchresult.getValue('custrecord_expe_qty');

				nlapiLogExecution('ERROR', 'custrecord_ebizzone_no value', putzone);
				var trailerVal= searchresult.getValue('custrecord_ebiz_trailer_no');

				var fields = ['name'];
				if(putzone != "" && putzone != null)
				{
					//case# 20124156 Start
					var pzonefilter=new Array();
					pzonefilter[0] = new nlobjSearchFilter('internalid', null, 'is',putzone );
					pzonefilter[1] = new nlobjSearchFilter('isinactive', null, 'is','F' );
					
					var pzonecolumn=new Array();
					pzonecolumn[0] = new nlobjSearchColumn('name');
					
					var srchres=nlapiSearchRecord('customrecord_ebiznet_putpick_zone',null,pzonefilter,pzonecolumn);
					
					if(srchres!=null && srchres!='')
						{
						ZoneName = srchres[0].getText('name');
						}
					//case# 20124156 End
					/*var columns = nlapiLookupField('customrecord_ebiznet_putpick_zone', putzone, fields);
					ZoneName= columns.name;*/
				}


				if (enddate != null && enddate != '') 
					dstatus = 'COMPLETED';
				else 
					dstatus = 'INCOMPLETE';
				/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh  as part of Standard bundle***/
				var priorityId = request.getParameter('custpage_priority');
				nlapiLogExecution('ERROR','priorityId',priorityId);
				/*** up to here ***/
				if((vStatusId == 'I' && dstatus == 'INCOMPLETE') || (vStatusId == 'C' && dstatus == 'COMPLETED')  || vStatusId == 'ALL')
				{	nlapiLogExecution('ERROR', 'dstatus', dstatus);
				nlapiLogExecution('ERROR', 'i', i);
				nlapiLogExecution('ERROR', 'intcnt', intcnt);
				form.getSubList('custpage_results').setLineItemValue('custpage_slno', intcnt+ 1, ""+(intcnt +1));
				form.getSubList('custpage_results').setLineItemValue('custpage_taskno', intcnt + 1, taskno);
				form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', intcnt + 1, tasktype);
				form.getSubList('custpage_results').setLineItemValue('custpage_taskpriority', intcnt + 1, 'HIGH');
				form.getSubList('custpage_results').setLineItemValue('custpage_status', intcnt + 1, dstatus);
				form.getSubList('custpage_results').setLineItemValue('custpage_order', intcnt + 1, vPONo);
				form.getSubList('custpage_results').setLineItemValue('custpage_qty', intcnt + 1, vQty);
				form.getSubList('custpage_results').setLineItemValue('custpage_begintime', intcnt + 1, (begindte + " " + begintime));
				form.getSubList('custpage_results').setLineItemValue('custpage_endtime', intcnt + 1, (enddate + " " + endtime));
				form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', intcnt + 1, beginloc);
				form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', intcnt + 1, endloc);
				form.getSubList('custpage_results').setLineItemValue('custpage_putzone', intcnt + 1, ZoneName);
				//form.getSubList('custpage_results').setLineItemValue('custpage_refno', i + 1, '888');
				form.getSubList('custpage_results').setLineItemValue('custpage_item', intcnt + 1, itemname);
				form.getSubList('custpage_results').setLineItemValue('custpage_lp', intcnt + 1, lp);
				if (taskassigned  != null && taskassigned != "") {
					form.getSubList('custpage_results').setLineItemValue('custpage_user', intcnt + 1, taskassigned);
				}
				nlapiLogExecution('ERROR', 'custpage_results', 'custpage_results');
				form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', intcnt + 1, lotbat);
				form.getSubList('custpage_results').setLineItemValue('custpage_trailer', intcnt + 1, trailerVal);
				intcnt++;
				}

			}
			if(vStatusId == 'C' || vStatusId == 'ALL')
			{
				//Closed task

				var i = 0;
				var filters = new Array();
				var param = request.getParameter('custpage_sku');
				nlapiLogExecution('ERROR', 'Length:', taskarray.length);
				if (taskarray.length != 0 && value != '') {
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', taskarray);
					nlapiLogExecution('ERROR', 'filters[i]', taskarray);
					i++;
				}
				if (request.getParameter('custpage_taskstatus') != "") {

					nlapiLogExecution('ERROR', 'Inside Task Status', request.getParameter('custpage_taskstatus'));
					// filters[i] = new nlobjSearchFilter('custrecordact_begin_date', null, 'on', request.getParameter('custpage_fromdate'));
					//i++;
				}
				if (poid != null && poid!='') {
					nlapiLogExecution('ERROR', 'Inside PO here', poid);
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'is', poid);
					i++;
				}
				
				/*if (poidarray != null && poidarray!= '') {
					nlapiLogExecution('ERROR', 'Inside poidarray', poidarray);
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'anyof', [poidarray]);
					i++;
				}		
				*/
				if (request.getParameter('custpage_sku') != "") {
					nlapiLogExecution('ERROR', 'Inside SKU Param', request.getParameter('custpage_sku'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_sku_no', null, 'is', request.getParameter('custpage_sku'));
					i++;
				}

				if (request.getParameter('custpage_lotbatch') != "") {
					nlapiLogExecution('ERROR', 'Inside Lot/Batch', request.getParameter('custpage_lotbatch'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_batch_no', null, 'is', request.getParameter('custpage_lotbatch'));
					i++;
				}

				if (request.getParameter('custpage_beginlocation') != "") {
					nlapiLogExecution('ERROR', 'Inside Begin Loc', request.getParameter('custpage_beginlocation'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_actbeginloc', null, 'is', request.getParameter('custpage_beginlocation'));
					i++;
				}
				if (request.getParameter('custpage_endlocation') != "") {
					nlapiLogExecution('ERROR', 'Inside End Loc', request.getParameter('custpage_endlocation'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_actendloc', null, 'is', request.getParameter('custpage_endlocation'));
					i++;
				}
				if (request.getParameter('custpage_lp') != "") {
					nlapiLogExecution('ERROR', 'Inside LP', request.getParameter('custpage_lp'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_lpno', null, 'is', request.getParameter('custpage_lp'));
					i++;
				}
				if (request.getParameter('custpage_employee') != "") {
					nlapiLogExecution('ERROR', 'Inside LP', request.getParameter('custpage_employee'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_taskassgndid', null, 'is', request.getParameter('custpage_employee'));
					i++;
				}

				if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {
					nlapiLogExecution('ERROR', 'fromdate', request.getParameter('custpage_fromdate'));
					nlapiLogExecution('ERROR', 'todate', request.getParameter('custpage_todate'));

					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate') );
					i++;
				} 
				if (request.getParameter('custpage_trailer') != "") {
					var ebizTrailer=request.getParameter('custpage_trailer');
					var tailerRec = nlapiLoadRecord('customrecord_ebiznet_trailer', ebizTrailer);
					nlapiLogExecution('ERROR', 'Inside Closed task Trailer', tailerRec.getFieldValue('name'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_trailer_no', null, 'is', tailerRec.getFieldValue('name'));
					i++;
				}

				if (request.getParameter('custpage_location') != "" && request.getParameter('custpage_location') != null) 
				{
					nlapiLogExecution('ERROR', 'location', request.getParameter('custpage_location'));

					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_wms_location', null, 'anyof', request.getParameter('custpage_location'));
					i++;
				}
				filters.push(new nlobjSearchFilter('custrecord_ebiz_process_direction', 'custrecord_ebiztask_tasktype', 'is', [1]));

				// return opportunity sales rep, customer custom field, and customer ID
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiztask_tasktype');
				columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_lpno');
				columns[2] = new nlobjSearchColumn('custrecord_ebiztask_actbeginloc');
				columns[3] = new nlobjSearchColumn('custrecord_ebiztask_actendloc');
				columns[4] = new nlobjSearchColumn('custrecord_ebiztask_act_begin_date');
				columns[5] = new nlobjSearchColumn('custrecord_ebiztask_act_end_date');
				columns[6] = new nlobjSearchColumn('custrecord_ebiztask_actualbegintime');
				columns[7] = new nlobjSearchColumn('custrecord_ebiztask_actualendtime');
				columns[8] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_sku_no');
				columns[9] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_task_no');
				columns[10] = new nlobjSearchColumn('custrecord_ebiztask_batch_no');
				columns[11] = new nlobjSearchColumn('custrecord_ebiztask_taskassgndid');
				columns[12] = new nlobjSearchColumn('custrecord_ebiztask_zone_no');
				columns[13] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_trailer_no');
				columns[14] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
				columns[15] = new nlobjSearchColumn('custrecord_ebiztask_expe_qty');

				// execute the  search, passing null filter and return columns
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);

				if(searchresults !=null && searchresults !=' ')
				{
					nlapiLogExecution('ERROR', 'searchresults.Length:', searchresults.length);
					opentaskserchresultcount=intcnt;
					nlapiLogExecution('ERROR', 'sublistline_closedTask', opentaskserchresultcount);
				}

				var tasktype, lp, beginloc, endloc, begindte, enddate, begintime, endtime, itemname, dstatus, taskno, lotbat,taskassigned;
				var vPONo='';
				var vQty;
                                nlapiLogExecution('ERROR', 'opentaskserchresultcount', opentaskserchresultcount);
				/*Searching records from custom record and dynamically adding to the sublist lines */
				for (var i = 0; searchresults != null && i < searchresults.length; i++) {
					var searchresult = searchresults[i];
					tasktype = searchresult.getText('custrecord_ebiztask_tasktype');
					lp = searchresult.getValue('custrecord_ebiztask_ebiz_lpno');
					beginloc = searchresult.getValue('custrecord_ebiztask_actbeginloc');
					endloc = searchresult.getValue('custrecord_ebiztask_actendloc');
					begindte = searchresult.getValue('custrecord_ebiztask_act_begin_date');
					enddate = searchresult.getValue('custrecord_ebiztask_act_end_date');
					begintime = searchresult.getValue('custrecord_ebiztask_actualbegintime');
					endtime = searchresult.getValue('custrecord_ebiztask_actualendtime');
					itemname = searchresult.getValue('custrecord_ebiztask_ebiz_sku_no');
					taskno = searchresult.getValue('custrecord_ebiztask_ebiz_task_no');
					lotbat = searchresult.getValue('custrecord_ebiztask_batch_no');
					putzone= searchresult.getValue('custrecord_ebiztask_zone_no');
					taskassigned = searchresult.getText('custrecord_ebiztask_taskassgndid');
					vPONo = searchresult.getText('custrecord_ebiztask_ebiz_order_no');
					vQty= searchresult.getValue('custrecord_ebiztask_expe_qty');
					dstatus = 'COMPLETED';
					var colindex=intcnt+i;
					var trailerVal= searchresult.getValue('custrecord_ebiztask_ebiz_trailer_no');

					nlapiLogExecution('ERROR', 'opentaskserchresultcount', opentaskserchresultcount);

					form.getSubList('custpage_results').setLineItemValue('custpage_slno', opentaskserchresultcount + 1, ""+(opentaskserchresultcount +1));
					form.getSubList('custpage_results').setLineItemValue('custpage_taskno', opentaskserchresultcount + 1, taskno);
					form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', opentaskserchresultcount + 1, tasktype);
					form.getSubList('custpage_results').setLineItemValue('custpage_taskpriority', opentaskserchresultcount + 1, 'HIGH');
					form.getSubList('custpage_results').setLineItemValue('custpage_status', opentaskserchresultcount + 1, dstatus);
					form.getSubList('custpage_results').setLineItemValue('custpage_order', opentaskserchresultcount + 1, vPONo);
					form.getSubList('custpage_results').setLineItemValue('custpage_qty', opentaskserchresultcount + 1, vQty);
					form.getSubList('custpage_results').setLineItemValue('custpage_begintime', opentaskserchresultcount + 1, (begindte + " " + begintime));
					form.getSubList('custpage_results').setLineItemValue('custpage_endtime', opentaskserchresultcount + 1, (enddate + " " + endtime));
					form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', opentaskserchresultcount + 1, beginloc);
					form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', opentaskserchresultcount + 1, endloc);
					form.getSubList('custpage_results').setLineItemValue('custpage_putzone', opentaskserchresultcount + 1, ZoneName);
					//form.getSubList('custpage_results').setLineItemValue('custpage_refno', i + 1, '888');
					form.getSubList('custpage_results').setLineItemValue('custpage_item', opentaskserchresultcount + 1, itemname);
					form.getSubList('custpage_results').setLineItemValue('custpage_lp', opentaskserchresultcount + 1, lp);
					//form.getSubList('custpage_results').setLineItemValue('custpage_user', colindex + 1, 'Saritha');
					form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', opentaskserchresultcount + 1, lotbat);
					//if (taskassigned  != null && taskassigned != "") {
					form.getSubList('custpage_results').setLineItemValue('custpage_user', opentaskserchresultcount + 1, taskassigned);
					//}
					form.getSubList('custpage_results').setLineItemValue('custpage_trailer', opentaskserchresultcount + 1, trailerVal);
					opentaskserchresultcount=opentaskserchresultcount+1;

				}
			}
		} 
		catch (error) {
			nlapiLogExecution('ERROR', 'Into Catch Block', error);
		}
		response.writePage(form);
	}
}

function GetPOInternalId(POText,ordtype)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',POText);
	//nlapiLogExecution('ERROR','ordertype',ordertype);

	var ActualPOID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',POText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));
	var RoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR', 'RoleLocation', RoleLocation);
	if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
	{
		filter.push(new nlobjSearchFilter('location', null, 'anyof', RoleLocation));
	}
	nlapiLogExecution('ERROR', 'ordtype', ordtype);


	var columns=new Array();
	//columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord(ordtype,null,filter,columns);
	/*if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}*/
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualPOID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualPOID);

	return ActualPOID;
}

function GetPOInternalIdArray(ordtype)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Array)',ordtype);
	//nlapiLogExecution('ERROR','ordertype',ordertype);

	var ActualPOID='-1';

	var filter=new Array();
	//filter.push(new nlobjSearchFilter('tranid',null,'is',POText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));
	var RoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR', 'RoleLocation', RoleLocation);
	if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
	{
		filter.push(new nlobjSearchFilter('location', null, 'anyof', RoleLocation));
	}
	nlapiLogExecution('ERROR', 'ordtype', ordtype);


	var columns=new Array();
	//columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrecnew=nlapiSearchRecord(ordtype,null,filter,columns);
	/*if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}*/
	var internalidarray = new Array();
	if(searchrecnew!=null && searchrecnew!='' && searchrecnew.length>0)
	{
		for(var i1 = 0; i1 <searchrecnew.length; i1 ++)
		{
			internalidarray.push(searchrecnew[i1].getId());
		}
		//ActualPOID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output ARry)',searchrecnew);

	return internalidarray;
}

function clientCall(lp){
	nlapiLogExecution('ERROR', 'Check this Value', lp);
}
