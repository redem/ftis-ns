/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_MergePacking_OrderNo.js,v $
 *� $Revision: 1.1.2.1.4.2.4.6.4.1 $
 *� $Date: 2015/11/24 11:28:29 $
 *� $Author: aanchal $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_MergePacking_OrderNo.js,v $
 *� Revision 1.1.2.1.4.2.4.6.4.1  2015/11/24 11:28:29  aanchal
 *� 2015.2 issue fix
 *� 201415756
 *�
 *� Revision 1.1.2.1.4.2.4.6  2014/06/13 13:09:05  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.1.2.1.4.2.4.5  2014/05/30 00:41:02  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.1.4.2.4.4  2014/05/13 15:22:37  sponnaganti
 *� case# 20148122
 *� (filter added)
 *� Standard Bundle Issue Fix
 *�
 *� Revision 1.1.2.1.4.2.4.3  2013/07/09 09:43:23  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� GSUSA :: Issue Fixes
 *� Case# : 20123239
 *�
 *� Revision 1.1.2.1.4.2.4.2  2013/06/11 14:30:19  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.1.2.1.4.2.4.1  2013/04/18 07:30:11  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.1.2.1.4.2  2012/09/26 12:28:40  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.1.2.1.4.1  2012/09/24 14:23:04  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi Lnaguage
 *�
 *� Revision 1.1.2.1  2012/04/24 14:48:38  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� Packing
 *�
 *
 ****************************************************************************/

/**
 * @param request
 * @param response
 */
function MergePackingOrderNo(request,response)
{
	if (request.getMethod() == 'GET') 
	{      	
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4;

		if( getLanguage == 'es_ES')
		{
			st1 = "ENTER / SCAN ORDEN # ";
			//st2 = "ENTER / SCAN CART&#211;N #";
			st2 = "ENTER / SCAN CAJA #";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";


		}
		else
		{
			st1 = "ENTER/SCAN ORDER #: ";
			st2 = "ENTER/SCAN CARTON #:";
			st3 = "SEND";
			st4 = "PREV";


		}

		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
	//	html = html + " document.getElementById('enterorderno').focus();";        
		html = html + "</script>";
		html = html + "</head><body>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"";		
		html = html + "				</td>";
		html = html + "			</tr>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterorderno' id='enterorderno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";        
		html = html + "				<td align = 'left'>"+ st2 +"";		
		html = html + "				</td>";
		html = html + "			</tr>"; 
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercarton' type='text'/>";
		html = html + "				</td>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterorderno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage =  request.getParameter('hdngetLanguage');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st5,st6;

		if( getLanguage == 'es_ES')
		{

			st5 = "ORDEN NO V&#193;LIDO # ";
			st6 = "NO TEMAS DE LINEA EMPACAR";

		}
		else
		{

			st5 = "INVALID ORDER #";
			st6 = "NO LINE ITEMS TO PACK";
			st7 = "INVALID CARTON #";


		}
		var getOrderNo = request.getParameter('enterorderno');
		var getCortonNo=request.getParameter('entercarton');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();

		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_error"] = st5;//'INVALID Order #';
		SOarray["custparam_screenno"] = '24MERGE';

		nlapiLogExecution('DEBUG', 'Order #', getOrderNo);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') 
		{
//			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);
			response.sendRedirect('SUITELET', 'customscript_rf_packing_menu', 'customdeploy_rf_packing_menu_di', false, optedEvent);
		}
		else 
		{
			if ((getOrderNo != null &&getOrderNo!='' )|| (getCortonNo!=null&&getCortonNo!='')) 
			{
				nlapiLogExecution('DEBUG', 'Into If', 'hi1');
				
			
				
				
				var filter2= new Array();
				if(getOrderNo!=null && getOrderNo!='')
					filter2.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
				var columns2 = new Array();
				columns2 = new nlobjSearchColumn('custrecord_container_lp_no');
				var SOSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter2, columns2);
				nlapiLogExecution('DEBUG', 'SOSearchResults1', SOSearchResults1);
				if (SOSearchResults1 == null || SOSearchResults1 =='') 
				{
					SOarray["custparam_error"] = st5;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Wrong Order no.');
					return;
				
				}
			
				var filter1= new Array();
				if(getCortonNo!=null && getCortonNo!='')
					filter1.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', getCortonNo));
				var columns1 = new Array();
				columns1 = new nlobjSearchColumn('custrecord_container_lp_no');
				var SOSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter1, columns1);
				nlapiLogExecution('DEBUG', 'SOSearchResults1', SOSearchResults1);
				if (SOSearchResults1 == null || SOSearchResults1 =='') 
				{
					SOarray["custparam_error"] = st7;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Wrong carton no.');
					return;
				
				}
				var SOFilters = new Array();
				if(getOrderNo!=null && getOrderNo!='')
					SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));
				if(getCortonNo!=null && getCortonNo!='')
					SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', getCortonNo));

				SOFilters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));
				SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
				//case# 20148122 starts (filter added)
				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof', [8]));
				//case# 20148122 end
				nlapiLogExecution('DEBUG', 'Into If', 'hi2');                 

				var SOColumns = new Array();
				SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
				SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
				SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
				SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
				SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
				SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
				SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
				SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
				SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
				SOColumns[14] = new nlobjSearchColumn('name');



				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);                

				if (SOSearchResults != null && SOSearchResults.length > 0) 
				{
					nlapiLogExecution('DEBUG', 'No Of Line Items', SOSearchResults.length);
					nlapiLogExecution('DEBUG', 'Given Order', getOrderNo);
					var SOSearchResult = SOSearchResults[0];
					// SOarray["custparam_waveno"] = getWaveNo;
					nlapiLogExecution('DEBUG', 'order', SOSearchResult.getValue('custrecord_ebiz_order_no'));
					nlapiLogExecution('DEBUG', 'contLP', SOSearchResult.getValue('custrecord_container_lp_no'));
					nlapiLogExecution('DEBUG', 'Expected Qty', SOSearchResult.getValue('custrecord_expe_qty'));
					//nlapiLogExecution('DEBUG', 'Given Order', getOrderNo);

					SOarray["custparam_linecount"] = SOSearchResults.length;
					SOarray["custparam_orderno"] = getOrderNo;
					SOarray["custparam_contlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
					SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
					SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
					SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
					SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
					SOarray["custparam_item"] = SOSearchResult.getText('custrecord_sku');
					SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
					SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
					SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
					SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
					SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
					SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
					SOarray['custparam_loopcount']=SOSearchResults.length;
					SOarray['custparam_count']=SOSearchResults.length;
					SOarray['custparam_wmslocation']=SOSearchResult.getValue('custrecord_wms_location');
					SOarray['custparam_name']=SOSearchResult.getValue('name');

					response.sendRedirect('SUITELET', 'customscript_rf_mergepacking_containerlp', 'customdeploy_rf_mergepacking_contlp_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
					/*
                     response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
                     nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');*/
				}
				else 
				{
					SOarray["custparam_error"] = st6;//'No Line Items To Pack';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'No Line Items To Pack');
				}
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Order # not found');
			}
		}
	}
}