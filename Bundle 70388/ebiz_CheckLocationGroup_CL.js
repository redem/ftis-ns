/***************************************************************************
���������������������
����������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_CheckLocationGroup_CL.js,v $
*� $Revision: 1.3.14.1 $
*� $Date: 2013/09/11 15:23:51 $
*� $Author: rmukkera $
*� $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_CheckLocationGroup_CL.js,v $
*� Revision 1.3.14.1  2013/09/11 15:23:51  rmukkera
*� Case# 20124376
*�
*� Revision 1.3  2012/01/06 06:51:20  spendyala
*� CASE201112/CR201113/LOG201121
*� commented  one of the search filter criteria for fetching records from binlocation group to validate name.
*�
*
****************************************************************************/

function fnCheckLocationGroup(type)
{
	var vargetlocgroup =  nlapiGetFieldValue('name');  
	var vLocation	= nlapiGetFieldValue('custrecord_ebizsiteloc');  
	var vCompany	= nlapiGetFieldValue('custrecord_ebizcompanylocgrp');  
	var vLocType	= nlapiGetFieldValue('custrecord_grplocationtype');  

	/* 
	 * Below validations were added by Phani on 1/4/2011
	 */

	var vsequenceNo = nlapiGetFieldValue('custrecord_sequenceno');

	// Check if the location is not selected
	if(vLocation == '')
	{
		alert('Select a Location from the list');
	}
	else
		// Check if the company is not selected
		if(vCompany == '')
		{
			alert('Select a Company from the list');
		}
		else
			// Check if the Location Type is not selected.
			if(vLocType == '')
			{
				alert('Select a Location Type from the list');
			}
			else
				// Check if the sequence number is entered.
				if(vsequenceNo == '')
				{
					alert('Enter the sequence number');
				}
				else
				{
					/*
					 * Added on 1/4/2011 by Phani
					 * The below variable will fetch the internal id of the record selected.
					 */    

					var vId = nlapiGetFieldValue('id');

					var filters = new Array();
					filters[0] = new nlobjSearchFilter('name', null, 'is',vargetlocgroup);
					filters[1] = new nlobjSearchFilter('custrecord_ebizsiteloc', null, 'anyof',[vLocation]);
					//filters[2] = new nlobjSearchFilter('custrecord_ebizcompanylocgrp', null, 'anyof',[vCompany]);//Commented by suman.
					filters[2] = new nlobjSearchFilter('custrecord_grplocationtype', null, 'anyof',[vLocType]);

					/*
					 * Added on 1/4/2011 by Phani
					 * The below variable will filter based on the internal id of the record selected.
					 */

					if (vId != null && vId != "") {
						filters[3] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
					}

					var searchresults = nlapiSearchRecord('customrecord_ebiznet_loc_group', null, filters);


					if(searchresults)
					{
						alert("Bin Location group already exists.");
						return false;
					}
					else
					{
						return true;
					} 

				}
}