/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_PutAwayQtyexception_Serial.js,v $
 *     	   $Revision: 1.1.2.13 $
 *     	   $Date: 2015/06/23 14:19:12 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PutAwayQtyexception_Serial.js,v $
 * Revision 1.1.2.13  2015/06/23 14:19:12  schepuri
 * case# 201413008
 *
 * Revision 1.1.2.12  2014/08/05 15:25:09  sponnaganti
 * Case# 20149823
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.11  2014/06/13 06:27:16  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.10  2014/05/30 00:26:50  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.9  2013/09/26 15:47:18  rmukkera
 * Case# 20124514�
 *
 * Revision 1.1.2.8  2013/09/25 15:50:14  rmukkera
 * Case# 20124534
 *
 * Revision 1.1.2.7  2013/09/24 15:42:13  rmukkera
 * Case# 20124540�
 *
 * Revision 1.1.2.6  2013/09/04 15:47:32  grao
 * Afosa Issue fixes 20124265
 *
 * Revision 1.1.2.5  2013/08/19 16:12:15  rmukkera
 * Issue Fix related to 20123915�,20123912�
 *
 * Revision 1.1.2.4  2013/08/09 15:23:10  rmukkera
 * standard bundle issue fix
 *
 * Revision 1.1.2.3  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.2  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1  2013/04/02 14:38:18  schepuri
 * tsg sb putaway qty exception
 *
 *****************************************************************************/

function PutAwayQtyExpSerial(request, response){
	if (request.getMethod() == 'GET') 
	{
		nlapiLogExecution('DEBUG', 'Into PutAwayQtyExpSerial');

		var getCartLPNo = request.getParameter('custparam_cartno');
		var getFetchedLocationId = request.getParameter('custparam_beginlocation');
		var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
		var getLP = request.getParameter('custparam_lpno');
		var getPOLineItemStatus = request.getParameter('custparam_skustatus');
		var getExceptionQty = request.getParameter('custparam_exceptionqty');
		var getExceptionFlag = request.getParameter('custparam_qtyexceptionflag');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getBaseUomQty = request.getParameter('custparam_baseuomqty');
		var NewGenLP = request.getParameter('custparam_polineitemlp');
		var qtyexceptionFlag=request.getParameter('custparam_qtyexceptionflag');
		var newserlp = request.getParameter('custparam_newlpno');
		nlapiLogExecution('DEBUG', 'qtyexceptionFlag',qtyexceptionFlag);
		
		if(getLP == null || getLP == '')
		{
			getLP = newserlp;
		}
		var Putconfirmserial = request.getParameter('custparam_putawayscreen'); 

		var getNumber = request.getParameter('custparam_number');
		var getNumber=0;
		if(request.getParameter('custparam_number') != null && request.getParameter('custparam_number') != '')
			getNumber = request.getParameter('custparam_number');
		var getItemQuantity = request.getParameter('custparam_itemqty');
		nlapiLogExecution('DEBUG', 'NewGenLP in get', NewGenLP);
		nlapiLogExecution('DEBUG', 'getItemQuantity in get', getItemQuantity);
		var getLPNo = request.getParameter('custparam_lpno');
		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_serial_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enterserialno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_checkin_serial_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>UOM ID : <label>" + getUOMIDText + "</label>";
		html = html + "				<td align = 'left'><input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getPOLineItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnExceptionQty' value=" + getExceptionQty + ">";
		html = html + "				<input type='hidden' name='hdnLPNo' value=" + getLPNo + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnbaseuomqty' value=" + getBaseUomQty + ">";

		//html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		//html = html + "				<input type='hidden' name='hdnUOMID' value=" + getUOMID + ">";
		//html = html + "				<input type='hidden' name='hdnUOMText' value=" + getUOMIDText + ">";
		html = html + "				<input type='hidden' name='hdnNumber' value=" + getNumber + ">";
		//html = html + "				<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "				<input type='hidden' name='hdnItemQuantity' value=" + getItemQuantity + ">";
		//html = html + "				<input type='hidden' name='hdnUOMQty' value=" + getUOMQty + ">";
		//html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnvCartLpno' value=" + getCartLPNo + ">";
		html = html + "				<input type='hidden' name='hdnNewGenLP' value=" + NewGenLP + ">";
		html = html + "				<input type='hidden' name='hdnqtyexceptionFlag' value=" + qtyexceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnputcnfmserial' value=" + Putconfirmserial + ">";
		html = html + "				<input type='hidden' name='hdnnewserlp' value=" + newserlp + ">";
		
		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + (parseInt(getNumber) + 1) + " OF <label>" + getExceptionQty + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER SERIAL NO: ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterserialno' id='enterserialno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>PARENT ID : <label>" + getLP + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterserialno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		//var getUOMID = request.getParameter('hdnUOMID');
		var getNumber = request.getParameter('hdnNumber');
		//var getBeginLocation = request.getParameter('custparam_location');
		var getSerialNo = request.getParameter('enterserialno');
		var getItemQuantity = request.getParameter('hdnItemQuantity');
		var getLP = request.getParameter('hdnLP');
		//	var getUOMQty = request.getParameter('hdnUOMQty');
		var Putconfirserial = request.getParameter('hdnputcnfmserial');
		
		nlapiLogExecution('DEBUG', 'Putconfirserial', Putconfirserial);
		nlapiLogExecution('DEBUG', 'getNumber', getNumber);
		nlapiLogExecution('DEBUG', 'getSerialNo', getSerialNo);
		nlapiLogExecution('DEBUG', 'getItemQuantity', getItemQuantity);
		nlapiLogExecution('DEBUG', 'getLP', getLP);
		var POarray = new Array();
		var EndLocationArray = new Array();

		//	Get the PO# from the previous screen, which is passed as a parameter		
		//var getPONo = request.getParameter('custparam_poid');
		//var getPOItem = request.getParameter('custparam_poitem');
		//var getPOLineNo = request.getParameter('custparam_lineno');
		//var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		//var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getExceptionQty = request.getParameter('custparam_exceptionqty');

		var getPOLineItemStatus = request.getParameter('custparam_skustatus');

		var getItemCube = request.getParameter('custparam_itemcube');
		var getActualBeginDate = request.getParameter('hdnActualBeginDate');
		var getBaseUomQty = request.getParameter('custparam_baseuomqty');
		var getCartlpno=request.getParameter('custparam_cartno');
		//var getLocId = request.getParameter('custparam_locid');
		nlapiLogExecution('DEBUG', 'getCartlpno', getCartlpno);
		nlapiLogExecution('DEBUG', 'getItemCube', getItemCube);
		//var trantype = nlapiLookupField('transaction', getPOInternalId, 'recordType');
		nlapiLogExecution('DEBUG', 'getBaseUomQty', getBaseUomQty);

		nlapiLogExecution('DEBUG', 'getPOLineItemStatus', getPOLineItemStatus);

		/*var getLineCount = 1;
		var getBaseUOM = 'EACH';
		var getBinLocation = "";
		var getuomlevel = "";

		var eBizItemDims=geteBizItemDimensions(getFetchedItemId);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					getBaseUOM = eBizItemDims[z].getText('custrecord_ebizuomskudim');
					getuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
				}
			}
		}

		nlapiLogExecution('DEBUG', 'getBaseUOM', getBaseUOM);
		nlapiLogExecution('DEBUG', 'getuomlevel', getuomlevel);


		var getLineCount = 1;
		var getBaseUOM = getUOMID;
		var getBinLocation = "";*/
		POarray["custparam_polineitemlp"] = request.getParameter('hdnNewGenLP');
		var newgenLp = request.getParameter('hdnNewGenLP');
		POarray["custparam_qtyexceptionflag"] = request.getParameter('hdnqtyexceptionFlag');

		nlapiLogExecution('DEBUG', 'POarray["custparam_polineitemlp"]', POarray["custparam_polineitemlp"]);

		POarray["custparam_error"] = 'INVALID SERIAL NO';
		POarray["custparam_screenno"] = 'putqtyexpserial';
		POarray["custparam_cartno"] = request.getParameter('custparam_cartno');
		POarray["custparam_beginlocation"] = request.getParameter('custparam_beginlocation');
		POarray["custparam_lpNumbersArray"] = request.getParameter('custparam_lpNumbersArray');
		POarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		POarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
		POarray["custparam_exceptionqty"] = request.getParameter('custparam_exceptionqty');
		POarray["custparam_qtyexceptionflag"] = request.getParameter('custparam_qtyexceptionflag');
		POarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		POarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		POarray["custparam_number"] = parseInt(getNumber);
		POarray["custparam_cartlpno"] = request.getParameter('custparam_cartno');
		//case# 20149823 (changed because need to update serialparentid in serial entry for remaining qty)
		//POarray["custparam_remainingqty"] = request.getParameter('custparam_remainingqty');
		POarray["custparam_remainingqtyserial"]=request.getParameter('custparam_remainingqtyserial');
		//case# 20149823 ends
		POarray["custparam_hdnlpno_qtyexception"] = request.getParameter('hdnLPNo');
		POarray["custparam_newlpno"] = request.getParameter('hdnnewserlp');
		
		POarray["custparam_putawayscreen"] = Putconfirserial;
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') {
			if(getCartlpno!=null && getCartlpno!='' && getCartlpno!='null' && getCartlpno!='undefined')
			{						
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayqtyexp', 'customdeploy_ebiz_rf_cart_putawayqtyexp', false, POarray);

			}
			else
			{
				var recid=request.getParameter('custparam_recordid');
				if(recid!=null && recid!='')
				{
					var fields = new Array();
					var values = new Array();
					fields[0] = 'custrecord_expe_qty';
					values[0] = parseFloat(getItemQuantity);


					var vputwrecid1=nlapiSubmitField('customrecord_ebiznet_trn_opentask', recid, fields, values);
					nlapiLogExecution('DEBUG', 'vputwrecid1 in serial F7', vputwrecid1);
				}
				if(Putconfirserial=='PUTWCONFIRMSERIAL')
				{
					response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);

				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_putaway_qty_exception', 'customdeploy_rf_putaway_qty_exception_di', false, POarray);
				}
			}
		}
		//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
		//            if (optedEvent != '' && optedEvent != null) {
		else 
			if (getSerialNo == "") {
				//	if the 'Send' button is clicked without any option value entered,
				//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Enter Serial No', getSerialNo);
				POarray["custparam_error"] = 'ENTER SERIAL NO';
			}
			else {
				nlapiLogExecution('DEBUG', 'Serial No', getSerialNo);

				var LP = request.getParameter('custparam_lpno');
				nlapiLogExecution('DEBUG', 'LP', LP);
				var vSrchRecordLP = '';
				if(LP!=null && LP!='')
				{
					var vfiltersLP = new Array();

					vfiltersLP[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);

					 vSrchRecordLP = nlapiSearchRecord('customrecord_ebiznetserialentry', null, vfiltersLP);
				}
				if(vSrchRecordLP!=null && vSrchRecordLP!='')				
				{


					//checking serial no's in records
					var filtersLP = new Array();
					filtersLP[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);
					filtersLP[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);

				var SrchRecordLP = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersLP);
				if (SrchRecordLP == null || SrchRecordLP == "") {
					POarray["custparam_error"] = 'SERIAL NO ENTERED DOESNT BELONG TO LP';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}

				var getActualEndDate = DateStamp();
				var getActualEndTime = TimeStamp();
				var TempSerialNoArray = new Array();
				if (request.getParameter('custparam_serialno') != null) 
					TempSerialNoArray = request.getParameter('custparam_serialno').split(',');

//				if (getBeginLocation != '') 
//				{
				nlapiLogExecution('DEBUG', 'INTO SERIAL ENTRY');
				//checking serial no's in already scanned one's
				for (var t = 0; t < TempSerialNoArray.length; t++) {
					if (getSerialNo == TempSerialNoArray[t]) {
						POarray["custparam_error"] = "Serial No. Already Scanned";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return;
					}
				}

				//checking serial no's in records
				var filtersser = new Array();
				filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
				nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
				for ( var i = 0; SrchRecord!= null && i < SrchRecord.length; i++) 
				{
					var searchresultserial = SrchRecord[i];

					var serrialid=searchresultserial.getId();
				}
				nlapiLogExecution('DEBUG', 'serrialid', serrialid);
				/*if (SrchRecord && trantype!='returnauthorization' && trantype!='transferorder'  ) {
					POarray["custparam_error"] = "Serial No. Already Exists";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				else {*/
				// nlapiLogExecution('DEBUG', 'SERIAL NO NOT FOUND');
				if (request.getParameter('custparam_serialno') == null || request.getParameter('custparam_serialno') == "") {
					POarray["custparam_serialno"] = getSerialNo;
				}
				else {
					POarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
				}

				POarray["custparam_number"] = parseInt(getNumber) + 1;
				//}
				TempSerialNoArray.push(getSerialNo);
				nlapiLogExecution('DEBUG', '(getNumber + 1)', (parseInt(getNumber) + 1));
				nlapiLogExecution('DEBUG', 'getExceptionQty', getExceptionQty);
				if ((parseInt(getNumber) + 1) < parseInt(getExceptionQty)) {
					nlapiLogExecution('DEBUG', 'Scanning Serial No.');
					response.sendRedirect('SUITELET', 'customscript_putawayqtyexp_serial_no', 'customdeploy_putawayqtyexp_serial_no', false, POarray);
					return;

				}
				else 
				{
					//var remaingqty=request.getParameter('custparam_remainingqty');
					var remaingqty=request.getParameter('custparam_remainingqtyserial');//case# 20149823
					if(TempSerialNoArray!=null && TempSerialNoArray!='' && TempSerialNoArray.length>0 )
					{
						var filtersLP = new Array();						
						filtersLP[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);
						filtersLP[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
						var columns=new Array();
						columns[0]=new nlobjSearchColumn('custrecord_serialnumber');

							var SrchRecordSerialEntry = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filtersLP,columns);
							var tempremcount=0;
							if (SrchRecordSerialEntry != null && SrchRecordSerialEntry != "") {

								for (var s1=0;s1<SrchRecordSerialEntry.length;s1++)
								{
									var serial=SrchRecordSerialEntry[s1].getValue('custrecord_serialnumber');

									if(TempSerialNoArray.indexOf(serial)==-1)
									{
										nlapiLogExecution('ERROR', 'tempremcount.',tempremcount);
										nlapiLogExecution('ERROR', 'remaingqty.',remaingqty);
										if(remaingqty!=null && remaingqty!='' && parseInt(remaingqty)>0 && parseInt(tempremcount)<=parseInt(remaingqty))
										{


										tempremcount+=tempremcount;
										nlapiLogExecution('ERROR', 'tempremcount.',tempremcount);

											nlapiLogExecution('ERROR', 'updating the serial no.',serial);
											nlapiLogExecution('ERROR', 'newgenLp',newgenLp);
											//newgenLp
											var fieldArray=new Array();
											//fieldArray[0]='custrecord_serialparentid';
											fieldArray[0]='custrecord_serialstatus';

											var Values=new Array();
										//	Values[0] = newgenLp;
											Values[0] = "P";

											nlapiSubmitField('customrecord_ebiznetserialentry',SrchRecordSerialEntry[s1].getId(),fieldArray,Values);

									}
									else
									{
										nlapiLogExecution('ERROR', 'deleting the serial no.',serial);
										/*nlapiLogExecution('ERROR', 'newgenLp',newgenLp);
										//newgenLp
										var fieldArray=new Array();
										fieldArray[0]='custrecord_serialparentid';
										var Values=new Array();
										Values[0] = newgenLp;

										nlapiSubmitField('customrecord_ebiznetserialentry',SrchRecordSerialEntry[s1].getId(),fieldArray,Values);*/
										nlapiDeleteRecord('customrecord_ebiznetserialentry',SrchRecordSerialEntry[s1].getId());
									}
								}
							}
						}
					}
					var opentaskid=request.getParameter('custparam_recordid');
					if(opentaskid!=null && opentaskid!='')
					{
						nlapiLogExecution('ERROR', 'opentaskid.',opentaskid);
						var fieldArray=new Array();
						fieldArray[0]='custrecord_serial_no';
						var Values=new Array();
						Values[0]=TempSerialNoArray.toString();
						nlapiLogExecution('ERROR', 'TempSerialNoArray.',TempSerialNoArray.toString());
						nlapiSubmitField('customrecord_ebiznet_trn_opentask',opentaskid,fieldArray,Values);

						}

						if(getCartlpno!=null && getCartlpno!='' && getCartlpno!='null' && getCartlpno!='undefined')
						{						
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
							return;
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
							return;
						}

					}
//					}
//					else {
//					POarray["custparam_error"] = "Location Not Found";
//					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
//					nlapiLogExecution('DEBUG', 'Enter Location');
//					}
				}
				else
				{
					
					nlapiLogExecution('DEBUG', 'getSerialNo@@', getSerialNo);
					
					var vfiltersLP = new Array();
					vfiltersLP[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);
					//filtersLP[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);

					var vSrchRecordLP = nlapiSearchRecord('customrecord_ebiznetserialentry', null, vfiltersLP);
					if (vSrchRecordLP != null && vSrchRecordLP != "") {
						POarray["custparam_error"] = 'SERIAL# ALREADY EXISTS';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return;
					}
					
					var opentaskid=request.getParameter('custparam_recordid');
					nlapiLogExecution('DEBUG', 'opentaskid', opentaskid);
					
					
					var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', opentaskid);
					
					var getPOInternalId = transaction.getFieldValue('custrecord_ebiz_order_no');
					var getPOLineNo = transaction.getFieldValue('custrecord_line_no');
					var getFetchedItemId = transaction.getFieldValue('custrecord_sku');
								
					nlapiLogExecution('DEBUG', 'getPOInternalId', getPOInternalId);
					nlapiLogExecution('DEBUG', 'getFetchedItemId', getFetchedItemId);
					nlapiLogExecution('DEBUG', 'getPOLineNo', getPOLineNo);
					if(LP == null || LP == '')
					{
						LP = POarray["custparam_newlpno"];
					}
					
					var customrecord = nlapiCreateRecord('customrecord_ebiznetserialentry');
					customrecord.setFieldValue('name', getSerialNo);
					customrecord.setFieldValue('custrecord_serialebizpono', getPOInternalId);
					customrecord.setFieldValue('custrecord_serialpolineno', getPOLineNo);
					customrecord.setFieldValue('custrecord_serialitem', getFetchedItemId);
					customrecord.setFieldValue('custrecord_serialparentid', LP);					
					customrecord.setFieldValue('custrecord_serialnumber', getSerialNo);				 
					customrecord.setFieldValue('custrecord_serialwmsstatus', '3');
					var rec = nlapiSubmitRecord(customrecord, false, true);
					POarray["custparam_number"] = parseInt(getNumber) + 1;
					POarray["custparam_recordid"] = opentaskid;
					POarray["custparam_newlpno"] = LP;
					POarray["custparam_lpno"] ='';
					
					if ((parseInt(getNumber) + 1) < parseInt(getExceptionQty)) {
						nlapiLogExecution('DEBUG', 'Scanning Serial No.');
						response.sendRedirect('SUITELET', 'customscript_putawayqtyexp_serial_no', 'customdeploy_putawayqtyexp_serial_no', false, POarray);
						return;

					}

					nlapiLogExecution('DEBUG', 'rec', rec);
					if(getCartlpno!=null && getCartlpno!='' && getCartlpno!='null' && getCartlpno!='undefined')
					{						
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
						return;
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
						return;
					}
				}
	}
	}

	nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
}


if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}



