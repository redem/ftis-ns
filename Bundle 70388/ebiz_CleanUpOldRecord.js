/***************************************************************************
                          eBizNET Solution Inc
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Scheduled/Attic/ebiz_CleanUpOldRecord.js,v $
*  $Revision: 1.1.2.3 $
*  $Date: 2014/03/04 12:15:28 $
*  $Author: spendyala $
*  $Name: t_NSWMS_2014_1_2_0 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_CleanUpOldRecord.js,v $
*  Revision 1.1.2.3  2014/03/04 12:15:28  spendyala
*  CASE201112/CR201113/LOG201121
*  Issue fixed related to case#20127487
*
*  Revision 1.1.2.2  2013/10/25 17:25:27  spendyala
*  CASE201112/CR201113/LOG201121
*  Added Inventory snap shot on to the search list ,case#0125362
*
*  Revision 1.1.2.1  2013/10/25 15:53:56  spendyala
*  CASE201112/CR201113/LOG201121
*  New Files for the case#0125362
*
*
****************************************************************************/
//Scheduler will delete record from the below list of custom record which are 60 days older.
//List of Records
/*-	Cleanup Label Printing
-	Cleanup Error Log
-	Cleanup Inventory Audit*/


function AutoDeletion_60Days()
{
	try
	{
		//TO get the date which is 60 days past from now.
		var currentdate = new Date();
		var requiredDate='';
		currentdate.setDate(currentdate.getDate()-parseInt(60));
		requiredDate=((currentdate.getMonth()+1)+"/"+(currentdate .getDate())+"/"+(currentdate.getFullYear()));
		nlapiLogExecution('ERROR', 'RequiredDate', requiredDate);
		//End of date conversion.

		//Declare Set of custom records which need to delete.
		var globalArrayIDs=new Array();
		globalArrayIDs.push("customrecord_ebiznet_lockrecs");
		globalArrayIDs.push("customrecord_labelprinting");
		globalArrayIDs.push("customrecord_exception_log");
		globalArrayIDs.push("customrecord_ebiznet_createinv_audit");
		globalArrayIDs.push("customrecord_ebiz_inventory_sanpshot");
		//End of declaration.

		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining Usage',context.getRemainingUsage());

		var customrecordID="";

		for(var vIDCount=0;vIDCount<globalArrayIDs.length;vIDCount++)
		{
			customrecordID=globalArrayIDs[vIDCount];
			nlapiLogExecution("Debug","Begining of customrecordID",customrecordID);
			nlapiLogExecution('ERROR','Remaining Usage',context.getRemainingUsage());
			if(context.getRemainingUsage()> 30)
			{
				if(vIDCount==0)
					var searchrecord=GetDeleteRecordsIDs(-1, DateStamp(),customrecordID);
				else
					var searchrecord=GetDeleteRecordsIDs(-1,requiredDate,customrecordID);
				if(searchrecord!=null && searchrecord!='')
				{
					for ( var count = 0; count < searchrecord.length; count++)
					{
						var tempSearchRecord=searchrecord[count];
						if(tempSearchRecord!=null && tempSearchRecord.length>0)
						{
							for ( var tempcount = 0; tempcount < tempSearchRecord.length; tempcount++)
							{
								if(context.getRemainingUsage()> 30)
								{
									var id=nlapiDeleteRecord(customrecordID,tempSearchRecord[tempcount].getId());
								}
								else
									break;
							}
						}
					}
				}
			}
			else
				break;
			nlapiLogExecution("Debug","END of customrecordID",customrecordID);
		}
	}
	catch(exp)
	{
		nlapiLogExecution("Debug","Exception in AutoDelete 60 days",exp);
	}
}

var TempDeleteRecords=new Array();
function GetDeleteRecordsIDs(maxno,requiredDate,customrecordID)
{
	try
	{
		nlapiLogExecution('Error', 'maxno',maxno);
		var vDate=requiredDate;

		var filters = new Array();
		filters.push(new nlobjSearchFilter('created', null, 'before',vDate));
		if(maxno!=-1)
		{
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');
		columns[0].setSort(false);
		var SearchResults= nlapiSearchRecord(customrecordID, null, filters,columns);	

		if(SearchResults!=null)
		{
			if(SearchResults.length>=1000)
			{
				var maxno1=SearchResults[SearchResults.length-1].getValue(columns[0]);

				TempDeleteRecords.push(SearchResults);
				GetDeleteRecordsIDs(maxno1,vDate,customrecordID);
			}
			else
			{
				TempDeleteRecords.push(SearchResults);
			}
		}
		return TempDeleteRecords;
	}
	catch(exp)
	{
		nlapiLogExecution("Debug","Exception in GetDeleteRecordsIDs",exp);
	}
}

function DateStamp()
{
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());

}
