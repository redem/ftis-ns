/***************************************************************************
	  		   eBizNET Solutions               
 ****************************************************************************/
/*  
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_WODeallocate_SL.js,v $
 *     	   $Revision: 1.1.6.11 $
 *     	   $Date: 2015/08/20 15:57:50 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WODeallocate_SL.js,v $
 * Revision 1.1.6.11  2015/08/20 15:57:50  nneelam
 * case# 201413842
 *
 * Revision 1.1.6.10  2015/04/17 15:40:39  skreddy
 * Case# 201223810
 * Jawbone prod issue fix
 *
 * Revision 1.1.6.9  2015/04/13 09:25:57  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.6.8  2014/08/23 01:13:56  gkalla
 * case#201410062
 * JB Deallocation issues after pick confirm
 *
 * Revision 1.1.6.7  2014/06/13 15:34:29  sponnaganti
 * case# 20148583
 * Stnd Bundle Issue Fix
 *
 * Revision 1.1.6.6  2013/10/17 07:04:01  gkalla
 * Case# 20125081
 * PCT to restrict picked or partially picked orders from deallocation
 *
 * Revision 1.1.6.5  2013/03/08 14:43:37  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.6.4  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.1.6.3  2013/02/28 07:12:22  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Monobind.
 *
 * Revision 1.1.6.2  2013/02/27 13:22:58  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from Monobind
 *
 * Revision 1.1  2012/02/24 07:58:10  gkalla
 * CASE201112/CR201113/LOG201121
 * ForWO Deallocate
 *
 * Revision 1.12  2011/12/09 14:44:57  gkalla
 *
 *****************************************************************************/


function ebiznet_WODeallocate(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('De-allocate WO Locations');
		var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		WorkOrder.addSelectOption("","");
		fillWOField(form, WorkOrder,-1);

		// Retrieve all sales orders
		//var WorkOrderList = getAllWorkOrders();		
		// Add all sales orders to SO Field
		//WorkOrder.setDefaultValue(request.getParameter('custpage_workorder'));
		//addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);

		WorkOrder.setMandatory(true);
		//fillsalesorderField(form, salesorder,-1);

		form.addSubmitButton('De-allocate');

		response.writePage(form);
	}
	else //this is the POST block
	{
		var woid=request.getParameter('custpage_workorder');


		DeallocateInv(woid,request, response);



	}
}
function DeallocateInv(woid,request, response)
{
	nlapiLogExecution('ERROR', 'Deallocation ');
	var form = nlapiCreateForm('De-allocate WO Locations');
	var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
	WorkOrder.addSelectOption("","");
	fillWOField(form, WorkOrder,-1);

	// Retrieve all sales orders
	//var WorkOrderList = getAllWorkOrders();		
	// Add all sales orders to SO Field
	//WorkOrder.setDefaultValue(request.getParameter('custpage_workorder'));
	//addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);

	WorkOrder.setMandatory(true);
	//fillsalesorderField(form, salesorder,-1);

	form.addSubmitButton('De-allocate');

	if(woid!=null && woid!='')
	{
		nlapiLogExecution('ERROR','INTOwoid1',woid);
		var wosearchresultsitem = nlapiLoadRecord('workorder', woid);
		var itemcount= wosearchresultsitem.getLineItemCount('item');
		if(wosearchresultsitem!=null && wosearchresultsitem!='') 
		{ 	    	
			var qty = wosearchresultsitem.getFieldValue('quantity'); 	    	     
			var vLocation = wosearchresultsitem.getFieldValue('location');  
			var vWOId = wosearchresultsitem.getFieldValue('tranid'); 
			var assemItem=wosearchresultsitem.getFieldValue('assemblyitem'); 
			var vstatus= wosearchresultsitem.getFieldValue('status');
			nlapiLogExecution('ERROR', 'vstatus', vstatus);

			if(vstatus == 'Built' || vstatus == 'Closed')
			{
				showInlineMessage(form, 'Error','You are not allowed to De-allocate ' + vstatus + ' Work order', '');
				response.writePage(form);


			}
			else
			{ 
				nlapiLogExecution('ERROR', 'vWOId', woid);

				var filters = new Array(); 
				nlapiLogExecution('ERROR', 'into Block vWOId', woid);
				if (woid != "") {
					filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', woid));
				} 
				filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid));
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','9']));
				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3));

				var columns = new Array();				 
				columns[0] = new nlobjSearchColumn('custrecord_wms_status_flag',null,'group');
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

				if(searchresults != null && searchresults != "" && searchresults.length>0)
				{ 
					for(var v=0;v<searchresults.length;v++)
					{
						var vWMSStatus=searchresults[v].getValue('custrecord_wms_status_flag',null,'group');
						if(vWMSStatus == 8 || vWMSStatus == '8')
						{
							nlapiLogExecution('ERROR', 'Picking process completed for some of the items', woid);
							showInlineMessage(form, 'Error', 'Picking process completed for some of the items, So this order is not allowed to De-Allocation.', '');
							response.writePage(form);
							return;
						}	
					}

					var vCompareLinesNSvsEbiz = fnCompareLinesNSvsEbiz(woid);
					if(vCompareLinesNSvsEbiz == false)
					{
						nlapiLogExecution('ERROR','vCompareLinesNSvsEbiz ',vCompareLinesNSvsEbiz); 
						showInlineMessage(form, 'Error','Bin locations are not allocated for all the items of this work order', '');
						response.writePage(form);
					}
					// Not allow to do deallocation for already picked/partially picked work orders.					
					var filters1 = new Array(); 
					nlapiLogExecution('ERROR', 'into Block vWOId', woid);
					if (woid != "") {
						filters1.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', woid));
					} 
					filters1.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
					var columns1 = new Array();
					columns1[0] = new nlobjSearchColumn('custrecord_expe_qty');
					columns1[1] = new nlobjSearchColumn('custrecord_invref_no');
					var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters1, columns1);
					if(searchresults1 != null && searchresults1 != '')
					{	 

						DeleteOpentask(searchresults1);
						/*var ErrorMsg = nlapiCreateError('CannotAllocate','De-allocated Successfully', true);
						throw ErrorMsg;*/
						showInlineMessage(form, 'Confirmation', 'De-allocated Successfully', '');						 
						nlapiSubmitField('workorder', woid, 'custbody_ebn_bin_generated', 'F');
						response.writePage(form);
						return;

					}
					else
					{	
						showInlineMessage(form, 'Error', 'Picking process already initiated, So this order is not allowed to De-Allocation.', '');
						response.writePage(form);
						return;

					}
				}
				else
				{	
					showInlineMessage(form, 'Error', 'Bin locations are not allocated to this work order', '');
					response.writePage(form);
					return;
				}
			} 

		}


	}
	response.writePage(form);

}
function DeleteOpentask(OTSearchResults)
{  
	if(OTSearchResults != null && OTSearchResults != "")
	{
		nlapiLogExecution('ERROR', 'OTSearchResults.length : ', OTSearchResults.length);
		for(var p=0;p<OTSearchResults.length;p++)
		{
			var vinvrefno=OTSearchResults[p].getValue('custrecord_invref_no');
			var RevPickQty=OTSearchResults[p].getValue('custrecord_expe_qty');
			nlapiLogExecution('ERROR', 'vinvrefno', vinvrefno);
			nlapiLogExecution('ERROR', 'RevPickQty', RevPickQty);
			if((vinvrefno!=null && vinvrefno!='') && (RevPickQty!=null && RevPickQty!=''))
				deAllocateInv(vinvrefno,RevPickQty);
			var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', OTSearchResults[p].getId());
			nlapiLogExecution('ERROR', 'Deleted Rec from OpenTask : ', id);
		}
	} 		

	nlapiLogExecution('ERROR', 'Open task Deletion Success'); 
}

function deAllocateInv(vinvrefno,RevPickQty)
{

	if(parseFloat(RevPickQty)>0)
	{
//		updating Inventory


		var scount=1;
		var id2;
		var invqoh = 0;

		LABL1: for(var p=0;p<scount;p++)
		{	

			nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', p);
			try
			{
				nlapiLogExecution('ERROR', 'vinvrefno', vinvrefno);
				var invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', vinvrefno);

				invqoh = invttran.getFieldValue('custrecord_ebiz_qoh');
				var invallocqty = invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				nlapiLogExecution('ERROR', 'invqoh', invqoh);
				nlapiLogExecution('ERROR', 'invallocqty', invallocqty);
				if (isNaN(invallocqty)) {
					invallocqty = 0;
				}
				if (invallocqty == "") {
					invallocqty = 0;
				}
				var newallocateQty = parseFloat(invallocqty) - parseFloat(RevPickQty);

				if (newallocateQty <0) 
					newallocateQty=0;
				nlapiLogExecution('ERROR', 'newallocateQty', newallocateQty);
				invttran.setFieldValue('custrecord_ebiz_alloc_qty',newallocateQty);
				invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');
				id2 = nlapiSubmitRecord(invttran, false, true);  

			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 
				nlapiLogExecution('ERROR', 'Exception in WO De-allocate : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		} 

	}
}
function fillWOField(form, WOField,maxno){
	var WOFilers = new Array();
	WOFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	WOFilers.push(new nlobjSearchFilter('status', null, 'is', ['WorkOrd:B','WorkOrd:D']));
	//case# 20148583 starts
	var vRoleLocation = getRoledBasedLocationNew();
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		WOFilers.push(new nlobjSearchFilter('location', null, 'anyof', vRoleLocation));
	}
	//case# 20148583 ends

	if(maxno!=-1)
	{
		WOFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
	}


	WOField.addSelectOption("", "");

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('tranid');
	columns[0].setSort(true);
	columns[1]=new nlobjSearchColumn('internalid');
	columns[2]=new nlobjSearchColumn('type');


	var customerSearchResults = nlapiSearchRecord('workorder', null, WOFilers,columns);


	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('tranid') != null && customerSearchResults[i].getValue('tranid') != "" && customerSearchResults[i].getValue('tranid') != " ")
		{
			var resdo = form.getField('custpage_workorder').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		WOField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var column=new Array();
		column[0]=new nlobjSearchColumn('tranid');		
		column[1]=new nlobjSearchColumn('internalid');
		column[1].setSort(true);

		var OrderSearchResults = nlapiSearchRecord('workorder', null, WOFilers,column);

		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillWOField(form, WOField,maxno);	

	}
}

function fnCompareLinesNSvsEbiz(woid)
{
	/*var filtersNS = new Array();

	filtersNS[0] = new nlobjSearchFilter('internalid', null, 'is', woid);
	filtersNS[1] = new nlobjSearchFilter('mainline', null, 'is', 'F');//ship task


	var columnsNS = new Array();*/
	var searchresultsNS =  nlapiLoadRecord('workorder', woid);
	//nlapiLogExecution('Debug','Time stamp 4',TimeStampinSec());
	var itemcount= searchresultsNS.getLineItemCount('item');
	nlapiLogExecution('Debug','itemcount',itemcount);
	var vTotNSBomLines=0;

	if(searchresultsNS != null  && searchresultsNS != ''){
		for(var s=1; s<=itemcount;s++)
		{
			var CompItemType = searchresultsNS.getLineItemValue('item', 'itemtype', s);
			nlapiLogExecution('Debug','CompItemType',CompItemType);
			if(CompItemType != 'Service' && CompItemType != 'NonInvtPart' && CompItemType !='OthCharge')
				vTotNSBomLines += 1;

		}		
	}
	nlapiLogExecution('Debug','vTotNSBomLines',vTotNSBomLines);
	//vTotNSBomLines= searchresultsNS.length;

	var filterOT = new Array();
	filterOT[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid);
	filterOT[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']);
	filterOT[2] = new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty');
	filterOT[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3);

	var columnsOT = new Array();

	columnsOT[0] = new nlobjSearchColumn('custrecord_line_no',null,'group');

	columnsOT[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');

	var searchresultsOT = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOT,columnsOT);	
	var vToteBizLines=0;

	if(searchresultsOT != null  && searchresultsOT != '')
		vToteBizLines= searchresultsOT.length;
	nlapiLogExecution('Debug','vToteBizLines',vToteBizLines);
	if(vTotNSBomLines != 0 && vToteBizLines != 0 && vTotNSBomLines == vToteBizLines)
		return true;
	else
		return false;
}