/***************************************************************************
 			eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CreateInventoryItemStatus.js,v $
 *     	   $Revision: 1.7.2.10.4.4.4.13.2.5 $
 *     	   $Date: 2014/11/04 14:50:57 $
 *     	   $Author: skavuri $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_156 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_CreateInventoryItemStatus.js,v $
 * Revision 1.7.2.10.4.4.4.13.2.5  2014/11/04 14:50:57  skavuri
 * Case# 201410931 Std bundle issue fixed
 *
 * Revision 1.7.2.10.4.4.4.13.2.4  2014/10/31 14:32:55  schepuri
 * 201410847
 *
 * Revision 1.7.2.10.4.4.4.13.2.3  2014/10/28 09:27:20  schepuri
 *  201410847 issue fix
 *
 * Revision 1.7.2.10.4.4.4.13.2.2  2014/10/15 16:09:36  skavuri
 * Case# 201410634 Std bundle issue fixed
 *
 * Revision 1.7.2.10.4.4.4.13.2.1  2014/09/26 15:39:17  skavuri
 * Case#  sonic cr fixed
 *
 * Revision 1.7.2.10.4.4.4.13  2014/06/13 10:24:00  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.7.2.10.4.4.4.12  2014/05/30 00:34:19  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.7.2.10.4.4.4.11  2014/03/12 06:32:42  skreddy
 * case 20127606
 * Deal med SB issue fixs
 *
 * Revision 1.7.2.10.4.4.4.10  2014/03/06 15:26:46  skavuri
 * Case # 20127537 issue fixed(now packcode alows only numbers)
 *
 * Revision 1.7.2.10.4.4.4.9  2014/03/04 14:01:58  nneelam
 * case#  20127473
 * Standard Bundle Issue Fix.
 *
 * Revision 1.7.2.10.4.4.4.8  2014/01/28 15:42:27  sponnaganti
 * Case# 20126966
 * Locationfilter added for getting itemstatus
 *
 * Revision 1.7.2.10.4.4.4.7  2014/01/20 15:46:54  gkalla
 * case#20126854�
 * Standard bundle item status issue fix
 *
 * Revision 1.7.2.10.4.4.4.6  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.7.2.10.4.4.4.5  2013/10/03 09:07:44  schepuri
 * After Scanning the LP# it is saying "Invalid custrecord_ebiz_inv_sku_status reference key
 *
 * Revision 1.7.2.10.4.4.4.4  2013/06/24 15:17:39  gkalla
 * CASE201112/CR201113/LOG201121
 * fix of PMME case#20123156
 *
 * Revision 1.7.2.10.4.4.4.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.7.2.10.4.4.4.2  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.7.2.10.4.4.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.7.2.10.4.4  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.7.2.10.4.3  2012/10/05 06:34:12  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting Multiple language into given suggestions
 *
 * Revision 1.7.2.10.4.2  2012/09/26 12:33:02  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.7.2.10.4.1  2012/09/24 10:08:56  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.7.2.10  2012/09/04 14:21:34  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related to Item status populating.
 *
 * Revision 1.7.2.9  2012/08/08 12:57:01  schepuri
 * CASE201112/CR201113/LOG201121
 * default sku status
 *
 * Revision 1.7.2.8  2012/07/06 13:25:37  schepuri
 * CASE201112/CR201113/LOG201121
 * stable bundle  issues
 *
 * Revision 1.7.2.7  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7.2.6  2012/04/23 13:58:47  schepuri
 * CASE201112/CR201113/LOG201121
 * changed getfetchedsitelocation to locationid
 *
 * Revision 1.7.2.5  2012/03/27 09:17:05  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.7.2.4  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.7.2.3  2012/02/20 14:42:41  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to create inventory was fixed.
 *
 * Revision 1.7.2.2  2012/02/14 07:46:01  schepuri
 * CASE201112/CR201113/LOG201121
 * CVS Keyword $Log is added
 *
 *****************************************************************************/
function CreateInventoryItemStatus(request, response)
{
	var getBinLocationId = request.getParameter('custparam_binlocationid');
	var locationId = request.getParameter('custparam_locationId');
	nlapiLogExecution('ERROR', 'locationId', locationId);
	var getBinLocation = request.getParameter('custparam_binlocationname');
	var getItemId = request.getParameter('custparam_itemid');
	var getItem = request.getParameter('custparam_item');
	var getQuantity = request.getParameter('custparam_quantity');
	var getActualBeginDate = request.getParameter('custparam_actualbegindate');
	var getActualBeginTime = request.getParameter('custparam_actualbegintime');
	var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

	var getBatchId = request.getParameter('custparam_BatchId');
	var getBatchNo = request.getParameter('custparam_BatchNo');

	var getTaskType = "";
	nlapiLogExecution('ERROR', 'Item Id', getItemId);

	var ItemDimensionsFilters = new Array();
	ItemDimensionsFilters.push(new nlobjSearchFilter('internalid', 'custrecord_ebizitemdims', 'is', getItemId));

	/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
	ItemDimensionsFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims',null, 'anyof', getItemId));
	/* Up to here */ 
	var ItemDimensionsColumns = new Array();

	ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizitemdims'));
	ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizpackcodeskudim'));

	var ItemDimensionsResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, ItemDimensionsFilters, ItemDimensionsColumns);

	if (ItemDimensionsResults != null) 
	{
		var ItemDimensionsResult = ItemDimensionsResults[0];
		var getFetchedPackCodeId = ItemDimensionsResult.getId();
		var getFetchedPackCode = ItemDimensionsResult.getValue('custrecord_ebizpackcodeskudim');
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/		
		nlapiLogExecution('ERROR', 'getFetchedPackCodeId', getFetchedPackCodeId);
	}
	else
	{
		var getFetchedPackCode = 1;
		//nlapiLogExecution('ERROR', 'getFetchedPackCode', getFetchedPackCode);
		/* Up to here */ 	
	}

	nlapiLogExecution('ERROR', 'Fetched Pack Code', getFetchedPackCode);

	var getFetchedItemStatusId='';
	var getFetchedItemStatus ='';
	var getFetchedSiteLocation = '';
	var getFetchedAdjustmentTypeId='';
	var getFetchedAdjustmentType='';

	// Fetch the item status for the item entered
	var ItemStatusFilters = new Array();
	ItemStatusFilters.push(new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T'));
	//Added on 20Feb 2012 by suman
	//One more filter criteria is added to search the item status depending upon the location.
	if(locationId!=null&&locationId!="")
		ItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', locationId));

	ItemStatusFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));


	var ItemStatusColumns = new Array();

	ItemStatusColumns.push(new nlobjSearchColumn('internalid'));
	ItemStatusColumns.push(new nlobjSearchColumn('name'));
	ItemStatusColumns.push(new nlobjSearchColumn('custrecord_ebizsiteskus'));

	var ItemStatusResults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemStatusFilters, ItemStatusColumns);

	if (ItemStatusResults != null) 
	{
		var ItemStatusResult = ItemStatusResults[0];
		getFetchedItemStatusId = ItemStatusResult.getId();
		getFetchedItemStatus = ItemStatusResult.getValue('name');
		getFetchedSiteLocation = ItemStatusResult.getValue('custrecord_ebizsiteskus');
	}

	nlapiLogExecution('ERROR', 'Fetched Item Status', getFetchedItemStatus);
	nlapiLogExecution('ERROR', 'Fetched Site Location', getFetchedSiteLocation);

	// Fetch the adjustment type for the item entered
	var AdjustmentTypeFilters = new Array();
	AdjustmentTypeFilters.push(new nlobjSearchFilter('custrecord_adjustdefaultflag', null, 'is', 'T'));
	AdjustmentTypeFilters.push(new nlobjSearchFilter('custrecord_adjusttasktype', null, 'is', '10'));//INVT
	AdjustmentTypeFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(locationId!=null && locationId!='')
		AdjustmentTypeFilters.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'is', locationId));//Site Location

	var AdjustmentTypeColumns = new Array();

	AdjustmentTypeColumns.push(new nlobjSearchColumn('internalid'));
	AdjustmentTypeColumns.push(new nlobjSearchColumn('name'));
	AdjustmentTypeColumns.push(new nlobjSearchColumn('custrecord_adjusttasktype'));

	var AdjustmentTypeResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, AdjustmentTypeFilters, AdjustmentTypeColumns);

	if (AdjustmentTypeResults != null) 
	{
		var AdjustmentTypeResult = AdjustmentTypeResults[0];
		getFetchedAdjustmentTypeId = AdjustmentTypeResult.getId();
		getFetchedAdjustmentType = AdjustmentTypeResult.getValue('name');
		getTaskType = AdjustmentTypeResult.getValue('custrecord_adjusttasktype');
	}

	nlapiLogExecution('ERROR', 'Fetched Adjustment Type', getFetchedAdjustmentType);

	// Sonic starts
	var defaultvalue_createInv = getSystemRuleValue('Populate Default values in RF Create Inventory? Y/N',locationId);
	nlapiLogExecution('ERROR', 'defaultvalue_createInv', defaultvalue_createInv);
	// Sonic 
	if (request.getMethod() == 'GET') 
	{
		var getLanguage = request.getParameter('custparam_language');
		var st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES')
		{		
			st1 = "CREAR INVENTARIO";
			st2 = "ESTADO DEL ELEMENTO:";
			st3 = "C&#211;DIGO DE PAQUETE:";
			st4 = "TIPOS DE AJUSTE:";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";

		}
		else
		{
			st1 = "CREATE INVENTORY";
			st2 = "ITEM STATUS:";	
			st3 = "PACK CODE:";
			st4 = "ADJUST TYPE:";
			st5 = "SEND";
			st6 = "PREV";
		}
		var functionkeyHtml=getFunctionkeyScript('_rf_createinvitemstatus');
		var html = "<html><head><title>" + st1 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  

		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('enteritemstatus').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_createinvitemstatus' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + "<label>" + getFetchedItemStatus + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		// sonic
		if(defaultvalue_createInv =='Y')
			html = html + "				<td align = 'left'><input name='enteritemstatus' type='text' value='"+ getFetchedItemStatus +"'/>";
		else
			html = html + "				<td align = 'left'><input name='enteritemstatus' type='text' />";
		// sonic
		html = html + "				<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "				<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnFetchedPackCode' value=" + getFetchedPackCode + ">";
		html = html + "				<input type='hidden' name='hdnFetchedPackCodeId' value=" + getFetchedPackCodeId + ">";
		html = html + "				<input type='hidden' name='hdnFetchedItemStatus' value=" + getFetchedItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnFetchedItemStatusId' value=" + getFetchedItemStatusId + ">";
		html = html + "				<input type='hidden' name='hdnFetchedAdjustmentType' value=" + getFetchedAdjustmentType + ">";
		html = html + "				<input type='hidden' name='hdnFetchedAdjustmentTypeId' value=" + getFetchedAdjustmentTypeId + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnSiteLocation' value=" + getFetchedSiteLocation + ">";
		html = html + "				<input type='hidden' name='hdnLocationName' value=" + locationId+ ">";
		html = html + "				<input type='hidden' name='hdnLocationInternalid' value=" + locationId+ ">";
		html = html + "				<input type='hidden' name='hdnCompname' value=" + locationId + ">";
		html = html + "				<input type='hidden' name='hdnTaskType' value=" + getTaskType + ">";

		html = html + "				<input type='hidden' name='hdnBatchId' value=" + getBatchId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	

		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <label>" + getFetchedPackCode + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//sonic
		if(defaultvalue_createInv =='Y')
			html = html + "				<td align = 'left'><input name='enterpackcode' type='text' value='"+ getFetchedPackCode +"'/>";	
		else
			html = html + "				<td align = 'left'><input name='enterpackcode' type='text'/>";
		//sonic
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " <label>" + getFetchedAdjustmentType + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		// sonic
		if(defaultvalue_createInv =='Y')
			html = html + "				<td align = 'left'><input name='enteradjusttype' type='text' value='"+ getFetchedAdjustmentType +"'/>";	
		else
			html = html + "				<td align = 'left'><input name='enteradjusttype' type='text'/>";
		// sonic
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritemstatus').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var getItemStatus = request.getParameter('enteritemstatus');
		nlapiLogExecution('ERROR', 'New Item Status', getItemStatus);

		var getPackCode = request.getParameter('enterpackcode');
		nlapiLogExecution('ERROR', 'New Pack Code', getPackCode);

		var getAdjustType = request.getParameter('enteradjusttype');
		nlapiLogExecution('ERROR', 'New Adjust Type', getAdjustType);

		var getBinLocationId = request.getParameter('hdnBinLocationId');
		var getBinLocation = request.getParameter('hdnBinLocation');
		var getItemId = request.getParameter('hdnItemId');
		var getItem = request.getParameter('hdnItem');
		var getQuantity = request.getParameter('hdnQuantity');
		nlapiLogExecution('ERROR', 'getQuantity', getQuantity);

		var getFetchedPackCode = request.getParameter('hdnFetchedPackCode');
		var getFetchedPackCodeId = request.getParameter('hdnFetchedPackCodeId');
		var getFetchedItemStatus = request.getParameter('hdnFetchedItemStatus');
		var getFetchedItemStatusId = request.getParameter('hdnFetchedItemStatusId');
		var getFetchedAdjustmentType = request.getParameter('hdnFetchedAdjustmentType');
		var getFetchedAdjustmentTypeId = request.getParameter('hdnFetchedAdjustmentTypeId');

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var locationId = request.getParameter('hdnLocationInternalid');
		var getTaskType = request.getParameter('hdnTaskType');
		nlapiLogExecution('ERROR', 'locationId from prev', locationId);
		var getBatchId = request.getParameter('hdnBatchId');
		var getBatchNo = request.getParameter('hdnBatchNo');

		var packcode;

		var CIarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		CIarray["custparam_language"] = getLanguage;

		nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);


		var st7,st8,st9,st10,st11,st12,st13;
		if( getLanguage == 'es_ES')
		{
			st7 = "ESTADO ELEMENTO NO V&#193;LIDO";
			st8 = "C&#211;DIGO DE PAQUETE NO V&#193;LIDO";
			st9 = "NO V&#193;LIDO AJUSTAR EL TIPO DE";
			st10 = "CANTIDAD ES MAYOR QUE CANTIDAD DE PALETA";
			st11 = "CANTIDAD EXCEDE CUBE UBICACI&#211;N";
			st12 = "POR FAVOR, ENTRE ESTADO TEMA";
			st13 = "POR FAVOR, ENTRE C�DIGO DE PAQUETE";
		}
		else
		{
			st7 = "INVALID ITEM STATUS";
			st8 = "INVALID PACK CODE";
			st9 = "INVALID ADJUST TYPE";
			st10 = "QTY IS GREATER THAN PALLET QTY";
			st11 = "QUANTITY EXCEEDS LOCATION CUBE";
			st12 = "PLEASE ENTER ITEM STATUS";
			st13 = "PLEASE ENTER PACK CODE";
		}

		CIarray["custparam_binlocationid"] = getBinLocationId;
		CIarray["custparam_binlocationname"] = getBinLocation;
		CIarray["custparam_itemid"] = getItemId;
		CIarray["custparam_item"] = getItem;
		CIarray["custparam_quantity"] = getQuantity;
		CIarray["custparam_sitelocation"] = request.getParameter('hdnSiteLocation');;

		CIarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegintimeampm');

		CIarray["custparam_screenno"] = '16';
		CIarray["custparam_locationId"] = locationId;
		CIarray["custparam_TaskType"] = getTaskType;
//		CIarray["custparam_language"] = request.getParameter('hdngetLanguage');

		nlapiLogExecution('ERROR', 'CIarray["custparam_TaskType"]', CIarray["custparam_TaskType"]);

		CIarray["custparam_BatchId"] = getBatchId;
		CIarray["custparam_BatchNo"] = getBatchNo;


		if(getFetchedItemStatusId != null && getFetchedItemStatusId != '')
		{
			CIarray["custparam_itemstatusid"] = getFetchedItemStatusId;
			CIarray["custparam_itemstatus"] = getFetchedItemStatus;
		}
		if(getFetchedPackCodeId != null && getFetchedPackCodeId != '')
		{
			CIarray["custparam_packcodeid"] = getFetchedPackCodeId;
			CIarray["custparam_packcode"] = getFetchedPackCode;
			packcode = getFetchedPackCode;
		}
		if(getFetchedAdjustmentTypeId != null && getFetchedAdjustmentTypeId != '')
		{
			CIarray["custparam_adjustmenttypeid"] = getFetchedAdjustmentTypeId;
			CIarray["custparam_adjustmenttype"] = getFetchedAdjustmentType;
		}



		var TimeArray = new Array();
		CIarray["custparam_actualbegintime"] = getActualBeginTime;
		CIarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;

		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_create_invt_binloc', 'customdeploy_rf_create_invt_binloc_di', false, CIarray);
		}
		else 
		{
			var ErrorFlag='F';
			try 
			{
				if (getItemStatus == null || getItemStatus == '')
				{
					CIarray["custparam_itemstatusid"] = "";
					CIarray["custparam_itemstatus"] = "";
					CIarray["custparam_error"] = st12;
					nlapiLogExecution('DEBUG', 'Item status not fetched', '');
					ErrorFlag='T'; 
				}	
				else if (getItemStatus != null && getItemStatus != '') 
				{
					// Fetch the item status for the item entered
					nlapiLogExecution('ERROR', 'At Item Status', getItemStatus);

					var ItemStatusFilters = new Array();
					ItemStatusFilters.push(new nlobjSearchFilter('name', null, 'is', getItemStatus.toString()));
					//Case# 20126966 starts (location filter added for getting item status)
					if(locationId!=null&&locationId!="")
						ItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', locationId));
					//end Case# 20126966
					var ItemStatusColumns = new Array();

					ItemStatusColumns.push(new nlobjSearchColumn('internalid'));
					ItemStatusColumns.push(new nlobjSearchColumn('name'));
					ItemStatusColumns.push(new nlobjSearchColumn('custrecord_ebizsiteskus'));

					var ItemStatusResults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemStatusFilters, ItemStatusColumns);

					if (ItemStatusResults != null) 
					{
						var ItemStatusResult = ItemStatusResults[0];
						CIarray["custparam_itemstatusid"] = ItemStatusResult.getId();
						CIarray["custparam_itemstatus"] = ItemStatusResult.getValue('name');
						CIarray["custparam_sitelocation"] = ItemStatusResult.getValue('custrecord_ebizsiteskus');



//						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
						nlapiLogExecution('ERROR', 'custparam_itemstatus', CIarray["custparam_itemstatus"]);
					}
					else 
					{
						CIarray["custparam_itemstatusid"] = getFetchedItemStatusId;
						CIarray["custparam_itemstatus"] = getFetchedItemStatus;



						CIarray["custparam_error"] = st7;
						nlapiLogExecution('ERROR', 'Fetched custparam_itemstatus', getFetchedItemStatus);
						ErrorFlag='T';
//						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					}
				}
				//Code commented on 20Feb by suman

//				else 
//				{
//				CIarray["custparam_error"] = 'INVALID ITEM STATUS';
//				nlapiLogExecution('ERROR', 'Fetched custparam_itemstatus', 'Item Status is not found');
//				response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
//				}
				//end of code commented.

				// Fetch the pack code for the item entered
				//case # 20127473 start
				if (getPackCode == null || getPackCode == '')
				{
					CIarray["custparam_itemstatusid"] = "";
					CIarray["custparam_itemstatus"] = "";
					CIarray["custparam_error"] = st13;
					nlapiLogExecution('DEBUG', 'pack code not entered', '');
					ErrorFlag='T'; 
				}//end
				var itemQuantity=0
				if(getItemId !=null && getItemId !='')
				{
					itemQuantity = getPalletQuantityFromItemDims(getItemId, getPackCode);
				}
				nlapiLogExecution('DEBUG', 'itemQuantity', itemQuantity);
				// Case # 20127537 starts
				if (isNaN(getPackCode) || parseFloat(itemQuantity)==0)
				{
					CIarray["custparam_itemstatusid"] = "";
					CIarray["custparam_itemstatus"] = "";
					CIarray["custparam_error"] = st8;
					nlapiLogExecution('DEBUG', 'pack code not a number', '');
					ErrorFlag='T'; 
				}// Case # 20127537 end
				else if (getPackCode != null && getPackCode != '') 
				{
					var ItemDimensionsFilters = new Array();
					ItemDimensionsFilters.push(new nlobjSearchFilter('internalid', 'custrecord_ebizitemdims', 'is', getItemId));

					var ItemDimensionsColumns = new Array();

					ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizitemdims'));
					ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizpackcodeskudim'));

					var ItemDimensionsResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, ItemDimensionsFilters, ItemDimensionsColumns);

					if (ItemDimensionsResults != null) 
					{
						var ItemDimensionsResult = ItemDimensionsResults[0];
						CIarray["custparam_packcodeid"] = ItemDimensionsResult.getId();
						CIarray["custparam_packcode"] = ItemDimensionsResult.getValue('custrecord_ebizpackcodeskudim');

						packcode = ItemDimensionsResult.getValue('custrecord_ebizpackcodeskudim');

//						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
						nlapiLogExecution('ERROR', 'Fetched custparam_packcode', CIarray["custparam_packcode"]);
					}
					else 
					{
						CIarray["custparam_packcodeid"] = getFetchedPackCodeId;
						CIarray["custparam_packcode"] = getFetchedPackCode;
						packcode = getFetchedItemStatus;
						CIarray["custparam_error"] = st8;
						nlapiLogExecution('ERROR', 'Fetched custparam_packcode', getFetchedPackCode);
						ErrorFlag='T';
//						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					}
				}

				//Code commented on 20Feb by suman
//				else 
//				{
//				CIarray["custparam_error"] = 'INVALID PACK CODE';
//				nlapiLogExecution('ERROR', 'Fetched custparam_itemstatus', 'Pack Code is not found');
//				response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
//				}
				//end of code commented.

				// Fetch the adjustment type for the item entered
				if (getAdjustType != null && getAdjustType!="") 
				{
					var AdjustmentTypeFilters = new Array();
					AdjustmentTypeFilters.push(new nlobjSearchFilter('name', null, 'is', getAdjustType));
					AdjustmentTypeFilters.push(new nlobjSearchFilter('custrecord_adjusttasktype', null, 'is', '10'));//INVT
					if(locationId!=null && locationId!='')
						AdjustmentTypeFilters.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'is', locationId));//Site Location
					AdjustmentTypeFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

					var AdjustmentTypeColumns = new Array();

					AdjustmentTypeColumns.push(new nlobjSearchColumn('internalid'));

					var AdjustmentTypeResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, AdjustmentTypeFilters, AdjustmentTypeColumns);

					if (AdjustmentTypeResults != null) 
					{
						var AdjustmentTypeResult = AdjustmentTypeResults[0];
						CIarray["custparam_adjustmenttypeid"] = AdjustmentTypeResult.getId();
						CIarray["custparam_adjustmenttype"] = AdjustmentTypeResult.getValue('name');
						nlapiLogExecution('ERROR', 'Fetched custparam_adjustmenttype', getFetchedAdjustmentType);
						nlapiLogExecution('ERROR', 'CIarray["custparam_adjustmenttypeid"]', CIarray["custparam_adjustmenttypeid"]);
//						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
					}
					else 
					{
						CIarray["custparam_adjustmenttypeid"] = getFetchedAdjustmentTypeId;
						CIarray["custparam_adjustmenttype"] = getFetchedAdjustmentType;
						nlapiLogExecution('ERROR', 'Fetched custparam_adjustmenttype', getFetchedAdjustmentType);
						CIarray["custparam_error"] = st9;
						nlapiLogExecution('ERROR', 'Fetched custparam_adjustmenttype11', getFetchedAdjustmentType);
						nlapiLogExecution('ERROR', 'CIarray["custparam_adjustmenttypeid"]', CIarray["custparam_adjustmenttypeid"]);
						ErrorFlag='T';
						//response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
						nlapiLogExecution('ERROR', 'Fetched custparam_adjustmenttype11333', CIarray["custparam_error"]);
					}
				}

				//Code commented on 20Feb by suman
//				else 
//				{
//				//CIarray["custparam_error"] = 'INVALID ADJUSTMENT TYPE';
//				CIarray["custparam_adjustmenttypeid"] = AdjustmentTypeResult.getId();
//				CIarray["custparam_adjustmenttype"] = getFetchedAdjustmentType;
//				nlapiLogExecution('ERROR', 'Fetched custparam_itemstatus', 'Adjustment Type is not found');
//				response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
//				}
				//end of Code comment.

				var itemQuantity = getPalletQuantityFromItemDims(getItemId, packcode);
				var locationCubeValidate = validateLocationCube(getQuantity, getItemId, getBinLocationId);

				if(parseFloat(getQuantity) > parseFloat(itemQuantity)){

					CIarray["custparam_error"] =  st10;					
					ErrorFlag="T";
				}
				else if(locationCubeValidate == false)
				{
					CIarray["custparam_error"] =  st11;					
					ErrorFlag="T";
				}

				if(ErrorFlag!='T')
				{
					//Case# 201410847 starts
					if(getBatchId != null && getBatchId != '' && getBatchId != 'null')
					{
						//Case# 201410634 starts
						var transaction = nlapiLoadRecord('customrecord_ebiznet_batch_entry', getBatchId);
						if (CIarray["custparam_itemstatusid"] != null && CIarray["custparam_itemstatusid"] != '')
							transaction.setFieldValue('custrecord_ebizskustatus', CIarray["custparam_itemstatusid"]);
						if (CIarray["custparam_packcode"] != null && CIarray["custparam_packcode"] != '') //Case# 201410931
							transaction.setFieldValue('custrecord_ebizpackcode', CIarray["custparam_packcode"]);
						var rec = nlapiSubmitRecord(transaction, false, true);
						//Case# 201410634 ends
					}
					//Case# 201410847 ends
						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
						return;
					
				}
				else
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);


				nlapiLogExecution('ERROR', 'Navigating to LP', 'Success');
			} 
			catch (e) 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}
	}
}

/**
 * This function is to validate the location cube for the item, bin location and the quantity passed
 */
function validateLocationCube(quantity, itemValue, binLocation){

	var cube = 0.0;
	var baseUOMQty = 0.0;
	var remCube = 0.0;
	var itemUomCube = 0.0;
	var anotherItemCube = 0.0;

	var resultQty = quantity;

//	This filters are to fetch the item dimensions for the item and the bin location	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemValue);		    
	filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');
	filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(skuDimsSearchResults != null && skuDimsSearchResults.length > 0){
		for (var i = 0; i < skuDimsSearchResults.length; i++){
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			baseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	if ((!isNaN(cube))){
		var uomqty = parseFloat(quantity)/parseFloat(baseUOMQty);
		itemUomCube = parseFloat(uomqty) * parseFloat(cube);
		var itemCube = itemUomCube.toString();

	} else {
		itemUomCube = 0;
		itemCube=0;
	}

	var LocRemCube = GetLocCube(binLocation);

	var locRem = LocRemCube.toString();

	remCube = parseFloat(locRem) - parseFloat(itemCube);

	var remainingCube = remCube.toString();


	if (binLocation != null && binLocation != "" && remainingCube >= 0){
		return true;
	}
	else{
		return false;
	}
}

/**
 * @param LocID
 * @returns {Number}
 */
function GetLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('DEBUG', 'GetLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('DEBUG', 'GetLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}


function getPalletQuantityFromItemDims(itemId, packCode){
	var itemQuantity = 0;
	var filters = new Array();          
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is',itemId);
//	filters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', [3]);		  
	filters[1] = new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',packCode);
	filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true);//it fetches the records with highest uomlevel first.

	var searchItemResults = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);
	if(searchItemResults!=null){    		
		itemQuantity = searchItemResults[0].getValue('custrecord_ebizqty');  
	}    	

	return itemQuantity;	
}
