/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_CheckInPO.js,v $
 *     	   $Revision: 1.8.2.8.4.5.2.10.2.1 $
 *     	   $Date: 2015/11/16 16:41:18 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInPO.js,v $
 * Revision 1.8.2.8.4.5.2.10.2.1  2015/11/16 16:41:18  aanchal
 * 2015.2 Issue fix
 * 201415683
 *
 * Revision 1.8.2.8.4.5.2.10  2014/10/16 14:58:32  skavuri
 * Case# 201410723 Std Bundle issue fixed
 *
 * Revision 1.8.2.8.4.5.2.9  2014/09/19 10:00:25  snimmakayala
 * no message
 *
 * Revision 1.8.2.8.4.5.2.8  2014/08/07 06:09:37  skreddy
 * case # 20149717
 * One Industries SB issue fix
 *
 * Revision 1.8.2.8.4.5.2.7  2014/06/20 14:57:33  skavuri
 * Case # 20148882 SB Issue Fixed
 *
 * Revision 1.8.2.8.4.5.2.6  2014/06/13 07:08:01  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.8.2.8.4.5.2.5  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.8.2.8.4.5.2.4  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.8.2.8.4.5.2.3  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.8.2.8.4.5.2.2  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.8.2.8.4.5.2.1  2013/02/27 13:06:28  rmukkera
 * monobid production bundle changes were merged to cvs with tag
 * t_eBN_2013_1_StdBundle_2
 *
 * Revision 1.8.2.8.4.5  2013/02/12 02:20:49  kavitha
 * CASE201112/CR201113/LOG201121
 * LexJet - Location level restriction
 *
 * Revision 1.8.2.8.4.4  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.8.2.8.4.3  2012/09/27 13:13:26  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.8.2.8.4.2  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.8.2.8.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.8.2.8  2012/06/18 07:34:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.8.2.7  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8.2.6  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.8.2.5  2012/05/03 14:36:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Checkin changes
 *
 * Revision 1.8.2.3  2012/03/20 16:18:38  gkalla
 * CASE201112/CR201113/LOG201121
 * changed the code to restrict pending approval and closed status POs
 *
 * Revision 1.8.2.2  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.8.2.1  2012/02/28 01:33:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin issue fixes
 *
 * Revision 1.9  2012/02/28 01:24:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin issue fixes
 *
 * Revision 1.8  2011/12/12 09:06:53  rgore
 * CASE201112/CR201113/LOG201121
 * Modularized code to call method from data access library.
 * - Ratnakar
 * 12 Dec 2011
 *
 * Revision 1.7  2011/09/12 08:24:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/07/07 09:15:42  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/04/29 16:02:38  skota
 * CASE201112/CR201113/LOG201121
 * Added function key functionality
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/19 09:40:19  vpasula
 * CASE2009936/CR200912052/LOG20093196
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function CheckInPO(request, response)
{
	if (request.getMethod() == 'GET') 
	{

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);


		var st0,st1,st2,st3;

		if( getLanguage == 'es_ES')
		{
			st0 = "CHECK-IN POR PALLET";
			st1 = "INTRODUZCA ORDEN DE COMPRA";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";	

		}
		else
		{
			st0 = "CHECK-IN BY PALLET";
			st1 = "ENTER/SCAN PO#";
			st2 = "SEND";
			st3 = "PREV";

		}		

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		//customscript_rf_general_script  
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript'>";
		html = html + "function OnKeyDown_CL() ";
		html = html + " { ";     
		html = html + "         if (";
		html = html + " event.keyCode == 112 || event.keyCode == 113 || event.keyCode == 114 || event.keyCode == 115 || event.keyCode == 116 || event.keyCode == 117 ||";
		html = html + " event.keyCode == 118 || event.keyCode == 119 || event.keyCode == 120 || event.keyCode == 121 || event.keyCode == 122) {";
		html = html + " var arrElements = document.getElementsByTagName('input');";
		html = html + " var keyFound = false;";
		html = html + " for (i = 0; i < arrElements.length; i++) {";
		html = html + " if (arrElements[i].type == 'submit') {";
		html = html + " switch (event.keyCode) {";
		html = html + " case 112:";
		html = html + " if (arrElements[i].value == 'F1')";	//F7 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 113:";
		html = html + " if (arrElements[i].value == 'F2')";		//F8 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 114:";
		html = html + " if (arrElements[i].value == 'F3')";	//F9 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 115:";
		html = html + " if (arrElements[i].value == 'F4')";		//F10 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 116:";
		html = html + " if (arrElements[i].value == 'F5')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 117:";
		html = html + " if (arrElements[i].value == 'F6')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 118:";
		html = html + " if (arrElements[i].value == 'F7')";		//F7 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 119:";
		html = html + " if (arrElements[i].value == 'F8')";		//F8 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 120:";
		html = html + " if (arrElements[i].value == 'F9')";		//F9 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 121:";
		html = html + " if (arrElements[i].value == 'F10')";		//F10 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 122:";
		html = html + " if (arrElements[i].value == 'F11')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " }";
		html = html + " if (keyFound == true) {";        
		html = html + " eval('document._rf_checkin_po.' + arrElements[i].name + '.click();');";
		html = html + " return false;";
		html = html + " }";
		html = html + " }";
		html = html + " }";
		html = html + " }    ";        
		html = html + "    return true; ";
		html = html + "    }";        
		html = html + " </SCRIPT>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enterpo').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);";

		//html = html + "	  alert(evt.keyCode);";
		//html = html + "	  alert(node.type);";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html + " </head>";
		html = html + "<body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_po' method='POST'>"; //onkeydown='return OnKeyDown_CL()' >";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterpo' id='enterpo' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterpo').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Processing RF', 'Validating PO');

		var ActualBeginDate = DateStamp();
		var ActualBeginTime = TimeStamp();
		var TimeArray = new Array();
		TimeArray = ActualBeginTime.split(' ');

		// Determine if 'F7' is clicked, in order to navigate to the previous screen
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the PO# entered.
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			st4 = "NO V&#193;LIDO PO";
			st5 = "ESTADO INV&#193;LIDO PO";
		}
		else
		{
			st4 = "INVALID PO";
			st5 = "INVALID PO STATUS";
		}

		POarray["custparam_poid"] = request.getParameter('enterpo');
		POarray["custparam_error"] = st4;
		POarray["custparam_screenno"] = '1';
		POarray["custparam_option"] = request.getParameter('hdnOptenField');
		POarray["custparam_actualbegindate"] = ActualBeginDate;
		POarray["custparam_actualbegintime"] = TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = TimeArray[1];

		var message = 'PO ID = ' + POarray["custparam_poid"] + '<br>';
		message = message + 'OptedField = ' + POarray["custparam_option"] + '<br>';
		message = message + 'ActualBeginDate = ' + POarray["custparam_actualbegindate"] + '<br>';
		message = message + 'ActualBeginTime = ' + POarray["custparam_actualbegintime"] + '<br>';
		message = message + 'ActualBeginTimeAMPM = ' + POarray["custparam_actualbegintimeampm"] + '<br>';
		message = message + 'OptedEvent = ' + optedEvent;

		nlapiLogExecution('DEBUG', 'RF - PO SCREEN', message);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
		}
		else 
		{
			var trantype='';
			var getPOid=request.getParameter('enterpo');
			nlapiLogExecution('DEBUG','getPOid',getPOid);
			var POtrantypefilters=new Array();
			POtrantypefilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
			POtrantypefilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

			var POtrantypecols=new Array();
			POtrantypecols[0]=new nlobjSearchColumn('internalid');
			POtrantypecols[1]=new nlobjSearchColumn('tranid');

			var PORecinternalids=nlapiSearchRecord('purchaseorder',null,POtrantypefilters,POtrantypecols);
			var poid='';
			if(PORecinternalids!=null && PORecinternalids!='')
			{
				poid=PORecinternalids[0].getValue('internalid');
				POarray["custparam_poid"] = PORecinternalids[0].getValue('tranid');
				//}
				if(poid!=null&&poid!="")
					trantype = nlapiLookupField('transaction', poid, 'recordType');
				nlapiLogExecution('DEBUG','trantype',trantype);
				POarray["custparam_trantype"] = trantype;
				var POfilters=new Array();
				POfilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
				POfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
				POfilters.push(new nlobjSearchFilter('recordtype',null,'is','purchaseorder'));
				var vRoleLocation=getRoledBasedLocation();
				if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
				{
					POfilters.push(new nlobjSearchFilter('location', null, 'anyof', vRoleLocation));
				}


				var POcols=new Array();
				POcols[0]=new nlobjSearchColumn('status');
				POcols[1]=new nlobjSearchColumn('location');
//				POcols[2]= new nlobjSearchColumn('custbody_nswms_company');

				var PORec=nlapiSearchRecord('transaction',null,POfilters,POcols);
				if(PORec!=null&&PORec!='')
				{
					var poStatus=PORec[0].getValue('status');
					nlapiLogExecution('DEBUG','poStatus',poStatus);
					var poToLocationID=PORec[0].getValue('location');
					nlapiLogExecution('DEBUG','poToLocation',poToLocationID);
					var Tomwhsiteflag='F';
					if(poToLocationID !=null && poToLocationID!='')
					{
						var fields = ['custrecord_ebizwhsite'];
						var locationcolumns = nlapiLookupField('Location', poToLocationID, fields);
						Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;

						// Call method to retrieve the PO based on the PO ID
					}
					nlapiLogExecution('DEBUG','Tomwhsiteflag',Tomwhsiteflag);
					if(poStatus=='pendingReceipt'||poStatus=='partiallyReceived' ||poStatus=='pendingBillPartReceived')
					{
						if(Tomwhsiteflag=='T')
						{
							var poSearchResults = eBiz_RF_GetPOListForTranID(POarray["custparam_poid"],trantype);

							if(poSearchResults != null && poSearchResults.length > 0){
								if (POarray["custparam_poid"] != null) 
								{
									var whLocation= poSearchResults[0].getValue('location');
									var whCompany= poSearchResults[0].getValue('custbody_nswms_company');
									POarray["custparam_whlocation"] = whLocation;
									POarray["custparam_company"] =whCompany;

									message = 'whLocation = ' + whLocation + '<br>';
									message = message + 'whCompany = ' + whCompany;
									nlapiLogExecution('DEBUG', 'PO Details', message);
									//Case# 201410723 starts 
									var filters = new Array();
									filters.push(new nlobjSearchFilter ('tranid', null, 'is', POarray["custparam_poid"]));
									filters.push(new nlobjSearchFilter ('mainline', null, 'is', 'F'));
									var columns = new Array();
									columns.push(new nlobjSearchColumn('custcol_nswmspackcode'));
									var poPackcodeResults = nlapiSearchRecord(trantype, null, filters, columns);
									if(poPackcodeResults !='' && poPackcodeResults!=null && poPackcodeResults !='null')
									{
										var packcode= poPackcodeResults[0].getValue('custcol_nswmspackcode');
										POarray["custparam_packcode"] =packcode;
										nlapiLogExecution('DEBUG', 'POarray["custparam_packcode"]', POarray["custparam_packcode"]);
									}
									//Case# 201410723 ends
									// Redirecting the control to the next screen - SCAN SKU
									response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false, POarray);
								}
								else 
								{
									//	if the 'Send' button is clicked without any option value entered,
									//  it has to remain in the same screen ie., Receiving Menu.
									nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								}
							}
							else 
							{
								nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
						}
						else
						{
							nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);			
						}
					}
					else
					{
						POarray["custparam_error"] = st5;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
					}
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
				}
				/* code merged from monobid on feb 27th 2013 by Radhika */

			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
			}
		}
	}
	//end of else loop.



}

