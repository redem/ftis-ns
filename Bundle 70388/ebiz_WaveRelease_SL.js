/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_WaveRelease_SL.js,v $
 *     	   $Revision: 1.1.4.8.2.41.2.5 $
 *     	   $Date: 2015/10/23 23:20:26 $
 *     	   $Author: snimmakayala $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveRelease_SL.js,v $
 * Revision 1.1.4.8.2.41.2.5  2015/10/23 23:20:26  snimmakayala
 * 201415191
 *
 * Revision 1.1.4.8.2.41.2.4  2015/10/15 10:20:24  skreddy
 * case:201414992
 * KRM shipcomplete issue fix
 *
 * Revision 1.1.4.8.2.41.2.3  2015/10/09 07:29:08  skreddy
 * case:201414627
 * Briggs issue fix
 *
 * Revision 1.1.4.8.2.41.2.2  2015/10/06 13:41:03  schepuri
 * case# 201414874
 *
 * Revision 1.1.4.8.2.41.2.1  2015/09/11 14:09:10  schepuri
 * case# 201414310
 *
 * Revision 1.1.4.8.2.41  2015/08/25 10:18:44  schepuri
 * case# 201414116
 *
 * Revision 1.1.4.8.2.40  2015/08/07 19:06:05  snimmakayala
 * Case#: 201413918
 *
 * Revision 1.1.4.8.2.39  2015/07/29 15:52:54  nneelam
 * case# 201413677
 *
 * Revision 1.1.4.8.2.38  2015/07/28 16:32:25  grao
 * 2015.2   issue fixes  201413054
 *
 * Revision 1.1.4.8.2.37  2015/07/28 13:36:49  schepuri
 * case# 201413334
 *
 * Revision 1.1.4.8.2.36  2015/07/22 13:21:05  schepuri
 * case# 201413334
 *
 * Revision 1.1.4.8.2.35  2015/07/13 07:42:54  snimmakayala
 * Case#: 201413420
 * Wave List and Order List options.
 *
 * Revision 1.1.4.8.2.34  2015/05/07 13:25:51  schepuri
 * case #201412685
 *
 * Revision 1.1.4.8.2.33  2015/04/27 22:57:48  sponnaganti
 * Case# 201412513
 * While validating search criteria in if condition values checking wrongly.
 *
 * Revision 1.1.4.8.2.32  2015/03/16 07:53:31  snimmakayala
 * 201411870
 *
 * Revision 1.1.4.8.2.31  2015/02/04 07:35:14  sponnaganti
 * Case# 201411508
 * True Fab outbound process for selected lot in so/to
 *
 * Revision 1.1.4.8.2.30  2015/01/29 07:27:41  snimmakayala
 * Case#: 201411473
 *
 * Revision 1.1.4.8.2.29  2014/11/21 15:13:49  grao
 * Case#:201411099 PMM Issue Fixes(Added Total# of Order Qty field in Sublist
 *
 * Revision 1.1.4.8.2.28  2014/11/14 07:49:17  rrpulicherla
 * Std fixes
 *
 * Revision 1.1.4.8.2.27  2014/11/05 10:38:15  sponnaganti
 * Case# 201410941
 * True Fab SB Issue fixed
 *
 * Revision 1.1.4.8.2.26  2014/09/17 16:06:12  sponnaganti
 * Case# 201410403
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.4.8.2.25  2014/09/05 09:04:52  sponnaganti
 * case# 201410245
 * Stnd Bundle issue fix
 *
 * Revision 1.1.4.8.2.24  2014/08/05 15:26:39  sponnaganti
 * Case# 20149816
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.4.8.2.23  2014/07/22 06:33:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149462
 *
 * Revision 1.1.4.8.2.22  2014/07/11 07:07:21  gkalla
 * case#20148894
 * Surftech  Wave generation concurrency issue fix
 *
 * Revision 1.1.4.8.2.21  2014/06/27 07:13:01  sponnaganti
 * case# 20149120
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.4.8.2.20  2014/06/12 14:41:54  grao
 * Case#: 20148784  New GUI account issue fixes
 *
 * Revision 1.1.4.8.2.19  2014/05/30 12:35:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148577
 *
 * Revision 1.1.4.8.2.18  2014/05/28 06:37:39  skavuri
 * Case # 20148579 SB Issue Fixed
 *
 * Revision 1.1.4.8.2.17  2014/05/27 07:36:58  snimmakayala
 * Case#: 20148135
 * Schedule Script Status
 *
 * Revision 1.1.4.8.2.16  2014/05/22 15:37:07  nneelam
 * case#  20148456
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.4.8.2.15  2014/05/22 06:48:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148481
 *
 * Revision 1.1.4.8.2.14  2014/04/29 06:18:10  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201219412
 *
 * Revision 1.1.4.8.2.13  2014/03/19 09:08:44  snimmakayala
 * Case #: 20127688
 *
 * Revision 1.1.4.8.2.12  2014/01/20 13:40:26  snimmakayala
 * Case# : 20126712
 *
 * Revision 1.1.4.8.2.11  2013/12/24 15:24:38  skreddy
 * case#20126502
 * TPP SB issue fix
 *
 * Revision 1.1.4.8.2.10  2013/12/05 11:55:36  snimmakayala
 * no message
 *
 * Revision 1.1.4.8.2.9  2013/09/16 15:42:01  rmukkera
 * Case# 20124327
 *
 * Revision 1.1.4.8.2.8  2013/09/12 15:34:47  rmukkera
 * Case# 20124327
 *
 * Revision 1.1.4.8.2.7  2013/09/03 00:05:13  nneelam
 * Case#.20124236
 * Wave generate and release Issue Fix.
 *
 * Revision 1.1.4.8.2.6  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.4.8.2.5  2013/05/21 07:27:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.4.8.2.4  2013/04/30 23:30:47  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.4.8.2.3  2013/03/21 14:18:14  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.4.8.2.2  2013/03/19 11:45:09  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.4.8.2.1  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.4.8  2013/02/20 15:40:09  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.1.4.7  2013/02/16 12:11:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.1.4.6  2013/01/09 15:20:02  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Order by Ship date.
 * .
 *
 * Revision 1.1.4.5  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.1.4.4  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.4.3  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.1.4.2  2012/10/07 23:33:55  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Wave Selection by Order changes.
 *
 *
 *
 **********************************************************************************************************************/


/**
 * 
 * @param request
 * @param response
 */
function WaveRelease(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Wave Creation & Release');
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');	
		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
		var OrdersCount=form.addField('custpage_orderscount','text', 'Orders Count');//.setDisabled(true).setDefaultValue(parseInt(0));
		OrdersCount.setDefaultValue(parseInt(0));
		OrdersCount.setLayoutType('outsidebelow', 'startrow');
		OrdersCount.setDisabled(true);
		OrdersCount.setDisplaySize(50,30);
		var OrdersLineCount=form.addField('custpage_orderslinecount','text', 'Order Lines Count');//.setDisabled(true).setDefaultValue(parseInt(0));
		OrdersLineCount.setDefaultValue(parseInt(0));
		OrdersLineCount.setLayoutType('outsidebelow', 'startcol');
		OrdersLineCount.setDisabled(true);
		OrdersLineCount.setDisplaySize(50,30);
		form.setScript('customscript_wavecreationrelease_cl');
		var empassingedtowave = form.addField('custpage_empassigntowave', 'select', 'Wave Assigned To','Employee');
		// Create the sublist
		//createWaveGenSublist(form);
		var orderList = getOrdersForWaveGen(request,null,form,null);
		//Add two functions
		var vDueDaysList=getDueDaysList();
		var vCreditExceedList=getCreditExceedList();

		var vConfig=nlapiLoadConfiguration('accountingpreferences');
		var vDueDaysRestrict;
		if(vConfig != null && vConfig != '')
		{
			vDueDaysRestrict=vConfig.getFieldValue('CREDLIMDAYS');
		}
		if(orderList != null && orderList.length > 0){
			setPagingForSublist(orderList,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,request);
		}
		form.addSubmitButton('Generate Wave');
		response.writePage(form);
	}
	else{
		generateWave(request, response);
	}
}

/**
 * Create the sublist that will display the items for wave generation
 * 
 * @param form
 */
function createWaveGenSublist(form){
	var sublist = form.addSubList("custpage_items", "list", "ItemList");	
	sublist.addField("custpage_so", "checkbox", "Confirm").setDefaultValue('F');
	sublist.addField("custpage_sono", "text", "Fulfillment Order #");
	sublist.addField("custpage_lineno", "text", "# of Lines Selected");
	sublist.addField("custpage_totallines", "text", "Total # of Lines");//.setDisplayType('hidden');//santosh
	//sublist.addField("custpage_totalorderqty", "text", "Total Order Qty");
	sublist.addField("custpage_totalorderqty", "float", "Total Order Qty");//cae# 201412685
	sublist.addField("custpage_customer", "text", "Customer");
	sublist.addField("custpage_carrier", "text", "Shipping Method");
	sublist.addField("custpage_ordtype", "text", "Order Type");
	sublist.addField("custpage_ordpriority", "text", "Order Priority");
	sublist.addField("custpage_ordcrtddate", "date", "Order Date");
	sublist.addField("custpage_shipdate", "date", "Ship Date");//santosh
	sublist.addField("custpage_shipmentno", "text", "Shipment #");//case# 201413334
	sublist.addField("custpage_linestatus", "text", "Order Line Status").setDisplayType('hidden');
	sublist.addField("custpage_paymenthold", "text", "Notes");
	sublist.addField("custpage_shipcomplete", "text", "Ship Complete").setDisplayType('hidden');
	sublist.addMarkAllButtons();
}

var searchResultArray=new Array();

function getOrdersForWaveGen(request,maxno,form,maxordno,waveno){
	// Validating all the request parameters and pushing to a local array
	var localVarArray;
	//this is to maintain the querystring parameters while refreshing the paging drop down
	if (request.getMethod() == 'POST') {
		var queryparams=request.getParameter('custpage_qeryparams');
		var tempArray=new Array();
		tempArray.push(queryparams.split(','));
		localVarArray=tempArray;
	}
	else
	{
		localVarArray = validateRequestParams(request,form);
	}

	// Get all the search filters for wave generation
	var filters = new Array();   
	filters = specifyWaveFilters(localVarArray,maxno,maxordno,waveno);
	// Get all the columns that the search should return
	var columns = new Array();
	columns = getWaveColumns(localVarArray);

	var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	if(orderList!=null && orderList!='')
	{


		var setObj = orderList.filter(function(obj){
			nlapiLogExecution('DEBUG','test3',obj.getValue('shipstate','custrecord_ns_ord','group'));
			return obj.getValue('shipstate','custrecord_ns_ord','group') == localVarArray[0][16]; //modify using parametersval array
			//if searchresult > shipstate column == VIC
		});

		nlapiLogExecution('DEBUG','test4',setObj);
		if((setObj==null || setObj=='') && (localVarArray[0][16]==null || localVarArray[0][16]==''))
			setObj=orderList;

		if( setObj!=null && setObj.length>=1000)
		{ 
			searchResultArray.push(setObj); 
			var maxordno1=orderList[orderList.length-1].getValue(columns[8]);
			getOrdersForWaveGen(request,null,form,maxordno1);	
		}
		else
		{
			searchResultArray.push(setObj); 	
		}
		return searchResultArray;
	}
}

/**
 * Function to add one fulfillment order to the sublist on the form
 * 
 * @param form
 * @param currentOrder
 * @param i
 */
function addFulfilmentOrderToSublist(form, currentOrder, i,orderList,vDueDaysList,vCreditExceedList,vDueDaysRestrict,request){

	var vCarrier='';
	var wmsstatusflag='';
	var vnotes = ''; 

	vCarrier=currentOrder.getText('custrecord_do_carrier',null,'group');

	form.getSubList('custpage_items').setLineItemValue('custpage_shipcomplete', i + 1, 
			currentOrder.getValue('custrecord_shipcomplete',null,'group'));

	form.getSubList('custpage_items').setLineItemValue('custpage_customer', i + 1,
			currentOrder.getText('custrecord_do_customer',null,'group'));

	form.getSubList('custpage_items').setLineItemValue('custpage_carrier', i + 1,
			vCarrier);

	form.getSubList('custpage_items').setLineItemValue('custpage_ordtype', i + 1,
			currentOrder.getText('custrecord_do_order_type',null,'group'));

	form.getSubList('custpage_items').setLineItemValue('custpage_ordpriority', i + 1, 
			currentOrder.getText('custrecord_do_order_priority',null,'group'));

	form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1, 
			currentOrder.getValue('custrecord_lineord',null,'group'));

	form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1, 
			currentOrder.getValue('custrecord_ordline',null,'count'));

	if(currentOrder.getValue('custrecord_ebiz_orderdate',null,'group') !=null 
			&& currentOrder.getValue('custrecord_ebiz_orderdate',null,'group')!='')
	{
		form.getSubList('custpage_items').setLineItemValue('custpage_ordcrtddate', i + 1, 
				currentOrder.getValue('custrecord_ebiz_orderdate',null,'group'));
	}
	else
	{
		form.getSubList('custpage_items').setLineItemValue('custpage_ordcrtddate', i + 1, 
				currentOrder.getValue('custrecord_record_linedate',null,'group'));
	}

	if (request.getParameter('custpage_wmsstatusflag')!=null && request.getParameter('custpage_wmsstatusflag')!="" ){
		wmsstatusflag=request.getParameter('custpage_wmsstatusflag'); 
	}

	form.getSubList('custpage_items').setLineItemValue('custpage_linestatus', i + 1, 
			wmsstatusflag);	

	form.getSubList('custpage_items').setLineItemValue('custpage_shipdate', i + 1, 
			currentOrder.getValue('custrecord_ebiz_shipdate',null,'group'));

	form.getSubList('custpage_items').setLineItemValue('custpage_totallines', i + 1,
			currentOrder.getValue('custrecord_ordline',null,'count'));

	form.getSubList('custpage_items').setLineItemValue('custpage_totalorderqty', i + 1,
			currentOrder.getValue('custrecord_ord_qty',null,'sum'));

	form.getSubList('custpage_items').setLineItemValue('custpage_shipmentno', i + 1,
			currentOrder.getValue('custrecord_shipment_no',null,'group'));

	var vCustomer=currentOrder.getValue('entity','custrecord_ns_ord','group');
	var vStatus = currentOrder.getValue('status','custrecord_ns_ord','group');

	if(vStatus == 'closed' || vStatus == 'cancelled')
	{
		vnotes = vStatus.toUpperCase();
		form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, '<font color=#ff0000><b>' +  vnotes + '</b></font>');
	}
	else
	{
		if(vnotes == 'HOLD')
			form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, '<font color=#ff0000><b>' +  vnotes + '</b></font>');	//Payment hold status
		else if((vnotes == null || vnotes == '') && (vDueDaysList != null && vDueDaysList != '') || (vCreditExceedList != null && vCreditExceedList != ''))
		{
			if(vDueDaysRestrict != null && vDueDaysRestrict != '' && parseInt(vDueDaysRestrict)>0 && vDueDaysList!=null && vDueDaysList!='')
			{
				for(var k=0;k<vDueDaysList.length;k++)
				{
					if(vDueDaysList[k].getValue('internalid',null,'group')==vCustomer)
					{
						vnotes='Days Over Due';
						form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, '<font color=#ff0000><b>' +  vnotes + '</b></font>');	//Payment hold status
						return;
					}	
				}
			}
			//case# 20124236?,if condition added.
			if(vCreditExceedList!=null && vCreditExceedList!=''){
				for(var k=0;k<vCreditExceedList.length;k++)
				{				
					if(vCreditExceedList[k].getValue('internalid',null,'group')==vCustomer)
					{
						vnotes='Credit Limit Exceed';
						form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, '<font color=#ff0000><b>' +  vnotes + '</b></font>');	//Payment hold status
						return;
					}	
				} 	

			}

		}
		else
			form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, vnotes);
	}
}

function validateRequestParams(request,form){
	var soNo = "";
	var companyName = "";
	var itemName= "";
	var customerName = "";
	var orderPriority = "";
	var orderType = "";
	var itemGroup = "";
	var itemFamily = "";
	var packCode = "";
	var UOM = "";
	var itemStatus = "";
	var itemInfo1 = "";
	var itemInfo2 = "";
	var itemInfo3 = "";
	var shippingCarrier = "";
	var shipCountry = ""; 
	var shipState = "";
	var shipCity = "";
	var shipAddr1 = "";
	var shipmentNo = "";
	var routeNo = "";
	var carrier = "";
	var shipdate = "";
	var shiptodate="";
	var ordertodate="";
	var Soid="";
	var wmsstatusflag = "",orderdate="";
	var LinesFrom="";
	var LinesTo="";
	var backorder='';
	var normalorder='';
	var orderselection="";
	var trantype="";
	var location="";
	var localVarArray = new Array();

	if (request.getParameter('custpage_site')!=null && request.getParameter('custpage_site')!="" ){
		location=request.getParameter('custpage_site'); 
	}

	if (request.getParameter('custpage_qbso') != null && request.getParameter('custpage_qbso') != "") {
		soNo = request.getParameter('custpage_qbso');
	}

	if (request.getParameter('custpage_company') != null && request.getParameter('custpage_company') != "") {
		companyName = request.getParameter('custpage_company');
	}

	if (request.getParameter('custpage_item') != null && request.getParameter('custpage_item') != "") {
		itemName = request.getParameter('custpage_item');
	}

	if (request.getParameter('custpage_consignee') != null && request.getParameter('custpage_consignee') != "") {
		customerName = request.getParameter('custpage_consignee');
	}

	if (request.getParameter('custpage_ordpriority') != null && request.getParameter('custpage_ordpriority') != "") {
		orderPriority = request.getParameter('custpage_ordpriority');
	}

	if (request.getParameter('custpage_ordtype') != null && request.getParameter('custpage_ordtype') != "") {
		orderType = request.getParameter('custpage_ordtype');
	}

	if (request.getParameter('custpage_itemgroup') != null && request.getParameter('custpage_itemgroup') != "") {
		itemGroup = request.getParameter('custpage_itemgroup');
	}

	if (request.getParameter('custpage_itemfamily') != null && request.getParameter('custpage_itemfamily') != "") {
		itemFamily = request.getParameter('custpage_itemfamily');
	}

	if (request.getParameter('custpage_packcode') != null && request.getParameter('custpage_packcode') != "") {
		packCode = request.getParameter('custpage_packcode');
	}

	if (request.getParameter('custpage_uom') != null && request.getParameter('custpage_uom') != "") {
		UOM = request.getParameter('custpage_uom');
	}

	if (request.getParameter('custpage_itemstatus') != null && request.getParameter('custpage_itemstatus') != "") {
		itemStatus = request.getParameter('custpage_itemstatus');
	}

	if (request.getParameter('custpage_siteminfo1')!=null && request.getParameter('custpage_siteminfo1')!="" ){
		nlapiLogExecution('DEBUG','request.getParameter(custpage_siteminfo1)',request.getParameter('custpage_siteminfo1'));
		itemInfo1=request.getParameter('custpage_siteminfo1'); 
	}

	if (request.getParameter('custpage_siteminfo2')!=null && request.getParameter('custpage_siteminfo2')!="" ){
		itemInfo2=request.getParameter('custpage_siteminfo2'); 
	}

	if (request.getParameter('custpage_siteminfo3')!=null && request.getParameter('custpage_siteminfo3')!="" ){
		itemInfo3=request.getParameter('custpage_siteminfo3'); 
	}

	if (request.getParameter('custpage_shippingcarrier')!=null && request.getParameter('custpage_shippingcarrier')!="" ){
		shippingCarrier=request.getParameter('custpage_shippingcarrier'); 
	}

	if (request.getParameter('custpage_country')!=null && request.getParameter('custpage_country')!="" ){
		shipCountry=request.getParameter('custpage_country'); 
	}

	if (request.getParameter('custpage_state')!=null && request.getParameter('custpage_state')!="" ){
		shipState=request.getParameter('custpage_state'); 
	}

	if (request.getParameter('custpage_city')!=null && request.getParameter('custpage_city')!="" ){
		shipCity=request.getParameter('custpage_city'); 
	}

	if (request.getParameter('custpage_addr1')!=null && request.getParameter('custpage_addr1')!="" ){
		shipAddr1=request.getParameter('custpage_addr1'); 
	}

	if (request.getParameter('custpage_shipmentno')!=null && request.getParameter('custpage_shipmentno')!="" ){
		shipmentNo=request.getParameter('custpage_shipmentno'); 
	}

	if (request.getParameter('custpage_routeno')!=null && request.getParameter('custpage_routeno')!="" ){
		routeNo=request.getParameter('custpage_routeno'); 
	}

	if (request.getParameter('custpage_carrier')!=null && request.getParameter('custpage_carrier')!="" ){
		carrier=request.getParameter('custpage_carrier'); 
	}

	if (request.getParameter('custpage_soshipdate')!=null && request.getParameter('custpage_soshipdate')!="" ){
		shipdate=request.getParameter('custpage_soshipdate'); 
	}
	if (request.getParameter('custpage_soshiptodate')!=null && request.getParameter('custpage_soshiptodate')!="" ){
		shiptodate=request.getParameter('custpage_soshiptodate'); 
	}
	if (request.getParameter('custpage_wmsstatusflag')!=null && request.getParameter('custpage_wmsstatusflag')!="" ){
		wmsstatusflag=request.getParameter('custpage_wmsstatusflag'); 
	}
	if (request.getParameter('custpage_soorderdate')!=null && request.getParameter('custpage_soorderdate')!="" ){
		orderdate=request.getParameter('custpage_soorderdate'); 
	}
	if (request.getParameter('custpage_soordertodate')!=null && request.getParameter('custpage_soordertodate')!="" ){
		ordertodate=request.getParameter('custpage_soordertodate'); 
	}


	if (request.getParameter('custpage_linesfrom')!=null && request.getParameter('custpage_linesfrom')!="" ){
		LinesFrom=request.getParameter('custpage_linesfrom'); 
	}
	if (request.getParameter('custpage_linesto')!=null && request.getParameter('custpage_linesto')!="" ){
		LinesTo=request.getParameter('custpage_linesto'); 
	}

	if (request.getParameter('custpage_backorder')!=null && request.getParameter('custpage_backorder')!="" ){
		backorder=request.getParameter('custpage_backorder'); 
	}
	if (request.getParameter('custpage_orderselection')!=null && request.getParameter('custpage_orderselection')!="" ){
		orderselection=request.getParameter('custpage_orderselection'); 
	}

	if (request.getParameter('custpage_sono')!=null && request.getParameter('custpage_sono')!="" ){
		Soid=request.getParameter('custpage_sono'); 
	}
	if (request.getParameter('custpage_normalorder')!=null && request.getParameter('custpage_normalorder')!="" ){
		normalorder=request.getParameter('custpage_normalorder'); 
	}

	if (request.getParameter('custpage_trantype')!=null && request.getParameter('custpage_trantype')!="" ){
		trantype=request.getParameter('custpage_trantype'); 
	}	

	var currentRow = [soNo, companyName, itemName, customerName, orderPriority, orderType, itemGroup, itemFamily,
	                  packCode, UOM, itemStatus, itemInfo1, itemInfo2, itemInfo3, shippingCarrier, shipCountry, 
	                  shipState, shipCity, shipAddr1, shipmentNo, routeNo, carrier, shipdate,wmsstatusflag,orderdate,
	                  LinesFrom,LinesTo,backorder,shiptodate,ordertodate,Soid,orderselection,trantype,location];

	localVarArray.push(currentRow);

	if (request.getParameter('custpage_qeryparams')==null )
	{
		var hiddenQueryParams=form.getField('custpage_qeryparams');
		hiddenQueryParams.setDefaultValue(localVarArray.toString());
	}

	return localVarArray;
}

/**
 * Function to articulate all the search filters for wave generation
 * 
 * @param localVarArray
 * @returns {Array}
 */
function specifyWaveFilters(localVarArray,maxno,maxordno,waveno){

	nlapiLogExecution('DEBUG','maxno',maxno);// case# 201416942
	nlapiLogExecution('DEBUG','maxordno',maxordno);

	var filters = new Array();

	// Sales Order No
	if(localVarArray[0][0] != "" && localVarArray[0][0] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][0]));

	nlapiLogExecution('DEBUG','localVarArray[0][0]',localVarArray[0][0]);

	// Company
	if(localVarArray[0][1] != "" && localVarArray[0][1] != null)
		filters.push(new nlobjSearchFilter('custrecord_ordline_company', null, 'is', localVarArray[0][1]));

	// Item Name
	if(localVarArray[0][2] != "" && localVarArray[0][2] != null)
	{
		var soidarr = new Array();
		var itemfilters = new Array();
		if(localVarArray[0][0] != "" && localVarArray[0][0] != null)
			filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][0]));

		itemfilters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'anyof', localVarArray[0][2]));
		if(localVarArray[0][23] != "" && localVarArray[0][23] != null)
			itemfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[0][23]]));


		var itemcolumns = new Array();

		itemcolumns.push(new nlobjSearchColumn('custrecord_ns_ord'));

		var ordersList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, itemfilters, itemcolumns);
		if(ordersList!=null && ordersList!='')
		{
			nlapiLogExecution('DEBUG','ordersList',ordersList.length);
			for (var z = 0; ordersList != null && z < ordersList.length; z++) {
				soidarr.push(ordersList[z].getValue('custrecord_ns_ord'));
			}
		}
		nlapiLogExecution('DEBUG','soidarr',soidarr);
		if(soidarr != "" && soidarr != null && soidarr.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soidarr));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'anyof', localVarArray[0][2]));
		}
	}

	// Customer Name
	if(localVarArray[0][3] != "" && localVarArray[0][3] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_customer', null, 'is', localVarArray[0][3]));

	// Order Priority
	if(localVarArray[0][4] != "" && localVarArray[0][4] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_order_priority', null, 'is', localVarArray[0][4]));

	// Order Type
	if(localVarArray[0][5] != "" && localVarArray[0][5] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_order_type', null, 'is', localVarArray[0][5]));

	// Item Group
	if(localVarArray[0][6] != "" && localVarArray[0][6] != null)
		//filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][6]));
		filters.push(new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_linesku', 'is', localVarArray[0][6]));

	// Item Family
	if(localVarArray[0][7] != "" && localVarArray[0][7] != null)
		//filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][7]));
		filters.push(new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_linesku', 'is', localVarArray[0][7]));

	// Pack Code
	if(localVarArray[0][8] != "" && localVarArray[0][8] != null)
		filters.push(new nlobjSearchFilter('custrecord_linepackcode', null, 'is', localVarArray[0][8]));

	// UOM
	if(localVarArray[0][9] != "" && localVarArray[0][9] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineuom_id', null, 'is', localVarArray[0][9]));

	// Item Status
	if(localVarArray[0][10] != "" && localVarArray[0][10] != null)
		filters.push(new nlobjSearchFilter('custrecord_linesku_status', null, 'is', localVarArray[0][10]));

	// Item Info 1
//	if(localVarArray[0][11] != "" && localVarArray[0][11] != null)
//	filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));

	if(localVarArray[0][11] != "" && localVarArray[0][11] != null)
	{

		nlapiLogExecution('DEBUG','localVarArray[0][11]',localVarArray[0][11]);
		
//		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		var soidarr = new Array();
		/*var itemfilters = new Array();
		itemfilters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		if(localVarArray[0][23] != "" && localVarArray[0][23] != null)
			itemfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[0][23]]));

		var itemcolumns = new Array();
		itemcolumns.push(new nlobjSearchColumn('custrecord_ns_ord'));*/

		/*var ordersList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, itemfilters, itemcolumns);
		if(ordersList!=null && ordersList!='')
		{
			nlapiLogExecution('DEBUG','ordersList',ordersList.length);
			for (var z = 0; ordersList != null && z < ordersList.length; z++) {
				soidarr.push(ordersList[z].getValue('custrecord_ns_ord'));
			}
		}*/


		var ordersList = nlapiLoadSearch('customrecord_ebiznet_ordline','customsearch_fobasedon_iteminfo1', null, null, null);
		
		ordersList.addFilter(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		if(localVarArray[0][23] != "" && localVarArray[0][23] != null)		
		ordersList.addFilter(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][23]));

		//var TotalusedSOidcount = 0;
		if(ordersList!=null && ordersList!='')
		{
			
			var OrderResults = ordersList.runSearch();
			nlapiLogExecution('DEBUG','OrderResults',OrderResults.length);
			
			if(OrderResults != null && OrderResults  != '')
			{

				var vResultCount=0;
				var blnContinue=true;
				var j=0;

				while(blnContinue) {

					var firstThreeResultsUsed = OrderResults.getResults((j*1000), (((j+1)*1000)-1));
					blnContinue=false;
					if(firstThreeResultsUsed != null && firstThreeResultsUsed  != '' && firstThreeResultsUsed.length>=1000)
						blnContinue=true;

					for (var z = 0; firstThreeResultsUsed != null && z < firstThreeResultsUsed .length; z++) {
						//soidarr.push(firstThreeResultsUsed [z].getValue('custrecord_ns_ord'));
						soidarr.push(firstThreeResultsUsed [z].getValue('name',null,'group'));

					}
					//TotalusedSOidcount =TotalusedSOidcount + firstThreeResultsUsed.length;
					z++;
				}  
			}

		}
		nlapiLogExecution('DEBUG','soidarr',soidarr);
		if(soidarr != "" && soidarr != null && soidarr.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soidarr));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		}

	}
	// Item Info 2
	if(localVarArray[0][12] != "" && localVarArray[0][12] != null)
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo2', null, 'is', localVarArray[0][12]));

	// Item Info 3
	if(localVarArray[0][13] != "" && localVarArray[0][13] != null)
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo3', null, 'is', localVarArray[0][13]));

	// Shipping Carrier
	if(localVarArray[0][14] != "" && localVarArray[0][14] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_carrier',null, 'anyof', [localVarArray[0][14]]));		
	//	filters.push(new nlobjSearchFilter('shipmethod', 'custrecord_ns_ord', 'anyof', [localVarArray[0][14]]));

	// Ship Country
	if(localVarArray[0][15] != "" && localVarArray[0][15] != null)
		filters.push(new nlobjSearchFilter('shipcountry', 'custrecord_ns_ord', 'anyof', [localVarArray[0][15]]));

	// Ship State
	//if(localVarArray[0][16] != "" && localVarArray[0][16] != null)
	//filters.push(new nlobjSearchFilter('shipstate', 'custrecord_ns_ord', 'anyof', localVarArray[0][16]));

	// Ship City
	if(localVarArray[0][17] != "" && localVarArray[0][17] != null)
		filters.push(new nlobjSearchFilter('shipcity', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Masquerading the ship address with ship city
	if(localVarArray[0][18] != "" && localVarArray[0][18] != null)
		filters.push(new nlobjSearchFilter('shipaddress', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Shipment No.
	if(localVarArray[0][19] != "" && localVarArray[0][19] != null)
		filters.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'is', localVarArray[0][19]));

	// Route No.
	if(localVarArray[0][20] != "" && localVarArray[0][20] != null)
		filters.push(new nlobjSearchFilter('custrecord_route_no', null, 'anyof', [localVarArray[0][20]]));

	//WMS Staus Flag :: 11-Partially Picked, 13-Partially Shipped,15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filters.push(new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [11,13,15,25,26]));

	// Indicates the Sales Order header
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ns_ord', 'is', 'T'));			
	// carrier Added By Suman2q111q
	if(localVarArray[0][21] != "" && localVarArray[0][21] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_wmscarrier', null, 'anyof', [localVarArray[0][21]]));

	nlapiLogExecution('DEBUG','Shipdate',localVarArray[0][22]);
	if((localVarArray[0][22] != "" && localVarArray[0][22] != null))
	{
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'onorafter', [localVarArray[0][22]]));
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'onorbefore', [localVarArray[0][22]]));
		//modified as on 15th July by suman.
		//If ship date is empty then replace the value with the created date of FO
		filters.push(new nlobjSearchFilter('formuladate', null, 'onorbefore', [localVarArray[0][22]]).setFormula("nvl({custrecord_ebiz_shipdate},{created})"));
	}

	nlapiLogExecution('DEBUG','status flag',localVarArray[0][23]);

	if(localVarArray[0][23] != "" && localVarArray[0][23] != null)
	{
		if(localVarArray[0][23]=="-1")
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['11','15','25','26','13']));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[0][23]]));
		}

	}

	if(maxno!=null && maxno!='')
		//filters.push(new nlobjSearchFilter('id', null, 'lessthan', maxno));
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));

	if(maxordno!=null && maxordno!='' && maxordno!=0)
		filters.push(new nlobjSearchFilter('internalidnumber','custrecord_ns_ord', 'greaterthan', maxordno));

	if((localVarArray[0][24] != "" && localVarArray[0][24] != null) && (localVarArray[0][29] == "" || localVarArray[0][29] == null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'on', [localVarArray[0][24]]));

	if((localVarArray[0][24] != "" && localVarArray[0][24] != null) && (localVarArray[0][29] != "" && localVarArray[0][29] != null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'within', [localVarArray[0][24],localVarArray[0][29]]));

	nlapiLogExecution('DEBUG','localVarArray[0][25]',localVarArray[0][25]);
	if(localVarArray[0][25] !=null && localVarArray[0][25] !='')
		filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'greaterThanorequalto', parseInt(localVarArray[0][25])).setSummaryType('count'));

	if(localVarArray[0][26] !=null && localVarArray[0][26] !='')
		filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'lessthanorequalto', parseInt(localVarArray[0][26])).setSummaryType('count'));

	nlapiLogExecution('DEBUG','Order',localVarArray[0][30]);

	if(localVarArray[0][30] != "" && localVarArray[0][30] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord',null, 'contains', localVarArray[0][30]));

	if((localVarArray[0][31] == 'ALL'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'greaterthanorequalto', 1));

	if((localVarArray[0][31] == 'BO'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'greaterthan', 1));

	if((localVarArray[0][31] == 'RO'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'equalto', 1));

	if((localVarArray[0][32] == '1'))
		filters.push(new nlobjSearchFilter('type', 'custrecord_ns_ord', 'anyof', ['SalesOrd']));
	else if((localVarArray[0][32] == '2'))
		filters.push(new nlobjSearchFilter('type', 'custrecord_ns_ord', 'anyof', ['TrnfrOrd']));

	filters.push(new nlobjSearchFilter('isinactive',"custrecord_ebiz_linesku", 'is', "F"));
	// Case# 20148579 starts
	
	nlapiLogExecution('DEBUG','waveno@@',waveno);
	if(waveno!=null && waveno!="")
	filters.push(new nlobjSearchFilter("custrecord_ebiz_wave",null, 'notequalto', waveno));
	// Case# 20148579 ends
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('DEBUG','vRoleLocation',vRoleLocation);
	nlapiLogExecution('DEBUG','localVarArray[0][33]',localVarArray[0][33]);

	if(vRoleLocation!="" && vRoleLocation!=null && vRoleLocation!=0)
	{
		if(localVarArray[0][33] ==null || localVarArray[0][33]=='')
			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',vRoleLocation));
		else
		{
			nlapiLogExecution('DEBUG','index',vRoleLocation.indexOf(localVarArray[0][33]));
			if(vRoleLocation.indexOf(parseInt(localVarArray[0][33]))!=-1)
				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',localVarArray[0][33]));
			else
				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',["@NONE@"]));
		}
	}
	else
	{
		if(localVarArray[0][33] !=null && localVarArray[0][33] !='')
			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',localVarArray[0][33]));
	}	
	//upto here
	//new filter added by suman as on 040114
	//case# 20149816 starts (custrecord_pickqty is changed to custrecord_pickgen_qty)
	//filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickqty}),0)"));
	filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickgen_qty}),0)"));
	//case# 20149816 ends
	//end of the code

	return filters;
}

/**
 * 
 * @returns {Array}
 */
function getWaveColumns(localVarArray){
	nlapiLogExecution('DEBUG','test1','test1');
	//nlapiLogExecution('DEBUG','test2',localVarArray[16]);
	nlapiLogExecution('DEBUG','test2',localVarArray[0][16]);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_lineord',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_do_customer',null,'group');
	columns[2] = new nlobjSearchColumn('custrecord_do_carrier',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_do_order_type',null,'group');
	columns[4] = new nlobjSearchColumn('custrecord_do_order_priority',null,'group');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_orderdate',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_shipdate',null,'group');
	columns[7] = new nlobjSearchColumn('custrecord_ordline',null,'count');
	columns[8] = new nlobjSearchColumn('internalid','custrecord_ns_ord','group');	
	columns[9] = new nlobjSearchColumn('entity','custrecord_ns_ord','group');
	columns[10] = new nlobjSearchColumn('status','custrecord_ns_ord','group');
	columns[11] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	columns[12] = new nlobjSearchColumn('custrecord_shipment_no',null,'group');
	columns[13] = new nlobjSearchColumn('custrecord_shipcomplete',null,'group');
	columns[14] = new nlobjSearchColumn('shipzip','custrecord_ns_ord','group');
	columns[15] = new nlobjSearchColumn('entityid','custrecord_do_customer','group');
	//columns[16] = new nlobjSearchColumn('altname','custrecord_do_customer','group');// case# 201416942
	columns[16] = new nlobjSearchColumn('shipstate','custrecord_ns_ord','group');
	columns[8].setSort();
	return columns;
}


/**
 * 
 * @param request
 * @param response
 */
function generateWave(request, response){

	var form = nlapiCreateForm('Wave Creation & Release');
	try{
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');	
		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','Remaining usage at the start',context.getRemainingUsage());
		form.setScript('customscript_wavecreationrelease_cl');

		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
		if(request.getParameter('custpage_qeryparams')!=null)
		{
			hiddenQueryParams.setDefaultValue(request.getParameter('custpage_qeryparams'));
		}

		hiddenQueryParamsval = request.getParameter('custpage_qeryparams');
		nlapiLogExecution('DEBUG','hiddenQueryParamsval',hiddenQueryParamsval);

		var vwaveassignedto = request.getParameter('custpage_empassigntowave');

		nlapiLogExecution('DEBUG','vwaveassignedto',vwaveassignedto);

		var temparray='';
		var wmsstatusflag='';
		var fono = '';
		var shipmentno='';
		var shipmethod='';

		if(hiddenQueryParamsval!=null && hiddenQueryParamsval!='')
		{
			temparray=hiddenQueryParamsval.split(',');
			nlapiLogExecution('DEBUG','temparray 1',temparray);

			if(temparray.length>0 && temparray[23]!='')
			{
				fono = temparray[0];
				shipmethod = temparray[14];
				shipmentno = temparray[19];
				wmsstatusflag=temparray[23];				
			}
		}
		nlapiLogExecution('DEBUG', 'Wave.', eBizWaveNo);

		var linestatus='';
		var lineCount = request.getLineItemCount('custpage_items');

		for(var k = 0; k < lineCount; k++){
			linestatus = request.getLineItemValue('custpage_items', 'custpage_linestatus', k+1);
		}

		nlapiLogExecution('DEBUG','linestatus',linestatus);

		if(linestatus==null || linestatus=='')
			linestatus=wmsstatusflag;

		nlapiLogExecution('DEBUG','linestatus',linestatus);

		// Get the list of fulfillment orders selected for wave generation
		// Every element of the returned list contains index, soInternalID, lineNo, doNo, doName, 
		// itemName, itemNo, itemStatus, availQty, orderQty, packCode, uom, lotBatchNo, company, 
		// itemInfo1, itemInfo2, itemInfo3, orderType and orderPriority
		var fulfillOrderList = getSelectedFulfillmentOrderInfo(request);

		if(fulfillOrderList !=null)
			nlapiLogExecution('DEBUG', 'fulfillOrderList.length', fulfillOrderList.length);

		// Proceed only if there is atleast 1 fulfillment order selected
		if(fulfillOrderList!=null && fulfillOrderList.length > 0){

			var eBizWaveNo=createwaveordrecord(fulfillOrderList,linestatus,fono,shipmentno,shipmethod,temparray,vwaveassignedto);//
			if(eBizWaveNo=='RCRD_DSNT_EXIST')
			{				
				showInlineMessage(form, 'Error', 'Wave Error: Orders Modified by User');
			}
			else
			{
				var waveDetails = new Array();
				nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);
				waveDetails["ebiz_wave_no"] = eBizWaveNo;
				waveDetails["short_pick"] = 'N';	

				if(eBizWaveNo!='')
				{
					showInlineMessage(form, 'Confirmation', 'Wave created and Pick generation has been initiated for Wave '+ eBizWaveNo);
					form.addButton('custpage_wavestatus','Wave Status Report','gotowavestatus()');
					form.addButton('custpage_clusterstatus','Cluster Status Report','gotoclusterstatus()');
					var vDueDaysList=getDueDaysList();
					var vCreditExceedList=getCreditExceedList();

					var vConfig=nlapiLoadConfiguration('accountingpreferences');
					var vDueDaysRestrict;
					if(vConfig != null && vConfig != '')
					{
						vDueDaysRestrict=vConfig.getFieldValue('CREDLIMDAYS');
					}

					var orderList = getOrdersForWaveGen(request,0,form,null,eBizWaveNo);
					if(orderList != null && orderList.length > 0){//
						setPagingForSublist(orderList,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,request);

						var OrdersCount=form.addField('custpage_orderscount','text', 'Orders Count');//.setDisabled(true).setDefaultValue(parseInt(0));
						OrdersCount.setDefaultValue(parseInt(0));
						OrdersCount.setLayoutType('outsidebelow', 'startrow');
						OrdersCount.setDisabled(true);
						OrdersCount.setDisplaySize(50,30);
						var OrdersLineCount=form.addField('custpage_orderslinecount','text', 'Order Lines Count');//.setDisabled(true).setDefaultValue(parseInt(0));
						OrdersLineCount.setDefaultValue(parseInt(0));
						OrdersLineCount.setLayoutType('outsidebelow', 'startcol');
						OrdersLineCount.setDisabled(true);
						OrdersLineCount.setDisplaySize(50,30);
						form.addSubmitButton('Generate Wave');
					}
				}
			}
			form.addButton('custpage_backtosearch','Back to Search','backtoreleaseorders()');

			response.writePage(form);
		}
		else
		{
			var vDueDaysList=getDueDaysList();
			var vCreditExceedList=getCreditExceedList();

			var vConfig=nlapiLoadConfiguration('accountingpreferences');
			var vDueDaysRestrict;
			if(vConfig != null && vConfig != '')
			{
				vDueDaysRestrict=vConfig.getFieldValue('CREDLIMDAYS');
			}

			var orderList = getOrdersForWaveGen(request,0,form);
			if(orderList != null && orderList.length > 0){
				setPagingForSublist(orderList,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,request);

				var OrdersCount=form.addField('custpage_orderscount','text', 'Orders Count');//.setDisabled(true).setDefaultValue(parseInt(0));
				OrdersCount.setDefaultValue(parseInt(0));
				OrdersCount.setLayoutType('outsidebelow', 'startrow');
				OrdersCount.setDisabled(true);
				OrdersCount.setDisplaySize(50,30);
				var OrdersLineCount=form.addField('custpage_orderslinecount','text', 'Order Lines Count');//.setDisabled(true).setDefaultValue(parseInt(0));
				OrdersLineCount.setDefaultValue(parseInt(0));
				OrdersLineCount.setLayoutType('outsidebelow', 'startcol');
				OrdersLineCount.setDisabled(true);
				OrdersLineCount.setDisplaySize(50,30);

				var empassingedtowave = form.addField('custpage_empassigntowave', 'select', 'Wave Assigned To','Employee');

				form.addSubmitButton('Generate Wave');
			}			
		}

		nlapiLogExecution('DEBUG','Remaining usage at the end',context.getRemainingUsage());

		response.writePage(form);
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in generatewave ', exp);	
		showInlineMessage(form, 'DEBUG', 'Wave Generation Failed', "");
		response.writePage(form);
	}
}

function setPagingForSublist(orderList,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,request)
{
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('DEBUG', 'orderList length ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		var test='';

		if(orderListArray.length>0)
		{


			nlapiLogExecution('DEBUG', 'orderListArray length ', orderListArray.length);

			// case # 20124327� start
			createWaveGenSublist(form);
			// case # 20124327� end

			if(orderListArray.length>500)// case# 201414116
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("500");
					pagesizevalue=500;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue=500;
						pagesize.setDefaultValue("500");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{
					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}

			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);

				var pagevalue=request.getParameter('custpage_pagesize');

				if(pagevalue!=null)
				{
					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{
						var selectedPageArray=selectno.split(',');
						minval=parseFloat(selectedPageArray[0])-1;
						maxval=parseFloat(selectedPageArray[1]);
					}
				}
			}

			var c=0;
			var minvalue;
			minvalue=minval;
			var prevVal=null;var currVal=null;

			if(parseInt(orderListArray.length)<parseInt(maxval))
			{
				minval=0;
				maxval=orderListArray.length;
			}

			nlapiLogExecution('DEBUG', 'maxval ', minval+"/"+maxval); 

			for(var j = minvalue; j < maxval; j++){		

				var currentOrder = orderListArray[j];
				currVal=currentOrder.getValue('custrecord_lineord',null,'group');
				if(prevVal!=currVal)
				{
					//case# 20149120 starts
					//addFulfilmentOrderToSublist(form, currentOrder, c,orderListArray,vDueDaysList,vCreditExceedList,vDueDaysRestrict);
					addFulfilmentOrderToSublist(form, currentOrder, c,orderListArray,vDueDaysList,vCreditExceedList,vDueDaysRestrict,request);
					//case#20149120 ends
					prevVal=currVal;
					c=c+1;
				}
				else
				{
					//var fullfilmentordcount=form.getSubList('custpage_items').getLineItemValue('custpage_lineno',c);
					//form.getSubList('custpage_items').setLineItemValue('custpage_lineno', c,(parseFloat(fullfilmentordcount)+1).toString());
				}
			}
		}
	}
}


function specifyOrderFilters(localVarArray,maxno){

	nlapiLogExecution('DEBUG','maxno',maxno);

	var filters = new Array();

	// Sales Order No
	if(localVarArray[0] != "" && localVarArray[0] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0]));

	// Company
	if(localVarArray[1] != "" && localVarArray[1] != null)
		filters.push(new nlobjSearchFilter('custrecord_ordline_company', null, 'is', localVarArray[1]));

	// Item Name
//	if(localVarArray[2] != "" && localVarArray[2] != null)
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'is', localVarArray[2]));

	// Item Name
	if(localVarArray[2] != "" && localVarArray[2] != null)
	{
		var soidarr = new Array();
		var itemfilters = new Array();

		itemfilters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'anyof', localVarArray[2]));
		if(localVarArray[23] != "" && localVarArray[23] != null)
			itemfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[23]]));

		var itemcolumns = new Array();

		itemcolumns.push(new nlobjSearchColumn('internalid','custrecord_ns_ord','group').setSort());

		var ordersList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, itemfilters, itemcolumns);
		if(ordersList!=null && ordersList!='')
		{
			nlapiLogExecution('DEBUG','ordersList',ordersList.length);
			for (var z = 0; ordersList != null && z < ordersList.length; z++) {
				soidarr.push(ordersList[z].getValue('internalid','custrecord_ns_ord','group'));
			}
		}
		nlapiLogExecution('DEBUG','soidarr',soidarr);
		if(soidarr != "" && soidarr != null && soidarr.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soidarr));
		}
	}

	// Customer Name
	if(localVarArray[3] != "" && localVarArray[3] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_customer', null, 'is', localVarArray[3]));

	// Order Priority
	if(localVarArray[4] != "" && localVarArray[4] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_order_priority', null, 'is', localVarArray[4]));

	// Order Type
	if(localVarArray[5] != "" && localVarArray[5] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_order_type', null, 'is', localVarArray[5]));

	// Item Group
	if(localVarArray[6] != "" && localVarArray[6] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[6]));

	// Item Family
	if(localVarArray[7] != "" && localVarArray[7] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[7]));

	// Pack Code
	if(localVarArray[8] != "" && localVarArray[8] != null)
		filters.push(new nlobjSearchFilter('custrecord_linepackcode', null, 'is', localVarArray[8]));

	// UOM
	if(localVarArray[9] != "" && localVarArray[9] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineuom_id', null, 'is', localVarArray[9]));

	// Item Status
	if(localVarArray[10] != "" && localVarArray[10] != null)
		filters.push(new nlobjSearchFilter('custrecord_linesku_status', null, 'is', localVarArray[10]));

	// Item Info 1
//	if(localVarArray[11] != "" && localVarArray[11] != null)
//	filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[11]));

	/*if(localVarArray[11] != "" && localVarArray[11] != null)
	{

//		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		var soidarr = new Array();
		var itemfilters = new Array();
		itemfilters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[11]));
		if(localVarArray[23] != "" && localVarArray[23] != null)
			itemfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[23]]));

		var itemcolumns = new Array();
		itemcolumns.push(new nlobjSearchColumn('custrecord_ns_ord'));

		var ordersList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, itemfilters, itemcolumns);
		if(ordersList!=null && ordersList!='')
		{
			nlapiLogExecution('DEBUG','ordersList',ordersList.length);
			for (var z = 0; ordersList != null && z < ordersList.length; z++) {
				soidarr.push(ordersList[z].getValue('custrecord_ns_ord'));
			}
		}
		nlapiLogExecution('DEBUG','soidarr1',soidarr);
		if(soidarr != "" && soidarr != null && soidarr.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soidarr));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[11]));
		}

	}*/
	
	
	
	if(localVarArray[11] != "" && localVarArray[11] != null) // case# 201414874
	{

		nlapiLogExecution('DEBUG','localVarArray[11]1',localVarArray[11]);
		
//		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		var soidarr = new Array();
		/*var itemfilters = new Array();
		itemfilters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		if(localVarArray[0][23] != "" && localVarArray[0][23] != null)
			itemfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[0][23]]));

		var itemcolumns = new Array();
		itemcolumns.push(new nlobjSearchColumn('custrecord_ns_ord'));*/

		/*var ordersList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, itemfilters, itemcolumns);
		if(ordersList!=null && ordersList!='')
		{
			nlapiLogExecution('DEBUG','ordersList',ordersList.length);
			for (var z = 0; ordersList != null && z < ordersList.length; z++) {
				soidarr.push(ordersList[z].getValue('custrecord_ns_ord'));
	}
		}*/


		var ordersList = nlapiLoadSearch('customrecord_ebiznet_ordline','customsearch_fobasedon_iteminfo1', null, null, null);// case# 201414310
		
		ordersList.addFilter(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[11]));
		if(localVarArray[23] != "" && localVarArray[23] != null)		
		ordersList.addFilter(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[23]));

		//var TotalusedSOidcount = 0;
		if(ordersList!=null && ordersList!='')
		{
			
			var OrderResults = ordersList.runSearch();
			nlapiLogExecution('DEBUG','OrderResults',OrderResults.length);
			
			if(OrderResults != null && OrderResults  != '')
			{

				var vResultCount=0;
				var blnContinue=true;
				var j=0;

				while(blnContinue) {

					var firstThreeResultsUsed = OrderResults.getResults((j*1000), (((j+1)*1000)-1));
					blnContinue=false;
					if(firstThreeResultsUsed != null && firstThreeResultsUsed  != '' && firstThreeResultsUsed.length>=1000)
						blnContinue=true;

					for (var z = 0; firstThreeResultsUsed != null && z < firstThreeResultsUsed .length; z++) {
						//soidarr.push(firstThreeResultsUsed [z].getValue('custrecord_ns_ord'));
						soidarr.push(firstThreeResultsUsed [z].getValue('name',null,'group'));

					}
					//TotalusedSOidcount =TotalusedSOidcount + firstThreeResultsUsed.length;
					z++;
				}  
			}

		}
		nlapiLogExecution('DEBUG','soidarr',soidarr);
		if(soidarr != "" && soidarr != null && soidarr.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soidarr));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		}

	}
	
	
	// Item Info 2
	if(localVarArray[12] != "" && localVarArray[12] != null)
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo2', null, 'is', localVarArray[12]));

	// Item Info 3
	if(localVarArray[13] != "" && localVarArray[13] != null)
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo3', null, 'is', localVarArray[13]));

	// Shipping Carrier
	if(localVarArray[14] != "" && localVarArray[14] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_carrier', null, 'anyof', localVarArray[14]));
	//filters.push(new nlobjSearchFilter('shipmethod', 'custrecord_ns_ord', 'anyof', [localVarArray[0][14]]));
	//above filter was commented beacuse instead of checking shipmethod from Salesorder, checked from FO 

	// Ship Country
	if(localVarArray[15] != "" && localVarArray[15] != null)
		filters.push(new nlobjSearchFilter('shipcountry', 'custrecord_ns_ord', 'anyof', [localVarArray[15]]));

	// Ship State
	//if(localVarArray[16] != "" && localVarArray[16] != null)
	//filters.push(new nlobjSearchFilter('shipstate', 'custrecord_ns_ord', 'anyof', localVarArray[16]));

	// Ship City
	if(localVarArray[17] != "" && localVarArray[17] != null)
		filters.push(new nlobjSearchFilter('shipcity', 'custrecord_ns_ord', 'is', localVarArray[17]));

	// Masquerading the ship address with ship city
	if(localVarArray[18] != "" && localVarArray[18] != null)
		filters.push(new nlobjSearchFilter('shipaddress', 'custrecord_ns_ord', 'is', localVarArray[17]));

	// Shipment No.
	if(localVarArray[19] != "" && localVarArray[19] != null)
		filters.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'is', localVarArray[19]));

	// Route No.
	if(localVarArray[20] != "" && localVarArray[20] != null)
		filters.push(new nlobjSearchFilter('custrecord_route_no', null, 'anyof', [localVarArray[20]]));

	//WMS Staus Flag :: 11-Partially Picked, 13-Partially Shipped,15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filters.push(new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [11,13,15,25,26]));

	// Indicates the Sales Order header
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ns_ord', 'is', 'T'));			

	if(localVarArray[21] != "" && localVarArray[21] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_wmscarrier', null, 'anyof', [localVarArray[21]]));

//	if(localVarArray[22] != "" && localVarArray[22] != null)
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'on', [localVarArray[22]]));

	/*if((localVarArray[22] != "" && localVarArray[22] != null) && (localVarArray[28] == "" || localVarArray[28] == null) )
		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'onorbefore', [localVarArray[22]]));




	if((localVarArray[22] != "" && localVarArray[22] != null) && (localVarArray[28] != "" && localVarArray[28] != null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'within', [localVarArray[22],localVarArray[28]]));*/


	nlapiLogExecution('DEBUG','Shipdate',localVarArray[0][22]);
	if((localVarArray[0][22] != "" && localVarArray[0][22] != null))
	{

		//If ship date is empty then replace the value with the created date of FO
		filters.push(new nlobjSearchFilter('formuladate', null, 'onorbefore', [localVarArray[0][22]]).setFormula("nvl({custrecord_ebiz_shipdate},{created})"));
	}


	nlapiLogExecution('DEBUG','status flag',localVarArray[23]);

	if(localVarArray[23] != "" && localVarArray[23] != null)
	{
		if(localVarArray[23]=="-1")
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['11','15','25','26','13']));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[23]]));
		}
	}



	if(maxno!=null && maxno!='')
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));

//	if(localVarArray[24] != "" && localVarArray[24] != null)
//	filters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'on', [localVarArray[24]]));

//	if(localVarArray[24] != "" && localVarArray[24] != null)
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'on', [localVarArray[24]]));

	if((localVarArray[24] != "" && localVarArray[24] != null) && (localVarArray[29] == "" || localVarArray[29] == null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'on', [localVarArray[24]]));

	if((localVarArray[24] != "" && localVarArray[24] != null) && (localVarArray[29] != "" && localVarArray[29] != null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'within', [localVarArray[24],localVarArray[29]]));

//	nlapiLogExecution('DEBUG','Back Order',localVarArray[27]);
//	if(localVarArray[27] == 'T')
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno', null, 'greaterthan', 1));

	if((localVarArray[31] == 'ALL'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'greaterthanorequalto', 1));

	if((localVarArray[31] == 'BO'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'greaterthan', 1));

	if((localVarArray[31] == 'RO'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'equalto', 1));

	if(localVarArray[30] != "" && localVarArray[30] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord',null, 'contains', localVarArray[30]));

	if((localVarArray[32] == '1'))
		filters.push(new nlobjSearchFilter('type', 'custrecord_ns_ord', 'anyof', ['SalesOrd']));
	else if((localVarArray[32] == '2'))
		filters.push(new nlobjSearchFilter('type', 'custrecord_ns_ord', 'anyof', ['TrnfrOrd']));
	filters.push(new nlobjSearchFilter('isinactive',"custrecord_ebiz_linesku", 'is', "F"));

	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('DEBUG','vRoleLocation',vRoleLocation);
	nlapiLogExecution('DEBUG','localVarArray[33]',localVarArray[33]);

	if(vRoleLocation!="" && vRoleLocation!=null && vRoleLocation!=0)
	{
		if(localVarArray[33] ==null || localVarArray[33]=='')
			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',vRoleLocation));
		else
		{
			nlapiLogExecution('DEBUG','index',vRoleLocation.indexOf(localVarArray[33]));
			if(vRoleLocation.indexOf(localVarArray[33])!=-1)
				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',localVarArray[33]));
			else
				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',["@NONE@"]));
		}
	}
	else
	{
		if(localVarArray[33] !=null && localVarArray[33] !='')
			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',localVarArray[33]));
	}	
	//new filter added by suman as on 040114
	//case# 20149816 starts (custrecord_pickqty is changed to custrecord_pickgen_qty)
	//filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickqty}),0)"));
	filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickgen_qty}),0)"));
	//case# 20149816 ends
	//end of the code
	return filters;
}

var orderListArray = new Array();

function getOrdersList(parametersval,maxno)
{
	nlapiLogExecution('DEBUG', 'Into  getOrdersList',parametersval);	

	var filters = new Array();   
	filters = specifyOrderFilters(parametersval,maxno);

	var focolumns = new Array();
	focolumns[0] = new nlobjSearchColumn('custrecord_lineord');
	focolumns[1] = new nlobjSearchColumn('custrecord_ordline_company');
	focolumns[2] = new nlobjSearchColumn('custrecord_ordline_wms_location');
	focolumns[3] = new nlobjSearchColumn('custrecord_ordline');
	focolumns[4] = new nlobjSearchColumn('id').setSort();
	focolumns[5] = new nlobjSearchColumn('custrecord_ebiz_lot_number');
	focolumns[6] = new nlobjSearchColumn('custrecord_ebiz_lotnumber_qty');
	focolumns[7] = new nlobjSearchColumn('custrecord_ebiz_lotno_internalid');
	focolumns[8] = new nlobjSearchColumn('shipstate', 'custrecord_ns_ord');
	var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, focolumns);
	var setObj = orderList.filter(function(obj){
		nlapiLogExecution('DEBUG', 'Into  getOrdersList',parametersval[16]);	
		return obj.getValue('shipstate', 'custrecord_ns_ord') == parametersval[16]; //modify using parametersval array
		//if searchresult > shipstate column == VIC
	});

	nlapiLogExecution('DEBUG', 'setObj',setObj);	
	if( orderList!=null && orderList!='')
	{
		nlapiLogExecution('DEBUG', 'orderList', orderList.length);
		if(orderList.length>=1000)
		{ 
			for(var i = 0; i < orderList.length; i++)
			{
				orderListArray.push(orderList[i]); 
			}

			var maxno=orderList[orderList.length-1].getValue('id');
			getOrdersList(parametersval,maxno);	
		}
		else
		{
			for(var i = 0; i < orderList.length; i++)
			{
				orderListArray.push(orderList[i]); 
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getOrdersList');	
	return orderListArray;
}


//function createwaveordrecord(fulfillOrderList,eBizWaveNo,vlineStatus,fono,shipmentno,shipmethod,parametersval)
//{
//nlapiLogExecution('DEBUG', 'Into  createwaveordrecord ', eBizWaveNo);	
function createwaveordrecord(fulfillOrderList,vlineStatus,fono,shipmentno,shipmethod,parametersval,waveassignedto)
{

	try
	{
		var eBizWaveNo='';
		var str = 'vlineStatus. = ' + vlineStatus + '<br>';
		str = str + 'fono. = ' + fono + '<br>';	
		str = str + 'shipmentno. = ' + shipmentno + '<br>';	
		str = str + 'shipmethod. = ' + shipmethod + '<br>';	
		str = str + 'parametersval. = ' + parametersval + '<br>';	
		str = str + 'fulfillOrderList length. = ' + fulfillOrderList.length + '<br>';
		str = str + 'waveassignedto. = ' + waveassignedto + '<br>';

		nlapiLogExecution('DEBUG', 'Parameters ', str);	

		var fosearchResults = getOrdersList(parametersval,null);

		if(fosearchResults != null && fosearchResults != '')
		{
			nlapiLogExecution('DEBUG', 'fosearchResults length', fosearchResults.length);	
		}

		if(fulfillOrderList != null && fulfillOrderList.length > 0)
		{
			var context = nlapiGetContext();
			var currentUserID = context.getUser();	
			var searchResults = new Array();


			for(var i = 0; i < fulfillOrderList.length; i++)
			{	
				for(var s = 0; fosearchResults!=null && s < fosearchResults.length; s++)
				{
					var vfono = fosearchResults[s].getValue('custrecord_lineord');

					if(fulfillOrderList[i] == vfono)
					{
						if(eBizWaveNo=='')
						{
							eBizWaveNo = GetMaxTransactionNo('WAVE');
							nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);
						}

						var ordno = fosearchResults[s].getValue('custrecord_lineord');
						var ordlineno = fosearchResults[s].getValue('custrecord_ordline');
						var company = fosearchResults[s].getValue('custrecord_ordline_company');
						var wmslocation = fosearchResults[s].getValue('custrecord_ordline_wms_location');
						var fointrid = fosearchResults[s].getId();
						var folotnumber="";
						var folotqty="";
						var folotinternalid="";
						folotnumber = fosearchResults[s].getValue('custrecord_ebiz_lot_number');
						folotqty = fosearchResults[s].getValue('custrecord_ebiz_lotnumber_qty');
						folotinternalid = fosearchResults[s].getValue('custrecord_ebiz_lotno_internalid');

						var currentRow = [fointrid,ordno, ordlineno,company,wmslocation,folotnumber,folotqty,folotinternalid];

						searchResults.push(currentRow);
						var vAllowWave='T';
						if(fointrid != null && fointrid != '')
						{	
							var vFOLine= nlapiLoadRecord('customrecord_ebiznet_ordline', fointrid);
							if(vFOLine != null && vFOLine != '')
							{	
								var vFOPresentStatus= vFOLine.getFieldValue('custrecord_linestatus_flag');
								nlapiLogExecution('DEBUG', 'vFOPresentStatus', vFOPresentStatus);
								nlapiLogExecution('DEBUG', 'vlineStatus', vlineStatus);
								/*if(vFOPresentStatus == '15' && vlineStatus != '15')
									vAllowWave='F';*/
								if(vlineStatus != null && vlineStatus != '' && vFOPresentStatus != vlineStatus)
									vAllowWave='F';	
							}
						}
						if(vAllowWave == 'T')
						{
							updateFulfilmentOrder(fointrid, eBizWaveNo);
						}

					}
				}						
			}

//			for(var j=0;searchResults!=null && j<searchResults.length;j++)
//			{
//			updateFulfilmentOrder(searchResults[j][0], eBizWaveNo);
//			}	
			if(eBizWaveNo!='')
			{
				nlapiLogExecution('DEBUG', 'Invoking Schedule Script Starts', TimeStampinSec());	

				var param = new Array();
				param['custscript_ebizwaveno'] = eBizWaveNo;
				param['custscript_ebizwaveassignto'] = waveassignedto;
				var schstatus = nlapiScheduleScript('customscript_ebiz_pickgen_scheduler', null,param);
				
				nlapiLogExecution('ERROR', 'schstatus ', schstatus );

				while(schstatus != 'QUEUED') {
					schstatus = nlapiScheduleScript('customscript_ebiz_pickgen_scheduler', null,param);
				}

				//nlapiLogExecution('DEBUG', 'tranType', tranType);
				updateScheduleScriptStatus('WAVE RELEASE',currentUserID,'Submitted',eBizWaveNo,null);

				nlapiLogExecution('DEBUG', 'Invoking Schedule Script Ends', TimeStampinSec());
			}

		}
	}
	catch(exp) 
	{
		nlapiLogExecution('ERROR', 'Exception in createwaveordrecord ', exp);
		var exCode='';
		if (exp instanceof nlobjError) 
		{
			exCode=exp.getCode();
		}
		else
		{
			exCode=exp.toString();
		} 			
		nlapiLogExecution('DEBUG', 'exCode : ', exCode); 

		if(exCode=='RCRD_DSNT_EXIST')
		{
			eBizWaveNo = exCode;
			folinesreversel(eBizWaveNo);
		}			
	}
	nlapiLogExecution('DEBUG', 'Out of  createwaveordrecord ', eBizWaveNo);
	return eBizWaveNo;
}

/**
 * Return the fulfillment order information to be used for wave generation
 * NOTE: WILL NEVER RETURN NULL; WILL ALWAYS RETURN ATLEAST A BLANK ARRAY
 * 
 * @param request
 * @returns {Array}
 */
function getSelectedFulfillmentOrderInfo(request){
	nlapiLogExecution('DEBUG','into getSelectedFulfillmentOrderInfo', '');
	var fulfillOrderInfoArray = new Array();
	var orderInfoCount = 0;

	// Retrieve the number of items for which wave needs to be created
	var lineCount = request.getLineItemCount('custpage_items');

	for(var k = 1; k <= lineCount; k++){
		var selectValue = request.getLineItemValue('custpage_items', 'custpage_so', k);

		if(isItemSelected(request,'custpage_items', 'custpage_so', k)){	

			var doName = request.getLineItemValue('custpage_items', 'custpage_sono', k);
			fulfillOrderInfoArray[fulfillOrderInfoArray.length] = doName;
		}
	}

	nlapiLogExecution('DEBUG','out of getSelectedFulfillmentOrderInfo', '');

	return fulfillOrderInfoArray;
}


/**
 * 
 */
function isItemSelected(request, subList, element, index){
	var retVal = false;
	if(request.getLineItemValue(subList, element, index) == 'T')
		retVal = true;

	return retVal;
}

function updateFulfilmentOrder(fointrid, eBizWaveNo){

	nlapiLogExecution('DEBUG', 'Into updateFulfilmentOrder ',TimeStampinSec());

	var recId = fointrid;

	var fieldNames = new Array(); 				//fields to be updated
	fieldNames[0] = "custrecord_ebiz_wave";  	
	fieldNames[1] = "custrecord_linestatus_flag";

	var newValues = new Array(); 				//new field values
	newValues[0] = eBizWaveNo;
	newValues[1] =  '15';

	nlapiSubmitField('customrecord_ebiznet_ordline', recId, fieldNames, newValues);

	nlapiLogExecution('DEBUG', 'Fulfilment Order Updated Successfully', fointrid);
	nlapiLogExecution('DEBUG', 'Out of updateFulfilmentOrder',TimeStampinSec());
}

function folinesreversel(vebizwaveno)
{
	nlapiLogExecution('ERROR', 'Into  folinesreversel', vebizwaveno);
	try
	{
		var filters = new Array();

		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto', parseInt(vebizwaveno)));
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [15]));

		var columns = new Array();

		columns[0] = new nlobjSearchColumn('custrecord_lineord');
		columns[1] = new nlobjSearchColumn('custrecord_ordline');

		var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
		if(orderList!=null && orderList!='')
		{
			for(z=0;z<orderList.length;z++)
			{
				var fointrid = orderList[z].getId();

				var fieldNames = new Array(); 				
				fieldNames[0] = "custrecord_linestatus_flag";

				var newValues = new Array(); 				
				newValues[0] =  '25';

				nlapiSubmitField('customrecord_ebiznet_ordline', fointrid, fieldNames, newValues);

			}

		}

	}
	catch(ex)
	{
		nlapiLogExecution('ERROR', 'Into folinesreversel Exception', ex);
	}
	nlapiLogExecution('ERROR', 'Out of folinesreversel', vebizwaveno);
}


