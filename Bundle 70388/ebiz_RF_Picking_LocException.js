/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_Picking_LocException.js,v $
<<<<<<< ebiz_RF_Picking_LocException.js
 *     	   $Revision: 1.1.2.11.4.10.2.46.2.7 $
 *     	   $Date: 2015/12/04 14:56:34 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.1.2.11.4.10.2.46.2.7 $
 *     	   $Date: 2015/12/04 14:56:34 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.1.2.11.4.10.2.25
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Picking_LocException.js,v $
 * Revision 1.1.2.11.4.10.2.46.2.7  2015/12/04 14:56:34  grao
 * 2015.2 Issue Fixes 201415967
 *
 * Revision 1.1.2.11.4.10.2.46.2.6  2015/11/09 16:12:28  deepshikha
 * 2015.2 issues
 * 201415337
 *
 * Revision 1.1.2.11.4.10.2.46.2.5  2015/10/06 15:10:11  aanchal
 * 2015.2 issue fixes
 * 201414660
 *
 * Revision 1.1.2.11.4.10.2.46.2.4  2015/10/06 13:40:01  schepuri
 * case# 201414660,201414686
 *
 * Revision 1.1.2.11.4.10.2.46.2.3  2015/10/06 10:22:57  deepshikha
 * 2015.2 issues
 * 201414327
 *
 * Revision 1.1.2.11.4.10.2.46.2.2  2015/10/06 06:22:34  aanchal
 * 2015.2 issue fixes
 * 201414327
 *
 * Revision 1.1.2.11.4.10.2.46.2.1  2015/09/24 13:00:05  schepuri
 * case# 201414553
 *
 * Revision 1.1.2.11.4.10.2.46  2015/08/14 09:55:55  nneelam
 * case# 201413975
 *
 * Revision 1.1.2.11.4.10.2.45  2015/08/04 14:07:03  grao
 * 2015.2   issue fixes  20143630
 *
 * Revision 1.1.2.11.4.10.2.44  2015/07/27 10:39:36  nneelam
 * case# 201413665
 *
 * Revision 1.1.2.11.4.10.2.43  2015/07/23 15:32:56  grao
 * 2015.2   issue fixes  201413589
 *
 * Revision 1.1.2.11.4.10.2.42  2015/06/01 15:44:42  grao
 * SB  2015.2 issue fixes  201412916
 *
 * Revision 1.1.2.11.4.10.2.41  2015/05/19 13:14:24  schepuri
 * case# 201412841
 *
 * Revision 1.1.2.11.4.10.2.40  2015/05/12 19:51:35  nneelam
 * case# 20142755
 *
 * Revision 1.1.2.11.4.10.2.39  2015/04/28 12:44:40  skreddy
 * Case# 201412507
 * standard bundle  issue fix
 *
 * Revision 1.1.2.11.4.10.2.38  2015/02/27 16:15:07  sponnaganti
 * Case# 201411635
 * KRM SB Issue fix
 *
 * Revision 1.1.2.11.4.10.2.37  2015/02/04 05:52:39  snimmakayala
 * Case#: 201411485
 *
 * Revision 1.1.2.11.4.10.2.36  2015/01/21 14:31:29  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.11.4.10.2.35  2014/09/25 17:57:16  skreddy
 * case # 201410534
 * Alpha comm CR (''pick by carton LP")
 *
 * Revision 1.1.2.11.4.10.2.34  2014/07/17 15:34:09  sponnaganti
 * Case# 20148648
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.11.4.10.2.33  2014/06/13 12:43:25  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.11.4.10.2.32  2014/06/05 15:20:13  nneelam
 * case#  20148719
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.11.4.10.2.31  2014/06/04 14:48:24  rmukkera
 * Case # 20148679
 *
 * Revision 1.1.2.11.4.10.2.30  2014/05/30 00:41:03  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.11.4.10.2.29  2014/05/28 15:04:53  sponnaganti
 * case# 20148588
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.11.4.10.2.28  2014/05/27 06:51:55  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148531
 *
 * Revision 1.1.2.11.4.10.2.27  2014/05/06 15:05:25  sponnaganti
 * case# 20148264
 * (Invalid lot validation message)
 *
 * Revision 1.1.2.11.4.10.2.26  2014/04/22 15:15:55  grao
 * Case#: 20148082  Sonic Sb issue fixes
 *
 * Revision 1.1.2.11.4.10.2.25  2014/04/02 15:49:20  skavuri
 * Case # 20127886 issue fixed
 *
 * Revision 1.1.2.11.4.10.2.24  2014/03/14 15:21:14  skavuri
 * Case # 20127440 issue fixed
 *
 * Revision 1.1.2.11.4.10.2.23  2014/02/19 13:40:35  sponnaganti
 * case# 20127170
 * (single quotes added to getItem,getItemName)
 *
 * Revision 1.1.2.11.4.10.2.22  2014/01/20 13:41:03  snimmakayala
 * Case# : 20126850�
 *
 * Revision 1.1.2.11.4.10.2.21  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.11.4.10.2.20  2013/12/26 15:21:25  rmukkera
 * Case # 20126477
 *
 * Revision 1.1.2.11.4.10.2.19  2013/12/24 15:12:03  rmukkera
 * Case # 20126541
 *
 * Revision 1.1.2.11.4.10.2.18  2013/12/20 15:43:30  rmukkera
 * Case # 20126039
 *
 * Revision 1.1.2.11.4.10.2.17  2013/11/14 06:32:04  skreddy
 * Case# 201215000
 * Fast Picking issue fix
 *
 * Revision 1.1.2.11.4.10.2.16  2013/11/05 15:24:44  rmukkera
 * Case# 20125556
 *
 * Revision 1.1.2.11.4.10.2.15  2013/10/24 15:53:48  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Afosa issue fixes
 *
 * Revision 1.1.2.11.4.10.2.14  2013/10/09 15:40:15  rmukkera
 * Case# 20124668
 *
 * Revision 1.1.2.11.4.10.2.13  2013/09/19 15:18:16  rmukkera
 * Case# 20124446
 *
 * Revision 1.1.2.11.4.10.2.12  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.1.2.11.4.10.2.11  2013/07/25 07:02:46  skreddy
 * Case# 20123468
 * qty exception in cluster picking
 *
 * Revision 1.1.2.11.4.10.2.10  2013/06/24 15:11:19  nneelam
 * CASE201112/CR201113/LOG201121
 * Standard Bundle, Pick Location exception through RF
 * :Issue No. 20123134
 *
 * Revision 1.1.2.11.4.10.2.9  2013/06/20 14:13:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 201215543
 *
 * Revision 1.1.2.11.4.10.2.8  2013/06/18 13:33:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * nls issue fixes
 *
 * Revision 1.1.2.11.4.10.2.7  2013/06/17 14:58:16  nneelam
 * CASE201112/CR201113/LOG201121
 * Standard Bundle, BuildShip Units:Issue No. 20122673
 *
 * Revision 1.1.2.11.4.10.2.6  2013/06/11 16:01:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Stdbundle issue fixes
 *
 * Revision 1.1.2.11.4.10.2.5  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.11.4.10.2.4  2013/05/07 15:37:53  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.1.2.11.4.10.2.3  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.11.4.10.2.2  2013/04/08 08:47:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.2.11.4.10.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.1.2.11.4.10  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.1.2.11.4.9  2012/12/31 05:53:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Inventory Sync issue fixes
 * .
 *
 * Revision 1.1.2.11.4.8  2012/12/28 16:31:43  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Exception in Cluster Picking
 *
 * Revision 1.1.2.11.4.7  2012/12/18 13:21:25  gkalla
 * CASE201112/CR201113/LOG201121
 * GSUSA invalid location issue
 *
 * Revision 1.1.2.11.4.6  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.1.2.11.4.5  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.11.4.4  2012/10/23 17:08:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.1.2.11.4.3  2012/10/04 10:28:57  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.1.2.11.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.11.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.11  2012/09/11 00:45:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.1.2.10  2012/08/31 01:42:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fisk Production Issue Fixes
 *
 * Revision 1.1.2.9  2012/07/17 15:35:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating Location.
 *
 * Revision 1.1.2.8  2012/05/23 12:40:02  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix in pick strategy
 *
 * Revision 1.1.2.7  2012/05/22 15:37:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.1.2.6  2012/05/08 15:45:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Picking Exception
 *
 * Revision 1.1.2.5  2012/05/07 12:56:20  mbpragada
 * CASE201112/CR201113/LOG201121
 * System did not update the quantity for new location
 * and new location did not displayed in Buildship Units Screen.
 *
 * Revision 1.1.2.4  2012/04/24 16:11:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.1.2.3  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.1.2.2  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.1.2.1  2012/02/15 10:21:07  spendyala
 * CASE201112/CR201113/LOG201121
 * Loc exception issue is fixed.
 *
 * Revision 1.4  2012/01/27 07:12:42  snimmakayala
 *****************************************************************************/

function PickingLocException(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var fastpick = request.getParameter('custparam_fastpick');

		nlapiLogExecution('ERROR', 'fastpick', fastpick);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10;

		if( getLanguage == 'es_ES')
		{
			st1 = "EXCEPCI&#211;N NO ES POSIBLE NO HAY INVENTARIO EN LUGAR";
			st2 = "PARA ESTE ART&#205;CULO";
			st3 = "EXCEPCI&#211;N NO ES POSIBLE NO HAY OTRO LUGAR PICKFACE PARA ESTE ART&#205;CULO";
			st4 = "INVENTARIO DE DISPONIBILIDAD DE PARTIDA: ";
			st5 = "CANTIDAD: ";
			st6 = "INGRESAR / ESCANEAR UBICACI&#211;N";
			st7 = "ENVIAR";
			st8 = "ANTERIOR";

		}
		else
		{
			st1 = "EXCEPTION IS NOT POSSIBLE AS THERE IS NO INVENTORY IN LOCATION ";
			st2 = "FOR THIS ITEM";
			st3 = "EXCEPTION IS NOT POSSIBLE AS THERE IS NO OTHER PICKFACE LOCATION FOR THIS ITEM";//have to check the meaning of this again in spanish
			st4 = "INVENTORY AVAILABILITY FOR ITEM: ";
			st5 = "QTY : ";
			st6 = "ENTER/SCAN LOCATION";
			st7 = "SEND ";
			st8 = "PREV";

		}

		var getWaveNo = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		nlapiLogExecution('DEBUG', 'getRecordInternalId', getRecordInternalId);
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
//		var getItemName = request.getParameter('custparam_itemdescription');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemNo = request.getParameter('custparam_iteminternalid');
		var getdoLoineId = request.getParameter('custparam_dolineid');
		nlapiLogExecution('DEBUG', 'getdoLoineId', getdoLoineId);
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getBeginLocationName = request.getParameter('custparam_beginlocationname');
		var getEndLocationInternalId = request.getParameter('custparam_endlocinternalid');
		var getEndLocation = request.getParameter('custparam_endlocation');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getClusterNo = request.getParameter('custparam_clusterno');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getNoofRecords = request.getParameter('custparam_noofrecords');
		var getNextLocation = request.getParameter('custparam_nextlocation');
		var name = request.getParameter('name');
		var getRecCount = request.getParameter('custparam_RecCount');
		var getContainerSize = request.getParameter('custparam_containersize');
		var getEbizOrdNo = request.getParameter('custparam_ebizordno');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var whLocation = request.getParameter('custparam_whlocation');
		//parameters erquired if serializedinventoryitem
		var getnumber = request.getParameter('custparam_number');
		var getRecType = request.getParameter('custparam_RecType');
		var getSerOut = request.getParameter('custparam_SerOut');
		var getSerIn = request.getParameter('custparam_SerIn');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);
		var pickType=request.getParameter('custparam_picktype');
		var detailTask=request.getParameter('custparam_detailtask');
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		
			var getdisplayqty = request.getParameter('custparam_displayquantity');
		nlapiLogExecution('DEBUG', 'getdisplayqty', getdisplayqty);

		var serialscanflag=request.getParameter('custparam_serialscanflag');

		var NextExptdQty = request.getParameter('custparam_nextexpectedquantity');
		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');
		var itemstatus = request.getParameter('custparam_itemstatus');
		nlapiLogExecution('DEBUG', 'getBatchNo', getBatchNo);

		var BatchId = request.getParameter('custparam_lotnointernalid');	
		
		nlapiLogExecution('DEBUG', 'BatchId', BatchId);
		nlapiLogExecution('DEBUG', 'serialscanflag in get', serialscanflag);
		
//		nlapiLogExecution('DEBUG', 'getWaveNo', getWaveNo);
//		nlapiLogExecution('DEBUG', 'getRecordInternalId', getRecordInternalId);
//		nlapiLogExecution('DEBUG', 'getContainerLpNo', getContainerLpNo);
//		nlapiLogExecution('DEBUG', 'getQuantity', getQuantity);
//		nlapiLogExecution('DEBUG', 'getBeginLocation', getBeginLocation);
//		nlapiLogExecution('DEBUG', 'getItem', getItem);
//		nlapiLogExecution('DEBUG', 'getItemName', getItemName);
//		nlapiLogExecution('DEBUG', 'getItemNo', getItemNo);
//		nlapiLogExecution('DEBUG', 'getdoLoineId', getdoLoineId);
//		nlapiLogExecution('DEBUG', 'getInvoiceRefNo', getInvoiceRefNo);
//		nlapiLogExecution('DEBUG', 'getBeginLocationName', getBeginLocationName);
//		nlapiLogExecution('DEBUG', 'getEndLocationInternalId', getEndLocationInternalId);
//		nlapiLogExecution('DEBUG', 'getEndLocation', getEndLocation);
//		nlapiLogExecution('DEBUG', 'getOrderLineNo', getOrderLineNo);
//		nlapiLogExecution('DEBUG', 'getClusterNo', getClusterNo);
//		nlapiLogExecution('DEBUG', 'getBatchNo', getBatchNo);
//		nlapiLogExecution('DEBUG', 'getNoofRecords', getNoofRecords);
//		nlapiLogExecution('DEBUG', 'getNextLocation', getNextLocation);
//		nlapiLogExecution('DEBUG', 'name', name);
//		nlapiLogExecution('DEBUG', 'getRecCount', getRecCount);
//		nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);
//		nlapiLogExecution('DEBUG', 'getEbizOrdNo', getEbizOrdNo);
//		nlapiLogExecution('DEBUG', 'NextExptdQty', NextExptdQty);
//		nlapiLogExecution('DEBUG', 'getnumber', getnumber);
//		nlapiLogExecution('DEBUG', 'getRecType', getRecType);
//		nlapiLogExecution('DEBUG', 'getSerOut', getSerOut);
//		nlapiLogExecution('DEBUG', 'getSerIn', getSerIn);
//		nlapiLogExecution('DEBUG', 'getBeginLocation', getBeginLocation);
//		nlapiLogExecution('DEBUG', 'getItemNo', getItemNo);
//		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);

		if(getBeginLocation==null)
		{
			getBeginLocation=getNextLocation;
		}
		if(getItemNo==null)
		{
			getItemNo=NextItemInternalId;
		}
		var LocArr=new Array();
		var IsPickFaceLoc = 'N';

		IsPickFaceLoc = isPickFaceLocation(getItemNo,getEndLocationInternalId);

		//The below line is added by Satish.N on 12/11/2012
		IsPickFaceLoc='N';
		//Upto here.

		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_waveno"] = request.getParameter('custparam_waveno');
		SOarray["custparam_recordinternalid"] = request.getParameter('custparam_recordinternalid');
		SOarray["custparam_containerlpno"] = request.getParameter('custparam_containerlpno');
		SOarray["custparam_expectedquantity"] = request.getParameter('custparam_expectedquantity');
		SOarray["custparam_beginLocation"] = request.getParameter('custparam_beginLocation');
		SOarray["custparam_item"] = request.getParameter('custparam_item');
		SOarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');
		SOarray["custparam_dolineid"] = request.getParameter('custparam_dolineid');
		SOarray["custparam_invoicerefno"] = request.getParameter('custparam_invoicerefno');
		SOarray["custparam_beginlocationname"] = request.getParameter('custparam_beginLocationname');
		SOarray["custparam_beginLocationname"] = request.getParameter('custparam_beginLocationname');
		SOarray["custparam_endlocinternalid"] = request.getParameter('custparam_endlocinternalid');
		SOarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
		SOarray["custparam_orderlineno"] = request.getParameter('custparam_orderlineno');
		SOarray["custparam_nextlocation"]=request.getParameter('custparam_nextlocation');
		SOarray["custparam_picktype"]=request.getParameter('custparam_picktype');
		SOarray["custparam_nooflocrecords"]=request.getParameter('custparam_nooflocrecords');
		SOarray["custparam_nextiteminternalid"]=request.getParameter('custparam_nextiteminternalid');
		SOarray["custparam_nextitem"]=request.getParameter('custparam_nextitem');
		SOarray["name"]=request.getParameter('name');
		SOarray["custparam_clusterno"] = request.getParameter('custparam_clusterno');        
		SOarray["custparam_number"]= request.getParameter('custparam_number');   
		SOarray["custparam_whlocation"]= request.getParameter('custparam_whlocation');   
		SOarray["custparam_skipid"] = request.getParameter('custparam_skipid');
		SOarray["custparam_ebizzoneno"] = request.getParameter('custparam_ebizzoneno');
		SOarray["custparam_fastpick"] = fastpick;
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('custparam_nextexpectedquantity');
		SOarray["custparam_itemType"] = itemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('custparam_Actbatchno');
		SOarray["custparam_Expbatchno"] = request.getParameter('custparam_Expbatchno');
		SOarray["custparam_batchno"] = request.getParameter('custparam_batchno');
		
		SOarray["custparam_lotnointernalid"] = request.getParameter('custparam_lotnointernalid');
			SOarray["custparam_displayquantity"] = request.getParameter('custparam_displayquantity');
		if(fastpick!="Y")
			SOarray["custparam_screenno"] = '12EXP';
		else
			SOarray["custparam_screenno"] = '13NEW';	

		if(IsPickFaceLoc=='T')
		{
			nlapiLogExecution('DEBUG', 'IsPickFaceLoc', IsPickFaceLoc);

			var openreplns = new Array();
			openreplns=getOpenReplns(getItemNo,getEndLocationInternalId);
			if(openreplns!=null && openreplns!='' && openreplns.length>0)
			{				
				SOarray["custparam_error"]='LOCATION EXCEPTION IS NOT ALLOWED AT PICKFACE LOCATION';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}

			var pfLocSearch = getPickFaceLocation(getItemNo,getEndLocationInternalId);
			if(pfLocSearch!=null && pfLocSearch!='' && pfLocSearch.length>0)
			{
				for(var j=0;j<pfLocSearch.length;j++)
				{
					LocArr.push(pfLocSearch[j].getValue('custrecord_pickbinloc'));
				}

				if(LocArr!=null && LocArr!='' && LocArr.length>0)
				{
					var arryinvt=getInvtDetails(getItemNo,LocArr,itemstatus);
					if(arryinvt==null || arryinvt=='')
					{
						var Loc = pfLocSearch[0].getText('custrecord_pickbinloc');
						if(fastpick!="Y")
							SOarray["custparam_screenno"] = '12EXP';
						else
							SOarray["custparam_screenno"] = '13NEW';
						//SOarray["custparam_error"]='EXCEPTION IS NOT POSSIBLE AS THERE IS NO INVENTORY IN LOCATION '+Loc+' FOR THIS ITEM';
						SOarray["custparam_error"]=st1 +Loc+ st2;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
				}
			}
			else
			{
				if(fastpick!="Y")
					SOarray["custparam_screenno"] = '12EXP';
				else
					SOarray["custparam_screenno"] = '13NEW';
				SOarray["custparam_error"]=st3;//'EXCEPTION IS NOT POSSIBLE AS THERE IS NO OTHER PICKFACE LOCATION FOR THIS ITEM';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}
		}

		var itemSubtype;
		var batchflag="F";
		var serialInflg="F";	
		if(getItemNo != null && getItemNo != '')
		{
			var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', getItemNo, fields);
			//var columns = nlapiLookupField('item', POarray["custparam_fetcheditemid"], 'recordType');

			if(columns != null && columns != '')
			{				
				itemSubtype = columns.recordType;
				serialInflg = columns.custitem_ebizserialin;
				batchflag= columns.custitem_ebizbatchlot;
				nlapiLogExecution('ERROR', 'citem subtype is =', itemSubtype);
			}
		}
		var solinebatch = '';
		var solinebatchid = '';
		if(itemSubtype == "lotnumberedinventoryitem" || itemSubtype == "lotnumberedassemblyitem")
		{			
			var folinerec = nlapiLoadRecord('customrecord_ebiznet_ordline',getdoLoineId);	

			solinebatch = folinerec.getFieldValue('custrecord_ebiz_lot_number');
			solinebatchid = folinerec.getFieldValue('custrecord_ebiz_lotno_internalid');
		}

		
		if(solinebatchid == null || solinebatchid == '' || solinebatchid == 'null')
		{

			solinebatchid = BatchId;
		}
		if(IsPickFaceLoc!='T')
		{
			nlapiLogExecution('DEBUG', 'IsPickFaceLoc', IsPickFaceLoc);
			nlapiLogExecution('DEBUG', 'getItemNo is', getItemNo);
			nlapiLogExecution('DEBUG', 'getQuantity is', getQuantity);
			nlapiLogExecution('DEBUG', 'getEndLocationInternalId is', getEndLocationInternalId);
			nlapiLogExecution('DEBUG', 'whLocation is', whLocation);
			nlapiLogExecution('DEBUG', 'itemstatus is', itemstatus);
			nlapiLogExecution('DEBUG', 'solinebatchid', solinebatchid);
			var arryinvt=PickStrategy(getItemNo, getQuantity,getEndLocationInternalId,whLocation,itemstatus,solinebatchid);
			if(arryinvt!=null && arryinvt!='')
			{
				arryinvt=removeDuplicateLocations(arryinvt);
			}
			nlapiLogExecution('DEBUG', 'arryinvt after remove duplicate is ', arryinvt);
		}
		// case # 20123468 start

		nlapiLogExecution('ERROR', 'solinebatch', solinebatch);
		nlapiLogExecution('ERROR', 'pickType', pickType);
		if(solinebatch!=null && solinebatch!='')
		{
			var vLotFound='F';
			if(arryinvt!=null && arryinvt!='')
			{
				for (var z = 0; z <  arryinvt.length ; z++) 
				{

					var vinvtLot = arryinvt[z][4];
					nlapiLogExecution('ERROR', 'vinvtLot', vinvtLot);
					if(vinvtLot==solinebatch)
					{
						nlapiLogExecution('ERROR', 'vinvtLot1', vinvtLot);
						vLotFound='T';
						break;
					}
				}
			}

			if(vLotFound=='F')
			{
				if(pickType=="CL")
					SOarray["custparam_screenno"] = 'PICKCC1';				
				SOarray["custparam_error"] = 'EXCEPTION IS NOT POSSIBLE AS REQUIRED LOT# IS NOT FOUND.';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}
		}
		//}
		//end

		var functionkeyHtml=getFunctionkeyScript('_rf_pickinglocexception'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('enterloc').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdConfirm').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_pickinglocexception' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + name + "</label> ## WAVE# :<label>" + getWaveNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st4 +"<label>" + getItem + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBeginLocation + ">";
		//case# 20127170 starts (single quotes added to getItem,getItemName)
		html = html + "				<input type='hidden' name='hdnItem' value='" + getItem + "'>";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		//case# 20127170 end
		html = html + "				<input type='hidden' name='hdnItemNo' value=" + getItemNo + ">";
		html = html + "				<input type='hidden' name='hdndoLoineId' value=" + getdoLoineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnEndLocationInternalId' value=" + getEndLocationInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEndLocation' value=" + getEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + getClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnNoofRecords' value=" + getNoofRecords + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + getNextLocation + ">";
		html = html + "				<input type='hidden' name='hdnname' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + getRecCount + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnEbizOrdNo' value=" + getEbizOrdNo + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";	
		html = html + "				<input type='hidden' name='hdnnumber' value=" + getnumber + ">";
		html = html + "				<input type='hidden' name='hdnRecType' value=" + getRecType + ">";
		html = html + "				<input type='hidden' name='hdnSerOut' value=" + getSerOut + ">";
		html = html + "				<input type='hidden' name='hdnSerIn' value=" + getSerIn + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnZoneNo' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnNextExptdQty' value=" + NextExptdQty + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		// Case# 20127886 starts
		//html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemSubtype + ">";
		// Case# 20127886 ends
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";	
		html = html + "				<input type='hidden' name='hdndetailTask' value=" + detailTask + ">";
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";
		html = html + "				<input type='hidden' name='hdnitemstatus' value=" + itemstatus + ">";
		html = html + "				<input type='hidden' name='hdnsobatch' value=" + solinebatch + ">";
		html = html + "				<input type='hidden' name='hdnsobatchid' value=" + solinebatchid + ">";
		html = html + "				<input type='hidden' name='hdnserialscanflag' value=" + serialscanflag + ">";
html = html + "				<input type='hidden' name='hdnActQty' value=" + getdisplayqty + ">";
		
		
		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr><td><table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left' width='100%'>";
		html = html + "				<label><u>Loc</u></label>";
		html = html + "				</td>";
		html = html + "				<td align = 'left' width='75%'>";
		html = html + "				<label><u>LoT</u></label>";
		html = html + "				</td>";
		html = html + "				<td align = 'left' width='25%'>";
		html = html + "				<label><u>Qty</u></label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		var vInvtLoc='';
		var vInvtQty='';
		var vInvtRecId='';
		var vInvtLocId='';
		var vInvtLotId='';
		var vInvtLotText='';
		var vLot='';


		var vInvttotLoc='';
		var vInvttotQty='';
		var vInvttotRecId='';
		var vInvttotLocId='';
		var vInvttotLotId='';
		var vInvttotLotText='';
		var vtotLot='';
		nlapiLogExecution('DEBUG', 'arryinvt length is ', arryinvt.length);//sri
		if(arryinvt != null && arryinvt != '' && arryinvt.length>0)
		{	
			for (var i = 0; i <  arryinvt.length ; i++) {
				var invtarray= arryinvt[i];

				if (invtarray != null)
				{
					var vtotLoc = invtarray[3];
					var vtotQty = invtarray[0];
					var vtotLot = invtarray[4];

					nlapiLogExecution('DEBUG', 'vLoc', invtarray[3]);
					nlapiLogExecution('DEBUG', 'vQty', invtarray[0]);
					nlapiLogExecution('DEBUG', 'lot', invtarray[4]);
					if(i==0)
					{
						if(arryinvt.length==1)
						{
							vInvttotLoc=vtotLoc+',';
							vInvttotQty=vtotQty+',';
							vInvttotRecId=invtarray[2]+',';
							vInvttotLocId=invtarray[1]+',';
							vInvttotLotId=invtarray[5]+',';
							vInvttotLotText=invtarray[4]+',';
						}
						else
						{
							vInvttotLoc=vtotLoc;
							vInvttotQty=vtotQty;
							vInvttotRecId=invtarray[2];
							vInvttotLocId=invtarray[1];
							vInvttotLotId=invtarray[5];
							vInvttotLotText=invtarray[4];
						}
					}
					else
					{
						vInvttotLoc += ','+ vtotLoc;
						vInvttotQty+=','+vtotQty;
						vInvttotRecId+=','+invtarray[2];
						vInvttotLocId+=','+invtarray[1];
						vInvttotLotId+=','+invtarray[5];
						vInvttotLotText+=','+invtarray[4];
					}	
				} 

			} 
		}


		nlapiLogExecution('DEBUG', 'vInvttotLotText1', vInvttotLotText);

		if(arryinvt != null && arryinvt != '' && arryinvt.length>0)
		{	
			nlapiLogExecution('DEBUG', 'arryinvt.length', arryinvt.length);
			for (var i = 0; i < Math.min(5, arryinvt.length); i++) {
				var invtarray= arryinvt[i];

				if (invtarray != null)
				{
					var vLoc = invtarray[3];
					var vQty = invtarray[0];
					var vLot = invtarray[4];

					html = html + "			<tr><td align = 'left' width='100%'>"+ vLoc +"</td>" ;
					html = html + "			<td align = 'left' width='25%'>"+ vLot +"</td>" ;
					html = html + "			<td align = 'left' width='25%'>"+ vQty.toFixed(4);
					html = html + "	</td></tr>";
					nlapiLogExecution('DEBUG', 'vLoc', vLoc);
					nlapiLogExecution('DEBUG', 'vQty', vQty);
					if(i==0)
					{
						if(arryinvt.length==1)
						{
							vInvtLoc=vLoc+',';
							vInvtQty=vQty+',';
							vInvtRecId=invtarray[2]+',';
							vInvtLocId=invtarray[1]+',';
							vInvtLotId=invtarray[5]+',';
							vInvtLotText=invtarray[4]+',';
						}
						else
						{
							vInvtLoc=vLoc;
							vInvtQty=vQty;
							vInvtRecId=invtarray[2];
							vInvtLocId=invtarray[1];
							vInvtLotId=invtarray[5];
							vInvtLotText=invtarray[4];
						}
					}
					else
					{
						vInvtLoc += ','+ vLoc;
						vInvtQty+=','+vQty;
						vInvtRecId+=','+invtarray[2];
						vInvtLocId+=','+invtarray[1];
						vInvtLotId+=','+invtarray[5];
						vInvtLotText+=','+invtarray[4];
					}	
				} 

			} 
		}

		nlapiLogExecution('DEBUG', 'vInvtLotText2', vInvtLotText);
		html = html + "			</table></td></tr><tr>";
		html = html + "				<td align = 'left'>"+ st6 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterloc' id='enterloc' type='text'/>";
		html = html + "				<input type='hidden' name='hdninvloc' value=" + vInvtLoc + ">";
		html = html + "				<input type='hidden' name='hdninvqty' value=" + vInvtQty + ">";
		html = html + "				<input type='hidden' name='hdninvrec' value=" + vInvtRecId + ">";
		html = html + "				<input type='hidden' name='hdninvlocid' value=" + vInvtLocId + ">";
		html = html + "				<input type='hidden' name='hdninvlotid' value=" + vInvtLotId + ">";
		html = html + "				<input type='hidden' name='hdninvlottext' value='" + vInvtLotText + "'>";
		html = html + "				<input type='hidden' name='hdntotinvloc' value='" + vInvttotLoc + "'>";
		html = html + "				<input type='hidden' name='hdntotinvqty' value=" + vInvttotQty + ">";
		html = html + "				<input type='hidden' name='hdntotinvrec' value=" + vInvttotRecId + ">";
		html = html + "				<input type='hidden' name='hdntotinvlocid' value=" + vInvttotLocId + ">";
		html = html + "				<input type='hidden' name='hdntotinvlotid' value=" + vInvttotLotId + ">";
		html = html + "				<input type='hidden' name='hdntotinvlottext' value='" + vInvttotLotText + "'>";
		html = html + "				<input type='hidden' name='hdnarrayinvtresults' value=" + arryinvt + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		//if(itemType=='lotnumberedinventoryitem')
		// case # 20123468 start
		if (itemSubtype == 'lotnumberedinventoryitem' || itemSubtype=="lotnumberedassemblyitem" || batchflag=="T") 
		{// end
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>ENTER/SCAN ACTUAL LOT#";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterlot' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st7 +" <input name='cmdConfirm' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st8 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterloc').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response');

		var fastpick = request.getParameter('hdnfastpick');

		nlapiLogExecution('ERROR', 'fastpick', fastpick);

		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		// Entered Location
		var getEnteredLocation = request.getParameter('enterloc');
		var getEnteredLoT = request.getParameter('enterlot');
		var getInvLocs = request.getParameter('hdninvloc');
		var getInvQtys = request.getParameter('hdninvqty');
		var getInvRecs = request.getParameter('hdninvrec');
		var getInvLocsId = request.getParameter('hdninvlocid');
		var getInvLotsId = request.getParameter('hdninvlotid');
		var getInvLotText = request.getParameter('hdninvlottext');
		var getInvtotLocs = request.getParameter('hdntotinvloc');
		var getInvtotQtys = request.getParameter('hdntotinvqty');
		var getInvtotRecs = request.getParameter('hdntotinvrec');
		var getInvtotLocsId = request.getParameter('hdntotinvlocid');
		var getInvtotLotsId = request.getParameter('hdntotinvlotid');
		var getInvtotLotText = request.getParameter('hdntotinvlottext');
		var getItemStatus = request.getParameter('hdnitemstatus');

		nlapiLogExecution('DEBUG', 'getEnteredLocation', getEnteredLocation);
		nlapiLogExecution('DEBUG', 'getInvLocs', getInvLocs);
		nlapiLogExecution('DEBUG', 'getInvQtys', getInvQtys);
		nlapiLogExecution('DEBUG', 'getInvRecs', getInvRecs);
		nlapiLogExecution('DEBUG', 'getInvLocsId', getInvLocsId);
		nlapiLogExecution('DEBUG', 'getInvLotId', getInvLotsId);
		nlapiLogExecution('DEBUG', 'getInvLotText', getInvLotText);
		nlapiLogExecution('DEBUG', 'getInvtotLocs', getInvtotLocs);
		nlapiLogExecution('DEBUG', 'getInvtotQtys', getInvtotQtys);
		nlapiLogExecution('DEBUG', 'getInvtotRecs', getInvtotRecs);
		nlapiLogExecution('DEBUG', 'getInvtotLocsId', getInvtotLocsId);
		nlapiLogExecution('DEBUG', 'getInvtotLotsId', getInvtotLotsId);
		nlapiLogExecution('DEBUG', 'getInvtotLotText', getInvtotLotText);

		/*var getEnteredReason = request.getParameter('enterreason');
		nlapiLogExecution('DEBUG', 'getEnteredReason', getEnteredReason);
		 */
		//var optedEvent = request.getParameter('cmdPrevious');

		var pickLocExceptionarray = new Array();
		var getLanguage =  request.getParameter('hdngetLanguage');

		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);


		var st9,st10;

		if( getLanguage == 'es_ES')
		{

			st9 = "POR FAVOR ENTRAR UBICACI&#211;N";
			st10 = "ubicaci&#243;n no v&#225;lida";

		}
		else
		{	
			st9 = "PLEASE ENTER LOCATION";
			st10 = "INVALID LOCATION / LOT#";				
		}

		pickLocExceptionarray["custparam_language"] = getLanguage;
		var WaveNo = request.getParameter('hdnWaveNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLpNo = request.getParameter('hdnContainerLpNo');
		var FetchedQuantity = request.getParameter('hdnQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocation');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var ItemNo = request.getParameter('hdnItemNo');
		var doLoineId = request.getParameter('hdndoLoineId');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginLocationName');
		var EndLocationInternalId = request.getParameter('hdnEndLocationInternalId');
		var EndLocation = request.getParameter('hdnEndLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var ClusterNo = request.getParameter('hdnClusterNo');
		var BatchNo = request.getParameter('hdnBatchNo');
		

		//var BatchNo = request.getParameter('enterlot');
		var NoofRecords = request.getParameter('hdnNoofRecords');
		var NextLocation = request.getParameter('hdnNextLocation');
		var name = request.getParameter('hdnname');
		var RecCount = request.getParameter('hdnRecCount');
		var ContainerSize = request.getParameter('hdnContainerSize');
		var EbizOrdNo = request.getParameter('hdnEbizOrdNo');
		var NextItemInternalId=request.getParameter('hdnNextItemId');
		var NextExptdQty=request.getParameter('hdnNextExptdQty');
		var ItemType=request.getParameter('hdnitemtype');
		var detailtask=request.getParameter('hdndetailTask');		
		var number = request.getParameter('hdnnumber');
		var RecType = request.getParameter('hdnRecType');
		var SerOut = request.getParameter('hdnSerOut');
		var SerIn = request.getParameter('hdnSerIn');
		var Picktype = request.getParameter('hdnpicktype');
		var vwhloc = request.getParameter('hdnwhlocation');
		var vsolinebatch = request.getParameter('hdnsobatch');
		var solinebatchid = request.getParameter('hdnsobatchid'); 
		pickLocExceptionarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		pickLocExceptionarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		pickLocExceptionarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		pickLocExceptionarray["custparam_fastpick"] = fastpick;
	pickLocExceptionarray["custparam_displayquantity"] = request.getParameter('hdnActQty');

		nlapiLogExecution('DEBUG', 'vwhloc', vwhloc);
		nlapiLogExecution('DEBUG', 'solinebatchid', solinebatchid);
		
		
		var vinventoryresults=PickStrategy(ItemNo, FetchedQuantity,EndLocationInternalId,vwhloc,getItemStatus,solinebatchid);
		if(vinventoryresults!=null && vinventoryresults!='')
		{
			vinventoryresults=removeDuplicateLocations(vinventoryresults);
		}
		nlapiLogExecution('DEBUG', 'vinventoryresults after remove duplicate is ', vinventoryresults);

//		nlapiLogExecution('DEBUG', 'getFetchedQuantity', FetchedQuantity);
//		nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
//		nlapiLogExecution('DEBUG', 'RecordInternalId', RecordInternalId);
//		nlapiLogExecution('DEBUG', 'ContainerLpNo', ContainerLpNo);
//		nlapiLogExecution('DEBUG', 'FetchedQuantity', FetchedQuantity);
//		nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);
//		nlapiLogExecution('DEBUG', 'Item', Item);
//		nlapiLogExecution('DEBUG', 'ItemName', ItemName);
//		nlapiLogExecution('DEBUG', 'ItemNo', ItemNo);
//		nlapiLogExecution('DEBUG', 'doLoineId', doLoineId);
//		nlapiLogExecution('DEBUG', 'InvoiceRefNo', InvoiceRefNo);
//		nlapiLogExecution('DEBUG', 'BeginLocationName', BeginLocationName);
//		nlapiLogExecution('DEBUG', 'EndLocationInternalId', EndLocationInternalId);
//		nlapiLogExecution('DEBUG', 'EndLocation', EndLocation);
//		nlapiLogExecution('DEBUG', 'OrderLineNo', OrderLineNo);
//		nlapiLogExecution('DEBUG', 'ClusterNo', ClusterNo);
//		nlapiLogExecution('DEBUG', 'BatchNo', BatchNo);
//		nlapiLogExecution('DEBUG', 'NoofRecords', NoofRecords);
//		nlapiLogExecution('DEBUG', 'NextLocation', NextLocation);
//		nlapiLogExecution('DEBUG', 'name', name);
//		nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);
//		nlapiLogExecution('DEBUG', 'EbizOrdNo', EbizOrdNo);
//		nlapiLogExecution('DEBUG', 'number', number);
//		nlapiLogExecution('DEBUG', 'RecType', RecType);
//		nlapiLogExecution('DEBUG', 'SerOut', SerOut);
//		nlapiLogExecution('DEBUG', 'SerIn', SerIn);
//		nlapiLogExecution('DEBUG', 'Picktype', Picktype);


		//pickLocExceptionarray["custparam_enteredReason"] = getEnteredReason;
		//pickLocExceptionarray["custparam_fetchedQty"] = FetchedQuantity;
		pickLocExceptionarray["custparam_serialscanflag"] = request.getParameter('hdnserialscanflag');
		pickLocExceptionarray["custparam_screenno"] = 'PickLocEXP';
		pickLocExceptionarray["custparam_waveno"] = WaveNo;
		pickLocExceptionarray["custparam_recordinternalid"] = RecordInternalId;
		pickLocExceptionarray["custparam_containerlpno"] = ContainerLpNo;
		pickLocExceptionarray["custparam_expectedquantity"] = FetchedQuantity;
		pickLocExceptionarray["custparam_beginLocation"] = BeginLocation;
		pickLocExceptionarray["custparam_item"] = Item;
		pickLocExceptionarray["custparam_itemdescription"] = ItemName;
		pickLocExceptionarray["custparam_iteminternalid"] = ItemNo;
		pickLocExceptionarray["custparam_dolineid"] = doLoineId;
		pickLocExceptionarray["custparam_invoicerefno"] = InvoiceRefNo;
		pickLocExceptionarray["custparam_beginlocationname"] = BeginLocationName;
		pickLocExceptionarray["custparam_beginLocationname"] = BeginLocationName;
		pickLocExceptionarray["custparam_endlocinternalid"] = EndLocationInternalId;
		pickLocExceptionarray["custparam_endlocation"] = EndLocation;
		pickLocExceptionarray["custparam_orderlineno"] = OrderLineNo;
		pickLocExceptionarray["custparam_clusterno"] = ClusterNo;
		pickLocExceptionarray["custparam_batchno"] = BatchNo;
		pickLocExceptionarray["custparam_noofrecords"] = NoofRecords;
		pickLocExceptionarray["custparam_nextlocation"] = NextLocation;
		pickLocExceptionarray["name"] = name;
		pickLocExceptionarray["custparam_containersize"] = ContainerSize;
		pickLocExceptionarray["custparam_ebizordno"] = EbizOrdNo;
		pickLocExceptionarray["custparam_number"] = number;
		pickLocExceptionarray["custparam_RecType"] = RecType;
		pickLocExceptionarray["custparam_SerOut"] = SerOut;
		pickLocExceptionarray["custparam_SerIn"] = SerIn;
		pickLocExceptionarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		pickLocExceptionarray["custparam_nextexpectedquantity"] = request.getParameter('hdnNextExptdQty');
		var vSkipId=request.getParameter('hdnskipid');
		pickLocExceptionarray["custparam_skipid"] = request.getParameter('hdnskipid');
		pickLocExceptionarray["custparam_ebizzoneno"] = request.getParameter('hdnZoneNo');;
		pickLocExceptionarray["custparam_itemType"] = ItemType;
		pickLocExceptionarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		pickLocExceptionarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');
		pickLocExceptionarray["custparam_detailtask"] = detailtask;
		pickLocExceptionarray["custparam_itemname"] = request.getParameter('hdnItemName');
		// Case# 20127440 starts
		//pickLocExceptionarray["custparam_itemstatus"] = request.getParameter('getItemStatus');
		pickLocExceptionarray["custparam_itemstatus"] = request.getParameter('hdnitemstatus');
		pickLocExceptionarray["custparam_fetchcontainerlpno"] = ContainerLpNo;
		// Case# 20127440 end

		nlapiLogExecution('DEBUG', 'hdnNextItemId', request.getParameter('hdnNextItemId'));
		//Case # 20148679  start

		pickLocExceptionarray["custparam_nooflocrecords"] =request.getParameter('custparam_nooflocrecords');
		//Case # 20148679  end
//		if the previous button 'F7' is clicked, it has to go to the previous screen 
		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, pickLocExceptionarray);
			if(Picktype =='CL')
			{   nlapiLogExecution('DEBUG', 'Cluster', request.getParameter('cmdPrevious'));
				response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, pickLocExceptionarray);
			}
			else
			{
				if(fastpick !='Y') // case# 201414553
				{	nlapiLogExecution('DEBUG', 'fastpick', request.getParameter('cmdPrevious'));
					response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, pickLocExceptionarray);
				}
				else
				{	nlapiLogExecution('DEBUG', 'check', request.getParameter('cmdPrevious'));
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, pickLocExceptionarray);
				}
			}
		}
		else 
			nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
		if (request.getParameter('hdnflag') == 'ENT') {    
			
			
			var itemSubtype;
			var batchflag="F";
			var serialInflg="F";	
			if(ItemNo != null && ItemNo != '')
			{
				var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
				var columns = nlapiLookupField('item', ItemNo, fields);
				//var columns = nlapiLookupField('item', POarray["custparam_fetcheditemid"], 'recordType');

				if(columns != null && columns != '')
				{				
					itemSubtype = columns.recordType;
					serialInflg = columns.custitem_ebizserialin;
					batchflag= columns.custitem_ebizbatchlot;
					nlapiLogExecution('ERROR', 'citem subtype is =', itemSubtype);
				}
			}
		
			var EmptyLotEntered='F';
			if(itemSubtype == "lotnumberedinventoryitem" || itemSubtype == "lotnumberedassemblyitem" || batchflag=="T")
			{
				if(getEnteredLoT == null || getEnteredLoT == '')
				{
					EmptyLotEntered='T';
				}
			}
			if((getEnteredLocation == null || getEnteredLocation == '')||(EmptyLotEntered=='T'))
			{
				pickLocExceptionarray["custparam_error"] = 'PLEASE ENTER LOCATION';
				if(EmptyLotEntered=='T')
					pickLocExceptionarray["custparam_error"] = 'PLEASE ENTER LOT';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickLocExceptionarray);
				nlapiLogExecution('DEBUG', 'lOCATION NOT ENTERED');
			}
			else
			{
				nlapiLogExecution('DEBUG', 'vsolinebatch',vsolinebatch);
				nlapiLogExecution('DEBUG', 'getEnteredLoT',getEnteredLoT);

				if(vsolinebatch!=null && vsolinebatch!='' && vsolinebatch!='null' && vsolinebatch!='undefined')
				{
					if(vsolinebatch!=getEnteredLoT)
					{
						pickLocExceptionarray["custparam_error"] = 'GIVEN LOT# IS DIFFERENT FROM THE LOT ON SALES ORDER LINE.';
						nlapiLogExecution('DEBUG', 'GIVEN LOT# IS DIFFERENT FROM THE LOT ON SALES ORDER LINE.');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickLocExceptionarray);
						return;
					}
				}

				if(ItemType=='lotnumberedinventoryitem')
				{
					var vnsinvlotfound = fnvalidategivenlot(ItemNo,vwhloc,getEnteredLoT)
					if(vnsinvlotfound=='F')
					{
						pickLocExceptionarray["custparam_error"] = 'EXCEPTION IS NOT POSSIBLE AS GIVEN LOT# IS NOT FOUND IN NETSUITE.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickLocExceptionarray);
						return;
					}
				}

				var errorflag='F';
				var vRecId='';
				var vLocId='';
				var vLP='';
				var InvalidLot='T';
				nlapiLogExecution('DEBUG', 'getInvLocs', getInvLocs);
				nlapiLogExecution('DEBUG', 'getInvtotLotText', getInvtotLotText);
				//if(getInvLocs != null && getInvLocs  != "" && getInvLocs.indexOf(',') != -1)
				
				
				if(vinventoryresults != null && vinventoryresults != '' && vinventoryresults.length>0)
				{	
					
					var tempcount=0;
					var vLotId=null;
					var tempflag='F';
					var templocflag = 'F';
					nlapiLogExecution('DEBUG', 'Length', vinventoryresults.length);
					for (var i1 = 0; i1 <  vinventoryresults.length ; i1++) 
					{
						var vinvtarray= vinventoryresults[i1];

						if (vinvtarray != null)
						{
							var vQtyArr = vinvtarray[0];
							var vRecArr = vinvtarray[1]; 
							var vLocIdArr = vinvtarray[2];
							var vLocArr = vinvtarray[3];
							var vLotIdArr = vinvtarray[5];							
							var vLotTextArr = vinvtarray[4];

							nlapiLogExecution('DEBUG', 'vQtyArr', vQtyArr);
							nlapiLogExecution('DEBUG', 'vRecArr', vRecArr);
							nlapiLogExecution('DEBUG', 'vLocIdArr', vLocIdArr);
							nlapiLogExecution('DEBUG', 'vtotLoc', vtotLoc);
							nlapiLogExecution('DEBUG', 'vLotIdArr', vLotIdArr);
							nlapiLogExecution('DEBUG', 'vLotTextArr', vLotTextArr);

							
							

							nlapiLogExecution('DEBUG', 'vLocArr', vLocArr);
							nlapiLogExecution('DEBUG', 'getEnteredLocation', getEnteredLocation);
							if(vLocArr.toUpperCase()==getEnteredLocation.toUpperCase())
							{
								templocflag='T';
								tempcount=i1;
								vRecId=vRecArr;
								vLocId=vLocIdArr;
								vLotId=vLotIdArr;
								nlapiLogExecution('DEBUG', 'vRecId',vRecId);
								nlapiLogExecution('DEBUG', 'vLocId',vLocId);
								nlapiLogExecution('DEBUG', 'vLotId',vLotId);
								//break;
							}
							



							nlapiLogExecution('DEBUG', 'vLotTextArr', vLotTextArr);
							nlapiLogExecution('DEBUG', 'getEnteredLoT', getEnteredLoT);
							//var vOldLot=vLotTextArr[p1].toUpperCase();

							/*if(getEnteredLoT!=null && getEnteredLoT!='')
							{

								if(vLotTextArr==getEnteredLoT)
								{
									tempflag='T';
									//break;
								}	
							}*/

							nlapiLogExecution('DEBUG', 'ItemType', ItemType);

							if(ItemType=='lotnumberedinventoryitem' || ItemType =='lotnumberedassemblyitem')
							{
								tempflag='F';
									if(getEnteredLoT!=null && getEnteredLoT!='')
									{
										if(vLotTextArr==getEnteredLoT)
										{
											nlapiLogExecution('DEBUG', 'tempcount', tempcount);
											nlapiLogExecution('DEBUG', 'i1', i1);
											if(tempcount==i1){
												tempflag='T';
												InvalidLot='F';
											}
										}
									}

								
							}
						}
					}
					if(templocflag=='F')
					{
						nlapiLogExecution('DEBUG', 'Invalid Location',getEnteredLocation);
						pickLocExceptionarray["custparam_error"] = st10;//'INVALID LOCATION';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickLocExceptionarray);
						return false;
					}
					//case# 20148264 starts (Invalid lot validation message)
					if(ItemType=='lotnumberedinventoryitem' || ItemType =='lotnumberedassemblyitem')
					{
						if(InvalidLot=='T'){
							pickLocExceptionarray["custparam_error"] = "INVALID LOT";
							nlapiLogExecution('DEBUG', 'InvalidLot',InvalidLot);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickLocExceptionarray);
							return false;
						}
					}
					//case# 20148264 end




					nlapiLogExecution('DEBUG', 'tempflag',tempflag);
					nlapiLogExecution('DEBUG', 'InvalidLot',InvalidLot);

					//if(tempflag == 'T')//this condition is removed because ,if we have other than lot item then tempflag never becomes 'T'
					//{							
						var locfilters = new Array();	
						locfilters[0] = new nlobjSearchFilter('name', null, 'is', getEnteredLocation);
						locfilters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
						var loccolumns = new Array();
						loccolumns[0] = new nlobjSearchColumn('name');						
						var locsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, locfilters, loccolumns);

						if(locsearchresults != null && locsearchresults != '')
						{	
							vLocId=locsearchresults[0].getId();
						}
						nlapiLogExecution('DEBUG', 'else vLocId',vLocId);
						//vRecId
						var recfilters = new Array();	
						recfilters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', vLocId);

						//code added on 17th July 2012 by suman.
						recfilters[1] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', ItemNo);
						recfilters[2] = new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0);
						recfilters[3] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']);//3=putaway complete 19-storage
						if(vLotId!=null && vLotId!="")
							recfilters[4] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', vLotId);
						//end of code as of 17th July 2012.

						var reccolumns = new Array();
						reccolumns[0] = new nlobjSearchColumn('name');	
						reccolumns[1]=new nlobjSearchColumn('custrecord_ebiz_inv_lp');
						//case# 20148648 starts(getting refno lessthan the pick qty)
						reccolumns[2]=new nlobjSearchColumn('id');
						reccolumns[3]=new nlobjSearchColumn('custrecord_ebiz_qoh');

						reccolumns[2].setSort(true);
						reccolumns[3].setSort(false);
						//case# 20148648 ends
						var recsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, recfilters, reccolumns);

						if(recsearchresults != null && recsearchresults != '')
						{
							errorflag='T';
							vLP=recsearchresults[0].getValue('custrecord_ebiz_inv_lp');
							//Case # 20126303Ã¯Â¿Â½ start
							VInvrefNo=recsearchresults[0].getId();
							//Case # 20126303Ã¯Â¿Â½ End
							nlapiLogExecution('DEBUG','vLP',vLP);
							nlapiLogExecution('DEBUG', 'ItemNo', ItemNo);
							vRecId=recsearchresults[0].getId();
							nlapiLogExecution('DEBUG','vRecId',vRecId);
							if(Picktype =='CL')
							{
								nlapiLogExecution('DEBUG','Inside pick type CL',Picktype);
								nlapiLogExecution('DEBUG','ClusterNo',ClusterNo);
								nlapiLogExecution('DEBUG','EndLocationInternalId',EndLocationInternalId);
								var SOFilters = new Array();

								SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
								SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
								if(ClusterNo!=null && ClusterNo!='')
									SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusterNo));
								if(EndLocationInternalId!=null && EndLocationInternalId!='')
									SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', EndLocationInternalId));
								if(ItemNo!=null && ItemNo!='')
									SOFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', ItemNo));
								
								var SOColumns = new Array();
//								SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
								//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
								SOColumns[0]=new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
								//Code end as on 290414
								SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
								SOColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
								SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
								SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
								SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
								SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
								SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
								SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
								SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
								SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');
								SOColumns[11] = new nlobjSearchColumn('custrecord_wms_location');
								SOColumns[12] = new nlobjSearchColumn('custrecord_comp_id');
								SOColumns[13] = new nlobjSearchColumn('name');
								SOColumns[14] = new nlobjSearchColumn('custrecord_container');
								SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_order_no');
								SOColumns[16] = new nlobjSearchColumn('custrecord_container_lp_no');
								SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
								SOColumns[18] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
								SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');
								SOColumns[20] = new nlobjSearchColumn('custrecord_taskassignedto');
								
								SOColumns[0].setSort();	
								SOColumns[1].setSort();		//	Location Pick Sequence
								SOColumns[3].setSort();		//	SKU
								
								var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
								if (SOSearchResults != null && SOSearchResults.length > 0) 
								{
									for(var f=0;f<SOSearchResults.length;f++)
									{
										var RecordId=SOSearchResults[f].getId();
										nlapiLogExecution("ERROR","RecordId",RecordId);
										var vLpRec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',RecordId);

										var fromlp=vLpRec.getFieldValue('custrecord_from_lp_no');
										var oldLocation=vLpRec.getFieldValue('custrecord_actbeginloc');
										var expQty=vLpRec.getFieldValue('custrecord_expe_qty');
										vLpRec.setFieldValue('custrecord_lpno',vLP);
										vLpRec.setFieldValue('custrecord_from_lp_no',vLP);
										vLpRec.setFieldValue('custrecord_batch_no',getEnteredLoT);
										nlapiSubmitRecord(vLpRec);

										//Case # 20124670 Start

										var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
										var columns = nlapiLookupField('item', ItemNo, fields);
										if(columns != null && columns != '')
										{
											Itype = columns.recordType;	
											serialInflg = columns.custitem_ebizserialin;
										}
										nlapiLogExecution("ERROR","Itype",Itype);
										nlapiLogExecution("ERROR","serialInflg",serialInflg);							
										if (Itype == "serializedinventoryitem"|| Itype == "serializedassemblyitem" || serialInflg == "T") {
											if(fromlp!=null && fromlp!='')
											{
												nlapiLogExecution('DEBUG', 'fromlp',fromlp);
												var Serialfilters = new Array();	
												Serialfilters[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', fromlp);
												nlapiLogExecution('DEBUG', 'ItemNo',ItemNo);
												//code added on 17th July 2012 by suman.
												Serialfilters[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', ItemNo);
												nlapiLogExecution('DEBUG', 'oldLocation',oldLocation);
												Serialfilters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof',['3','19']);//3=putaway complete 19-storage
												if(oldLocation!=null && oldLocation!="")
													Serialfilters[3] = new nlobjSearchFilter('custrecord_serialbinlocation', null, 'anyof', oldLocation);
												//end of code as of 17th July 2012.


												var SerailSearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, Serialfilters, null);
												nlapiLogExecution('DEBUG', 'SerailSearchresults',SerailSearchresults);
												if(SerailSearchresults!=null && SerailSearchresults!='' && SerailSearchresults.length>0)
												{
													for(var k1=0;k1<SerailSearchresults.length && k1<parseInt(expQty);k1++)
													{
														var SerialId=SerailSearchresults[k1].getId();
														nlapiLogExecution('DEBUG', 'updating MasterLp for the Serial',SerialId);
														nlapiLogExecution('DEBUG', 'updating MasterLp with',vLP);
														var vSerialRec=nlapiLoadRecord('customrecord_ebiznetserialentry',SerialId);
														vSerialRec.setFieldValue('custrecord_serialparentid',vLP);
														nlapiSubmitRecord(vSerialRec);
													}
												}
											}

										}
									}
								}
							}
							else
							{
								var vLpRec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',RecordInternalId);

								var fromlp=vLpRec.getFieldValue('custrecord_from_lp_no');
								var oldLocation=vLpRec.getFieldValue('custrecord_actbeginloc');
								var expQty=vLpRec.getFieldValue('custrecord_expe_qty');
								vLpRec.setFieldValue('custrecord_lpno',vLP);
								vLpRec.setFieldValue('custrecord_from_lp_no',vLP);
								vLpRec.setFieldValue('custrecord_batch_no',getEnteredLoT);
								nlapiSubmitRecord(vLpRec);

								//Case # 20124670 Start

								var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
								var columns = nlapiLookupField('item', ItemNo, fields);
								if(columns != null && columns != '')
								{
									Itype = columns.recordType;	
									serialInflg = columns.custitem_ebizserialin;
								}
								nlapiLogExecution("ERROR","Itype",Itype);
								nlapiLogExecution("ERROR","serialInflg",serialInflg);							
								if (Itype == "serializedinventoryitem"|| Itype == "serializedassemblyitem" || serialInflg == "T") {
									if(fromlp!=null && fromlp!='')
									{
										nlapiLogExecution('DEBUG', 'fromlp',fromlp);
										var Serialfilters = new Array();	
										Serialfilters[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', fromlp);
										nlapiLogExecution('DEBUG', 'ItemNo',ItemNo);
										//code added on 17th July 2012 by suman.
										Serialfilters[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', ItemNo);
										nlapiLogExecution('DEBUG', 'oldLocation',oldLocation);
										Serialfilters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof',['3','19']);//3=putaway complete 19-storage
										if(oldLocation!=null && oldLocation!="")
											Serialfilters[3] = new nlobjSearchFilter('custrecord_serialbinlocation', null, 'anyof', oldLocation);
										//end of code as of 17th July 2012.


										var SerailSearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, Serialfilters, null);
										nlapiLogExecution('DEBUG', 'SerailSearchresults',SerailSearchresults);
										if(SerailSearchresults!=null && SerailSearchresults!='' && SerailSearchresults.length>0)
										{
											for(var k1=0;k1<SerailSearchresults.length && k1<parseInt(expQty);k1++)
											{
												var SerialId=SerailSearchresults[k1].getId();
												nlapiLogExecution('DEBUG', 'updating MasterLp for the Serial',SerialId);
												nlapiLogExecution('DEBUG', 'updating MasterLp with',vLP);
												var vSerialRec=nlapiLoadRecord('customrecord_ebiznetserialentry',SerialId);
												vSerialRec.setFieldValue('custrecord_serialparentid',vLP);
												nlapiSubmitRecord(vSerialRec);
											}
										}
									}

								}
							}
							//Case # 20124670 End
						}
					//}
				}
				nlapiLogExecution('DEBUG', 'errorflag',errorflag);
				if(errorflag=='T')
				{
					pickLocExceptionarray["custparam_entloc"] = getEnteredLocation;
					pickLocExceptionarray["custparam_recid"] = vRecId;
					pickLocExceptionarray["custparam_entlocid"] = vLocId; 
					pickLocExceptionarray["custparam_entLotId"] = getEnteredLoT;
					//pickLocExceptionarray["custparam_containerlpno"] = vLP;
					//pickLocExceptionarray["custparam_batchno"] = getEnteredLoT;

					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_loc_exc_confrm', 'customdeploy_ebiz_rf_pick_loc_exc_con_di', false, pickLocExceptionarray);
				}
				else 
				{
					/*nlapiLogExecution('DEBUG', 'Invalid Location',getEnteredLocation);
					pickLocExceptionarray["custparam_error"] = st10;//'INVALID LOCATION';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickLocExceptionarray);*/
					//case# 20148588 starts

					nlapiLogExecution('DEBUG', 'Invalid Location',getEnteredLocation);
					pickLocExceptionarray["custparam_error"] = st10;//'INVALID LOCATION';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickLocExceptionarray);
					//case# 20148588 ends
				}
			}
		}
	}
}


var tempPikzoneResultsArray=new Array();
function getpikzone(maxno,whLocation)
{


	var filterszone = new Array();	
	//filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', [vpickzone]);
	filterszone[0] = new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',whLocation]);
	filterszone[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	if(maxno!=-1)
	{
		filterszone[2] = new nlobjSearchFilter('id', null,'lessthan', maxno);
	}


	var columnzone = new Array();

	columnzone[0] = new nlobjSearchColumn('id');
	columnzone[0].setSort(true);
	columnzone[1] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
	columnzone[2] = new nlobjSearchColumn('custrecord_locgroup_no');
	columnzone[3] = new nlobjSearchColumn('custrecord_zone_seq');//.setSort();				

	var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);

	if(searchresults!=null)
	{
		if(searchresults.length>=1000)
		{	
			var maxno1=searchresults[searchresults.length-1].getId();	
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempPikzoneResultsArray.push(searchresults[y]);
			}
			getpikzone(maxno1,whLocation);
		}
		else
		{
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempPikzoneResultsArray.push(searchresults[y]);
			}
		}
	}

	nlapiLogExecution('DEBUG', 'tempPikzoneResultsArray', tempPikzoneResultsArray.length);
	return tempPikzoneResultsArray;


}


var tempLocGrouResultsArray=new Array();
function getLocGroup(maxno,item,OldLocationInternalId,itemstatus,lotintid)
{

	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['19']));
	filtersinvt.push(new nlobjSearchFilter('isinactive','custrecord_ebiz_inv_binloc', 'is','F'));
	if(OldLocationInternalId != null && OldLocationInternalId != '')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof',OldLocationInternalId));
	if(lotintid != null && lotintid != '' && lotintid != 'null')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof',[lotintid]));
	//filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));				 

	if(itemstatus != null && itemstatus != '' && itemstatus!='null')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof',itemstatus));

	//filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_invholdflg', null, 'is', 'F'));
	//filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_cycl_count_hldflag', null, 'is', 'F'));

	if(maxno!=-1)
	{
		filtersinvt.push(new nlobjSearchFilter('id', null,'lessthan', maxno));
	}
	//nlapiLogExecution('DEBUG', "custrecord_ebiz_inv_sku: " + item);

	var columnsinvt = new Array();

	columnsinvt.push(new nlobjSearchColumn('id'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));					
	columnsinvt.push(new nlobjSearchColumn('custrecord_pickseqno'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_qty'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));				 
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc'));

	columnsinvt[0].setSort(true);
	columnsinvt[1].setSort();
	columnsinvt[2].setSort();				
	columnsinvt[3].setSort();
	columnsinvt[4].setSort(false);
	columnsinvt[5].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);


	if(searchresults!=null)
	{
		if(searchresults.length>=1000)
		{	
			var maxno1=searchresults[searchresults.length-1].getId();	
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempLocGrouResultsArray.push(searchresults[y]);
			}
			getLocGroup(maxno,item,OldLocationInternalId,itemstatus);
		}
		else
		{
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempLocGrouResultsArray.push(searchresults[y]);
			}
		}
	}

	nlapiLogExecution('DEBUG', 'tempLocGrouResultsArray', tempLocGrouResultsArray.length);
	return tempLocGrouResultsArray;


}

function PickStrategy(item,avlqty,OldLocationInternalId,whLocation,itemstatus,lotintid){
	//alert("5");
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('DEBUG','item',item); 

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('item',item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;

	nlapiLogExecution('DEBUG', 'Item', item);
	nlapiLogExecution('DEBUG', 'ItemFamily', ItemFamily);
	nlapiLogExecution('DEBUG', 'ItemGroup', ItemGroup);
	nlapiLogExecution('DEBUG', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('DEBUG', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('DEBUG', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('DEBUG', 'whLocation', whLocation);

	var filters = new Array();	
	filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', ['@NONE@',whLocation]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@']));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof',['@NONE@', ItemInfo1]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof',['@NONE@']));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof',['@NONE@', ItemInfo2]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof',['@NONE@']));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof',['@NONE@', ItemInfo3]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof',['@NONE@']));
	}

	if(itemstatus!=null && itemstatus != "" && itemstatus!='null')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskustatspickrul', null, 'anyof',['@NONE@', itemstatus]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskustatspickrul', null, 'anyof',['@NONE@']));
	}

	var k=1; 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	//columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul').setSort();
	columns[3]=new nlobjSearchColumn('formulanumeric');
	columns[3].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();

	//Fetching pick rule
	nlapiLogExecution('DEBUG','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);
	var vTotRecCount=0;
	if(searchresults != null && searchresults != '')
	{	

		var searchresultszone=getpikzone(-1,whLocation);
		nlapiLogExecution('DEBUG','searchresultszone',searchresultszone.length);	

		var searchresultsinvt=getLocGroup(-1,item,OldLocationInternalId,itemstatus,lotintid);
		nlapiLogExecution('DEBUG','searchresultsinvt',searchresultsinvt.length);


		for (var i = 0;  i < searchresults.length; i++) {

			var searchresultpick = searchresults[i];				
			var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');			

			//nlapiLogExecution('DEBUG', "LP: " + LP);
			//nlapiLogExecution('DEBUG','PickZone',vpickzone);
			if(vpickzone != null && vpickzone != '')
			{	
				nlapiLogExecution('DEBUG','vpickzone',vpickzone);
				/*var filterszone = new Array();	
				filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', [vpickzone]);
				filterszone[1] = new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',whLocation]);
				filterszone[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

				var columnzone = new Array();
				columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');
				columnzone[1] = new nlobjSearchColumn('custrecord_zone_seq').setSort();				

				var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
				nlapiLogExecution('DEBUG','Loc Group Fetching');
				nlapiLogExecution('DEBUG','searchresultszone1',searchresultszone.length);*/

				for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {
					var pikzone = searchresultszone[j].getValue('custrecordcustrecord_putzoneid');
					//if(vpickzone == searchresultszone[j][1])
					if(vpickzone == pikzone	)
					{
						//nlapiLogExecution('DEBUG','pikzone',pikzone);
						var searchresultzone = searchresultszone[j];
						var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');
						//var vlocgroupno = searchresultzone[2];
						//nlapiLogExecution('DEBUG','Loc Group Fetching',vlocgroupno);

						/*var filtersinvt = new Array();

						filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
						filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['19']));
						if(OldLocationInternalId != null && OldLocationInternalId != '')
							filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof',OldLocationInternalId));
						filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));				 
						//filtersinvt.push(new nlobjSearchFilter('greaterthanorequalto', 'greaterthanorequalto', avlqty));

						//nlapiLogExecution('DEBUG', "custrecord_ebiz_inv_sku: " + item);

						var columnsinvt = new Array();
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));					
						columnsinvt.push(new nlobjSearchColumn('custrecord_pickseqno'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_qty'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));				 
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
						columnsinvt[0].setSort();
						columnsinvt[1].setSort();				
						columnsinvt[2].setSort();
						columnsinvt[3].setSort(false);

						var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);*/
						var vOldLocation='';
						var vOldRec='';
						var vOldLocId='';
						var vTotRemQty=0;
						var vIntCount=0;
						var vOldlot='';
						var vOldlotId='';
						for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

							var LocGrp = searchresultsinvt[l].getValue('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc');
							//var LocGrp = searchresultsinvt[l].getValue(columns[9]);
							//nlapiLogExecution('DEBUG','vlocgroupno',vlocgroupno);
							//nlapiLogExecution('DEBUG','Loc Group Fetching new',LocGrp);
							//nlapiLogExecution('DEBUG','searchresultsinvt[l].getId',searchresultsinvt[l].getId());
							//nlapiLogExecution('DEBUG','searchresultszone[j]..getId',searchresultszone[j].getId());

							if(vlocgroupno == LocGrp)
							{
								//nlapiLogExecution('DEBUG','entered1',LocGrp);
								var searchresult = searchresultsinvt[l];
								var actqty = searchresult.getValue('custrecord_ebiz_qoh');
								var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
								var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
								var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
								var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
								var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
								var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
								var Recid = searchresult.getId();  

								if (isNaN(allocqty)) {
									allocqty = 0;
								}
								if (allocqty == "") {
									allocqty = 0;
								}
								if( parseFloat(actqty)<0)
								{
									actqty=0;
								}

								var remainqty=0;

								//Added by Ganesh not to allow negative or zero Qtys 
								if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0)
									remainqty = parseFloat(actqty) - parseFloat(allocqty);
								//nlapiLogExecution('DEBUG', 'remainqty:',remainqty); 
								//nlapiLogExecution('DEBUG', 'avlqty:',avlqty); 
								var cnfmqty;


								if (parseFloat(remainqty) >= parseFloat(avlqty)) { 
									//nlapiLogExecution('DEBUG', 'vOldLocation:',vOldLocation);
									//nlapiLogExecution('DEBUG', 'vactLocationtext:',vactLocationtext);
									if(vOldLocation!=vactLocationtext)
									{
										if(vIntCount==0)
										{
											vTotRemQty=remainqty;
											vOldRec=Recid;
											vOldLocation=vactLocationtext;
											vOldLocId=vactLocation;
											vOldlot=vlotText;
											vOldlotId=vlotno;
											vIntCount=1;
										}
										else
										{
											//nlapiLogExecution('DEBUG', 'vTotRemQty :',vTotRemQty); 
											//nlapiLogExecution('DEBUG', 'vOldLocation:',vOldLocation); 
											//nlapiLogExecution('DEBUG', 'vTotRecCount :',vTotRecCount); 
											var invtarray = new Array();
											////alert('cnfmqty3 ' +cnfmqty);
											invtarray[0]= vTotRemQty; 
											invtarray[1]= vOldLocId; 
											invtarray[2] = vOldRec; 
											invtarray[3] =vOldLocation; 
											invtarray[4] =vOldlot;
											invtarray[5] =vOldlotId;
											//nlapiLogExecution('DEBUG', 'RecId:',vOldRec); 
											Resultarray.push(invtarray);
											vTotRecCount=parseFloat(vTotRecCount) +1;
											//if(vTotRecCount ==5)
											//return Resultarray;
											vOldLocation=vactLocationtext;
											vTotRemQty=remainqty;
											vOldLocId=vactLocation;
											vOldRec=Recid;
											vOldlot=vlotText;
											vOldlotId=vlotno;
										}								

									}
									else if(vOldlot==vlotText)
									{
										vTotRemQty= parseFloat(vTotRemQty) + parseFloat(remainqty);

									}
									else
									{
										var invtarray = new Array();
										////alert('cnfmqty3 ' +cnfmqty);
										invtarray[0]= vTotRemQty; 
										invtarray[1]= vOldLocId; 
										invtarray[2] = vOldRec; 
										invtarray[3] =vOldLocation; 
										invtarray[4] =vOldlot;
										invtarray[5] =vOldlotId;
										//nlapiLogExecution('DEBUG', 'RecId:',vOldRec); 
										Resultarray.push(invtarray);
										vTotRecCount=parseFloat(vTotRecCount) +1;
										//if(vTotRecCount ==5)
										//return Resultarray;
										vOldLocation=vactLocationtext;
										vTotRemQty=remainqty;
										vOldLocId=vactLocation;
										vOldRec=Recid;
										vOldlot=vlotText;
										vOldlotId=vlotno;
									}
								} 
							}
						} 
						if(vTotRemQty != null && vTotRemQty != '' && vTotRemQty != '0' && vTotRemQty != 0)
						{	
							//nlapiLogExecution('DEBUG','entered2',LocGrp);
							nlapiLogExecution('DEBUG', 'vTotRemQty in out :',vTotRemQty); 
							//nlapiLogExecution('DEBUG', 'vOldLocation:',vOldLocation); 
							//nlapiLogExecution('DEBUG', 'vTotRecCount in out :',vTotRecCount); 

							var invtarray = new Array();
							////alert('cnfmqty3 ' +cnfmqty);
							invtarray[0]= vTotRemQty; 
							invtarray[1]= vOldLocId; 
							invtarray[2] = vOldRec; 
							invtarray[3] =vOldLocation; 
							invtarray[4] =vOldlot;
							invtarray[5] =vOldlotId;
							//nlapiLogExecution('DEBUG', 'RecId:',vOldRec); 
							Resultarray.push(invtarray);
							vTotRecCount=parseFloat(vTotRecCount) +1;				 
							vOldLocation='';
							vTotRemQty='';
							vOldLocId='';
							vOldRec='';
							vOldlot='';
							vOldlotId='';
						}
					}
				}
			}
		} 
	}
	return Resultarray;  
}

function getPickFaceLocation(item,location)
{
	nlapiLogExecution('DEBUG', 'Into getPickFaceLocation');
	var PickFaceSearchResults = new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'noneof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_pickbinloc');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);

	nlapiLogExecution('DEBUG', 'Return Value',PickFaceSearchResults);
	nlapiLogExecution('DEBUG', 'Out of getPickFaceLocation');

	return PickFaceSearchResults;
}

function getInvtDetails(item,location,itemstatus)
{
	nlapiLogExecution('DEBUG', 'Into getInvtDetails');
	var InvtSearchResults = new Array();
	var InvtResultsArr = new Array();
	var InvtFilters = new Array();
	var InvtColumns = new Array();

	var curdate=DateStamp();

	InvtFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item)); 
	InvtFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location)); 
	InvtFilters.push(new nlobjSearchFilter('isinactive','custrecord_ebiz_inv_binloc', 'is', 'F')); 
	InvtFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));
	InvtFilters.push(new nlobjSearchFilter('custrecord_ebiz_expdate', null, 'onorafter', ['@NONE@',curdate]));
	//Case # 20126477  start
	//InvtFilters.push(new nlobjSearchFilter('custrecord_ebiz_invholdflg', null, 'is', 'F')); 
	//Case # 20126477  End

	if(itemstatus != null && itemstatus != '' && itemstatus!='null')
		InvtFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof',itemstatus));

	InvtColumns[0] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	InvtColumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');

	InvtSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, InvtFilters, InvtColumns);

	if(InvtSearchResults!=null && InvtSearchResults!='' && InvtSearchResults.length>0)
	{
		for(var g=0;g<InvtSearchResults.length;g++)
		{
			var invtarray = new Array();
			invtarray[0] = InvtSearchResults[g].getValue('custrecord_ebiz_avl_qty');
			invtarray[1] = InvtSearchResults[g].getValue('custrecord_ebiz_inv_binloc');
			invtarray[2] = InvtSearchResults[g].getId();
			invtarray[3] = InvtSearchResults[g].getText('custrecord_ebiz_inv_binloc');
			InvtResultsArr.push(invtarray);
		}
	}

	nlapiLogExecution('DEBUG', 'Return Value',InvtResultsArr);
	nlapiLogExecution('DEBUG', 'Out of getInvtDetails');

	return InvtResultsArr;
}

function removeDuplicateLocations(arrayName,flag){
	var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < newArray.length; j++) {
			if(flag=='batchitem'){
				if (newArray[j][4] == arrayName[i][4]) 
					continue label;
			}
			else
				if (newArray[j][3] == arrayName[i][3]) 
					continue label;
		}

		var invtarray = new Array();
		invtarray[0] = arrayName[i][0]; 
		invtarray[1] = arrayName[i][1]; 
		invtarray[2] = arrayName[i][2]; 
		invtarray[3] = arrayName[i][3]; 
		invtarray[4] = arrayName[i][4]; 
		invtarray[5] = arrayName[i][5]; 
		newArray.push(invtarray);
	}
	return newArray;
}


function fnvalidategivenlot(item,location,inputlot)
{
	nlapiLogExecution('DEBUG', 'Into fnvalidategivenlot');

	var str = 'item. = ' + item + '<br>';				
	str = str + 'location. = ' + location + '<br>';
	str = str + 'inputlot. = ' + inputlot + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var vnslotfound='F';

	var filterszone = new Array();	
	filterszone[0] = new nlobjSearchFilter('item', null, 'anyof', item);
	filterszone[1] = new nlobjSearchFilter('location', null, 'anyof',location);
	filterszone[2] = new nlobjSearchFilter('quantityavailable', null, 'greaterthan', 0);

	var columnzone = new Array();
	columnzone[0] = new nlobjSearchColumn('location');
	columnzone[1] = new nlobjSearchColumn('inventorynumber');
	columnzone[2] = new nlobjSearchColumn('item');
	columnzone[3] = new nlobjSearchColumn('quantityavailable');
	columnzone[4] = new nlobjSearchColumn('quantityonhand');	

	var searchresults = nlapiSearchRecord('inventorynumber', null, filterszone, columnzone);
	if(searchresults!=null && searchresults!='')
	{
		for(var n=0; n<searchresults.length;n++) 
		{
			var invlot = searchresults[n].getValue('inventorynumber');
			nlapiLogExecution('DEBUG', 'invlot', invlot);
			if(invlot==inputlot)
			{
				vnslotfound='T';
				break;
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of fnvalidategivenlot',vnslotfound);
	return vnslotfound;
}