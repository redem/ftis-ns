/***************************************************************************
   eBizNET Solutions            
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_order_history_SL.js,v $
 *     	   $Revision: 1.12.2.7.4.5.4.3 $
 *     	   $Date: 2013/03/19 12:08:11 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_order_history_SL.js,v $
 * Revision 1.12.2.7.4.5.4.3  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.12.2.7.4.5.4.2  2013/03/19 11:46:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.12.2.7.4.5.4.1  2013/03/08 14:41:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.12.2.7.4.5  2013/01/11 13:26:32  skreddy
 * CASE201112/CR201113/LOG201121
 * added trantype  to differentiate salesorders and transfer order
 *
 * Revision 1.12.2.7.4.4  2012/12/18 18:29:29  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12.2.7.4.3  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.12.2.7.4.2  2012/10/26 13:58:22  skreddy
 * CASE201112/CR201113/LOG201121
 * Integrated Cluster number in report
 *
 * Revision 1.12.2.7.4.1  2012/09/28 02:15:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.12.2.5  2012/07/13 00:36:23  gkalla
 * CASE201112/CR201113/LOG201121
 * Added filter
 *
 * Revision 1.12.2.4  2012/07/13 00:03:06  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed picked by field value
 *
 * Revision 1.12.2.3  2012/07/04 07:12:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related toTime Stamp is resolved.
 *
 * Revision 1.12.2.2  2012/07/02 06:27:06  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Picker information and Time Stamp .
 *
 * Revision 1.12.2.1  2012/02/09 17:04:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * order history report
 *
 * Revision 1.12  2011/12/09 14:44:57  gkalla
 * CASE201112/CR201113/LOG201121
 * not to transfer by default  to Transfer order
 *
 * Revision 1.11  2011/12/06 07:47:40  gkalla
 * CASE201112/CR201113/LOG201121
 * New report for order History
 *
 * Revision 1.10  2011/12/05 16:53:56  gkalla
 * CASE201112/CR201113/LOG201121
 * New report for order History
 *
 * Revision 1.8  2011/12/01 18:21:24  gkalla
 * CASE201112/CR201113/LOG201121
 * New report for order History
 *
 * Revision 1.7  2011/12/01 15:14:35  gkalla
 * CASE201112/CR201113/LOG201121
 * New report for order History
 *
 * Revision 1.6  2011/12/01 14:38:07  spendyala
 * CASE201112/CR201113/LOG201121
 * changed NS status to Sales Order Status field name
 *
 * Revision 1.5  2011/12/01 11:28:35  gkalla
 * CASE201112/CR201113/LOG201121
 * New report for order History
 *
 * Revision 1.4  2011/12/01 10:43:12  spendyala
 * CASE201112/CR201113/LOG201121
 * added NS Status field
 *
 * Revision 1.3  2011/12/01 10:31:36  gkalla
 * CASE201112/CR201113/LOG201121
 * New report for order History
 *
 * Revision 1.2  2011/12/01 10:21:44  gkalla
 * CASE201112/CR201113/LOG201121
 * New report for order History
 *
 * Revision 1.1  2011/11/30 17:49:57  gkalla
 * CASE201112/CR201113/LOG201121
 * New report for order History
 * 
 *****************************************************************************/


function ebiznet_OrderHistoryDetails(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var form = nlapiCreateForm('Order History Report');
		var vQbWave = "";
		var salesorder = form.addField('custpage_so', 'text', 'SO/TO #');

		salesorder.setMandatory(true);

		var selectcriteria = form.addField('custpage_qbtrantype', 'select', 'Tran Type').setLayoutType('startrow','none');
		selectcriteria.addSelectOption('SO', 'Sales order');
		selectcriteria.addSelectOption('TO', 'Transfer Order');
		var vtrantype=request.getParameter('custpage_qbtrantype');
		selectcriteria.setDefaultValue(vtrantype);

		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else //this is the POST block
	{

		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start',context.getRemainingUsage());

		var form = nlapiCreateForm('Order History Report');
		var soidno = request.getParameter('custpage_so');
		var salesorder = form.addField('custpage_so', 'text', 'SO/TO #');
		salesorder.setMandatory(true);
		salesorder.setDefaultValue(soidno);	

		var selectcriteria = form.addField('custpage_qbtrantype', 'select', 'Tran Type').setLayoutType('startrow','none');
		selectcriteria.addSelectOption('SO', 'Sales order');
		selectcriteria.addSelectOption('TO', 'Transfer Order');
		var vtrantype=request.getParameter('custpage_qbtrantype');
		selectcriteria.setDefaultValue(vtrantype);

		form.addSubmitButton('Display');
		var soid = GetSOInternalId(soidno,vtrantype);

		if(soid != null && soid != "")
		{
			var customerSearchResults = new Array();

			var trantype = nlapiLookupField('transaction', soid, 'recordType');
			nlapiLogExecution('ERROR', 'trantype', trantype);


			var transactiontype='';
			if(trantype=='salesorder')
				transactiontype = 'Sales Order';
			else if(trantype=='transferorder')
				transactiontype = 'Transfer Order';			

			// To fetch quantities from Sales Order

			var salesorderFilers = new Array();
			salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'F')); 
			salesorderFilers.push(new nlobjSearchFilter('internalid', null, 'is', soid)); 

			var columns=new Array();			 
			columns.push(new nlobjSearchColumn('quantityshiprecv'));
			columns.push(new nlobjSearchColumn('quantitypicked'));
			columns.push(new nlobjSearchColumn('quantity'));
			columns.push(new nlobjSearchColumn('line'));

			if(trantype=='salesorder')
				customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,columns);
			else if(trantype=='transferorder')
				customerSearchResults = nlapiSearchRecord('transferorder', null, salesorderFilers,columns);


			// To get the data from SHip manifest
			var ShipFilters = new Array(); 
			ShipFilters.push(new nlobjSearchFilter('custrecord_ship_order', null, 'anyof', soid));
			ShipFilters.push(new nlobjSearchFilter('custrecord_ship_trackno', null, 'isnotempty'));
			var Shipcolumns=new Array();
			Shipcolumns.push(new nlobjSearchColumn('custrecord_ship_trackno'));
			var ShipSearchResults = nlapiSearchRecord('customrecord_ship_manifest', null, ShipFilters, Shipcolumns);	
			var eBizTrackingNo="";
			var TrackingNo="";

			if(ShipSearchResults != null && ShipSearchResults != "")
			{
				nlapiLogExecution('ERROR', 'ShipSearchResults.length', ShipSearchResults.length);
				for(var i=0; i<ShipSearchResults.length; i++)
				{
					nlapiLogExecution('ERROR', 'ShipSearchResults Id ', ShipSearchResults[i].getId());
					if(eBizTrackingNo != null && eBizTrackingNo!= ""  )
						eBizTrackingNo=eBizTrackingNo+', '+ShipSearchResults[i].getValue("custrecord_ship_trackno");
					else
						eBizTrackingNo=ShipSearchResults[i].getValue("custrecord_ship_trackno");

				}
			}

			nlapiLogExecution('ERROR', 'soid', soid);

			if(trantype=='salesorder')
				var searchresults = nlapiLoadRecord('salesorder', soid);
			else if(trantype=='transferorder')
				var searchresults = nlapiLoadRecord('transferorder', soid);


			var NsTrack="";
			if(searchresults.getFieldValue('linkedtrackingnumbers') != null && searchresults.getFieldValue('linkedtrackingnumbers') != "")
			{
				var trackArr =  searchresults.getFieldValue('linkedtrackingnumbers').split(' ');
				nlapiLogExecution('ERROR', 'trackArr', trackArr.length);
				NsTrack = trackArr.join(','); 
			}

			nlapiLogExecution('ERROR', 'trackArr2', trackArr);

			//for Fulfillment Order
			var salessearchfilters = new Array(); 
			salessearchfilters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soid));

			var columns=new Array();
			columns.push(new nlobjSearchColumn('custrecord_lineord'));
			columns.push(new nlobjSearchColumn('custrecord_ordline'));
			columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
			columns.push(new nlobjSearchColumn('custrecord_pickgen_qty'));
			columns.push(new nlobjSearchColumn('custrecord_pickqty'));
			columns.push(new nlobjSearchColumn('custrecord_ship_qty'));
			columns.push(new nlobjSearchColumn('custrecord_ebiz_linesku'));
			columns.push(new nlobjSearchColumn('custrecord_record_linedate'));
			columns.push(new nlobjSearchColumn('custrecord_linestatus_flag'));
			columns.push(new nlobjSearchColumn('custrecord_printflag'));
			columns.push(new nlobjSearchColumn('custrecord_record_linetime'));
			columns.push(new nlobjSearchColumn('created'));
			columns.push(new nlobjSearchColumn('custrecord_ebiz_wave'));	

			columns[0].setSort();
			columns[1].setSort();			

			var salessearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, salessearchfilters, columns);

			var Opensearchfilters = new Array(); 
			Opensearchfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', soid));
			Opensearchfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
			Opensearchfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'noneof', ['26','30']));// Failed Pick and Short Pick
			//Opensearchfilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));

			var Opencolumns=new Array();
			Opencolumns.push(new nlobjSearchColumn('name'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_line_no'));
			Opencolumns.push(new nlobjSearchColumn('custrecordact_begin_date'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_act_end_date'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_pack_confirmed_date'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_taskassignedto'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_actualbegintime'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_actualendtime'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_upd_ebiz_user_no'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));	
			Opencolumns.push(new nlobjSearchColumn('custrecord_ship_lp_no'));	
			Opencolumns.push(new nlobjSearchColumn('custrecord_ebiz_trailer_no'));	//custrecord_ebiz_trailer_no
			Opencolumns.push(new nlobjSearchColumn('custrecord_ebiz_clus_no'));//added by santosh


			var OpenTasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Opensearchfilters, Opencolumns);

			var Closedsearchfilters = new Array(); 
			//Closedsearchfilters.push(new nlobjSearchFilter('name', null, 'is', ordno));
			//Closedsearchfilters.push(new nlobjSearchFilter('custrecord_ebiztask_line_no', null, 'is', ordline));
			Closedsearchfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'is', soid));
			Closedsearchfilters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3]));
			Closedsearchfilters.push(new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'isnotempty'));

			var Closedcolumns=new Array();
			Closedcolumns.push(new nlobjSearchColumn('name'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_line_no'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_act_begin_date'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_act_end_date'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_pack_confirmed_date'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiz_nsconf_refno_ebiztask'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_taskassgndid'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_actualbegintime'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_actualendtime'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_upd_ebiz_user_no'));
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no'));	
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_ship_lp_no'));	
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_ebiz_trailer_no'));	//custrecord_ebiz_trailer_no
			Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_cluster_no'));//added by santosh

			var ClosedTasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, Closedsearchfilters, Closedcolumns);


			form.addField('custpage_sono', 'text', 'SO/TO # : ').setLayoutType("startrow").setDisplayType('inline').setDefaultValue(searchresults.getFieldValue('tranid'));	
			form.addField('custpage_trantype', 'text', ' Type : ').setDisplayType('inline').setDefaultValue(transactiontype);
			form.addField('custpage_creation_date', 'text', 'Creation Date : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldValue('createddate'));
			form.addField('custpage_customer', 'text', 'Customer : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(searchresults.getFieldValue('entityname'));
			form.addField('custpage_location', 'text', 'Location : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldText('location'));
			form.addField('custpage_ordtype', 'text', 'Order Type : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldText('custbody_nswmssoordertype'));
			form.addField('custpage_ordpriority', 'text', 'Order Priority : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(searchresults.getFieldText('custbody_nswmspriority'));
			var createfulfill = form.addField('custpage_fulfillflag', 'text', 'Create Fulfillment Flag : ').setDisplayType('inline');

			if(searchresults.getFieldValue('custbody_create_fulfillment_order') != null && searchresults.getFieldValue('custbody_create_fulfillment_order') != "" && searchresults.getFieldValue('custbody_create_fulfillment_order') == 'T')		
				createfulfill.setDefaultValue('Yes');
			else
				createfulfill.setDefaultValue('No');
			form.addField('custpage_ordflag', 'text', 'Order Flag : ').setDisplayType('inline');
			form.addField('custpage_shipterms', 'text', 'Shipping Method : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(searchresults.getFieldText('shipmethod'));
			form.addField('custpage_billingterms', 'text', 'Billing Terms : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldText('terms'));
			form.addField('custpage_carrier', 'text', 'Carrier : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldText('custbody_salesorder_carrier'));
			var vNSTrackingNo='';
			if(NsTrack!=null && NsTrack!='')
				vNSTrackingNo = NsTrack.substring(0,290);
			form.addField('custpage_nstrackno', 'text', 'NS Tracking# :').setDisplayType('inline').setDefaultValue(vNSTrackingNo);
			var vTrackingNo='';
			if(eBizTrackingNo!=null && eBizTrackingNo!='')
				vTrackingNo = eBizTrackingNo.substring(0,290);
			form.addField('custpage_ebiztrackno', 'text', 'Ship Manifest Tracking# :').setDisplayType('inline').setDefaultValue(vTrackingNo);
			//added NS Status field by suman
			if(searchresults.getFieldValue('status') != null && searchresults.getFieldValue('status') != "" )
				form.addField('custpage_ebiznsstatus', 'text', 'Order Status :').setDisplayType('inline').setDefaultValue(searchresults.getFieldValue('status'));
			//end of NS Status field

			//form.addSubmitButton('Display');

			var sublist = form.addSubList("custpage_items", "list", "Order History");

			sublist.addField("custpage_fo", "text", "FO#");
			sublist.addField("custpage_line", "text", "Line#");
			sublist.addField("custpage_createdt", "text", "Date Created");	
			sublist.addField("custpage_item", "text", "Item");
			sublist.addField("custpage_dropship", "text", "Drop Ship");
			sublist.addField("custpage_nsordqty", "text", "NS Order Qty");
			sublist.addField("custpage_nspickqty", "text", "NS Picked Qty");
			sublist.addField("custpage_nsshipqty", "text", "NS Ship Qty");
			sublist.addField("custpage_ordqty", "text", "Whse Order Qty");
			sublist.addField("custpage_wave", "text", "Wave#");
			sublist.addField("custpage_wavereldate", "text", "Wave Released Date");
			sublist.addField("custpage_pickgendqty", "text", "Pick Gen Qty");
			sublist.addField("custpage_isprinted", "text", "Pick List Printed?");
			sublist.addField("custpage_pickconfdt", "text", "Pick Confirmed Date");
			sublist.addField("custpage_pickerinfo", "text", "Picked By");
			sublist.addField("custpage_cartonno", "text", "Carton #");
			sublist.addField("custpage_pickqty", "text", "Whse Pick Qty");
			sublist.addField("custpage_confdt", "text", "Pack Confirmed Date");
			sublist.addField("custpage_labeldt", "text", "Label Generated Date");
			sublist.addField("custpage_fulfillref", "text", "Item Fulfillment Ref#");
			sublist.addField("custpage_shipqty", "text", "Whse Ship Qty");	
			sublist.addField("custpage_shiplp", "text", "Ship LP #");
			sublist.addField("custpage_trailer", "text", "Trailer #");
			sublist.addField("custpage_clusterno", "text", "Cluster#");//added by santosh
			sublist.addField("custpage_status", "text", "Status");


			var vSOLineCount=0;
			if(searchresults != null && searchresults != "")
			{
				vSOLineCount=searchresults.getLineItemCount('item');
			}
			var vRecCount=0;
			if(vSOLineCount >0)
			{
				for(var j=1;j<=vSOLineCount;j++)
				{
					var  vLineFound=false;
					var vSOLineNo=searchresults.getLineItemValue('item','line',j);				

					if(salessearchresults != null && salessearchresults != "")
					{
						for (var i = 0; i < salessearchresults.length; i++) {
							var ordline=salessearchresults[i].getValue('custrecord_ordline');
							if(ordline==vSOLineNo)
							{
								nlapiLogExecution('ERROR', 'i', i);
								var ordno=salessearchresults[i].getValue('custrecord_lineord');

								var ordqty=salessearchresults[i].getValue('custrecord_ord_qty');
								var pickgenqty=salessearchresults[i].getValue('custrecord_pickgen_qty');
								var pickqty=salessearchresults[i].getValue('custrecord_pickqty');
								var shipqty=salessearchresults[i].getValue('custrecord_ship_qty');
								var sku=salessearchresults[i].getText('custrecord_ebiz_linesku');
								var skuId=salessearchresults[i].getValue('custrecord_ebiz_linesku');
								var ebizwave=salessearchresults[i].getValue('custrecord_ebiz_wave');
								var fulfillStatus=salessearchresults[i].getText('custrecord_linestatus_flag');
								var fulfillStatusValue=salessearchresults[i].getValue('custrecord_linestatus_flag');
								nlapiLogExecution('ERROR', 'fulfillStatus', fulfillStatus);
								var PrintFlag=salessearchresults[i].getValue('custrecord_printflag');

								var createDate=salessearchresults[i].getValue('created');

								if(PrintFlag == null || PrintFlag == "")
									PrintFlag='No';
								else
									PrintFlag='Yes';

								// To get the data from NS Sales Order Items
								var nsOrdQty;
								var nsPickQty;
								var nsShipQty;
								var createpo;
								var DropShip='No';


								if(customerSearchResults != null && customerSearchResults != "")
								{
									for(var m=0;m<=customerSearchResults.length;m++)
									{
										var NSlineNo = customerSearchResults[m].getValue('line');
										if(NSlineNo == ordline)
										{

											if(customerSearchResults[m].getValue('quantity') != null &&  customerSearchResults[m].getValue('quantity') != '')
											{
												nsOrdQty=customerSearchResults[m].getValue('quantity');
												if(parseFloat(nsOrdQty)<0)
													nsOrdQty=(nsOrdQty*-1);
											}
											else
												nsOrdQty="0";

											if(customerSearchResults[m].getValue('quantitypicked') != null && customerSearchResults[m].getValue('quantitypicked') != '')
												nsPickQty=customerSearchResults[m].getValue('quantitypicked');
											else
												nsPickQty="0";

											if(customerSearchResults[m].getValue('quantityshiprecv') != null && customerSearchResults[m].getValue('quantityshiprecv') != '')
												nsShipQty=customerSearchResults[m].getValue('quantityshiprecv');
											else
												nsShipQty="0";

											if(searchresults.getLineItemValue('item','createpo',ordline) != null && searchresults.getLineItemValue('item','createpo',ordline).trim() != '')
												createpo=searchresults.getLineItemValue('item','createpo',ordline);
											if(createpo != null && createpo != "")
												DropShip='Yes';
											else
												DropShip='No';
											break;
										}
									}
								}

								nlapiLogExecution('ERROR', 'nsPickQty', nsPickQty);
								nlapiLogExecution('ERROR', 'createpo', createpo);
								var PackConfDt="";
//								var packrec=GetPackedDate(soid,skuId);
//								if(packrec!=null&&packrec!="")
//								PackConfDt=packrec[0].getValue('lastmodified');
								if(ordno != null && ordno != "" && ordline != null && ordline != "")
								{
									nlapiLogExecution('ERROR', 'ordno', ordno);
									nlapiLogExecution('ERROR', 'ordline', ordline);
									nlapiLogExecution('ERROR', 'soid', soid);
//									var Opensearchfilters = new Array(); 
//									Opensearchfilters.push(new nlobjSearchFilter('name', null, 'is', ordno));
//									Opensearchfilters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', ordline));
//									Opensearchfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', soid));
//									Opensearchfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));

//									var Opencolumns=new Array();
//									Opencolumns.push(new nlobjSearchColumn('custrecordact_begin_date'));
//									Opencolumns.push(new nlobjSearchColumn('custrecord_act_end_date'));
//									Opencolumns.push(new nlobjSearchColumn('custrecord_pack_confirmed_date'));
//									Opencolumns.push(new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no'));
//									Opencolumns.push(new nlobjSearchColumn('custrecord_taskassignedto'));
//									Opencolumns.push(new nlobjSearchColumn('custrecord_actualbegintime'));
//									Opencolumns.push(new nlobjSearchColumn('custrecord_actualendtime'));
//									Opencolumns.push(new nlobjSearchColumn('custrecord_upd_ebiz_user_no'));
//									Opencolumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));	
//									Opencolumns.push(new nlobjSearchColumn('custrecord_ship_lp_no'));	
//									Opencolumns.push(new nlobjSearchColumn('custrecord_ebiz_trailer_no'));	//custrecord_ebiz_trailer_no


//									var OpenTasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Opensearchfilters, Opencolumns);
								}
								var WaveRelDt="";
								var WaveReltime="";
								var pickConfDt="";
								var pickConftime="";
								var trailerno='';
								var cartonno='';
								var shiplpno='';
								var Clusterno=""; //added by santosh
								var ItemFulfillRef="";
								var pickerinfo="";
								nlapiLogExecution('ERROR', 'after Open task search', "after Open task search");
								//var ClosedTasksearchresults;

								if(OpenTasksearchresults != null && OpenTasksearchresults != "")
								{
									for(var z=0;z<OpenTasksearchresults.length;z++)
									{
										if(ordno==OpenTasksearchresults[z].getValue("name") 
												&& ordline==OpenTasksearchresults[z].getValue("custrecord_line_no"))
										{
											nlapiLogExecution('ERROR', 'OpenTasksearchresults', OpenTasksearchresults.length);
											if(OpenTasksearchresults[z].getValue("custrecordact_begin_date") != null && OpenTasksearchresults[z].getValue("custrecordact_begin_date") != "")
											{
												WaveRelDt=OpenTasksearchresults[z].getValue("custrecordact_begin_date");
											}

											if(OpenTasksearchresults[z].getValue("custrecord_act_end_date") != null && OpenTasksearchresults[z].getValue("custrecord_act_end_date") != "")
											{
												pickConfDt=OpenTasksearchresults[z].getValue("custrecord_act_end_date");
											}
											if(OpenTasksearchresults[z].getValue("custrecord_pack_confirmed_date") != null && OpenTasksearchresults[z].getValue("custrecord_pack_confirmed_date") != "")
											{
												PackConfDt=OpenTasksearchresults[z].getValue("custrecord_pack_confirmed_date");
											}
											if(OpenTasksearchresults[z].getValue("custrecord_ebiz_nsconfirm_ref_no") != null && OpenTasksearchresults[z].getValue("custrecord_ebiz_nsconfirm_ref_no") != "")
											{
												ItemFulfillRef=OpenTasksearchresults[z].getValue("custrecord_ebiz_nsconfirm_ref_no");
											}
											if(OpenTasksearchresults[z].getValue("custrecord_container_lp_no") != null && OpenTasksearchresults[z].getValue("custrecord_container_lp_no") != "")
											{
												cartonno=OpenTasksearchresults[z].getValue("custrecord_container_lp_no");
											}
											if(OpenTasksearchresults[z].getValue("custrecord_ship_lp_no") != null && OpenTasksearchresults[z].getValue("custrecord_ship_lp_no") != "")
											{
												shiplpno=OpenTasksearchresults[z].getValue("custrecord_ship_lp_no");
											}
											if(OpenTasksearchresults[z].getValue("custrecord_ebiz_trailer_no") != null && OpenTasksearchresults[z].getValue("custrecord_ebiz_trailer_no") != "")
											{
												trailerno=OpenTasksearchresults[z].getValue("custrecord_ebiz_trailer_no");
											}
											//code added by santosh on 01 Oct12
											if(OpenTasksearchresults[z].getValue("custrecord_ebiz_clus_no") != null && OpenTasksearchresults[z].getValue("custrecord_ebiz_clus_no") != "")
											{
												Clusterno=OpenTasksearchresults[z].getValue("custrecord_ebiz_clus_no");
											}

											pickerinfo=OpenTasksearchresults[z].getText("custrecord_upd_ebiz_user_no");

											WaveReltime=OpenTasksearchresults[z].getValue("custrecord_actualbegintime");
											pickConftime=OpenTasksearchresults[z].getValue("custrecord_actualendtime");
										}
									}

								}
								else
								{
									nlapiLogExecution('ERROR', 'pickqty', 'pickqty');
									if(pickqty != null && pickqty != "" && parseFloat(pickqty)>0)
									{
										// To fetch the data from closed task if data moved to closed task

//										var Closedsearchfilters = new Array(); 
//										Closedsearchfilters.push(new nlobjSearchFilter('name', null, 'is', ordno));
//										Closedsearchfilters.push(new nlobjSearchFilter('custrecord_ebiztask_line_no', null, 'is', ordline));
//										Closedsearchfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'is', soid));
//										Closedsearchfilters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3]));

//										var Closedcolumns=new Array();
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_act_begin_date'));
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_act_end_date'));
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_pack_confirmed_date'));
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiz_nsconf_refno_ebiztask'));
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_taskassgndid'));
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_actualbegintime'));
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_actualendtime'));
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_upd_ebiz_user_no'));
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no'));	
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_ship_lp_no'));	
//										Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_ebiz_trailer_no'));	//custrecord_ebiz_trailer_no

//										ClosedTasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, Closedsearchfilters, Closedcolumns);
										nlapiLogExecution('ERROR', 'test2', 'test2');
										if(ClosedTasksearchresults != null && ClosedTasksearchresults != "")
										{
											for(var x=0;x<ClosedTasksearchresults.length;x++)
											{
												if(ordno==ClosedTasksearchresults[x].getValue("name") 
														&& ordline==ClosedTasksearchresults[x].getValue("custrecord_ebiztask_line_no"))
												{
													nlapiLogExecution('ERROR', 'ClosedTasksearchresults', ClosedTasksearchresults.length);
													if(ClosedTasksearchresults[x].getValue("custrecord_ebiztask_act_begin_date") != null && ClosedTasksearchresults[x].getValue("custrecord_ebiztask_act_begin_date") != "")
													{
														WaveRelDt=ClosedTasksearchresults[x].getValue("custrecord_ebiztask_act_begin_date");
													}

													if(ClosedTasksearchresults[x].getValue("custrecord_ebiztask_act_end_date") != null && ClosedTasksearchresults[x].getValue("custrecord_ebiztask_act_end_date") != "")
													{
														pickConfDt=ClosedTasksearchresults[x].getValue("custrecord_ebiztask_act_end_date");
													}
													if(ClosedTasksearchresults[x].getValue("custrecord_ebiztask_pack_confirmed_date") != null && ClosedTasksearchresults[x].getValue("custrecord_ebiztask_pack_confirmed_date") != "")
													{
														PackConfDt=ClosedTasksearchresults[x].getValue("custrecord_ebiztask_pack_confirmed_date");
													}
													if(ClosedTasksearchresults[x].getValue("custrecord_ebiz_nsconf_refno_ebiztask") != null && ClosedTasksearchresults[x].getValue("custrecord_ebiz_nsconf_refno_ebiztask") != "")
													{
														ItemFulfillRef=ClosedTasksearchresults[x].getValue("custrecord_ebiz_nsconf_refno_ebiztask");
													}
													if(ClosedTasksearchresults[x].getValue("custrecord_ebiztask_ebiz_contlp_no") != null && ClosedTasksearchresults[x].getValue("custrecord_ebiztask_ebiz_contlp_no") != "")
													{
														cartonno=ClosedTasksearchresults[x].getValue("custrecord_ebiztask_ebiz_contlp_no");
													}
													if(ClosedTasksearchresults[x].getValue("custrecord_ebiztask_ship_lp_no") != null && ClosedTasksearchresults[x].getValue("custrecord_ebiztask_ship_lp_no") != "")
													{
														shiplpno=ClosedTasksearchresults[x].getValue("custrecord_ebiztask_ship_lp_no");
													}
													if(ClosedTasksearchresults[x].getValue("custrecord_ebiztask_ebiz_trailer_no") != null && ClosedTasksearchresults[x].getValue("custrecord_ebiztask_ebiz_trailer_no") != "")
													{
														trailerno=ClosedTasksearchresults[x].getValue("custrecord_ebiztask_ebiz_trailer_no");
													}

													//code added by santosh on 01 Oct 12
													if(ClosedTasksearchresults[x].getValue("custrecord_ebiztask_cluster_no") != null && ClosedTasksearchresults[x].getValue("custrecord_ebiztask_cluster_no") != "")
													{
														Clusterno=ClosedTasksearchresults[x].getValue("custrecord_ebiztask_cluster_no");
													}
													pickerinfo=ClosedTasksearchresults[x].getText("custrecord_ebiztask_upd_ebiz_user_no");
													WaveReltime=ClosedTasksearchresults[x].getValue("custrecord_ebiztask_actualbegintime");
													pickConftime=ClosedTasksearchresults[x].getValue("custrecord_ebiztask_actualendtime");
												}
											}
										}
									}
								}
								nlapiLogExecution('ERROR', 'test3', 'test3');
								form.getSubList('custpage_items').setLineItemValue('custpage_pickconfdt', vRecCount + 1, pickConfDt+' '+pickConftime);

								form.getSubList('custpage_items').setLineItemValue('custpage_pickerinfo', vRecCount + 1, pickerinfo);

								form.getSubList('custpage_items').setLineItemValue('custpage_confdt', vRecCount + 1, PackConfDt);
								form.getSubList('custpage_items').setLineItemValue('custpage_fulfillref', vRecCount + 1, ItemFulfillRef);	
								form.getSubList('custpage_items').setLineItemValue('custpage_wavereldate', vRecCount + 1, WaveRelDt+' '+WaveReltime);	
								form.getSubList('custpage_items').setLineItemValue('custpage_fo', vRecCount + 1, ordno);
								form.getSubList('custpage_items').setLineItemValue('custpage_line', vRecCount + 1, ordline);
								form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', vRecCount + 1, ordqty);			
								form.getSubList('custpage_items').setLineItemValue('custpage_pickgendqty', vRecCount + 1, pickgenqty);			 
								form.getSubList('custpage_items').setLineItemValue('custpage_pickqty', vRecCount + 1, pickqty);
								form.getSubList('custpage_items').setLineItemValue('custpage_shipqty', vRecCount + 1, shipqty);
								form.getSubList('custpage_items').setLineItemValue('custpage_item', vRecCount + 1, sku);
								form.getSubList('custpage_items').setLineItemValue('custpage_wave', vRecCount + 1, ebizwave);
								form.getSubList('custpage_items').setLineItemValue('custpage_createdt', vRecCount + 1, createDate);
								form.getSubList('custpage_items').setLineItemValue('custpage_status', vRecCount + 1, fulfillStatus);			 
								form.getSubList('custpage_items').setLineItemValue('custpage_isprinted', vRecCount + 1, PrintFlag);
								form.getSubList('custpage_items').setLineItemValue('custpage_nsordqty', vRecCount + 1, nsOrdQty);
								form.getSubList('custpage_items').setLineItemValue('custpage_nspickqty', vRecCount + 1, nsPickQty);
								form.getSubList('custpage_items').setLineItemValue('custpage_nsshipqty', vRecCount + 1, nsShipQty);
								form.getSubList('custpage_items').setLineItemValue('custpage_dropship', vRecCount + 1, DropShip);
								form.getSubList('custpage_items').setLineItemValue('custpage_labeldt', vRecCount + 1, DropShip);
								form.getSubList('custpage_items').setLineItemValue('custpage_cartonno', vRecCount + 1, cartonno);
								form.getSubList('custpage_items').setLineItemValue('custpage_shiplp', vRecCount + 1, shiplpno);
								form.getSubList('custpage_items').setLineItemValue('custpage_trailer', vRecCount + 1, trailerno);
								form.getSubList('custpage_items').setLineItemValue('custpage_clusterno', vRecCount + 1,Clusterno);//added by santosh

								if(shipqty != null && shipqty != "" && parseFloat(shipqty)>0 && (fulfillStatusValue == 8 || fulfillStatusValue == 28 ))// Ship qty is not null and status with either pick confirmed or pack completed then status changing to Shipped
								{
									fulfillStatus="Shipped";
								}
								else if(PackConfDt != null && PackConfDt != "" && fulfillStatusValue == 8) // if pick confirm date not null but status with STATUS.OUTBOUND.PICK_CONFIRMED then changing to Pack completed 
								{
									nlapiLogExecution('ERROR', 'Pack completd');
									fulfillStatus="Pack Completed";
								}
								if(fulfillStatusValue == '15')// for STATUS.OUTBOUND.SELECTED_INTO_WAVE
								{
									nlapiLogExecution('ERROR', 'Pick gen failed');
									fulfillStatus="Pickgen failed";
								}
								else if(fulfillStatusValue == '25') // for STATUS.INBOUND_OUTBOUND.EDIT
								{
									nlapiLogExecution('ERROR', 'Edit status');
									fulfillStatus="Open";
								}

								nlapiLogExecution('ERROR', 'fulfillStatusValue', fulfillStatusValue);
								nlapiLogExecution('ERROR', 'PackConfDt', PackConfDt);
								nlapiLogExecution('ERROR', 'fulfillStatus', fulfillStatus);

								form.getSubList('custpage_items').setLineItemValue('custpage_status', vRecCount + 1, fulfillStatus);
								vRecCount=vRecCount+1;
								vLineFound=true;
							}
						}
						if(vLineFound==false)
						{
							var ordno="";
							var ordline=searchresults.getLineItemValue('item','line',j);
							var ordqty=searchresults.getLineItemValue('item','quantity',j);
							var pickgenqty="";
							var pickqty="";
							var shipqty="";
							var sku=searchresults.getLineItemValue('item','item_display',j);
							var ebizwave="";
							var createDate="";
							var fulfillStatus="Fulfillment Order not created";
							var fulfillStatusValue="";
							nlapiLogExecution('ERROR', 'fulfillStatus', fulfillStatus);
							var PrintFlag='No';
							var DropShip='No';
							form.getSubList('custpage_items').setLineItemValue('custpage_line', vRecCount + 1, ordline);
							form.getSubList('custpage_items').setLineItemValue('custpage_item', vRecCount + 1, sku);
							form.getSubList('custpage_items').setLineItemValue('custpage_status', vRecCount + 1, fulfillStatus);			 
							form.getSubList('custpage_items').setLineItemValue('custpage_isprinted', vRecCount + 1, PrintFlag);
							form.getSubList('custpage_items').setLineItemValue('custpage_nsordqty', vRecCount + 1, ordqty);
							form.getSubList('custpage_items').setLineItemValue('custpage_dropship', vRecCount + 1, DropShip);
							form.getSubList('custpage_items').setLineItemValue('custpage_status', vRecCount + 1, fulfillStatus);
							vRecCount = vRecCount +1;
						}
					}
					else
					{
						var ordno="";
						var ordline=searchresults.getLineItemValue('item','line',j);
						var ordqty=searchresults.getLineItemValue('item','quantity',j);
						var pickgenqty="";
						var pickqty="";
						var shipqty="";
						var sku=searchresults.getLineItemValue('item','item_display',j);
						var ebizwave="";
						var createDate="";
						var fulfillStatus="Fulfillment Order not created";
						var fulfillStatusValue="";
						nlapiLogExecution('ERROR', 'fulfillStatus', fulfillStatus);
						var PrintFlag='No';
						var DropShip='No';

						form.getSubList('custpage_items').setLineItemValue('custpage_line', vRecCount + 1, ordline);					
						form.getSubList('custpage_items').setLineItemValue('custpage_item', vRecCount + 1, sku);
						form.getSubList('custpage_items').setLineItemValue('custpage_status', vRecCount + 1, fulfillStatus);			 
						form.getSubList('custpage_items').setLineItemValue('custpage_isprinted', vRecCount + 1, PrintFlag);
						form.getSubList('custpage_items').setLineItemValue('custpage_nsordqty', vRecCount + 1, ordqty);						
						form.getSubList('custpage_items').setLineItemValue('custpage_dropship', vRecCount + 1, DropShip);
						form.getSubList('custpage_items').setLineItemValue('custpage_status', vRecCount + 1, fulfillStatus);
						vRecCount = vRecCount +1;
					}
				}
			}
		}
		else
		{
			//Invalid SOid
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
			var msgtext = "Invalid Order# : "+soidno;
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+ msgtext + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
		}
		nlapiLogExecution('ERROR','Remaining usage at the End',context.getRemainingUsage());
		response.writePage(form);
	}

	//nlapiLogExecution('ERROR','Remaining usage at the End',context.getRemainingUsage());
}


function fillsalesorderField(form, salesorderField,maxno){
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	if(maxno!=-1)
	{
		salesorderFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
	}


	salesorderField.addSelectOption("", "");

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('tranid');
	columns[0].setSort(true);
	columns[1]=new nlobjSearchColumn('internalid');
	columns[2]=new nlobjSearchColumn('type');


	var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,columns);


	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		if(customerSearchResults[i].getValue('tranid') != null && customerSearchResults[i].getValue('tranid') != "" && customerSearchResults[i].getValue('tranid') != " ")
		{
			var resdo = form.getField('custpage_so').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		salesorderField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var column=new Array();
		column[0]=new nlobjSearchColumn('tranid');		
		column[1]=new nlobjSearchColumn('internalid');
		column[1].setSort(true);

		var OrderSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,column);

		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillsalesorderField(form, salesorderField,maxno);	

	}
}

function Printreport(){

	var salessearchfilters = new Array();
	var tempstring;
	if ((nlapiGetFieldValue('custpage_fromdate') != "" && nlapiGetFieldValue('custpage_fromdate') != null) && (nlapiGetFieldValue('custpage_todate') != "" && nlapiGetFieldValue('custpage_todate') != null) ) {

		tempstring="&custpage_fromdate="+nlapiGetFieldValue('custpage_fromdate')+"&custpage_todate="+nlapiGetFieldValue('custpage_todate');
	} 
	if ((nlapiGetFieldValue('custpage_fulfillmentorder') != "" && nlapiGetFieldValue('custpage_fulfillmentorder') != null)){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_fulfillmentorder="+nlapiGetFieldValue('custpage_fulfillmentorder');
		}
		else
		{
			tempstring="&custpage_fulfillmentorder="+nlapiGetFieldValue('custpage_fulfillmentorder');
		}

	}	
	if (nlapiGetFieldValue('custpage_order') !=null && nlapiGetFieldValue('custpage_order') !=""){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_order="+nlapiGetFieldValue('custpage_order');
		}
		else
		{
			tempstring="&custpage_order="+nlapiGetFieldValue('custpage_order');
		}

	}
	if (nlapiGetFieldValue('custpage_wave') !=null && nlapiGetFieldValue('custpage_wave') !=""){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_wave="+nlapiGetFieldValue('custpage_wave');
		}
		else
		{
			tempstring="&custpage_wave="+nlapiGetFieldValue('custpage_wave');
		}

	}

	if (nlapiGetFieldValue('custpage_customer') !=null && nlapiGetFieldValue('custpage_customer') !=""){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_customer="+nlapiGetFieldValue('custpage_customer');
		}
		else
		{
			tempstring="&custpage_customer="+nlapiGetFieldValue('custpage_customer');
		}

	}
	if (nlapiGetFieldValue('custpage_location') !=null && nlapiGetFieldValue('custpage_location') !=""){
		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_location="+nlapiGetFieldValue('custpage_location');
		}
		else
		{
			tempstring="&custpage_location="+nlapiGetFieldValue('custpage_location');
		}

	}     
	if (nlapiGetFieldValue('custpage_salesorder') !=null && nlapiGetFieldValue('custpage_salesorder') !=""){
		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_salesorder="+nlapiGetFieldValue('custpage_salesorder');
		}
		else
		{
			tempstring="&custpage_salesorder="+nlapiGetFieldValue('custpage_salesorder');
		}

	}    
	var BolPDFURL = nlapiResolveURL('SUITELET', 'customscript_ordsummarydetailsreport', 'customdeploy_ordsummarydetailsreport');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		BolPDFURL = 'https://system.netsuite.com' + BolPDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			BolPDFURL = 'https://system.sandbox.netsuite.com' + BolPDFURL;				
		}	*/
	BolPDFURL=BolPDFURL+tempstring;
	window.open(BolPDFURL);
}
/**
 * Function to return the current date in mm/dd/yyyy format
 * @author Phani
 * @returns Current system date
 */
function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}

/**
 * 
 * @param SOid // To get SO internal Id
 */
function GetSOInternalId(SOid,vtrantype)
{
	nlapiLogExecution('ERROR','SOid',SOid);
	nlapiLogExecution('ERROR','vtrantype',vtrantype);

	if(vtrantype=='SO')
	{
		var salesorderFilers = new Array();
		salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
		salesorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', SOid)); 

		var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,null);	
		if(customerSearchResults != null && customerSearchResults != "")
		{
			nlapiLogExecution('ERROR', 'customerSearchResults.length ',customerSearchResults.length);
			return customerSearchResults[0].getId();
		}
		else
		{
			return null;
		}
	}
	else if(vtrantype=='TO')
	{
		var transferorderFilers = new Array();
		transferorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
		transferorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', SOid)); 

		var customerSearchResults = nlapiSearchRecord('transferorder', null, transferorderFilers,null);	
		if(customerSearchResults != null && customerSearchResults != "")
		{
			nlapiLogExecution('ERROR', 'customerSearchResults.length ',customerSearchResults.length);
			return customerSearchResults[0].getId();
		}
		else
		{
			return null;
		}
	}
}

function GetPackedDate(soid,Item)
{
	var filter=new Array();
	if(soid!=null&&soid!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no',null,'anyof',soid));
	if(Item!=null&&Item!="")
		filter.push(new nlobjSearchFilter('custrecord_sku',null,'anyof',Item));
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',28));//28=packed

	var column=new Array();
	column[0]=new nlobjSearchColumn('lastmodified');

	var searchrec=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,column);

	return searchrec;

}