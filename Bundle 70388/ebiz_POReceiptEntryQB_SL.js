/***************************************************************************
	  		   eBizNET
                eBizNET Solutions Inc
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_ConfirmPutaway_SL.js,v $
 *     	   $Revision: 1.30 $
 *     	   $Date: 2011/12/06 11:57:27 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum/ stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SU/M: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *� $Log: ebiz_ConfirmPutaway_SL.js,v $
 *
 **�
 *****************************************************************************/

function poReceiptEntryQBSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('PO Receipt');

		var pono = form.addField('custpage_po', 'text', 'PO #').setMandatory(true);;
		pono.setLayoutType('startrow', 'none');
		//pono.setLayoutType('normal', 'startcol');
		
		var receiptno = form.addField('custpage_receipt', 'text', 'Receipt #');
		receiptno.setLayoutType('normal', 'startrow');
		
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else{
		var vpono = request.getParameter('custpage_po');
		var vreceiptno = request.getParameter('custpage_receipt');
		
		var PoReceiptArray = new Array();
		PoReceiptArray["custparam_po"] = vpono;
		PoReceiptArray["custparam_receipt"] = vreceiptno;
				 
		response.sendRedirect('SUITELET', 'customscript_poreceiptentry', 'customdeploy_poreceiptentry_di', false, PoReceiptArray);
	}
}

