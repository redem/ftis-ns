/***************************************************************************
eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_wo_outboundreversal_qb.js,v $
*� $Revision: 1.1.2.1 $
*� $Date: 2013/03/08 14:43:37 $
*� $Author: skreddy $
*� $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_wo_outboundreversal_qb.js,v $
*� Revision 1.1.2.1  2013/03/08 14:43:37  skreddy
*� CASE201112/CR201113/LOG201121
*� Code merged from Endochoice as part of Standard bundle
*�
*� Revision 1.1.2.2.4.3  2012/12/03 16:41:00  schepuri
*� CASE201112/CR201113/LOG201121
*� Additional text fields order field ,serial no# and carton no#
*�
*
****************************************************************************/

function OutboundReversalWOQbSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start of GET',context.getRemainingUsage());

		var form = nlapiCreateForm('WO Outbound Reversal');
 		 

		var woid=request.getParameter('custpage_workorder');
		nlapiLogExecution('ERROR','woid',woid);

		var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		WorkOrder.addSelectOption("","");
		fillWOField(form, WorkOrder,-1);
		

		WorkOrder.setMandatory(true);
		//fillsalesorderField(form, salesorder,-1);

		form.addSubmitButton('Display');


		response.writePage(form);
	}
	else
	{
		var ordArray = new Array();		
		ordArray["custpage_workorder"] = request.getParameter('custpage_workorder');
		 
		response.sendRedirect('SUITELET', 'customscript_ebiz_wo_obreversal','customdeploy_ebiz_wo_obreversal_di', false, ordArray);

	}
}

function fillWOField(form, WOField,maxno){
	var WOFilers = new Array();
	WOFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	WOFilers.push(new nlobjSearchFilter('status', null, 'is', 'WorkOrd:G'));
	
	if(maxno!=-1)
	{
		WOFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
	}


	WOField.addSelectOption("", "");

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('tranid');
	columns[0].setSort(true);
	columns[1]=new nlobjSearchColumn('internalid');
	columns[2]=new nlobjSearchColumn('type');


	var customerSearchResults = nlapiSearchRecord('workorder', null, WOFilers,columns);


	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('tranid') != null && customerSearchResults[i].getValue('tranid') != "" && customerSearchResults[i].getValue('tranid') != " ")
		{
			var resdo = form.getField('custpage_workorder').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		WOField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var column=new Array();
		column[0]=new nlobjSearchColumn('tranid');		
		column[1]=new nlobjSearchColumn('internalid');
		column[1].setSort(true);

		var OrderSearchResults = nlapiSearchRecord('workorder', null, WOFilers,column);

		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillWOField(form, WOField,maxno);	

	}
}

