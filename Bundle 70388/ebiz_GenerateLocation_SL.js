/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_GenerateLocation_SL.js,v $
 *     	   $Revision: 1.25.2.12.4.2.2.21.2.5 $
 *     	   $Date: 2015/12/02 15:06:23 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_GenerateLocation_SL.js,v $
 * Revision 1.25.2.12.4.2.2.21.2.5  2015/12/02 15:06:23  skreddy
 * case# 201415973
 * CT prod  issue fix
 *
 * Revision 1.25.2.12.4.2.2.21.2.4  2015/11/16 10:32:15  skreddy
 * case 201415655
 * 2015.2 issue fix
 *
 * Revision 1.25.2.12.4.2.2.21.2.3  2015/11/14 14:53:07  skreddy
 * case 201415655
 * 2015.2 issue fix
 *
 * Revision 1.25.2.12.4.2.2.21.2.2  2015/11/10 15:02:07  gkalla
 * case# 201414986
 * When doing Auto break down, system is generating Bin Locations even though Generate Bin Location flag is false.
 *
 * Revision 1.25.2.12.4.2.2.21.2.1  2015/09/21 11:23:45  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.25.2.12.4.2.2.21  2015/06/11 13:33:55  schepuri
 * case# 201412982
 *
 * Revision 1.25.2.12.4.2.2.20  2015/03/30 13:48:54  schepuri
 * case# 201412152
 *
 * Revision 1.25.2.12.4.2.2.19  2015/02/27 15:51:13  sponnaganti
 * Case# 201411718
 * CT Prod Issue fix
 *
 * Revision 1.25.2.12.4.2.2.18  2014/07/30 15:24:38  grao
 * Case#: 20148771 �Standard bundel  issue fixes
 *
 * Revision 1.25.2.12.4.2.2.17  2014/06/17 15:29:14  sponnaganti
 * case# 20148732
 * Stnd Bundle Issue fix
 *
 * Revision 1.25.2.12.4.2.2.16  2014/06/12 14:26:47  grao
 * Case#: 20148779  and 20148780  New GUI account issue fixes
 *
 * Revision 1.25.2.12.4.2.2.15  2014/06/12 14:24:52  grao
 * Case#: 20148779  and 20148780   New GUI account issue fixes
 *8
 * Revision 1.25.2.12.4.2.2.14  2014/06/04 14:57:43  sponnaganti
 * Case# 20148709
 * Stnd Bundle Issue fix
 *
 * Revision 1.25.2.12.4.2.2.13  2014/05/05 14:38:51  sponnaganti
 * case# 20148251
 * (Pickface location issue)
 *
 * Revision 1.25.2.12.4.2.2.12  2013/12/24 14:14:50  schepuri
 * 20126511
 *
 * Revision 1.25.2.12.4.2.2.11  2013/10/30 13:23:35  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20124515
 * Empty location check during putaway location generation.
 *
 * Revision 1.25.2.12.4.2.2.10  2013/10/29 15:21:21  grao
 * Issue fix related to the 20125390
 *
 * Revision 1.25.2.12.4.2.2.9  2013/10/23 16:15:26  nneelam
 * Case# 20124849
 * Serial numbers are displayed in Serial/lot textbox..
 *
 * Revision 1.25.2.12.4.2.2.8  2013/10/21 15:44:16  nneelam
 * Case# 20124849
 * Serial number not appeared after break down..
 *
 * Revision 1.25.2.12.4.2.2.7  2013/09/19 15:20:04  rmukkera
 * Case# 20124445
 *
 * Revision 1.25.2.12.4.2.2.6  2013/08/07 15:17:14  rmukkera
 * Issue Fix for case# 20123765
 * For multiple item PO, After performing auto break down,  if we  generate the locations then system displayed the put gen qty as in multiples of PO line qty in PO status report.
 *
 * Revision 1.25.2.12.4.2.2.5  2013/07/19 13:43:03  schepuri
 * case# 20123407
 * ryonet sb issue fix
 *
 * Revision 1.25.2.12.4.2.2.4  2013/05/31 15:38:24  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.25.2.12.4.2.2.3  2013/05/22 11:39:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating PO Status before user perform transaction.
 *
 * Revision 1.25.2.12.4.2.2.2  2013/04/25 13:08:47  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.25.2.12.4.2.2.1  2013/04/23 15:28:36  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.25.2.12.4.2  2013/02/16 12:11:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.25.2.12.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.25.2.12  2012/07/12 14:42:33  spendyala
 * CASE201112/CR201113/LOG201121
 * Generate location depending upon Trailer.
 *
 * Revision 1.25.2.11  2012/07/03 21:58:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Putaway Strategy changes
 *
 * Revision 1.25.2.10  2012/06/25 22:22:36  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned using child-parent relation
 *
 * Revision 1.25.2.9  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.25.2.8  2012/05/03 16:53:18  gkalla
 * CASE201112/CR201113/LOG201121
 * Remaining cube update issue
 *
 * Revision 1.25.2.7  2012/03/09 07:53:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Code Merged from trunk.
 *
 * Revision 1.25.2.6  2012/02/10 09:31:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.25.2.5  2012/02/10 08:37:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.25.2.4  2012/02/09 16:10:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.31  2012/02/09 14:43:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.30  2012/02/02 11:26:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * putstrategy issue fixes
 *
 * Revision 1.29  2012/02/01 14:00:45  spendyala
 * CASE201112/CR201113/LOG201121
 *  Merged files from batch up to 1.25.2.2 revision.
 *
 * Revision 1.28  2012/01/27 07:16:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.27  2012/01/11 06:45:37  spendyala
 * CASE201112/CR201113/LOG201121
 * commented search filter criteria i.e., actual bin location field.
 *
 * Revision 1.26  2012/01/06 17:08:46  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.25  2012/01/04 16:01:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge/Mix strategy issue fixes
 *
 * Revision 1.24  2011/12/28 13:51:50  schepuri
 * CASE201112/CR201113/LOG201121
 * added new fields from itemstatus,to itemstatus
 *
 * Revision 1.23  2011/12/27 23:24:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.22  2011/12/27 16:44:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.21  2011/12/26 23:45:39  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin changes
 *
 * Revision 1.20  2011/12/26 22:41:05  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin changes
 *
 * Revision 1.19  2011/12/26 13:01:46  gkalla
 * CASE201112/CR201113/LOG201121
 * PO Receipt related changes
 *
 * Revision 1.18  2011/12/16 12:30:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.16  2011/10/18 16:54:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * TO Generate Locations
 *
 * Revision 1.15  2011/08/31 11:13:55  schepuri
 * CASE201112/CR201113/LOG201121
 * Inventory Report updating PutGen Qty
 *
 * Revision 1.14  2011/08/24 09:03:22  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/07/12 05:35:12  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/07/08 13:57:21  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Create Inventory, Inventory Move Changes
 *
 * Revision 1.11  2011/06/30 15:18:19  rgore
 * CASE201112/CR201113/LOG201121
 * Added necessary ';' to remove any code warnings
 * Ratnakar
 * 30 June 2011
 *
 * Revision 1.10  2011/06/30 14:00:37  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * WBC Issues
 *
 * Revision 1.9  2011/06/24 07:39:16  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * Location Cube Calculation changes
 *
 * Revision 1.8  2011/06/21 12:58:32  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * Auto Breakdown changes
 *
 * Revision 1.7  2011/06/17 07:39:47  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * Confirm putaway changes
 *
 * Revision 1.6  2011/06/09 13:22:04  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * WBC Issues
 *
 * Revision 1.4  2011/06/06 06:17:25  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/05/30 15:39:35  skota
 * CASE201112/CR201113/LOG201121
 * drop down field  'PO#'  converted to non-editable text field
 *
 * Revision 1.2  2011/05/13 12:30:17  skota
 * CASE201112/CR201113/LOG201121
 *  
 *
 *****************************************************************************/


function QCLocationGenertion(request, response){
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('ERROR', 'Time Stamp at the start of request',TimeStampinSec());	

		var getPOid = request.getParameter('custparam_po');
		var getTrailer = request.getParameter('custparam_trailer');
		var getReceipt = request.getParameter('custparam_receipt');
		var getPoValue = request.getParameter('custparam_povalue');
		var whloc = request.getParameter('custparam_whloc');

		var str = 'getPOid. = ' + getPOid + '<br>';
		str = str + 'getTrailer. = ' + getTrailer + '<br>';	
		str = str + 'getReceipt. = ' + getReceipt + '<br>';	
		str = str + 'getPoValue. = ' + getPoValue + '<br>';	
		str = str + 'whloc. = ' + whloc + '<br>';	

		nlapiLogExecution('ERROR', 'Parameter Values in GET', str);

		var form = nlapiCreateForm('Putaway Location Generation');
		form.setScript('customscriptebiz_generatelocation_cl');
		var trantype;
		var poloadrec='';
		var towhloc='';
		if(getPOid != null && getPOid != "")
		{
			trantype = nlapiLookupField('transaction', getPOid, 'recordType');
			poloadrec = nlapiLoadRecord(trantype, getPOid);
			whloc = poloadrec.getFieldValue('location');
			towhloc = poloadrec.getFieldValue('transferlocation');
		}
		else if((getTrailer != null && getTrailer != "") || (getReceipt != null && getReceipt != ""))
		{
			nlapiLogExecution('ERROR', 'trantype inif', trantype);
			trantype ='purchaseorder';
		}	
		if(trantype==null || trantype=='')
		{
			trantype ='purchaseorder';
		}
		nlapiLogExecution('ERROR', 'trantype', trantype);
		nlapiLogExecution('ERROR', 'whloc', whloc);
		nlapiLogExecution('ERROR', 'towhloc', towhloc);
		var selectReceive;
		var selectRType;
		var selectPO;
		var hiddenField = form.addField('custpage_hiddenfield', 'text', '').setDisplayType('hidden');
		hiddenField.setDefaultValue('T');
		var POHidden = form.addField('custpage_hdnpo', 'text', '').setDisplayType('hidden');
		POHidden.setDefaultValue(getPOid);
		var hdnTrailer = form.addField('custpage_trailer', 'text', '').setDisplayType('hidden');
		hdnTrailer.setDefaultValue(getTrailer);
		var hdnReceipt = form.addField('custpage_receipt', 'text', '').setDisplayType('hidden');
		hdnReceipt.setDefaultValue(getReceipt);
		var hdnPOValue = form.addField('custpage_povalue', 'text', '').setDisplayType('hidden');
		hdnPOValue.setDefaultValue(getPoValue);
		
		// Serial# s for serialized Item
		var arrayLP = new Array();
		var lpcolumns = new Array();
		lpcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
		lpcolumns[1] = new nlobjSearchColumn('custrecord_serialnumber');
		
		// case # 20124849,System is not displaying the serial# in the location generation screen after auto break down .
		var lpfilters=new Array();
		if(getPOid!=null && getPOid!='')
		{
			if(trantype=='purchaseorder')
		lpfilters = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', getPOid);
			else
				lpfilters = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', getPOid);
		//end
		
		var lpsearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, lpfilters, lpcolumns);
		for (var i = 0; lpsearchresults != null && i < lpsearchresults.length; i++) {
			var SerialLP = new Array();
			SerialLP[0] = lpsearchresults[i].getValue('custrecord_serialparentid');
			SerialLP[1] = lpsearchresults[i].getValue('custrecord_serialnumber');
			arrayLP.push(SerialLP);

			}
		}
		/*for (var i = 0; i < arrayLP.length; i++) 
			nlapiLogExecution('ERROR', 'arrayLP - [LP#|Serial#]', '[' + arrayLP[i][0] + '|' + arrayLP[i][1] + ']');
*/
		nlapiLogExecution('ERROR', 'arrayLP', arrayLP);

		
		
		
		if(trantype=='purchaseorder')
		{
			selectReceive = form.addField('custpage_selectreceive', 'select', 'Receive Mode').setLayoutType('startrow');

			selectReceive.addSelectOption('P', 'PO');
			selectReceive.addSelectOption('A', 'ASN');

			selectRType = form.addField('custpage_receive_type', 'select', 'Receipt Type').setLayoutType('midrow');
			selectRType.addSelectOption('PO', 'Purchase Order');

			selectPO = form.addField('custpage_po', 'text', 'PO #').setDisplayType('disabled');
			selectPO.setLayoutType('endrow');
			selectPO.setDefaultValue(getPoValue);

			selectRecipt = form.addField('custpage_receiptno', 'text', 'Receipt #').setDisplayType('disabled');
			selectRecipt.setLayoutType('startrow');

			selectTrailer = form.addField('custpage_trailerno', 'text', 'Trailer #').setDisplayType('disabled');
			selectTrailer.setLayoutType('midrow');

			if(getTrailer != null && getTrailer != "")
				selectTrailer.setDefaultValue(getTrailer);
			if(getReceipt != null && getReceipt != "")
				selectRecipt.setDefaultValue(getReceipt);
		}
		else if(trantype=='returnauthorization')
		{
			selectReceive = form.addField('custpage_selectreceive', 'select', 'Receive Mode').setLayoutType('startrow');
			selectReceive.addSelectOption('R', 'RMA');           

			selectRType = form.addField('custpage_receive_type', 'select', 'Receipt Type').setLayoutType('midrow');
			selectRType.addSelectOption('RMA', 'Return Authorization');

			selectPO = form.addField('custpage_po', 'text', 'RMA #').setDisplayType('disabled');
			selectPO.setLayoutType('endrow');
			selectPO.setDefaultValue(getPoValue);
		}
		else
		{
			selectReceive = form.addField('custpage_selectreceive', 'select', 'Receive Mode').setLayoutType('startrow');

			selectReceive.addSelectOption('T', 'TO');

			selectRType = form.addField('custpage_receive_type', 'select', 'Receipt Type').setLayoutType('midrow');
			selectRType.addSelectOption('TO', 'Transfer Order');

			selectPO = form.addField('custpage_po', 'text', 'TO #').setDisplayType('disabled');
			selectPO.setLayoutType('endrow');
			selectPO.setDefaultValue(getPoValue);
		}


		FromStatus = form.addField('custpage_fromstatus',  "select", "From Item Status","customrecord_ebiznet_sku_status").setLayoutType('midrow');
		ToStatus = form.addField('custpage_tostatus',  "select", "To Item Status","customrecord_ebiznet_sku_status").setLayoutType('endrow');
		form.addButton('custpage_set','SET','Set()');//.setLayoutType('endrow');
		var GenLocFlag= form.addField('custpage_qcbinlocgen', 'checkbox', 'Bin Location Generation').setDisplayType('disabled').setDefaultValue('T');
		/*
         Defining SubList lines
		 */
		var sublist = form.addSubList("custpage_items", "list", "ItemList");
		sublist.addMarkAllButtons();
		sublist.addField("custpage_chk", "checkbox","Check").setDefaultValue('T');
		if(trantype=='purchaseorder')
		{
			sublist.addField("custpage_po_asn", "text", "PO/ASN#");
			sublist.addField("custpage_receipt", "text", "Receipt#");
			sublist.addField("custpage_trailerno", "text", "Appointment#");
		}
		else if(trantype=='returnauthorization')
		{
			sublist.addField("custpage_po_asn", "text", "RMA#");
		}
		else
		{
			sublist.addField("custpage_po_asn", "text", "TO#");
		}

		sublist.addField("custpage_line", "text", "Line#");
		sublist.addField("custpage_itemname", "text", "Item");
		//sublist.addField("custpage_itemstatus", "select", "Item Status","customrecord_ebiznet_sku_status");
		
		
		var vItemStatus = sublist.addField('custpage_itemstatus', 'select', 'Item Status');	
		vItemStatus.addSelectOption("", "");
		var whlocarray = new Array();
		var itemStatusFilters = new Array();
		itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
		itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
		var whlocarray = new Array();
		whlocarray.push('@NONE@');		
		
		if(whloc !=null && whloc!='' && trantype!='transferorder')
		{
			whlocarray.push(whloc);
		}
		else
		{
			if(towhloc!=null && towhloc!='')
				whlocarray.push(towhloc);
		}
		 if(whlocarray != null && whlocarray != '')
			itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',whlocarray);
		var itemStatusColumns = new Array();
		itemStatusColumns[0] = new nlobjSearchColumn('internalid');
		itemStatusColumns[1] = new nlobjSearchColumn('name');
	//	itemStatusColumns[0].setSort();
		itemStatusColumns[1].setSort();

		var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);


		nlapiLogExecution('ERROR', 'Location infomartion for check in', whloc);


		if (itemStatusSearchResult != null){
			for (var i = 0; i < itemStatusSearchResult.length; i++) 
			{					
				vItemStatus.addSelectOption(itemStatusSearchResult[i].getValue('internalid'), itemStatusSearchResult[i].getValue('name'));		
			}
		}
		
		/*var vselectitemstatusres =FillItemstatus(whloc);
		selectitemstatus.addSelectOption("", "");
		var vsearchresults = new Array();
		if (vselectitemstatusres != null)
		{
			nlapiLogExecution("ERROR","vselectitemstatusres",vselectitemstatusres.length);
			for (var j = 0; j < vselectitemstatusres.length; j++) 
			{		
				//nlapiLogExecution("ERROR","vselectitemstatusres[j]",vselectitemstatusres[j]);
				vsearchresults.push(vselectitemstatusres[j]);
				for (var i1 = 0; i1 < vsearchresults.length; i1++) 
				{
					//nlapiLogExecution("ERROR","searchresults.length",vsearchresults.length);
					selectitemstatus.addSelectOption(vsearchresults[i1].getId(), vsearchresults[i1].getValue('name'));
				}
			}
		}*/
		
		
		sublist.addField("custpage_uom", "text", "UOM");
		sublist.addField("custpage_quantity", "text", "Quantity");
		sublist.addField("custpage_lp", "text", "LP#");
		// case no 20126511
		sublist.addField("custpage_seriallot", "textarea", "Serial/Lot Numbers");
		sublist.addField("custpage_location", "select", "Bin Location", "customrecord_ebiznet_location").setDisplayType('entry');
		sublist.addField("custpage_cartlp", "text", "Cart LP");
		sublist.addField("custpage_postatus", "text", "POStatus");
		sublist.addField("custpage_poid", "text", "PO ID").setDisplayType('hidden');
		sublist.addField("custpage_qcitemid", "text", "QcItem ID").setDisplayType('hidden');
		sublist.addField("custpage_opentaskid", "text", "Opentask ID").setDisplayType('hidden');
		sublist.addField("custpage_whsite", "text", "whsite").setDisplayType('hidden');

		nlapiLogExecution("ERROR","getPOid",getPOid);
		var trantype = nlapiLookupField('transaction', getPOid, 'recordType');
		var validate=CheckOrderStatus(getPOid,trantype);
		
//		To get search results based on page selection
		var searchresults=fnGetSearchresults(getPOid,getTrailer,getReceipt,-1);

		//Need to add function
		setPagingForSublist(searchresults,form,getPoValue,receiptno,getTrailer,getPOid,validate,arrayLP,request);

		form.addPageLink('crosslink', 'QC Validation', nlapiResolveURL('RECORD', 'purchaseorder', getPOid));
		

		nlapiLogExecution('ERROR', 'Time Stamp at the end of request',TimeStampinSec());	

		var autobutton = form.addSubmitButton('Generate Locations');
		response.writePage(form);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response of assign putaway', 'QC Location Generation Putaway');

		nlapiLogExecution('ERROR', 'Time Stamp at the start of response',TimeStampinSec());	
		nlapiLogExecution('ERROR', 'request.getParameter(custpage_hiddenfield)', request.getParameter('custpage_hiddenfield'));

		if(request.getParameter('custpage_hiddenfield')!=null && request.getParameter('custpage_hiddenfield')!='' && request.getParameter('custpage_hiddenfield')!='F')
		{				
			var POInternalId = request.getLineItemValue('custpage_items', 'custpage_poid', 1);
			var poValue = request.getLineItemValue('custpage_items', 'custpage_po_asn', 1);
			var trailerno=request.getParameter('custpage_trailerno');
			var receiptno=request.getParameter('custpage_receiptno');
			var trantype = nlapiLookupField('transaction', POInternalId, 'recordType');

			var POarrayAP = new Array();
			POarrayAP["custparam_po"] = POInternalId;
			POarrayAP["custparam_povalue"] = poValue;
			POarrayAP["custparam_ttype"] = "ASPW";
			POarrayAP["custparam_trailer"]=trailerno;
			POarrayAP["custparam_receipt"]=receiptno;

			var str = 'POInternalId. = ' + POInternalId + '<br>';
			str = str + 'poValue. = ' + poValue + '<br>';	
			str = str + 'trailerno. = ' + trailerno + '<br>';	
			str = str + 'receiptno. = ' + receiptno + '<br>';	
			str = str + 'trantype. = ' + trantype + '<br>';	

			nlapiLogExecution('ERROR', 'Field Values1 in POST', str);

			//To Create record in Throwaway custom record.
			var context = nlapiGetContext();
			nlapiLogExecution('ERROR','Remaining usage 1',context.getRemainingUsage());


			var lineCnt = request.getLineItemCount('custpage_items');
			var vItemArr=new Array();
			var vItemStatusArr=new Array();
// case# 201412152
	var newitemstat=new Array();
			var vRcvQtyArr=new Array();
			var vOpenTaskIdArr=new Array();
			var vBinLocArr=new Array();
			var vWHSiteArr=new Array();
			var vRecordsArr=new Array();
			var vRecLineNoArray = new Array();

			var vRecArr=new Array();
			for (var j = 1; j <= lineCnt; j++) {


				var confirmFlag= request.getLineItemValue('custpage_items', 'custpage_chk', j);	
				//nlapiLogExecution('ERROR','confirmFlag',confirmFlag);
				if(confirmFlag=='T')
				{
					var vItem1 = request.getLineItemValue('custpage_items', 'custpage_qcitemid', j);
					if(vItemArr.indexOf(vItem1) == -1)
						vItemArr.push(vItem1);

					//var itemType= nlapiLookupField('item',ItemId,'recordType');
					var ItmStatus1= request.getLineItemValue('custpage_items','custpage_itemstatus',j);			
	var ItmStatus1= request.getLineItemValue('custpage_items','custpage_itemstatus',j);	
					if(newitemstat.indexOf(ItmStatus1) == -1)
					{
						newitemstat.push(ItmStatus1);
						
					}
					var RcvQty1 = request.getLineItemValue('custpage_items', 'custpage_quantity', j);
					var openTaskID1 = request.getLineItemValue('custpage_items', 'custpage_opentaskid', j);
					var enterdBinLocID1 = request.getLineItemValue('custpage_items', 'custpage_location', j);
					var whsite1 = request.getLineItemValue('custpage_items', 'custpage_whsite', j);
					var lineno = request.getLineItemValue('custpage_items', 'custpage_line', j);
					if(vWHSiteArr.indexOf(whsite1) == -1)
						vWHSiteArr.push(whsite1);
					vRecArr.push(vItem1);
					vRecArr.push(ItmStatus1);
					vRecArr.push(RcvQty1);
					vRecArr.push(openTaskID1);
					vRecArr.push(enterdBinLocID1);
					vRecArr.push(whsite1);
					vRecLineNoArray.push(lineno);
					vRecordsArr.push(vRecArr);
				}
				else
				{

					nlapiLogExecution('ERROR', 'check box false');					  
					var form = nlapiCreateForm('Generate Location');
					nlapiLogExecution('ERROR', 'Form Called', 'form');					  
					var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
					msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Please check at least one line', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
					nlapiLogExecution('ERROR', 'message', msg);					  
					response.writePage(form);                  						
					return;		
				}
			}
	newitemstat.push('@NONE@');
			//

			var str = 'vItemArr.length. = ' + vItemArr.length + '<br>';
			str = str + 'vWHSiteArr.length. = ' + vWHSiteArr.length + '<br>';	
			str = str + 'vRecordsArr.length. = ' + vRecordsArr.length + '<br>';	

			nlapiLogExecution('ERROR', 'Field Values2 in POST', str);

			// To get all the Priority putaway records
			if(vItemArr.length>0)
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', vItemArr));
				filters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));
				filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				if(vWHSiteArr!=null && vWHSiteArr!='')
					filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', vWHSiteArr));
if(newitemstat!=null && newitemstat!='' && newitemstat.length >1 )
					filters.push(new nlobjSearchFilter('custrecord_pickface_itemstatus', null, 'anyof',newitemstat));
				//custrecord_pickbinloc
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_pickbinloc');
				columns[1] = new nlobjSearchColumn('custrecord_maxqty');
				columns[2] = new nlobjSearchColumn('custrecord_pickzone');
				columns[3] = new nlobjSearchColumn('custrecord_pickface_itemstatus');
				//case# 20148251 starts (Pickface location issue)
				columns[4] = new nlobjSearchColumn('custrecord_pickfacesku');
				columns[5] = new nlobjSearchColumn('custrecord_pickface_location');
				//case# 20148251 end
				var pickFaceResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);
				if(pickFaceResults != null && pickFaceResults != '')
					nlapiLogExecution('ERROR', 'pickFaceResults.length', pickFaceResults.length);

				var filters1 = new Array();
				filters1[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', vItemArr);            
				filters1[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

				var columns1 = new Array();
				columns1[0] = new nlobjSearchColumn('custrecord_ebizcube');
				columns1[1] = new nlobjSearchColumn('custrecord_ebizqty');
				columns1[2] = new nlobjSearchColumn('custrecord_ebizitemdims');
				var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters1, columns1);

				var filters2 = new Array();

				var columns2 = new Array();
				columns2[0] = new nlobjSearchColumn('custitem_item_family');
				columns2[1] = new nlobjSearchColumn('custitem_item_group');
				columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
				columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
				columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
				columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
				columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

				filters2.push(new nlobjSearchFilter('internalid', null, 'is', vItemArr));

				var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);
				var filters3=new Array();
				filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				if(vWHSiteArr !=null && vWHSiteArr !='')
					filters3.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', vWHSiteArr));
				var columns3=new Array();
				columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
				columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
				columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
				columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
				columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
				columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
				columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
				columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
				columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
				columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
				columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
				columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
				columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
				columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
				//  Case #:start 20125390� Fetching putaway strategies
				columns3[14] = new nlobjSearchColumn('formulanumeric');
				columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");				
				columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
				//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
				columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
				// Upto here on 29OCT2013 - Case # 20124515
				columns3[14].setSort();
				//end

				var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);			
			}	
			nlapiLogExecution('ERROR','Remaining usage 11',context.getRemainingUsage());
			/*var newParent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record
		var parentid = nlapiSubmitRecord(newParent); //save parent record
		nlapiLogExecution('ERROR','Remaining usage 2',context.getRemainingUsage());
		var parent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid); //load Open task parent record back into memory
			 */		//var parentBinLoc = nlapiLoadRecord('recmachcustrecord_ebiz_binloc_parent', parentid); //load Bin Location parent record back into memory

			nlapiLogExecution('ERROR','Remaining usage 3',context.getRemainingUsage());

			var Loctdims =new Array();
			var putstrategy='';
			var putmethod='';
			var prevItemId='';

			for (var s = 1; s <= lineCnt; s++) {

				//nlapiLogExecution('ERROR', 's', s);
				var confirmFlag= request.getLineItemValue('custpage_items', 'custpage_chk', s);	
				//nlapiLogExecution('ERROR','confirmFlag',confirmFlag);
				if(confirmFlag=='T')
				{
					var ItemId = request.getLineItemValue('custpage_items', 'custpage_qcitemid', s);
					//var itemType= nlapiLookupField('item',ItemId,'recordType');
					var ItmStatus= request.getLineItemValue('custpage_items','custpage_itemstatus',s);			
					var RcvQty = request.getLineItemValue('custpage_items', 'custpage_quantity', s);
					var openTaskID = request.getLineItemValue('custpage_items', 'custpage_opentaskid', s);
					var enterdBinLocID = request.getLineItemValue('custpage_items', 'custpage_location', s);
					var whsite = request.getLineItemValue('custpage_items', 'custpage_whsite', s);
					var lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
					var vbatch = request.getLineItemValue('custpage_items', 'custpage_seriallot', s);
					
					

					/*
					nlapiLogExecution('ERROR', 'ItemId', ItemId);
					nlapiLogExecution('ERROR', 'fetched ItmStatus', ItmStatus);
					nlapiLogExecution('ERROR', 'whsite', whsite);					
					 */

					// Item dimensions
					//nlapiLogExecution('ERROR', 'openTaskID', openTaskID);
					var dimArray = new Array();
					var cube =0;
					var BaseUOMQty=0;


					if(skuDimsSearchResults != null){
						for (var i = 0; i < skuDimsSearchResults.length; i++) {
							var skuDim = skuDimsSearchResults[i];
							if(skuDim.getValue('custrecord_ebizitemdims') != null && skuDim.getValue('custrecord_ebizitemdims') != '' && skuDim.getValue('custrecord_ebizitemdims') ==ItemId)
							{ 	
								cube = skuDim.getValue('custrecord_ebizcube');
								BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
							}
						}
					}

					dimArray["BaseUOMItemCube"] = cube;
					dimArray["BaseUOMQty"] = BaseUOMQty;

					nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:Cube', cube);
					nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:Weight', BaseUOMQty);

					var itemCube = 0;
					if (cube != "" && cube != null) 
					{
						if (!isNaN(cube)) 
						{
							var uomqty = ((parseFloat(RcvQty))/(parseFloat(BaseUOMQty)));			
							itemCube = (parseFloat(uomqty) * parseFloat(cube));
							nlapiLogExecution('ERROR', 'ItemCube', itemCube);
						}
						else 
						{
							itemCube = 0;
						}
					}
					else 
					{
						itemCube = 0;
					}
					nlapiLogExecution('ERROR', 'ItemId Values', (ItemId + "-" + itemCube));

					//Calling Function to get Location Id.
					var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;

					var priorityPutawayLocnArr = priorityPutawayNew(ItemId, RcvQty,whsite,pickFaceResults);
					var priorityRemainingQty = priorityPutawayLocnArr[0];
					var priorityQty = priorityPutawayLocnArr[1];
					var priorityLocnID = priorityPutawayLocnArr[2];

					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityRemainingQty', priorityRemainingQty);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityQty', priorityQty);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityLocnID', priorityLocnID);

					// If quantity can be putaway'ed on priority, create PUTW task in TRN_OPENTASK
					// set taskType = 2(PUTW) and wmsStatusFlag = 2(INBOUND/LOCATIONS ASSIGNED)
					// beginLocation = priorityLocnID and endLocn = ""

					if (priorityQty != 0 && (priorityLocnID != null && priorityLocnID != ""))
					{
						nlapiLogExecution('ERROR', 'Inside  Loop priorityQty', priorityQty);
						//taskType = "2"; // PUTW
						//wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
						LocId = priorityLocnID;
						RemCube = GeteLocCube(LocId);
						nlapiLogExecution('ERROR', 'RemCube', RemCube);
						if (LocId != null && LocId != "") 
						{
							var locremcube = (parseFloat(RemCube) - parseFloat(itemCube));
							nlapiLogExecution('ERROR', 'RemCube', locremcube);
							//var retValue = UpdateLocCubeParent(LocId, parseFloat(locremcube),parent);
							var retValue = UpdateLocCube(LocId, parseFloat(locremcube));
							nlapiLogExecution('ERROR', 'LocId in UpdateLocCube', retValue);

						}
						//endLocn = "";            	
					}
					nlapiLogExecution('ERROR', 'enterdBinLocID', enterdBinLocID);
					var Loctdims =new Array();
					var putstrategy='';
					var putmethod='';

					// Check if there is any remaining quantity to be putaway
					if (parseFloat(priorityRemainingQty) != 0) 
					{
						if(enterdBinLocID == "" || enterdBinLocID == null)
						{
							nlapiLogExecution('ERROR', 'calling GetPutawayLocation');
							nlapiLogExecution('ERROR', 'ItemId',ItemId);
							nlapiLogExecution('ERROR', 'ItmStatus',ItmStatus);
							nlapiLogExecution('ERROR', 'itemCube',itemCube);
							//nlapiLogExecution('ERROR', 'itemType',itemType);
							nlapiLogExecution('ERROR', 'whsite',whsite);
							//Uncomment "GenerateBinLocationforPutaway" and comment "GetPutawayLocation" for EPICUREN 
							//Loctdims = GenerateBinLocationforPutaway(ItemId, null, ItmStatus, null, itemCube,itemType,whsite);      
							Loctdims = GetPutawayLocationNew(ItemId, null, ItmStatus, null, itemCube,null,whsite,ItemInfoResults,putrulesearchresults,priorityLocnID); // case# 201412982

							if (Loctdims.length > 0) 
							{
								if (Loctdims[0] != "") 
								{
									LocName = Loctdims[0];
									nlapiLogExecution('ERROR', 'LocName', LocName);
								}
								else 
								{
									LocName = "";
									nlapiLogExecution('ERROR', 'in else LocName', LocName);
								}
								if (!isNaN(Loctdims[1])) 
								{
									RemCube = Loctdims[1];
								}
								else 
								{
									RemCube = 0;
								}
								if (!isNaN(Loctdims[2])) 
								{
									LocId = Loctdims[2];
									nlapiLogExecution('ERROR', 'LocId', LocId);
								}
								else 
								{
									LocId = "";
									nlapiLogExecution('ERROR', 'in else LocId', LocId);
								}
								if (!isNaN(Loctdims[3])) 
								{
									OubLocId = Loctdims[3];
								}
								else 
								{
									OubLocId = "";
								}
								if (!isNaN(Loctdims[4])) 
								{
									PickSeq = Loctdims[4];
								}
								else 
								{
									PickSeq = "";
								}
								if (!isNaN(Loctdims[5])) 
								{
									InbLocId = Loctdims[5];
								}
								else 
								{
									InbLocId = "";
								}
								if (!isNaN(Loctdims[6])) {
									PutSeq = Loctdims[6];
								}
								else {
									PutSeq = "";
								}
								if (!isNaN(Loctdims[7])) {
									LocRemCube = Loctdims[7];
								}
								else {
									LocRemCube = "";
								}

								putmethod=Loctdims[9];
								putstrategy=Loctdims[10];
							}
							else 
							{
								nlapiLogExecution('ERROR', 'If Loc Length is Null', 'Making Values Null');
								LocName = "";
								LocId = "";
								RemCube = "";
								InbLocId = "";
								OubLocId = "";
								PickSeq = "";
								PutSeq = "";
								putmethod="";
								putstrategy="";
							}
						}
						else
						{
							LocId = enterdBinLocID;
							RemCube = GeteLocCube(LocId);
							if(prevItemId!=ItemId)
							{
								prevItemId=ItemId;

								Loctdims = GetPutawayLocationNew(ItemId, null, ItmStatus, null, itemCube,null,whsite,ItemInfoResults,putrulesearchresults,priorityLocnID); // case# 201412982

								if (Loctdims.length > 0) 
								{							
									putmethod=Loctdims[9];
									putstrategy=Loctdims[10];
								}
							}
						}
						nlapiLogExecution('ERROR', 'LocId in UpdateLocCube', LocId);
						nlapiLogExecution('ERROR', 'itemCube', itemCube);
						nlapiLogExecution('ERROR', 'RemCube', RemCube);
						if (LocId != null && LocId != "") 
						{
							//if(itemCube <= RemCube)
							if (RemCube >= 0)
							{
								if(enterdBinLocID!=null && enterdBinLocID!='')
								{
									var oldremcube = (parseFloat(RemCube) - parseFloat(itemCube));		

									if(parseFloat(oldremcube)<0)
										oldremcube=0;

									RemCube = parseFloat(oldremcube);
									var retValue = UpdateLocCube(LocId, parseFloat(RemCube));
								}
								else
								{							
									//var retValue = UpdateLocCubeParent(LocId, parseFloat(RemCube),parent);
									var retValue = UpdateLocCube(LocId, parseFloat(RemCube));
								}
								nlapiLogExecution('ERROR', 'LocId in UpdateLocCube', retValue);
							}
							
							var itemSubtype = nlapiLookupField('item', ItemId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
							
							nlapiLogExecution('ERROR', 'vbatch@@@', vbatch);
							nlapiLogExecution('ERROR', 'ItemId@@@', ItemId);
							nlapiLogExecution('ERROR', 'whsite@@@', whsite);
							
							if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == "lotnumberedassemblyitem" ||itemSubtype.recordType == "assemblyitem" || itemSubtype.custitem_ebizbatchlot == 'T')
							{
								var filtersbat = new Array();
								filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', vbatch);
								if(ItemId!=null&&ItemId!="")
									filtersbat[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', ItemId);
								if(whsite!=null && whsite!="")
									filtersbat[2] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whsite);	
								
								var columnsbat = new Array();
								columnsbat[0] = new nlobjSearchColumn('custrecord_ebizmfgdate');
								columnsbat[1] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
								columnsbat[2] = new nlobjSearchColumn('custrecord_ebizexpirydate');
								columnsbat[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
								columnsbat[4] = new nlobjSearchColumn('custrecord_ebizfifocode');
								columnsbat[5] = new nlobjSearchColumn('custrecord_ebiz_manufacturelot');
								columnsbat[6] = new nlobjSearchColumn('custrecord_ebizlastavldate');
								var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat,columnsbat);
								if (SrchRecord !=null && SrchRecord !='' ) 
								{

									var mfgdate = SrchRecord[0].getValue('custrecord_ebizmfgdate');
									var bfdate = SrchRecord[0].getValue('custrecord_ebizbestbeforedate');
									var expirydate = SrchRecord[0].getValue('custrecord_ebizexpirydate');
									var fifodate = SrchRecord[0].getValue('custrecord_ebizfifodate');
									var fifocode = SrchRecord[0].getValue('custrecord_ebizfifocode');
									var manflot = SrchRecord[0].getValue('custrecord_ebiz_manufacturelot');
									var lastvaldlot = SrchRecord[0].getValue('custrecord_ebizlastavldate');

									nlapiLogExecution('DEBUG', ' BATCH FOUND');
									var transaction = nlapiLoadRecord('customrecord_ebiznet_batch_entry', SrchRecord[0].getId());
									if(mfgdate != null && mfgdate!= '')
										transaction.setFieldValue('custrecord_ebizmfgdate', mfgdate);
									if(bfdate != null && bfdate != '')
										transaction.setFieldValue('custrecord_ebizbestbeforedate', bfdate);
									if(expirydate != null && expirydate != '')
										transaction.setFieldValue('custrecord_ebizexpirydate', expirydate);
									if(fifodate!= null && fifodate != '')
										transaction.setFieldValue('custrecord_ebizfifodate', fifodate);
									if(fifocode != null && fifocode != '')
										transaction.setFieldValue('custrecord_ebizfifocode', fifocode);
									if(lastvaldlot != null && lastvaldlot != '')
										transaction.setFieldValue('custrecord_ebizlastavldate', lastvaldlot);
									if(manflot != null && manflot != '')
									{	transaction.setFieldValue('custrecord_ebiz_manufacturelot',manflot);

									}

									var rec = nlapiSubmitRecord(transaction, false, true);

								}
								else
								{
									nlapiLogExecution('DEBUG', 'BATCH NOT FOUND');
									var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
									customrecord.setFieldValue('name', vbatch);
									if(vbatch!=null && vbatch!='')
									customrecord.setFieldValue('custrecord_ebizlotbatch', vbatch);
									if(whsite!=null && whsite!='')
									customrecord.setFieldValue('custrecord_ebizsitebatch',whsite);
									if(ItemId != null && ItemId != '')
										customrecord.setFieldValue('custrecord_ebizsku', ItemId);
									if(ItmStatus != null && ItmStatus != '')
									customrecord.setFieldValue('custrecord_ebizskustatus', ItmStatus);									
									var	getPOLinePackCode=1;							
									customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);
									
									customrecord.setFieldValue('custrecord_ebizexpirydate', DateStamp());
									customrecord.setFieldValue('custrecord_ebizfifodate', DateStamp());
									var rec = nlapiSubmitRecord(customrecord, false, true);

								}
								
							}
							
						}
					}

					nlapiLogExecution('ERROR', 'priorityLocnID', priorityLocnID);
					nlapiLogExecution('ERROR', 'itemCube', itemCube);
					nlapiLogExecution('ERROR', 'RemCube', RemCube);
					nlapiLogExecution('ERROR', 'trantype', trantype);
					var type = trantype;
					//case# 20148709 starts
					//for (var p1 = 0; p1 < vRecLineNoArray.length; p1++) 
					//{
						//nlapiLogExecution('ERROR', 'int new trnline updation', vRecLineNoArray[p1]);
					nlapiLogExecution('ERROR', 'lineno', lineno);
					nlapiLogExecution('ERROR', 'poValue', poValue);
						var ttype = 'ASPW';	
						//var lineno = vRecLineNoArray[p1];
						//var tranValue = TrnLineUpdation(type, ttype, null, poValue, lineno, null, null, RcvQty);
						var tranValue = TrnLineUpdation(trantype, ttype, poValue, POInternalId, lineno, ItemId, '', RcvQty,'','')
					//}
					//case# 20148709 ends
					/*for (var p1 = 0; p1 < vRecLineNoArray.length; p1++) 
					{
						nlapiLogExecution('ERROR', 'int new trnline updation', vRecLineNoArray[p1]);
					
						var ttype = 'ASPW';	
						var lineno = vRecLineNoArray[p1];
						//var tranValue = TrnLineUpdation(type, ttype, null, poValue, lineno, null, null, RcvQty);
						//var tranValue = TrnLineUpdationNew(trantype, ttype, poValue, POInternalId, lineno, ItemId, '', RcvQty,'','')
					}*/

					if(0 <= RemCube || (priorityLocnID != null && priorityLocnID != ""))
					{
						//updatePUTWTask(LocId,ItmStatus,openTaskID,putmethod,putstrategy,parent);
						updatePUTWTask(LocId,ItmStatus,openTaskID,putmethod,putstrategy);
					}
					else
					{
						if(enterdBinLocID!=null && enterdBinLocID!='')
						{
							//updatePUTWTask(LocId,ItmStatus,openTaskID,putmethod,putstrategy,parent);
							updatePUTWTask(LocId,ItmStatus,openTaskID,putmethod,putstrategy);

							var remcube = 0;	
							nlapiLogExecution('ERROR', 'RemCube', remcube);
							//var retValueLoc = UpdateLocCubeParent(LocId, parseFloat(remcube),parent);
							var retValueLoc = UpdateLocCube(LocId, parseFloat(remcube));
							nlapiLogExecution('ERROR', 'LocId in UpdateLocCube', retValueLoc);
						}
						else
						{

							nlapiLogExecution('ERROR', 'inside Loc Cube', 'inside loop');					  
							var form = nlapiCreateForm('Generate Location');
							nlapiLogExecution('ERROR', 'Form Called', 'form');					  
							var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
							msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Qty exceeds location cube capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
							nlapiLogExecution('ERROR', 'message', msg);					  
							response.writePage(form);                  						
							return;		
						}
					}
				}
				else
				{

					nlapiLogExecution('ERROR', 'check box false');					  
					var form = nlapiCreateForm('Generate Location');
					nlapiLogExecution('ERROR', 'Form Called', 'form');					  
					var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
					msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Please check at least one line', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
					nlapiLogExecution('ERROR', 'message', msg);					  
					response.writePage(form);                  						
					return;		
				}
				
				
			}
			//nlapiSubmitRecord(parent); //submit the parent record, all child records will also be updated
			//nlapiSubmitRecord(parentOT); //submit the parent record, all child records will also be updated
			nlapiLogExecution('ERROR','Remaining usage 4',context.getRemainingUsage());
			POarrayAP["custparam_ttype"] = "ASPW";

			nlapiLogExecution('ERROR', 'Time Stamp at the end of response',TimeStampinSec());	
			if(trantype=='purchaseorder')
			{
				//response.sendRedirect('SUITELET', 'customscript_assignputaway', 'customdeploy_ebizassignputaway', false, POarrayAP);

				//The following code is added by Satish.N to skip putaway report.
				var POarrayAPNew = new Array();
				POarrayAPNew["custparam_trailer"]=trailerno;
				POarrayAPNew["custparam_receipt"]=receiptno;
				POarrayAPNew["custparam_poid"]=POInternalId;

				response.sendRedirect('SUITELET', 'customscript_confirmputaway', 'customdeploy1', false, POarrayAPNew);
				//Upto here.

			}
			else if(trantype=='returnauthorization')
			{
				response.sendRedirect('SUITELET', 'customscript_rma_putawayreport_sl', 'customdeploy_rma_putawayreport_di', false, POarrayAP);
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_to_putawayreport_sl', 'customdeploy_to_putawayreport_di', false, POarrayAP);
			}
		} 
		else
		{
			var getPOid = request.getParameter('custpage_hdnpo');
			nlapiLogExecution('ERROR', 'getPOid', getPOid);

			var getTrailer = request.getParameter('custpage_trailer');

			nlapiLogExecution('ERROR', 'getTrailer', getTrailer);

			var getReceipt = request.getParameter('custpage_receipt');
			nlapiLogExecution('ERROR', 'getReceipt', getReceipt);

			var getPoValue = request.getParameter('custpage_povalue');

			nlapiLogExecution('ERROR', 'getPoValue', getPoValue);

			var form = nlapiCreateForm('Putaway Location Generation');
			var trantype;
			if(getPOid != null && getPOid != "")
				trantype = nlapiLookupField('transaction', getPOid, 'recordType');
			else if((getTrailer != null && getTrailer != "") || (getReceipt != null && getReceipt != ""))
			{
				nlapiLogExecution('ERROR', 'trantype inif', trantype);
				trantype ='purchaseorder';
			}	

			nlapiLogExecution('ERROR', 'trantype', trantype);

			var POHidden = form.addField('custpage_hdnpo', 'text', '').setDisplayType('hidden');
			POHidden.setDefaultValue(getPOid);
			var hdnTrailer = form.addField('custpage_trailer', 'text', '').setDisplayType('hidden');
			hdnTrailer.setDefaultValue(getTrailer);
			var hdnReceipt = form.addField('custpage_receipt', 'text', '').setDisplayType('hidden');
			hdnReceipt.setDefaultValue(getReceipt);
			var hdnPOValue = form.addField('custpage_povalue', 'text', '').setDisplayType('hidden');
			hdnPOValue.setDefaultValue(getPoValue);

			var selectReceive;
			var selectRType;
			var selectPO;
			var hiddenField = form.addField('custpage_hiddenfield', 'text', '').setDisplayType('hidden');
			hiddenField.setDefaultValue('T');
			if(trantype=='purchaseorder')
			{
				selectReceive = form.addField('custpage_selectreceive', 'select', 'Receive Mode').setLayoutType('startrow');

				selectReceive.addSelectOption('P', 'PO');
				selectReceive.addSelectOption('A', 'ASN');

				selectRType = form.addField('custpage_receive_type', 'select', 'Receipt Type').setLayoutType('midrow');
				selectRType.addSelectOption('PO', 'Purchase Order');

				selectPO = form.addField('custpage_po', 'text', 'PO #').setDisplayType('disabled');
				selectPO.setLayoutType('endrow');
				selectPO.setDefaultValue(getPoValue);

				selectRecipt = form.addField('custpage_receiptno', 'text', 'Receipt #').setDisplayType('disabled');
				selectRecipt.setLayoutType('startrow');

				selectTrailer = form.addField('custpage_trailerno', 'text', 'Trailer #').setDisplayType('disabled');
				selectTrailer.setLayoutType('midrow');

				if(getTrailer != null && getTrailer != "")
					selectTrailer.setDefaultValue(getTrailer);
				if(getReceipt != null && getReceipt != "")
					selectRecipt.setDefaultValue(getReceipt);

				nlapiLogExecution('ERROR', 'getTrailer', getTrailer);
				nlapiLogExecution('ERROR', 'getReceipt', getReceipt);
			}
			else if(trantype=='returnauthorization')
			{
				selectReceive = form.addField('custpage_selectreceive', 'select', 'Receive Mode').setLayoutType('startrow');
				selectReceive.addSelectOption('R', 'RMA');           

				selectRType = form.addField('custpage_receive_type', 'select', 'Receipt Type').setLayoutType('midrow');
				selectRType.addSelectOption('RMA', 'Return Authorization');

				selectPO = form.addField('custpage_po', 'text', 'RMA #').setDisplayType('disabled');
				selectPO.setLayoutType('endrow');
				selectPO.setDefaultValue(getPoValue);
			}
			else
			{
				selectReceive = form.addField('custpage_selectreceive', 'select', 'Receive Mode').setLayoutType('startrow');

				selectReceive.addSelectOption('T', 'TO');

				selectRType = form.addField('custpage_receive_type', 'select', 'Receipt Type').setLayoutType('midrow');
				selectRType.addSelectOption('TO', 'Transfer Order');

				selectPO = form.addField('custpage_po', 'text', 'TO #').setDisplayType('disabled');
				selectPO.setLayoutType('endrow');
				selectPO.setDefaultValue(getPoValue);
			}



			// Serial# s for serialized Item
			var arrayLP = new Array();
			var lpcolumns = new Array();
			lpcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
			lpcolumns[1] = new nlobjSearchColumn('custrecord_serialnumber');
			// case # 20124849,System is not displaying the serial# in the location generation screen after auto break down .
			var lpfilters=new Array();
			if(getPOid!=null && getPOid!='')
			{
				if(trantype=='returnauthorization')
					lpfilters = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', getPOid);
				else
			lpfilters = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', getPOid);
			//End
			var lpsearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, lpfilters, lpcolumns);
			for (var i = 0; lpsearchresults != null && i < lpsearchresults.length; i++) {
				var SerialLP = new Array();
				SerialLP[0] = lpsearchresults[i].getValue('custrecord_serialparentid');
				SerialLP[1] = lpsearchresults[i].getValue('custrecord_serialnumber');
				arrayLP.push(SerialLP);

			}
			}
			/*for (var i = 0; i < arrayLP.length; i++) 
				nlapiLogExecution('ERROR', 'arrayLP - [LP#|Serial#]', '[' + arrayLP[i][0] + '|' + arrayLP[i][1] + ']');*/

			FromStatus = form.addField('custpage_fromstatus',  "select", "From Item Status","customrecord_ebiznet_sku_status").setLayoutType('midrow');
			ToStatus = form.addField('custpage_tostatus',  "select", "To Item Status","customrecord_ebiznet_sku_status").setLayoutType('endrow');
			form.addButton('custpage_set','SET','Set()');//.setLayoutType('endrow');
			var GenLocFlag= form.addField('custpage_qcbinlocgen', 'checkbox', 'Bin Location Generation').setDisplayType('disabled').setDefaultValue('T');
			/*
	         Defining SubList lines
			 */
			var sublist = form.addSubList("custpage_items", "list", "ItemList");
			sublist.addMarkAllButtons();
			sublist.addField("custpage_chk", "checkbox","Check").setDefaultValue('T');
			if(trantype=='purchaseorder')
			{
				sublist.addField("custpage_po_asn", "text", "PO/ASN#");
				sublist.addField("custpage_receipt", "text", "Receipt#");
				sublist.addField("custpage_trailerno", "text", "Appointment#");
			}
			else if(trantype=='returnauthorization')
			{
				sublist.addField("custpage_po_asn", "text", "RMA#");
			}
			else
			{
				sublist.addField("custpage_po_asn", "text", "TO#");
			}

			sublist.addField("custpage_line", "text", "Line#");
			sublist.addField("custpage_itemname", "text", "Item");
			sublist.addField("custpage_itemstatus", "select", "Item Status","customrecord_ebiznet_sku_status");
			sublist.addField("custpage_uom", "text", "UOM");
			sublist.addField("custpage_quantity", "text", "Quantity");
			sublist.addField("custpage_lp", "text", "LP#");
			sublist.addField("custpage_seriallot", "text", "Serial/Lot Numbers");
			sublist.addField("custpage_location", "select", "Bin Location", "customrecord_ebiznet_location").setDisplayType('entry');
			sublist.addField("custpage_cartlp", "text", "Cart LP");
			sublist.addField("custpage_postatus", "text", "POStatus");
			sublist.addField("custpage_poid", "text", "PO ID").setDisplayType('hidden');
			sublist.addField("custpage_qcitemid", "text", "QcItem ID").setDisplayType('hidden');
			sublist.addField("custpage_opentaskid", "text", "Opentask ID").setDisplayType('hidden');
			sublist.addField("custpage_whsite", "text", "whsite").setDisplayType('hidden');

//			To get search results based on page selection
			var searchresults=fnGetSearchresults(getPOid,getTrailer,getReceipt,-1);
			
			nlapiLogExecution("ERROR","getPOid",getPOid);
			var trantype = nlapiLookupField('transaction', getPOid, 'recordType');
			var validate=CheckOrderStatus(getPOid,trantype);
			
			//Need to add function
			setPagingForSublist(searchresults,form,getPoValue,receiptno,trailerno,getPOid,validate,arrayLP,request);

			form.addPageLink('crosslink', 'QC Validation', nlapiResolveURL('RECORD', 'purchaseorder', getPOid));
			form.setScript('customscriptebiz_generatelocation_cl');

			nlapiLogExecution('ERROR', 'Time Stamp at the end of response',TimeStampinSec());	

			var autobutton = form.addSubmitButton('Generate Locations');
			response.writePage(form);

		}
	} //Close of else.
}

//function updatePUTWTask(LocId,ItmStatus,openTaskID,putmethod,putstrategy,parent)
function updatePUTWTask(LocId,ItmStatus,openTaskID,putmethod,putstrategy)
{
	nlapiLogExecution('ERROR','Into Updating open task',openTaskID);

	//Updating PUTW task type in trn_opentask table for this PO with location and status flag as "L".
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_actbeginloc';
	fields[1] = 'custrecord_upd_date';
	fields[2] = 'custrecord_wms_status_flag';
	fields[3] = 'custrecord_sku_status';
	fields[4] = 'custrecordact_begin_date';
	fields[5] = 'custrecord_ebizrule_no';
	fields[6] = 'custrecord_ebizmethod_no';
	fields[7] = 'custrecord_actualbegintime';
	nlapiLogExecution('ERROR', 'ItmStatus in updateopentask', ItmStatus);	
	values[0] = LocId;
	values[1] = DateStamp();
	values[2] = '2';
	values[3] = ItmStatus;
	values[4] = DateStamp();
	values[5] = putstrategy;
	values[6] = putmethod;
	values[7] = TimeStamp();

	var updatefields = nlapiSubmitField('customrecord_ebiznet_trn_opentask', openTaskID, fields, values);
	nlapiLogExecution('ERROR','Updating open task',openTaskID);
	/*parent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent'); //select a new line from the "Child 1" sublist
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', openTaskID); //add an existing child record
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actbeginloc', LocId); //add an existing child record
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_upd_date', DateStamp()); //set child field c1f1
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_wms_status_flag', '2'); //set child field c1f2
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_sku_status', ItmStatus); //set child field c1f3
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecordact_begin_date', DateStamp()); //add an existing child record
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebizrule_no', putstrategy); //set child field c1f1
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebizmethod_no', putmethod); //set child field c1f2
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actualbegintime', TimeStamp()); //set child field c1f3
	parent.commitLineItem('recmachcustrecord_ebiz_ot_parent'); //commit the Child record line*/	
	nlapiLogExecution('ERROR','End of Updating open task',openTaskID);
}

function GenerateBinLocationforPutaway(Item, PackCode, ItemStatus, UOM, ItemCube, ItemType,poLocn){
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining usage at the start',context.getRemainingUsage());
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod,PutMethodName,PutRuleId, MergeFlag = 'F',Mixsku='F';
	nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);//0
	nlapiLogExecution('DEBUG', 'ItemType', ItemType);
	var itemresults;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	//var ItemStatus = columns.custitem_ebizdefskustatus;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;

	nlapiLogExecution('ERROR', 'ItemFamily', ItemFamily);
	nlapiLogExecution('ERROR', 'ItemGroup', ItemGroup);
	nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
	nlapiLogExecution('ERROR', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('ERROR', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('ERROR', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('ERROR', 'putVelocity', putVelocity);
	nlapiLogExecution('ERROR', 'poLocn', poLocn);

	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();

	var filters = new Array();
	columns[0] = new nlobjSearchColumn('formulanumeric');
	columns[0].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', ItemGroup]));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', ItemStatus]));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'any',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'any',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'any',['@NONE@', ItemInfo3]));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', Item]));		
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_abcvelpickrule', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(poLocn != null && poLocn != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', [poLocn]));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var t1 = new Date();
	var rulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (rulesearchresults != null) {
		nlapiLogExecution('DEBUG', 'rules count', rulesearchresults.length);
		for (var s = 0; s < rulesearchresults.length; s++) {
			locgroupid = rulesearchresults[s].getValue('custrecord_locationgrouppickrule');
			zoneid = rulesearchresults[s].getValue('custrecord_putawayzonepickrule');
			PutMethod = rulesearchresults[s].getValue('custrecord_putawaymethod');
			PutMethodName = rulesearchresults[s].getText('custrecord_putawaymethod');
			PutRuleId = rulesearchresults[s].getValue('custrecord_ruleidpickrule');
			nlapiLogExecution('DEBUG', 'Put Rule', PutRuleId);
			nlapiLogExecution('DEBUG', 'Put Method', PutMethod);
			nlapiLogExecution('DEBUG', 'Location Group Id from Putaway Rule', locgroupid);

			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = rulesearchresults[s].getValue('custrecord_manual_location_generation');
			nlapiLogExecution('DEBUG', 'Manual Location Generation', ManualLocationGeneration);

			//if (ManualLocationGeneration == 'F') {
			if (locgroupid != 0 && locgroupid != null) {
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
				columns[0].setSort();
				columns[1] = new nlobjSearchColumn('name');
				columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
				columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
				columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
				columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

				nlapiLogExecution('DEBUG', 'locgroupid', locgroupid);

				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]));	
				filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				if(poLocn != null && poLocn != "")
					filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', [poLocn]));


				t1 = new Date();
				var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
				t2 = new Date();
				nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:location',
						getElapsedTimeDuration(t1, t2));

				nlapiLogExecution('DEBUG', 'locResults', locResults);
				if (locResults != null) {
					nlapiLogExecution('DEBUG', 'ifnot ');
					for (var u = 0; u < locResults.length; u++) {

						Location = locResults[u].getValue('name');
						var locationIntId = locResults[u].getId();

						nlapiLogExecution('DEBUG', 'Location', Location);
						nlapiLogExecution('DEBUG', 'Location Id', locationIntId);

						//Commented by satish.N on 04-JAN-2012 as the search results will give the same value.
						//LocRemCube = GeteLocCube(locationIntId);
						LocRemCube = locResults[u].getValue('custrecord_remainingcube');							
						OBLocGroup = locResults[u].getValue('custrecord_outboundinvlocgroupid');
						PickSeqNo  = locResults[u].getValue('custrecord_startingpickseqno');
						IBLocGroup = locResults[u].getValue('custrecord_inboundinvlocgroupid');
						PutSeqNo   = locResults[u].getValue('custrecord_startingputseqno');

						nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
						nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);

						if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
							RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
							location_found = true;

							LocArry[0] = Location;
							LocArry[1] = RemCube;
							LocArry[2] = locationIntId;	
							LocArry[3] = OBLocGroup;
							LocArry[4] = PickSeqNo;
							LocArry[5] = IBLocGroup;
							LocArry[6] = PutSeqNo;
							LocArry[7] = LocRemCube;
							LocArry[8] = zoneid;
							LocArry[9] = PutMethodName;
							LocArry[10] = PutRuleId;

							nlapiLogExecution('DEBUG', 'SA-Location', Location);
							nlapiLogExecution('DEBUG', 'Location Internal Id',locationIntId);
							nlapiLogExecution('DEBUG', 'Sa-Location Cube', RemCube);
							return LocArry;
						}

					}
				}
			}
			else {
				nlapiLogExecution('DEBUG', 'Fetching Location Group from Zone', zoneid);
				var mixarray =  new Array();
				if(PutMethod!=null && PutMethod!='')
					mixarray=GetMergeFlag(PutMethod);
				if(mixarray!=null && mixarray!='' && mixarray.length>0)
				{
					MergeFlag = mixarray[0][0];
					Mixsku = mixarray[0][1];
				}
				nlapiLogExecution('DEBUG', 'Merge Flag', MergeFlag);
				nlapiLogExecution('DEBUG', 'Mix SKU', Mixsku);

				if (zoneid != null && zoneid != ""){ 
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_zone_seq');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('custrecord_locgroup_no');

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', zoneid));
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));
					t1 = new Date();
					var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:zone_locgroup',
							getElapsedTimeDuration(t1, t2));
					for (var i = 0; i < Math.min(30, searchresults.length); i++) {
						locgroupid = searchresults[i].getValue('custrecord_locgroup_no');
						nlapiLogExecution('DEBUG', 'Fetched Location Group Id from Putaway Zone', locgroupid);
						var RemCube = 0;
						var LocRemCube = 0;

						if (MergeFlag == 'T') {
							nlapiLogExecution('DEBUG', 'Inside Merge ');
							var locResults;
							nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);
							nlapiLogExecution('DEBUG', 'poLocn', poLocn);
							nlapiLogExecution('DEBUG', 'locgroupid', locgroupid);

							try {
								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
								columns[0].setSort();
								columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
								columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
								columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
								columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
								columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
								columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
								columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

								var filters = new Array();
								if(Mixsku!='T')
								{
									filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
								}

								if(poLocn!=null && poLocn!='')
									filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [poLocn]));
								filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'is', locgroupid));
								filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));                                        

								t1 = new Date();
								locResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
								t2 = new Date();
								nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:createinv',
										getElapsedTimeDuration(t1, t2));
							} //try close.
							catch (Error) {
								nlapiLogExecution('DEBUG', 'Into Error', Error);
							}
							try {
								nlapiLogExecution('DEBUG', 'Inside Try', 'TRY');

								if (locResults != null && locResults != '') {
									nlapiLogExecution('ERROR', 'LocationResults', locResults.length );
									for (var j = 0; j < locResults.length; j++) {
										nlapiLogExecution('DEBUG', 'locResults[j].getId()', locResults[j].getId());
										Location = locResults[j].getText('custrecord_ebiz_inv_binloc');
										var locationInternalId = locResults[j].getValue('custrecord_ebiz_inv_binloc');

										nlapiLogExecution('ERROR', 'Location', Location );
										nlapiLogExecution('ERROR', 'Location Id', locationInternalId);

										//LocRemCube = GeteLocCube(locationInternalId);
										LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
										OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
										IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

										nlapiLogExecution('DEBUG', 'Loc Rem Cube', LocRemCube);
										nlapiLogExecution('DEBUG', 'Item Cube', ItemCube);

										if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

											RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

											location_found = true;

											LocArry[0] = Location;
											LocArry[1] = RemCube;
											LocArry[2] = locationInternalId;		//locResults[j].getId();
											LocArry[3] = OBLocGroup;
											LocArry[4] = PickSeqNo;
											LocArry[5] = IBLocGroup;
											LocArry[6] = PutSeqNo;
											LocArry[7] = LocRemCube;
											LocArry[8] = zoneid;
											LocArry[9] = PutMethodName;
											LocArry[10] = PutRuleId;

											nlapiLogExecution('DEBUG', 'after changes Location Cube', RemCube);
											return LocArry;
										}
									}
								}
							} 
							catch (exps) {
								nlapiLogExecution('DEBUG', 'included exps', exps);
							}

							//Added by satish.N on 04-JAN-2012 to consider open taks while merging.
							if (location_found == false) {

								try{
									nlapiLogExecution('DEBUG', 'Inside  consider open taks while merging');

									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
									columns[0].setSort();
									columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
									columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
									columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_actbeginloc');
									columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
									columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
									columns[6] = new nlobjSearchColumn('custrecord_sku');
									columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_actbeginloc');

									var filters = new Array();
									if(Mixsku!='T')
									{
										filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', Item));
									}	
									if(poLocn!=null && poLocn!='')
										filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));
									filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
									filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'is', locgroupid));
									filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_actbeginloc', 'greaterthanorequalto', ItemCube));

									locResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

									if (locResults != null && locResults != '') {
										for (var j = 0; j < locResults.length; j++) {

											Location = locResults[j].getText('custrecord_actbeginloc');
											var locationInternalId = locResults[j].getValue('custrecord_actbeginloc');

											nlapiLogExecution('ERROR', 'Location', Location );
											nlapiLogExecution('ERROR', 'Location Id', locationInternalId);

											LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
											OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
											PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
											IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
											PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

											nlapiLogExecution('DEBUG', 'Loc Rem Cube', LocRemCube);
											nlapiLogExecution('DEBUG', 'Item Cube', ItemCube);

											if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

												RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

												location_found = true;

												LocArry[0] = Location;
												LocArry[1] = RemCube;
												LocArry[2] = locationInternalId;		//locResults[j].getId();
												LocArry[3] = OBLocGroup;
												LocArry[4] = PickSeqNo;
												LocArry[5] = IBLocGroup;
												LocArry[6] = PutSeqNo;
												LocArry[7] = LocRemCube;
												LocArry[8] = zoneid;
												LocArry[9] = PutMethodName;
												LocArry[10] = PutRuleId;

												nlapiLogExecution('DEBUG', 'after changes Location Cube', RemCube);
												return LocArry;
											}
										}										
									}
								}
								catch(exp){
									nlapiLogExecution('DEBUG', 'included exps in opentasks', exps);
								}
							}
						}
						if (location_found == false) {

							//code modified by suman on 27/01/12
							//Instead of searching each and individual binloc in inventory and open task record it seems to have a goverence issue.
							//So I bought all binloc from open task and inventory records depending upon "locgr"Id and placing it in an array so that on comparing this array with the binlocation 
							//will decides weather it is empty or not.

							nlapiLogExecution('DEBUG', 'Location Not found for Merge Location for Location Group', locgroupid);
							var locResults = GetLocation(locgroupid,ItemCube);

							if (locResults != null && locResults != '') {

								var TotalListOfBinLoc=new Array();
								for ( var x = 0; x < locResults.length; x++) 
								{
									TotalListOfBinLoc[x]=locResults[x].getId();

								}
								var emptyloccheck =	binLocationChecknew(TotalListOfBinLoc);
								nlapiLogExecution('ERROR', 'emptyloccheck', emptyloccheck);

								nlapiLogExecution('DEBUG', 'locResults.length', locResults.length);
								for (var j = 0; j < Math.min(300, locResults.length); j++) {
									var LocFlag='N';
									var emptyBinLocationId = locResults[j].getId();
									nlapiLogExecution('ERROR', 'emptyBinLocationId', emptyBinLocationId);
//									var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//									nlapiLogExecution('DEBUG', 'emptyloccheck', emptyloccheck);

									for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
									{
//										nlapiLogExecution('ERROR', 'emptyloccheck[0][InvBinLocCount]', emptyloccheck[0][InvBinLocCount]);

										if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
										{
											nlapiLogExecution('ERROR', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
											LocFlag='Y';
											break;
										}
									}
									for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
									{
										if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
										{
											nlapiLogExecution('ERROR', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
											LocFlag='Y';
											break;
										}
									}
//									end of code changed 27/01/12 
									nlapiLogExecution('DEBUG', 'LocFlag', LocFlag);
									nlapiLogExecution('DEBUG', 'Main Loop ', s);
									nlapiLogExecution('ERROR','Remaining usage ',context.getRemainingUsage());
									if(LocFlag == 'N'){
										//Changes done by Sarita (index i is changed to j).
										LocRemCube = locResults[j].getValue('custrecord_remainingcube');
										OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid');
										PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno');
										IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid');
										PutSeqNo = locResults[j].getValue('custrecord_startingputseqno');

										nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
										nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);

										if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
											RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
											location_found = true;
											Location = locResults[j].getValue('name');
											LocArry[0] = Location;
											LocArry[1] = RemCube;
											LocArry[2] = locResults[j].getId();
											LocArry[3] = OBLocGroup;
											LocArry[4] = PickSeqNo;
											LocArry[5] = IBLocGroup;
											LocArry[6] = PutSeqNo;
											LocArry[7] = LocRemCube;
											LocArry[8] = zoneid;
											LocArry[9] = PutMethodName;
											LocArry[10] = PutRuleId;

											nlapiLogExecution('DEBUG', 'Location', Location);
											nlapiLogExecution('DEBUG', 'Location Cube', RemCube);
											return LocArry;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			nlapiLogExecution('DEBUG', 'Main Loop ', s);
			nlapiLogExecution('ERROR','Remaining usage ',context.getRemainingUsage());
			//}
		}
	}// end of put rule for loop
	else
	{
		nlapiLogExecution('DEBUG', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	//  } // end of main if
	return LocArry;
}

var tempOTResultsArray=new Array();
function fnGetSearchresults(getPOid,getTrailer,getReceipt,maxno)
{
	// define search filters
	var filters = new Array();
	if(getPOid != null && getPOid != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', getPOid));
	if(getTrailer != null && getTrailer != "")
	{
		nlapiLogExecution('ERROR', 'getTrailer', getTrailer);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', getTrailer));
	}	
	if(getReceipt != null && getReceipt != "")
	{
		nlapiLogExecution('ERROR', 'getReceipt', getReceipt);
		filters.push(new nlobjSearchFilter('custrecord_ot_receipt_no', null, 'is', getReceipt));
	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2,6]));
	//commented on 10/01/12 by suman 
	//as to fetch the records frm open task whose actbinloc are 'null' and with some 'binloc'.
//	filters.push(new nlobjSearchFilter('custrecord_actbeginloc',null,'is','@NONE@'));
	if(maxno!=-1)
	{

		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}
	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	columns.push(new nlobjSearchColumn('custrecord_line_no'));
	columns.push(new nlobjSearchColumn('custrecord_sku'));
	columns.push(new nlobjSearchColumn('custrecord_sku_status'));
	columns.push(new nlobjSearchColumn('custrecord_tasktype'));
	columns.push(new nlobjSearchColumn('custrecord_uom_id'));
	columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_act_qty'));
	columns.push(new nlobjSearchColumn('custrecord_lpno'));
	columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	columns.push(new nlobjSearchColumn('custrecord_batch_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	columns.push(new nlobjSearchColumn('custrecord_transport_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ot_receipt_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_trailer_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
	columns.push(new nlobjSearchColumn('custrecord_wms_location'));
	columns[0].setSort();
	columns[2].setSort();

	// execute the  search, passing null filter and return columns
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	if(searchresults!=null)
	{
		if(searchresults.length>=1000)
		{
			var maxno1=searchresults[searchresults.length-1].getValue(columns[0]);
			tempOTResultsArray.push(searchresults);
			fnGetSearchresults(getPOid,getTrailer,getReceipt,maxno1);

		}
		else
		{
			tempOTResultsArray.push(searchresults);
		}
	}
	return tempOTResultsArray;
	//return searchresults;
}

function setPagingForSublist(orderList,form,getPoValue,receiptno,trailerno,getPOid,validate,arrayLP,request)
{
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			//nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				//nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		//nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';
//		form.setScript('customscript_inventoryclientvalidations');
		if(orderListArray.length>0)
		{

			if(orderListArray.length>100)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records(# of Lines)');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("100");
					pagesizevalue=100;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue=100;
						pagesize.setDefaultValue("100");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}

			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				//nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				//nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{

					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						//nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						//nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						//nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}

			var c=1;
			var minvalue;
			minvalue=minval;
			var prevVal=null;var currVal=null;
			//nlapiLogExecution('ERROR', 'minvalue ', minvalue);  
			//nlapiLogExecution('ERROR', 'maxval ', maxval); 
			for(var j = minvalue; j < maxval; j++){		

				var currentOrder = orderListArray[j];

				addPOOrderToSublist(form, currentOrder, c,getPoValue,receiptno,trailerno,getPOid,validate,arrayLP,request);				
				c=c+1;
			}
		}
	}
}

/**
 * Function to add one fulfillment order to the sublist on the form
 * 
 * @param form
 * @param currentOrder
 * @param i
 */
function addPOOrderToSublist(form, searchresult, i,getPoValue,receiptno,trailerno,getPOid,validate,arrayLP,request){
	var po_asn, line, itemname, itemstatus, taskstatus, uom, qty, lp, loc, seriallot, qcitemid, 
	getopentaskId,cartlp,receiptno,trailerno,whsite,itemintid;
	po_asn = searchresult.getValue('custrecord_ebiz_cntrl_no');
	//nlapiLogExecution('ERROR', 'po_asn ', po_asn);
	//var getTrailer = request.getParameter('custparam_trailer');
	var getTrailer =trailerno;
	//nlapiLogExecution('ERROR', 'getTrailer', getTrailer);
	var getReceipt = request.getParameter('custparam_receipt');
	//nlapiLogExecution('ERROR', 'getReceipt', getReceipt);
	if(po_asn != null && po_asn != "")
	{
		if((getTrailer != null && getTrailer != "") || (getReceipt != null && getReceipt != ""))
		{
			getPOid = searchresult.getValue('custrecord_ebiz_cntrl_no');
			getPoValue= searchresult.getText('custrecord_ebiz_order_no');
			//nlapiLogExecution('ERROR', 'getPoValue', getPoValue);
		}
	}

	line = searchresult.getValue('custrecord_line_no');
	itemname = searchresult.getText('custrecord_sku');
	itemstatus = searchresult.getValue('custrecord_sku_status');
	uom = searchresult.getValue('custrecord_uom_id');
	qty = searchresult.getValue('custrecord_expe_qty');
	lp = searchresult.getValue('custrecord_lpno');
	loc = searchresult.getValue('custrecord_actbeginloc');
	seriallot = searchresult.getValue('custrecord_batch_no');
	qcitemid = searchresult.getValue('custrecord_ebiz_sku_no');
	getopentaskId = searchresult.getId();
	cartlp = searchresult.getValue('custrecord_transport_lp');
	receiptno=searchresult.getValue('custrecord_ot_receipt_no');
	trailerno=searchresult.getValue('custrecord_ebiz_trailer_no');
	whsite=searchresult.getValue('custrecord_wms_location');
	itemintid = searchresult.getValue('custrecord_sku');
	
	//nlapiLogExecution('ERROR', 'itemintid', itemintid);
	var serialInflg = "F";
	var batchflg = "F";
	var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
	var columns = nlapiLookupField('item', itemintid, fields);
	var ItemType = columns.recordType;
	serialInflg = columns.custitem_ebizserialin;
	batchflg = columns.custitem_ebizbatchlot;
	
	

	form.getSubList('custpage_items').setLineItemValue('custpage_po_asn', i, getPoValue);
	form.getSubList('custpage_items').setLineItemValue('custpage_receipt', i, receiptno);
	form.getSubList('custpage_items').setLineItemValue('custpage_trailerno', i, trailerno);
	form.getSubList('custpage_items').setLineItemValue('custpage_line', i, line);
	form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i, itemname);
	form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i, itemstatus);
	form.getSubList('custpage_items').setLineItemValue('custpage_uom', i, uom);
	form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i, qty);
	form.getSubList('custpage_items').setLineItemValue('custpage_lp', i, lp);
	form.getSubList('custpage_items').setLineItemValue('custpage_location', i, loc);
	form.getSubList('custpage_items').setLineItemValue('custpage_cartlp', i, cartlp);            
	form.getSubList('custpage_items').setLineItemValue('custpage_poid', i, getPOid);
	//form.getSubList('custpage_items').setLineItemValue('custpage_seriallot', i, seriallot);
	form.getSubList('custpage_items').setLineItemValue('custpage_qcitemid', i, qcitemid);
	form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i, getopentaskId);
	form.getSubList('custpage_items').setLineItemValue('custpage_whsite', i, whsite);
	if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
		retserialcsv = getSerialNoCSV(arrayLP, lp);
		form.getSubList('custpage_items').setLineItemValue('custpage_seriallot', i, retserialcsv);
	}
	else if (ItemType == "lotnumberedinventoryitem"||ItemType =="lotnumberedassemblyitem" || batchflg == "T") {
		form.getSubList('custpage_items').setLineItemValue('custpage_seriallot', i, seriallot);
	}
	
	if(validate=="F")
		form.getSubList('custpage_items').setLineItemValue('custpage_postatus', i, "<b><font color='red'>Closed/Cancelled</font></b>");

	//nlapiLogExecution('ERROR', 'POInternalId in Request Object', getPOid);
	//index=index+1;	
}




function getSerialNoCSV(arrayLP, lp){
	var csv = "";
	for (var i = 0; i < arrayLP.length; i++) {
		if (arrayLP[i][0] == lp) {
			csv += arrayLP[i][1] + " , ";
		}
	}
	return csv;
}


function TrnLineUpdationNew(orderType, ttype, poValue, poid, lineno, ItemId, quan, confqty,chkAssignputW,ItemStatus){
	nlapiLogExecution('ERROR','orderType',orderType);
	nlapiLogExecution('ERROR','ttype',ttype);
	nlapiLogExecution('ERROR','poValue',poValue);
	nlapiLogExecution('ERROR','poid',poid);
	nlapiLogExecution('ERROR','lineno',lineno);
	nlapiLogExecution('ERROR','ItemId',ItemId);
	nlapiLogExecution('ERROR','quan',quan);
	nlapiLogExecution('ERROR','confqty',confqty);
	nlapiLogExecution('ERROR','chkAssignputW',chkAssignputW);
	//nlapiLogExecution('ERROR','ItemStatus',ItemStatus);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poid));
	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));

	// get filter based on order type
	if(orderType!='transferorder')
	{
		filters.push(getFilterForOrderType(orderType));
	}


	nlapiLogExecution('ERROR', 'filters[0]', poid);
	nlapiLogExecution('ERROR', 'filters[1]', lineno);


	//nlapiLogExecution('ERROR', 'ItemStatus status', ItemStatus);

	var columns = new Array();
	
		columns.push(getColumnForTransactionType(ttype));
	//columns.push(getColumnForTransactionType("ASPW"));

	var itemstatus = 'GOOD';//itemRec.getFieldValue('customrecord_ebiznet_sku_status');
	var uomid = 'EACH';

	var t1 = new Date();
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);
	nlapiLogExecution('ERROR','chkpt','sucess');
	nlapiLogExecution('ERROR','searchResults',searchResults);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSearchRecord:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t1, t2));

	if(searchResults != null){
		var trnId = searchResults[0].getId();
		var lastConfQty = 0;// SearchRecd[0].getValue('custrecord_orderlinedetails_checkin_qty');

		var fldArray = new Array();
		fldArray[0] = 'custrecord_orderlinedetails_ebiz_ord_no';
		fldArray[1] = 'custrecord_orderlinedetails_orderline_no';
		fldArray[2] = 'custrecord_orderlinedetails_record_date';
		fldArray[3] = 'custrecord_orderlinedetails_record_time';
		fldArray[4] = getFieldArrayValueForTransType(ttype);
		lastConfQty = searchResults[0].getValue(fldArray[4]);
		if(lastConfQty == null || lastConfQty == "")
			lastConfQty = 0;

		var valArray = new Array();
		valArray[0] = poid;
		valArray[1] = lineno;
		valArray[2] = DateStamp();
		valArray[3] = TimeStamp();
		valArray[4] = parseFloat(lastConfQty) + parseFloat(confqty);

		if(chkAssignputW=="Y")
		{
			ttype="ASPW";
			fldArray[5] = getFieldArrayValueForTransType(ttype);
			nlapiLogExecution("ERROR", "fldArray[5]", fldArray[5]);
			//lastConfQty = searchResults[0].getValue("'"+fldArray[5]+"'");
			lastConfQty = searchResults[0].getValue(fldArray[5]);
			nlapiLogExecution("ERROR", "searchResults[0].getValue(fldArray[5]) ", searchResults[0].getValue(fldArray[5]));
			if(lastConfQty == null || lastConfQty == "")
				lastConfQty = 0;
			valArray[5] = parseFloat(lastConfQty) + parseFloat(confqty);

		}

		nlapiLogExecution("ERROR", "TrnLineUpdation:Submitted Quantity valArray[4]", valArray[4]);//chkin Qty
		nlapiLogExecution("ERROR", "TrnLineUpdation:Submitted Quantity valArray[5]", valArray[5]);//PutGen Qty

		var t3 = new Date();
		nlapiSubmitField('customrecord_ebiznet_order_line_details', searchResults[0].getId(), fldArray, valArray);
		var t4 = new Date();
		nlapiLogExecution('ERROR', 'nlapiSubmitField:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t3, t4));
	}else{/*
		var t5 = new Date();
		parent.selectNewLineItem('recmachcustrecord_ebiz_toline_parent');
		var t6 = new Date();
		nlapiLogExecution('ERROR', 'nlapiCreateRecord:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t5, t6));
		nlapiLogExecution('ERROR', 'Item Status', itemstatus);
		nlapiLogExecution('ERROR', 'UOM ID', uomid);
		nlapiLogExecution('ERROR', 'PO Value', poValue);
		if(poValue!=''&& poValue!=null)
		{
			nlapiLogExecution('ERROR', 'PO', poValue);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','name', poValue);		
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_order_no', poValue);
		}

		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_ebiz_ord_no', parseFloat(poid));
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_orderline_no', lineno);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_item', ItemId);
//		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_item_status', 1);
		nlapiLogExecution('ERROR', 'Item Status', ItemStatus);
		if(ItemStatus!=null && ItemStatus!="")
			parent.setCurrentLineItemText('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_item_status', ItemStatus);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_uom_id', uomid);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_ebiz_sku_no', ItemId);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_order_qty', parseFloat(quan).toFixed(5));
		// set order category depending on the order type
		setOrderLineDetailsOrderCategoryNew(parent, orderType);
		setOrderLineDetailsOrderQuantityNew(parent, ttype, confqty);
		if(chkAssignputW=="Y")
		{
			setOrderLineDetailsOrderQuantityNew(parent, "ASPW", confqty);
		}

		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_record_date', DateStamp());
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_record_time', TimeStamp());

		t5 = new Date();
		//var tranRecId = nlapiSubmitRecord(createTranRec);
		parent.commitLineItem('recmachcustrecord_ebiz_toline_parent'); 
		t6 = new Date();
		nlapiLogExecution('ERROR', 'nlapiSubmitRecord:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t5, t6));
	*/}
	return true;
}
