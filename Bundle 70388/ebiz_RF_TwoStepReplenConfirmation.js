/***************************************************************************
 eBizNET Solutions Inc  
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplenConfirmation.js,v $
 *     	   $Revision: 1.1.4.3 $
 *     	   $Date: 2015/04/30 15:20:00 $
 *     	   $Author: grao $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_79 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplenConfirmation.js,v $
 * Revision 1.1.4.3  2015/04/30 15:20:00  grao
 * SB issue fixes  201412557
 *
 * Revision 1.1.4.2  2015/04/13 16:14:07  snimmakayala
 * Case#:201411349
 * Two Step Replenishment
 *
 * Revision 1.1.2.4  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.3  2015/02/12 07:02:42  spendyala
 * 201411472
 *
 * Revision 1.1.2.2  2015/01/30 07:11:21  spendyala
 * case # 201411481
 *
 * Revision 1.1.2.1  2015/01/02 15:05:00  skreddy
 * Case# 201411349
 * Two step replen process
 *
 *
 *****************************************************************************/

function TwoStepReplenConfirmation(request, response){
	if (request.getMethod() == 'GET') {
		var getCartLP;

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		getCartLP = request.getParameter('custparam_cartlpno');
		toLocationId = request.getParameter('custparam_tolocation');
		var vzone = request.getParameter('custparam_zoneno');
		nlapiLogExecution('DEBUG', 'getCartLP', getCartLP);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		// 20126048

		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "";
			st1 = "LP Carro";
			st2 = "ENTRAR / ESCANEAR N&#218;MERO DE CARRO";	
			st3 = "N&#250;mero de placa";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";

		}
		else
		{
			st0 = "";
			st1 = "TO LOCATION";
			st2 = "ENTER/SCAN TO LOCATION";
			//st3 = "LICENSE PLATE NUMBER";
			st4 = "SEND";
			st5 = "PREV";

		}

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['8']);
		filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['24']);
		filters[2] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLP);
		if(toLocationId!=null && toLocationId!='')
		filters[3] = new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', toLocationId);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actendloc','group');
		columns[1] = new nlobjSearchColumn('custrecord_sku',null,'group');
		columns[2] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
		columns.push(new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
		//columns.push(new nlobjSearchColumn('custrecord_expe_qty',null,'sum'));
		columns.push(new nlobjSearchColumn('custrecord_act_qty',null,'sum'));
		columns.push(new nlobjSearchColumn('salesdescription','custrecord_sku','group'));

		columns[3].setSort(true);
		//columns[0].setSort();

		var item='';
		var itemDescription='';
		var Itemdescription='';
		var qty ='';
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		nlapiLogExecution('DEBUG', 'searchresults', searchresults);       
        var toLocation = "";
        var skuNo ='';
		if(searchresults != null && searchresults != '')
		{
			item	=  searchresults[0].getText('custrecord_sku',null,'group');
			itemDescription= searchresults[0].getValue('salesdescription','custrecord_sku','group');
			//qty= searchresults[0].getValue('custrecord_expe_qty',null,'sum');
			qty= searchresults[0].getValue('custrecord_act_qty',null,'sum');
			toLocation = searchresults[0].getText('custrecord_actendloc',null,'group');
			skuNo	=  searchresults[0].getValue('custrecord_sku',null,'group');
			
			nlapiLogExecution('ERROR', 'skuNo', skuNo);
			
			if(skuNo !=null && skuNo !='')
			{
				var fields=new Array();
				fields[0]='name';
				fields[1]='custitem_ebizdescriptionitems';
				fields[2]='description';

				var rec=nlapiLookupField('item', skuNo, fields);

				if(rec != null && rec != '')
				{
					sku=rec.name;
					Itemdescription=rec.custitem_ebizdescriptionitems;
					nlapiLogExecution('ERROR', 'custitem_ebizdescriptionitems', Itemdescription);
					if(Itemdescription==null || Itemdescription=='')
					{
						Itemdescription=rec.description;
						Itemdescription=Itemdescription.substring(0, 20)
					}
				}
			}
			nlapiLogExecution('ERROR', 'Itemdescription', Itemdescription);
		}

		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entercartlp').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>"; 
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> ";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> TO LOCATION : "+ toLocation +"</td></tr>";
		html = html + "				<input type='hidden' name='hdntolocid' value=" + toLocationId + ">";
		html = html + "			<tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> ITEM : "+ item +"</td></tr>";
		html = html + "				<input type='hidden' name='hdntolocid' value=" + toLocationId + ">";
		html = html + "			<tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> ITEM DESCRIPTION : "+ Itemdescription +"</td></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> QTY : "+ qty +"</td></tr>";
		html = html + "			<tr>";

		html = html + "				<input type='hidden' name='hdncartlp' value=" + getCartLP + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' id='hdnvzone' value="+vzone+" >";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4  +" <input name='cmdSend' type='submit' id='cmdSend' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSkip.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' id='cmdPrevious' value='F7' />";
		//html = html + "					SKIP <input name='cmdSkip' type='submit' id='cmdSkip' value='F8' />";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercartlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
		var getCartLPNo = request.getParameter('hdncartlp');
		// This variable is to hold the LP entered.
		var getTolocation = request.getParameter('hdntolocation');
		var toLocationId= request.getParameter('hdntolocid');
		nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);
		nlapiLogExecution('DEBUG', 'getTolocation', getTolocation);
		nlapiLogExecution('DEBUG', 'toLocationId', toLocationId);
		var vZoneId=request.getParameter('hdnvzone');
		nlapiLogExecution('DEBUG', 'vZoneId', vZoneId);


		var POarray = new Array();


		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		if(optedEvent==null || optedEvent== "")
			optedEvent = request.getParameter('cmdSend');

		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);

		POarray["custparam_cartlpno"] =getCartLPNo;
		var getLanguage = request.getParameter('hdngetLanguage');
		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;
		POarray["custparam_screenno"] = '2Stepcnfrepln';
		POarray["custparam_language"] = getLanguage;
		POarray["custparam_zoneno"] = vZoneId;
		POarray["custparam_tolocation"] = toLocationId;

		var st4,st5,st6;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st4 = "ESTE LP CART NO EXISTE";
			st5 = "NO V&#193;LIDO LP CART";
			st6 = "Introduzca CARRO LP";

		}
		else
		{

			st4 = "THIS CART LP DOESN'T EXISTS";
			st5 = "INVALID CART LP";
			st6 = "Please Enter CART LP";//Case# 20148710

		}

		POarray["custparam_error"] = st5;
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_scanloc', 'customdeploy_ebiz_rf_twostp_scanloc', false, POarray);
			return;
		}
		else {

			try {

				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['8']);
				filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['24']);
				filters[2] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);
				if(toLocationId!=null && toLocationId!='')
				filters[3] = new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', toLocationId);
				if(vZoneId!=null && vZoneId!='')
				{
					nlapiLogExecution('ERROR','vZoneId',vZoneId);
					filters[4]=new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId);
					
				}
				var columns = new Array();	
				columns.push(new nlobjSearchColumn('custrecord_actendloc'));
				columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
				columns.push(new nlobjSearchColumn('custrecord_sku'));
				
				//columns[3].setSort(true);
				//columns[0].setSort();

				var item='';
				var itemDescription='';
				var qty ='';
				var replenSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
				nlapiLogExecution('DEBUG', 'replenSearchResults', replenSearchResults);    

				if(replenSearchResults != null && replenSearchResults !='')
				{
					nlapiLogExecution('DEBUG', 'replenSearchResults', replenSearchResults.length);
					for(var j=0;j<replenSearchResults.length;j++)
					{
						nlapiLogExecution('DEBUG', 'replenSearchResults[j].getId()', replenSearchResults[j].getId());
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask',replenSearchResults[j].getId());								
						transaction.setFieldValue('custrecord_actendloc', toLocationId);
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
						transaction.setFieldValue('custrecord_tasktype', 8);
						transaction.setFieldValue('custrecord_wms_status_flag', 19);
						//transaction.setFieldValue('custrecord_act_qty', qty);
						
						
						var varsku=transaction.getFieldValue('custrecord_ebiz_sku_no');
						var ebizTaskno=transaction.getFieldValue('custrecord_ebiz_task_no');
						var invtLpno=transaction.getFieldValue('custrecord_lpno');
						var whLocation=transaction.getFieldValue('custrecord_wms_location');
						var actendloc=transaction.getFieldValue('custrecord_actendloc');
						var beginlocation = transaction.getFieldValue('custrecord_actbeginloc');
						var fromLPno=transaction.getFieldValue('custrecord_from_lp_no');
						var itemstatus=transaction.getFieldValue('custrecord_sku_status');
						var fromInvRef=transaction.getFieldValue('custrecord_invref_no');
						var expectedqty =transaction.getFieldValue('custrecord_expe_qty');
						var qty = transaction.getFieldValue('custrecord_act_qty');
						var dorefno = transaction.getFieldValue('custrecord_ebiz_cntrl_no');
						var vContLpNo = transaction.getFieldValue('custrecord_container_lp_no');
						var vBatchno = transaction.getFieldValue('custrecord_batch_no');
						var TotalWeight=0;
						var accountNumber = getAccountNumber(whLocation);
                        var remainingreplenqty = '';
						var stageLocation=GetStageLocation(varsku, "", whLocation, "", "BOTH");
						nlapiLogExecution('Debug', 'stageLocation', stageLocation);
						nlapiLogExecution('Debug', 'expectedqty', expectedqty);
						nlapiLogExecution('Debug', 'actualQty', qty);
						nlapiLogExecution('Debug', 'vBatchno', vBatchno);
						/*if(parseFloat(expectedqty) != parseFloat(actualQty))
						{
							qty = parseFloat(expectedqty) - parseFloat(actualQty);							
							var Systemrules = SystemRuleForStockAdjustment(whLocation);						
							nlapiLogExecution('ERROR', 'Systemrules', Systemrules);
							if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
								Systemrules=null;							
							CreateNewShortPick(replenSearchResults[j].getId(),expectedqty,actualQty,dorefno,j,vContLpNo,TotalWeight,vBatchno,qty,Systemrules,request);//have to check for wt and cube calculation
						}
						else
						{
							qty = parseFloat(expectedqty);
						}
						*/
						var vBatchId = GetBatchId(varsku,vBatchno);
						nlapiLogExecution('Debug', 'vBatchId', vBatchId);
						if(stageLocation == -1){

							if(fromInvRef == null || fromInvRef == '')
							{
								var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno,itemstatus,'BULK');

								nlapiLogExecution('Debug','bulkLocationInventoryResults', bulkLocationInventoryResults);
								if(bulkLocationInventoryResults != null && bulkLocationInventoryResults != '')
									//fromInvRef=bulkLocationInventoryResults[0].getid();
									fromInvRef=bulkLocationInventoryResults[0].getId();
							}

							if(fromInvRef != null && fromInvRef != '')
								updateBulkLocation(fromInvRef, varsku, beginlocation, qty, itemstatus,expectedqty,whLocation,remainingreplenqty);

							var invtresults=getRecord(varsku, actendloc,invtLpno,itemstatus,'PF',vBatchId);
							var vGetFifoDate="";
							if(fromInvRef!=null && fromInvRef != '')
								vGetFifoDate=GetFiFoDate(fromInvRef);
							else
								vGetFifoDate=DateStamp();
							updateOrCreatePickfaceInventory(invtresults, varsku, qty, invtLpno, actendloc, 
									itemstatus, accountNumber,vBatchId,whLocation,varsku);
							nlapiSubmitRecord(transaction, true);
						}
						else
						{

							if(fromInvRef == null || fromInvRef == '')
							{
								var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno,itemstatus,'BULK');
								if(bulkLocationInventoryResults != null && bulkLocationInventoryResults != '')
									fromInvRef=bulkLocationInventoryResults[0].getId();
							}

							if(fromInvRef != null && fromInvRef != '')
								updateBulkLocation(fromInvRef, varsku, beginlocation, qty, itemstatus,expectedqty,whLocation,remainingreplenqty);

							var vGetFifoDate="";
							if(fromInvRef!=null && fromInvRef != '')
								//vGetFifoDate=GetFiFoDate(fromInvRef);
								vGetFifoDate=GetFiFoDate(fromInvRef);
							else
								vGetFifoDate=DateStamp();

								var invRefNo=transaction.getFieldValue('custrecord_invref_no');                           
							var result = nlapiSubmitRecord(transaction);


							//var invtresults=getRecord(varsku, endloc,invtLpno,itemstatus,'PF',vBatchId);
							//var invtresults=getRecord(varsku, beginlocation,invtLpno,itemstatus,'PF',vBatchId);
							var invtresults=getRecord(varsku, actendloc,invtLpno,itemstatus,'PF',vBatchId);

							nlapiLogExecution('Debug','invtresults', invtresults);
							nlapiLogExecution('Debug','invRefNo1', invRefNo);

							if(invRefNo != null && invRefNo != "")
							{

								updateOrCreatePickfaceInventory(invtresults, varsku, qty, invtLpno, actendloc, 
										itemstatus, accountNumber,vBatchId,whLocation,varsku,vGetFifoDate);
							//	nlapiLogExecution('Debug', 'beginLocation', beginLocation);
								var LocationType = nlapiLookupField('customrecord_ebiznet_location', beginlocation, ['custrecord_ebizlocationtype']);

								if (LocationType.custrecord_ebizlocationtype == '8' )
								{
									DeleteInventoryRecord(invRefNo);
								}
							}
						}
						
						
						
					}

				}
				
				var opentaskResults = getOpenTasksCount(getCartLPNo);
				
				  if(opentaskResults != null  && opentaskResults!='' && opentaskResults.length > 0)
					  {
					  nlapiLogExecution('Debug', 'opentaskResults.length',opentaskResults.length);
					  	POarray["custparam_cartlpno"] =getCartLPNo;
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_scanloc', 'customdeploy_ebiz_rf_twostp_scanloc', false, POarray);						
						return;
					  }
				  else
				  {
					  var filtersmlp = new Array(); 
					  filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', getCartLPNo);

					  var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

					  if (SrchRecord != null && SrchRecord.length > 0) {

						  nlapiSubmitField('customrecord_ebiznet_master_lp', SrchRecord[0].getId(), 'custrecord_ebiz_cart_closeflag', 'F'); //	2 UNITS
					  }
					  else {
							nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

						}
					  response.sendRedirect('SUITELET', 'customscript_ebiz_twostep_putawaycartlp', 'customdeploy_ebiz_twostep_putawaycartlp', false, POarray);
						return;
					  }



			} 
			catch (e) {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'In error catch block ', e);
			}

		}
	}
}
function getAccountNumber(whLocation){
	var filtersAccount = new Array();
	filtersAccount[0] = new nlobjSearchFilter('custrecord_location', null, 'is', whLocation);

	var columnsAccount = new Array();
	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);              

	if (accountSearchResults != null){
		var accountNumber = accountSearchResults[0].getValue('custrecord_accountno');                                
		nlapiLogExecution('Debug', 'Account #',accountNumber);
	}

	return accountNumber;
}
function getRecord(itemid,location, invtLPNo,itemstatus,loctype,vBatchno)
{
	nlapiLogExecution('Debug', 'Into getRecord');
	var str = 'itemid. = ' + itemid + '<br>';
	str = str + 'location. = ' + location + '<br>';	
	str = str + 'invtLPNo. = ' + invtLPNo + '<br>';	
	str = str + 'itemstatus. = ' + itemstatus + '<br>';	
	str = str + 'loctype. = ' + loctype + '<br>';	
	str = str + 'vBatchno. = ' + vBatchno + '<br>';	
	
	nlapiLogExecution('Debug', 'Parameter Values', str);

	var filtersso = new Array();
	var socount=0;		

	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', [itemid]));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));
	filtersso.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
	if((loctype == 'BULK') && (invtLPNo!=null && invtLPNo!=''))
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', invtLPNo));
	if(itemstatus!=null && itemstatus!='')
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', itemstatus));
	
	if(vBatchno!=null && vBatchno!='')
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', vBatchno));
	
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsLine != null)
	{
		nlapiLogExecution('Debug', 'Inventory count', searchresultsLine.length);
		socount=searchresultsLine.length; 
	} 
	else
	{ 
		nlapiLogExecution('Debug', 'inside else if results a', 'null');
	}       
	return searchresultsLine;
}
function updateBulkLocation(bulkLocationInventoryRecId, itemID, beginLocationId, actQty, itemStatus,expectedqty,
		whLocation,remainingreplenqty)
{
	nlapiLogExecution('Debug', "Into updateBulkLocation");		
	var str = 'expectedqty.' + expectedqty + '<br>';
	str = str + 'actQty.' + actQty + '<br>';
	str = str + 'bulkLocationInventoryRecId.' + bulkLocationInventoryRecId + '<br>';	
	str = str + 'remainingreplenqty.' + remainingreplenqty + '<br>';	

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	if(bulkLocationInventoryRecId != null)
	{	

		if (expectedqty == null || isNaN(expectedqty) || expectedqty == "") 
		{
			expectedqty = 0;
		}

		if (actQty == null || isNaN(actQty) || actQty == "") 
		{
			actQty = 0;
		}

		var discrepancyqty =0;
		var QOHQty=0;
		var calculatedQOHQty=0;
		var vnotes1="This is from replen qty Exception";
		var tasktype=11; //11 is the internalid value of tasktype "ADJT"; 
		var vAdjustType1=getAdjustmentType(whLocation);

		var scount=1;
		var newqoh=0;
		var vNewAllocQty=0;

		LABL1: for(var i=0;i<scount;i++)
		{	
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', bulkLocationInventoryRecId);
				var inventoryQty = transaction.getFieldValue('custrecord_ebiz_inv_qty');
				var inventoryAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				var inventoryQOHQty= transaction.getFieldValue('custrecord_ebiz_qoh');

				if (inventoryQty == null || isNaN(inventoryQty) || inventoryQty == "") 
				{
					inventoryQty = 0;
				}
				if (inventoryAllocQty == null || isNaN(inventoryAllocQty) || inventoryAllocQty == "") 
				{
					inventoryAllocQty = 0;
				}
				if (inventoryQOHQty == null || isNaN(inventoryQOHQty) || inventoryQOHQty == "") 
				{
					inventoryQOHQty = 0;
				}

				vNewAllocQty = parseInt(inventoryAllocQty)-parseInt(expectedqty);

				if(parseInt(expectedqty) != parseInt(actQty))
				{
					if(remainingreplenqty==null || remainingreplenqty=='null'  || remainingreplenqty=='' || isNaN(remainingreplenqty))
					{
						newqoh = parseInt(inventoryQOHQty) - parseInt(actQty);
					}
					else
					{
						newqoh = parseInt(vNewAllocQty) + parseInt(remainingreplenqty);
					}
				}
				else
				{
					newqoh = parseInt(inventoryQOHQty) - parseInt(actQty);
				}


				calculatedQOHQty= parseInt(inventoryQOHQty)- parseInt(actQty);

				if(remainingreplenqty!=null && remainingreplenqty!='null' && remainingreplenqty!='')
				{
					discrepancyqty = (parseInt(actQty)+parseInt(remainingreplenqty)+parseInt(vNewAllocQty))-parseInt(inventoryQOHQty);
				}


				var str = 'inventoryAllocQty.' + inventoryAllocQty + '<br>';
				str = str + 'vNewAllocQty.' + vNewAllocQty + '<br>';	
				str = str + 'inventoryQOHQty.' + inventoryQOHQty + '<br>';	
				str = str + 'newqoh. ' + newqoh + '<br>';					
				str = str + 'remainingreplenqty. ' + remainingreplenqty + '<br>';	
				str = str + 'discrepancyqty. ' + discrepancyqty + '<br>';	
				str = str + 'calculatedQOHQty. ' + calculatedQOHQty + '<br>';	

				nlapiLogExecution('Debug', 'Qty Details', str);

				if(parseInt(newqoh)<=0)
					newqoh=0;

				if(parseInt(vNewAllocQty)<=0)
					vNewAllocQty=0;

				transaction.setFieldValue('custrecord_ebiz_alloc_qty',vNewAllocQty);
				transaction.setFieldValue('custrecord_ebiz_qoh',newqoh);
				transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
				transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

				nlapiSubmitRecord(transaction);
				nlapiLogExecution('Debug', 'Replen Confirmed for inv ref ',bulkLocationInventoryRecId);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

		/*if(parseInt(expectedqty) != parseInt(actQty))
		{
			if(discrepancyqty != 0)
			{
				var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(itemID,itemStatus,whLocation,parseInt(discrepancyqty),
						vnotes1,tasktype,vAdjustType1,'');
				nlapiLogExecution('Debug', 'netsuiteadjustId', netsuiteadjustId);
			}
		}*/

		nlapiLogExecution('Debug', 'beginLocationId', beginLocationId);
		var retValue =  LocationCubeUpdation(itemID, beginLocationId, actQty, 'P');	

		if(parseInt(newqoh)<=0)
		{
			nlapiLogExecution('Debug', 'Deleting ZERO qty invetory record...',bulkLocationInventoryRecId);
			nlapiDeleteRecord('customrecord_ebiznet_createinv', bulkLocationInventoryRecId);
		}

	}

	nlapiLogExecution('Debug', "Out of updateBulkLocation");		
}
function GetFiFoDate(fromInvRef)
{
	try
	{
		var Fifodate=nlapiLookupField('customrecord_ebiznet_createinv',fromInvRef,'custrecord_ebiz_inv_fifo');
		return Fifodate;
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in GetFiFoDate',exp);	
	}
}
function updateOrCreatePickfaceInventory(invtresults, varsku, expQty, invtLpno, binLocation, 
		itemStatus, accountNumber,batchno,whlocation,skuno,GetFifoDate){

	nlapiLogExecution('Debug', "Into updateOrCreatePickfaceInventory");	

	nlapiLogExecution('Debug', 'Time Stamp at the start of updateOrCreatePickfaceInventory',TimeStampinSec());

	nlapiLogExecution('Debug', "invtresults",invtresults);	
	
	
	
	var columns = nlapiLookupField('item', varsku, [ 'recordType','custitem_ebizserialout' ]);
	var ItemType = columns.recordType;

	if(invtresults != null)
	{
		nlapiLogExecution('Debug', "invtresults length",invtresults.length);	

		nlapiLogExecution('Debug', "Inventory Exists", invtresults[0].getId());	

		var scount=1;
		LABL1: for(var i=0;i<scount;i++)
		{	
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);

			try
			{
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtresults[0].getId());								
				var invtqty = transaction.getFieldValue('custrecord_ebiz_qoh');	
				var pfallocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');

				if (invtqty == null || isNaN(invtqty) || invtqty == "") {
					invtqty = 0;
				}

				if (pfallocqty == null || isNaN(pfallocqty) || pfallocqty == "") {
					pfallocqty = 0;
				}

				invtqty = parseInt(invtqty) + parseInt(expQty);						
				//transaction.setFieldValue('custrecord_ebiz_inv_sku_status',itemStatus);	
				transaction.setFieldValue('custrecord_ebiz_qoh',invtqty);	
				transaction.setFieldValue('custrecord_ebiz_callinv','N');	
				transaction.setFieldValue('custrecord_ebiz_displayfield','N');	

				nlapiLogExecution('Debug', "Quantity Assigned ", invtqty);						
				nlapiSubmitRecord(transaction);	
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
		updateInvRefforOpenPickTasks(varsku,binLocation,invtresults[0].getId(),pfallocqty,invtqty);
	}
	else
	{
		nlapiLogExecution('Debug', "Inventory Not Exists", varsku);					
		var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
		invrecord.setFieldValue('name', varsku+1);					
		invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
		invrecord.setFieldValue('custrecord_ebiz_inv_note1','Replenished');
		invrecord.setFieldValue('custrecord_ebiz_qoh', expQty);
		invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		
		invrecord.setFieldValue('custrecord_ebiz_inv_lp', invtLpno);		
		invrecord.setFieldValue('custrecord_ebiz_inv_binloc', binLocation);	
		invrecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
		invrecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
		invrecord.setFieldValue('custrecord_ebiz_inv_qty', expQty);
		if(accountNumber!=null && accountNumber!='')
			invrecord.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);

		invrecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
		invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
		invrecord.setFieldValue('custrecord_invttasktype', 8);
		invrecord.setFieldValue('custrecord_inv_ebizsku_no', skuno);
		nlapiLogExecution('Debug', "GetFifoDate", GetFifoDate);	
		if(GetFifoDate==null || GetFifoDate=='')
			GetFifoDate=DateStamp();
		invrecord.setFieldValue('custrecord_ebiz_inv_fifo', GetFifoDate);
		if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
		{
			invrecord.setFieldValue('custrecord_ebiz_expdate', GetFifoDate);
		}
	

		if(batchno!=null && batchno!='')
			invrecord.setFieldValue('custrecord_ebiz_inv_lot', batchno);
			//invrecord.setFieldText('custrecord_ebiz_inv_lot', batchno);
		invrecord.setFieldValue('custrecord_ebiz_inv_loc', whlocation);

		var invtrefno = nlapiSubmitRecord(invrecord);

		updateInvRefforOpenPickTasks(varsku,binLocation,invtrefno,0,expQty);
	}

	var retValue =  LocationCubeUpdation(varsku, binLocation, expQty, 'M');

	nlapiLogExecution('Debug', 'Time Stamp at the end of updateOrCreatePickfaceInventory',TimeStampinSec());
	nlapiLogExecution('Debug', 'Out of updateOrCreatePickfaceInventory LocCubeUpdation', 'Success');
}
function DeleteInventoryRecord(invRefNo)
{
	nlapiLogExecution('Debug','Inside DeleteInvtRecCreatedforCHKNTask ','Function');
	var Ifilters = new Array();
	Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
	Ifilters.push(new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [8]));
	Ifilters.push(new nlobjSearchFilter('internalid', null, 'is', invRefNo));

	var invtId = "";
	var invtType = "";
	var searchInventory= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (searchInventory){
		for (var s = 0; s < searchInventory.length; s++) {
			invtId = searchInventory[s].getId();
			invtType= searchInventory[s].getRecordType();
			nlapiDeleteRecord(searchInventory[s].getRecordType(),searchInventory[s].getId());
			nlapiLogExecution('Debug','Inventory record deleted ',invtId);
		}
	}
}

function updateInvRefforOpenPickTasks(itemno,binlocation,invrefno,allocqty,qoh)
{
	nlapiLogExecution('Debug', 'Into  updateInvRefforOpenPickTasks');

	var str = 'itemno. = ' + itemno + '<br>';
	str = str + 'binlocation. = ' + binlocation + '<br>';	
	str = str + 'invrefno. = ' + invrefno + '<br>';
	str = str + 'allocqty. = ' + allocqty + '<br>';
	str = str + 'qoh. = ' + qoh + '<br>';

	var totalpickqty = 0;

	nlapiLogExecution('Debug', 'Parameter Details', str);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', binlocation));
	filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', itemno));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));	 // TASK TYPE - PICK	
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	 // STATUS - PICKS GENERATED
	filters.push(new nlobjSearchFilter('custrecord_invref_no', null, 'isempty'));

	var columns = new Array();

	columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_skiptask'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		for (var s = 0; s < searchresults.length; s++) {

			var taskrecid = searchresults[s].getId();
			var pickqty = searchresults[s].getValue('custrecord_expe_qty');

			if(pickqty==null || pickqty=='' || isNaN(pickqty))
			{
				pickqty=0;
			}

			totalpickqty = parseFloat(totalpickqty)+parseFloat(pickqty);

			if(taskrecid!=null && taskrecid!='')
			{
				nlapiLogExecution('Debug', 'Updating PICK task with inventory reference number...',taskrecid);
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', taskrecid, 'custrecord_invref_no', invrefno);
			}
		}

		nlapiLogExecution('Debug', 'totalpickqty',totalpickqty);

		var totalallocqty = parseFloat(totalpickqty)+parseFloat(allocqty);

		nlapiLogExecution('Debug', 'totalallocqty',totalallocqty);

		if(totalallocqty==null || totalallocqty=='' || isNaN(totalallocqty))
		{
			totalallocqty=0;
		}

		nlapiLogExecution('Debug', 'Updating Inventory Record with allocate qty...',invrefno);
		//nlapiSubmitField('customrecord_ebiznet_createinv', invrefno, 'custrecord_ebiz_alloc_qty', totalallocqty);

		var scount=1;
		LABL1: for(var i=0;i<scount;i++)
		{	
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);			
				var inventoryAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');


				if (inventoryAllocQty == null || isNaN(inventoryAllocQty) || inventoryAllocQty == "") 
				{
					inventoryAllocQty = 0;
				}

				var totalallocqty = parseFloat(totalpickqty)+parseFloat(inventoryAllocQty);

				var str = 'totalallocqty.' + totalallocqty + '<br>';
				nlapiLogExecution('Debug', 'Qty Details', str);

				transaction.setFieldValue('custrecord_ebiz_alloc_qty',totalallocqty);			
				transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
				transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

				nlapiSubmitRecord(transaction);		
				nlapiLogExecution('Debug', 'Allocation Qty updated');
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
	}

	nlapiLogExecution('Debug', 'Out of  updateInvRefforOpenPickTasks');
}
function getOpenTasksCount(cartLPNo)
{
	var replenSearchResults = '';
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['8']);
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['24']);
	filters[2] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', cartLPNo);
	

	var columns = new Array();				
	columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_sku'));
	columns.push(new nlobjSearchColumn('custrecord_actendloc'));
	columns[2].setSort(true);


	
	var replenSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	nlapiLogExecution('DEBUG', 'replenSearchResults', replenSearchResults);    
	
	return replenSearchResults;
}
function getAdjustmentType(vWMSLocation1)
{
	var vAdjustType1='';
	var filters = new Array();		
	if(vWMSLocation1!=null && vWMSLocation1!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'anyof',[vWMSLocation1]));		
	filters.push(new nlobjSearchFilter('custrecord_adjusttasktype', null,'is','11'));//ADJT
	filters.push(new nlobjSearchFilter('custrecord_adjustdefaultflag', null,'is','T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');	

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filters,columns);		

	if(searchresults != null && searchresults !="")
	{
		if(searchresults.length > 0){
			vAdjustType1 =searchresults[0].getId();
		}
	}

	return vAdjustType1;
}
function LocationCubeUpdation(itemID, beginLocationId, expQty, optype)
{
	var arrDims = getSKUCubeAndWeight(itemID, 1);
	var itemCube = 0;
	var vTotalCubeValue = 0;
	if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
	{
		var uomqty = ((parseFloat(expQty))/(parseFloat(arrDims[1])));			
		itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
	} 

	var vOldRemainingCube = GeteLocCube(beginLocationId);
	nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

	if(optype == "P")
		vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
	else
		vTotalCubeValue = parseFloat(vOldRemainingCube)- parseFloat(itemCube);

	nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

	var retValue =  UpdateLocCube(beginLocationId,vTotalCubeValue);
	nlapiLogExecution('ERROR', 'LocCubeUpdation', retValue);

	return retValue; 
}




function GetBatchId(itemid,getEnteredbatch)
{
	try
	{
		nlapiLogExecution('DEBUG','itemid',itemid);
		nlapiLogExecution('DEBUG','getEnteredbatch',getEnteredbatch);

		var BatchID='';

		var filter=new Array();
		if(itemid!=null&&itemid!="")
			filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid));
		if(getEnteredbatch!=null && getEnteredbatch!='')
		filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',getEnteredbatch));
		var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filter,null);
		if(rec!=null&&rec!="")
		{
			BatchID=rec[0].getId();

		}

		nlapiLogExecution('DEBUG','BatchID',BatchID);
		return BatchID;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetBatchId',exp);
	}
}
