/**
 * 
 *//***************************************************************************
	  		 eBizNET Solutions .
  ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_WaveCreationQb_SL.js,v $
 *     	   $Revision: 1.2.2.9.4.1.4.10.2.4 $
 *     	   $Date: 2014/10/15 16:17:23 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveCreationQb_SL.js,v $
 * Revision 1.2.2.9.4.1.4.10.2.4  2014/10/15 16:17:23  skavuri
 * Case# 201410631 Std bundle issue fixed
 *
 * Revision 1.2.2.9.4.1.4.10.2.3  2014/09/19 12:01:01  skavuri
 * case # 201410303
 *
 * Revision 1.2.2.9.4.1.4.10.2.2  2014/09/18 13:40:36  sponnaganti
 * Case# 201410320
 * TPP SB Issue fix
 *
 * Revision 1.2.2.9.4.1.4.10.2.1  2014/07/14 15:30:55  sponnaganti
 * Case# 20149447
 * Compatibility Issue fix
 *
 * Revision 1.2.2.9.4.1.4.10  2014/06/24 06:07:38  skreddy
 * case # 20148997
 * Sportshq prod  issue fix
 *
 * Revision 1.2.2.9.4.1.4.9  2014/05/22 15:43:17  skavuri
 * Case # 20148491 SB Issue Fixed
 *
 * Revision 1.2.2.9.4.1.4.8  2014/03/19 09:08:44  snimmakayala
 * Case #: 20127688
 *
 * Revision 1.2.2.9.4.1.4.7  2014/02/07 07:37:28  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127065
 *
 * Revision 1.2.2.9.4.1.4.6  2013/12/13 13:48:24  grao
 * Added Location (site) filtetr in for Sports Hq..
 *
 * Revision 1.2.2.9.4.1.4.5  2013/10/16 07:23:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdchanges
 *
 * Revision 1.2.2.9.4.1.4.4  2013/09/11 12:18:36  rmukkera
 * Case# 20124374
 *
 * Revision 1.2.2.9.4.1.4.3  2013/08/27 15:22:42  skreddy
 * Case#: 20124078
 * Issue rellated to restrict transctions if location mapped to user
 *
 * Revision 1.2.2.9.4.1.4.2  2013/07/19 11:53:23  grao
 * Nautilus SB issue fixes case#:20123474
 * shipping method filed showing no values (empty) Related issue fixes
 *
 * Revision 1.2.2.9.4.1.4.1  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.2.9.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.9  2012/07/26 10:26:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.2.2.8  2012/05/09 07:52:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Partial Status Handling
 *
 * Revision 1.2.2.7  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.8  2012/03/20 17:59:44  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Screen Name is changed to Wave Creation by Line
 *
 * Revision 1.7  2012/03/14 07:06:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.6  2012/02/17 20:21:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.5  2012/02/11 00:21:43  gkalla
 * CASE201112/CR201113/LOG201121
 * Added functionality to populate fulfillment orders in dropdown for more than 1000
 *
 * Revision 1.4  2012/02/01 05:53:19  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.2.2.1
 *
 * Revision 1.3  2012/01/27 06:53:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation UE
 *
 * Revision 1.2  2012/01/05 14:01:49  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code tunning
 *
 * Revision 1.1  2011/12/21 16:45:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Gen User Event
 *
 * Revision 1.9  2011/11/15 09:05:44  spendyala
 * CASE201112/CR201113/LOG201121
 * added ship date fields
 *
 * Revision 1.54  2011/11/02 11:09:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * 
 *****************************************************************************/
function fillfulfillorderField(form, FulfillOrderField,shipmentField,maxno){
	var filtersso = new Array();		
	filtersso.push(new nlobjSearchFilter( 'custrecord_ord_qty', null, 'greaterthan', '0' ));
	//11-Partially Picked, 13-Partially Shipped, 15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filtersso.push(new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [11,13,15,25,26]));
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
//case# : 20124078 start 
	var vRoleLocation=getRoledBasedLocation();
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', vRoleLocation));

	}//end
	//new filter added by suman as on 040114
	filtersso.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickqty}),0)"));
	//end of the code
	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('id');	 
	columnsinvt[1] = new nlobjSearchColumn('custrecord_lineord');
	columnsinvt[0].setSort(true);
	columnsinvt[2] = new nlobjSearchColumn('custrecord_shipment_no');
	//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');

	FulfillOrderField.addSelectOption("", "");
	shipmentField.addSelectOption("","");

	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersso,columnsinvt);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('DEBUG', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('custrecord_lineord') != null && customerSearchResults[i].getValue('custrecord_lineord') != "" && customerSearchResults[i].getValue('custrecord_lineord') != " ")
		{
			var resdo = form.getField('custpage_qbso').getSelectOptions(customerSearchResults[i].getValue('custrecord_lineord'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		FulfillOrderField.addSelectOption(customerSearchResults[i].getValue('custrecord_lineord'), customerSearchResults[i].getValue('custrecord_lineord'));
	}
	for (var j = 0; customerSearchResults != null && j < customerSearchResults.length; j++) {

		//nlapiLogExecution('DEBUG', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[j].getValue('custrecord_shipment_no') != null && customerSearchResults[j].getValue('custrecord_shipment_no') != "" && customerSearchResults[j].getValue('custrecord_shipment_no') != " ")
		{
			var resdo2 = form.getField('custpage_qbshipmentno').getSelectOptions(customerSearchResults[j].getValue('custrecord_shipment_no'), 'is');
			if (resdo2 != null) {
				if (resdo2.length > 0) {
					continue;
				}
			}
			shipmentField.addSelectOption(customerSearchResults[j].getValue('custrecord_shipment_no'), customerSearchResults[j].getValue('custrecord_shipment_no'));
		}

	}

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		fillfulfillorderField(form, FulfillOrderField,shipmentField,maxno);	
	}
}
function WaveQbSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Wave Selection by Line');


		var soField = form.addField('custpage_qbso', 'select', 'Fulfillment Ord#');
		soField.setLayoutType('startrow', 'none');  		
//		Shipment #
		var shipmentField = form.addField('custpage_qbshipmentno', 'select', 'Shipment #');
		shipmentField .setLayoutType('startrow', 'none');
		fillfulfillorderField(form, soField,shipmentField,-1);
		/*var filtersso = new Array();		
		filtersso[0] = new nlobjSearchFilter( 'custrecord_ord_qty', null, 'greaterthan', '0' );
		//15-Selected Into Wave, 25-Edit, 26-Picks Failed
		filtersso[1] = new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [15,25,26] );
		soField.addSelectOption("","");
		var columnsinvt = new Array();
		columnsinvt[0] = new nlobjSearchColumn('custrecord_lineord');
		columnsinvt[0].setSort();
		columnsinvt[1] = new nlobjSearchColumn('custrecord_shipment_no');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersso,columnsinvt);

		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			//Searching for Duplicates		
			var res=  form.getField('custpage_qbso').getSelectOptions(searchresults[i].getValue('custrecord_lineord'), 'is');
			if (res != null && res != '') {
				nlapiLogExecution('DEBUG', 'res.length', res.length +searchresults[i].getValue('custrecord_lineord') );
				if (res.length > 0) {
					continue;
				}
			}		
			soField.addSelectOption(searchresults[i].getValue('custrecord_lineord'), searchresults[i].getValue('custrecord_lineord'));
		}*/		

		/*shipmentField.addSelectOption("","");
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {

			if(searchresults[i].getValue('custrecord_shipment_no')!=null && searchresults[i].getValue('custrecord_shipment_no')!="")
			{
				//Searching for Duplicates		
				var res=  form.getField('custpage_qbshipmentno').getSelectOptions(searchresults[i].getValue('custrecord_shipment_no'), 'is');
				if (res != null && res != '') {
					nlapiLogExecution('DEBUG', 'res.length', res.length +searchresults[i].getValue('custrecord_shipment_no') );
					if (res.length > 0) {
						continue;
					}
				}		
				shipmentField.addSelectOption(searchresults[i].getValue('custrecord_shipment_no'), searchresults[i].getValue('custrecord_shipment_no'));
			}  
		}*/


		//Route #
		var RouteField = form.addField('custpage_qbrouteno', 'select', 'Route','customlist_ebiznetroutelov');
		RouteField.setLayoutType('startrow', 'none');


		var soOrdPriority = form.addField('custpage_ordpriority', 'select', 'Ord Priority','customlist_ebiznet_order_priority');
		soOrdPriority.setLayoutType('startrow', 'none'); 		

		var soOrdType = form.addField('custpage_ordtype', 'select', 'Order Type','customrecord_ebiznet_order_type');
		soOrdType.setLayoutType('startrow', 'none');

		var Consignee = form.addField('custpage_consignee', 'select', 'Consignee','customer');
		Consignee.setLayoutType('startrow', 'none');

		//var consigneeid=getFieldValue('custpage_consignee');
		//nlapiLogExecution('DEBUG','CONSIGNEEID',consigneeid);


		var itemfamily = form.addField('custpage_itemfamily', 'select', 'Item Family','customrecord_ebiznet_sku_family');

		var itemgroup = form.addField('custpage_itemgroup', 'select', 'Item Group','customrecord_ebiznet_sku_group');

		var item = form.addField('custpage_item', 'select', 'Item','inventoryitem');	

		var iteminfo1= form.addField('custpage_iteminfoone', 'select', 'Item Info1');
		iteminfo1.addSelectOption("","");
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_item_info_1',null,'group');			 
		var filters= new Array();			
		filters.push(new nlobjSearchFilter('custitem_item_info_1', null, 'noneof',['@NONE@']));	
		//Searchng Duplicates
		var result= nlapiSearchRecord('item',null,filters,columns);
		for (var i = 0; result != null && i < result.length; i++) {

			var res=  form.getField('custpage_iteminfoone').getSelectOptions(result[i].getValue('custitem_item_info_1',null,'group'), 'is');
			if (res != null) {						
				if (res.length > 0) {
					continue;
				}
			}

			iteminfo1.addSelectOption(result[i].getValue('custitem_item_info_1',null,'group'),result[i].getText('custitem_item_info_1',null,'group'));
		}

		var iteminfo2= form.addField('custpage_iteminfotwo', 'select', 'Item Info2');
		iteminfo2.addSelectOption("","");
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_item_info_2',null,'group');			 
		var filters= new Array();	
		filters.push(new nlobjSearchFilter('custitem_item_info_2', null, 'noneof',['@NONE@']));	
		var result= nlapiSearchRecord('item',null,filters,columns);
		for (var i = 0; result != null && i < result.length; i++) {

			var res=  form.getField('custpage_iteminfotwo').getSelectOptions(result[i].getValue('custitem_item_info_2',null,'group'), 'is');
			if (res != null) {						
				if (res.length > 0) {
					continue;
				}
			}

			iteminfo2.addSelectOption(result[i].getValue('custitem_item_info_2',null,'group'),result[i].getText('custitem_item_info_2',null,'group'));
		}

		var iteminfo3= form.addField('custpage_iteminfothree', 'select', 'Item Info3');
		iteminfo3.addSelectOption("","");
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_item_info_3',null,'group');			 
		var filters= new Array();		
		filters.push(new nlobjSearchFilter('custitem_item_info_3', null, 'noneof',['@NONE@']));	
		var result= nlapiSearchRecord('item',null,filters,columns);
		for (var i = 0; result != null && i < result.length; i++) {
			var res=  form.getField('custpage_iteminfothree').getSelectOptions(result[i].getValue('custitem_item_info_3',null,'group'), 'is');
			if (res != null) {						
				if (res.length > 0) {
					continue;
				}
			}
			iteminfo3.addSelectOption(result[i].getValue('custitem_item_info_3',null,'group'),result[i].getText('custitem_item_info_3',null,'group'));
		}


		//var packcode = form.addField('custpage_packcode', 'select', 'Pack Code','customlist73');
		var packcode = form.addField('custpage_packcode', 'select', 'Pack Code','customlist_ebiznet_packcode');	

		//var uom = form.addField('custpage_uom', 'select', 'UOM','customlist75');
		var uom = form.addField('custpage_uom', 'select', 'UOM','customlist_ebiznet_uom');
		var Company = form.addField('custpage_company', 'select', 'Company','customrecord_ebiznet_company');

		var Destination = form.addField('custpage_dest', 'select', 'Destination','customlist_nswmsdestinationlov');

		Destination.setLayoutType('startrow', 'none');		

		var shippingcarrier = form.addField('custpage_shippingcarrier', 'select', 'Shipping Method');
		shippingcarrier.setLayoutType('startrow', 'none');
		
		var ShipmethodFilter = new Array();
		var vRoleLocation=getRoledBasedLocation();
		nlapiLogExecution('ERROR', 'vRoleLocation',vRoleLocation );
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			ShipmethodFilter.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', vRoleLocation));

		}
		//added filters because of performance
		ShipmethodFilter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [15,25,26]));
		ShipmethodFilter.push(new nlobjSearchFilter('custrecord_do_carrier', null, 'isnotempty'));
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_do_carrier',null,'group');

		var selectshipmd= nlapiSearchRecord('customrecord_ebiznet_ordline',null,ShipmethodFilter,column);
		shippingcarrier.addSelectOption("","");
		for(var i=0;  selectshipmd != null && i<selectshipmd.length;i++)
		{
			nlapiLogExecution('ERROR', selectshipmd[i].getValue('custrecord_do_carrier',null,'group') + ',' + selectshipmd[i].getText('custrecord_do_carrier',null,'group') );
			shippingcarrier.addSelectOption(selectshipmd[i].getValue('custrecord_do_carrier',null,'group'),selectshipmd[i].getText('custrecord_do_carrier',null,'group'));
		}

		/*var record = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});

		var methodsField = record.getField('shipmethod');
		var methodOptions = methodsField.getSelectOptions(null, null);
		nlapiLogExecution('DEBUG','methodOptions',methodOptions);

		shippingcarrier.addSelectOption("",""); 

		for (var i = 0; methodOptions != null && i < methodOptions.length; i++) {
			nlapiLogExecution('DEBUG', methodOptions[i].getId() + ',' + methodOptions[i].getText() );
			shippingcarrier.addSelectOption(methodOptions[i].getId(),methodOptions[i].getText());
		}	*/
		//added by shylaja on 091011 to filter the statuses which are not having allow pick at sku status.
		var itemstatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
		var ItemstatusFilter = new Array();
		var Itemstatuscolumns = new Array();

		ItemstatusFilter[0] = new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T');
		Itemstatuscolumns[0] = new nlobjSearchColumn('custrecord_statusdescriptionskustatus');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemstatusFilter, Itemstatuscolumns);

		itemstatus.addSelectOption("","");
		if (searchresults != null) {	
			for (var i = 0; i < Math.min(500, searchresults.length); i++) {

				var res = form.getField('custpage_itemstatus').getSelectOptions(searchresults[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}
				nlapiLogExecution('DEBUG','res',res);
				nlapiLogExecution('DEBUG','searchresults[i].getId()',searchresults[i].getId());
				itemstatus.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('custrecord_statusdescriptionskustatus'));
				nlapiLogExecution('DEBUG','itemstatus',itemstatus);
			}
		}
		nlapiLogExecution('DEBUG','itemstatusnew',itemstatus);



		var country = form.addField('custpage_country', 'select', 'Country');
		
		var record = nlapiCreateRecord('location', {recordmode: 'dynamic'});	
		
		//Case# 201410631 starts
		//var addsubrec= record.createSubrecord('mainaddress');
		
		//var countryField = addsubrec.getField('country');// Case# 201410303
		var countryField = record.getField('country');
		//Case# 201410631 ends
		
		
		//case# 20149447 starts
		if(countryField!=null && countryField!='')
		{
			var countryOptions = countryField.getSelectOptions(null, null);
			nlapiLogExecution('DEBUG','countryOptions',countryOptions);
			nlapiLogExecution('DEBUG', countryOptions[0].getId() + ',' + countryOptions[0].getText() );
			country.addSelectOption("","");
			for (var i = 0; countryOptions != null && i < countryOptions.length; i++) {
				nlapiLogExecution('DEBUG', countryOptions[i].getId() + ',' + countryOptions[i].getText() );
				country.addSelectOption(countryOptions[i].getId(),countryOptions[i].getText());
			}
		}
		//case# 20149447 ends

		var state = form.addField('custpage_state', 'select', 'Ship State');
		var addr1 = form.addField('custpage_addr1', 'select', 'Ship Address');
		var City = form.addField('custpage_city', 'select', 'Ship City');

		state.addSelectOption("","");
		addr1.addSelectOption("","");
		City.addSelectOption("","");
		var columns = new Array();
		columns.push(new nlobjSearchColumn('shipstate'));
		columns.push(new nlobjSearchColumn('shipcity'));
		columns.push(new nlobjSearchColumn('shipaddress'));

		var filters= new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));

		var result= nlapiSearchRecord('salesorder',null,filters,columns);

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];
			var vshipstate=soresult.getValue('shipstate');			
			var res=  form.getField('custpage_state').getSelectOptions(vshipstate, 'is');
			if (res != null) {				
				if (res.length > 0) {
					continue;
				}
			}		
			state.addSelectOption(vshipstate, vshipstate);
		}

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];
			var vshipstate=soresult.getValue('shipstate');
			var vshipcity=soresult.getValue('shipcity');
			var vshipaddr=soresult.getValue('shipaddress');

			var res1=  form.getField('custpage_addr1').getSelectOptions(vshipaddr, 'is');
			if (res1 != null) {				
				if (res1.length > 0) {
					continue;
				}
			}		
			addr1.addSelectOption(vshipaddr, vshipaddr);
		}

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];			
			var vshipcity=soresult.getValue('shipcity');

			var res1=  form.getField('custpage_city').getSelectOptions(vshipcity, 'is');
			if (res1 != null) {				
				if (res1.length > 0) {
					continue;
				}
			}		

			City .addSelectOption(vshipcity, vshipcity);
		}


		//added carrier field By suman
		var carrier = form.addField('custpage_carrier', 'select', 'Carrier','customrecord_ebiznet_carrier');
		//end of carrier field

		var soShipdate = form.addField('custpage_soshipdate', 'date', 'Ship Date');

		var sofrightterms = form.addField('custpage_sofrieghtterms', 'select', 'Freight Terms','customlist_nswmsfreighttermslov');

		var soOrderdate = form.addField('custpage_soorderdate', 'date', 'Order Date');
		form.addField('custpage_wmsstatusflag','select','WMS Status Flag','customrecord_wms_status_flag').setDefaultValue(25);
		form.addSubmitButton('Display');
		
		var location = form.addField('custpage_site', 'select', 'Location');
		var locationFilter = new Array();
		var locationcolumns = new Array();
		locationFilter.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));
		locationFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		locationcolumns[0] = new nlobjSearchColumn('custrecord_ebizlocationid');

		var locsearchresults = nlapiSearchRecord('location', null, locationFilter, locationcolumns);

		location.addSelectOption("","");
		if (locsearchresults != null) {	
			for (var i = 0; i < Math.min(500, locsearchresults.length); i++) {

				var res = form.getField('custpage_site').getSelectOptions(locsearchresults[i].getValue('custrecord_ebizlocationid'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}

				location.addSelectOption(locsearchresults[i].getId(), locsearchresults[i].getValue('custrecord_ebizlocationid'));

			}
		}




		response.writePage(form);
	}
	else //this is the POST block
	{
		var vSo = request.getParameter('custpage_qbso');
		var vitem = request.getParameter('custpage_item');
		var vcustomer = request.getParameter('custpage_consignee');
		var vordPriority = request.getParameter('custpage_ordpriority');
		var vordtype = request.getParameter('custpage_ordtype');
		var vitemgroup = request.getParameter('custpage_itemgroup');
		var vitemfamily = request.getParameter('custpage_itemfamily');
		var vpackcode = request.getParameter('custpage_packcode');
		var vuom = request.getParameter('custpage_uom');
		var vitemstatus = request.getParameter('custpage_itemstatus');
		var siteminfo1= request.getParameter('custpage_iteminfoone'); 
		var siteminfo2= request.getParameter('custpage_iteminfotwo');
		var siteminfo3= request.getParameter('custpage_iteminfothree');
		var vCompany= request.getParameter('custpage_company');
		var vShippingCarrier= request.getParameter('custpage_shippingcarrier');
		var vShipCountry= request.getParameter('custpage_country');
		var vShipState= request.getParameter('custpage_state');
		var vShipCity= request.getParameter('custpage_city');
		var vShipAddr1= request.getParameter('custpage_addr1');
		var vShipmentNo= request.getParameter('custpage_qbshipmentno');
		var vRouteno= request.getParameter('custpage_qbrouteno');
		//added By Suman
		var vCarrier= request.getParameter('custpage_carrier');

		var vShipdate= request.getParameter('custpage_soshipdate');
		var vFreightTerms= request.getParameter('custpage_sofrieghtterms');
		var vOrderDate= request.getParameter('custpage_soorderdate');
		var wmsstatusflag=request.getParameter('custpage_wmsstatusflag');
		nlapiLogExecution('DEBUG','FreightTerms',vFreightTerms);
		
		var vSite=request.getParameter('custpage_site');
		nlapiLogExecution('DEBUG','vSite',vSite);
		nlapiLogExecution('DEBUG','vShipdate',vShipdate);
		nlapiLogExecution('DEBUG','vOrderDate',vOrderDate);
		nlapiLogExecution('DEBUG','wmsstatusflag',wmsstatusflag);
		var WaveQbparams = new Array();		
		if (vSo!=null &&  vSo != "") {
			WaveQbparams["custpage_qbso"] = vSo;
		}

		if (vitem!=null &&  vitem != "") {
			WaveQbparams["custpage_item"] = vitem;
		}
		if (vcustomer!=null && vcustomer != "") {
			WaveQbparams ["custpage_consignee"] = vcustomer;
		}
		if (vordPriority!=null && vordPriority != "") {
			WaveQbparams ["custpage_ordpriority"] = vordPriority;
		}
		if (vordtype!=null && vordtype != "") {
			WaveQbparams ["custpage_ordtype"] = vordtype;
		}
		if (vitemgroup!=null && vitemgroup != "") {
			WaveQbparams ["custpage_itemgroup"] = vitemgroup;
		}
		if (vitemfamily!=null && vitemfamily != "") {
			WaveQbparams ["custpage_itemfamily"] = vitemfamily;
		}
		if (vpackcode!=null && vpackcode != "") {
			WaveQbparams ["custpage_packcode"] = vpackcode;
		}
		if (vuom!=null && vuom != "") {
			WaveQbparams ["custpage_uom"] = vuom;
		}
		if (vitemstatus!=null && vitemstatus != "") {
			WaveQbparams ["custpage_itemstatus"] = vitemstatus;
		}
		if (siteminfo1!=null && siteminfo1 != "") { 
			WaveQbparams ["custpage_siteminfo1"] = siteminfo1;
		}
		if (siteminfo2!=null && siteminfo2 != "") { 
			WaveQbparams ["custpage_siteminfo2"] = siteminfo2;
		}
		if (siteminfo3!=null && siteminfo3 != "") { 
			WaveQbparams ["custpage_siteminfo3"] = siteminfo3;
		}
		if (vCompany!=null && vCompany != "") {
			WaveQbparams ["custpage_company"] = vCompany;
		}
		if (vShippingCarrier!=null && vShippingCarrier != "") {
			WaveQbparams ["custpage_shippingcarrier"] = vShippingCarrier;
		}
		if (vShipCountry!=null && vShipCountry != "") {
			WaveQbparams ["custpage_country"] = vShipCountry;
		}
		if (vShipState!=null && vShipState != "") {
			WaveQbparams ["custpage_state"] = vShipState;
		}
		if (vShipCity!=null && vShipCity != "") {
			WaveQbparams ["custpage_city"] = vShipCity;
		}
		if (vShipAddr1!=null && vShipAddr1 != "") {
			WaveQbparams ["custpage_addr1"] = vShipAddr1;
		}  
		if (vShipmentNo!=null && vShipmentNo != "") {
			WaveQbparams ["custpage_shipmentno"] = vShipmentNo;
		}  
		if (vRouteno!=null && vRouteno != "") {
			WaveQbparams ["custpage_routeno"] = vRouteno;
		}  
		//added Carrier By Suman
		if (vCarrier!=null && vCarrier != "") {
			// Case# 20148491 starts
			var columns = new Array();
			columns.push(new nlobjSearchColumn('name'));
			var filters= new Array();
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filters.push(new nlobjSearchFilter('internalid', null, 'is',vCarrier));
			var result= nlapiSearchRecord('customrecord_ebiznet_carrier',null,filters,columns);
			var vCarrier1='';
			if(result.length !='' && result.length!=null && result.length !='null')
				vCarrier1=result[0].getValue('name');
			nlapiLogExecution('DEBUG', 'vCarrier1',vCarrier1);
			//WaveQbparams ["custpage_carrier"] = vCarrier;
			  WaveQbparams ["custpage_carrier"] = vCarrier1;
			//Case# 20148491 ends
		}

		if (vShipdate!=null && vShipdate != "") {
			WaveQbparams ["custpage_soshipdate"] = vShipdate;
		}
		if (vFreightTerms!=null && vFreightTerms != "") {
			nlapiLogExecution('DEBUG', 'freightterms',vFreightTerms) ;
			WaveQbparams ["custpage_sofrieghtterms"] = vFreightTerms;
		}
		if (vOrderDate!=null && vOrderDate != "") {
			WaveQbparams ["custpage_soorderdate"] = vOrderDate;
		}
		if (wmsstatusflag!=null && wmsstatusflag != "") {
			WaveQbparams ["custpage_wmsstatusflag"] = wmsstatusflag;
		}
		if (vSite!=null && vSite != "") {
			WaveQbparams ["custpage_site"] = vSite;
		}
		response.sendRedirect('SUITELET', 'customscript_wavecreation', 'customdeploy_wavecreationdi', false, WaveQbparams );
	}
}

function myGenerateLocationButton(){
	alert("Hi"); 
}
