/***************************************************************************
	  		   eBizNET Solutions .
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_POReceiptEntry_SL.js,v $
 *     	   $Revision: 1.1.2.4 $
 *     	   $Date: 2013/10/24 15:53:48 $
 *     	   $Author: rrpulicherla $
 *
 *   eBizNET version and checksum/ stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SU/M: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *� $Log: ebiz_POReceiptEntry_SL.js,v $
 *� Revision 1.1.2.4  2013/10/24 15:53:48  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� Afosa issue fixes
 *�
 *� Revision 1.1.2.3  2013/09/11 12:17:06  rmukkera
 *� Case# 20124374
 *�
 *� Revision 1.1.2.2  2013/04/17 16:05:33  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.1.2.1  2013/04/09 13:19:32  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue related to display Item name instead of Item Id
 *�
 *
 **�
 *****************************************************************************/

function poReceiptEntrySuitelet(request, response){
	if (request.getMethod() == 'GET') {
		//create the Suitelet form
		var form = nlapiCreateForm('PO Receipt');
		form.setScript('customscript_poreceiptentryvalidation');
		var vpono = request.getParameter('custparam_po');
		var vreceiptno = request.getParameter('custparam_receipt');

		/*if(vreceiptno != "" && vreceiptno != null)
		{
			var receiptField = form.addField('custpage_receipt', 'text', 'Receipt #').setMandatory(true).setDefaultValue(vreceiptno).setDisabled(true);
			receiptField.setLayoutType('normal', 'startrow');
		}
		else
		{*/
			var receiptField = form.addField('custpage_receipt', 'text', 'Receipt #').setMandatory(true);
			receiptField.setLayoutType('normal', 'startrow');
		//}
		

		var TrailerField = form.addField('custpage_trailer', 'select', 'Trailer #', 'customrecord_ebiznet_trailer');


		var BOLField = form.addField('custpage_bol', 'text', 'BOL #');
		BOLField.setLayoutType('normal', 'startrow');


		//Adding  ItemSubList
		var ItemSubList = form.addSubList("custpage_receipt_items", "list", "ItemList");
		ItemSubList.addMarkAllButtons();
		ItemSubList.addField("custpage_receipt_chksel", "checkbox","Create Receipt");
		ItemSubList.addField("custpage_receipt_pono", "text", "PO#");//.setDefaultValue(request.getParameter('custparam_po_line_num'));
		ItemSubList.addField("custpage_receipt_polineno", "text", "PO Line#");
		ItemSubList.addField("custpage_receipt_item", "text", "Item");
		ItemSubList.addField("custpage_receipt_poqty", "text", "PO Qty");       
		ItemSubList.addField("custpage_receipt_remaningqty", "text", "Remaining qty");
		ItemSubList.addField("custpage_receipt_receiptqty", "text", "Receipt qty").setDisplayType('entry').setMandatory(true);; 
		ItemSubList.addField("custpage_receipt_location", "text", "location").setDisplayType('hidden'); 
		ItemSubList.addField("custpage_receipt_company", "text", "company").setDisplayType('hidden'); 
		ItemSubList.addField("custpage_receipt_poid", "text", "po id").setDisplayType('hidden'); 
		ItemSubList.addField("custpage_receipt_itemid", "text", "itemid").setDisplayType('hidden'); 

		var internalId;
		var pono,polineno,ordqty,item,company,location;


		var filters = new Array();
		if (vpono != "" && vpono != null) 
			filters[0] = new nlobjSearchFilter('tranid', null, 'is', vpono);
		filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'F');
		filters[2]=new nlobjSearchFilter('taxline', null, 'is', 'F');


		var columns = new Array();
		columns[0] = new nlobjSearchColumn('line');
		columns[1] = new nlobjSearchColumn('item');
		columns[2] = new nlobjSearchColumn('quantity');
		columns[3] = new nlobjSearchColumn('tranid');
		columns[4] = new nlobjSearchColumn('location');
		columns[5] = new nlobjSearchColumn('custbody_nswms_company');
		columns[6] = new nlobjSearchColumn('Internalid');
		columns[0].setSort();

		var searchresults = nlapiSearchRecord('purchaseorder', null, filters, columns);
		if(searchresults != "" && searchresults != null)
			var internalId = searchresults[0].getValue('Internalid');




		var filterspoReceipt = new Array();
		if (internalId != "" && internalId != null) 
			filterspoReceipt[0] = new nlobjSearchFilter('custrecord_ebiz_poreceipt_pono', null, 'is', internalId);
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptqty');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_wmsstatusflag');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_lineno');

		var searchresultsPoReceipt = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, filterspoReceipt, columns);



		for (var i = 0; searchresults != null && i < searchresults.length; i++) 
		{
			var lineReceiptQty=0;
			polineno = searchresults[i].getValue('line');
			//internalId = searchresults[i].getValue('Internalid');
			pono = searchresults[i].getValue('tranid');
			ordqty = searchresults[i].getValue('quantity');
			itemId = searchresults[i].getValue('item');
			var item = searchresults[i].getText('item');
			company = searchresults[i].getValue('custbody_nswms_company');
			location = searchresults[i].getValue('location');

			if(searchresultsPoReceipt != "" && searchresultsPoReceipt != null)
			{
				for (var k = 0;  k < searchresultsPoReceipt.length; k++) 
				{

					nlapiLogExecution('ERROR', 'searchresultsPoReceipt',searchresultsPoReceipt.length);
					//var searchresultsRcpt = searchresults[i];	
					var poReceiptLineno = searchresultsPoReceipt[k].getValue('custrecord_ebiz_poreceipt_lineno');
					nlapiLogExecution('ERROR', 'searchresultsPoReceipt polineno',polineno);
					nlapiLogExecution('ERROR', 'searchresultsPoReceipt poReceiptLineno',poReceiptLineno);
					if(k == 0 && polineno == poReceiptLineno)
					{
						if(searchresultsPoReceipt[k].getValue('custrecord_ebiz_poreceipt_receiptqty') != "" && searchresultsPoReceipt[k].getValue('custrecord_ebiz_poreceipt_receiptqty') != null)
							lineReceiptQty = parseInt(searchresultsPoReceipt[k].getValue('custrecord_ebiz_poreceipt_receiptqty'));
						nlapiLogExecution('ERROR', 'lineReceiptQty in if',lineReceiptQty);

						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_pono',  i + 1, pono); 
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_polineno',  i + 1, polineno);                       
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_item',  i + 1, item);   
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_poqty',i + 1, ordqty);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_location',i + 1, location);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_company',i + 1, company);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_poid',i + 1, internalId);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_itemid',i + 1, itemId);
						var remaningqty = 0;
						remaningqty = parseInt(ordqty)-parseInt(lineReceiptQty);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_remaningqty',i + 1, parseInt(remaningqty).toString());
					}
					else if(k > 0 &&  polineno == poReceiptLineno)
					{

						if(searchresultsPoReceipt[k].getValue('custrecord_ebiz_poreceipt_receiptqty') != "" && searchresultsPoReceipt[k].getValue('custrecord_ebiz_poreceipt_receiptqty') != null)
							lineReceiptQty = parseInt(lineReceiptQty) + parseInt(searchresultsPoReceipt[k].getValue('custrecord_ebiz_poreceipt_receiptqty'));
						nlapiLogExecution('ERROR', 'lineReceiptQty in else',lineReceiptQty);

						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_pono',  i + 1, pono); 
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_polineno',  i + 1, polineno);                       
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_item',  i + 1, item);   
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_poqty',i + 1, ordqty);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_location',i + 1, location);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_company',i + 1, company);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_poid',i + 1, internalId);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_itemid',i + 1, itemId);
						var remaningqty = 0;
						remaningqty = parseInt(ordqty)-parseInt(lineReceiptQty);
						form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_remaningqty',i + 1, parseInt(remaningqty).toString());
					}

				}
			}

			else
			{
				nlapiLogExecution('ERROR', 'new record','new record');

				form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_pono',  i + 1, pono); 
				form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_polineno',  i + 1, polineno);                       
				form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_item',  i + 1, item);   
				form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_poqty',i + 1, ordqty);
				form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_location',i + 1, location);
				form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_company',i + 1, company);
				form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_poid',i + 1, internalId);
				form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_itemid',i + 1, itemId);
				var remaningqty = 0;
				remaningqty = parseInt(ordqty)-parseInt(lineReceiptQty);
				form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_remaningqty',i + 1, parseInt(remaningqty).toString());
			}
		}
		nlapiLogExecution('ERROR', 'outforloop','outforloop');
		//form.addSubmitButton('Display');
		form.addSubmitButton('Create Receipt');
		response.writePage(form);
	}
	else //this is the POST block
	{
		var form = nlapiCreateForm('PO Receipt');
		nlapiLogExecution('ERROR', 'intoresponse','intoresponse');
		form.setScript('customscript_poreceiptentryvalidation');
		
		var trailerno = request.getParameter('custpage_trailer');
		var bolno = request.getParameter('custpage_bol');
		var receiptno = request.getParameter('custpage_receipt');

		var RecCnt = request.getLineItemCount('custpage_receipt_items');
		nlapiLogExecution('ERROR', 'Po Receipt REC count ', RecCnt);
		

		var vpono,vpolineno,vpoitem,vpolineqty;
		for (var k = 1; k <= RecCnt; k++) 
		{
			var vpono = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_pono', k);
			var vpolineno = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_polineno', k);
			//var vpoitem = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_item', k);
			var vpoitem = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_itemid', k);
			var vpolineqty = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_poqty', k);
			var vporeceiptqty = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_receiptqty', k);
			var company = request.getLineItemValue('custpage_receipt_items','custpage_receipt_company', k);
			var location = request.getLineItemValue('custpage_receipt_items','custpage_receipt_location', k);
			var internalId = request.getLineItemValue('custpage_receipt_items','custpage_receipt_poid', k);
			var remaningqty = request.getLineItemValue('custpage_receipt_items','custpage_receipt_remaningqty', k);
			
			nlapiLogExecution('ERROR', 'remaningqty ', remaningqty);
			nlapiLogExecution('ERROR', 'Po Receipt vpono ', vpono);


			var PoReceiptRec = nlapiCreateRecord('customrecord_ebiznet_trn_poreceipt');
			nlapiLogExecution('ERROR', 'Create PoReceipt  REC ', 'PoReceipt');
			//PoReceiptRec.setFieldValue('name', idl);
			
			
			PoReceiptRec.setFieldValue('name', receiptno);			
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_pono', internalId);			
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_lineno', vpolineno);			
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_item', vpoitem);			
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_polineordqty', remaningqty);	
			//PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_polineordqty', vpolineqty);			
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_trailerno', trailerno);			
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_bol', bolno);		
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_receiptno', receiptno);			
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_receiptqty', parseInt(vporeceiptqty).toString());
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_wmsstatusflag', '25');//OPEN
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_compid', company);
			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_siteid', location);



			var currentContext = nlapiGetContext();  
			var currentUserID = currentContext.getUser();

			PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_user', currentUserID);
			//PoReceiptRec.setFieldValue('custrecord_ebiz_poreceipt_updateduser', currentUserID);
			//transaction.setFieldValue('custrecord_ebiz_poreceipt_closeddate', DateStamp());
			var PoReceiptrecid = nlapiSubmitRecord(PoReceiptRec, false, true);
		}

		form.addSubmitButton('Create Receipt');

		var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);      
		 msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'PO Receipt Added Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");

		response.writePage(form);



		//response.sendRedirect('SUITELET', 'customscript_ebiznet_poreceipt', 'customdeploy_poreceipt', false, null);

	}
}

