/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WO_Sch_Allocate_SL.js,v $
 *     	   $Revision: 1.1.4.2 $
 *     	   $Date: 2014/09/26 15:25:28 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *  $Log: ebiz_WO_Sch_Allocate_SL.js,v $
 *  Revision 1.1.4.2  2014/09/26 15:25:28  skavuri
 *  Case# 201410092 jawbone cr
 *
 *  Revision 1.3.2.11.4.4.4.13  2014/04/09 06:03:46  skreddy
 *  case 20127925
 *  LL sb  issue fix
 *
 *  Revision 1.3.2.11.4.4.4.12  2013/09/24 07:15:58  gkalla
 *  Case# 20124548
 *  Work order issue of not picking from bulk
 *
 *  Revision 1.3.2.11.4.4.4.11  2013/09/02 08:06:08  spendyala
 *  case# 20124214
 *  issue fixed related to case20124214
 *
 *  Revision 1.3.2.11.4.4.4.10  2013/07/11 14:57:43  gkalla
 *  Case# 20123352
 *  PCT Issue
 *
 *  Revision 1.3.2.11.4.4.4.9  2013/07/09 15:40:57  skreddy
 *  Case# 20123356
 *  remove duplicate error message
 *
 *  Revision 1.3.2.11.4.4.4.8  2013/06/18 14:52:08  skreddy
 *  CASE201112/CR201113/LOG201121
 *  expiry date for Lot numbers
 *
 *  Revision 1.3.2.11.4.4.4.7  2013/06/03 07:49:17  gkalla
 *  CASE201112/CR201113/LOG201121
 *  Getting lot numbers from inventory changed for PCT
 *
 *  Revision 1.3.2.11.4.4.4.6  2013/06/03 06:54:40  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 *  Inventory Changes
 *
 *  Revision 1.3.2.11.4.4.4.5  2013/04/01 20:59:31  snimmakayala
 *  CASE201112/CR201113/LOG2012392
 *  Prod and UAT issue fixes.
 *
 *  Revision 1.3.2.11.4.4.4.4  2013/03/19 11:46:26  snimmakayala
 *  CASE201112/CR201113/LOG2012392
 *  Production and UAT issue fixes.
 *
 *  Revision 1.3.2.11.4.4.4.3  2013/03/08 14:43:37  skreddy
 *  CASE201112/CR201113/LOG201121
 *  Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.25.2.20  2012/08/30 02:04:56  gkalla
 *
 *****************************************************************************/
function GeneratLocationsSuiteletSch(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Generate Bin Locations for Work orders');




		//var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		//WorkOrder.addSelectOption("","");			
		// Retrieve all sales orders
		//var WorkOrderList = getAllWorkOrders();		
		// Add all sales orders to SO Field
		//WorkOrder.setDefaultValue(request.getParameter('custpage_workorder'));
		//addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);

		//WorkOrder.setMandatory(true);
		//fillsalesorderField(form, salesorder,-1);
		//WorkOrder.setDefaultValue(woidno);	

		form.addSubmitButton('Generate Bin Location');
		var woid=request.getParameter('custpage_workorder');
		nlapiLogExecution('ERROR','woid',woid);

		var wointid = form.addField('custpage_hdnwono', 'text', 'wono');
		wointid.setDisplayType('hidden');
		wointid.setDefaultValue(woid); 
		var built="";
		if(woid!=null && woid!='')
		{
			nlapiLogExecution('ERROR','INTOwoid1',woid);
			var searchresults = nlapiLoadRecord('workorder', woid);
			//var lineItems= searchresults.getLineItemCount('item');
			if(searchresults!=null && searchresults!='') 
			{ 	    	
				var qty = searchresults.getFieldValue('quantity');    	     
				var itemid=searchresults.getFieldValue('assemblyitem');
				var itemText=searchresults.getFieldText('assemblyitem');
				var WoCreatedDate=searchresults.getFieldValue('trandate');
				var WoDueDate=searchresults.getFieldValue('custbody_ebiz_woduedate');
				var WoExpCompDate=searchresults.getFieldValue('custbody_ebiz_expcompletiondate');
				var WoActCompDate=searchresults.getFieldValue('custbody_ebiz_actcompletiondate');
				var WoExpcompletionDate=searchresults.getFieldValue('custbody_ebiz_expcompletiondate');
				var itemQty=searchresults.getFieldValue('quantity');
				var itemStatus=searchresults.getFieldValue('status');
				var itemLocation=searchresults.getFieldValue('location');
				var WOName=searchresults.getFieldValue('tranid');
				var Approveflagtext=searchresults.getFieldText('custbody_ebiz_wo_qc_status');

				var filters = new Array();
				filters[0] = new nlobjSearchFilter('internalid', null, 'is', woid);
				filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');//ship task


				var columns = new Array();
				columns[0] = new nlobjSearchColumn('quantityshiprecv');

				var searchresultsWaveNo = nlapiSearchRecord('workorder', null, filters, columns);
				if(searchresultsWaveNo!=null && searchresultsWaveNo!=''){
					built = searchresultsWaveNo[0].getValue('quantityshiprecv');
				}
				nlapiLogExecution('ERROR','qty',qty);
				nlapiLogExecution('ERROR','built',built);
				if(built==null || built =="")
					built=0;
				qty = parseInt(qty) - parseInt(built);
				nlapiLogExecution('ERROR','qty',qty);
				var wointname = form.addField('custpage_hdnwoname', 'text', 'woname');
				wointname.setDisplayType('hidden');
				wointname.setDefaultValue(WOName);
				/* code merged from monobid prod bundle on 27th feb 2013 by Radhika */

				var kitItemStatus="";
				var ItemFilters = new Array();
				ItemFilters.push(new nlobjSearchFilter('internalid', null, 'anyof',itemid));

				var ItemColumns = new Array();
				ItemColumns.push(new nlobjSearchColumn('custitem_ebizdefskustatus'));

				var ItemResults = nlapiSearchRecord('item', null, ItemFilters, ItemColumns);
				if(ItemResults !=null && ItemResults!="")
				{
					kitItemStatus= ItemResults[0].getValue('custitem_ebizdefskustatus');
					nlapiLogExecution('ERROR','kitItemStatus',kitItemStatus);

				}

				/* upto here */
				var itemLocationtext="";
				if(itemLocation == null || itemLocation =="")
				{
					itemLocation="&nbsp;";
				}
				else
				{

					var searchresultslocation = nlapiLoadRecord('location', itemLocation);
					if(searchresultslocation!=null && searchresultslocation!='') 
					{
						itemLocationtext=searchresultslocation.getFieldValue('name');
					}
				}

				form.addField('custpage_wo', 'text', 'WO# : ').setLayoutType("startrow").setDisplayType('inline').setDefaultValue(WOName);	
				form.addField('custpage_kititem', 'text', 'Item : ').setDisplayType('inline').setDefaultValue(itemText);
				form.addField('custpage_kitqty', 'text', 'Qty : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(qty);
				form.addField('custpage_kitlocation', 'text', 'Location : ').setDisplayType('inline').setDefaultValue(itemLocationtext);
				form.addField('custpage_createdate', 'text', 'Created Date : ').setDisplayType('inline').setDefaultValue(WoCreatedDate);
				form.addField('custpage_duedate', 'text', 'Due Date : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(WoDueDate);
				form.addField('custpage_expdate', 'text', 'Expected Completion Date : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(WoExpCompDate);
				form.addField('custpage_actdate', 'text', 'Actual Completion Date : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(WoActCompDate);
				form.addField('custpage_itemstatus', 'text', 'WO Status : ').setDisplayType('inline').setDefaultValue(itemStatus);
				form.addField('custpage_qcstatus', 'text', 'QC Status : ').setDisplayType('inline').setDefaultValue(Approveflagtext);
				var hdnBackOrdered = form.addField('custpage_hdnbackordered', 'text', 'Backordered Bool : ').setDisplayType('inline').setDisplayType('hidden');
				form.addField('custpage_hdnitemtext', 'text', 'Assembly Item Text : ').setDisplayType('inline').setDisplayType('hidden').setDefaultValue(WOName);





				var vLineCount=searchresults.getLineItemCount('item');
				//Adding  ItemSubList

				var ItemSubList = form.addSubList("custpage_deliveryord_items", "list", "Components");
				ItemSubList.addField("custpage_deliveryord_sku", "text","Item");
				ItemSubList.addField("custpage_hidden_skuid", "text","Itemid").setDisplayType('hidden');//added by santosh;
				ItemSubList.addField("custpage_deliveryord_itemdesc", "text", "Description");
				ItemSubList.addField("custpage_deliveryord_memqty", "text", "Quantity");
				//ItemSubList.addField("custpage_deliveryord_memqty", "text", "Required Qty").setDisplayType('hidden');       
				ItemSubList.addField("custpage_deliveryord_comitqty", "text", "Committed");        
				ItemSubList.addField("custpage_deliveryord_backordqty", "text", 'Back Ordered');

				var serialno= ItemSubList.addField("custpage_deliveryord_lotno", "select", "Serial/Lot Numbers");
				serialno.addSelectOption('', '');
				ItemSubList.addField("custpage_comments", "text", "Comments").setDisplayType('entry');
				ItemSubList.addField("custpage_hiddenlotno", "text", "lotno").setDisplayType('hidden');//added by santosh
				ItemSubList.addField("custpage_lineno", "text", "Lineno").setDisplayType('hidden');//added by santosh

				//ItemSubList.addField("custpage_skurecid", "text", "Recid").setDisplayType('hidden');
				//ItemSubList.addField("custpage_deliveryord_lotno", "select", "Serial/Lot Numbers");
				//ItemSubList.addField("custpage_lotno", "text", "Commit"); 
				/*ItemSubList.addField("custpage_lineno", "text", "LP#");
				ItemSubList.addField("custpage_lineno", "text", "Bin Location");
				ItemSubList.addField("custpage_lineno", "text", "Inventory Ref#");
				ItemSubList.addField("custpage_lineno", "text", "Available Qty");
				ItemSubList.addField("custpage_lineno", "text", "Bin Location");
				ItemSubList.addField("custpage_lineno", "text", "Lot Info");
				ItemSubList.addField("custpage_lineno", "text", "Line No");*/

				//to get distinct Itemid 
				var skuarray=new Array();
				for (z=1; z<=vLineCount; z++) 
				{
					var skuid =searchresults.getLineItemValue('item', 'item', z);	
					nlapiLogExecution('ERROR', 'skuid', skuid);
					if(skuarray.indexOf(skuid)==-1)
					{
						skuarray.push(skuid);
					}

				}
				//end of the code 

				//binding sublist dropdown
				/* code merged from monobid on feb 27th 2013 by Radhika (added Kititemstatus field to the addSerialno fun) */
				addSerialno(skuarray,itemLocation,form,serialno,kitItemStatus);


				var k=0;c=1;
				for(var m=1;m<=vLineCount;m++)
				{
					nlapiLogExecution('ERROR','m',m);
					var LineNo =searchresults.getLineItemValue('item','line',m);
					var lineItem = searchresults.getLineItemText('item', 'item', m);
					var lineItemvalue = searchresults.getLineItemValue('item', 'item', m);
					var CompItemType = searchresults.getLineItemValue('item', 'itemtype', m);
					var CommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', m);
					var vBackOrdQty = searchresults.getLineItemValue('item', 'quantitybackordered', m);
					nlapiLogExecution('ERROR','CompItemType',CompItemType);
					nlapiLogExecution('ERROR','vBackOrdQty',vBackOrdQty);
					nlapiLogExecution('ERROR','CommittedQty',CommittedQty);
					if((CompItemType != 'Service' && CompItemType != 'NonInvtPart') && ((CommittedQty !=null && CommittedQty !='' && CommittedQty !=0)||((CommittedQty == null||CommittedQty==0||CommittedQty=='')&&(vBackOrdQty>0))))
					{	
						if(lineItem== null || lineItem =="")
							lineItem="";

						var LineQty =searchresults.getLineItemValue('item','quantity',m);
						if(LineQty== null || LineQty =="")	
							LineQty="&nbsp;";

						var BackOrdQty = searchresults.getLineItemValue('item', 'quantitybackordered', m);
						if(BackOrdQty== null || BackOrdQty =="")
							BackOrdQty="";

						nlapiLogExecution('ERROR','BackOrdQty',BackOrdQty);

						if(BackOrdQty != null && BackOrdQty != "" && parseFloat(BackOrdQty) >0)
						{
							hdnBackOrdered.setDefaultValue('T');

						}	
						//hdnBackOrdered.setDefaultValue('T');
						var CommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', m);
						if(CommittedQty== null || CommittedQty =="")
							CommittedQty="";

						// code added from FactoryMation on 01Mar13 by santosh; to restrict ItemDescription 
						var ItemDesc='';
						var ItemDescription = searchresults.getLineItemValue('item', 'description', m);
						if(ItemDescription !=null && ItemDescription!='')
						{
							ItemDesc=ItemDescription.substring(0, 280);
						}//upto here
						//var ItemDesc = searchresults.getLineItemValue('item', 'description', m);
						if(ItemDesc== null || ItemDesc =="")
							ItemDesc="";

						var Lotno = searchresults.getLineItemValue('item', 'serialnumbers', m);
						if(Lotno== null || Lotno =="")
							Lotno="";

						nlapiLogExecution('ERROR', 'Lotno',Lotno);
						var vLotnoArr=new Array();
						var vLotnoArr = Lotno.split('');
						if(vLotnoArr.length>1)
						{
							k=c;
							nlapiLogExecution('ERROR', 'vLotnoArr.length',vLotnoArr.length);
							for(var j=0;j<vLotnoArr.length;j++)
							{
								nlapiLogExecution('ERROR', 'k',k);
								var LotNumber=vLotnoArr[j];
								nlapiLogExecution('ERROR', 'LotNumber',LotNumber);
								var pos = LotNumber.indexOf("(")+1;
								nlapiLogExecution('ERROR', 'pos ', pos);
								var vQty=LotNumber.slice(pos, -1);
								nlapiLogExecution('ERROR', 'vQty', vQty);
								var vLotno=LotNumber.slice(0, pos-1);
								nlapiLogExecution('ERROR', 'vLotno', vLotno);
								nlapiLogExecution('ERROR', 'lineItemvalue', lineItemvalue);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku', k, lineItem);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hidden_skuid', k, lineItemvalue);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_itemdesc', k, ItemDesc);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_memqty', k, vQty);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_backordqty', k, BackOrdQty);                       		        		   
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_comitqty', k, CommittedQty);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lineno', k, LineNo);	
								//form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotno', m, Lotno);
								// addSerialno(lineItemvalue, itemLocation,form,k,serialno);
								GetLotno(vLotno,lineItemvalue,k,form);
								k++;
							}
							c=k;
						}
						else
						{
							nlapiLogExecution('ERROR', 'c',c);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku', c, lineItem);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hidden_skuid', c, lineItemvalue);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_itemdesc', c, ItemDesc);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_memqty', c, LineQty);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_backordqty', c, BackOrdQty);                       		        		   
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_comitqty', c, CommittedQty);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lineno', c, LineNo);	
							//form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotno', m, Lotno);
							// addSerialno(lineItemvalue, itemLocation,form,c,serialno);
							GetLotno(Lotno,lineItemvalue,c,form);
							c++;
						}
					}
				}
			}

		} 

		response.writePage(form);

	}
	else  
	{ 
		
		var form = nlapiCreateForm('Generate Bin Locations for Work orders');
		var BackorderAvailable= request.getParameter('custpage_hdnbackordered');
		var AssemblyItemText= request.getParameter('custpage_hdnitemtext');

		nlapiLogExecution('ERROR', 'BackorderAvailable',BackorderAvailable);
		if(BackorderAvailable == 'T') 
		{
			//var woidno = request.getParameter('custpage_hdnwono');
			nlapiLogExecution('ERROR','Backordered Workorder ', AssemblyItemText);
			var ErrorMsg = nlapiCreateError('CannotAllocate','Work Order ' + AssemblyItemText + ' has Backordered quantity components ', true);
			throw ErrorMsg;
		}
		else
		{
			var woidno = request.getParameter('custpage_hdnwono');
			var woidname = request.getParameter('custpage_hdnwoname');
			var filter=new Array();
			filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no','null','is',woidno));
			filter.push(new nlobjSearchFilter('custrecord_wms_status_flag','null','anyof',['9','8','14']));
			var searchrecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,null);
			nlapiLogExecution('ERROR', 'Time Stamp after search record on open task',TimeStampinSec());
			if(searchrecord != null && searchrecord != "")
			{
				nlapiLogExecution('ERROR','Already allocated ');
				var ErrorMsg = nlapiCreateError('CannotAllocate','Bin locations are already generated for this Work Order ' + woidname, true);
				throw ErrorMsg; //throw this error object, do not catch it
			}	
			else
			{	
			//GenerateLocationsPOSTRequest(request, response); // For Schedule purpose
			nlapiLogExecution('DEBUG', 'Invoking Schedule Script Starts', TimeStampinSec());	
			
			var lineCount = request.getLineItemCount('custpage_deliveryord_items');
			nlapiLogExecution('ERROR','lineCount',lineCount);

			
			var vSkuArray='';
			var vOldLOTnoArray='';
			var vNewLOTnoArray ='';
			var gridLinenoArray='';
			var vCommentsArray='';
			for(var k=1;k<=lineCount;k++){
				//vSkuArray.push(request.getLineItemValue('custpage_deliveryord_items', 'custpage_hidden_skuid', k));
				if(vSkuArray !=null  && vSkuArray !='')
					vSkuArray=vSkuArray +','+request.getLineItemValue('custpage_deliveryord_items', 'custpage_hidden_skuid', k);
				else
					vSkuArray=request.getLineItemValue('custpage_deliveryord_items', 'custpage_hidden_skuid', k);
				
				//vOldLOTnoArray.push(request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotno', k));
				if(vOldLOTnoArray !=null  && vOldLOTnoArray !='')
					vOldLOTnoArray=vOldLOTnoArray +','+request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotno', k);
				else
					vOldLOTnoArray=request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotno', k);
				
				//vNewLOTnoArray.push(request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_lotno', k));
				if(vNewLOTnoArray !=null  && vNewLOTnoArray !='')
					vNewLOTnoArray=vNewLOTnoArray +','+request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_lotno', k);
				else
					vNewLOTnoArray=request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_lotno', k);
				
				//gridLinenoArray.push(request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', k));
				if(gridLinenoArray !=null  && gridLinenoArray !='')
					gridLinenoArray=gridLinenoArray +','+request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', k);
				else
					gridLinenoArray=request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', k);
				
				//vCommentsArray.push(request.getLineItemValue('custpage_deliveryord_items', 'custpage_comments', k));
				if(vCommentsArray !=null  && vCommentsArray !='')
					vCommentsArray=vCommentsArray +','+request.getLineItemValue('custpage_deliveryord_items', 'custpage_comments', k);
				else
					vCommentsArray=request.getLineItemValue('custpage_deliveryord_items', 'custpage_comments', k);
			}
			nlapiLogExecution('ERROR','vSkuArray',vSkuArray);
			nlapiLogExecution('ERROR','vOldLOTnoArray',vOldLOTnoArray);
			nlapiLogExecution('ERROR','vNewLOTnoArray',vNewLOTnoArray);
			nlapiLogExecution('ERROR','gridLinenoArray',gridLinenoArray);
			nlapiLogExecution('ERROR','vCommentsArray',vCommentsArray);
			
			var eBizWorkOrder=request.getParameter('custpage_wo');
			var woidno = request.getParameter('custpage_hdnwono');

			nlapiLogExecution('ERROR','eBizWorkOrder',eBizWorkOrder);
			nlapiLogExecution('ERROR','woidno',woidno);
			
			

				var param = new Array();
				param['custscript_workorder'] = eBizWorkOrder;
				param['custscript_woidno'] = woidno;
				param['custscript_linecount'] = lineCount;
				param['custscript_vskuarray'] = vSkuArray;
				param['custscript_voldlotnoarray'] = vOldLOTnoArray;
				param['custscript_vnewlotnoarray'] = vNewLOTnoArray;
				param['custscript_gridlinenoarray'] = gridLinenoArray;
				param['custscript_vcommentsarray'] = vCommentsArray;
				nlapiScheduleScript('customscript_ebiz_wogenloc_scheduler', null,param);

				showInlineMessage(form, 'Confirmation', 'Location generation has been initiated for WO# '+ eBizWorkOrder);

				response.writePage(form);
				nlapiLogExecution('DEBUG', 'Invoking Schedule Script Ends', TimeStampinSec());
			}
		}	


	}
}
function GetLotno(Lotno,lineItemvalue,m,form)
{
	if(Lotno != "" && Lotno != null)
	{
		nlapiLogExecution('ERROR', 'GetLotno', Lotno);	
		var filterbatch = new Array();
		filterbatch[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', Lotno);
		filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', lineItemvalue);
		var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch);
		if(receiptsearchresults!=null)
		{
			var lotid= receiptsearchresults[0].getId();
			nlapiLogExecution('ERROR', 'lotid', lotid);
			//serialno.setDefaultValue(lotid);
			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotno', m, lotid);
			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlotno', m, lotid);
		}

	}	

}
function addSerialno(temparray,Location,form,serialno,kitItemStatus){

	nlapiLogExecution('ERROR', 'temparray',temparray.length);
	nlapiLogExecution('ERROR', 'kitItemStatus',kitItemStatus);
	nlapiLogExecution('ERROR', 'Location',Location);
	nlapiLogExecution('ERROR', 'temparray',temparray);
	var filterStatus = new Array();

	filterStatus.push(new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T'));
	filterStatus.push(new nlobjSearchFilter('custrecord_activeskustatus', null, 'is','T'));
	if(Location != null && Location != '')
		filterStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof',Location));
	var searchresultsStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filterStatus, null);
	var vStatusArr=new Array();
	if(searchresultsStatus != null && searchresultsStatus != '')
	{
		for(var k=0;k<searchresultsStatus.length;k++)
		{
			vStatusArr.push(searchresultsStatus[k].getId());
		}
	}
	nlapiLogExecution('ERROR', 'vStatusArr',vStatusArr);

	var filter= new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof', temparray));
	//if(kitItemStatus!=null && kitItemStatus!='')
	//	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof', kitItemStatus));
	if(vStatusArr != null && vStatusArr != '' && vStatusArr.length>0)
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof', vStatusArr));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'anyof', Location));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));
	filter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']));


	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'noneof','@NONE@'));




	var columns = new Array();	
	//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku',null,'group'));			 
	columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot',null,'group'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty',null,'sum'));
	//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
	//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
	columns[0].setSort(true);
	columns[1].setSort();
	columns[2].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filter, columns);
	/*
	var filterLotno = new Array();
	filterLotno[0] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', temparray);
	filterLotno[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', Location);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[2] = new nlobjSearchColumn('custrecord_ebizsku');
	columns[2].setSort(true);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterLotno,columns);*/
	if(searchresults!=null)
	{
		nlapiLogExecution('ERROR', 'Lotnoresult', searchresults.length);

		for(var i=0;i<searchresults.length;i++)
		{
			serialno.addSelectOption(searchresults[i].getValue('custrecord_ebiz_inv_lot',null,'group'), (searchresults[i].getText('custrecord_ebiz_inv_lot',null,'group')+"(Qty : "+searchresults[i].getValue('custrecord_ebiz_avl_qty',null,'sum') + ")" +"_"+searchresults[i].getText('custrecord_ebiz_inv_sku',null,'group')));


		}
	}

}
