/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_Replenishment_Confirmation.js,v $
 *     	   $Revision: 1.13.2.11.4.3.4.36.2.3 $
 *     	   $Date: 2015/11/14 14:31:33 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Replenishment_Confirmation.js,v $
 * Revision 1.13.2.11.4.3.4.36.2.3  2015/11/14 14:31:33  grao
 * 2015.2 Issue Fixes 201415608
 *
 * Revision 1.13.2.11.4.3.4.36.2.2  2015/11/14 06:45:01  grao
 * 2015.2 Issue Fixes 201415604
 *
 * Revision 1.13.2.11.4.3.4.36.2.1  2015/11/12 15:03:20  skreddy
 * case 201415501
 * Boombah SB  issue fix
 *
 * Revision 1.13.2.11.4.3.4.36  2015/08/07 19:08:04  snimmakayala
 * Case#: #201413917
 *
 * Revision 1.13.2.11.4.3.4.35  2015/08/07 17:47:27  snimmakayala
 * no message
 *
 * Revision 1.13.2.11.4.3.4.34  2015/07/14 13:23:51  schepuri
 * case#  201413378
 *
 * Revision 1.13.2.11.4.3.4.33  2015/07/13 13:25:14  schepuri
 * case# 201413379,201413461
 *
 * Revision 1.13.2.11.4.3.4.32  2015/07/03 15:40:46  skreddy
 * Case# 201412907 & 201412906
 * Signwarehouse SB issue fix
 *
 * Revision 1.13.2.11.4.3.4.31  2015/05/27 15:57:11  nneelam
 * case# 201412896
 *
 * Revision 1.13.2.11.4.3.4.30  2015/05/15 22:36:56  nneelam
 * case# 201412813
 *
 * Revision 1.13.2.11.4.3.4.29  2015/05/11 15:40:38  skreddy
 * Case# 201412702
 * Hilmot  SB issue fix
 *
 * Revision 1.13.2.11.4.3.4.28  2015/05/08 15:49:12  nneelam
 * case# 201412702
 *
 * Revision 1.13.2.11.4.3.4.27  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.13.2.11.4.3.4.26  2015/02/27 15:58:34  sponnaganti
 * Case# 201411719
 * DCD Prod Issue fix
 *
 * Revision 1.13.2.11.4.3.4.25  2015/01/21 14:31:29  rrpulicherla
 * Case#201411317
 *
 * Revision 1.13.2.11.4.3.4.24  2015/01/21 14:14:58  nneelam
 * case# 201411342
 *
 * Revision 1.13.2.11.4.3.4.23  2014/12/23 06:57:17  spendyala
 * case # 201411303
 *
 * Revision 1.13.2.11.4.3.4.22  2014/12/05 06:53:32  sponnaganti
 * case# 201222193
 * Jawbone Prod issue fix
 *
 * Revision 1.13.2.11.4.3.4.21  2014/11/19 14:52:20  schepuri
 * case#  201411078
 *
 * Revision 1.13.2.11.4.3.4.20  2014/08/28 15:35:55  skavuri
 * Case# 20149805 Std Bundle issue fixed
 *
 * Revision 1.13.2.11.4.3.4.19  2014/07/01 06:39:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Alphacommchanges
 *
 * Revision 1.13.2.11.4.3.4.18  2014/06/13 09:40:49  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.13.2.11.4.3.4.17  2014/06/12 15:28:04  sponnaganti
 * case# 20148141
 * Stnd Bundle Issue Fix
 *
 * Revision 1.13.2.11.4.3.4.16  2014/06/06 07:25:28  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.13.2.11.4.3.4.15  2014/05/30 00:34:22  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.13.2.11.4.3.4.14  2014/01/09 14:08:58  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.13.2.11.4.3.4.13  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.13.2.11.4.3.4.12  2013/11/20 20:45:48  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20125767
 * Replen Exceptions
 *
 * Revision 1.13.2.11.4.3.4.11  2013/11/14 15:41:55  skreddy
 * Case# 20125719 & 20125658
 * Afosa SB issue fix
 *
 * Revision 1.13.2.11.4.3.4.10  2013/11/14 14:33:50  rmukkera
 * Case # 20125742
 *
 * Revision 1.13.2.11.4.3.4.9  2013/11/12 06:40:59  skreddy
 * Case# 20125658 & 20125602
 * Afosa SB issue fix
 *
 * Revision 1.13.2.11.4.3.4.8  2013/11/01 13:48:35  schepuri
 * 20125477
 *
 * Revision 1.13.2.11.4.3.4.7  2013/11/01 13:39:29  rmukkera
 * Case# 20125479
 *
 * Revision 1.13.2.11.4.3.4.6  2013/06/04 07:44:29  rmukkera
 * Issue Fix For
 * RF Replenishment Confirmation Issue - Even though user keyed in the quantity, in the next screen system showing expected quantity instead of keyed in quantity.
 *
 * Revision 1.13.2.11.4.3.4.5  2013/05/14 14:16:37  schepuri
 * RF Replen confirm based on filters
 *
 * Revision 1.13.2.11.4.3.4.4  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.13.2.11.4.3.4.3  2013/04/02 14:16:04  rmukkera
 * issue Fix related to vaidating the serailno
 *
 * Revision 1.13.2.11.4.3.4.2  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.13.2.11.4.3.4.1  2013/03/05 14:46:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.13.2.11.4.3  2012/10/11 14:50:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * REPLEN CHANGES
 *
 * Revision 1.13.2.11  2012/08/27 16:28:52  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * fiskreplenissues
 *
 * Revision 1.13.2.10  2012/08/24 16:09:57  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Replen confirm
 *
 * Revision 1.13.2.9  2012/08/09 07:05:31  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Round turn in scanning other Items.
 *
 * Revision 1.13.2.8  2012/08/06 06:51:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Item Description.
 *
 * Revision 1.13.2.7  2012/07/12 14:47:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.13.2.6  2012/06/04 11:47:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * HUBPEN - REPLEN ISSUE FIXES
 *
 * Revision 1.13.2.5  2012/05/07 12:01:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.13.2.4  2012/03/22 08:09:08  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Replenishment
 *
 * Revision 1.13.2.3  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.13.2.2  2012/02/21 13:25:28  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.13.2.1  2012/02/07 12:40:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Replen Issue Fixes
 *
 * Revision 1.15  2012/01/17 13:53:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.14  2012/01/09 13:15:23  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.13  2012/01/05 17:24:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/12/09 22:01:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move changes
 *
 * Revision 1.11  2011/09/19 10:36:39  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/07/20 12:57:48  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.4  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function Replenishment_Confirmation(request, response){

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);


	if (request.getMethod() == 'GET') {
		var getNo = request.getParameter('custparam_repno');
		var getBegLoc = request.getParameter('custparam_repbegloc');
		var getPickLoc = request.getParameter('custparam_reppickloc');
		var getPickLocNo = request.getParameter('custparam_reppicklocno');
		var getSku = request.getParameter('custparam_repsku');
		var getSkuNo = request.getParameter('custparam_repskuno');
		var getExpQty = request.getParameter('custparam_torepexpqty');
		var getid = request.getParameter('custparam_repid');
		var vClustno = request.getParameter('custparam_clustno');
		var whlocation = request.getParameter('custparam_whlocation');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var fromlpno=request.getParameter('custparam_fromlpno');
		var tolpno=request.getParameter('custparam_tolpno');
		var batchno=request.getParameter('custparam_batchno');
		var nextlocation=request.getParameter('custparam_nextlocation');
		var nextqty=request.getParameter('custparam_nextqty');
		var nextitem=request.getParameter('custparam_nextitem');
		var nextitemno=request.getParameter('custparam_nextitemno');
		var serialnos=request.getParameter('custparam_serialnoID');
		var taskpriority = request.getParameter('custparam_entertaskpriority');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var getNumber = request.getParameter('custparam_number');
		var getreportNo=request.getParameter('custparam_enterrepno');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');
		var remainingreplenqty = request.getParameter('custparam_remainingreplenqty');
		var enterfromloc = request.getParameter('custparam_enterfromloc');


		nlapiLogExecution('ERROR', 'End Location Id', getPickLocNo);
		nlapiLogExecution('ERROR', 'getSkuNo', getSkuNo);
		nlapiLogExecution('ERROR', 'End Location', getPickLoc);
		nlapiLogExecution('ERROR', 'fromlpno', fromlpno);
		nlapiLogExecution('ERROR', 'serialnos', serialnos);
		nlapiLogExecution('ERROR', 'taskpriority in GET', taskpriority);
		nlapiLogExecution('ERROR', 'RecordCount in GET', RecordCount);
		nlapiLogExecution('ERROR', 'entereditem', getitem);
		nlapiLogExecution('ERROR', 'entereditemid', getitemid);
		nlapiLogExecution('ERROR', 'enteredloc', getloc);
		nlapiLogExecution('ERROR', 'enteredlocid', getlocid);		
		nlapiLogExecution('ERROR', 'taskpriority', taskpriority);
		nlapiLogExecution('ERROR', 'getreportNo', getreportNo);
		nlapiLogExecution('ERROR', 'getNumber', getNumber);
		nlapiLogExecution('ERROR', 'remainingreplenqty', remainingreplenqty);
		nlapiLogExecution('ERROR', 'enterfromloc', enterfromloc);


		var fields=new Array();
		fields[0]='name';
		fields[1]='custitem_ebizdescriptionitems';
		/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle ***/
		fields[2]='description';
		/*** up to here***/
		var rec=nlapiLookupField('item', getSkuNo, fields);
		var sku=rec.name;
		var skudesc=rec.custitem_ebizdescriptionitems;
		/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle ***/
		if(skudesc==null || skudesc=='')
			skudesc=rec.description;
		nlapiLogExecution('ERROR', 'sku',sku);
		var Replenqtyresults= getReplentotqty(getNo,getSku,getPickLocNo,fromlpno);
		if(Replenqtyresults!=null && Replenqtyresults!='')
		{
			/*var Replenqty = Replenqtyresults[0].getValue('custrecord_expe_qty',null,'sum');*/
			var Replenqty = Replenqtyresults[0].getValue('custrecord_act_qty',null,'sum');
		}
		/*** up to here***/
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage',getLanguage);

		//case20125658 and 20125602 start:spanish conversion
		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "CONFIRMACI&#211;N DE REPOSICI&#211;N";
			st1 = "ART&#205;CULO :";
			st2 = "DESCRIPCI&#211;N DE ART&#205;CULO :";
			st3 = "CANTIDAD :";
			st4 = "ENTER / SCAN PARA UBICACI&#211;N";
			st5 = "CONFIRMAR";
			st6 = "ANTERIOR";
			st7 = "SIGUIENTE";
			st8 = "LOTE";
			st9 = "Introduzca la cantidad";
			st10 = "IR A LA UBICACI&#211;N :";

		}
		else
		{
			st0 = "REPLENISHMENT CONFIRMATION";
			st1 = "ITEM:";
			st2 = "ITEM DESCRIPTION: ";
			st3 = "QUANTITY: ";
			st4 = "ENTER/SCAN TO LOCATION:";
			st5 = "CONFIRM";
			st6 = "PREV";
			st7 = "NEXT";
			st8 = "BATCH";
			st9 = "ENTER QUANTITY";
			st10 ="GO TO LOCATION :";

		}
		//case20125658 and 20125602 end
		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>"+st0+"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";     
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends

		//	html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";

		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		/*html = html + " document.getElementById('enterlp').focus();";

		html = html + "</script>";  */

		//html = html + " document.getElementById('entertoloc').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>FROM LOCATION:  <label>" + getBegLoc + "</label></td>";
		html = html + "				<input type='hidden' name='hdnNo' value=" + getNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBegLoc + ">";
		html = html + "				<input type='hidden' name='hdnSku' value='" + getSku + "'>";
		html = html + "				<input type='hidden' name='hdnExpQty' value=" + getExpQty + ">";
		html = html + "				<input type='hidden' name='hdnPickLoc' value=" + getPickLoc + ">";
		html = html + "				<input type='hidden' name='hdnPickLocNo' value=" + getPickLocNo + ">";
		html = html + "				<input type='hidden' name='hdnid' value=" + getid + ">";
		html = html + "				<input type='hidden' name='hdnclustno' value=" + vClustno + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
		html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
		html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
		html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
		html = html + "				<input type='hidden' name='hdnSkuNo' value=" + getSkuNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + batchno + ">";
		html = html + "				<input type='hidden' name='hdnnitem' value=" + nextitem + ">";
		html = html + "				<input type='hidden' name='hdnnextitemno' value=" + nextitemno + ">";
		html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
		html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
		html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
		html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
		html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
		html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
		html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
		html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
		html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
		html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
		html = html + "				<input type='hidden' name='hdnremainingreplenqty' value=" + remainingreplenqty + ">";
		html = html + "				<input type='hidden' name='hdnenterfromloc' value=" + enterfromloc + ">";
		html = html + "				<input type='hidden' name='hdnnserialnos' value=" + serialnos + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st10+"  <label>" + getPickLoc + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>  "+st1+"  <label>" + sku + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st2+"  <label>" + skudesc + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle ***/
		//html = html + "				<td align = 'left'> "+ st3+":  <label>" + Replenqty + "</label></td>";
		/*** upto here***/
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st4+":";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entertoloc' id='entertoloc' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st5+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					"+st6+" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entertoloc').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);        
	}
	else {
		try {
			var Reparray = new Array();
			var optedEvent = request.getParameter('cmdPrevious');
			var gettoloc = request.getParameter('entertoloc');
			var gethdnPickLocation = request.getParameter('hdnPickLoc');
			var vClustNo = request.getParameter('hdnclustno');
			var reportNo=request.getParameter('hdnNo');
			var sku=request.getParameter('hdnSku');
			var skuno=request.getParameter('hdnSkuNo');
			//case20125658 and 20125602 start:spanish conversion
			var getLanguage = request.getParameter('hdngetLanguage');
			Reparray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);
			//case20125658 and 20125602 end
			var RecordCount = request.getParameter('hdnRecCount');	
			nlapiLogExecution('ERROR', 'RecordCount', RecordCount);


			var getLanguage = request.getParameter('custparam_language');
			nlapiLogExecution('ERROR', 'getLanguage',getLanguage);

			var st0,st1,st2;
			if( getLanguage == 'es_ES' || getLanguage =='es_AR')
			{
				st0 = "NO V&#193;LIDO SELECCI&#211;N CARA UBICACI&#211;N";
				st1 = "UBICACI&#211;N ESTADO INACTIVO";
				st2 = "La ubicaci?n es NULL";


			}
			else
			{
				st0 = "INVALID PICK FACE LOCATION";
				st1 = "LOCATION INACTIVE STATUS";
				st2 = "PLEASE ENTER VALID TO LOCATION";


			}


			if(request.getParameter('hdngetNumber')!= null && request.getParameter('hdngetNumber') != "")
				getNumber = request.getParameter('hdngetNumber');
			else
				getNumber=0;	
			var recid;
			var Invrecid;

			nlapiLogExecution('ERROR', 'gettoloc', '|'+gettoloc+'|');
			nlapiLogExecution('ERROR', 'gethdnPickLocation', '|'+gethdnPickLocation+'|');
			nlapiLogExecution('ERROR','hdnwhlocation',request.getParameter('hdnwhlocation'));
			nlapiLogExecution('ERROR','skuno',request.getParameter('hdnSkuNo'));

			Reparray["custparam_repno"] = request.getParameter('hdnNo');
			Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');
			Reparray["custparam_repsku"] = request.getParameter('hdnSku');
			Reparray["custparam_torepexpqty"] = request.getParameter('hdnExpQty');
			Reparray["custparam_repid"] = request.getParameter('hdnid');
			Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
			Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnPickLocNo');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnPickLoc');
			Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
			Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
			Reparray["custparam_batchno"] = request.getParameter('hdnbatchno');
			var vhdnBeginLocation = request.getParameter('hdnBeginLocation');
			nlapiLogExecution('ERROR','vhdnBeginLocation',vhdnBeginLocation);
			Reparray["custparam_noofrecords"] = RecordCount;

			Reparray["custparam_error"] = st0;//'INVALID PICK FACE LOCATION';
			Reparray["custparam_screenno"] = 'R6';
			Reparray["custparam_clustno"] = vClustNo;
			Reparray["custparam_nextitem"] = request.getParameter('hdnnitem');
			Reparray["custparam_nextitemno"] = request.getParameter('hdnnextitemno');
			Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
			Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_serialnoID"]=request.getParameter('hdnnserialnos');
			//Reparray["custparam_nextitemno"] = request.getParameter('nextitemno');
			Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');
			Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');
			Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
			Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
			Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
			Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
			Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
			Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
			Reparray["custparam_number"] = request.getParameter('hdngetNumber');
			Reparray["custparam_remainingreplenqty"] = request.getParameter('hdnremainingreplenqty');
			Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

			var getitemid = request.getParameter('hdngetitemid');
			var getlocid = request.getParameter('hdngetlocid');
			var taskpriority = request.getParameter('hdntaskpriority');
			var taskpriority = request.getParameter('hdntaskpriority');
			var getreportNo = request.getParameter('hdngetreportno');
			var replenType = request.getParameter('hdnReplenType');

			var expQty= request.getParameter('hdnExpQty');
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
			Reparray["custparam_repexpqty"]=expQty;
			var beginLocationId = request.getParameter('hdnbeginLocId');

			nlapiLogExecution('ERROR','hdntoLpno',request.getParameter('hdntoLpno'));
			nlapiLogExecution('ERROR','hdnExpQty',request.getParameter('hdnExpQty'));
			nlapiLogExecution('ERROR','hdnremainingreplenqty',request.getParameter('hdnremainingreplenqty'));
			//case# 20148141 starts
			//var systemRule=GetSystemRuleForReplen();
			var systemRule=GetSystemRuleForReplen(Reparray["custparam_whlocation"]);
			//case# 20148141 ends

			var recNo=0;
			if(getNumber!=0)
				recNo=parseFloat(getNumber);
			
			if(isNaN(recNo))
			{
				recNo = 0;
			}
			nlapiLogExecution('ERROR', 'recNo in taskpriority ',recNo);	
			var searchresults =getReplenTasks(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType,Reparray["custparam_whlocation"]);
			nlapiLogExecution('ERROR', 'searchresults', 'searchresults');
			if (searchresults != null && searchresults.length > 0) {
				nlapiLogExecution('ERROR', 'searchresults', 'searchresults.length');
				var SOSearchResult = searchresults[recNo];
//				if(searchresults.length > 1)
//				{
//				var SOSearchnextResult = searchresults[1];
//				Reparray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
//				Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty');

//				}
				nlapiLogExecution('ERROR', 'test2', 'test2');
				if(systemRule=='Y')
				{
					Reparray["custparam_repbegloc"] = SOSearchResult.getText('custrecord_actbeginloc');
					Reparray["custparam_repbeglocId"] = SOSearchResult.getValue('custrecord_actbeginloc');
					Reparray["custparam_repsku"] = SOSearchResult.getText('custrecord_sku');
					Reparray["custparam_repexpqty"] = SOSearchResult.getValue('custrecord_expe_qty');
					Reparray["custparam_repskuno"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
					Reparray["custparam_repid"] =SOSearchResult.getId();
					Reparray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
					Reparray["custparam_reppicklocno"] = SOSearchResult.getValue('custrecord_actendloc');
					Reparray["custparam_reppickloc"] = SOSearchResult.getText('custrecord_actendloc');
					Reparray["custparam_fromlpno"] = SOSearchResult.getValue('custrecord_from_lp_no');
					Reparray["custparam_tolpno"] = SOSearchResult.getValue('custrecord_lpno');
					Reparray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
					Reparray["custparam_noofrecords"] = searchresults.length;
					Reparray["custparam_clustno"] = vClustNo;
					Reparray["custparam_repno"] = reportNo;
					recid=SOSearchResult.getId();
					Invrecid=SOSearchResult.getValue('custrecord_invref_no');
					nlapiLogExecution('ERROR', 'test2', 'test2');
				}
				else
				{
					//Temporary Fix
					SOSearchResult = searchresults[0];

					Reparray["custparam_repbegloc"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
					Reparray["custparam_repbeglocId"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
					Reparray["custparam_repsku"] = SOSearchResult.getText('custrecord_sku',null,'group');
					Reparray["custparam_repexpqty"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
					Reparray["custparam_repskuno"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');					
					Reparray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
					Reparray["custparam_reppicklocno"] = SOSearchResult.getValue('custrecord_actendloc',null,'group');
					Reparray["custparam_reppickloc"] = SOSearchResult.getText('custrecord_actendloc',null,'group');			
					Reparray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no',null,'group');
					Reparray["custparam_noofrecords"] = SOSearchResult.length;
					Reparray["custparam_clustno"] = vClustNo;
					Reparray["custparam_repno"] = reportNo;
				}

			}	

			if (sessionobj!=context.getUser()) {
				try
				{

					if(sessionobj==null || sessionobj=='')
					{
						sessionobj=context.getUser();
						context.setSessionObject('session', sessionobj); 
					}

					if (optedEvent == 'F7') {
						nlapiLogExecution('ERROR', 'REPLENISHMENT Confirmation F7 Pressed');

						response.sendRedirect('SUITELET', 'customscript_replen_lp', 'customdeploy_rf_replen_lp_di', false, Reparray);
						return;
					}
					else {
						if (gettoloc != '') {

							if (trim(gettoloc) == trim(gethdnPickLocation)) {
								nlapiLogExecution('ERROR', 'test1','test1');
								var locationvalid=getLocation(gettoloc);
								nlapiLogExecution('ERROR', 'locationvalid', locationvalid);
								if(locationvalid==true)
								{
									//case# 20148141 starts
									nlapiLogExecution('ERROR', 'getitemid in', Reparray["custparam_repskuno"]);
									var	itemDims = getSKUCubeAndWeight(Reparray["custparam_repskuno"],"");
									var itemCubevalue = itemDims[0];
									var getBaseUomQtantiy= itemDims[1];

									var TotalItemCubevalue=CubeCapacity(Reparray["custparam_repexpqty"],getBaseUomQtantiy,itemCubevalue);

									nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCubevalue );


									nlapiLogExecution('DEBUG', 'Reparray["custparam_reppicklocno"] ', Reparray["custparam_reppicklocno"]);
									var binLocationRemCube = GeteLocCube(Reparray["custparam_reppicklocno"]);

									var remCube = parseFloat(binLocationRemCube) - parseFloat(TotalItemCubevalue);
									nlapiLogExecution('DEBUG', 'remCube ', remCube);
									if(remCube<0 || remCube<TotalItemCubevalue){
										Reparray["custparam_error"] = 'INSUFFICIENT REMAINING CUBE';
										Reparray["custparam_screenno"] = 'R6';
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
										nlapiLogExecution('DEBUG', 'INSUFFICIENT REMAINING CUBE ', 'INSUFFICIENT REMAINING CUBE');
										return false;	
									}

									//case# 20148141 ends
									nlapiLogExecution('ERROR', 'test1','test1');
									response.sendRedirect('SUITELET', 'customscript_replen_tolp', 'customdeploy_rf_replen_tolp_di', false, Reparray);
								}
								else
								{

									var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
									var expectedqty = transaction.getFieldValue('custrecord_expe_qty');
									transaction.setFieldValue('custrecord_wms_status_flag', 32);
									var result = nlapiSubmitRecord(transaction);

									var invtrecord=nlapiLoadRecord('customrecord_ebiznet_createinv', Invrecid);
									var Invallocqty = invtrecord.getFieldValue('custrecord_ebiz_alloc_qty');
									nlapiLogExecution('ERROR', 'Invallocqty',Invallocqty);
									nlapiLogExecution('ERROR', 'expectedqty',expectedqty);
									if((Invallocqty!=null && Invallocqty!='' && Invallocqty>0) && (expectedqty!=null && expectedqty!='' && expectedqty>0))
										invtrecord.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(Invallocqty)- parseInt(expectedqty));

									var invtrecid = nlapiSubmitRecord(invtrecord, false, true);

									Reparray["custparam_error"] = st1;//'LOCATION INACTIVE STATUS';
									Reparray["custparam_screenno"] = 'R1';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
									nlapiLogExecution('ERROR', 'Location IS INACTIVE');
								}
								//case# 20148141 starts
								//var systemRule=GetSystemRuleForReplen();
								nlapiLogExecution('DEBUG', 'Reparray["custparam_whlocation"]1', Reparray["custparam_whlocation"] );
								
								var rectype="";
									var serin="";
									var serout="";
									
								if(request.getParameter('custparam_repskuno') !=null && request.getParameter('custparam_repskuno') !='')
								{
									var ItemFilter=new Array();
									ItemFilter[0]=new nlobjSearchFilter("internalid",null,"anyof",request.getParameter('custparam_repskuno'));

									var Itemcolumn=new Array();
									Itemcolumn[0]=new nlobjSearchColumn("type");
									Itemcolumn[1]=new nlobjSearchColumn("custitem_ebizserialin");
									Itemcolumn[2]=new nlobjSearchColumn("custitem_ebizserialout");

									var ItemSearchResults=nlapiSearchRecord("item",null,ItemFilter,Itemcolumn);
									if(ItemSearchResults!=null && ItemSearchResults!="")
									{

										rectype=ItemSearchResults[0].recordType;
										serin=ItemSearchResults[0].getValue("custitem_ebizserialin");
										serout=ItemSearchResults[0].getValue("custitem_ebizserialout");

									}

								}
								
								
								var systemRule=GetSystemRuleForReplen(Reparray["custparam_whlocation"]);
								//case# 20148141 ends
								if(systemRule=='Y')
								{
									
									
									if (rectype == "serializedinventoryitem" || rectype == "serializedassemblyitem" || serout == "T")
									{
										UpdateSerialNumbers(request.getParameter('custparam_repskuno'),Reparray["custparam_repbeglocId"], Reparray["custparam_fromlpno"],Reparray["custparam_tolpno"],Reparray["custparam_reppicklocno"]);
										
									}
									response.sendRedirect('SUITELET', 'customscript_replen_tolp', 'customdeploy_rf_replen_tolp_di', false, Reparray);
								}
								else
								{

									//case# 20148141 starts
									nlapiLogExecution('DEBUG', 'getitemid ', Reparray["custparam_repskuno"]);
									var	itemDims = getSKUCubeAndWeight(Reparray["custparam_repskuno"],"");
									var itemCubevalue = itemDims[0];
									var getBaseUomQtantiy= itemDims[1];

									var TotalItemCubevalue=CubeCapacity(Reparray["custparam_repexpqty"],getBaseUomQtantiy,itemCubevalue);

									nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCubevalue );


									nlapiLogExecution('DEBUG', 'Reparray["custparam_reppicklocno"] ', Reparray["custparam_reppicklocno"]);
									var binLocationRemCube = GeteLocCube(Reparray["custparam_reppicklocno"]);

									var remCube = parseFloat(binLocationRemCube) - parseFloat(TotalItemCubevalue);
									nlapiLogExecution('DEBUG', 'remCube ', remCube);
									if(remCube<0){
										Reparray["custparam_error"] = 'INSUFFICIENT REMAINING CUBE';
										Reparray["custparam_screenno"] = 'R6';
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
										nlapiLogExecution('DEBUG', 'INSUFFICIENT REMAINING CUBE ', 'INSUFFICIENT REMAINING CUBE');
										return false;	
									}

									//case# 20148141 ends
									var filters = new Array();
									var vOrdFormat='R';
									var vOrdFormat;

									//if(reportNo!=null && reportNo!='')
									if(reportNo != null && reportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
									{
										nlapiLogExecution('ERROR','replenreportNo',reportNo);
										filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
										vOrdFormat='R';
									}

									if(request.getParameter('custparam_repskuno')!=null && request.getParameter('custparam_repskuno')!='')
									{
										nlapiLogExecution('ERROR','Replenitem',request.getParameter('custparam_repskuno'));
										filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', request.getParameter('custparam_repskuno')));
										vOrdFormat=vOrdFormat + 'I';
									}

									if(request.getParameter('custparam_repbeglocId')!=null && request.getParameter('custparam_repbeglocId')!='')
									{
										nlapiLogExecution('ERROR','Replenprimaryloc',request.getParameter('custparam_repbeglocId'));
										filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('custparam_repbeglocId')));
										vOrdFormat=vOrdFormat + 'L';
									}

									if(taskpriority!=null && taskpriority!='')
									{
										nlapiLogExecution('ERROR','Replenpriority',taskpriority);
										filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
										vOrdFormat=vOrdFormat + 'P';
									}

									if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='')
									{
										nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
										filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));
									}

									if(request.getParameter('hdnenterfromloc')!=null && request.getParameter('hdnenterfromloc')!='' && request.getParameter('hdnenterfromloc')!='null')
									{
										nlapiLogExecution('ERROR','custrecord_actbeginloc',request.getParameter('hdnenterfromloc'));
										filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('hdnenterfromloc')));
									}
									/*if(request.getParameter('hdnbatchno')!=null && request.getParameter('hdnbatchno')!='' && request.getParameter('hdnbatchno')!='null')
							{
								nlapiLogExecution('ERROR','batchno',request.getParameter('hdnbatchno'));
								filters.push(new nlobjSearchFilter('custrecord_batch_no', null, 'is', request.getParameter('hdnbatchno')));

							}*/

									filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
									filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
									//filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));
									filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
									columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
									columns[2] = new nlobjSearchColumn('custrecord_sku');
									columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
									columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
									columns[5] = new nlobjSearchColumn('custrecord_wms_location');
									columns[6] = new nlobjSearchColumn('custrecord_actendloc');
									columns[7] = new nlobjSearchColumn('custrecord_lpno');
									columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
									columns[9] = new nlobjSearchColumn('name');	
									columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	
									columns[11] = new nlobjSearchColumn('custrecord_sku_status');	
									columns[12] = new nlobjSearchColumn('custrecord_act_qty');
									columns[13] = new nlobjSearchColumn('custrecord_reversalqty');
									columns[14] = new nlobjSearchColumn('custrecord_ebiz_task_no');
									columns[15] = new nlobjSearchColumn('custrecord_invref_no');
									columns[16] = new nlobjSearchColumn('custrecord_batch_no');
									columns[1].setSort(false);
									columns[2].setSort(false);
									columns[3].setSort(false);

									var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
									var totalexpectedQty=0;var varsku='';var whLocation='';var totalQty=0;var binLocation='';
									var beginLocation='';var ebizTaskno='';var beginlocation='';var skuno='';var remainingreplenqty='';
									var tempreportno='';
									var batch='';
									var fromLPno="";
									var FrmInvtRefNo=new Array();
									var temparray=new Array();
									if(searchresults!=null && searchresults.length>0)
									{
										for (var s = 0; s < searchresults.length; s++) 
										{
											nlapiLogExecution('ERROR', 'searchresults if lp is same', searchresults.length);
											var Rid =searchresults[s].getId();
											whLocation = searchresults[s].getValue('custrecord_wms_location');
											tempreportno=searchresults[s].getValue('name');
											var expectedqty =searchresults[s].getValue('custrecord_expe_qty');
											var qty =searchresults[s].getValue('custrecord_act_qty');
											nlapiLogExecution('ERROR', 'qty', qty);
											if(qty==null || qty=='' || qty=='undefined')
											{
												qty=0;
											}
											nlapiLogExecution('ERROR', 'qty', qty);
											//if(parseInt(qty)!=0)
											//if(parseInt(qty)>=0)
											//{
											batch=searchresults[s].getValue('custrecord_batch_no');
											skuno=searchresults[s].getValue('custrecord_ebiz_sku_no');
											beginlocation =searchresults[s].getValue('custrecord_actbeginloc');
											var itemStatus =searchresults[s].getValue('custrecord_sku_status');
											var endloc =searchresults[s].getValue('custrecord_actendloc');
											remainingreplenqty = searchresults[s].getValue('custrecord_reversalqty');//temporarily remaning qty is updated fetched from this field not from above hdn filed
											nlapiLogExecution('ERROR', 'test Rid', Rid);
											nlapiLogExecution('ERROR', 'test remainingreplenqty', remainingreplenqty);
											nlapiLogExecution('ERROR', 'qty', qty);
											totalexpectedQty=parseInt(totalexpectedQty)+parseInt(expectedqty);
											totalQty=parseInt(totalQty)+parseInt(qty);
											try {
												// Load the record from opentask and update the corresponding values
												var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask',Rid);
												transaction.setFieldValue('custrecord_actendloc', endloc);
												transaction.setFieldValue('custrecord_act_end_date', DateStamp());
												transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
												transaction.setFieldValue('custrecord_tasktype', 8);
												transaction.setFieldValue('custrecord_wms_status_flag', 19);
												transaction.setFieldValue('custrecord_act_qty', qty);

												varsku=transaction.getFieldValue('custrecord_ebiz_sku_no');
												ebizTaskno=transaction.getFieldValue('custrecord_ebiz_task_no');
												var invtLpno=transaction.getFieldValue('custrecord_lpno');
												whLocation=transaction.getFieldValue('custrecord_wms_location');
												binLocation=transaction.getFieldValue('custrecord_actendloc');
												beginLocation = transaction.getFieldValue('custrecord_actbeginloc');
												fromLPno=transaction.getFieldValue('custrecord_from_lp_no');
												FrmInvtRefNo.push(transaction.getFieldValue('custrecord_invref_no'));
												temparray[s]=new Array();
												temparray[s][0]=transaction.getFieldValue('custrecord_invref_no');
												temparray[s][1]=qty;
												temparray[s][2]=expectedqty;
												nlapiLogExecution('ERROR', 'FrmInvtRefNo', FrmInvtRefNo);
												nlapiSubmitRecord(transaction, true);
											}

											catch (e) {
												nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed', e);
											}
											//}
										}
									}
									else
									{
										nlapiLogExecution('ERROR', 'NO REPLEN TASKS');
										Reparray["custparam_screenno"] = 'R16';
										Reparray["custparam_error"] = 'REPLENS ARE ALREADY CONFIRMED';
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
										return;
									}
									nlapiLogExecution('ERROR', 'temparray', temparray);
									var accountNumber = getAccountNumber(request.getParameter('hdnwhlocation'));
									nlapiLogExecution('ERROR', 'accountNumber', accountNumber);
									nlapiLogExecution('ERROR', 'ebizTaskno', ebizTaskno);
									nlapiLogExecution('ERROR', 'totalQty', totalQty);
									nlapiLogExecution('ERROR', 'totalexpectedQty', totalexpectedQty);
									var stageLocation=GetStageLocation(varsku, "", whLocation, "", "BOTH");
									nlapiLogExecution('ERROR', 'stageLocation', stageLocation);
									var rectype="";
									var serin="";
									var serout="";

									nlapiLogExecution('ERROR', 'ItemNo', request.getParameter('custparam_repskuno'));

									if(request.getParameter('custparam_repskuno') !=null && request.getParameter('custparam_repskuno') !='')
									{
										var ItemFilter=new Array();
										ItemFilter[0]=new nlobjSearchFilter("internalid",null,"anyof",request.getParameter('custparam_repskuno'));

										var Itemcolumn=new Array();
										Itemcolumn[0]=new nlobjSearchColumn("type");
										Itemcolumn[1]=new nlobjSearchColumn("custitem_ebizserialin");
										Itemcolumn[2]=new nlobjSearchColumn("custitem_ebizserialout");

										var ItemSearchResults=nlapiSearchRecord("item",null,ItemFilter,Itemcolumn);
										if(ItemSearchResults!=null && ItemSearchResults!="")
										{

											rectype=ItemSearchResults[0].recordType;
											serin=ItemSearchResults[0].getValue("custitem_ebizserialin");
											serout=ItemSearchResults[0].getValue("custitem_ebizserialout");

										}

									}
									if(stageLocation == -1){

//										var bulkLocationInventoryResults=getRecord(request.getParameter('custparam_repskuno'), beginlocation, fromLPno);

//										if(bulkLocationInventoryResults != null)
//										updateBulkLocation(bulkLocationInventoryResults, varsku, beginlocation, totalQty, itemStatus,totalexpectedQty,whLocation,remainingreplenqty);
										var bulkLocationInventoryResults=getRecord(request.getParameter('custparam_repskuno'), beginlocation, fromLPno,'BULK',FrmInvtRefNo);

										if(bulkLocationInventoryResults != null)
											updateBulkLocation(bulkLocationInventoryResults, varsku, beginlocation, totalQty, itemStatus,totalexpectedQty,whLocation,remainingreplenqty,temparray);

										//var invtresults=getRecord(request.getParameter('custparam_repskuno'), binLocation,invtLpno);
										var invtresults=getRecord(request.getParameter('custparam_repskuno'), binLocation,invtLpno,'PF','','',itemStatus); //Case# 20149805
										var vGetFifoDate="";
										if(bulkLocationInventoryResults!=null)
											vGetFifoDate=GetFiFoDate(bulkLocationInventoryResults);
										else
											vGetFifoDate=DateStamp();

										updateOrCreatePickfaceInventory(invtresults, varsku, totalQty, invtLpno, binLocation, 
												itemStatus, accountNumber,batch,whLocation,skuno);
										if (rectype == 'serializedinventoryitem' || serout == 'T') 
										{
											UpdateSerialNumbers(request.getParameter('custparam_repskuno'),beginLocation,fromLPno,invtLpno,binLocation);

										}
									}
									else
									{
//										var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno);
										var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno,'BULK',FrmInvtRefNo);

										if(bulkLocationInventoryResults != null)
											updateBulkLocation(bulkLocationInventoryResults, varsku, beginlocation, totalQty, itemStatus,
													totalexpectedQty,whLocation,remainingreplenqty,temparray);

										var vGetFifoDate="";
										if(bulkLocationInventoryResults!=null)
											vGetFifoDate=GetFiFoDate(bulkLocationInventoryResults);
										else
											vGetFifoDate=DateStamp();

										nlapiLogExecution('ERROR','qty', qty);
										nlapiLogExecution('ERROR','varsku', varsku);
										nlapiLogExecution('ERROR','Location',beginlocation);
										nlapiLogExecution('ERROR','invtLpno', invtLpno);
										nlapiLogExecution('ERROR','beginLocationId', beginlocation);
										nlapiLogExecution('ERROR','Inventory Ref No before', transaction.getFieldValue('custrecord_invref_no'));

										var invRefNo=transaction.getFieldValue('custrecord_invref_no');                           
										var result = nlapiSubmitRecord(transaction);

										nlapiLogExecution('ERROR','varsku', varsku);
										nlapiLogExecution('ERROR','Bin Location Id', binLocation);
										nlapiLogExecution('ERROR','Begin Location Id', beginLocation);
										nlapiLogExecution('ERROR','Inventory Ref No', invRefNo);

										//var invtresults=getRecord(varsku, endloc,invtLpno,batch);
										var invtresults=getRecord(varsku, endloc,invtLpno,'PF',batch,'',itemStatus); //Case# 20149805
										if(invRefNo != null && invRefNo != "")
										{
											//Update Inventory in pickface bin location
											updateOrCreatePickfaceInventory(invtresults, varsku, totalQty, invtLpno, binLocation, 
													itemStatus, accountNumber,batch,whLocation,skuno,vGetFifoDate);
											nlapiLogExecution('ERROR', 'beginLocation', beginLocation);
											var LocationType = nlapiLookupField('customrecord_ebiznet_location', beginLocation, ['custrecord_ebizlocationtype']);
											nlapiLogExecution('ERROR', 'LocationType', LocationType);
											if (LocationType.custrecord_ebizlocationtype == '8' )
											{
												DeleteInventoryRecord(invRefNo);
											}
										}

										if (rectype == 'serializedinventoryitem' || serout == 'T') 
										{
											UpdateSerialNumbers(request.getParameter('custparam_repskuno'),beginLocation,fromLPno,invtLpno,binLocation);
										}
									}

									var opentaskcount=0;									
									var	opentasksearchresults=getConsolidatedopentaskcount(reportNo,request.getParameter('custparam_repskuno'),getlocid,taskpriority,getreportNo,replenType);
									if(opentasksearchresults!=null && opentasksearchresults.length>0)
									{
										opentaskcount=opentasksearchresults.length;
									}	
									if(opentaskcount >= 1)
									{
										var item=opentasksearchresults[0].getValue('custrecord_sku',null,'group');
										var binloc=opentasksearchresults[0].getText('custrecord_actbeginloc',null,'group');
										var ItemType = nlapiLookupField('item', skuno, ['recordType','custitem_ebizbatchlot']);
										if(item==skuno && vhdnBeginLocation==binloc && (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ))
										{
											Reparray['custparam_repbatchno']=opentasksearchresults[0].getValue('custrecord_batch_no',null,'group');
											Reparray['custparam_repexpqty']=opentasksearchresults[0].getValue('custrecord_expe_qty',null,'sum');
//											if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
//											{
											response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);
											return;
//											}
										}
										else if(item!=skuno && vhdnBeginLocation==binloc )
										{
											nlapiLogExecution('ERROR', 'Test3', 'Test3');

											//The below changes are done by Satish.N on 07/28/2015
											Reparray['custparam_repskuno']=opentasksearchresults[0].getValue('custrecord_sku',null,'group');
											Reparray['custparam_repsku']=opentasksearchresults[0].getText('custrecord_sku',null,'group');
											Reparray['custparam_reppicklocno']=opentasksearchresults[0].getValue('custrecord_actendloc',null,'group');
											Reparray['custparam_reppickloc']=opentasksearchresults[0].getText('custrecord_actendloc',null,'group');
											Reparray['custparam_torepexpqty']=opentasksearchresults[0].getValue('custrecord_expe_qty',null,'sum');
											Reparray['custparam_repbeglocId']=opentasksearchresults[0].getValue('custrecord_actbeginloc',null,'group');
											Reparray['custparam_repbegloc']=opentasksearchresults[0].getText('custrecord_actbeginloc',null,'group');

											//response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);
											response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
											return;
										}
										else
										{
											response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
											return;
										}


									}
									else
									{
										nlapiLogExecution('ERROR', 'Test2', 'Test2');
										response.sendRedirect('SUITELET', 'customscript_rf_replen_cont', 'customdeploy_rf_replen_cont_di', false, Reparray);
									}
								}

							}
							else {
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
								Reparray["custparam_error"] = st0;//'INVALID PICK FACE LOCATION';
								nlapiLogExecution('ERROR', 'Pick Face Location not Matched');
							}
						}
						else {
							Reparray["custparam_error"] = st2;//'LOCATION IS NULL';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
							nlapiLogExecution('ERROR', 'Location IS NULL');
						}
					}
				}
				catch (exp)  {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					nlapiLogExecution('ERROR', 'exception ',exp);
				} finally {					
					context.setSessionObject('session', null);
					nlapiLogExecution('DEBUG', 'finally','block');

				}
			}
			else
			{
				Reparray["custparam_screenno"] = 'R1';
				Reparray["custparam_error"] = 'REPLEN ALREADY IN PROCESS';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			nlapiLogExecution('ERROR', 'Catch: Location not found',e);
		}
	}
}

function getendReplenTasks(ReportNo,skuno,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('ERROR', 'ReportNo', ReportNo);
	nlapiLogExecution('ERROR', 'ReportNo', skuno);
	var openreccount=0;

	var filters = new Array();

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));
	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
	}

	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
	}

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isnotempty')); // case# 201416817

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_batch_no');

	columns[1].setSort(false);
	columns[2].setSort(false);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!=''&& searchresults.length>0)
		openreccount=searchresults.length;		


	return searchresults;

}

function getReplenTasks(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType,whlocation)
{
	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);

	var filters = new Array();

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));
	}

	nlapiLogExecution('ERROR', 'tst newgetitemid',getitemid);
	nlapiLogExecution('ERROR', 'tst replenType',replenType);
	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}

	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
	}

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	nlapiLogExecution('ERROR', 'tst new',getreportNo);
	//case# 20148141 starts
	//var systemRule=GetSystemRuleForReplen();
	if(whlocation!=null && whlocation!='')
		var systemRule=GetSystemRuleForReplen(whlocation);
	//case# 20148141 ends
	var columns = new Array();
	if(systemRule=='N')
	{

		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
		columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
		columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
		var fields=new Array();
		nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
		fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
		var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
		if(sresult!=null && sresult.length>0)
			filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', sresult[0].getId()));
		/*	if(getitemid!=null && getitemid!='')
		{
			var ItemType = nlapiLookupField('item', getitemid, ['recordType','custitem_ebizbatchlot']);
			if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
			{
				columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
			}
		}	*/
	}
	else
	{
		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc');
		columns[7] = new nlobjSearchColumn('custrecord_lpno');
		columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[9] = new nlobjSearchColumn('name');	
		columns[10] = new nlobjSearchColumn('custrecord_batch_no');

		columns[11] = new nlobjSearchColumn('custrecord_taskpriority');	
	}
	columns[1].setSort(false);
	columns[2].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

function GetSystemRuleForReplen(whlocation)
{
	try
	{
		var rulevalue='Y';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for Replen?'));
		//case# 20148141 starts
		nlapiLogExecution('ERROR','whlocation',whlocation);
		if(whlocation!=null && whlocation!='')
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'any',whlocation));
		//case# 20148141 ends
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null && searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);


		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}
/**
 * 
 * @param invtresults
 */
function updateOrCreatePickfaceInventory(invtresults, varsku, expQty, invtLpno, binLocation, 
		itemStatus, accountNumber,batchno,whLocation,skuno,GetFifoDate){

	nlapiLogExecution('ERROR', 'Into updateOrCreatePickfaceInventory', invtresults);

	var str = 'varsku. = ' + varsku + '<br>';
	str = str + 'expQty. = ' + expQty + '<br>';	
	str = str + 'invtLpno. = ' + invtLpno + '<br>';	
	str = str + 'binLocation. = ' + binLocation + '<br>';
	str = str + 'itemStatus. = ' + itemStatus + '<br>';
	str = str + 'accountNumber. = ' + accountNumber + '<br>';
	str = str + 'batchno. = ' + batchno + '<br>';
	str = str + 'whLocation. = ' + whLocation + '<br>';
	str = str + 'skuno. = ' + skuno + '<br>';
	str = str + 'GetFifoDate. = ' + GetFifoDate + '<br>';

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	if(parseInt(expQty)>0)
	{

		if(invtresults != null)
		{
			nlapiLogExecution("ERROR", "Inventory Exists", invtresults[0].getId());	
			var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtresults[0].getId());								
			var invtqty = transaction.getFieldValue('custrecord_ebiz_qoh');	
			var pfallocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
			var fifioDtExist = transaction.getFieldValue('custrecord_ebiz_inv_fifo');// case# 201413379
			var lotnoExistforitem = transaction.getFieldValue('custrecord_ebiz_inv_lot');
			if (invtqty == null || isNaN(invtqty) || invtqty == "") {
				invtqty = 0;
			}

			if (pfallocqty == null || isNaN(pfallocqty) || pfallocqty == "") {
				pfallocqty = 0;
			}

			//commented because decimal values addition results to NaN
			//invtqty = parseInt(invtqty) + parseInt(expQty);	
			invtqty = parseFloat(invtqty) + parseFloat(expQty); 
			transaction.setFieldValue('custrecord_ebiz_inv_sku_status',itemStatus);	
			transaction.setFieldValue('custrecord_ebiz_qoh',invtqty);	
			transaction.setFieldValue('custrecord_ebiz_callinv','N');	
			transaction.setFieldValue('custrecord_ebiz_displayfield','N');	
			if((lotnoExistforitem == null || lotnoExistforitem == '')&& (fifioDtExist == null || fifioDtExist == ''))
				transaction.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
			nlapiLogExecution("ERROR", "Quantity Assigned ", invtqty);						
			nlapiSubmitRecord(transaction);	

			updateInvRefforOpenPickTasks(varsku,binLocation,invtresults[0].getId(),pfallocqty,invtqty);
		}
		else
		{
			nlapiLogExecution("ERROR", "Inventory Not Exists", varsku);					
			var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
			invrecord.setFieldValue('name', varsku+1);					
			invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
			invrecord.setFieldValue('custrecord_ebiz_inv_note1','Replenished');
			invrecord.setFieldValue('custrecord_ebiz_qoh', expQty);
			invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		
			invrecord.setFieldValue('custrecord_ebiz_inv_lp', invtLpno);		
			invrecord.setFieldValue('custrecord_ebiz_inv_binloc', binLocation);	
			invrecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
			invrecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
			invrecord.setFieldValue('custrecord_ebiz_inv_qty', expQty);
			if(accountNumber!=null && accountNumber!='')
				invrecord.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);

			invrecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
			invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
			invrecord.setFieldValue('custrecord_invttasktype', 8);
			invrecord.setFieldValue('custrecord_inv_ebizsku_no', skuno);
			nlapiLogExecution("ERROR", "GetFifoDate", GetFifoDate);	
			/*if(GetFifoDate==null || GetFifoDate=='')
				GetFifoDate=DateStamp();*/
			//invrecord.setFieldValue('custrecord_ebiz_inv_fifo', GetFifoDate);
			//invrecord.setFieldValue('custrecord_ebiz_expdate', GetFifoDate);
//			case# 201411078
			var ItemTypeRec = nlapiLookupField('item', varsku, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);				
			var batchflag = ItemTypeRec.custitem_ebizbatchlot;
			var ItemType = ItemTypeRec.recordType;


			if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
			{
				var expdate='';
				var filterspor = new Array();
				filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));

				if(varsku!=null && varsku!="")
					filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', varsku));

				var column=new Array();
				column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
				column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

				var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
				if(receiptsearchresults!=null)
				{
					getlotnoid= receiptsearchresults[0].getId();
					nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
					expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
					GetFifoDate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
					nlapiLogExecution('DEBUG', 'expdate', expdate);
					nlapiLogExecution('DEBUG', 'GetFifoDate', GetFifoDate);
					if(expdate!=null && expdate!='')
						invrecord.setFieldValue('custrecord_ebiz_expdate', expdate);
					if(GetFifoDate!=null && GetFifoDate!='')
						invrecord.setFieldValue('custrecord_ebiz_inv_fifo', GetFifoDate);
				}
				else
				{
					GetFifoDate=DateStamp();
					expdate=DateStamp();
					if(expdate!=null && expdate!='')
						invrecord.setFieldValue('custrecord_ebiz_expdate', expdate);
					if(GetFifoDate!=null && GetFifoDate!='')
						invrecord.setFieldValue('custrecord_ebiz_inv_fifo', GetFifoDate);
				}
			}

			if(batchno!=null && batchno!='')
				invrecord.setFieldText('custrecord_ebiz_inv_lot', batchno);
			invrecord.setFieldValue('custrecord_ebiz_inv_loc', whLocation);

			var invtrefno = nlapiSubmitRecord(invrecord);

			updateInvRefforOpenPickTasks(varsku,binLocation,invtrefno,0,expQty);
		}

		//var retValue =  LocationCubeUpdation(varsku, binLocation, expQty, 'M'); // case#  201413378
	}
	nlapiLogExecution('ERROR', 'Out of updateOrCreatePickfaceInventory LocCubeUpdation', 'Success');
}

function updateInvRefforOpenPickTasks(itemno,binlocation,invrefno,allocqty,qoh)
{
	nlapiLogExecution('ERROR', 'Into  updateInvRefforOpenPickTasks');

	var str = 'itemno. = ' + itemno + '<br>';
	str = str + 'binlocation. = ' + binlocation + '<br>';	
	str = str + 'invrefno. = ' + invrefno + '<br>';
	str = str + 'allocqty. = ' + allocqty + '<br>';
	str = str + 'qoh. = ' + qoh + '<br>';

	var totalpickqty = 0;

	nlapiLogExecution('ERROR', 'Parameter Details', str);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', binlocation));
	filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', itemno));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));	 // TASK TYPE - PICK	
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	 // STATUS - PICKS GENERATED
	filters.push(new nlobjSearchFilter('custrecord_invref_no', null, 'isempty'));

	var columns = new Array();

	columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_skiptask'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		for (var s = 0; s < searchresults.length; s++) {

			var taskrecid = searchresults[s].getId();
			var pickqty = searchresults[s].getValue('custrecord_expe_qty');

			if(pickqty==null || pickqty=='' || isNaN(pickqty))
			{
				pickqty=0;
			}

			totalpickqty = parseFloat(totalpickqty)+parseFloat(pickqty);

			if(taskrecid!=null && taskrecid!='')
			{
				nlapiLogExecution('ERROR', 'Updating PICK task with inventory reference number...',taskrecid);
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', taskrecid, 'custrecord_invref_no', invrefno);
			}
		}

		nlapiLogExecution('ERROR', 'totalpickqty',totalpickqty);

		var totalallocqty = parseFloat(totalpickqty)+parseFloat(allocqty);

		nlapiLogExecution('ERROR', 'totalallocqty',totalallocqty);

		if(totalallocqty==null || totalallocqty=='' || isNaN(totalallocqty))
		{
			totalallocqty=0;
		}

		nlapiLogExecution('ERROR', 'Updating Inventory Record with allocate qty...',invrefno);
		nlapiSubmitField('customrecord_ebiznet_createinv', invrefno, 'custrecord_ebiz_alloc_qty', totalallocqty);
	}

	nlapiLogExecution('ERROR', 'Out of  updateInvRefforOpenPickTasks');
}

/**
 * This is to update the status of the scanned location from 'R' to 'T'
 * @param recId
 */
function updateScannedLoc(recId)
{
	try {
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
		transaction.setFieldValue('custrecord_wms_status_flag', 24);
		var result = nlapiSubmitRecord(transaction);
	}
	catch (e) {
		nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed', e);
	}
}

function getRecord(itemid,location, invtLPNo,loctype,FrmInvtRefNo,batchno,itemstatus)
{
	nlapiLogExecution('ERROR', 'getRecord', location);
	nlapiLogExecution('ERROR', 'batchno', batchno);
	nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
	var batchid = '';
	if(batchno!=null && batchno!='')
	{

		var filtersbatch = new Array();		

		filtersbatch.push(new nlobjSearchFilter('name', null, 'is', batchno));
		filtersbatch.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var columnsbatch = new Array();
		columnsbatch[0] = new nlobjSearchColumn('internalid');

		var searchresultsbatch = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbatch, columnsbatch);
		if(searchresultsbatch != null && searchresultsbatch!='')
		{
			batchid = searchresultsbatch[0].getValue('internalid');
		}
	}
	nlapiLogExecution('ERROR', 'batchid', batchid);

	var filtersso = new Array();
	var socount=0;		

	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', [itemid]));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));
	filtersso.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
	if(batchid!='' && batchid!=null && batchid!='null')
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', [batchid]));
	//filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', invtLPNo));
	if(FrmInvtRefNo!=null && FrmInvtRefNo!='')
		filtersso.push(new nlobjSearchFilter('internalid', null, 'anyof', FrmInvtRefNo));
	if(itemstatus!=null && itemstatus!='')
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', itemstatus));
	/*	if(batch !=null && batch!='' && batch!='null')
	{
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot',null,'anyof',batch)); //Case# 20149805
	}*/
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsLine != null)
	{
		nlapiLogExecution('ERROR', 'Inventory count', searchresultsLine.length);
		socount=searchresultsLine.length; 
	} 
	else
	{ 
		nlapiLogExecution('ERROR', 'inside else if results a', 'null');
	}       
	return searchresultsLine;
}

/**
 * 
 * @param recId
 * @param invrecord
 */
function updateStagTask(recId,invrecord)
{
	try {
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
		transaction.setFieldValue('custrecord_wms_status_flag', 24);		//20);
		transaction.setFieldValue('custrecord_invref_no', invrecord);
		var result = nlapiSubmitRecord(transaction);
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed', e);
	}
}

/**
 * 
 * @param vitem
 * @param varsku
 * @param varqty
 * @param lp
 * @param whLocation
 * @param binLocation
 * @param itemStatus
 * @param accountNumber
 * @returns
 */
function createstagInvtRecord(vitem, varsku, varqty, lp, whLocation, binLocation, itemStatus, accountNumber)
{
	nlapiLogExecution('ERROR','Inside InvtRecCreatedfor ','Function');
	var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	invrecord.setFieldValue('name', vitem+1);
	invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku);
	invrecord.setFieldValue('custrecord_ebiz_inv_note1','Replenished');
	invrecord.setFieldValue('custrecord_ebiz_qoh', varqty);
	invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');
	invrecord.setFieldValue('custrecord_ebiz_inv_lp', lp);
	invrecord.setFieldValue('custrecord_ebiz_inv_binloc', binLocation);
	invrecord.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
	invrecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
	invrecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
	invrecord.setFieldValue('custrecord_ebiz_inv_qty', varqty);
	invrecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
	invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
	invrecord.setFieldValue('custrecord_invttasktype', 8);

	var retval=nlapiSubmitRecord(invrecord);
	return retval;
}

/**
 * 
 */
function DeleteInventoryRecord(invRefNo)
{
	nlapiLogExecution('ERROR','Inside DeleteInvtRecCreatedforCHKNTask ','Function');
	var Ifilters = new Array();
	Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
	Ifilters.push(new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [8]));
	Ifilters.push(new nlobjSearchFilter('internalid', null, 'is', invRefNo));

	var invtId = "";
	var invtType = "";
	var searchInventory= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (searchInventory){
		for (var s = 0; s < searchInventory.length; s++) {
			invtId = searchInventory[s].getId();
			invtType= searchInventory[s].getRecordType();
			nlapiDeleteRecord(searchInventory[s].getRecordType(),searchInventory[s].getId());
			nlapiLogExecution('ERROR','Inventory record deleted ',invtId);
		}
	}
}

/**
 * 
 */
function UpdateInventory()
{
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
	transaction.setFieldValue('custrecord_wms_status_flag', 23);

	var result = nlapiSubmitRecord(transaction);
}

/**
 * 
 * Below is the search criteria to fetch the default item status from the item status master
 */

function getDefaultItemStatus(){

	var filtersitemstatus = new Array();
	filtersitemstatus[0] = new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T');

	var searchresultsitemstatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersitemstatus, null);
	if(searchresultsitemstatus != null && searchresultsitemstatus.length > 0)
	{
		var itemStatus = searchresultsitemstatus[0].getId();
		nlapiLogExecution("ERROR", "searchresultsitemstatus[0].getId()", itemStatus);
	}
	return itemStatus;
}

/**
 * 
 * @param whLocation
 * @returns
 */
function getAccountNumber(whLocation){
	var filtersAccount = new Array();
	filtersAccount[0] = new nlobjSearchFilter('custrecord_location', null, 'is', whLocation);

	var columnsAccount = new Array();
	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);              

	if (accountSearchResults != null){
		var accountNumber = accountSearchResults[0].getValue('custrecord_accountno');                                
		nlapiLogExecution('ERROR', 'Account #',accountNumber);
	}

	return accountNumber;
}


/**
 * 
 * @param bulkLocationInventoryResults
 * @param expQty
 * @param itemStatus
 */
function updateBulkLocation(bulkLocationInventoryResults, itemID, beginLocationId, actQty, itemStatus,expectedqty,
		whLocation,remainingreplenqty,temparray)
{
	nlapiLogExecution("ERROR", "Into updateBulkLocation");	

	var str = 'expectedqty.' + expectedqty + '<br>';
	str = str + 'actQty.' + actQty + '<br>';	
	str = str + 'remainingreplenqty.' + remainingreplenqty + '<br>';	
	str = str + 'temparray.' + temparray + '<br>';
	nlapiLogExecution('ERROR', 'Function Parameters', str);

	if(bulkLocationInventoryResults != null)
	{	
		nlapiLogExecution('ERROR', 'bulkLocationInventoryResults.length', bulkLocationInventoryResults.length);
		for(var cnt=0;cnt<bulkLocationInventoryResults.length;cnt++)
		{
			var bulkLocationInventoryRecId=bulkLocationInventoryResults[cnt].getId();
			nlapiLogExecution('ERROR', 'bulkLocationInventoryRecId', bulkLocationInventoryRecId);
			for(var vlinecnt=0;vlinecnt<temparray.length;vlinecnt++)
			{
				var actbinlocID=temparray[vlinecnt][0];
				nlapiLogExecution('ERROR', 'actbinlocID', actbinlocID);

				if(actbinlocID==bulkLocationInventoryRecId)
				{
					actQty=temparray[vlinecnt][1];
					nlapiLogExecution('ERROR', 'actQty', actQty);

					expectedqty=temparray[vlinecnt][2];
					nlapiLogExecution('ERROR', 'expectedqty', expectedqty);
					if (expectedqty == null || isNaN(expectedqty) || expectedqty == "") 
					{
						expectedqty = 0;
					}
//					if (remainingreplenqty == null || isNaN(remainingreplenqty) || remainingreplenqty == "") 
//					{
//					remainingreplenqty = 0;
//					} 
					if (actQty == null || isNaN(actQty) || actQty == "") 
					{
						actQty = 0;
					}

					var discrepancyqty =0;
					var QOHQty=0;
					var calculatedQOHQty=0;
					var vnotes1="This is from replen qty Exception";
					var tasktype=11; //11 is the internalid value of tasktype "ADJT"; 
					var vAdjustType1=getAdjustmentType(whLocation);

					var scount=1;
					var newqoh=0;
					var vNewAllocQty=0;

					LABL1: for(var i=0;i<scount;i++)
					{	

						nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
						try
						{
							var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', bulkLocationInventoryRecId);
							var inventoryQty = transaction.getFieldValue('custrecord_ebiz_inv_qty');
							var inventoryAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
							var inventoryQOHQty= transaction.getFieldValue('custrecord_ebiz_qoh');

							if (inventoryQty == null || isNaN(inventoryQty) || inventoryQty == "") 
							{
								inventoryQty = 0;
							}
							if (inventoryAllocQty == null || isNaN(inventoryAllocQty) || inventoryAllocQty == "") 
							{
								inventoryAllocQty = 0;
							}
							if (inventoryQOHQty == null || isNaN(inventoryQOHQty) || inventoryQOHQty == "") 
							{
								inventoryQOHQty = 0;
							}

							vNewAllocQty = parseInt(inventoryAllocQty)-parseInt(expectedqty);
							
							var str = 'expectedqty.' + expectedqty + '<br>';
							str = str + 'actQty.' + actQty + '<br>';	
							str = str + 'inventoryAllocQty.' + inventoryAllocQty + '<br>';	
							str = str + 'inventoryQOHQty.' + inventoryQOHQty + '<br>';
							str = str + 'vNewAllocQty.' + vNewAllocQty + '<br>';
							nlapiLogExecution('ERROR', 'Function Parameters new', str);

							if(parseInt(expectedqty) != parseInt(actQty))
							{
								if(remainingreplenqty==null || remainingreplenqty=='null'  || remainingreplenqty=='' || isNaN(remainingreplenqty))
								{
									newqoh = parseInt(inventoryQOHQty) - parseInt(actQty);
									nlapiLogExecution('ERROR', 'newqoh1', newqoh);
								}
								else
								{
									newqoh = parseInt(vNewAllocQty) + parseInt(remainingreplenqty);
									nlapiLogExecution('ERROR', 'newqoh2', newqoh);
								}
							}
							else
							{
								newqoh = parseInt(inventoryQOHQty) - parseInt(actQty);
								nlapiLogExecution('ERROR', 'newqoh3', newqoh);
							}


							calculatedQOHQty= parseInt(inventoryQOHQty)- parseInt(actQty);

							if(remainingreplenqty!=null && remainingreplenqty!='null' && remainingreplenqty!='')
							{
								discrepancyqty = (parseInt(actQty)+parseInt(remainingreplenqty)+parseInt(vNewAllocQty))-parseInt(inventoryQOHQty);
							}


							var str = 'inventoryAllocQty.' + inventoryAllocQty + '<br>';
							str = str + 'vNewAllocQty.' + vNewAllocQty + '<br>';	
							str = str + 'inventoryQOHQty.' + inventoryQOHQty + '<br>';	
							str = str + 'newqoh. ' + newqoh + '<br>';					
							str = str + 'remainingreplenqty. ' + remainingreplenqty + '<br>';	

							nlapiLogExecution('ERROR', 'Qty Details', str);

							if(parseInt(newqoh)<=0)
								newqoh=0;

							if(parseInt(vNewAllocQty)<=0)
								vNewAllocQty=0;

							transaction.setFieldValue('custrecord_ebiz_alloc_qty',vNewAllocQty);
							if(itemStatus!=null && itemStatus!='')
								transaction.setFieldValue('custrecord_ebiz_inv_sku_status',itemStatus);
							transaction.setFieldValue('custrecord_ebiz_qoh',newqoh);
							transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
							transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

							nlapiSubmitRecord(transaction);
							nlapiLogExecution('ERROR', 'Replen Confirmed for inv ref ',bulkLocationInventoryRecId);


						}
						catch(ex)
						{
							var exCode='CUSTOM_RECORD_COLLISION'; 
							var wmsE='Inventory record being updated by another user. Please try again...';
							if (ex instanceof nlobjError) 
							{	
								wmsE=ex.getCode() + '\n' + ex.getDetails();
								exCode=ex.getCode();
							}
							else
							{
								wmsE=ex.toString();
								exCode=ex.toString();
							} 

							nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', wmsE); 
							if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
							{ 
								scount=scount+1;
								continue LABL1;
							}
							else break LABL1;
						}
					}

					if(parseInt(expectedqty) != parseInt(actQty))
					{
						if(discrepancyqty != 0)
						{
							var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(itemID,itemStatus,whLocation,parseInt(discrepancyqty),
									vnotes1,tasktype,vAdjustType1,'');
							nlapiLogExecution('ERROR', 'netsuiteadjustId', netsuiteadjustId);
						}
					}

					nlapiLogExecution('ERROR', 'beginLocationId', beginLocationId);
					//	var retValue =  LocationCubeUpdation(itemID, beginLocationId, actQty, 'P');	

					if(parseInt(newqoh)<=0)
					{
						nlapiLogExecution('ERROR', 'Deleting ZERO qty invetory record...',bulkLocationInventoryRecId);
						nlapiDeleteRecord('customrecord_ebiznet_createinv', bulkLocationInventoryRecId);
					}
					//break;
				}
			}
		}
	}

	nlapiLogExecution("ERROR", "Out of updateBulkLocation");		
}

function LocationCubeUpdation(itemID, beginLocationId, expQty, optype)
{
	var arrDims = getSKUCubeAndWeight(itemID, 1);
	var itemCube = 0;
	var vTotalCubeValue = 0;
	if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
	{
		var uomqty = ((parseFloat(expQty))/(parseFloat(arrDims["BaseUOMQty"])));			
		itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
	} 

	var vOldRemainingCube = GeteLocCube(beginLocationId);
	nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

	if(optype == "P")
		vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
	else
		vTotalCubeValue = parseFloat(itemCube) - parseFloat(vOldRemainingCube);

	nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

	var retValue =  UpdateLocCube(beginLocationId,vTotalCubeValue);
	nlapiLogExecution('ERROR', 'LocCubeUpdation', retValue);

	return retValue; 
}


function GetFiFoDate(SearchRec)
{
	try
	{
		var Fifodate=nlapiLookupField('customrecord_ebiznet_createinv',SearchRec[0].getId(),'custrecord_ebiz_inv_fifo');
		return Fifodate;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetFiFoDate',exp);	
	}
}

function getAdjustmentType(vWMSLocation1)
{
	var vAdjustType1='';
	var filters = new Array();		
	if(vWMSLocation1!=null && vWMSLocation1!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'anyof',[vWMSLocation1]));		
	filters.push(new nlobjSearchFilter('custrecord_adjusttasktype', null,'is','11'));//ADJT
	filters.push(new nlobjSearchFilter('custrecord_adjustdefaultflag', null,'is','T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');	

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filters,columns);		

	if(searchresults != null && searchresults !="")
	{
		if(searchresults.length > 0){
			vAdjustType1 =searchresults[0].getId();
		}
	}

	return vAdjustType1;
}

function  getIdInvokeNSInventoryAdjustment(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot)
{
	/*
		var vItemMapLocation =  getItemStatusMapLoc(itemstatus);
		var vAccountNo = getAccountNo(loc,vItemMapLocation);

		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', vAccountNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, vItemMapLocation);
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseInt(qty));
		nlapiSubmitRecord(outAdj, false, true);
		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	 */

	nlapiLogExecution('ERROR', 'Location info::', loc);
	nlapiLogExecution('ERROR', 'Task Type::', tasktype);
	nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
	nlapiLogExecution('ERROR', 'qty::', qty);

	var vAccountNo;	
	//Code for fetching account no from stockadjustment custom record	
	vAccountNo = getStockAdjustmentAccountNo(loc,tasktype,adjusttype);			
	nlapiLogExecution('ERROR', 'Fetcht Account from stockadjustment', vAccountNo);		
	//upto to here stockadjustment

	var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
	nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);	

	if(vAccountNo==null || vAccountNo=="")
	{		
		vAccountNo = getAccountNo(loc,vItemMapLocation);
		nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
	}

	var vCost;
	var vAvgCost;

	var filters = new Array();          
	filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
	//Changed on 30/4/12by suman
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	//End of changes as on 30/4/12.
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('cost');
	columns[1] = new nlobjSearchColumn('averagecost');

	var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
	if (itemdetails !=null) 
	{
		vCost = itemdetails[0].getValue('cost');
		nlapiLogExecution('ERROR', 'vCost', vCost);
		vAvgCost = itemdetails[0].getValue('averagecost');
		nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
	}		

	var outAdj = nlapiCreateRecord('inventoryadjustment');			
	outAdj.setFieldValue('account', vAccountNo);
	outAdj.setFieldValue('memo', notes);
	outAdj.insertLineItem('inventory', 1);
	outAdj.setLineItemValue('inventory', 'item', 1, item);
	outAdj.setLineItemValue('inventory', 'location', 1, vItemMapLocation);	
	outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseInt(qty));
	if(vAvgCost != null &&  vAvgCost != "")	
	{
		nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
		outAdj.setLineItemValue('inventory', 'unitcost', 1, vAvgCost);
	}
	else
	{
		nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
		outAdj.setLineItemValue('inventory', 'unitcost', 1, vCost);
	}
	//outAdj.setFieldValue('subsidiary', 3);
	//This following line will return 'true' for ONE WORLD Account, 'false' for non-ONE WORLD Account.
	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('ERROR', 'subs', subs);
	if(subs==true)
	{
		var vSubsidiaryVal=getSubsidiaryNew(loc);
		if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
		nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
	}
	//Added by Sudheer on 093011 to send lot# for inventory posting

	if(lot!=null && lot!='')
	{
		if(parseInt(qty)<0)
			qty=parseInt(qty)*(-1);

		nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
		outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseInt(qty) + ")");
	}

	var internalid = nlapiSubmitRecord(outAdj, true, true);
	nlapiLogExecution('ERROR', 'type argument', 'type is create');
	return internalid;
}

function getConsolidatedopentaskcount(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType)
{

	nlapiLogExecution('ERROR', 'reportNo',reportNo);

	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);
	nlapiLogExecution('ERROR', 'replenType in replen task',replenType);

	/*var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
		filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
		filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);
		filters[3] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
		filters[4] = new nlobjSearchFilter('custrecord_act_qty',null, 'isempty');
	 */
	var filters = new Array();

	/*if(reportNo!=null && reportNo!='')
		{
			nlapiLogExecution('ERROR','replenreportNo',reportNo);
			filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
			vOrdFormat='R'
		}*/
	//if(getItemId!=null && getItemId!='')

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}

	//if(getBinLocationId!=null && getBinLocationId!='')
	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

	}
	/*if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='')
	{
		nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));

	}*/


	//if(getPriority!=null && getPriority!='')
	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	//filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
	columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
//	var fields=new Array();
//	nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
//	fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
//	var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
//	if(sresult!=null && sresult.length>0)
//	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', sresult[0].getId()));
	if(getitemid!=null && getitemid!='')
	{
		var ItemType = nlapiLookupField('item', getitemid, ['recordType','custitem_ebizbatchlot']);
		if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
		{
			columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
		}
	}

	columns[1].setSort(false);
	columns[2].setSort(false);
	columns[3].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR', 'searchresults.length ingetreplen',searchresults.length);
	else
	{
		var filters = new Array();
		if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
		{
			nlapiLogExecution('ERROR','name',getreportNo);
			filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

		}
		if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
		{
			nlapiLogExecution('ERROR','Replenitem',getitemid);
			filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

		}
		if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
		{
			nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
			filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

		}
		if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
		{
			nlapiLogExecution('ERROR','Replenpriority',taskpriority);
			filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

		}
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
		filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
		filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		//nlapiLogExecution('ERROR', 'searchresults.length ingetreplen',searchresults.length);
	}
	return searchresults;

}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle ***/
function getReplentotqty(getNo,getSku,getPickLocNo,fromlpno)
{
	nlapiLogExecution('ERROR', 'getReplentotqty getNo',getNo);
	nlapiLogExecution('ERROR', 'getReplentotqty getSku',getSku);
	nlapiLogExecution('ERROR', 'getReplentotqty getPickLocNo',getPickLocNo);
	nlapiLogExecution('ERROR', 'getReplentotqty fromlpno',fromlpno);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', getNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isnotempty'));	

	if(getPickLocNo!=null && getPickLocNo!='')
	{		

		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getPickLocNo));
	}
	filters.push( new nlobjSearchFilter('custrecord_from_lp_no', null,'is', fromlpno));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_act_qty',null,'sum');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;

}
/*** up to here ***/


function getBatchNo(item, location, batch ){
	nlapiLogExecution('DEBUG', 'getBatchNo function');
	nlapiLogExecution('DEBUG', 'getitem',item);

	nlapiLogExecution('DEBUG', 'getlocation',location);
	nlapiLogExecution('DEBUG', 'getbatch',batch);

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[item]));	
	var locationarray = new Array()
	if(location!=null && location!='')
	{
		locationarray.push('@NONE@'); 
		locationarray.push(location); 
		filter.push(new nlobjSearchFilter('custrecord_ebizsitebatch',null,'anyof',locationarray));
	}
	//filter.push(new nlobjSearchFilter('custrecord_ebizsitebatch',null,'anyof',[location]));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batch));
	filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
	column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
	column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
	column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
	column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
	column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');

	var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);

	return searchRecord;
}
function getLocation(gettoloc)
{
	var validlocation=false;
	nlapiLogExecution('ERROR', 'gettoloc', gettoloc);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', gettoloc);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns=new Array();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

	if(searchresults!=null && searchresults!='')
		validlocation=true;

	nlapiLogExecution('ERROR', 'validlocation', validlocation);

	return validlocation;

}

//case# 20148141 starts
function CubeCapacity(RcvQty,BaseUOMQty,cube)
{
	try{
		var itemCube;
		if (cube != "" && cube != null) 
		{
			if (!isNaN(cube)) 
			{
				var uomqty = ((parseFloat(RcvQty))/(parseFloat(BaseUOMQty)));			
				itemCube = (parseFloat(uomqty) * parseFloat(cube));
				nlapiLogExecution('DEBUG', 'ItemCube', itemCube);
			}
			else 
			{
				itemCube = 0;
			}
		}
		else 
		{
			itemCube = 0;
		}	
		nlapiLogExecution('DEBUG','itemcube',itemCube);
		return itemCube;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in cubecapacity',exp);
	}
}
//case# 20148141 ends
function UpdateSerialNumbers(skuno,beginLocationId,fromLP,ToLP,Endloction)
{
	try
	{

		var str = 'skuno. = ' + skuno + '<br>';
		str = str + 'beginLocationId. = ' + beginLocationId + '<br>';	
		str = str + 'fromLP. = ' + fromLP + '<br>';
		str = str + 'ToLP. = ' + ToLP + '<br>';
		str = str + 'Endloction. = ' + Endloction + '<br>';

		nlapiLogExecution('DEBUG', 'Log Update', str);

		var filtersser = new Array();
		filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', skuno);
		filtersser[1] = new nlobjSearchFilter('custrecord_serialbinlocation', null, 'anyof', beginLocationId);
		filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['19','3']);
		filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', fromLP);
		filtersser[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'R');
		var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, null);
		nlapiLogExecution('ERROR', 'SrchRecord', SrchRecord);
		if(SrchRecord!= null && SrchRecord!= '')
		{
			for ( var i = 0; i < SrchRecord.length; i++)  // case# 201413461
			{
				var vserialid = SrchRecord[i].getId();

				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialparentid';
				fields[1] = 'custrecord_serialbinlocation';
				fields[2] = 'custrecord_serialstatus';


				values[0] = ToLP;
				values[1] = Endloction;
				values[2] = '';

				var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',vserialid, fields, values);
				nlapiLogExecution('ERROR', 'Record Updated with Merged LP:');
			}
		}

	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'exception in updating serial', exp);

	}


}