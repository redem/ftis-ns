/***************************************************************************
����������������������eBizNET
�������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_CycleCountStatusReport.js,v $
*� $Revision: 1.1.10.1 $
*� $Date: 2012/11/01 14:54:57 $
*� $Author: schepuri $
*� $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_CycleCountStatusReport.js,v $
*� Revision 1.1.10.1  2012/11/01 14:54:57  schepuri
*� CASE201112/CR201113/LOG201121
*� Decimal Qty Conversions
*�
*� Revision 1.1  2011/11/29 10:41:43  spendyala
*� CASE201112/CR201113/LOG201121
*� created cycle count report
*�
*
****************************************************************************/


function ebiznet_CycleCountStatusReport_SL(request, response){

	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Cycle Count Status Report');
		var selcyclecount = form.addField('custpage_cyclecountplan', 'select', 'Cycle Count Plan#', 'customrecord_ebiznet_cylc_createplan');
		var buttondisplay = form.addSubmitButton('Display');
		response.writePage(form);
	}
	else {
		var form = nlapiCreateForm('Cycle Count Status Report');
		var selcyclecount = form.addField('custpage_cyclecountplan', 'select', 'Cycle Count Plan#', 'customrecord_ebiznet_cylc_createplan');
		try {
			nlapiLogExecution('ERROR', 'Resolve Plan No1', request.getParameter('custpage_cyclecountplan'));
			if (request.getParameter('custpage_cyclecountplan') != null) {
				nlapiLogExecution('ERROR', 'Resolve Plan No2', request.getParameter('custpage_cyclecountplan'));
				var sublist = form.addSubList("custpage_items", "list", "Cycle Count Status Report");
				sublist.addField("custpage_locationtext", "text", "Location", "customrecord_ebiznet_location").setDisplayType('inline');
				sublist.addField("custpage_location", "text", "Location", "customrecord_ebiznet_location").setDisplayType('hidden');
				sublist.addField("custpage_expsku", "text", "Expected SKU", "item").setDisplayType('inline');
				sublist.addField("custpage_actsku", "text", "Actual SKU", "item").setDisplayType('hidden');
				sublist.addField("custpage_actskutext", "text", "Actual SKU", "item").setDisplayType('inline');
				sublist.addField("custpage_expqty", "text", "Actual Qty");
				var actqty = sublist.addField("custpage_actqty", "text", "Expected Qty");
				sublist.addField("custpage_explp", "text", "Expected LP");
				var actlp = sublist.addField("custpage_actlp", "text", "Actual LP");                
				sublist.addField("custpage_expskustatus", "text", "Expected SKU Status");
				sublist.addField("custpage_actskustatus", "text", "Actual SKU Status").setDisplayType('hidden');
				sublist.addField("custpage_actskustatusvalue", "text", "Actual SKU Status value").setDisplayType('hidden');
				sublist.addField("custpage_id", "text", "ID").setDisplayType('hidden');
				sublist.addField("custpage_planno", "text", "Plan#").setDisplayType('hidden');
				sublist.addField("custpage_opentaskid", "text", "Open Task ID");
				sublist.addField("custpage_invtid", "text", "Invt ID");
				sublist.addField("custpage_recorddate", "text", "Record Date");
				sublist.addField("custpage_recordedby", "text", "Recorded By");
				sublist.addField("custpage_varianceqty", "text", "Variance Qty ");
				sublist.addField("custpage_status", "text", "Status");
				sublist.addField("custpage_resolvedate", "text", "Resolve Date");
				sublist.addField("custpage_resolvedby", "text", "Resolved By");
				sublist.addField("custpage_name", "text", "Name").setDisplayType('hidden');
				sublist.addField("custpage_site", "text", "Site").setDisplayType('hidden');

				// define search filters
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', request.getParameter('custpage_cyclecountplan'));
				//filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', ['31','20','21','22']);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_cycact_beg_loc');
				columns[1] = new nlobjSearchColumn('custrecord_cyclecount_exp_ebiz_sku_no');
				columns[2] = new nlobjSearchColumn('custrecord_cycleact_sku');
				columns[3] = new nlobjSearchColumn('custrecord_cycleexp_qty');
				columns[4] = new nlobjSearchColumn('custrecord_cycle_act_qty');
				columns[5] = new nlobjSearchColumn('custrecord_cyclelpno');
				columns[6] = new nlobjSearchColumn('custrecord_cycact_lpno');
				columns[7] = new nlobjSearchColumn('custrecord_cycle_count_plan');
				columns[8] = new nlobjSearchColumn('custrecord_opentaskid');
				columns[9] = new nlobjSearchColumn('custrecord_invtid');
				columns[10] = new nlobjSearchColumn('custrecord_expcyclesku_status');
				columns[11] = new nlobjSearchColumn('custrecord_cyclesku_status');
				columns[12] = new nlobjSearchColumn('custrecord_cyclesite_id');
				columns[13] = new nlobjSearchColumn('custrecord_cyclesku'); 
				columns[14] = new nlobjSearchColumn('custrecord_cyclerec_date'); 
				columns[15] = new nlobjSearchColumn('custrecord_cycleupd_user_no'); 
				columns[16] = new nlobjSearchColumn('custrecord_cyclerec_upd_date'); 
				columns[17] = new nlobjSearchColumn('custrecord_cyclestatus_flag');
				columns[18] = new nlobjSearchColumn('custrecord_cyccplan_recorddate');
				columns[19] = new nlobjSearchColumn('custrecord_cyccplan_recordedby');
				columns[20] = new nlobjSearchColumn('custrecord_cyccplan_resolvedate');
				columns[21] = new nlobjSearchColumn('custrecord_cyccplan_resolvedby');
										
				//  execute the search, passing null filter and return columns
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);
				var loc, expsku, actsku, expqty, actqty, explp, actlp, planno, name, opentaskid, invtid,expskustatus,actskustatus,actskustatusvalue,locText,recorddate,recordedby,varianceqty=0,status,vStatus,resolvedate,resolvedby;
				var resolvedby;
				var rcptqty = new Array();

				/*
				 *  Searching records from custom record and dynamically adding to
				 * the sublist lines
				 */
				for (var i = 0; searchresults != null && i < searchresults.length; i++) {
					var searchresult = searchresults[i];
					nlapiLogExecution('ERROR', 'searchresults.length', searchresults.length);

					loc = searchresult.getValue('custrecord_cycact_beg_loc');
					locText = searchresult.getText('custrecord_cycact_beg_loc');
					expsku = searchresult.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
					expskutext = searchresult.getValue('custrecord_cyclesku');
					actskutext = searchresult.getValue('custrecord_cycleact_sku');
					actsku = searchresult.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
					expqty = searchresult.getValue('custrecord_cycleexp_qty');
					actqty = searchresult.getValue('custrecord_cycle_act_qty');
					explp = searchresult.getValue('custrecord_cyclelpno');
					actlp = searchresult.getValue('custrecord_cycact_lpno');
					expskustatus = searchresult.getText('custrecord_expcyclesku_status');
					actskustatus = searchresult.getText('custrecord_cyclesku_status');
					actskustatusvalue = searchresult.getValue('custrecord_cyclesku_status');
					planno = searchresult.getValue('custrecord_cycle_count_plan');
					name = searchresult.getValue('custrecord_cycle_count_plan');
					opentaskid = searchresult.getValue('custrecord_opentaskid');
					invtid = searchresult.getValue('custrecord_invtid');
					recorddate = searchresult.getValue('custrecord_cyccplan_recorddate');//Record Date
					resolvedate = searchresult.getValue('custrecord_cyccplan_resolvedate');//Record updation Date
					recordedby = searchresult.getText('custrecord_cyccplan_recordedby');//recordedby
					resolvedby = searchresult.getText('custrecord_cyccplan_resolvedby');//resolvedby
					nlapiLogExecution('ERROR', 'recordedby', recordedby);
					status = searchresult.getValue('custrecord_cyclestatus_flag');//status
					nlapiLogExecution('ERROR', 'status', status);

					if(status == 31)
						vStatus = "Recorded";
					else if(status == 20)
						vStatus = "Open";
					else if(status == 21)
						vStatus = "Ignored";
					else if(status == 22)
						vStatus = "Resolved";//have to other condition for recounted????

					siteid = searchresult.getValue('custrecord_cyclesite_id');
					form.getSubList('custpage_items').setLineItemValue('custpage_locationtext', i + 1, locText);
					form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, loc);
					form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i + 1, expsku);
					form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i + 1, actsku);                    
					form.getSubList('custpage_items').setLineItemValue('custpage_actskutext', i + 1, actskutext);
					form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i + 1, expqty);
					form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i + 1, actqty);
					form.getSubList('custpage_items').setLineItemValue('custpage_explp', i + 1, explp);
					form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i + 1, actlp);
					form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus', i + 1, expskustatus);
					form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus', i + 1, actskustatus);
					form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue', i + 1, actskustatusvalue);
					form.getSubList('custpage_items').setLineItemValue('custpage_id', i + 1, searchresult.getId());
					form.getSubList('custpage_items').setLineItemValue('custpage_planno', i + 1, planno);
					form.getSubList('custpage_items').setLineItemValue('custpage_name', i + 1, name);
					form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i + 1, opentaskid);
					form.getSubList('custpage_items').setLineItemValue('custpage_invtid', i + 1, invtid);
					form.getSubList('custpage_items').setLineItemValue('custpage_recorddate', i + 1, recorddate);
					form.getSubList('custpage_items').setLineItemValue('custpage_recordedby', i + 1, recordedby);

					if(actqty == null || actqty == "")
						actqty=0;
					if(expqty == null || expqty == "")
						expqty=0;
					if(actqty != null || actqty != "" && expqty != null || expqty != "")
					{
//						if(actqty > expqty)
							varianceqty = parseFloat(actqty)-parseFloat(expqty);
					}
					form.getSubList('custpage_items').setLineItemValue('custpage_varianceqty', i + 1, parseFloat(varianceqty).toString());
					form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, vStatus);//status
					form.getSubList('custpage_items').setLineItemValue('custpage_resolvedate', i + 1, resolvedate);//resolvedate
					form.getSubList('custpage_items').setLineItemValue('custpage_resolvedby', i + 1, recordedby);//resolvedby
					form.getSubList('custpage_items').setLineItemValue('custpage_site', i + 1, siteid);
				}
			}
		} 
		catch (e) 
		{
			nlapiLogExecution('ERROR', 'Catch', e);
		}
		var buttondisplay = form.addSubmitButton('Display');
		response.writePage(form);
	}
}


