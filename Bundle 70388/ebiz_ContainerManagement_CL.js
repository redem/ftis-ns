/***************************************************************************
	  		   eBizNET Solutions Inc               
****************************************************************************/
/* 
*     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_ContainerManagement_CL.js,v $
*     	   $Revision: 1.4.2.1.16.1 $
*     	   $Date: 2015/11/14 13:01:35 $
*     	   $Author: rrpulicherla $
*
*   eBizNET version and checksum stamp.  Do not remove.
*   $eBiznet_VER: .............. $eBizNET_SUM: .....
* 
* PRAMETERS
*
* DESCRIPTION
* 
* REVISION HISTORY
* $Log: ebiz_ContainerManagement_CL.js,v $
* Revision 1.4.2.1.16.1  2015/11/14 13:01:35  rrpulicherla
* 2015.2 issues
*
* Revision 1.5  2012/05/11 10:41:31  rgore
* CASE201112/CR201113/LOG201121
* Sanitized 'ALERT' messages for spelling and grammar.
* - Ratnakar
* 11 May 2012
*
* Revision 1.4  2011/12/12 13:04:13  snimmakayala
* CASE201112/CR201113/LOG201121
* spelling corrections in success message
*
* Revision 1.3  2011/08/25 14:16:50  mbpragada
* CASE201112/CR201113/LOG201121
* Completed Container Management Logic.
* Modularized the Container Management Logic.
*
* Revision 1.2  2011/08/24 15:04:25  mbpragada
* CASE201112/CR201113/LOG201121
* Completed Container Management Logic.
*
*****************************************************************************/

/**
 *  * This is the main function it checks whether the given LP is valid or not.

 */
function containerManagement_CL()
{
	try
	{
		var LineCount= nlapiGetLineItemCount('custpage_items');
		var newContainer="";
		var newContainerSize="";
		var newQty= "";
		var internalId=""; 
		var flag="";
		var location="";
//		var itemWeight;
//		var itemCube;
//		var itemCubeAndWeight;
		for(var s=1;s <=LineCount;s++){
			flag= nlapiGetLineItemValue('custpage_items', 'custpage_select', s);
			if(flag == "T"){
				newContainer = nlapiGetLineItemValue('custpage_items', 'custpage_newcontainer', s);
				newContainerSize= nlapiGetLineItemValue('custpage_items', 'custpage_newcontainersize', s);
				newQty= nlapiGetLineItemValue('custpage_items', 'custpage_newquantity', s);
				itemId = nlapiGetLineItemValue('custpage_items', 'custpage_itemno', s);
				internalId= nlapiGetLineItemValue('custpage_items', 'custpage_internalid', s);
				salesOrderInternalId = nlapiGetLineItemValue('custpage_items', 'custpage_sointernalid', s);
				location = nlapiGetLineItemValue('custpage_items', 'custpage_wmslocation', s);

				var containerLPValidate = ebiznet_LPRange_CL(newContainer, '2',location,'2');
				
				if(containerLPValidate){
//					var contLPExists = containerLPExists(newContainer);	
//					if(contLPExists != null){
//						updateLPInOpentask(newContainer, itemId, newContainerSize, internalId,newQty);
//					}
//					else{
//						createNewLPRecord(itemId, newContainerSize, newContainer,newQty);
//					}
//
//					/*	update values to opentask with the values newContainer, 
//					 * 		newContainerSize, newQty, internalId
//					 */
//					updateValuesInOpentask(newContainer, newContainerSize, newQty, internalId);
					return true;
				}
				else{
					alert('The specified container LP is invalid.');
					return false;
				}
			}
		}
	}
	catch(exp)
	{
		alert('Exception'+exp);
	}
	alert("Container Move Successful");
	//return true;
}

