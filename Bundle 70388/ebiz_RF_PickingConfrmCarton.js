/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_PickingConfrmCarton.js,v $
 *     	   $Revision: 1.1.2.47.2.3 $
 *     	   $Date: 2015/11/20 14:06:10 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingConfrmCarton.js,v $
 * Revision 1.1.2.47.2.3  2015/11/20 14:06:10  schepuri
 * case# 201415790
 *
 * Revision 1.1.2.47.2.2  2015/11/14 13:18:51  snimmakayala
 * 201415646
 *
 * Revision 1.1.2.47.2.1  2015/11/09 17:01:02  sponnaganti
 * case# 201413004
 * 2015.2 issue fix
 *
 * Revision 1.1.2.47  2015/08/21 11:01:22  nneelam
 * case# 201414102
 *
 * Revision 1.1.2.46  2015/07/24 15:25:27  skreddy
 * Case# 201413629
 * SW SB  issue fix
 *
 * Revision 1.1.2.45  2015/07/13 15:32:56  grao
 * 2015.2 ssue fixes  201413032
 *
 * Revision 1.1.2.44  2015/07/07 15:49:51  nneelam
 * case# 201413264
 *
 * Revision 1.1.2.43  2015/06/29 15:38:20  skreddy
 * Case# 201413266,201413264
 * sighwarehouse SB issue fix
 *
 * Revision 1.1.2.42  2015/06/22 15:40:29  grao
 * SB issue fixes  201412952
 *
 * Revision 1.1.2.41  2015/04/29 13:52:39  skreddy
 * Case# 201411134
 * Stnadard bundle issue fix
 *
 * Revision 1.1.2.40  2015/04/17 15:31:46  snimmakayala
 * Case#:201412073
 *
 * Revision 1.1.2.39  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.38  2015/03/02 07:20:41  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.37  2015/01/29 07:27:58  snimmakayala
 * Case#: 201411485
 *
 * Revision 1.1.2.36  2014/12/23 14:00:05  schepuri
 * issue# 201411307
 *
 * Revision 1.1.2.35  2014/11/27 15:45:05  skavuri
 * Case# 201411135 Std bundle issue fixed
 *
 * Revision 1.1.2.34  2014/11/27 07:43:56  spendyala
 * case # 201411117
 *
 * Revision 1.1.2.33  2014/11/19 15:30:34  skavuri
 * Case# 201411088 Std bundle issue fixed
 *
 * Revision 1.1.2.32  2014/11/12 15:32:57  schepuri
 * 201411029,201411045�  issue fix
 *
 * Revision 1.1.2.31  2014/09/30 14:25:22  skavuri
 * Case# 201410520 std bundle issue fixed
 *
 * Revision 1.1.2.30  2014/09/25 17:57:16  skreddy
 * case # 201410534
 * Alpha comm CR (''pick by carton LP")
 *
 * Revision 1.1.2.29  2014/09/24 15:42:49  sponnaganti
 * Case# 201410497 201410374
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.28  2014/09/15 14:48:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201410380
 *
 * Revision 1.1.2.27  2014/08/13 09:20:02  gkalla
 * case#20149227
 * JB-Cycle Count - Even though the Cycle Count is in process user should be able to pick the product
 *
 * Revision 1.1.2.26  2014/07/25 06:54:19  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149328
 *
 * Revision 1.1.2.25  2014/07/11 07:52:10  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149309
 *
 * Revision 1.1.2.24  2014/06/27 10:12:41  sponnaganti
 * case# 20149020
 * New UI Compatability issue fix
 *
 * Revision 1.1.2.23  2014/06/19 15:32:31  grao
 * Case#: 20148976� New GUI account issue fixes
 *
 * Revision 1.1.2.22  2014/06/18 13:55:05  grao
 * Case#: 20148927    New GUI account issue fixes
 *
 * Revision 1.1.2.21  2014/06/13 12:35:58  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.20  2014/06/06 15:16:34  sponnaganti
 * case# 20148755
 * stnd Bundle Issue Fix
 *
 * Revision 1.1.2.19  2014/05/26 07:08:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.1.2.18  2014/05/20 15:45:17  skavuri
 * Case # 20148439 SB Issue Fixed
 *
 * Revision 1.1.2.17  2014/04/16 06:42:18  skreddy
 * case # 20147988
 * Dealmed sb  issue fix
 *
 * Revision 1.1.2.16  2014/03/29 21:35:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to 20127825
 *
 * Revision 1.1.2.15  2014/03/13 15:24:44  snimmakayala
 * Case #: 20127669
 *
 * Revision 1.1.2.14  2014/03/05 12:03:30  snimmakayala
 * Case #: 20127499
 *
 * Revision 1.1.2.13  2014/02/26 15:15:15  nneelam
 * case#  20126849
 * Standard Bundle Issue Fix.
 *
 * Revision 1.1.2.12  2014/02/14 16:09:42  sponnaganti
 * case# 20127170
 * (stnd bundle issue)
 *
 * Revision 1.1.2.11  2014/01/20 13:41:03  snimmakayala
 * Case# : 20126850�
 *
 * Revision 1.1.2.10  2014/01/20 09:55:27  snimmakayala
 * Case# : 20126503
 * Cartonization Issue
 *
 * Revision 1.1.2.9  2014/01/20 09:53:38  snimmakayala
 * Case# : 20126503
 * Cartonization Issue
 *
 * Revision 1.1.2.8  2014/01/16 13:37:12  schepuri
 * 20126817
 * standard bundle issue fix
 *
 * Revision 1.1.2.7  2014/01/06 07:59:49  schepuri
 * 20126637
 *
 * Revision 1.1.2.6  2013/12/24 15:12:23  rmukkera
 * Case # 20126541,20126540,20126538
 *
 * Revision 1.1.2.5  2013/11/20 23:28:13  skreddy
 * Case# 20125850
 * Lost and Found CR for MHP
 * issue fix
 *
 * Revision 1.1.2.4  2013/11/14 06:32:04  skreddy
 * Case# 201215000
 * Fast Picking issue fix
 *
 * Revision 1.1.2.3  2013/10/22 14:01:29  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20124795
 * Replen Priorities
 *
 * Revision 1.1.2.2  2013/09/16 09:40:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.1.2.1  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 *
 *
 *****************************************************************************/


function PickingContainerNo(request, response){

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	

	var str = 'sessionobj = ' + sessionobj + '<br>';
	str = str + 'user = ' + user+ '<br>';

	nlapiLogExecution('Debug', 'Session Details', str);

	if (request.getMethod() == 'GET') {

		var fastpick = request.getParameter('custparam_fastpick');

		nlapiLogExecution('DEBUG', 'fastpick', fastpick);

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getItemName = request.getParameter('custparam_itemname');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		if(getBeginBinLocation == null || getBeginBinLocation == '' || getBeginBinLocation =='null' || getBeginBinLocation =='undefined')
			getBeginBinLocation = request.getParameter('custparam_beginLocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getSerialNo = request.getParameter('custparam_serialno');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');		
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');
		var name=request.getParameter('name');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var whLocation = request.getParameter('custparam_whlocation');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var getRemQty = request.getParameter('custparam_remqty');
		var NextRecordId=request.getParameter('custparam_nextrecordid');
		var getItem = '';
		var serialscanflag=request.getParameter('custparam_serialscanflag');
		var itemstatus = request.getParameter('custparam_itemstatus');
		var getNumber = request.getParameter('custparam_number');
		nlapiLogExecution('DEBUG', 'serialscanflag in get', serialscanflag);		
		var ruleDet = IsCustomScreenRequired('RFPICKINGCARTONSCAN');
		if(ruleDet!=null && ruleDet!='')
		{
			var vScreenType = ruleDet[0].getValue('custrecord_ebizrulevalue');
			var vScriptId = ruleDet[0].getValue('custrecord_ebizrule_scriptid');
			var vDeployId = ruleDet[0].getValue('custrecord_ebizrule_deployid');

			nlapiLogExecution('DEBUG', 'vScreenType', vScreenType);
			nlapiLogExecution('DEBUG', 'vScriptId', vScriptId);
			nlapiLogExecution('DEBUG', 'vDeployId', vDeployId);			
		}

var getdisplayqty = request.getParameter('custparam_displayquantity');
		nlapiLogExecution('DEBUG', 'getdisplayqty', getdisplayqty);			


		nlapiLogExecution('DEBUG', 'getItemName', getItemName);

		if(getItemName==null || getItemName=='')
		{

			if(getItemInternalId!=null && getItemInternalId!='')
			{
				var Itemtype = nlapiLookupField('item', getItemInternalId, 'recordType');

				var filtersitem = new Array();
				var columnsitem = new Array();

				filtersitem.push(new nlobjSearchFilter('internalid', null, 'is',getItemInternalId));
				filtersitem.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

				columnsitem[0] = new nlobjSearchColumn('description');    
				columnsitem[1] = new nlobjSearchColumn('salesdescription');
				columnsitem[2] = new nlobjSearchColumn('itemid');

				var itemRecord = nlapiSearchRecord(Itemtype, null, filtersitem, columnsitem);
				//Case # 20126540� Start
				var Itemdescription=null;
				//Case # 20126540� End
				if(itemRecord!=null && itemRecord!='' && itemRecord.length>0)
				{
					if(itemRecord[0].getValue('description') != null && itemRecord[0].getValue('description') != '')
					{
						Itemdescription = itemRecord[0].getValue('description');
					}
					else if(itemRecord[0].getValue('salesdescription') != null && itemRecord[0].getValue('salesdescription') != "")
					{	
						Itemdescription = itemRecord[0].getValue('salesdescription');
					}

					getItem = itemRecord[0].getValue('itemid');
					//case 201411134
					getItemName= getItem;
				}
				//Case # 20126540� Start
				if(Itemdescription!=null)
				{
					Itemdescription = Itemdescription.substring(0, 20);
				}
				//Case # 20126540� End.
			}
		}
		else
		{
			getItem=getItemName;
		}

		var fields = ['recordType', 'custitem_ebizbatchlot'];
		var columns;
		var Itemtype;					
		var batchflag;
		if(getItemInternalId!=null && getItemInternalId!='')
		{
			columns = nlapiLookupField('item', getItemInternalId, fields);
			Itemtype = columns.recordType;					
			batchflag = columns.custitem_ebizbatchlot;
		}


		//code added on 15Feb 2012 by suman
		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var EntLot = request.getParameter('custparam_entLotId');
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');

		var trackingno_redirect = getSystemRuleValue('ShipAlone');
		if(trackingno_redirect=='Y')
		{

			nlapiLogExecution('DEBUG', 'ReDirecting into Custom Script', '');
			var SOarray = new Array();

			SOarray["custparam_waveno"] = getWaveno;
			SOarray["custparam_recordinternalid"] = getRecordInternalId;
			SOarray["custparam_containerlpno"] = getContainerLpNo ;
			SOarray["custparam_expectedquantity"] =getExpectedQuantity ;
			SOarray["custparam_beginLocation"] =getBeginLocationId ;
			SOarray["custparam_itemdescription"] = getItemDescription ;
			SOarray["custparam_iteminternalid"] = getItemInternalId;
			SOarray["custparam_itemname"] = request.getParameter('custparam_itemname');
			SOarray["custparam_dolineid"] = getDOLineId ;
			SOarray["custparam_invoicerefno"] =getInvoiceRefNo;
			SOarray["custparam_endlocinternalid"] = request.getParameter('custparam_endlocinternalid');
			SOarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
			SOarray["custparam_beginlocationname"] = getBeginBinLocation;
			SOarray["custparam_orderlineno"] = getOrderLineNo;
			SOarray["custparam_serialno"] = request.getParameter('custparam_serialno');
			SOarray["custparam_clusterno"] = vClusterNo;
			SOarray["custparam_batchno"] =vBatchno ;
			SOarray["custparam_noofrecords"] = request.getParameter('custparam_noofrecords');
			SOarray["custparam_nextlocation"] = NextLocation;
			SOarray["custparam_nextiteminternalid"] = NextItemInternalId;
			SOarray["custparam_picktype"] = pickType;
			SOarray["name"] =name;
			SOarray["custparam_ebizordno"] = getOrderNo;
			SOarray["custparam_whlocation"] = whLocation;
			SOarray["custparam_ebizzoneno"] = request.getParameter('custparam_ebizzoneno');
			SOarray["custparam_remqty"] = request.getParameter('custparam_remqty');
			SOarray["custparam_nextrecordid"] = request.getParameter('custparam_remqty');
			SOarray["custparam_enteredQty"] = request.getParameter('custparam_enteredQty');
			SOarray["custparam_EntLoc"] = request.getParameter('custparam_EntLoc');
			SOarray["custparam_EntLocRec"] = request.getParameter('custparam_EntLocRec');
			SOarray["custparam_EntLocId"] = request.getParameter('custparam_EntLocId');
			SOarray["custparam_Exc"] = request.getParameter('custparam_Exc');
			SOarray["custparam_fastpick"] = request.getParameter('custparam_fastpick');
			//check with ebiz_RF_pickingconfrmCarton_LL script at custom bundle.
			response.sendRedirect('SUITELET', 'customscript_ebiz_custom_sl_17', 'customdeploy_ebiz_custom_sl_17', false, SOarray);
		}

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		//end of code as of 15Feb.

		var getPickQty=0;
		getPickQty=request.getParameter('custparam_enteredQty');
		nlapiLogExecution('DEBUG', 'enteredQty', getPickQty);
	if(getdisplayqty!= null && getdisplayqty!='' && getdisplayqty!='null')
			{
			
			getPickQty = getdisplayqty;
			nlapiLogExecution('DEBUG', 'getPickQty', getPickQty);

			}
		if(getPickQty == "" || getPickQty == null)
		{
			getPickQty=request.getParameter('custparam_expectedquantity');
		}
		var getReason=request.getParameter('custparam_enteredReason');

		var getLPContainerSize= '';
		var wmsstatusflag = '';

		var str = 'EntLoc. = ' + EntLoc + '<br>';
		str = str + 'EntLocRec. = ' + EntLocRec + '<br>';	
		str = str + 'EntLot. = ' + EntLot + '<br>';	
		str = str + 'EntLocID. = ' + EntLocID + '<br>';	
		str = str + 'ExceptionFlag. = ' + ExceptionFlag + '<br>';	
		str = str + 'getnextExpectedQuantity. = ' + getnextExpectedQuantity + '<br>';	
		str = str + 'getItem. = ' + getItem + '<br>';	
		str = str + 'getOrderNo. = ' + getOrderNo + '<br>';	
		str = str + 'getPickQty. = ' + getPickQty + '<br>';	
		str = str + 'getExpectedQuantity. = ' + getExpectedQuantity + '<br>';	
		str = str + 'getReason. = ' + getReason + '<br>';	
		str = str + 'getEnteredLocation. = ' + getEnteredLocation + '<br>';	
		str = str + 'getContainerLpNo. = ' + getContainerLpNo + '<br>';	
		str = str + 'getEndLocInternalId. = ' + getEndLocInternalId + '<br>';	
		str = str + 'getBeginBinLocation. = ' + getBeginBinLocation + '<br>';
		str = str + 'getRecordInternalId. = ' + getRecordInternalId + '<br>';	
		str = str + 'fastpick. = ' + fastpick + '<br>';	

		nlapiLogExecution('DEBUG', 'Parameter Values', str);

		try
		{
			nlapiLogExecution('DEBUG', 'Lading Open Task...', getRecordInternalId);
			var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
			getContainerLpNo= SORec.getFieldValue('custrecord_container_lp_no');
			getLPContainerSize= SORec.getFieldText('custrecord_container');
			itemstatus = SORec.getFieldValue('custrecord_sku_status');
			wmsstatusflag = SORec.getFieldValue('custrecord_wms_status_flag');
			getDOLineId = SORec.getFieldValue('custrecord_ebiz_cntrl_no');
			var SOarray = new Array();
			if(wmsstatusflag=='8' || wmsstatusflag == '28')
			{
				SOarray["custparam_error"] = 'Picking/Packing Already Completed';
				SOarray["custparam_screenno"] = '12';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			}
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in Loading Open Task', exp);
		}

		nlapiLogExecution('DEBUG', 'getContainerLpNo', getContainerLpNo);

		var vdisplaylocation='';
		if(EntLoc!=null && EntLoc!='')
			vdisplaylocation=EntLoc;
		else
			vdisplaylocation=getBeginBinLocation;

		if(EntLot!=null && EntLot!='')
			vBatchno=EntLot;

		if(vScreenType=='CUSTOM')
		{
			var SOarray = new Array();

			SOarray["custparam_error"] = 'INVALID CARTON #';
			SOarray["custparam_screenno"] = '13NEW';
			SOarray["custparam_picktype"] = request.getParameter('custparam_picktype');
			SOarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
			SOarray["custparam_ebizordno"] = request.getParameter('custparam_ebizordno');
			SOarray["custparam_fastpick"] = fastpick;		
			SOarray["custparam_skipid"] = request.getParameter('custparam_skipid');
			SOarray["custparam_waveno"] = request.getParameter('custparam_waveno');
			SOarray["custparam_nextrecordid"] = request.getParameter('custparam_nextrecordid');
			SOarray["custparam_recordinternalid"] = request.getParameter('custparam_recordinternalid');
			SOarray["custparam_containerlpno"] = request.getParameter('custparam_containerlpno');
			SOarray["custparam_expectedquantity"] = request.getParameter('custparam_expectedquantity');
			SOarray["custparam_beginLocation"] = request.getParameter('custparam_beginLocation');
			SOarray["custparam_item"] = request.getParameter('custparam_item');
			SOarray["custparam_itemname"] = request.getParameter('custparam_itemname');
			SOarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
			SOarray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');
			SOarray["custparam_dolineid"] = request.getParameter('custparam_dolineid');
			SOarray["custparam_invoicerefno"] = request.getParameter('custparam_invoicerefno');
			SOarray["custparam_beginlocationname"] = request.getParameter('custparam_beginlocationname');
			SOarray["custparam_beginLocationname"] = request.getParameter('custparam_beginLocationname');
			SOarray["custparam_endlocinternalid"] = request.getParameter('custparam_endlocinternalid');
			SOarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
			SOarray["custparam_orderlineno"] = request.getParameter('custparam_orderlineno');
			SOarray["custparam_serialno"] = request.getParameter('custparam_serialno');
			SOarray["custparam_clusterno"] = request.getParameter('custparam_clusterno');
			SOarray["custparam_batchno"] = request.getParameter('custparam_batchno');
			SOarray["custparam_nextlocation"] = request.getParameter('custparam_nextlocation');
			SOarray["custparam_nextiteminternalid"] = request.getParameter('custparam_nextiteminternalid');
			SOarray["custparam_newcontainerlp"] = request.getParameter('custparam_newcontainerlp');
			SOarray["custparam_itemstatus"] = request.getParameter('custparam_itemstatus');
			SOarray["custparam_nextexpectedquantity"] = request.getParameter('custparam_nextexpectedquantity');
			SOarray["name"] = request.getParameter('name');			
			SOarray["custparam_ebizzoneno"] =  request.getParameter('custparam_ebizzoneno');

			response.sendRedirect('SUITELET', vScriptId, vDeployId, false, SOarray );
			return;		

		}
		var caseQty = fetchCaseQuantity(getItemInternalId,whLocation,null);
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no');
		var html = "<html><head>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//	html = html + " document.getElementById('enterlotno').focus();";   

		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + name + "</label> ## WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LOCATION : <label>" + vdisplaylocation + "</label>&nbsp;##&nbsp;ITEM : <label>" + getItemName + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>QTY : <label>" + getPickQty + " Each</label>";
		var pikQtyBreakup = getQuantityBreakdown(caseQty,getPickQty);
		html = html + "	("+caseQty+"/Case:"+pikQtyBreakup+")";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CARTON NO : <label>" + getContainerLpNo + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value='" + getItem + "'>";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value='" + getEnteredLocation + "'>";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value='" + getBeginBinLocation + "'>";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnserialno' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";		
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value='" + NextLocation + "'>";
		html = html + "				<input type='hidden' name='hdnNextItem' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getLPContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnReason' value=" + getReason + ">";
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getPickQty + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnremqty' value=" + getRemQty + ">";
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnNextrecordid' value=" + NextRecordId + ">";
		html = html + "				<input type='hidden' name='hdnQtyExcpflag'>";
		html = html + "				<input type='hidden' name='hdnConfirmflag'>";
		html = html + "				<input type='hidden' name='hdnLocExcpflag'>";
		html = html + "				<input type='hidden' name='hdnCartExcpflag'>";
		html = html + "				<input type='hidden' name='hdnlotExcpflag'>";
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";
		html = html + "				<input type='hidden' name='hdnserialscanflag' value=" + serialscanflag + ">";
		html = html + "				<input type='hidden' name='hdnitemstatus' value=" + itemstatus + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		if(Itemtype == "lotnumberedinventoryitem" || Itemtype == "lotnumberedassemblyitem"){
			html = html + "				<td align = 'left'>LOT # : <label>" + vBatchno + "</label>" ;

			html = html + "				</td>";


			html = html + "			</tr>";	

			// case no 20126637� 
			/*	html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN LOT# ";
		html = html + "				</td>";
		html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterlotno' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";*/
		}

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN CARTON NO ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercontainerno' id='entercontainerno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		if(getLPContainerSize!=null && getLPContainerSize!='')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>CARTON SIZE : <label>" + getLPContainerSize + "</label>";
			html = html + "				</td>";
			html = html + "			</tr>";
			/*html = html + "			<tr>";
			html = html + "				<td align = 'left'>ENTER SIZE ";
			html = html + "				</td>";
			html = html + "			</tr>";*/
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='entersize' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnConfirmflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdlocexc.disabled=true;this.form.cmdPickException.disabled=true;this.form.cmdcartexc.disabled=true;return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				QTY EXC <input name='cmdPickException' type='submit' value='F2' onclick='this.form.hdnQtyExcpflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdlocexc.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdcartexc.disabled=true;return false'/>&nbsp;&nbsp;LOC EXC <input name='cmdlocexc' type='submit' value='F3' onclick='this.form.hdnLocExcpflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdPickException.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdcartexc.disabled=true;return false'/></td></tr>";
		//html = html + "				<td align = 'left'>LOC EXC <input name='cmdlocexc' type='submit' value='F3' onclick='this.form.hdnLocExcpflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdPickException.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdcartexc.disabled=true;return false'/></td></tr>";
		//html = html + "				<tr><td align = 'left'>CARTON EXC <input name='cmdcartexc' type='submit' value='F11' onclick='this.form.hdnCartExcpflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdPickException.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdlocexc.disabled=true;return false'/></td>";

		html = html + "				<tr><td align = 'left'>CARTON EXC <input name='cmdcartexc' type='submit' value='F11' onclick='this.form.hdnCartExcpflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdPickException.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdlocexc.disabled=true;return false'/>";
		if(Itemtype == "lotnumberedinventoryitem" || Itemtype == "lotnumberedassemblyitem"  || batchflag=="T"){
			html = html + "              LOT EXC <input name='cmdlotexc' type='submit' value='F12' onclick='this.form.hdnlotExcpflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdPickException.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdlocexc.disabled=true;return false'/>";
		}
		html = html + "</td></tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercontainerno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		nlapiLogExecution('DEBUG', 'Time Stamp at the start of response',TimeStampinSec());

		var mastLPRequired = getSystemRuleValue('Consider Picking Carton as Master LP');
		if(mastLPRequired==null || mastLPRequired=='')
			mastLPRequired='N';

		var fastpick = request.getParameter('hdnfastpick');
		var getNumber = request.getParameter('custparam_number');
		var vRemaningqty=0,vDisplayedQty=0,vActqty=0;
		var vZoneId=request.getParameter('hdnebizzoneno');
		vActqty = request.getParameter('hdnActQty');
		var vReason = request.getParameter('hdnReason');
		var TotalWeight=0;
		var getEnteredContainerNo = request.getParameter('entercontainerno');
		if(getEnteredContainerNo==null || getEnteredContainerNo=="")
		{
			getEnteredContainerNo=request.getParameter('hdnFetchedContainerLPNo');
		}
		var getFetchedContainerNo = request.getParameter('hdnFetchedContainerLPNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerSize=request.getParameter('hdnContainerSize');

		var ventersize = request.getParameter('entersize');

		if(getContainerSize==null || getContainerSize=="")
		{
			getContainerSize = request.getParameter('entersize');
		}			
		var getWaveNo = request.getParameter('hdnWaveNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var vbatchno = request.getParameter('hdnbatchno');		
		var ItemNo = request.getParameter('hdnItemInternalId');
		var ItemDesc = request.getParameter('hdnItemDescription');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var PickQty = request.getParameter('hdnExpectedQuantity');
		var RcId = request.getParameter('hdnRecordInternalId');
		var EndLocation = request.getParameter('hdnEndLocInternalId');
		var RecCount = request.getParameter('hdnRecCount');
		var NextShowLocation=request.getParameter('hdnNextLocation');
		var NextShowItem=request.getParameter('hdnNextItem');
		var OrdName=request.getParameter('hdnName');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var SerialNo = request.getParameter('hdnserialno');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchno = request.getParameter('hdnbatchno');	
		var vRemQty = request.getParameter('hdnremqty');
		var NextrecordID=request.getParameter('hdnNextrecordid');
		var serailscanflag=request.getParameter('hdnserialscanflag');

		//code added on 15Feb by suman
		var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		if(ExceptionFlag=='L')
			if(EntLocID!=null&&EntLocID!="")
				EndLocation=EntLocID;
		//End of code.

		var enterlot=request.getParameter('enterlotno');
		//case# 201410374 passing location value to stock adjustment
		var vSite = request.getParameter('hdnwhlocation');
		//var Systemrules = SystemRuleForStockAdjustment();
		var Systemrules = SystemRuleForStockAdjustment(vSite);
		//nlapiLogExecution('ERROR', 'Systemrules', Systemrules);
		if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
			Systemrules=null;
		var vCarrier;
		//var vSite;
		var vCompany;
		var stgDirection="OUB";
		var vDoname=request.getParameter('hdnName');
		var vdono=request.getParameter('hdnDOLineId');
		var vPrevinvrefno=request.getParameter('hdnInvoiceRefNo');
		var SalesOrderInternalId=request.getParameter('hdnebizOrdNo');
		var vPickType = request.getParameter('hdnpicktype');
		//vSite = request.getParameter('hdnwhlocation');
		var itemstatus = request.getParameter('hdnitemstatus');

		var str = 'Container. = ' + request.getParameter('hdnContainerSize') + '<br>';
		str = str + 'vActqty. = ' + request.getParameter('hdnActQty') + '<br>';	
		str = str + 'getEnteredContainerNo. = ' + request.getParameter('entercontainerno') + '<br>';	
		str = str + 'OrdName. = ' + OrdName + '<br>';	
		str = str + 'NextShowLocation. = ' + NextShowLocation + '<br>';	
		str = str + 'getRecordInternalId. = ' + getRecordInternalId + '<br>';	
		str = str + 'getFetchedContainerNo. = ' + getFetchedContainerNo + '<br>';	
		str = str + 'getContainerSize. = ' + getContainerSize + '<br>';	
		str = str + 'ventersize. = ' + ventersize + '<br>';	
		str = str + 'NextShowItem. = ' + NextShowItem + '<br>';	
		str = str + 'ItemNo. = ' + ItemNo + '<br>';	
		str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
		str = str + 'vPickType. = ' + vPickType + '<br>';	
		str = str + 'vSite. = ' + vSite + '<br>';	
		str = str + 'getWaveNo. = ' + getWaveNo + '<br>';	
		str = str + 'InvoiceRefNo. = ' + InvoiceRefNo + '<br>';	
		str = str + 'EndLocation. = ' + EndLocation + '<br>';	
		str = str + 'BeginLocationName. = ' + BeginLocationName + '<br>';	
		str = str + 'Expected Qty. = ' + PickQty + '<br>';
		str = str + 'RcId. = ' + RcId + '<br>';
		str = str + 'itemstatus. = ' + itemstatus + '<br>';

		nlapiLogExecution('DEBUG', 'Parameter Values in Response', str);
		if(ventersize!=null && ventersize!="")
		{
			getContainerSize = ventersize;
		}	

		nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);

		//ItemFulfillment(getWaveNo, RecordInternalId, ContainerLPNo, PickQty, BeginLocation, Item, ItemDesc, ItemNo, vdono, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo, SerialNo,vBatchno);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();

		SOarray["custparam_error"] = 'INVALID CARTON #';
		SOarray["custparam_screenno"] = '13NEW';
		//case# 201410520 starts
		//SOarray["custparam_enteredQty"] =request.getParameter('hdnActQty');//201411029�
		//case# 201410520 ends
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');
		SOarray["custparam_fastpick"] = fastpick;
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		SOarray["custparam_enteredQty"] = request.getParameter('hdnActQty');
		SOarray["custparam_enteredqty"] = request.getParameter('hdnActQty');
		SOarray["custparam_number"] = getNumber;
		vSkipId=0;


		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');


		if(request.getParameter('hdnNextrecordid')!=null&&request.getParameter('hdnNextrecordid')!="")	
			SOarray["custparam_nextrecordid"] = request.getParameter('hdnNextrecordid');

		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		nlapiLogExecution('DEBUG', 'getRecordInternalId', SOarray["custparam_recordinternalid"]);	
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		//nlapiLogExecution('Error','request.getParameter(hdnItemname)',request.getParameter('hdnItemName'));
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_serialno"] = request.getParameter('hdnserialno');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = vbatchno;
		SOarray["custparam_nextlocation"] = NextShowLocation;
		SOarray["custparam_nextiteminternalid"] = NextShowItem;
		SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;
		SOarray["custparam_itemstatus"] = itemstatus;
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnextExpectedQuantity');
		//case # 20126849�added below parameters.

		SOarray["custparam_EntLoc"] = EntLoc;
		SOarray["custparam_EntLocRec"] = EntLocRec;
		SOarray["custparam_EntLocId"] = EntLocID;
		SOarray["custparam_Exc"] = ExceptionFlag;
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
		if (optedEvent == 'F7') 
		{
			SOarray["custparam_fastpick"] = fastpick;
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
			return;
		}
		if(ventersize!=null && ventersize!="")
		{
			//	Case# 201413032....

			validContainerSize = containerSizeCheck(getContainerSize,ventersize,vSite);
			if(validContainerSize!="")
			{
				SOarray["custparam_error"]=validContainerSize;

				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}

			else
			{
				getContainerSize = ventersize;

			} 

			//Case# 201413032....
		}	

		if (sessionobj!=context.getUser()) {

			//Code Added by Satish.N on 10-JUN-2013

			try
			{
				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

				var nextexpqty= request.getParameter('hdnextExpectedQuantity');
				SOarray["name"] = OrdName;
				if(vZoneId!=null && vZoneId!="")
				{
					SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
				}
				else
					SOarray["custparam_ebizzoneno"] = '';
				nlapiLogExecution('DEBUG', 'NextLocation', NextShowLocation);

				var invtqty=0;

				var IsPickFaceLoc = 'F';

				IsPickFaceLoc = isPickFaceLocation(ItemNo,EndLocInternalId);

				if(IsPickFaceLoc=='T')
				{
					var openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemNo);

					if(openinventory!=null && openinventory!='')
					{
						nlapiLogExecution('DEBUG', 'Pick Face Inventory Length', openinventory.length);

						for(var x=0; x< openinventory.length;x++)
						{
							var vinvtqty=openinventory[x].getValue('custrecord_ebiz_qoh');

							if(vinvtqty==null || vinvtqty=='' || isNaN(vinvtqty))
							{
								vinvtqty=0;
							}

							invtqty=parseFloat(invtqty)+parseFloat(vinvtqty);
						}						
					}
				}

				if(invtqty==null || invtqty=='' || isNaN(invtqty))
				{
					invtqty=0;
				}

				nlapiLogExecution('DEBUG', 'invtqty', invtqty);

				if(IsPickFaceLoc=='T' && (parseFloat(PickQty)>parseFloat(invtqty)))
				{
					var openreplns = new Array();
					openreplns=getOpenReplns(ItemNo,EndLocInternalId);

					if(openreplns!=null && openreplns!='')
					{
						for(var i=0; i< openreplns.length;i++)
						{
							//var taskpriority=openreplns[i].getValue('custrecord_taskpriority');

							var expqty=openreplns[i].getValue('custrecord_expe_qty');
							if(expqty == null || expqty == '')
							{
								expqty=0;
							}	
							nlapiLogExecution('DEBUG', 'open repln qty',expqty);
							nlapiLogExecution('DEBUG', 'getQuantity',PickQty);


							nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');

						}

						var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
						if(skiptask==null || skiptask=='')
						{
							skiptask=1;
						}
						else
						{
							skiptask=parseFloat(skiptask)+1;
						}

						nlapiLogExecution('DEBUG', 'skiptask',skiptask);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

						var vPickType = request.getParameter('hdnpicktype');

						var SOarray=new Array();
						SOarray=buildTaskArray(getWaveNo,null,OrdName,SalesOrderInternalId,vZoneId,vPickType,ContainerLPNo);
						SOarray["custparam_skipid"] = vSkipId;
						SOarray["custparam_screenno"] = 'REPLENEXP';
						SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
						SOarray["custparam_fastpick"] = fastpick;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					else
					{
						var replengen = generateReplenishment(EndLocInternalId,ItemNo,vSite,getWaveNo,'0');
						nlapiLogExecution('DEBUG', 'replengen',replengen);

						nlapiLogExecution('DEBUG', 'RecordInternalId 1',RecordInternalId);

						var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
						if(skiptask==null || skiptask=='')
						{
							skiptask=1;
						}
						else
						{
							skiptask=parseFloat(skiptask)+1;
						}

						nlapiLogExecution('DEBUG', 'skiptask',skiptask);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

						var vPickType = request.getParameter('hdnpicktype');
						var SOarray=new Array();
						nlapiLogExecution('DEBUG', 'ContainerLPNo before build array',ContainerLPNo);
						SOarray=buildTaskArray(getWaveNo,null,OrdName,SalesOrderInternalId,vZoneId,vPickType,ContainerLPNo);

						SOarray["custparam_skipid"] = vSkipId;
						SOarray["custparam_screenno"] = 'REPLENEXP';

						if(replengen!=true)
							SOarray["custparam_error"]='Replen Generation Failed.Insufficient inventory in Bulk location';
						else 
							SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';

						SOarray["custparam_fastpick"] = fastpick;

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
				}
				else
				{

					if(IsPickFaceLoc=='T' && (InvoiceRefNo==null || InvoiceRefNo==''))
					{
						if(openinventory!=null && openinventory!='')
						{
							var openinvtrecid = openinventory[0].getId();
							nlapiLogExecution('DEBUG', 'openinvtrecid', openinvtrecid);

							try
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_invref_no', openinvtrecid);
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG', 'Exception in updating inventory reference number', exp);
							}
						}
					}

					var itemSubtype = nlapiLookupField('item', ItemNo, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

//					nlapiLogExecution('DEBUG', 'itemSubtype.recordType', itemSubtype.recordType);
//					nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizserialout', itemSubtype.custitem_ebizserialout);
					nlapiLogExecution('DEBUG', 'SOarray["custparam_serialscanflag"]', serailscanflag);

					if(request.getParameter('hdnConfirmflag')=='ENT'){
						if(serailscanflag!="TRUE"){
							if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') {
								nlapiLogExecution('DEBUG', 'itemSubtype.recordType', itemSubtype.recordType);
								nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizserialout', itemSubtype.custitem_ebizserialout);
								if(request.getParameter('entercontainerno')!=null && request.getParameter('entercontainerno')!='')
								{
									if ((/[^a-z0-9\.]/gi).test(request.getParameter('entercontainerno'))) 
									{
										nlapiLogExecution('DEBUG', 'checking for Special characters', 'done');
										SOarray["custparam_error"] = 'CARTON# SHOULD NOT BE CONTAIN SPECIAL CHARACTERS';
										SOarray["custparam_fastpick"] = fastpick;
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
										return;
									}
									if(getEnteredContainerNo!=getFetchedContainerNo)
									{
										//Case# 201411088 starts
										var vOpenTaskRecs=fnValidateEnteredContLP(getEnteredContainerNo,SalesOrderInternalId);
										nlapiLogExecution('DEBUG', 'Time Stamp at the end of fnValidateEnteredContLP',TimeStampinSec());
										if(vOpenTaskRecs != null && vOpenTaskRecs != '')
										{
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
											return;
											nlapiLogExecution('DEBUG', 'Error:-vOpenTaskRecs ', 'Container# is invalid');
										}
										else
										{	
											var result = fnValidateContainer(getEnteredContainerNo,getFetchedContainerNo, '2','2',vSite);
											if(result == false)
											{
												response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
												nlapiLogExecution('DEBUG', 'Error: ', 'Container# is invalid');
												return;
											}
										}
									}

									//Case# 201411088 ends
								}
								//201413266,201413264 start
								if(request.getParameter('custparam_number') !=null && request.getParameter('custparam_number') !='')
									SOarray["custparam_number"]=request.getParameter('custparam_number');
								else
									SOarray["custparam_number"] = 0;
								//201413266,201413264 end
								SOarray["custparam_RecType"] = itemSubtype.recordType;
								SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
								SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
								SOarray["custparam_fastpick"] = fastpick;
								SOarray["custparam_serialscanflag"] = 'FALSE';
								response.sendRedirect('SUITELET', 'customscript_rf_fastpick_serialscan', 'customdeploy_rf_fastpick_serialscan_di', false, SOarray);
								return;
							}	
						}
					}

				}

				//Upto here on 10-JUN-2013

				//	if the previous button 'F7' is clicked, it has to go to the previous screen 
				//  ie., it has to go to accept SO #.
				/*if (optedEvent == 'F7') 
				{
					SOarray["custparam_fastpick"] = fastpick;
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
				}*/
				if (request.getParameter('hdnConfirmflag') == 'ENT') 
				{
					//Start
					var opentaskcountNav=0;
					nlapiLogExecution('DEBUG', 'vbatchno', vbatchno);
					nlapiLogExecution('DEBUG', 'enterlot', enterlot);
					if(enterlot!=null && enterlot!='' && vbatchno!=enterlot)
					{
						var result=GetBatchId(ItemNo,enterlot);
						var BatchId=result[0];
						var IsValidflag=result[1];
						nlapiLogExecution('DEBUG', 'IsValidflag', IsValidflag);
						if(IsValidflag=='T')
						{
							var result=IsBatchNoValid(SOarray["custparam_endlocinternalid"],ItemNo,PickQty,BatchId);
							if(result.length>0){
								var IsValidBatchForBinLoc=result[0];
								var NewInvtRecId=result[1];
								var NewLp=result[2];
								var NewPackcode=result[3];
								var NewItemStatus=result[4];

//								nlapiLogExecution('DEBUG', 'IsValidBatchForBinLoc', IsValidBatchForBinLoc);
//								nlapiLogExecution('DEBUG', 'NewInvtRecId', NewInvtRecId);
//								nlapiLogExecution('DEBUG', 'NewItemStatus', NewItemStatus);
//								nlapiLogExecution('DEBUG', 'IsValidBatchForBinLoc', IsValidBatchForBinLoc);
								if(IsValidBatchForBinLoc=='T')
								{


									//	nlapiLogExecution('DEBUG', 'Wave picking', 'Wave Picking');
									//update open task with new lot# and Invtrefno
									var UpdateOpentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
									UpdateOpentask.setFieldValue('custrecord_batch_no', enterlot);
									UpdateOpentask.setFieldValue('custrecord_lpno', NewLp);
									//UpdateOpentask.setFieldValue('custrecord_packcode', NewPackcode);
									//UpdateOpentask.setFieldValue('custrecord_sku_status', NewItemStatus);
									UpdateOpentask.setFieldValue('custrecord_invref_no',NewInvtRecId); 
									nlapiSubmitRecord(UpdateOpentask, false, true);

									//deallocate old lot# allocated Qty in create inventory					
									var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',InvoiceRefNo);
									var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
									var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
									var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
									var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
									var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

//									nlapiLogExecution('DEBUG', 'qty', qty);
//									nlapiLogExecution('DEBUG', 'allocqty', allocqty);
//									nlapiLogExecution('DEBUG', 'getExpectedQuantity', PickQty);
									transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(PickQty)));
									nlapiSubmitRecord(transaction, false, true);


									//update allocated qty to new Lot# in create inventory
									var InvtDetails = nlapiLoadRecord('customrecord_ebiznet_createinv', NewInvtRecId);
									var Invtqty = InvtDetails.getFieldValue('custrecord_ebiz_qoh');
									var Invtallocqty = InvtDetails.getFieldValue('custrecord_ebiz_alloc_qty');
									var InvtvLP=InvtDetails.getFieldValue('custrecord_ebiz_inv_lp');
									var InvtvPackcode=InvtDetails.getFieldValue('custrecord_ebiz_inv_packcode');

									InvtDetails.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invtallocqty)+ parseFloat(PickQty)));
									nlapiSubmitRecord(InvtDetails, false, true);

									vbatchno=enterlot;

									SOarray["custparam_batchno"] = vbatchno;
									//SOarray["custparam_entloc"] = getBeginLocation;
									SOarray["custparam_recid"] = NewInvtRecId;
									//SOarray["custparam_entlocid"] = SOarray["custparam_endlocinternalid"]; 
									SOarray["custparam_Exc"] = 'L';
									//response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
									//nlapiLogExecution('DEBUG', 'Done customrecord1', 'Success');
									//response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
									nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');	


								}
								else
								{
									SOarray["custparam_error"] = 'INVALID LOT#';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
									nlapiLogExecution('DEBUG', 'Error: ', 'Does not belong to the bin location');
									return;
								}

							}
							else
							{
								SOarray["custparam_error"] = 'INVALID LOT#';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								return;
							}
						}
						else 
						{
							SOarray["custparam_error"] = 'INVALID LOT#';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;

							//nlapiLogExecution('DEBUG', 'Error: ', 'Scanned batch is invalid');
						}
					}


					//End

					var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
					var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
					getLPContainerSize= SORec.getFieldText('custrecord_container');
					var pickMethod=SORec.getFieldValue('custrecord_ebizmethod_no');
					var wmsstatusflag=SORec.getFieldValue('custrecord_wms_status_flag');
					nlapiLogExecution('DEBUG', 'wmsstatusflag', wmsstatusflag);
					if(pickMethod!=null && pickMethod!='' && pickMethod!='null')
					{
						pickMethod=parseInt(pickMethod);
					}
					opentaskcountNav=getOpenTasksCountForNavigation(getWaveNo,getFetchedContainerNo,vPickType,SalesOrderInternalId,OrdName,vZoneId,vSite,pickMethod);
					nlapiLogExecution('DEBUG', 'opentaskcountNav', opentaskcountNav);


					var opentaskcount=0;

					nlapiLogExecution('DEBUG', 'Time Stamp at the start of getOpenTasksCount',TimeStampinSec());
					opentaskcount=getOpenTasksCount(getWaveNo,getFetchedContainerNo,vPickType,SalesOrderInternalId,OrdName,vZoneId);
					nlapiLogExecution('DEBUG', 'Time Stamp at the end of getOpenTasksCount',TimeStampinSec());

					nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
					//For fine tuning pick confirmation and doing autopacking in user event after last item
					var IsitLastPick='F';
					if(opentaskcount > parseInt(vSkipId) + 1)
						IsitLastPick='F';
					else
						IsitLastPick='T';

					//For qty exception
					nlapiLogExecution('DEBUG', 'RcId', RcId);
					var InvQOH=0;



					if(wmsstatusflag!=9)
					{
						nlapiLogExecution('ERROR', 'UserRefreshedThePage', "UserRefreshedThePage");
						nlapiLogExecution('ERROR', 'opentaskcountNav', opentaskcountNav);

						nlapiLogExecution('ERROR', 'BeginLocation', BeginLocation);
						nlapiLogExecution('ERROR', 'NextShowLocation', NextShowLocation);
						nlapiLogExecution('ERROR', 'ItemNo', ItemNo);
						nlapiLogExecution('ERROR', 'NextShowItem', NextShowItem);			
						nlapiLogExecution('ERROR', 'BeginLocationName', BeginLocationName);
						nlapiLogExecution('ERROR', 'nextexpqty', nextexpqty);


						if(opentaskcountNav >  0)
						{
							if(BeginLocationName != NextShowLocation)
							{ 
								SOarray["custparam_beginLocationname"]=null;
								SOarray["custparam_iteminternalid"]=null;
								SOarray["custparam_expectedquantity"]=null;
								SOarray["custparam_remqty"]=null;
								SOarray["custparam_fastpick"] = fastpick;
								nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								

							}
					else if(ItemName != NextShowItem)
							{ 
								nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
								SOarray["custparam_iteminternalid"] = NextShowItem;
								SOarray["custparam_beginLocationname"]=null;
								SOarray["custparam_expectedquantity"]=null;
								SOarray["custparam_remqty"]=null;
								SOarray["custparam_fastpick"] = fastpick;
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								

							}
					else if(ItemName == NextShowItem)
							{ 
								var SOFilters = new Array();

								SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
								SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

								if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && getWaveNo != null && getWaveNo != "")
								{
									SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
								}	 
								if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
								{
									nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
									SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
								}
								if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
								{
									nlapiLogExecution('DEBUG', 'SO Id inside If', SalesOrderInternalId);
									SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
								}
								if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
								{
									nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
									SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
								}
								if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getFetchedContainerNo!=null && getFetchedContainerNo!="" && getFetchedContainerNo!= "null")
								{
									nlapiLogExecution('DEBUG', 'Carton # inside If', getFetchedContainerNo);
									SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));			
								}

								var SOColumns=new Array();
								SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
								SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
								SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
								SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
								SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
								SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
								SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
								SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
								SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
								SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
								SOColumns.push(new nlobjSearchColumn('name'));
								SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
								SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
								SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
								SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));


								SOColumns[0].setSort();
								SOColumns[1].setSort();
								SOColumns[2].setSort();
								SOColumns[3].setSort();
								SOColumns[4].setSort();
								SOColumns[5].setSort(true);


								var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
								if(SOSearchResults!=null && SOSearchResults!='')
									SOarray["custparam_recordinternalid"]=SOSearchResults[0].getId();

								SOarray["custparam_iteminternalid"] = NextShowItem;
								SOarray["custparam_beginLocationname"]=null;
								SOarray["custparam_expectedquantity"]=nextexpqty;
								SOarray["custparam_remqty"]=null;
								nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
								SOarray["custparam_fastpick"] = fastpick;
								SOarray["custparam_EntLoc"] = null;
								SOarray["custparam_EntLocRec"] = null;
								SOarray["custparam_EntLocId"] = null;
								SOarray["custparam_Exc"] = null;
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);								
								nlapiLogExecution('DEBUG', 'test1', 'done');

							}
						}
						else{
							nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
							//For fine tuning pick confirmation and doing autopacking in user event after last item				
							nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
							var SkipStaging = getSystemRuleValue('Skip Staging?');
							nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
							if(SkipStaging!="Y")
							{
								SOarray["custparam_fastpick"] = fastpick;
								response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
							}
							else
							{
								AutoStageRecordcreaton(SOarray);
							}
						}
//						response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
//						return;

					}
					else
					{
						if(request.getParameter('entercontainerno')==''||request.getParameter('entercontainerno')==null)
						{
							var cartonrequiredflag = getSystemRule('RF Picking Carton # Scan Required?');
							nlapiLogExecution('DEBUG', 'cartonrequiredflag', cartonrequiredflag);

							if(cartonrequiredflag=='Y')
							{
								SOarray["custparam_error"] = 'PLEASE ENTER/SCAN CARTON#';
								SOarray["custparam_fastpick"] = fastpick;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								return;
							}
						}

						if ((/[^a-z0-9\.]/gi).test(request.getParameter('entercontainerno'))) 
						{
							SOarray["custparam_error"] = 'CARTON# SHOULD NOT BE CONTAIN SPECIAL CHARACTERS';
							SOarray["custparam_fastpick"] = fastpick;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
						}
						if (getEnteredContainerNo==''||getEnteredContainerNo==null||getEnteredContainerNo.replace(/\s+$/,"") == getFetchedContainerNo)
						{
							nlapiLogExecution('DEBUG', 'getEnteredContainerNo = getFetchedContainerNo');

							nlapiLogExecution('DEBUG', 'Time Stamp at the start of getSKUCubeAndWeightforconfirm',TimeStampinSec());
							var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
							nlapiLogExecution('DEBUG', 'Time Stamp at the end of getSKUCubeAndWeightforconfirm',TimeStampinSec());
							var itemCube = 0;
							var itemWeight=0;						

							if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
								//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
								itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
								itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	
							}

							var ContainerCube;					
							var containerInternalId;
							var ContainerSize;
							nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	

							if(getContainerSize=="" || getContainerSize==null)
							{
								getContainerSize=ContainerSize;					
							}
							nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	
							nlapiLogExecution('DEBUG', 'Time Stamp at the start of getContainerCubeAndTarWeight',TimeStampinSec());
							// Case# 201411135 starts
							//var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);
							var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize,vSite);
							// Case# 201411135 ends
							nlapiLogExecution('DEBUG', 'Time Stamp at the end of getContainerCubeAndTarWeight',TimeStampinSec());
							if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

								ContainerCube =  parseFloat(arrContainerDetails[0]);						
								containerInternalId = arrContainerDetails[3];
								ContainerSize=arrContainerDetails[4];
								TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
							} 
							nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);	
							nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	

							//UpdateOpenTask,fulfillmentorder

							nlapiLogExecution('DEBUG', 'Time Stamp at the start of UpdateRFFulfillOrdLine',TimeStampinSec());
							UpdateRFFulfillOrdLine(vdono,vActqty);
							nlapiLogExecution('DEBUG', 'Time Stamp at the end of UpdateRFFulfillOrdLine',TimeStampinSec());
							nlapiLogExecution('DEBUG', 'Before Process', RcId);	

							nlapiLogExecution('DEBUG', 'Time Stamp at the start of UpdateRFOpenTask',TimeStampinSec());
							var vResults=UpdateRFOpenTask(RcId,vActqty, containerInternalId,getEnteredContainerNo,itemCube,itemWeight,EndLocation,vReason,PickQty,
									IsitLastPick,ExceptionFlag,EntLocRec,ItemNo,SalesOrderInternalId,vRemQty,InvoiceRefNo,vbatchno,
									Systemrules,SerialNo,mastLPRequired,request);
							if(vResults != null && vResults.indexOf('ERROR') != -1)
							{
								SOarray["custparam_error"] = 'Insufficient Inventory.  Please do quantity exception';//'INVALID CARTON #';
								SOarray["custparam_screenno"] = '12EXP';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('Debug', 'Error: ', 'Insufficient Inventory');
							}
							nlapiLogExecution('DEBUG', 'Time Stamp at the end of UpdateRFOpenTask',TimeStampinSec());

							vRemaningqty = PickQty-vActqty;
							var recordcount=1;//it is iterator in GUI
							nlapiLogExecution('DEBUG', 'vDisplayedQty', PickQty);	
							nlapiLogExecution('DEBUG', 'vActqty', vActqty);	
							if(PickQty != vActqty)
							{
								//case# 20149020 added request field also
								nlapiLogExecution('DEBUG', 'inexception condition1', 'inexception condition');	
								CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,
										vbatchno,vRemQty,Systemrules,request);//have to check for wt and cube calculation
							}

							nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
							nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);
							nlapiLogExecution('DEBUG', 'BeginLocationName', BeginLocationName);
							nlapiLogExecution('DEBUG', 'NextShowLocation', NextShowLocation);
							nlapiLogExecution('DEBUG', 'ItemNo', ItemNo);
							nlapiLogExecution('DEBUG', 'NextShowItem', NextShowItem);
							nlapiLogExecution('DEBUG', 'nextexpqty', nextexpqty);
							SOarray["custparam_number"] = 0;
							if(opentaskcountNav >  1)
							{
								if(BeginLocationName != NextShowLocation)
								{ 
									SOarray["custparam_beginLocationname"]=null;
									SOarray["custparam_iteminternalid"]=null;
									SOarray["custparam_expectedquantity"]=null;
									SOarray["custparam_remqty"]=null;
									SOarray["custparam_fastpick"] = fastpick;
									nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								

								}
						else if(ItemName != NextShowItem)
								{ 
									nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
									SOarray["custparam_iteminternalid"] = NextShowItem;
									SOarray["custparam_beginLocationname"]=null;
									SOarray["custparam_expectedquantity"]=null;
									SOarray["custparam_remqty"]=null;
									SOarray["custparam_fastpick"] = fastpick;
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								

								}
						else if(ItemName == NextShowItem)
								{ 
									var SOFilters = new Array();

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

									if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && getWaveNo != null && getWaveNo != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
									}	 
									if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
									{
										nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
										SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
									}
									if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
									{
										nlapiLogExecution('DEBUG', 'SO Id inside If', SalesOrderInternalId);
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
									}
									if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
									{
										nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
										SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
									}
									if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getFetchedContainerNo!=null && getFetchedContainerNo!="" && getFetchedContainerNo!= "null")
									{
										nlapiLogExecution('DEBUG', 'Carton # inside If', getFetchedContainerNo);
										SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));			
									}

									var SOColumns=new Array();
									SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//									SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
									//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
									SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
									//Code end as on 290414
									SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
									SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
									SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
									SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
									SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
									SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
									SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
									SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
									SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
									SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
									SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
									SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
									SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
									SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
									SOColumns.push(new nlobjSearchColumn('name'));
									SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
									SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
									SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
									SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

									SOColumns[0].setSort();
									SOColumns[1].setSort();
									SOColumns[2].setSort();
									SOColumns[3].setSort();
									SOColumns[4].setSort();
									SOColumns[5].setSort(true);

									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									if(SOSearchResults!=null && SOSearchResults!='')
									{
										var SOSearchResult = SOSearchResults[0];	
										SOarray["custparam_recordinternalid"]=SOSearchResult.getId();
										SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
										SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
										SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
										SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
										SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
										SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
										SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
										SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
										SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
										SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
										SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
										SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
										SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
										SOarray["custparam_noofrecords"] = SOSearchResults.length;		
										SOarray["name"] =  SOSearchResult.getValue('name');
										SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
										SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
										SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
										SOarray["custparam_fastpick"] = 'Y';
										SOarray["custparam_enteredQty"] = '';
										SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');	

										if(SOSearchResults.length > 1)
										{
											var SOSearchnextResult = SOSearchResults[1];
											SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
											SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
											SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
											nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
											nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
											nlapiLogExecution('DEBUG', 'Next Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));	
										}
										else
										{
											var SOSearchnextResult = SOSearchResults[0];
											SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
											SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
											nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
											nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
										}


//										SOarray["custparam_iteminternalid"] = NextShowItem;
//										SOarray["custparam_beginLocationname"]=null;
//										SOarray["custparam_expectedquantity"]=nextexpqty;
										SOarray["custparam_remqty"]=null;
										nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
										//SOarray["custparam_fastpick"] = fastpick;
										//Set values to empty because of Location exception done for this line but if we have same item from same bin with another task system showing exception bin location for other task also
										SOarray["custparam_EntLoc"] = null;
										SOarray["custparam_EntLocRec"] = null;
										SOarray["custparam_EntLocId"] = null;
										SOarray["custparam_Exc"] = null;
										SOarray["custparam_enteredQty"] = "";
										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);								
										nlapiLogExecution('DEBUG', 'test1', 'done');
									}
									else
									{
										nlapiLogExecution('DEBUG', 'Navigating To11', 'Stage Location');
										var SkipStaging = getSystemRuleValue('Skip Staging?');
										nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
										if(SkipStaging!="Y")
										{
											SOarray["custparam_fastpick"] = fastpick;
											response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
										}
										else
										{
											AutoStageRecordcreaton(SOarray);
										}
									}
								}
							}
							else{

								//For fine tuning pick confirmation and doing autopacking in user event after last item				
								nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
								var SkipStaging = getSystemRuleValue('Skip Staging?');
								nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
								if(SkipStaging!="Y")
								{
									SOarray["custparam_fastpick"] = fastpick;
									response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
								}
								else
								{
									AutoStageRecordcreaton(SOarray);
								}
							}
						}

						else if (getEnteredContainerNo != '' && getEnteredContainerNo != getFetchedContainerNo)
						{
							nlapiLogExecution('DEBUG', 'getEnteredContainerNo', 'userdefined');
							nlapiLogExecution('DEBUG', 'Time Stamp at the start of fnValidateEnteredContLP',TimeStampinSec());
							var vOpenTaskRecs=fnValidateEnteredContLP(getEnteredContainerNo,SalesOrderInternalId);
							nlapiLogExecution('DEBUG', 'Time Stamp at the end of fnValidateEnteredContLP',TimeStampinSec());
							if(vOpenTaskRecs != null && vOpenTaskRecs != '')
							{
								SOarray["custparam_enteredQty"] =request.getParameter('hdnActQty');//201411029?
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('DEBUG', 'Error: ', 'Container# is invalid');
								return;
							}
							else
							{	
								var result = fnValidateContainer(getEnteredContainerNo,getFetchedContainerNo, '2','2',vSite);
								if(result == false)
								{
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
									nlapiLogExecution('DEBUG', 'Error: ', 'Container# is invalid');
									return;
								}

								var getEnteredContainerNoPrefix = getEnteredContainerNo.substring(0, 3).toUpperCase();
								var LPReturnValue = ebiznet_LPRange_CL_withLPType(getEnteredContainerNo, '2','2',vSite);//'2'UserDefiend,'2'PICK
								nlapiLogExecution('DEBUG', 'Time Stamp at the end of ebiznet_LPRange_CL_withLPType',TimeStampinSec());
								if(LPReturnValue == true){
									nlapiLogExecution('DEBUG', 'getItem', ItemNo);
									var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
									nlapiLogExecution('DEBUG', 'Time Stamp at the end of getSKUCubeAndWeightforconfirm',TimeStampinSec());
									var itemCube = 0;
									var itemWeight=0;						
									if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
										itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
										itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//									
									} 

									var ContainerCube;					
									var containerInternalId;
									var ContainerSize;
									if(getContainerSize=="" || getContainerSize==null)
									{
										getContainerSize=ContainerSize;
										nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	
										ContainerCube = itemCube;
										TotalWeight=itemWeight;
									}	
									else
									{
										// Case# 201411135 starts
										//var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);
										var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize,vSite);
										// Case# 201411135 ends
										nlapiLogExecution('DEBUG', 'Time Stamp at the end of getContainerCubeAndTarWeight',TimeStampinSec());
										if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

											ContainerCube =  parseFloat(arrContainerDetails[0]);						
											containerInternalId = arrContainerDetails[3];
											ContainerSize=arrContainerDetails[4];
											TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));// added on 28 nov
										} 
									}
									nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);				
									//Insert LP Record
									//var ResultText=GenerateLable('1');
									var ResultText='';
									//nlapiLogExecution('DEBUG', 'Time Stamp at the end of GenerateLable',TimeStampinSec());
									CreateRFMasterLPRecord(getEnteredContainerNo,containerInternalId,ContainerCube,TotalWeight,getContainerSize,ResultText);
									nlapiLogExecution('DEBUG', 'Time Stamp at the end of CreateRFMasterLPRecord',TimeStampinSec());
									//added by suman as on 05/08/12.
									//update all the open picks with the new container LP#.
									UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SalesOrderInternalId,OrdName,getWaveNo,vZoneId,vPickType,containerInternalId);
									//end of the code as of 05/08/12.
									nlapiLogExecution('DEBUG', 'Time Stamp at the end of UpdateAllContainerLp',TimeStampinSec());
									UpdateRFFulfillOrdLine(vdono,vActqty);
									nlapiLogExecution('DEBUG', 'Time Stamp at the end of UpdateRFFulfillOrdLine',TimeStampinSec());
									//UpdateOpenTask
									nlapiLogExecution('DEBUG', 'Before Process', RcId);	
									var vResults=UpdateRFOpenTask(RcId,vActqty, containerInternalId,getEnteredContainerNo,itemCube,itemWeight,EndLocation,vReason,PickQty,
											IsitLastPick,ExceptionFlag,EntLocRec,ItemNo,SalesOrderInternalId,vRemQty,InvoiceRefNo,
											vbatchno,Systemrules,SerialNo,mastLPRequired,request);
									if(vResults != null && vResults.indexOf('ERROR') != -1)
									{
										SOarray["custparam_error"] = 'Insufficient Inventory.  Please do quantity exception';//'INVALID CARTON #';
										SOarray["custparam_screenno"] = '12EXP';
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
										nlapiLogExecution('Debug', 'Error: ', 'Insufficient Inventory');
									}
									nlapiLogExecution('DEBUG', 'Time Stamp at the end of UpdateRFOpenTask',TimeStampinSec());
									updatelastpicktaskforoldcarton(getFetchedContainerNo,SalesOrderInternalId);
									nlapiLogExecution('DEBUG', 'Time Stamp at the end of updatelastpicktaskforoldcarton',TimeStampinSec());

									//create new record in opentask,fullfillordline when we have qty exception
									vRemaningqty = PickQty-vActqty;
									var recordcount=1;//it is iterator in GUI
									if(PickQty != vActqty)
									{
										//case# 20149020 added request field also
										nlapiLogExecution('DEBUG', 'inexception condition2', 'inexception condition');	
										CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,
												vbatchno,vRemQty,Systemrules,request);//have to check for wt and cube calculation
									}

									nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
									nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);
									nlapiLogExecution('DEBUG', 'BeginLocationName', BeginLocationName);
									nlapiLogExecution('DEBUG', 'NextShowLocation', NextShowLocation);
									nlapiLogExecution('DEBUG', 'ItemNo', ItemNo);
									nlapiLogExecution('DEBUG', 'NextShowItem', NextShowItem);
									nlapiLogExecution('DEBUG', 'nextexpqty', nextexpqty);
									nlapiLogExecution('DEBUG', 'ItemName', ItemName);
									nlapiLogExecution('DEBUG', 'opentaskcountNav', opentaskcountNav);
									SOarray["custparam_number"] = 0;

									//if(RecCount > 1)
									//if(opentaskcount > parseInt(vSkipId) + 1)
									if(opentaskcountNav > 1)
									{
										if(BeginLocationName != NextShowLocation)
										{ 
											SOarray["custparam_beginLocationname"]=null;
											SOarray["custparam_iteminternalid"]=null;
											SOarray["custparam_expectedquantity"]=null;
											SOarray["custparam_remqty"]=null;
											SOarray["custparam_fastpick"] = fastpick;
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								
											nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
										}
								else if(ItemName != NextShowItem)
										{ 
											SOarray["custparam_iteminternalid"] = NextShowItem;
											SOarray["custparam_beginLocationname"]=null;
											SOarray["custparam_expectedquantity"]=null;
											SOarray["custparam_remqty"]=null;
											SOarray["custparam_fastpick"] = fastpick;
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								
											nlapiLogExecution('DEBUG', 'Navigating To1 chek', 'Picking Item');
										}
								else if(ItemName == NextShowItem)
										{ 
											var SOFilters = new Array();

											SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
											SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

											if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && getWaveNo != null && getWaveNo != "")
											{
												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
											}	 
											if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
											{
												nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
												SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
											}
											if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
											{
												nlapiLogExecution('DEBUG', 'SO Id inside If', SalesOrderInternalId);
												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
											}
											if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
											{
												nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
												SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
											}											
											if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getFetchedContainerNo!=null && getFetchedContainerNo!="" && getFetchedContainerNo!= "null")
											{
												nlapiLogExecution('DEBUG', 'Carton # inside If', getFetchedContainerNo);
												SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));			
											}

											var SOColumns=new Array();
											SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//											SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
											//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
											SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
											//Code end as on 290414
											SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
											SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
											SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
											SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
											SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
											SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
											SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
											SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
											SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
											SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
											SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
											SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
											SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
											SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
											SOColumns.push(new nlobjSearchColumn('name'));
											SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
											SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
											SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
											SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

											SOColumns[0].setSort();
											SOColumns[1].setSort();
											SOColumns[2].setSort();
											SOColumns[3].setSort();
											SOColumns[4].setSort();
											SOColumns[5].setSort(true);

											var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
											if(SOSearchResults!=null && SOSearchResults!='')
											{

												var SOSearchResult = SOSearchResults[0];	
												SOarray["custparam_recordinternalid"]=SOSearchResult.getId();
												SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
												SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
												SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
												SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
												SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
												SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
												SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
												SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
												SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
												SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
												SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
												SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
												SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
												SOarray["custparam_noofrecords"] = SOSearchResults.length;		
												SOarray["name"] =  SOSearchResult.getValue('name');
												SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
												SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
												SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
												SOarray["custparam_fastpick"] = 'Y';
												SOarray["custparam_enteredQty"] = '';
												SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');	

												if(SOSearchResults.length > 1)
												{
													var SOSearchnextResult = SOSearchResults[1];
													SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
													SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
													SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
													nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
													nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
													nlapiLogExecution('DEBUG', 'Next Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));	
												}
												else
												{
													var SOSearchnextResult = SOSearchResults[0];
													SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
													SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
													nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
													nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
												}

//												SOarray["custparam_iteminternalid"] = NextShowItem;
//												SOarray["custparam_beginLocationname"]=null;
//												SOarray["custparam_expectedquantity"]=nextexpqty;
												SOarray["custparam_remqty"]=null;
												nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
												//SOarray["custparam_fastpick"] = fastpick;
												SOarray["custparam_enteredQty"] = "";
												SOarray["custparam_EntLoc"] = null;
												SOarray["custparam_EntLocRec"] = null;
												SOarray["custparam_EntLocId"] = null;
												SOarray["custparam_Exc"] = null;
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);								
												nlapiLogExecution('DEBUG', 'test1', 'done');
											}
											else
											{
												nlapiLogExecution('DEBUG', 'Navigating To22', 'Stage Location');
												var SkipStaging = getSystemRuleValue('Skip Staging?');
												nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
												if(SkipStaging!="Y")
												{
													SOarray["custparam_fastpick"] = fastpick;
													response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
												}
												else
												{
													AutoStageRecordcreaton(SOarray);
												}
											}
										}
									}
									else{
										nlapiLogExecution('DEBUG', 'Navigating To2', 'Stage Location');
										var SkipStaging = getSystemRuleValue('Skip Staging?');
										nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
										if(SkipStaging!="Y")
										{
											SOarray["custparam_fastpick"] = fastpick;
											response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
										}
										else
										{
											AutoStageRecordcreaton(SOarray);
										}
									}
								}
								else  
								{
									SOarray["custparam_fastpick"] = fastpick;
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
									nlapiLogExecution('DEBUG', 'Error: ', 'Container# is invalid');
								}
							}
						}
						else if((getEnteredContainerNo != "" && getEnteredContainerNo != null) || getFetchedContainerNo=="" || getFetchedContainerNo==null)
						{
							nlapiLogExecution('DEBUG', 'else', 'userdefined');
							nlapiLogExecution('DEBUG', 'Before Process', RcId);	
							var vResults=UpdateRFOpenTask(RcId,vActqty, null,null,null,null,EndLocation,vReason,PickQty,
									IsitLastPick,ExceptionFlag,EntLocRec,ItemNo,SalesOrderInternalId,vRemQty,InvoiceRefNo,
									vbatchno,null,SerialNo,mastLPRequired,request);
							if(vResults != null && vResults.indexOf('ERROR') != -1)
							{
								SOarray["custparam_error"] = 'Insufficient Inventory.  Please do quantity exception';//'INVALID CARTON #';
								SOarray["custparam_screenno"] = '12EXP';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('Debug', 'Error: ', 'Insufficient Inventory');
							}
							//create new record in opentask,fullfillordline when we have qty exception
							vRemaningqty = PickQty-vActqty;
							var recordcount=1;//it is iterator in GUI
							if(PickQty != vActqty)
							{
								//case# 20149020 added request field also
								nlapiLogExecution('DEBUG', 'inexception condition3', 'inexception condition');	
								CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,vbatchno,vRemQty,'',request);//have to check for wt and cube calculation
							}

							if(opentaskcountNav >  1)
							{
						if(BeginLocationName != NextShowLocation)
								{  
									response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
									nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
								}
						else if(ItemName != NextShowItem)
								{ 
									SOarray["custparam_iteminternalid"] = NextShowItem;
									response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								

								}
						else if(ItemName == NextShowItem)
								{ 
									response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								

								}

							}
							else{

								nlapiLogExecution('DEBUG', 'Navigating To3', 'Stage Location');
								var SkipStaging = getSystemRuleValue('Skip Staging?');
								nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
								if(SkipStaging!="Y")
								{
									SOarray["custparam_fastpick"] = fastpick;
									response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
								}
								else
								{
									AutoStageRecordcreaton(SOarray);
								}
							}
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

						}
					}
				}
				else if (request.getParameter('hdnQtyExcpflag') == 'F2')
				{
					SOarray["custparam_fastpick"] = fastpick;
					SOarray["custparam_serialscanflag"] = request.getParameter('hdnserialscanflag');
					nlapiLogExecution('DEBUG', 'SOarray[custparam_serialscanflag] in qty exception',SOarray["custparam_serialscanflag"]);
					var WaveNo = request.getParameter('hdnWaveNo');
					var ebizOrdNo=request.getParameter('hdnebizOrdNo');
					var OrdName=request.getParameter('hdnName');
					var vPickType = request.getParameter('hdnpicktype');
					var vRecId = request.getParameter('hdnRecordInternalId');
					nlapiLogExecution('DEBUG', 'getItem in F2 before', SOarray["custparam_item"]);
					SOarray["custparam_nextrecordid"]=getNextOpentaskInternalId(WaveNo,vClusterNo,ebizOrdNo,vZoneId,vSkipId,SOarray,vPickType,OrdName,vRecId,request,getFetchedContainerNo);
					nlapiLogExecution('DEBUG', 'SOarray[custparam_nextrecordid]',SOarray["custparam_nextrecordid"]);
					nlapiLogExecution('DEBUG', 'Clicked on hdnQtyExcpflag', request.getParameter('hdnQtyExcpflag'));
					nlapiLogExecution('DEBUG', 'getItem in F2 after', SOarray["custparam_item"]);
					response.sendRedirect('SUITELET', 'customscript_rf_pickingqtyexception', 'customdeploy_rf_pickingqtyexception_di', false, SOarray);
					return;
				}
				else if (request.getParameter('hdnLocExcpflag') == 'F3') 
				{
					SOarray["custparam_fastpick"] = fastpick;
					SOarray["custparam_serialscanflag"] = request.getParameter('hdnserialscanflag');
					nlapiLogExecution('DEBUG', 'SOarray[custparam_serialscanflag] in loc exception',SOarray["custparam_serialscanflag"]);
					// case no 20126817� start 
//					new qty should dispaly if we perform qty exception before loc exception
					if(request.getParameter('hdnActQty') != null && request.getParameter('hdnActQty') != '')
SOarray["custparam_displayquantity"] = request.getParameter('hdnActQty');
					//	SOarray["custparam_expectedquantity"] = request.getParameter('hdnActQty');
					// case no 20126817�end
					nlapiLogExecution('DEBUG', 'getexpected qty field value after F2 ', SOarray["custparam_expectedquantity"]);
					nlapiLogExecution('DEBUG', 'Clicked on cmdLocException', request.getParameter('hdnLocExcpflag'));
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_loc_exception', 'customdeploy_ebiz_rf_pick_loc_exc_di', false, SOarray);
				}
				else if (request.getParameter('hdnCartExcpflag') == 'F11') 
				{
					//case# 20148755 starts
					if(request.getParameter('hdnActQty') != null && request.getParameter('hdnActQty') != '')
						SOarray["custparam_expectedquantity"] = request.getParameter('hdnActQty');
					//case# 20148755 ends
					SOarray["custparam_fastpick"] = fastpick;
					nlapiLogExecution('DEBUG', 'Clicked on CartExcpflag', request.getParameter('hdnCartExcpflag'));
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcartonxfer', 'customdeploy_ebiz_rf_pickingcartonxfer', false, SOarray);
				}
				else if (request.getParameter('hdnlotExcpflag') == 'F12') 
				{
					//case# 20148755 starts
					if(request.getParameter('hdnActQty') != null && request.getParameter('hdnActQty') != '')
						SOarray["custparam_expectedquantity"] = request.getParameter('hdnActQty');
					//case# 20148755 ends
					SOarray["custparam_fastpick"] = fastpick;
					nlapiLogExecution('DEBUG', 'Clicked on LotExcpflag', request.getParameter('hdnlotExcpflag'));
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_lot_exp', 'customdeploy_ebiz_rf_pick_lot_exp_di', false, SOarray);
				}
			}
			catch (e)
			{
				SOarray["custparam_screenno"] = '13NEW';
				SOarray["custparam_error"]=e;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			}
			finally
			{
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');
			}
		}
		else
		{
			SOarray["custparam_screenno"] = '13NEW';
			SOarray["custparam_error"]='PICK IS ALREADY IN PROCESS';
			SOarray["custparam_fastpick"] = fastpick;
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}

function getOpenTasksCount(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdName,vZoneId)
{
	nlapiLogExecution('DEBUG', 'Into getOpenTasksCount...');

	var str = 'waveno. = ' + waveno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'vPickType. = ' + vPickType + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str = str + 'OrdName. = ' + OrdName + '<br>';
	str = str + 'vZoneId. = ' + vZoneId + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	
	if(containerlpno !=null && containerlpno !='' && containerlpno !='null')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(waveno)));
	}	 
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && containerlpno!=null && containerlpno!="" && containerlpno!= "null")
	{
		nlapiLogExecution('DEBUG', 'Carton # inside If', containerlpno);
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));			
	}

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('DEBUG', 'Out of getOpenTasksCount...',openreccount);

	return openreccount;

}


function updatelastpicktaskforoldcarton(containerlpno,SOOrder)
{
	nlapiLogExecution('DEBUG', 'Into updatelastpicktaskforoldcarton', containerlpno);
	nlapiLogExecution('DEBUG', 'SOOrder', SOOrder);

	var openreccount=0;

	var SOFilters = new Array();
	var SOColumns = new Array();
	var updatetask='T';

	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

	if(SOOrder!=null&&SOOrder!="")
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseInt(SOOrder)));

	if(containerlpno!=null && containerlpno!='' && containerlpno!='null')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	
	else
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	


	SOColumns[0] = new nlobjSearchColumn('custrecord_wms_status_flag');
	SOColumns[1] = new nlobjSearchColumn('custrecord_device_upload_flag');

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
	{
		for (var i = 0; i < SOSearchResults.length; i++){

			var status  = SOSearchResults[i].getValue('custrecord_wms_status_flag');
			var deviceuploadflag  = SOSearchResults[i].getValue('custrecord_device_upload_flag');

			nlapiLogExecution('DEBUG', 'status', status);
			nlapiLogExecution('DEBUG', 'deviceuploadflag', deviceuploadflag);

			if(status=='9')//Pick Generated
			{
				updatetask='F';
			}
		}
		nlapiLogExecution('DEBUG', 'updatetask', updatetask);

		if(updatetask=='T')
		{
			nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[0].getId(),'custrecord_device_upload_flag', 'T');
		}
	}

	nlapiLogExecution('DEBUG', 'Out of updatelastpicktaskforoldcarton', updatetask);
}

function getAllContainers(containersize)
{
	nlapiLogExecution('DEBUG', 'into getAllContainers', '');
	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_containername', null, 'is', containersize));				

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);

	nlapiLogExecution('DEBUG', 'out of getAllContainers', '');

	return containerslist;
}

function AutoPacking(vebizOrdNo){	
	nlapiLogExecution('DEBUG', 'into AutoPacking', vebizOrdNo);
	if(vebizOrdNo!=null && vebizOrdNo!='')
	{
		nlapiLogExecution('DEBUG', 'into AutoPacking vebizOrdNo is not null', vebizOrdNo);
		var vOrdAutoPackFlag="";
		var vStgAutoPackFlag="";
		var vStgCarrierType="";
		var vCarrierType="";
		var vStgCarrierName="";
		var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vebizOrdNo);
		if(vOrderAutoPackFlag!=null&& vOrderAutoPackFlag.length>0){
			vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
			vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
			nlapiLogExecution('DEBUG', 'AutoPackFlag(Order Type)',vOrdAutoPackFlag);
			nlapiLogExecution('DEBUG', 'Carrier(Order Type)',vCarrierType);
		}

		if(vOrdAutoPackFlag!='T' || (vCarrierType=="" ||vCarrierType==null)){
			var vStageAutoPackFlag = getAutoPackFlagforStage();
			if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
				vStgCarrierType = vStageAutoPackFlag[0][0];
				vStgAutoPackFlag = vStageAutoPackFlag[0][1];
				vStgCarrierName = vStageAutoPackFlag[0][2];
				nlapiLogExecution('DEBUG', 'AutoPackFlag(Stage)',vStgAutoPackFlag);
				nlapiLogExecution('DEBUG', 'Carrier(Stage)',vStgCarrierType);
			}
		}

		if(vCarrierType=="" && vStgCarrierType!="")
			vCarrierType=vStgCarrierType;

		nlapiLogExecution('DEBUG', 'Carrier', vCarrierType);


		if(vOrdAutoPackFlag=='T' || vStgAutoPackFlag=='T'){

			UpdatesInOpenTask(vebizOrdNo,vCarrierType,vStgCarrierName);
		}
	}
	nlapiLogExecution('DEBUG', 'out of AutoPacking');
}

function getAutoPackFlagforStage(){
	nlapiLogExecution('DEBUG', 'into getAutoPackFlagforStage');
	var vStgRule = new Array();
	vStgRule = getStageRule('', '', '', '', 'OUB');
	nlapiLogExecution('DEBUG', 'Stage Rule', vStgRule);

	nlapiLogExecution('DEBUG', 'out of getAutoPackFlagforStage');
	return vStgRule;
}

function getAutoPackFlagforOrdType(vebizOrdNo){
	nlapiLogExecution('DEBUG', 'into getAutoPackFlagforOrdType eBizOrdNo',vebizOrdNo);
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
	columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	nlapiLogExecution('DEBUG', 'out of getAutoPackFlagforOrdType', searchresults);
	return searchresults;

}

function getOrderType(vebizOrdNo){
	nlapiLogExecution('DEBUG', 'into getOrderType', vebizOrdNo);
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('tranid', null, 'is', vebizOrdNo));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custbody_salesorder_carrier');
	columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	nlapiLogExecution('DEBUG', 'out of getOrderType', vOrderType);
	return searchresults;
}

function getAutoPackFlagforStageRule(vStgRule){
	nlapiLogExecution('DEBUG', 'into getAutoPackFlagforStageRule', vStgRule);
	var vAutoPackFlag='F';
	var filters = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', vStgRule));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_stgautopackflag');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if(searchresults){
		vAutoPackFlag = searchresults[0].getValue('custrecord_stgautopackflag');		
	}
	nlapiLogExecution('DEBUG', 'out of getAutoPackFlagforStageRule', vAutoPackFlag);
	return vAutoPackFlag;
}

function UpdatesInOpenTask(vebizOrdNo,vCarrierType,CarrieerName){
	nlapiLogExecution('DEBUG', 'into UpdatesInOpenTask function', vebizOrdNo);
	nlapiLogExecution('DEBUG', 'CarrierType', vCarrierType);
	var packstation = getPackStation();
	var containerlpArray=new Array();var ebizorderno;
	var filters = new Array();
	nlapiLogExecution('DEBUG','orderno in getSalesOrderlist',vebizOrdNo);

	if(vebizOrdNo!=""){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vebizOrdNo));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	//filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLp));//Task Type - PICK

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_comp_id');
	columns[5] = new nlobjSearchColumn('custrecord_site_id');
	columns[6] = new nlobjSearchColumn('custrecord_container');
	columns[7] = new nlobjSearchColumn('custrecord_total_weight');
	columns[8] = new nlobjSearchColumn('custrecord_sku');

	columns[0].setSort();
	columns[1].setSort();
	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	if(salesOrderList != null && salesOrderList.length > 0){
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item;	
		for (var i = 0; i < salesOrderList.length; i++){
			orderno = salesOrderList[i].getValue('name');
			containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
			ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
			ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
			compid = salesOrderList[i].getValue('custrecord_comp_id');
			siteid = salesOrderList[i].getValue('custrecord_site_id');
			containersize = salesOrderList[i].getValue('custrecord_container');
			totweight = salesOrderList[i].getValue('custrecord_total_weight');
			item = salesOrderList[i].getValue('custrecord_sku');//modified on dec 12

			internalid = salesOrderList[i].getId();
			containerlpArray[i]=containerno;
			updateStatusInOpenTask(internalid,28);//28 - Status Flag - Outbound Pack Complete
			updateStatusInLPMaster(containerno,28);//28 - Status Flag - Outbound Pack Complete
			CreatePACKTask(compid,siteid,ebizorderno,orderno,containerno,14,packstation,packstation,28,ebizcntrlno,item);//14 - Task Type - PACK

		}
		if(containerlpArray.length>0&&vCarrierType=="PC")
		{
			var oldcontainer="";
			for (var k = 0; k < containerlpArray.length; k++)
			{				
				var containerlpno = containerlpArray[k];
				nlapiLogExecution('DEBUG', 'Old Container', oldcontainer);
				nlapiLogExecution('DEBUG', 'New Container', containerlpno);
				if(oldcontainer!=containerlpno)
				{
					CreateShippingManifestRecord(ebizorderno,containerlpArray[k],CarrieerName);
					oldcontainer = containerlpno;
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'out of UpdatesInOpenTask function', vebizOrdNo);
}

/**
 * @param RcId
 * @param PickQty
 * @param containerInternalId
 * @param getEnteredContainerNo
 * @param itemCube
 * @param itemWeight
 * @param EndLocation
 * @param vReason
 * @param ActPickQty
 * @param IsItLastPick
 * @param ExceptionFlag
 * @param NewRecId
 */
function UpdateRFOpenTask(RcId, ActPickQty, containerInternalId, getEnteredContainerNo, itemCube,itemWeight,EndLocation,vReason,
		expPickQty,IsItLastPick,ExceptionFlag,NewRecId,ItemNo,SalesOrderInternalId,vRemQty,InvoiceRefNo,vbatchno,Systemrules,SerialNo,mastLPRequired,request)
{
	nlapiLogExecution('DEBUG', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	nlapiLogExecution('DEBUG', 'InvoiceRefNo',InvoiceRefNo);
	nlapiLogExecution('DEBUG', 'Open task record ID', RcId);
	nlapiLogExecution('DEBUG', 'EndLocation', EndLocation);
	nlapiLogExecution('Debug', 'SerialNo: ', SerialNo);
	
	var str = 'containerInternalId. = ' + containerInternalId + '<br>';
	str = str + 'EndLocation. = ' + EndLocation + '<br>';	
	str = str + 'itemWeight. = ' + itemWeight + '<br>';	
	str = str + 'itemCube. = ' + itemCube + '<br>';
	str = str + 'getEnteredContainerNo. = ' + getEnteredContainerNo + '<br>';
	str = str + 'mastLPRequired. = ' + mastLPRequired + '<br>';
	str = str + 'IsItLastPick. = ' + IsItLastPick + '<br>';
	str = str + 'ActPickQty. = ' + ActPickQty + '<br>';
	str = str + 'expPickQty. = ' + expPickQty + '<br>';
	str = str + 'mastLPRequired. = ' + mastLPRequired + '<br>';


	nlapiLogExecution('DEBUG', 'Function Parameters', str);
	
	
	try
	{
		
		var vinvrefno=0;
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
		vinvrefno=transaction.getFieldValue('custrecord_invref_no');
		nlapiLogExecution('DEBUG', 'vinvrefno',vinvrefno);

		//To check for sufficient inv available or not
		var InvttranNew = nlapiLoadRecord('customrecord_ebiznet_createinv', vinvrefno);
		nlapiLogExecution('Debug', 'InvttranNew', InvttranNew);		 
		var InvPresentQOH = InvttranNew.getFieldValue('custrecord_ebiz_qoh');
		if(InvPresentQOH == null || InvPresentQOH == '')
			InvPresentQOH=0;
		if(parseFloat(InvPresentQOH) < parseFloat(ActPickQty))
			return 'ERROR,No Sufficient Inv';
		//End here
		transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
		if(ActPickQty!= null && ActPickQty !="")
			transaction.setFieldValue('custrecord_act_qty', ActPickQty);
		if(containerInternalId!= null && containerInternalId !="")
			transaction.setFieldValue('custrecord_container', containerInternalId);
		if(EndLocation!=null && EndLocation!="")
			transaction.setFieldValue('custrecord_actendloc', EndLocation);	
		if(itemWeight!=null && itemWeight!="")
			transaction.setFieldValue('custrecord_total_weight', itemWeight);
		if(itemCube!=null && itemCube!="")
			transaction.setFieldValue('custrecord_totalcube', itemCube);	
		if(getEnteredContainerNo!=null && getEnteredContainerNo!="" && mastLPRequired!='Y')
			transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
		if(getEnteredContainerNo!=null && getEnteredContainerNo!="" && mastLPRequired=='Y')
			transaction.setFieldValue('custrecord_mast_ebizlp_no', getEnteredContainerNo);
		transaction.setFieldValue('custrecord_act_end_date', DateStamp());
		transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
		if(vReason!=null && vReason!="")
			transaction.setFieldValue('custrecord_notes', vReason);	
		if(SerialNo!=null && SerialNo!="")
			transaction.setFieldValue('custrecord_serial_no', SerialNo);
		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();
		transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
		nlapiLogExecution('DEBUG', 'IsItLastPick: ', IsItLastPick);
		transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
		var vemployee = request.getParameter('custpage_employee');
		nlapiLogExecution('Error', 'ItemNo', ItemNo);
		//Case # 20126541�Start
		nlapiLogExecution('Error', 'NewRecId', NewRecId);
		if(NewRecId!=null && NewRecId!='' && NewRecId!='null')
		{
			transaction.setFieldValue('custrecord_invref_no',NewRecId);
		}
		//Case # 20126541� End.
		var fields = ['recordType', 'custitem_ebizbatchlot'];
		var columns = nlapiLookupField('item', ItemNo, fields);
		var Itemtype = columns.recordType;					
		var batchflag = columns.custitem_ebizbatchlot;
		if(Itemtype == "lotnumberedinventoryitem" || Itemtype == "lotnumberedassemblyitem" || batchflag=="T"){
			transaction.setFieldValue('custrecord_batch_no',vbatchno);
		}

		nlapiLogExecution('DEBUG', 'Updating RF Open Task Record Id: ', RcId);
		if(ActPickQty != expPickQty)
		{
			var vkititem = '';
			var vkititemtext = '';
			var filters = new Array(); 			 
			filters[0] = new nlobjSearchFilter('custrecord_sku', null, 'is', ItemNo);	
			filters[1] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

			var columns1 = new Array(); 
			columns1[0] = new nlobjSearchColumn('custrecord_parent_sku_no' ); 

			var searchresultskititem = nlapiSearchRecord( 'customrecord_ebiznet_trn_opentask', null, filters, columns1 ); 
			if(searchresultskititem!=null && searchresultskititem!='')
			{
				vkititem = searchresultskititem[0].getValue('custrecord_parent_sku_no');
				vkititemtext = searchresultskititem[0].getText('custrecord_parent_sku_no');
			}

			var kititemTypesku ='';

			if(vkititem!=null && vkititem!='')
			{
				kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');
			}

			if(kititemTypesku=='kititem')
			{
				nlapiLogExecution('DEBUG', 'vsalesordInternid', SalesOrderInternalId);
				nlapiLogExecution('DEBUG', 'kititem', vkititem);
				var kitfilters = new Array(); 			 
				kitfilters[0] = new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', vkititem);	
				kitfilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);	
				kitfilters[2] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

				var kitcolumns1 = new Array(); 
				kitcolumns1[0] = new nlobjSearchColumn( 'custrecord_expe_qty' ); 			
				kitcolumns1[1] = new nlobjSearchColumn( 'custrecord_act_qty' );

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, kitfilters, kitcolumns1 ); 
				for(var z=0; z<searchresults.length;z++) 
				{										
					var kitrcid=searchresults[z].getId();
					var Expqty=searchresults[z].getValue('custrecord_expe_qty');
					var Actqty=searchresults[z].getValue('custrecord_act_qty');

					nlapiLogExecution('DEBUG', 'Actqty', Actqty);
					nlapiLogExecution('DEBUG', 'Expqty', Expqty);
					if(Actqty==Expqty || Actqty=='')
					{
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', kitrcid, 'custrecord_kit_exception_flag', 'T');
					}
				}
			}
		}

		nlapiSubmitRecord(transaction, false, true);
		
		
		
		
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in OT submit',exp);
		return 'ERROR,No Sufficient Inv';
	}

	

	nlapiLogExecution('DEBUG', 'Time Stamp after updating open task',TimeStampinSec());
	nlapiLogExecution('DEBUG', 'Updated RF Open Task successfully');	
	nlapiLogExecution('DEBUG', 'ExceptionFlag',ExceptionFlag);
	if((SerialNo!=null && SerialNo!='') && SerialNo!='null' )
		UpdateSerialentry(RcId,SerialNo);

	//code added on 15Feb by suman.
	if(ExceptionFlag!='L')
	{
		try
		{
			nlapiLogExecution('DEBUG', 'Time Stamp at the start of deleteAllocations',TimeStampinSec());
			deleteAllocations(vinvrefno,ActPickQty,getEnteredContainerNo,expPickQty,vRemQty,SalesOrderInternalId,ItemNo,EndLocation,Systemrules);
			nlapiLogExecution('DEBUG', 'Time Stamp at the end of deleteAllocations',TimeStampinSec());
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
		}
	}
	else
	{
		nlapiLogExecution('DEBUG', 'vinvrefno',vinvrefno);
		nlapiLogExecution('DEBUG', 'NewRecId',NewRecId);

		try
		{
			if(vinvrefno!=null && vinvrefno!='')
				deleteOldAllocations(vinvrefno,ActPickQty,getEnteredContainerNo,expPickQty,vRemQty,SalesOrderInternalId,ItemNo,EndLocation);
			deleteNewAllocations(NewRecId,ActPickQty,getEnteredContainerNo,expPickQty,vRemQty,SalesOrderInternalId,ItemNo,EndLocation,Systemrules);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in deleteOldNewAllocations',exp);
		}
	}
	//End of code as of 15Feb.
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,ActPickQty,contlpno,expPickQty,remQty,SalesOrderInternalId,itemno,binlocation,Systemrules)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);
	var str = 'Inv Ref No. = ' + invrefno + '<br>';
	str = str + 'CARTON LP. = ' + contlpno + '<br>';	
	str = str + 'ActPickQty. = ' + ActPickQty + '<br>';	
	str = str + 'expPickQty. = ' + expPickQty + '<br>';	
	str = str + 'remQty. = ' + remQty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	str = str + 'itemno. = ' + itemno + '<br>';	
	str = str + 'binlocation. = ' + binlocation + '<br>';	
	nlapiLogExecution('DEBUG', 'Parameter Values', str);

	try
	{
		var invtfilters = new Array(); 			 
		invtfilters[0] = new nlobjSearchFilter('internalidnumber', null, 'equalto', invrefno);

		var invtcolumns1 = new Array(); 
		invtcolumns1[0] = new nlobjSearchColumn( 'custrecord_ebiz_inv_sku' ); 			
		invtcolumns1[1] = new nlobjSearchColumn( 'custrecord_ebiz_inv_binloc' );

		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns1); 

		nlapiLogExecution('Debug', 'invtsearchresults', invtsearchresults);

		if(invtsearchresults!=null && invtsearchresults!='' && invtsearchresults.length>0)
		{
			nlapiLogExecution('Debug', 'invtsearchresults length', invtsearchresults.length);
			var scount=1;
			var invtrecid;
			var newqoh = 0;
			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
					var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
					var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
					var vNewAllocQty=0;
					if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
						vNewAllocQty=parseFloat(Invallocqty)- parseFloat(expPickQty);
						if(parseFloat(vNewAllocQty)<0)
							vNewAllocQty=0;

						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(vNewAllocQty));
					}
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

					nlapiLogExecution('DEBUG', 'vNewAllocQty',vNewAllocQty);

					var remainqty=0;

					if(parseFloat(expPickQty) != parseFloat(ActPickQty))
					{
						nlapiLogExecution('Debug', 'Systemrules.length', Systemrules.length);
						nlapiLogExecution('Debug', 'Systemrules[0]', Systemrules[0]);
						nlapiLogExecution('Debug', 'Systemrules[1]', Systemrules[1]);

						if(Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
						{
							if(Systemrules[1] =="STND")
								newqoh = parseFloat(InvQOH) - parseFloat(expPickQty);
							else if (Systemrules[1] =="NOADJUST")
								newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
						}
						else if(Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] =="ADJUSTBINLOC")
						{

							newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);

						}
						else
						{
							newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
						}
					}
					else
					{
						newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
						Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh)); 
					}

					nlapiLogExecution('Debug', 'newqoh',newqoh);
					Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh));   
					Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
					Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

					invtrecid = nlapiSubmitRecord(Invttran, false, true);
					nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

					if(invtrecid!=null && invtrecid!='' && parseFloat(ActPickQty)>0)
						CreateSTGInvtRecord(invtrecid, contlpno,ActPickQty,SalesOrderInternalId);

					if((parseFloat(newqoh)) <= 0)
					{		
						nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
						var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
					}


				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					} 			
					nlapiLogExecution('DEBUG', 'Exception in RF Inv move : ', wmsE); 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}
		}
		else
		{
			clearItemAllocations(ActPickQty,contlpno,expPickQty,remQty,SalesOrderInternalId,itemno,binlocation);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,SalesOrderInternalId) 
{
	nlapiLogExecution('DEBUG', 'Into CreateSTGInvtRecord');
	var str = 'Inv Ref No. = ' + invtrecid + '<br>';
	str = str + 'CARTON LP. = ' + vContLp + '<br>';	
	str = str + 'Qty. = ' + vqty + '<br>';	
	nlapiLogExecution('DEBUG', 'STG INVT Details', str);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', vqty);
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', vqty);
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');			
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note1', '');	
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note2', '');	
		// Temporarily hard coded by Satish.N
		//stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', 7218);	
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', SalesOrderInternalId);
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of CreateSTGInvtRecord');
}

/**
 * 
 * @param lp
 * @returns
 */
function containerRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	return searchresults;
}

function OpenTaskRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', lp));	 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}

function getLPWeight(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	var lpmastArr=new Array();
	nlapiLogExecution('DEBUG', 'Before:',lp); 
	if(searchresults != null && searchresults!= "" && searchresults.length>0)
	{
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));
		nlapiLogExecution('DEBUG', 'lp:',searchresults[0].getValue('custrecord_ebiz_lpmaster_lp')); 
		nlapiLogExecution('DEBUG', 'lpweight:',searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght')); 
	}
	return lpmastArr;
}
function containerSizeExists(lp,SizeId){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	nlapiLogExecution('DEBUG', 'SizeId:',SizeId); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_sizeid', null, 'anyof', SizeId));	
	nlapiLogExecution('DEBUG', 'Size Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);        	       	
	return searchresults;
}
/**
 * 
 * @param vContLpNo
 * @param vContainerSize
 * @param TotalWeight
 * @param ContainerCube
 */
function CreateRFMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,ItemWeight,getContainerSize,ResultText)
{
	var contLPExists = containerRFLPExists(vContLpNo);
	var TotalWeight=ItemWeight;
	if(contLPExists==null){		

		var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
		MastLP.setFieldValue('name', vContLpNo);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_lptype', 2);	//case# 201416835 in packing UI with carton# filter we have filter of type but we are not updating LP type while creating userdefined LP record.
		if(containerInternalId!="" && containerInternalId!=null)
		{
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
		}	
		nlapiLogExecution('DEBUG', 'TotalWeight',TotalWeight);
		nlapiLogExecution('DEBUG', 'ContainerCube',ContainerCube);
		if(TotalWeight!="" && TotalWeight!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotalWeight);
		if(ContainerCube!="" && ContainerCube!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', ContainerCube);
		if(ResultText != null && ResultText != '')
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
		var currentContext = nlapiGetContext();		        		
		var retktoval = nlapiSubmitRecord(MastLP);
		nlapiLogExecution('DEBUG', 'Master LP Insertion','Success');

	}
	else
	{
		var recordId="";

		if(contLPExists!=null){


			for (var i = 0; i < contLPExists.length; i++){
				recordId = contLPExists[i].getId();
				LPWeight = contLPExists[i].getValue('custrecord_ebiz_lpmaster_totwght');
				nlapiLogExecution('DEBUG', 'recordId',recordId);		

				var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	
				TotWeight=parseFloat(ItemWeight)+parseFloat(LPWeight);
				nlapiLogExecution('DEBUG', 'itemWeight',ItemWeight);
				nlapiLogExecution('DEBUG', 'LPWeight',LPWeight);
				nlapiLogExecution('DEBUG', 'TotWeight',TotWeight);

				if(TotWeight!="" && TotWeight!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotWeight);
				if(ContainerCube!="" && ContainerCube!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', ContainerCube);
				if(ResultText != null && ResultText != '')
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
				var currentContext = nlapiGetContext();	        				
				nlapiSubmitRecord(MastLPUpdate, false, true);        				 
				nlapiLogExecution('DEBUG', 'Master LP Updation','Success');

			}

		}
	}
}
/**
 * 
 * @param WaveNo
 * @param RecordInternalId
 * @param ContainerLPNo
 * @param ExpectedQuantity
 * @param BeginLocation
 * @param Item
 * @param ItemDescription
 * @param ItemInternalId
 * @param DoLineId
 * @param InvoiceRefNo
 * @param BeginLocationName
 * @param EndLocInternalId
 * @param EndLocation
 * @param OrderLineNo
 * @param SerialNo
 * @param vBatchno
 */
function ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, 
		ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo,SerialNo,vBatchno){
	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');

	nlapiLogExecution('DEBUG', "Line Count" + lineCnt);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	nlapiLogExecution('DEBUG', "RecordInternalId", RecordInternalId);
	nlapiLogExecution('DEBUG', "ExpectedQuantity", ExpectedQuantity);
	nlapiLogExecution('DEBUG', "ExpectedQuantity", ExpectedQuantity);
	nlapiLogExecution('DEBUG', "EndLocInternalId", EndLocInternalId);

	//updating allocqty in opentask
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
	OpenTaskTransaction.setFieldValue('custrecord_act_qty', ExpectedQuantity);
	OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
	OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);

	nlapiLogExecution('DEBUG', 'Done PICK Record Insertion :', 'Success');

	//Updating DO 

	var DOLine = nlapiLoadRecord('customrecord_ebiznet_ordline', DoLineId);
	DOLine.setFieldValue('custrecord_linestatus_flag', '1'); //Statusflag='C'
	DOLine.setFieldValue('custrecord_pickqty', parseFloat(ExpectedQuantity));
	var SalesOrderInternalId = DOLine.getFieldValue('name');

	try {
		nlapiLogExecution('DEBUG', 'Main Sales Order Internal Id', SalesOrderInternalId);
		var TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');

		var SOLength = TransformRec.getLineItemCount('item');

		for (var j = 1; j <= SOLength; j++) {
			nlapiLogExecution('DEBUG', "Get Line Value" + TransformRec.getLineItemValue('item', 'line', j ));

			var item_id = TransformRec.getLineItemValue('item', 'item', j);
			var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);

			if (itemLineNo == OrderLineNo) {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
				TransformRec.setCurrentLineItemValue('item', 'quantity', ExpectedQuantity);
				//TransformRec.setCurrentLineItemValue('item', 'location', 1);
				var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
				var serialsspaces="";
				if (itemSubtype.recordType == 'serializedinventoryitem') {

					arrSerial= SerialNo.split(',');
					nlapiLogExecution('DEBUG', "arrSerial", arrSerial);	
					for (var n = 0; n < arrSerial.length; n++) {
						if (n == 0) {
							serialsspaces = arrSerial[n];
						}
						else
						{
							serialsspaces += " " +arrSerial[n];
						}
					}					
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', serialsspaces);
				}
				//vBatchno
				if (itemSubtype.recordType == 'lotnumberedinventoryitem'){
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', vBatchno+'('+ExpectedQuantity+')');
				}


				TransformRec.commitLineItem('item');
			}
			else {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');
				TransformRec.commitLineItem('item');

			}
		}
		var TransformRecId = nlapiSubmitRecord(TransformRec, true);
	} 

	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}

	///Updating qty,qoh in inventory
	nlapiLogExecution('DEBUG', 'Invoice Ref No', InvoiceRefNo);
	var InventoryTranaction = nlapiLoadRecord('customrecord_ebiznet_createinv', InvoiceRefNo);

	var InvAllocQty = InventoryTranaction.getFieldValue('custrecord_ebiz_alloc_qty');
	var InvQOH = InventoryTranaction.getFieldValue('custrecord_ebiz_qoh');

	if (isNaN(InvAllocQty)) {
		InvAllocQty = 0;
	}
	InventoryTranaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(InvQOH) - parseFloat(ExpectedQuantity));
	InventoryTranaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(InvAllocQty) - parseFloat(ExpectedQuantity));
	var InventoryTranactionId = nlapiSubmitRecord(InventoryTranaction, false, true);
	OpenTaskTransaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
	nlapiSubmitRecord(OpenTaskTransaction, true);
	nlapiSubmitRecord(DOLine, true);
}

function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	var pickqtyfinal =0;
	nlapiLogExecution('DEBUG', 'into UpdateRFFulfillOrdLine : ', vdono);

	try
	{

		var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);	

		var oldpickQty=doline.getFieldValue('custrecord_pickqty');
		var ordQty=doline.getFieldValue('custrecord_ord_qty');

		if(isNaN(oldpickQty) || oldpickQty==null || oldpickQty=='' || oldpickQty == -1 )
			oldpickQty=0;

		if(isNaN(ordQty) || ordQty==null || ordQty=='')
			ordQty=0;

		if(isNaN(vActqty) || vActqty==null || vActqty=='')
			vActqty=0;

		pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);

		var str = 'pickqtyfinal. = ' + pickqtyfinal + '<br>';
		str = str + 'oldpickQty. = ' + oldpickQty + '<br>';	
		str = str + 'ordQty. = ' + ordQty + '<br>';	
		str = str + 'vActqty. = ' + vActqty + '<br>';	

		nlapiLogExecution('DEBUG', 'Qty Details', str);

//		if(parseInt(ordQty)>=parseInt(pickqtyfinal))
//		{
		doline.setFieldValue('custrecord_upddate', DateStamp());
		doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal));
		//doline.setFieldValue('custrecord_pickgen_qty', parseInt(pickqtyfinal));

		if(parseFloat(pickqtyfinal)<parseFloat(ordQty))
			doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
		else
			doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

		nlapiSubmitRecord(doline, false, true);
//		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in UpdateRFFulfillOrdLine',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of UpdateRFFulfillOrdLine ');
}


/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteOldAllocations(invrefno,actPickQty,contlpno,expPickQty,vRemQty,SalesOrderInternalId,itemno,binlocation)
{
	nlapiLogExecution('DEBUG', 'Into deleteOldAllocations (Inv Ref NO)',invrefno);

	try
	{
		var scount=1;
		var invtrecid;

		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				nlapiLogExecution('DEBUG', 'Invallocqty',Invallocqty);
				nlapiLogExecution('DEBUG', 'expPickQty',expPickQty);

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {

					if(parseFloat(Invallocqty)- parseFloat(expPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(Invallocqty)- parseFloat(expPickQty));
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('DEBUG', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of deleteOldAllocations (Inv Ref NO)',invrefno);
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteNewAllocations(invrefno,actPickQty,contlpno,expPickQty,vRemQty,SalesOrderInternalId,itemno,binlocation,Systemrules)
{
	nlapiLogExecution('Debug', 'Into deleteNewAllocations (Inv Ref NO)',invrefno);

	var str = 'invrefno. = ' + invrefno + '<br>';
	str = str + 'actPickQty. = ' + actPickQty + '<br>';	
	str = str + 'expPickQty. = ' + expPickQty + '<br>';	
	str = str + 'contlpno. = ' + contlpno + '<br>';	
	str = str + 'vRemQty. = ' + vRemQty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	str = str + 'itemno. = ' + itemno + '<br>';	
	str = str + 'binlocation. = ' + binlocation + '<br>';	
	str = str + 'Systemrules. = ' + Systemrules + '<br>';	

	nlapiLogExecution('Debug', 'Parameter Details', str);

	try
	{
		var invtfilters = new Array(); 			 
		invtfilters[0] = new nlobjSearchFilter('internalidnumber', null, 'equalto', invrefno);

		var invtcolumns1 = new Array(); 
		invtcolumns1[0] = new nlobjSearchColumn( 'custrecord_ebiz_inv_sku' ); 			
		invtcolumns1[1] = new nlobjSearchColumn( 'custrecord_ebiz_inv_binloc' );

		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns1); 

		nlapiLogExecution('Debug', 'invtsearchresults', invtsearchresults);

		if(invtsearchresults!=null && invtsearchresults!='' && invtsearchresults.length>0)
		{
           nlapiLogExecution('Debug', 'invtsearchresults', invtsearchresults.length);
			var scount=1;
			var invtrecid;
			var InvQOH =0;

			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
					var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
					InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

					//Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH) - parseFloat(expPickQty));
					nlapiLogExecution('Debug', 'InvQOH', InvQOH);
					nlapiLogExecution('Debug', 'Invallocqty', Invallocqty);
					if (InvQOH != null && InvQOH != "" && InvQOH>0) {


						//Case # 20126289 Start

						nlapiLogExecution('ERROR', 'Systemrules.length', Systemrules.length);
						nlapiLogExecution('ERROR', 'Systemrules[0]', Systemrules[0]);
						nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);
						var newqoh=0;
						if(Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
						{

							if(Systemrules[1] =="STND")
								newqoh = parseFloat(InvQOH) - parseFloat(expPickQty);
							else if (Systemrules[1] =="NOADJUST")
								newqoh = parseFloat(InvQOH) - parseFloat(actPickQty);
						}
						else if(Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] =="ADJUSTBINLOC")
						{


							newqoh = parseFloat(InvQOH) - parseFloat(actPickQty);

						}
						else
						{
							newqoh = parseFloat(InvQOH) - parseFloat(actPickQty);
						}
                        nlapiLogExecution('Debug', 'newqoh', newqoh);
						Invttran.setFieldValue('custrecord_ebiz_qoh',newqoh);

						/*if(parseFloat(InvQOH)- parseFloat(ActPickQty)>=0)
							Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty));
						else
							Invttran.setFieldValue('custrecord_ebiz_qoh',0);*/

					}
					else
					{
						Invttran.setFieldValue('custrecord_ebiz_qoh',0);
					}

					
					
					Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
					Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

					var invtrecid = nlapiSubmitRecord(Invttran, false, true);
					nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					} 

					nlapiLogExecution('DEBUG', 'Exception in RF Inv move : ', wmsE); 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}


			if(invtrecid!=null && invtrecid!='' && parseFloat(actPickQty)>0)
				CreateSTGInvtRecord(invtrecid, contlpno,actPickQty,SalesOrderInternalId);

			if((parseFloat(InvQOH) - parseFloat(expPickQty)) <= 0)
			{		
				nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
				var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
			}
		}
		else
		{
			clearItemAllocations(actPickQty,contlpno,expPickQty,vRemQty,SalesOrderInternalId,itemno,binlocation);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}	

	nlapiLogExecution('DEBUG', 'Out of deleteNewAllocations (Inv Ref NO)',invrefno);
}



/**
 * @param getEnteredContainerNo
 * @param getFetchedContainerNo
 * @param SOOrder
 * @param OrdName
 */
function UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SOOrder,OrdName,waveno,vZoneId,vPickType,getContainerSize)
{
	nlapiLogExecution('DEBUG','Into UpdateAllContainerLp');

	try
	{

		var str = 'waveno. = ' + waveno + '<br>';
		str = str + 'SOOrder. = ' + SOOrder + '<br>';	
		str = str + 'OrdName. = ' + OrdName + '<br>';
		str = str + 'vZoneId. = ' + vZoneId + '<br>';
		str = str + 'getFetchedContainerNo. = ' + getFetchedContainerNo + '<br>';
		str = str + 'getEnteredContainerNo. = ' + getEnteredContainerNo + '<br>';
		str = str + 'vPickType. = ' + vPickType + '<br>';

		nlapiLogExecution('DEBUG', 'Function Parameters', str);

		var context = nlapiGetContext();
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item,waveno,
		beginloc,endloc,lot,actqty,expqty,uomlevel,empid,begindate,begintime,packcode,lp,lineno,Zoneno,ebizLP,
		taskassignedto;

		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);

		var filteropentask=new Array();
		filteropentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
		filteropentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

		if(getFetchedContainerNo!=null && getFetchedContainerNo!='' && getFetchedContainerNo!='null')
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));	
		else
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	

		filteropentask.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

		if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
		{
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(waveno)));
		}	 
		if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
		{
			filteropentask.push(new nlobjSearchFilter('name', null, 'is', OrdName));
		}
		if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SOOrder!=null && SOOrder!="" && SOOrder!= "null")
		{
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SOOrder));
		}
		if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
		{	
			filteropentask.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_comp_id');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_container');
		columns[7] = new nlobjSearchColumn('custrecord_total_weight');
		columns[8] = new nlobjSearchColumn('custrecord_sku');
		columns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[10] = new nlobjSearchColumn('custrecord_actendloc');
		columns[11] = new nlobjSearchColumn('custrecord_batch_no');
		columns[12] = new nlobjSearchColumn('custrecord_act_qty');
		columns[13] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[14] = new nlobjSearchColumn('custrecord_uom_level');
		columns[15] = new nlobjSearchColumn('custrecord_ebizuser');
		columns[16] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[17] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_lpno');
		columns[20] = new nlobjSearchColumn('custrecord_line_no');
		columns[21] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
		columns[23] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
		columns[25] = new nlobjSearchColumn('custrecord_taskassignedto');

		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filteropentask, columns);
		if(SearchResults!=null&&SearchResults!="")
		{
			for ( var i = 0; i < SearchResults.length; i++) 
			{
				orderno = SearchResults[i].getValue('name');
				containerno = SearchResults[i].getValue('custrecord_container_lp_no');
				ebizorderno = SearchResults[i].getValue('custrecord_ebiz_order_no');
				ebizcntrlno = SearchResults[i].getValue('custrecord_ebiz_cntrl_no');
				compid = SearchResults[i].getValue('custrecord_comp_id');
				siteid = SearchResults[i].getValue('custrecord_wms_location');
				containersize = SearchResults[i].getValue('custrecord_container');
				totweight = SearchResults[i].getValue('custrecord_total_weight');
				item = SearchResults[i].getValue('custrecord_sku');
				beginloc = SearchResults[i].getValue('custrecord_actbeginloc');
				endloc = SearchResults[i].getValue('custrecord_actendloc');
				lot = SearchResults[i].getValue('custrecord_batch_no');
				actqty = SearchResults[i].getValue('custrecord_act_qty');
				expqty = SearchResults[i].getValue('custrecord_expe_qty');
				uomlevel = SearchResults[i].getValue('custrecord_uom_level');
				empid = SearchResults[i].getValue('custrecord_taskassignedto');
				internalid = SearchResults[i].getId();
				begindate=SearchResults[i].getValue('custrecordact_begin_date');
				begintime=SearchResults[i].getValue('custrecord_actualbegintime');
				packcode=SearchResults[i].getValue('custrecord_packcode');
				lp=SearchResults[i].getValue('custrecord_lpno');
				lineno=SearchResults[i].getValue('custrecord_line_no');
				waveno = SearchResults[i].getValue('custrecord_ebiz_wave_no');
				Zoneno = SearchResults[i].getValue('custrecord_ebiz_zoneid');
				ebizLP = SearchResults[i].getValue('custrecord_ebiz_lpno');
				var vZoneno=SearchResults[i].getValue('custrecord_ebizzone_no');
				taskassignedto = SearchResults[i].getValue('custrecord_taskassignedto');

				newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', internalid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', orderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', waveno);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', endloc);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', lot);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', 9);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', siteid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_level', uomlevel);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', actqty);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', context.getUser());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',begindate);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', begintime);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', empid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', ebizcntrlno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', 3);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', expqty);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', packcode);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', lp);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', ebizorderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', lineno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', compid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', getEnteredContainerNo);
				if(getContainerSize!=null && getContainerSize!='')
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', getContainerSize);
				else
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', containersize);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', totweight);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid', Zoneno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizzone_no', vZoneno);

				if(ebizLP==null||ebizLP=="")
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', ebizLP);

				if(taskassignedto!=null&&taskassignedto!="")
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', taskassignedto);

				newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');


			}
			nlapiSubmitRecord(newParent);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in UpdateAllContianerLp',exp);	
	}

	nlapiLogExecution('DEBUG','Out of UpdateAllContianerLp');	
}



function fnValidateEnteredContLP(EnteredContLP,SalesOrderInternalId)
{
	nlapiLogExecution('DEBUG', 'Into fnValidateEnteredContLP',EnteredContLP);	
	nlapiLogExecution('DEBUG', 'SalesOrderInternalId',SalesOrderInternalId);	
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');



	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(salesOrderList == null || salesOrderList == '')
	{

		nlapiLogExecution('Debug', 'Into Closed Task',SalesOrderInternalId);
		var closedTskfilters =new Array();
		//closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_contlp_no', null, 'is', EnteredContLP));
		closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_contlp_no', null, 'is', EnteredContLP));		
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
			closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  

		var ClosedTaskcolumns = new Array();
		ClosedTaskcolumns[0] = new nlobjSearchColumn('name');

		salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, closedTskfilters, ClosedTaskcolumns);

	}

	return salesOrderList;
}

function GenerateLable(uompackflag){

	nlapiLogExecution('DEBUG', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	//added by mahesh


	try 
	{	
		var lpMaxValue=GetMaxLPNo('2', '5','');
		nlapiLogExecution('DEBUG', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('DEBUG', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;

		//to get company id

		duns=GetCompanyDUNSnumber('');

		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('DEBUG', 'uom', uom);
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseInt(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseInt(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	return ResultText;
}

function GetCompanyDUNSnumber(company)
{
	var dunsfilters = new Array();
	var duns='';

	if(company!=null && company!='')
		dunsfilters.push(new nlobjSearchFilter('internalid', null, 'is', company));

	dunsfilters.push(new nlobjSearchFilter('custrecord_compduns', null, 'isnotempty'));
	dunsfilters.push(new nlobjSearchFilter('isinactive', null,'is', 'F'));

	var dunscolumns = new Array();
	dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

	var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
	if (dunssearchresults !=null && dunssearchresults !="") 
	{
		duns = dunssearchresults[0].getValue('custrecord_compduns');
		nlapiLogExecution('DEBUG', 'duns', duns);
	}
	return duns;
}

function updateWtInLPMaster(containerlp){
	nlapiLogExecution('DEBUG', 'In to updateWtInLPMaster...',containerlp);
	var recordId="";

	var filtersSO = new Array();
	filtersSO.push(new nlobjSearchFilter('name', null, 'is', containerlp));
	filtersSO.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_sscc', null, 'isempty'));

	var colsSO = new Array();
	colsSO[0] = new nlobjSearchColumn('name');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersSO, colsSO);

	if(searchresults){
		var ResultText=GenerateLable('1');
		for (var i = 0; i < searchresults.length; i++){

			recordId = searchresults[i].getId();
			nlapiLogExecution('DEBUG', 'recordId',recordId);

			nlapiSubmitField('customrecord_ebiznet_master_lp', recordId,'custrecord_ebiz_lpmaster_sscc', ResultText);

			nlapiLogExecution('DEBUG', 'Updated LP Master with SSCC',ResultText);
		}
	}

	nlapiLogExecution('DEBUG', 'Out of updateWtInLPMaster');
}

function fnValidateContainer(EnteredContainerNo,FetchedContainerNo, type ,lptype,whloc)
{
	var vResult = "";
	var vargetlpno = EnteredContainerNo;

	nlapiLogExecution('DEBUG', 'vargetlpno.length', vargetlpno.length);
	if (vargetlpno.length > 0) {
		var filters = new Array();

		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'anyof', [type]));
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lptype]));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(whloc!=null && whloc!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', [whloc]));

		nlapiLogExecution('DEBUG', 'userdefined type', type);

		var columns=new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_begin'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_end'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters,columns);
		if(searchresults!=null && searchresults.length>0)
		{
			for (var i = 0; i < searchresults.length; i++) {
				try {
					var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');

					var varBeginLPRange = searchresults[i].getValue('custrecord_ebiznet_lprange_begin');

					var varEndRange = searchresults[i].getValue('custrecord_ebiznet_lprange_end');

					var getLPGenerationTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lpgentype');
					var getLPTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lptype');

					if (getLPGenerationTypeValue == type) {
						var getLPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');
						var LPprefixlen = getLPrefix.length;
						var vLPLen = vargetlpno.substring(0, LPprefixlen);

						if (vLPLen == getLPrefix) {
							var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);
							nlapiLogExecution('DEBUG', 'varnum', varnum);

							if (varnum.length > varEndRange.length) {
								vResult = "N";
								nlapiLogExecution('DEBUG', 'length more than range', vResult);
								break;
							}								

							for(var j = 0; j < varnum.length; j++)
							{
								var a = varnum.charAt(j);

								if(a >= 'a' && a <= 'z')
								{
									vResult = "N";
									nlapiLogExecution('DEBUG', 'contains lowercase a-z', vResult);
									break;
								}

								if(a >= 'A' && a <= 'Z')
								{
									vResult = "N";
									nlapiLogExecution('DEBUG', 'contains uppercase A-Z', vResult);
									break;
								}
							}

							if(vResult == 'N')
							{
								break;
							}

							var EnteredContainer = "";
							for(var k = 0; k < varnum.length; k++)
							{
								var c = varnum.charAt(k);
								if(c != "0")
								{
									EnteredContainer = varnum.substring(k, varnum.length);
									break;
								}							   
							}
							nlapiLogExecution('DEBUG', 'EnteredContainer', EnteredContainer);

							if ((parseFloat(varnum, 10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum, 10) > parseFloat(varEndRange))) {
								vResult = "N";
								nlapiLogExecution('DEBUG', 'not in range', vResult);
								break;
							}
							else {
								vResult = "Y";
								nlapiLogExecution('DEBUG', 'in range', vResult);
								break;
							}						

						}
						else {
							nlapiLogExecution('DEBUG', 'else', 'here');
							vResult = "N";
						}
					} //end of if statement
				} 

				catch (err) {
				}
			} //end of for loop
		}
		if (vResult == "Y") {
			nlapiLogExecution('DEBUG', 'final if', 'true');
			return true;
		}
		else {
			nlapiLogExecution('DEBUG', 'final else', 'false');
			return false;
		}
	}
	else {
		return false;
	}
}

function isPickFaceLocation(item,location)
{
	nlapiLogExecution('DEBUG', 'Into isPickFaceLocation');

	var str = 'item. = ' + item + '<br>';
	str = str + 'location. = ' + location + '<br>';	

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var isPickFace='F';
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_autoreplen');
	var PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);
	if(PickFaceSearchResults!=null && PickFaceSearchResults!=''&& PickFaceSearchResults.length>0)
	{
		isPickFace='T';
	}

	nlapiLogExecution('DEBUG', 'Out of isPickFaceLocation - Return Value',isPickFace);

	return isPickFace;
}

function getQOHForAllSKUsinPFLocation(location,item){

	nlapiLogExecution('DEBUG','Into getQOHForAllSKUsinPFLocation');

	var str = 'item. = ' + item + '<br>';
	str = str + 'location. = ' + location + '<br>';	

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('DEBUG','Out of getQOHForAllSKUsinPFLocation - Return Value',pfSKUInvList);

	return pfSKUInvList;
}

function buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,vZoneId,vPickType,getContainerLpNo)
{
	try{
		
	
	
	var SOarray = new Array();
	var SOFilters = new Array();
	var SOColumns = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(WaveNo)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getContainerLpNo!=null && getContainerLpNo!="" && getContainerLpNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'Carton # inside If', getContainerLpNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getContainerLpNo));			
	}
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
	SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
	//Code end as on 290414
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));


	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	SOarray["custparam_ebizordno"] = ebizOrdNo;

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		nlapiLogExecution('DEBUG', 'getRecordInternalId 2', SOarray["custparam_recordinternalid"]);
		//	SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');

		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_picktype"] = vPickType;
		SOarray["custparam_fastpick"] = 'Y';
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}
	}
	catch (e) 
	{
		nlapiLogExecution('Debug', 'Exception in build array: ', e);
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
	} 
	finally 
	{		
		nlapiLogExecution('Debug', 'finally in build array: ', 'finally');
		//context.setSessionObject('session', null);
	}
	return SOarray;
}

function getNextOpentaskInternalId(WaveNo,ClusNo,ebizOrdNo,vZoneId,vSkipId,SOarray,vPickType,OrdName,RecId,request,getContainerLpNo)
{
	nlapiLogExecution('DEBUG','getNextOpentaskInternalId',getNextOpentaskInternalId);
	nlapiLogExecution('DEBUG','WaveNo',WaveNo);
	nlapiLogExecution('DEBUG','ClusNo',ClusNo);
	nlapiLogExecution('DEBUG','ebizOrdNo',ebizOrdNo);
	nlapiLogExecution('DEBUG','vZoneId',vZoneId);
	nlapiLogExecution('DEBUG','vSkipId',vSkipId);
	nlapiLogExecution('DEBUG','vPickType',vPickType);
	nlapiLogExecution('DEBUG','OrdName',OrdName);
	nlapiLogExecution('DEBUG','getContainerLpNo',getContainerLpNo);

	var recid='';
	var SOFilters = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(WaveNo)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getContainerLpNo!=null && getContainerLpNo!="" && getContainerLpNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'Carton # inside If', getContainerLpNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getContainerLpNo));			
	}
	var SOColumns=new Array();

	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
	SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
	//Code end as on 290414
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
	nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);
	if (SOSearchResults != null && SOSearchResults.length > 0) 
	{
		nlapiLogExecution('DEBUG', 'Results.length', SOSearchResults.length);
		vSkipId=0;

		if(SOSearchResults.length <= parseInt(vSkipId))
		{
			vSkipId=0;
		}
		var SOSearchResult = SOSearchResults[parseInt(vSkipId)];							
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		//SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_recordinternalid"] = RecId;
		nlapiLogExecution('DEBUG', 'getRecordInternalId 3', SOarray["custparam_recordinternalid"]);


		//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');

		// Commented by Satish.N on 07/03/2013
		//SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
		// Case# 20148439 starts
		//SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_item"] = SOSearchResult.getText('custrecord_sku');
		// Case# 20148439 ends
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_fastpick"] = 'Y';
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';

		if(SOSearchResults.length > parseInt(vSkipId) + 1)
		{
			var SOSearchnextResult = SOSearchResults[parseInt(vSkipId) + 1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			//SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));		
			nlapiLogExecution('DEBUG', 'Next Item Ex Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));		

		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
		}

		var SOSearchnextResult = SOSearchResults[parseInt(vSkipId) + 1];
		if(SOSearchnextResult!=null && SOSearchnextResult!='')
			recid = SOSearchnextResult.getId();
		//}
	}
	return recid;

}



function UpdateSerialentry(RcId,SerialNo)
{
	try
	{
		nlapiLogExecution('Debug', 'RcId', RcId);
		nlapiLogExecution('Debug', 'SerialNo', SerialNo);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
		var  fromLpno=transaction.getFieldValue('custrecord_from_lp_no');
		var  getOrdName=transaction.getFieldValue('name');
		var  getOrdLineNo=transaction.getFieldValue('custrecord_line_no');
		var item=transaction.getFieldValue('custrecord_sku');
		var getEbizOrd=transaction.getFieldValue('custrecord_ebiz_order_no');
		var getContainerLP=transaction.getFieldValue('custrecord_container_lp_no');


		var getSerialNoarr=SerialNo.split(',');
		if(getSerialNoarr.length>0)
		{
			for(var z=0; z< getSerialNoarr.length ;z++)
			{
				var filtersser = new Array();
				filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNoarr[z]);
				if(item !=null && item!='')
					filtersser[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', item);


				var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
				for ( var i = 0; i < SrchRecord.length; i++) {
					var searchresultserial = SrchRecord[i];
					var vserialid = searchresultserial.getId();
					//nlapiLogExecution('Debug', 'vserialid', vserialid);
				}

				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialwmsstatus';
				fields[1] = 'custrecord_serialebizsono';
				fields[2] = 'custrecord_serialsono';
				fields[3] = 'custrecord_serialsolineno';
				fields[4] = 'custrecord_serialparentid';
				fields[5] = 'custrecord_serialitem';
				values[0] = '8';//STATUS.OUTBOUND.PICK_CONFIRMED
				values[1] = getEbizOrd;
				values[2] = getOrdName;
				values[3] = getOrdLineNo;
				values[4] = getContainerLP;
				values[5] = item;

				var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',vserialid, fields, values);

			}
		}
	}
	catch (e) 
	{
		nlapiLogExecution('Debug', 'Exception in Updating Serial',e);
	}
}



function SystemRulesForAdjustment()
{

	try{

		var ResultArray = new Array();
		var rulevalue='N';

		var filters = new Array();
		//Standard
		filters[0] = new nlobjSearchFilter('name', null, 'is', 'Short picks with adjusting pick qty');
		//filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');
		columns[1].setSort();

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			rulevalue = searchresults[0].getValue('custrecord_ebizrulevalue');

			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != ''
				&& searchresults[0].getValue('custrecord_ebizrulevalue') == 'Y')
			{
				var vmessage="STND";
				ResultArray[0]=searchresults[0].getValue('custrecord_ebizrulevalue');				
				ResultArray[1]= vmessage;
				return ResultArray;
			}
		}

		if(rulevalue!='Y')
		{
			//Boombah
			var filters1 = new Array();
			filters1[0] = new nlobjSearchFilter('name', null, 'is', 'Short picks with no qty adjustment');

			var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters1, columns);
			if(searchresults1 != null && searchresults1 != '')
			{
				rulevalue = searchresults1[0].getValue('custrecord_ebizrulevalue');

				if(searchresults1[0].getValue('custrecord_ebizrulevalue') != null && searchresults1[0].getValue('custrecord_ebizrulevalue') != ''
					&& searchresults1[0].getValue('custrecord_ebizrulevalue') == 'Y')
				{
					var vmessage="NOADJUST";
					ResultArray[0]=searchresults1[0].getValue('custrecord_ebizrulevalue');
					ResultArray[1]= vmessage;
					return ResultArray;
				}
			}
		}
		if(rulevalue!='Y')
		{
			// GSUSA
			var filters2 = new Array();
			filters2[0] = new nlobjSearchFilter('name', null, 'is', 'Short picks with adjusting bin location qty');

			var searchresults2 = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters2, columns);
			if(searchresults2 != null && searchresults2 != '')
			{
				rulevalue = searchresults2[0].getValue('custrecord_ebizrulevalue');

				if(searchresults2[0].getValue('custrecord_ebizrulevalue') != null && searchresults2[0].getValue('custrecord_ebizrulevalue') != ''
					&& searchresults2[0].getValue('custrecord_ebizrulevalue') == 'Y')
				{
					var vmessage="ADJUSTBINLOC";
					ResultArray[0]=searchresults2[0].getValue('custrecord_ebizrulevalue');
					ResultArray[1]= vmessage;
					return ResultArray;
				}
			}
		}
		if(rulevalue!='Y')
		{
			var vmessage="STND";
			ResultArray[0]='Y';				
			ResultArray[1]= vmessage;
			return ResultArray;
		}
	}
	catch (exp) 
	{
		nlapiLogExecution('ERROR', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		var vmessage="STND";
		ResultArray[0]='Y';				
		ResultArray[1]= vmessage;
		return ResultArray;
	}	
}

function getOpenTasksCountForNavigation(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdName,vZoneId,vSite,pickMethodId)
{
	nlapiLogExecution('Debug', 'Into getOpenTasksCountNav...');

	var str = 'waveno. = ' + waveno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'vPickType. = ' + vPickType + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str = str + 'OrdName. = ' + OrdName + '<br>';
	str = str + 'vZoneId. = ' + vZoneId + '<br>';
	str = str + 'vSite. = ' + vSite + '<br>';
	str = str + 'pickMethodId. = ' + pickMethodId + '<br>';
	nlapiLogExecution('Debug', 'Function Parameters', str);



	/*var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', vSite));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[1] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	var pickMethodId ;
	var Stagedet;
	if(pickRuleSearchResult != null){
		for (var i=0; i<pickRuleSearchResult.length; i++){			
			pickMethodId = pickRuleSearchResult[0].getValue('custrecord_ebizpickmethod');

			Stagedet = pickRuleSearchResult[i].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
		}
	}
	nlapiLogExecution('ERROR', 'pickMethodId', pickMethodId);*/
	var pickmethodSearchResult=null;
	if(pickMethodId!=null && pickMethodId!='')
	{

		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', pickMethodId));


		var columns = new Array();

		columns.push(new nlobjSearchColumn('custrecord_ebiz_stagedetermination'));

		var pickmethodSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filters, columns);
	}

	var pickMethodId ;
	var Stagedet;
	if(pickmethodSearchResult != null){
		for (var i=0; i<pickmethodSearchResult.length; i++){			
			Stagedet = pickmethodSearchResult[i].getValue('custrecord_ebiz_stagedetermination');
		}
	}
	nlapiLogExecution('ERROR', 'pickMethodId', pickMethodId);

	//The below changes done by Satish.N
	//If stage determination is not defined,take carton level as default.

	if(Stagedet==null || Stagedet=='')
		Stagedet=3;

	//Upto here.


	/*var fields=new Array();	
	fields[0]='custrecord_ebiz_stagedetermination';
	var pickmethodcolumns = nlapiLookupField('customrecord_ebiznet_pick_method', pickMethodId, fields);	
	if(pickmethodcolumns!=null)

	var Stagedet = pickmethodcolumns.custrecord_ebiz_stagedetermination;*/

	nlapiLogExecution('ERROR', 'Stagedet', Stagedet);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

	if(Stagedet == 3)//Carton Level
	{
		nlapiLogExecution('ERROR', 'containerlpno', containerlpno);
		if(containerlpno !=null && containerlpno !='' && containerlpno !='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));
	}
	else if(Stagedet == 2)//Wave Level
	{
		nlapiLogExecution('ERROR', 'waveno', waveno);
		if(waveno !=null && waveno !='' && waveno !='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}
	else if(Stagedet == 1)//Order Level
	{
		nlapiLogExecution('ERROR', 'OrdName', OrdName);
		if(OrdName !=null && OrdName !='' && OrdName !='null')
			SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	/*if(OrdName != null && OrdName != '')
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));*/

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		nlapiLogExecution('ERROR', 'waveno', waveno);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}	 
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('Debug', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('Debug', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('Debug', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	// case# 201417285
	if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && containerlpno!=null && containerlpno!="" && containerlpno!= "null") 
	{
		nlapiLogExecution('ERROR', 'Carton # inside If', containerlpno);
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));			
	}
	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('Debug', 'Out of getOpenTasksCountForNav...',openreccount);

	return openreccount;

}

function AutoStageRecordcreaton(SOarray)
{
	try
	{
		nlapiLogExecution('ERROR', 'AutoStageRecordcreaton',SOarray);
		var stgDirection="OUB";
		var fastpick = SOarray['custparam_fastpick'];
		nlapiLogExecution('ERROR', 'fastpick',fastpick);
		var getWaveno = SOarray['custparam_waveno'];
		var getRecordInternalId = SOarray['custparam_recordinternalid'];
		var getContainerLpNo = SOarray['custparam_newcontainerlp'];
		var getExpectedQuantity = SOarray['custparam_expectedquantity'];
		var getBeginLocationId = SOarray['custparam_beginLocation'];
		var getItemName = SOarray['custparam_itemname'];
		var getItemDescription = SOarray['custparam_itemdescription'];
		var getItemInternalId = SOarray['custparam_iteminternalid'];
		var getDOLineId = SOarray['custparam_dolineid'];
		var getInvoiceRefNo = SOarray['custparam_invoicerefno'];
		var getEndLocInternalId = SOarray['custparam_endlocinternalid'];
		var getEnteredLocation = SOarray['custparam_endlocation'];
		var getBeginBinLocation = SOarray['custparam_beginlocationname'];
		var getOrderLineNo = SOarray['custparam_orderlineno'];		
		var getSerialNo = SOarray['custparam_serialno'];
		var vClusterNo = SOarray['custparam_clusterno'];		
		var name = SOarray['name'];
		var pickType=SOarray['custparam_picktype'];	
		var vSOId=SOarray['custparam_ebizordno'];
		var RecordCount=SOarray['custparam_noofrecords'];
		var NextItemInternalId=SOarray['custparam_nextiteminternalid'];
		var ContainerSize=SOarray['custparam_containersize'];
		var getOrderNo=SOarray['custparam_ebizordno'];
		var whLocation = SOarray['custparam_whlocation'];
		var getZoneNo=SOarray['custparam_ebizzoneno'];
		var vBatchno = SOarray['custparam_batchno'];
		var getItem = '';
		var vondemandstage = SOarray['custparam_ondemandstage'];
		var vSkipId=0;
		if(SOarray['custparam_skipid'] !=null &&  SOarray['custparam_skipid'] !="")
			vSkipId=SOarray['custparam_skipid'];
		//Code Added for Displaying the Location Name
//		*****************************************
		var SOarray = new Array();
		var WaveNo = getWaveno;
		var RecordInternalId = getRecordInternalId;
		var ContainerLPNo = getContainerLpNo;			
		var ExpectedQuantity = getExpectedQuantity;
		var BeginLocation = getBeginLocationId;
		var Item = getItemName;
		var ItemName = getItemName;
		var ItemDescription = getItemDescription ;
		var ItemInternalId = getItemInternalId ;
		var DoLineId = getDOLineId ; // internal id of DO
		var InvoiceRefNo = getInvoiceRefNo ;
		var BeginLocationName = getBeginBinLocation ;
		var EndLocInternalId = getEndLocInternalId ;
		var EndLocation = getEnteredLocation;
		var OrderLineNo = getOrderLineNo ;
		var SerialNo = getSerialNo ;
		var vPickType = pickType;
		SOarray["custparam_ebizordno"] = vSOId;
		SOarray["custparam_picktype"] = vPickType;
		var vSite;
		var vCompany;
		var stgDirection="OUB";
		var vDoname=name;
		var vdono=getDOLineId;
		var vPrevinvrefno=getInvoiceRefNo ;
		//***********************************

		var Carrier;
		var Site;
		var Company;
		var stgOutDirection="OUB";
		var carriertype='';
		var wmscarrier='';
		var customizetype='';
		var ordertype='';
		var customer='';


		nlapiLogExecution('DEBUG', 'getDOLineId',getDOLineId); 
		nlapiLogExecution('DEBUG', 'WaveNo ', getWaveno);


		if(getDOLineId!=null && getDOLineId!='')
		{
			var columns = new Array();
			var filters = new Array();

			filters.push(new nlobjSearchFilter('internalid', null, 'is',getDOLineId));

			columns[0] = new nlobjSearchColumn('custrecord_do_wmscarrier');	
			columns[1] = new nlobjSearchColumn('custrecord_do_carrier');				
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_wave');
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_ordline_customizetype');
			columns[4] = new nlobjSearchColumn('custrecord_do_order_type');
			columns[5] = new nlobjSearchColumn('custrecord_do_customer');

			var searchresultsord = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
			if(searchresultsord!=null && searchresultsord!='' && searchresultsord.length>0)
			{
				Carrier = searchresultsord[0].getValue('custrecord_do_carrier');
				customizetype = searchresultsord[0].getValue('custrecord_ebiz_ordline_customizetype');
				ordertype = searchresultsord[0].getValue('custrecord_do_order_type');
				customer = searchresultsord[0].getValue('custrecord_do_customer');
				nlapiLogExecution('DEBUG', 'Carrier',Carrier); 
				nlapiLogExecution('DEBUG', 'customizetype',customizetype); 
				nlapiLogExecution('DEBUG', 'ordertype',ordertype); 
				if(Carrier!=null && Carrier!='')
				{
					var filterscarr = new Array();
					var columnscarr = new Array();

					filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',Carrier));
					filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

					columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
					columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
					columnscarr[2] = new nlobjSearchColumn('id');

					var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
					if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
					{
						carriertype = searchresultscarr[0].getValue('custrecord_carrier_type');
						wmscarrier = searchresultscarr[0].getId();
					}
				}

				if(Carrier==null || Carrier=='')
					wmscarrier=searchresultsord[0].getValue('custrecord_do_wmscarrier');
				if(getWaveno==null || getWaveno=='')
					getWaveno=searchresultsord[0].getValue('custrecord_ebiz_wave');
			}
		}

		nlapiLogExecution('DEBUG', 'carriertype',carriertype); 
		nlapiLogExecution('DEBUG', 'wmscarrier',wmscarrier); 
		nlapiLogExecution('DEBUG', 'WaveNo ', getWaveno);

		var stgLocation = '';
		var FetchBinLocation='';

		var stgLocationresults = GetStageLocationFromSTGM(name,getWaveno);
		if(stgLocationresults!=null && stgLocationresults!='' && stgLocationresults.length>0)
		{
			stgLocation = stgLocationresults[0].getValue('custrecord_actbeginloc');
			FetchBinLocation = stgLocationresults[0].getText('custrecord_actbeginloc');
		}

		nlapiLogExecution('DEBUG', 'Stage Location from STGM ', FetchBinLocation);
		nlapiLogExecution('DEBUG', 'Stage Location Id from STGM ', stgLocation);

		if(stgLocation==null || stgLocation=='')
		{

			stgLocation = GetPickStageLocation(getItemInternalId, wmscarrier, whLocation, Company,
					stgOutDirection,carriertype,customizetype,null,ordertype,customer);


			nlapiLogExecution('DEBUG', 'After Getting StagingLocation in page load::',stgLocation);
			var FetchBinLocation;
			if(stgLocation!=null && stgLocation!=-1)
			{
				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('name');
				filtersLocGroup.push(new nlobjSearchFilter('internalid', null, 'is',stgLocation));
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				if(searchresultsloc != null && searchresultsloc != '')
					FetchBinLocation=searchresultsloc[0].getValue('name');
			}

			nlapiLogExecution('DEBUG', 'After Getting StagingLocation Name in page load::',FetchBinLocation);
		}
		var vCarrier = Carrier;
		var vstageLoc=stgLocation;
		var voldcontainer='';
		var vZoneId=getZoneNo;
		var vhdncarrier=wmscarrier;
		var vhdncarriertype=carriertype;
		var vhdncustomizetype=customizetype;
		var WHLocation = whLocation;
		var Stagedet=3;
		nlapiLogExecution('ERROR', 'Stagedet', Stagedet);

		var taskfilters = new Array();

		taskfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));//Task Type - PICK
		taskfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','28']));// Pick Confirm and Pack Complete
		if(WaveNo!=null && WaveNo!='')
			taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));	
		if (vPickType!='CL') {
			if(Stagedet==3)//Carrier level
			{
				if(ContainerLPNo!=null && ContainerLPNo!='' && ContainerLPNo!='null')
					taskfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ContainerLPNo));
			}
		}
		if (vPickType=='CL') {
			taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));	
		}

		var taskcolumns = new Array();
		taskcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
		taskcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		taskcolumns[2] = new nlobjSearchColumn('custrecord_invref_no');
		taskcolumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		taskcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		taskcolumns[5] = new nlobjSearchColumn('name');
		taskcolumns[0].setSort();

		var tasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskfilters, taskcolumns);

		if(tasksearchresults!=null)
		{
			for (var i = 0; i < tasksearchresults.length; i++)
			{
				ContainerLPNo = tasksearchresults[i].getValue('custrecord_container_lp_no');
				Item = tasksearchresults[i].getValue('custrecord_ebiz_sku_no');
				var salesorderintrid = tasksearchresults[i].getValue('custrecord_ebiz_order_no');
				vDoname = tasksearchresults[i].getValue('name');

				nlapiLogExecution('DEBUG', 'voldcontainer', voldcontainer);
				nlapiLogExecution('DEBUG', 'ContainerLPNo', ContainerLPNo);
				nlapiLogExecution('DEBUG', 'salesorderintrid', salesorderintrid);
				nlapiLogExecution('DEBUG', 'vondemandstage', vondemandstage);

				if(voldcontainer!=ContainerLPNo)
				{
					CreateSTGMRecord(ContainerLPNo, WaveNo, RecordInternalId, vDoname, vCompany, WHLocation, vdono, 
							Item, stgDirection, vCarrier, vSite,stgLocation,vClusterNo,salesorderintrid,vondemandstage);

					voldcontainer=ContainerLPNo;
				}
			}
		}	
		if(vondemandstage == 'Y')
		{
			var SOarray = new Array();
			SOarray["custparam_fastpick"] = fastpick;
			response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
		}
		else
		{
			var vFootPrintFlag='N'; // Put Y to navigate to Dynacraft specific screen for scanning 'No. of foot prints/Pallets'
			if(vFootPrintFlag != 'Y')
			{
				if(vPickType != null && vPickType != '' && vPickType!='CL')
				{

					nlapiLogExecution('DEBUG', 'Inside Both Wave No condition', parseFloat(WaveNo));
					nlapiLogExecution('DEBUG', 'Inside Both Order No condition', DoLineId);

					var SOFilters = new Array();


					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated					
					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

					if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
					{
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
					} 
					if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && vDoname!=null && vDoname!="" && vDoname!= "null")
					{
						nlapiLogExecution('DEBUG', 'OrdNo inside If', vDoname);
						SOFilters.push(new nlobjSearchFilter('name', null, 'is', vDoname));
					}
					if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
					{
						nlapiLogExecution('DEBUG', 'SO Id inside If', vSOId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
					}
					if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
					{
						nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
					}

					if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && ContainerLPNo!=null && ContainerLPNo!="" && ContainerLPNo!= "null")
					{
						nlapiLogExecution('DEBUG', 'Carton # inside If', ContainerLPNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ContainerLPNo));			
					}


					var SOColumns = new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0 ) {
						nlapiLogExecution('DEBUG', 'into Search Results by Wave No', WaveNo);
						nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);
						vSkipId=0;
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}
						//nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);

						var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
						getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

						nlapiLogExecution('DEBUG', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
						nlapiLogExecution('DEBUG', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
						nlapiLogExecution('DEBUG', 'Wave # of given Wave', getWaveNo);

						SOarray["custparam_waveno"] = getWaveNo;
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["name"] = SOSearchResult.getValue('name');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');	
						SOarray["custparam_clusterno"] = vClusterNo;
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_ebizordno"] = SOSearchResult.getValue('custrecord_ebiz_order_no');
						SOarray["custparam_fastpick"] = fastpick;
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						if(vZoneId!=null && vZoneId!="")
						{
							SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
						}
						else
							SOarray["custparam_ebizzoneno"] = '';
						if(fastpick!='Y')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
						}							
						return;

					}
					else {
						nlapiLogExecution('DEBUG', 'Search Results by Wave No is NULL', WaveNo);
						var SOarray = new Array();
						SOarray["custparam_fastpick"] = fastpick;
						response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
					}


				}	
				else if (vPickType=='CL') {

					nlapiLogExecution('DEBUG', 'Inside Cluster No condition', vClusterNo);

					var SOFilters = new Array();

					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));

					var SOColumns = new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_clus_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);
					SOColumns[19].setSort();	//	Container LP
					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

					if (SOSearchResults != null && SOSearchResults.length > 0) {
						vSkipId=0;
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}

						var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];

						SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no');
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');						
						SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no');
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
						SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
						SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
						SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
						SOarray["custparam_picktype"] =  'CL';
						SOarray["name"] =  SOSearchResult.getValue('name');

						nlapiLogExecution('DEBUG', 'Redirecting to Location Screen');
						response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);					

					}
					else {
						response.sendRedirect('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di', false, null);
					}
				}
				else if (WaveNo != null && WaveNo != "" && vPickType!='CL') {
					nlapiLogExecution('DEBUG', 'Inside Wave No condition', parseFloat(WaveNo));

					var SOFilters = new Array();

					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

					var SOColumns = new Array();

					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
					SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');
					SOColumns[20] = new nlobjSearchColumn('custrecord_container_lp_no');


					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0) {
						nlapiLogExecution('DEBUG', 'into Search Results by Wave No', WaveNo);
						vSkipId=0;
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}
						//nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
						var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
						getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

						nlapiLogExecution('DEBUG', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
						nlapiLogExecution('DEBUG', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
						nlapiLogExecution('DEBUG', 'Wave # of given Wave', getWaveNo);

						SOarray["custparam_waveno"] = getWaveNo;
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						//	SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["name"] = SOSearchResult.getValue('name');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');	
						SOarray["custparam_clusterno"] = vClusterNo;
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_ebizordno"] = SOSearchResult.getValue('custrecord_ebiz_order_no');
						SOarray["custparam_fastpick"] = fastpick;

						if(fastpick!='Y')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);

						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
						}
						return;

					}
					else {
						nlapiLogExecution('DEBUG', 'Search Results by Wave No is NULL', WaveNo);
						var SOarray = new Array();
						SOarray["custparam_fastpick"] = fastpick;
						response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
					}
				}
				else
				{
					var SOarray = new Array();
					SOarray["custparam_fastpick"] = fastpick;
					response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
				}

			}
			else
			{
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Foot print');
				response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, SOarray);
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","AutoStageRecordcreaton",exp);
	}
}

function GetStageLocationFromSTGM(fono,waveno)
{
	nlapiLogExecution('DEBUG', 'Into GetStageLocationFromSTGM');
	nlapiLogExecution('DEBUG', 'fono', fono);
	nlapiLogExecution('DEBUG', 'waveno', waveno);

	var SOSearchResults = new Array();

	var SOFilters = new Array();

	if(fono!=null && fono!='')
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', fono));
	if(waveno!=null && waveno!='')
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveno));
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [13])); // Task Type - STGM

	var SOColumns = new Array();

	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('internalid'));	
	SOColumns[1].setSort(true);

	SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	nlapiLogExecution('DEBUG', 'Out of GetStageLocationFromSTGM',SOSearchResults);

	return SOSearchResults;	
}


function GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,customizetype,StageBinInternalId,
		ordertype,vcustomer){

	nlapiLogExecution('DEBUG', 'into GetPickStageLocation', Item);
	nlapiLogExecution('DEBUG', 'Item', Item);
	nlapiLogExecution('DEBUG', 'vSite', vSite);
	nlapiLogExecution('DEBUG', 'vCompany', vCompany);
	nlapiLogExecution('DEBUG', 'stgDirection', stgDirection);
	nlapiLogExecution('DEBUG', 'vCarrier', vCarrier);
	nlapiLogExecution('DEBUG', 'vCarrierType', vCarrierType);
	nlapiLogExecution('DEBUG', 'ordertype', ordertype);
	nlapiLogExecution('DEBUG', 'customizetype', customizetype);
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();

	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 = "";
	var ItemInfo3 = "";
	var columns = nlapiLookupField('item', Item, fields);
	if(columns!=null){
		ItemFamily = columns.custitem_item_family;
		ItemGroup = columns.custitem_item_group;
		ItemStatus = columns.custitem_ebizdefskustatus;
		ItemInfo1 = columns.custitem_item_info_1;
		ItemInfo2 = columns.custitem_item_info_2;
		ItemInfo3 = columns.custitem_item_info_3;
	}

	nlapiLogExecution('DEBUG', 'ItemFamily', ItemFamily);
	nlapiLogExecution('DEBUG', 'ItemGroup', ItemGroup);
	nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);
	nlapiLogExecution('DEBUG', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('DEBUG', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('DEBUG', 'ItemInfo3', ItemInfo3);

	nlapiLogExecution('DEBUG', 'Before Variable declaration');

	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}

	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
	if (vCarrier != null && vCarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

	} // code added from Boombah account on 25Feb13 by santosh
	else
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@']));
	// upto here
	if (vCarrierType != null && vCarrierType != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));
	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));
	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));
	}
	if (ordertype != null && ordertype != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@', ordertype]));
	}
	else
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@']));
	if (customizetype != null && customizetype != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@', customizetype]));

	} // code added from Boombah account on 25Feb13 by santosh
	else
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'noneof', ['14']));

	if (vcustomer != null && vcustomer != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_customer', null, 'anyof', ['@NONE@', vcustomer]));
	}
	nlapiLogExecution('DEBUG', 'Before Searching of Stageing Rules');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	nlapiLogExecution('DEBUG', 'searchresults',searchresults);
	if (searchresults != null) {
		for (var i = 0; i < Math.min(10, searchresults.length); i++) {
			//GetPickStagLpCount                
			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			var vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');

			nlapiLogExecution('DEBUG', 'Stageing Location Value::', vstgLocation);
			nlapiLogExecution('DEBUG', 'Stageing Location Text::', vstgLocationText);
			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');
			nlapiLogExecution('DEBUG', 'vnoofFootPrints::', vnoofFootPrints);

			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetPickStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}
			nlapiLogExecution('DEBUG', 'final stagelocation::', vstgLocation);

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('DEBUG', 'final stagelocation::', vstgLocation);
			nlapiLogExecution('DEBUG', 'LocGroup::', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');
				if (stgDirection == 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('DEBUG', 'searchresultsloc::', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();
					nlapiLogExecution('DEBUG', 'vLocid::', vLocid);

					var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');

					//--
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}
			}
		}
	}
	return -1;
}

/**
 * 
 * @param location
 * @param site
 * @param company
 * @returns {Number}
 */
function GetPickStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');

	var outLPCount = 0;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));

	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));

	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');

	return outLPCount;
}

function CreateSTGMRecord(vContLp, vebizWaveNo, vebizOrdNo, vDoname, vCompany,vlocation,vdono,
		Item,stgDirection,vCarrier,vSite,stageLocation,vClusterNo,salesorderintrid,vondemandstage) {

	nlapiLogExecution('DEBUG', 'into CreateSTGMRecord');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,
			'is', '9')); //STATUS.OUTBOUND.PICK_GENERATED('G') 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',
			vebizWaveNo));
	if(vContLp!=null && vContLp!='' && vContLp!='null')
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,
				'is', vContLp));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'isempty'));
	}

	if(vClusterNo!=null && vClusterNo!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null,
				'is', vClusterNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',
			null, filters, columns);
	if (searchresults == null || vondemandstage=='Y') {
		if(stageLocation==null || stageLocation=='')
			stageLocation = GetPickStageLocation(Item, vCarrier, vSite, vCompany,
					stgDirection,null,null,null,null,null);
		var STGMtask = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'stageLocation',stageLocation);
		nlapiLogExecution('DEBUG', 'Name',vDoname);
		nlapiLogExecution('DEBUG', 'DO NO',vdono);
		if(vDoname==null || vDoname=='')
			vDoname = vdono;
		STGMtask.setFieldValue('name', vDoname);
		STGMtask.setFieldValue('custrecord_ebiz_concept_po', vebizOrdNo);
		STGMtask.setFieldValue('custrecord_tasktype', '13');//STATUS.OUTBOUND.ORDERLINE_PARTIALLY_SHIPPED('PS')
		STGMtask.setFieldValue('custrecord_wms_location', vlocation);
		STGMtask.setFieldValue('custrecord_actbeginloc', stageLocation);
		STGMtask.setFieldValue('custrecord_actendloc', stageLocation);
		STGMtask.setFieldValue('custrecord_container_lp_no', vContLp);
		STGMtask.setFieldValue('custrecord_ebiz_cntrl_no', vdono);
		STGMtask.setFieldValue('custrecord_ebiz_receipt_no', vdono);
		STGMtask.setFieldValue('custrecord_ebiz_wave_no', vebizWaveNo);
		STGMtask.setFieldValue('custrecordact_begin_date', DateStamp());
		STGMtask.setFieldValue('custrecordact_end_date', DateStamp());
		STGMtask.setFieldValue('custrecord_actualendtime', TimeStamp());
		STGMtask.setFieldValue('custrecord_actualbegintime', TimeStamp());
		STGMtask.setFieldValue('custrecord_sku', Item);
		STGMtask.setFieldValue('custrecord_ebiz_sku_no', Item);
		STGMtask.setFieldValue('custrecord_ebiz_order_no', salesorderintrid);
		//STGMtask.setFieldValue('custrecord_comp_id', vCompany);
		var currentContext = nlapiGetContext();
		var currentUserID = currentContext.getUser();
		STGMtask.setFieldValue('custrecord_ebizuser', currentUserID);
		var retSubmitLP= nlapiSubmitRecord(STGMtask);
		nlapiLogExecution('DEBUG', 'out of CreateSTGMRecord');

		nlapiLogExecution('DEBUG', 'updating outbound inventory with satge location',stageLocation);
		updateStageInventorywithStageLoc(vContLp,stageLocation);
	}
}
//Case# 201413032....
function containerSizeCheck(ContainerSize,vsize,whsite)
{

	var mesg="";
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'vsize::', vsize);

	filters.push(new nlobjSearchFilter('name', null, 'is', vsize));
	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', ['@NONE@',whsite]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');

	containersizelist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);
	nlapiLogExecution('DEBUG', 'list is::', containersizelist);

	if(containersizelist== null || containersizelist== "" || containersizelist.length==0)
	{	
		mesg="invalid carton size";
	}

	return mesg;
}
//Case# 201413032....
function updateStageInventorywithStageLoc(contlpno,stageLocation)
{
	nlapiLogExecution('DEBUG', 'into updateStageInventorywithStageLoc');
	nlapiLogExecution('DEBUG', 'contlpno',contlpno);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null,'is', '18')); //FLAG.INVENTORY.OUTBOUND('O') 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is', contlpno));
	filters.push(new nlobjSearchFilter('custrecord_invttasktype', null,'is', '3')); ////Task Type - PICK

	var columns = new Array();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null, filters, columns);
	if (invtsearchresults != null && invtsearchresults != '' && invtsearchresults.length > 0) 
	{
		for (var i = 0; i < invtsearchresults.length; i++)
		{
			var invtid=invtsearchresults[i].getId();

			nlapiLogExecution('DEBUG', 'Deleting Outbound Inventory....',invtid);
			nlapiSubmitField('customrecord_ebiznet_createinv', invtid, 'custrecord_ebiz_inv_binloc', stageLocation);
		}		
	}

	nlapiLogExecution('DEBUG', 'out of updateStageInventorywithStageLoc');
}