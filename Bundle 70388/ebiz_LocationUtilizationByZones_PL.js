/**
 * This function is to get the zone list from the Warehouse Zones for the Inbound Zone Type
 * @returns {String}
 */
function getPutZone(){
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_zonetype', null, 'anyof', ['1']);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('internalid');
	var zoneList = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, filters, columns);
	nlapiLogExecution('ERROR', 'zoneList', zoneList.length);
	return zoneList;
}

function putZoneList(zoneList){
	var BinLocationGroupZoneList = "";

	if(zoneList != null && zoneList.length > 0){
		for(var i = 0; i < zoneList.length; i++){
			if(i == 0)
				BinLocationGroupZoneList += zoneList[i];
			else{
				BinLocationGroupZoneList += ",";
				BinLocationGroupZoneList += zoneList[i];
			}
		}
	}
	return BinLocationGroupZoneList;
}
function getbinLocationGroupList(locationGroupList){
	var binLocationGroupList ="";
	if(locationGroupList != null && locationGroupList.length > 0){
		for(var i = 0; i < locationGroupList.length; i++){
			if(i == 0)
				binLocationGroupList += locationGroupList[i].getId();
			else{
				binLocationGroupList += ",";
				binLocationGroupList += locationGroupList[i].getId();
			}
		}
	}
	return binLocationGroupList ;
}
/**
 * This function will get all the bin location groups for the zone list passed.
 * @param zoneList
 * @returns {String}
 */
function getBinLocationGroup(zoneList){
	var binLocationGroupList = "";
	var putZonesList=putZoneList(zoneList);
	var putlist=  putZonesList.split(',');
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', putlist);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
	columns[1] = new nlobjSearchColumn('custrecord_locgroup_no');
	var locationGroupList = nlapiSearchRecord('customrecord_zone_locgroup', null, filter, columns);
	return locationGroupList;
}

function getBinLocations(binLocationGroupList){

	var binLocationSearch = new Array();
	var locnGroupList = new Array();
	var binLocationsCount=0;
	locnGroupList=binLocationGroupList.split(',');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', locnGroupList);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
	columns[2] = new nlobjSearchColumn('internalid');
	columns[2].setSort();
	binLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
	if(binLocationSearch != null)
	{
		binLocationsCount=binLocationsCount+parseInt(binLocationSearch.length);
		if(binLocationSearch.length>=1000)
		{

			var maxno=binLocationSearch[binLocationSearch.length-1].getValue(columns[2]);
			var count=	getbinLoationsearchresults(maxno,locnGroupList);
			binLocationsCount=binLocationsCount+parseInt(count);
		}   

	}

	return binLocationsCount;
}
var binLocationsCount1=0;
function getbinLoationsearchresults(maxno,locnGroupList)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', locnGroupList);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filters[2] = new nlobjSearchFilter('id', null, 'greaterthan', maxno);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
	columns[2] = new nlobjSearchColumn('internalid'); 
	columns[2].setSort();
	var  binLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
	if(binLocationSearch.length>=1000)
	{          
		var maxno=binLocationSearch[binLocationSearch.length-1].getValue(columns[2]);
		binLocationsCount1=binLocationsCount1+parseInt(binLocationSearch.length);
		getbinLoationsearchresults(maxno,locnGroupList); 
	}
	binLocationsCount1=binLocationsCount1+parseInt(binLocationSearch.length);
	return binLocationsCount1 ;
}

/**
 * This function will fetch all the binlocations and inventory records for the location group passed.
 * @param binLocationGroupList
 * @returns {Array}
 */
function getBinLocationsFromopentask(binLocationGroupList){
	var opentaskSearchResults = new Array();
	var locnGroupList = new Array();
	var opentaskbinlocationsCount=0;
	locnGroupList=binLocationGroupList.split(',');
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'anyof', locnGroupList);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty', '@NONE@');
	filters[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['2']);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('internalid'); 
	columns[2] = new nlobjSearchColumn('custrecord_act_end_date'); 
	columns[1].setSort();
	opentaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(opentaskSearchResults != null)
	{
		opentaskbinlocationsCount=opentaskbinlocationsCount+parseInt(opentaskSearchResults.length);
		if(opentaskSearchResults.length>=1000)
		{

			var maxno=opentaskSearchResults[opentaskSearchResults.length-1].getValue(columns[1]);
			var count=	getopentaskBinLocations(maxno,locnGroupList);
			opentaskbinlocationsCount=opentaskbinlocationsCount+parseInt(count);
		}   

	}
	return opentaskbinlocationsCount;
}
var opentaskbinCount=0;
function getopentaskBinLocations(maxno,locnGroupList)
{
	var opentaskSearchResults = new Array();
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'anyof', locnGroupList);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty', '@NONE@');
	filters[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['2']);
	filters[4] = new nlobjSearchFilter('id', null, 'greaterthan', maxno);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('internalid'); 
	columns[2] = new nlobjSearchColumn('custrecord_act_end_date'); 
	columns[1].setSort();
	opentaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(opentaskSearchResults.length>=1000)
	{          
		var maxno=opentaskSearchResults[opentaskSearchResults.length-1].getValue(columns[1]);
		opentaskbinCount=opentaskbinCount+parseInt(opentaskSearchResults.length);
		getopentaskBinLocations(maxno,locnGroupList); 
	}
	opentaskbinCount=opentaskbinCount+parseInt(opentaskSearchResults.length);
	return opentaskbinCount ;
}

function getBinLocationsFromInventory(binLocationGroupList){
	var inventorySearchResults = new Array();
	var locnGroupList = new Array();
	var inventorybinlocationsCount=0;
	locnGroupList=binLocationGroupList.split(',');
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', locnGroupList);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('internalid'); 
	columns[1].setSort();
	inventorySearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(inventorySearchResults != null)
	{
		inventorybinlocationsCount=inventorybinlocationsCount+parseInt(inventorySearchResults.length);
		if(inventorySearchResults.length>=1000)
		{
			var maxno=inventorySearchResults[inventorySearchResults.length-1].getValue(columns[1]);
			var count=	getinventoryBinLoationsearchresults(maxno,locnGroupList);
			inventorybinlocationsCount=inventorybinlocationsCount+parseInt(count);
		}   

	}
	return inventorybinlocationsCount;
}
var inventorybinCount=0;
function getinventoryBinLoationsearchresults(maxno,locnGroupList)
{
	var inventorySearchResults = new Array();
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', locnGroupList);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	filters[2] = new nlobjSearchFilter('id', null, 'greaterthan', maxno);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('internalid'); 
	columns[1].setSort();
	inventorySearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(inventorySearchResults.length>=1000)
	{          
		var maxno=inventorySearchResults[inventorySearchResults.length-1].getValue(columns[1]);
		inventorybinCount=inventorybinCount+parseInt(inventorySearchResults.length);
		getinventoryBinLoationsearchresults(maxno,locnGroupList); 
	}
	inventorybinCount=inventorybinCount+parseInt(inventorySearchResults.length);
	return inventorybinCount ;
}

function LocationUtilizationByZonesPortlet12(portlet, column)
{
//	<script type='text/javascript' src='https://www.google.com/jsapi'></script>;
//	content+="google.load('visualization', '1', {packages:['corechart']});"; 
//
//	content+="var data= new google.visualization.DataTable();";
//	content+="data.addColumn('string', 'Year');"; 
//	content+="data.addColumn('number', 'Sales');";
//	content+="data.addColumn('number', 'Expenses');";
//	content+="data.addRows(4);";
//	content+="data.setValue(0, 0, '2004');";
//	content+="data.setValue(0, 1, 1000);"; 
//	content+="data.setValue(0, 2, 400);";
//	content+="data.setValue(1, 0, '2005');";
//	content+="data.setValue(1, 1, 1170);";
//	content+="data.setValue(1, 2, 460);";
//	content+="data.setValue(2, 0, '2006');";
//	content+="data.setValue(2, 1, 660);";
//	content+="data.setValue(2, 2, 1120);"; 
//	content+="data.setValue(3, 0, '2007');"; 
//	content+="data.setValue(3, 1, 1030);"; 
//	content+="data.setValue(3, 2, 540);"; 
//	content+=" var chart = new google.visualization.BarChart(document.getElementById('chart_div'));" ;
//	content+="chart.draw(data, {width: 400, height: 240, title: 'Company Performance', vAxis: {title: 'Year', titleTextStyle: {color: 'red'}} });";
//	content+="<div id='chart_div' style='border: 1px dotted red; height: 300px; width: 300px' align=center/>;";
//	portlet.setHtml(content);
	//	  var fld = portlet.addField('divfield','inlinehtml');
//	fld.setDefaultValue("<div id='chart_div' style='border: 1px dotted red; height: 300px; width: 300px'></div>");
}
function LocationUtilizationByZonesPortlet(portlet, column)
{  
	portlet.setTitle('Location&nbsp;Utilization&nbsp;By&nbsp;Zones');	
	var putZoneSearchResults = getPutZone();
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('internalid');
	var chxl="|";var lenth=new Array(); var ids=new Array();var filled="";var empty="";var maxno="";var chdl="";
	var zoneIdArray = new Array();var zonetextArray=new Array();
	if(putZoneSearchResults !=null){
		for (var i = 0; i < putZoneSearchResults.length; i++) {
			var zone = putZoneSearchResults[i].getValue(columns[0]);
			//zone = zone.split(' ').join('+');
//			chxl=chxl+zone+"|";    	
			lenth[i]=zone.length;
			zoneIdArray[i]=putZoneSearchResults[i].getValue(columns[1]);
			zonetextArray[i]=putZoneSearchResults[i].getValue(columns[0]);
		}
	}

	var largest = Math.max.apply(Math, lenth); 
	largest=4*largest;
//	chxl = chxl.substring(0, chxl.length-1);


	var binLocationGroupList = getBinLocationGroup(zoneIdArray);
	if(binLocationGroupList != null)
	{
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
		columns[1] = new nlobjSearchColumn('custrecord_locgroup_no');
		for (var mn = 0; mn< zoneIdArray.length; mn++) {
			var len=new Array();
			var count=0;
			var binLocationsGrpLst="";var pzone="";var zonename="";
			for ( var i = 0; i < binLocationGroupList.length; i++) {
				if(binLocationGroupList[i].getValue(columns[0])!=null)
				{
					if(binLocationGroupList[i].getValue(columns[0])==zoneIdArray[mn])
					{

						var tempvar=binLocationGroupList[i].getText(columns[1]);
						len[count]=tempvar; 
						count=count+1;
						maxno=maxno+count+",";
						binLocationsGrpLst=binLocationsGrpLst+binLocationGroupList[i].getValue(columns[1])+",";

						if(pzone=="" && binLocationsGrpLst!="")
						{
							pzone = binLocationGroupList[i].getText(columns[0]);
							pzone = pzone.split(' ').join('+');
//							zonename=pzone;
//							if(pzone.length>3)
//								{
//							pzone = pzone.substring(0, 3);}
							
							chxl=chxl+pzone+"|";   
						}
						
					}
					
					
				}	   		    

			}
			
			if(binLocationsGrpLst!="")
			{
				binLocationsGrpLst = binLocationsGrpLst.substring(0, binLocationsGrpLst.length-1);
				nlapiLogExecution('ERROR', 'binLocationsGrpLst', binLocationsGrpLst);
				
				var binLocCount=0;var inventoryCount=0;var opentaskcount=0;var totalFilledCount=0;
				var binLoccolumns = new Array();		
				binLocCount = getBinLocations(binLocationsGrpLst);
				nlapiLogExecution('ERROR', 'binLocCount', binLocCount);


				if(binLocCount>0)
				{
					inventoryCount = getBinLocationsFromInventory(binLocationsGrpLst);
					opentaskcount=getBinLocationsFromopentask(binLocationsGrpLst);
					nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);
					totalFilledCount=parseInt(inventoryCount)+parseInt(opentaskcount);
					nlapiLogExecution('ERROR', 'inventoryCount', inventoryCount);
					nlapiLogExecution('ERROR', 'totalFilledCount', totalFilledCount);
					filled=filled+totalFilledCount+",";
					chdl=chdl+zonename+"(filled)"+"|";
					if(totalFilledCount>=0){
						var largenum=Math.max(binLocCount,inventoryCount);
						var smallnum=Math.min(binLocCount,inventoryCount);
						var emp=parseInt(binLocCount)-parseInt(totalFilledCount);
						empty=empty+emp+",";
						if(emp>0)
						{
							chdl=chdl+zonename+"(empty)"+"|";
						}
					}
					else
					{
						var emp="0";
						empty=empty+emp+",";
						//chdl=chdl+zonename+"(empty)"+"|";
					}

				}
				else
				{
					filled=filled+"0"+",";
					var emp="0";
					empty=empty+emp+",";
					//chdl=chdl+zonename+"(filled)"+"|";
					//chdl=chdl+zonename+"(empty)"+"|";

				}
			}
			else
			{
				filled=filled+"0"+",";
				empty=empty+"0"+",";
				pzone =zonetextArray[mn];
				
				pzone = pzone.split(' ').join('+');
				chxl=chxl+pzone+"|";
				//chdl=chdl+zonename+"(filled)"+"|";
				//chdl=chdl+zonename+"(empty)"+"|";
			}
		}
		chdl = chdl.substring(0, chdl.length-1);
		chxl = chxl.substring(0, chxl.length-1);
		filled = filled.substring(0, filled.length-1); 
		empty = empty.substring(0, empty.length-1); 
		var tempvar=filled+","+empty+","+maxno;
		var temparray=tempvar.split(',');
		var maxnum = Math.max.apply(Math, temparray); 
		var chmemptyparamarray=empty.split(',');
		var chmfilledparamarray=filled.split(',');
		var chmparam="";
		for ( var n = 0; n < chmemptyparamarray.length; n++) {
			nlapiLogExecution('ERROR', 'chmemptyparamarray', chmemptyparamarray[n]);
			nlapiLogExecution('ERROR', 'chmfilledparamarray', chmfilledparamarray[n]);

			chmparam=chmparam+"t"+chmemptyparamarray[n]+",000000,0,"+n+",13"+"|";
			chmparam=chmparam+"t"+chmfilledparamarray[n]+",000000,1,"+n+",13"+"|";
		}
		chmparam = chmparam.substring(0, chmparam.length-1); 
		empty=empty+"|";

		nlapiLogExecution('ERROR', 'chmparam', chmparam);
		nlapiLogExecution('ERROR', 'filled', filled);
		nlapiLogExecution('ERROR', 'empty', empty);
		nlapiLogExecution('ERROR', 'maxnum', maxnum);
		nlapiLogExecution('ERROR', 'chxl', chxl);
		nlapiLogExecution('ERROR', 'chdl', chdl);
		var  content="<div align=center ><img src= http://chart.apis.google.com/";
		content+="chart?";
		content+="&chxr=1,0,"+parseInt(maxnum)+"";	
		content+="&chxt=x,y";		
		content+="&chxl=0:"+chxl+"";
		//content+="&chbh=19,0,"+parseInt(largest)+"";
		content+="&chbh=19,0,80";
		content+="&chs=900x250" ;
		content+="&cht=bvg";
		content+="&chco=6699cc,811300";	
		content+="&chd=t:"+empty+""+filled+"";
		content+="&chds=0,"+parseInt(maxnum)+"" ;
		content+="&chm="+chmparam+"";
		content+="&chg=0,9,0,0";
		//content+="&chdl="+chdl+"&chdls=000000,12";
		content+="&chdl=empty|filled&chdls=000000,12";
		content+="&chtt=Location&nbsp;Utilization&nbsp;By&nbsp;Zones";  
		content+=" /></div>";
		portlet.setHtml( content );


	}
}











