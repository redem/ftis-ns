/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_Pack_CloseCarton.js,v $
 *     	   $Revision: 1.1.2.5.4.3.4.18 $
 *     	   $Date: 2015/04/23 21:01:16 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Pack_CloseCarton.js,v $
 * Revision 1.1.2.5.4.3.4.18  2015/04/23 21:01:16  grao
 * SB issue fixes  201412472
 *
 * Revision 1.1.2.5.4.3.4.17  2014/07/03 15:33:20  skavuri
 * Case# 20148118 Compatability Issue Fixed
 *
 * Revision 1.1.2.5.4.3.4.16  2014/06/20 06:54:02  skavuri
 * Case # 20148118 SB Issue Fixed
 *
 * Revision 1.1.2.5.4.3.4.15  2014/06/19 15:39:52  skavuri
 * Case # 20148118 SB Issue Fixed
 *
 * Revision 1.1.2.5.4.3.4.14  2014/06/17 15:43:41  skavuri
 * Case# 20148858 SB Issue Fixed.
 *
 * Revision 1.1.2.5.4.3.4.13  2014/06/13 13:00:14  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.5.4.3.4.12  2014/06/06 07:04:48  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.5.4.3.4.11  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.5.4.3.4.10  2014/05/28 15:20:08  skavuri
 * Case# 20148591 SB Issue Fixed
 *
 * Revision 1.1.2.5.4.3.4.9  2014/05/27 13:57:22  snimmakayala
 * Case#: 20148567
 * RF Packing issue fixes
 *
 * Revision 1.1.2.5.4.3.4.8  2014/03/19 07:08:57  gkalla
 * case#20127671
 * LL issue fix
 *
 * Revision 1.1.2.5.4.3.4.7  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.5.4.3.4.6  2013/12/02 08:57:54  schepuri
 * 20125893,20125974,20125993
 *
 * Revision 1.1.2.5.4.3.4.5  2013/11/28 06:06:59  skreddy
 * Case# 20125944
 * Afosa SB issue fix
 *
 * Revision 1.1.2.5.4.3.4.4  2013/11/14 15:42:27  skreddy
 * Case# 20125642
 * Afosa SB issue fix
 *
 * Revision 1.1.2.5.4.3.4.3  2013/09/30 15:57:43  rmukkera
 * Case#  20124700
 *
 * Revision 1.1.2.5.4.3.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.5.4.3.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.5.4.3  2013/01/02 15:42:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Double Click restriction.
 * .
 *
 * Revision 1.1.2.5.4.2  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.5.4.1  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.5  2012/07/10 22:59:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Fixed Shipmanifest issue
 *
 * Revision 1.1.2.4  2012/06/28 07:49:59  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.1.2.3  2012/06/15 16:26:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.1.2.2  2012/06/15 10:08:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.1.2.1  2012/06/06 07:39:03  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.32.4.21  2012/05/14 14:37:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to validating LP
 *
 * Revision 1.32.4.20  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.32.4.19  2012/05/09 15:58:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing Trantype in query string was missing
 *
 * Revision 1.32.4.18  2012/04/27 07:25:07  gkalla
 * CASE201112/CR201113/LOG201121
 * QC check based on rule configuration
 *
 * Revision 1.32.4.17  2012/04/11 22:02:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related navigating to TO Item Status when the user click on Previous button.
 *
 * Revision 1.32.4.16  2012/04/10 22:59:15  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related navigating to previous screen is resolved.
 *
 * Revision 1.32.4.15  2012/03/21 11:08:48  schepuri
 * CASE201112/CR201113/LOG201121
 * Added movetask
 *
 * Revision 1.32.4.14  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.32.4.13  2012/03/06 11:00:05  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.12  2012/03/05 14:42:30  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.11  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.43  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.42  2012/02/21 07:36:44  spendyala
 * CASE201112/CR201113/LOG201121
 * code merged upto 1.32.4.9
 *
 * Revision 1.41  2012/02/14 07:13:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged upto 1.32.4.8.
 *
 * Revision 1.40  2012/02/13 13:39:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.39  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.38  2012/02/10 10:25:15  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.6.
 *
 * Revision 1.37  2012/02/07 07:24:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.4.
 *
 * Revision 1.36  2012/02/01 12:53:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.3.
 *
 * Revision 1.35  2012/02/01 06:30:36  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.32.4.2
 *
 * Revision 1.34  2012/01/19 00:43:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Changes
 *
 * Revision 1.33  2012/01/09 13:11:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Issue if there a space in batch no
 *
 * Revision 1.32  2012/01/06 13:03:09  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing, redirecting to serialno entry
 *
 * Revision 1.31  2012/01/04 20:18:38  gkalla
 * CASE201112/CR201113/LOG201121
 * To add Item to Batch entry
 *
 * Revision 1.30  2012/01/04 15:08:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.29  2011/12/30 14:09:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.28  2011/12/27 23:24:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.27  2011/12/23 23:36:41  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.26  2011/12/23 16:21:51  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.25  2011/12/23 13:24:07  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.24  2011/12/23 13:19:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.23  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.22  2011/12/15 14:10:40  schepuri
 * CASE201112/CR201113/LOG201121
 * modified field names in customrecord_ebiznet_trn_poreceipt
 *
 * Revision 1.21  2011/12/02 13:19:35  schepuri
 * CASE201112/CR201113/LOG201121
 * RF To generate location acc to the location defined in PO
 *
 * Revision 1.20  2011/11/17 08:44:28  spendyala
 * CASE201112/CR201113/LOG201121
 * wms status flag for create inventory is changed to 17 i .e, FLAG INVENTORY INBOUND
 *
 * Revision 1.19  2011/09/29 12:04:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.18  2011/09/28 16:08:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/09/26 19:58:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.16  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.14  2011/09/14 05:27:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/09/12 08:24:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/09/10 11:28:23  schepuri
 * CASE201112/CR201113/LOG201121
 *
 * redirecting to customscript_serial_no
 *
 * Revision 1.11  2011/07/05 12:36:44  schepuri
 * CASE201112/CR201113/LOG201121
 * RF CheckIN changes
 *
 * Revision 1.10  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.9  2011/06/17 07:39:47  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * Confirm putaway changes
 *
 * Revision 1.8  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.6  2011/06/06 07:10:13  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * WBC Issues
 *
 * Revision 1.5  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function Packing_CloseCarton(request, response){
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') 
	{


		var getOrderno = request.getParameter('custparam_orderno');
		var getContainerNo = request.getParameter('custparam_containerno');
		//var getContainerSize = request.getParameter('custparam_containersize');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var bulkqty=request.getParameter('custparam_bulkqty');
		var fetchedcontainerlp =request.getParameter('custparam_fetchedcontainerlp');
		var getnumber =request.getParameter('custparam_number');
		var wmslocation =request.getParameter('custparam_wmslocation');
		var waveno=request.getParameter('custparam_waveno');// case# 201416891
		
		var filtersContiner = new Array(); 

		filtersContiner.push(new nlobjSearchFilter('internalid', null, 'is', 1));	

		var colsContainer = new Array();
		colsContainer[0]=new nlobjSearchColumn('custrecord_cubecontainer');  
		colsContainer[1]=new nlobjSearchColumn('custrecord_tareweight');  
		colsContainer[2]=new nlobjSearchColumn('custrecord_inventory_in_on');
		colsContainer[2]=new nlobjSearchColumn('name');

		var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersContiner, colsContainer);  
		nlapiLogExecution('DEBUG','ContainerSearchResults',ContainerSearchResults);
		if(ContainerSearchResults!=null && ContainerSearchResults!='')
		{
			var getContainerSize=ContainerSearchResults[0].getValue('name');
		}
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		
		
		var st1,st2,st3,st4,st5,st6;
		
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st1 = "PEDIDO #";
			st2 = "CAJA # :";
			// 20125974
			st3 = "ENTRAR / SCAN PESO DE LA CAJA:";
			st4 = "CAJA CERRADO:";
			st5 = "ORDEN DE CERRAR :";
			st6 ="ANTERIOR";
			
		}
		else
		{
			st1 = "ORDER#";
			st2 = "CARTON# :";
			st3 = "ENTER/SCAN CARTON  WEIGHT :";
			st4 = "CLOSE CARTON :";
			st5 = "CLOSE ORDER :";
			st6 ="PREV";
				
		}

		
		var functionkeyHtml=getFunctionkeyScript('_rf_pack_scancontainer'); 	
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		//html = html + " document.getElementById('enterweight').focus();";        
		html = html + " function check(){var size=document.getElementById('entersize').value;if(size=='' || size==null){alert('pls enter carton size');return false;}else{return true;}}";   
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_pack_scancontainer' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getOrderno + "</label>";	
		html = html + "				</td>";
		html = html + "			</tr>";  
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ": <label>" + getContainerNo + "</label>";		
		html = html + "				</td>";
		html = html + "			</tr>";  
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdnOrderNo' value=" + getOrderno + ">";			
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + fetchedcontainerlp + ">";
		html = html + "				<input type='hidden' name='hdnbulkqty' value=" + bulkqty + ">";
		html = html + "				<input type='hidden' name='hdnnumber' value=" + getnumber + ">";
		html = html + "				<input type='hidden' name='hdnwmslocation' value=" + wmslocation + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnwaveno' value=" + waveno + ">";
		html = html + "				<input type='hidden' name='hdnclosecarton'>";
		html = html + "				<input type='hidden' name='hdncloseorder'>";
		html = html + "				<input type='hidden' name='hdnprevious'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterweight' id='enterweight' type='text'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "					" + st4 + " <input name='cmdclosecarton' type='submit' value='F8' onclick='this.form.hdnclosecarton.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdclosecartonandorder.disabled=true;return false'/>";
		//html = html + "					"+st6+" <input name='cmdPrevious' type='submit' value='F7' onclick='this.form.hdnprevious.value=this.value;this.form.submit();this.disabled=true;this.form.cmdclosecarton.disabled=true;this.form.cmdclosecartonandorder.disabled=true;return false'/>";		
		//html = html + "					" + st4 + " <input name='cmdclosecarton' type='submit' value='F8' onclick='this.form.hdnclosecarton.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdclosecartonandorder.disabled=true;return false'/>";
		html = html + "				" + st5 + " <input name='cmdclosecartonandorder' type='submit' value='F9' onclick='this.form.hdncloseorder.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdclosecarton.disabled=true;return false'/>";
		html = html + "					"+st6+" <input name='cmdPrevious' type='submit' value='F7' onclick='this.form.hdnprevious.value=this.value;this.form.submit();this.disabled=true;this.form.cmdclosecarton.disabled=true;this.form.cmdclosecartonandorder.disabled=true;return false'/></br>";		

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterweight').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	
	}
	else
	{
		var optedEvent=request.getParameter('hdnprevious');	
		if(optedEvent==null || optedEvent=='')
		{
			optedEvent=request.getParameter('hdnclosecarton');
		}
		if(optedEvent==null || optedEvent=='')
		{
			optedEvent=request.getParameter('hdncloseorder');
		}
//		if(optedEvent==null || optedEvent=='')
//		{
//		optedEvent=request.getParameter('cmdcloseandprint');
//		}
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
		var SOarray=new Array();
		var getLanguage =  request.getParameter('hdngetLanguage');
		
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
				
				
				var st6,st7,st8,st9,st10,st11;
				
				if( getLanguage == 'es_ES' || getLanguage =='es_AR')
				{
					
					st6 = "ENTER PESO CAJA V#193;LIDO.";
					st7 = "PAQUETES PENDIENTES PARA LA ORDEN #";
					st8 = "POR FAVOR RECOJA EL RESTO DE LOS ELEMENTOS";
					st9 = "NO ABRA LOS PAQUETES DE ESTE CARTON #";
					st10 = "NO SE PUEDE CERRAR";
					st11 = "ORDEN V&#193;LIDA #";
					
				}
				else
				{	st6 = "ENTER VALID CARTON  WEIGHT";
					st7 = "PENDING PACKS FOR THE ORDER #";
					st8 = "PLEASE PICK THE REST OF THE ITEMS";
					st9 = "THERE ARE OPEN PACKS FOR THIS CARTON#";
					st10 = "YOU CANNOT CLOSE";
					st11 = "INVALID ORDER #";
					
				}
		 SOarray["custparam_fetchedcontainerlp"]=request.getParameter('hdnFetchedContainerLPNo');  
		SOarray['custparam_orderno']=	request.getParameter('custparam_orderno');
		SOarray['custparam_containerno']=	request.getParameter('custparam_containerno');
		SOarray['custparam_iteminternalid']=	request.getParameter('custparam_iteminternalid');
		SOarray['custparam_containersize']=	request.getParameter('custparam_containersize');
		SOarray['custparam_expectedquantity']=	request.getParameter('custparam_expectedquantity');
		SOarray['custparam_recordinternalid']=	request.getParameter('custparam_recordinternalid');
		SOarray['custparam_bulkqty']=	request.getParameter('hdnbulkqty');
		SOarray["custparam_number"]=request.getParameter('hdnnumber');
		SOarray['custparam_wmslocation']=request.getParameter('hdnwmslocation');
		SOarray['custparam_actualquantity']=request.getParameter('custparam_actualquantity');
		SOarray['custparam_language']=request.getParameter('hdngetLanguage');
		SOarray["custparam_waveno"] = request.getParameter('hdnwaveno');

		var ItemInternalId=request.getParameter('custparam_iteminternalid');
		var orderno=request.getParameter('hdnOrderNo');
		var varEnteredSize=request.getParameter('custparam_containersize');
		var contlp=request.getParameter('hdnFetchedContainerLPNo');
		var varEnteredLP=request.getParameter('custparam_containerno');
		var ContainerLPNo=request.getParameter('hdnFetchedContainerLPNo');
		nlapiLogExecution('DEBUG', 'varEnteredLP', varEnteredLP);
		nlapiLogExecution('DEBUG', 'ContainerLPNo', ContainerLPNo);

		var trantype='salesorder';


		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if(optedEvent=='F7')
				{
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanqty', 'customdeploy_ebiz_rf_pack_scanqty_di', false, SOarray);
				}
				else if(optedEvent=='F9')//close cartonandorder
				{
					var weight= request.getParameter('enterweight');

					if( weight==null || weight=='' || isNaN(weight) )
					{
						SOarray["custparam_error"] = st6;
						SOarray["custparam_screenno"] = 'Pack5';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}

			///Closecarton     

			nlapiLogExecution('DEBUG', 'CLOSE', 'CLOSE');

			var now = new Date();

			var a_p = "";
			var d = new Date();
			var curr_hour = now.getHours();
			if (curr_hour < 12) {
				a_p = "am";
			}
			else {
				a_p = "pm";
			}
			if (curr_hour == 0) {
				curr_hour = 12;
			}
			if (curr_hour > 12) {
				curr_hour = curr_hour - 12;
			}

			var curr_min = now.getMinutes();

			curr_min = curr_min + "";

			if (curr_min.length == 1) 
			{
				curr_min = "0" + curr_min;
			}

			var CurrentDate = DateStamp();

			var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
			var itemCube = 0;
			var itemWeight=0;						
			var ContainerCube;					
			var containerInternalId;
			var ContainerSize;
			nlapiLogExecution('DEBUG', 'getContainerSize', varEnteredSize);	



			var arrContainerDetails = getContainerCubeAndTarWeight("",varEnteredSize);					
			if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

				ContainerCube =  parseFloat(arrContainerDetails[0]);						
				containerInternalId = arrContainerDetails[3];
				ContainerSize=arrContainerDetails[4];
				TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
				nlapiLogExecution('DEBUG', 'ContainerSize1', ContainerSize);	
			} 
			if(varEnteredSize=="" || varEnteredSize==null)
			{
				varEnteredSize=ContainerSize;
				nlapiLogExecution('DEBUG', 'ContainerSize2', ContainerSize);	
			}

			var varContainerCube;
			var varContainerTareWeight;
			var varContInventory;

			var filtersContiner = new Array(); 
			if(varEnteredSize!=null && varEnteredSize!='')
				filtersContiner.push(new nlobjSearchFilter('name', null, 'is', varEnteredSize));	

			var colsContainer = new Array();
			colsContainer[0]=new nlobjSearchColumn('custrecord_cubecontainer');  
			colsContainer[1]=new nlobjSearchColumn('custrecord_tareweight');  
			colsContainer[2]=new nlobjSearchColumn('custrecord_inventory_in_on');  

			var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersContiner, colsContainer);  
			var varcontintid;
			if(ContainerSearchResults!=null)
			{	
				varcontintid =  ContainerSearchResults[0].getId();
				varContainerCube = ContainerSearchResults[0].getValue('custrecord_cubecontainer');
				varContainerTareWeight = ContainerSearchResults[0].getValue('custrecord_tareweight');
				varContInventory = ContainerSearchResults[0].getValue('custrecord_inventory_in_on');				
			}
//			else
//			{
//			SOarray["custparam_error"] = 'CARTON Not Matched';
//			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
//			nlapiLogExecution('DEBUG', 'Container Not Matched', varEnteredLP);
//			return;
//			}

			var fetchQty;
			var diffQty=null;
			var contlpno;
			var wmslocation;
			var cubeinfo1;
			var cubeinfo2=0;
			var weightinfo1;
			var weightinfo2=0;
			var opentaskwt;
			var opentasktotalcube;
			var varname;




					var OpenTaskitemFilters = new Array();	
					//if(orderno!=null && orderno!='')	    	 	
					//OpenTaskitemFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
					OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varEnteredLP));   	 	
					OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_pack_confirmed_date',null, 'isnotempty', null));
					OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//pack complete
					OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pick
					OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));
					var OpenTaskitemColumns = new Array();
					OpenTaskitemColumns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
					OpenTaskitemColumns[1] = new nlobjSearchColumn('custrecord_act_qty');
					OpenTaskitemColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
					OpenTaskitemColumns[3] = new nlobjSearchColumn('name');
					OpenTaskitemColumns[4] = new nlobjSearchColumn('custrecord_total_weight');
					OpenTaskitemColumns[5] = new nlobjSearchColumn('custrecord_totalcube');
					OpenTaskitemColumns[6] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					OpenTaskitemColumns[7] = new nlobjSearchColumn('custrecord_ebiz_lpno');
					OpenTaskitemColumns[8] = new nlobjSearchColumn('custrecord_wms_location');
					var  k=0;var Recid;
					var OpenTaskitemSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskitemFilters, OpenTaskitemColumns);
					if(OpenTaskitemSearchResults!=null && OpenTaskitemSearchResults!='')
					{
						if(OpenTaskitemSearchResults[0].getValue('custrecord_ebiz_order_no') != null && OpenTaskitemSearchResults[0].getValue('custrecord_ebiz_order_no') != '')
						{	
							nlapiLogExecution('DEBUG', 'eBizOrdNoNew', OpenTaskitemSearchResults[0].getValue('custrecord_ebiz_order_no'));
							trantype = nlapiLookupField('transaction', OpenTaskitemSearchResults[0].getValue('custrecord_ebiz_order_no'), 'recordType');
						}
						nlapiLogExecution('DEBUG', 'trantype1', trantype);

				//creating the masterlp record with new containerLP
//				nlapiLogExecution('DEBUG', 'LP NOT FOUND');
//				var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
//				customrecord.setFieldValue('name', varEnteredLP);
//				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', varEnteredLP);
//				var rec = nlapiSubmitRecord(customrecord, false, true);
				nlapiLogExecution('DEBUG', '<OpenTaskitemSearchResults.length',OpenTaskitemSearchResults.length);
				for(var i=0;i<OpenTaskitemSearchResults.length;i++)
				{
					iteminternalid=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_sku_no');
					getExpQty = OpenTaskitemSearchResults[i].getValue('custrecord_expe_qty'); //Assign the Enter Qty here imp

					Recid= OpenTaskitemSearchResults[i].getId();
					//getExpQty
					fetchQty =OpenTaskitemSearchResults[i].getValue('custrecord_act_qty');
					contlpno = OpenTaskitemSearchResults[i].getValue('custrecord_container_lp_no');
					wmslocation = OpenTaskitemSearchResults[i].getValue('custrecord_wms_location');
					opentaskwt	= OpenTaskitemSearchResults[i].getValue('custrecord_total_weight');
					opentasktotalcube = OpenTaskitemSearchResults[i].getValue('custrecord_totalcube');
					varname = OpenTaskitemSearchResults[i].getValue('name');
					var vOldContLp=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_lpno');
					var skuname = OpenTaskitemSearchResults[i].getText('custrecord_sku');

					nlapiLogExecution('DEBUG', 'skuname::', skuname);

					nlapiLogExecution('DEBUG', 'getExpQty::', getExpQty);
					nlapiLogExecution('DEBUG', 'fetchQty::', fetchQty);
					nlapiLogExecution('DEBUG', 'contlpno::', contlpno);
					nlapiLogExecution('DEBUG', 'wmslocation::', wmslocation);
					var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);					

//					transaction.setFieldValue('custrecord_wms_status_flag', '28');//28 stands for packcomplete //Added on 24th
//					transaction.setFieldValue('custrecord_container_lp_no', varEnteredLP);
//					transaction.setFieldValue('custrecord_expe_qty', parseFloat(fetchQty));
//					nlapiSubmitRecord(transaction, false, true);
					cubeinfo2 =  parseFloat(cubeinfo2)+ ((getExpQty * parseFloat(opentasktotalcube)) / fetchQty);
					nlapiLogExecution('DEBUG', 'cubeinfo2::', cubeinfo2);
					weightinfo2 = parseFloat(weightinfo2)+((getExpQty * parseFloat(opentaskwt)) / fetchQty);
					nlapiLogExecution('DEBUG', 'weightinfo2::', weightinfo2);
					nlapiLogExecution('DEBUG', 'varContainerTareWeight::', varContainerTareWeight);
					var diffqty=parseFloat(getExpQty)-parseFloat(fetchQty);
					nlapiLogExecution('DEBUG', 'diffqty', diffqty);
//					if(diffqty!='' && diffqty> 0)
//					{
//					var Remainqtyrecord =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	
//					Remainqtyrecord.setFieldValue('custrecord_act_qty', parseFloat(diffqty)); 
//					Remainqtyrecord.setFieldValue('custrecord_expe_qty', parseFloat(diffqty)); 
//					Remainqtyrecord.setFieldValue('custrecord_wms_status_flag', 8);
//					Remainqtyrecord.setFieldValue('name', varname);
//					Remainqtyrecord.setFieldValue('custrecord_pack_confirmed_date', null);


//					var rempick = nlapiSubmitRecord(Remainqtyrecord, false, true);
//					}


					var weight= request.getParameter('enterweight');
					if(i==0)
						var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	 	//for Assign Station purpose

					UpdateoutboundInventory(ContainerLPNo,varEnteredLP);
					//UpdateoutboundInventory(vOldContLp,varEnteredLP);

				//} // Case# 20148591
					var fields = ['name']; //  Case# 20148858
						var locationcolumns = nlapiLookupField('location', wmslocation, fields);
						if(locationcolumns!=null)
							sitetext = locationcolumns.name;

						var sitetextvalue;
						if(sitetext=='Garden Grove')
							sitetextvalue=1;
						else
							sitetextvalue=2;
				//	} // Case # 20148591	
					var BOLSeqNo="";
					if(sitetextvalue !=null && sitetextvalue !='')
						BOLSeqNo=orderno+date+'-'+sitetextvalue;
					else
						BOLSeqNo=orderno+date;
					AssignPackStationrec.setFieldValue('custrecord_bol', BOLSeqNo);
				}	

				AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
				AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
				AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
				AssignPackStationrec.setFieldValue('custrecord_sku', null);
				AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
				AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);
				AssignPackStationrec.setFieldValue('name', varname);
				if(weight!=null && weight!='')
				{
					AssignPackStationrec.setFieldValue('custrecord_total_weight',parseFloat(weight).toFixed(4));
				}
				else
				{
					AssignPackStationrec.setFieldValue('custrecord_total_weight',parseFloat(weightinfo2).toFixed(4));
				}

				//custrecord_sku', null, 'anyof', iteminternalid);
				nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
				var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
				nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
				var vebizOrdNo=null;
				var filter=new Array();
				filter.push(new nlobjSearchFilter('tranid',null,'is',orderno));
				filter.push(new nlobjSearchFilter('mainline',null,'is','F'));

				var columns=new Array(); 		


				var searchrec=nlapiSearchRecord(trantype,null,filter,columns);

				if(searchrec!=null && searchrec!='' && searchrec.length>0)
				{
					vebizOrdNo=searchrec[0].getId();
				}
				if(weight==null || weight=='')
				{
					weight=weightinfo2;
				}
				nlapiLogExecution('DEBUG', 'trantype2', trantype);
				CreateShippingManifestRecord(vebizOrdNo,varEnteredLP,null,null,weight);

			}

			///********8close Orer
			var openTaskFilters = new Array();	

			if(orderno!=null && orderno!='')
			{
				openTaskFilters.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
			}
			openTaskFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); //PICK
			openTaskFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['9','25','15','26','8']));//9 //Picks generated or STATUS.INBOUND_OUTBOUND.EDIT or STATUS.OUTBOUND.SELECTED_INTO_WAVE or STATUS.OUTBOUND.FAILED
			// openTaskFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));

			var openTaskColumns = new Array();
			openTaskColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			openTaskColumns[1] = new nlobjSearchColumn('name');

			var openTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openTaskFilters, openTaskColumns);
			nlapiLogExecution('DEBUG', 'openSearchResults', openTaskSearchResults);


			if(openTaskSearchResults==null )
			{
				var openTaskFilters1 = new Array();
				var openTaskColumns = new Array();
				openTaskColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				openTaskColumns[1] = new nlobjSearchColumn('name');//
				openTaskColumns[2] = new nlobjSearchColumn('custrecord_wms_location');
				if(orderno!=null && orderno!='')
				{
					openTaskFilters1.push( new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
				}
				openTaskFilters1.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
				//openTaskFilters1.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8']));
				var openTaskSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openTaskFilters1, openTaskColumns);
				nlapiLogExecution('DEBUG', 'openSearchResults1', openTaskSearchResults1);
				if(openTaskSearchResults1!=null && openTaskSearchResults1.length>0)   
				{
					nlapiLogExecution('DEBUG', 'openSearchResults1', openTaskSearchResults1.length);
					var vebizOrdNo=openTaskSearchResults1[0].getValue('custrecord_ebiz_order_no');
					var fulfillmentnumber=openTaskSearchResults1[0].getValue('name');
					var whlocation=openTaskSearchResults1[0].getValue('custrecord_wms_location');
					nlapiLogExecution('DEBUG', 'vebizOrdNo', vebizOrdNo);
					nlapiLogExecution('DEBUG', 'fulfillmentnumber', fulfillmentnumber);
					if(vebizOrdNo!=null && vebizOrdNo!='' && fulfillmentnumber!=null && fulfillmentnumber !='')
					{
						AutoPacking(vebizOrdNo,whlocation);
						var printername='';
						createpacklisthtml(vebizOrdNo,fulfillmentnumber,printername,orderno);

					}
				}
			}
			else
			{


				SOarray["custparam_screenno"] = 'Pack5';
				SOarray["custparam_error"] =  st7 + orderno + st8;
				SOarray["custparam_orderno"] = orderno;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			}


		}
		else //if(optedEvent=='F8')
		{
			var weight1= request.getParameter('enterweight');
			if(weight1==null || weight1=="" || isNaN(weight1))
			{
				SOarray["custparam_screenno"] = 'Pack5';
				SOarray["custparam_error"] = st6;			
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}
			else
			{

				nlapiLogExecution('DEBUG', 'CLOSE', 'CLOSE');

				var now = new Date();

				var a_p = "";
				var d = new Date();
				var curr_hour = now.getHours();
				if (curr_hour < 12) {
					a_p = "am";
				}
				else {
					a_p = "pm";
				}
				if (curr_hour == 0) {
					curr_hour = 12;
				}
				if (curr_hour > 12) {
					curr_hour = curr_hour - 12;
				}

				var curr_min = now.getMinutes();

				curr_min = curr_min + "";

				if (curr_min.length == 1) 
				{
					curr_min = "0" + curr_min;
				}

				var CurrentDate = DateStamp();




				//*************************************************************************//

				var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
				var itemCube = 0;
				var itemWeight=0;						
//				if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
//				//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
//				itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
//				itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


//				} 
//				nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
//				nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);


				var ContainerCube;					
				var containerInternalId;
				var ContainerSize;
				nlapiLogExecution('DEBUG', 'getContainerSize', varEnteredSize);	



				var arrContainerDetails = getContainerCubeAndTarWeight("",varEnteredSize);					
				if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

					ContainerCube =  parseFloat(arrContainerDetails[0]);						
					containerInternalId = arrContainerDetails[3];
					ContainerSize=arrContainerDetails[4];
					TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
					nlapiLogExecution('DEBUG', 'ContainerSize1', ContainerSize);	
				} 
				if(varEnteredSize=="" || varEnteredSize==null)
				{
					varEnteredSize=ContainerSize;
					nlapiLogExecution('DEBUG', 'ContainerSize2', ContainerSize);	
				}

				var varContainerCube;
				var varContainerTareWeight;
				var varContInventory;

				var filtersContiner = new Array(); 
				if(varEnteredSize!=null && varEnteredSize!='')
					filtersContiner.push(new nlobjSearchFilter('name', null, 'is', varEnteredSize));	

				var colsContainer = new Array();
				colsContainer[0]=new nlobjSearchColumn('custrecord_cubecontainer');  
				colsContainer[1]=new nlobjSearchColumn('custrecord_tareweight');  
				colsContainer[2]=new nlobjSearchColumn('custrecord_inventory_in_on');  

				var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersContiner, colsContainer);  
				var varcontintid;
				if(ContainerSearchResults!=null)
				{	
					varcontintid =  ContainerSearchResults[0].getId();
					varContainerCube = ContainerSearchResults[0].getValue('custrecord_cubecontainer');
					varContainerTareWeight = ContainerSearchResults[0].getValue('custrecord_tareweight');
					varContInventory = ContainerSearchResults[0].getValue('custrecord_inventory_in_on');				
				}
//				else
//				{
//				SOarray["custparam_error"] = 'CARTON Not Matched';
//				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
//				nlapiLogExecution('DEBUG', 'Container Not Matched', varEnteredLP);
//				return;
//				}

				var fetchQty;
				var diffQty=null;
				var contlpno;
				var wmslocation;
				var cubeinfo1;
				var cubeinfo2=0;
				var weightinfo1;
				var weightinfo2=0;
				var opentaskwt;
				var opentasktotalcube;
				var varname;


				nlapiLogExecution('DEBUG', 'orderno', orderno);
				nlapiLogExecution('DEBUG', 'contlp', contlp);
				nlapiLogExecution('DEBUG', 'varEnteredLP', varEnteredLP);

				var OpenTaskitemFilters = new Array();	
				//if(orderno!=null && orderno!='')	    	 	
				//OpenTaskitemFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
				OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varEnteredLP));   	 	
				OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_pack_confirmed_date',null, 'isnotempty', null));
				OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//pack complete
				OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pick
				OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));
				var OpenTaskitemColumns = new Array();
				OpenTaskitemColumns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				OpenTaskitemColumns[1] = new nlobjSearchColumn('custrecord_act_qty');
				OpenTaskitemColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
				OpenTaskitemColumns[3] = new nlobjSearchColumn('name');
				OpenTaskitemColumns[4] = new nlobjSearchColumn('custrecord_total_weight');
				OpenTaskitemColumns[5] = new nlobjSearchColumn('custrecord_totalcube');
				OpenTaskitemColumns[6] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				OpenTaskitemColumns[7] = new nlobjSearchColumn('custrecord_ebiz_lpno');
				OpenTaskitemColumns[8] = new nlobjSearchColumn('custrecord_wms_location');
				var  k=0;var Recid;
				var veBizOrdNew;
				var OpenTaskitemSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskitemFilters, OpenTaskitemColumns);
				if(OpenTaskitemSearchResults!=null && OpenTaskitemSearchResults!='')
				{
					if(OpenTaskitemSearchResults[0].getValue('custrecord_ebiz_order_no') != null && OpenTaskitemSearchResults[0].getValue('custrecord_ebiz_order_no') != '')
					{	
						nlapiLogExecution('DEBUG', 'eBizOrdNoNew', OpenTaskitemSearchResults[0].getValue('custrecord_ebiz_order_no'));
						trantype = nlapiLookupField('transaction', OpenTaskitemSearchResults[0].getValue('custrecord_ebiz_order_no'), 'recordType');
					}
					nlapiLogExecution('DEBUG', 'trantype3', trantype);

					//creating the masterlp record with new containerLP
//					nlapiLogExecution('DEBUG', 'LP NOT FOUND');
//					var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
//					customrecord.setFieldValue('name', varEnteredLP);
//					customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', varEnteredLP);
//					var rec = nlapiSubmitRecord(customrecord, false, true);
					nlapiLogExecution('DEBUG','OpenTaskitemSearchResults.length', OpenTaskitemSearchResults.length);
					for(var i=0;i<OpenTaskitemSearchResults.length;i++)
					{
						iteminternalid=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_sku_no');
						getExpQty = OpenTaskitemSearchResults[i].getValue('custrecord_expe_qty'); //Assign the Enter Qty here imp

						Recid= OpenTaskitemSearchResults[i].getId();
						//getExpQty
						fetchQty =OpenTaskitemSearchResults[i].getValue('custrecord_act_qty');
						contlpno = OpenTaskitemSearchResults[i].getValue('custrecord_container_lp_no');
						wmslocation = OpenTaskitemSearchResults[i].getValue('custrecord_wms_location');
						opentaskwt	= OpenTaskitemSearchResults[i].getValue('custrecord_total_weight');
						opentasktotalcube = OpenTaskitemSearchResults[i].getValue('custrecord_totalcube');
						varname = OpenTaskitemSearchResults[i].getValue('name');
						var vOldContLp=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_lpno');


						var skuname = OpenTaskitemSearchResults[i].getText('custrecord_sku');

						nlapiLogExecution('DEBUG', 'skuname::', skuname);

						nlapiLogExecution('DEBUG', 'getExpQty::', getExpQty);
						nlapiLogExecution('DEBUG', 'fetchQty::', fetchQty);
						nlapiLogExecution('DEBUG', 'contlpno::', contlpno);
						nlapiLogExecution('DEBUG', 'wmslocation::', wmslocation);
						nlapiLogExecution('DEBUG', 'diffQty::', diffQty);					
//						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);
//						//transaction.setFieldValue('custrecord_totalcube',cubeinfo1);	
//						//transaction.setFieldValue('custrecord_total_weight',parseFloat(weightinfo1));				    	
//						transaction.setFieldValue('custrecord_wms_status_flag', '28');//28 stands for packcomplete //Added on 24th
//						transaction.setFieldValue('custrecord_container_lp_no', varEnteredLP);
//						transaction.setFieldValue('custrecord_expe_qty', parseFloat(fetchQty));
//						nlapiSubmitRecord(transaction, false, true);
						
						
//						var diffqty=parseFloat(getExpQty)-parseFloat(fetchQty);
//						nlapiLogExecution('DEBUG', 'diffqty', diffqty);
//						if(diffqty!='' && diffqty> 0)
//						{
//							var Remainqtyrecord =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	
//							Remainqtyrecord.setFieldValue('custrecord_act_qty', parseFloat(diffqty)); 
//							Remainqtyrecord.setFieldValue('custrecord_expe_qty', parseFloat(diffqty)); 
//							Remainqtyrecord.setFieldValue('custrecord_wms_status_flag', 8);
//							Remainqtyrecord.setFieldValue('name', varname);
//							Remainqtyrecord.setFieldValue('custrecord_pack_confirmed_date', null);
//							var rempick = nlapiSubmitRecord(Remainqtyrecord, false, true);
//						}
						var weight= request.getParameter('enterweight');
						if(i==0)
							var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	 	//for Assign Station purpose

						UpdateoutboundInventory(ContainerLPNo,varEnteredLP);
						//UpdateoutboundInventory(vOldContLp,varEnteredLP);//
						
						
						
						cubeinfo2 =  parseFloat(cubeinfo2)+ ((getExpQty * parseFloat(opentasktotalcube)) / fetchQty);
						nlapiLogExecution('DEBUG', 'cubeinfo2::', cubeinfo2);
						weightinfo2 = parseFloat(weightinfo2)+((getExpQty * parseFloat(opentaskwt)) / fetchQty);

					}
					if(varname != null && varname != '')
					{
						var ordernovalues=varname.split('.');
						var orderno=ordernovalues[0];
						var now = new Date();
						var date=now.getFullYear() +''+ (parseInt(now.getMonth()) + 1) +''+now.getDate();
						var sitetextvalue="";
						if(wmslocation != null && wmslocation != '')
						{
							var fields = ['name'];

							var locationcolumns = nlapiLookupField('location', wmslocation, fields);
							if(locationcolumns!=null)
								sitetext = locationcolumns.name;

							var sitetextvalue;
							if(sitetext=='Garden Grove')
								sitetextvalue=1;
							else
								sitetextvalue=2;
						}	
						var BOLSeqNo="";
						if(sitetextvalue !=null && sitetextvalue !='')
							BOLSeqNo=orderno+date+'-'+sitetextvalue;
						else
							BOLSeqNo=orderno+date;
						AssignPackStationrec.setFieldValue('custrecord_bol', BOLSeqNo);
					}

					AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
					AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
					AssignPackStationrec.setFieldValue('custrecord_container_lp_no', varEnteredLP);
					AssignPackStationrec.setFieldValue('custrecord_sku', null);
					AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
					AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);
					AssignPackStationrec.setFieldValue('name', varname);
					if(weight!=null && weight!='')
					{
						AssignPackStationrec.setFieldValue('custrecord_total_weight',parseFloat(weight).toFixed(4));
					}
					else
					{
						AssignPackStationrec.setFieldValue('custrecord_total_weight',parseFloat(weightinfo2).toFixed(4));
					}

					//custrecord_sku', null, 'anyof', iteminternalid);
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
					var assignpkrec = nlapiSubmitRecord(AssignPackStationrec, false, true);
					nlapiLogExecution('DEBUG', 'Before Assign station Record calling','Before Assign station Record calling');
					//CreateShippingManifestRecord(orderno,contlp);


					//creating ship manifest record if the carrier type is PC

					if(orderno!=null && orderno!='')
					{
						var vebizOrdNo=null;
						var filter=new Array();
						filter.push(new nlobjSearchFilter('tranid',null,'is',orderno));
						filter.push(new nlobjSearchFilter('mainline',null,'is','F'));

						var columns=new Array(); 		

						var searchrec=nlapiSearchRecord(trantype,null,filter,columns);

						if(searchrec!=null && searchrec!='' && searchrec.length>0)
						{
							vebizOrdNo=searchrec[0].getId();
						}
						nlapiLogExecution('DEBUG', 'vebizOrdNoe',vebizOrdNo);
						if(weight==null || weight=='')
						{
							weight=weightinfo2;
						}
						nlapiLogExecution('DEBUG', 'trantype4', trantype);
						CreateShippingManifestRecord(vebizOrdNo,varEnteredLP,null,null,weight);
						//Case# 20148118 starts update The Package Weight in shipmanifest record
						var shipfilters=new Array();
						if(orderno!='' && orderno!=null && orderno!='null')
						shipfilters.push(new nlobjSearchFilter('custrecord_ship_orderno',null,'is',orderno));
						if(contlp!='' && contlp!=null && contlp!='null')
						shipfilters.push(new nlobjSearchFilter('custrecord_ship_contlp',null,'is',contlp));
						var shipcolumns=new Array();
						shipcolumns[0]=new nlobjSearchColumn('internalid');
						var shiprefno=nlapiSearchRecord('customrecord_ship_manifest',null,shipfilters,shipcolumns);
						if(shiprefno!=null && shiprefno!='')
						{
							var shipid=shiprefno[0].getValue('internalid');
							var shiptran = nlapiLoadRecord('customrecord_ship_manifest', shipid);
							shiptran.setFieldValue('custrecord_ship_pkgwght',weight);	
							var invtrecid = nlapiSubmitRecord(shiptran, false, true);
							nlapiLogExecution('DEBUG', 'update The Package Weight ',weight);
						}
						// Case# 20148118 ends
						if(vebizOrdNo!=null && vebizOrdNo!='')
						{
							var vOrdAutoPackFlag="";
							var vStgAutoPackFlag="";
							var vStgCarrierType="";
							var vCarrierType="";
							var vStgCarrierName="";
							var vCarrier="";
							var vordtype='';
							var whlocation='';
							var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vebizOrdNo,trantype);
							if(vOrderAutoPackFlag!=null && vOrderAutoPackFlag.length>0){
								vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
								vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
								vCarrier = vOrderAutoPackFlag[0].getText('custbody_salesorder_carrier');
								vordtype = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
								whlocation = vOrderAutoPackFlag[0].getValue('location');
								nlapiLogExecution('DEBUG', 'Order Type',vordtype);
								nlapiLogExecution('DEBUG', 'WH Location',whlocation);
								nlapiLogExecution('DEBUG', 'AutoPackFlag @ Order Type',vOrdAutoPackFlag);
								nlapiLogExecution('DEBUG', 'Carrier Type @ Order Type',vCarrierType);
								nlapiLogExecution('DEBUG', 'Carrier @ Order Type',vCarrier);
							}

							if(vOrdAutoPackFlag!='T' || (vCarrier=="" ||vCarrier==null)){
								var vStageAutoPackFlag = getAutoPackFlagforStage(whlocation,vordtype);
								if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
									vStgCarrierType = vStageAutoPackFlag[0][0];
									vStgAutoPackFlag = vStageAutoPackFlag[0][1];	 
									vStgCarrierName = vStageAutoPackFlag[0][2];
									nlapiLogExecution('DEBUG', 'AutoPackFlag(Stage)',vStgAutoPackFlag);
									nlapiLogExecution('DEBUG', 'Carrier Type(Stage)',vStgCarrierType);
									nlapiLogExecution('DEBUG', 'Carrier(Stage)',vStgCarrierName);
								}
							}

							if((vCarrier=="" || vCarrier==null) && (vStgCarrierName!="" && vStgCarrierName!=null))
								vCarrier=vStgCarrierName;

							if((vCarrierType=="" || vCarrierType==null) && (vStgCarrierType!="" && vStgCarrierType!=null))
								vCarrierType=vStgCarrierType;

							nlapiLogExecution('DEBUG', 'Carrier', vCarrierType);

							if(vCarrierType=="PC")
							{
								//CreateShippingManifestRecord(vebizOrdNo,varEnteredLP,vCarrier);
							}
						}

					}
				}
				else
				{
					nlapiLogExecution('DEBUG', 'test1', 'test1');
					SOarray["custparam_screenno"] = 'Pack5';
					SOarray["custparam_error"] =   st9 + varEnteredLP+", " + st10;
					SOarray["custparam_orderno"] = orderno;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				
				
				if(vebizOrdNo!=null && vebizOrdNo !='')
				{
					var packfilters = new Array();
					packfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',vebizOrdNo));
					packfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['13','14']));//13 - STGM, 14 - PACK

					var packcolumns= new Array();
					packcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
					packcolumns[1] = new nlobjSearchColumn('custrecord_tasktype');

					var packtasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,packfilters ,packcolumns);
					if(packtasksearchresults !=null && packtasksearchresults !='')
					{
						nlapiLogExecution('ERROR', 'packtasksearchresults length2',packtasksearchresults.length);

						for (var k = 0; k < packtasksearchresults.length; k++) 
						{

							var vTaskType = packtasksearchresults[k].getValue('custrecord_tasktype');
							var vCartonNo = packtasksearchresults[k].getValue('custrecord_container_lp_no');
							var vTaskId = packtasksearchresults[k].getId();

							if(vTaskType=='13')
							{
								var fieldStgm= new Array(); 
								fieldStgm.push('custrecord_container_lp_no');  

								var newValuesStgm = new Array(); 
								newValuesStgm.push(varEnteredLP);

								nlapiSubmitField('customrecord_ebiznet_trn_opentask', vTaskId, fieldStgm, newValuesStgm);
								nlapiLogExecution('ERROR', 'STGM Updated');
							}

							nlapiLogExecution('DEBUG', 'vCartonNo', vCartonNo);
							nlapiLogExecution('DEBUG', 'contlp', contlp);
							
							
							if(vTaskType=='14' && vCartonNo==contlp)
							{
								var fieldPACK= new Array(); 
								fieldPACK.push('custrecord_device_upload_flag');  

								var newValuesPACK = new Array(); 
								newValuesPACK.push('T');

								nlapiSubmitField('customrecord_ebiznet_trn_opentask', vTaskId, fieldPACK, newValuesPACK);
								nlapiLogExecution('ERROR', 'PACK Updated');
							}
						}
					}
				}

				nlapiLogExecution('DEBUG', 'orderno', orderno);
				var OpenTaskpickFilters = new Array();	
				if(orderno!=null && orderno!='')	    	 	
					OpenTaskpickFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', orderno));
		    	 	//OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', varEnteredLP));   	 	
				OpenTaskpickFilters.push(new nlobjSearchFilter('custrecord_pack_confirmed_date',null, 'isempty', null));
				OpenTaskpickFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//pick confirmed
				OpenTaskpickFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pick
				OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

				var OpenTaskpicksSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskpickFilters, null);

			//	var SOarray = new Array();
				SOarray["custparam_error"] = st11;
				SOarray["custparam_screenno"] = 'Pack5';
				SOarray["custparam_orderno"] = request.getParameter('hdnOrderNo');
				SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
				SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
				SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
				SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
				SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
				SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
				SOarray['custparam_linecount'] = '1';
				SOarray['custparam_loopcount'] ='1';
				//SOarray['custparam_language']=request.getParameter('hdngetLanguage');
				nlapiLogExecution('DEBUG', 'custparam_language', SOarray['custparam_language']);


				if(OpenTaskpicksSearchResults==null || OpenTaskpicksSearchResults=='')
				{


					/*SOarray["custparam_screenno"] = 'Pack6';
					SOarray["custparam_error"] =  "Order packing completed";
					SOarray["custparam_orderno"] = orderno;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);*/
					//case 20125944 start : added false in below li
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanorder', 'customdeploy_ebiz_rf_pack_scanorder_di',false,SOarray);
					return;
					//case 20125944 start :
				}

				nlapiLogExecution('DEBUG', 'loop count', SOarray['custparam_loopcount']);
				nlapiLogExecution('DEBUG', 'line count', SOarray['custparam_linecount']);
				nlapiLogExecution('DEBUG', 'Order', orderno);

				//response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);


						nlapiLogExecution('DEBUG', 'Before Passing to item from close button', 'Before Passing to item from close button');
						//response.sendRedirect('SUITELET', 'customscript_rf_packing_item', 'customdeploy_rf_packing_item_di', false, SOarray);
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanorder', 'customdeploy_ebiz_rf_pack_scanorder_di',false,SOarray);
						nlapiLogExecution('DEBUG', 'After Passing to item from close button', 'After Passing to item from close button');
					}
				}
			}
			catch (e)  {
				nlapiLogExecution('DEBUG', 'error message',e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			SOarray["custparam_screenno"] = 'Pack1';
			SOarray["custparam_error"] = 'CARTON# ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}

	}
}
function DataRec(vCompId, vSiteId, vEbizOrdNo,vOrdNo,vContainer,vPackStation,vEbizControlNo,vItem) 
{
	if(vCompId != null && vCompId != '')
		this.DataRecCompId = vCompId;
	else
		this.DataRecCompId=null;

	if(vSiteId != null && vSiteId != '')
		this.DataRecSiteId = vSiteId;
	else
		this.DataRecSiteId=null;
	this.DataRecEbizOrdNo = vEbizOrdNo;
	this.DataRecOrdNo = vOrdNo;
	this.DataRecContainer = vContainer;
	this.DataRecPackStation = vPackStation;
	this.DataRecEbizControlNo = vEbizControlNo;
	this.DataRecItem = vItem;	
}	
function createpacklisthtml(vebizOrdNo,fulfillmentnumber,printername,orderno)
{
	try
	{	
		nlapiLogExecution('DEBUG', 'createpackcodehtml ', 'createpackcodehtml');
		nlapiLogExecution('DEBUG', 'createpackcodehtmlvebizOrdNo ', vebizOrdNo);
		nlapiLogExecution('DEBUG', 'fulfillmentnumber ', fulfillmentnumber);
		var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
		nlapiLogExecution('DEBUG', 'trantype', trantype);

		var salesorderdetails =nlapiLoadRecord(trantype, vebizOrdNo);
		var billtoaddress=salesorderdetails.getFieldValue('billaddress'); 
		var shipaddress=salesorderdetails.getFieldValue('shipaddress');
		var locationId =salesorderdetails.getFieldValue('location');
		var expdate=salesorderdetails.getFieldValue('custbody_nswmspoexpshipdate');
		var orderdate=salesorderdetails.getFieldValue('trandate');
		var ordernumber=salesorderdetails.getFieldValue('tranid');
		var terms=salesorderdetails.getFieldText('terms');
		var customerpo=salesorderdetails.getFieldValue('otherrefnum');
		var entity=salesorderdetails.getFieldText('entity');
		if((expdate==null)||(expdate==''))
		{
			expdate="";
		}
		if((orderdate==null)||(orderdate==''))
		{
			orderdate="";
		}
		if((expdate==null)||(expdate==''))
		{
			expdate="";
		}
		if((customerpo==null)||(customerpo==''))
		{
			customerpo="";
		}
		if((terms==null)||(terms==''))
		{
			terms="";
		}

		var billaddressee="";var billaddr1="";var billcity="";var billcountry="";var billstate="";var billzip="";var billstateandcountry="";
		billaddressee=salesorderdetails.getFieldValue('billaddressee');
		billaddr1=salesorderdetails.getFieldValue('billaddr1');
		billcity=salesorderdetails.getFieldValue('billcity');
		billcountry=salesorderdetails.getFieldValue('billcountry');
		billstate=salesorderdetails.getFieldValue('billstate');
		billzip=salesorderdetails.getFieldValue('billzip');
		billstateandcountry=billcity+","+billstate+","+billzip;
		var shipaddressee="";var shipaddr1="";var shipcity="";var shipcountry="";var shipstate="";var shipzip="";var shipstateandcountry="";
		shipaddressee=salesorderdetails.getFieldValue('shipaddressee'); 
		shipaddr1=salesorderdetails.getFieldValue('shipaddr1'); 
		shipcity=salesorderdetails.getFieldValue('shipcity'); 
		shipcountry=salesorderdetails.getFieldValue('shipcountry'); 
		shipstate=salesorderdetails.getFieldValue('shipstate'); 
		shipzip=salesorderdetails.getFieldValue('shipzip'); 
		shipstateandcountry=shipcity+","+shipstate+","+shipzip;

		var shipmethod=salesorderdetails.getFieldText('shipmethod');
		var totalLine=salesorderdetails.getLineItemCount('item');
		var totalcube=0;
		var totalweight=0;
		for(var j=1;j<= parseFloat(totalLine);j++)
		{
			var item =salesorderdetails.getLineItemValue('item','item',j);
			var vUOM="";
			if (vUOM == "") {
				vUOM =1;
			}

			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', item));
			filters.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', vUOM));
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
			columns[1] = new nlobjSearchColumn('custrecord_ebiztareweight');
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

			for (var i = 0; searchresults != null && i < searchresults.length; i++)
			{
				var searchresult = searchresults[i];
				var vCube = searchresult.getValue('custrecord_ebizcube');
				totalcube=parseFloat(totalcube)+parseFloat(vCube);
				var vweight = searchresult.getText('custrecord_ebiztareweight');


				totalweight=parseFloat(totalweight)+parseFloat(vweight);
			}
		}
		if((totalweight.toString()=="NaN")||(totalweight==''))
		{
			totalweight='';
		}

		nlapiLogExecution('DEBUG', 'totalcube ', totalcube);
		nlapiLogExecution('DEBUG', 'totalweight ', totalweight);
		nlapiLogExecution('DEBUG', 'location ', locationId);
		var locationadress =nlapiLoadRecord('Location',locationId);

		var addr1="";var city="";var state="";var zip=""; var stateandzip=""; var returnadresse="";
		addr1=locationadress.getFieldValue('addr1');
		city=locationadress.getFieldValue('city');
		state=locationadress.getFieldValue('state');
		zip=locationadress.getFieldValue('zip');
		returnadresse=locationadress.getFieldValue('addressee');
		stateandzip=city+","+state+","+zip;

		var groupopentaskfilterarray = new Array();
		//Please uncheck  the fillementnumber code 
		groupopentaskfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',vebizOrdNo));
		groupopentaskfilterarray.push(new nlobjSearchFilter('name', null, 'is',fulfillmentnumber.toString()));
		groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3])); 

		var groupopentaskcolumnarray = new Array();
		groupopentaskcolumnarray[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
		groupopentaskcolumnarray[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
		groupopentaskcolumnarray[2] = new nlobjSearchColumn('custrecord_line_no', null, 'group');
		groupopentaskcolumnarray[3] = new nlobjSearchColumn('custrecord_act_end_date', null, 'group');

		groupopentaskcolumnarray[4] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');	

		var groupopentasksearchresults  = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, groupopentaskfilterarray, groupopentaskcolumnarray);
		var appendcontlp="";
		var actualenddate="";
		var strVar="";
		nlapiLogExecution('DEBUG', 'groupopentasksearchresults.length ', groupopentasksearchresults.length);
		if(groupopentasksearchresults.length!=null)
		{
			for(var g=0; g<groupopentasksearchresults.length;g++)
			{
				var repeatcontlp;
				var vContainerLpNo = groupopentasksearchresults [g].getValue('custrecord_container_lp_no', null, 'group');
				actualenddate = groupopentasksearchresults [g].getValue('custrecord_act_end_date', null, 'group');
				if(repeatcontlp!=vContainerLpNo)
				{ 
					if((vContainerLpNo!="")&&(vContainerLpNo!=null)&&(vContainerLpNo!="- None -"))
						appendcontlp +=vContainerLpNo +","; 
				}
				repeatcontlp=vContainerLpNo;
			}
			var totalamount='';
			var groupcount=groupopentasksearchresults.length;
			var grouplength=0;
			var invoicetasklength=groupopentasksearchresults.length;
			var linenumber=1;
			var pagecount=1;

			var totalinvoice=0;

			var totalcount=groupopentasksearchresults.length;
			var totalshipqty=0;
			while(0<totalcount)
			{
				//for(var h=0; h<totalcount;h++)
				//{
				var count=0;
				strVar +="<html>";
				strVar +="<body style=\"padding-right:0px; padding-left:0px;padding-top:0px;padding-bottom:0px;\">";
				strVar += "<table width=\"95%\" style=\"height:100%;\">";
				strVar += "<tr style=\"vertical-align:top\">";
				strVar += "                <td align=\"left\" style=\"width: 40%; font-size: 12px; vertical-align: top;\">";
				strVar += "                    <img src=\"headerimage\"\/><br \/>";
				strVar += "                    Ph. Inquiries:1-800-811-9342<br \/>";
				strVar += "                    Fax Inquiries:1-800-643-0639";
				strVar += "                <\/td>";
				strVar += "                <td align=\"left\" style=\"font-size: 12px; width: 20%; \">";
				strVar += "                    ***PACKING SLIP ***";
				strVar += "                <\/td>";
				strVar += "                <td align=\"right\" style=\"width: 40%\">";
				strVar += "                    <b>PACKING SLIP<\/b><br \/>";
				strVar += "                    <table border=\"0\" width=\"250px\" frame=\"box\" rules=\"all\" cellpadding=\"2\" cellspacing=\"0\" style=\"vertical-align: top;\">";
				strVar += "                        <tr>";
				strVar += "                              <td style=\"visibility: hidden; border: none;\">";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; background-color: Gray; border-top: 1px solid black;";
				strVar += "                                border-left: 1px solid black; border-bottom: 1px solid black;\">";
				strVar += "                                Taken By";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-top: 1px solid black; border-right: 1px solid black;";
				strVar += "                                border-left: 1px solid black; background-color: Gray; border-bottom: 1px solid black;\">";
				strVar += "                                ORDER #";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"visibility: hidden; border: none;\">";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-right: 1px solid black;\">";
				strVar += "                                maje";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black;\">";
				strVar += "                              "+fulfillmentnumber+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr style=\"background-color: Gray;\">";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;\">";
				strVar += "                                ORDER DATE";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;\">";
				strVar += "                                CUSTOMER PO#";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;";
				strVar += "                                border-right: 1px solid black;\">";
				strVar += "                                PAGE";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 12px; border-left: 1px solid black; border-top: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; text-align: center;\">";
				strVar += "                                "+orderdate+"";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-left: 1px solid black; border-top: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; text-align: center;\">";
				strVar += "                                "+customerpo+"";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; border-top: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;\">";
				strVar += "                                "+pagecount+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "            <\/tr>";
				strVar += "            <tr style=\"vertical-align:top\">";
				strVar += "                <td>";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 10px; white-space: nowrap; vertical-align: top;\">";
				strVar += "                                &nbsp;&nbsp;&nbsp;BILL TO:";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 14px;\">";

				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+billaddressee+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+billaddr1+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";

				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+billstateandcountry+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "                <td style=\"font-size: 10px; vertical-align: top;\">";
				strVar += "                    CUST #:&nbsp;&nbsp;"+entity+"";
				strVar += "                <\/td>";
				strVar += "                <td>";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 9px; vertical-align: top;\">";
				strVar += "                                RETURNS TO:";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+returnadresse+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";	
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+addr1+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+stateandzip+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "            <\/tr>";
				strVar += "            <tr style=\"vertical-align:top\">";
				strVar += "                <td>";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 10px; white-space: nowrap; vertical-align: top;\">";
				strVar += "                                &nbsp;&nbsp;&nbsp;SHIP TO:";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                    <table>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+shipaddressee+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+shipaddr1+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px;\">";
				strVar += "                                &nbsp;"+shipstateandcountry+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "                <td>";
				strVar += "                <\/td>";
				strVar += "                <td style=\"vertical-align: top;\">";
				strVar += "                    <table  style=\"border: 1px solid black;\" frame=\"box\" rules=\"all\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">";
				strVar += "                        <tr style=\"background-color: Gray;\">";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black;width:50%; border-bottom: 1px solid black;\"";
				strVar += "                                   colspan=\"2\">";
				strVar += "                                SHIP POINT";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; width:50% border-bottom: 1px solid black;\"";
				strVar += "                                colspan=\"2\">";
				strVar += "                                INSTRUCTIONS";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black;\" colspan=\"2\">";
				strVar += "                                Girl Scouts of the usa";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black;\" colspan=\"2\">";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr style=\"background-color: Gray;\">";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                                SHIP VIA";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                                REQUEST DATE";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                                PICKED";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-bottom: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                                TERMS";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                               "+shipmethod+" ";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                               "+expdate+"";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-right: 1px solid black; text-align: center; border-top: 1px solid black;\">";
				strVar += "                               "+actualenddate+"";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-top: 1px solid black;\">";
				strVar += "                             "+terms+"";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "            <\/tr>";
				strVar += "<tr style=\"vertical-align:top\">";
				strVar += "<td colspan=\"3\">";
				strVar +="<table>";
				strVar +="<tr>";
				strVar +="<td  style=\"font-size: 12px;\" colspan=\"3\">";
				strVar +="LEAST EXPENSIVE SHIPPING METHOD/GSM TO DECIDE(RECOMENDED)EFFECTIVE 10/21/09";
				strVar +="<\/td>";
				strVar +="<\/tr>";
				strVar +="<tr>";
				strVar += "                <td colspan=\"3\">";
				strVar += "                    <table width=\"100%\" border=\"0\" frame=\"box\" rules=\"all\" cellpadding=\"2\" cellspacing=\"0\">";
				strVar += "                        <tr style=\"background-color: Gray;\">";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-left: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; border-right: 1px solid black; text-align: center;\">";
				strVar += "                                Line No";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                PRODUCT AND DESCRIPTION";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                QUANTITY ORDERED";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                QUANTITY SHIPPED";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                QUANTITY B.O.";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                SUGGESTED RETAIL";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                UNIT PRICE";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                DISCOUNT %";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                NET UNIT PRICE";
				strVar += "                            <\/td>";
				strVar += "                            <td style=\"font-size: 12px; border-top: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                border-right: 1px solid black; text-align: center;\">";
				strVar += "                                AMOUNT (NET)";
				strVar += "                            <\/td>";
				strVar += "                        <\/tr>";


				if(pagecount==1)
				{
					strVar += "                        <tr style=\"line-height:10px;\">";
					strVar += "                            <td style=\"border-left: 1px solid black; border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"font-size: 12px; white-space: nowrap; border-right: 1px solid black;\">";
					strVar += "                                This Order Is Contained in the Following Carton(s):";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                        <\/tr>";	
					strVar += "                        <tr style=\"background-color: Gray;line-height:10px;\">";
					strVar += "                            <td style=\"border-left: 1px solid black; border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"font-size: 12px;  white-space: nowrap; border-right: 1px solid black;\">";
					strVar += "                                "+appendcontlp+"";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                            <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                            <\/td>";
					strVar += "                        <\/tr>";

				}



				for(var g=grouplength; g<groupopentasksearchresults.length;g++)
				{
					count++;
					grouplength++;
					var vContainerLpNo = groupopentasksearchresults [g].getValue('custrecord_container_lp_no', null, 'group');
					var itemText = groupopentasksearchresults [g].getText('custrecord_sku', null, 'group');
					var lineno = groupopentasksearchresults [g].getValue('custrecord_line_no', null, 'group');
					var totalqty = groupopentasksearchresults [g].getValue('custrecord_expe_qty', null, 'sum');
					totalshipqty =parseInt (totalshipqty)+parseInt (totalqty);
					nlapiLogExecution('DEBUG', 'totalqty ', totalqty);
					var unitvalue,backordervalue,decscription;
					nlapiLogExecution('DEBUG', 'lineNum ', totalLine);
					nlapiLogExecution('DEBUG', 'groupopentasklineno ', lineno);
					unitvalue=salesorderdetails.getLineItemValue('item','amount',lineno);
					backordervalue=salesorderdetails.getLineItemValue('item','backordered',lineno);
					decscription=salesorderdetails.getLineItemValue('item','description',lineno); 
					var totalamount=parseFloat(totalqty)*parseFloat(unitvalue);
					totalinvoice=parseFloat(totalinvoice)+parseFloat(totalamount);
					nlapiLogExecution('DEBUG', 'totalinvoice ', totalinvoice);
					var backgroundcolor="";
					if(pagecount==1)
					{
						backgroundcolor="Gray";

					}
					else
					{
						backgroundcolor="white";

					}

					strVar += "                            <tr style=\"background-color: ;line-height:10px;\">";
					strVar += "                                <td style=\"font-size: 12px;border-left: 1px solid black; border-right: 1px solid black; font-size: 10px;\">";
					strVar += "                                  "+linenumber+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px; white-space: nowrap; border-right: 1px solid black;\">";
					strVar += "                                   "+itemText+"  "+decscription+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                            <\/tr>";
					strVar += "                            <tr style=\"line-height:10px;\">";
					strVar += "                                <td style=\"border-left: 1px solid black; border-right: 1px solid black;\">&nbsp;";

					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px; white-space: nowrap; border-right: 1px solid black;\">";
					strVar += "                                  "+vContainerLpNo+" "+totalqty+"Each";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px; text-align:right ;border-right: 1px solid black;\">";
					strVar += "                                 "+totalqty+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+totalqty+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+backordervalue+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"border-right: 1px solid black;\">&nbsp;";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+unitvalue+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 0.00";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+totalamount+"";
					strVar += "                                <\/td>";
					strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
					strVar += "                                 "+totalamount+"";
					strVar += "                                <\/td>";
					strVar += "                            <\/tr>";

					if(g==groupopentasksearchresults.length-1)
					{
						strVar += "<tr style=\"line-height:10px;\">";
						strVar += "                                <td style=\"border-left: 1px solid black; text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ; white-space: nowrap; border-right: 1px solid black;\">";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                  Total";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                 "+totalinvoice+"";
						strVar += "                                <\/td>";

						strVar += "<\/tr>";

						strVar += "<tr style=\"line-height:10px;\">";
						strVar += "                                <td style=\"border-left: 1px solid black;text-align:right ; border-right: 1px solid black;\">&nbsp;";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px; white-space: nowrap;text-align:right ; border-right: 1px solid black;\">";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";

						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                  Invoice";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                  Total";
						strVar += "                                <\/td>";
						strVar += "                                <td style=\"font-size: 12px;text-align:right ;border-right: 1px solid black;\">&nbsp;";
						strVar += "                                 $"+totalinvoice+"";
						strVar += "                                <\/td>";
						strVar += "<\/tr>";
					}
					linenumber++;


					if(pagecount==1)
					{
						if(count==6)
						{
							break;
						}
					}
					else
					{
						if(count==7)
						{
							break;
						}

					}

				}
				var totalshipped='';
				var totalshipcube='';
				var totalshipweight='';
				var totallines='';
				if(grouplength==groupopentasksearchresults.length)
				{
					totallines=linenumber-1;
					totalshipped=totalshipqty;
					totalshipcube=totalcube;
					totalshipweight=totalweight;
				}
				else
				{ 
					totallines='';
					totalshipcube='';
					totalshipweight='';
					totalshipped='';
				}


				strVar += "                        <tfoot style=\"border-collapse:collapse\">";
				strVar += "                            <tr>";
				strVar += "                                <td colspan=\"10\" style=\"border-top: none;border-top: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;";
				strVar += "                                    border-bottom: 1px solid black; font-size: 10px\">";
				strVar += "                                    Orders received should be checked against packing list<br \/>";
				strVar += "                                   All claims against this order must be filed within 30days.Inquiries should be submitted ";
				strVar += "                                   either by phone or fax using the telephone numbers shown above";
				strVar += "                                    <br \/>";
				strVar += "                                    GSUSA Open Account customers will receive their invoice separately .The Merchandise";
				strVar += "                                    Total above does not include Freight,Handling or Tax<br \/>";
				strVar += "                                   GSUSA Cash and Credit Card customers are on automatic back order;merchandise will";
				strVar += "                                      be shipped when available.All applicable refunds will be mailed under separate";
				strVar += "                                    cover.Please allow 4-6 weeks from ship date.When returning <br \/>";
				strVar += "                                   merchandise ,a copy of original packing slip should be enclosed and should be sent";
				strVar += "                                  to the address shown above,Randolph,NJ ";
				strVar += "                                <\/td>";
				strVar += "                            <\/tr>";
				strVar += "                            <tr>";
				strVar += "                                <td colspan=\"10\">";
				strVar += "                                    <table width=\"100%\" cellpadding=\"2\" frame=\"box\" rules=\"all\" cellspacing=\"0\">";
				strVar += "                                        <tr>";
				strVar += "                                            <td style=\" border-right: 1px solid black; border-left: 1px solid black; border-bottom: 1px solid black;font-size: 12px;\">";
				strVar += "                                               "+totallines+"&nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                LINES TOTAL";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                # OF LINES NOT PRINTED";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                0";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                QTY SHIPPED TOTAL";
				strVar += "                                                <img src=\"\" height=\"2px\" \/>";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                               "+totalshipped+"&nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\" border-bottom: 1px solid black;border-right: 1px solid black; font-size: 12px;\"";
				strVar += "                                                colspan=\"4\">";
				strVar += "                                                No.OF CARTONS";
				strVar += "                                            <\/td>";
				strVar += "                                        <\/tr>";
				strVar += "                                        <tr style=\"background-color: Gray; font-size: 11px;\">";
				strVar += "                                            <td style=\" border-right: 1px solid black; border-bottom: 1px solid black;font-size: 12px;\">";

				strVar += "                                                PICKED BY";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;border-left: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                PACKED BY";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                CHECKED BY";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                CUBE";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                WEIGHT";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                FREIGHT CHARGE";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                RECEIVED BY";
				strVar += "                                            <\/td>";
				strVar += "                                            <td colspan=\"2\" style=\" border-right: 1px solid black;border-bottom: 1px solid black;";
				strVar += "                                                font-size: 12px;\">";
				strVar += "                                                DATE RECEIVED";
				strVar += "                                            <\/td>";
				strVar += "                                        <\/tr>";
				strVar += "                                        <tr>";
				strVar += "<td style=\"border-right: 1px solid black; border-bottom: 1px solid black;  border-left: 1px solid black;font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                               "+totalshipcube+" &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                               "+totalshipweight+"&nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td style=\"border-right: 1px solid black;  border-bottom: 1px solid black; font-size: 12px;\">";
				strVar += "                                                &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                            <td colspan=\"2\" style=\" border-bottom: 1px solid black;border-right: 1px solid black;font-size: 12px;\"> &nbsp;";
				strVar += "                                            <\/td>";
				strVar += "                                        <\/tr>";
				strVar += "                                    <\/table>";
				strVar += "                                <\/td>";
				strVar += "                            <\/tr>";
				strVar += "                        <\/tfoot>";
				strVar += "                    <\/table>";
				strVar +="<\/td>";
				strVar +="<\/tr>";
				strVar += "                    <\/table>";
				strVar += "                <\/td>";
				strVar += "            <\/tr>";
				strVar += "        <\/table>";
				strVar += " </body>";
				strVar += " </html>";
				if(grouplength==groupopentasksearchresults.length)
				{
					strVar +="<p style=\" page-break-after:avoid\"></p>";
				}
				else
				{
					strVar +="<p style=\" page-break-after:always\"></p>";
				}

				pagecount++;	 
				nlapiLogExecution('DEBUG', 'totalcount', totalcount);
				totalcount=parseFloat(totalcount)-parseFloat(count);
				nlapiLogExecution('DEBUG', 'totalcountafter', totalcount);
			}
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Recordsnotfound', 'Recordsnotfound');
		}
		var tasktype='14';
		var labeltype='PackList';
		var print='F';
		var reprint='F';
		var company='';
		var location='';
		var formattype='html';
		var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
		labelrecord.setFieldValue('name', fulfillmentnumber); 
		labelrecord.setFieldValue('custrecord_labeldata',strVar);  
		labelrecord.setFieldValue('custrecord_label_refno',fulfillmentnumber);     
		labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);//chkn task   
		labelrecord.setFieldValue('custrecord_labeltype',labeltype);    
		labelrecord.setFieldValue('custrecord_label_lp',fulfillmentnumber);

		labelrecord.setFieldValue('custrecord_label_print', print);
		labelrecord.setFieldValue('custrecord_label_reprint', reprint);
		labelrecord.setFieldValue('custrecord_label_company', company);
		labelrecord.setFieldValue('custrecord_label_location', location);
		labelrecord.setFieldValue('custrecord_label_printername',printername);

		//nlapiLogExecution('DEBUG', 'htmlstring ', strVar);

		var tranid = nlapiSubmitRecord(labelrecord);

		if(tranid!=null && tranid!='')
		{
			var getCartonLPNo = request.getParameter('custparam_cartonno');
			nlapiLogExecution('DEBUG', 'getCartLPNo', getCartonLPNo);
			var getFONO= request.getParameter('custparam_fono');
			nlapiLogExecution('DEBUG', 'getFONO', getFONO);


//			var POarray=new Array();
//			POarray["custparam_screenno"] = 'Pack5';
//			POarray["custparam_printername"] = printername;
//			POarray["custparam_error"] =  "Packing List Printed Sucessfully for this  "+orderno+"";
//			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			var POarray=new Array();
			POarray["custparam_orderno"] = orderno;	
			//20125893
			var getLanguage =  request.getParameter('hdngetLanguage');
			POarray["custparam_language"]=getLanguage;
		
			response.sendRedirect('SUITELET', 'customscript_rf_packing_complete', 'customdeploy_rf_packing_complete_di', false, POarray);
		}

	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'exceptionhtmlpackcode ', exp);

	}
}
function AutoPacking(vebizOrdNo,whlocation){	
	nlapiLogExecution('DEBUG', 'into AutoPacking (SO Internal Id)', vebizOrdNo);
	nlapiLogExecution('DEBUG', 'into AutoPacking (WH Location)', whlocation);
	if(vebizOrdNo!=null && vebizOrdNo!='')
	{
		nlapiLogExecution('DEBUG', 'into AutoPacking vebizOrdNo is not null', vebizOrdNo);
		
		var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
		
		var vOrdAutoPackFlag="";
		var vStgAutoPackFlag="";
		var vStgCarrierType="";
		var vCarrierType="";
		var vStgCarrierName="";
		var vorderType='';
		var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vebizOrdNo,trantype);
		if(vOrderAutoPackFlag!=null&& vOrderAutoPackFlag.length>0){
			vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
			vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
			vorderType = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
			nlapiLogExecution('DEBUG', 'Order Type',vOrdAutoPackFlag);
			nlapiLogExecution('DEBUG', 'AutoPackFlag @ Order Type',vOrdAutoPackFlag);
			nlapiLogExecution('DEBUG', 'Carrier @ Order Type',vCarrierType);
		}

		if(vOrdAutoPackFlag!='T' || (vCarrierType=="" || vCarrierType==null)){
			var vStageAutoPackFlag = getAutoPackFlagforStage(whlocation,vorderType);
			if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
				vStgCarrierType = vStageAutoPackFlag[0][0];
				vStgAutoPackFlag = vStageAutoPackFlag[0][1];
				vStgCarrierName = vStageAutoPackFlag[0][2];
				nlapiLogExecution('DEBUG', 'AutoPackFlag(Stage)',vStgAutoPackFlag);
				nlapiLogExecution('DEBUG', 'Carrier(Stage)',vStgCarrierType);
			}
		}

		if(vCarrierType=="" && vStgCarrierType!="")
			vCarrierType=vStgCarrierType;

		nlapiLogExecution('DEBUG', 'Carrier', vCarrierType);

		if(vOrdAutoPackFlag=='T' || vStgAutoPackFlag=='T'){
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage at the start of UpdatesInOpenTask ',context.getRemainingUsage());
			UpdatesInOpenTask(vebizOrdNo,vCarrierType,vStgCarrierName);
		}
	}
	nlapiLogExecution('DEBUG', 'out of AutoPacking');
}
function getAutoPackFlagforOrdType(vebizOrdNo,trantype){
	nlapiLogExecution('DEBUG', 'into getAutoPackFlagforOrdType eBizOrdNo',vebizOrdNo);
	nlapiLogExecution('DEBUG', 'trantype5',trantype);
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
	columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	columns[2] = new nlobjSearchColumn('custbody_nswmssoordertype');
	searchresults = nlapiSearchRecord(trantype, null, filters, columns);
//	var	searchresults = nlapiLoadRecord('salesorder', vebizOrdNo);
	nlapiLogExecution('DEBUG', 'out of getAutoPackFlagforOrdType', searchresults);
	return searchresults;

}
function getAutoPackFlagforStage(whlocation,vorderType){
	nlapiLogExecution('DEBUG', 'into getAutoPackFlagforStage');
	nlapiLogExecution('DEBUG', 'whlocation',whlocation);
	nlapiLogExecution('DEBUG', 'vorderType',vorderType);
	var vStgRule = new Array();
	//getStageRule(Item, vCarrier, vSite, vCompany, stgDirection,ordertype)
	vStgRule = getStageRule('', '', whlocation, '', 'OUB',vorderType);
	nlapiLogExecution('DEBUG', 'Stage Rule', vStgRule);

	nlapiLogExecution('DEBUG', 'out of getAutoPackFlagforStage');
	return vStgRule;
}
function UpdatesInOpenTask(vebizOrdNo,vCarrierType,CarrieerName){
	nlapiLogExecution('DEBUG', 'into UpdatesInOpenTask function', vebizOrdNo);
	nlapiLogExecution('DEBUG', 'CarrierType', vCarrierType);
	var packstation = getPackStation();
	var DataRecArr=new Array();
	var containerlpArray=new Array();
	var ebizorderno;
	var filters = new Array();
	nlapiLogExecution('DEBUG','orderno in getSalesOrderlist',vebizOrdNo);

	if(vebizOrdNo!=""){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vebizOrdNo));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	//filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLp));//Task Type - PICK

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_comp_id');
	columns[5] = new nlobjSearchColumn('custrecord_site_id');
	columns[6] = new nlobjSearchColumn('custrecord_container');
	columns[7] = new nlobjSearchColumn('custrecord_total_weight');
	columns[8] = new nlobjSearchColumn('custrecord_sku');

	//columns[0].setSort();
	columns[1].setSort();
	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	if(salesOrderList != null && salesOrderList.length > 0){
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item;	
		for (var i = 0; i < salesOrderList.length; i++){
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage in the for loop starting ',context.getRemainingUsage());
			orderno = salesOrderList[i].getValue('name');
			containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
			ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
			ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
			compid = salesOrderList[i].getValue('custrecord_comp_id');
			siteid = salesOrderList[i].getValue('custrecord_site_id');
			containersize = salesOrderList[i].getValue('custrecord_container');
			totweight = salesOrderList[i].getValue('custrecord_total_weight');
			item = salesOrderList[i].getValue('custrecord_sku');//modified on dec 12

			internalid = salesOrderList[i].getId();
			containerlpArray[i]=containerno;
			DataRecArr.push(new DataRec(compid,siteid,ebizorderno,orderno,containerno,packstation,ebizcntrlno,item));
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage Before updating status to pack complete to open task ',context.getRemainingUsage());
			updateStatusInOpenTask(internalid,28);//28 - Status Flag - Outbound Pack Complete
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage After update status to opentask ',context.getRemainingUsage());

		} 

		if(DataRecArr.length>0)
		{
			var vtotalpackcount=removeDuplicateElement(containerlpArray);

			nlapiLogExecution('DEBUG', 'DataRecArr.length', DataRecArr.length);
			var oldcontainer="";
			var packcount=0;

			for (var k = 0; k < DataRecArr.length; k++)
			{	
				var DataRecord=DataRecArr[k];
				var containerlpno =  DataRecord.DataRecContainer;
				nlapiLogExecution('DEBUG', 'Old Container', oldcontainer);
				nlapiLogExecution('DEBUG', 'New Container', containerlpno);

				if(oldcontainer!=containerlpno)
				{
					packcount=parseInt(packcount)+1;
					updateStatusInLPMaster(containerno,28);//28 - Status Flag - Outbound Pack Complete
					var context = nlapiGetContext();
					nlapiLogExecution('DEBUG','Remaining usage after update status in lp master ',context.getRemainingUsage());
					CreatePACKTask(DataRecord.compid,DataRecord.DataRecSiteId,DataRecord.DataRecEbizOrdNo,DataRecord.DataRecOrdNo,DataRecord.DataRecContainer,14,DataRecord.DataRecPackStation,DataRecord.DataRecPackStation,28,DataRecord.DataRecEbizControlNo,DataRecord.DataRecItem);//14 - Task Type - PACK
					var context = nlapiGetContext();
					nlapiLogExecution('DEBUG','Remaining usage after pack task creation ',context.getRemainingUsage()); 
					if(vCarrierType=="PC")
					{	
						CreateShippingManifestRecord(ebizorderno,containerlpno,CarrieerName,null,null,packcount,vtotalpackcount);
					}
					var context = nlapiGetContext();
					nlapiLogExecution('DEBUG','Remaining usage after Ship manifest ',context.getRemainingUsage());
					oldcontainer = containerlpno;
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'out of UpdatesInOpenTask function', vebizOrdNo);
}

function removeDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray.length;
}

function UpdateoutboundInventory(ContainerLPNo,varEnteredLP)
{
	nlapiLogExecution('DEBUG', 'Into UpdateoutboundInventory');

	nlapiLogExecution('DEBUG', 'ContainerLPNo',ContainerLPNo);
	nlapiLogExecution('DEBUG', 'varEnteredLP',varEnteredLP);

	try
	{

		var invtfilters=new Array();
		invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp',null,'is',ContainerLPNo));


		var invtcolumns=new Array();
		invtcolumns[0]=new nlobjSearchColumn('internalid');

		var Invtrefno=nlapiSearchRecord('customrecord_ebiznet_createinv',null,invtfilters,invtcolumns);
		if(Invtrefno!=null && Invtrefno!='')
		{
			var invtid=Invtrefno[0].getValue('internalid');
			var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invtid);

			Invttran.setFieldValue('custrecord_ebiz_inv_lp',varEnteredLP);	
			Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
			Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

			var invtrecid = nlapiSubmitRecord(Invttran, false, true);
			nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);
		}

	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of UpdateoutboundInventory');
}
