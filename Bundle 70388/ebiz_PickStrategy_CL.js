/***************************************************************************
 eBizNET Solutions               
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_PickStrategy_CL.js,v $
 *     	   $Revision: 1.6.2.1.4.2.4.1 $
 *     	   $Date: 2013/03/22 11:44:32 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_PickStrategy_CL.js,v $
 * Revision 1.6.2.1.4.2.4.1  2013/03/22 11:44:32  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.6.2.1.4.2  2013/01/15 11:47:04  grao
 * no message
 *
 * Revision 1.6.2.1.4.1  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.6.2.1  2012/02/09 16:10:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.7  2012/02/09 14:43:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.6  2012/01/06 13:04:52  spendyala
 * CASE201112/CR201113/LOG201121
 * added code to remove space.
 *
 * Revision 1.5  2012/01/06 09:18:03  spendyala
 * CASE201112/CR201113/LOG201121
 * added new filter to search criteria .
 *
 * Revision 1.4  2011/08/20 13:53:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Fixed the issue while updating values and not allowing the user to create duplicate records
 *
 *  
 *
 *****************************************************************************/


function fnCheckPickStrategyEntry(type)
{
	var varZonename = nlapiGetFieldValue('name');        
	var vSite = nlapiGetFieldValue('custrecord_ebizsitepickrule');
	var vCompany = nlapiGetFieldValue('custrecord_ebizcompanypickrule');
	var vId = nlapiGetFieldValue('id');
	var vSeqNo	= nlapiGetFieldValue('custrecord_ebizsequencenopickrul');

	if(varZonename!=null && varZonename!="" )
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is', varZonename.replace(/\s+$/, ""));//Added by suman on 06/01/12 to trim spaces at right.

		/*
		 * Added on 6/1/2012 by suman
		 * The below variable will filter based on the site of the record selected.
		 */
		filters[1] = new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof',['@NONE@',vSite]);

		if(vId!=null && vId!="")
		{
			filters[2] = new nlobjSearchFilter('internalid', null, 'noneof',vId);
		}

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters);

		if (searchresults != null && searchresults.length > 0)
		{
			alert("The Put/Pick Strategy already exists.");
			return false;
		}
		else
		{
			var filters2 = new Array();
			filters2[0] = new nlobjSearchFilter('custrecord_ebizsequencenopickrul', null, 'is',vSeqNo);
			filters2[1] = new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof',['@NONE@',vSite]);
			filters2[2] = new nlobjSearchFilter('isinactive', null, 'is','F');

			if(vId!=null && vId!="")
			{
				filters2[3] = new nlobjSearchFilter('internalid', null, 'noneof',vId);  
			}

			var searchresults2 = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters2);

			if (searchresults2 != null && searchresults2 != '' && searchresults2.length > 0)
			{	
				alert("This Location and Sequence Number Combination already exists.");
				return false;
			}
			else
			{
				return true;
			}    
		}
	}
	else
	{
		return true;  
	}
}


//function fnCheckPickStrategyEdit(type,name)
function fnCheckPickStrategyEdit(){
	var vId = nlapiGetFieldValue('id');
	var vSeqNo	= nlapiGetFieldValue('custrecord_ebizsequencenopickrul');    
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizsequencenopickrul', null, 'equalTo',[vSeqNo]);        
//	filters[1] = new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', [vSite]);
//	filters[2] = new nlobjSearchFilter('custrecord_ebizcompanypickrule', null, 'anyof', [vCompany]);
	if(vId!=null && vId!=""){
		filters[1] = new nlobjSearchFilter('internalid', null, 'noneof',vId);    
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters);
	}
	if (searchresults != null && searchresults.length > 0){
		alert("The Put/Pick PickStrategy already exists.");
		return false;
	}
	else{
		return true;
	}    
}
