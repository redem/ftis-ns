/***************************************************************************
	  		  eBizNET Solutions Inc .
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_confirmputawaysch_qb_sl.js,v $
 *     	   $Revision: 1.1.2.1.4.1.4.1 $
 *     	   $Date: 2013/09/11 12:03:10 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_confirmputawaysch_qb_sl.js,v $
 * Revision 1.1.2.1.4.1.4.1  2013/09/11 12:03:10  rmukkera
 * Case# 20124374
 *
 * Revision 1.1.2.1.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.1  2012/07/16 06:31:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Confirm Putaway Schedule Script.
 *
 * 
 *****************************************************************************/
function fillPOfield(form, POrderField,maxno){

	var filterspo = new Array();		

	//2 - Locations Assigned
	filterspo.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [2]));
	filterspo.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [2]));
	if(maxno!=-1)
	{
		filterspo.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columnspo = new Array();
	columnspo[0] = new nlobjSearchColumn('id');	 
	columnspo[1] = new nlobjSearchColumn('name');
	columnspo[0].setSort(true);	

	POrderField.addSelectOption("", "");	

	var poSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterspo,columnspo);

	for (var i = 0; poSearchResults != null && i < poSearchResults.length; i++) {
		
		if(poSearchResults[i].getValue('name') != null && poSearchResults[i].getValue('name') != "" && poSearchResults[i].getValue('name') != " ")
		{
			var resdo = form.getField('custpage_qbpo').getSelectOptions(poSearchResults[i].getValue('name'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		POrderField.addSelectOption(poSearchResults[i].getValue('name'), poSearchResults[i].getValue('name'));
	}
	
	if(poSearchResults!=null && poSearchResults.length>=1000)
	{
		var maxno=poSearchResults[poSearchResults.length-1].getValue('id');		
		fillPOfield(form, POrderField,maxno);
	}
}

function ConfirmPutawayQbSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Confirm Putaway');

		var poField = form.addField('custpage_qbpo', 'select', 'PO/TO #');
		poField.setLayoutType('startrow', 'none');
		fillPOfield(form, poField,-1);
		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else //this is the POST block
	{
		var vPo = request.getParameter('custpage_qbpo');	
		nlapiLogExecution('ERROR','vPo',vPo);
		
		var POQbparams = new Array();		
		if (vPo!=null &&  vPo != "") {
			POQbparams["custparam_poid"] = vPo;
		}
		
		response.sendRedirect('SUITELET', 'customscript_putconfirmsch', 'customdeploy_putconfirmsch', false, POQbparams );
	}
}