/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_ReplenCancellation_SL.js,v $
 *� $Revision: 1.1.2.2.4.2.4.1 $
 *� $Date: 2015/09/23 13:18:36 $
 *� $Author: schepuri $
 *� $Name: t_WMS_2015_2_StdBundle_1_13 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_ReplenCancellation_SL.js,v $
 *� Revision 1.1.2.2.4.2.4.1  2015/09/23 13:18:36  schepuri
 *� case# 201414504
 *�
 *� Revision 1.1.2.2.4.2  2014/06/13 15:28:02  sponnaganti
 *� case# 20148563 20148447
 *� Stnd Bundle Issue Fix
 *�
 *� Revision 1.1.2.2.4.1  2013/03/19 11:46:26  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production and UAT issue fixes.
 *�
 *� Revision 1.1.2.2  2012/12/21 09:05:15  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� GSUSA after go live Fixes.
 *�
 *
 ****************************************************************************/

/**
 * 
 * @param request
 * @param response
 */
function CancelReplenishment(request, response){
	if(request.getMethod() == 'GET')
	{
		var form = nlapiCreateForm('Cancel Replenishment');
		buildReplenCancellationForm(form, request, response);
	} 
	else if (request.getMethod() == 'POST')
	{
		try
		{
			var form = nlapiCreateForm('Cancel Replenishment');

			var context = nlapiGetContext();
			var currentUserID = context.getUser();

			var userselection = false;
			var CancelFlag;
			var lincount = request.getLineItemCount('custpage_rplndetails');
			var rplnrptno;
			var rplnitemno;
			var rplnbeginloc;
			var rplnendloc;
			var rplnpriority;

			var boolProcessReplencancellation="F";
			var vTaskIntIdArr=new Array();
			var vInvRefArr=new Array();

			//allow user to cancel the open replens only when Cancel button is clicked.
			if(request.getParameter('custpage_hiddenfield')!=null && request.getParameter('custpage_hiddenfield')!='' && request.getParameter('custpage_hiddenfield')!='F')
			{		
				if(lincount!=null && lincount!='')
				{
					//nlapiLogExecution('ERROR', 'Remaining Usage 2', nlapiGetContext().getRemainingUsage());
					for (var p = 1; p <= lincount; p++) 
					{
						CancelFlag = request.getLineItemValue('custpage_rplndetails', 'custpage_rplncancel', p);
						rplnrptno = request.getLineItemValue('custpage_rplndetails', 'custpage_rplnreportno', p);
						rplnitemno = request.getLineItemValue('custpage_rplndetails', 'custpage_rplnitem', p);
						rplnbeginloc = request.getLineItemValue('custpage_rplndetails', 'custpage_beginloc', p);
						rplnendloc = request.getLineItemValue('custpage_rplndetails', 'custpage_endloc', p);
						rplnpriority = request.getLineItemValue('custpage_rplndetails', 'custpage_rplnpriority', p);
						rplnqty = request.getLineItemValue('custpage_rplndetails', 'custpage_rplnqty', p);

						taskid=request.getLineItemValue('custpage_rplndetails', 'custpage_taskid', p);
						var vInvRef=request.getLineItemValue('custpage_rplndetails', 'custpage_invref', p);

						//nlapiLogExecution('ERROR', 'vInvRef', vInvRef);

						if (CancelFlag == 'T') 
						{
							userselection = true;	        	
						}

						//nlapiLogExecution('ERROR', 'moveFlag', CancelFlag);

						if (CancelFlag == 'T') {
							vTaskIntIdArr.push(taskid);
							if(vInvRef!=null && vInvRef!='')
								vInvRefArr.push(vInvRef);							
						}
					}

					//nlapiLogExecution('ERROR', 'vTaskIntIdArr', vTaskIntIdArr);
					//nlapiLogExecution('ERROR', 'vInvRefArr', vInvRefArr);

					if(vTaskIntIdArr!= null && vTaskIntIdArr != '' && vTaskIntIdArr.length>0)
					{
						var vOpenTaskDets=GetAllOpenTaskRecs(vTaskIntIdArr,0);
						var vInvDets=GetAllInvtDetails(vInvRefArr,0);
						if(vOpenTaskDets != null && vOpenTaskDets != '' && vOpenTaskDets.length>0)
						{
							var context = nlapiGetContext();
							var currentUserID = context.getUser();	
							ReplenCancelTransaction(vOpenTaskDets, vInvDets,currentUserID);
							boolProcessReplencancellation="T";
						}
					}	

					if(boolProcessReplencancellation=="T")
					{
						var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Replenishment Cancelled Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					//nlapiLogExecution('ERROR', 'cancelReplen - POST', 'End');
				}
			}
			buildReplenCancellationForm(form, request, response);
		}
		catch(exp) {
			nlapiLogExecution('ERROR', 'Exception in Replen Cancelation ', exp);	
			showInlineMessage(form, 'Error', 'Replenishment Cancellation Failed', "");
			response.writePage(form);
		}
	}
	response.writePage(form);
}

function buildReplenCancellationForm(form, request, response){
	//nlapiLogExecution('ERROR', 'buildReplenCancellationForm', 'Start');

	var vQbRplnRptNo,vQbRplnItem,vQbRplnBeginLoc,vQbRplnEndLoc,vQbRplnPriority;

	var hiddenField = form.addField('custpage_hiddenfield', 'text', '').setDisplayType('hidden');
	hiddenField.setDefaultValue('T');

	nlapiLogExecution('ERROR', 'custpage_rplnreportno_qb', request.getParameter('custpage_rplnreportno_qb'));
	nlapiLogExecution('ERROR', 'custpage_rplnitem_qb', request.getParameter('custpage_rplnitem_qb'));
	nlapiLogExecution('ERROR', 'custpage_rplnbeginloc_qb', request.getParameter('custpage_rplnbeginloc_qb'));
	nlapiLogExecution('ERROR', 'custpage_rplnendloc_qb', request.getParameter('custpage_rplnendloc_qb'));
	nlapiLogExecution('ERROR', 'custpage_rplnpriority_qb', request.getParameter('custpage_rplnpriority_qb'));

	var ReportNoListField = form.addField('custpage_rplnreportno_qb', 'text', 'Report #');
	ReportNoListField.setDisplayType('hidden');
	if(request.getParameter('custpage_rplnreportno_qb')!='' && request.getParameter('custpage_rplnreportno_qb')!=null)
	{
		ReportNoListField.setDefaultValue(request.getParameter('custpage_rplnreportno_qb'));
		vQbRplnRptNo = request.getParameter('custpage_rplnreportno_qb');
	}

	var RplnItemField = form.addField('custpage_rplnitem_qb', 'text', 'Item');
	RplnItemField.setDisplayType('hidden');
	if(request.getParameter('custpage_rplnitem_qb')!='' && request.getParameter('custpage_rplnitem_qb')!=null)
	{
		RplnItemField.setDefaultValue(request.getParameter('custpage_rplnitem_qb'));
		vQbRplnItem = request.getParameter('custpage_rplnitem_qb');
	}

	var BeginLocField = form.addField('custpage_rplnbeginloc_qb', 'text', 'Begin Location');
	BeginLocField.setDisplayType('hidden');
	if(request.getParameter('custpage_rplnbeginloc_qb')!='' && request.getParameter('custpage_rplnbeginloc_qb')!=null)
	{
		BeginLocField.setDefaultValue(request.getParameter('custpage_rplnbeginloc_qb'));
		vQbRplnBeginLoc = request.getParameter('custpage_rplnbeginloc_qb');
	}

	var EndLocField = form.addField('custpage_rplnendloc_qb', 'text', 'End Location');
	EndLocField.setDisplayType('hidden');
	if(request.getParameter('custpage_rplnendloc_qb')!='' && request.getParameter('custpage_rplnendloc_qb')!=null)
	{
		EndLocField.setDefaultValue(request.getParameter('custpage_rplnendloc_qb'));
		vQbRplnEndLoc = request.getParameter('custpage_rplnendloc_qb');
	}

	var PriorityField = form.addField('custpage_rplnpriority_qb', 'text', 'Priority');
	PriorityField.setDisplayType('hidden');
	if(request.getParameter('custpage_rplnpriority_qb')!='' && request.getParameter('custpage_rplnpriority_qb')!=null)
	{
		PriorityField.setDefaultValue(request.getParameter('custpage_rplnpriority_qb'));
		vQbRplnPriority = request.getParameter('custpage_rplnpriority_qb');
	}

	nlapiLogExecution('ERROR', 'vQbRplnRptNo', vQbRplnRptNo);
	nlapiLogExecution('ERROR', 'vQbRplnItem', vQbRplnItem);
	nlapiLogExecution('ERROR', 'vQbRplnBeginLoc', vQbRplnBeginLoc);
	nlapiLogExecution('ERROR', 'vQbRplnEndLoc', vQbRplnEndLoc);
	nlapiLogExecution('ERROR', 'vQbRplnPriority', vQbRplnPriority);

	var openTaskRecords = getOpenReplenTaskRecords(vQbRplnRptNo,vQbRplnItem,vQbRplnBeginLoc,vQbRplnEndLoc,vQbRplnPriority,0);

	if(openTaskRecords != null && openTaskRecords != "")
	{		
		setPagingForSublist(openTaskRecords,form);	
	}
	// Add submit button
	var submit = form.addSubmitButton('Cancel Replenishment');
	form.setScript('customscript_ebiz_replencancel_cl');
	
}

function setPagingForSublist(openTaskRecords,form)
{
	nlapiLogExecution('ERROR', 'openTaskRecords.length', openTaskRecords.length);

	var sublist = form.addSubList("custpage_rplndetails", "list", "Replen Details");
	sublist.addMarkAllButtons();
	sublist.addField("custpage_rplncancel", "checkbox", "Cancel").setDefaultValue("F");
	sublist.addField("custpage_rplnreportno", "text", "Report #");
	sublist.addField("custpage_rplnitem", "text", "Item");
	sublist.addField("custpage_beginloc", "text", "Begin Location");
	sublist.addField("custpage_endloc", "text", "End Location");
	sublist.addField("custpage_rplnqty", "text", "Quantity");
	sublist.addField("custpage_rplnpriority", "text", "Priority");
	sublist.addField("custpage_taskid", "text", "Task Id").setDisplayType('hidden');
	sublist.addField("custpage_invref", "text", "Inv RefNo").setDisplayType('hidden');

	var openTaskRecordsArray=new Array();	

	for(k=0;k<openTaskRecords.length;k++)
	{
		//nlapiLogExecution('ERROR', 'openTaskRecords[k]', openTaskRecords[k]); 
		var openTasksearchresult = openTaskRecords[k];

		if(openTasksearchresult!=null)
		{
			//nlapiLogExecution('ERROR', 'openTasksearchresult.length ', openTasksearchresult.length); 
			for(var j=0;j<openTasksearchresult.length;j++)
			{
				openTaskRecordsArray[openTaskRecordsArray.length]=openTasksearchresult[j];				
			}
		}
	}
	var test='';

	if(openTaskRecordsArray.length>0 && openTaskRecordsArray.length>100)
	{
		var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
		pagesize.setDisplaySize(10,10);
		pagesize.setLayoutType('outsidebelow', 'startrow');
		var select= form.addField('custpage_selectpage','select', 'Select Records');	
		select.setLayoutType('outsidebelow', 'startrow');			
		select.setDisplaySize(200,30);
		if (request.getMethod() == 'GET'){
			pagesize.setDefaultValue("100");
			pagesizevalue=100;
		}
		else
		{
			if(request.getParameter('custpage_pagesize')!=null)
			{pagesizevalue= request.getParameter('custpage_pagesize');}
			else
			{pagesizevalue= 100;pagesize.setDefaultValue("100");}
		}
		//this is to add the pageno's to the dropdown.
		var len=openTaskRecordsArray.length/parseInt(pagesizevalue);
		for(var k=1;k<=Math.ceil(len);k++)
		{

			var from;var to;

			to=parseInt(k)*parseInt(pagesizevalue);
			from=(parseInt(to)-parseInt(pagesizevalue))+1;

			if(parseInt(to)>openTaskRecordsArray.length)
			{
				to=openTaskRecordsArray.length;
				test=from.toString()+","+to.toString(); 
			}

			var temp=from.toString()+" to "+to.toString();
			var tempto=from.toString()+","+to.toString();
			select.addSelectOption(tempto,temp);

		} 
		if (request.getMethod() == 'POST'){

			if(request.getParameter('custpage_selectpage')!=null ){

				select.setDefaultValue(request.getParameter('custpage_selectpage'));	

			}
			if(request.getParameter('custpage_pagesize')!=null ){

				pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

			}
		}
	}
	else
	{
		pagesizevalue=parseInt(openTaskRecordsArray.length);
	}
	var minval=0;var maxval=parseInt(pagesizevalue);
	if(parseInt(pagesizevalue)>openTaskRecordsArray.length)
	{
		maxval=openTaskRecordsArray.length;
	}
	var selectno=request.getParameter('custpage_selectpage');
	//nlapiLogExecution('ERROR', 'selectno ', selectno); 
	if(selectno!=null )
	{
		var selectedPage= request.getParameter('custpage_selectpage');
		var selectedPageArray=selectedPage.split(',');			
		var diff=parseInt(selectedPageArray[1])-(parseInt(selectedPageArray[0])-1);
		//nlapiLogExecution('ERROR', 'diff',diff);

		var pagevalue=request.getParameter('custpage_pagesize');
		//nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
		if(pagevalue!=null)
		{

			if(parseInt(diff)==parseInt(pagevalue)|| test==selectno)
			{

				var selectedPageArray=selectno.split(',');	
				//nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
				minval=parseInt(selectedPageArray[0])-1;
				//nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
				maxval=parseInt(selectedPageArray[1]);
				//nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
			}
		}
	}

	var reportno,datetimecreated,rplnstatus,rplnitem,rplnbeginloc,rplnendloc,rplnqty,rplnpriority;
	var taskid,vInvRef;

	var index=1;
	for (var i = minval; i < maxval; i++) 
	{
		var openTaskRecord = openTaskRecordsArray[i];
		reportno = openTaskRecord.getValue('name');
		rplnitem = openTaskRecord.getText('custrecord_sku');
		rplnbeginloc = openTaskRecord.getText('custrecord_actbeginloc');
		rplnendloc = openTaskRecord.getText('custrecord_actendloc');
		rplnqty = openTaskRecord.getValue('custrecord_expe_qty');
		rplnpriority = openTaskRecord.getValue('custrecord_taskpriority');
		taskid = openTaskRecord.getId();
		vInvRef = openTaskRecord.getValue('custrecord_invref_no');

		form.getSubList('custpage_rplndetails').setLineItemValue('custpage_rplnreportno', index, reportno);
		form.getSubList('custpage_rplndetails').setLineItemValue('custpage_rplnitem', index, rplnitem);  
		form.getSubList('custpage_rplndetails').setLineItemValue('custpage_beginloc', index, rplnbeginloc);  
		form.getSubList('custpage_rplndetails').setLineItemValue('custpage_endloc', index, rplnendloc);  
		form.getSubList('custpage_rplndetails').setLineItemValue('custpage_rplnqty', index, rplnqty);
		form.getSubList('custpage_rplndetails').setLineItemValue('custpage_rplnpriority', index, rplnpriority);

		form.getSubList('custpage_rplndetails').setLineItemValue('custpage_taskid', index,taskid);
		form.getSubList('custpage_rplndetails').setLineItemValue('custpage_invref', index,vInvRef);

		index=index+1;
	}
}


function getOpenTaskColumns(){
	nlapiLogExecution('ERROR', 'getOpenTaskColumns', 'Start');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecordact_begin_date');
	columns[2] = new nlobjSearchColumn('custrecord_actualbegintime');
	columns[3] = new nlobjSearchColumn('custrecord_sku');
	columns[4] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[5] = new nlobjSearchColumn('custrecord_taskpriority');
	columns[6] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[7] = new nlobjSearchColumn('custrecord_actendloc');
	columns[8] = new nlobjSearchColumn('custrecord_invref_no');
	columns[9] = new nlobjSearchColumn('custrecord_tasktype');
	columns[0].setSort();

	nlapiLogExecution('ERROR', 'getOpenTaskColumns', 'End');
	return columns;
}

/**
 * Function to retrieve open replen task records 
 * 
 * @param RptNo
 * @param ItemNo
 * @param BeginLoc
 * @param EndLoc
 * @param Priority
 * @returns [LIST OF OPEN TASK RECORDS]
 */
var tempReplnResultsArray=new Array();
function getOpenReplenTaskRecords(vQbRplnRptNo,vQbRplnItem,vQbRplnBeginLoc,vQbRplnEndLoc,vQbRplnPriority,maxid){
	nlapiLogExecution('ERROR', 'getOpenReplenTaskRecords', 'Start');

	var j = 2;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[1] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

	if (vQbRplnRptNo != "" && vQbRplnRptNo != null) {        	
		filters[j] = new nlobjSearchFilter('name', null, 'is', vQbRplnRptNo);
		j = j + 1;
	}

	if (vQbRplnItem != "" && vQbRplnItem != null) {
		filters[j] = new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', vQbRplnItem);
		j = j + 1;
	}

	if (vQbRplnBeginLoc != "" && vQbRplnBeginLoc != null) {
		filters[j] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', vQbRplnBeginLoc);
		j = j + 1;
	}

	if (vQbRplnEndLoc != "" && vQbRplnEndLoc != null) {
		filters[j] = new nlobjSearchFilter('custrecord_actendloc', null, 'is', vQbRplnEndLoc);
		j = j + 1;
	}    

	if (vQbRplnPriority != "" && vQbRplnPriority != null) {
		filters[j] = new nlobjSearchFilter('custrecord_taskpriority', null, 'is', vQbRplnPriority);
		j = j + 1;
	}

	if(maxid!=0)
		filters[j] = new nlobjSearchFilter('id', null, 'greaterthan', maxid);
	var columns = getOpenTaskColumns();

	// Retrieve all open task records for the selected replens
	var searchresults = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(searchresults!=null)
	{
		if(searchresults.length>=1000)
		{
			nlapiLogExecution('ERROR', 'InTo More Than 1000 rec');
			var maxno1=searchresults[searchresults.length-1].getId();
			tempReplnResultsArray.push(searchresults);
			getOpenReplenTaskRecords(vQbRplnRptNo,vQbRplnItem,vQbRplnBeginLoc,vQbRplnEndLoc,vQbRplnPriority,maxno1);
		}
		else
		{
			nlapiLogExecution('ERROR', 'InTo Less Than 1000 rec');
			tempReplnResultsArray.push(searchresults);
		}
	}
	return tempReplnResultsArray;
}

var searchResultArr=new Array();
function GetAllOpenTaskRecs(TaskIntIdArr,maxid){

	nlapiLogExecution('ERROR', 'taskid', TaskIntIdArr);

	if(TaskIntIdArr != null && TaskIntIdArr != '' && TaskIntIdArr.length>0)
	{	
		var filters = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'is', TaskIntIdArr));
		if(maxid!=0)
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
		var columns = getOpenTaskColumns();

		// Retrieve all open replen task records
		var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		if(openTaskRecords!=null && openTaskRecords.length>=1000)
		{ 
			for(var p=0;p<openTaskRecords.length;p++)
				searchResultArr.push(openTaskRecords); 
			var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[1]);
			GetAllOpenTaskRecs(TaskIntIdArr,maxno);	
		}
		else
		{
			for(var p=0;p<openTaskRecords.length;p++)
				searchResultArr.push(openTaskRecords); 
		}
	}
	logCountMessage('ERROR', openTaskRecords);
	nlapiLogExecution('ERROR', 'openTaskRecords', openTaskRecords.length);

	return openTaskRecords;
}

function ReplenCancelTransaction(openTaskRecords, InvtDetails,emp){
	nlapiLogExecution('ERROR', 'Into ReplenCancelTransaction', null);
	//nlapiLogExecution('ERROR', 'openTaskRecords.length', openTaskRecords.length);
	//nlapiLogExecution('ERROR', 'InvtDetails', InvtDetails);
	var usage = 0;

	var vInvRecNew=new Array();
	var vInvRecIdNew=new Array();
	var vInvExpQtyNew=new Array();

	if(openTaskRecords != null && openTaskRecords.length > 0){
		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);
		for(var i = 0; i < openTaskRecords.length; i++)
		{
			// Check if it is a RPLN task
			nlapiLogExecution('ERROR', 'TaskType', parseInt(openTaskRecords[i].getValue('custrecord_tasktype')));
			if(parseInt(openTaskRecords[i].getValue('custrecord_tasktype')) == parseInt(8))
			{
				var task = openTaskRecords[i];
				var recordId = task.getId();
				var expectedQty = task.getValue('custrecord_expe_qty');
				var invtRefNo = task.getValue('custrecord_invref_no');

				//nlapiLogExecution('ERROR', 'invtRefNo', invtRefNo);
				if(invtRefNo != null && invtRefNo != '')
				{
					var vInvRec=GetInvDetails(invtRefNo,InvtDetails);

					//nlapiLogExecution('ERROR', 'vInvRec', vInvRec);

					// Update the inventory to reduce the allocation quantity
					if(vInvRec!=null && vInvRec!='')
					{
						if(vInvRecIdNew.indexOf(invtRefNo) == -1)
						{
							vInvRecIdNew.push(invtRefNo);
							vInvRecNew.push(vInvRec);
							vInvExpQtyNew.push(expectedQty);
						}
						else
						{
							var vOldInvExpQty=vInvExpQtyNew[vInvRecIdNew.indexOf(invtRefNo)];
							if(vOldInvExpQty == null || vOldInvExpQty == '')
								vOldInvExpQty=0;
							vInvExpQtyNew[vInvRecIdNew.indexOf(invtRefNo)]=parseInt(expectedQty) + parseInt(vOldInvExpQty);
						}
					}
				}
			}
		}
		if(vInvRecIdNew != null && vInvRecIdNew != '')
		{	
			//nlapiLogExecution('ERROR', 'vInvRecIdNew', vInvRecIdNew);
			//nlapiLogExecution('ERROR', 'vInvExpQtyNew', vInvExpQtyNew);
			//nlapiLogExecution('ERROR', 'vInvRecIdNew.length', vInvRecIdNew.length);
			for(var t=0;t<vInvRecIdNew.length;t++)
			{
				//nlapiLogExecution('ERROR', 'vInvRecNew[t]', vInvRecNew[t]);

				if(vInvRecNew[t] != null)
					updateInventoryForTaskParent(vInvRecNew[t], vInvExpQtyNew[t],emp,newParent);	

			}
		}
		nlapiSubmitRecord(newParent);
		for(var l = 0; l < openTaskRecords.length; l++)
		{
			// Check if it is a RPLN task
			nlapiLogExecution('ERROR', 'TaskType', parseInt(openTaskRecords[l].getValue('custrecord_tasktype')));
			if(parseInt(openTaskRecords[l].getValue('custrecord_tasktype')) == parseInt(8)){
				var task = openTaskRecords[l];
				var recordId = task.getId();
				// Delete the open task record for RPLN task
				if(recordId != null && recordId !='')
					UpdatePickTaskToDel(recordId);
				nlapiLogExecution('ERROR', 'Deleted open task record', recordId);
			}
		}
	}
	nlapiLogExecution('ERROR', 'Remaining Usage 2', nlapiGetContext().getRemainingUsage());
}

function updateInventoryForTaskParent(vInvRec, expectedQty,emp,newParent){
	nlapiLogExecution('ERROR', 'updateInventoryForTaskParent', 'Start');
	nlapiLogExecution('ERROR', 'invtRefNo', vInvRec);
	nlapiLogExecution('ERROR', 'expectedQty', expectedQty);
	var inventoryRecord = null;
	try
	{
		var allocationQty = vInvRec.getValue('custrecord_ebiz_alloc_qty'); 
		nlapiLogExecution('ERROR', 'Inventory Allocation Qty[PREV]', allocationQty);
		nlapiLogExecution('ERROR', 'PackcodeNew', vInvRec.getValue('custrecord_ebiz_inv_packcode'));
		nlapiLogExecution('ERROR', 'IntId', vInvRec.getId());

		nlapiLogExecution('ERROR', 'QTY', vInvRec.getValue('custrecord_ebiz_inv_qty'));
		nlapiLogExecution('ERROR', 'QTY on hand', vInvRec.getValue('custrecord_ebiz_qoh'));
		nlapiLogExecution('ERROR', 'avail QTY', vInvRec.getValue('custrecord_ebiz_avl_qty'));
		// Updating allocation qty
//		if(parseInt(allocationQty) <= 0){
//		allocationQty = parseInt(allocationQty) + parseInt(expectedQty);
//		} 
//		else 
		if(parseInt(allocationQty) > 0){
			allocationQty = parseInt(allocationQty) - parseInt(expectedQty);
		}

		if(parseInt(allocationQty) <= 0){
			allocationQty=0;
		}

		newParent.selectNewLineItem('recmachcustrecord_ebiz_inv_parent');
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'id', vInvRec.getId());
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'name', vInvRec.getValue('name'));
		if(vInvRec.getValue('custrecord_ebiz_inv_binloc') != null && vInvRec.getValue('custrecord_ebiz_inv_binloc') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_binloc', vInvRec.getValue('custrecord_ebiz_inv_binloc'));

		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_invholdflg', vInvRec.getValue('custrecord_ebiz_invholdflg'));		
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_lp', vInvRec.getValue('custrecord_ebiz_inv_lp'));
		if(vInvRec.getValue('custrecord_ebiz_inv_sku') != null && vInvRec.getValue('custrecord_ebiz_inv_sku') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_sku', vInvRec.getValue('custrecord_ebiz_inv_sku'));
		if(vInvRec.getValue('custrecord_ebiz_inv_sku_status') != null && vInvRec.getValue('custrecord_ebiz_inv_sku_status') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_sku_status', vInvRec.getValue('custrecord_ebiz_inv_sku_status'));
		if(vInvRec.getValue('custrecord_ebiz_inv_packcode') != null && vInvRec.getValue('custrecord_ebiz_inv_packcode') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_packcode', vInvRec.getValue('custrecord_ebiz_inv_packcode'));

		// case# 201414504
		if(vInvRec.getValue('custrecord_ebiz_inv_qty') == null || vInvRec.getValue('custrecord_ebiz_inv_qty') == ''|| vInvRec.getValue('custrecord_ebiz_inv_qty') == ' ')
		{
			nlapiLogExecution('ERROR', 'if', vInvRec.getId());
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_qty', vInvRec.getValue('custrecord_ebiz_qoh'));
		}
		else
		{
			nlapiLogExecution('ERROR', 'else', vInvRec.getId());
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_qty', vInvRec.getValue('custrecord_ebiz_inv_qty'));
		}


		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_lot', vInvRec.getValue('custrecord_ebiz_inv_lot'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_fifo', vInvRec.getValue('custrecord_ebiz_inv_fifo'));
		if(vInvRec.getValue('custrecord_ebiz_inv_loc') != null && vInvRec.getValue('custrecord_ebiz_inv_loc') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_loc', vInvRec.getValue('custrecord_ebiz_inv_loc'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_alloc_qty', allocationQty);
		if(vInvRec.getValue('custrecord_inv_ebizsku_no') != null && vInvRec.getValue('custrecord_inv_ebizsku_no') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_inv_ebizsku_no', vInvRec.getValue('custrecord_inv_ebizsku_no'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_invt_ebizlp', vInvRec.getValue('custrecord_invt_ebizlp'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_qoh', vInvRec.getValue('custrecord_ebiz_qoh'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_expdate', vInvRec.getValue('custrecord_ebiz_expdate'));
		if(vInvRec.getValue('custrecord_wms_inv_status_flag') != null && vInvRec.getValue('custrecord_wms_inv_status_flag') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_wms_inv_status_flag', vInvRec.getValue('custrecord_wms_inv_status_flag'));
		if(vInvRec.getValue('custrecord_invttasktype') != null && vInvRec.getValue('custrecord_invttasktype') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_invttasktype', vInvRec.getValue('custrecord_invttasktype'));
		if(vInvRec.getValue('custrecord_outboundinvlocgroupid') != null && vInvRec.getValue('custrecord_outboundinvlocgroupid') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_outboundinvlocgroupid', vInvRec.getValue('custrecord_outboundinvlocgroupid'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_pickseqno', vInvRec.getValue('custrecord_pickseqno'));
		if(vInvRec.getValue('custrecord_inboundinvlocgroupid') != null && vInvRec.getValue('custrecord_inboundinvlocgroupid') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_inboundinvlocgroupid', vInvRec.getValue('custrecord_inboundinvlocgroupid'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_putseqno', vInvRec.getValue('custrecord_putseqno'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_callinv', 'N');
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_avl_qty', vInvRec.getValue('custrecord_ebiz_avl_qty'));
		if(vInvRec.getValue('custrecord_ebiz_transaction_no') != null && vInvRec.getValue('custrecord_ebiz_transaction_no') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_transaction_no', vInvRec.getValue('custrecord_ebiz_transaction_no'));
		//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_serialnumbers', vInvRec.getValue('custrecord_ebiz_serialnumbers'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_putseqno', vInvRec.getValue('custrecord_putseqno'));
		if(emp != null && emp != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_updated_user_no', emp);
		else
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_updated_user_no', nlapiGetContext().getUser());

		newParent.commitLineItem('recmachcustrecord_ebiz_inv_parent');
		nlapiLogExecution('ERROR', 'updateInventoryForReplenCancel', 'End');

	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in Replen Cancellation Inv updation', exp);
	}
}

var searchInvArr=new Array();
function GetAllInvtDetails(InvRefId,maxid){
	nlapiLogExecution('ERROR', 'GetAllInvtDetails', 'Start');

	nlapiLogExecution('ERROR', 'InvRefId', InvRefId);

	// Filter inventory records 
	if(InvRefId != null && InvRefId != '' && InvRefId.length>0)
	{	
		var filters = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'is', InvRefId));
		if(maxid!=0)
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_packcode'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));
		columns.push(new nlobjSearchColumn('custrecord_inv_ebizsku_no'));
		columns.push(new nlobjSearchColumn('custrecord_invt_ebizlp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_expdate'));
		columns.push(new nlobjSearchColumn('custrecord_wms_inv_status_flag'));
		columns.push(new nlobjSearchColumn('custrecord_invttasktype'));
		columns.push(new nlobjSearchColumn('custrecord_outboundinvlocgroupid'));
		columns.push(new nlobjSearchColumn('custrecord_pickseqno'));
		columns.push(new nlobjSearchColumn('custrecord_inboundinvlocgroupid'));
		columns.push(new nlobjSearchColumn('custrecord_putseqno'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_transaction_no'));
		columns.push(new nlobjSearchColumn('name'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_invholdflg'));	

		// Retrieve all inventory records
		var InvRecs = new nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
		if(InvRecs!=null && InvRecs.length>=1000)
		{ 
			for(var p=0;p<InvRecs.length;p++)
				searchInvArr.push(InvRecs[p]); 
			var maxno=InvRecs[InvRecs.length-1].getValue(columns[0]);
			GetAllInvtDetails(InvRefId,maxno);	
		}
		else
		{
			for(var p=0;p<InvRecs.length;p++)
				searchInvArr.push(InvRecs[p]); 
		}
	}
	logCountMessage('ERROR', searchInvArr);
	nlapiLogExecution('ERROR', 'InvRecords', searchInvArr.length);
	nlapiLogExecution('ERROR', 'GetAllInvtDetails', 'End');

	return searchInvArr;
}

function GetInvDetails(invtRefNo,InvtDetails)
{
	for(var p=0;p<InvtDetails.length;p++)
	{
		nlapiLogExecution('ERROR', 'InvtDetails[p].getId()',InvtDetails[p].getId());
		nlapiLogExecution('ERROR', 'invtRefNo',invtRefNo);
		if(InvtDetails[p].getId()==invtRefNo)
		{
			nlapiLogExecution('ERROR', 'invtRefNo',InvtDetails[p].getValue('custrecord_ebiz_alloc_qty'));
			return InvtDetails[p];
		}	
	}	
	return InvtDetails;
}

function UpdatePickTaskToDel(recordId)
{
	var deletedId = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', recordId); 	// 4 UNITS
	nlapiLogExecution('ERROR', 'Deleting open task record', recordId);
}