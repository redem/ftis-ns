/***************************************************************************
eBizNET Solutions               
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Schedulers/Attic/ebiz_autowave_scheduler.js,v $
 *     	   $Revision: 1.1.2.1.2.1 $
 *     	   $Date: 2014/07/08 09:40:17 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: ..
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_autowave_scheduler.js,v $
 * Revision 1.1.2.1.2.1  2014/07/08 09:40:17  snimmakayala
 * 201217295
 *
 * Revision 1.1.2.2  2014/07/08 09:38:56  snimmakayala
 * 201217295
 *
 * Revision 1.1.2.1  2014/01/06 13:11:01  snimmakayala
 * Case# : 201217295
 * Auto Wave Scheduler
 *
 * Revision 1.1.2.1  2013/06/11 12:50:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Place Holders for Schedulers and Restlets
 *

 *****************************************/

function getOrderDetailsForWaveGen(ordno,ordlineno)
{
	nlapiLogExecution('Debug', 'Into getOrderDetailsForWaveGen');
	var orderList = new Array();
	var filters = new Array();
	var fulfillmentordlist = new Array();
	var orderInfoCount = 0;

	//nlapiLogExecution('Debug', 'ebizwaveno', ebizwaveno);
	nlapiLogExecution('Debug', 'ordno', ordno);
	nlapiLogExecution('Debug', 'ordlineno', ordlineno);

	var vGetOrdersList = getOrderHeaderDetsForWaveGen();
	if(vGetOrdersList != null && vGetOrdersList != '')
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [25]));
		//filters.push(new nlobjSearchFilter('custrecord_do_order_type', null, 'anyof', [3]));
		filters.push(new nlobjSearchFilter('custrecord_do_order_priority', null, 'is', '1'));
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno', null, 'equalto', '1'));
		if(vGetOrdersList != null && vGetOrdersList != '')
		{	
			nlapiLogExecution('Debug', 'vGetOrdersList', vGetOrdersList);
			nlapiLogExecution('Debug', 'vGetOrdersList.length', vGetOrdersList.length);
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', vGetOrdersList));
		}
		var columns = new Array();

		columns[0] = new nlobjSearchColumn('custrecord_linesku_status');
		columns[1] = new nlobjSearchColumn('custrecord_do_customer');
		columns[2] = new nlobjSearchColumn('custrecord_do_carrier');
		columns[3] = new nlobjSearchColumn('custrecord_do_order_type');
		columns[4] = new nlobjSearchColumn('custrecord_do_order_priority');
		columns[5] = new nlobjSearchColumn('custrecord_lineord');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_linesku');
		columns[7] = new nlobjSearchColumn('custrecord_ord_qty');	
		columns[8] = new nlobjSearchColumn('custrecord_ebiz_orderdate');
		columns[8].setSort();
		columns[9] = new nlobjSearchColumn('custrecord_lineord');
		columns[9].setSort();		
		columns[10] = new nlobjSearchColumn('custrecord_ordline');
		columns[10].setSort();
		columns[11] = new nlobjSearchColumn('custrecord_linepackcode');
		columns[12] = new nlobjSearchColumn('custrecord_lineuom_id');
		columns[13] = new nlobjSearchColumn('custrecord_batch');
		columns[14] = new nlobjSearchColumn('custrecord_ordline_wms_location');
		columns[15] = new nlobjSearchColumn('custrecord_ordline_company');
		columns[16] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo1');
		columns[17] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo2');
		columns[18] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo3');	
		columns[19] = new nlobjSearchColumn('custrecord_pickgen_qty');
		columns[20] = new nlobjSearchColumn('custrecord_do_wmscarrier');
		columns[21] = new nlobjSearchColumn('custrecord_nsconfirm_ref_no');
		columns[22] = new nlobjSearchColumn('custrecord_linenotes1');
		columns[23] = new nlobjSearchColumn('custrecord_linenotes2');
		columns[24] = new nlobjSearchColumn('custrecord_pickgen_flag');
		columns[25] = new nlobjSearchColumn('custrecord_pickqty');
		columns[26] = new nlobjSearchColumn('custrecord_ship_qty');
		columns[27] = new nlobjSearchColumn('custrecord_ns_ord');
		columns[28] = new nlobjSearchColumn('custrecord_shipment_no');
		columns[29] = new nlobjSearchColumn('custrecord_printflag');
		columns[30] = new nlobjSearchColumn('custrecord_print_count');
		columns[31] = new nlobjSearchColumn('custrecord_ebiz_freightterms');
		columns[32] = new nlobjSearchColumn('custrecord_shipcomplete');
		columns[33] = new nlobjSearchColumn('custrecord_ebiz_pr_dateprinted');
		columns[34] = new nlobjSearchColumn('name');

		orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

		if(orderList!=null && orderList!='' && orderList.length>0)
		{
			nlapiLogExecution('Debug', 'orderList length', orderList.length);
			for(i=0;i<Math.min(100, orderList.length);i++)
			{
				var currentOrder=orderList[i];

				var vOrdQty = parseInt(currentOrder.getValue('custrecord_ord_qty'));

				//nlapiLogExecution('Debug', 'vOrdQty', vOrdQty);

				var vPickGenQty=0;

				if(currentOrder.getValue('custrecord_pickgen_qty')!=null && currentOrder.getValue('custrecord_pickgen_qty')!='')	
					vPickGenQty = parseInt(currentOrder.getValue('custrecord_pickgen_qty'));

				//nlapiLogExecution('Debug', 'vPickGenQty', vPickGenQty);

				vOrdQty = parseInt(vOrdQty)-parseInt(vPickGenQty);

				//nlapiLogExecution('Debug', 'vOrdQty', vOrdQty);

				var itemName = currentOrder.getText('custrecord_ebiz_linesku');
				var itemNo = currentOrder.getValue('custrecord_ebiz_linesku');
				var foordqty = currentOrder.getValue('custrecord_ord_qty');
				var availQty = 0;
				var orderQty = vOrdQty;
				var fointrid = currentOrder.getId();
				var doNo = currentOrder.getId();
				var lineNo = currentOrder.getValue('custrecord_ordline');
				var foname = currentOrder.getValue('custrecord_lineord');
				var doName = currentOrder.getValue('custrecord_lineord');
				var soInternalID = currentOrder.getValue('name');
				var itemStatus = currentOrder.getValue('custrecord_linesku_status');
				var packCode = currentOrder.getText('custrecord_linepackcode');
				var uom = currentOrder.getValue('custrecord_lineuom_id');
				var lotBatchNo = currentOrder.getValue('custrecord_batch');
				var location = currentOrder.getValue('custrecord_ordline_wms_location');
				var company = currentOrder.getValue('custrecord_ordline_company');
				var itemInfo1 = currentOrder.getText('custrecord_fulfilmentiteminfo1');
				var itemInfo2 = currentOrder.getText('custrecord_fulfilmentiteminfo2');
				var itemInfo3 = currentOrder.getText('custrecord_fulfilmentiteminfo3');
				var orderType = currentOrder.getValue('custrecord_do_order_type');
				var orderPriority = currentOrder.getValue('custrecord_do_order_priority');
				var wmsCarrier = currentOrder.getValue('custrecord_do_wmscarrier');
				var nsrefno = currentOrder.getValue('custrecord_nsconfirm_ref_no');			
				var note1 = currentOrder.getValue('custrecord_linenotes1');
				var note2 = currentOrder.getValue('custrecord_linenotes2');
				var pickgenflag = currentOrder.getValue('custrecord_pickgen_flag');
				var pickqty = currentOrder.getValue('custrecord_pickqty');
				var shipqty = currentOrder.getValue('custrecord_ship_qty');
				var parentordno = currentOrder.getValue('custrecord_ns_ord');
				var shipmentno = currentOrder.getValue('custrecord_shipment_no');
				var printflag = currentOrder.getValue('custrecord_printflag');
				var printcount = currentOrder.getValue('custrecord_print_count');
				var freightterms = currentOrder.getValue('custrecord_ebiz_freightterms');
				var shipcomplete = currentOrder.getValue('custrecord_shipcomplete');
				var pickreportprintdt = currentOrder.getValue('custrecord_ebiz_pr_dateprinted');
				var customer = currentOrder.getValue('custrecord_do_customer');
				var shipmethod = currentOrder.getValue('custrecord_do_carrier');

				if(nsrefno == null || isNaN(nsrefno))
					nsrefno='';

				var currentRow = [i, soInternalID, lineNo, fointrid, foname, itemName, itemNo, itemStatus,foordqty, orderQty, 
				                  packCode, uom, lotBatchNo, company,itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority, 
				                  location,wmsCarrier,nsrefno,note1,note2,pickgenflag,pickqty,shipqty,parentordno,shipmentno,
				                  printflag,printcount,freightterms,shipcomplete,pickreportprintdt,vPickGenQty,customer,shipmethod];

				/*var currentRow = [i, soInternalID, lineNo, doNo, doName, itemName, itemNo, itemStatus, 
			                  availQty, orderQty, packCode, uom, lotBatchNo, company, 
			                  itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority,location,wmsCarrier,nsrefno];*/
				//nlapiLogExecution('Debug', 'currentRow', currentRow);
				fulfillmentordlist[orderInfoCount++] = currentRow;

			}
		}
	}

	nlapiLogExecution('Debug', 'Out of getOrderDetailsForWaveGen');

	return fulfillmentordlist;
}

function getOrderHeaderDetsForWaveGen()
{
	nlapiLogExecution('Debug', 'Into getOrderHeaderDetsForWaveGen');
	var orderList = new Array();
	var filters = new Array();
	var fulfillmentordlist = new Array();
	var orderInfoCount = 0;


	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [25]));
	//filters.push(new nlobjSearchFilter('custrecord_do_order_type', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_do_order_priority', null, 'is', '1'));
	//filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno', null, 'equalto', '1'));
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ns_ord', 'is', 'T'));
	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_orderdate',null,'group');
	columns[0].setSort(true);
	columns[1] = new nlobjSearchColumn('internalid','custrecord_ns_ord','group');
	columns[2] = new nlobjSearchColumn('custrecord_ordline',null,'count');
	columns[3] = new nlobjSearchColumn('status','custrecord_ns_ord','max');

	columns[1].setSort();
	orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	var vArrOrdersIntId=new Array();
	if(orderList != null && orderList != '')
	{
		var vLineCount=0;

		for(var i=0;(vArrOrdersIntId.length < 10 && i<orderList.length ) ;i++)
		{
			var vTempStatus=orderList[i].getValue('status','custrecord_ns_ord','max');
			if(vTempStatus!= 'Closed' && vTempStatus !='Cancel')
			{
				var vTempLineCount=0;
				if(orderList[i].getValue('custrecord_ordline',null,'count')!=null && orderList[i].getValue('custrecord_ordline',null,'count') != '')
					vTempLineCount=orderList[i].getValue('custrecord_ordline',null,'count');
				else
					vTempLineCount=0;

				nlapiLogExecution('Debug', 'vTempLineCount', vTempLineCount);
				nlapiLogExecution('Debug', 'vLineCount', vLineCount);
				if(parseInt(vLineCount) +parseInt(vTempLineCount) <=100)
				{	
					vLineCount=parseInt(vLineCount) + parseInt(vTempLineCount);
					vArrOrdersIntId.push(orderList[i].getValue('internalid','custrecord_ns_ord','group'));
				}	
				else
					break;
			}
		}	

	}
	return vArrOrdersIntId;
}

function eBiz_Auto_Wave(type)
{
	var context = nlapiGetContext(); 
	var vordno="";
	var vordlineno="";
	var vwaveno="";
	var vlastlineinorder='F';
	var vlocation="";
	var vcompany="";
	var userId="";
	var allocstrategy='';
	var vlastlineinwave='F';
	var vwaveordrecid=-1;
	var vusedflag='';
	var voldordno='';
	var lefoflag='';
	try{

		var vGetHours=GetHours();
		var vProcessSchedulerFlag='F';

//		if(vGetHours!= null && vGetHours != '')
//		{
//		if(vGetHours >= 18 && vGetHours <= 20)
//		vProcessSchedulerFlag='T';
//		} 	
		nlapiLogExecution('Debug', 'vProcessSchedulerFlag',vProcessSchedulerFlag);

		if(vProcessSchedulerFlag=='T')
		{
			for(var z=0;z<5 && context.getRemainingUsage() > 10000 ;z++)//It creates 5 waves
			{
				nlapiLogExecution('Debug', 'Time Stamp at the start of scheduler',TimeStampinSec());

				if(type != 'aborted')
				{
					var fulfillOrdersList = getOrderDetailsForWaveGen(vordno,vordlineno);
					if(fulfillOrdersList != null && fulfillOrdersList != '')
					{	
						var eBizWaveNo = GetMaxTransactionNo('WAVE');
						nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);

						for(var i = 0; i < fulfillOrdersList.length; i++)
						{
							var fointrid = fulfillOrdersList[i][3];
							updateFulfilmentOrder(fointrid, eBizWaveNo);
						}

						if(eBizWaveNo!='')
						{
							nlapiLogExecution('DEBUG', 'Invoking Schedule Script Starts', TimeStampinSec());	

							var param = new Array();
							param['custscript_ebizwavenonew'] = eBizWaveNo;
							nlapiScheduleScript('customscript_ebiz_hook_scheduler1', null,param);

							nlapiLogExecution('DEBUG', 'Invoking Schedule Script Ends', TimeStampinSec());
						}
					}
				}
				nlapiLogExecution('Debug', 'Time Stamp at the end of scheduler',TimeStampinSec());
			}
		}		
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in Scheduler',exp);
	}
}

function updateFulfilmentOrder(fointrid, eBizWaveNo){

	nlapiLogExecution('DEBUG', 'Into updateFulfilmentOrder ',TimeStampinSec());

	var recId = fointrid;

	var fieldNames = new Array(); 				//fields to be updated
	fieldNames[0] = "custrecord_ebiz_wave";  	
	fieldNames[1] = "custrecord_linestatus_flag";
	fieldNames[2] = "custrecord_linenotes1";

	var newValues = new Array(); 				//new field values
	newValues[0] = eBizWaveNo;
	newValues[1] =  '15';
	newValues[2] =  'Auto Wave Release';

	nlapiSubmitField('customrecord_ebiznet_ordline', recId, fieldNames, newValues);

	nlapiLogExecution('DEBUG', 'Fulfilment Order Updated Successfully', fointrid);
	nlapiLogExecution('DEBUG', 'Out of updateFulfilmentOrder',TimeStampinSec());
}

function GetHours()
{
	var now = new Date();	  
	var curr_hour = now.getHours();
	nlapiLogExecution('Debug', 'curr_hour : ', curr_hour);
	if(curr_hour != null && curr_hour != '')//Converting to EST time
	{	
		curr_hour=parseInt(curr_hour) +3;
		if(parseInt(curr_hour)>=24)
		{
			curr_hour=parseInt(curr_hour)-24;

		}
		nlapiLogExecution('Debug', 'New curr_hour : ', curr_hour);

	}
	return curr_hour;
}

