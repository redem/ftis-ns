/***************************************************************************
??????????????????????????????? ? ????????????????????????????? ??eBizNET
???????????????????????                           eBizNET Solutions Inc
****************************************************************************
*
*? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_TOPartialPutawayConfirmation_SL.js,v $
*? $Revision: 1.3.2.2.4.1 $
*? $Date: 2012/11/01 14:55:15 $
*? $Author: schepuri $
*? $Name: t_NSWMS_2013_1_2_7 $
*
* DESCRIPTION
*? Functionality
*
* REVISION HISTORY
*? $Log: ebiz_TOPartialPutawayConfirmation_SL.js,v $
*? Revision 1.3.2.2.4.1  2012/11/01 14:55:15  schepuri
*? CASE201112/CR201113/LOG201121
*? Decimal Qty Conversions
*?
*? Revision 1.3.2.2  2012/04/10 22:46:30  snimmakayala
*? CASE201112/CR201113/LOG201121
*? RF TO confirmation issue
*?
*? Revision 1.3  2012/04/10 22:32:47  snimmakayala
*? CASE201112/CR201113/LOG201121
*? Production issue fixes
*?
*? Revision 1.2  2012/03/02 13:41:23  rmukkera
*? CASE201112/CR201113/LOG201121
*?  code changes
*?
*? Revision 1.1  2012/02/09 11:39:55  rmukkera
*? CASE201112/CR201113/LOG201121
*? Transfer Order Confirmation new File
*?
*
****************************************************************************/
function ToPartialPutawayConfirmation(request,response)
{
	if(request.getMethod()=='GET')
	{
		var form=nlapiCreateForm('TO Putaway Confirmation');
		createform(form);
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else
	{
		var form=nlapiCreateForm('TO Putaway Confirmation');
		form.setScript('customscript_ebiz_toconfirmation_cl');  
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		msg.setLayoutType('outside','startcol');
		createform(form);		
		form.addSubmitButton('Display');
		var vTo = request.getParameter('custpage_qbto');
		nlapiLogExecution('ERROR','vTo',vTo);
		var filtersPicktsk = new Array();
		var lineCount = request.getLineItemCount('custpage_items');
		nlapiLogExecution('ERROR','lineCount',lineCount);
		if(lineCount!=null && lineCount>0)
		{
			var to=request.getLineItemValue('custpage_items', 'custpage_to', 1);
			if(vTo==to){
				var trecord ;var adjustNsInventoryArray=new Array();
				for (var k=1; k<=lineCount; k++){
					var lineno=request.getLineItemValue('custpage_items', 'custpage_lineno', k);

					if(lineno.indexOf('Total')!=-1)
					{
						if(trecord==null)
						{
							trecord = nlapiTransformRecord('transferorder', vTo, 'itemreceipt');
						}
						var tempOrdQty=request.getLineItemValue('custpage_items', 'custpage_orderqty', k-1);
						nlapiLogExecution('ERROR','tempOrdQty',tempOrdQty);
						var lineno=request.getLineItemValue('custpage_items', 'custpage_lineno', k-1);
						nlapiLogExecution('ERROR','lineno',lineno);
						var itemstatus=request.getLineItemValue('custpage_items', 'custpage_itemstatus', k-1);
						nlapiLogExecution('ERROR','itemstatus',itemstatus);
						var item=request.getLineItemValue('custpage_items', 'custpage_item', k-1);
						nlapiLogExecution('ERROR','item',item);
						var location=request.getLineItemValue('custpage_items', 'custpage_location', k-1);
						nlapiLogExecution('ERROR','location',location);
						var templine=request.getLineItemValue('custpage_items', 'custpage_lineno', k);
						nlapiLogExecution('ERROR','templine',templine);
						var tempArray=templine.split(',');
						nlapiLogExecution('ERROR','tempArray.length',tempArray.length);
						var tempChkArray=tempArray[0].split(':');
						var tempChkQty=tempChkArray[1];
						nlapiLogExecution('ERROR','tempChkQty',tempChkQty);
						var tempPutArray=tempArray[1].split(':');
						var tempPutWQty=tempPutArray[1];
						nlapiLogExecution('ERROR','tempPutWQty',tempPutWQty);
						if(parseFloat(tempChkQty)==parseFloat(tempPutWQty))
						{
							ConfirmtoNs(item,itemstatus,location,lineno,trecord,tempOrdQty,tempPutWQty,adjustNsInventoryArray);
						}


					}

				}
				var id = nlapiSubmitRecord(trecord);  var confirmedlines='';
				if(id!=null)
				{
					var totranid=trecord.getFieldValue('tranid');
					if(adjustNsInventoryArray.length>0)
					{
						nlapiLogExecution('ERROR','adjustNsInventoryArray',adjustNsInventoryArray.toString());

						for(var i=0;i<adjustNsInventoryArray.length;i++)
						{
							if(i!=adjustNsInventoryArray.length-1)
								confirmedlines=confirmedlines+adjustNsInventoryArray[i][1]+",";
							else
								confirmedlines=confirmedlines+adjustNsInventoryArray[i][1];	
							//if orderQty > putconfQty then inventory adjustment will be called
							if(parseFloat(adjustNsInventoryArray[i][2])>parseFloat(adjustNsInventoryArray[i][3]))
							{

								AdjustNsInventory(adjustNsInventoryArray[i][0],adjustNsInventoryArray[i][1],adjustNsInventoryArray[i][2],adjustNsInventoryArray[i][3],adjustNsInventoryArray[i][4],adjustNsInventoryArray[i][5],totranid);

							}
						}
					}

				}
				nlapiLogExecution('ERROR','confirmedlines',confirmedlines);
				if(confirmedlines!='')
				{
					msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Confirmation', 'TO Received lines "+confirmedlines+".', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
				}
			}

		}
//		else
//		{
		if(vTo!=null && vTo!="")
		{
			var toRecord=nlapiLoadRecord('transferorder',vTo);
			if(toRecord!=null)
			{
				var TOLength = toRecord.getLineItemCount('item'); 		
				var totranid=toRecord.getFieldValue('tranid');
				nlapiLogExecution('ERROR','totranid',totranid);
				var filtersPicktsk=new Array();
				filtersPicktsk[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['1']);//chkn task
				filtersPicktsk[1] = new nlobjSearchFilter('name', null, 'is',totranid);

				var columns=new Array();
				columns[0]=new nlobjSearchColumn('custrecord_line_no');
				columns[1]=new nlobjSearchColumn('custrecord_lpno');
				columns[2]=new nlobjSearchColumn('custrecord_act_qty');
				columns[3]=new nlobjSearchColumn('custrecord_tasktype');
				columns[0].setSort();

				var opentasksosearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPicktsk, columns);
				displaySublist(opentasksosearchresults,form,toRecord);


			}
		}
		//}
		response.writePage(form);
	}
}
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}
function createform(form)
{
	var toField = form.addField('custpage_qbto', 'select', 'Transfer Order#');
	toField.setLayoutType('startrow', 'none');
	toField.setDisplaySize('80', '20');
	toField.setMandatory(true);
	var vSo = request.getParameter('custpage_qbto');
	toField.setDefaultValue(vSo);
	var locationFilters = new Array();
	locationFilters[0] = new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is' ,'T');
	var locationColumns = new Array();
	locationColumns[0] = new nlobjSearchColumn('name'); 
	var mainwhLocs=new Array();
	var locationSearchresult = nlapiSearchRecord('location',null,locationFilters ,locationColumns );
	if(locationSearchresult!=null)
	{
		nlapiLogExecution('ERROR','locationSearchresult.length',locationSearchresult.length);
		for(var j=0;j<locationSearchresult.length;j++)
		{
			mainwhLocs[mainwhLocs.length]=locationSearchresult[j].getId();
		}

	}
	nlapiLogExecution('ERROR','mainwhLocs',mainwhLocs.toString());
	var filters = new Array();	
	filters[0] = new nlobjSearchFilter( 'mainline', null, 'is', 'T' );
	filters[1] = new nlobjSearchFilter( 'transferlocation', null, 'anyof', mainwhLocs);

	var columns=new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('statusref');

	var Tocount=0;
	var searchResults = nlapiSearchRecord('transferorder', null, filters, columns, null );
	toField.addSelectOption("","");

	for (var i = 0; searchResults != null && i < searchResults.length; i++) {
		if(searchResults[i].getValue('statusref')=='pendingReceipt'||searchResults[i].getValue('statusref')=='pendingReceiptPartFulfilled')
		{
			//Searching for Duplicates		
			var res=  form.getField('custpage_qbto').getSelectOptions(searchResults[i].getValue('tranid'), 'is');
			if (res != null) {

				if (res.length > 0) {
					continue;
				}
			}		
			Tocount=Tocount+1;
//			nlapiLogExecution('ERROR','searchResults.length',searchResults.length-1);
//			nlapiLogExecution('ERROR','i',i);
//			if(i==searchResults.length-1)
//			{
//			toField.addSelectOption(searchResults[i].getId(), searchResults[i].getValue('tranid')+" ("+Tocount+")");
//			}
//			else
//			{
			toField.addSelectOption(searchResults[i].getId(), searchResults[i].getValue('tranid'));
//			}

		}
	}		

	var sublist = form.addSubList("custpage_items", "list", "ItemList");	
	sublist.addField("custpage_lineno", "text", "Line Number").setDisplayType('inline');
	sublist.addField("custpage_itemtext", "text", "Item").setDisplayType('inline');
	sublist.addField("custpage_itemdesc", "text", "Item Description").setDisplayType('inline');
	sublist.addField("custpage_orderqty", "text", "Order Quantity").setDisplayType('inline');
	sublist.addField("custpage_checkinqty", "text", "Check In Qty").setDisplayType('inline');
	sublist.addField("custpage_putconfirmqty", "text", "Putaway Qty").setDisplayType('inline');
	sublist.addField("custpage_lp", "text", "LP").setDisplayType('inline');
	sublist.addField("custpage_to", "text", "to").setDisplayType('hidden');
	sublist.addField("custpage_item", "text", "SKU").setDisplayType('hidden');
	sublist.addField("custpage_itemstatus", "text", "SKU Status").setDisplayType('hidden');
	sublist.addField("custpage_location", "text", "location").setDisplayType('hidden');
}

function displaySublist(opentasksosearchresults,form,toRecord)
{
	nlapiLogExecution('ERROR','into displaySublist');
	nlapiLogExecution('ERROR','Tran ID',toRecord.getFieldValue('tranid') );
	var columns=new Array();
	columns[0]=new nlobjSearchColumn('custrecord_line_no');
	columns[1]=new nlobjSearchColumn('custrecord_lpno');
	columns[2]=new nlobjSearchColumn('custrecord_act_qty');
	columns[3]=new nlobjSearchColumn('custrecord_tasktype');
	columns[0].setSort();
	var filtersPicktsk=new Array();
	filtersPicktsk[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['2']);//putaway task
	filtersPicktsk[1] = new nlobjSearchFilter('name', null, 'is', toRecord.getFieldValue('tranid'));
	filtersPicktsk[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty');

	var opentaskputwsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPicktsk, columns);
	nlapiLogExecution('ERROR','opentaskputwsearchresults',opentaskputwsearchresults);
	
	var closedtaskputwsearchresults = new Array();

	if(opentaskputwsearchresults==null || opentaskputwsearchresults=='' || opentaskputwsearchresults.length==0)
	{
		nlapiLogExecution('ERROR','opentaskputwsearchresults is null');
		var putwcolumns=new Array();
		putwcolumns[0]=new nlobjSearchColumn('custrecord_ebiztask_line_no');
		putwcolumns[1]=new nlobjSearchColumn('custrecord_ebiztask_lpno');
		putwcolumns[2]=new nlobjSearchColumn('custrecord_ebiztask_expe_qty');
		putwcolumns[3]=new nlobjSearchColumn('custrecord_ebiztask_tasktype');
		putwcolumns[0].setSort();
		var filtersPutwtsk=new Array();
		filtersPutwtsk[0] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', ['2']);//putaway task
		filtersPutwtsk[1] = new nlobjSearchFilter('name', null, 'is', toRecord.getFieldValue('tranid'));
		filtersPutwtsk[2] = new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'isnotempty');

		closedtaskputwsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filtersPutwtsk, putwcolumns);
		nlapiLogExecution('ERROR','closedtaskputwsearchresults',closedtaskputwsearchresults);

	}
	else
		//nlapiLogExecution('ERROR','opentaskputwsearchresults length',opentaskputwsearchresults.length);
		

	
	var prevlineno=0;var c=1;
	if(opentasksosearchresults!=null && opentasksosearchresults!='' && opentasksosearchresults.length>0)
	{
		nlapiLogExecution('ERROR','opentask results is not null');
		var strcntlp=new Array;var totalchkqty=0;var totalputawayqty=0;var templine;
		for(var i=0;i<opentasksosearchresults.length;i++)
		{
			var lineno=opentasksosearchresults[i].getValue('custrecord_line_no');
			var lp=opentasksosearchresults[i].getValue('custrecord_lpno');
			var chkqty=opentasksosearchresults[i].getValue('custrecord_act_qty');			
			nlapiLogExecution('ERROR','lineno',lineno);
			if(i==0)
			{
				prevlineno=lineno;

			}


			if(chkqty=='')
			{
				chkqty='0';
			}
			var templineno=0;
			var itemscount=toRecord.getLineItemCount('item');
			for(var k=1;k<=toRecord.getLineItemCount('item');k++)
			{
				if(lineno==toRecord.getLineItemValue('item', 'line', k ))
				{
					templineno=k;
				}

			}
			//nlapiLogExecution('ERROR','templineno',templineno);
			var location=toRecord.getFieldValue('transferlocation');
			//fetching the order quantity from Transfer Order
			var ordqty=toRecord.getLineItemValue('item', 'quantity', templineno );
			var item=toRecord.getLineItemValue('item', 'item', templineno );
			var itemtext=toRecord.getLineItemValue('item','custcol_sku_code',templineno);
			nlapiLogExecution('ERROR','itemtext',itemtext);
			var itemdesc=toRecord.getLineItemValue('item','description',templineno);			
			var itemstatus=toRecord.getLineItemValue('item', 'custcol_ebiznet_item_status', templineno );
			var itemreceivedQty=toRecord.getLineItemValue('item', 'quantityreceived', templineno );
			
			//check whether the item is already received
			if(parseFloat(ordqty)!=parseFloat(itemreceivedQty))
			{
				if(c!=1)
				{
					templine = form.getSubList('custpage_items').getLineItemValue('custpage_lineno', c-1);  
					nlapiLogExecution('ERROR','templine',templine);
					//to calculate and display the totals of checkin  quantitys for the lines
					if((parseFloat(templine)!=null)    && (templine != lineno))
					{	

						form.getSubList('custpage_items').setLineItemValue('custpage_lineno', c, 'Total CHKNQTY:'+totalchkqty+',Total PUTAWAYQTY :'+totalputawayqty+'');
						c=c+1;totalchkqty=0;totalputawayqty=0;
						totalchkqty=parseFloat(totalchkqty)+parseFloat(chkqty);		
					}
					else
					{

						totalchkqty=parseFloat(totalchkqty)+parseFloat(chkqty);	
					}
				}
				else
				{
					templine = lineno;
					nlapiLogExecution('ERROR','templine',templine);
					totalchkqty=parseFloat(totalchkqty)+parseFloat(chkqty);
				}

				form.getSubList('custpage_items').setLineItemValue('custpage_lineno', c, lineno);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemtext', c, itemtext);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemdesc', c, itemdesc);
				form.getSubList('custpage_items').setLineItemValue('custpage_orderqty', c, ordqty);
				form.getSubList('custpage_items').setLineItemValue('custpage_checkinqty', c, chkqty);	
				form.getSubList('custpage_items').setLineItemValue('custpage_putconfirmqty',c, '0');
				form.getSubList('custpage_items').setLineItemValue('custpage_lp', c, lp);
				form.getSubList('custpage_items').setLineItemValue('custpage_to', c, request.getParameter('custpage_qbto'));
				form.getSubList('custpage_items').setLineItemValue('custpage_item', c, item);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', c, itemstatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_location', c, location);

				if(opentaskputwsearchresults!=null && opentaskputwsearchresults!='')
				{
					nlapiLogExecution('ERROR','opentaskputwsearchresults is not null');
					nlapiLogExecution('ERROR','opentaskputwsearchresults length',opentaskputwsearchresults.length);
					for(var j=0;j<opentaskputwsearchresults.length;j++)
					{
						var linenonew=opentaskputwsearchresults[j].getValue('custrecord_line_no');
						var lpnew=opentaskputwsearchresults[j].getValue('custrecord_lpno');
						if(linenonew==lineno && lpnew==lp)
						{
							var putconfqty=opentaskputwsearchresults[j].getValue('custrecord_act_qty');	

							form.getSubList('custpage_items').setLineItemValue('custpage_putconfirmqty',c, putconfqty);

							//to calculate and display the totals of putaway  quantitys for the lines
							if((templine!=lineno)&& c!=1)
							{
								totalputawayqty=parseFloat(totalputawayqty)+parseFloat(putconfqty);
							}
							else
							{
								totalputawayqty=parseFloat(totalputawayqty)+parseFloat(putconfqty);
							}
						}
					}
				}
				else if(closedtaskputwsearchresults!=null && closedtaskputwsearchresults!='')
				{
					nlapiLogExecution('ERROR','closedtaskputwsearchresults is not null');
					for(var j=0;j<closedtaskputwsearchresults.length;j++)
					{
						var linenonew=closedtaskputwsearchresults[j].getValue('custrecord_ebiztask_line_no');
						var lpnew=closedtaskputwsearchresults[j].getValue('custrecord_ebiztask_lpno');
						nlapiLogExecution('ERROR','linenonew',linenonew);
						nlapiLogExecution('ERROR','lineno',lineno);
						nlapiLogExecution('ERROR','lpnew',lpnew);
						nlapiLogExecution('ERROR','lp',lp);
						if(linenonew==lineno && lpnew==lp)
						{
							var putconfqty=closedtaskputwsearchresults[j].getValue('custrecord_ebiztask_expe_qty');	
							nlapiLogExecution('ERROR','putconfqty',putconfqty);

							form.getSubList('custpage_items').setLineItemValue('custpage_putconfirmqty',c, putconfqty); 

							//to calculate and display the totals of putaway  quantitys for the lines
							if((templine!=lineno)&& c!=1)
							{
								totalputawayqty=parseFloat(totalputawayqty)+parseFloat(putconfqty);
							}
							else
							{
								totalputawayqty=parseFloat(totalputawayqty)+parseFloat(putconfqty);
							}
						}
					}
				}

//				nlapiLogExecution('ERROR','totalchkqty',totalchkqty);
//				nlapiLogExecution('ERROR','totalputawayqty',totalputawayqty);
//				nlapiLogExecution('ERROR','opentasksosearchresults.length',opentasksosearchresults.length);
//				nlapiLogExecution('ERROR','i',i);
				if(i==opentasksosearchresults.length-1)
				{
					c=c+1;
					form.getSubList('custpage_items').setLineItemValue('custpage_lineno', c, 'Total CHKNQTY :'+totalchkqty+',Total PUTAWAYQTY :'+totalputawayqty+'');
					totalchkqty=0;totalputawayqty=0;
				}
				else
				{
					c=c+1;
				}
				prevlineno=lineno;
			}
			else
			{
				nlapiLogExecution('ERROR','templine',templine);
				nlapiLogExecution('ERROR','lineno',lineno);
				nlapiLogExecution('ERROR','c',c);
				nlapiLogExecution('ERROR','totalchkqty',totalchkqty);
				nlapiLogExecution('ERROR','totalputawayqty',totalputawayqty);
				if((templine !=null) && (templine!=lineno) && (c!=1))
				{
					form.getSubList('custpage_items').setLineItemValue('custpage_lineno', c, 'Total CHKNQTY:'+totalchkqty+',Total PUTAWAYQTY :'+totalputawayqty+'');
					//totalchkqty=0;totalputawayqty=0;
					templine=null;
				}

			}
		}
		//Get the button
		var button = form.getButton('submitter');		 
		//Relabel the button's UI label
		button.setLabel('Submit');
	}
	else
	{		
		form.getSubList('custpage_items').setLineItemValues(opentasksosearchresults);
		//Get the button
		var button = form.getButton('submitter');		 
		//Relabel the button's UI label
		button.setLabel('Display');
	}
	
	nlapiLogExecution('ERROR','out of displaySublist');

}


//this method is to fetch more than 1000 records (transfer orders)
var TempSearchResults=new Array();
function getSearchResultsOFTo(maxno)
{

	var filters = new Array();

	filters[0] = new nlobjSearchFilter( 'mainline', null, 'is', 'T' );
	filters[1] = new nlobjSearchFilter( 'internalidnumber', null, 'greaterthan', maxno);
	filters[2] = new nlobjSearchFilter( 'transferlocation', null, 'anyof', ['1']);

	var columns=new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	var searchResults = nlapiSearchRecord('transferorder', null, filters, columns, null );
	TempSearchResults.push(searchResults);
	if(searchResults.length>=1000)
	{
		var mno=searchResults[searchResults.length-1].getId();	
		getSearchResultsOFSo(mno);
	}

	return TempSearchResults;
}
function ConfirmtoNs(item,itemstatus,location,lineno,trecord,orderqty,putconfQty,adjustNsInventoryArray)
{

	try
	{
//		var trecord = nlapiTransformRecord('transferorder', ToID, 'itemreceipt');
		var confirmedLines='';	
		if(trecord!=null)
		{
			var tolinelength = trecord.getLineItemCount('item');
			var totranid=trecord.getFieldValue('tranid');
			for (var j = 1; j <= tolinelength; j++) {
				var item_id = trecord.getLineItemValue('item', 'item', j);

				var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
				var itemLineNo = trecord.getLineItemValue('item', 'line', j);
				var commitflag = 'N';
				var quantity=orderqty;

				if ((item == item_id || itemLineNo == lineno) && (itemrec == 'F')){
					//isvalid=true;index[count]=item;count=count+1;
					//	adjustNsInventoryArray[adjustNsInventoryArray.length]=new Array(2);
					adjustNsInventoryArray[adjustNsInventoryArray.length]=[item,lineno,orderqty,putconfQty,itemstatus,location];
					//adjustNsInventoryArray[adjustNsInventoryArray.length][1]=lineno;
					commitflag = 'Y';			
					Itype = nlapiLookupField('item', item, 'recordType');

					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', quantity);				

					if (Itype == "serializedinventoryitem") {									
						var serialline = new Array();
						var serialId = new Array();

						var tempSerial = "";
						var filters = new Array();
						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is',totranid);
						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

						if (serchRec) {
							for (var n = 0; n < serchRec.length; n++) {
								//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
								if (tempserialId == "") {
									tempserialId = serchRec[n].getId();
								}
								else {
									tempserialId = tempserialId + "," + serchRec[n].getId();
								}

								//This is for Serial num loopin with Space separated.
								if (tempSerial == "") {
									tempSerial = serchRec[n].getValue('custrecord_serialnumber');
								}
								else {
									tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

								}
							}

							serialnumcsv = tempSerial;
							tempSerial = "";
						}

						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);


					}
					else 
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {

							var tempBatch = "";
							var batchcsv = "";
							var batfilter = new Array();
							batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
							//batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
							batfilter[1] = new nlobjSearchFilter('name', null, 'is', totranid);
							batfilter[2] = new nlobjSearchFilter('custrecord_line_no', null, 'is', lineno);
							//batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);
							var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, new nlobjSearchColumn('custrecord_lotnowithquantity'));
							if (batchsearchresults) {
								for (var n = 0; n < batchsearchresults.length; n++) {
									//This is for Batch num loopin with Space separated.
									if (tempBatch == "") {
										tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
									}
									else {
										tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');

									}
								}

								batchcsv = tempBatch;
								tempBatch = "";
							}


							trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
						}

					trecord.commitLineItem('item');

				}

				if (commitflag == 'N') {

					trecord.selectLineItem('item', j);

					trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');

					trecord.commitLineItem('item');

				}
			}		
		}
	}
	catch(exp) {
		nlapiLogExecution('ERROR','exception',exp.getDetails());
	}
}
function AdjustNsInventory(item,lineno,orderqty,putconfQty,itemstatus,location,totranid)
{
	var vTaskType=11;
	var adjustmenttype='';	var lot='';


	var stockadjfilters = new Array();
	stockadjfilters[0] = new nlobjSearchFilter('custrecord_adjusttasktype', null, 'anyof', 11);	
	stockadjfilters[1] = new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'anyof', location);

	var stockadjcolumns = new Array();
	stockadjcolumns[0] = new nlobjSearchColumn('name');
	var serialnumcsv = "";
	var serchRec = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, stockadjfilters, stockadjcolumns);
	if(serchRec!=null)
	{
		adjustmenttype=serchRec[0].getId();
	}
	nlapiLogExecution('ERROR', 'adjustmenttype', adjustmenttype);
	var Itype = nlapiLookupField('item', item, 'recordType');
	var totext=totranid;	
	if (Itype == "serializedinventoryitem") {									
		var serialline = new Array();
		var serialId = new Array();
		var tempSerial = "";
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', totext);
		filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var serialnumcsv = "";
		var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
		if (serchRec) {
			for (var n = 0; n < serchRec.length; n++) {
				//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
				if (tempserialId == "") {
					tempserialId = serchRec[n].getId();
				}
				else {
					tempserialId = tempserialId + "," + serchRec[n].getId();
				}
				//This is for Serial num loopin with Space separated.
				if (tempSerial == "") {
					tempSerial = serchRec[n].getValue('custrecord_serialnumber');
				}
				else {
					tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

				}
			}

			lot = tempSerial;
			tempSerial = "";
		}

		nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
		nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);

	}
	else 
		if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
			var tempBatch = "";
			var batchcsv = "";
			var batfilter = new Array();
			batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);			
			batfilter[1] = new nlobjSearchFilter('name', null, 'is', totext);
			batfilter[2] = new nlobjSearchFilter('custrecord_line_no', null, 'is', lineno);

			var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, new nlobjSearchColumn('custrecord_lotnowithquantity'));

			if (batchsearchresults) {
				for (var n = 0; n < batchsearchresults.length; n++) {
					//This is for Batch num loopin with Space separated.
					if (tempBatch == "") {
						tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
					}
					else {
						tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');

					}
				}

				lot = tempBatch;
				tempBatch = "";
			}


		}
	var adjustqty=parseFloat(orderqty)-parseFloat(putconfQty);
	InvokeNSInventoryAdjustment(item,itemstatus,location,-(adjustqty),'',vTaskType,adjustmenttype,lot);
}