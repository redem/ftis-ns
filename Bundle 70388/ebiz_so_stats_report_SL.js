/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_order_history_SL.js,v $
 *     	   $Revision: 1.7 $
 *     	   $Date: 2011/12/01 15:14:35 $
 *     	   $Author: gkalla $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_order_history_SL.js,v $
 * 
 *****************************************************************************/


function ebiznet_OrderHistoryDetails(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Order Statistics Report');
		var vQbWave = "";

		//var salesorder = form.addField('custpage_so', 'select', 'Order#','salesorder');	
		var fromDate = form.addField('custpage_fromdt', 'date', 'From Date');
		var toDate = form.addField('custpage_todt', 'date', 'To Date');
		fromDate.setMandatory(true);
		toDate.setMandatory(true);
		//fillsalesorderField(form, salesorder,-1);

		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else //this is the POST block
	{

		var form = nlapiCreateForm('Order Statistics Report');
		var fromdt = request.getParameter('custpage_fromdt');
		var todt = request.getParameter('custpage_todt');
		//var salesorder = form.addField('custpage_so', 'select', 'Order#','salesorder').setLayoutType("startrow");
		form.addSubmitButton('Display');
		var fromDate = form.addField('custpage_fromdt', 'date', 'From Date').setLayoutType("normal", "startcol");
		var toDate = form.addField('custpage_todt', 'date', 'To Date').setLayoutType("normal", "startcol");
		fromDate.setMandatory(true);
		toDate.setMandatory(true);

		fromDate.setDefaultValue(fromdt);
		toDate.setDefaultValue(todt);

		nlapiLogExecution('ERROR', 'From Date ',fromdt);
		nlapiLogExecution('ERROR', 'To Date ',todt);
		// To get the Pending SOs List
		var ShipFilters = new Array(); 
		ShipFilters.push(new nlobjSearchFilter('datecreated', null, 'within', fromdt,todt));
		ShipFilters.push(new nlobjSearchFilter('status', null, 'noneof', ['SalesOrd:G','SalesOrd:H','SalesOrd:A','SalesOrd:F']));
		ShipFilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));



		/*var Shipcolumns=new Array();
		Shipcolumns.push(new nlobjSearchColumn('custrecord_ship_trackno'));*/
		//Shipcolumns.push(new nlobjSearchColumn('createddate'));
		var NoofPendSOs=0;
		var columns=new Array();
		columns.push(new nlobjSearchColumn('datecreated'));
		columns.push(new nlobjSearchColumn('status'));
		nlapiLogExecution('ERROR', 'Hi21 ');
		var SOSearchResults = nlapiSearchRecord('salesorder', null, ShipFilters, columns);	

		nlapiLogExecution('ERROR', 'Hi2 ');
		if(SOSearchResults != null && SOSearchResults != "")
		{
			nlapiLogExecution('ERROR', 'Hi3');
			nlapiLogExecution('ERROR', 'SOSearchResults.length', SOSearchResults.length);
			NoofPendSOs=SOSearchResults.length;
		}



		form.addField('custpage_socount', 'text', 'Total Pending Sales Orders : ').setLayoutType("startrow").setDisplayType('inline').setDefaultValue(NoofPendSOs);

		// To get No of Sales Orders/Pick tickets : 
		var NoofSOperPick=0;
		var SOperPickFilter = new Array(); 
		SOperPickFilter.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', fromdt,todt));
		SOperPickFilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','8','28'])); 

		var columnsPerPick=new Array();
		columnsPerPick.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
		//columnsPerPick.push(new nlobjSearchColumn('custrecordact_begin_date'));
		columnsPerPick[0].setSort();

		var SOSearchResultsperPick = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOperPickFilter, columnsPerPick);
		var vPrevSO;
		if(SOSearchResultsperPick != null && SOSearchResultsperPick.length > 0){
			for (var i = 0; i < SOSearchResultsperPick.length ; i++) {	
				var vPresSO=SOSearchResultsperPick[i].getValue('custrecord_ebiz_order_no');
				if(vPresSO != null && vPresSO != "")
				{
					if(vPrevSO == null || vPrevSO == "")
					{
						vPrevSO=vPresSO;
						NoofSOperPick=1;
					}
					if(vPrevSO != vPresSO)
					{	
						vPrevSO=vPresSO;
						NoofSOperPick = NoofSOperPick +1;
					}
						
				} 
			}
		}



		form.addField('custpage_soperpick', 'text', 'Number of  sales orders/pick tickets generated : ').setLayoutType("startrow").setDisplayType('inline').setDefaultValue(NoofSOperPick);
		
		// To get No of Picks Completed :

		 



		/*var Shipcolumns=new Array();
		Shipcolumns.push(new nlobjSearchColumn('custrecord_ship_trackno'));*/
		//Shipcolumns.push(new nlobjSearchColumn('createddate'));
		var NoofPickCompSO=0;
		var NoofPickCompSOLines=0;
		var SOperPickFilter = new Array(); 
		SOperPickFilter.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'within', fromdt,todt));
		SOperPickFilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28']));
		 

		var columnsPerPick=new Array();
		columnsPerPick.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
		//columnsPerPick.push(new nlobjSearchColumn('custrecordact_begin_date'));
		columnsPerPick[0].setSort();

		var SOSearchResultsperPick = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOperPickFilter, columnsPerPick);
		var vPrevSO2;
		if(SOSearchResultsperPick != null && SOSearchResultsperPick.length > 0){
			for (var i = 0; i < SOSearchResultsperPick.length ; i++) {	
				var vPresSO2=SOSearchResultsperPick[i].getValue('custrecord_ebiz_order_no');
				if(vPresSO2 != null && vPresSO2 != "")
				{
					NoofPickCompSOLines = NoofPickCompSOLines +1;
					if(vPrevSO2 == null || vPrevSO2 == "")
					{
						vPrevSO2=vPresSO2;
						NoofPickCompSO=1;
					}
					if(vPrevSO2 != vPresSO2)
					{	
						vPrevSO2=vPresSO2;
						NoofPickCompSO = NoofPickCompSO +1;
					}
						
				} 
			}
		}



		form.addField('custpage_pickscomp', 'text', 'Number of  picks completed : ').setLayoutType("startrow").setDisplayType('inline').setDefaultValue(NoofPickCompSO + ' Orders('+ NoofPickCompSOLines +' Lines)');

		/*var searchresults = nlapiLoadRecord('salesorder', soid);
		var NsTrack="";
		if(searchresults.getFieldValue('linkedtrackingnumbers') != null && searchresults.getFieldValue('linkedtrackingnumbers') != "")
			{
		var trackArr =  searchresults.getFieldValue('linkedtrackingnumbers').split(' ');
		nlapiLogExecution('ERROR', 'trackArr', trackArr.length);
		NsTrack = trackArr.join(','); 
			}

		nlapiLogExecution('ERROR', 'trackArr2', trackArr);



		form.addField('custpage_sono', 'text', 'SO# : ').setLayoutType("startrow").setDisplayType('inline').setDefaultValue(searchresults.getFieldValue('tranid'));	
		form.addField('custpage_creation_date', 'text', 'Creation Date : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldValue('createddate'));
		form.addField('custpage_customer', 'text', 'Customer : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(searchresults.getFieldValue('entityname'));
		form.addField('custpage_location', 'text', 'Location : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldText('location'));
		form.addField('custpage_ordtype', 'text', 'Order Type : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldText('custbody_nswmssoordertype'));
		form.addField('custpage_ordpriority', 'text', 'Order Priority : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(searchresults.getFieldText('custbody_nswmspriority'));
		var createfulfill = form.addField('custpage_fulfillflag', 'text', 'Create Fulfillment Flag : ').setDisplayType('inline');

		if(searchresults.getFieldValue('custbody_create_fulfillment_order') != null && searchresults.getFieldValue('custbody_create_fulfillment_order') != "" && searchresults.getFieldValue('custbody_create_fulfillment_order') == 'T')		
		      createfulfill.setDefaultValue('Yes');
		else
			createfulfill.setDefaultValue('No');
		form.addField('custpage_ordflag', 'text', 'Order Flag : ').setDisplayType('inline');
		form.addField('custpage_shipterms', 'text', 'Shipping Terms : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(searchresults.getFieldText('shipmethod'));
		form.addField('custpage_billingterms', 'text', 'Billing Terms : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldText('terms'));
		form.addField('custpage_carrier', 'text', 'Carrier : ').setDisplayType('inline').setDefaultValue(searchresults.getFieldText('custbody_salesorder_carrier'));
		form.addField('custpage_nstrackno', 'text', 'NS Tracking# :').setDisplayType('inline').setDefaultValue(NsTrack);
		form.addField('custpage_ebiztrackno', 'text', 'Ship Manifest Tracking# :').setDisplayType('inline').setDefaultValue(eBizTrackingNo);
		//added NS Status field by suman
		if(searchresults.getFieldValue('status') != null && searchresults.getFieldValue('status') != "" )
		form.addField('custpage_ebiznsstatus', 'text', 'Sales Order Status :').setDisplayType('inline').setDefaultValue(searchresults.getFieldValue('status'));
		//end of NS Status field

		//form.addSubmitButton('Display');

		var sublist = form.addSubList("custpage_items", "list", "Order History");

		sublist.addField("custpage_fo", "text", "FO#");
		sublist.addField("custpage_line", "text", "Line#");
		sublist.addField("custpage_createdt", "text", "Date Created");	
		sublist.addField("custpage_item", "text", "Item");

		sublist.addField("custpage_dropship", "text", "Drop Ship");
		sublist.addField("custpage_nsordqty", "text", "NS Order Qty");

		sublist.addField("custpage_nspickqty", "text", "NS Picked Qty");
		sublist.addField("custpage_nsshipqty", "text", "NS Ship Qty");
		//sublist.addField("custpage_nstrackno", "text", "NS Tracking#");
		sublist.addField("custpage_ordqty", "text", "Whse Order Qty");
		sublist.addField("custpage_wave", "text", "Wave#");
		//sublist.addField("custpage_location", "text", "Location");
		sublist.addField("custpage_wavereldate", "text", "Wave Released Date");
		sublist.addField("custpage_pickgendqty", "text", "Pick Gen Qty");
		sublist.addField("custpage_isprinted", "text", "Pick List Printed?");
		sublist.addField("custpage_pickconfdt", "text", "Pick Confirmed Date");
		sublist.addField("custpage_pickqty", "text", "Whse Pick Qty");
		sublist.addField("custpage_confdt", "text", "Pack Confirmed Date");
		sublist.addField("custpage_fulfillref", "text", "Item Fulfillment Ref#");
		sublist.addField("custpage_shipqty", "text", "Whse Ship Qty");
		//sublist.addField("custpage_manifesttrackno", "text", "Ship Manifest Tracking#");
		//sublist.addField("custpage_manifestdt", "text", "Ship Manifest Date");		
		sublist.addField("custpage_status", "text", "Status");



		//for Fulfillment Order
		var salessearchfilters = new Array(); 
		salessearchfilters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soid)); 
		var columns=new Array();
		columns.push(new nlobjSearchColumn('custrecord_lineord'));
		columns.push(new nlobjSearchColumn('custrecord_ordline'));
		columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
		columns.push(new nlobjSearchColumn('custrecord_pickgen_qty'));
		columns.push(new nlobjSearchColumn('custrecord_pickqty'));
		columns.push(new nlobjSearchColumn('custrecord_ship_qty'));
	    columns.push(new nlobjSearchColumn('custrecord_ebiz_linesku'));
	    columns.push(new nlobjSearchColumn('custrecord_record_linedate'));
	    columns.push(new nlobjSearchColumn('custrecord_linestatus_flag'));
	    //columns.push(new nlobjSearchColumn('custrecord_ebiz_shipdate'));
	    columns.push(new nlobjSearchColumn('custrecord_printflag'));



	    columns[0].setSort();
	    columns[1].setSort();
	    columns.push(new nlobjSearchColumn('custrecord_ebiz_wave'));	

		var salessearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, salessearchfilters, columns);



		if(salessearchresults != null)
		{
			form.setScript('customscript_ordersummary_details');
			form.addButton('custpage_print','Print','Printreport()');
		}
		//var createdsoord=0,createdsolines=0,fulfillsoord=0,fulfillsolines=0,inpickssoord=0,inpickssolines=0,pickconfirmedsoord=0,pickconfirmedsolines=0,Shippedsoord=0,Shippedsolines=0;
      if(salessearchresults != null && salessearchresults != "")
	{
		for (var i = 0; i < salessearchresults.length; i++) {

			nlapiLogExecution('ERROR', 'i', i);
			var ordno=salessearchresults[i].getValue('custrecord_lineord');
			var ordline=salessearchresults[i].getValue('custrecord_ordline');
			var ordqty=salessearchresults[i].getValue('custrecord_ord_qty');
			var pickgenqty=salessearchresults[i].getValue('custrecord_pickgen_qty');
			//var ordno=salessearchresults[i].getValue('custrecord_pickgen_qty',null,'sum');
			var pickqty=salessearchresults[i].getValue('custrecord_pickqty');
			var shipqty=salessearchresults[i].getValue('custrecord_ship_qty');
			var sku=salessearchresults[i].getText('custrecord_ebiz_linesku');
			var ebizwave=salessearchresults[i].getValue('custrecord_ebiz_wave');
			var createDate=salessearchresults[i].getValue('custrecord_record_linedate');
			var fulfillStatus=salessearchresults[i].getText('custrecord_linestatus_flag');
			var fulfillStatusValue=salessearchresults[i].getValue('custrecord_linestatus_flag');
			nlapiLogExecution('ERROR', 'fulfillStatus', fulfillStatus);
			//var ShipDate=salessearchresults[i].getText('custrecord_ebiz_shipdate');
			var PrintFlag=salessearchresults[i].getText('custrecord_printflag');
			if(PrintFlag == null || PrintFlag == "")
				PrintFlag='No';

			// To get the data from NS Sales Order Items
			var nsOrdQty;
			var nsPickQty;
			var nsShipQty;
			var createpo;
			var DropShip='No';

			// To fetch quantities from Sales Order

			var salesorderFilers = new Array();
			salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'F')); 
		salesorderFilers.push(new nlobjSearchFilter('internalid', null, 'is', soid)); 
		//salesorderFilers.push(new nlobjSearchFilter('line', null, 'is', ordline)); 
			var columns=new Array();			 
		columns.push(new nlobjSearchColumn('quantityshiprecv'));
		columns.push(new nlobjSearchColumn('quantitypicked'));
		columns.push(new nlobjSearchColumn('quantity'));
		columns.push(new nlobjSearchColumn('line'));

			var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,columns);
if(customerSearchResults != null && customerSearchResults != "")
	{
	for(var m=0;m<=customerSearchResults.length;m++)
		{
		var NSlineNo = customerSearchResults[m].getValue('line');
		if(NSlineNo == ordline)
    	{

    	if(customerSearchResults[m].getValue('quantity') != null &&  customerSearchResults[m].getValue('quantity') != '')
    		nsOrdQty=customerSearchResults[m].getValue('quantity');
    	else
    		nsOrdQty="0";

    	if(customerSearchResults[m].getValue('quantitypicked') != null && customerSearchResults[m].getValue('quantitypicked') != '')
    		nsPickQty=customerSearchResults[m].getValue('quantitypicked');
    	else
    		nsPickQty="0";

    	if(customerSearchResults[m].getValue('quantityshiprecv') != null && customerSearchResults[m].getValue('quantityshiprecv') != '')
    		nsShipQty=customerSearchResults[m].getValue('quantityshiprecv');
    	else
    		nsShipQty="0";

    	if(searchresults.getLineItemValue('item','createpo',ordline) != null && searchresults.getLineItemValue('item','createpo',ordline).trim() != '')
    		createpo=searchresults.getLineItemValue('item','createpo',ordline);
    	if(createpo != null && createpo != "")
    		DropShip='Yes';
    	else
    		DropShip='No';
    	break;
    	}
		}
	}


			var vLineCount=searchresults.getLineItemCount('item');
			for(var m=1;m<=vLineCount;m++)
			{
				var NSlineNo = searchresults.getLineItemValue('item','line',m);
			    if(NSlineNo == ordline)
			    	{

			    	if(searchresults.getLineItemValue('item','quantity',m) != null &&  searchresults.getLineItemValue('item','quantity',m).trim() != '')
			    		nsOrdQty=searchresults.getLineItemValue('item','quantity',m);
			    	else
			    		nsOrdQty="0";

			    	if(searchresults.getLineItemValue('item','quantitypicked',m) != null && searchresults.getLineItemValue('item','quantitypicked',m).trim() != '')
			    		nsPickQty=searchresults.getLineItemValue('item','quantitypicked',m);
			    	else
			    		nsPickQty="0";

			    	if(searchresults.getLineItemValue('item','quantityfulfilled',m) != null && searchresults.getLineItemValue('item','quantityfulfilled',m).trim() != '')
			    		nsShipQty=searchresults.getLineItemValue('item','quantityfulfilled',m);
			    	else
			    		nsShipQty="0";

			    	if(searchresults.getLineItemValue('item','createpo',m) != null && searchresults.getLineItemValue('item','createpo',m).trim() != '')
			    		createpo=searchresults.getLineItemValue('item','createpo',m);
			    	if(createpo != null && createpo != "")
			    		DropShip='Yes';
			    	else
			    		DropShip='No';
			    	break;
			    	}
			}
			nlapiLogExecution('ERROR', 'nsPickQty', nsPickQty);
			nlapiLogExecution('ERROR', 'createpo', createpo);


			if(ordno != null && ordno != "" && ordline != null && ordline != "")
				{
			var Opensearchfilters = new Array(); 
			Opensearchfilters.push(new nlobjSearchFilter('name', null, 'is', ordno));
			Opensearchfilters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', ordline));
			Opensearchfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', soid));

			var Opencolumns=new Array();
			Opencolumns.push(new nlobjSearchColumn('custrecordact_begin_date'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_act_end_date'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_pack_confirmed_date'));
			Opencolumns.push(new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no'));



			var OpenTasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Opensearchfilters, Opencolumns);
				}
			var WaveRelDt="";
			var pickConfDt="";
			var PackConfDt="";
			var ItemFulfillRef="";
			nlapiLogExecution('ERROR', 'after Open task search', "after Open task search");
			var ClosedTasksearchresults;

			if(OpenTasksearchresults != null && OpenTasksearchresults != "")
			{
				nlapiLogExecution('ERROR', 'OpenTasksearchresults', OpenTasksearchresults.length);
				if(OpenTasksearchresults[0].getValue("custrecordact_begin_date") != null && OpenTasksearchresults[0].getValue("custrecordact_begin_date") != "")
		    	{
					WaveRelDt=OpenTasksearchresults[0].getValue("custrecordact_begin_date");
		    	}

			    if(OpenTasksearchresults[0].getValue("custrecord_act_end_date") != null && OpenTasksearchresults[0].getValue("custrecord_act_end_date") != "")
			    	{
			    	pickConfDt=OpenTasksearchresults[0].getValue("custrecord_act_end_date");
			    	}
			    if(OpenTasksearchresults[0].getValue("custrecord_pack_confirmed_date") != null && OpenTasksearchresults[0].getValue("custrecord_pack_confirmed_date") != "")
		    	{
			    	PackConfDt=OpenTasksearchresults[0].getValue("custrecord_pack_confirmed_date");
		    	}
			    if(OpenTasksearchresults[0].getValue("custrecord_ebiz_nsconfirm_ref_no") != null && OpenTasksearchresults[0].getValue("custrecord_ebiz_nsconfirm_ref_no") != "")
		    	{
			    	ItemFulfillRef=OpenTasksearchresults[0].getValue("custrecord_ebiz_nsconfirm_ref_no");
		    	}
			}
			else
				{
				  if(pickqty != null && pickqty != "" && parseInt(pickqty)>0)
					  {
					  // To fetch the data from closed task if data moved to closed task
					   var Closedsearchfilters = new Array(); 
					   Closedsearchfilters.push(new nlobjSearchFilter('name', null, 'is', ordno));
					   Closedsearchfilters.push(new nlobjSearchFilter('custrecord_ebiztask_line_no', null, 'is', ordline));
					   Closedsearchfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', soid));

						var Closedcolumns=new Array();
						Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_act_begin_date'));
						Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_act_end_date'));
						Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiztask_pack_confirmed_date'));
						Closedcolumns.push(new nlobjSearchColumn('custrecord_ebiz_nsconf_refno_ebiztask'));

						ClosedTasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, Closedsearchfilters, Closedcolumns);

						if(ClosedTasksearchresults != null && ClosedTasksearchresults != "")
						{
							nlapiLogExecution('ERROR', 'OpenTasksearchresults', ClosedTasksearchresults.length);
							if(ClosedTasksearchresults[0].getValue("custrecordact_begin_date") != null && ClosedTasksearchresults[0].getValue("custrecordact_begin_date") != "")
					    	{
								WaveRelDt=ClosedTasksearchresults[0].getValue("custrecordact_begin_date");
					    	}

						    if(ClosedTasksearchresults[0].getValue("custrecord_act_end_date") != null && ClosedTasksearchresults[0].getValue("custrecord_act_end_date") != "")
						    	{
						    	pickConfDt=ClosedTasksearchresults[0].getValue("custrecord_act_end_date");
						    	}
						    if(ClosedTasksearchresults[0].getValue("custrecord_pack_confirmed_date") != null && ClosedTasksearchresults[0].getValue("custrecord_pack_confirmed_date") != "")
					    	{
						    	PackConfDt=ClosedTasksearchresults[0].getValue("custrecord_pack_confirmed_date");
					    	}
						    if(ClosedTasksearchresults[0].getValue("custrecord_ebiz_nsconfirm_ref_no") != null && ClosedTasksearchresults[0].getValue("custrecord_ebiz_nsconfirm_ref_no") != "")
					    	{
						    	ItemFulfillRef=ClosedTasksearchresults[0].getValue("custrecord_ebiz_nsconfirm_ref_no");
					    	}
						}

					  }
				}

			form.getSubList('custpage_items').setLineItemValue('custpage_pickconfdt', i + 1, pickConfDt);
			form.getSubList('custpage_items').setLineItemValue('custpage_confdt', i + 1, PackConfDt);
			form.getSubList('custpage_items').setLineItemValue('custpage_fulfillref', i + 1, ItemFulfillRef);	
			form.getSubList('custpage_items').setLineItemValue('custpage_wavereldate', i + 1, WaveRelDt);	


			form.getSubList('custpage_items').setLineItemValue('custpage_fo', i + 1, ordno);
			form.getSubList('custpage_items').setLineItemValue('custpage_line', i + 1, ordline);
			form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, ordqty);			
			form.getSubList('custpage_items').setLineItemValue('custpage_pickgendqty', i + 1, pickgenqty);			 
			form.getSubList('custpage_items').setLineItemValue('custpage_pickqty', i + 1, pickqty);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipqty', i + 1, shipqty);
			form.getSubList('custpage_items').setLineItemValue('custpage_item', i + 1, sku);
			form.getSubList('custpage_items').setLineItemValue('custpage_wave', i + 1, ebizwave);
			form.getSubList('custpage_items').setLineItemValue('custpage_createdt', i + 1, createDate);
			form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, fulfillStatus);			 
			form.getSubList('custpage_items').setLineItemValue('custpage_isprinted', i + 1, PrintFlag);


			form.getSubList('custpage_items').setLineItemValue('custpage_nsordqty', i + 1, nsOrdQty);
			form.getSubList('custpage_items').setLineItemValue('custpage_nspickqty', i + 1, nsPickQty);
			form.getSubList('custpage_items').setLineItemValue('custpage_nsshipqty', i + 1, nsShipQty);
			form.getSubList('custpage_items').setLineItemValue('custpage_dropship', i + 1, DropShip);

			if(shipqty != null && shipqty != "" && parseFloat(shipqty)>0 && (fulfillStatusValue == 8 || fulfillStatusValue == 28 ))// Ship qty is not null and status with either pick confirmed or pack completed then status changing to Shipped
				{
				fulfillStatus="Shipped";
				}
			else if(PackConfDt != null && PackConfDt != "" && fulfillStatusValue == 8) // if pick confirm date not null but status with STATUS.OUTBOUND.PICK_CONFIRMED then changing to Pack completed 
				{
				nlapiLogExecution('ERROR', 'Pack completd');
				fulfillStatus="Pack Completed";
				}
			if(fulfillStatusValue == '15')// for STATUS.OUTBOUND.SELECTED_INTO_WAVE
				{
				nlapiLogExecution('ERROR', 'Pick gen failed');
				fulfillStatus="Pickgen failed";
				}
			else if(fulfillStatusValue == '25') // for STATUS.INBOUND_OUTBOUND.EDIT
				{
				nlapiLogExecution('ERROR', 'Edit status');
				fulfillStatus="Open";
				}

			nlapiLogExecution('ERROR', 'fulfillStatusValue', fulfillStatusValue);
			nlapiLogExecution('ERROR', 'PackConfDt', PackConfDt);
			nlapiLogExecution('ERROR', 'fulfillStatus', fulfillStatus);

			form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, fulfillStatus);

		}
	}*/
		response.writePage(form);
	}
}


function fillsalesorderField(form, salesorderField,maxno){
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	if(maxno!=-1)
	{
		salesorderFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseInt(maxno)));
	}


	salesorderField.addSelectOption("", "");

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('tranid');
	columns[0].setSort(true);
	columns[1]=new nlobjSearchColumn('internalid');
	columns[2]=new nlobjSearchColumn('type');


	var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,columns);


	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('tranid') != null && customerSearchResults[i].getValue('tranid') != "" && customerSearchResults[i].getValue('tranid') != " ")
		{
			var resdo = form.getField('custpage_so').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		salesorderField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var column=new Array();
		column[0]=new nlobjSearchColumn('tranid');		
		column[1]=new nlobjSearchColumn('internalid');
		column[1].setSort(true);

		var OrderSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,column);

		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillsalesorderField(form, salesorderField,maxno);	

	}
}

function Printreport(){

	var salessearchfilters = new Array();
	var tempstring;
	if ((nlapiGetFieldValue('custpage_fromdate') != "" && nlapiGetFieldValue('custpage_fromdate') != null) && (nlapiGetFieldValue('custpage_todate') != "" && nlapiGetFieldValue('custpage_todate') != null) ) {

		tempstring="&custpage_fromdate="+nlapiGetFieldValue('custpage_fromdate')+"&custpage_todate="+nlapiGetFieldValue('custpage_todate');
	} 
	if ((nlapiGetFieldValue('custpage_fulfillmentorder') != "" && nlapiGetFieldValue('custpage_fulfillmentorder') != null)){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_fulfillmentorder="+nlapiGetFieldValue('custpage_fulfillmentorder');
		}
		else
		{
			tempstring="&custpage_fulfillmentorder="+nlapiGetFieldValue('custpage_fulfillmentorder');
		}

	}	
	if (nlapiGetFieldValue('custpage_order') !=null && nlapiGetFieldValue('custpage_order') !=""){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_order="+nlapiGetFieldValue('custpage_order');
		}
		else
		{
			tempstring="&custpage_order="+nlapiGetFieldValue('custpage_order');
		}

	}
	if (nlapiGetFieldValue('custpage_wave') !=null && nlapiGetFieldValue('custpage_wave') !=""){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_wave="+nlapiGetFieldValue('custpage_wave');
		}
		else
		{
			tempstring="&custpage_wave="+nlapiGetFieldValue('custpage_wave');
		}

	}

	if (nlapiGetFieldValue('custpage_customer') !=null && nlapiGetFieldValue('custpage_customer') !=""){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_customer="+nlapiGetFieldValue('custpage_customer');
		}
		else
		{
			tempstring="&custpage_customer="+nlapiGetFieldValue('custpage_customer');
		}

	}
	if (nlapiGetFieldValue('custpage_location') !=null && nlapiGetFieldValue('custpage_location') !=""){
		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_location="+nlapiGetFieldValue('custpage_location');
		}
		else
		{
			tempstring="&custpage_location="+nlapiGetFieldValue('custpage_location');
		}

	}     
	if (nlapiGetFieldValue('custpage_salesorder') !=null && nlapiGetFieldValue('custpage_salesorder') !=""){
		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_salesorder="+nlapiGetFieldValue('custpage_salesorder');
		}
		else
		{
			tempstring="&custpage_salesorder="+nlapiGetFieldValue('custpage_salesorder');
		}

	}    
	var BolPDFURL = nlapiResolveURL('SUITELET', 'customscript_ordsummarydetailsreport', 'customdeploy_ordsummarydetailsreport');
	var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		BolPDFURL = 'https://system.netsuite.com' + BolPDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			BolPDFURL = 'https://system.sandbox.netsuite.com' + BolPDFURL;				
		}	
	BolPDFURL=BolPDFURL+tempstring;
	window.open(BolPDFURL);
}
/**
 * Function to return the current date in mm/dd/yyyy format
 * @author Phani
 * @returns Current system date
 */
function DateStamp(){
	var now = new Date();
	return ((parseInt(now.getMonth()) + 1) + '/' + (parseInt(now.getDate())) + '/' + now.getFullYear());
}
