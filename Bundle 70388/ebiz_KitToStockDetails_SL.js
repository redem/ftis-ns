/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_KitToStockDetails_SL.js,v $
 *     	   $Revision: 1.25.2.20.4.7.2.46.2.4 $
 *     	   $Date: 2015/11/16 13:10:17 $
 *     	   $Author: gkalla $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_KitToStockDetails_SL.js,v $
 * Revision 1.25.2.20.4.7.2.46.2.4  2015/11/16 13:10:17  gkalla
 * case# 201415544
 * In confirm assembly build quantities are wrong.
 *
 * Revision 1.25.2.20.4.7.2.46.2.3  2015/11/13 16:51:49  sponnaganti
 * case# 201414372
 * 2015.2 issue fix
 *
 * Revision 1.25.2.20.4.7.2.46.2.2  2015/11/09 15:21:37  schepuri
 * case# 201415477
 *
 * Revision 1.25.2.20.4.7.2.46.2.1  2015/09/21 14:02:19  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.25.2.20.4.7.2.46  2015/07/06 11:23:53  snimmakayala
 * Case#: 201413050
 *
 * Revision 1.25.2.20.4.7.2.45  2015/05/13 12:58:21  schepuri
 * case# 201412753
 *
 * Revision 1.25.2.20.4.7.2.44  2015/04/17 15:40:25  skreddy
 * Case# 201412401
 * truefab prod issue fix
 *
 * Revision 1.25.2.20.4.7.2.43  2015/04/16 12:35:42  schepuri
 * case# 201412354
 *
 * Revision 1.25.2.20.4.7.2.42  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.25.2.20.4.7.2.41  2015/04/08 14:47:07  skreddy
 * Case# 201412194
 * JB Prod  issue fix
 *
 * Revision 1.25.2.20.4.7.2.40  2015/03/13 15:29:07  skreddy
 * Case# 201412029
 * JB Prod issue fix
 *
 * Revision 1.25.2.20.4.7.2.39  2014/11/28 15:39:41  skavuri
 * Case# 201411044 Std bundle issue fixed
 *
 * Revision 1.25.2.20.4.7.2.38  2014/11/14 04:44:46  vmandala
 * case#  201410773 stdbundle issue fixed
 *
 * Revision 1.25.2.20.4.7.2.37  2014/09/25 18:11:53  skreddy
 * case # 201410541
 * Jawbone prod issue fix
 *
 * Revision 1.25.2.20.4.7.2.36  2014/09/15 07:58:47  skavuri
 * Case# 201410279 SB Issue Fixed
 *
 * Revision 1.25.2.20.4.7.2.35  2014/09/10 16:13:21  skavuri
 * Case# 201410279 Std Bundle issue fixed
 *
 * Revision 1.25.2.20.4.7.2.34  2014/08/25 15:49:10  nneelam
 * case# 201410105 , 201410106
 * jawbone Issue Fix.
 *
 * Revision 1.25.2.20.4.7.2.33  2014/08/20 15:26:17  rmukkera
 * Case # 20148417,20148562
 *
 * Revision 1.25.2.20.4.7.2.32  2014/08/14 10:02:37  sponnaganti
 * case# 20149992
 * stnd bundle issue fix
 *
 * Revision 1.25.2.20.4.7.2.31  2014/08/08 14:59:10  nneelam
 * case# 20149745
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.25.2.20.4.7.2.30  2014/08/04 09:11:15  nneelam
 * case#  20149371
 * Stanadard Bundle Partial WO Issue Fix.
 *
 * Revision 1.25.2.20.4.7.2.29  2014/07/31 15:51:30  sponnaganti
 * Case# 20149787
 * Stnd Bundle Issue fix
 *
 * Revision 1.25.2.20.4.7.2.28  2014/07/29 05:54:02  skreddy
 * case # 20149374
 * jawbone SB issue fix
 *
 * Revision 1.25.2.20.4.7.2.27  2014/07/08 15:37:55  skreddy
 * case # 20147960
 * Compatability issue fix
 *
 * Revision 1.25.2.20.4.7.2.26  2014/07/08 15:35:48  sponnaganti
 * case# 20149240
 * Compatibility issue fix
 *
 * Revision 1.25.2.20.4.7.2.25  2014/07/07 15:42:25  skreddy
 * case # 20149241
 * Compatability issue fix
 *
 * Revision 1.25.2.20.4.7.2.24  2014/04/16 16:00:50  gkalla
 * case#20148019
 * PCT WO outbound stage issue
 *
 * Revision 1.25.2.20.4.7.2.23  2014/03/24 08:15:59  nneelam
 * case#  20124024
 * Roynet Issue Fix.
 *
 * Revision 1.25.2.20.4.7.2.22  2014/02/21 14:58:54  rmukkera
 * Case# 20127203
 *
 * Revision 1.25.2.20.4.7.2.21  2014/02/14 06:24:48  skreddy
 * case 20127123
 * Nucourse SB issue fix
 *
 * Revision 1.25.2.20.4.7.2.20  2014/02/06 14:39:01  skreddy
 * case 20127087
 * Nucourse SB issue fix
 *
 * Revision 1.25.2.20.4.7.2.19  2013/10/25 16:12:23  skreddy
 * Case# 20125252
 * standard bundle  issue fix
 *
 * Revision 1.25.2.20.4.7.2.18  2013/09/27 15:12:15  schepuri
 * Bin Location is not getting displayed in Inventory report
 *
 * Revision 1.25.2.20.4.7.2.17  2013/09/26 15:47:17  skreddy
 * Case# 20124024
 * Merge LP  issue fix
 *
 * Revision 1.25.2.20.4.7.2.16  2013/09/25 06:48:44  schepuri
 * bin loc is not getting displayed in ivn report
 *
 * Revision 1.25.2.20.4.7.2.15  2013/09/19 15:18:53  rmukkera
 * Case# 20124445
 *
 * Revision 1.25.2.20.4.7.2.14  2013/09/06 15:04:44  skreddy
 * Case# 20124290
 * standard bundle issue fix
 *
 * Revision 1.25.2.20.4.7.2.13  2013/08/21 16:54:30  grao
 * Issue Fix related to
 * 20123966
 *
 * Revision 1.25.2.20.4.7.2.12  2013/08/21 14:21:24  skreddy
 * Case# 20123541
 * issue rellated to expiry date for item in NS
 *
 * Revision 1.25.2.20.4.7.2.11  2013/08/09 21:55:17  grao
 * GFT Issue Fixes, Lot Numbers not displayed in Confirm Assembly Build screen
 *
 * Revision 1.25.2.20.4.7.2.10  2013/07/25 07:00:27  skreddy
 * Case# 20123540
 * diaplay lot# in drop down
 *
 * Revision 1.25.2.20.4.7.2.9  2013/06/28 16:24:20  skreddy
 * Case#:20123234
 * Monobind SB issue fix
 *
 * Revision 1.25.2.20.4.7.2.8  2013/06/03 07:49:17  gkalla
 * CASE201112/CR201113/LOG201121
 * Getting lot numbers from inventory changed for PCT
 *
 * Revision 1.25.2.20.4.7.2.7  2013/05/09 12:50:37  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue related to work order whose parent item has only one single item as its member item.
 *
 * Revision 1.25.2.20.4.7.2.6  2013/05/02 15:22:50  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.25.2.20.4.7.2.5  2013/04/04 20:20:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixes
 *
 * Revision 1.25.2.20.4.7.2.4  2013/03/19 11:46:26  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.25.2.20.4.7.2.3  2013/03/08 14:43:37  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.25.2.20.4.7.2.2  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.25.2.20.4.7.2.1  2013/02/28 06:43:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Monobind.
 *
 * Revision 1.25.2.20.4.7  2013/02/19 00:51:39  gkalla
 * CASE201112/CR201113/LOG201121
 * Issue fixing
 *
 * Revision 1.25.2.20.4.6  2013/01/11 13:23:15  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to error while doing confirm assembly build
 *
 * Revision 1.25.2.20.4.5  2013/01/03 14:51:55  skreddy
 * CASE201112/CR201113/LOG201121
 * Inserted in new Lot# and expiry date in batch entry
 *
 * Revision 1.25.2.20.4.4  2012/12/28 16:29:51  skreddy
 * CASE201112/CR201113/LOG201121
 *  issue related to item dimensions
 *
 * Revision 1.25.2.20.4.3  2012/12/07 14:29:31  skreddy
 * CASE201112/CR201113/LOG201121
 * to process confirm build tthrough UI and picking from RF
 *
 * Revision 1.25.2.20.4.2  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.25.2.20.4.1  2012/10/26 08:02:57  gkalla
 * CASE201112/CR201113/LOG201121
 * Lot# exception enhancement in work orders
 *
 * Revision 1.25.2.17  2012/08/10 15:39:06  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned version
 *
 * Revision 1.25.2.16  2012/07/18 06:28:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Item Status list.
 *
 * Revision 1.25.2.15  2012/06/07 15:55:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Added inbound functionality to main item using put strategies
 *
 * Revision 1.25.2.14  2012/05/28 09:41:37  gkalla
 * CASE201112/CR201113/LOG201121
 * Palletisation of main item
 *
 * Revision 1.25.2.13  2012/05/17 16:12:12  gkalla
 * CASE201112/CR201113/LOG201121
 * Assembly issue fixed
 *
 * Revision 1.25.2.12  2012/05/16 09:49:14  gkalla
 * CASE201112/CR201113/LOG201121
 * Assembly build issue fixed
 *
 * Revision 1.25.2.10  2012/04/24 14:54:10  gkalla
 * CASE201112/CR201113/LOG201121
 * To accept multiple locations and to get bin location details based on selected bin location dropdown
 *
 * 
 *****************************************************************************/


function addAllWorkOrdersToField(workorder, workorderList,form){
	if(workorderList != null && workorderList.length > 0){
		for (var i = 0; i < workorderList.length ; i++) {	
			var res=  form.getField('custpage_workorder').getSelectOptions(workorderList[i].getValue('tranid'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			workorder.addSelectOption(workorderList[i].getValue('internalid'), workorderList[i].getValue('tranid'));
//			for(var i = 0; i < workorderList.length; i++){
//			workorder.addSelectOption(workorderList[i].getValue('internalid'), workorderList[i].getValue('tranid'));
//			}
		}
	}
}	


function getAllWorkOrders(){

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['WorkOrd:B','WorkOrd:D']));


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[0].setSort(true);

	var searchResults = nlapiSearchRecord('workorder', null, filters, columns);	


	return searchResults;
}

//to bind AssemblyItem Lot# to dropdown
function getKitItemLotno(vKitItem,vLocation,KitItemLotno){

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', vKitItem));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'is',vLocation ));


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[2] = new nlobjSearchColumn('custrecord_ebizsku');
	columns[0].setSort(true);

	var batchResults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);	

	if(batchResults != null && batchResults.length > 0){
		for (var i = 0; i < batchResults.length ; i++) {	

			KitItemLotno.addSelectOption(batchResults[i].getValue('internalid'), (batchResults[i].getValue('custrecord_ebizlotbatch')+"_"+batchResults[i].getText('custrecord_ebizsku')));
		}
	}
}


//To bind Item status for Kit ItemItem for that Location
function BindKitItemItemStatus(vLocation,vItemStatus){

	nlapiLogExecution('ERROR', 'Itemstatus_vItemStatus ',vItemStatus );
	nlapiLogExecution('ERROR', 'Itemstatus_vLocation',vLocation );
	var filterStatus = new Array();

	filterStatus.push(new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T'));
	filterStatus.push(new nlobjSearchFilter('custrecord_activeskustatus', null, 'is','T'));
	if(vLocation != null && vLocation != '')
		filterStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof',vLocation));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('name');
	columns[0].setSort(true);

	var searchresultsStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filterStatus, columns);

	if(searchresultsStatus != null && searchresultsStatus != '' && searchresultsStatus.length>0)
	{
		for(var d=0;d<searchresultsStatus.length;d++)
		{
			vItemStatus.addSelectOption(searchresultsStatus[d].getValue('internalid'), searchresultsStatus[d].getValue('name'));
		}
	}

}







function CheckKitItemLotno(vKitItemlot,vLocation,ItemId,ExpiryDate){
	nlapiLogExecution('ERROR', 'vKitItemlot ',vKitItemlot );
	nlapiLogExecution('ERROR', 'vLocation ',vLocation );
	nlapiLogExecution('ERROR', 'ItemId ',ItemId );
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', vKitItemlot));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'is',vLocation ));
	filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',ItemId ));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[2] = new nlobjSearchColumn('custrecord_ebizsku');
	columns[0].setSort(true);
	var batchResults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);	
	if(batchResults != null && batchResults.length > 0){
		var vlotid= batchResults[0].getId();
		return vlotid;
	}
	else
	{
		var now = new Date();
		var dt='';
		var dtsettingFlag = 'MM';
		if(dtsettingFlag == 'DD')
		{
			dt=((parseFloat(now.getDate())) + '/' + (parseFloat(now.getMonth()) + 1) + '/' +now.getFullYear());
		}
		else
		{
			dt=((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + (parseFloat(now.getFullYear()) + 2));
		}
		nlapiLogExecution('ERROR', 'now ',dt );

		var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
		customrecord.setFieldValue('name', vKitItemlot);
		customrecord.setFieldValue('custrecord_ebizlotbatch', vKitItemlot);
		customrecord.setFieldValue('custrecord_ebizmfgdate', DateStamp());
		//customrecord.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
		if(ExpiryDate!=null && ExpiryDate!='')
		{
			customrecord.setFieldValue('custrecord_ebizexpirydate', ExpiryDate);
		}
		else
		{
			customrecord.setFieldValue('custrecord_ebizexpirydate', dt);
		}
		//customrecord.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
		//customrecord.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
		//customrecord.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
		nlapiLogExecution('ERROR', 'Item Id',ItemId);
		//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
		customrecord.setFieldValue('custrecord_ebizsku', ItemId);
		customrecord.setFieldValue('custrecord_ebizsitebatch',vLocation);	
		//customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);

		var rec = nlapiSubmitRecord(customrecord, false, true);
		nlapiLogExecution('ERROR', 'rec ',rec );
		return rec;

	}
}

function kitToStockDtlsSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		//create the Suitelet form
		var form = nlapiCreateForm('Confirm Assembly Build');


		//var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		var WorkOrder  = form.addField('custpage_workorder', 'text','Work Order').setDisplayType('hidden');

		//WorkOrder.addSelectOption("","");			
		// Retrieve all sales orders
		//var WorkOrderList = getAllWorkOrders();		
		// Add all sales orders to SO Field
		WorkOrder.setDefaultValue(request.getParameter('custpage_workorder'));

		//addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);


		var qty;
		var mainqty;
		var location;
		var woid;
		var item;
		var tranid;
		var itemid;
		var lineItem;
		var LineQty;
		var LineLP;
		var LineLocation;
		var LineLotNo;
		var Lineno;
		var MainItemLotno;
		var ApproveFlag;
		var ResultFlag;
		var ExpiryDate;  
		var kitItemStatus="";
		var built="";
		var woid=request.getParameter('custpage_workorder');
		nlapiLogExecution('ERROR','woid',woid);
		var wosearchresultsitem='';
		if(woid!=null && woid!='')
		{
			nlapiLogExecution('ERROR','INTOwoid1',woid);
			wosearchresultsitem = nlapiLoadRecord('workorder', woid);
			var lineItems= wosearchresultsitem.getLineItemCount('item');
			if(wosearchresultsitem!=null && wosearchresultsitem!='') 
			{ 	    	
				var vWorkOrderText= form.addField('custpage_workorder_name', 'text','Work Order').setDisplayType('inline');

				qty = wosearchresultsitem.getFieldValue('quantity'); 	
				mainqty = wosearchresultsitem.getFieldValue('quantity');

				nlapiLogExecution('ERROR','qty',qty);
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('internalid', null, 'is', woid);
				filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');//ship task


				var columns = new Array();
				columns[0] = new nlobjSearchColumn('quantityshiprecv');

				var searchresultsWaveNo = nlapiSearchRecord('workorder', null, filters, columns);
				if(searchresultsWaveNo!=null && searchresultsWaveNo!=''){
					built = searchresultsWaveNo[0].getValue('quantityshiprecv');
				}
				nlapiLogExecution('ERROR','built',built);
				if(built==null || built =="")
					built=0;
				qty = parseFloat(qty) - parseFloat(built);

				//built = wosearchresultsitem.getFieldValue('built'); 

				nlapiLogExecution('ERROR','qty',qty);
				nlapiLogExecution('ERROR','built',built);

				location = wosearchresultsitem.getFieldValue('location'); 
				woid = wosearchresultsitem.getFieldValue('id'); 
				tranid = wosearchresultsitem.getFieldValue('tranid');
				item=wosearchresultsitem.getFieldText('assemblyitem'); 
				itemid=wosearchresultsitem.getFieldValue('assemblyitem'); 
				MainItemLotno = wosearchresultsitem.getFieldValue('custbody_ebiz_lotnumber');
				ApproveFlag = wosearchresultsitem.getFieldValue('custbody_ebiz_wo_qc_status');
				ExpiryDate=wosearchresultsitem.getFieldValue('custbody_ebiz_expirydate');
				nlapiLogExecution('ERROR','ExpiryDate',ExpiryDate);
				nlapiLogExecution('ERROR','MainItemLotno',MainItemLotno);
				nlapiLogExecution('ERROR','ApproveFlag',ApproveFlag);
				vWorkOrderText.setDefaultValue(tranid);
			}
		}

		if(ApproveFlag=='2')//if Approved
		{
			ResultFlag =true;
			/*var WOItemcount =0;// Items Count from WorkOrder
			for(var k=1;k<lineItems;k++)
			{
				var CompItemType = wosearchresultsitem.getLineItemValue('item', 'itemtype', k);
				if(CompItemType != 'Service' && CompItemType != 'NonInvtPart')
				{
					WOItemcount= parseInt(WOItemcount)+1;
				}
			}
			var ResultFlag =CheckComponentItems(woid,WOItemcount);
			nlapiLogExecution('ERROR','ResultFlag ',ResultFlag);*/
		}
		else//Pending
		{
			nlapiLogExecution('ERROR','Approve flag ',ApproveFlag);
			//var ErrorMsg = nlapiCreateError('CannotAllocate','Please Approve this Work Order:' + vWOId, true);
			//throw ErrorMsg; //throw this error object, do not catch it
			showInlineMessage(form, 'Error','Please Approve this Work order ' + woid + '', '');
			response.writePage(form);
		}
		if(ResultFlag == true)
		{
			var soDate = form.addField('custpage_sodate', 'text', 'Date');
			soDate.setLayoutType('normal', 'startrow');
			soDate.setDefaultValue(request.getParameter('custpage_sodate'));


			var itemText= form.addField('cusppage_item_text', 'text','Kit Item').setDisplayType('inline').setDefaultValue(item);
			//itemText.setDisplayType('inline');
			var item= form.addField('cusppage_item', 'text','Kit Item').setDisplayType('hidden');
			/*var item = form.addField('cusppage_item', 'select', 'Kit Item', 'item');
			item.setLayoutType('normal', 'startrow');*/

			if(request.getParameter('custparam_item')!='select')
			{

				item.setDefaultValue(request.getParameter('custparam_item'));	
			}
			else
			{
				item.setDefaultValue(itemid);
				nlapiLogExecution('ERROR','item.setDefaultValue(itemid);',itemid);
			}

			var kitQty = form.addField('custpage_kitqty', 'text', 'Qty');
			kitQty.setLayoutType('normal', 'startrow');
			/*if(request.getParameter('custpage_kitqty') != null && request.getParameter('custpage_kitqty') != "")
			{
				kitQty.setDefaultValue(request.getParameter('custpage_kitqty'));
			}

			else*/
			{
				kitQty.setDefaultValue(parseFloat(qty));	
			}
			kitQty.setDisplayType('inline');



			varAssemblyQty=request.getParameter('custpage_kitqty');


			var kitQtyexp = form.addField('custpage_kitqtyexp', 'text', 'Exp Qty');
			kitQtyexp.setLayoutType('normal', 'startrow');

			if(request.getParameter('custpage_kitqty') != null && request.getParameter('custpage_kitqty') != "")
			{
				kitQtyexp.setDefaultValue(parseFloat(request.getParameter('custpage_kitqty')));
			}

			else
			{
				kitQtyexp.setDefaultValue(parseFloat(qty));	
			}

			//kitQtyexp.setDefaultValue(qty);	





			//var LPNo = form.addField('custpage_lp', 'text', 'LP #').setMandatory(true); //Case#201410279
			var LPNo = form.addField('custpage_lp', 'text', 'LP #'); 
			//var SerialNo = form.addField("custpage_lotnoserialno", "select", "Serial/Lot#", "customrecord_ebiznet_batch_entry").setDisplayType('entry');

			//Inserting KitItem Lotno in BatchEntry if required
			var MainItemLotid='';
			if(MainItemLotno != null && MainItemLotno !='')
			{
				MainItemLotid = CheckKitItemLotno(MainItemLotno,location,itemid,ExpiryDate);
				nlapiLogExecution('ERROR','MainItemLotid',MainItemLotid);
			}

			var ItemType= nlapiLookupField('item', itemid, 'recordType');
			nlapiLogExecution('ERROR', 'itemType1', ItemType);
			var batchflag="F";	
			//Binding KitItem Lotno's to dropdown 
			var KitItemLotno = form.addField("custpage_lotnoserialno", "select", "Serial/Lot#");
			KitItemLotno.addSelectOption("","");
			if(ItemType!=null && ItemType!='')
			{
				var fields = ['recordType','custitem_ebizbatchlot'];
				var columns = nlapiLookupField('item', itemid, fields);
				ItemType= columns.recordType;
				batchflag= columns.custitem_ebizbatchlot;
				nlapiLogExecution('ERROR', 'itemType', ItemType);
				if(ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" || ItemType == "serializedinventoryitem" || batchflag=='T')
				{
					//nlapiLogExecution('ERROR', 'itemType2', itemType);
					KitItemLotno.setMandatory(true);
				}
				else
				{
					//nlapiLogExecution('ERROR', 'itemType3', itemType);
					KitItemLotno.setMandatory(false);
				}
			}
			getKitItemLotno(itemid,location,KitItemLotno);
			if(MainItemLotid != null && MainItemLotid !='')
			{
				KitItemLotno.setDefaultValue(MainItemLotid);

			}
			//var SerialNo = form.addField('custpage_lotnoserialno', 'text', 'Serial/Lot#'); 
			var WOinternalid = form.addField('custpage_wointernalid', 'text', 'Wid').setDisplayType('hidden');  

			//customrecord_ebiznet_location
			//var binloc = form.addField('custpage_binlocation','select','Bin Location','customrecord_ebiznet_location').setMandatory(true); //Case# 201410279
			var binloc = form.addField('custpage_binlocation','select','Bin Location','customrecord_ebiznet_location');

			var site = form.addField('cusppage_site', 'select', 'Location', 'location').setDisplayType('inline').setMandatory(true);
			site.setLayoutType('normal', 'startrow');
			nlapiLogExecution('ERROR','Loc',request.getParameter('custparam_locaiton'));
			if(request.getParameter('custparam_locaiton')!='')
			{
				site.setDefaultValue(request.getParameter('custparam_locaiton'));	
			}
			else
			{
				site.setDefaultValue(location);	
			}

			var Sointernalid = form.addField('custpage_sointernalid', 'text', 'Sointernalid').setDisplayType('hidden');

			var NotesField = form.addField('custpage_notes','text','Notes');
			NotesField.setLayoutType('normal', 'startrow');


			var chknCompany = form.addField('custpage_company', 'select', 'Company', 'customrecord_ebiznet_company');
			chknCompany.setDefaultValue(request.getParameter('custpage_company'));
			chknCompany.setDisplayType('inline');

			//var ItemStatus= form.addField('custpage_itemstatus','select','Item Status','customrecord_ebiznet_sku_status').setMandatory(true);

			var ItemStatus = form.addField("custpage_itemstatus", "select", "Item Status").setMandatory(true);
			ItemStatus.addSelectOption("","");
			//to bind itemstatus for kitItem
			BindKitItemItemStatus(location,ItemStatus);

			form.addField("custpage_lpmerge", "checkbox", "Merge LP").setDefaultValue('F');
			var now = new Date();
			//a Date object to be used for a random value
			var now = new Date();
			//now= now.getHours();
			//Getting time in hh:mm tt format.
			var a_p = "";
			var d = new Date();
			var curr_hour = now.getHours();
			if (curr_hour < 12) {
				a_p = "am";
			}
			else {
				a_p = "pm";
			}
			if (curr_hour == 0) {
				curr_hour = 12;
			}
			if (curr_hour > 12) {
				curr_hour = curr_hour - 12;
			}

			var curr_min = now.getMinutes();

			curr_min = curr_min + "";

			if (curr_min.length == 1) {
				curr_min = "0" + curr_min;
			}

			var bgDate = form.addField('custpage_bgdate', 'text', 'BeginDate');      
			bgDate.setDisplayType('hidden');
			bgDate.setDefaultValue(DateStamp());

			var bgTime = form.addField('custpage_bgtime', 'text', 'BeginTime');      
			bgTime.setDisplayType('hidden');
			bgTime.setDefaultValue((curr_hour) + ":" + (curr_min) + " " + a_p);

			var vMainQty = form.addField('custpage_mainqty', 'text', 'Main Qty');      
			vMainQty.setDisplayType('hidden');
			vMainQty.setDefaultValue(mainqty);

			nlapiLogExecution('ERROR','beforeitemsublist');
			//Adding  ItemSubList
			form.setScript('customscript_ebiz_kitting_checklotno_cl');
			var ItemSubList = form.addSubList("custpage_deliveryord_items", "list", "ItemList");
			ItemSubList.addField("custpage_deliveryord_sku", "select", "Component","item").setDisplayType('inline');
			ItemSubList.addField("custpage_deliveryord_memqty", "text", "Required Qty").setDisplayType('hidden');       
			ItemSubList.addField("custpage_deliveryord_lp", "text", "LP #");        
			ItemSubList.addField("custpage_deliveryord_location", "select", "Bin Location", "customrecord_ebiznet_location");
			ItemSubList.addField("custpage_deliveryord_expqty", "text", "Qty");
			ItemSubList.addField("custpage_deliveryord_qtyexp", "text", "Exp Qty").setDisplayType('hidden');;
			//ItemSubList.addField("custpage_skurecid", "text", "Recid").setDisplayType('hidden');
			ItemSubList.addField("custpage_skurecid", "text", "Recid").setDisplayType('hidden');
			//ItemSubList.addField("custpage_lotno", "text", "Serial/Lot#").setDisplayType( 'entry'); 
			ItemSubList.addField("custpage_lineno", "text", "Line #"); 
			ItemSubList.addField("custpage_hiddenlotno", "text", "lotno").setDisplayType('hidden');//added by santosh
			ItemSubList.addField("custpage_hiddenlotnotext", "text", "lottext").setDisplayType('hidden');//added by santosh
			ItemSubList.addField("custpage_hiddenlocation", "text", "location").setDisplayType('hidden');//added by santosh
			ItemSubList.addField("custpage_hiddenitemid", "text", "itemid").setDisplayType('hidden');//added by santosh
			//added by santosh
			var serialno= ItemSubList.addField("custpage_lotno", "select", "Serial/Lot Numbers");
			serialno.addSelectOption('', '');

			//Newly added hidden fields
			ItemSubList.addField("custpage_skuno", "text", "skuNo").setDisplayType('hidden');
			ItemSubList.addField("custpage_cntrlno", "text", "cntrlNo").setDisplayType('hidden');
			ItemSubList.addField("custpage_contlpno", "text", "contLpNo").setDisplayType('hidden');
			ItemSubList.addField("custpage_vtemploctext", "text", "vTempLocText").setDisplayType('hidden');
			ItemSubList.addField("custpage_skustatus", "text", "skuStatus").setDisplayType('hidden');
			ItemSubList.addField("custpage_vpackcode", "text", "vpackcode").setDisplayType('hidden');
			ItemSubList.addField("custpage_vdono", "text", "vdono").setDisplayType('hidden');
			ItemSubList.addField("custpage_vfromlp", "text", "vfromlp").setDisplayType('hidden');
			ItemSubList.addField("custpage_vkitrefno", "text", "vkitrefno").setDisplayType('hidden');
			ItemSubList.addField("custpage_lastkititem", "text", "lastkititem").setDisplayType('hidden');
			ItemSubList.addField("custpage_kititem", "text", "kititem").setDisplayType('hidden');
			ItemSubList.addField("custpage_vserialno", "text", "vserialno").setDisplayType('hidden');
			ItemSubList.addField("custpage_vcompany", "text", "vCompany").setDisplayType('hidden');
			ItemSubList.addField("custpage_vebizwaveno", "text", "vEbizWaveNo").setDisplayType('hidden');
			ItemSubList.addField("custpage_vsointernid", "text", "vSointernid").setDisplayType('hidden');
			ItemSubList.addField("custpage_warehouselocation", "text", "warehouseLocation").setDisplayType('hidden');
			ItemSubList.addField("custpage_vcontainersize", "text", "vcontainersize").setDisplayType('hidden');
			ItemSubList.addField("custpage_vid", "text", "vid").setDisplayType('hidden');
			ItemSubList.addField("custpage_skustatustext", "text", "vid").setDisplayType('hidden');
			ItemSubList.addField("custpage_zoneid", "text", "zoneid").setDisplayType('hidden');
			ItemSubList.addField("custpage_prevwmsstatus", "text", "zoneid").setDisplayType('hidden');
			ItemSubList.addField("custpage_deliveryord_backordqty", "text", "Backorder qty").setDisplayType('hidden');
			ItemSubList.addField("custpage_deliveryord_comitqty", "text", "Commited Qty").setDisplayType('hidden');
			ItemSubList.addField("custpage_deliveryord_bomqty", "text", "BOM Qty").setDisplayType('hidden');
			//End





			var vitem="",strItem="",strQty=0,vcomponent,vlp,vlocation,vqty,vlotno,vlineno;
			var itemvalue=request.getParameter("custparam_item");
			nlapiLogExecution('ERROR','itemvalue',itemvalue);
			if(itemvalue==''|| itemvalue==null || itemvalue=='select')
			{
				itemvalue=itemid;
			}
			nlapiLogExecution('ERROR','itemvalue',itemvalue);
			var ItemType = nlapiLookupField('item', itemvalue, 'recordType');
			nlapiLogExecution('ERROR','ItemType',ItemType);
			var searchresultsitem;
			/*if(ItemType!=null && ItemType !='' && ItemType =='lotnumberedassemblyitem')
    	{
    	 searchresultsitem = nlapiLoadRecord('lotnumberedassemblyitem', request.getParameter("custparam_item")); //1020
    	}
    else if(ItemType!=null && ItemType !='' && ItemType =='kititem')
        	{
        	 searchresultsitem = nlapiLoadRecord('kititem', request.getParameter("custparam_item")); //1020
    	}
    else
    	{
    	searchresultsitem = nlapiLoadRecord('assemblyitem', request.getParameter("custparam_item")); //1020
    	}

			 */

//			The above code is commented by sudheer on 092111 to support for any type of items
			nlapiLogExecution('ERROR','Afteritemsublist');
			if(ItemType!=null && ItemType !='')

			{
				nlapiLogExecution('ERROR','Item Type',ItemType);
				nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);

				searchresultsitem = nlapiLoadRecord(ItemType, itemvalue); //1020
			}

			else
			{
				nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
			}

			//var searchresultsitem = nlapiLoadRecord('assemblyitem', request.getParameter("custparam_item")); //1020
			//var searchresultsitemTemp = nlapiLoadRecord('assemblyitem', request.getParameter("custparam_item")); //1020
			//nlapiLogExecution('ERROR','searchresultsitemTemp',searchresultsitemTemp);

			nlapiLogExecution('ERROR','searchresultsitem',searchresultsitem);
			//var searchresultsitem = nlapiLoadRecord('assemblyitem', item); //1020  

			var SkuNo=searchresultsitem.getFieldValue('itemid');
			nlapiLogExecution('ERROR','SkuNo',SkuNo);   
			var recCount=0; 
			/*var filters = new Array(); 
			//filters[0] = new nlobjSearchFilter('itemid', null, 'is', request.getParameter("custparam_item"));//kit_id is the parameter name 
			filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);//kit_id is the parameter name 
			filters[1] = new nlobjSearchFilter('type', null, 'anyof', 'Assembly');//For Assembly type Items 

			var columns1 = new Array(); 


			columns1[0] = new nlobjSearchColumn( 'memberitem' ); 


			//  columns1[0] = new nlobjSearchColumn( 'internalid' ); 
			columns1[1] = new nlobjSearchColumn( 'memberquantity' );

			var j=0,k=0;

			var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); */

			//for(var i=0; i<searchresults.length;i++) 
			//{ 
//			strItem = searchresults[i].getValue('memberitem');
//			strQty = searchresults[i].getValue('memberquantity');
//			nlapiLogExecution('ERROR','strItem',strItem);
//			nlapiLogExecution('ERROR','strQty',strQty);
//			var fields = ['custitem_item_family', 'custitem_item_group','recordType','custitem_ebizbatchlot'];
//			var batchflag="F";

//			//Added by Sudheer on 121011 to make it general for all types of items

//			//var columns = nlapiLookupField('inventoryitem', strItem, fields);

//			var ComponentItemType = nlapiLookupField('item', strItem, 'recordType');

//			nlapiLogExecution('ERROR','Component Item Type is',ComponentItemType);
//			var vCmpItemType='';
//			if (ComponentItemType != null && ComponentItemType !='')
//			{
//			var columns = nlapiLookupField(ComponentItemType, strItem, fields);
//			var itemfamily = columns.custitem_item_family;
//			var itemgroup = columns.custitem_item_group;
//			vCmpItemType = columns.recordType;
//			batchflag= columns.custitem_ebizbatchlot;
//			}



			//var columns = nlapiLookupField('inventoryitem', strItem, fields);
			//var itemfamily = columns.custitem_item_family;
			//var itemgroup = columns.custitem_item_group;

			//PickStrategieKittoOrder(item, itemfamily, itemgroup, avlqty)    
//			nlapiLogExecution('ERROR','Inventory Search',searchresults[i].getText('memberitem'));
//			avlqty= (parseFloat(strQty)*parseFloat(varAssemblyQty));
			/*var arryinvt=PickStrategieKittoOrder(strItem, itemfamily, itemgroup, avlqty);
			nlapiLogExecution('ERROR','Search End',searchresults[i].getText('memberitem'));
			// var searchresultsLine=getRecord(strItem)
			nlapiLogExecution('ERROR','arryinvt.length',arryinvt.length);
			if(arryinvt.length>0)
			{
				nlapiLogExecution('ERROR', 'Inside If=', i);
				for (j=0; j < arryinvt.length; j++) 
				{			
					var invtarray= arryinvt[j];  	

					vitem =searchresults[i].getValue('memberitem');		      	  		      	  	        
					vlp = invtarray[2];
					vqty = invtarray[0];
					vlocation = invtarray[1];
					var vrecid = invtarray[3];
				}
			}*/


			//code added by santosh on 20Aug12
			var skuarray=new Array();
			var filterLotno = new Array();
			filterLotno[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid);
			filterLotno[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','9']);
			filterLotno[2] = new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty');
			filterLotno[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3);

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('name');
			columns[1] = new nlobjSearchColumn('custrecord_sku');
			columns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[3] = new nlobjSearchColumn('custrecord_lpno');
			columns[4] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[5] = new nlobjSearchColumn('custrecord_line_no');
			columns[6] = new nlobjSearchColumn('custrecord_batch_no');
			columns[7] = new nlobjSearchColumn('custrecord_invref_no');
			columns[8] = new nlobjSearchColumn('custrecord_act_qty');
			columns[9] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			columns[11] = new nlobjSearchColumn('custrecord_sku_status');
			columns[12] = new nlobjSearchColumn('custrecord_packcode');
			columns[13] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[14] = new nlobjSearchColumn('custrecord_kit_refno');
			columns[15] = new nlobjSearchColumn('custrecord_last_member_component');
			columns[16] = new nlobjSearchColumn('custrecord_parent_sku_no');
			columns[17] = new nlobjSearchColumn('custrecord_serial_no');
			columns[18] = new nlobjSearchColumn('custrecord_comp_id');
			columns[19] = new nlobjSearchColumn('custrecord_container_lp_no');
			columns[20] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
			columns[21] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			columns[22] = new nlobjSearchColumn('custrecord_wms_location');
			columns[23] = new nlobjSearchColumn('custrecord_container');
			columns[24] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
			columns[25] = new nlobjSearchColumn('custrecord_wms_status_flag');
			columns[26] = new nlobjSearchColumn('custrecord_actendloc');
			columns[5].setSort();
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterLotno,columns);

			//to get distinct Itemid 
			if(searchresults !=null && searchresults!='')
			{

				for (z=0; z< searchresults.length; z++) 
				{
					var skuid =searchresults[z].getValue('custrecord_sku');	
					nlapiLogExecution('ERROR', 'skuid', skuid);
					if(skuarray.indexOf(skuid)==-1)
					{
						skuarray.push(skuid);
					}

				}
				//end of the code 
				//binding sublist dropdown
				addSerialno(skuarray, location,form,serialno,kitItemStatus);



				for (s=0; s< searchresults.length; s++) 
				{
					nlapiLogExecution('ERROR', 'searchresults', searchresults.length);

					lineItem = searchresults[s].getValue('custrecord_sku');	
					lineItemvalue =searchresults[s].getValue('custrecord_sku');	
					LineQty =searchresults[s].getValue('custrecord_expe_qty');
					LineNo =searchresults[s].getValue('custrecord_line_no');

					//Newly added fields
					var skuNo = searchresults[s].getValue('custrecord_ebiz_sku_no');
					var cntrlNo = searchresults[s].getValue('custrecord_ebiz_cntrl_no');
					var contLpNo = searchresults[s].getValue('custrecord_container_lp_no');
					var vTempLocText = searchresults[s].getText('custrecord_actbeginloc');
					var skuStatus = searchresults[s].getValue('custrecord_sku_status');
					var vpackcode = searchresults[s].getValue('custrecord_packcode');
					var vdono = searchresults[s].getValue('name');
					var vfromlp = searchresults[s].getValue('custrecord_from_lp_no');
					var vkitrefno = searchresults[s].getValue('custrecord_kit_refno');
					var lastkititem = searchresults[s].getValue('custrecord_last_member_component');
					var kititem = searchresults[s].getValue('custrecord_parent_sku_no');
					var vserialno = searchresults[s].getValue('custrecord_serial_no');
					var vCompany = searchresults[s].getValue('custrecord_comp_id');
					//var vContlpNo = searchresults[s].getValue('custrecord_container_lp_no');
					var vEbizWaveNo = searchresults[s].getValue('custrecord_ebiz_wave_no');
					var vSointernid = searchresults[s].getValue('custrecord_ebiz_order_no');
					var warehouseLocation = searchresults[s].getValue('custrecord_wms_location');
					var vcontainersize = searchresults[s].getValue('custrecord_container');
					var vid =  searchresults[s].getId();
					var skuStatusText = searchresults[s].getText('custrecord_sku_status');
					var zoneid = searchresults[s].getValue('custrecord_ebiz_zoneid');
					var prevwmsstatus = searchresults[s].getValue('custrecord_wms_status_flag');
					//End

					var vTempLoc=searchresults[s].getValue('custrecord_actbeginloc');
					var NLotno=searchresults[s].getValue('custrecord_batch_no');
					var LP=searchresults[s].getValue('custrecord_lpno');
					var invtref=searchresults[s].getValue('custrecord_invref_no');
					var ActQty=searchresults[s].getValue('custrecord_act_qty');
					var vActTempLoc=searchresults[s].getValue('custrecord_actendloc');
					nlapiLogExecution('ERROR', 'lineItem', lineItem);
					nlapiLogExecution('ERROR', 'lineItemvalue', lineItemvalue);
					nlapiLogExecution('ERROR', 'LineQty', LineQty);
					nlapiLogExecution('ERROR', 'LineQty', LP);
					nlapiLogExecution('ERROR', 'vTempLoc', vTempLoc);
					nlapiLogExecution('ERROR', 'vActTempLoc', vActTempLoc);
					if(vActTempLoc !=null && vActTempLoc !='' && vActTempLoc !='null')
					{
						if(vActTempLoc != vTempLoc)
							vTempLoc=vActTempLoc;

					}

					nlapiLogExecution('ERROR', 'vTempLoc', vTempLoc);

					//addSerialno(lineItemvalue, location,form,s,serialno);

					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku',s+1, lineItem);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_memqty', s + 1, ActQty);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_expqty',s+1,  LineQty);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_qtyexp',s+1,  LineQty);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenitemid',s+1, lineItemvalue);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lineno',s+1, LineNo);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lp',s+1, LP);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_location',s+1, vTempLoc);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlocation',s+1, vTempLoc);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_skurecid', s+1, invtref);

					//Start
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_skuno', s+1, skuNo);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_cntrlno', s+1, cntrlNo);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_contlpno', s+1, contLpNo);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vtemploctext', s+1, vTempLocText);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_skustatus', s+1, skuStatus);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vpackcode', s+1, vpackcode);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vdono', s+1, vdono);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vfromlp', s+1, vfromlp);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vkitrefno', s+1, vkitrefno);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lastkititem', s+1, lastkititem);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_kititem', s+1, kititem);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vserialno', s+1, vserialno);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vcompany', s+1, vCompany);
					//form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_skurecid', s+1, vContlpNo);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vebizwaveno', s+1, vEbizWaveNo);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vsointernid', s+1, vSointernid);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_warehouselocation', s+1, warehouseLocation);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vcontainersize', s+1, vcontainersize);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_vid', s+1, vid);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_skustatustext', s+1, skuStatusText);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_zoneid', s+1, zoneid);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_prevwmsstatus', s+1, prevwmsstatus);

					//End

					if(NLotno != "" && NLotno != null)
					{
						nlapiLogExecution('ERROR', 'Lotno', NLotno);	
						var filterbatch = new Array();
						filterbatch[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', NLotno);
						filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', lineItemvalue);
						//case 20123540 : start
						if(location != null && location != '')
							filterbatch[2] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', location);

						//end
						var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch);
						if(receiptsearchresults!=null)
						{
							var lotid= receiptsearchresults[0].getId();
							nlapiLogExecution('ERROR', 'lotid', lotid);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lotno',s+1, lotid );
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlotno',s+1,  lotid);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlotnotext',s+1,  NLotno);
						}
					}
					if(wosearchresultsitem !=null && wosearchresultsitem !='')
					{
						var vLineCount=wosearchresultsitem.getLineItemCount('item');
						var itemQty=wosearchresultsitem.getFieldValue('quantity');
						for(var i=1;i<=vLineCount;i++)
						{		
							var WOLineNo =wosearchresultsitem.getLineItemValue('item','line',i)+".0";
							if(WOLineNo== null || WOLineNo =="")
								WOLineNo="&nbsp;";

							var WOlineItemvalue = wosearchresultsitem.getLineItemValue('item', 'item', i);

							var WOlineItem = wosearchresultsitem.getLineItemText('item', 'item', i);
							if(WOlineItem== null || WOlineItem =="")
								WOlineItem="";					


							var WOLineQty = wosearchresultsitem.getLineItemValue('item','quantity',i);
							if(WOLineQty== null || WOLineQty =="")
								WOLineQty="&nbsp;";

							var BomQty =WOLineQty/itemQty;
							if(BomQty== null || BomQty =="")
								BomQty="&nbsp;";


							var BackOrdQty = 0;
							if(wosearchresultsitem.getLineItemValue('item', 'quantitybackordered', i) != null && wosearchresultsitem.getLineItemValue('item', 'quantitybackordered', i) != "")
								BackOrdQty=parseFloat(wosearchresultsitem.getLineItemValue('item', 'quantitybackordered', i));

							var CommittedQty = wosearchresultsitem.getLineItemValue('item', 'quantitycommitted', i);
							if(CommittedQty== null || CommittedQty =="" || CommittedQty == 'undefined')
								CommittedQty="";
							nlapiLogExecution('ERROR', 'BackOrdQty',BackOrdQty);
							nlapiLogExecution('ERROR', 'WOLineNo',WOLineNo);
							nlapiLogExecution('ERROR', 'LineNo',LineNo);
							nlapiLogExecution('ERROR', 'WOlineItemvalue',WOlineItemvalue);
							nlapiLogExecution('ERROR', 'lineItem',lineItem);


							if(parseInt(WOLineNo)==parseInt(LineNo) && WOlineItemvalue==lineItem)
							{
								nlapiLogExecution('ERROR', 'if conditon','sucess');
								//form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_itemdesc', vCount, ItemDesc);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_backordqty', s+1, BackOrdQty);			
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_comitqty', s+1, CommittedQty);					
								nlapiLogExecution('ERROR', 'a9','a9');
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_bomqty', s+1, parseFloat(BomQty).toFixed(5));
								break;
							}
						}	

					}
				} 
			}
			else
			{
				//nlapiLogExecution('ERROR','Approve flag ',ApproveFlag);
				showInlineMessage(form, 'Error','Bin locations are not yet generated for this Work order', '');
				response.writePage(form);
				return false;

			}
//			{

//			lineItem = wosearchresultsitem.getLineItemText('item', 'item', m);	
//			lineItemvalue = wosearchresultsitem.getLineItemValue('item', 'item', m);	
//			//LineQty =wosearchresultsitem.getLineItemValue('item','quantity',m);
//			LineQty =wosearchresultsitem.getLineItemValue('item','custcol_ebizwoavalqty',m);
//			//LineLocation=wosearchresultsitem.getLineItemValue('item','custcol_locationsitemreceipt',m);

//			LineLocation=wosearchresultsitem.getLineItemValue('item','custcol_ebizwobinloc',m);
//			//LineLocation=wosearchresultsitem.getLineItemValue('item','custcol_ebizwobinlocvalue',m);
//			LineLP =wosearchresultsitem.getLineItemValue('item','custcol_ebiz_lp',m);
//			LineNo =wosearchresultsitem.getLineItemValue('item','custcol_ebizwolotlineno',m);
//			nlapiLogExecution('ERROR', 'LineNo', LineNo);
//			LineLotNo =wosearchresultsitem.getLineItemValue('item','custcol_ebizwolotinfo',m);

//			var VRec=wosearchresultsitem.getLineItemValue('item','custcol_ebizwoinventoryref',m);
//			nlapiLogExecution('ERROR', 'VRec', VRec);
//			var vRecArr=new Array();
//			if(VRec!= null && VRec != "")
//			vRecArr=VRec.split(',');

//			nlapiLogExecution('ERROR', 'LineLP', LineLP);
//			var vRecArrLP=new Array();
//			if(LineLP!= null && LineLP != "")
//			vRecArrLP=LineLP.split(',');
//			nlapiLogExecution('ERROR', 'vRecArrLP.length', vRecArrLP.length);

//			var vRecArrLot=new Array();
//			if(LineLotNo!= null && LineLotNo != "")
//			vRecArrLot=LineLotNo.split(',');


//			var vRecArrBin=new Array();
//			if(LineLocation!= null && LineLocation != "")
//			vRecArrBin=LineLocation.split(',');

//			nlapiLogExecution('ERROR', 'vRecArr.length', vRecArr.length);

//			var vRecArrQty=new Array();
//			if(LineQty!= null && LineQty != "")
//			vRecArrQty=LineQty.split(',');

//			addSerialno(lineItemvalue,location,form,m,serialno)//santosh

//			for(var k=0;k<vRecArr.length;k++)
//			{


//			nlapiLogExecution('ERROR', 'lineItem', LineQty);
//			nlapiLogExecution('ERROR', 'lineItemvalue', lineItemvalue);
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku',s+1, lineItem);
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_memqty',s+1,  LineQty);
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenitemid',s+1, lineItemvalue);//added by santosh


//			nlapiLogExecution('ERROR', 'vRecArrBin[k]', vRecArrBin[k]);

//			var filtersinvt = new Array();

//			filtersinvt[0] = new nlobjSearchFilter('name', null, 'is', vRecArrBin[k]);

//			var columnsinvt = new Array();
//			columnsinvt[0] = new nlobjSearchColumn('internalid'); 
//			columnsinvt[1] = new nlobjSearchColumn('name'); 
//			var vTempLoc=vRecArrBin[k];

//			var searchresultsBin = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersinvt, columnsinvt);
//			if(searchresultsBin != null && searchresultsBin != "" &&   searchresultsBin.length>0) {

//			vTempLoc=searchresultsBin[0].getId();

//			}

//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_location',s+1, vTempLoc);
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlocation',s+1, vTempLoc);//added by santosh

//			nlapiLogExecution('ERROR', 'vRecArrBin[k]1', vRecArrBin[k]);
//			nlapiLogExecution('ERROR', 'k', k);
//			nlapiLogExecution('ERROR', 'vRecArrLP[0]', vRecArrLP[0]);

//			nlapiLogExecution('ERROR', 'vRecArrLP.length', vRecArrLP.length);

//			nlapiLogExecution('ERROR', 'vRecArrLP[k]', vRecArrLP[k]);
//			if(vRecArrLP.length>=k)
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lp',s+1,  vRecArrLP[k]);
//			else
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lp',s+1,  vRecArrLP[vRecArrLP.length-1]);

//			nlapiLogExecution('ERROR', 'LineQty', vRecArrQty[k]);
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_expqty', s+1, vRecArrQty[k]);

//			nlapiLogExecution('ERROR', 'vRecArr[k]', vRecArr[k]);
//			if(vRecArr.length>=k)
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_skurecid',s+1,  vRecArr[k]);
//			else
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_skurecid',s+1,  vRecArr[vRecArr.length-1]);

//			nlapiLogExecution('ERROR', 'vRecArrLot[k]', vRecArrLot[k]);
//			//if (vCmpItemType == "lotnumberedinventoryitem" || vCmpItemType=="lotnumberedassemblyitem" || batchflag=="T" )
//			if (vCmpItemType == "lotnumberedinventoryitem" || vCmpItemType=="lotnumberedassemblyitem")
//			{	
//			//code added by santosh
//			var NLotno= vRecArrLot[k]
//			if(NLotno != "" && NLotno != null)
//			{	nlapiLogExecution('ERROR', 'NLotno', NLotno);	
//			var filterbatch = new Array();
//			filterbatch[0] = new nlobjSearchFilter('name', null, 'is', NLotno);
//			filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', lineItemvalue);
//			var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch);
//			if(receiptsearchresults!=null)
//			{
//			var lotid= receiptsearchresults[0].getId();
//			nlapiLogExecution('ERROR', 'lotid', lotid);

//			if(vRecArrLot.length>=k)
//			{
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lotno',s+1, lotid );
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlotno',s+1,  lotid);//added by santosh

//			}	

//			else
//			{		

//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lotno',s+1,  vRecArrLot[vRecArrLot.length-1]);
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlotno',s+1,  vRecArrLot[vRecArrLot.length-1]);//added by santosh

//			}

//			}

//			}
//			else
//			{
//			if(vRecArrLot.length>=k)
//			{

//			vRecArrLot[k]="";
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lotno',s+1,  vRecArrLot[k]);
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlotno',s+1,  vRecArrLot[k]);//added by santosh

//			}	

//			else
//			{		

//			vRecArrLot[k]="";
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lotno',s+1,  vRecArrLot[vRecArrLot.length-1]);
//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlotno',s+1,  vRecArrLot[vRecArrLot.length-1]);//added by santosh

//			}
//			}
//			//end of the code
//			}

//			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lineno',s+1,  LineNo);

//			s=s+1;
//			}
//			}
			//}



			nlapiLogExecution('ERROR', 'Submit', 'Submit'); 
			//doField.setDefaultValue('4');

			form.addSubmitButton('Confirm Assembly Build');  
			response.writePage(form);
		}

	}
	else //this is the POST block Added by ramana from else part.
	{ 

		if(request.getParameter('custpage_workorder')!='')
		{		
			fnGenerateKitOrder(request, response);
		}
		else
		{
			kitToOrderDetailsPOSTRequest(request, response);
		}
	}
}

function kitToOrderDetailsPOSTRequest(request, response)
{
	var form = nlapiCreateForm('Confirm Assembly Build');
	var vitem = request.getParameter('cusppage_item');		
	var vloc  = request.getParameter('cusppage_site');
	var vqty  = request.getParameter('custpage_kitqty');
	var vdate = request.getParameter('custpage_sodate');
	var vmemo = request.getParameter('custpage_notes');
	var vlp   = request.getParameter('custpage_lp');
	var vheaderlotno=request.getParameter('custpage_lotnoserialno');
	//var vheaderlotnoVal;
	var vbinloc = request.getParameter('custpage_binlocation');
	var vBeginDate=request.getParameter('custpage_bgdate');
	var vBeginTime=request.getParameter('custpage_bgtime');
	var vSointernalId=request.getParameter('custpage_sointernalid');

	var TempFlag=true;
	//var vaccountno = request.getParameter('cusppage_accountno');

	//Create Work Order 
	var Wo=nlapiCreateRecord('workorder');
	Wo.setFieldValue('assemblyitem',vitem);
	Wo.setFieldValue('location', vloc);			
	Wo.setFieldValue('quantity', vqty);
	Wo.setFieldValue('trandate', vdate);
	Wo.setFieldValue('createdfrom', vSointernalId);
	var lineCnt = request.getLineItemCount('custpage_deliveryord_items');
	//case 201412753
	form.setScript('customscript_ebiz_kitting_checklotno_cl');
	for (var k = 1; k <= lineCnt; k++) 
	{				
		var vitem = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
		var vLocation = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', k);
		var vlp =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_lp', k);
		var vQty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);
		var vLotNo =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotnotext', k);


		nlapiLogExecution('ERROR', 'vLocation',vLocation);
		nlapiLogExecution('ERROR', 'vlp',vlp);	
		nlapiLogExecution('ERROR', 'vQty',vQty);	
		Wo.setLineItemValue('item', 'item', k,vitem);
		Wo.setLineItemValue('item', 'quantity', k,vQty) ;
		if(vLotNo!=null && vLotNo!="")
			Wo.setLineItemValue('item', 'serialnumbers', k,vLotNo + "(" + vQty + ")") ;

		Wo.setLineItemValue('item', 'custcol_locationsitemreceipt', k,vLocation);
		Wo.setLineItemValue('item', 'custcol_ebiz_lp', k,vlp) ;

		Wo.selectLineItem('item', k);

		//Wo.commitLineItem('item');
	}

	var woid=nlapiSubmitRecord(Wo);
	nlapiLogExecution('ERROR', 'Outside If=', woid); 

	var filters = new Array();			   

	filters[0] = new nlobjSearchFilter('internalid', null, 'is', woid);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');			


	var searchResults = nlapiSearchRecord('workorder', null, filters, columns);
	var wovalue;
	if(searchResults!= null && searchResults != "" && searchResults.length >0)
	{
		wovalue=searchResults[0].getValue('tranid');
	}

	showInlineMessage(form, 'Confirmation', 'Work Order Created', wovalue);	

	response.writePage(form);

}

var Err="";
function fnGenerateKitOrder(request, response)
{
	//Reducing the Inventory Qty of Line level sku details from custom defined inventory table
	var form = nlapiCreateForm('Confirm Assembly Build');
	var woid = request.getParameter('custpage_workorder');
	var vitem = request.getParameter('cusppage_item');		
	var vloc  = request.getParameter('cusppage_site');
	var vqty  = request.getParameter('custpage_kitqty');
	var vKitqty  = request.getParameter('custpage_kitqty');
	var vdate = request.getParameter('custpage_sodate');
	var vmemo = request.getParameter('custpage_notes');
	var vlp   = request.getParameter('custpage_lp');
	var vMainItemQty   = request.getParameter('custpage_mainqty');

	//var vheaderlotno=request.getParameter('custpage_lotnoserialno');
	var Lotnovalue=request.getParameter('custpage_lotnoserialno');
	nlapiLogExecution('ERROR', 'Lotnovalue', Lotnovalue);
	nlapiLogExecution('ERROR', 'wmslocation',vloc);

	var vbinloc = request.getParameter('custpage_binlocation');
	var vBeginDate=request.getParameter('custpage_bgdate');
	var vBeginTime=request.getParameter('custpage_bgtime');
	var vSointernalId=request.getParameter('custpage_sointernalid');
	var wointernalid=request.getParameter('custpage_workorder');
	nlapiLogExecution('ERROR', 'wointernalid', wointernalid);
	var tmpLPMerge = request.getParameter('custpage_lpmerge');
	var Qty=request.getParameter('custpage_kitqty');
	var expQty=request.getParameter('custpage_kitqtyexp');

	var str = 'vloc.' + vloc + '<br>';
	str = str + 'Lotnovalue.' + Lotnovalue + '<br>';
	str = str + 'wointernalid.' + wointernalid + '<br>';
	str = str + 'tmpLPMerge.' + tmpLPMerge + '<br>';
	str = str + 'vitem.' + vitem + '<br>';
	str = str + 'Qty.' + Qty + '<br>';
	str = str + 'expQty' + expQty + '<br>';
	str = str + 'vbinloc' + vbinloc + '<br>';
	str = str + 'vlp' + vlp + '<br>';

	nlapiLogExecution('DEBUG', 'WO Values', str);

	var filterWO=new Array();


	filterWO.push(new nlobjSearchFilter('custrecord_ebiz_order_no','null','is',woid));

	var searchrecordWO=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filterWO,null);
	if(searchrecordWO == null || searchrecordWO == "")
	{
		var rcptrecordid3 = nlapiLoadRecord('workorder', woid);
		var vWOText= rcptrecordid3.getFieldValue('tranid');
		nlapiLogExecution('ERROR', 'vWOText',vWOText);

		nlapiLogExecution('ERROR','Bin locations are not yet generated for this Work order# '+ vWOText + '. Please generate bin locations first then perform Assembly Build');
		var ErrorMsg = 'Bin locations are not yet generated for this Work order# '+ vWOText + '. Please generate bin locations first then perform Assembly Build';

		showInlineMessage(form, 'Error', ErrorMsg , "");
		response.writePage(form);
		return false;
	}
	else
	{
		nlapiLogExecution('ERROR', 'vitem',vitem);
		var ValidLP=fnValidateEnteredContLP(vlp);
		if(!ValidLP)
		{
			nlapiLogExecution('ERROR','Entered LP# already exists.  Please enter another LP#');
			var ErrorMsg = 'Entered LP# already exists.  Please enter another LP#';

			showInlineMessage(form, 'Error', ErrorMsg , "");
			response.writePage(form);
			return false;
		}
		else
		{
			var SkuDims = getItemDimensions(vitem);
			if (SkuDims != null && SkuDims.length > 0) 
			{
				var ComponentItemType = nlapiLookupField('item', vitem, 'recordType');
				var fields = ['recordType','custitem_ebizbatchlot'];
				var ItemType="";
				var batchflag="F";
				if (ComponentItemType != null && ComponentItemType !='')
				{
					var columns = nlapiLookupField(ComponentItemType, vitem, fields);
					ItemType = columns.recordType;
					batchflag= columns.custitem_ebizbatchlot;
				}
				nlapiLogExecution('ERROR','ItemType 1',ItemType);
				//if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
				var vLotFifoDate="";
				var vLotExpDate="";
				//if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
				//{

				var filters = new Array();	

				if(Lotnovalue != null && Lotnovalue != "")
				{
					var searchresults = nlapiLoadRecord('customrecord_ebiznet_batch_entry', Lotnovalue);

					var vheaderlotno;

					if(searchresults!= null && searchresults!= "")
					{
						nlapiLogExecution('ERROR', 'Lotno Condition ', Lotnovalue);
						vheaderlotno=searchresults.getFieldValue('name');
						nlapiLogExecution('ERROR', 'vheaderlotno', vheaderlotno);
						vLotFifoDate=searchresults.getFieldValue('custrecord_ebizfifodate');
						nlapiLogExecution('ERROR', 'vLotFifoDate', vLotFifoDate);
						vLotExpDate=searchresults.getFieldValue('custrecord_ebizexpirydate');
						nlapiLogExecution('ERROR', 'vLotExpDate', vLotExpDate);
						vEbizLotSKU=searchresults.getFieldValue('custrecord_ebizsku');

						var str = 'Lotnovalue.' + Lotnovalue + '<br>';
						str = str + 'vheaderlotno.' + vheaderlotno + '<br>';
						str = str + 'vLotFifoDate.' + vLotFifoDate + '<br>';
						str = str + 'vLotExpDate.' + vLotExpDate + '<br>';
						str = str + 'vEbizLotSKU.' + vEbizLotSKU + '<br>';

						nlapiLogExecution('DEBUG', 'LOT Values', str);

						if(vitem != vEbizLotSKU)
						{
							showInlineMessage(form, 'Error', 'Please Enter Valid Serial/Lot#','');
							response.writePage(form);
							nlapiLogExecution('ERROR', 'Before false');
							return false;
						}	
					}
					//Lotnovalue=vheaderlotno;
				}
				else if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"  || batchflag=="T")// Validating the lot# field for mandatory. 211011 --Added by Sudheer
				{
					showInlineMessage(form, 'Error', 'Please Enter Serial/Lot#','');
					response.writePage(form);
					nlapiLogExecution('ERROR', 'Before false');
					return false;
				}
				//}

				nlapiLogExecution('ERROR', 'After False');

				var vLotcnt=0;// Modified Lot# count
				//change of code as of 210812 by suman
				var vlineCnt = request.getLineItemCount('custpage_deliveryord_items');
				for ( var vcount = 1; vcount <= vlineCnt; vcount++) 
				{
					var vActualBinloc = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlocation', vcount);	
					var vActualLOTno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotno', vcount);	
					var vEnteredBinloc = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', vcount);	
					var vEnteredLOTno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', vcount);
					var vlineno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', vcount);
					var vrecid = request.getLineItemValue('custpage_deliveryord_items', 'custpage_skurecid', vcount);

					var str = 'vActualBinloc.' + vActualBinloc + '<br>';
					str = str + 'vActualLOTno.' + vActualLOTno + '<br>';
					str = str + 'vEnteredBinloc.' + vEnteredBinloc + '<br>';
					str = str + 'vEnteredLOTno.' + vEnteredLOTno + '<br>';
					str = str + 'vlineno.' + vlineno + '<br>';

					nlapiLogExecution('DEBUG', 'LOT & Bin Loc Values', str);

					if((vEnteredLOTno !=null && vEnteredLOTno !='' && vEnteredLOTno !='null') && (vEnteredBinloc !=null && vEnteredBinloc !='' && vEnteredBinloc !='null'))
					{
						if((vActualBinloc!=vEnteredBinloc)||(vActualLOTno!=vEnteredLOTno))
						{   
							var varLineqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_qtyexp', vcount);
							nlapiLogExecution('ERROR', 'IfLotno are Same','same');
							CheckAvailability(woid,vlineno,vEnteredBinloc,vEnteredLOTno,vActualBinloc,vActualLOTno,varLineqty);
							vLotcnt++;
							//AllocateInv(woid,vlineno,vEnteredBinloc,vEnteredLOTno);
						}
					}
				}
				//end of code as of 210812.

				//code added by santosh on 26Sep12
				var vresult=false;
				if(GlobalArray!="" && GlobalArray!=null ){
					nlapiLogExecution('ERROR', 'vLotcnt',vLotcnt);
					nlapiLogExecution('ERROR', 'GlobalArray',GlobalArray.length);
					if(vLotcnt==GlobalArray.length)
					{
						vresult=UpdateNS(woid,GlobalArray,SearchresultsArray);
						nlapiLogExecution('ERROR', 'vresult',vresult);
					}
				}



				//To send to Assembly build

				var Qty=request.getParameter('custpage_kitqty');
				var expQty=request.getParameter('custpage_kitqtyexp');
				nlapiLogExecution('ERROR', 'Qty', Qty);
				nlapiLogExecution('ERROR', 'expQty', expQty);

				if(Qty!=expQty)
					vqty=expQty;
				var assmid= transformAssemblyItem(wointernalid,vqty,vloc,vheaderlotno,vitem,vLotExpDate,vMainItemQty);
				if(assmid != null && assmid != "")// && assmid.split(',').length==1)
				{
					nlapiLogExecution('ERROR', 'assmid', assmid);

					var lineCnt = request.getLineItemCount('custpage_deliveryord_items');
					var varqty ;
					nlapiLogExecution('ERROR', 'lineCnt', lineCnt);

					// Create a Record in Opentask
					var now = new Date();
					//a Date object to be used for a random value
					var now = new Date();
					//now= now.getHours();
					//Getting time in hh:mm tt format.
					var a_p = "";
					var d = new Date();
					var curr_hour = now.getHours();
					if (curr_hour < 12) {
						a_p = "am";
					}
					else {
						a_p = "pm";
					}
					if (curr_hour == 0) {
						curr_hour = 12;
					}
					if (curr_hour > 12) {
						curr_hour = curr_hour - 12;
					}

					var curr_min = now.getMinutes();

					curr_min = curr_min + "";

					if (curr_min.length == 1) {
						curr_min = "0" + curr_min;
					}
					//nlapiLogExecution("ERROR", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p))



					//Adding or updating the kitted SKU to custom defined inventory table.
					//var searchresultsLine=getRecord(vitem)

					var arrdims = new Array();

					//var arrdims = fnGetSkucubeandweight(vitem, 1);

					// The below code is added to get the sku status, packcode and LP at the time of create inventory for the main item -- Added by Mahesh.p & Sudheer on 111011
					//getting default skustaus and pack code

					var ItemDimensionsFilters = new Array();
					ItemDimensionsFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims',null, 'is', vitem));
					ItemDimensionsFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

					var ItemDimensionsColumns = new Array();

					ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizitemdims'));
					ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizpackcodeskudim'));
					ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizcube'));
					ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebiztareweight'));
					ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizuomskudim'));

					var ItemDimensionsResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, ItemDimensionsFilters, ItemDimensionsColumns);

					if (ItemDimensionsResults != null) 
					{
						for (var r = 0; r < ItemDimensionsResults.length; r++) 
						{
							var vuomdim = ItemDimensionsResults[r].getValue('custrecord_ebizuomskudim');

							if(parseInt(vuomdim)==1)
							{
								var vCube = ItemDimensionsResults[r].getValue('custrecord_ebizcube');
								var vweight = ItemDimensionsResults[r].getValue('custrecord_ebiztareweight');

								arrdims[0] = vCube;
								arrdims[1] = vweight;
							}
						}

						var ItemDimensionsResult = ItemDimensionsResults[0];
						var getFetchedPackCodeId = ItemDimensionsResult.getId();
						var getFetchedPackCode = ItemDimensionsResult.getValue('custrecord_ebizpackcodeskudim');
					}

					//nlapiLogExecution('ERROR', 'Fetched Pack Code', getFetchedPackCode);

					var getFetchedItemStatus=request.getParameter('custpage_itemstatus');
					var getFetchedItemStatusId=request.getParameter('custpage_itemstatus');
					if(getFetchedItemStatus==null||getFetchedItemStatus=="")
					{
						// Fetch the item status for the item entered
						var ItemStatusFilters = new Array();
						ItemStatusFilters.push(new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T'));

						var ItemStatusColumns = new Array();

						ItemStatusColumns.push(new nlobjSearchColumn('internalid'));
						ItemStatusColumns.push(new nlobjSearchColumn('name'));
						ItemStatusColumns.push(new nlobjSearchColumn('custrecord_ebizsiteskus'));

						var ItemStatusResults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemStatusFilters, ItemStatusColumns);

						if (ItemStatusResults != null) 
						{
							var ItemStatusResult = ItemStatusResults[0];
							getFetchedItemStatusId = ItemStatusResult.getId();
							getFetchedItemStatus = ItemStatusResult.getValue('name');
							var getFetchedSiteLocation = ItemStatusResult.getValue('custrecord_ebizsiteskus');
						}
					}

					var str = 'getFetchedItemStatus.' + getFetchedItemStatus + '<br>';
					str = str + 'getFetchedItemStatusId.' + getFetchedItemStatusId + '<br>';
					str = str + 'getFetchedPackCode.' + getFetchedPackCode + '<br>';

					nlapiLogExecution('DEBUG', 'Item Status Values', str);


					if(vbinloc ==null || vbinloc  == '')
					{
						var error =SetPalletization(vitem,vqty,vloc,Lotnovalue,vLotFifoDate,vLotExpDate,woid,vmemo,vlp,vBeginDate,vBeginTime,vSointernalId,wointernalid,arrdims,getFetchedItemStatusId,getFetchedPackCode,vheaderlotno);
						nlapiLogExecution('ERROR', 'error', error);
						if(error =='error')
						{
							showInlineMessage(form, 'Error', 'Location not Generated for kitItem,check Itemcube and Putaway Strategies','');
							response.writePage(form);
							nlapiLogExecution('ERROR', 'Before false');
							return false;
						}

					}
					else
					{
						var ItemCube = 0;
						if (arrdims.length > 0) {
							if (!isNaN(arrdims[0])) {
								ItemCube = (parseFloat(arrdims[0]) * parseFloat(vqty));
								nlapiLogExecution('ERROR', 'ItemCube', ItemCube);
							}
							else {
								ItemCube = 0;
							}
						}
						else {
							ItemCube = 0;
						}
						var ItemType = nlapiLookupField('item', vitem, 'recordType');
						nlapiLogExecution('ERROR','ItemType 1',ItemType)	;
						var Loctdims=new Array();

						Loctdims = getLocationDetails(vbinloc,vitem,vloc,ItemCube);

						var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;
						//code added from FactoryMation on 01Feb13 by santosh
						if (Loctdims != null && Loctdims.length > 0) {//upto here
							if (Loctdims[0] != "") {
								LocName = Loctdims[0];
								nlapiLogExecution('ERROR', 'LocName', LocName);
							}
							else {
								LocName = "";
								nlapiLogExecution('ERROR', 'in else LocName', LocName);
							}
							if (!isNaN(Loctdims[1])) {
								RemCube = Loctdims[1];
							}
							else {
								RemCube = 0;
							}
							if (!isNaN(Loctdims[2])) {
								LocId = Loctdims[2];
								nlapiLogExecution('ERROR', 'LocId', LocId);
							}
							else {
								LocId = "";
								nlapiLogExecution('ERROR', 'in else LocId', LocId);
							}
							if (!isNaN(Loctdims[3])) {
								OubLocId = Loctdims[3];
								nlapiLogExecution('ERROR', 'OubLocId ', OubLocId );
							}
							else {
								OubLocId = "";
							}
							if (!isNaN(Loctdims[4])) {
								PickSeq = Loctdims[4];
								nlapiLogExecution('ERROR', 'PickSeq ', PickSeq );
							}
							else {
								PickSeq = "";
							}
							if (!isNaN(Loctdims[5])) {
								InbLocId = Loctdims[5];
								nlapiLogExecution('ERROR', 'InbLocId ', InbLocId );
							}
							else {
								InbLocId = "";
							}
							if (!isNaN(Loctdims[6])) {
								PutSeq = Loctdims[6];
								nlapiLogExecution('ERROR', 'PutSeq ', PutSeq );
							}
							else {
								PutSeq = "";
							}
							if (!isNaN(Loctdims[7])) {
								LocRemCube = Loctdims[7];
							}
							else {
								LocRemCube = "";
							}

						}
						else
						{//case 20127087 : if Selected binlocation Remaining Cube is less than Item cube
							showInlineMessage(form, 'Error', 'No Sufficient Remaining Cube For Binlocation','');
							response.writePage(form);
							nlapiLogExecution('ERROR', 'No Sufficient Remaining cube');
							return false;
						}


						//end region
						//nlapiLogExecution('ERROR', 'before getting lp', 'done');
						var noofLpsRequired=1;
						var MultipleLP = GetMultipleLP(1,1,noofLpsRequired,vloc);
						nlapiLogExecution('ERROR', 'MultipleLP', MultipleLP);

						var maxLPPrefix = MultipleLP.split(",")[0];
						var maxLP = MultipleLP.split(",")[1];
						maxLP = parseFloat(maxLP) - parseFloat(noofLpsRequired); 

						nlapiLogExecution('ERROR', 'MAX LP:', maxLP);
						// Calculate LP
						maxLP = parseFloat(maxLP) + 1;
						var LP = maxLPPrefix + maxLP.toString();
						nlapiLogExecution('ERROR', 'LP:', LP);


						//Code added by suman on 190913
						var putItemPC='1';
						var varPaltQty = getMaxUOMQty(vitem,putItemPC);

						nlapiLogExecution('DEBUG', 'varPaltQty', varPaltQty);
						nlapiLogExecution('DEBUG', 'tmpLPMerge', tmpLPMerge);
						var invtid='';


						//Case# 201411044 starts
						//check FIFO Policy for FIFO date,if item is not lotnumber item. 
						var vTempfifodate=''; 
						if(vitem != null && vitem !='')
						{
							var vItemType ='';
							var vbatchflg="F"; 
							var itemgrpId='';
							var itemfamId='';
							var vfilter=new Array();						
							vfilter[0]=new nlobjSearchFilter("internalid",null,"anyof",vitem);
							var vcolumn=new Array();
							vcolumn[0]=new nlobjSearchColumn("type");
							vcolumn[1]=new nlobjSearchColumn("custitem_ebizbatchlot");
							vcolumn[2]=new nlobjSearchColumn("custitem_item_group");
							vcolumn[3]=new nlobjSearchColumn("custitem_item_family");
							var searchres=nlapiSearchRecord("item",null,vfilter,vcolumn);
							if(searchres!=null &&searchres!="")
							{
								vItemType = searchres[0].recordType;
								vbatchflg=searchres[0].getValue("custitem_ebizbatchlot");
								itemgrpId=searchres[0].getValue("custitem_item_group");
								itemfamId=searchres[0].getValue("custitem_item_family");
							}

							if(vLotFifoDate == null || vLotFifoDate =='' || vLotFifoDate =='undefined')
							{
								vTempfifodate = WOFifovalueCheck(vItemType,vitem,itemfamId,itemgrpId,'',wointernalid,Lotnovalue);
								nlapiLogExecution('ERROR','FIFO DATE from FIFOvalue check fn',vTempfifodate);
							}
							nlapiLogExecution('ERROR','Into CreateInventory---FIFO DATE from FIFOvalue check fn',vTempfifodate);
						}
						//Case# 201411044 ends

						if(tmpLPMerge =='T')
						{
							nlapiLogExecution('DEBUG', 'Checking for merge the item');
							var filtersinvt = new Array();
							if(vitem!=null&&vitem!="")
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', vitem));

							if(getFetchedItemStatusId!=null&&getFetchedItemStatusId!="")
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', getFetchedItemStatusId));

							if(vbinloc!=null&&vbinloc!="")
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', vbinloc));
							else
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is',LocId));
							filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', '19'));

							if(vLotFifoDate!=null && vLotFifoDate!='' && vLotFifoDate!='undefined')
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', vLotFifoDate));

							if(putItemPC!=null && putItemPC!='')
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', putItemPC));							

							if(Lotnovalue!=null && Lotnovalue!='')
							{
								nlapiLogExecution('DEBUG', 'Lotnovalue inside', Lotnovalue);
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', Lotnovalue));
							}

							if(varPaltQty!=null && varPaltQty!='')
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'lessthan', varPaltQty));							

							var columnsinvt = new Array();
							columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
							columnsinvt[0].setSort();

							var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
							nlapiLogExecution('DEBUG', 'invtsearchresults', invtsearchresults);
							if(invtsearchresults != null && invtsearchresults != ''  && invtsearchresults.length > 0)
							{
								nlapiLogExecution('DEBUG', 'invtsearchresults.length', invtsearchresults.length);
								var putQty=vqty;
								var newputqty=putQty;
								var BoolInvMerged=false;
								for (var i = 0; i < invtsearchresults.length; i++) 
								{
									var qoh=invtsearchresults[i].getValue('custrecord_ebiz_qoh');

									var str = 'qoh.' + qoh + '<br>';
									str = str + 'putQty.' + putQty + '<br>';
									str = str + 'varPaltQty.' + varPaltQty + '<br>';

									nlapiLogExecution('DEBUG', 'Qty Values1', str);

									if(newputqty>0 && (parseFloat(putQty)+parseFloat(qoh))<=parseFloat(varPaltQty))
									{
										//nlapiLogExecution('DEBUG', 'Inventory Record ID', invtsearchresults[i].getId());
										BoolInvMerged=true;
										//nlapiLogExecution('DEBUG', 'BoolInvMerged', BoolInvMerged);

										var scount=1;
										LABL1: for(var z=0;z<scount;z++)
										{
											try
											{
												var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[i].getId());											

												var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
												var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
												var varExistLP=invttransaction.getFieldValue('custrecord_ebiz_inv_lp');

												var str = 'varExistQOHQty.' + varExistQOHQty + '<br>';
												str = str + 'putQty.' + putQty + '<br>';
												str = str + 'varPaltQty.' + varPaltQty + '<br>';

												nlapiLogExecution('DEBUG', 'Qty Values', str);

												var varupdatedQOHqty = parseFloat(varExistQOHQty) + parseFloat(putQty);
												var varupdatedInvqty = parseFloat(varExistInvQty) + parseFloat(putQty);

												invttransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(varupdatedQOHqty).toFixed(5));
												invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varupdatedInvqty).toFixed(5));
												invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

												nlapiSubmitRecord(invttransaction,false, true);
												invtid=invtsearchresults[i].getId();
												//nlapiLogExecution('DEBUG', 'nlapiSubmitRecord', 'Record Submitted');
												nlapiLogExecution('DEBUG', 'Inventory is merged to the LP '+varExistLP);
												newputqty=0;
											}
											catch(ex)
											{
												nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

												var exCode='CUSTOM_RECORD_COLLISION'; 
												var wmsE='Inventory record being updated by another user. Please try again...';
												if (ex instanceof nlobjError) 
												{	
													wmsE=ex.getCode() + '\n' + ex.getDetails();
													exCode=ex.getCode();
												}
												else
												{
													wmsE=ex.toString();
													exCode=ex.toString();
												}  

												nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

												if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
												{ 
													scount=scount+1;
													continue LABL1;
												}
												else break LABL1;
											}
										}
									}
								}
								if(BoolInvMerged==false)
								{
									var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
									invrecord.setFieldValue('name', vitem+1);					
									invrecord.setFieldValue('custrecord_ebiz_inv_sku', vitem); 	
									var vMainItem=vitem;
									var vMainQty=vqty;
									//invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
									//invrecord.setFieldValue('custrecord_ebiz_inv_qty', vqty);
									//invrecord.setFieldValue('custrecord_ebiz_inv_account_no',vaccountno);
									invrecord.setFieldValue('custrecord_ebiz_inv_note1',vmemo);
									invrecord.setFieldValue('custrecord_ebiz_inv_loc', vloc);
									if(vbinloc!=null && vbinloc!="")
									{
										nlapiLogExecution('ERROR','vbinloc',vbinloc);
										invrecord.setFieldValue('custrecord_ebiz_inv_binloc', vbinloc);
									}
									else
									{
										invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);
									}
									//invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);
									invrecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(5));
									invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		

									if(Lotnovalue!=null && Lotnovalue != "") 
										invrecord.setFieldValue('custrecord_ebiz_inv_lot', Lotnovalue);	
									invrecord.setFieldValue('custrecord_ebiz_inv_qty',parseFloat(vqty).toFixed(5));
									invrecord.setFieldValue('custrecord_outboundinvlocgroupid',OubLocId);
									if(PickSeq != null && PickSeq != '')
										invrecord.setFieldValue('custrecord_pickseqno',PickSeq);
									invrecord.setFieldValue('custrecord_inboundinvlocgroupid',InbLocId);

									if(PutSeq != null && PutSeq != '')
										invrecord.setFieldValue('custrecord_putseqno',PutSeq);	

									//newly added fields
									invrecord.setFieldValue('custrecord_ebiz_inv_sku_status',getFetchedItemStatusId);	

									if(getFetchedPackCode==null || getFetchedPackCode=='')
										getFetchedPackCode=1;

									invrecord.setFieldValue('custrecord_ebiz_inv_packcode',getFetchedPackCode);	
									nlapiLogExecution('ERROR', 'vlp', vlp);
									nlapiLogExecution('ERROR', 'LP', LP);
									if(vlp==null || vlp=='')
										invrecord.setFieldValue('custrecord_ebiz_inv_lp',LP);
									else
										invrecord.setFieldValue('custrecord_ebiz_inv_lp', vlp);	

									//Added by Satish.N on 10/27/2011
									invrecord.setFieldValue('custrecord_ebiz_displayfield','N');
									invrecord.setFieldValue('custrecord_invttasktype','5');
									invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
									if(vLotFifoDate != null && vLotFifoDate != '')
										invrecord.setFieldValue('custrecord_ebiz_inv_fifo',vLotFifoDate);
									//Case# 201411044 starts
									else if(vTempfifodate != null && vTempfifodate != '')
										invrecord.setFieldValue('custrecord_ebiz_inv_fifo',vTempfifodate);
									//Case# 201411044 ends
									if(vLotExpDate != null && vLotExpDate != '')
										invrecord.setFieldValue('custrecord_ebiz_expdate',vLotExpDate);
									invrecord.setFieldValue('custrecord_ebiz_transaction_no', wointernalid);
									//Upto Here

									//invrecord.setFieldValue('custrecord_ebiz_inv_account_no', '120');					
									invtid=	nlapiSubmitRecord(invrecord,false,true );	
								}
							}
						}
						//End of code as on 190913.
						else
						{
							var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
							invrecord.setFieldValue('name', vitem+1);					
							invrecord.setFieldValue('custrecord_ebiz_inv_sku', vitem); 	
							var vMainItem=vitem;
							var vMainQty=vqty;
							//invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
							//invrecord.setFieldValue('custrecord_ebiz_inv_qty', vqty);
							//invrecord.setFieldValue('custrecord_ebiz_inv_account_no',vaccountno);
							invrecord.setFieldValue('custrecord_ebiz_inv_note1',vmemo);
							invrecord.setFieldValue('custrecord_ebiz_inv_loc', vloc);
							if(vbinloc!=null && vbinloc!="")
							{
								nlapiLogExecution('ERROR','vbinloc',vbinloc);
								invrecord.setFieldValue('custrecord_ebiz_inv_binloc', vbinloc);
							}
							else
							{
								invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);
							}
							//invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);
							invrecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(5));
							invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		

							if(Lotnovalue!=null && Lotnovalue != "") 
								invrecord.setFieldValue('custrecord_ebiz_inv_lot', Lotnovalue);	
							invrecord.setFieldValue('custrecord_ebiz_inv_qty',parseFloat(vqty).toFixed(5));
							invrecord.setFieldValue('custrecord_outboundinvlocgroupid',OubLocId);
							if(PickSeq != null && PickSeq != '')
								invrecord.setFieldValue('custrecord_pickseqno',PickSeq);
							invrecord.setFieldValue('custrecord_inboundinvlocgroupid',InbLocId);

							if(PutSeq != null && PutSeq != '')
								invrecord.setFieldValue('custrecord_putseqno',PutSeq);	

							//newly added fields
							invrecord.setFieldValue('custrecord_ebiz_inv_sku_status',getFetchedItemStatusId);	

							if(getFetchedPackCode==null || getFetchedPackCode=='')
								getFetchedPackCode=1;

							invrecord.setFieldValue('custrecord_ebiz_inv_packcode',getFetchedPackCode);	
							nlapiLogExecution('ERROR', 'vlp', vlp);
							nlapiLogExecution('ERROR', 'LP', LP);
							if(vlp==null || vlp=='')
								invrecord.setFieldValue('custrecord_ebiz_inv_lp',LP);
							else
								invrecord.setFieldValue('custrecord_ebiz_inv_lp', vlp);	

							//Added by Satish.N on 10/27/2011
							invrecord.setFieldValue('custrecord_ebiz_displayfield','N');
							invrecord.setFieldValue('custrecord_invttasktype','5');
							invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
							if(vLotFifoDate != null && vLotFifoDate != '')
								invrecord.setFieldValue('custrecord_ebiz_inv_fifo',vLotFifoDate);
							//Case# 201411044 starts
							else if(vTempfifodate != null && vTempfifodate != '')
								invrecord.setFieldValue('custrecord_ebiz_inv_fifo',vTempfifodate);
							//Case# 201411044 ends
							if(vLotExpDate != null && vLotExpDate != '')
								invrecord.setFieldValue('custrecord_ebiz_expdate',vLotExpDate);
							invrecord.setFieldValue('custrecord_ebiz_transaction_no', wointernalid);
							//Upto Here

							//invrecord.setFieldValue('custrecord_ebiz_inv_account_no', '120');					
							invtid =	nlapiSubmitRecord(invrecord,false,true );			 
						}
						//open task for main item

						var vMainItem=vitem;
						var vMainQty=vqty;
						nlapiLogExecution('ERROR','invtid ',invtid);
						var customrecord2 = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
						customrecord2.setFieldValue('custrecord_tasktype', 5); // 5 for KTS, 6 for KTO
						//customrecord2.setFieldValue('custrecordact_begin_date', now);
						customrecord2.setFieldValue('custrecordact_begin_date', vBeginDate);
						customrecord2.setFieldValue('custrecord_actualbegintime', vBeginTime);
						customrecord2.setFieldValue('custrecord_act_end_date', DateStamp());
						customrecord2.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						//customrecord2.setFieldValue('custrecord_expe_qty', vqty); //  commented by sudheer on 131111
						//nlapiLogExecution('ERROR','Main SKU Qty ',parseFloat(vMainQty).toFixed(5));
						//customrecord2.setFieldValue('custrecord_expe_qty', parseFloat(vKitqty).toFixed(5)); //  added by sudheer to Trimming the qty to integer.
						customrecord2.setFieldValue('custrecord_expe_qty', parseFloat(vMainItemQty).toFixed(5)); 
						//nlapiLogExecution('ERROR','Expe Qty value',parseFloat(vMainQty).toFixed(5));
						//customrecord2.setFieldValue('custrecord_act_qty', parseFloat(vKitqty).toFixed(5));  // Text type of field. So no need of trimming the decimal value
						customrecord2.setFieldValue('custrecord_act_qty', parseFloat(vMainQty).toFixed(5));
						if(vheaderlotno!= null && vheaderlotno != "")
							customrecord2.setFieldValue('custrecord_batch_no', vheaderlotno); 

						if(vbinloc!=null && vbinloc!="")
						{
							customrecord2.setFieldValue('custrecord_actbeginloc', vbinloc);
							customrecord2.setFieldValue('custrecord_actendloc', vbinloc);

						}
						else
						{
							customrecord2.setFieldValue('custrecord_actbeginloc', LocId);
							customrecord2.setFieldValue('custrecord_actendloc', LocId);					
						}


						customrecord2.setFieldValue('custrecord_wms_status_flag', 3);	// Putaway completed				
						//customrecord2.setFieldValue('custrecord_lpno', vlp); 
						customrecord2.setFieldValue('custrecord_upd_date', now);
						//customrecord2.setFieldValue('custrecord_sku', vskuText);
						//customrecord2.setFieldValue('custrecord_ebiz_sku_no', vMainItem);
						//nlapiLogExecution('ERROR', 'vitem', vMainItem);
						customrecord2.setFieldValue('custrecord_sku', vMainItem);
						customrecord2.setFieldValue('name', vMainItem+1);		
						customrecord2.setFieldValue('custrecord_lpno', vlp);
						customrecord2.setFieldValue('custrecord_wms_location', vloc);
						var currentContext = nlapiGetContext();  
						var currentUserID = currentContext.getUser();
						customrecord2.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
						customrecord2.setFieldValue('custrecord_ebizuser', currentUserID);
						customrecord2.setFieldValue('custrecord_ebiz_order_no', wointernalid);
						customrecord2.setFieldValue('custrecord_invref_no', invtid);
						nlapiSubmitRecord(customrecord2);


						/*var customkittranrecord = nlapiCreateRecord('customrecord_ebiznet_order_line_details');					
					customkittranrecord.setFieldValue('custrecord_orderlinedetails_checkin_qty', parseFloat(vKitqty).toFixed(5)); 
					customkittranrecord.setFieldValue('custrecord_orderlinedetails_putgen_qty', parseFloat(vKitqty).toFixed(5)); 
					customkittranrecord.setFieldValue('custrecord_orderlinedetails_putconf_qty', parseFloat(vKitqty).toFixed(5)); 
					customkittranrecord.setFieldValue('custrecord_orderlinedetails_status_flag', 3); //3 stands for s inbound process 
					customkittranrecord.setFieldValue('custrecord_orderlinedetails_item', vMainItem);
					customkittranrecord.setFieldValue('custrecord_orderlinedetails_ord_category',4); // 3 stands for workorder	
					customkittranrecord.setFieldValue('custrecord_orderlinedetails_order_no','WO'+vMainItem);
					customkittranrecord.setFieldValue('name','WO'+varsku); 
					nlapiSubmitRecord(customkittranrecord);*/
						/*
	         if(searchresultsLine!=null)  //A record already exists in inventory then need to update 
	         {

				var vrid=searchresultsLine.getId();
				var transactionupdate = nlapiLoadRecord('customrecord_ebiznet_createinv', vrid);
				var Existsqty = transactionupdate.getFieldValue('custrecord_ebiz_inv_qty');
				transactionupdate.setFieldValue('custrecord_ebiz_qoh',Existsqty+vqty);
				transactionupdate.setFieldValue('custrecord_ebiz_inv_qty',Existsqty+vqty);
				//transactionupdate.setFieldValue('custrecord_ebiz_callinv', 'N');
				nlapiSubmitRecord(transactionupdate);

		     }		
			 else  // if record not exists then need to create a new record in inventory
			 {
					var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
					invrecord.setFieldValue('name', vitem+vaccountno);					
					invrecord.setFieldValue('custrecord_ebiz_inv_sku', vitem); 	
					//invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
					invrecord.setFieldValue('custrecord_ebiz_inv_qty', vqty);
					invrecord.setFieldValue('custrecord_ebiz_inv_account_no',vaccountno);
					invrecord.setFieldValue('custrecord_ebiz_inv_note1',vmemo);
					invrecord.setFieldValue('custrecord_ebiz_inv_loc', vloc);
					//invrecord.setFieldValue('custrecord_ebiz_qoh', vqty);
					invrecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty));  //Commented and added to resovle decimal issue
					invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');

					nlapiSubmitRecord(invrecord);			 
			 }
						 */
						//upto to here adding or updateing the inventory table.
					}

					//nlapiLogExecution('ERROR', 'assmid', assmid);
					var id = nlapiSubmitRecord(assmid, false);
					nlapiLogExecution('ERROR', 'id', id);

					var Qty=request.getParameter('custpage_kitqty');
					var expQty=request.getParameter('custpage_kitqtyexp');
					nlapiLogExecution('ERROR', 'Qty', Qty);
					nlapiLogExecution('ERROR', 'expQty', expQty);

					if(Qty!=expQty){

						var vparWopros = fnForpartialWoProcessing(wointernalid,id,vMainItemQty);

					}
					else{


						nlapiLogExecution('ERROR', 'test1', 'test1');
						nlapiLogExecution('ERROR', 'lineCnt', lineCnt);
						var pickqtyfinal=0;
						var ItemStatusFilters = new Array();
						ItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', wointernalid));
						ItemStatusFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3));
						ItemStatusFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', 9));
						//ItemStatusFilters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', x+ ".0"));

						nlapiLogExecution('ERROR', 'test2', 'test2');
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_line_no');
						columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
						columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
						columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
						columns[4] = new nlobjSearchColumn('custrecord_lpno');
						columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
						columns[6] = new nlobjSearchColumn('custrecord_sku');
						columns[7] = new nlobjSearchColumn('custrecord_invref_no');
						columns[8] = new nlobjSearchColumn('custrecord_sku_status');
						columns[9] = new nlobjSearchColumn('custrecord_packcode');
						columns[10] = new nlobjSearchColumn('name');
						columns[11] = new nlobjSearchColumn('custrecord_batch_no');
						columns[12] = new nlobjSearchColumn('custrecord_from_lp_no');
						columns[13] = new nlobjSearchColumn('custrecord_kit_refno');
						columns[14] = new nlobjSearchColumn('custrecord_last_member_component');
						columns[15] = new nlobjSearchColumn('custrecord_parent_sku_no');
						columns[16] = new nlobjSearchColumn('custrecord_serial_no');
						columns[17] = new nlobjSearchColumn('custrecord_comp_id');
						columns[18] = new nlobjSearchColumn('custrecord_container_lp_no');
						columns[19] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
						columns[20] = new nlobjSearchColumn('custrecord_ebiz_order_no');
						columns[21] = new nlobjSearchColumn('custrecord_wms_location');
						columns[22] = new nlobjSearchColumn('custrecord_container');			

						var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, ItemStatusFilters, columns);
						var context = nlapiGetContext();
						var emp = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_emp');
						var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
						var parentid = nlapiSubmitRecord(otparent); 
						var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);
						var tolineparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
						for (var x = 1; x <= lineCnt; x++) 
						{	
							var vBatch="";
							var vBinlocation="";
							var varsku = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', x);
							//var varqty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', x);
							//	var varqty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_expqty', x);
							var varqty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_qtyexp', x);
							var vactbeginloc = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', x);
							var vlineno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', x);


							//code added by santosh on 24aug12
							/*			var vOldLOTno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotno', x);
				var vNewLOTno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', x);
				var vActualBinloc = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlocation', x);	
				var vEnteredBinloc = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', x);	

				if(vNewLOTno!=vOldLOTno)
				{
					var filterbatch = new Array();
					filterbatch[0] = new nlobjSearchFilter('InternalId', null, 'is', vNewLOTno);
					var columns=new Array();
					columns[0] = new nlobjSearchColumn('name');
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
					if(searchresults!=null)
					{
						vBatch= searchresults[0].getValue('name');
					}
				}
				else
				{
					vBatch="";

				}
				if(vActualBinloc!=vEnteredBinloc)
				{
					vBinlocation=vEnteredBinloc;
				}
				else
				{
					vBinlocation="";
				}


				nlapiLogExecution('ERROR', 'vOldLOTno', vOldLOTno);
				nlapiLogExecution('ERROR', 'vNewLOTno', vNewLOTno);
				nlapiLogExecution('ERROR', 'vBatch', vBatch); */

							//end of the code on 24 Aug12
							//nlapiLogExecution('ERROR', 'varsku', varsku);

							//nlapiLogExecution('ERROR', 'wointernalid', wointernalid);
							/*var ItemStatusFilters = new Array();
				ItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', wointernalid));
				ItemStatusFilters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', x+ ".0"));

				nlapiLogExecution('ERROR', 'test2', 'test2');
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_line_no');
				columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
				columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				columns[4] = new nlobjSearchColumn('custrecord_lpno');
				columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
				columns[6] = new nlobjSearchColumn('custrecord_sku');
				columns[7] = new nlobjSearchColumn('custrecord_invref_no');
				columns[8] = new nlobjSearchColumn('custrecord_sku_status');
				columns[9] = new nlobjSearchColumn('custrecord_packcode');
				columns[10] = new nlobjSearchColumn('name');
				columns[11] = new nlobjSearchColumn('custrecord_batch_no');
				columns[12] = new nlobjSearchColumn('custrecord_from_lp_no');
				columns[13] = new nlobjSearchColumn('custrecord_kit_refno');
				columns[14] = new nlobjSearchColumn('custrecord_last_member_component');
				columns[15] = new nlobjSearchColumn('custrecord_parent_sku_no');
				columns[16] = new nlobjSearchColumn('custrecord_serial_no');
				columns[17] = new nlobjSearchColumn('custrecord_comp_id');
				columns[18] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[19] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
				columns[20] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				columns[21] = new nlobjSearchColumn('custrecord_wms_location');
				columns[22] = new nlobjSearchColumn('custrecord_container');			

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, ItemStatusFilters, columns);*/
							var vline, vitem, vqty, vso, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono, vBatchno, vfromlp;
							var vkitrefno, lastkititem, kititem, vserialno, vCompany, vContlpNo, vEbizWaveNo, vSointernid, warehouseLocation,vcontainersize;
							var vInvRefArr=new Array();
							for (var i = 0; searchresults != null && i < searchresults.length; i++) 
							{
								var searchresult = searchresults[i];

								vline = searchresult.getValue('custrecord_line_no');
								vitem = searchresult.getValue('custrecord_ebiz_sku_no');

								vqty = searchresult.getValue('custrecord_expe_qty');
								vso = searchresult.getValue('custrecord_ebiz_cntrl_no');
								vLpno = searchresult.getValue('custrecord_container_lp_no');
								vlocationid = searchresult.getValue('custrecord_actbeginloc');

								vlocation = searchresult.getText('custrecord_actbeginloc');


								// vSKU = searchresult.getValue('custrecord_sku');
								vskustatusvalue = searchresult.getValue('custrecord_sku_status');
								vSKU = searchresult.getText('custrecord_sku');
								vinvrefno = searchresult.getValue('custrecord_invref_no');
								vrecid = searchresult.getId();
								// vskustatus = searchresult.getValue('custrecord_sku_status');
								vskustatus = searchresult.getText('custrecord_sku_status');
								vpackcode = searchresult.getValue('custrecord_packcode');
								vdono = searchresult.getValue('name');
								vfromlp = searchresult.getValue('custrecord_from_lp_no');
								vBatchno = searchresult.getValue('custrecord_batch_no');
								if(parseFloat(vlineno)==parseFloat(vline))
								{	 
									//code added by santosh
//									if(vBatch!=null && vBatch!="")
//									{
//									vBatchno=vBatch;
//									}
//									else
//									{
//									vBatchno = searchresult.getValue('custrecord_batch_no');
//									}
//									if(vBinlocation!=null && vBinlocation!="")
//									{
//									vlocationid=vBinlocation;
//									}
//									else
//									{
//									vlocationid = searchresult.getValue('custrecord_actbeginloc');
//									}
									//end of the code
									vkitrefno = searchresult.getValue('custrecord_kit_refno');
									lastkititem = searchresult.getValue('custrecord_last_member_component');
									kititem = searchresult.getValue('custrecord_parent_sku_no');
									vserialno = searchresult.getValue('custrecord_serial_no');
									vCompany = searchresult.getValue('custrecord_comp_id');
									vContlpNo = searchresult.getValue('custrecord_container_lp_no');
									vEbizWaveNo = searchresult.getValue('custrecord_ebiz_wave_no');
									vSointernid = searchresult.getValue('custrecord_ebiz_order_no');
									warehouseLocation = searchresult.getValue('custrecord_wms_location');
									vcontainersize = searchresult.getValue('custrecord_container');

									var arrDims = getSKUCubeAndWeightforconfirm(vitem, 1);
									var itemCube = 0;
									var itemWeight=0;			

									if (arrDims[0] != "" && (!isNaN(arrDims[1]))) 
									{
										itemCube = (parseFloat(vqty) * parseFloat(arrDims[0]));
										itemWeight = (parseFloat(vqty) * parseFloat(arrDims[1]));
//										itemCube = (parseFloat(arrDims[0]) * parseFloat(priorityRemainingQty));					
									} 
									nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
									nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemweight', itemWeight);
									nlapiLogExecution('ERROR', 'ContainerSize:ContainerSize', vcontainersize);				
									var arrContainerDetails=getContainerCubeAndTarWeight(vcontainersize,"");
									var ContainerCube=0;
									var TotalWeight=0;

									if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) 
									{
										ContainerCube =  parseFloat(arrContainerDetails[0]);
										TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));
									} 

									//update binlocation remaining cube
									nlapiLogExecution('DEBUG', 'itemCube', itemCube);
									nlapiLogExecution('DEBUG', 'vlocationid', vlocationid);
									if(vactbeginloc!=null && vactbeginloc!="" && vactbeginloc!=vlocationid)
									{
										vlocationid=vactbeginloc;
									}

									nlapiLogExecution('DEBUG', 'vlocationid', vlocationid);
									if(vlocationid !=null && vlocationid !='')
									{
										var LocRemCube = GeteLocCube(vlocationid);
										nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
										var RemCube;

										RemCube = parseFloat(LocRemCube) + parseFloat(itemCube);
										nlapiLogExecution('DEBUG', 'RemCube', RemCube);
										var retValue = UpdateLocCube(vlocationid, RemCube);
										nlapiLogExecution('DEBUG', 'LocId in UpdateLocCube', retValue);
									}

									nlapiLogExecution('ERROR', 'checkInPOSTRequest:ContainerCube', ContainerCube);
									nlapiLogExecution('ERROR', 'checkInPOSTRequest:TotalWeight', TotalWeight);
									nlapiLogExecution('ERROR','RecId',vrecid);
									newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', vrecid);
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'name', searchresult.getValue('name'));
									if(searchresult.getValue('custrecord_ebiz_sku_no') != null && searchresult.getValue('custrecord_ebiz_sku_no') != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebiz_sku_no', searchresult.getValue('custrecord_ebiz_sku_no'));
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_tasktype', 3);
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecordact_begin_date', searchresult.getValue('custrecordact_begin_date'));		
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actualbegintime', searchresult.getValue('custrecord_actualbegintime'));
									if(searchresult.getValue('custrecord_actbeginloc') != null && searchresult.getValue('custrecord_actbeginloc') != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actbeginloc', searchresult.getValue('custrecord_actbeginloc'));
									var currentContext = nlapiGetContext();
									var currentUserID = currentContext.getUser();


									if(currentUserID != null && currentUserID != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_upd_ebiz_user_no', currentUserID);
									if(searchresult.getValue('custrecord_ebizuser') != null && searchresult.getValue('custrecord_ebizuser') != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebizuser', searchresult.getValue('custrecord_ebizuser'));
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_expe_qty', parseFloat(searchresult.getValue('custrecord_expe_qty')).toFixed(5));
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_act_qty', parseFloat(vqty).toFixed(5));
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_wms_status_flag', 8);
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_batch_no', vBatchno);
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebiz_cntrl_no', searchresult.getValue('custrecord_ebiz_cntrl_no'));
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_line_no', searchresult.getValue('custrecord_line_no'));
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_lpno', searchresult.getValue('custrecord_lpno'));

									if(searchresult.getValue('custrecord_ebiz_order_no') != null && searchresult.getValue('custrecord_ebiz_order_no') != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebiz_order_no', searchresult.getValue('custrecord_ebiz_order_no'));
									if(searchresult.getValue('custrecord_sku') != null && searchresult.getValue('custrecord_sku') != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_sku', searchresult.getValue('custrecord_sku'));

									nlapiLogExecution('ERROR','searchresult.getValue(custrecord_packcode)',searchresult.getValue('custrecord_packcode'));
									if(searchresult.getValue('custrecord_packcode') != null && searchresult.getValue('custrecord_packcode') != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_packcode', searchresult.getValue('custrecord_packcode'));
									else
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_packcode', 1);// case# 201415477


									if(searchresult.getValue('custrecord_sku_status') != null && searchresult.getValue('custrecord_sku_status') != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_sku_status', searchresult.getValue('custrecord_sku_status'));
									if(searchresult.getValue('custrecord_wms_location') != null && searchresult.getValue('custrecord_wms_location') != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_wms_location', searchresult.getValue('custrecord_wms_location'));
									if(vlocationid != null && vlocationid != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actendloc', vlocationid);
									if(vcontainersize != null && vcontainersize != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_container', vcontainersize);
									if(itemWeight != null && itemWeight != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_total_weight', parseFloat(itemWeight).toFixed(5));
									if(itemCube != null && itemCube != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_totalcube', parseFloat(itemCube).toFixed(5));
									//if(vlocationid != null && vlocationid != '')
									//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actendloc', vlocationid);
									if(vContlpNo != null && vContlpNo != '')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_container_lp_no', vContlpNo);
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_act_end_date', DateStamp());
									newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
									var vemployee = request.getParameter('custpage_employee');
									nlapiLogExecution('ERROR', 'vemployee :', vemployee);
									if (vemployee != null && vemployee != "") 
									{
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_taskassignedto', vemployee);
									}	  
									else 
									{
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_taskassignedto', currentUserID);

									}
									if(id !=null && id!='' && id!='null')
										newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebiz_nsconfirm_ref_no', id);

									newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');
									//Added to transaction order line details

									/*tolineparent.selectNewLineItem('recmachcustrecord_ebiz_toline_parent');
							tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'name', 'WO'+varsku+x);
							tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_pickgen_qty', parseFloat(varqty).toFixed(5));
							tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_pickconf_qty', parseFloat(varqty).toFixed(5));
							tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_ship_qty', parseFloat(varqty).toFixed(5));
							tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_status_flag', 14);
							tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_item', varsku);
							tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_ord_category', 4);
							tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_order_no', 'WO'+varsku+x);
							tolineparent.commitLineItem('recmachcustrecord_ebiz_toline_parent');*/

								}
							}

							/*			var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vso);

			doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
			var oldpickQty=doline.getFieldValue('custrecord_pickqty');

			if(isNaN(oldpickQty))
				oldpickQty=0;

			pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vqty);

			doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal));
			doline.setFieldValue('custrecord_pickgen_qty', parseFloat(pickqtyfinal));
			nlapiSubmitRecord(transaction, false, true);

			nlapiSubmitRecord(doline, false, true);*/
//							var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
//							customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP
//							//customrecord.setFieldValue('custrecordact_begin_date', now);
//							customrecord.setFieldValue('custrecordact_begin_date', vBeginDate);
//							customrecord.setFieldValue('custrecord_actualbegintime', vBeginTime);
//							customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
//							customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
//							//customrecord.setFieldValue('custrecord_expe_qty', varqty); 
//							nlapiLogExecution('ERROR','expe qty ',varqty);		
//							customrecord.setFieldValue('custrecord_expe_qty', parseFloat(varqty));  //Commented and added to resovle decimal issue
//							customrecord.setFieldValue('custrecord_act_qty', varqty);
//							customrecord.setFieldValue('custrecord_wms_status_flag', 14);	// 14 stands for outbound process
//							customrecord.setFieldValue('custrecord_actbeginloc', vactbeginloc);	
//							customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
//							//customrecord.setFieldValue('custrecord_lpno', vlp); 
//							customrecord.setFieldValue('custrecord_upd_date', now);
//							//customrecord.setFieldValue('custrecord_sku', vskuText);
//							customrecord.setFieldValue('custrecord_ebiz_sku_no', varsku);
//							customrecord.setFieldValue('name', vitem+1);		
//							var currentContext = nlapiGetContext();  
//							var currentUserID = currentContext.getUser();
//							customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
//							customrecord.setFieldValue('custrecord_ebizuser', currentUserID);			
//							nlapiSubmitRecord(customrecord);

							//For SHIP Task
							/*var customrecord1 = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
			customrecord1.setFieldValue('custrecord_tasktype', 4); // 5 for KTS,3 for PICK, 4 for SHIP
			//customrecord1.setFieldValue('custrecordact_begin_date', now);
			customrecord1.setFieldValue('custrecordact_begin_date', vBeginDate);
			customrecord1.setFieldValue('custrecord_actualbegintime', vBeginTime);
			customrecord1.setFieldValue('custrecord_act_end_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
			customrecord1.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
			//customrecord1.setFieldValue('custrecord_expe_qty', varqty); //parseFloat(vqty)
			customrecord1.setFieldValue('custrecord_expe_qty', parseFloat(varqty));  //Commented and added to resovle decimal issue
			customrecord1.setFieldValue('custrecord_act_qty', varqty); 
			customrecord1.setFieldValue('custrecord_actbeginloc', vactbeginloc);
			customrecord1.setFieldValue('custrecord_actendloc', vbinloc);
			customrecord1.setFieldValue('custrecord_wms_status_flag', 14);	 // 14 stands for outbound process
			//customrecord1.setFieldValue('custrecord_lpno', vlp); 
			customrecord1.setFieldValue('custrecord_upd_date', now);
			//customrecord1.setFieldValue('custrecord_sku', vskuText);
			customrecord1.setFieldValue('custrecord_ebiz_sku_no', varsku);
			customrecord1.setFieldValue('name', vitem+1);	
			var currentContext = nlapiGetContext();  
			var currentUserID = currentContext.getUser();
			customrecord1.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
			customrecord1.setFieldValue('custrecord_ebizuser', currentUserID);				
			nlapiSubmitRecord(customrecord1);*/
						}
						var context = nlapiGetContext();
						nlapiLogExecution('ERROR','Remaining usage before inventory',context.getRemainingUsage());
						//nlapiLogExecution('ERROR', 'lineCnt', lineCnt);

						/*			var OpentskFilters = new Array();
			OpentskFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', wointernalid));



			var columns1 = new Array();
			columns1[0] = new nlobjSearchColumn('custrecord_line_no');
			columns1[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns1[2] = new nlobjSearchColumn('custrecord_expe_qty');
			columns1[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			columns1[4] = new nlobjSearchColumn('custrecord_lpno');
			columns1[5] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns1[6] = new nlobjSearchColumn('custrecord_sku');
			columns1[7] = new nlobjSearchColumn('custrecord_invref_no');
			columns1[8] = new nlobjSearchColumn('custrecord_sku_status');
			columns1[9] = new nlobjSearchColumn('custrecord_packcode');
			columns1[10] = new nlobjSearchColumn('name');
			var OpenResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpentskFilters, columns1);
						 */
						var vInvRefArr=new Array();
						if(searchresults!=null && searchresults!='')
						{
							for (var k = 0; k < searchresults.length; k++) 
							{
								var resultsset = searchresults[k];
								var varRecid = resultsset.getValue('custrecord_invref_no');
								//var varRecid = request.getLineItemValue('custpage_deliveryord_items', 'custpage_skurecid', k);
								//nlapiLogExecution('ERROR', 'skuInvtRecid_NO', varRecid);
								if(vInvRefArr.indexOf(varRecid)==-1)
								{

									vInvRefArr.push(varRecid);
								}	
							}	
						}	
						nlapiLogExecution('ERROR', 'vInvRefArr', vInvRefArr);
						if(vInvRefArr != null && vInvRefArr != '' && vInvRefArr.length>0)
							InvtDetails=GetAllInvtDetails(vInvRefArr,0);
						var vInvRecNew=new Array();
						var vInvRecIdNew=new Array();
						var vInvExpQtyNew=new Array();
						var context = nlapiGetContext();  

						if(searchresults!=null && searchresults!='')
						{
							for (var z = 0; z < searchresults.length; z++) 
							{
								var vResults = searchresults[z];
								//nlapiLogExecution('ERROR', 'z', z);

								var varsku =vResults.getValue('custrecord_ebiz_sku_no');				
								varqty =vResults.getValue('custrecord_expe_qty');			
								var varRecid =vResults.getValue('custrecord_invref_no');	
								var varLotNo = vResults.getValue('custrecord_batch_no');
								var LineNo=vResults.getValue('custrecord_line_no');

								/*	
				var varsku = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);	
				//varqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);
				varqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_expqty', k);			
				var varRecid = request.getLineItemValue('custpage_deliveryord_items', 'custpage_skurecid', k);	
				var varLotNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', k);	
				var varLotQty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);
				var LineNo=request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', k);*/

								nlapiLogExecution('ERROR', 'inventory intern id', varRecid);
								nlapiLogExecution('ERROR', 'InvtDetails', InvtDetails);
								if(InvtDetails != null && InvtDetails != '')
								{
									var vInvRec=GetInvDetails(varRecid,InvtDetails);

									nlapiLogExecution('ERROR', 'vInvRec', vInvRec);


									// Update the inventory to reduce the allocation quantity
									if(vInvRec!=null && vInvRec!='')
									{
										if(vInvRecIdNew.indexOf(varRecid) == -1)
										{
											vInvRecIdNew.push(varRecid);
											vInvRecNew.push(vInvRec);
											vInvExpQtyNew.push(varqty);
										}
										else
										{
											var vOldInvExpQty=vInvExpQtyNew[vInvRecIdNew.indexOf(varRecid)];
											if(vOldInvExpQty == null || vOldInvExpQty == '')
												vOldInvExpQty=0;
											vInvExpQtyNew[vInvRecIdNew.indexOf(varRecid)]=parseFloat(varqty) + parseFloat(vOldInvExpQty);
											//vInvExpQtyNew.push(parseFloat(expectedQty) + parseFloat(vOldInvExpQty));
										}
										//updateInventoryForTaskParent(vInvRec, expectedQty,emp,newParent);									// 4 UNITS
									}
								}
							}
						}
						//upto to here for custom defined invenotry table.

						if(vInvRecIdNew != null && vInvRecIdNew != '' && vInvRecIdNew.length>0)
						{	
							//nlapiLogExecution('ERROR', 'vInvRecIdNew', vInvRecIdNew);
							//nlapiLogExecution('ERROR', 'vInvExpQtyNew', vInvExpQtyNew);
							for(var t=0;t<vInvRecIdNew.length;t++)
							{
								nlapiLogExecution('ERROR', 'vInvRecNew[t]', vInvRecNew[t]);
								nlapiLogExecution('ERROR', 'vInvExpQtyNew[t]', vInvExpQtyNew[t]);
								if(vInvRecNew[t] != null)
									updateInventoryForTaskParent(vInvRecNew[t], vInvExpQtyNew[t],emp,newParent);	

							}
						}
						var pId = nlapiSubmitRecord(newParent);
						nlapiLogExecution('ERROR', 'Before updating transfer ord line',pId);
						//nlapiSubmitRecord(tolineparent);
						nlapiLogExecution('ERROR', 'Update Inventory End');


						//nlapiLogExecution('ERROR','vLotNo',varLotNo);
						//nlapiLogExecution('ERROR','varLotQty',varLotQty);


						//var assmid=transformAssemblyItem(woid,vqty,vloc,varLotNo);
						//var assmid= transformAssemblyItem(wointernalid,vqty,vloc,vheaderlotno,vitem);

						nlapiLogExecution('ERROR','assmid',assmid);
						var context = nlapiGetContext();
						nlapiLogExecution('ERROR','Remaining usage after Inventory',context.getRemainingUsage());
						// this else part is excute , whenever component Items pick confirm through RF and Confirm build in UI
						var vstageLoc ='';
						var vCarrier ='';  
						var vSite;
						var vCompany;
						var stgDirection="OUB";
						nlapiLogExecution('ERROR', 'vitem', vitem);
						nlapiLogExecution('ERROR', 'vloc', vloc);
						/*if(vstageLoc==null || vstageLoc=='')
					vstageLoc = GetPickStageLocation(vitem,vCarrier,vloc,vCompany,stgDirection,null,null,null,null);
				nlapiLogExecution('ERROR', 'vstagLoc', vstageLoc);
				if (vstageLoc != null && vstageLoc != "" && vstageLoc != "-1") */
						{										
							DeleteInvtRecdOfMemberItems(woid, vstageLoc,vloc);
						}
						updateOpenTaskwithns(wointernalid,id);
					}

					/*
					var customrecord3 = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
					customrecord3.setFieldValue('custrecord_tasktype', 1); // 5 for KTS, 1 for CHKN, 2 for PUTW
					//customrecord3.setFieldValue('custrecordact_begin_date', now);
					customrecord3.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
					customrecord3.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					customrecord3.setFieldValue('custrecord_expe_qty', vqty); 
					//customrecord3.setFieldValue('custrecord_lpno', vlp); 
					customrecord3.setFieldValue('custrecord_upd_date', now);
					//customrecord3.setFieldValue('custrecord_sku', vskuText);
					customrecord3.setFieldValue('custrecord_ebiz_sku_no', vitem);
					customrecord3.setFieldValue('name', vitem+1);				
					customrecord3.setFieldValue('custrecord_lpno', vlp);
					nlapiSubmitRecord(customrecord3);

					var customrecord4 = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
					customrecord4.setFieldValue('custrecord_tasktype', 2); // 5 for KTS, 1 for CHKN, 2 for PUTW
					//customrecord4.setFieldValue('custrecordact_begin_date', now);
					customrecord4.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
					customrecord4.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					customrecord4.setFieldValue('custrecord_expe_qty', vqty); 
					//customrecord4.setFieldValue('custrecord_lpno', vlp); 
					customrecord4.setFieldValue('custrecord_upd_date', now);
					//customrecord4.setFieldValue('custrecord_sku', vskuText);
					customrecord4.setFieldValue('custrecord_ebiz_sku_no', vitem);
					customrecord4.setFieldValue('name', vitem+1);					
					customrecord4.setFieldValue('custrecord_lpno', vlp);
					nlapiSubmitRecord(customrecord4);

					 */


					//upto to here for creation record in open task.


					//response.write("varsku::"+varsku);
					//response.write("\n");	
					//response.write("varqty::"+varqty);
					//response.write("\n");
					//response.write("varRecid::"+varRecid);
					//response.write("\n");
					// response.write("Kit to Stock done Successfully!!!");
					//Commented by Ganesh to redirect to SO after successfully completed
					if(assmid != null && assmid != '')
					{
						//var id = nlapiSubmitRecord(assmid, false);		
						if(id== null || id == '')
						{
							var vErr="Confirm Assembly Build Failed";		
							nlapiLogExecution('ERROR', 'Confirm Assembly Build failed');
							//nlapiLogExecution('ERROR', 'assmid.split(",").length ',assmid.split(",").length);
							/*if(assmid.split(',').length==2)
						{
							//vErr=assmid.split(',')[1];
						}*/
							showInlineMessage(form, 'Error', vErr, "");
							response.writePage(form);
						}
						else
						{
							//20123541 start
							nlapiLogExecution('ERROR','vExpDateGlobArr',vExpDateGlobArr);
							if(id != null && id != '' && vExpDateGlobArr != null && vExpDateGlobArr != '')
							{	
								for(var p=0;p<vExpDateGlobArr.length;p++)
								{
									var vExpDate=vExpDateGlobArr[p][2];
									if(vExpDate != null && vExpDate != '')
									{

										var filters = new Array();
										if(vExpDateGlobArr[p][0]!= null && vExpDateGlobArr[p][0]!= '')
											filters.push(new nlobjSearchFilter( 'item', null, 'is', vExpDateGlobArr[p][0] ));
										if(vExpDateGlobArr[p][1]!= null && vExpDateGlobArr[p][1]!= '')
											filters.push(new nlobjSearchFilter( 'inventorynumber', null, 'is', vExpDateGlobArr[p][1] ));



										// Define search columns
										var columns = new Array();
										columns[0] = new nlobjSearchColumn('item');
										columns[1] = new nlobjSearchColumn('inventorynumber');
										// Create the saved search
										var searchresults=nlapiSearchRecord( 'inventorynumber',null, filters, columns );

										for (var i in searchresults) {

											var searchResult = searchresults[i].getId(); // Gets the internal id
											var assemblyRecord = nlapiLoadRecord('inventorynumber',searchResult);

											assemblyRecord.setFieldValue('expirationdate',vExpDate);
											nlapiSubmitRecord(assemblyRecord,false);
										}
									}	
								}	
							}//end


							//	



						}
					}
					response.sendRedirect('TASKLINK','LIST_TRAN_BUILD',null,null,null);
				}	
				/*else
			{
				var vErr="Confirm Assembly Build Failed";		
				nlapiLogExecution('ERROR', 'Confirm Assembly Build failed');
				nlapiLogExecution('ERROR', 'assmid.split(",").length ',assmid.split(",").length);
				if(assmid.split(',').length==2)
				{
					//vErr=assmid.split(',')[1];
				}
			showInlineMessage(form, 'Error', vErr, "");
			response.writePage(form);

		}*/
			}
			else
			{
				showInlineMessage(form, 'Error', 'Dimensions are not configured for the KitItem');
				response.writePage(form);
				nlapiLogExecution('ERROR', 'Before false');
				return false;
			}
		} 
	}
	nlapiLogExecution('Debug','Time stamp at the end',TimeStampinSec());
} 
function fnValidateEnteredContLP(EnteredContLP)
{
	nlapiLogExecution('Debug', 'Into fnValidateEnteredContLP',EnteredContLP);	

	var filtersmlp = new Array();
	filtersmlp.push(new nlobjSearchFilter('name', null, 'is', EnteredContLP));
	/*if(vsite!=null&&vsite!="")
		filtersmlp.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', vsite));*/		 
	var LPExists = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);
	if(LPExists != null && LPExists != '' && LPExists.length>0)
		return false;
	else
		return true;	 
}
function getRecord(itemid)
{
	var filtersso = new Array();
	var socount=0;		

	filtersso[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemid);
	//   filtersso[0] = new nlobjSearchFilter('custrecord_ebiz_inv_qty', null, 'is', '2');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsLine !=null)
	{
		nlapiLogExecution('ERROR', 'SO count', searchresultsLine.length);
		socount=searchresultsLine.length;  
	}        
	return searchresultsLine;
}
function PickStrategieKittoOrder(item,itemfamily,itemgroup,avlqty)
{
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','itemgroup',itemgroup);
	nlapiLogExecution('ERROR','itemfamily',itemfamily);

	var filters = new Array();	
	filters[0] = new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]);
	var k=1;
	if (itemgroup != "" && itemgroup != null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskugrouppickrul',null,  'anyof', ['@NONE@',itemgroup]);
		k=k+1;
	}
	if (itemfamily!= "" && itemfamily!= null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@',itemfamily]);
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	//Fetching pick rule
	nlapiLogExecution('ERROR','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);
	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		var searchresultpick = searchresults[i];				
		var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');	
		nlapiLogExecution('ERROR','PickZone',vpickzone)	;		
		var filterszone = new Array();	
		if(vpickzone!=null&&vpickzone!="")
			filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', vpickzone);

		var columnzone = new Array();
		columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');

		var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
		nlapiLogExecution('ERROR','Loc Group Fetching');
		//fetching loc group
		for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {

			var searchresultzone = searchresultszone[j];
			var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');

			nlapiLogExecution('ERROR', 'LocGroup'+ searchresultzone.getValue('custrecord_locgroup_no'), searchresultzone.getText('custrecord_locgroup_no'));					
			var filtersinvt = new Array();

			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
			filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));
			filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));
			nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno');
			columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
			columnsinvt[5].setSort();

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
				var vlotno = searchresult.getText('custrecord_ebiz_inv_lot');
				var Recid = searchresult.getId();
				//santosh
				if(vlotno== null || vlotno =="")
					vlotno="";

				//santosh
				nlapiLogExecution('ERROR', 'Recid1', Recid);
				if (isNaN(allocqty)) {
					allocqty = 0;
				}
				if (allocqty == "") {
					allocqty = 0;
				}
				if( parseFloat(actqty)<0)
				{
					actqty=0;
				}
				nlapiLogExecution('ERROR', "allocqty: " + allocqty);
				nlapiLogExecution('ERROR', "LP: " + LP);

				var remainqty = actqty - allocqty;
				nlapiLogExecution('ERROR', 'remainqty' + remainqty);

				var cnfmqty = 0;
				nlapiLogExecution('ERROR', 'LOOP BEGIN');

				if (remainqty > 0) {
					nlapiLogExecution('ERROR', 'INTO LOOP');
					if ((avlqty - actallocqty) <= remainqty) {
						cnfmqty = avlqty - actallocqty;
						actallocqty = avlqty;
					}
					else {
						cnfmqty = remainqty;
						actallocqty = actallocqty + remainqty;
					}
					nlapiLogExecution('ERROR','cnfmqty', cnfmqty);
					nlapiLogExecution('ERROR','LotNo', vlotno);
					if (cnfmqty > 0) {
						//Resultarray
						var invtarray = new Array();
						invtarray[0]= cnfmqty;							
						invtarray[1]= vactLocation;
						invtarray[2] = LP;
						invtarray[3] = Recid;
						invtarray[4] = vlotno;
						nlapiLogExecution('ERROR', 'RecId:',Recid);
						//invtarray[3] = vpackcode;
						//invtarray[4] = vskustatus;
						//invtarray[5] = vuomid;
						Resultarray.push(invtarray);								
					}
				}
				if ((avlqty - actallocqty) == 0) {
					nlapiLogExecution('ERROR', 'Into Break:');
					return Resultarray;
				}
			}
		}
	}
	return Resultarray;  
}

/*function transformAssemblyItem(assemblyrecid,vqty,vloc,varLotNo){

	try
	{
		var fromRecord = 'workorder'; 

		var toRecord = 'assemblybuild'; 
		var record = nlapiTransformRecord(fromRecord, assemblyrecid, toRecord);

		record.setFieldValue('quantity', vqty);
		record.setFieldValue('location', vloc);	
		nlapiLogExecution('ERROR', 'Location for main component',vloc);
		nlapiLogExecution('ERROR', 'Quantity for main component',vqty);

		record.setFieldValue('serialnumbers',  varLotNo + "(" + vqty + ")") ;


		nlapiLogExecution('ERROR', 'varLotNo1', varLotNo + "(" + vqty + ")");

		var lineCnt = request.getLineItemCount('custpage_deliveryord_items');
		nlapiLogExecution('ERROR', 'lineCnt',lineCnt);

		for (var k = 1; k <= lineCnt; k++) 
		{				

			var LotNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', k);
			var varqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);
			var varsku =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
			nlapiLogExecution('ERROR', 'LotNo',LotNo);
			nlapiLogExecution('ERROR', 'k',k);
			nlapiLogExecution('ERROR', 'Lot number sending for component',LotNo + "(" + varqty + ")");
			record.setLineItemValue('component', 'item', k,varsku) ;
			record.setLineItemValue('component', 'componentnumbers', k,LotNo + "(" + varqty + ")") ;

		}


		var id = nlapiSubmitRecord(record, false);
		return id;

	} catch (e) {
		if (e instanceof nlobjError)
			lapiLogExecution('ERROR', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		else
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
	}
} 	*/	
//case 20123541 start
var vExpDateGlobArr=new Array();
//end
function transformAssemblyItem(assemblyrecid,vqty,vloc,varLotNo,vitem,vLotExpDate,vMainQty)
{
	try
	{

		var fromRecord = 'workorder'; 

		var toRecord = 'assemblybuild'; 
		var vAdvBinManagement=false;
		var context = nlapiGetContext();
		if(context != null && context != '')
		{
			if(context.getFeature('advbinseriallotmgmt').toString() != null && context.getFeature('advbinseriallotmgmt').toString() != '')
				vAdvBinManagement=context.getFeature('advbinseriallotmgmt');
			nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
		}
		var record = nlapiTransformRecord(fromRecord, assemblyrecid, toRecord);

		record.setFieldValue('quantity', vqty);
		record.setFieldValue('location', vloc);	
		nlapiLogExecution('ERROR', 'Location for main component',vloc);
		nlapiLogExecution('ERROR', 'Quantity for main component',vqty);

		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(vloc);


		nlapiLogExecution('ERROR', 'varLotNo1', varLotNo + "(" + vqty + ")");

		var lineCnt = request.getLineItemCount('custpage_deliveryord_items');

		var str = 'vloc. = ' + vloc + '<br>';
		str = str + 'vqty. = ' + vqty + '<br>';
		str = str + 'varLotNo. = ' + varLotNo + '<br>';	
		str = str + 'vitem. = ' + vitem + '<br>';	
		str = str + 'vMainQty. = ' + vMainQty + '<br>';
		str = str + 'assemblyrecid. = ' + assemblyrecid + '<br>';
		str = str + 'lineCnt. = ' + lineCnt + '<br>';
		str = str + 'confirmLotToNS. = ' + confirmLotToNS + '<br>';

		nlapiLogExecution('Debug', 'transformAssemblyItem parameters', str);

		// To calculate the no of components for the main assembly.
		nlapiLogExecution('ERROR', 'vitem',vitem);
		var ComponentItemType = nlapiLookupField('item', vitem, 'recordType');
		var fields = ['recordType','custitem_ebizbatchlot'];
		//nlapiLogExecution('ERROR','ItemType',ItemType);
		var searchresultsitem;
		var batchflag="F";
		var ItemType="";
		if(ComponentItemType!=null && ComponentItemType !='')
		{
			nlapiLogExecution('ERROR','Item Type',ComponentItemType);
			var columns = nlapiLookupField(ComponentItemType, vitem, fields); 

			ItemType = columns.recordType;
			batchflag= columns.custitem_ebizbatchlot; 
			searchresultsitem = nlapiLoadRecord(ItemType, vitem); //1020
		}

		else
		{
			nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
		}

		var SkuNo=searchresultsitem.getFieldValue('itemid'); 
		nlapiLogExecution('ERROR','confirmLotToNS111',confirmLotToNS);
		if(confirmLotToNS=='N')
			varLotNo=SkuNo.replace(/ /g,"-");
		//nlapiLogExecution('ERROR','varLotNo Main',varLotNo);

		//if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
		if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
		{	//case 20123541 start
			record.setFieldValue('serialnumbers',  varLotNo + "(" + vqty + ")") ;
			if(vLotExpDate != null && vLotExpDate != '')
			{	
				record.setFieldValue('expirationdate',  vLotExpDate) ;
				var vLocExpDate=new Array();
				vLocExpDate[0]=vitem;
				vLocExpDate[1]=varLotNo;
				vLocExpDate[2]=vLotExpDate;
				vExpDateGlobArr.push(vLocExpDate);
			}




			if(!vAdvBinManagement)
			{	
				record.setFieldValue('serialnumbers',  varLotNo + "(" + vqty + ")") ;
			}

			else
			{
				var compSubRecord1 = record.createSubrecord('inventorydetail');
				compSubRecord1.selectNewLineItem('inventoryassignment');

				//nlapiLogExecution('ERROR', 'expdate1', expdate1);
				//if(expdate1 !=null && expdate1 !='')
				//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'expirationdate', expdate1);
				if(confirmLotToNS == 'Y')
					compSubRecord1.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', varLotNo);
				else
					compSubRecord1.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vitem);
				compSubRecord1.setCurrentLineItemValue('inventoryassignment', 'quantity', vqty);
				compSubRecord1.commitLineItem('inventoryassignment');
				compSubRecord1.commit();

			}






		}
//		end
		var recCount=0; 
		/*var filters = new Array(); 
		//filters[0] = new nlobjSearchFilter('itemid', null, 'is', request.getParameter("custparam_item"));//kit_id is the parameter name 
		filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);//kit_id is the parameter name 
		var columns1 = new Array(); 


		columns1[0] = new nlobjSearchColumn( 'memberitem' ); 


		//  columns1[0] = new nlobjSearchColumn( 'internalid' ); 
		columns1[1] = new nlobjSearchColumn( 'memberquantity' );

		var j=0,k=1;
		var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); */


		var vAlert='';
		//Case#20123234 start :
		var CompItemCount=0;
		if(record !=null && record!='')
		{
			CompItemCount=record.getLineItemCount('component');
		}
		nlapiLogExecution('ERROR', 'CompItemCount',CompItemCount);
		//Case#20123234 end
		//nlapiLogExecution('ERROR','Member items count is ',searchresults.length);
		var j=1;	
//		for(var i=1; i<=searchresults.length;i++) // This loop contains the member items of the assembly 
//		{ 
		var previtem,prevbatch, prevQty , strbatchtoNS,prevLine,prevItemText; // These variables are to track the item and batch and consolidate the data and pass inputs to assembly build
		var boolfirstitem= true;

		nlapiLogExecution('ERROR','Grid count is',lineCnt);

		var memitemqty = new Array();
		memitemqty =fngetMemItemQty(assemblyrecid,vqty,vMainQty);


		for(var m = 0; m<memitemqty.length; m++){
			var memitem = memitemqty[m][0];
			var vTempMemQty = memitemqty[m][2];
			var memline = memitemqty[m][3];
			var memqty = vTempMemQty;
			var vRemQty=memqty;
			var compSubRecord =null;
			nlapiLogExecution('ERROR','memitemqty LENGTH',memitemqty.length);
			//making linecount =1 when ever WO contains only 1 component item (inventory/lotnumbered item)
			if(parseInt(memitemqty.length) == 1)
			{
				lineCnt=1;
			}
			nlapiLogExecution('ERROR','lineCnt',lineCnt);
			for (var k = 1; k <= lineCnt && parseFloat(vRemQty) >0; k++) 
			{				

				//var LotNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotnotext', k);
				var LotNo="";
				//	var varqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_expqty', k);
				var varqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_qtyexp', k);
				//var varsku =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
				var varsku =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenitemid', k);//santosh
				nlapiLogExecution('ERROR', 'varsku',varsku);
				//var varskuText =  request.getLineItemText('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
				var varLineno=  request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', k);
				nlapiLogExecution('ERROR','varLineno',varLineno);
				nlapiLogExecution('ERROR','memline',memline);

				if(parseInt(varLineno)==parseInt(memline)){

					if(parseFloat(varqty)> parseFloat(vRemQty))
					{
						varqty=vRemQty;
					}	 
					vRemQty=parseFloat(vRemQty)-parseFloat(varqty);

					var varskuText;
					var vnItemType="";
					var ItemTypeComp = nlapiLookupField('item', varsku, ['recordType','itemid']);
					/*var searchresultsitemComp = nlapiLoadRecord(ItemTypeComp, varsku); //1020
			if(searchresultsitemComp != null && searchresultsitemComp != "")
				varskuText= searchresultsitemComp.getFieldValue('itemid');*/
					if(ItemTypeComp != null && ItemTypeComp != '')
					{
						varskuText=ItemTypeComp.itemid;
						vnItemType = ItemTypeComp.recordType;
					}
					//nlapiLogExecution('ERROR', 'varskuText',varskuText);
					nlapiLogExecution('ERROR','vnItemType',vnItemType);

					//code added by santosh on 24aug12
					var vOldLOTno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotno', k);
					var vNewLOTno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', k);

					if(vNewLOTno!=vOldLOTno)
					{
						var filterbatch = new Array();
						filterbatch[0] = new nlobjSearchFilter('InternalId', null, 'is', vNewLOTno);
						var columns=new Array();
						columns[0] = new nlobjSearchColumn('name');
						var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
						if(searchresults!=null)
						{
							LotNo= searchresults[0].getValue('name');
						}
					}
					else
					{
						LotNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotnotext', k);

					}

					nlapiLogExecution('ERROR', 'vOldLOTno', vOldLOTno);
					nlapiLogExecution('ERROR', 'vNewLOTno', vNewLOTno);
					nlapiLogExecution('ERROR', 'LotNo', LotNo);

					//end of the code on 24 Aug12

					var str = 'varsku. = ' + varsku + '<br>';
					str = str + 'varskuText. = ' + varskuText + '<br>';
					str = str + 'LotNo. = ' + LotNo + '<br>';	
					str = str + 'varqty. = ' + varqty + '<br>';	
					str = str + 'vOldLOTno. = ' + vOldLOTno + '<br>';
					str = str + 'vNewLOTno. = ' + vNewLOTno + '<br>';
					str = str + 'LotNo. = ' + LotNo + '<br>';
					str = str + 'previtem = ' + previtem + '<br>';
					str = str + 'prevItemText = ' + prevItemText + '<br>';
					str = str + 'prevbatch = ' + prevbatch + '<br>';
					str = str + 'prevQty = ' + prevQty + '<br>';
					str = str + 'prevLine = ' + prevLine + '<br>';
					str = str + 'k. = ' + k + '<br>';
					str = str + 'm. = ' + m + '<br>';

					nlapiLogExecution('Debug', 'Component parameters', str);

					if (boolfirstitem == true) // If first time , store the values to previous variables.
					{

						nlapiLogExecution('ERROR','First time cond');
						previtem = varsku;
						prevItemText=varskuText;
						prevbatch = LotNo;
						prevQty = varqty;
						prevLine=varLineno;

						nlapiLogExecution('ERROR', 'previtem',previtem);
						nlapiLogExecution('ERROR', 'prevbatch',prevbatch);
						nlapiLogExecution('ERROR', 'prevQty',prevQty);
						nlapiLogExecution('ERROR', 'parseInt(memitemqty.length)',parseInt(memitemqty.length));

						var str = 'previtem. = ' + previtem + '<br>';
						str = str + 'prevbatch. = ' + prevbatch + '<br>';
						str = str + 'prevQty. = ' + prevQty + '<br>';	
						str = str + 'parseInt(memitemqty.length). = ' + parseInt(memitemqty.length) + '<br>';	
						str = str + 'prevLine. = ' + prevLine + '<br>';
						str = str + 'varsku. = ' + varsku + '<br>';

						nlapiLogExecution('Debug', 'First time parameters', str);
						var vlineCount = request.getLineItemCount('custpage_deliveryord_items');
						nlapiLogExecution('ERROR', 'vlineCount',vlineCount);
						//Case#20123234 start : code for Assembly main item conatins only one compaonent item	 
						if(CompItemCount !=null && CompItemCount!='' && CompItemCount!=0)
						{
							if(parseInt(memitemqty.length) == 1)
							{

								var itemIndex=1;
								var prevTempbatch ='';
								var prevTempQty ='';
								var strbatchtoNS='';
								for (var s = 1; s <= vlineCount; s++) 
								{
									var varTempqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_qtyexp', s);
									var varTempsku =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenitemid', s);

									var varTempLineno=  request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', s);
									var varTempLotNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotnotext', s);
									if(s == 1)
									{
										strbatchtoNS = varTempLotNo + "(" + varTempqty + ")";
										nlapiLogExecution('Debug', "s=1", strbatchtoNS);
									}
									else
									{
										if (prevTempbatch == varTempLotNo) 
										{
											prevTempQty = parseFloat(prevTempQty) + parseFloat(varTempqty); 
											strbatchtoNS = prevTempbatch + "(" + prevTempQty + ")";		
											nlapiLogExecution('Debug', "strbatchtoNS_if", strbatchtoNS);
										}
										else
										{
											nlapiLogExecution('Debug', "strbatchtoNS_else", strbatchtoNS);
											if(strbatchtoNS != null && strbatchtoNS != "")
												strbatchtoNS = strbatchtoNS + ',' + varTempLotNo + "(" + varTempqty + ")"; 
											else
												strbatchtoNS = varTempLotNo + "(" + varTempqty + ")";
										}
									}
									prevTempbatch = varTempLotNo;
									prevTempQty = varTempqty;
								}

								nlapiLogExecution('Debug', "itemIndex", itemIndex);
								//nlapiLogExecution('ERROR', 'CompItemCount',CompItemCount);
								//var strbatchtoNS = prevbatch + "(" + prevQty + ")";
								nlapiLogExecution('ERROR', 'strbatchtoNS11',strbatchtoNS);
								record.selectLineItem('component', 1);
								//record.setLineItemValue('component', 'item', prevLine,varsku) ; 
								record.setLineItemValue('component', 'item', itemIndex,varsku) ; 
								/*record.setLineItemValue('component', 'componentnumbers', prevLine,strbatchtoNS) ;
								record.setCurrentLineItemValue('component', 'quantity', prevQty);*/

								var TempLotArray =new Array();
								if(strbatchtoNS !=null && strbatchtoNS !='' && strbatchtoNS !='null')
								{
									TempLotArray=strbatchtoNS.split(',');
									nlapiLogExecution('ERROR','TempLotArray ',TempLotArray.length);
								}

								if(!vAdvBinManagement)
								{	
									if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
										record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS) ;
								}
								else
								{
									if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
									{
										var vcompSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');
										for(var s1=0;s1<TempLotArray.length;s1++)
										{
											var TempLotNumber=TempLotArray[s1];
											nlapiLogExecution('ERROR', 'TempLotNumber',TempLotNumber);
											var TempPos = TempLotNumber.indexOf("(")+1;
											nlapiLogExecution('ERROR', 'TempPos ', TempPos);
											var TempvQty=TempLotNumber.slice(TempPos, -1);
											nlapiLogExecution('ERROR', 'TempvQty', TempvQty);
											var vTempLotno=TempLotNumber.slice(0, TempPos-1);
											nlapiLogExecution('ERROR', 'vTempLotno', vTempLotno);
											if(vcompSubRecord !=null && vcompSubRecord!='')
											{
												nlapiLogExecution('ERROR', 'vcompSubRecord', vcompSubRecord);
												//vcompSubRecord.selectLineItem('inventoryassignment', 1);	
												var subrecordcount=vcompSubRecord.getLineItemCount('inventoryassignment')
												nlapiLogExecution('ERROR','subrecordcount1',subrecordcount);
												if(subrecordcount>1)
													vcompSubRecord.selectLineItem('inventoryassignment', 1);
												else
													vcompSubRecord.selectNewLineItem('inventoryassignment');	

											}
											else								
											{
												nlapiLogExecution('ERROR','else ','done');
												vcompSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
												vcompSubRecord.selectNewLineItem('inventoryassignment');
											}
											//vcompSubRecord.selectNewLineItem('inventoryassignment');
											vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vTempLotno);
											vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', TempvQty);
											vcompSubRecord.commitLineItem('inventoryassignment');
											//vcompSubRecord.commit();
										}
										vcompSubRecord.commit();
										record.commitLineItem('component');
										/*
									var vcompSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');

									nlapiLogExecution('ERROR', 'vcompSubRecord123',vcompSubRecord);

									if(vcompSubRecord !=null && vcompSubRecord!='')
									{
										vcompSubRecord.selectLineItem('inventoryassignment', 1);
									}
									else								
									{
										nlapiLogExecution('ERROR', 'create',varsku);																		 
										vcompSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
										vcompSubRecord.selectNewLineItem('inventoryassignment');
									}
									//vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', strbatchtoNS);
									vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', prevbatch);
									vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', prevQty);
									//vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', 3);
									vcompSubRecord.commitLineItem('inventoryassignment');
									vcompSubRecord.commit();
									record.commitLineItem('component');

										 */
									}
								}
							}
						}
						//Case#20123234 end
					}

					else // for boolfirstitem ==true
					{
						// to get Index value from  transform record
						var WOLength=0;
						var itemIndex=0;
						if(record!=null && record !='')
						{
							WOLength = record.getLineItemCount('component');  

							for (var cnt1 = 1; cnt1 <= WOLength; cnt1++) {

								var itemLineNo = record.getLineItemValue('component', 'orderline', cnt1);

								nlapiLogExecution('Debug', "itemLineNo", itemLineNo); 
								nlapiLogExecution('Debug', "prevLine here", prevLine);
								if (itemLineNo == prevLine) {
									itemIndex=cnt1;    
									break;
									//nlapiLogExecution('Debug', "itemIndex", itemIndex);
								}
							}
						}
						nlapiLogExecution('Debug', "itemIndex", itemIndex);
						if ( previtem == varsku  && prevLine==varLineno)

						{
							if (prevbatch == LotNo) // If the batch no is same , then qty is getting added.
							{
//								nlapiLogExecution('ERROR','Batch no same_strbatchtoNS',strbatchtoNS);
//								nlapiLogExecution('ERROR','Batch no same',lineCnt);
								prevQty = parseFloat(prevQty) + parseFloat(varqty); 
								//	nlapiLogExecution('ERROR','Batch no same - Qty',prevQty);

								if ( k == lineCnt) // If this is last line in the grid, we have to submitt details now
								{
									//nlapiLogExecution('ERROR','k=linecnt cond. this is lastline','');
									if(strbatchtoNS != null && strbatchtoNS != "")
										strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
									else
										strbatchtoNS = prevbatch + "(" + prevQty + ")";

									//nlapiLogExecution('ERROR', 'Lot number sending for component',LotNo + "(" + varqty + ")");
									//Assigning the details for first item to NS build assembly
									nlapiLogExecution('ERROR','strbatchtoNS',strbatchtoNS);
									nlapiLogExecution('ERROR','NS Item1',record.getLineItemValue('component', 'item', itemIndex));
									record.selectLineItem('component', itemIndex);
									//record.setLineItemValue('component', 'item', prevLine,varsku) ;   // Index i is the index for member items of assembly
									record.setLineItemValue('component', 'item', itemIndex,varsku) ;

									if(confirmLotToNS=='N')
										strbatchtoNS=varskuText.replace(/ /g,"-");

									//strbatchtoNS=varskuText.replace(/ /g,"-");
									nlapiLogExecution('ERROR','NS strbatchtoNS1 ',strbatchtoNS);
									nlapiLogExecution('ERROR','NS strbatchtoNSFinal1 ',strbatchtoNS);
									var vnItemTypeComp = nlapiLookupField('item', varsku, ['recordType','itemid']);
									if(vnItemTypeComp != null && vnItemTypeComp != '')
									{
										vnItemType = vnItemTypeComp.recordType;
									}
									nlapiLogExecution('ERROR','vnItemType',vnItemType);
									if(!vAdvBinManagement)
									{	
										if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
											record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS) ;
									}
									else
									{
										if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
										{
											compSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');

											nlapiLogExecution('ERROR','Lastlinecustrecord',compSubRecord);
											if(compSubRecord !=null && compSubRecord!='' )
											{
												var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
												nlapiLogExecution('ERROR','subrecordcount1',subrecordcount);
												if(subrecordcount>1)
													compSubRecord.selectLineItem('inventoryassignment', 1);
												else
													compSubRecord.selectNewLineItem('inventoryassignment');	
												//compSubRecord.selectLineItem('inventoryassignment', 1);
											}
											else								
											{
												nlapiLogExecution('ERROR','subrecordcountelse','Else');
												compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
											}


											/*if(compSubRecord !=null && compSubRecord!='' )
										{
											//compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
											//compSubRecord.selectNewLineItem('inventoryassignment');
//											var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
//											if(subrecordcount>1)
//											{
//												nlapiLogExecution('ERROR','testrecrd','testrecrd');
//												compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
//												compSubRecord.selectNewLineItem('inventoryassignment');
//											}											
//											else

												compSubRecord.selectNewLineItem('inventoryassignment');	
										}
										else								
										{
											compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
											compSubRecord.selectNewLineItem('inventoryassignment');
										}*/
											nlapiLogExecution('ERROR','prevbatch',prevbatch);
											nlapiLogExecution('ERROR','prevQty',prevQty);
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', prevbatch);
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', prevQty);
											compSubRecord.commitLineItem('inventoryassignment');
											compSubRecord.commit();
											record.commitLineItem('component');


										}
									}

									// Assigning batch string for the first item
									nlapiLogExecution('ERROR','sending inputs for assembly. last item sku',varsku);
									nlapiLogExecution('ERROR','sending inputs for assembly. last item lotnos',strbatchtoNS);
									j=j+1;
									strbatchtoNS='';

								} // end if for k == linecnt cond
							} // end if for prevbatch = lotno
							else // else for prevbatch = lotno // If the batch no changes , then preparing the batch string for first batch and storing second batch details
							{
								nlapiLogExecution('ERROR','Batch no Change-SKU same');
								nlapiLogExecution('ERROR','Batch no Change-Batchstring');
								nlapiLogExecution('ERROR','itemIndex',itemIndex);
								if(strbatchtoNS != null && strbatchtoNS != "")
									strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
								else
									strbatchtoNS = prevbatch + "(" + prevQty + ")";
								nlapiLogExecution('ERROR','Batch no Change-Batchstring',strbatchtoNS);
								prevbatch = LotNo;
								prevQty = parseFloat(varqty);

								if ( k == lineCnt) // If this is last line in the grid, we have to submitt details now
								{
									nlapiLogExecution('ERROR','This is lastline');
									if(strbatchtoNS != null && strbatchtoNS != "")
										strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
									else
										strbatchtoNS = prevbatch + "(" + prevQty + ")";

									//Assigning the details for first item to NS build assembly

									nlapiLogExecution('ERROR','NS Item2',record.getLineItemValue('component', 'item', itemIndex));
									record.selectLineItem('component', itemIndex);
									record.setLineItemValue('component', 'item', itemIndex,varsku) ;   // Index i is the index for member items of assembly

									if(confirmLotToNS=='N')
										strbatchtoNS=varskuText.replace(/ /g,"-");
									//strbatchtoNS=varskuText.replace(/ /g,"-");
									nlapiLogExecution('ERROR','NS strbatchtoNS2 ',strbatchtoNS);

									//strbatchtoNS=varskuText.replace(/ /g,"-");
									nlapiLogExecution('ERROR','NS strbatchtoNS1 ',strbatchtoNS);
									nlapiLogExecution('ERROR','NS strbatchtoNSFinal3 ',strbatchtoNS);
									nlapiLogExecution('ERROR','NS line ',j);
									var vnItemTypeComp = nlapiLookupField('item', varsku, ['recordType','itemid']);
									if(vnItemTypeComp != null && vnItemTypeComp != '')
									{
										vnItemType = vnItemTypeComp.recordType;
									}
									nlapiLogExecution('ERROR','vnItemType',vnItemType);
									var LotArray =new Array();
									if(strbatchtoNS !=null && strbatchtoNS !='' && strbatchtoNS !='null')
									{
										LotArray=strbatchtoNS.split(',');
										nlapiLogExecution('ERROR','LotArray ',LotArray.length);
									}
									//record.setLineItemValue('component', 'componentnumbers', prevLine,strbatchtoNS) ;
									if(!vAdvBinManagement)
									{	
										if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
											record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS) ;
									}
									else
									{
										if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
										{
											var compSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');
											for(var d=0;d<LotArray.length;d++)
											{
												var LotNumber=LotArray[d];
												nlapiLogExecution('ERROR', 'LotNumber',LotNumber);
												var pos = LotNumber.indexOf("(")+1;
												nlapiLogExecution('ERROR', 'pos ', pos);
												var vQty=LotNumber.slice(pos, -1);
												nlapiLogExecution('ERROR', 'vQty', vQty);
												var vLotno=LotNumber.slice(0, pos-1);
												nlapiLogExecution('ERROR', 'vLotno', vLotno);
												if(compSubRecord !=null && compSubRecord!='')
												{
													nlapiLogExecution('ERROR', 'compSubRecord', compSubRecord);
													/*var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment');
												nlapiLogExecution('ERROR','subrecordcount ',subrecordcount);
												if(subrecordcount>1)
													compSubRecord.selectLineItem('inventoryassignment', 1);
												else*/
													compSubRecord.selectNewLineItem('inventoryassignment');	

												}
												else								
												{
													nlapiLogExecution('ERROR','else ','done');
													compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
													compSubRecord.selectNewLineItem('inventoryassignment');
												}
												//compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vLotno);
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vQty);
												compSubRecord.commitLineItem('inventoryassignment');
												//compSubRecord.commit();
											}
											compSubRecord.commit();
											record.commitLineItem('component');


										}
									}// Assigning batch string for the first item
									nlapiLogExecution('ERROR','sending inputs for assembly. last item sku',varsku);
									nlapiLogExecution('ERROR','sending inputs for assembly. last item lotnos',strbatchtoNS);
									j=j+1;
									strbatchtoNS='';

								} // end if for k==linecnt cond


							}
						} // end if for previtem==varsku.
						else // for previtem==varsku cond else for previtem == varsku cond.  If the prev and current items are not equal , assign prev details to NS and store new items details
						{
							nlapiLogExecution('ERROR','SKU Changed');

							if (prevbatch != null && prevbatch !='' ) // When the item changes completing the preparation of batch string of first item
							{
								nlapiLogExecution('ERROR','SKU Changed1');
								if(strbatchtoNS!= null && strbatchtoNS != "")
									strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
								else
									strbatchtoNS = prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
								nlapiLogExecution('ERROR','varsku',varsku);
								nlapiLogExecution('ERROR','strbatchtoNS',strbatchtoNS);
								nlapiLogExecution('ERROR','prevLine',prevLine);
								//Assigning the details for first item to NS build assembly

								nlapiLogExecution('ERROR','j',j);
								//nlapiLogExecution('ERROR','NS Item3',record.getLineItemValue('component', 'item', itemIndex));
								record.selectLineItem('component', itemIndex);
								record.setLineItemValue('component', 'item', itemIndex,previtem);   // Index i is the index for member items of assembly

								if(confirmLotToNS=='N')
									strbatchtoNS=prevItemText.replace(/ /g,"-");
								//strbatchtoNS=prevItemText.replace(/ /g,"-");
								nlapiLogExecution('ERROR','prevLine',prevLine);
								nlapiLogExecution('ERROR','previtem',previtem);
								nlapiLogExecution('ERROR','NS strbatchtoNS1 ',strbatchtoNS);
								nlapiLogExecution('ERROR','NS strbatchtoNSFinal4 ',strbatchtoNS);
								nlapiLogExecution('ERROR','NS line ',j);
								var vnItemTypeComp = nlapiLookupField('item', previtem, ['recordType','itemid']);
								if(vnItemTypeComp != null && vnItemTypeComp != '')
								{
									vnItemType = vnItemTypeComp.recordType;
								}
								nlapiLogExecution('ERROR','vnItemType',vnItemType);
								var LotArray2 =new Array();
								if(strbatchtoNS !=null && strbatchtoNS !='' && strbatchtoNS !='null')
								{
									LotArray2=strbatchtoNS.split(',');
									nlapiLogExecution('ERROR','LotArray2 ',LotArray2.length);
								}
								if(!vAdvBinManagement)
								{
									if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
										record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS);
								}
								else
								{
									if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
									{
										compSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');

										for(var d1=0;d1<LotArray2.length;d1++)
										{
											var LotNumber2=LotArray2[d1];
											nlapiLogExecution('ERROR', 'LotNumber2',LotNumber2);
											var pos2 = LotNumber2.indexOf("(")+1;
											nlapiLogExecution('ERROR', 'pos2 ', pos2);
											var vQty2=LotNumber2.slice(pos2, -1);
											nlapiLogExecution('ERROR', 'vQty2', vQty2);
											var vLotno2=LotNumber2.slice(0, pos2-1);
											nlapiLogExecution('ERROR', 'vLotno2', vLotno2);

											if(compSubRecord !=null && compSubRecord!='')
											{
												var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
												nlapiLogExecution('ERROR','subrecordcount',subrecordcount);
												if(subrecordcount>1)
													compSubRecord.selectLineItem('inventoryassignment', 1);
												else
													compSubRecord.selectNewLineItem('inventoryassignment');	
												//compSubRecord.selectLineItem('inventoryassignment', 1);
											}
											else								
											{
												nlapiLogExecution('ERROR','subrecordcountelse','Else');
												compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
											}
											nlapiLogExecution('ERROR','Inuts to Assembly build -item',prevbatch);
											nlapiLogExecution('ERROR','Inuts to Assembly build -item',prevQty);
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vLotno2);
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vQty2);
											compSubRecord.commitLineItem('inventoryassignment');
										}
										compSubRecord.commit();
										record.commitLineItem('component');
									}
								}
								j=j+1;
								nlapiLogExecution('ERROR','SKU Changed2');
								strbatchtoNS ='';

								nlapiLogExecution('ERROR','Inuts to Assembly build -item',varsku);
								nlapiLogExecution('ERROR','Inuts to Assembly build -lotno',strbatchtoNS);

								// Start assigning the data for the new item

								previtem = varsku;
								prevItemText=varskuText;
								prevbatch = LotNo;
								prevQty = varqty;
								prevLine=varLineno;
								if ( k == lineCnt) // If this is last line in the grid, we have to submitt details now
								{
									var WOLength=0;
									var itemIndex=0;
									if(record!=null && record !='')
									{
										WOLength = record.getLineItemCount('component');  

										for (var cnt2 = 1; cnt2 <= WOLength; cnt2++) {

											var itemLineNo = record.getLineItemValue('component', 'orderline', cnt2);

											nlapiLogExecution('Debug', "itemLineNo", itemLineNo); 
											nlapiLogExecution('Debug', "prevLine here", prevLine);
											if (itemLineNo == prevLine) {
												itemIndex=cnt2;    
												break;
												//nlapiLogExecution('Debug', "itemIndex", itemIndex);
											}
										}
									}
									nlapiLogExecution('ERROR','This is lastline');

									nlapiLogExecution('ERROR','itemIndex',itemIndex);
									record.selectLineItem('component', itemIndex);
									nlapiLogExecution('ERROR','This is lastline',prevLine);
									if(strbatchtoNS!= null && strbatchtoNS != "")
										strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
									else
										strbatchtoNS = LotNo + "(" + varqty + ")"; // Batch string for item is prepared

									nlapiLogExecution('ERROR','strbatchtoNS',strbatchtoNS);
									//Assigning the details for first item to NS build assembly
									nlapiLogExecution('ERROR','NS Item',record.getLineItemValue('component', 'item', itemIndex));
									nlapiLogExecution('ERROR','varsku',varsku);
									nlapiLogExecution('ERROR','j',j);

									if(confirmLotToNS=='N')
										strbatchtoNS=varskuText.replace(/ /g,"-");

									//strbatchtoNS=varskuText.replace(/ /g,"-");
									nlapiLogExecution('ERROR','NS strbatchtoNS1 ',strbatchtoNS);
									record.setLineItemValue('component', 'item', itemIndex,varsku);
									//record.setLineItemValue('component', 'item', prevLine,varsku) ;   // Index i is the index for member items of assembly
									nlapiLogExecution('ERROR','NS strbatchtoNSFinal2 ',strbatchtoNS);
									var vnItemTypeComp = nlapiLookupField('item', varsku, ['recordType','itemid']);
									if(vnItemTypeComp != null && vnItemTypeComp != '')
									{
										vnItemType = vnItemTypeComp.recordType;
									}
									nlapiLogExecution('ERROR','vnItemType',vnItemType);
									var LotArray1 =new Array();
									if(strbatchtoNS !=null && strbatchtoNS !='' && strbatchtoNS !='null')
									{
										LotArray1=strbatchtoNS.split(',');
										nlapiLogExecution('ERROR','LotArray1 ',LotArray1.length);
									}
									if(!vAdvBinManagement)
									{	
										if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
											record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS) ;
									}
									else
									{
										if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
										{
											compSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');

											for(var d2=0;d2<LotArray1.length;d2++)
											{
												var LotNumber1=LotArray1[d2];
												nlapiLogExecution('ERROR', 'LotNumber1',LotNumber1);
												var pos1 = LotNumber1.indexOf("(")+1;
												nlapiLogExecution('ERROR', 'pos1 ', pos1);
												var vQty1=LotNumber1.slice(pos1, -1);
												nlapiLogExecution('ERROR', 'vQty1', vQty1);
												var vLotno1=LotNumber1.slice(0, pos1-1);
												nlapiLogExecution('ERROR', 'vLotno1', vLotno1);
												if(compSubRecord !=null && compSubRecord!='')
												{
													var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
													nlapiLogExecution('ERROR','subrecordcount',subrecordcount);
													if(subrecordcount>1)
														compSubRecord.selectLineItem('inventoryassignment', 1);
													else
														compSubRecord.selectNewLineItem('inventoryassignment');	
													//compSubRecord.selectLineItem('inventoryassignment', 1);
												}
												else								
												{
													nlapiLogExecution('ERROR','subrecordcountelse','Else');
													compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
													compSubRecord.selectNewLineItem('inventoryassignment');
												}

												/*if(compSubRecord !=null && compSubRecord!='')
										{
											compSubRecord.selectLineItem('inventoryassignment', 1);
										}
										else								
										{
											compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
											compSubRecord.selectNewLineItem('inventoryassignment');
										}*/
												//compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vLotno1);
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vQty1);
												compSubRecord.commitLineItem('inventoryassignment');
											}
											compSubRecord.commit();
											record.commitLineItem('component');

										}

									}// Assigning batch string for the first item
									nlapiLogExecution('ERROR','sending inputs for assembly. last item sku',varsku);
									nlapiLogExecution('ERROR','sending inputs for assembly. last item lotnos',strbatchtoNS);
									j=j+1;
									strbatchtoNS='';

								} // end if for k==linecnt condf

							}// end if if (prevbatch != null && prevbatch !='' ) 


						}	// end if previtem==varsku



					}// end if for boolfirstitem ==true

					boolfirstitem= false;  // For second item onwards it should compare for current item and prev item
					//break;
				}
			} // End for loop (var k = 1; k <= lineCnt; k++) 

		}

//		} // Ending for loop for Searchresults			

		nlapiLogExecution('ERROR','Before submitting assemblybuild');

		//var id = nlapiSubmitRecord(record, false);

		//nlapiLogExecution('ERROR','After submitting assemblybuild and id is ',id);
		nlapiLogExecution('ERROR','After submitting assemblybuild and record is ',record);
		return record;

	} catch (e) {
		if (e instanceof nlobjError)
			nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		else
		{
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			Err=e.toString();
			return 'ERROR,' + e.toString();
		}
	}
} 		 

function GetPutawayLocation(Item, PackCode, ItemStatus, UOM, ItemCube, ItemType,SiteLocation)
{
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod, MergeFlag = 'F',Mixsku='F';
	nlapiLogExecution('ERROR', 'ItemCube', ItemCube);//0
	nlapiLogExecution('ERROR', 'ItemType', ItemType);
	var itemresults;
	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus','custitem_item_info_1','custitem_item_info_2','custitem_item_info_3'];

	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
	/*if(ItemStatus != null && ItemStatus != '')
		ItemStatus = columns.custitem_ebizdefskustatus;*/
	if(ItemStatus == null || ItemStatus == '')
		ItemStatus = columns.custitem_ebizdefskustatus;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;

	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('formulanumeric');
	columns[0].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';

	if(ItemFamily!=null && ItemFamily != "")
	{
		nlapiLogExecution('ERROR', 'ItemFamily', ItemFamily);
		filters[i] = new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', ItemFamily]);
		i++;
	}
	if(ItemGroup!=null && ItemGroup != "")
	{
		nlapiLogExecution('ERROR', 'ItemGroup', ItemGroup);
		filters[i] = new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', ItemGroup]);
		i++;
	}
	if(ItemStatus!=null && ItemStatus != "")
	{
		nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
		filters[i] = new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', ItemStatus]);
		i++;
	}
	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		nlapiLogExecution('ERROR', 'ItemInfo1', ItemInfo1);
		filters[i] = new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'is',ItemInfo1);
		i++;
	}
	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		nlapiLogExecution('ERROR', 'ItemInfo2', ItemInfo2);
		filters[i] = new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'is',ItemInfo2);
		i++;
	}
	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		nlapiLogExecution('ERROR', 'ItemInfo3', ItemInfo3);
		filters[i] = new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'is',ItemInfo3);
		i++;
	}
	if(Item != null && Item != "")
	{
		nlapiLogExecution('ERROR', 'Item', Item);
		filters[i] = new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', Item]);
		i++;
	}
	if(SiteLocation != null && SiteLocation != "")
	{
		nlapiLogExecution('ERROR', 'SiteLocation', SiteLocation);
		filters[i] = new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', ['@NONE@', SiteLocation]);
		i++;
	}

	var t1 = new Date();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (searchresults != null) {
		for (var i = 0; i < searchresults.length; i++) {
			locgroupid = searchresults[i].getValue('custrecord_locationgrouppickrule');
			zoneid = searchresults[i].getValue('custrecord_putawayzonepickrule');
			PutMethod = searchresults[i].getValue('custrecord_putawaymethod');
			nlapiLogExecution('ERROR', 'Put Rule', searchresults[i].getValue('custrecord_ruleidpickrule'));
			nlapiLogExecution('ERROR', 'Put Method', PutMethod);
			nlapiLogExecution('ERROR', 'Location Group Id from Putaway Rule', locgroupid);

			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = searchresults[i].getValue('custrecord_manual_location_generation');
			nlapiLogExecution('ERROR', 'Manual Location Generation', ManualLocationGeneration);

			if (ManualLocationGeneration == 'F') {
				if (locgroupid != 0 && locgroupid != null) {
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

					nlapiLogExecution('ERROR', 'locgroupid', locgroupid);
					var filters = new Array();
					filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]);

					t1 = new Date();
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:location',
							getElapsedTimeDuration(t1, t2));

					nlapiLogExecution('ERROR', 'locResults', locResults);
					if (locResults != null) {
						nlapiLogExecution('ERROR', 'ifnot ');
						for (var i = 0; i < locResults.length; i++) {

							Location = locResults[i].getValue('name');
							var locationIntId = locResults[i].getId();

							LocRemCube = GeteLocCube(locationIntId);
							nlapiLogExecution('ERROR', 'Location', Location);
							nlapiLogExecution('ERROR', 'locResults[i].getId()', locResults[i].getId());
							OBLocGroup = locResults[i].getValue('custrecord_outboundinvlocgroupid');
							PickSeqNo = locResults[i].getValue('custrecord_startingpickseqno');
							IBLocGroup = locResults[i].getValue('custrecord_inboundinvlocgroupid');
							PutSeqNo = locResults[i].getValue('custrecord_startingputseqno');
							nlapiLogExecution('ERROR', 'LocRemCube', LocRemCube);
							if (parseFloat(LocRemCube) > parseFloat(ItemCube)) {
								RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
								location_found = true;

								LocArry[0] = Location;
								LocArry[1] = RemCube;
								LocArry[2] = locationIntId;		//locResults[i].getId();
								LocArry[3] = OBLocGroup;
								LocArry[4] = PickSeqNo;
								LocArry[5] = IBLocGroup;
								LocArry[6] = PutSeqNo;
								LocArry[7] = LocRemCube;
								LocArry[8] = zoneid;
								nlapiLogExecution('ERROR', 'SA-Location', Location);
								nlapiLogExecution('ERROR', 'Location Internal Id',locationIntId);
								nlapiLogExecution('ERROR', 'Sa-Location Cube', RemCube);
								return LocArry;
							}

						}
					}
				}
				else {
					nlapiLogExecution('ERROR', 'Fetching Location Group from Zone', zoneid);
					//MergeFlag = GetMergeFlag(PutMethod);

					var mixarray =  new Array();
					if(PutMethod!=null && PutMethod!='')
						mixarray=GetMergeFlag(PutMethod);
					if(mixarray!=null && mixarray!='' && mixarray.length>0)
					{
						MergeFlag = mixarray[0][0];
						if(mixarray.length>1)
							Mixsku = mixarray[0][1];	
					}

					nlapiLogExecution('ERROR', 'Merge Flag', MergeFlag);
					nlapiLogExecution('ERROR', 'Mix SKU', Mixsku);

					if (zoneid != null && zoneid != ""){ //&& zoneid != "" ) {
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_locgroup_no');
						columns[1] = columns[0].setSort();
						columns[2] = new nlobjSearchColumn('custrecord_locgroup_no');

						var filters = new Array();
						if(zoneid!=null&&zoneid!="")
							filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', zoneid));
						filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', SiteLocation]));
						t1 = new Date();
						var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
						t2 = new Date();
						nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:zone_locgroup',
								getElapsedTimeDuration(t1, t2));
						for (var i = 0; i < Math.min(30, searchresults.length); i++) {
							locgroupid = searchresults[i].getValue('custrecord_locgroup_no');
							nlapiLogExecution('ERROR', 'Fetched Location Group Id from Putaway Zone', locgroupid);
							var RemCube = 0;
							var LocRemCube = 0;

							if (MergeFlag == 'T') {
								var locResults;
								try {
									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_putseqno');
									columns[1] = columns[0].setSort();
									columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
									columns[3] = new nlobjSearchColumn('custrecord_outboundinvlocgroupid');
									columns[4] = new nlobjSearchColumn('custrecord_pickseqno');
									columns[5] = new nlobjSearchColumn('custrecord_inboundinvlocgroupid');
									columns[6] = new nlobjSearchColumn('custrecord_putseqno');
									columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');

									var filters = new Array();
									filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
									filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'is', locgroupid));
									if(SiteLocation != null && SiteLocation != "")
										filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',SiteLocation));
									t1 = new Date();
									locResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
									t2 = new Date();
									nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:createinv',
											getElapsedTimeDuration(t1, t2));
								} //try close.
								catch (Error) {
									nlapiLogExecution('ERROR', 'Into Error', Error);
								}
								try {
									nlapiLogExecution('ERROR', 'Inside Try', 'TRY');
									if (locResults != null) {
										for (var j = 0; j < locResults.length; j++) {
											nlapiLogExecution('ERROR', 'locResults[j].getId()', locResults[j].getId());
											Location = locResults[j].getText('custrecord_ebiz_inv_binloc');
											var locationInternalId = locResults[j].getValue('custrecord_ebiz_inv_binloc');

											nlapiLogExecution('ERROR', 'Location', Location );
											nlapiLogExecution('ERROR', 'Location Internal Id', locationInternalId);

											LocRemCube = GeteLocCube(locationInternalId);
											OBLocGroup = locResults[j].getValue('custrecord_outboundinvlocgroupid');
											PickSeqNo = locResults[j].getValue('custrecord_pickseqno');
											IBLocGroup = locResults[j].getValue('custrecord_inboundinvlocgroupid');
											PutSeqNo = locResults[j].getValue('custrecord_putseqno');

											if (parseFloat(LocRemCube) > parseFloat(ItemCube)) {
												RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
												nlapiLogExecution('ERROR', 'Loc Rem Cube', LocRemCube);
												nlapiLogExecution('ERROR', 'Item Cube', ItemCube);

												location_found = true;

												LocArry[0] = Location;
												LocArry[1] = RemCube;
												LocArry[2] = locationInternalId;		//locResults[j].getId();
												LocArry[3] = OBLocGroup;
												LocArry[4] = PickSeqNo;
												LocArry[5] = IBLocGroup;
												LocArry[6] = PutSeqNo;
												LocArry[7] = LocRemCube;
												LocArry[8] = zoneid;
												nlapiLogExecution('ERROR', 'after changes Location Cube', RemCube);
												return LocArry;
											}

										}
									}
								} 
								catch (exps) {
									nlapiLogExecution('ERROR', 'included exps', exps);
								}
							}
							if (location_found == false) {
								nlapiLogExecution('ERROR', 'Location Not found for Merge Location for Location Group', locgroupid);
								var locResults = GetLocation(locgroupid);

								if (locResults != null) {
									nlapiLogExecution('ERROR', 'locResults.length', locResults.length);
									for (var j = 0; j < Math.min(300, locResults.length); j++) {

										var emptyBinLocationId = locResults[j].getId();
										var emptyloccheck =	binLocationCheck(emptyBinLocationId);
										nlapiLogExecution('ERROR', 'emptyloccheck', emptyloccheck);

										if(emptyloccheck == 'N'){
											//Changes done by Sarita (index i is changed to j).
											LocRemCube = locResults[j].getValue('custrecord_remainingcube');
											OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid');
											PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno');
											IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid');
											PutSeqNo = locResults[j].getValue('custrecord_startingputseqno');

											nlapiLogExecution('ERROR', 'LocRemCube', LocRemCube);
											nlapiLogExecution('ERROR', 'ItemCube', ItemCube);

											if (parseFloat(LocRemCube) > parseFloat(ItemCube)) {
												RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
												location_found = true;
												Location = locResults[j].getValue('name');
												LocArry[0] = Location;
												LocArry[1] = RemCube;
												LocArry[2] = locResults[j].getId();
												LocArry[3] = OBLocGroup;
												LocArry[4] = PickSeqNo;
												LocArry[5] = IBLocGroup;
												LocArry[6] = PutSeqNo;
												LocArry[7] = LocRemCube;
												LocArry[8] = zoneid;
												nlapiLogExecution('ERROR', 'Location', Location);
												nlapiLogExecution('ERROR', 'Location Cube', RemCube);
												return LocArry;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}// end of put rule for loop
	else
	{
		nlapiLogExecution('ERROR', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	//  } // end of main if
	return LocArry;
}
function getLocationDetails(binlocid,Item,SiteLocation,ItemCube)
{
	var LocArry = new Array();
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('name');
	columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
	columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
	columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
	columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

	nlapiLogExecution('ERROR', 'binlocid', binlocid);
	nlapiLogExecution('ERROR', 'SiteLocation', SiteLocation);
	nlapiLogExecution('ERROR', 'ItemCube', ItemCube);
	var filters = new Array();
	//filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
	filters.push(new nlobjSearchFilter('internalId', null , 'is', binlocid));
	if(SiteLocation != null && SiteLocation != "")
		filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof',SiteLocation));
	t1 = new Date();

	//var locResults = nlapiLoadRecord('customrecord_ebiznet_createinv', binlocid);
	var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);



	nlapiLogExecution('ERROR', 'locResults', locResults);
	if (locResults != null && locResults != '') {
		nlapiLogExecution('ERROR', 'ifnot ');
		for (var i = 0; i < locResults.length; i++) {
			Location = locResults[i].getValue('name');
			var locationIntId = locResults[i].getId();

			LocRemCube = GeteLocCube(locationIntId);
			nlapiLogExecution('ERROR', 'Location', Location);

			nlapiLogExecution('ERROR', 'locResults[i].getId()', locResults[i].getId());
			OBLocGroup = locResults[i].getValue('custrecord_outboundinvlocgroupid');
			PickSeqNo = locResults[i].getValue('custrecord_startingpickseqno');
			IBLocGroup = locResults[i].getValue('custrecord_inboundinvlocgroupid');
			PutSeqNo = locResults[i].getValue('custrecord_startingputseqno');
			nlapiLogExecution('ERROR', 'LocRemCube', LocRemCube);
			//case 20127124
			if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
				RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
				location_found = true;

				LocArry[0] = Location;
				LocArry[1] = RemCube;
				LocArry[2] = locationIntId;		//locResults[i].getId();
				LocArry[3] = OBLocGroup;
				LocArry[4] = PickSeqNo;
				LocArry[5] = IBLocGroup;
				LocArry[6] = PutSeqNo;
				LocArry[7] = LocRemCube;
				//LocArry[8] = zoneid;
				nlapiLogExecution('ERROR', 'SA-Location', Location);
				nlapiLogExecution('ERROR', 'Location Internal Id',locationIntId);
				nlapiLogExecution('ERROR', 'Sa-Location Cube', RemCube);
				return LocArry;
			}
		}
	}
}

function SetPalletization(itemId,ordQty,poloc,Lotnovalue,vLotFifoDate,vLotExpDate,woid,vmemo,vlp,vBeginDate,vBeginTime,
		vSointernalId,wointernalid,arrdims,getFetchedItemStatusId,getFetchedPackCode,vheaderlotno)
{ 	 
	nlapiLogExecution('Debug','Into SetPalletization',TimeStampinSec());

	/* To fetch item Sub type.*/
	var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
	var columns = nlapiLookupField('item', itemId, fields);

	var lgItemType = columns.recordType;
	var batchflg = columns.custitem_ebizbatchlot;
	var itemfamId= columns.custitem_item_family;
	var itemgrpId= columns.custitem_item_group;


	// Get item dimentions for the item id					

	var eachQty = 0;
	var caseQty = 0;
	var palletQty = 0;
	var noofPallets = 0;
	var noofCases = 0;
	var remQty = 0;
	var tOrdQty = ordQty;
	var AllItemDims = getItemDimensions(itemId);
	if (AllItemDims != null && AllItemDims.length > 0) 
	{							
		for(var p = 0; p < AllItemDims.length; p++)
		{
			var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
			var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');

			if(AllItemDims[p].getValue('custrecord_ebizitemdims') == itemId)
			{
				nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', itemId);

				var skuDim = AllItemDims[p].getValue('custrecord_ebizuomlevelskudim');
				var skuQty = AllItemDims[p].getValue('custrecord_ebizqty');						
				//nlapiLogExecution('ERROR','skuDim',skuDim);
				//nlapiLogExecution('ERROR','SKUQTY',skuQty);
				if(skuDim == '1')
				{
					eachQty = skuQty;
				}
				else if(skuDim == '2')
				{
					caseQty = skuQty;
				}
				else if(skuDim == '3')
				{
					palletQty = skuQty;
				}

			}									
		}

		if(parseFloat(eachQty) == 0)
		{
			nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim EACH is not configured for SKU: '+ itemId);
		}
		if(parseFloat(caseQty) == 0)
		{
			nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim CASE is Not Configured for SKU: '+ itemId);
		}
		if(parseFloat(palletQty) == 0)
		{
			nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim PALLET is not Configured for SKU: '+ itemId);
		}
		if(eachQty>0 ||caseQty>0 || palletQty>0)  // break down selected PO line quantity provided Item dims are configured
		{					
			//nlapiLogExecution('ERROR', 'Item', itemId);

			var str = 'eachQty.' + eachQty + '<br>';
			str = str + 'caseQty.' + caseQty + '<br>';
			str = str + 'palletQty.' + palletQty + '<br>';
			str = str + 'ordQty.' + ordQty + '<br>';

			nlapiLogExecution('DEBUG', 'UOM Qtys', str);

			// calculate how many LP's are required to break down selected PO line
			var noofLpsRequired = 0;

			// Get the number of pallets required for this item (there will be one LP per pallet)
			noofPallets = getSKUDimCount(ordQty, palletQty);
			remQty = parseFloat(ordQty) - (parseFloat(noofPallets) * parseFloat(palletQty));

			noofLpsRequired = parseFloat(noofPallets);

			// check whether we need to break down into cases or not
			if (parseFloat(remQty) > 0 && parseFloat(caseQty) != 0) 
			{
				// Get the number of cases required for this item (only one LP for all cases)
				noofCases = getSKUDimCount(remQty, caseQty);
				remQty = parseFloat(remQty) - (noofCases * caseQty);
				if(noofCases > 0)
					noofLpsRequired = parseFloat(noofLpsRequired) + 1;
			}

			// check whether we need to further break down into eaches or not (there will be one LP for all eaches)
			if (parseFloat(remQty) > 0)  
				noofLpsRequired = parseFloat(noofLpsRequired) + 1;


			var str = 'noofPallets.' + noofPallets + '<br>';
			str = str + 'noofCases.' + noofCases + '<br>';
			str = str + 'remQty.' + remQty + '<br>';
			str = str + 'noofLpsRequired.' + noofLpsRequired + '<br>';

			nlapiLogExecution('DEBUG', '# of LPs', str);

			// In single call we are updating MaxLP in "LP range" custom record with required no of lp's					

			var MultipleLP = GetMultipleLP(1,1,noofLpsRequired,poloc);


			var maxLPPrefix = MultipleLP.split(",")[0];
			var maxLP = MultipleLP.split(",")[1];;
			maxLP = parseFloat(maxLP) - parseFloat(noofLpsRequired); 

			nlapiLogExecution('ERROR', 'MAX LP:', maxLP);

			var stageLocn = "";
			var DockLoc = "";
			var endloc = "";
			var beginloc = "";

			var RcvQty=0; 

			var totalrcvqty=0;
			for (var p = 0; p < noofPallets; p++) 
			{
				// Calculate LP
				maxLP = parseFloat(maxLP) + 1;
				var LP = maxLPPrefix + maxLP.toString();

				if(RcvQty=='' || RcvQty==null)
					RcvQty=0;

				//RcvQty = parseFloat(RcvQty)+parseFloat(palletQty);
				RcvQty = parseFloat(palletQty);
				totalrcvqty=parseFloat(totalrcvqty)+parseFloat(RcvQty);

				//Case # 20127203 Start
				var priorityPutawayLocnArr = priorityPutaway(itemId, RcvQty,poloc,getFetchedItemStatusId);
				//Case # 20127203 End
				var priorityRemainingQty = priorityPutawayLocnArr[0];
				var priorityQty = priorityPutawayLocnArr[1];
				var priorityLocnID = priorityPutawayLocnArr[2];
				var zoneid = priorityPutawayLocnArr[3];
				var priorityfixedLP = priorityPutawayLocnArr[5];

				var str = 'LP.' + LP + '<br>';
				str = str + 'Pallet RcvQty.' + RcvQty + '<br>';
				str = str + 'priorityRemainingQty.' + priorityRemainingQty + '<br>';
				str = str + 'priorityQty.' + priorityQty + '<br>';
				str = str + 'priorityLocnID.' + priorityLocnID + '<br>';
				str = str + 'zoneid.' + zoneid + '<br>';
				str = str + 'priorityfixedLP.' + priorityfixedLP + '<br>';

				nlapiLogExecution('DEBUG', 'Pallet Priority Putaway Values', str);

				if (priorityQty != 0){
					taskType = "2"; // PUTW
					wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
					beginLocn = priorityLocnID;
					endLocn = "";
					var LP=null;
					if(priorityfixedLP !=null && priorityfixedLP !='')
						LP=priorityfixedLP;
					//ProcessTransactionPickFace(itemId,RcvQty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,arrdims,getFetchedItemStatusId,getFetchedPackCode,wointernalid,vheaderlotno,vmemo,vBeginDate,vBeginTime,beginLocn,woid,priorityQty);
					maxLP=ProcessTransactionPickFace(itemId,priorityQty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,
							arrdims,getFetchedItemStatusId,getFetchedPackCode,wointernalid,vheaderlotno,vmemo,
							vBeginDate,vBeginTime,beginLocn,woid,priorityQty,maxLP,maxLPPrefix,vlp);
				}
				else
					priorityRemainingQty=RcvQty;

				if (parseFloat(priorityRemainingQty) != 0) {
					var LP=null;
					maxLP=ProcessTransaction(itemId,priorityRemainingQty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,
							arrdims,getFetchedItemStatusId,getFetchedPackCode,wointernalid,vheaderlotno,vmemo,
							vBeginDate,vBeginTime,woid,maxLP,maxLPPrefix,vlp);
				}

			}

			nlapiLogExecution('ERROR', 'Total RcvQty', totalrcvqty);



			//for (var p = 0; p < noofCases; p++)
			if(parseFloat(noofCases) > 0)
			{
				/*// Calculate LP
				maxLP = parseFloat(maxLP) + 1;
				var LP = maxLPPrefix + maxLP.toString();
				nlapiLogExecution('ERROR', 'Case LP', LP);	*/						

				RcvQty = parseFloat(noofCases) * parseFloat(caseQty);
				//				beginloc = "";

				var priorityPutawayLocnArr = priorityPutaway(itemId, RcvQty,poloc,getFetchedItemStatusId);
				var priorityRemainingQty = priorityPutawayLocnArr[0];
				var priorityQty = priorityPutawayLocnArr[1];
				var priorityLocnID = priorityPutawayLocnArr[2];
				var zoneid = priorityPutawayLocnArr[3];
				var priorityfixedLP = priorityPutawayLocnArr[5];

				var str = 'totalrcvqty' + totalrcvqty + '<br>';
				str = str + 'Case RcvQty.' + RcvQty + '<br>';
				str = str + 'priorityRemainingQty.' + priorityRemainingQty + '<br>';
				str = str + 'priorityQty.' + priorityQty + '<br>';
				str = str + 'priorityLocnID.' + priorityLocnID + '<br>';
				str = str + 'zoneid.' + zoneid + '<br>';
				str = str + 'priorityfixedLP.' + priorityfixedLP + '<br>';

				nlapiLogExecution('DEBUG', 'Case Priority Putaway Values', str);

				if (priorityQty != 0){
					taskType = "2"; // PUTW
					wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
					beginLocn = priorityLocnID;
					endLocn = "";
					var LP=null;
					if(priorityfixedLP !=null && priorityfixedLP !='')
						LP=priorityfixedLP;
					maxLP=ProcessTransactionPickFace(itemId,priorityQty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,
							arrdims,getFetchedItemStatusId,getFetchedPackCode,wointernalid,vheaderlotno,vmemo,
							vBeginDate,vBeginTime,beginLocn,woid,priorityQty,maxLP,maxLPPrefix,vlp);
				}
				else
					priorityRemainingQty=RcvQty;

				if (parseFloat(priorityRemainingQty) != 0) {
					var LP=null;
					maxLP=ProcessTransaction(itemId,priorityRemainingQty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,
							arrdims,getFetchedItemStatusId,getFetchedPackCode,wointernalid,vheaderlotno,vmemo,
							vBeginDate,vBeginTime,woid,maxLP,maxLPPrefix,vlp);
				}
			}

			nlapiLogExecution('ERROR', 'remQty', remQty);
			//alert("Rem Qty After Casses :" +remQty);
			if (parseFloat(remQty) > 0) 
			{
				// Calculate LP
				/*maxLP = parseFloat(maxLP) + 1;
				var LP = maxLPPrefix + maxLP.toString();*/
				nlapiLogExecution('ERROR', 'Each LP', LP);
				RcvQty = remQty;
				nlapiLogExecution('ERROR', 'Each RcvQty', RcvQty);

				var priorityPutawayLocnArr = priorityPutaway(itemId, RcvQty,poloc,getFetchedItemStatusId);
				var priorityRemainingQty = priorityPutawayLocnArr[0];
				var priorityQty = priorityPutawayLocnArr[1];
				var priorityLocnID = priorityPutawayLocnArr[2];
				var zoneid = priorityPutawayLocnArr[3];
				var priorityfixedLP = priorityPutawayLocnArr[5];

				var str = 'remQty' + remQty + '<br>';
				str = str + 'Each RcvQty.' + RcvQty + '<br>';
				str = str + 'priorityRemainingQty.' + priorityRemainingQty + '<br>';
				str = str + 'priorityQty.' + priorityQty + '<br>';
				str = str + 'priorityLocnID.' + priorityLocnID + '<br>';
				str = str + 'zoneid.' + zoneid + '<br>';
				str = str + 'priorityfixedLP.' + priorityfixedLP + '<br>';

				nlapiLogExecution('DEBUG', 'Each Priority Putaway Values', str);

				if (priorityQty != 0){
					taskType = "2"; // PUTW
					wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
					beginLocn = priorityLocnID;
					endLocn = "";
					var LP=null;
					if(priorityfixedLP !=null && priorityfixedLP !='')
						LP=priorityfixedLP;
					maxLP=ProcessTransactionPickFace(itemId,priorityQty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,arrdims,
							getFetchedItemStatusId,getFetchedPackCode,wointernalid,vheaderlotno,vmemo,vBeginDate,vBeginTime,
							beginLocn,woid,priorityQty,maxLP,maxLPPrefix,vlp);
				}
				else
					priorityRemainingQty=RcvQty;

				if (parseFloat(priorityRemainingQty) != 0) {
					var LP=null;
					maxLP=ProcessTransaction(itemId,priorityRemainingQty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,arrdims,
							getFetchedItemStatusId,getFetchedPackCode,wointernalid,vheaderlotno,vmemo,vBeginDate,vBeginTime,
							woid,maxLP,maxLPPrefix,vlp);
				}
				//ProcessTransaction(itemId,RcvQty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,arrdims,getFetchedItemStatusId,getFetchedPackCode,wointernalid,vheaderlotno,vmemo,vBeginDate,vBeginTime);

			}
			//Case 20125252 start : retun value
			return maxLP;
			//Case 20125252 end
		}// end of auto break down calculation
	} 
	else 
	{ 
		nlapiLogExecution('ERROR', 'UOM Dims', 'Dimensions are not configured ');
	}

	nlapiLogExecution('Debug','Out of SetPalletization',TimeStampinSec());
}

function CreateInventory(vitem,vmemo,vloc,LocId,vqty,vlp,Lotnovalue,OubLocId,PickSeq,InbLocId,PutSeq,getFetchedItemStatusId,
		getFetchedPackCode,vLotFifoDate,vLotExpDate,wointernalid,PutMethod)
{
	try{
		nlapiLogExecution('ERROR', 'Into CreateInventory',TimeStampinSec());
		//Case# 201411044 starts (//Checking FIFO Policy.)
		var vTempfifodate=''; 
		if(vitem != null && vitem !='')
		{
			var ItemType ='';
			var batchflg="F"; 
			var itemgrpId='';
			var itemfamId='';
			var vfilter=new Array();						
			vfilter[0]=new nlobjSearchFilter("internalid",null,"anyof",vitem);
			var vcolumn=new Array();
			vcolumn[0]=new nlobjSearchColumn("type");
			vcolumn[1]=new nlobjSearchColumn("custitem_ebizbatchlot");
			vcolumn[2]=new nlobjSearchColumn("custitem_item_group");
			vcolumn[3]=new nlobjSearchColumn("custitem_item_family");
			var searchres=nlapiSearchRecord("item",null,vfilter,vcolumn);
			if(searchres!=null &&searchres!="")
			{
				ItemType = searchres[0].recordType;
				batchflg=searchres[0].getValue("custitem_ebizbatchlot");
				itemgrpId=searchres[0].getValue("custitem_item_group");
				itemfamId=searchres[0].getValue("custitem_item_family");
			}

			if(vLotFifoDate == null || vLotFifoDate =='' || vLotFifoDate =='undefined')
			{
				vTempfifodate = WOFifovalueCheck(ItemType,vitem,itemfamId,itemgrpId,'',wointernalid,Lotnovalue);
				nlapiLogExecution('ERROR','FIFO DATE from FIFOvalue check fn',vTempfifodate);
			}
			nlapiLogExecution('ERROR','Into CreateInventory---FIFO DATE from FIFOvalue check fn',vLotFifoDate);

		}
		//Case# 201411044 ends

		//Code added by suman on 190913
		var id='';
		var putItemPC='1';
		var varPaltQty = getMaxUOMQty(vitem,putItemPC);

		var str = 'vitem=' + vitem + '<br>';
		str = str +'getFetchedItemStatusId =' + getFetchedItemStatusId + '<br>';
		str = str +'LocId=' + LocId + '<br>';
		str = str +'vLotFifoDate=' + vLotFifoDate + '<br>';
		str = str +'putItemPC=' + putItemPC + '<br>';
		str = str +'Lotnovalue=' + Lotnovalue + '<br>';
		str = str +'varPaltQty=' + varPaltQty + '<br>';
		str = str +'PutMethod=' + PutMethod + '<br>';
		str = str +'varPaltQty=' + varPaltQty + '<br>';

		nlapiLogExecution('ERROR', 'Function parameters',str );


		var varMergeLP = "F";
		//case# 20149787 starts(if condition is changed from methodid to PutMethod )
		//if(methodid!=null && methodid!='')
		if(PutMethod!=null && PutMethod!='')
			varMergeLP = isMergeLP(vloc,PutMethod);
		nlapiLogExecution('ERROR', 'varMergeLP', varMergeLP);
		if(varMergeLP=="T")
		{
			nlapiLogExecution('ERROR', 'Checking for merge the item');
			var filtersinvt = new Array();
			if(vitem!=null&&vitem!="")
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', vitem));

			if(getFetchedItemStatusId!=null&&getFetchedItemStatusId!="")
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', getFetchedItemStatusId));

			if(LocId!=null&&LocId!="")
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', LocId));
			else
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is',LocId));
			filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', '19'));

			if(vLotFifoDate!=null && vLotFifoDate!='' && vLotFifoDate!='undefined')
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', vLotFifoDate));

			if(putItemPC!=null && putItemPC!='')
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', putItemPC));							

			if(Lotnovalue!=null && Lotnovalue!='')
			{
				nlapiLogExecution('ERROR', 'Lotnovalue inside', Lotnovalue);
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', Lotnovalue));
			}

			if(varPaltQty!=null && varPaltQty!='')
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'lessthan', varPaltQty));							

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
			columnsinvt[0].setSort();

			var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			nlapiLogExecution('ERROR', 'invtsearchresults', invtsearchresults);
			if(invtsearchresults != null && invtsearchresults != ''  && invtsearchresults.length > 0)
			{
				nlapiLogExecution('ERROR', 'invtsearchresults.length', invtsearchresults.length);
				var putQty=vqty;
				var newputqty=putQty;
				var BoolInvMerged=false;
				for (var i = 0; i < invtsearchresults.length; i++) 
				{
					var qoh=invtsearchresults[i].getValue('custrecord_ebiz_qoh');
					nlapiLogExecution('ERROR', 'exiting qoh', qoh);
					nlapiLogExecution('ERROR', 'putQty', putQty);
					nlapiLogExecution('ERROR', 'varPaltQty', varPaltQty);

					if(newputqty>0 && (parseFloat(putQty)+parseFloat(qoh))<=parseFloat(varPaltQty))
					{
						nlapiLogExecution('ERROR', 'Into Merging Inventory', 'Sucess');
						nlapiLogExecution('ERROR', 'Inventory Record ID', invtsearchresults[i].getId());
						BoolInvMerged=true;
						nlapiLogExecution('ERROR', 'BoolInvMerged', BoolInvMerged);

						var scount=1;
						LABL1: for(var z=0;z<scount;z++)
						{
							try
							{
								var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[i].getId());											

								var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
								var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
								var varExistLP=invttransaction.getFieldValue('custrecord_ebiz_inv_lp');

								nlapiLogExecution('ERROR', 'varExistQOHQty', varExistQOHQty);
								nlapiLogExecution('ERROR', 'varExistInvQty', varExistInvQty);

								var varupdatedQOHqty = parseFloat(varExistQOHQty) + parseFloat(putQty);
								var varupdatedInvqty = parseFloat(varExistInvQty) + parseFloat(putQty);

								invttransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(varupdatedQOHqty).toFixed(5));
								invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varupdatedInvqty).toFixed(5));
								invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

								id = nlapiSubmitRecord(invttransaction,false, true);
								InvtRefNo=invtsearchresults[i].getId();
								nlapiLogExecution('ERROR', 'nlapiSubmitRecord', 'Record Submitted');
								nlapiLogExecution('ERROR', 'Inventory is merged to the LP '+varExistLP);
								newputqty=0;
							}
							catch(ex)
							{
								nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

								var exCode='CUSTOM_RECORD_COLLISION'; 
								var wmsE='Inventory record being updated by another user. Please try again...';
								if (ex instanceof nlobjError) 
								{	
									wmsE=ex.getCode() + '\n' + ex.getDetails();
									exCode=ex.getCode();
								}
								else
								{
									wmsE=ex.toString();
									exCode=ex.toString();
								}  

								nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

								if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
								{ 
									scount=scount+1;
									continue LABL1;
								}
								else break LABL1;
							}
						}
					}
				}
				if(BoolInvMerged==false)
				{
					nlapiLogExecution('ERROR', 'BoolInvMerged ',BoolInvMerged);
					var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
					invrecord.setFieldValue('name', vitem+1);					
					invrecord.setFieldValue('custrecord_ebiz_inv_sku', vitem); 	
					var vMainItem=vitem;
					var vMainQty=vqty;
					//invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
					//invrecord.setFieldValue('custrecord_ebiz_inv_qty', vqty);
					//invrecord.setFieldValue('custrecord_ebiz_inv_account_no',vaccountno);
					invrecord.setFieldValue('custrecord_ebiz_inv_note1',vmemo);
					invrecord.setFieldValue('custrecord_ebiz_inv_loc', vloc);

					invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);

					//invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);
					invrecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(5));
					invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		
					invrecord.setFieldValue('custrecord_ebiz_inv_lp', vlp);	
					if(Lotnovalue!=null && Lotnovalue != "") 
						invrecord.setFieldValue('custrecord_ebiz_inv_lot', Lotnovalue);	
					invrecord.setFieldValue('custrecord_ebiz_inv_qty',parseFloat(vqty).toFixed(5));
					invrecord.setFieldValue('custrecord_outboundinvlocgroupid',OubLocId);
					if(PickSeq != null && PickSeq != '')
						invrecord.setFieldValue('custrecord_pickseqno',PickSeq);
					invrecord.setFieldValue('custrecord_inboundinvlocgroupid',InbLocId);

					if(PutSeq != null && PutSeq != '')
						invrecord.setFieldValue('custrecord_putseqno',PutSeq);	

					//newly added fields
					invrecord.setFieldValue('custrecord_ebiz_inv_sku_status',getFetchedItemStatusId);	

					if(getFetchedPackCode==null || getFetchedPackCode=='')
						getFetchedPackCode=1;

					invrecord.setFieldValue('custrecord_ebiz_inv_packcode',getFetchedPackCode);	
					//invrecord.setFieldValue('custrecord_ebiz_inv_lp',LP);

					//Added by Satish.N on 10/27/2011
					invrecord.setFieldValue('custrecord_ebiz_displayfield','N');
					invrecord.setFieldValue('custrecord_invttasktype','5');
					invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
					if(vLotFifoDate != null && vLotFifoDate != '')
						invrecord.setFieldValue('custrecord_ebiz_inv_fifo',vLotFifoDate);
					//Case# 201411044 starts
					else if(vTempfifodate != null && vTempfifodate != '')
						invrecord.setFieldValue('custrecord_ebiz_inv_fifo',vTempfifodate);
					//Case# 201411044 ends
					if(vLotExpDate != null && vLotExpDate != '')
						invrecord.setFieldValue('custrecord_ebiz_expdate',vLotExpDate);
					invrecord.setFieldValue('custrecord_ebiz_transaction_no', wointernalid);
					//Upto Here

					//invrecord.setFieldValue('custrecord_ebiz_inv_account_no', '120');					
					id = nlapiSubmitRecord(invrecord,false,true );	
				}
			}
		}
		//End of code as on 190913.
		else
		{

			var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
			invrecord.setFieldValue('name', vitem+1);					
			invrecord.setFieldValue('custrecord_ebiz_inv_sku', vitem); 	
			var vMainItem=vitem;
			var vMainQty=vqty;
			//invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
			//invrecord.setFieldValue('custrecord_ebiz_inv_qty', vqty);
			//invrecord.setFieldValue('custrecord_ebiz_inv_account_no',vaccountno);
			invrecord.setFieldValue('custrecord_ebiz_inv_note1',vmemo);
			invrecord.setFieldValue('custrecord_ebiz_inv_loc', vloc);

			invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);

			//invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);
			invrecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(5));
			invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		
			invrecord.setFieldValue('custrecord_ebiz_inv_lp', vlp);	
			if(Lotnovalue!=null && Lotnovalue != "") 
				invrecord.setFieldValue('custrecord_ebiz_inv_lot', Lotnovalue);	
			invrecord.setFieldValue('custrecord_ebiz_inv_qty',parseFloat(vqty).toFixed(5));
			invrecord.setFieldValue('custrecord_outboundinvlocgroupid',OubLocId);
			if(PickSeq != null && PickSeq != '')
				invrecord.setFieldValue('custrecord_pickseqno',PickSeq);
			invrecord.setFieldValue('custrecord_inboundinvlocgroupid',InbLocId);

			if(PutSeq != null && PutSeq != '')
				invrecord.setFieldValue('custrecord_putseqno',PutSeq);	

			//newly added fields
			invrecord.setFieldValue('custrecord_ebiz_inv_sku_status',getFetchedItemStatusId);	

			if(getFetchedPackCode==null || getFetchedPackCode=='')
				getFetchedPackCode=1;

			invrecord.setFieldValue('custrecord_ebiz_inv_packcode',getFetchedPackCode);	
			//invrecord.setFieldValue('custrecord_ebiz_inv_lp',LP);

			//Added by Satish.N on 10/27/2011
			invrecord.setFieldValue('custrecord_ebiz_displayfield','N');
			invrecord.setFieldValue('custrecord_invttasktype','5');
			invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
			if(vLotFifoDate != null && vLotFifoDate != '')
				invrecord.setFieldValue('custrecord_ebiz_inv_fifo',vLotFifoDate);
			//Case# 201411044 starts
			else if(vTempfifodate != null && vTempfifodate != '')
				invrecord.setFieldValue('custrecord_ebiz_inv_fifo',vTempfifodate);
			//Case# 201411044 ends
			if(vLotExpDate != null && vLotExpDate != '')
				invrecord.setFieldValue('custrecord_ebiz_expdate',vLotExpDate);
			invrecord.setFieldValue('custrecord_ebiz_transaction_no', wointernalid);
			//Upto Here

			//invrecord.setFieldValue('custrecord_ebiz_inv_account_no', '120');					
			var id=nlapiSubmitRecord(invrecord,false,true );
		}
		return id;
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','no Sufficient Remaining Cube For KitItem', e.toString());
		var ErrorMsg = nlapiCreateError('no Sufficient Remaining Cube For KitItem','',true);
		throw ErrorMsg;

	}
}

function ProcessTransaction(itemId,vqty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,arrdims,getFetchedItemStatusId,
		getFetchedPackCode,wointernalid,vheaderlotno,vmemo,vBeginDate,vBeginTime,woid,maxLP,maxLPPrefix,vlp)
{ 
	var ItemType = nlapiLookupField('item', itemId, 'recordType');


	var ItemCube = 0;
	if (arrdims.length > 0) {
		/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
		if (!isNaN(arrdims[0])&& arrdims[0] !='' && arrdims[0]!=null) {/*** up to here ***/
			ItemCube = (parseFloat(arrdims[0]) * parseFloat(vqty));
			nlapiLogExecution('ERROR', 'ItemCube', ItemCube);
		}
		else {
			ItemCube = 0;
		}
	}
	else {
		ItemCube = 0;
	}


	nlapiLogExecution('ERROR', 'Fetched Item Status Internal Id', getFetchedItemStatusId);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', itemId));
	filters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(poloc!=null && poloc!='')
		filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', poloc));

	//custrecord_pickbinloc
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_pickbinloc');
	columns[1] = new nlobjSearchColumn('custrecord_maxqty');
	columns[2] = new nlobjSearchColumn('custrecord_pickzone');
	columns[3] = new nlobjSearchColumn('custrecord_pickface_itemstatus');
	columns[4] = new nlobjSearchColumn('custrecord_pickfacesku');
	columns[5] = new nlobjSearchColumn('custrecord_pickface_location');

	var pickFaceResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);
	if(pickFaceResults != null && pickFaceResults != '')
		nlapiLogExecution('ERROR', 'pickFaceResults.length', pickFaceResults.length);

	var filters1 = new Array();
	filters1[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemId);            
	filters1[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

	var columns1 = new Array();
	columns1[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns1[1] = new nlobjSearchColumn('custrecord_ebizqty');
	columns1[2] = new nlobjSearchColumn('custrecord_ebizitemdims');
	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters1, columns1);

	var filters2 = new Array();

	var columns2 = new Array();
	columns2[0] = new nlobjSearchColumn('custitem_item_family');
	columns2[1] = new nlobjSearchColumn('custitem_item_group');
	columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
	columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
	columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
	columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
	columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

	filters2.push(new nlobjSearchFilter('internalid', null, 'is', itemId));

	var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);
	/*var filters3=new Array();
	filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters3.push(new nlobjSearchFilter('custrecord_ebiz_wo_flag', null, 'is', 'T'));
	var columns3=new Array();
	columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
	columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
	columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
	columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
	columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
	columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
	columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
	columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
	columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
	columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
	columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns3[14] = new nlobjSearchColumn('formulanumeric');
	columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns3[14].setSort();



	var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);*/
//	nlapiLogExecution('ERROR', 'itemId', itemId);
//	nlapiLogExecution('ERROR', 'getFetchedItemStatusId', getFetchedItemStatusId);
//	nlapiLogExecution('ERROR', 'ItemCube', ItemCube);
//	nlapiLogExecution('ERROR', 'poloc', poloc);
//	nlapiLogExecution('ERROR', 'ItemInfoResults', ItemInfoResults);
//	nlapiLogExecution('ERROR', 'putrulesearchresults', putrulesearchresults);
	//case 201410773 start	

	var filters3=new Array();
	filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var columns3=new Array();
	columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
	columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
	columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
	columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
	columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
	columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
	columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
	columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
	columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
	columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
	columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns3[14] = new nlobjSearchColumn('formulanumeric');
	columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
	//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
	columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
	// Upto here on 29OCT2013 - Case # 20124515
	columns3[14].setSort();

	var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);

	var Loctdims=new Array();
	Loctdims = GetPutawayLocationNew(itemId, getFetchedPackCode, getFetchedItemStatusId, null, ItemCube,ItemType.recordType,poloc,ItemInfoResults,putrulesearchresults);

	//Loctdims = GetPutawayLocationNew(itemId, null, getFetchedItemStatusId, null, ItemCube,null,poloc,ItemInfoResults,putrulesearchresults);
	//Loctdims = GetPutawayLocationRFandWO(itemId, getFetchedPackCode, getFetchedItemStatusId, null, ItemCube,ItemType.recordType,poloc,ItemInfoResults);
	//case 201410773 end
	//Loctdims = GetPutawayLocation(itemId, null, null, null, ItemCube,ItemType,poloc);

	var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube,PutMethod;
	if (Loctdims != null && Loctdims != '' && Loctdims.length > 0) {
		if (Loctdims[0] != "") {
			LocName = Loctdims[0];
			nlapiLogExecution('ERROR', 'LocName', LocName);
		}
		else {
			LocName = "";
			nlapiLogExecution('ERROR', 'in else LocName', LocName);
		}
		if (!isNaN(Loctdims[1])) {
			RemCube = Loctdims[1];
		}
		else {
			RemCube = 0;
		}
		if (!isNaN(Loctdims[2])) {
			LocId = Loctdims[2];
			nlapiLogExecution('ERROR', 'LocId', LocId);
		}
		else {
			LocId = "";
			nlapiLogExecution('ERROR', 'in else LocId', LocId);
		}
		if (!isNaN(Loctdims[3])) {
			OubLocId = Loctdims[3];
			nlapiLogExecution('ERROR', 'OubLocId ', OubLocId );
		}
		else {
			OubLocId = "";
		}
		if (!isNaN(Loctdims[4])) {
			PickSeq = Loctdims[4];
			nlapiLogExecution('ERROR', 'PickSeq ', PickSeq );
		}
		else {
			PickSeq = "";
		}
		if (!isNaN(Loctdims[5])) {
			InbLocId = Loctdims[5];
			nlapiLogExecution('ERROR', 'InbLocId ', InbLocId );
		}
		else {
			InbLocId = "";
		}
		if (!isNaN(Loctdims[6])) {
			PutSeq = Loctdims[6];
			nlapiLogExecution('ERROR', 'PutSeq ', PutSeq );
		}
		else {
			PutSeq = "";
		}
		if (!isNaN(Loctdims[7])) {
			LocRemCube = Loctdims[7];
		}
		else {
			LocRemCube = "";
		}
		if (!isNaN(Loctdims[9])) {
			PutMethod = Loctdims[9];
		}
		else {
			PutMethod = "";
		}
	}
	else
	{
		nlapiLogExecution('ERROR','no Sufficient Remaining Cube For KitItem');
		var vErr=" Location not generated for KitItem";
		var verror='error';
		//var ErrorMsg = nlapiCreateError('no Sufficient Remaining Cube For KitItem','',true);
		//showInlineMessage(form, 'Error', vErr, "");
		//response.writePage(form);
		return verror;

	}
	maxLP = parseFloat(maxLP) + 1;
	LP = maxLPPrefix + maxLP.toString();
	nlapiLogExecution('ERROR', 'LP  ' , LP );
	nlapiLogExecution('ERROR', 'LocId' , LocId);
	if(vlp!=null && vlp!='')
		LP=vlp;
	var invtref=	CreateInventory(itemId,vmemo,poloc,LocId,vqty,LP,Lotnovalue,OubLocId,PickSeq,InbLocId,PutSeq,getFetchedItemStatusId,getFetchedPackCode,vLotFifoDate,vLotExpDate,wointernalid,PutMethod);

	nlapiLogExecution('ERROR', 'Inventory Created ' );
	CreateOpentaskMain(vqty,vheaderlotno,LocId,itemId,LP,itemId,vBeginDate,vBeginTime,woid,getFetchedItemStatusId,invtref);
	nlapiLogExecution('ERROR', 'Updated in open task and Transaction ord line ' );
	return maxLP;
}


function ProcessTransactionPickFace(itemId,vqty,poloc,LP,Lotnovalue,vLotFifoDate,vLotExpDate,arrdims,getFetchedItemStatusId,
		getFetchedPackCode,wointernalid,vheaderlotno,vmemo,vBeginDate,vBeginTime,pickFaceLoc,woid,priorityQty,maxLP,
		maxLPPrefix,vlp)
{   

	nlapiLogExecution('ERROR', 'Into ProcessTransactionPickFace',TimeStampinSec());

	var str = 'maxLP.' + maxLP + '<br>';
	str = str + 'LP.' + LP + '<br>';

	nlapiLogExecution('DEBUG', 'LP Values', str);

	var invtref='';
	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemId));
	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', getFetchedItemStatusId));
	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', pickFaceLoc));
	if(LP!=null && LP!='')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', LP));


	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
	columnsinvt[0].setSort();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
	if(invtsearchresults != null && invtsearchresults != ''  && invtsearchresults.length > 0)
	{
		nlapiLogExecution('ERROR', 'invtsearchresults.length', invtsearchresults.length);
		//var newputqty=putQty;

		for (var i = 0; i < invtsearchresults.length; i++) 
		{
			var scount=1;
			LABL1: for(var z=0;z<scount;z++)
			{
				try
				{
					var qoh=invtsearchresults[i].getValue('custrecord_ebiz_qoh');
					//nlapiLogExecution('ERROR', 'Inventory Record ID', invtsearchresults[i].getId());

					var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[i].getId());											

					var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
					var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
					var varExistLP=invttransaction.getFieldValue('custrecord_ebiz_inv_lp');
					LP=varExistLP;

					var str = 'varExistQOHQty.' + varExistQOHQty + '<br>';
					str = str + 'varExistInvQty.' + varExistInvQty + '<br>';

					nlapiLogExecution('DEBUG', 'Qty Details', str);

					var varupdatedQOHqty = parseFloat(varExistQOHQty) + parseFloat(vqty);
					var varupdatedInvqty = parseFloat(varExistInvQty) + parseFloat(vqty);

					invttransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(varupdatedQOHqty).toFixed(5));
					invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varupdatedInvqty).toFixed(5));
					invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

					invtref=	nlapiSubmitRecord(invttransaction, true);
				}
				catch(ex)
				{
					nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					}  

					nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}
		}
	}
	else
	{
		if(pickFaceLoc != null && pickFaceLoc != '')
			var BinLocDetails = nlapiLoadRecord('customrecord_ebiznet_location', pickFaceLoc);
		var OubLocId;
		var InbLocId;
		if(BinLocDetails != null && BinLocDetails != '')
		{
			OubLocId=BinLocDetails.getFieldValue('custrecord_outboundlocgroupid');
			InbLocId=BinLocDetails.getFieldValue('custrecord_inboundlocgroupid');
		}	

		var PickSeq="";
		var PutSeq="";

		// Calculate LP
		if(LP==null || LP=='')
		{
			maxLP = parseFloat(maxLP) + 1;
			LP = maxLPPrefix + maxLP.toString();
			//nlapiLogExecution('ERROR', 'Pallet LP', LP);
			if(vlp!=null && vlp!='')
				LP=vlp;
		}
		nlapiLogExecution('ERROR', 'getFetchedItemStatusId', getFetchedItemStatusId);
		//nlapiLogExecution('ERROR', 'getFetchedItemStatusId', getFetchedItemStatusId!='1');
		if(getFetchedItemStatusId!='1')
		{
			maxLP = parseFloat(maxLP) + 1;
			LP=maxLPPrefix + maxLP.toString();

		}

		var str = 'LP.' + LP + '<br>';
		str = str + 'vlp.' + vlp + '<br>';
		str = str + 'maxLPPrefix.' + maxLPPrefix + '<br>';
		str = str + 'maxLP' + maxLP + '<br>';

		nlapiLogExecution('DEBUG', 'LP Details', str);

		//CreateInventory(itemId,vmemo,poloc,LocId,vqty,LP,Lotnovalue,OubLocId,PickSeq,InbLocId,PutSeq,getFetchedItemStatusId,getFetchedPackCode,vLotFifoDate,vLotExpDate,wointernalid);
		invtref=CreateInventory(itemId,vmemo,poloc,pickFaceLoc,priorityQty,LP,Lotnovalue,OubLocId,PickSeq,InbLocId,PutSeq,
				getFetchedItemStatusId,getFetchedPackCode,vLotFifoDate,vLotExpDate,wointernalid);
	}
	nlapiLogExecution('ERROR', 'Updated in Inventory ' );
	CreateOpentaskMain(vqty,vheaderlotno,pickFaceLoc,itemId,LP,itemId,vBeginDate,vBeginTime,woid,getFetchedItemStatusId,
			invtref);
	nlapiLogExecution('ERROR', 'Updated in open task and Transaction ord line ' );
	nlapiLogExecution('ERROR', 'Out of ProcessTransactionPickFace',TimeStampinSec());
	return maxLP;
}

function CreateOpentaskMain(vKitqty,vheaderlotno,LocId,vMainItem,vlp,varsku,vBeginDate,vBeginTime,woid,ItemStatus,invtref)
{
	//open task for main item
	var now = new Date();
	//a Date object to be used for a random value
	var now = new Date();
	//now= now.getHours();
	//Getting time in hh:mm tt format.
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}
	//nlapiLogExecution("ERROR", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p))

	var customrecord2 = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	customrecord2.setFieldValue('custrecord_tasktype', 5); // 5 for KTS, 6 for KTO
	//customrecord2.setFieldValue('custrecordact_begin_date', now);
	customrecord2.setFieldValue('custrecordact_begin_date', vBeginDate);
	customrecord2.setFieldValue('custrecord_actualbegintime', vBeginTime);
	customrecord2.setFieldValue('custrecord_act_end_date', DateStamp());
	customrecord2.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	//customrecord2.setFieldValue('custrecord_expe_qty', vqty); //  commented by sudheer on 131111
	//nlapiLogExecution('ERROR','Main SKU Qty ',parseFloat(vKitqty).toFixed(5));
	customrecord2.setFieldValue('custrecord_expe_qty', parseFloat(vKitqty).toFixed(5)); //  added by sudheer to Trimming the qty to integer.
	//nlapiLogExecution('ERROR','Expe Qty value',parseFloat(vKitqty).toFixed(5));
	customrecord2.setFieldValue('custrecord_act_qty', parseFloat(vKitqty).toFixed(5));  // Text type of field. So no need of trimming the decimal value
	customrecord2.setFieldValue('custrecord_ebiz_order_no', woid);
	customrecord2.setFieldValue('custrecord_sku', vMainItem);
	if(invtref!=null && invtref!='')
		customrecord2.setFieldValue('custrecord_invref_no', invtref);
	if(vheaderlotno!= null && vheaderlotno != "")
		customrecord2.setFieldValue('custrecord_batch_no', vheaderlotno); 


	customrecord2.setFieldValue('custrecord_actbeginloc', LocId);
	customrecord2.setFieldValue('custrecord_actendloc', LocId);		




	customrecord2.setFieldValue('custrecord_wms_status_flag', 3);	// Putaway completed				
	//customrecord2.setFieldValue('custrecord_lpno', vlp); 
	customrecord2.setFieldValue('custrecord_upd_date', now);
	//customrecord2.setFieldValue('custrecord_sku', vskuText);
	customrecord2.setFieldValue('custrecord_ebiz_sku_no', vMainItem);
	customrecord2.setFieldValue('name', vMainItem+1);		
	customrecord2.setFieldValue('custrecord_lpno', vlp);		
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	customrecord2.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
	customrecord2.setFieldValue('custrecord_ebizuser', currentUserID);	
	customrecord2.setFieldValue('custrecord_sku_status', ItemStatus);	
	// customrecord2.setFieldValue('custrecord_wms_location', LocId);
	nlapiSubmitRecord(customrecord2);	

	var filtersTL = new Array();
	filtersTL[0] = new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'is', 'WO'+vMainItem);            
	var TransLineResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filtersTL, null);
	if(TransLineResults == null || TransLineResults == '' )
	{
		var customkittranrecord = nlapiCreateRecord('customrecord_ebiznet_order_line_details');					
		customkittranrecord.setFieldValue('custrecord_orderlinedetails_checkin_qty', parseFloat(vKitqty).toFixed(5)); 
		customkittranrecord.setFieldValue('custrecord_orderlinedetails_putgen_qty', parseFloat(vKitqty).toFixed(5)); 
		customkittranrecord.setFieldValue('custrecord_orderlinedetails_putconf_qty', parseFloat(vKitqty).toFixed(5)); 
		customkittranrecord.setFieldValue('custrecord_orderlinedetails_status_flag', 3); //3 stands for s inbound process 
		customkittranrecord.setFieldValue('custrecord_orderlinedetails_item', vMainItem);
		customkittranrecord.setFieldValue('custrecord_orderlinedetails_ord_category',4); // 4 stands for workorder	
		customkittranrecord.setFieldValue('custrecord_orderlinedetails_order_no','WO'+vMainItem);
		customkittranrecord.setFieldValue('name','WO'+varsku); 
		nlapiSubmitRecord(customkittranrecord);
		nlapiLogExecution('ERROR','Updated in Transaction Ord line1');
	}
	else
	{
		var TLId=TransLineResults[0].getId();
		var customkittranrecord = nlapiLoadRecord('customrecord_ebiznet_order_line_details', TLId);
		if(customkittranrecord != null && customkittranrecord != '')
		{
			var OldQty=customkittranrecord.getFieldValue('custrecord_orderlinedetails_checkin_qty');
			var vNewQty;
			if(OldQty != null && OldQty != '')
				vNewQty=parseFloat(OldQty) + parseFloat(vKitqty);
			//nlapiLogExecution('ERROR','vNewQty',vNewQty);
			customkittranrecord.setFieldValue('custrecord_orderlinedetails_checkin_qty', parseFloat(vNewQty).toFixed(5)); 
			customkittranrecord.setFieldValue('custrecord_orderlinedetails_putgen_qty', parseFloat(vNewQty).toFixed(5)); 
			customkittranrecord.setFieldValue('custrecord_orderlinedetails_putconf_qty', parseFloat(vNewQty).toFixed(5)); 
			//customkittranrecord.setFieldValue('custrecord_orderlinedetails_status_flag', 3); //3 stands for s inbound process 
			//customkittranrecord.setFieldValue('custrecord_orderlinedetails_item', vMainItem);
			customkittranrecord.setFieldValue('custrecord_orderlinedetails_ord_category',4); // 4 stands for workorder	


			nlapiSubmitRecord(customkittranrecord);
			nlapiLogExecution('ERROR','Updated in Transaction ord line');
		}	
	}

}
/**
 * 
 * @param itemID
 * @returns
 */
function getItemDimensions(itemID)
{
	//nlapiLogExecution('ERROR', 'In Item Dimensions', "here1");
	var retItemDimArray = null;

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemID));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomskudim');
	columns[3] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');
	columns[0].setSort(true);

	retItemDimArray = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, columns);
	if(retItemDimArray != null && retItemDimArray != '')
		nlapiLogExecution('ERROR', 'retItemDimArray', retItemDimArray.length);
	return retItemDimArray;
}
/**
 * 
 * @param orderQty
 * @param palletQty
 * @returns {Number}
 */
function getSKUDimCount(orderQty, dimQty)
{
	var retSKUDimCount = 0;

	if(dimQty > 0 && (parseFloat(orderQty) > parseFloat(dimQty)))
	{
		skuDimCount = parseFloat(orderQty)/parseFloat(dimQty);
		retSKUDimCount = Math.floor(skuDimCount);
		if(retSKUDimCount == 0)
			retSKUDimCount = 1;
	}

	return retSKUDimCount;
}



var searchInvArr=new Array();
function GetAllInvtDetails(InvRefId,maxid)
{
	nlapiLogExecution('ERROR', 'getInventoryRecordsForWavecancel', 'Start');
	//var ebizordno=request.getParameter('custpage_orderlist');

	nlapiLogExecution('ERROR', 'InvRefId', InvRefId);
	nlapiLogExecution('ERROR', 'InvRefId.length', InvRefId.length);
	// Filter open task records by wave number and wms_status_flag

	if(InvRefId != null && InvRefId != '' && InvRefId.length>0)
	{	
		//var ebizordno=request.getParameter('custpage_orderlist');
		var filters = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'is', InvRefId));
		if(maxid!=0)
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_packcode'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));
		columns.push(new nlobjSearchColumn('custrecord_inv_ebizsku_no'));
		columns.push(new nlobjSearchColumn('custrecord_invt_ebizlp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_expdate'));
		columns.push(new nlobjSearchColumn('custrecord_wms_inv_status_flag'));
		columns.push(new nlobjSearchColumn('custrecord_invttasktype'));
		columns.push(new nlobjSearchColumn('custrecord_outboundinvlocgroupid'));
		columns.push(new nlobjSearchColumn('custrecord_pickseqno'));
		columns.push(new nlobjSearchColumn('custrecord_inboundinvlocgroupid'));
		columns.push(new nlobjSearchColumn('custrecord_putseqno'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_transaction_no'));
		//columns.push(new nlobjSearchColumn('custrecord_ebiz_serialnumbers'));
		columns.push(new nlobjSearchColumn('name'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_invholdflg'));		
		// Retrieve all open task records for the selected wave
		var InvRecs = new nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
		if(InvRecs!=null && InvRecs.length>=1000)
		{ 
			for(var p=0;p<InvRecs.length;p++)
				searchInvArr.push(InvRecs[p]); 
			var maxno=InvRecs[InvRecs.length-1].getValue(columns[0]);
			GetAllInvtDetails(InvRefId,maxno);	
		}
		else
		{
			for(var p=0;p<InvRecs.length;p++)
				searchInvArr.push(InvRecs[p]); 
		}
	}
	logCountMessage('ERROR', searchInvArr);
	nlapiLogExecution('ERROR', 'InvRecords', searchInvArr.length);
	nlapiLogExecution('ERROR', 'getInventoryRecordsForWavecancel', 'End');

	return searchInvArr;
}
function GetInvDetails(invtRefNo,InvtDetails)
{
	for(var p=0;p<InvtDetails.length;p++)
	{
		if(InvtDetails[p].getId()==invtRefNo)
			return InvtDetails[p];
	}	
	return InvtDetails;
}
function updateInventoryForTaskParent(vInvRec, expectedQty,emp,newParent)
{
	nlapiLogExecution('ERROR', 'updateInventoryForWaveCancel', 'Start');
	nlapiLogExecution('ERROR', 'invtRefNo', vInvRec);
	nlapiLogExecution('ERROR', 'expectedQty', expectedQty);
	var inventoryRecord = null;
	try
	{


		var allocationQty = vInvRec.getValue('custrecord_ebiz_alloc_qty');
		var QOHqty = vInvRec.getValue('custrecord_ebiz_qoh'); 
		nlapiLogExecution('ERROR', 'Inventory Allocation Qty[PREV]', allocationQty); 
		nlapiLogExecution('ERROR', 'Inventory QOH Qty[PREV]', QOHqty); 
		// Updating allocation qty
		if(parseFloat(allocationQty)>parseFloat(expectedQty)){
			allocationQty = parseFloat(allocationQty) - parseFloat(expectedQty);
		} 
		else {
			allocationQty = 0;
		}

		if(parseFloat(QOHqty)>parseFloat(expectedQty)){
			QOHqty = parseFloat(QOHqty) - parseFloat(expectedQty);
		} 
		else {
			QOHqty = 0;
		}

		if(parseFloat(allocationQty) <= 0){
			allocationQty=0;
		}
		nlapiLogExecution('ERROR', 'Inventory of item', vInvRec.getValue('custrecord_ebiz_inv_sku'));

		newParent.selectNewLineItem('recmachcustrecord_ebiz_inv_parent');
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'id', vInvRec.getId());
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'name', vInvRec.getValue('name'));
		if(vInvRec.getValue('custrecord_ebiz_inv_binloc') != null && vInvRec.getValue('custrecord_ebiz_inv_binloc') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_binloc', vInvRec.getValue('custrecord_ebiz_inv_binloc'));

		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_invholdflg', vInvRec.getValue('custrecord_ebiz_invholdflg'));		
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_lp', vInvRec.getValue('custrecord_ebiz_inv_lp'));
		if(vInvRec.getValue('custrecord_ebiz_inv_sku') != null && vInvRec.getValue('custrecord_ebiz_inv_sku') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_sku', vInvRec.getValue('custrecord_ebiz_inv_sku'));
		if(vInvRec.getValue('custrecord_ebiz_inv_sku_status') != null && vInvRec.getValue('custrecord_ebiz_inv_sku_status') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_sku_status', vInvRec.getValue('custrecord_ebiz_inv_sku_status'));
		if(vInvRec.getValue('custrecord_ebiz_inv_packcode') != null && vInvRec.getValue('custrecord_ebiz_inv_packcode') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_packcode', vInvRec.getValue('custrecord_ebiz_inv_packcode'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_qty', parseFloat(vInvRec.getValue('custrecord_ebiz_inv_qty')).toFixed(5));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_lot', vInvRec.getValue('custrecord_ebiz_inv_lot'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_fifo', vInvRec.getValue('custrecord_ebiz_inv_fifo'));
		if(vInvRec.getValue('custrecord_ebiz_inv_loc') != null && vInvRec.getValue('custrecord_ebiz_inv_loc') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_loc', vInvRec.getValue('custrecord_ebiz_inv_loc'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_alloc_qty', parseFloat(allocationQty).toFixed(5));
		if(vInvRec.getValue('custrecord_inv_ebizsku_no') != null && vInvRec.getValue('custrecord_inv_ebizsku_no') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_inv_ebizsku_no', vInvRec.getValue('custrecord_inv_ebizsku_no'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_invt_ebizlp', vInvRec.getValue('custrecord_invt_ebizlp'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_qoh', parseFloat(QOHqty).toFixed(5));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_expdate', vInvRec.getValue('custrecord_ebiz_expdate'));
		if(vInvRec.getValue('custrecord_wms_inv_status_flag') != null && vInvRec.getValue('custrecord_wms_inv_status_flag') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_wms_inv_status_flag', vInvRec.getValue('custrecord_wms_inv_status_flag'));
		if(vInvRec.getValue('custrecord_invttasktype') != null && vInvRec.getValue('custrecord_invttasktype') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_invttasktype', vInvRec.getValue('custrecord_invttasktype'));
		if(vInvRec.getValue('custrecord_outboundinvlocgroupid') != null && vInvRec.getValue('custrecord_outboundinvlocgroupid') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_outboundinvlocgroupid', vInvRec.getValue('custrecord_outboundinvlocgroupid'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_pickseqno', vInvRec.getValue('custrecord_pickseqno'));
		if(vInvRec.getValue('custrecord_inboundinvlocgroupid') != null && vInvRec.getValue('custrecord_inboundinvlocgroupid') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_inboundinvlocgroupid', vInvRec.getValue('custrecord_inboundinvlocgroupid'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_putseqno', vInvRec.getValue('custrecord_putseqno'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_callinv', 'N');
		if(vInvRec.getValue('custrecord_ebiz_avl_qty') != null && vInvRec.getValue('custrecord_ebiz_avl_qty') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_avl_qty', parseFloat(vInvRec.getValue('custrecord_ebiz_avl_qty')).toFixed(5));
		if(vInvRec.getValue('custrecord_ebiz_transaction_no') != null && vInvRec.getValue('custrecord_ebiz_transaction_no') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_transaction_no', vInvRec.getValue('custrecord_ebiz_transaction_no'));
		//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_serialnumbers', vInvRec.getValue('custrecord_ebiz_serialnumbers'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_putseqno', vInvRec.getValue('custrecord_putseqno'));
		if(emp != null && emp != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_updated_user_no', emp);
		else
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_updated_user_no', nlapiGetContext().getUser());

		newParent.commitLineItem('recmachcustrecord_ebiz_inv_parent');
		nlapiLogExecution('ERROR', 'updateInventoryForWaveCancel', 'End');

	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in Wave Cancelation Inv updation', exp);	
		//showInlineMessage(form, 'Error', 'Wave Cancelation Failed', "");
		//response.writePage(form);
	}
}

/**
 * 
 * @param Item
 * @param Location
 * @param sublist dropdown
 *To bind Lotnumbers to sublist Dropdown
 */


/*function addSerialno(temparray, Location,form,serialno)
{
	nlapiLogExecution('ERROR', 'temparray',temparray.length);


	var filterLotno = new Array();
	filterLotno[0] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', temparray);
	filterLotno[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', ['@NONE@',Location]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[2] = new nlobjSearchColumn('custrecord_ebizsku');
	columns[2].setSort(true);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterLotno,columns);
	if(searchresults!=null)
	{
		nlapiLogExecution('ERROR', 'searchresults_batch', searchresults.length);
		for(var i=0;i<searchresults.length;i++)
		{ var InternalId=searchresults[i].getValue('internalid');

		serialno.addSelectOption(searchresults[i].getValue('internalid'), (searchresults[i].getValue('custrecord_ebizlotbatch')+"_"+searchresults[i].getText('custrecord_ebizsku')));

		//nlapiLogExecution('ERROR', 'Lotnoresult', searchresults.length);
		}
	}
}*/

/*code merged from monobind on feb 27th 2013 by Radhika added kititemstatus filter condition */
function addSerialno(temparray,Location,form,serialno,kitItemStatus){

	nlapiLogExecution('ERROR', 'temparray',temparray.length);
	nlapiLogExecution('ERROR', 'kitItemStatus',kitItemStatus);
	nlapiLogExecution('ERROR', 'Location',Location);

	var filterStatus = new Array();

	filterStatus.push(new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T'));
	filterStatus.push(new nlobjSearchFilter('custrecord_activeskustatus', null, 'is','T'));
	if(Location != null && Location != '')
		filterStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof',Location));
	var searchresultsStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filterStatus, null);
	var vStatusArr=new Array();
	if(searchresultsStatus != null && searchresultsStatus != '')
	{
		for(var k=0;k<searchresultsStatus.length;k++)
		{
			vStatusArr.push(searchresultsStatus[k].getId());
		}
	}
	nlapiLogExecution('ERROR', 'vStatusArr',vStatusArr);

	var filter= new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof', temparray));
	//if(kitItemStatus!=null && kitItemStatus!='')
	//	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof', kitItemStatus));
	if(vStatusArr != null && vStatusArr != '' && vStatusArr.length>0)
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof', vStatusArr));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'anyof', Location));
	//filter.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));
	//case#:20123854 Start
	filter.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
	//end
	filter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'noneof','@NONE@'));

	var columns = new Array();	
	//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku',null,'group'));			 
	columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot',null,'group'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty',null,'sum'));

	columns[0].setSort(true);
	columns[1].setSort();
	columns[2].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filter, columns);

	if(searchresults!=null)
	{
		nlapiLogExecution('ERROR', 'Lotnoresult', searchresults.length);

		for(var i=0;i<searchresults.length;i++)
		{
			serialno.addSelectOption(searchresults[i].getValue('custrecord_ebiz_inv_lot',null,'group'), (searchresults[i].getText('custrecord_ebiz_inv_lot',null,'group')+"(Qty Rem: "+searchresults[i].getValue('custrecord_ebiz_avl_qty',null,'sum') + ")" +"_"+searchresults[i].getText('custrecord_ebiz_inv_sku',null,'group')));


		}
	}

}



function CheckAvailability(woidno,lineno,actualbinloc,actuallotno,oldBinloc,oldLOTno,varLineqty)
{
	nlapiLogExecution('ERROR', 'Into Check Availablity '); 
	var vMessage= fnGetDetails(woidno,lineno,actualbinloc,actuallotno,oldBinloc,oldLOTno,varLineqty);

	if(vMessage== 'SUCCESS') 
	{ 
		return true;
	} 
	else
	{
		if(vMessage!= null && vMessage != "")
		{
			var vArrMsg=vMessage.split('~');
			if(vArrMsg.length>1)
			{
				nlapiLogExecution('ERROR','Allocation Failed ',vArrMsg[1]);
				var ErrorMsg = nlapiCreateError('CannotAllocate',vArrMsg[1], true);
				throw ErrorMsg; //throw this error object, do not catch it
			}
			else
			{
				nlapiLogExecution('ERROR','eBiz Allocation Failed ');
				var ErrorMsg = nlapiCreateError('CannotAllocate','Allocation Failed', true);
				throw ErrorMsg; //throw this error object, do not catch it
			}	
		}
		return false;
	}

}

var SearchresultsArray = new Array();
var GlobalArray = new Array();
function fnGetDetails(vId,lineno,actualbinloc,actuallotno,oldBinloc,oldLOTno,varLineqty)
{
	nlapiLogExecution('ERROR','Into Get Details');
	nlapiLogExecution('ERROR', 'vrecid', vrecid);
	//var vId = request.getParameter('custpage_workorder');
	var tempcount=0;
	var searchresults = nlapiLoadRecord('workorder', vId);
	var itemcount= searchresults.getLineItemCount('item');
	var vWOId=searchresults.getFieldValue('tranid');
	var SkuNo=searchresults.getFieldValue('assemblyitem');

	var varAssemblyQty=searchresults.getFieldValue('quantity');
	var vLocation= searchresults.getFieldValue('location');
	var vWMSlocation=searchresults.getFieldValue('location');
	var qty=0;
	if(qty1!= null && qty1!="" && parseFloat(qty1)>0)
		qty=qty1;

	nlapiLogExecution('ERROR', 'vId', vId);
	nlapiLogExecution('ERROR', 'vWOId', vWOId);
	/*var searchresults = nlapiLoadRecord('workorder', vId);
	if(searchresults !=null && searchresults != "" && searchresults.length>0)
		vWOId=searchresults.getFieldValue('tranid');
	nlapiLogExecution('ERROR', 'vWOId', vWOId);*/

	nlapiLogExecution('ERROR', 'ItemCount', itemcount);
	//nlapiLogExecution('ERROR', 'assemItem', assemItem);



	var vInvReturn=new Array();
	//alert("3");
	var strItem="",strQty=0,vcomponent;

	/*	var ItemType = nlapiLookupField('item', vitem, 'recordType');
	nlapiLogExecution('ERROR','ItemType',ItemType);
	var searchresultsitem;
	if(ItemType!=null && ItemType !='')

	{
		nlapiLogExecution('ERROR','Item Type',ItemType);
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);

		searchresultsitem = nlapiLoadRecord(ItemType, vitem); //1020
	}

	else
	{
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
	}
	var SkuNo=searchresultsitem.getFieldValue('itemid');*/
	var recCount=0; 
	//Written by Ganesh
	//alert("ItemCount : "+ itemcount);
	var vAlert='';
	nlapiLogExecution('ERROR','itemcount ',itemcount);

	var vItemArr=new Array();
	for(var s=1; s<=itemcount;s++)
	{
		var vCompItem = searchresults.getLineItemValue('item', 'item', s);
		if(vItemArr.indexOf(vCompItem) == -1)
			vItemArr.push(vCompItem);
	}
	if(vItemArr.length>0)
	{
		var filters2 = new Array();

		var columns2 = new Array();
		columns2[0] = new nlobjSearchColumn('custitem_item_family');
		columns2[1] = new nlobjSearchColumn('custitem_item_group');
		columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
		columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
		columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
		columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
		columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

		filters2.push(new nlobjSearchFilter('internalid', null, 'is', vItemArr));

		var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

		var filters3=new Array();
		filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		var columns3=new Array();

		columns3[0] = new nlobjSearchColumn('name');
		columns3[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
		columns3[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
		columns3[3]=new nlobjSearchColumn('formulanumeric');
		columns3[3].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();
		var pickrulesarray = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters3, columns3);
	}

	for(var m=1; m<=itemcount;m++) 
	{
		nlapiLogExecution('ERROR','lineno ',lineno);
		nlapiLogExecution('ERROR','m ',m);
		if(m==lineno)
		{
			var serialNo="";
			serialNo=actuallotno;
			var batchname;//Code added by Santosh on 13Aug12
			//alert("SkuNo" + SkuNo);
			strItem = searchresults.getLineItemValue('item', 'item', m);
			//alert("strItem" + strItem);
			strQty = searchresults.getLineItemValue('item', 'quantity', m);
			if(varLineqty !=null && varLineqty !='')
				strQty=varLineqty;
			//Code added by Santosh on 13Aug12
			//batchname = searchresults.getLineItemValue('item', 'serialnumbers', m);
			nlapiLogExecution('ERROR','batchname ',batchname);
			nlapiLogExecution('ERROR','strQty ',strQty);
			//alert("strQty" + strQty);
			//alert("varAssemblyQty" + varAssemblyQty);
			nlapiLogExecution('ERROR','Hi1 ');
			nlapiLogExecution('ERROR','strItem ',strItem);
			var fields = ['custitem_item_family', 'custitem_item_group','recordType','custitem_ebizbatchlot'];
			var ComponentItemType = nlapiLookupField('item', strItem, 'recordType');
			var vItemType="";
			var batchflag="F";
			if (ComponentItemType != null && ComponentItemType !='')
			{
				nlapiLogExecution('ERROR','Hi2 ');
				var columns = nlapiLookupField(ComponentItemType, strItem, fields);


				var itemfamily = columns.custitem_item_family;
				var itemgroup = columns.custitem_item_group;
				vItemType = columns.recordType;
				batchflag= columns.custitem_ebizbatchlot;
			}
			//avlqty= (parseFloat(strQty)*parseFloat(varAssemblyQty));
			//alert("avlqty " + avlqty);
			//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, avlqty);
			nlapiLogExecution('ERROR','Hi3 ');

			//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, strQty,vLocation,serialNo);

			var arryinvt=PickStrategieKittoStockNew(strItem,strQty,vLocation,ItemInfoResults,pickrulesarray,serialNo,actualbinloc);
			var vitemqty=0;
			var vrecid="" ;
			var vlp="",vlocation="",vqty="",vlotnoWithQty="",vremainqty="",location,vlocationText="";
			vitemText=searchresults.getLineItemText('item', 'item', m);
			if(arryinvt.length>0)
			{ 	 

				var vLot="";
				var vPrevLot="";
				var vLotQty=0;
				var vTempLot="";
				var vBoolFound=false;
				//var Newsearchresults = nlapiLoadRecord('workorder', vId);
				//alert("vitemText" + vitemText);
				for (j=0; j < arryinvt.length; j++) 
				{			
					var invtarray= arryinvt[j];  	

					//vitem =searchresults[i].getValue('memberitem');		
					//vitemText =searchresults[i].getText('memberitem');


					if(vlp == "" || vlp == null)
						vlp = invtarray[2];
					else
						vlp = vlp+","+ invtarray[2];
					if(vTempLot == "" || vTempLot == null)
						vTempLot = invtarray[4];
					else
						vTempLot = vTempLot+","+ invtarray[4];
					if(vPrevLot==null || vPrevLot== "")
					{
						vPrevLot=invtarray[4];
						vLotQty=parseFloat(invtarray[0]);
						vLot=invtarray[4];
						vBoolFound=true;
					}
					else if(vPrevLot==invtarray[4])
					{
						vLotQty=vLotQty+ parseFloat(invtarray[0]);						
					}
					else if(vPrevLot!=invtarray[4])
					{
						if(vlotnoWithQty == "" || vlotnoWithQty == null)
							vlotnoWithQty = vPrevLot+"(" + vLotQty +")";//invtarray[7] is lotno id
						else
							vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + vPrevLot +"(" + vLotQty +")";

						if(vLot == "" || vLot == null)
							vLot = invtarray[4];
						else
							vLot = vLot+","+ invtarray[4];

						vPrevLot=invtarray[4];
						vLotQty=parseFloat(invtarray[0]);

					}
					if(vqty == "" || vqty == null)
						vqty = invtarray[0];
					else
						vqty = vqty+","+ invtarray[0];

					var qty1 = invtarray[0];

					if(vlocationText == "" || vlocationText == null)
						vlocationText = invtarray[6];
					else
						vlocationText = vlocationText+","+ invtarray[6];

					if(vlocation == "" || vlocation == null)
						vlocation = invtarray[1];
					else
						vlocation = vlocation+","+ invtarray[1];


					nlapiLogExecution('ERROR','invtarray[3] ',invtarray[3]);

					if(vrecid == "" || vrecid == null)
						vrecid = invtarray[3];
					else
						vrecid = vrecid+","+ invtarray[3];




					if(vremainqty == "" || vremainqty == null)
						vremainqty = invtarray[5];
					else
						vremainqty = vremainqty+","+ invtarray[5];
					location = invtarray[1];
					vitemqty=parseFloat(vitemqty)+parseFloat(qty1);
					//vrecid=invtarray[3] ;vlp=invtarray[2]; vqty=invtarray[0]; vTempLot=invtarray[4]; 
					//vLocation=invtarray[1];
					//var CurrentRow=[vId,vrecid,vWOId,strItem,vlp,vqty,vTempLot,vlocation,vWMSlocation,vComments,m];


					nlapiLogExecution('ERROR', 'else_muliple ','sucess');
					var CurrentRow=[vId,invtarray[3],vWOId,strItem,invtarray[2],invtarray[0],invtarray[4],invtarray[1],vWMSlocation,m,lineno,oldBinloc,oldLOTno];
					SearchresultsArray.push(CurrentRow);
					nlapiLogExecution('ERROR', 'CurrentRow ',CurrentRow);	



				}
				if(vBoolFound==true)
				{
					/*if(vlotnoWithQty == "" || vlotnoWithQty == null)
					vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";//invtarray[7] is lotno id
				else
					vlotnoWithQty = vlotnoWithQty+","+ invtarray[4]+"(" + vLotQty +")";*/

					if(vlotnoWithQty == "" || vlotnoWithQty == null)
						vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";
					else
						vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + invtarray[4]+"(" + vLotQty +")";


					//alert("vTempLot" + vTempLot);
					//alert("vLot.split(',').length" + vLot.split(',').length);
					// alert("vLotQty " + vlotnoWithQty);				
					nlapiLogExecution('ERROR', 'vLot ', vLot);
					nlapiLogExecution('ERROR', 'vlotnoWithQty ', vlotnoWithQty);
					if(vLot.split(',').length>1)
						vLot=vlotnoWithQty;
				}

				//if(avlqty > vitemqty )
				if(strQty > vitemqty )			
				{ 
					vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";

				}
				else 
				{
					//var CurrentRow=[vId,vrecid,vWOId,strItem,vlp,vqty,vTempLot,vlocation,vWMSlocation,vComments,m];
					//santosh
					var Row=[vId,vrecid,vWOId,strItem,vlp,vqty,vTempLot,vlocation,vWMSlocation,m,vLot,location,vlocationText,lineno,vItemType];
					GlobalArray.push(Row);
					nlapiLogExecution('ERROR', 'GlobalArray ',Row);

				}
			}
			else
			{
				vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";
			}
		}
	}
	if(vAlert!=null && vAlert!='')
	{ 
		/*var vErrorRet=new Array();
		vErrorRet.push('FAIL');
		vErrorRet.push(vAlert);
		return vErrorRet;*/
		return 'FAIL~' + vAlert;
	}
	else
	{
		nlapiLogExecution('ERROR', 'Before commit ');
		//var CommittedId = nlapiSubmitRecord(Newsearchresults, true);
		//AllocateInv(vId,SearchresultsArray);
		//nlapiLogExecution('ERROR', 'CommittedId ', CommittedId);
		nlapiLogExecution('ERROR', 'Committed ', 'Success');
		return 'SUCCESS';
	}
}
/**To get confirm LOT toNS based on system rule
 * 
 * @param Site
 * @returns Y or N
 */
function GetConfirmLotToNS(Site)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'WMSLOT');
		filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('ERROR', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}

function AllocateInv(vId,SearchresultsArray)
{
	// Create a Record in Opentask
	var now = new Date();
	//a Date object to be used for a random value
	var now = new Date();
	//now= now.getHours();
	//Getting time in hh:mm tt format.
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}


	if(SearchresultsArray!=null && SearchresultsArray!='' && SearchresultsArray.length>0)
	{
		nlapiLogExecution('ERROR', 'SearchresultsArray',SearchresultsArray.length);


		for (var i=0; i<SearchresultsArray.length; i++){
			var vId=SearchresultsArray[i][0];
			var vLineRec=SearchresultsArray[i][1];
			var vWOId=SearchresultsArray[i][2];
			var varsku=SearchresultsArray[i][3];
			var vLP=SearchresultsArray[i][4];
			var vLineQty=SearchresultsArray[i][5];
			var vBatch=SearchresultsArray[i][6];
			var vactLocationtext=SearchresultsArray[i][7];
			var vLocation=SearchresultsArray[i][8];
			//var vComments=SearchresultsArray[i][9];
			var vlineno=SearchresultsArray[i][9];

			nlapiLogExecution('ERROR', 'vId',vId);
			nlapiLogExecution('ERROR', 'vLineRec',vLineRec);
			nlapiLogExecution('ERROR', 'varsku',varsku);
			nlapiLogExecution('ERROR', 'vBatch',vBatch);
			nlapiLogExecution('ERROR', 'vactLocationtext',vactLocationtext);

			nlapiLogExecution('ERROR', 'vLineQty',vLineQty);
			nlapiLogExecution('ERROR', 'vlineno',vlineno);


			var scount=1;
			LABL1: for(var z=0;z<scount;z++)
			{
				try
				{
					var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vLineRec);
					var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
					var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
					var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
					var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
					var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

					nlapiLogExecution('ERROR', 'qty', qty);
					nlapiLogExecution('ERROR', 'allocqty', allocqty);
					nlapiLogExecution('ERROR', 'varqty', vLineQty);
					transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)+ parseFloat(vLineQty)).toFixed(5));
					nlapiSubmitRecord(transaction, false, true);
				}
				catch(ex)
				{
					nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					}  

					nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}

			var rcptrecordid3 = nlapiLoadRecord('workorder', vId);
			var woidWO= rcptrecordid3.getFieldValue('tranid');
			nlapiLogExecution('ERROR','woidWO  ',woidWO);
			vWOId=woidWO;

			var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
			customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP 
			//customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
			customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
			customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));

			//customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
			//customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
			//customrecord.setFieldValue('custrecord_expe_qty', varqty); 
			nlapiLogExecution('ERROR','expe qty ',parseFloat(vLineQty).toFixed(5));
			nlapiLogExecution('ERROR','binlocation',vactLocationtext);

			customrecord.setFieldValue('custrecord_expe_qty', parseFloat(vLineQty).toFixed(5));  //Commented and added to resovle decimal issue
			customrecord.setFieldValue('custrecord_act_qty', parseFloat(vLineQty).toFixed(5));
			customrecord.setFieldValue('custrecord_wms_status_flag', 9);	// 14 stands for outbound process


			customrecord.setFieldValue('custrecord_actbeginloc',vactLocationtext);	
			//customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
			//customrecord.setFieldValue('custrecord_lpno', vlp); 
			customrecord.setFieldValue('custrecord_upd_date', now);
			//customrecord.setFieldValue('custrecord_sku', vskuText);
			customrecord.setFieldValue('custrecord_ebiz_sku_no', varsku);
			customrecord.setFieldValue('name', vWOId);		
			var currentContext = nlapiGetContext();  
			var currentUserID = currentContext.getUser();
			customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
			customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
			//customrecord.setFieldValue('custrecord_ebiz_wave_no', eBizWaveNo);
			customrecord.setFieldValue('custrecord_ebiz_order_no', vId);
			customrecord.setFieldValue('custrecord_batch_no', vBatch);
			customrecord.setFieldValue('custrecord_ebiz_cntrl_no', vId);
			//customrecord.setFieldValue('custrecord_ebiz_cntrl_no', rcptrecordid);

			customrecord.setFieldValue('custrecord_sku', varsku);
			customrecord.setFieldValue('custrecord_line_no', parseFloat(vlineno));
			customrecord.setFieldValue('custrecord_lpno', vLP);
			customrecord.setFieldValue('custrecord_packcode', vPackcode);					
			customrecord.setFieldValue('custrecord_sku_status', vSKUStatus);
			customrecord.setFieldValue('custrecord_wms_location', vLocation);
			customrecord.setFieldValue('custrecord_invref_no', vLineRec);
			//customrecord.setFieldValue('custrecord_notes', vComments);			


			//nlapiLogExecution('ERROR','eBizWaveNo ',eBizWaveNo);


			nlapiSubmitRecord(customrecord);			

			nlapiLogExecution('ERROR', 'Success');


		}

	}

}


function PickStrategieKittoStockNew(item,avlqty,location,ItemInfoArr,pickrulesarray,serialNo,actualbinloc){
	//alert("5");
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','avlqty',avlqty);
	nlapiLogExecution('ERROR','ItemInfoArr',ItemInfoArr);
	nlapiLogExecution('ERROR','location',location);
	nlapiLogExecution('ERROR','pickrulesarray',pickrulesarray);
	nlapiLogExecution('ERROR','serialNo',serialNo);
	var ItemFamily;
	var ItemGroup;

	if(ItemInfoArr!= null && ItemInfoArr != '' )
	{
		nlapiLogExecution('ERROR', 'ItemInfoArr.length', ItemInfoArr.length);
		for(var p=0;p<ItemInfoArr.length;p++)
		{
			if(ItemInfoArr[p].getId()==item)
			{
				ItemFamily = ItemInfoArr[p].getValue('custitem_item_family');
				ItemGroup = ItemInfoArr[p].getValue('custitem_item_group');

			}	
		}	
	}

	nlapiLogExecution('ERROR', 'Item', item);
	nlapiLogExecution('ERROR', 'ItemFamily', ItemFamily);
	nlapiLogExecution('ERROR', 'ItemGroup', ItemGroup);

	var vPickRules=new Array();
	if(pickrulesarray != null && pickrulesarray != '' && pickrulesarray.length>0)
	{
		nlapiLogExecution('ERROR', 'pickrulesarray.length', pickrulesarray.length);
		for(var j = 0; j < pickrulesarray.length; j++)
		{
			nlapiLogExecution('ERROR', 'pickrulesarray[j]', pickrulesarray[j]);
			if((pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul') ==ItemFamily))	
			{
				if((pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul') ==ItemGroup))	
				{
					if((pickrulesarray[j].getValue('custrecord_ebizskupickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskupickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskupickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskupickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskupickrul') ==item))	
					{
						vPickRules.push(j);
						nlapiLogExecution('ERROR', 'j', j);

					}	
				}	
			}	
		}	
	}
	var vPickZoneArr=new Array();
	if (vPickRules != null) {
		nlapiLogExecution('ERROR', 'rules count', vPickRules.length);
		nlapiLogExecution('ERROR', 'vPutRules', vPickRules);
		for (var s = 0; s < vPickRules.length; s++) {
			nlapiLogExecution('ERROR', 'vPutRules[s]', vPickRules[s]);
			nlapiLogExecution('ERROR', 'Id', pickrulesarray[vPickRules[s]].getId());
			var vpickzone=pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickzonerul');
			vPickZoneArr.push(vpickzone);
		}	
	}
	var vLocGrpArr=new Array();
	if(vPickZoneArr != null && vPickZoneArr != '')
	{
		nlapiLogExecution('ERROR', "vPickZoneArr" + vPickZoneArr);
		nlapiLogExecution('ERROR', "vPickZoneArr.length" + vPickZoneArr.length);
		vLocGrpArr=GetAllLocGroups(vPickZoneArr,location,-1);

	}
	var searchresultsinvt;
	if(vLocGrpArr != null && vLocGrpArr != '' && vLocGrpArr.length>0)
	{
		nlapiLogExecution('ERROR', "vLocGrpArr" + vLocGrpArr);
		nlapiLogExecution('ERROR', "vLocGrpArr.length" + vLocGrpArr.length);
		searchresultsinvt=GetAllInventory(item,vLocGrpArr,location,serialNo,actualbinloc);

	}


	for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

		var searchresult = searchresultsinvt[l];
		var actqty = searchresult.getValue('custrecord_ebiz_qoh');
		var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
		var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
		var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
		var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
		var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
		var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
		var Recid = searchresult.getId(); 


		//alert(" 664 actqty:" +actqty);
		//alert(" 664 allocqty:" +allocqty);
		//alert(" 664 LP:" +LP);
		//alert(" 664 vactLocation:" +vactLocation);
		//alert(" 664 vactLocationtext:" +vactLocationtext);
		//alert(" 664 vlotno:" +vlotno);
		//alert(" 664 vlotText:" +vlotText);
		//alert(" 664 Recid:" +Recid);


		nlapiLogExecution('ERROR', 'Recid1', Recid);
		if (isNaN(allocqty)) {
			allocqty = 0;
		}
		if (allocqty == "") {
			allocqty = 0;
		}
		if( parseFloat(allocqty)<0)
		{
			allocqty=0;
		}
		if( parseFloat(actqty)<0)
		{
			actqty=0;
		}
		nlapiLogExecution('ERROR', "vactLocation: " + vactLocation);
		nlapiLogExecution('ERROR', "vactLocationtext: " + vactLocationtext);
		nlapiLogExecution('ERROR', "allocqty: " + allocqty);
		nlapiLogExecution('ERROR', "LP: " + LP);
		var remainqty=0;

		//Added by Ganesh not to allow negative or zero Qtys 
		if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0)
			remainqty = parseFloat(actqty) - parseFloat(allocqty);


		var cnfmqty;

////		alert("remainqty"+remainqty );
////		alert("avlqty"+avlqty );
		if (parseFloat(remainqty) > 0) {

			////alert("parseFloat(avlqty) "+parseFloat(avlqty));

			if ((parseFloat(avlqty) - parseFloat(actallocqty)) <= parseFloat(remainqty)) {
				cnfmqty = parseFloat(avlqty) - parseFloat(actallocqty);
				actallocqty = avlqty;
				////alert('cnfmqty in if '+cnfmqty);
			}
			else {
				cnfmqty = remainqty;
				////alert('cnfmqty2 ' +cnfmqty);
				actallocqty = parseFloat(actallocqty) + parseFloat(remainqty);
			}

			//alert(" 708 cnfmqty:" +cnfmqty);
			//alert(" 709 actallocqty:" +actallocqty);

			////alert('BeforeIf ' +cnfmqty);
			if (parseFloat(cnfmqty) > 0) {
				////alert('AfterIf ' +cnfmqty);
				//Resultarray
				var invtarray = new Array();
				////alert('cnfmqty3 ' +cnfmqty);
				invtarray[0]= cnfmqty;
				////alert('cnfmqty4 ' +cnfmqty);
				invtarray[1]= vactLocation;
				invtarray[2] = LP;
				invtarray[3] = Recid;
				invtarray[4] = vlotText;
				invtarray[5] =remainqty;
				invtarray[6] =vactLocationtext;
				invtarray[7] =vlotno;
				//alert("cnfmqty "+cnfmqty);
				//alert("cnfmqty " + cnfmqty);
				//alert(vlotno);
				//alert(vlotText);
				nlapiLogExecution('ERROR', 'RecId:',Recid);
				//invtarray[3] = vpackcode;
				//invtarray[4] = vskustatus;
				//invtarray[5] = vuomid;
				Resultarray.push(invtarray);								
			}


		}

		if ((avlqty - actallocqty) == 0) {

			return Resultarray;

		}

	} 
	return Resultarray;  
}

var vAllGrps=new Array();
function GetAllLocGroups(vpickzone,location,maxno){

	nlapiLogExecution('ERROR','location',location);
	nlapiLogExecution('ERROR','maxno',maxno);
	if(vpickzone!= null && vpickzone !='')
		vpickzone.push('@NONE@');
	nlapiLogExecution('ERROR','vpickzone',vpickzone);
	var filterszone = new Array();
	filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', vpickzone));

	filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',location]));
	if(maxno!=-1)
	{
		filterszone.push(new nlobjSearchFilter('internalid', null, 'lessthan', parseFloat(maxno)));
	}

	var columnzone = new Array();
	columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');
	columnzone[1]=new nlobjSearchColumn('internalid');
	columnzone[1].setSort(true);

	var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
	nlapiLogExecution('ERROR','Loc Group Fetching');

	for(var k=0;k<searchresultszone.length;k++)
	{
		vAllGrps.push(searchresultszone[k].getValue('custrecord_locgroup_no'));
	}
	if(searchresultszone!=null && searchresultszone.length>=1000)
	{ 
		var maxno=searchresultszone[searchresultszone.length-1].getValue('internalid');		
		GetAllLocGroups(vpickzone,location,maxno); 
	}
	return vAllGrps;
}


var vAllInv=new Array();
function GetAllInventory(item,vlocgroupno,location,serialNo,actualbinloc){

	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));
	filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'anyof', vlocgroupno));

	if (location!= "" && location!= null) {
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [location]));
	}

	//code added by santosh on 13aug12
	if (serialNo!= "" && serialNo!= null) {
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', serialNo));
	}

	if (actualbinloc!= "" && actualbinloc!= null) {
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', actualbinloc));
	}

	//end of the code on 13Aug12

	nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columnsinvt[1]=new nlobjSearchColumn('internalid');
	columnsinvt[0].setSort();
	columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columnsinvt[5] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
	columnsinvt[7] = new nlobjSearchColumn('custrecord_pickseqno'); 
	columnsinvt[8] = new nlobjSearchColumn('custrecord_ebiz_qoh');			
	columnsinvt[9] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columnsinvt[6].setSort();
	columnsinvt[7].setSort();
	columnsinvt[8].setSort(false);

	var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);


	nlapiLogExecution('ERROR','Inventory Fetching');

	return searchresultsinvt;
}



function DeleteOpentask(woid,SearchresultsArray)
{
	nlapiLogExecution('ERROR', 'DeleteOpentask : ', 'Start');

	var OTFilter = new Array();
	OTFilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid));

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('custrecord_line_no');
	columns[1]=new nlobjSearchColumn('custrecord_batch_no');
	columns[2]=new nlobjSearchColumn('custrecord_actbeginloc');
	columns[3]=new nlobjSearchColumn('custrecord_sku');
	columns[4]=new nlobjSearchColumn('custrecord_wms_location');
	columns[5]=new nlobjSearchColumn('custrecord_invref_no');
	columns[6]=new nlobjSearchColumn('custrecord_expe_qty');


	var OTSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OTFilter, columns);

	if(OTSearchResults != null && OTSearchResults != "" && SearchresultsArray != null && SearchresultsArray != "" && SearchresultsArray.length>0)
	{
		nlapiLogExecution('ERROR', 'OTSearchResults.length : ', OTSearchResults.length);
		nlapiLogExecution('ERROR', 'SearchresultsArray : ',SearchresultsArray.length );

		for(var z=0;z<SearchresultsArray.length; z++){

			var vLineRec=SearchresultsArray[z][1]; //recid
			var vstrItem=SearchresultsArray[z][3]; //itemid
			var vLineQty=SearchresultsArray[z][5]; //lineqty
			var vLotno=SearchresultsArray[z][6]; //lot#
			var vBinloc=SearchresultsArray[z][7]; //binlocation
			var vWMSlocation=SearchresultsArray[z][8];//wmslocation

			var vlineno=(SearchresultsArray[z][9])+".0"; //lineno
			var voldBinloc=SearchresultsArray[z][11]; //oldBinloc
			var voldBatch=SearchresultsArray[z][12]; //oldlot#

			var vBatch=GetLotno(vLotno,vstrItem,vWMSlocation);

			nlapiLogExecution('ERROR', 'vlineno', vlineno);
			nlapiLogExecution('ERROR', 'vLineRec', vLineRec);
			nlapiLogExecution('ERROR', 'vLotno', vLotno);
			nlapiLogExecution('ERROR', 'vBatch', vBatch);
			nlapiLogExecution('ERROR', 'voldBatch', voldBatch);
			nlapiLogExecution('ERROR', 'vBinloc', vBinloc);
			nlapiLogExecution('ERROR', 'voldBinloc', voldBinloc);


			for(var p=0;p<OTSearchResults.length;p++)
			{
				nlapiLogExecution('ERROR', 'P : ', p);

				var OpenTaskStrItem=OTSearchResults[p].getValue('custrecord_sku');
				var OpenTaskWmsLoc=OTSearchResults[p].getValue('custrecord_wms_location');

				var OpenTaskLineno=OTSearchResults[p].getValue('custrecord_line_no');
				var OTBatch=OTSearchResults[p].getValue('custrecord_batch_no');
				var OpenTaskBinloc=OTSearchResults[p].getValue('custrecord_actbeginloc');

				var OpenTaskInvtRef=OTSearchResults[p].getValue('custrecord_invref_no');
				var OpenTaskLineQty=OTSearchResults[p].getValue('custrecord_expe_qty');

				nlapiLogExecution('ERROR', 'OpenTaskLineno ', OpenTaskLineno);
				nlapiLogExecution('ERROR', 'OTBatch ', OTBatch);
				nlapiLogExecution('ERROR', 'OpenTaskBinloc', OpenTaskBinloc);
				nlapiLogExecution('ERROR', 'OpenTaskStrItem', OpenTaskStrItem);
				nlapiLogExecution('ERROR', 'OpenTaskWmsLoc', OpenTaskWmsLoc);
				nlapiLogExecution('ERROR', 'OpenTaskInvtRef', OpenTaskInvtRef);
				nlapiLogExecution('ERROR', 'OpenTaskLineQty', OpenTaskLineQty);

				var OpenTaskBatch=GetLotno(OTBatch,OpenTaskStrItem,OpenTaskWmsLoc);
				nlapiLogExecution('ERROR', 'OpenTaskBatch ', OpenTaskBatch);				

				if((parseInt(OpenTaskLineno)==parseInt(vlineno)) && (OpenTaskBatch!=vBatch ||OpenTaskBinloc!=vBinloc)&& (OpenTaskBatch==voldBatch && OpenTaskBinloc==voldBinloc))
				{nlapiLogExecution('ERROR', 'OTSearchResults.length : ', OTSearchResults.length);

				// delocated the qty in  createinventory
				var scount=1;
				LABL1: for(var i=0;i<scount;i++)
				{
					try
					{
						var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', OpenTaskInvtRef);
						var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
						var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
						var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
						var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
						var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

						nlapiLogExecution('ERROR', 'qty', qty);
						nlapiLogExecution('ERROR', 'allocqty', allocqty);
						nlapiLogExecution('ERROR', 'OpenTaskLineQty', OpenTaskLineQty);
						transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(OpenTaskLineQty)).toFixed(5));
						nlapiSubmitRecord(transaction, false, true);
					}
					catch(ex)
					{
						nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

						var exCode='CUSTOM_RECORD_COLLISION'; 
						var wmsE='Inventory record being updated by another user. Please try again...';
						if (ex instanceof nlobjError) 
						{	
							wmsE=ex.getCode() + '\n' + ex.getDetails();
							exCode=ex.getCode();
						}
						else
						{
							wmsE=ex.toString();
							exCode=ex.toString();
						}  

						nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

						if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
						{ 
							scount=scount+1;
							continue LABL1;
						}
						else break LABL1;
					}
				}

				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', OTSearchResults[p].getId());
				nlapiLogExecution('ERROR', 'Deleted Rec s OpenTask : ', id);
				}
			}
		}
	} 
	else
	{
		var ErrorMsg = nlapiCreateError('CannotAllocate','Not deleted', true);
		throw ErrorMsg;
	}

	nlapiLogExecution('ERROR', 'Open task Deletion Success'); 
}



//to update Netsuite in Delocation process
function UpdateNS(woid,GlobalArray,SearchresultsArray)
{
	try {

		if(GlobalArray!=null && GlobalArray!='' && GlobalArray.length>0)
		{
			nlapiLogExecution('ERROR', 'GlobalArray',GlobalArray.length);
			var Newsearchresults = nlapiLoadRecord('workorder', woid);

			for (var i=0; i<GlobalArray.length; i++){


				var vrecid=GlobalArray[i][1];
				var vlp=GlobalArray[i][4];
				var vqty=GlobalArray[i][5];
				var vTempLot=GlobalArray[i][6];
				var vlocation=GlobalArray[i][7];
				var m=GlobalArray[i][9];
				var vLot=GlobalArray[i][10];
				var location=GlobalArray[i][11];				
				var vlocationText=GlobalArray[i][12];
				var vlineno=GlobalArray[i][13];
				var vItemType=GlobalArray[i][14];

				var str = 'vrecid.' + vrecid + '<br>';
				str = str + 'vlp' + vlp + '<br>';
				str = str + 'vqty' + vqty + '<br>';
				str = str + 'vTempLot' + vTempLot + '<br>';
				str = str + 'location' + location + '<br>';
				str = str + 'vlocationText' + vlocationText + '<br>';
				str = str + 'vLot' + vLot + '<br>';
				str = str + 'vlineno' + vlineno + '<br>';
				str = str + 'vItemType' + vItemType + '<br>';

				nlapiLogExecution('DEBUG', 'UpdateNS Parameters', str);
				nlapiLogExecution('ERROR', 'vlp ', vlp);
				nlapiLogExecution('ERROR', 'vqty ', vqty);
				nlapiLogExecution('ERROR', 'vTempLot ', vTempLot);
				nlapiLogExecution('ERROR', 'm ', m);
				nlapiLogExecution('ERROR', 'location ', location);
				nlapiLogExecution('ERROR', 'vlocationText ', vlocationText);
				nlapiLogExecution('ERROR', 'vLot ', vLot);
				nlapiLogExecution('ERROR', 'vlineno ', vlineno);
				nlapiLogExecution('ERROR', 'vItemType ', vItemType);



				Newsearchresults.setLineItemValue('item','custcol_ebiz_lp',m,vlp);
				Newsearchresults.setLineItemValue('item','custcol_locationsitemreceipt',m, location);//previously it is vlocation
				Newsearchresults.setLineItemValue('item','custcol_ebizwoinventoryref', m,vrecid);


				nlapiLogExecution('ERROR', 'After ', 'After');

				var confirmLotToNS='Y';
				confirmLotToNS=GetConfirmLotToNS(location);

				if(confirmLotToNS=='N')
					vLot=vitemText.replace(/ /g,"-");

				nlapiLogExecution('ERROR', 'vItemType',vItemType);

				nlapiLogExecution('ERROR', 'confirmLotToNS',confirmLotToNS);
				nlapiLogExecution('ERROR', 'vLot',vLot);				 
				//if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T" )
				if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
					Newsearchresults.setLineItemValue('item','serialnumbers',m, vLot);


				/*var newLot = vLot.split(','); 
	newLot = newLot.join(String.fromCharCode(5)); 
	nlapiLogExecution('ERROR', 'Before Set Lot no',m, newLot);
	searchresults.setLineItemValue('item','serialnumbers',m, newLot);*/
				//nlapiLogExecution('ERROR', 'After Set Lot no', newLot);

				//nlapiLogExecution('ERROR', 'invref ', searchresults.getLineItemValue('item', 'custcol_ebizwoinventoryref', m));

				Newsearchresults.setFieldValue('custbody_ebiz_update_confirm','T');
				//if((vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T") && vTempLot != null && vTempLot != "")
				if((vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem") && vTempLot != null && vTempLot != "")
					Newsearchresults.setLineItemValue('item','custcol_ebizwolotinfo',m, vTempLot);

				Newsearchresults.setLineItemValue('item','custcol_ebizwoavalqty',m, vqty);
				Newsearchresults.setLineItemValue('item','custcol_ebizwobinloc',m, vlocation);
				Newsearchresults.setLineItemValue('item','custcol_ebizwobinlocvalue',m, vlocationText);						
				Newsearchresults.setLineItemValue('item','custcol_ebizwolotlineno', m,m);

				//nlapiCommitLineItem('item');
				//nlapiLogExecution('ERROR', 'linelot ', searchresults.getLineItemValue('item','custcol_ebizwolotlineno',m));
			}

			var CommittedId = nlapiSubmitRecord(Newsearchresults, true);
			nlapiLogExecution('ERROR', 'CommittedId ', CommittedId);
			DeleteOpentask(woid,SearchresultsArray); //delete records from open task
			AllocateInv(woid,SearchresultsArray);  //reallocate data in open task


		}

	}
	catch (e) {

		if (e instanceof nlobjError) 
		{
			nlapiLogExecution('ERROR', 'system error', e.getCode()+ '\n' + e.getDetails());

		}

		else 
		{ 
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			//alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
		}
//		nlapiCancelLineItem('item');

		nlapiLogExecution('ERROR','Error ', e.toString());
		var ErrorMsg = nlapiCreateError('CannotAllocate',e.toString(), true);
		throw ErrorMsg;
		return flase;


	}

	return true;

}


//to get Lotid
function GetLotno(Lotno,lineItemvalue,Location)
{
	if(Lotno != "" && Lotno != null)
	{nlapiLogExecution('ERROR', 'GetLotno', Lotno);	
	var filterbatch = new Array();
	filterbatch[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', Lotno);
	filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', lineItemvalue);
	filterbatch[2] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', Location);	

	var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch);
	if(receiptsearchresults!=null)
	{
		var lotid= receiptsearchresults[0].getId();
		nlapiLogExecution('ERROR', 'lotid', lotid);
		return lotid;		
	}

	}
	else
	{
		return null;
	}
}



//Check all component items pick confirm or not 
function CheckComponentItems(vWOId,WOItemcount)
{
	nlapiLogExecution('ERROR','WOItemcount ',WOItemcount);
	var OpentaskItemArray = new Array;
	var filterOpentask = new Array();
	filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','9']));	//	Status - Picks Confirm ,Pick gen
	filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
	if(vWOId != null && vWOId != "")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
	}

	var WOcolumns = new Array();
	WOcolumns[0] = new nlobjSearchColumn('custrecord_line_no');
	WOcolumns[1] = new nlobjSearchColumn('custrecord_sku');
	WOcolumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	WOcolumns[0].setSort();
	var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);
	if(WOSearchResults!=null && WOSearchResults!='' && WOSearchResults.length>0)
	{
		// Workorder Items Count of Opentask 
		for(var i=0; i<WOSearchResults.length ;i++)
		{
			var vlineno = WOSearchResults[i].getValue('custrecord_line_no');
			if(OpentaskItemArray.indexOf(vlineno)==-1)
			{
				OpentaskItemArray.push(vlineno);
			}
		}

		var OpenTskLineCount = OpentaskItemArray.length;
		nlapiLogExecution('ERROR','OpenTskLineCount ',OpenTskLineCount);
		if(parseInt(WOItemcount) == parseInt(OpenTskLineCount))
		{
			return true;
		}
		else
		{
			return false;
		}

	}
}

//For Stage Location
function GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,customizetype,StageBinInternalId,ordertype){
	nlapiLogExecution('Error', 'into GetPickStageLocation', Item);
	nlapiLogExecution('Error', 'Item', Item);
	nlapiLogExecution('Error', 'vSite', vSite);
	nlapiLogExecution('Error', 'vCompany', vCompany);
	nlapiLogExecution('Error', 'stgDirection', stgDirection);
	nlapiLogExecution('Error', 'vCarrier', vCarrier);
	nlapiLogExecution('Error', 'vCarrierType', vCarrierType);
	nlapiLogExecution('Error', 'ordertype', ordertype);
	nlapiLogExecution('Error', 'customizetype', customizetype);
	//var ordertype='5'; //Order Type ="WORK ORDER";
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 = "";
	var ItemInfo3 = "";
	var columns = nlapiLookupField('item', Item, fields);
	if(columns!=null){
		ItemFamily = columns.custitem_item_family;
		ItemGroup = columns.custitem_item_group;
		ItemStatus = columns.custitem_ebizdefskustatus;
		ItemInfo1 = columns.custitem_item_info_1;
		ItemInfo2 = columns.custitem_item_info_2;
		ItemInfo3 = columns.custitem_item_info_3;
	}
	nlapiLogExecution('Error', 'ItemFamily', ItemFamily);
	nlapiLogExecution('Error', 'ItemGroup', ItemGroup);
	nlapiLogExecution('Error', 'ItemStatus', ItemStatus);
	nlapiLogExecution('Error', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('Error', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('Error', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('Error', 'Before Variable declaration');
	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}
	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');
	columns[5] = new nlobjSearchColumn('custrecord_stagerule_ordtype');
	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
	if (vCarrier != null && vCarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));
	}
	if (vCarrierType != null && vCarrierType != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));
	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));
	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));
	}
	/*if (ordertype != null && ordertype != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@', ordertype]));
	}
	else
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@']));*/
	if (customizetype != null && customizetype != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@', customizetype]));
	}
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction]));
	filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'anyof', ['@NONE@','3']));
	/*if (StageBinInternalId != null && StageBinInternalId != "")
		//filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
	filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_outboundlocationgroup', 'anyof', StageBinInternalId));*/
	nlapiLogExecution('Error', 'Before Searching of Stageing Rules');
	var vstgLocation,vstgLocationText;
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < Math.min(10, searchresults.length); i++) {
			//GetPickStagLpCount                
			//fectch stage location for Ordertype = WORK ORDER		  
			var ordertype = searchresults[i].getText('custrecord_stagerule_ordtype');
			nlapiLogExecution('Error', 'ordertype::', ordertype);
			if(ordertype == 'WORK ORDER'){
				vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');
				vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');
			}
			nlapiLogExecution('Error', 'Stageing Location Value::', vstgLocation);
			nlapiLogExecution('Error', 'Stageing Location Text::', vstgLocationText);
			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');
			nlapiLogExecution('Error', 'vnoofFootPrints::', vnoofFootPrints);
			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetPickStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}
			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);
			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}
			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);
			nlapiLogExecution('Error', 'LocGroup::', LocGroup);
			if (LocGroup != null && LocGroup != "") {
				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');
				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				//This code is commented on 9th September 2011 by ramana
				/*
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				for (var k = 0; searchresults != null && k < searchresults.length; k++) {
					var vLocid=searchresults[k].getId();
						var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
						var vnoofFootPrints = searchresults[k].getValue('custrecord_nooffootprints');
						//--
						if (vnoofFootPrints != null && vnoofFootPrints != "") {
								if (vLpCnt < vnoofFootPrints) {
									return vLocid;
								}
						}
					else {
						return vLocid;
						}
				}
				 */
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('Error', 'searchresultsloc::', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();
//					var vLocid=searchresultsloc[0].getId();
					nlapiLogExecution('Error', 'vLocid::', vLocid);
					var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');
					//--
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}
			}
		}
	}
	return -1;
}
function GetPickStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');
	var outLPCount = 0;
	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));
	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));
	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));
	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');
	return outLPCount;
}
//delete Component Items; if Components items pick Confirm through RF
function DeleteInvtRecdOfMemberItems(WoIntid, BinLocId,WHLocation)
{
	try{

		var str = 'WoIntid. = ' + WoIntid + '<br>';
		str = str + 'BinLocId. = ' + BinLocId + '<br>';
		str = str + 'WHLocation. = ' + WHLocation + '<br>';	

		nlapiLogExecution('Debug', 'DeleteInvtRecdOfMemberItems Logs', str);

		var Ifilters = new Array();
		Ifilters[0] = new nlobjSearchFilter('custrecord_ebiz_transaction_no', null, 'anyof', WoIntid);
		Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [36]);
		//Ifilters[2] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', BinLocId);
		Ifilters[2] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', WHLocation);
		var invtId="";
		var invtType="";
		var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
		if (serchInvtRec) 
		{
			nlapiLogExecution('ERROR','serchInvtRec.length',serchInvtRec.length);

			for (var s = 0; s < serchInvtRec.length; s++) {
				nlapiLogExecution('ERROR','serchInvtRec.length',serchInvtRec.length);
				//nlapiLogExecution('ERROR','Inside loop','loop');
				var searchresult = serchInvtRec[ s ];
				nlapiLogExecution('ERROR','need to print this','print');
				invtId = serchInvtRec[s].getId();
				invtType= serchInvtRec[s].getRecordType();
				nlapiLogExecution('ERROR','CHKN INVT Id',invtId);
				//nlapiLogExecution('ERROR','CHKN invtType ',invtType);
				nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
				nlapiLogExecution('ERROR','Invt Deleted record Id',invtId);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','exception in delete ',e);
	}
}




function getMaxUOMQty(putItemId,putItemPC)
{
	var varPaltQty=0;
	var searchresults = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', putItemId));
	if(putItemPC!=null && putItemPC!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', putItemPC));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	columns[1].setSort(true);

	searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	if(searchresults != null && searchresults != '' && searchresults.length > 0)
	{
		varPaltQty = searchresults[0].getValue('custrecord_ebizqty');
	}

	return varPaltQty;
}



function fnForpartialWoProcessing(wointernalid,id,vMainQty){


	var now = new Date();
	//a Date object to be used for a random value
	var now = new Date();
	//now= now.getHours();
	//Getting time in hh:mm tt format.
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}


	var expqty = request.getParameter('custpage_kitqtyexp');
	nlapiLogExecution('ERROR','expqty',expqty);

	var vloc  = request.getParameter('cusppage_site');
	var woid = request.getParameter('custpage_workorder');

	var vstageLoc ='';

	var asmitem = request.getParameter('cusppage_item');
	nlapiLogExecution('ERROR','asmitem',asmitem);

	var memitemqty = new Array();
	memitemqty =fngetMemItemQty(wointernalid,expqty,vMainQty);
	nlapiLogExecution('ERROR','memitemqty',memitemqty);

	var lineCnt = request.getLineItemCount('custpage_deliveryord_items');

	var pickqtyfinal=0;
	var ItemStatusFilters = new Array();
	ItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', wointernalid));
	ItemStatusFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3));
	ItemStatusFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8,9]));


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_line_no');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_lpno');
	columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[6] = new nlobjSearchColumn('custrecord_sku');
	columns[7] = new nlobjSearchColumn('custrecord_invref_no');
	columns[8] = new nlobjSearchColumn('custrecord_sku_status');
	columns[9] = new nlobjSearchColumn('custrecord_packcode');
	columns[10] = new nlobjSearchColumn('name');
	columns[11] = new nlobjSearchColumn('custrecord_batch_no');
	columns[12] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[13] = new nlobjSearchColumn('custrecord_kit_refno');
	columns[14] = new nlobjSearchColumn('custrecord_last_member_component');
	columns[15] = new nlobjSearchColumn('custrecord_parent_sku_no');
	columns[16] = new nlobjSearchColumn('custrecord_serial_no');
	columns[17] = new nlobjSearchColumn('custrecord_comp_id');
	columns[18] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[19] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[21] = new nlobjSearchColumn('custrecord_wms_location');
	columns[22] = new nlobjSearchColumn('custrecord_container');			

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, ItemStatusFilters, columns);
	var context = nlapiGetContext();
	var emp = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_emp');
	var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
	var parentid = nlapiSubmitRecord(otparent); 
	var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);
	// var tolineparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');

	for(var m = 0; m<memitemqty.length; m++){
		var memitem = memitemqty[m][0];
		var vTempMemQty = memitemqty[m][2];
		var memline = memitemqty[m][3];
		var memqty = vTempMemQty;
		if(memqty == null || memqty == '')
			memqty=0;
		for (var x = 1; x <= lineCnt && parseFloat(memqty) > 0 ; x++) 
		{	
			var vBatch="";
			var vBinlocation="";
			var varsku = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', x);

			var varqty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_qtyexp', x);

			//var varqty = expqty[x];
			var varexpqty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_expqty', x);
			var vactbeginloc = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', x);





			var vline, vitem, vqty, vso, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono, vBatchno, vfromlp;
			var vkitrefno, lastkititem, kititem, vserialno, vCompany, vContlpNo, vEbizWaveNo, vSointernid, warehouseLocation,vcontainersize;
			var vInvRefArr=new Array();
			/*for (var i = 0; searchresults != null && i < searchresults.length; i++) 
	{*/

			//nlapiLogExecution('ERROR','searchresults.length',searchresults.length);

			//	var searchresult = searchresults[i];

			//vline = searchresult.getValue('custrecord_line_no');
			//vitem = searchresult.getValue('custrecord_ebiz_sku_no');
//			vqty = searchresult.getValue('custrecord_expe_qty');
//			vso = searchresult.getValue('custrecord_ebiz_cntrl_no');
			//vLpno = searchresult.getValue('custrecord_container_lp_no');
			//	vlocationid = searchresult.getValue('custrecord_actbeginloc');
			//vlocation = searchresult.getText('custrecord_actbeginloc');

			vline = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', x);
			vitem = request.getLineItemValue('custpage_deliveryord_items', 'custpage_skuno', x);
			vqty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_expqty', x);
			vso = request.getLineItemValue('custpage_deliveryord_items', 'custpage_cntrlno', x);
			vLpno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_contlpno', x);
			vlocationid = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlocation', x);
			vlocation = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vtemploctext', x);



			nlapiLogExecution('ERROR','vline',vline);
			nlapiLogExecution('ERROR','vitem',vitem);
			nlapiLogExecution('ERROR','vqty',vqty);
			nlapiLogExecution('ERROR','vso',vso);
			nlapiLogExecution('ERROR','vLpno',vLpno);
			nlapiLogExecution('ERROR','vlocationid',vlocationid);
			nlapiLogExecution('ERROR','vlocation',vlocation);

			/*vskustatusvalue = searchresult.getValue('custrecord_sku_status');
		vSKU = searchresult.getText('custrecord_sku');
		vinvrefno = searchresult.getValue('custrecord_invref_no');
		vrecid = searchresult.getId();

		vskustatus = searchresult.getText('custrecord_sku_status');
		vpackcode = searchresult.getValue('custrecord_packcode');
		vdono = searchresult.getValue('name');
		vfromlp = searchresult.getValue('custrecord_from_lp_no');
		vBatchno = searchresult.getValue('custrecord_batch_no');*/

			vskustatusvalue = request.getLineItemValue('custpage_deliveryord_items', 'custpage_skustatus', x);
			vSKU = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', x);
			vinvrefno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_skurecid', x);
			vrecid = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vid', x);
			vskustatus = request.getLineItemValue('custpage_deliveryord_items', 'custpage_skustatustext', x);
			vpackcode = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vpackcode', x);
			vdono = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vdono', x);
			vfromlp = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vfromlp', x);
			vBatchno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotnotext', x);
			var zoneid = request.getLineItemValue('custpage_deliveryord_items', 'custpage_zoneid', x);
			var prevwmsstatus = request.getLineItemValue('custpage_deliveryord_items', 'custpage_prevwmsstatus', x);
			nlapiLogExecution('ERROR','vSKU',vSKU);
			nlapiLogExecution('ERROR','vinvrefno',vinvrefno);
			nlapiLogExecution('ERROR','vrecid',vrecid);
			nlapiLogExecution('ERROR','vskustatus',vskustatus);
			nlapiLogExecution('ERROR','vpackcode',vpackcode);
			nlapiLogExecution('ERROR','vdono',vdono);
			nlapiLogExecution('ERROR','vfromlp',vfromlp);
			nlapiLogExecution('ERROR','vBatchno',vBatchno);



			/*	if(x==parseFloat(vline))
		{	*/

			/*	nlapiLogExecution('ERROR','x',x);
			nlapiLogExecution('ERROR','vline',vline);*/

			/*vkitrefno = searchresult.getValue('custrecord_kit_refno');
			lastkititem = searchresult.getValue('custrecord_last_member_component');
			kititem = searchresult.getValue('custrecord_parent_sku_no');
			vserialno = searchresult.getValue('custrecord_serial_no');
			vCompany = searchresult.getValue('custrecord_comp_id');
			vContlpNo = searchresult.getValue('custrecord_container_lp_no');
			vEbizWaveNo = searchresult.getValue('custrecord_ebiz_wave_no');
			vSointernid = searchresult.getValue('custrecord_ebiz_order_no');
			warehouseLocation = searchresult.getValue('custrecord_wms_location');
			vcontainersize = searchresult.getValue('custrecord_container');*/


			vkitrefno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vkitrefno', x);
			lastkititem = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lastkititem', x);
			kititem = request.getLineItemValue('custpage_deliveryord_items', 'custpage_kititem', x);
			vserialno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vserialno', x);
			vCompany = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vcompany', x);
			vContlpNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_cntrlno', x);
			vEbizWaveNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vebizwaveno', x);
			vSointernid = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vsointernid', x);
			warehouseLocation = request.getLineItemValue('custpage_deliveryord_items', 'custpage_warehouselocation', x);
			vcontainersize = request.getLineItemValue('custpage_deliveryord_items', 'custpage_vcontainersize', x);


			var str = 'vline. = ' + vline + '<br>';
			str = str + 'vitem. = ' + vitem + '<br>';
			str = str + 'vqty. = ' + vqty + '<br>';	
			str = str + 'vso. = ' + vso + '<br>';	
			str = str + 'vLpno. = ' + vLpno + '<br>';
			str = str + 'vlocationid. = ' + vlocationid + '<br>';			
			str = str + 'vlocation. = ' + vlocation + '<br>';
			str = str + 'vskustatusvalue. = ' + vskustatusvalue + '<br>';	
			str = str + 'vSKU. = ' + vSKU + '<br>';	
			str = str + 'vinvrefno. = ' + vinvrefno + '<br>';
			str = str + 'vrecid. = ' + vrecid + '<br>';			
			str = str + 'vskustatus. = ' + vskustatus + '<br>';
			str = str + 'vpackcode. = ' + vpackcode + '<br>';	
			str = str + 'vdono. = ' + vdono + '<br>';	
			str = str + 'vfromlp. = ' + vfromlp + '<br>';
			str = str + 'zoneid. = ' + zoneid + '<br>';
			str = str + 'prevwmsstatus. = ' + prevwmsstatus + '<br>';				
			str = str + 'vkitrefno. = ' + vkitrefno + '<br>';
			str = str + 'lastkititem. = ' + lastkititem + '<br>';	
			str = str + 'kititem. = ' + kititem + '<br>';
			str = str + 'vSointernid. = ' + vSointernid + '<br>';
			str = str + 'vContlpNo. = ' + vContlpNo + '<br>';
			str = str + 'warehouseLocation. = ' + warehouseLocation + '<br>';	

			nlapiLogExecution('Debug', 'fnForpartialWoProcessing Line Details', str);






			//for(var m = 0; m<memitemqty.length; m++){

			var str = 'memitemqty.length. = ' + memitemqty.length + '<br>';
			str = str + 'memitem. = ' + memitem + '<br>';
			str = str + 'kititem. = ' + kititem + '<br>';	
			str = str + 'vitem. = ' + vitem + '<br>';	
			str = str + 'memline. = ' + memline + '<br>';
			str = str + 'vline. = ' + vline + '<br>';

			nlapiLogExecution('Debug', 'Member Item Details', str);


			nlapiLogExecution('ERROR','memitem',memitem);
			nlapiLogExecution('ERROR','kititem',kititem);
			nlapiLogExecution('ERROR','vitem',vitem);
			nlapiLogExecution('ERROR','memline',memline);
			nlapiLogExecution('ERROR','vline',vline);
			//if(vitem==memitem && parseFloat(memqty)>0){
			if(parseInt(vline)==parseInt(memline) && parseFloat(memqty)>0){

				var arrDims = getSKUCubeAndWeightforconfirm(vitem, 1);
				var itemCube = 0;
				var itemWeight=0;			

				if (arrDims[0] != "" && (!isNaN(arrDims[1]))) 
				{
					/*itemCube = (parseFloat(vqty) * parseFloat(arrDims[0]));
				itemWeight = (parseFloat(vqty) * parseFloat(arrDims[1]));*/

					itemCube = (parseFloat(varqty) * parseFloat(arrDims[0]));
					itemWeight = (parseFloat(varqty) * parseFloat(arrDims[1]));

				} 

				var arrContainerDetails=getContainerCubeAndTarWeight(vcontainersize,"");
				var ContainerCube=0;
				var TotalWeight=0;

				if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) 
				{
					ContainerCube =  parseFloat(arrContainerDetails[0]);
					TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));
				} 

				var expQty;

				if(vqty>=memqty)
					expQty = memqty;
				else
					expQty = vqty; 

				var remQty = vqty - expQty;

				var str = 'expQty. = ' + expQty + '<br>';
				str = str + 'remQty. = ' + remQty + '<br>';
				str = str + 'memqty. = ' + memqty + '<br>';	

				nlapiLogExecution('Debug', 'Qty Details', str);

				memqty = parseFloat(memqty)-parseFloat(expQty);

				var currentContext = nlapiGetContext();
				var currentUserID = currentContext.getUser();

				var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', vrecid);
				var vOldOTStatus=transaction.getFieldValue('custrecord_wms_status_flag');
				// case# 201412354
				var currentContext = nlapiGetContext();  
				var currentUserID = currentContext.getUser();
				if(vOldOTStatus == '9')
				{
					transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
					transaction.setFieldValue('custrecord_actendloc', vlocationid);
					transaction.setFieldValue('custrecord_container', vcontainersize);
					transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(5));
					transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(5));
					transaction.setFieldValue('custrecord_batch_no', vBatchno);				
					transaction.setFieldValue('custrecord_container_lp_no', vContlpNo);
					transaction.setFieldValue('custrecord_act_end_date', DateStamp());
					transaction.setFieldValue('custrecord_actualendtime',((curr_hour) + ":" + (curr_min) + " " + a_p));
					if(currentUserID != null && currentUserID != '')
						transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
				}
				else
				{
					transaction.setFieldValue('custrecord_wms_status_flag', '14'); //Shipped
				}
				transaction.setFieldValue('custrecord_act_qty', parseFloat(expQty).toFixed(5));
				transaction.setFieldValue('custrecord_expe_qty', parseFloat(expQty).toFixed(5)); 
				if(currentUserID != null && currentUserID != '')
					transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);		

				transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no',id);
				transaction.setFieldValue('custrecord_notes','IN PROCESS');
				transaction.setFieldValue('name',vdono);

				nlapiLogExecution('ERROR','vOldOTStatus',vOldOTStatus);
				if(remQty!=0 && remQty>0){
					var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',vrecid);		    	
					//createopentaskrec.setFieldValue('custrecord_wms_status_flag', '9');//Statusflag='C'
					//	createopentaskrec.setFieldValue('custrecord_act_qty', parseFloat(vDiffQty).toFixed(5));
					createopentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(remQty).toFixed(5));
					if(vOldOTStatus == '8')
						createopentaskrec.setFieldValue('custrecord_act_qty', parseFloat(remQty).toFixed(5));
					else
						createopentaskrec.setFieldValue('custrecord_act_qty', 0);
					//createopentaskrec.setFieldValue('custrecord_wms_status_flag', '14');
					createopentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
					createopentaskrec.setFieldValue('custrecord_ebizuser', currentUserID);
					createopentaskrec.setFieldValue('name', vdono);
					createopentaskrec.setFieldValue('custrecord_notes','SPLIT');
					nlapiSubmitRecord(createopentaskrec, false, true);
				}

				nlapiSubmitRecord(transaction, false, true);
				//Inventory updation
				if(vOldOTStatus == '9')
					var invupdate=UpdateInventory(vinvrefno,expQty);


				//Outbound stage inventory updation
				var updateoubboundinv = UpdateInvtRecdOfMemberItems(woid, vstageLoc,vloc,expQty,vitem);

				if(memqty <=0)
					break;
			}
		}


		/*
			newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', vrecid);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'name', searchresult.getValue('name'));
			if(searchresult.getValue('custrecord_ebiz_sku_no') != null && searchresult.getValue('custrecord_ebiz_sku_no') != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebiz_sku_no', searchresult.getValue('custrecord_ebiz_sku_no'));
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_tasktype', 3);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecordact_begin_date', searchresult.getValue('custrecordact_begin_date'));		
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actualbegintime', searchresult.getValue('custrecord_actualbegintime'));
			if(searchresult.getValue('custrecord_actbeginloc') != null && searchresult.getValue('custrecord_actbeginloc') != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actbeginloc', searchresult.getValue('custrecord_actbeginloc'));
			var currentContext = nlapiGetContext();
			var currentUserID = currentContext.getUser();


			if(currentUserID != null && currentUserID != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_upd_ebiz_user_no', currentUserID);
			if(searchresult.getValue('custrecord_ebizuser') != null && searchresult.getValue('custrecord_ebizuser') != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebizuser', searchresult.getValue('custrecord_ebizuser'));

			//	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_expe_qty', parseFloat(searchresult.getValue('custrecord_expe_qty')).toFixed(5));
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_expe_qty', parseFloat(varqty).toFixed(5));
			//	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_act_qty', parseFloat(vqty).toFixed(5));
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_act_qty', parseFloat(varqty).toFixed(5));
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_wms_status_flag', 8);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_batch_no', vBatchno);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebiz_cntrl_no', searchresult.getValue('custrecord_ebiz_cntrl_no'));
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_line_no', searchresult.getValue('custrecord_line_no'));
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_lpno', searchresult.getValue('custrecord_lpno'));

			if(searchresult.getValue('custrecord_ebiz_order_no') != null && searchresult.getValue('custrecord_ebiz_order_no') != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_ebiz_order_no', searchresult.getValue('custrecord_ebiz_order_no'));
			if(searchresult.getValue('custrecord_sku') != null && searchresult.getValue('custrecord_sku') != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_sku', searchresult.getValue('custrecord_sku'));
			if(searchresult.getValue('custrecord_packcode') != null && searchresult.getValue('custrecord_packcode') != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_packcode', searchresult.getValue('custrecord_packcode'));
			if(searchresult.getValue('custrecord_sku_status') != null && searchresult.getValue('custrecord_sku_status') != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_sku_status', searchresult.getValue('custrecord_sku_status'));
			if(searchresult.getValue('custrecord_wms_location') != null && searchresult.getValue('custrecord_wms_location') != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_wms_location', searchresult.getValue('custrecord_wms_location'));
			if(vlocationid != null && vlocationid != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actendloc', vlocationid);
			if(vcontainersize != null && vcontainersize != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_container', vcontainersize);
			if(itemWeight != null && itemWeight != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_total_weight', parseFloat(itemWeight).toFixed(5));
			if(itemCube != null && itemCube != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_totalcube', parseFloat(itemCube).toFixed(5));
			if(vlocationid != null && vlocationid != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actendloc', vlocationid);
			if(vContlpNo != null && vContlpNo != '')
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_container_lp_no', vContlpNo);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_act_end_date', DateStamp());
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
			var vemployee = request.getParameter('custpage_employee');

			if (vemployee != null && vemployee != "") 
			{
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_taskassignedto', vemployee);
			}	  
			else 
			{
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'custrecord_taskassignedto', currentUserID);

			}
			newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');


				tolineparent.selectNewLineItem('recmachcustrecord_ebiz_toline_parent');
			tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'name', 'WO'+varsku+x);
			tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_pickgen_qty', parseFloat(varqty).toFixed(5));
			tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_pickconf_qty', parseFloat(varqty).toFixed(5));
			tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_ship_qty', parseFloat(varqty).toFixed(5));
			tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_status_flag', 14);
			tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_item', varsku);
			tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_ord_category', 4);
			tolineparent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent', 'custrecord_orderlinedetails_order_no', 'WO'+varsku+x);
			tolineparent.commitLineItem('recmachcustrecord_ebiz_toline_parent');



			var remqty = varexpqty - varqty;*/
		/*	if(remqty!=0 && remqty>0){

				var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',vrecid);		    	
				createopentaskrec.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
				createopentaskrec.setFieldValue('custrecord_act_qty', parseFloat(vDiffQty).toFixed(5));
				createopentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(vDiffQty).toFixed(5));
				createopentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
				createopentaskrec.setFieldValue('custrecord_ebizuser', currentUserID);
				createopentaskrec.setFieldValue('name',ffordno);

				nlapiSubmitRecord(createopentaskrec, false, true);



				var openTaskRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

				openTaskRecord.setFieldValue('name', searchresult.getValue('name'));							// DELIVERY ORDER NAME
				openTaskRecord.setFieldValue('custrecord_ebiz_sku_no', searchresult.getValue('custrecord_ebiz_sku_no');		// ITEM INTERNAL ID
				openTaskRecord.setFieldValue('custrecord_sku', searchresult.getValue('custrecord_sku'));				// ITEM NAME
				openTaskRecord.setFieldValue('custrecord_expe_qty', parseFloat(remqty).toFixed(5));				// FULFILMENT ORDER QUANTITY
				if(vContlpNo!=null && vContlpNo!='')
					openTaskRecord.setFieldValue('custrecord_container_lp_no', vContlpNo);
				openTaskRecord.setFieldValue('custrecord_line_no', searchresult.getValue('custrecord_line_no'));
				openTaskRecord.setFieldValue('custrecord_tasktype', '3'); 							// TASK TYPE = PICK
				if(searchresult.getValue('custrecord_actbeginloc') != null && searchresult.getValue('custrecord_actbeginloc') != '')
					openTaskRecord.setFieldValue('custrecord_actbeginloc', searchresult.getValue('custrecord_actbeginloc'));
				openTaskRecord.setFieldValue('custrecord_ebiz_cntrl_no',searchresult.getValue('custrecord_ebiz_cntrl_no'));

				if(searchresult.getValue('custrecord_ebiz_order_no') != null && searchresult.getValue('custrecord_ebiz_order_no') != '')
					openTaskRecord.setFieldValue('custrecord_ebiz_order_no', searchresult.getValue('custrecord_ebiz_order_no'));

				openTaskRecord.setFieldValue('custrecordact_begin_date',searchresult.getValue('custrecordact_begin_date'));

				openTaskRecord.setFieldValue('custrecord_wms_status_flag', '9'); 					// STATUS FLAG = G

				openTaskRecord.setFieldValue('custrecord_from_lp_no', vfromlp);
				openTaskRecord.setFieldValue('custrecord_lpno', searchresult.getValue('custrecord_lpno'));
				openTaskRecord.setFieldValue('custrecord_actualbegintime', searchresult.getValue('custrecord_actualbegintime'));
				openTaskRecord.setFieldValue('custrecord_invref_no', vinvrefno);
				if(searchresult.getValue('custrecord_packcode') != null && searchresult.getValue('custrecord_packcode') != '')
					openTaskRecord.setFieldValue('custrecord_packcode', searchresult.getValue('custrecord_packcode'));
				if(searchresult.getValue('custrecord_sku_status') != null && searchresult.getValue('custrecord_sku_status') != '')
					openTaskRecord.setFieldValue('custrecord_sku_status', searchresult.getValue('custrecord_sku_status'));

				openTaskRecord.setFieldValue('custrecord_batch_no', vBatchno);
				if(searchresult.getValue('custrecord_wms_location') != null && searchresult.getValue('custrecord_wms_location') != '')
					openTaskRecord.setFieldValue('custrecord_wms_location', searchresult.getValue('custrecord_wms_location'));	

				var currentContext = nlapiGetContext();  
				var currentUserID = currentContext.getUser();		
				openTaskRecord.setFieldValue('custrecord_ebizuser', currentUserID);		
				openTaskRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);						
				openTaskRecord.setFieldValue('custrecord_taskassignedto', currentUserID);
				if(itemWeight != null && itemWeight != '')
					openTaskRecord.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(5));
				if(itemCube != null && itemCube != '')
					openTaskRecord.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(5));
				if(vcontainersize != null && vcontainersize != '')
					openTaskRecord.setFieldValue('custrecord_container', vcontainersize);


				nlapiSubmitRecord(openTaskRecord,false,true);

			}*/


		//}
		//}




	}

	/*	var vInvRefArr=new Array();
	if(searchresults!=null && searchresults!='')
	{
		for (var k = 0; k < searchresults.length; k++) 
		{
			var resultsset = searchresults[k];
			var varRecid = resultsset.getValue('custrecord_invref_no');
			//var varRecid = request.getLineItemValue('custpage_deliveryord_items', 'custpage_skurecid', k);
			nlapiLogExecution('ERROR', 'skuInvtRecid_NO', varRecid);
			if(vInvRefArr.indexOf(varRecid)==-1)
			{

				vInvRefArr.push(varRecid);
			}	
		}	
	}*/	
	/*nlapiLogExecution('ERROR', 'vInvRefArr', vInvRefArr);
	if(vInvRefArr != null && vInvRefArr != '' && vInvRefArr.length>0)
		InvtDetails=GetAllInvtDetails(vInvRefArr,0);
	var vInvRecNew=new Array();
	var vInvRecIdNew=new Array();
	var vInvExpQtyNew=new Array();
	var context = nlapiGetContext();  

	if(searchresults!=null && searchresults!='')
	{
		for (var z = 0; z < searchresults.length; z++) 
		{
			var vResults = searchresults[z];
			nlapiLogExecution('ERROR', 'z', z);

			var varsku =vResults.getValue('custrecord_ebiz_sku_no');				
			varqty =vResults.getValue('custrecord_expe_qty');			
			var varRecid =vResults.getValue('custrecord_invref_no');	
			var varLotNo = vResults.getValue('custrecord_batch_no');
			var LineNo=vResults.getValue('custrecord_line_no');



			nlapiLogExecution('ERROR', 'inventory intern id', varRecid);
			nlapiLogExecution('ERROR', 'InvtDetails', InvtDetails);
			if(InvtDetails != null && InvtDetails != '')
			{
				var vInvRec=GetInvDetails(varRecid,InvtDetails);

				nlapiLogExecution('ERROR', 'vInvRec', vInvRec);


				// Update the inventory to reduce the allocation quantity
				if(vInvRec!=null && vInvRec!='')
				{
					if(vInvRecIdNew.indexOf(varRecid) == -1)
					{
						vInvRecIdNew.push(varRecid);
						vInvRecNew.push(vInvRec);
						vInvExpQtyNew.push(varqty);
					}
					else
					{
						var vOldInvExpQty=vInvExpQtyNew[vInvRecIdNew.indexOf(varRecid)];
						if(vOldInvExpQty == null || vOldInvExpQty == '')
							vOldInvExpQty=0;
						vInvExpQtyNew[vInvRecIdNew.indexOf(varRecid)]=parseFloat(varqty) + parseFloat(vOldInvExpQty);
						//vInvExpQtyNew.push(parseFloat(expectedQty) + parseFloat(vOldInvExpQty));
					}
					//updateInventoryForTaskParent(vInvRec, expectedQty,emp,newParent);									// 4 UNITS
				}
			}
		}
	}
	//upto to here for custom defined invenotry table.

	if(vInvRecIdNew != null && vInvRecIdNew != '' && vInvRecIdNew.length>0)
	{	
		nlapiLogExecution('ERROR', 'vInvRecIdNew', vInvRecIdNew);
		nlapiLogExecution('ERROR', 'vInvExpQtyNew', vInvExpQtyNew);
		for(var t=0;t<vInvRecIdNew.length;t++)
		{
			nlapiLogExecution('ERROR', 'vInvRecNew[t]', vInvRecNew[t]);
			nlapiLogExecution('ERROR', 'vInvExpQtyNew[t]', vInvExpQtyNew[t]);
			if(vInvRecNew[t] != null)
				updateInventoryForTaskParent(vInvRecNew[t], vInvExpQtyNew[t],emp,newParent);	

		}
	}
	var pId = nlapiSubmitRecord(newParent);
	nlapiLogExecution('ERROR', 'Before updating transfer ord line',pId);
	// nlapiSubmitRecord(tolineparent);
	nlapiLogExecution('ERROR', 'Update Inventory End');


	nlapiLogExecution('ERROR','vLotNo',varLotNo);
	//nlapiLogExecution('ERROR','varLotQty',varLotQty);


	//var assmid=transformAssemblyItem(woid,vqty,vloc,varLotNo);
	//var assmid= transformAssemblyItem(wointernalid,vqty,vloc,vheaderlotno,vitem);

	nlapiLogExecution('ERROR','assmid',assmid);
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining usage after Inventory',context.getRemainingUsage());
	// this else part is excute , whenever component Items pick confirm through RF and Confirm build in UI
	var vstageLoc ='';
	var vCarrier ='';  
	var vSite;
	var vCompany;
	var stgDirection="OUB";
	nlapiLogExecution('ERROR', 'vitem', vitem);
	nlapiLogExecution('ERROR', 'vloc', vloc);
	if(vstageLoc==null || vstageLoc=='')
	vstageLoc = GetPickStageLocation(vitem,vCarrier,vloc,vCompany,stgDirection,null,null,null,null);
nlapiLogExecution('ERROR', 'vstagLoc', vstageLoc);
if (vstageLoc != null && vstageLoc != "" && vstageLoc != "-1") 
	{										
		DeleteInvtRecdOfMemberItems(woid, vstageLoc,vloc);
	}*/


}

/*function fngetMemItemQtyOld(vitem,vReqQty){

	var filters = new Array(); 

	 filters[0] = new nlobjSearchFilter('internalid', null, 'is', vitem);//kit_id is the parameter name 
	 nlapiLogExecution('DEBUG', 'vitem', vitem); 
	 var columns1 = new Array(); 
	 columns1[0] = new nlobjSearchColumn( 'memberitem' ); 
	 columns1[1] = new nlobjSearchColumn( 'memberquantity' );

	 var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 );  
	 var kititemsarr = new Array();
	 for(var q=0; searchresults!=null && q<searchresults.length;q++) 
	 {
	  var vSubArr=new Array();
	  vSubArr.push(searchresults[q].getValue('memberitem'));
	  var vMemQty=searchresults[q].getValue('memberquantity');
	  if(vMemQty == null || vMemQty == '')
	   vMemQty=0;
	  var vActQty= parseFloat(vMemQty) * parseFloat(vReqQty);
	  vSubArr.push(searchresults[q].getValue('memberquantity'));
	  vSubArr.push(vActQty);
	  kititemsarr.push(vSubArr);
	 }

	return kititemsarr;

}*/

function fngetMemItemQty(woid,vReqQty,vMainQty){

	nlapiLogExecution('ERROR','vReqQty',vReqQty);
	nlapiLogExecution('ERROR','vMainQty',vMainQty);

	var filters = new Array(); 

	filters.push(new nlobjSearchFilter('internalid', null, 'is', woid));//kit_id is the parameter name 
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
//	nlapiLogExecution('DEBUG', 'vitem', vitem); 
	var columns1 = new Array(); 
	columns1[0] = new nlobjSearchColumn( 'item' ); 
	columns1[1] = new nlobjSearchColumn( 'quantity' );
	columns1[2] = new nlobjSearchColumn( 'line' );

	var searchresults = nlapiSearchRecord( 'workorder', null, filters, columns1 );  
	var kititemsarr = new Array();
	for(var q=0; searchresults!=null && q<searchresults.length;q++) 
	{
		var vSubArr=new Array();
		vSubArr.push(searchresults[q].getValue('item'));
		var vMemQty=searchresults[q].getValue('quantity');
		nlapiLogExecution('ERROR','vMemQty',vMemQty);
		if(vMemQty == null || vMemQty == '')
			vMemQty=0;
		var vActQty= (parseFloat(vMemQty)/parseFloat(vMainQty)) * parseFloat(vReqQty);
		vSubArr.push(searchresults[q].getValue('quantity'));
		vSubArr.push(vActQty);
		vSubArr.push(searchresults[q].getValue('line'));
		kititemsarr.push(vSubArr);
	}

	return kititemsarr;

}

function UpdateInventory(invid,qty){

	var scount=1;
	var alreadyQoH = 0;
	LABL1: for(var j=0;j<scount;j++)
	{	
		nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', j);
		nlapiLogExecution('Error', 'qty', qty);
		nlapiLogExecution('Error', 'invid', invid);

		try
		{
			var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invid);
			var alreadyallocqty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
			var totalallocqty=parseFloat(alreadyallocqty)- parseFloat(qty);

			nlapiLogExecution('Error', 'totalallocqty', totalallocqty);

			inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(totalallocqty).toFixed(5));

			alreadyQoH = inventoryTransaction.getFieldValue('custrecord_ebiz_qoh');
			var totalQoH = parseFloat(alreadyQoH) - parseFloat(qty);
			nlapiLogExecution('Error', 'totalQoH', totalQoH);

			inventoryTransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totalQoH).toFixed(5));

			inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
			inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

			var id2 = nlapiSubmitRecord(inventoryTransaction,false,true);
			nlapiLogExecution('DEBUG', 'Out of updateInventoryWithAllocation - Record ID',id2);

		}
		catch(ex)
		{
			var exCode='CUSTOM_RECORD_COLLISION'; 

			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			}				 
			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{ 
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;
		}
	}

	if(parseFloat(alreadyQoH) - parseFloat(qty)<=0)
	{
		var delRecId = nlapiDeleteRecord('customrecord_ebiznet_createinv', id2);	
	}

}

function UpdateInvtRecdOfMemberItems(WoIntid, BinLocId,WHLocation,qty,item){

	var alreadyQoH = 0;
	try{
		nlapiLogExecution('ERROR','Inside UpdateInvtRecdOfMemberItems ','Funciton');
		nlapiLogExecution('ERROR','WoIntid ',WoIntid);
		nlapiLogExecution('ERROR','BinLocId ',BinLocId);
		nlapiLogExecution('ERROR','WHLocation',WHLocation);	
		nlapiLogExecution('ERROR','item',item);	
		var Ifilters = new Array();
		Ifilters[0] = new nlobjSearchFilter('custrecord_ebiz_transaction_no', null, 'anyof', WoIntid);
		//Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18]);
		Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [36]);
		//Ifilters[2] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', BinLocId);
		Ifilters[2] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', WHLocation);
		Ifilters[3] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item);


		/*	var Icolumns = new Array();
		Icolumns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');
		Icolumns[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');*/
		var invtId="";
		var invtType="";
		var remQty=0;
		var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
		if (serchInvtRec) 
		{

			for (var s = 0; s < serchInvtRec.length; s++) {
				nlapiLogExecution('ERROR','serchInvtRec.length',serchInvtRec.length);
				nlapiLogExecution('ERROR','qty',qty);
				//nlapiLogExecution('ERROR','Inside loop','loop');
				var searchresult = serchInvtRec[ s ];
				nlapiLogExecution('ERROR','need to print this','print');
				invtId = serchInvtRec[s].getId();

				var scount=1;
				var alreadyQoH = 0;
				LABL1: for(var j=0;j<scount;j++)
				{	
					nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', j);
					nlapiLogExecution('Error', 'qty', qty);
					nlapiLogExecution('Error', 'invid', invtId);

					try
					{
						var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtId);
						//	var alreadyallocqty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
						//	var totalallocqty=parseFloat(alreadyallocqty)- parseFloat(qty);

						//	nlapiLogExecution('Error', 'totalallocqty', totalallocqty);

						//	inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(totalallocqty).toFixed(5));

						alreadyQoH = inventoryTransaction.getFieldValue('custrecord_ebiz_qoh');
						remQty =  qty - alreadyQoH;
						var totalQoH = parseFloat(alreadyQoH) - parseFloat(qty);
						nlapiLogExecution('Error', 'totalQoH', totalQoH);


						inventoryTransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totalQoH).toFixed(5));

						inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
						inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

						var id2 = nlapiSubmitRecord(inventoryTransaction,false,true);
						nlapiLogExecution('DEBUG', 'Out of updateInventoryWithAllocation - Record ID',id2);

					}
					catch(ex)
					{
						var exCode='CUSTOM_RECORD_COLLISION'; 

						if (ex instanceof nlobjError) 
						{	
							wmsE=ex.getCode() + '\n' + ex.getDetails();
							exCode=ex.getCode();
						}
						else
						{
							wmsE=ex.toString();
							exCode=ex.toString();
						}				 
						if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
						{ 
							scount=scount+1;
							continue LABL1;
						}
						else break LABL1;
					}
				}

				if(parseFloat(alreadyQoH) - parseFloat(qty)<=0)
				{
					var delRecId = nlapiDeleteRecord('customrecord_ebiznet_createinv', id2);	
				}
				if(remQty<=0)
					break;
				else
					qty = remQty;
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','exception in delete ',e);
	}



}
function isMergeLP(poloc,methodid)
{
	var varMergeLP = 'F';
	var filtersputmethod = new Array();
	filtersputmethod.push(new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof', ['@NONE@',poloc]));
	if(methodid!=null && methodid!='')
		filtersputmethod.push(new nlobjSearchFilter('custrecord_methodid', null, 'is', methodid));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            

	var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	if(putwmethodsearchresults != null &&putwmethodsearchresults != ''&& putwmethodsearchresults.length > 0)
	{

		varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
	}

	return varMergeLP;
}

function updateOpenTaskwithns(woid,id){

	var ItemStatusFilter = new Array();
	ItemStatusFilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', woid));
	ItemStatusFilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3));
	ItemStatusFilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', 8));
	ItemStatusFilter.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));

	nlapiLogExecution('ERROR', 'Into updateOpenTaskwithns');
	nlapiLogExecution('ERROR', 'woid',woid);
	nlapiLogExecution('ERROR', 'id',id);
	var column = new Array();
	column[0] = new nlobjSearchColumn('name');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, ItemStatusFilter, column);
	if(searchresults!=null && searchresults!=''){
		for(var i=0; i<searchresults.length; i++){

			var opid = searchresults[i].getId();

			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask',opid);

			transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no',id);

			var id2 = nlapiSubmitRecord(transaction,false,true);

		}
	}
}
