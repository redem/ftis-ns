/***************************************************************************
???????????????????????eBizNET
??????????????????eBizNET Solutions Inc
****************************************************************************
*
*? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_CheckCompanyEntry_CL.js,v $
*? $Revision: 1.2.4.1.8.2 $
*? $Date: 2013/07/15 07:53:46 $
*? $Author: spendyala $
*? $Name: t_NSWMS_2013_1_7_35 $
*
* DESCRIPTION
*? Functionality
*
* REVISION HISTORY
*? $Log: ebiz_CheckCompanyEntry_CL.js,v $
*? Revision 1.2.4.1.8.2  2013/07/15 07:53:46  spendyala
*? case# 20123149
*? UnExpected Error fixed
*?
*? Revision 1.2.4.1.8.1  2013/05/09 15:28:42  grao
*? CASE201112/CR201113/LOG201121
*? Standard bundle issues fixes
*?
*? Revision 1.2.4.1  2012/01/18 14:37:02  spendyala
*? CASE201112/CR201113/LOG201121
*? Added functionality to validate splCharacter entering into the fields and validating not to enter char/spl char into mobile no field .
*?
*
****************************************************************************/




/**
 * @param type
 * @returns {Boolean}
 */
function fnCheckCompanyEntry(type){
	var vCompanyName = nlapiGetFieldValue('name');

	//Added on 09052013 by Madhu Kumar
	//Added to check validations of all the feilds

	var str=nlapiGetFieldValue('custrecord_company');
	var sresult=saveValidateSplCharacter(str,'Company');

	str=nlapiGetFieldValue('custrecord_compdescription');
	sresult=sresult+saveValidateSplCharacter(str,'CompanyDesc');

	str=nlapiGetFieldValue('custrecord_compcity');
	sresult=sresult+saveValidateSplCharacter(str,'City');

	str=nlapiGetFieldValue('custrecord_compfirstname');
	sresult=sresult+saveValidateSplCharacter(str,'FirstName');

	str=nlapiGetFieldValue('custrecord_complastname');
	sresult=sresult+saveValidateSplCharacter(str,'LastName');

	str=nlapiGetFieldValue('custrecord_compjobtitle');
	sresult=sresult+saveValidateSplCharacter(str,'JobTitle');

	str=nlapiGetFieldValue('custrecord_compaddr');
	sresult=sresult+saveValidateSplCharacter(str,'Address');

	str=nlapiGetFieldValue('custrecord_comp_acc_no');
	sresult=sresult+saveValidateNumeric(str,"Account No");

	str=nlapiGetFieldValue('custrecord_compphone');
	sresult=sresult+saveValidateNumeric(str,"Phone");

	str=nlapiGetFieldValue('custrecord_compmobile');
	sresult=sresult+saveValidateNumeric(str,"Mobile");

	str=nlapiGetFieldValue('custrecord_compcstno');
	sresult=sresult+saveValidateNumeric(str,"CST No");

	str=nlapiGetFieldValue('custrecord_complstno');
	sresult=sresult+saveValidateNumeric(str,"LST No");

	str=nlapiGetFieldValue('custrecord_compcreditlimit');
	sresult=sresult+saveValidateNumeric(str,"Credit Limit");

	str=nlapiGetFieldValue('custrecord_compzipcode');
	sresult=sresult+saveValidateNumeric(str,"Zip Code");

	str=nlapiGetFieldValue('custrecord_compemailid');
	sresult=sresult+saveValidateEmail(str);

	str=nlapiGetFieldValue('custrecord_compemail');
	sresult=sresult+saveValidateEmail(str);

	if(sresult.trim() != "")
	{		
		alert("This form contains following errors :\n\n"+sresult+"\n Please Rectify these errors and then Submit...!");
		return false;
	}
	else
	{
		/*
		 * Added on 1/4/2011 by Phani
		 * The below variable will fetch the internal id of the record selected.
		 */    

	var vId = nlapiGetFieldValue('id');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', vCompanyName.replace(/\s+$/, ""));
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is','F');

	/*
	 * Added on 1/4/2011 by Phani
	 * The below variable will filter based on the internal id of the record selected.
	 */
	if (vId != null && vId != "") {
		filters[2] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
	}

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, filters);

		if (searchresults) {
			alert("The company already exists");
			return false;
		}
		else
		{
			return true;
		}
	}		
}


/**
 * @param type
 * @param name
 */
function OnChange(type,name)
{
	if(name=="custrecord_company")
	{
		var str=nlapiGetFieldValue('custrecord_company');
		var result=ValidateSplCharacter(str,'company');
		return result;
	}
	if(name=="custrecord_compdescription")
	{
		var str=nlapiGetFieldValue('custrecord_compdescription');
		var result=ValidateSplCharacter(str,'CompanyDesc');
		return result;
	}
	if(name=="custrecord_compcity")
	{
		var str=nlapiGetFieldValue('custrecord_compcity');
		var result=ValidateSplCharacter(str,'City');
		return result;
	}
	if(name=="custrecord_compfirstname")
	{
		var str=nlapiGetFieldValue('custrecord_compfirstname');
		var result=ValidateSplCharacter(str,'FirstName');
		return result;
	}
	if(name=="custrecord_complastname")
	{
		var str=nlapiGetFieldValue('custrecord_complastname');
		var result=ValidateSplCharacter(str,'LastName');
		return result;
	}
	if(name=="custrecord_compjobtitle")
	{
		var str=nlapiGetFieldValue('custrecord_compjobtitle');
		var result=ValidateSplCharacter(str,'JobTitle');
		return result;
	}
	if(name=="custrecord_compaddr")
	{
		var str=nlapiGetFieldValue('custrecord_compaddr');
		var result=ValidateSplCharacter(str,'Address');
		return result;
	}


	if(name=="custrecord_comp_acc_no")
	{
		var str=nlapiGetFieldValue('custrecord_comp_acc_no');
		var result=ValidateNumeric(str);
		return result;
	}
	if(name=="custrecord_compphone")
	{
		var str=nlapiGetFieldValue('custrecord_compphone');
		var result=ValidateNumeric(str);
		return result;
	}
	if(name=="custrecord_compmobile")
	{
		var str=nlapiGetFieldValue('custrecord_compmobile');
		var result=ValidateNumeric(str);
		return result;
	}
	if(name=="custrecord_compcstno")
	{
		var str=nlapiGetFieldValue('custrecord_compcstno');
		var result=ValidateNumeric(str);
		return result;
	}
	if(name=="custrecord_complstno")
	{
		var str=nlapiGetFieldValue('custrecord_complstno');
		var result=ValidateNumeric(str);
		return result;
	}
	if(name=="custrecord_compcreditlimit")
	{
		var str=nlapiGetFieldValue('custrecord_compcreditlimit');
		var result=ValidateNumeric(str);
		return result;
	}
	if(name=="custrecord_compzipcode")
	{
		var str=nlapiGetFieldValue('custrecord_compzipcode');
		var result=ValidateNumeric(str);
		return result;
	}

	if(name=="custrecord_compemailid")
	{
		var str=nlapiGetFieldValue('custrecord_compemailid');
		var result=ValidateEmail(str);
		return result;
	}
	if(name=="custrecord_compemail")
	{
		var str=nlapiGetFieldValue('custrecord_compemail');
		var result=ValidateEmail(str);
		return result;
	}
}


/**
 * @param string
 * @returns {Boolean}
 */
function ValidateSplCharacter(string,name)
{
	var iChars = "*|,\":<>[]{}`\';()@&$#%~!^?/";
	var length=string.length;
	for(var i=0;i<=length-1;i++)
	{
		if(iChars.indexOf(string.charAt(i))!=-1)
		{
			alert("Special Character are not allowed in \n"+name);
			break;
			return false;
		}
	}
	return true;
}

/**
 * This function is to validate for  
 * not entering character into the mobile no ,a/c no and land line no fields. 
 * @param string
 * @returns {Boolean}
 */
function ValidateNumeric(string)
{
	var iChars = "0123456789";
	var length=string.length;
	for(var i=0;i<=length;i++)
	{
		if(iChars.indexOf(string.charAt(i))==-1)
		{
			alert("you may only enter number into this field\n");
			break;
			return false;
		}
	}
	return true;
}


/**
 * @param string
 * @returns {Boolean}
 */
function ValidateEmail(string)
{
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if(reg.test(string) == false) 
	{
		alert("Invalid Email Address");
		return false;
	}
	return true;
}



function saveValidateSplCharacter(string,name)
{
	var iChars = "*|,\":<>[]{}`\';()@&$#%~!^?/";
	var length=string.length;
	for(var i=0;i<=length-1;i++)
	{
		if(iChars.indexOf(string.charAt(i))!=-1)
		{
			//alert(string.charAt(i)+ 'at '+i);
			//alert("Special Character are not allowed in \n"+name);
			//break;
			return "Special Character are not allowed in "+name+"\n";
		}
	}
	return "";
}


function saveValidateNumeric(string,name)
{
	var iChars = "0123456789";
	var length=string.length;
	for(var i=0;i<=length;i++)
	{
		if(iChars.indexOf(string.charAt(i))==-1)
		{
			//alert("you may only enter number into this field\n");
			//break;
			return "You may only enter Numerics in "+name+ "\n";
		}
	}
	return "";
}



function saveValidateEmail(string)
{
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if(string.trim()!="")
	{
		if(reg.test(string) == false) 
		{
			//alert("Invalid Email Address");
			return "Invalid Email Address\n";
		}
	}
	return "";
}

