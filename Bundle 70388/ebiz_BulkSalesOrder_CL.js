/***************************************************************************
	  		   eBizNET
                eBizNET Solutions Inc
****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
****************************************************************************
*
*     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_SalesOrder_CL.js,v $
*     	   $Revision: 1.3 $
*     	   $Date: 2011/06/17 14:46:46 $
*     	   $Author: skota $
*
*   eBizNET version and checksum stamp.  Do not remove.
*   $eBiznet_VER: .............. $eBizNET_SUM: .....
* PRAMETERS
*
*
* DESCRIPTION
*
*  	Default Data for Interfaces
*
* NOTES AND WARNINGS
*
* INITATED FROM
*
* REVISION HISTORY
*$Log: ebiz_SalesOrder_CL.js,v $
*Revision 1.3  2011/06/17 14:46:46  skota
*CASE201112/CR201113/LOG201121
*code changes in function 'CheckFulfillmentqty' for not allowing the user to enter -ve values etc.,
*
*****************************************************************************/
/*
This Script is called when a PUTaway Record is Saved with out a Location.
*/


//Added for Bulk fulfillment purpose
function CheckBulkFulfillmentqty(fld)
{	
	alert('Inside Bulk Fulfillment  function');
	var soBulklineordqty = nlapiGetCurrentLineItemValue('custpage_items','custpage_bulkorder_orderqty');
	var soBulklinefulfillmententeredqty = nlapiGetCurrentLineItemValue('custpage_items','custpage_bulkorder_fulfillmentqty');
	
	
	if(isNaN(soBulklinefulfillmententeredqty) == true)
	{
		alert('Please enter Fulfillment Quantity as a number');		
		return false;
	}
	else if(soBulklinefulfillmententeredqty < 0)
	{
		alert('Fulfillment Quantity cannot be -ve');		
		return false;
	}
	else if(soBulklinefulfillmententeredqty == 0)
	{
		alert('Fulfillment Quantity cannot be 0');		
		return false;
	}
	
	
	/*
	var totfulfillmentqty = parseInt(solinefulfillmentcreatedqty) + parseInt(solinefulfillmententeredqty);
		
	if(totfulfillmentqty > parseInt(solineordqty))
	{
		if(parseInt(solinefulfillmentcreatedqty) > 0)
		{
			solinefulfillmentcreatedqty = solinefulfillmentcreatedqty+"";
			alert('Fulfillment order is already created for qty:' + solinefulfillmentcreatedqty + ', total Fulfillment quantity exceeding Ordered quantity');
		}
		else
			alert('Fulfillment quantity exceeding Ordered quantity');
		return false;
	}
	*/
	
	
	return true;
}

//upto to here


