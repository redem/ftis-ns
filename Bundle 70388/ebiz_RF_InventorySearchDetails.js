/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_InventorySearchDetails.js,v $
 *     	   $Revision: 1.1.2.1.2.7.4.1 $
 *     	   $Date: 2015/10/13 15:48:47 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_31 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * 
 * *
 *****************************************************************************/
function InventorySearchDetails(request, response)
{

	nlapiLogExecution('ERROR', 'Time Stamp at the START',TimeStampinSec());

	var getBinLocationId = request.getParameter('custparam_imbinlocationid');
	var getBinLocation = request.getParameter('custparam_imbinlocationname');
	var getItemId = request.getParameter('custparam_imitemid');
	var getItem = request.getParameter('custparam_imitem');
	var getItemDescId = request.getParameter('custparam_itemDescid');
	var getItemDesc = request.getParameter('custparam_itemDesc');
	var getTotQuantity = '';//request.getParameter('custparam_totquantity');
	var getAvailQuantity = '';//request.getParameter('custparam_availquantity');
	var getLOTId = '';//request.getParameter('custparam_lotid');
	var getLOTNo = '';//request.getParameter('custparam_lot');
	var getStatusId = '';//request.getParameter('custparam_statusid');
	var getStatus = '';//request.getParameter('custparam_status');
	var getLPId = request.getParameter('custparam_lpid');
	var getLP = request.getParameter('custparam_lp');
	var getAllocatdqty = '';

	var getUOM = '';//request.getParameter('custparam_uom');
	var getUOMId = '';//request.getParameter('custparam_uom');
	var getActualBeginDate = request.getParameter('custparam_actualbegindate');
	var getActualBeginTime = request.getParameter('custparam_actualbegintime');
	var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
	var Searchtype = request.getParameter('custparam_searchtype');
	var getInvtRecID='';
	var locationId='';
	var GetNewLp='';
	var getNumber=0;
	var getLP ;
	var ItemDesc='';
	var compId='';
	var fifodate='';
	if(request.getParameter('custparam_number') != null && request.getParameter('custparam_number') != "")
	{
		getNumber=parseInt(request.getParameter('custparam_number'));
	}
	else
	{
		getNumber=0; 
		getLP = request.getParameter('custparam_lp');
	}
	var total;

	var str = 'BinLocation From Invt Move Main. = ' + getBinLocation + '<br>';				
	str = str + 'BinLocation ID From Invt Move Main. = ' + getBinLocationId + '<br>';
	str = str + 'LP From Invt Move Main. = ' + getLP + '<br>';
	str = str + 'getItem. = ' + getItem + '<br>';
	str = str + 'getItemDesc. = ' + getItemDesc + '<br>';
	str = str + 'getNumber. = ' + getNumber + '<br>';
	str = str + 'Searchtype. = ' + Searchtype + '<br>';

	nlapiLogExecution('ERROR', 'Log1', str);



	if((getLP != null && getLP != "")||(getItemId != null && getItemId != "")||(getBinLocationId != null && getBinLocationId != ""))
	{

		nlapiLogExecution('ERROR', 'Into Search Results by LP', getLPId);
		nlapiLogExecution('ERROR', 'Into Search Results by ITEM', getItemId);	
		nlapiLogExecution('ERROR', 'Into Search Results by Binlocation', getBinLocationId);	
		nlapiLogExecution('ERROR', 'Searchtype', Searchtype);	
		//L -- Lp Search
		//B --- Binlocation search
		//I ---- Item search
		/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
		var vRoleLocation=getRoledBasedLocation();
		nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);	

		/* Up to here */ 
		var searchFilters = new Array(); 
		//Case # 20126233� Start
         var errMsg="";
       //Case # 20126233� End
		if(Searchtype =='L')
		{
			nlapiLogExecution('ERROR', 'Searchtype', 'LP');	
			if((getLPId != null && getLPId != ""))
				searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', getLP));
			//Case # 20126233� Start
			errMsg="No Inventory found for this LP";
			//Case # 20126233� End
		}

		if(Searchtype =='B')
		{
			nlapiLogExecution('ERROR', 'Searchtype', 'Location');	
			if((getBinLocationId != null && getBinLocationId != ""))  
				searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', getBinLocationId));
			//Case # 20126233� Start
			errMsg="No Inventory found for this BinLocation";
			//Case # 20126233� End
		}
		if(Searchtype =='I'){
			if((getItemId != null && getItemId != ""))
				searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', getItemId));
			//Case # 20126233� Start
			errMsg="No Inventory found for this Item";
			//Case # 20126233� End
		}

		if(Searchtype =='LB')
		{
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', getLP));
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', getBinLocationId));
			//Case # 20126233� Start
			errMsg="No Inventory found for this LP/Binlocation";
			//Case # 20126233� End

		}
		if(Searchtype =='LI')
		{
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', getLP));
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', getItemId));
			//Case # 20126233� Start
			errMsg="No Inventory found for this LP/Item";
			//Case # 20126233� End

		}

		if(Searchtype =='BI')
		{

			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', getBinLocationId));
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', getItemId));
			//Case # 20126233� Start
			errMsg="No Inventory found for this BinLocation/Item";
			//Case # 20126233� End
		}

		if(Searchtype =='LBI')
		{
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', getLP));
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', getBinLocationId));
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', getItemId));
			//Case # 20126233� Start
			errMsg="No Inventory found for this LP/BinLocation/Item";
			//Case # 20126233� End
		}

		searchFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
		searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

		/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
		if(vRoleLocation != null && vRoleLocation !='' && vRoleLocation !='0')		
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', vRoleLocation));


		/* Up to here */ 

		var searchColumns = new Array();
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));//sitelocation
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_itemdesc'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_company'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));

		var searchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, searchFilters, searchColumns);

		if (searchResults != null && searchResults != '') 
		{
			total=searchResults.length;
			if(parseInt(getNumber)<parseInt(total))
			{
				var recNo=0;
				if(getNumber!=0)
					recNo=parseInt(getNumber);

				var ItemResult = searchResults[recNo];		
				getItem = ItemResult.getText('custrecord_ebiz_inv_sku');
				getItemId = ItemResult.getValue('custrecord_ebiz_inv_sku');
				getInvtRecID = ItemResult.getId();
				locationId = ItemResult.getValue('custrecord_ebiz_inv_loc');//siteLocation
				getBinLocation = ItemResult.getText('custrecord_ebiz_inv_binloc');
				getBinLocationId = ItemResult.getValue('custrecord_ebiz_inv_binloc');
				getStatus = ItemResult.getText('custrecord_ebiz_inv_sku_status');
				getStatusId = ItemResult.getValue('custrecord_ebiz_inv_sku_status');
				getTotQuantity = ItemResult.getValue('custrecord_ebiz_qoh');
				getAllocatdqty = ItemResult.getValue('custrecord_ebiz_alloc_qty');
				GetNewLp=ItemResult.getValue('custrecord_ebiz_inv_lp');
				//nlapiLogExecution('ERROR', 'Log in getTotQuantity', getTotQuantity);
				//nlapiLogExecution('ERROR', 'Log in getAllocatdqty', getAllocatdqty);
				if(getTotQuantity == null || getTotQuantity == '')
					getTotQuantity=0;
				if(getAllocatdqty == null || getAllocatdqty == '')
					getAllocatdqty=0;
				var availqty=parseFloat(getTotQuantity)- parseFloat(getAllocatdqty);
				getAvailQuantity = availqty;
				getLOTNo = ItemResult.getText('custrecord_ebiz_inv_lot');
				getLOTId = ItemResult.getValue('custrecord_ebiz_inv_lot');
				locationId = ItemResult.getValue('custrecord_ebiz_inv_loc');//siteLocation
				ItemDesc=ItemResult.getValue('custrecord_ebiz_itemdesc');
				compId=ItemResult.getValue('custrecord_ebiz_inv_company');
				fifodate=ItemResult.getValue('custrecord_ebiz_inv_fifo');
				getInvtRecID = ItemResult.getId();

				var str = 'Total QOH in Location. = ' + getTotQuantity + '<br>';				
				str = str + 'Allocated Qty. = ' + getAllocatdqty + '<br>';
				str = str + 'Available Qty. = ' + getAvailQuantity + '<br>';
				str = str + 'Invt Rec Id. = ' + getInvtRecID + '<br>';
				str = str + 'FIFO Date. = ' + fifodate + '<br>';
				str = str + 'Item. = ' + getItem + '<br>';
				str = str + 'Record #. = ' + recNo + '<br>';
				str = str + 'GetNewLp. = ' + GetNewLp + '<br>';

				nlapiLogExecution('ERROR', 'Log in Search Results by LP', str);
			}
			else
			{
				var IMovearray=new Array();
				IMovearray["custparam_error"] = 'No Records';
				IMovearray["custparam_screenno"] = 'INVTDTLS';
				IMovearray["custparam_imbinlocationid"]=getBinLocationId;
				IMovearray["custparam_imitemid"]=getItemId;
				IMovearray["custparam_searchtype"]=Searchtype;
				//response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_inventory_search', 'customdeploy_ebiz_rf_inventory_search_di', false, IMovearray);
				return;
			}

		}
		else
		{
			nlapiLogExecution('ERROR', 'INVALID Location', '');	    		
			var IMovearray=new Array();
			//Case # 20126233� Start
			IMovearray["custparam_error"] = errMsg;
			//Case # 20126233� End
			IMovearray["custparam_screenno"] = 'INVTSEAR';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
			return false;
		}
		nlapiLogExecution('ERROR', 'Fetched Location locationId else', locationId);
		nlapiLogExecution('ERROR', 'Fetched Location else', getBinLocation);



	}

	nlapiLogExecution('ERROR', 'GetNewLp', GetNewLp);
	nlapiLogExecution('ERROR', 'ItemDesc', ItemDesc);
	nlapiLogExecution('ERROR', 'getItem Internal Id', getItemId);
	var Itype = nlapiLookupField('item', getItemId, 'recordType');
	var ItemRec = nlapiLoadRecord(Itype, getItemId);

	var Itemdescription='';

	if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
	{	
		Itemdescription = ItemRec.getFieldValue('description');
	}
	else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
	{	
		Itemdescription = ItemRec.getFieldValue('salesdescription');
	}
	nlapiLogExecution('ERROR', 'Itemdescription', Itemdescription);

	nlapiLogExecution('ERROR', 'Time Stamp at the START of GET method',TimeStampinSec());

	if (request.getMethod() == 'GET') 
	{
		var functionkeyHtml=getFunctionkeyScript('_rf_inventorymove_continue');		
		var html = "<html><head><title>INVENTORY DETAILS</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('cmdSearch').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventorymove_continue' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SKU/LOCATION/LP SELECTED:";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + (parseInt(getNumber) + 1) + " OF <label>" + parseInt(total) + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LOCATION :<label>" + getBinLocation + "</label>";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "					<input type='hidden' name='hdnLP' value=" + GetNewLp + ">";
		html = html + "					<input type='hidden' name='hdnLPId' value=" + getLPId + ">";        
		html = html + "					<input type='hidden' name='hdnLOTNo' value=" + getLOTNo + ">";
		html = html + "					<input type='hidden' name='hdnLOTNoId' value=" + getLOTId + ">";
		html = html + "					<input type='hidden' name='hdnStatus' value=" + getStatus + ">";
		html = html + "					<input type='hidden' name='hdnStatusId' value=" + getStatusId + ">";
		html = html + "					<input type='hidden' name='hdnUOM' value=" + getUOM + ">";
		html = html + "					<input type='hidden' name='hdnUOMId' value=" + getUOMId + ">";
		html = html + "					<input type='hidden' name='hdnTotQty' value=" + getTotQuantity + ">";
		html = html + "					<input type='hidden' name='hdnAvailQty' value=" + getAvailQuantity + ">";
		html = html + "					<input type='hidden' name='hdnInvtRecID' value=" + getInvtRecID + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnlocationId' value=" + locationId + ">";
		html = html + "				    <input type='hidden' name='hdncompId' value=" + compId + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + ItemDesc + "'>";
		html = html + "				    <input type='hidden' name='hdnfifodate' value='" + fifodate + "'>";
		html = html + "				    <input type='hidden' name='hdnSearchtype' value='" + Searchtype + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ITEM :<label>" + getItem + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ITEM DESC :<label>" + Itemdescription + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>STATUS :<label>" + getStatus + "</label>";//LOT :<label>" + getLOTNo + "</label> ";
		html = html + "				</td>";
		html = html + "			</tr>";

		var itemSubtype = nlapiLookupField('item', getItemId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
		if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == 'lotnumberedassemblyitem'  || itemSubtype.custitem_ebizbatchlot == 'T')
		{
			nlapiLogExecution('ERROR', 'itemSubtype.recordType', itemSubtype.recordType);
			nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizbatchlot', itemSubtype.custitem_ebizbatchlot);

			html = html + "			<tr>";
			html = html + "				<td align = 'left'>LOT# :<label>" + getLOTNo + "</label>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LP :<label>" + GetNewLp + "</label>"; //UOM :<label>" + getUOM + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>TOTAL QTY :<label>" + getTotQuantity + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>AVAIL QTY :<label>" + getAvailQuantity + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr><td align = 'left'>";
		if(total>1)
		{
			html = html + "				NEXT <input name='cmdnext' id='cmdnext' type='submit' value='F9'/>";
		}
		//	html = html + "				CONTINUE <input name='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				PREV <input name='cmdPreviouscmdSearch' id='cmdPreviouscmdSearch' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdnext').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		nlapiLogExecution('ERROR', 'Time Stamp at the END of GET method',TimeStampinSec());

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('ERROR', 'Time Stamp at the START of POST method',TimeStampinSec());

		nlapiLogExecution('ERROR', 'ItemDesc1', request.getParameter('hdnItemdesc'));
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var IMarray = new Array();

		IMarray["custparam_imbinlocationid"] = request.getParameter('hdnBinLocationId');
		IMarray["custparam_imbinlocationname"] = request.getParameter('hdnBinLocation');
		IMarray["custparam_imitemid"] = request.getParameter('hdnItemId');
		IMarray["custparam_imitem"] = request.getParameter('hdnItem');
		IMarray["custparam_totquantity"] = request.getParameter('hdnTotQty');
		IMarray["custparam_availquantity"] = request.getParameter('hdnAvailQty');  		
		IMarray["custparam_status"] = request.getParameter('hdnStatus');
		IMarray["custparam_statusid"] = request.getParameter('hdnStatusId');
		IMarray["custparam_uom"] = request.getParameter('hdnUOM');
		IMarray["custparam_uomid"] = request.getParameter('hdnUOMId');		
		IMarray["custparam_lot"] = request.getParameter('hdnLOTNo');
		IMarray["custparam_lotid"] = request.getParameter('hdnLOTNoId');
		IMarray["custparam_lp"] = request.getParameter('hdnLP');
		IMarray["custparam_lpid"] = request.getParameter('hdnLPId');
		IMarray["custparam_invtrecid"] = request.getParameter('hdnInvtRecID');
		IMarray["custparam_locationId"] = request.getParameter('hdnlocationId');
		nlapiLogExecution('ERROR', 'custparam_locationId', IMarray["custparam_locationId"]);
		IMarray["custparam_actualbegindate"] = getActualBeginDate;
		IMarray["custparam_itemdesc"] = request.getParameter('hdnItemdesc');
		IMarray["custparam_compid"] = request.getParameter('hdncompId');
		IMarray["custparam_fifodate"] = request.getParameter('hdnfifodate');
		IMarray["custparam_searchtype"] = request.getParameter('hdnSearchtype');

		nlapiLogExecution('ERROR', 'Compid', request.getParameter('hdnSearchtype'));
		nlapiLogExecution('ERROR', 'Compid', request.getParameter('hdncompId'));


		IMarray["custparam_screenno"] = 'INVTDTLS';	


		var itemSubtype = nlapiLookupField('item', request.getParameter('hdnItemId'), ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
		IMarray["custparam_itemtype"] = itemSubtype.recordType;
		nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype.recordType);
		nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
		nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);

		var TimeArray = new Array();
		IMarray["custparam_actualbegintime"] = getActualBeginTime;
		IMarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;

		var optedEvent = request.getParameter('cmdPreviouscmdSearch');
		var optnext=request.getParameter('cmdnext');
		if(optnext=='F9')
		{
			getNumber=getNumber+1;
			IMarray["custparam_number"]= getNumber;
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_inventory_details', 'customdeploy_ebiz_rf_inventory_details_d', false, IMarray);
			//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_main', 'customdeploy_rf_inventory_move_main_di', false, IMarray);
		}
		else if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_inventory_search', 'customdeploy_ebiz_rf_inventory_search_di', false, IMarray);
		}
		else 
		{
			try 
			{    
				nlapiLogExecution('ERROR', 'Hi1');   
				nlapiLogExecution('ERROR', 'itemSubtype.recordType',itemSubtype.recordType);
				nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizbatchlot',itemSubtype.custitem_ebizbatchlot);
				//if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == 'lotnumberedassemblyitem' || itemSubtype.recordType == 'assemblyitem' || itemSubtype.custitem_ebizbatchlot == 'T') 
				if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == 'lotnumberedassemblyitem'  || itemSubtype.custitem_ebizbatchlot == 'T')
				{
					nlapiLogExecution('ERROR', 'Hi2');					
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_batchconf', 'customdeploy_rf_inventory_move_batch_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Navigating to Batch Confirm', 'Success');                      
				}
				else
				{    
					nlapiLogExecution('ERROR', 'Hi3');
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_qty', 'customdeploy_rf_inventory_move_qty_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Navigating to Move Qty', 'Success');
				}
			} 
			catch (e) 
			{
				nlapiLogExecution('ERROR', 'Hi4');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Hi5');
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}

		nlapiLogExecution('ERROR', 'Time Stamp at the END of POST method',TimeStampinSec());
	}

	nlapiLogExecution('ERROR', 'Time Stamp at the END',TimeStampinSec());
}
