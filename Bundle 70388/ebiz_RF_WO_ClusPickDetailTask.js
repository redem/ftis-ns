/***************************************************************************
 eBizNET Solutions
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_ClusPickDetailTask.js,v $
 * $Revision: 1.1.4.4 $
 * $Date: 2015/08/04 14:03:41 $
 * $Author: grao $
 * $Name: t_eBN_2015_1_StdBundle_1_242 $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_ClusPickDetailTask.js,v $
 * Revision 1.1.4.4  2015/08/04 14:03:41  grao
 * 2015.2   issue fixes 20143669
 *
 * Revision 1.1.4.3  2015/04/13 07:41:50  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.4.2  2015/03/02 14:46:38  snimmakayala
 * 201410541
 *
 * Revision 1.1.2.1  2015/02/13 14:34:37  sponnaganti
 * Jawbone Cluster Picking CR Changes
 * CR#  CR14US0323
 *
 * Revision 1.2.2.29.4.9.2.34  2014/04/21 14:02:35  skreddy
 * case # 20148051
 * Sonic SB  issue fix
 *
 * Revision 1.2.2.29.4.9.2.33  2014/03/05 12:03:30  snimmakayala
 * Case #: 20127499
 *
 * Revision 1.2.2.29.4.9.2.32  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.2.2.29.4.9.2.31  2013/12/02 15:13:43  skreddy
 * Case# 20126145
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.2.2.29.4.9.2.30  2013/11/29 15:06:11  grao
 * Case# 20125960  related issue fixes in SB 2014.1
 *
 * Revision 1.2.2.29.4.9.2.29  2013/11/22 14:18:14  schepuri
 * 20125891
 *
 * Revision 1.2.2.29.4.9.2.28  2013/11/20 23:28:13  skreddy
 * Case# 20125850
 * Lost and Found CR for MHP
 * issue fix
 *
 * Revision 1.2.2.29.4.9.2.27  2013/10/29 15:19:36  grao
 * Issue fix related to the 20125392
 *
 * Revision 1.2.2.29.4.9.2.26  2013/10/22 14:01:29  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20124795
 * Replen Priorities
 *
 * Revision 1.2.2.29.4.9.2.25  2013/10/16 14:33:24  schepuri
 * ebn inventory not updating properly
 *
 * Revision 1.2.2.29.4.9.2.24  2013/10/03 13:35:05  grao
 * issue fixes related to the Case#:20124757   ,20124721
 *
 * Revision 1.2.2.29.4.9.2.23  2013/10/01 08:38:40  grao
 * Issue Fix related to 20124722 and 20124721, 20124724
 *
 * Revision 1.2.2.29.4.9.2.22  2013/09/03 15:44:02  skreddy
 * Case# 20124235
 * standard bundle issue fix
 *
 * Revision 1.2.2.29.4.9.2.21  2013/08/29 15:01:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Given NEW SKIP option when there is an invt hold flag
 * against the inventory record
 *
 * Revision 1.2.2.29.4.9.2.20  2013/08/12 15:35:58  nneelam
 * Case# 20123862
 * Standard Bundle Issue Fixed..
 *
 * Revision 1.2.2.29.4.9.2.19  2013/07/25 07:01:30  skreddy
 * Case# 20123468
 * qty exception in cluster picking
 *
 * Revision 1.2.2.29.4.9.2.18  2013/07/24 16:09:27  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Boombahissue
 * case#20123588
 *
 * Revision 1.2.2.29.4.9.2.17  2013/07/24 14:53:39  grao
 * Optmistic locking related issues fixed to DJN
 *
 * Revision 1.2.2.29.4.9.2.16  2013/07/08 14:42:05  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *  Case# 20123339, 20123340, 20123341, 20123342
 * Picking Issues
 *
 * Revision 1.2.2.29.4.9.2.15  2013/06/21 15:05:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *  Case# 20123127
 * Surftech item fulfillment failure
 *
 * Revision 1.2.2.29.4.9.2.14  2013/06/14 07:24:49  mbpragada
 * 20120490
 *
 * Revision 1.2.2.29.4.9.2.13  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.2.29.4.9.2.12  2013/06/11 08:11:19  mbpragada
 * 20120490
 *
 * Revision 1.2.2.29.4.9.2.11  2013/05/31 15:12:25  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.2.2.29.4.9.2.10  2013/05/13 11:19:20  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.2.29.4.9.2.9  2013/05/07 15:37:53  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.2.2.29.4.9.2.8  2013/04/26 15:49:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.2.2.29.4.9.2.7  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.2.29.4.9.2.6  2013/04/16 14:55:56  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.2.29.4.9.2.5  2013/03/21 14:18:14  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.2.29.4.9.2.4  2013/03/19 11:47:21  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.2.2.29.4.9.2.3  2013/03/13 13:57:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.2.2.29.4.9.2.2  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.2.2.29.4.9.2.1  2013/03/01 14:34:59  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.2.2.29.4.9  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.2.2.29.4.8  2012/12/17 15:07:41  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for null
 *
 * Revision 1.2.2.29.4.7  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.29.4.6  2012/10/05 09:51:59  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting Multiple language into given suggestions
 *
 * Revision 1.2.2.29.4.5  2012/10/04 10:31:18  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.2.2.29.4.4  2012/10/02 22:45:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.2.2.29.4.3  2012/09/26 22:43:33  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.2.2.29.4.2  2012/09/26 12:28:38  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.2.29.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.2.29  2012/09/14 20:12:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.2.2.27  2012/09/04 20:46:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Incorporate SKIP option in Cluster Picking.
 *
 * Revision 1.2.2.26  2012/08/27 07:05:07  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * clusterpickingReplen
 *
 * Revision 1.2.2.25  2012/08/27 04:05:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah Issue Fixes
 *
 * Revision 1.2.2.24  2012/08/22 14:39:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stage Location determination for Boombah and Fisk.
 *
 * Revision 1.2.2.22  2012/08/16 11:43:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah UAT Issue Fixes.
 * Cartonization and Customization
 *
 * Revision 1.2.2.21  2012/08/16 07:53:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah UAT Issue Fixes.
 * Cartonization and Customization
 *
 * Revision 1.2.2.20  2012/08/10 14:34:46  gkalla
 * CASE201112/CR201113/LOG201121
 * To restrict entered Container LP to one order
 *
 * Revision 1.2.2.19  2012/08/08 15:14:59  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Kit to order
 *
 * Revision 1.2.2.18  2012/08/08 07:04:51  spendyala
 * CASE201112/CR201113/LOG201121
 * Updating few more fields while overriding the Container Lp.
 *
 * Revision 1.2.2.17  2012/08/07 12:00:02  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new function to update All Container Lp with valid new Container LP entered by User.
 *
 * Revision 1.2.2.16  2012/07/12 07:25:39  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.2.2.15  2012/05/25 22:25:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.2.2.14  2012/05/17 12:12:33  schepuri
 * CASE201112/CR201113/LOG201121
 * modified CONTAINER label with CARTON
 *
 * Revision 1.2.2.13  2012/05/09 07:52:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Partial Status Handling
 *
 * Revision 1.2.2.12  2012/04/27 13:15:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Cluster picking
 *
 * Revision 1.2.2.11  2012/04/20 07:46:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Disable Back and Previous buttons
 *
 * Revision 1.8  2012/04/20 07:43:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Disable Back and Previous buttons
 *
 * Revision 1.7  2012/04/10 13:53:54  spendyala
 * CASE201112/CR201113/LOG201121
 * merged code from branch.
 *
 *
 ****************************************************************************/

function WOClusterPickingDetTask(request, response){

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') {

		nlapiLogExecution('DEBUG', 'Time Stamp at the start of GET',TimeStampinSec());

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12;
//		case no 20125891
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st1 = "LUGAR CANTIDAD:";
			st2 = "N&#218;MERO DE EMPAQUE:";
			st3 = "TAMA&#209;O DEL EMPAQUE:";
			st4 = "INGRESAR / ESCANEAR DEL N&#218;MERO DE EMPAQUE";
			st5 = "INTRODUZCA EL TAMA&#209;O DE";
			st6 = "CANTIDAD RESTANTE:";
			st7 = "CANTIDAD EXCEPCI&#211;N";
			st8 = "ENVIAR";
			st9 = "ANTERIOR";



		}
		else
		{
			st1 = "PLACE QTY :"; 
			st2 = "CARTON NO : ";
			st3 = "CARTON SIZE : ";
			st4 = "ENTER/SCAN CARTON NO ";
			st5 = "ENTER SIZE ";
			st6 = "REMAINING QTY : ";
			st7 = "QTY EXCEPTION ";
			st8 = "SEND ";
			st9 = "PREV";


		}

		var mastLPRequired = getSystemRuleValue('Consider Picking Carton as Master LP');
		if(mastLPRequired==null || mastLPRequired=='')
			mastLPRequired='N';

		var getWaveno = request.getParameter('custparam_waveno');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var pickType=request.getParameter('custparam_picktype');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var detailTask=request.getParameter('custparam_detailtask');
		var serialscanned = request.getParameter('custparam_serialscanned');
		var name=request.getParameter('name');
		var venterzone=request.getParameter('custparam_venterzone');
		nlapiLogExecution('DEBUG', 'venterzone', venterzone);
		var getLPContainerSize='';
		var getQtybyContainer=0;
		var vRemainQty=0;
		var TotalQty=0;

		var str = 'getWaveno. = ' + getWaveno + '<br>';
		str = str + 'vClusterNo. = ' + vClusterNo + '<br>';	
		str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';
		str = str + 'getBeginLocationId. = ' + getBeginLocationId + '<br>';
		str = str + 'getExpectedQuantity. = ' + getExpectedQuantity + '<br>';

		nlapiLogExecution('DEBUG', 'Log Details1', str);

		var SOFilters = new Array();

		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
		SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
		SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
		if(getWaveno != null && getWaveno != "")
		{
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
		}

		if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
		{
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
		}

		if(getItemInternalId != null && getItemInternalId != "")
		{
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
		}

		if(getBeginLocationId != null && getBeginLocationId != "")
		{
			SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', getBeginLocationId));
		}
		
		if(venterzone!=null && venterzone!="" && venterzone!='null')
		{
			nlapiLogExecution('DEBUG', 'getZoneNo inside if', venterzone);
			SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));
		
		}

		var SOColumns = new Array();				
		SOColumns[0] = new nlobjSearchColumn('custrecord_sku',null,'group');
		SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
		SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
		SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');					
		SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
		SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
		SOColumns[7] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
		SOColumns[8] = new nlobjSearchColumn('custrecord_container',null,'group');
		SOColumns[9] = new nlobjSearchColumn('name',null,'group');

		SOColumns[0].setSort();	//SKU
		SOColumns[2].setSort();	//Location
		SOColumns[7].setSort();//Container LP

		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults);
		if (SOSearchResults != null && SOSearchResults.length > 0) {
			getContainerLpNo = SOSearchResults[0].getValue('custrecord_container_lp_no',null,'group');
			getLPContainerSize = SOSearchResults[0].getText('custrecord_container',null,'group');
			getQtybyContainer = SOSearchResults[0].getValue('custrecord_expe_qty',null,'sum');	
			name = SOSearchResults[0].getValue('name',null,'group');
			nlapiLogExecution('DEBUG', 'name inside', name);
			TotalQty = SOSearchResults[0].getValue('custrecord_expe_qty',null,'sum');
			nlapiLogExecution('DEBUG', 'TotalQty', TotalQty);
			nlapiLogExecution('DEBUG', 'getQtybyContainer', getQtybyContainer);
			vRemainQty=parseInt(getExpectedQuantity)-parseInt(getQtybyContainer);
			nlapiLogExecution('DEBUG', 'vRemainQty', vRemainQty);
		}

		//	vRemainQty=parseFloat(getExpectedQuantity)-parseFloat(getQtybyContainer);
		nlapiLogExecution('DEBUG', 'vRemainQty new', vRemainQty);
		//Case # Start 20125392ï¿½Restrict the same screen if remianing qty is zero
		/*if(vRemainQty==0 || vRemainQty==null || vRemainQty=='')
		vRemainQty = request.getParameter('custparam_remainqty');
		nlapiLogExecution('DEBUG', 'vRemainQty new 1', vRemainQty);*/
		//end

		if(vRemainQty==0 || vRemainQty==null || vRemainQty=='' ||vRemainQty=='null' )
			vRemainQty = 0;

		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getSerialNo = request.getParameter('custparam_serialno');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');		
		var RecordCount=request.getParameter('custparam_nooflocrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');	
		var NextItem=request.getParameter('custparam_nextitem');	
		var pickType=request.getParameter('custparam_picktype');

		/*var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var getItem = ItemRec.getFieldValue('itemid');*/

		var getItem="";
		var filter=new Array();
		filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

		var column=new Array();
		column[0]=new nlobjSearchColumn("itemid");

		var searchres=nlapiSearchRecord("item",null,filter,column);
		if(searchres!=null&&searchres!="")
		{
			getItem=searchres[0].getValue("itemid");
		}

		var getOrderNo=request.getParameter('custparam_ebizordno');
		var whLocation = request.getParameter('custparam_whlocation');
		//code added on 15Feb 2012 by suman
		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var EntLotID=request.getParameter('custparam_entLotId');

		//end of code as of 15Feb.
		var getPickQty=0;

		if(getQtybyContainer == "" || getQtybyContainer == null)
		{
			getPickQty=getQtybyContainer;
		}
		/*if(parseInt(getExpectedQuantity)!=parseInt(getQtybyContainer))
			getQtybyContainer=getExpectedQuantity;*/


		//var getDisplayedQty=request.getParameter('custparam_fetchedQty');
		//var getitemname=request.getParameter('custparam_itemname');
		//var getitemno=request.getParameter('custparam_itemno');
		var getReason=request.getParameter('custparam_enteredReason');

		var str = 'getQtybyContainer. = ' + getQtybyContainer + '<br>';
		str = str + 'vRemainQty. = ' + vRemainQty + '<br>';	
		str = str + 'name. = ' + name + '<br>';
		str = str + 'getEnteredLocation. = ' + getEnteredLocation + '<br>';
		str = str + 'getBeginBinLocation. = ' + getBeginBinLocation + '<br>';
		str = str + 'NextLocation. = ' + NextLocation + '<br>';
		str = str + 'NextItemInternalId. = ' + NextItemInternalId + '<br>';
		str = str + 'NextItem. = ' + NextItem + '<br>';

		nlapiLogExecution('DEBUG', 'Log Details2', str);


		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no');  
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		html = html + " document.getElementById('entercontainerno').focus();";     

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>WO#:<label>" + name + "</label> ## WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getQtybyContainer + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getContainerLpNo + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getQtybyContainer + ">";
		html = html + "				<input type='hidden' name='hdnOldExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnRemainQuantity' value=" + vRemainQty + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value='" + getBeginBinLocation + "'>";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnserialno' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";		
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value='" + NextLocation + "'>";
		html = html + "				<input type='hidden' name='hdnNextItem' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnNextItemname' value=" + NextItem + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getLPContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnReason' value=" + getReason + ">";
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getPickQty + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnlocreccount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdndetailTask' value=" + detailTask + ">";
		html = html + "				<input type='hidden' name='hdnserialscanned' value=" + serialscanned + ">";
		html = html + "				<input type='hidden' name='hdntotqty' value=" + TotalQty + ">";

		//code added on 15Feb 2012
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				<input type='hidden' name='hdnEntLotID' value=" + EntLotID + ">";
		html = html + "				<input type='hidden' name='hdnenterzone' value=" + venterzone + ">";
		//End of code.
		//html = html + "				<input type='hidden' name='hdnDisplayedQty' value=" + getDisplayedQty + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>" + getLPContainerSize + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st4 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercontainerno' type='text' value="+getContainerLpNo+" >";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st5;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersize' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 + ": <label>" + vRemainQty + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>" + st8 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>" + st8 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; this.form.cmdPickException.disabled=true;return false'/>";
		html = html + "					" + st9 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "					" + st7 + " <input name='cmdPickException' type='submit' value='F2'/>";
		//html = html + "					SUSPEND <input name='cmdSuspend' type='submit' value='F10'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		nlapiLogExecution('DEBUG', 'Time Stamp at the end of GET',TimeStampinSec());

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st10,st11;

		if( getLanguage == 'es_ES')
		{

			st10 = "CAJA NO V&#193;LIDO #";
			st11 = "CONFIRME LA REPLENS ABIERTAS";



		}
		else
		{

			st10 = "INVALID CARTON #";
			st11 = "CONFIRM THE OPEN REPLENS";		
		}

		var mastLPRequired = getSystemRuleValue('Consider Picking Carton as Master LP');
		if(mastLPRequired==null || mastLPRequired=='')
			mastLPRequired='N';

		nlapiLogExecution('DEBUG', 'Time Stamp at the start of POST',TimeStampinSec());

		var vRemaningqty=0,vDisplayedQty=0,vActqty=0;
		var TotalWeight=0;
		var vCarrier;
		var detailTask=request.getParameter('hdndetailTask');
		var getEnteredContainerNo = request.getParameter('entercontainerno');
		nlapiLogExecution('DEBUG', 'getEnteredContainerNo', getEnteredContainerNo);

		var locreccount=request.getParameter('hdnlocreccount');
		nlapiLogExecution('DEBUG', 'locreccount', locreccount);
		var remqtybycontlp = request.getParameter('hdnRemainQuantity');
		vActqty = request.getParameter('hdnActQty');
		var vReason = request.getParameter('hdnReason');
		var vSite = request.getParameter('hdnwhlocation');
		var vPickType = request.getParameter('hdnpicktype');
		var stgDirection="OUB";
		var vDoname=request.getParameter('hdnName');
		var vdono=request.getParameter('hdnDOLineId');
		var vPrevinvrefno=request.getParameter('hdnInvoiceRefNo');
		var SalesOrderInternalId=request.getParameter('hdnebizOrdNo');

		if(getEnteredContainerNo==null || getEnteredContainerNo=="")
		{
			getEnteredContainerNo=request.getParameter('hdnFetchedContainerLPNo');
		}

		var getFetchedContainerNo = request.getParameter('hdnFetchedContainerLPNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerSize=request.getParameter('hdnContainerSize');

		if(getContainerSize==null || getContainerSize=="")
		{
			getContainerSize = request.getParameter('entersize');
		}			
		var getWaveno = request.getParameter('hdnWaveNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var vbatchno = request.getParameter('hdnbatchno');		
		var ItemNo = request.getParameter('hdnItemInternalId');
		var ItemDesc = request.getParameter('hdnItemDescription');
		var Item = request.getParameter('hdnItem');
		var PickQty = request.getParameter('hdnExpectedQuantity');
		var RcId = request.getParameter('hdnRecordInternalId');
		var EndLocation = request.getParameter('hdnEndLocInternalId');
		var RecCount = request.getParameter('hdnRecCount');
		var NextShowLocation=request.getParameter('hdnNextLocation');
		var NextShowItem=request.getParameter('hdnNextItem');
		var NextShowItemname=request.getParameter('hdnNextItemname');
		var OrdName=request.getParameter('hdnName');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var SerialNo = request.getParameter('hdnserialno');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchno = request.getParameter('hdnbatchno');	

		//code added on 15Feb by suman
		var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		var EntLotID = request.getParameter('hdnEntLotID');
		var venterzone = request.getParameter('hdnenterzone');
		
		if(ExceptionFlag=='L')
		{
			if(EntLocID!=null&&EntLocID!="")
			{
				EndLocation=EntLocID;
				vBatchno = EntLotID;
			}
		}
		//End of code.

		var str = 'BeginLocationName. = ' + BeginLocationName + '<br>';
		str = str + 'EnteredLocationName. = ' + request.getParameter('hdnEnteredLocation') + '<br>';	
		str = str + 'NextShowLocation. = ' + NextShowLocation + '<br>';
		str = str + 'ItemNo. = ' + ItemNo + '<br>';
		str = str + 'NextShowItem. = ' + NextShowItem + '<br>';
		str = str + 'NextShowItemname. = ' + NextShowItemname + '<br>';
		str = str + 'vDoname. = ' + vDoname + '<br>';
		str = str + 'getEnteredContainerNo. = ' + getEnteredContainerNo + '<br>';
		str = str + 'getFetchedContainerNo. = ' + getFetchedContainerNo + '<br>';
		str = str + 'getWaveno. = ' + getWaveno + '<br>';
		str = str + 'vClusterNo. = ' + vClusterNo + '<br>';
		str = str + 'EndLocation. = ' + EndLocation + '<br>';
		str = str + 'vBatchno. = ' + vBatchno + '<br>';
		str = str + 'venterzone. = ' + venterzone + '<br>';

		nlapiLogExecution('DEBUG', 'Log Details1 in POST', str);

		var Systemrules = SystemRuleForStockAdjustment();
		nlapiLogExecution('ERROR', 'Systemrules', Systemrules);
		if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
			Systemrules=null;
		//ItemFulfillment(getWaveNo, RecordInternalId, ContainerLPNo, PickQty, BeginLocation, Item, ItemDesc, ItemNo, vdono, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo, SerialNo,vBatchno);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var serialscanned = request.getParameter('hdnserialscanned');
		var totOrdqty = request.getParameter('hdntotqty');
		nlapiLogExecution('DEBUG', 'totOrdqty', totOrdqty);

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_error"] = st10;//'INVALID CARTON #';
		SOarray["custparam_screenno"] = 'CLWO4';
		SOarray["custparam_detailtask"] = request.getParameter('hdndetailTask');
		SOarray["custparam_serialscanned"] = request.getParameter('hdnserialscanned');
		SOarray["custparam_nooflocrecords"] = locreccount;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');	
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_serialno"] = request.getParameter('hdnserialno');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = vbatchno;
		SOarray["custparam_nextlocation"] = NextShowLocation;
		SOarray["custparam_nextiteminternalid"] = NextShowItem;
		SOarray["custparam_nextitem"] = NextShowItemname;
		SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_remainqty"] = remqtybycontlp;

		SOarray["name"] = OrdName;
//		case 20123468 :start
		SOarray["custparam_EntLoc"] = EntLoc;
		SOarray["custparam_EntLocRec"] = EntLocRec;
		SOarray["custparam_EntLocId"] = EntLocID;
		SOarray["custparam_Exc"] = ExceptionFlag;
		SOarray["custparam_entLotId"] = EntLotID; 
		SOarray["custparam_venterzone"]=request.getParameter('hdnenterzone');
//		end

		var itemSubtype = nlapiLookupField('item', ItemNo, ['recordType', 'custitem_ebiz_captureattribute']);
		//var itemattribute = itemSubtype.custitem_ebiz_captureattribute;
		//nlapiLogExecution('DEBUG', 'itemattribute', itemattribute);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.

		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (optedEvent == 'F7') 
				{
					var prevscreen='';
					nlapiLogExecution('DEBUG', 'SOarray["custparam_prevscreen"]', request.getParameter("custparam_prevscreen"));
					if(request.getParameter('custparam_prevscreen')!=null && request.getParameter('custparam_prevscreen')!='')
					{
						prevscreen=request.getParameter('custparam_prevscreen');
					}
					nlapiLogExecution('DEBUG', 'prevscreen', prevscreen);
					if(prevscreen=='ItemScan')
					{
						response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_summtask', 'customdeploy_rf_wocluspicking_summtask', false, SOarray);
					}

				}
				else if (request.getParameter('cmdPickException') == 'F2') 
				{
					nlapiLogExecution('DEBUG', 'Navigating to Cluster Pick Qty Exception');
					response.sendRedirect('SUITELET', 'customscript_rf_wo_cluspicking_qtyexcep', 'customdeploy_rf_wo_cluspicking_qtyexcp_d', false, SOarray);
				}
				else
				{
					var contlp=request.getParameter('entercontainerno');
					if(contlp==null || contlp=='')
					{
						SOarray["custparam_error"]='ENTER CARTON NO';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					var openreplns = new Array();
					nlapiLogExecution('DEBUG', 'Time Stamp at the start of getOpenReplns',TimeStampinSec());
					openreplns=getOpenReplns(ItemNo,EndLocInternalId);
					nlapiLogExecution('DEBUG', 'Time Stamp at the end of getOpenReplns',TimeStampinSec());
					openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemNo);
					nlapiLogExecution('DEBUG', 'Time Stamp at the end of getQOHForAllSKUsinPFLocation',TimeStampinSec());
					var invtqty;
					if(openinventory!=null && openinventory!='')
					{
						invtqty=openinventory[0].getValue('custrecord_ebiz_alloc_qty',null,'sum');

						if(invtqty==null || invtqty=='')
						{
							invtqty=0;
						}
					}
					nlapiLogExecution('ERROR', 'detailTask', detailTask);
					nlapiLogExecution('ERROR', 'serialscanned', serialscanned);
					/*var itemSubtype = nlapiLookupField('item', ItemNo, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);
			nlapiLogExecution('ERROR', 'itemSubtype', itemSubtype);*/
					var rectype="";
					var serin="";
					var serout="";
					var vbatchlot="";

					nlapiLogExecution('ERROR', 'ItemNo', ItemNo);
					var filter=new Array();
					filter[0]=new nlobjSearchFilter("internalid",null,"anyof",ItemNo);

					var column=new Array();
					column[0]=new nlobjSearchColumn("type");
					column[1]=new nlobjSearchColumn("custitem_ebizserialin");
					column[2]=new nlobjSearchColumn("custitem_ebizserialout");
					column[3]=new nlobjSearchColumn("custitem_ebizbatchlot");

					var searchres=nlapiSearchRecord("item",null,filter,column);
					if(searchres!=null&&searchres!="")
					{
						rectype=searchres[0].getText("type").toLowerCase();
						rectype=rectype.replace(/\s/g, "");
						serin=searchres[0].getValue("custitem_ebizserialin");
						serout=searchres[0].getValue("custitem_ebizserialout");
						vbatchlot=searchres[0].getValue("custitem_ebizbatchlot");
					}

					if(detailTask !='' && detailTask !='F' && (serialscanned == null || serialscanned == ''))
					{
						if (rectype == 'serializedinventoryitem' || serout == 'T') {

							SOarray["custparam_number"] = 0;
							SOarray["custparam_RecType"] = rectype;
							SOarray["custparam_SerOut"] = serout;
							SOarray["custparam_SerIn"] = serin;
							response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
							return;
						}
					} 


					nlapiLogExecution('DEBUG', 'PickQty', PickQty);
					nlapiLogExecution('DEBUG', 'invtqty', invtqty);
					if((parseFloat(PickQty)>parseFloat(invtqty)) && (openreplns!=null && openreplns!=''))
					{
						if(openreplns!=null && openreplns!='')
						{
							for(var i=0; i< openreplns.length;i++)
							{

								var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
								var expqty=openreplns[i].getValue('custrecord_expe_qty');
								if(expqty == null || expqty == '')
								{
									expqty=0;
								}	
								nlapiLogExecution('DEBUG', 'expqty',expqty);
								nlapiLogExecution('DEBUG', 'PickQty',PickQty);

								nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');

							}

							var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
							if(skiptask==null || skiptask=='')
							{
								skiptask=1;
							}
							else
							{
								skiptask=parseFloat(skiptask)+1;
							}

							nlapiLogExecution('DEBUG', 'skiptask',skiptask);

							nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);	
							var SOarray=new Array();
							nlapiLogExecution('DEBUG', 'Time Stamp at the start of buildTaskArray',TimeStampinSec());
							SOarray=buildTaskArray(getWaveno,vClusterNo,OrdName,SalesOrderInternalId,null);
							nlapiLogExecution('DEBUG', 'Time Stamp at the end of buildTaskArray',TimeStampinSec());
							//SOarray["custparam_skipid"] = vSkipId;
							if(NextShowLocation!=null && NextShowLocation!='')
								SOarray["custparam_screenno"] = 'CL4EXP';
							else
								SOarray["custparam_screenno"] = 'CL4LOCEXP';	
							SOarray["custparam_error"]=st11;//'CONFIRM THE OPEN REPLENS';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
							//}
						}
					}
					else
					{
						if (getEnteredContainerNo==''||getEnteredContainerNo==null||getEnteredContainerNo.replace(/\s+$/,"")== getFetchedContainerNo)
						{
							nlapiLogExecution('DEBUG', 'getEnteredContainerNo = getFetchedContainerNo');

							var str = 'remqtybycontlp. = ' + remqtybycontlp + '<br>';
							str = str + 'getItem. = ' + ItemNo + '<br>';	
							str = str + 'locreccount. = ' + locreccount + '<br>';
							str = str + 'BeginLocationName. = ' + BeginLocationName + '<br>';
							str = str + 'NextShowLocation. = ' + NextShowLocation + '<br>';
							str = str + 'ItemNo. = ' + ItemNo + '<br>';
							str = str + 'NextShowItem. = ' + NextShowItem + '<br>';
							str = str + 'NextShowItemname. = ' + NextShowItemname + '<br>';
							str = str + 'OrdName. = ' + OrdName + '<br>';

							nlapiLogExecution('DEBUG', 'Log Details2 in POST', str);

							nlapiLogExecution('DEBUG', 'Time Stamp at the start of ConfirmPickTasks',TimeStampinSec());
							nlapiLogExecution('DEBUG', 'totOrdqty', totOrdqty);

							var vResults= ConfirmPickTasks(vClusterNo,getFetchedContainerNo,ItemNo,BeginLocation,EndLocation,getFetchedContainerNo,SerialNo,
									PickQty,totOrdqty,vBatchno,ExceptionFlag,EntLocRec,Systemrules,mastLPRequired);
							if(vResults != null && vResults.indexOf('ERROR') != -1)
							{
								SOarray["custparam_error"] = 'Insufficient Inventory.';
								SOarray["custparam_screenno"] = 'CLWO4';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('Debug', 'Error: ', 'Insufficient Inventory');
								return;
							}
							nlapiLogExecution('DEBUG', 'Time Stamp at the end of ConfirmPickTasks',TimeStampinSec());
							if(parseFloat(remqtybycontlp)>0)
							{
								/*if(itemattribute == 'T')
								{
									nlapiLogExecution('ERROR', 'Navigating To11', 'Item Attribute1');
									SOarray["custparam_expectedquantity"] = parseFloat(remqtybycontlp);														
									SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
									SOarray["custparam_redirectscreen"] = "DETTASK";
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
								}
								else*/
								{
									SOarray["custparam_expectedquantity"] = parseFloat(remqtybycontlp);
									nlapiLogExecution('ERROR', 'Navigating To', 'Same Screen');
									/* The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
									SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
									response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_dettask', 'customdeploy_rf_wocluspicking_dettask', false, SOarray);
								}
							}

							else if(parseFloat(locreccount) > 1)
							{					
								if(BeginLocationName != NextShowLocation)
								{ 
									/*if(itemattribute == 'T')
									{
										nlapiLogExecution('DEBUG', 'Navigating To1', 'Item Attribute1');
										SOarray["custparam_beginlocationname"]= NextShowLocation;
										SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
										SOarray["custparam_redirectscreen"] = "LOC";
										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
									}
									else*/
									{
										SOarray["custparam_beginlocationname"]= NextShowLocation;

										nlapiLogExecution('DEBUG', 'Navigating To', 'Picking Location');
										/* The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
										SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
										response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_location', 'customdeploy_rf_wocluspicking_location', false, SOarray);
									}
								}
								else if((BeginLocationName == NextShowLocation) && ItemNo != NextShowItem)
								{ 
									/*if(itemattribute == 'T')
									{
										nlapiLogExecution('DEBUG', 'Navigating To2', 'Item Attribute2');
										SOarray["custparam_item"] = NextShowItem;
										SOarray["custparam_iteminternalid"] = NextShowItem;
										SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
										SOarray["custparam_redirectscreen"] = "ITEM";
										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
									}
									else*/
									{
										//SOarray["custparam_item"] = NextShowItemname;
										SOarray["custparam_item"] = NextShowItem;
										SOarray["custparam_iteminternalid"] = NextShowItem;
										SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

										nlapiLogExecution('DEBUG', 'Navigating To', 'Picking Item');
										response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
									}
								}						
								else if((BeginLocationName == NextShowLocation) && ItemNo == NextShowItem)
								{ 
									var SOFilters = new Array();

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
									SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
									SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
									if(getWaveno != null && getWaveno != "")
									{
										nlapiLogExecution('DEBUG', 'WaveNo inside If', getWaveno);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
									}

									if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
									{
										nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
									}
									
									if(venterzone!=null && venterzone!="" && venterzone!='null')
									{
										nlapiLogExecution('DEBUG', 'getZoneNo inside if', venterzone);
										SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));
									
									}

									var SOColumns = new Array();

									SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
									SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
									SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
									SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
									SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
									SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
									SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
									SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
									SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
									SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');

									SOColumns[0].setSort(); 	//Location Pick Sequence
									SOColumns[1].setSort();		//SKU
									SOColumns[2].setSort();		//Location

									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									if (SOSearchResults != null && SOSearchResults.length > 0) {

										var SOSearchResult = SOSearchResults[0];
										var beginlocname = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
										var itemintrid = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

										nlapiLogExecution('ERROR', 'SOSearchResults length 11', SOSearchResults.length);
										//nlapiLogExecution('ERROR', 'SOSearchResults getid 11', SOSearchResults[0].getId());
										if(SOSearchResults.length > 1)
										{
											var SOSearchnextResult = SOSearchResults[1];
											//SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
											SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
											SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no',null,'group');
											SOarray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');									
										}

										if(BeginLocationName != beginlocname)
										{ 
											/*if(itemattribute == 'T')
											{
												nlapiLogExecution('ERROR', 'Navigating To3', 'Item Attribute3');
												nlapiLogExecution('ERROR', 'custparam_recordinternalid To3', SOarray["custparam_recordinternalid"]);
												SOarray["custparam_beginlocationname"]= beginlocname;
												SOarray["custparam_redirectscreen"] = "LOC";
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											}
											else*/
											{
												SOarray["custparam_beginlocationname"]= beginlocname;

												nlapiLogExecution('ERROR', 'Navigating To', 'Picking Location');
												response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_location', 'customdeploy_rf_wocluspicking_location', false, SOarray);
											}
										}
										else if((BeginLocationName == beginlocname) && ItemNo != itemintrid)
										{ 
											/*if(itemattribute == 'T')
											{
												nlapiLogExecution('ERROR', 'Navigating To4', 'Item Attribute4');
												SOarray["custparam_item"] = itemintrid;
												SOarray["custparam_iteminternalid"] = itemintrid;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
												SOarray["custparam_redirectscreen"] = "ITEM";
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											}
											else*/
											{
												SOarray["custparam_item"] = itemintrid;
												SOarray["custparam_iteminternalid"] = itemintrid;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

												nlapiLogExecution('ERROR', 'Navigating To', 'Picking Item');
												response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
											}
										}
										else
										{
											/*if(itemattribute == 'T')
											{
												nlapiLogExecution('ERROR', 'Navigating To5', 'Item Attribute5');
												SOarray["custparam_item"] = itemintrid;
												SOarray["custparam_iteminternalid"] = itemintrid;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
												SOarray["custparam_redirectscreen"] = "ITEM";
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											}
											else*/
											{
												SOarray["custparam_item"] = itemintrid;
												SOarray["custparam_iteminternalid"] = itemintrid;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

												nlapiLogExecution('ERROR', 'Navigating To Else', 'Picking Item');
												response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
											}
										}

									}
									else{

										/*if(itemattribute == 'T')
										{
											nlapiLogExecution('ERROR', 'Navigating To6', 'Item Attribute6');
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
										}
										else*/
										{
											nlapiLogExecution('ERROR', 'Navigating To', 'Stage Location');
											response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_stage_loc', 'customdeploy_rf_wocluspicking_stage_loc', false, SOarray);
										}
									}
								}
							}
							else{

								/*if(itemattribute == 'T')
								{
									nlapiLogExecution('ERROR', 'Navigating To7', 'Item Attribute7');
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
								}
								else*/
								{
									nlapiLogExecution('ERROR', 'Navigating To', 'Stage Location');
									response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_stage_loc', 'customdeploy_rf_wocluspicking_stage_loc', false, SOarray);
								}
							}
						}

						else if (getEnteredContainerNo != '' && getEnteredContainerNo.replace(/\s+$/,"") != getFetchedContainerNo)
						{
							nlapiLogExecution('ERROR', 'getEnteredContainerNo', 'userdefined');
							nlapiLogExecution('ERROR', 'Time Stamp at the start of fnValidateEnteredContLP',TimeStampinSec());
							var vOpenTaskRecs=fnValidateEnteredContLP(getEnteredContainerNo.replace(/\s+$/,""),OrdName);
							nlapiLogExecution('ERROR', 'Time Stamp at the end of fnValidateEnteredContLP',TimeStampinSec());
							//if(vOpenTaskRecs != null && vOpenTaskRecs.length > 0)
							nlapiLogExecution('ERROR', 'vOpenTaskRecs', vOpenTaskRecs);
							if(vOpenTaskRecs!=null && vOpenTaskRecs!='' && parseInt(vOpenTaskRecs)>0)
							{
								nlapiLogExecution('ERROR', 'vOpenTaskRecs.length', vOpenTaskRecs);
								nlapiLogExecution('ERROR', 'test1', 'test1');
								SOarray["custparam_expectedquantity"] = request.getParameter('hdnOldExpectedQuantity');
								SOarray["custparam_error"]  = "This Carton was already used for another Order";
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								//return false;
							}
							else
							{
								nlapiLogExecution('ERROR', 'test3', 'test3');

								var getEnteredContainerNoPrefix = getEnteredContainerNo.substring(0, 3).toUpperCase();
								nlapiLogExecution('ERROR', 'Time Stamp at the start of ebiznet_LPRange_CL_withLPType',TimeStampinSec());
								var LPReturnValue = ebiznet_LPRange_CL_withLPType(getEnteredContainerNo.replace(/\s+$/,""), '2','2',vSite);//'2'UserDefiend,'2'PICK
								nlapiLogExecution('ERROR', 'Time Stamp at the end of ebiznet_LPRange_CL_withLPType',TimeStampinSec());
								if(LPReturnValue == true){
									nlapiLogExecution('ERROR', 'getEnteredContainerNo != getFetchedContainerNo');

									var str = 'remqtybycontlp. = ' + remqtybycontlp + '<br>';
									str = str + 'getItem. = ' + ItemNo + '<br>';	
									str = str + 'locreccount. = ' + locreccount + '<br>';
									str = str + 'BeginLocationName. = ' + BeginLocationName + '<br>';
									str = str + 'NextShowLocation. = ' + NextShowLocation + '<br>';
									str = str + 'ItemNo. = ' + ItemNo + '<br>';
									str = str + 'NextShowItem. = ' + NextShowItem + '<br>';
									str = str + 'NextShowItemname. = ' + NextShowItemname + '<br>';
									str = str + 'OrdName. = ' + OrdName + '<br>';

									nlapiLogExecution('ERROR', 'Log Details2 in POST', str);



									//nlapiLogExecution('ERROR', 'getItem', ItemNo);
									nlapiLogExecution('ERROR', 'Time Stamp at the start of getSKUCubeAndWeightforconfirm',TimeStampinSec());
									var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
									nlapiLogExecution('ERROR', 'Time Stamp at the end of getSKUCubeAndWeightforconfirm',TimeStampinSec());
									var itemCube = 0;
									var itemWeight=0;						
									if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
										itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
										itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//									
									} 
//									nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
//									nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemweight', itemWeight);
//									nlapiLogExecution('ERROR', 'ContainerSize:ContainerSize', getContainerSize);		


									var ContainerCube;					
									var containerInternalId;
									var ContainerSize;
									if(getContainerSize=="" || getContainerSize==null)
									{
										getContainerSize=ContainerSize;
										nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);	
									}	


									nlapiLogExecution('ERROR', 'Time Stamp at the start of getContainerCubeAndTarWeight',TimeStampinSec());
									var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);	
									nlapiLogExecution('ERROR', 'Time Stamp at the end of getContainerCubeAndTarWeight',TimeStampinSec());
									if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

										ContainerCube =  parseFloat(arrContainerDetails[0]);						
										containerInternalId = arrContainerDetails[3];
										ContainerSize=arrContainerDetails[4];
										TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));
									} 
									nlapiLogExecution('ERROR', 'ContainerSizeInternalId', containerInternalId);	

									nlapiLogExecution('ERROR', 'Time Stamp at the start of CreateRFMasterLPRecord',TimeStampinSec());
									CreateRFMasterLPRecord(getEnteredContainerNo,containerInternalId,ContainerCube,itemWeight);
									nlapiLogExecution('ERROR', 'Time Stamp at the end of CreateRFMasterLPRecord',TimeStampinSec());

									//ConfirmPickTasks(vClusterNo,getFetchedContainerNo,ItemNo,BeginLocation,EndLocation,getEnteredContainerNo,SerialNo,vBatchno);
									ConfirmPickTasks(vClusterNo,getFetchedContainerNo,ItemNo,BeginLocation,EndLocation,getEnteredContainerNo,SerialNo,
											PickQty,totOrdqty,vBatchno,ExceptionFlag,EntLocRec,Systemrules,mastLPRequired);

									//added by suman as on 05/08/12.
									//update all the open picks with the new container LP#.
									nlapiLogExecution('ERROR', 'Time Stamp at the start of UpdateAllContainerLp',TimeStampinSec());
									UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SalesOrderInternalId,OrdName);
									nlapiLogExecution('ERROR', 'Time Stamp at the end of UpdateAllContainerLp',TimeStampinSec());
									//end of the code as of 05/08/12.

									nlapiLogExecution('ERROR', 'Time Stamp at the start of updatelastpicktaskforoldcarton',TimeStampinSec());
									updatelastpicktaskforoldcarton(getFetchedContainerNo,SalesOrderInternalId);
									nlapiLogExecution('ERROR', 'Time Stamp at the end of updatelastpicktaskforoldcarton',TimeStampinSec());


									if(parseFloat(remqtybycontlp)>0)
									{
										/*if(itemattribute == 'T')
										{
											nlapiLogExecution('ERROR', 'Navigating To12', 'Item Attribute1');
											SOarray["custparam_expectedquantity"] = parseFloat(remqtybycontlp);															
											SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
											SOarray["custparam_redirectscreen"] = "DETTASK";
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
										}
										else*/
										{
											SOarray["custparam_expectedquantity"] = parseFloat(remqtybycontlp);
											nlapiLogExecution('ERROR', 'Navigating To', 'Same Screen');
											/* The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
											SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
											response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_dettask', 'customdeploy_rf_wocluspicking_dettask', false, SOarray);
										}
									}

									else if(parseFloat(locreccount) > 1)
									{					
										if(BeginLocationName != NextShowLocation)
										{ 
											/*if(itemattribute == 'T')
											{
												nlapiLogExecution('ERROR', 'Navigating To8', 'Item Attribute8');
												SOarray["custparam_beginlocationname"]= NextShowLocation;	
												SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
												SOarray["custparam_redirectscreen"] = "LOC";
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											}
											else*/
											{
												SOarray["custparam_beginlocationname"]= NextShowLocation;
												nlapiLogExecution('ERROR', 'Navigating To', 'Picking Location');
												/* The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
												SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
												response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_location', 'customdeploy_rf_wocluspicking_location', false, SOarray);
											}
										}
										else if((BeginLocationName == NextShowLocation) && ItemNo != NextShowItem)
										{ 
											/*if(itemattribute == 'T')
											{
												nlapiLogExecution('ERROR', 'Navigating To9', 'Item Attribute9');
												SOarray["custparam_item"] = NextShowItem;
												SOarray["custparam_iteminternalid"] = NextShowItem;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
												SOarray["custparam_redirectscreen"] = "ITEM";
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											}
											else*/
											{
												//SOarray["custparam_item"] = NextShowItemname;
												SOarray["custparam_item"] = NextShowItem;
												SOarray["custparam_iteminternalid"] = NextShowItem;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

												nlapiLogExecution('ERROR', 'Navigating To', 'Picking Item');
												response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
											}
										}
										else if((BeginLocationName == NextShowLocation) && ItemNo == NextShowItem)
										{ 
											var SOFilters = new Array();

											SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
											SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));			//	Task Type - PICK
											SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
											SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
											if(getWaveno != null && getWaveno != "")
											{
												nlapiLogExecution('ERROR', 'WaveNo inside If', getWaveno);

												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
											}

											if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
											{
												nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);

												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
											}
											
											if(venterzone!=null && venterzone!="" && venterzone!='null')
											{
												nlapiLogExecution('DEBUG', 'getZoneNo inside if', venterzone);
												SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));
											
											}

											var SOColumns = new Array();

											SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
											SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
											SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
											SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
											SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
											SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
											SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
											SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
											SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
											SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
											SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');

											SOColumns[0].setSort(); 	//Location Pick Sequence
											SOColumns[1].setSort();		//SKU
											SOColumns[2].setSort();		//Location

											var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
											if (SOSearchResults != null && SOSearchResults.length > 0) {

												var SOSearchResult = SOSearchResults[0];
												var beginlocname = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
												var itemintrid = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

												if(SOSearchResults.length > 1)
												{
													var SOSearchnextResult = SOSearchResults[1];
													SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
													SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no',null,'group');
													SOarray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');									
												}

												if(BeginLocationName != beginlocname)
												{ 
													/*if(itemattribute == 'T')
													{
														nlapiLogExecution('DEBUG', 'Navigating To3', 'Item Attribute3');
														SOarray["custparam_beginlocationname"]= beginlocname;
														SOarray["custparam_redirectscreen"] = "LOC";
														response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
													}
													else*/
													{
														SOarray["custparam_beginlocationname"]= beginlocname;

														nlapiLogExecution('DEBUG', 'Navigating To', 'Picking Location');
														response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_location', 'customdeploy_rf_wocluspicking_location', false, SOarray);
													}
												}
												else if((BeginLocationName == beginlocname) && ItemNo != itemintrid)
												{ 
													/*if(itemattribute == 'T')
													{
														nlapiLogExecution('DEBUG', 'Navigating To4', 'Item Attribute4');
														SOarray["custparam_item"] = itemintrid;
														SOarray["custparam_iteminternalid"] = itemintrid;
														SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
														SOarray["custparam_redirectscreen"] = "ITEM";
														response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
													}
													else*/
													{
														SOarray["custparam_item"] = itemintrid;
														SOarray["custparam_iteminternalid"] = itemintrid;
														SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

														nlapiLogExecution('ERROR', 'Navigating To', 'Picking Item');
														response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
													}
												}	

											}
											else{

											/*	if(itemattribute == 'T')
												{
													nlapiLogExecution('DEBUG', 'Navigating To12', 'Item Attribute12');
													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
												}
												else*/
												{
													nlapiLogExecution('DEBUG', 'Navigating To', 'Stage Location');
													response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_stage_loc', 'customdeploy_rf_wocluspicking_stage_loc', false, SOarray);
												}
											}
										}

									}
									else{

										/*if(itemattribute == 'T')
										{
											nlapiLogExecution('DEBUG', 'Navigating To13', 'Item Attribute13');
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
										}
										else*/
										{
											nlapiLogExecution('DEBUG', 'Navigating To', 'Stage Location');
											response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_stage_loc', 'customdeploy_rf_wocluspicking_stage_loc', false, SOarray);
										}
									}
								}
								else  
								{
									nlapiLogExecution('DEBUG', 'Error: ', 'Container # is not in User Defined Range');

									SOarray["custparam_expectedquantity"] = request.getParameter('hdnOldExpectedQuantity');							
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								}
//								nlapiLogExecution('DEBUG', 'getItem', ItemNo);
//								var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
//								var itemCube = 0;
//								var itemWeight=0;						
//								if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
//								itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
//								itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//									
//								} 
//								nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
//								nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);
//								nlapiLogExecution('DEBUG', 'ContainerSize:ContainerSize', getContainerSize);		


//								var ContainerCube;					
//								var containerInternalId;
//								var ContainerSize;
//								if(getContainerSize=="" || getContainerSize==null)
//								{
//								getContainerSize=ContainerSize;
//								nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	
//								}	


//								var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);					
//								if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

//								ContainerCube =  parseFloat(arrContainerDetails[0]);						
//								containerInternalId = arrContainerDetails[3];
//								ContainerSize=arrContainerDetails[4];
//								TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));// added on 28 nov
//								} 
//								nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);				
//								//Insert LP Record
//								CreateRFMasterLPRecord(getEnteredContainerNo,containerInternalId,ContainerCube,itemWeight);

//								//UpdateOpenTask
//								UpdateRFOpenTask(RcId,vActqty, containerInternalId,getEnteredContainerNo,itemCube,itemWeight,EndLocation,vReason,PickQty,IsitLastPick);
//								UpdateRFFulfillOrdLine(vdono,vActqty);

//								//create new record in opentask,fullfillordline when we have qty exception
//								vRemaningqty = PickQty-vActqty;
//								var recordcount=1;//it is iterator in GUI
//								if(PickQty != vActqty)
//								{
//								nlapiLogExecution('DEBUG', 'inexception condition2', 'inexception condition');	
//								CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,vbatchno);//have to check for wt and cube calculation
//								}

//								//if(RecCount > 1)
//								if(opentaskcount > 1)
//								{
//								if(BeginLocation != NextShowLocation)
//								{ 
//								response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
//								nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
//								}
//								else if(ItemNo != NextItemInternalId)
//								{ 
//								response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
//								nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
//								}
//								else if(ItemNo == NextItemInternalId)
//								{ 
//								response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								
//								nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
//								}
//								}
//								else{
//								nlapiLogExecution('DEBUG', 'Before AutoPacking 2 : Sales Order InternalId', SalesOrderInternalId);
//								nlapiLogExecution('DEBUG', 'Navigating To2', 'Stage Location');
//								response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);

//								}
//								}
							}
						}
						else if((getEnteredContainerNo != "" && getEnteredContainerNo != null) || getFetchedContainerNo=="" || getFetchedContainerNo==null)
						{
//							nlapiLogExecution('DEBUG', 'else', 'userdefined');
//							UpdateRFOpenTask(RcId,vActqty, null,null,null,null,EndLocation,vReason,PickQty,IsitLastPick);

//							//create new record in opentask,fullfillordline when we have qty exception
//							vRemaningqty = PickQty-vActqty;
//							var recordcount=1;//it is iterator in GUI
//							if(PickQty != vActqty)
//							{
//							nlapiLogExecution('DEBUG', 'inexception condition3', 'inexception condition');	
//							CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,vbatchno);//have to check for wt and cube calculation
//							}

//							if(opentaskcount > 1)
//							{
//							if(BeginLocation != NextShowLocation)
//							{ 
//							response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
//							nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
//							}
//							else if(ItemNo != NextItemInternalId)
//							{ 
//							response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
//							nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
//							}
//							else if(ItemNo == NextItemInternalId)
//							{ 
//							response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								
//							nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
//							}

//							}
//							else{
//							nlapiLogExecution('DEBUG', 'Before AutoPacking 3 : Sales Order InternalId', SalesOrderInternalId);
//							nlapiLogExecution('DEBUG', 'Navigating To3', 'Stage Location');
//							response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);

//							}
						}
						else //if (getEnteredContainerNo != '' && getEnteredContainerNo == getFetchedContainerNo) 
						{
							nlapiLogExecution('DEBUG', 'Error: ', 'Wave # not found');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);				
						}
					}
				}		
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			SOarray["custparam_screenno"] = 'CLWO1';
			SOarray["custparam_error"] = 'CONTAINER# ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}

	}
}

function getOpenTasksCount(vdono,clusno,containerlpno)
{
	nlapiLogExecution('DEBUG', 'into getOpenTasksCount');

	var str = 'clusno. = ' + clusno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'vdono. = ' + vdono + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	
	if(containerlpno!=null && containerlpno!='')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	
	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', clusno));
	//SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', vdono));

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('DEBUG', 'Out of getOpenTasksCount',openreccount);

	return openreccount;

}

function getOpenTasksCountbyFO(OrdName,clusno,containerlpno)
{
	nlapiLogExecution('DEBUG', 'into getOpenTasksCountbyFO');

	var str = 'clusno. = ' + clusno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'OrdName. = ' + OrdName + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	
	if(containerlpno!=null && containerlpno!='')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	
	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', clusno));
	SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('DEBUG', 'Out of getOpenTasksCountbyFO',openreccount);

	return openreccount;

}


function ConfirmPickTasks(vClusterNo,getFetchedContainerNo,ItemNo,BeginLocation,vEndLocation,getEnteredContainerNo,SerialNo,
		ActPickQty,totOrdqty,vBatchno,ExceptionFlag,EntLocRec,Systemrules,mastLPRequired)
{
	nlapiLogExecution('DEBUG', 'into ConfirmPickTasks function');
	nlapiLogExecution('DEBUG', 'vClusterNo',vClusterNo);
	nlapiLogExecution('DEBUG', 'Time Stamp at the start of ConfirmPickTasks',TimeStampinSec());


	var TotalQty=0;
	if(ActPickQty !=null && ActPickQty!='')
		TotalQty = parseInt(ActPickQty);


	var str = 'vClusterNo. = ' + vClusterNo + '<br>';
	str = str + 'getFetchedContainerNo. = ' + getFetchedContainerNo + '<br>';	
	str = str + 'ItemNo. = ' + ItemNo + '<br>';
	str = str + 'BeginLocation. = ' + BeginLocation + '<br>';
	str = str + 'vEndLocation. = ' + vEndLocation + '<br>';
	str = str + 'TotalQty. = ' + TotalQty + '<br>';
	str = str + 'ActPickQty. = ' + ActPickQty + '<br>';
	str = str + 'vBatchno. = ' + vBatchno + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var SOFilters = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

	if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
	}

	if(ItemNo != null && ItemNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', ItemNo));
	}

	if(BeginLocation != null && BeginLocation != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', BeginLocation));
	}

	if(getFetchedContainerNo != null && getFetchedContainerNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));
	}

	var SOColumns = new Array();				
	SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
	SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
	SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
	SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc');
	SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');					
	SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location');
	SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id');
	SOColumns[7] = new nlobjSearchColumn('custrecord_container_lp_no');
	SOColumns[8] = new nlobjSearchColumn('custrecord_container');
	SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	SOColumns[11] = new nlobjSearchColumn('name');
	SOColumns[12] = new nlobjSearchColumn('custrecord_lpno');
	SOColumns[13] = new nlobjSearchColumn('custrecord_batch_no');

	SOColumns[0].setSort();	//SKU
	SOColumns[2].setSort();	//Location
	SOColumns[7].setSort();//Container LP

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if (SOSearchResults != null && SOSearchResults.length > 0) 
	{	
		for (var i = 0; i < SOSearchResults.length; i++)
		{
			var RcId=SOSearchResults[i].getId();
			var PickQty = SOSearchResults[i].getValue('custrecord_expe_qty');
			var expPickQty = SOSearchResults[i].getValue('custrecord_expe_qty');
			var EndLocation=SOSearchResults[i].getValue('custrecord_actbeginloc');
			var vdono=SOSearchResults[i].getValue('custrecord_ebiz_cntrl_no');
			var SalesOrderInternalId=SOSearchResults[i].getValue('custrecord_ebiz_order_no');
			var vContainerLP=SOSearchResults[i].getValue('custrecord_container_lp_no');
			var FromLp = SOSearchResults[i].getValue('custrecord_lpno');
			var getOrdName = SOSearchResults[i].getValue('name');
			var ItemInternalId = SOSearchResults[i].getValue('custrecord_sku');
			var batchno = SOSearchResults[i].getValue('custrecord_batch_no');
			var opentaskcount=0;

			if(vEndLocation !=null && vEndLocation !='' && vEndLocation != EndLocation)
			{
				EndLocation=vEndLocation;					
			}
			if(vBatchno !=null && vBatchno !='' && vBatchno != batchno)
			{
				batchno = vBatchno;					
			}

			nlapiLogExecution('DEBUG', 'EndLocation',EndLocation);
			nlapiLogExecution('DEBUG', 'batchno',batchno);

			nlapiLogExecution('DEBUG', 'Time Stamp at the start of getOpenTasksCount',TimeStampinSec());
			opentaskcount=getOpenTasksCount(vdono,vClusterNo,getFetchedContainerNo);
			nlapiLogExecution('DEBUG', 'Time Stamp at the end of getOpenTasksCount',TimeStampinSec());

			nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);

			//For fine tuning pick confirmation and doing autopacking in user event after last item
			var IsitLastPick='F';
			/*if(opentaskcount > 1)
				IsitLastPick='F';
			else
				IsitLastPick='T';*/

			nlapiLogExecution('DEBUG', 'ItemInternalId', ItemInternalId);
			if(ItemInternalId!= null && ItemInternalId !='')
			{
//				var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

				var rectype="";
				var serin="";
				var serout="";
				var vbatchlot="";

				var filter=new Array();
				//case 20126145  start : changed varaiable
				filter[0]=new nlobjSearchFilter("internalid",null,"anyof",ItemInternalId);
				//case 20126145  end

				var column=new Array();
				column[0]=new nlobjSearchColumn("type");
				column[1]=new nlobjSearchColumn("custitem_ebizserialin");
				column[2]=new nlobjSearchColumn("custitem_ebizserialout");
				column[3]=new nlobjSearchColumn("custitem_ebizbatchlot");

				var searchres=nlapiSearchRecord("item",null,filter,column);
				if(searchres!=null&&searchres!="")
				{
					rectype=searchres[0].getText("type").toLowerCase();
					rectype=rectype.replace(/\s/g, "");
					serin=searchres[0].getValue("custitem_ebizserialin");
					serout=searchres[0].getValue("custitem_ebizserialout");
					vbatchlot=searchres[0].getValue("custitem_ebizbatchlot");
				}

				if (rectype == 'serializedinventoryitem' || serout == 'T') {
					{
						if(parseInt(totOrdqty)!= parseInt(ActPickQty))
						{
							nlapiLogExecution('DEBUG', 'totOrdqty/ActPickQty', 'is not equall');
							var SerialCnt =SerialNumCount(FromLp,getOrdName);
							if(parseInt(SerialCnt)>0)
							{
								nlapiLogExecution('DEBUG', 'PickQty', PickQty);
								nlapiLogExecution('DEBUG', 'SerialCnt', SerialCnt);
								nlapiLogExecution('DEBUG', 'TotalQty', TotalQty);
								if((parseInt(PickQty)<=parseInt(SerialCnt))&& (parseInt(PickQty)<=parseInt(TotalQty)))
								{
									PickQty=PickQty;
									TotalQty = parseInt(TotalQty)-parseInt(PickQty);
								}
								else
								{
									PickQty=parseInt(TotalQty);
									TotalQty=0;
								}

							}
						}
					}
				}
			}
			nlapiLogExecution('DEBUG', 'PickQty', PickQty);
			var vResults= UpdateRFOpenTask(RcId, PickQty, '', getEnteredContainerNo, '','', EndLocation,'',
					expPickQty,IsitLastPick,ExceptionFlag,EntLocRec,ItemNo,SalesOrderInternalId,batchno,Systemrules,mastLPRequired);
if(vResults != null && vResults != '')
   return vResults;
			//UpdateRFFulfillOrdLine(vdono,PickQty);
			/*if((SerialNo!=null && SerialNo!='') && SerialNo!='null' )
				UpdateSerialentry(RcId,SerialNo);*/
			if(PickQty != expPickQty)
			{
				nlapiLogExecution('Debug', 'inexception condition3', 'inexception condition');	
				CreateNewShortPick(RcId,expPickQty,PickQty,vdono,1,getEnteredContainerNo,1,batchno,null,Systemrules);//have to check for wt and cube calculation
			}
		}		
	}

	nlapiLogExecution('DEBUG', 'out of ConfirmPickTasks function');
	nlapiLogExecution('DEBUG', 'Time Stamp at the end of ConfirmPickTasks',TimeStampinSec());
}

/**
 * 
 * @param RcId
 * @param PickQty
 * @param containerInternalId
 * @param getEnteredContainerNo
 * @param itemCube
 * @param itemWeight
 * @param EndLocation
 * @param vReason
 * @param ActPickQty
 * @param IsItLastPick
 * @param ExceptionFlag
 * @param NewRecId
 */

function UpdateSerialentry(RcId,SerialNo)
{
	nlapiLogExecution('DEBUG', 'RcId', RcId);
	nlapiLogExecution('DEBUG', 'SerialNo', SerialNo);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var  fromLpno=transaction.getFieldValue('custrecord_from_lp_no');
	var  getOrdName=transaction.getFieldValue('name');
	var  getOrdLineNo=transaction.getFieldValue('custrecord_line_no');
	var item=transaction.getFieldValue('custrecord_sku');
	var getEbizOrd=transaction.getFieldValue('custrecord_ebiz_order_no');
	var getContainerLP=transaction.getFieldValue('custrecord_container_lp_no');


	var getSerialNoarr=SerialNo.split(',');
	if(getSerialNoarr.length>0)
	{
		for(var z=0; z< getSerialNoarr.length ;z++)
		{
			var filtersser = new Array();
			filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNoarr[z]);
			if(item !=null && item!='')
				filtersser[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', item);


			var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
			for ( var i = 0; i < SrchRecord.length; i++) {
				var searchresultserial = SrchRecord[i];
				var vserialid = searchresultserial.getId();
			}

			var fields = new Array();
			var values = new Array();
			fields[0] = 'custrecord_serialwmsstatus';
			fields[1] = 'custrecord_serialebizsono';
			fields[2] = 'custrecord_serialsono';
			fields[3] = 'custrecord_serialsolineno';
			fields[4] = 'custrecord_serialparentid';
			fields[5] = 'custrecord_serialitem';
			values[0] = '8';//STATUS.OUTBOUND.PICK_CONFIRMED
			values[1] = getEbizOrd;
			values[2] = getOrdName;
			values[3] = getOrdLineNo;
			values[4] = getContainerLP;
			values[5] = item;

			var updatefields = nlapiSubmitField(
					'customrecord_ebiznetserialentry',
					vserialid, fields, values);

		}
	}


}
function UpdateRFOpenTask(RcId, PickQty, containerInternalId, getEnteredContainerNo, itemCube,
		itemWeight, EndLocation,vReason,ActPickQty,IsItLastPick,ExceptionFlag,NewRecId,ItemNo,SalesOrderInternalId,
		batchno,Systemrules,mastLPRequired)
{
	nlapiLogExecution('DEBUG', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	nlapiLogExecution('DEBUG', 'Time Stamp at the start of UpdateRFOpenTask',TimeStampinSec());
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
	
	//To check for sufficient inv available or not
	var InvttranNew = nlapiLoadRecord('customrecord_ebiznet_createinv', vinvrefno);
	nlapiLogExecution('Debug', 'InvttranNew', InvttranNew);		 
	var InvPresentQOH = InvttranNew.getFieldValue('custrecord_ebiz_qoh');
	if(InvPresentQOH == null || InvPresentQOH == '')
		InvPresentQOH=0;
	if(parseFloat(InvPresentQOH) < parseFloat(PickQty))
		return 'ERROR,No Sufficient Inv';
	//End here
	
	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	if(PickQty!= null && PickQty !="")
		transaction.setFieldValue('custrecord_act_qty', parseFloat(PickQty).toFixed(4));
	if(containerInternalId!= null && containerInternalId !="")
		transaction.setFieldValue('custrecord_container', containerInternalId);
	if(EndLocation!=null && EndLocation!="")
		transaction.setFieldValue('custrecord_actendloc', EndLocation);	
	if(itemWeight!=null && itemWeight!="")
		transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
	if(itemCube!=null && itemCube!="")
		transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
	if(getEnteredContainerNo!=null && getEnteredContainerNo!="" && mastLPRequired!='Y')
		transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
	if(getEnteredContainerNo!=null && getEnteredContainerNo!="" && mastLPRequired=='Y')
		transaction.setFieldValue('custrecord_mast_ebizlp_no', getEnteredContainerNo);
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
	if(vReason!=null && vReason!="")
		transaction.setFieldValue('custrecord_notes', vReason);	
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	nlapiLogExecution('DEBUG', 'IsItLastPick: ', IsItLastPick);
	transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
	var vemployee = request.getParameter('custpage_employee');
	nlapiLogExecution('DEBUG', 'vemployee :', vemployee);
	nlapiLogExecution('DEBUG', 'currentUserID :', currentUserID);

	if(batchno!=null && batchno!="")
		transaction.setFieldValue('custrecord_batch_no', batchno);	
	nlapiLogExecution('DEBUG', 'batchno :', batchno);

	nlapiLogExecution('DEBUG', 'Updating RF Open Task Record Id: ', RcId);
	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('DEBUG', 'Updated RF Open Task successfully');

	if(PickQty != ActPickQty)
	{
		var filters = new Array(); 			 
		filters[0] = new nlobjSearchFilter('custrecord_sku', null, 'is', ItemNo);	
		filters[1] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', RcId);	

		var columns1 = new Array(); 
		columns1[0] = new nlobjSearchColumn('custrecord_parent_sku_no' ); 

		var searchresultskititem = nlapiSearchRecord( 'customrecord_ebiznet_trn_opentask', null, filters, columns1 ); 
		if(searchresultskititem!=null && searchresultskititem!='')
		{
			var vkititem = searchresultskititem[0].getValue('custrecord_parent_sku_no');
			var vkititemtext = searchresultskititem[0].getText('custrecord_parent_sku_no');
		}

		var kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');
		if(kititemTypesku=='kititem')
		{
			nlapiLogExecution('DEBUG', 'vsalesordInternid', SalesOrderInternalId);
			nlapiLogExecution('DEBUG', 'kititem', vkititem);
			var kitfilters = new Array(); 			 
			kitfilters[0] = new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', vkititem);	
			kitfilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);	
			kitfilters[2] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

			var kitcolumns1 = new Array(); 
			kitcolumns1[0] = new nlobjSearchColumn( 'custrecord_expe_qty' ); 			
			kitcolumns1[1] = new nlobjSearchColumn( 'custrecord_act_qty' );

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, kitfilters, kitcolumns1 ); 
			for(var z=0; z<searchresults.length;z++) 
			{										
				var kitrcid=searchresults[z].getId();
				var Expqty=searchresults[z].getValue('custrecord_expe_qty');
				var Actqty=searchresults[z].getValue('custrecord_act_qty');

				nlapiLogExecution('DEBUG', 'Actqty', Actqty);
				nlapiLogExecution('DEBUG', 'Expqty', Expqty);
				if(Actqty==Expqty || Actqty=='')
				{
					var kitopentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', kitrcid);
					kitopentask.setFieldValue('custrecord_kit_exception_flag', 'T');
					nlapiSubmitRecord(kitopentask, false, true);
				}
			}

		}

	}
	nlapiLogExecution('DEBUG', 'ExceptionFlag', ExceptionFlag);
	nlapiLogExecution('DEBUG', 'NewRecId', NewRecId);
	//code added on 15Feb by suman.
	if(ExceptionFlag!='L')
		deleteAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty,SalesOrderInternalId,Systemrules);
	else
	{
		deleteOldAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty,SalesOrderInternalId);
		deleteNewAllocations(NewRecId,PickQty,getEnteredContainerNo,ActPickQty,SalesOrderInternalId);
	}
	nlapiLogExecution('DEBUG', 'Time Stamp at the end of UpdateRFOpenTask',TimeStampinSec());
	//End of code as of 15Feb.
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,ActPickQty,contlpno,expPickQty,SalesOrderInternalId,Systemrules)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);
	nlapiLogExecution('DEBUG', 'Time Stamp at the start of deleteAllocations',TimeStampinSec());

	nlapiLogExecution('Debug', 'Into deleteAllocations (Inv Ref NO)',invrefno);
	var str = 'Inv Ref No. = ' + invrefno + '<br>';
	str = str + 'CARTON LP. = ' + contlpno + '<br>';	
	str = str + 'ActPickQty. = ' + ActPickQty + '<br>';	
	str = str + 'expPickQty. = ' + expPickQty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	str = str + 'Systemrules[0]. = ' + Systemrules[0] + '<br>';	
	str = str + 'Systemrules[1]. = ' + Systemrules[1] + '<br>';	
	nlapiLogExecution('Debug', 'Parameter Values', str);

	try
	{

		var scount=1;
		var invtrecid;
		var newqoh = 0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				var vNewAllocQty=0;
				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
					vNewAllocQty=parseFloat(Invallocqty)- parseFloat(expPickQty);
					if(parseFloat(vNewAllocQty)<0)
						vNewAllocQty=0;
					//	Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(ActPickQty)).toFixed(4));
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(vNewAllocQty));
				}
				else
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

				nlapiLogExecution('Debug', 'vNewAllocQty',vNewAllocQty);


				var remainqty=0;
				//var newqoh = 0;

				if(parseInt(expPickQty) != parseInt(ActPickQty))
				{

					nlapiLogExecution('Debug', 'Systemrules.length', Systemrules.length);
					nlapiLogExecution('Debug', 'Systemrules[0]', Systemrules[0]);
					nlapiLogExecution('Debug', 'Systemrules[1]', Systemrules[1]);

					if(Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
					{

						if(Systemrules[1] =="STND")
							newqoh = parseFloat(InvQOH) - parseFloat(expPickQty);
						else if (Systemrules[1] =="NOADJUST")
							newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
					}
					else if(Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] =="ADJUSTBINLOC")
					{

						newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);

					}
					else
					{
						newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
					}

				}
				else
				{
					newqoh = parseInt(InvQOH) - parseInt(ActPickQty);
				}
				nlapiLogExecution('Debug', 'newqoh',newqoh);
				Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh));  
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}



		if(invtrecid!=null && invtrecid!='' && parseFloat(ActPickQty)>0)
			CreateSTGInvtRecord(invtrecid, contlpno,ActPickQty,SalesOrderInternalId);

		if((parseFloat(newqoh)) <= 0)
		{		
			nlapiLogExecution('ERROR', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in deleteAllocations',exp);
	}		
	/*	try
	{


		Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH) - parseFloat(ActPickQty)).toFixed(4)); 
		Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
		Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		if(invtrecid!=null && invtrecid!='' && parseFloat(pickqty)>0)
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty,SalesOrderInternalId);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		*/
	nlapiLogExecution('DEBUG', 'Time Stamp at the end of deleteAllocations',TimeStampinSec());
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,SalesOrderInternalId) 
{
	nlapiLogExecution('DEBUG', 'Into CreateSTGInvtRecord (Container LP)',vContLp);
	nlapiLogExecution('DEBUG', 'Time Stamp at the start of CreateSTGInvtRecord',TimeStampinSec());

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '36');	//Inventory at WIP 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');		
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', SalesOrderInternalId);	
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('DEBUG', 'Time Stamp at the end of CreateSTGInvtRecord',TimeStampinSec());
	nlapiLogExecution('DEBUG', 'Out of CreateSTGInvtRecord');
}

/**
 * 
 * @param lp
 * @returns
 */
function containerRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
//	var lpmastArr=new Array();
//	if(searchresults != null && searchresults!= "" && searchresults.length>0)
//	{
//	lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
//	lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));

//	}
	return searchresults;
}

function OpenTaskRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', lp));	 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}

function getLPWeight(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	var lpmastArr=new Array();
	nlapiLogExecution('DEBUG', 'Before:',lp); 
	if(searchresults != null && searchresults!= "" && searchresults.length>0)
	{
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));
		nlapiLogExecution('DEBUG', 'lp:',searchresults[0].getValue('custrecord_ebiz_lpmaster_lp')); 
		nlapiLogExecution('DEBUG', 'lpweight:',searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght')); 
	}
	return lpmastArr;
}
function containerSizeExists(lp,SizeId){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	nlapiLogExecution('DEBUG', 'SizeId:',SizeId); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_sizeid', null, 'anyof', SizeId));	
	nlapiLogExecution('DEBUG', 'Size Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);        	       	
	return searchresults;
}
/**
 * 
 * @param vContLpNo
 * @param vContainerSize
 * @param TotalWeight
 * @param ContainerCube
 */
function CreateRFMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,ItemWeight)
{
	var contLPExists = containerRFLPExists(vContLpNo);
	var LPWeightExits = getLPWeight(vContLpNo);
	if(LPWeightExits!=null && LPWeightExits!="")
	{		
		var ContLP=LPWeightExits[0];
		var LPWeight=LPWeightExits[1];
		nlapiLogExecution('DEBUG', 'LPWeight',LPWeight);
	}

	//var ContSizeExits=containerSizeExists(vContLpNo,containerInternalId);

	var TotalWeight;	
	var arrContainerDetails = getContainerCubeAndTarWeight(containerInternalId,"");

	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) 
	{		
		TotalWeight = (parseFloat(ItemWeight) + parseFloat(arrContainerDetails[1]));
	} 

	if(contLPExists==null){		

		var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
		MastLP.setFieldValue('name', vContLpNo);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
		if(containerInternalId!="" && containerInternalId!=null)
		{
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
		}	
		nlapiLogExecution('DEBUG', 'TotalWeight',TotalWeight);
		nlapiLogExecution('DEBUG', 'ContainerCube',ContainerCube);
		if(TotalWeight!="" && TotalWeight!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(4));
		if(ContainerCube!="" && ContainerCube!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));
		//MastLP.setFieldValue('custrecord_ebiz_lpmaster_company', vCompany);

		var currentContext = nlapiGetContext();		        		
		var retktoval = nlapiSubmitRecord(MastLP);
		nlapiLogExecution('DEBUG', 'Master LP Insertion','Success');

	}
	else
	{
		var recordId="";

		if(contLPExists!=null){
//			if(ContSizeExits)
//			{

			for (var i = 0; i < contLPExists.length; i++){
				recordId = contLPExists[i].getId();
				nlapiLogExecution('DEBUG', 'recordId',recordId);		

				var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	
				TotWeight=parseFloat(ItemWeight)+parseFloat(LPWeight);
				nlapiLogExecution('DEBUG', 'itemWeight',ItemWeight);
				nlapiLogExecution('DEBUG', 'LPWeight',LPWeight);
				nlapiLogExecution('DEBUG', 'TotWeight',TotWeight);

				//	MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_sizeid', vContainerSize);
				if(TotWeight!="" && TotWeight!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(4));
				if(ContainerCube!="" && ContainerCube!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));

				var currentContext = nlapiGetContext();	        				
				nlapiSubmitRecord(MastLPUpdate, false, true);        				 
				nlapiLogExecution('DEBUG', 'Master LP Updation','Success');

			}
			//}
//			else
//			{

//			for (var i = 0; i < contLPExists.length; i++){
//			recordId = contLPExists[i].getId();
//			nlapiLogExecution('DEBUG', 'recordId',recordId);		

//			var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	

//			if(containerInternalId!="")
//			{
//			MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
//			}
//			MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotalWeight);
//			MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', ContainerCube);

//			var currentContext = nlapiGetContext();	        				
//			nlapiSubmitRecord(MastLPUpdate, false, true);        				 
//			nlapiLogExecution('DEBUG', 'Master LP Updation','Success');

//			}

//			}

		}
	}
}


function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	try
	{
		nlapiLogExecution('DEBUG', 'Time Stamp at the start of UpdateRFFulfillOrdLine',TimeStampinSec());
		var pickqtyfinal =0;
		nlapiLogExecution('DEBUG', 'into UpdateRFFulfillOrdLine : ', vdono);

		var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);	

		var oldpickQty=doline.getFieldValue('custrecord_pickqty');
		var ordQty=doline.getFieldValue('custrecord_ord_qty');

		if(isNaN(oldpickQty) || oldpickQty == null || oldpickQty == '')
			oldpickQty=0;

		if(isNaN(ordQty) || ordQty == null || ordQty == '')
			ordQty=0;

		nlapiLogExecution('DEBUG', 'parseFloat(oldpickQty) in UpdateRFFulfillOrdLine', parseFloat(oldpickQty));	
		nlapiLogExecution('DEBUG', 'parseFloat(vActqty) in UpdateRFFulfillOrdLine', parseFloat(vActqty));


		pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);
		nlapiLogExecution('DEBUG', 'pickqtyfinal in UpdateRFFulfillOrdLine', pickqtyfinal);	

		doline.setFieldValue('custrecord_upddate', DateStamp());
		doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal).toFixed(4));
		doline.setFieldValue('custrecord_pickgen_qty', parseFloat(pickqtyfinal).toFixed(4));
		if(parseFloat(pickqtyfinal)<parseFloat(ordQty))
			doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
		else
			doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

		nlapiSubmitRecord(doline, false, true);

		nlapiLogExecution('DEBUG', 'Out of UpdateRFFulfillOrdLine ');

		nlapiLogExecution('DEBUG', 'Time Stamp at the start of UpdateRFFulfillOrdLine',TimeStampinSec());
	}
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception UpdateRFFulfillOrdLine ',e);
	}
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteOldAllocations(invrefno,pickqty,contlpno,ActPickQty,SalesOrderInternalId)
{
	/*
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	try
	{
		var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

		if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
			Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(ActPickQty)).toFixed(4));
		}

		//Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH) - parseFloat(ActPickQty)); 
		Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
		Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
	 */

	nlapiLogExecution('ERROR', 'Into deleteOldAllocations (Inv Ref NO)',invrefno);

	try
	{
		var scount=1;
		var invtrecid;

		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {

					if(parseFloat(Invallocqty)- parseFloat(ActPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(ActPickQty)).toFixed(4));
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

		if(invtrecid!=null && invtrecid!='')
			//CreateSTGInvtRecord(invtrecid, contlpno,pickqty);
		 
		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);		
			//case # 20123862 start
		}
		//case # 20123862 end
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}

	nlapiLogExecution('Debug', 'Out of deleteOldAllocations (Inv Ref NO)',invrefno);



}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteNewAllocations(invrefno,pickqty,contlpno,ActPickQty,SalesOrderInternalId)
{
	/*
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	try
	{
		var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');



		Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH) - parseFloat(ActPickQty)).toFixed(4)); 
		Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
		Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		if(invtrecid!=null && invtrecid!='' && parseFloat(pickqty) >0)
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty,SalesOrderInternalId);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
	 */

	nlapiLogExecution('Debug', 'Into deleteNewAllocations (Inv Ref NO)',invrefno);
//	case 20124235 start
	//nlapiLogExecution('Debug', 'actPickQty',actPickQty);
//	end
	try
	{

		var scount=1;
		var invtrecid;
		var InvQOH =0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (InvQOH != null && InvQOH != "" && InvQOH>0) {

					if(parseFloat(InvQOH)- parseFloat(ActPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH) - parseFloat(ActPickQty)).toFixed(4));
					else
						Invttran.setFieldValue('custrecord_ebiz_qoh',0);

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_qoh',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH) - parseFloat(expPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}




		if(invtrecid!=null && invtrecid!='' && parseFloat(pickqty) >0)
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty,SalesOrderInternalId);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
	}	

	nlapiLogExecution('Debug', 'Out of deleteNewAllocations (Inv Ref NO)',invrefno);



}


/**
 * @param getEnteredContainerNo
 * @param getFetchedContainerNo
 * @param SOOrder
 * @param OrdName
 */
function UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SOOrder,OrdName)
{
	try
	{
		nlapiLogExecution('DEBUG','Into UpdateAllContainerLp');
		nlapiLogExecution('DEBUG','getEnteredContainerNo',getEnteredContainerNo);
		nlapiLogExecution('DEBUG','getFetchedContainerNo',getFetchedContainerNo);
		nlapiLogExecution('DEBUG','SOOrder',SOOrder);
		nlapiLogExecution('DEBUG','OrdName',OrdName);

		var context = nlapiGetContext();
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item,waveno,
		beginloc,endloc,lot,actqty,expqty,uomlevel,empid,begindate,begintime,packcode,lp,lineno,Zoneno,ebizLP;

		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);


		var filteropentask=new Array();
		filteropentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
		filteropentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK		
		filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));	
		filteropentask.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

//		if(SOOrder!=null&&SOOrder!="")
//		filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SOOrder)));

//		filteropentask.push(new nlobjSearchFilter('name', null, 'is',OrdName));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_comp_id');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_container');
		columns[7] = new nlobjSearchColumn('custrecord_total_weight');
		columns[8] = new nlobjSearchColumn('custrecord_sku');
		columns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[10] = new nlobjSearchColumn('custrecord_actendloc');
		columns[11] = new nlobjSearchColumn('custrecord_batch_no');
		columns[12] = new nlobjSearchColumn('custrecord_act_qty');
		columns[13] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[14] = new nlobjSearchColumn('custrecord_uom_level');
		columns[15] = new nlobjSearchColumn('custrecord_ebizuser');
		columns[16] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[17] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_lpno');
		columns[20] = new nlobjSearchColumn('custrecord_line_no');
		columns[21] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
		columns[23] = new nlobjSearchColumn('custrecord_ebiz_lpno');


		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filteropentask, columns);
		if(SearchResults!=null&&SearchResults!="")
		{
			nlapiLogExecution('DEBUG','SearchResults.length',SearchResults.length);
			for ( var i = 0; i < SearchResults.length; i++) 
			{

//				var recid=SearchResults[count].getId();
//				var UpdateRec=nlapiSubmitField('customrecord_ebiznet_trn_opentask',recid,'custrecord_container_lp_no',getEnteredContainerNo);
//				nlapiLogExecution('DEBUG','UpdateRec',UpdateRec);

				orderno = SearchResults[i].getValue('name');
				containerno = SearchResults[i].getValue('custrecord_container_lp_no');
				ebizorderno = SearchResults[i].getValue('custrecord_ebiz_order_no');
				ebizcntrlno = SearchResults[i].getValue('custrecord_ebiz_cntrl_no');
				compid = SearchResults[i].getValue('custrecord_comp_id');
				siteid = SearchResults[i].getValue('custrecord_wms_location');
				containersize = SearchResults[i].getValue('custrecord_container');
				totweight = SearchResults[i].getValue('custrecord_total_weight');
				item = SearchResults[i].getValue('custrecord_sku');
				beginloc = SearchResults[i].getValue('custrecord_actbeginloc');
				endloc = SearchResults[i].getValue('custrecord_actendloc');
				lot = SearchResults[i].getValue('custrecord_batch_no');
				actqty = SearchResults[i].getValue('custrecord_act_qty');
				expqty = SearchResults[i].getValue('custrecord_expe_qty');
				uomlevel = SearchResults[i].getValue('custrecord_uom_level');
				empid = SearchResults[i].getValue('custrecord_ebizuser');
				internalid = SearchResults[i].getId();
				begindate=SearchResults[i].getValue('custrecordact_begin_date');
				begintime=SearchResults[i].getValue('custrecord_actualbegintime');
				packcode=SearchResults[i].getValue('custrecord_packcode');
				lp=SearchResults[i].getValue('custrecord_lpno');
				lineno=SearchResults[i].getValue('custrecord_line_no');
				waveno = SearchResults[i].getValue('custrecord_ebiz_wave_no');
				Zoneno = SearchResults[i].getValue('custrecord_ebiz_zoneid');
				ebizLP = SearchResults[i].getValue('custrecord_ebiz_lpno');

				nlapiLogExecution('DEBUG','internalid',internalid);
				newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', internalid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', orderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', waveno);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', endloc);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', lot);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', 9);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', siteid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_level', uomlevel);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', parseFloat(actqty).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', context.getUser());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',begindate);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', begintime);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', empid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', ebizcntrlno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', 3);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', parseFloat(expqty).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', packcode);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', lp);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', ebizorderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', lineno);

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', compid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', getEnteredContainerNo);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', containersize);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', parseFloat(totweight).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid', Zoneno);
				if(ebizLP==null||ebizLP=="")
				{
					nlapiLogExecution('DEBUG','INTO OLD Lp');
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', getFetchedContainerNo);
				}
				newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');


			}
			nlapiSubmitRecord(newParent);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in UpdateAllContianerLp',exp);	
	}
}
function fnValidateEnteredContLP(EnteredContLP,foname)
{
	var retVal = -1;
	nlapiLogExecution('DEBUG', 'Into fnValidateEnteredContLP',EnteredContLP);
	nlapiLogExecution('DEBUG', 'foname',foname);
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	if(foname!=null && foname!='')
		filters.push(new nlobjSearchFilter('name', null, 'isnot', foname));  
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');

	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(salesOrderList != null && salesOrderList != '')
	{
		retVal = salesOrderList.length;
		nlapiLogExecution('DEBUG', 'retVal',retVal);
	}
	else
	{
		var clfilters=new Array();
		clfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_contlp_no', null, 'is', EnteredContLP));
		if(foname!=null && foname!='')
			clfilters.push(new nlobjSearchFilter('name', null, 'isnot', foname));  
		var clcolumns = new Array();
		clcolumns[0] = new nlobjSearchColumn('name');

		var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, clfilters, clcolumns);
		if(salesOrderList != null && salesOrderList != '')
		{
			retVal = salesOrderList.length;
			nlapiLogExecution('DEBUG', 'retVal',retVal);
		}
		else
		{
			retVal = 0;
		}
	}

	nlapiLogExecution('DEBUG', 'Out of fnValidateEnteredContLP',retVal);
	return retVal;
}

function getOpenReplns(item,location)
{
	nlapiLogExecution('DEBUG', 'Into getOpenReplns');
	var PickFaceSearchResults= new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Task Type - RPLN	 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'is', location));	
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_taskpriority');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, PickFaceFilters, PickFaceColumns);



	return PickFaceSearchResults;

}

function getQOHForAllSKUsinPFLocation(location,item){

	nlapiLogExecution('DEBUG','location',location);
	nlapiLogExecution('DEBUG','item',item);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty',null,'sum');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	return pfSKUInvList;
}

function buildTaskArray(WaveNo,ClusNo,OrdNo,vSOId,vZoneId)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if(WaveNo != null && WaveNo != "")
	{
		nlapiLogExecution('DEBUG', 'WaveNo inside If', WaveNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
	}

	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);

		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}

	if(OrdNo!=null && OrdNo!="" && OrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdNo);

		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
	}

	if(vSOId!=null && vSOId!="" && vSOId!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', vSOId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
	}

	if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	SOarray["custparam_ebizordno"] = vSOId;
	var SOColumns = new Array();
	SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
	SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
	SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
	SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
	SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
	SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
	SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
	SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
	SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
	SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
	SOColumns[14] = new nlobjSearchColumn('name');
	SOColumns[15] = new nlobjSearchColumn('custrecord_container');
	SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	SOColumns[17] = new nlobjSearchColumn('custrecord_ebizzone_no');
	SOColumns[18] = new nlobjSearchColumn('custrecord_lpno');

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_clusterno"] = ClusNo;
		SOarray["custparam_picktype"] = 'CL';
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}

function updatelastpicktaskforoldcarton(containerlpno,SOOrder)
{
	try
	{
		nlapiLogExecution('DEBUG', 'Into updatelastpicktaskforoldcarton', containerlpno);
		nlapiLogExecution('DEBUG', 'SOOrder', SOOrder);

		var openreccount=0;

		var SOFilters = new Array();
		var SOColumns = new Array();
		var updatetask='T';

		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

		if(SOOrder!=null&&SOOrder!="")
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SOOrder)));

		if(containerlpno!=null && containerlpno!='' && containerlpno!='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	
		else
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	


		SOColumns[0] = new nlobjSearchColumn('custrecord_wms_status_flag');
		SOColumns[1] = new nlobjSearchColumn('custrecord_device_upload_flag');

		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		{
			for (var i = 0; i < SOSearchResults.length; i++){

				var status  = SOSearchResults[i].getValue('custrecord_wms_status_flag');
				var deviceuploadflag  = SOSearchResults[i].getValue('custrecord_device_upload_flag');

				nlapiLogExecution('DEBUG', 'status', status);
				nlapiLogExecution('DEBUG', 'deviceuploadflag', deviceuploadflag);

				if(status=='9')//Pick Generated
				{
					updatetask='F';
				}
			}
			nlapiLogExecution('DEBUG', 'updatetask', updatetask);

			if(updatetask=='T')
			{
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[0].getId(),'custrecord_device_upload_flag', 'T');
			}
		}

		nlapiLogExecution('DEBUG', 'Out of updatelastpicktaskforoldcarton', updatetask);
	}
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'Exception in updatelastpicktaskforoldcarton', e);
	}
}



function SerialNumCount(contLp,ebizSOno)
{
	nlapiLogExecution('DEBUG', 'contLp', contLp);
	nlapiLogExecution('DEBUG', 'ebizSOno', ebizSOno);
	var SerialNumCount=0;
	var filtersser = new Array();
	if(contLp !=null && contLp!='')
		filtersser[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', contLp);	
	if(ebizSOno !=null && ebizSOno!='')
		filtersser[1] = new nlobjSearchFilter('custrecord_serialsono', null, 'is', ebizSOno);
	var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
	if(SrchRecord !=null && SrchRecord!=''&& SrchRecord.length>0)
	{  SerialNumCount=SrchRecord.length;
	nlapiLogExecution('DEBUG', 'SrchCount', SerialNumCount);

	}
	return SerialNumCount;
}

