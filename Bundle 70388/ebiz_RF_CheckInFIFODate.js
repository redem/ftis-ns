/***************************************************************************
 eBizNET Solutions Inc 
 ***************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_CheckInFIFODate.js,v $
 *     	   $Revision: 1.6.2.7.4.6.2.7 $
 *     	   $Date: 2014/10/17 12:54:44 $
 *     	   $Author: skavuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInFIFODate.js,v $
 * Revision 1.6.2.7.4.6.2.7  2014/10/17 12:54:44  skavuri
 * Case# 201410580 Std bundle Issue fixed
 *
 * Revision 1.6.2.7.4.6.2.6  2014/05/30 00:26:48  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.6.2.7.4.6.2.5  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.6.2.7.4.6.2.4  2013/06/04 07:26:55  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Parsing CR for PCT
 *
 * Revision 1.6.2.7.4.6.2.3  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.6.2.7.4.6.2.2  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.6.2.7.4.6.2.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.6.2.7.4.6  2013/02/07 08:40:04  skreddy
 * CASE201112/CR201113/LOG201121
 * RF Lot auto generating FIFO enhancement
 *
 * Revision 1.6.2.7.4.5  2012/12/03 16:38:32  schepuri
 * CASE201112/CR201113/LOG201121
 * Partial put away generated PO is not displayed in the put away report list
 *
 * Revision 1.6.2.7.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6.2.7.4.3  2012/09/27 10:56:55  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.6.2.7.4.2  2012/09/26 12:47:07  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.6.2.7.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.6.2.7  2012/07/04 07:08:13  spendyala
 * CASE201112/CR201113/LOG201121
 * Invalid parameter type while passing to query string.
 *
 * Revision 1.6.2.6  2012/07/04 07:03:06  spendyala
 * Invalid parameter type while passing to query string.
 *
 * Revision 1.6.2.5  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.6.2.4  2012/03/02 13:55:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue Related to validating Mfg date and ExpDate .
 *
 * Revision 1.6.2.3  2012/02/24 12:23:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing BaseUomQty To Query String.
 *
 * Revision 1.6.2.2  2012/02/22 12:19:53  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.6.2.1  2012/02/10 07:31:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.6  2011/12/30 20:22:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/12/30 14:30:55  spendyala
 * CASE201112/CR201113/LOG201121
 * Added code to validate dates that are entered through screen.
 *
 * Revision 1.4  2011/12/22 22:42:00  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.3  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

/**
 * @param request
 * @param response
 */
function CheckInFIFODate(request, response){
	if (request.getMethod() == 'GET') {

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getItemBaseUomQty=request.getParameter('custparam_baseuomqty');
		/*		var getItemQuantity = request.getParameter('hdnQuantity');
		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');*/
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getMfgDate = request.getParameter('custparam_mfgdate');
		var getExpDate = request.getParameter('custparam_expdate');
		var getBestBeforeDate = request.getParameter('custparam_bestbeforedate');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var ItemShelfLife = request.getParameter('custparam_shelflife');
		var CaptureExpiryDate = request.getParameter('custparam_captureexpirydate');
		var CaptureFifoDate = request.getParameter('custparam_capturefifodate');
		nlapiLogExecution('DEBUG','getWHLocation',getWHLocation);
		nlapiLogExecution('DEBUG','getBatchNo',getBatchNo);
		var getPOLineItemStatusText = request.getParameter('custparam_polineitemstatustext');//Case# 201410580
		var actScannedBatchno = request.getParameter('custparam_actscanbatchno');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "CHECKIN FECHA DE CADUCIDAD";
			st1 = "&#218;LTIMA FECHA DISPONIBLE";
			st2 = "FECHA FIFO";
			st3 = "C&#211;DIGO FIFO";
			st4 = "FORMATO: MMDDAA";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";		
			st7 = "FORMAT : DDMMAA";
		}
		else
		{
			st0 = "CHECKIN EXPIRY DATE";
			st1 = "LAST AVL DATE";
			st2 = "FIFO DATE";
			st3 = "FIFO CODE";
			st4 = "FORMAT : MMDDYY";
			st5 = "SEND";
			st6 = "PREV";
			st7 = "FORMAT : DDMMYY";
		}

		var dtsettingFlag = DateSetting();
		nlapiLogExecution('Debug', 'dtsettingFlag', dtsettingFlag);
		
		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>" + st0 +"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 +":</td>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value='" + getPOLineItemStatus + "'></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnBaseUomQty' value=" + getItemBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemShelfLife' value=" + ItemShelfLife + ">";
		html = html + "				<input type='hidden' name='hdnCaptureExpiryDate' value=" + CaptureExpiryDate + ">";
		html = html + "				<input type='hidden' name='hdnCaptureFifoDate' value=" + CaptureFifoDate + ">";
		html = html + "				<input type='hidden' name='hdnscanbatchno' value=" + actScannedBatchno + ">";
		html = html + "				<input type='hidden' name='hdnitemstatustext' value='" + getPOLineItemStatusText + "'>";//Case# 201410580
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteravldate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 +" :</td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfifodate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 +" : </td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfifocode' type='text'/>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";

		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";*/
		
		
		if(dtsettingFlag == 'DD/MM/YYYY')
		{

			html = html + "			<tr>";
			html = html + "				<td align = 'left'><label>" + st7;
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		else
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><label>" + st4;
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		
		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		try {
			var optedEvent = request.getParameter('cmdPrevious');
			var getLastAvlDate = request.getParameter('enteravldate');
			var getFifoDate = request.getParameter('enterfifodate');
			var getFifoCode = request.getParameter('enterfifocode');
			var error='';
			var errorflag='F';

			var POarray = new Array();

			var getLanguage = request.getParameter('hdngetLanguage');
			POarray["custparam_language"] = getLanguage;
			nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


			var st7,st8,st9;
			if( getLanguage == 'es_ES')
			{
				st7 = "ERROR EN LA FECHA FIFO";
				st8 = "&#218;LTIMA FECHA DEBE SER B / W MFGDATE Y EXPDATE";	
				st9 = "FECHA FIFO DEBE SER B / W MFGDATE Y EXPDATE";
				st10 = "Fecha de la �ltima AVL formato debe ser MMDDAA";
				st11 = "FIFO formato de fecha debe ser MMDDAA";
			}
			else
			{

				st7 = "ERROR IN FIFO DATE";
				st8 = "LAST DATE SHOULD BE B/W MFGDATE AND EXPDATE";
				st9 = "FIFO DATE SHOULD BE B/W MFGDATE AND EXPDATE ";
				st10 = "LAST AVL DATE format should be MMDDYY  ";
				st11 = "FIFO DATE format should be MMDDYY";
			}
			POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
			POarray["custparam_poid"] = request.getParameter('custparam_poid');
			POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
			POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
			POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
			POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
			POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
			POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
			POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode'); //request.getParameter('custparam_polinepackcode');
			POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
			POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
			POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
			POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');

			//code added on 24 feb 2012 by suman
			//To get the baseuom qty .
			POarray["custparam_baseuomqty"] = request.getParameter('hdnBaseUomQty');
			nlapiLogExecution('DEBUG','ITEMINFO[0]',POarray["custparam_itemcube"]);
			nlapiLogExecution('DEBUG','ITEMINFO[1]',POarray["custparam_baseuomqty"]);
			//end of code as of 24 feb 2012.

			POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_error"] = st7;
			POarray["custparam_screenno"] = '3C';
			POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
			POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
			POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
			POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
			POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
			POarray["custparam_shelflife"] = request.getParameter('hdnItemShelfLife');
			POarray["custparam_captureexpirydate"] = request.getParameter('hdnCaptureExpiryDate');
			POarray["custparam_capturefifodate"] = request.getParameter('hdnCaptureFifoDate');
			POarray["custparam_actscanbatchno"] = request.getParameter('hdnscanbatchno');
			POarray["custparam_polineitemstatustext"] = request.getParameter('hdnitemstatustext');//Case# 201410580
			var itemShelflife = request.getParameter('hdnItemShelfLife');
			var CaptureExpirydate = request.getParameter('hdnCaptureExpiryDate');
			var CaptureFifodate = request.getParameter('hdnCaptureFifoDate');
			nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);
			nlapiLogExecution('DEBUG','hdnBatchNo',request.getParameter('hdnBatchNo'));
			// case# 201417094
			var dtsettingFlag = DateSetting();
			nlapiLogExecution('Debug', 'dtsettingFlag', dtsettingFlag);
			
			if (optedEvent == 'F7') {
				nlapiLogExecution('DEBUG', 'CHECK IN FIFO DATE F7 Pressed');
				if(CaptureExpirydate=='T')
					response.sendRedirect('SUITELET', 'customscript_rf_checkin_exp_date', 'customdeploy_rf_checkin_exp_date_di', false, POarray);
				else
					response.sendRedirect('SUITELET', 'customscript_rf_checkin_batch_no', 'customdeploy_rf_checkin_batch_no_di', false, POarray);
			}
			else {
				var ValueLastDate="";
				var ValueFiFoDate="";


				if(getLastAvlDate != '' && getLastAvlDate.length > 6)
				{
					POarray["custparam_error"] = st10;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				else if(getFifoDate != '' && getFifoDate.length > 6)
				{
					POarray["custparam_error"] = st11;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}

				// if (getFifoDate != '' && getFifoCode != '' && getLastAvlDate != '') {
				nlapiLogExecution('DEBUG','Test2','Test2');
				if (getLastAvlDate != '' && getLastAvlDate !=null)
				{
					var Lastdate= RFDateFormat(getLastAvlDate,dtsettingFlag);

					if(Lastdate[0]=='true')
					{
						ValueLastDate=Lastdate[1];
						POarray["custparam_lastdate"]=Lastdate[1];
					}
					else {
						errorflag='T';
						error='LastDate:'+Lastdate[1];
					}
				}
//				POarray["custparam_lastdate"] = RFDateFormat(getLastAvlDate);
//				if (POarray["custparam_fifodate"] == null || POarray["custparam_fifodate"] == "") {
				if (getFifoDate == null || getFifoDate == "") {
					POarray["custparam_fifodate"] = DateStamp();
					ValueFiFoDate=DateStamp();
					/*if (request.getParameter('hdnExpDate') != null || request.getParameter('hdnExpDate') != "") {
						POarray["custparam_fifodate"] = request.getParameter('hdnExpDate');
						ValueFiFoDate=request.getParameter('hdnExpDate');
					}
					else 
						if (request.getParameter('hdnMfgDate') != null || request.getParameter('hdnMfgDate') != "") {
							POarray["custparam_fifodate"] = request.getParameter('hdnMfgDate');
							ValueFiFoDate=request.getParameter('hdnMfgDate');
						}*/
				}
				else {
					nlapiLogExecution('DEBUG','Befor','Before');
					var Fifodate= RFDateFormat(getFifoDate,dtsettingFlag);
					if(Fifodate[0]=='true')
					{
						nlapiLogExecution('DEBUG','Fifodate',Fifodate[1]);
						POarray["custparam_fifodate"]=Fifodate[1];
						ValueFiFoDate=Fifodate[1];
					}
					else {
						errorflag='T';
						error='Fifodate:'+Fifodate[1];
					}
//					POarray["custparam_fifodate"] = RFDateFormat(getFifoDate);
				}
				nlapiLogExecution('DEBUG','DEBUG',error);
				if(errorflag=='F')
				{
					//Modified on 2Mar 2012 by suman.
					//Validating the condn LastDate and FIFODate shld be in b/w MfgDate and ExpDate.
					nlapiLogExecution('DEBUG','Expdate',request.getParameter('hdnExpDate'));
					nlapiLogExecution('DEBUG','Mfgdate',request.getParameter('hdnMfgDate'));
					nlapiLogExecution('DEBUG','ValueLastDate',ValueLastDate);
					nlapiLogExecution('DEBUG','ValueFiFoDate',ValueFiFoDate);
					POarray["custparam_fifocode"] = getFifoCode;
					if (Date.parse(request.getParameter('hdnMfgDate'))>= Date.parse(ValueLastDate)|| Date.parse(ValueLastDate)> Date.parse(request.getParameter('hdnExpDate'))) 
					{
						POarray["custparam_error"] = st8;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
					else if (Date.parse(request.getParameter('hdnMfgDate'))>= Date.parse(ValueFiFoDate)|| Date.parse(ValueLastDate) > Date.parse(request.getParameter('hdnExpDate'))) 
					{
						POarray["custparam_error"] = st9;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					} 
					else if (Date.parse(ValueFiFoDate) >= Date.parse(request.getParameter('hdnExpDate'))) 
					{
						POarray["custparam_error"] = "FIFO Date is greater than ExpDate";
						response.sendRedirect('SUITELET', 'customscript_rf_checkin_fifo_date_confrm', 'customdeploy_rf_checkin_fifo_date_conf_d', false, POarray);
					} 
					else
					{
						//POarray["custparam_fifocode"] = getFifoCode;
						response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
					}
					//End of code as of 2nd Mar
				}
				else
				{
					POarray["custparam_error"] = error;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);

				}
				//                }
				//                else {
				//                    POarray["custparam_error"] = 'FIFO DATE IS NULL';
				//                    response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				//                }
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'Catch: Location not found');

		}
	}
}
