/**
 * @author Phani Attili
 * This Suitelet is meant to display scan picking location Screen
 */
/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingItem.js,v $ 
 *     	   $Revision: 1.13.2.21.4.16.2.19.2.2 $
 *     	   $Date: 2014/08/06 20:56:25 $
 *     	   $Author: grao $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_34 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingItem.js,v $
 * Revision 1.13.2.21.4.16.2.19.2.2  2014/08/06 20:56:25  grao
 * Case#: 20149866 �Standard bdel  issue fixes
 *
 * Revision 1.13.2.21.4.16.2.19.2.1  2014/07/25 07:09:30  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed moved from enhancment branch
 *
 * Revision 1.13.2.21.4.16.2.19  2014/06/13 12:31:53  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.13.2.21.4.16.2.18  2014/05/30 00:41:04  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.13.2.21.4.16.2.17  2014/05/26 07:08:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.13.2.21.4.16.2.16  2014/04/22 16:35:52  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Item receipt
 *
 * Revision 1.13.2.21.4.16.2.15  2013/11/22 08:40:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201216680
 *
 * Revision 1.13.2.21.4.16.2.14  2013/09/06 07:33:46  rmukkera
 * Case# 20124246
 *
 * Revision 1.13.2.21.4.16.2.13  2013/08/06 03:48:12  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201214549
 * Performance tuning
 *
 * Revision 1.13.2.21.4.16.2.12  2013/08/05 06:37:14  schepuri
 * cesium prod location display issue fix
 *
 * Revision 1.13.2.21.4.16.2.11  2013/07/15 17:05:07  skreddy
 * Case# 20123446
 * Item info display CR
 *
 * Revision 1.13.2.21.4.16.2.10  2013/07/08 14:42:05  rrpulicherla
 * Case# 20123340
 * Picking Issues
 *
 * Revision 1.13.2.21.4.16.2.9  2013/06/20 14:13:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 201215543
 *
 * Revision 1.13.2.21.4.16.2.8  2013/06/19 22:56:06  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 *
 * Revision 1.13.2.21.4.16.2.7  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.13.2.21.4.16.2.6  2013/04/19 15:40:01  skreddy
 * CASE201112/CR201113/LOG201121
 * issue fixes
 *
 * Revision 1.13.2.21.4.16.2.5  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.13.2.21.4.16.2.4  2013/04/08 08:47:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.13.2.21.4.16.2.3  2013/03/19 11:47:21  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.13.2.21.4.16.2.2  2013/03/18 06:45:56  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.13.2.21.4.16.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.13.2.21.4.16  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.13.2.21.4.15  2013/01/18 13:45:20  mbpragada
 * 20120490
 *
 * Revision 1.13.2.21.4.14  2012/12/31 05:53:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Inventory Sync issue fixes
 * .
 *
 * Revision 1.13.2.21.4.13  2012/12/21 09:29:26  mbpragada
 * 20120490
 *
 * Revision 1.13.2.21.4.12  2012/12/21 09:05:15  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.13.2.21.4.11  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.13.2.21.4.10  2012/11/11 03:40:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * GSUSA UAT issue fixes.
 *
 * Revision 1.13.2.21.4.9  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.13.2.21.4.8  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.13.2.21.4.7  2012/10/23 17:08:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.13.2.21.4.6  2012/10/22 14:28:45  grao
 * CASE201112/CR201113/LOG201121
 * Fix Undefined issue
 *
 * Revision 1.13.2.21.4.5  2012/10/11 10:27:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick confirmation and other inventory issues
 *
 * Revision 1.13.2.21.4.4  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.13.2.21.4.3  2012/10/04 10:28:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.13.2.21.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.13.2.21.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.13.2.21  2012/08/26 17:55:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Pick Confirm issues for FISK
 *
 * Revision 1.13.2.20  2012/08/07 21:18:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.13.2.19  2012/08/01 07:16:42  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Routing based on Loc group seq
 *
 * Revision 1.13.2.18  2012/07/27 12:59:12  schepuri
 * CASE201112/CR201113/LOG201121
 * Trimming item desc
 *
 * Revision 1.13.2.17  2012/07/19 05:22:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.13.2.16  2012/07/09 06:50:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.13.2.15  2012/06/14 06:59:16  gkalla
 * CASE201112/CR201113/LOG201121
 * Invalid item issue fix
 *
 * Revision 1.13.2.14  2012/05/04 10:16:28  mbpragada
 * CASE201112/CR201113/LOG201121
 * item null error in RF picking
 *
 * Revision 1.13.2.13  2012/04/26 07:10:20  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issue unable to scan the item when we placed two items in a single
 *  location.
 *
 * Revision 1.13.2.12  2012/04/24 13:49:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.13.2.11  2012/04/24 11:25:13  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issues in skip location functionality in RF Picking
 *
 * Revision 1.13.2.10  2012/04/23 13:56:49  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.13.2.9  2012/04/23 13:37:38  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issues in skip location functionality in RF Picking
 *
 * Revision 1.13.2.8  2012/04/19 15:00:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.13.2.7  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.13.2.6  2012/04/17 23:33:36  gkalla
 * CASE201112/CR201113/LOG201121
 * To scan item
 *
 * Revision 1.13.2.5  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.13.2.4  2012/03/09 08:23:17  spendyala
 * CASE201112/CR201113/LOG201121
 * Code Merged from trunk.
 *
 * Revision 1.13.2.3  2012/02/20 15:24:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.13.2.2  2012/02/08 15:27:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * functional keys
 *
 * Revision 1.13.2.1  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.18  2012/01/23 23:21:17  gkalla
 * CASE201112/CR201113/LOG201121
 * RF Picking customized
 *
 * Revision 1.17  2012/01/12 17:56:58  gkalla
 * CASE201112/CR201113/LOG201121
 * To add item description
 *
 * Revision 1.16  2012/01/12 11:00:39  gkalla
 * CASE201112/CR201113/LOG201121
 * Added calling upc code function from generat functions
 *
 * Revision 1.5  2011/07/12 06:39:08  pattili
 * CASE201112/CR201113/LOG201121
 * 1. Changes to RF Putaway SKU and Putaway Location.
 *
 * Revision 1.4  2011/07/12 05:35:41  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function PickingItem(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8;

		if( getLanguage == 'es_ES')
		{
			st1 = "ART&#205;CULO:";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULO";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "OVERRIDE";
			st6 = "SKIP";
			st8 ="DESCRIPCI&#211;N DEL ART&#205;CULO";
		}
		else
		{
			st1 = "ITEM: ";
			st2 = "ENTER/SCAN ITEM ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "OVERRIDE";
			st6 = "SKIP";
			st8 = "ITEM DESCRIPTION:";
		}

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var vClusterno = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');
		var vBatchno = request.getParameter('custparam_batchno');
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');

		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		nlapiLogExecution('DEBUG', 'getnextExpectedQuantity', getnextExpectedQuantity);
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'NextItemId', NextItemId);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		if(getFetchedLocation==null)
		{
			getFetchedLocation=NextLocation;
		}
		if(getItemInternalId==null)
		{
			getItemInternalId=NextItemId;
		}

		var Itemdescription='';

		nlapiLogExecution('DEBUG', 'getItemName', getItemName);	

		if(getItemInternalId!=null && getItemInternalId!='')
		{
			//var Itemtype = nlapiLookupField('item', getItemInternalId, 'recordType');

			var filtersitem = new Array();
			var columnsitem = new Array();

			filtersitem.push(new nlobjSearchFilter('internalid', null, 'is',getItemInternalId));
			filtersitem.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

			columnsitem[0] = new nlobjSearchColumn('description');    
			columnsitem[1] = new nlobjSearchColumn('salesdescription');
			columnsitem[2] = new nlobjSearchColumn('itemid');

			var itemRecord = nlapiSearchRecord("item", null, filtersitem, columnsitem);
			if(itemRecord!=null && itemRecord!='' && itemRecord.length>0)
			{
				if(itemRecord[0].getValue('description') != null && itemRecord[0].getValue('description') != '')
				{
					Itemdescription = itemRecord[0].getValue('description');
				}
				else if(itemRecord[0].getValue('salesdescription') != null && itemRecord[0].getValue('salesdescription') != "")
				{	
					Itemdescription = itemRecord[0].getValue('salesdescription');
				}
				getItemName=itemRecord[0].getValue('itemid');
			}

			Itemdescription = Itemdescription.substring(0, 20);			
		}

		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');

		nlapiLogExecution('DEBUG', 'getItem', getItem);	
		nlapiLogExecution('DEBUG', 'Itemdescription', Itemdescription);
		nlapiLogExecution('DEBUG', 'Next Location', NextLocation);
		nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);

		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getFetchedBeginLocation);

		//  var getFetchedLocation = BinLocationRec.getFieldValue('custrecord_ebizlocname');
		//   nlapiLogExecution('DEBUG', 'Location Name is', getFetchedLocation);

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
	//	html = html + " document.getElementById('enteritem').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		//html = html + "	  alert(node.type);";
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + name + "</label> ## WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";		
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getItemName + "</label><br>"+ st8 +"<label>" + Itemdescription + "</label>";//<br>REMAINING QTY: <label>" + remqty + "</label>";

		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
//		case no Issue: 20123645  
		html = html + "				<input type='hidden' name='hdnBeginLocation' value='" + getFetchedLocation + "'>";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterno + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value='" + getEnteredLocation + "'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationid' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2;//ENTER/SCAN ITEM 
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnnextitem' value=" + NextItemId + ">";	
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";	
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdOverride.disabled=true; this.form.cmdSKIP.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					"+ st5 +" <input name='cmdOverride' type='submit' value='F11'/>";
		html = html + "					"+ st6 +" <input name='cmdSKIP' type='submit' value='F12'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st7,st8;

		if( getLanguage == 'es_ES')
		{
			st7 = "ART&#205;CULO INV&#193;LIDO";
			st8 = "DESCRIPCI&#211;N DEL ART&#205;CULO:";

		}
		else
		{
			st7 = "INVALID ITEM";
			st8 = "ITEM DESCRIPTION: ";
		}
		var getEnteredItem = request.getParameter('enteritem');
		nlapiLogExecution('DEBUG', 'Entered Item', getEnteredItem);
		nlapiLogExecution('DEBUG', 'hdnnext', request.getParameter('hdnnext'));
		nlapiLogExecution('DEBUG', 'hdnnextItemId', request.getParameter('hdnnextitem'));

		var getWaveNo = request.getParameter('hdnWaveNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getBeginLocationid = request.getParameter('hdnBeginLocationid');
		var getItem = request.getParameter('hdnItem');
		var getItemName = request.getParameter('hdnItemName');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		nlapiLogExecution('DEBUG', 'hdnItemName', getItemName);
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');	
		var whLocation = request.getParameter('hdnwhlocation');
		var vZoneId=request.getParameter('hdnebizzoneno');
		var nextexpqty=request.getParameter('hdnextExpectedQuantity');
		var whCompany = request.getParameter('hdnwhCompany');
		var RecCount=request.getParameter('hdnRecCount');
		var getNext=request.getParameter('hdnnext');
		var getNextItemId=request.getParameter('hdnnextitem');
		var OrdName=request.getParameter('hdnName');
		var ContainerSize=request.getParameter('hdnContainerSize');
		var ebizOrdNo=request.getParameter('hdnebizOrdNo');

		nlapiLogExecution('DEBUG', 'Name', OrdName);
		nlapiLogExecution('DEBUG', 'getNext', getNext);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optskipEvent = request.getParameter('cmdSKIP');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_error"] = st7;//'INVALID ITEM';
		SOarray["custparam_screenno"] = '14A';
		SOarray["custparam_whlocation"] = whLocation;
		SOarray["custparam_waveno"] = getWaveNo;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_beginLocation"] = getBeginLocation;
		SOarray["custparam_beginLocationname"] = getBeginLocation;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemname"] = getItemName;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_dolineid"] = getDOLineId;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_batchno"] = vBatchNo;
		SOarray["custparam_noofrecords"] = RecCount;
		SOarray["custparam_nextlocation"] = getNext;
		SOarray["custparam_nextiteminternalid"] = getNextItemId;
		SOarray["name"] = OrdName;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = ebizOrdNo;
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_nextexpectedquantity"] = nextexpqty;
		SOarray["custparam_Actbatchno"] = "";
		SOarray["custparam_Expbatchno"] = "";
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		nlapiLogExecution('DEBUG', 'Order LineNo is', getOrderLineNo);

		// Fetch the actual location based on the begin location value that is fetched and passed as a parameter
		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getBeginLocation);

		// getBeginBinLocation = BinLocationRec.getFieldTx('custrecord_ebizlocname');
		//  nlapiLogExecution('DEBUG', 'Location Name is', getBeginBinLocation);
		/*       
        getBeginLocationInternalId = BinLocationRec.getId();
        nlapiLogExecution('DEBUG', 'Begin Location Internal Id', getBeginLocationInternalId);

        nlapiLogExecution('DEBUG', 'After validateSKU',actItemid);
        nlapiLogExecution('DEBUG', 'After validateSKU',actItemid);
        nlapiLogExecution('DEBUG', 'After validateSKU',actItemid);
		 */   

		nlapiLogExecution('DEBUG', 'getEnteredItem',getEnteredItem);
		nlapiLogExecution('DEBUG', 'getItemName',getItemName);
		nlapiLogExecution('DEBUG', 'getItem',getItem);

		var str = 'getEnteredItem. = ' + getEnteredItem + '<br>';			
		str = str + 'getItemName. = ' + getItemName + '<br>';
		str = str + 'getItem. = ' + getItem + '<br>';
		str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';

		nlapiLogExecution('DEBUG', 'Item Details1', str);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
		}
		else if(optskipEvent == 'F12')
		{
			var vPickType=request.getParameter('hdnpicktype');
			nlapiLogExecution('DEBUG', 'vPickType',vPickType);
			nlapiLogExecution('DEBUG', 'getWaveNo',getWaveNo);
			var RecCount;
			var SOFilters = new Array();
			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
			if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && getWaveNo != null && getWaveNo != "")
			{
				nlapiLogExecution('DEBUG', 'getWaveno inside If',getWaveno);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
			}
			if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
			{
				nlapiLogExecution('DEBUG', 'ClusNo inside If', vClusterNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
			}
			if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
			{
				nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
			}
			if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
			{
				nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
			}
			if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
			{
				nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
			}
			SOarray["custparam_ebizordno"] = ebizOrdNo;
			var SOColumns = new Array();
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
    		SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
			//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
			SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
			//Code end as on 290414
			SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
			SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
			SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			SOColumns.push(new nlobjSearchColumn('name'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
			SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
			SOColumns[0].setSort();
			SOColumns[1].setSort();
			SOColumns[2].setSort();
			SOColumns[3].setSort();
			SOColumns[4].setSort();
			SOColumns[5].setSort(true);

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
			if (SOSearchResults != null && SOSearchResults.length > 0) {
				nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length);
				nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);
				//vSkipId=0;
				if(SOSearchResults.length <= parseFloat(vSkipId))
				{
					vSkipId=0;
				}
				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveno);
				//var SOSearchResult = SOSearchResults[0];
				var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
				if(SOSearchResults.length > parseFloat(vSkipId) + 1)
				{
					//var SOSearchnextResult = SOSearchResults[1];
					var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
					nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
					nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
				}
				else
				{
					var SOSearchnextResult = SOSearchResults[0];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
					nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
					nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
				}
				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_lpno'));
				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveno);
				SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
				SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
				getRecordInternalId = SOSearchResult.getId();
				//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
				SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
				SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
				SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
				SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
				SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
				SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
				SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
				SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
				SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
				SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
				SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
				SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
				SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
				SOarray["custparam_noofrecords"] = SOSearchResults.length;		
				SOarray["name"] =  SOSearchResult.getValue('name');
				SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
				SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
				getBeginLocation = SOSearchResult.getText('custrecord_actbeginloc');
				if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
				{
					SOarray["custparam_ebizzoneno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebizzone_no');
				}
				else
					SOarray["custparam_ebizzoneno"] = '';
			}

			var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
			if(skiptask==null || skiptask=='')
			{
				skiptask=1;
			}
			else
			{
				skiptask=parseInt(skiptask)+1;
			}

			nlapiLogExecution('DEBUG', 'skiptask',skiptask);

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);
			vSkipId= parseFloat(vSkipId) + 1;
			SOarray["custparam_skipid"] = vSkipId;		
//			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
//			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('DEBUG', 'SOarray["custparam_nextiteminternalid"]', SOarray["custparam_nextiteminternalid"]);
			nlapiLogExecution('DEBUG', 'SOarray["custparam_nextlocation"]', SOarray["custparam_nextlocation"]);
			nlapiLogExecution('DEBUG', 'getBeginLocation', getBeginLocation);
			nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
			if(getBeginLocation != SOarray["custparam_nextlocation"])
			{  
				SOarray["custparam_beginLocationname"]=null;
				SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
			}
			else if(getItemInternalId != SOarray["custparam_nextiteminternalid"])
			{ 
				SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
				SOarray["custparam_beginLocationname"]=null;
				//SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
			}
			else
			{
				SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
				SOarray["custparam_beginLocationname"]=null;
				//SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
			}
		}
		else {
			if (getEnteredItem != '' && getEnteredItem != getItemName) {

				var actItemidArray=validateSKUId(getEnteredItem,whLocation,'');
				nlapiLogExecution('DEBUG', 'After validateSKU1',actItemidArray);
				var actItemid = actItemidArray[1];
				nlapiLogExecution('DEBUG', 'After validateSKU11',actItemid);
				if(actItemid!=null && actItemid!="")
				{
					nlapiLogExecution('DEBUG', 'actItemid ', actItemid);
					//POarray["custparam_poitem"]=actItemid;
					getEnteredItem=actItemid;

					nlapiLogExecution('DEBUG', 'getEnteredItem ', getEnteredItem);
					nlapiLogExecution('DEBUG', 'getItem ', getItem);
					nlapiLogExecution('DEBUG', 'getItemName ', getItemName);

					var SOFilters = new Array();
					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

					if(getWaveNo != null && getWaveNo != "")
					{

						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
					}

					if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
					{
						nlapiLogExecution('DEBUG', 'ClusNo inside If', vClusterNo);

						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
					}

					if(ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
					{
						nlapiLogExecution('DEBUG', 'OrdNo inside If', ebizOrdNo);

						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
					}


//					var Itype = nlapiLookupField('item', actItemid, 'recordType');
//					var ItemRec = nlapiLoadRecord(Itype, actItemid);			
//					actItemName=	ItemRec.getFieldValue('itemid');

					if(actItemid != null && actItemid != "")
					{

						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', actItemid));
					}

					if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
					{
						nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
					}

					SOarray["custparam_ebizordno"] =ebizOrdNo;
					var SOColumns = new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
					SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
					//Code end as on 290414
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

					if (SOSearchResults != null && SOSearchResults.length > 0) {

						vSkipId=0;
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}

						SOarray["custparam_recordinternalid"] = SOSearchResults[parseFloat(vSkipId)].getId();
						SOarray["custparam_expectedquantity"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_expe_qty');
						SOarray["custparam_invoicerefno"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_invref_no');
						SOarray["custparam_batchno"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_batch_no');
						SOarray["custparam_ebizordno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebiz_order_no');
						if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
						{
							SOarray["custparam_ebizzoneno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebizzone_no');
						}
						else
							SOarray["custparam_ebizzoneno"] = '';						
					}
				}
			}
			/*else
			{
				var actItemid=validateSKU(getEnteredItem);
				nlapiLogExecution('DEBUG', 'After validateSKU',actItemid);
				if(actItemid!=null && actItemid!="")
				{
					//POarray["custparam_poitem"]=actItemid;
					nlapiLogExecution('DEBUG', 'actItemid ', actItemid);
					getEnteredItem=actItemid;

					nlapiLogExecution('DEBUG', 'getEnteredItem ', getEnteredItem);
					nlapiLogExecution('DEBUG', 'getItem ', getItem);
				}	
			}*/
			else if(getEnteredItem == getItemName)
			{
				nlapiLogExecution('DEBUG', 'getEnteredItem', getEnteredItem);
				nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
				getEnteredItem = getItemInternalId;
			}

			var str = 'getEnteredItem. = ' + getEnteredItem + '<br>';			
			str = str + 'getItemName. = ' + getItemName + '<br>';
			str = str + 'getItem. = ' + getItem + '<br>';
			str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';

			nlapiLogExecution('DEBUG', 'Item Details2', str);

			if (getEnteredItem != '' && getEnteredItem == getItemInternalId) {
				// case 20123446 start :added 'custitem_ebizmodelno'
				var ItemTypeRec = nlapiLookupField('item', getItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot','custitem_ebizmodelno']);				
				var batchflag = ItemTypeRec.custitem_ebizbatchlot;
				var ItemType = ItemTypeRec.recordType;
				var ItemSpecialInstr = ItemTypeRec.custitem_ebizmodelno;
				// case 20123446 end :

				var str = 'getWaveNo. = ' + getWaveNo + '<br>';
				str = str + 'ebizOrdNo. = ' + ebizOrdNo + '<br>';
				str = str + 'OrdName. = ' + OrdName + '<br>';
				str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';
				str = str + 'vClusterNo. = ' + vClusterNo + '<br>';
				str = str + 'ItemSpecialInstr. = ' + ItemSpecialInstr + '<br>';

				nlapiLogExecution('DEBUG', 'Parameters', str);	

				var SOFilters = new Array();
				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

				if(OrdName != null && OrdName != "")
				{
					SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
				}

				if(getWaveNo != null && getWaveNo != "")
				{

					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
				}

				if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
				{					

					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
				}

				if(ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
				{

					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
				}

				if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
				{
					nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
					SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
				}


//				if(getItemInternalId != null && getItemInternalId != "")
//				{

//				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
//				}

				var SOColumns = new Array();
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//				SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
				//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
				SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
				//Code end as on 290414
				SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
				SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
				SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
				SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
				SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
				SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
				SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
				SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
				SOColumns.push(new nlobjSearchColumn('name'));
				SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
				SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
				SOColumns.push(new nlobjSearchColumn('custrecord_sku_status'));
				
				SOColumns[0].setSort();
				SOColumns[1].setSort();
				SOColumns[2].setSort();
				SOColumns[3].setSort();
				SOColumns[4].setSort();
				SOColumns[5].setSort(true);

				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

				if (SOSearchResults != null && SOSearchResults.length > 0) {
					nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length);
					nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);

					vSkipId=0;
					if(SOSearchResults.length <= parseFloat(vSkipId))
					{
						vSkipId=0;
					}
					nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);
					nlapiLogExecution('DEBUG', 'SOSearchResults count', SOSearchResults.length);
					SOarray["custparam_recordinternalid"] = SOSearchResults[parseFloat(vSkipId)].getId();
					SOarray["custparam_expectedquantity"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_expe_qty');
					SOarray["custparam_invoicerefno"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_invref_no');
					SOarray["custparam_batchno"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_batch_no');
					SOarray["custparam_ebizordno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebiz_order_no');
					SOarray["custparam_orderlineno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_line_no');
					SOarray["custparam_itemstatus"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_sku_status');
					if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
					{
						SOarray["custparam_ebizzoneno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebizzone_no');
					}
					else
						SOarray["custparam_ebizzoneno"] = '';

					if(SOSearchResults.length > parseFloat(vSkipId) + 1)
					{
						var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
						SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
						SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
						nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
						nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
					}

				}


				nlapiLogExecution('DEBUG', 'ItemType', ItemType);
				// case 20123446 start: navigation to Item info page
				nlapiLogExecution('ERROR', 'ItemSpecialInstr', ItemSpecialInstr);
				SOarray["custparam_itemInstructions"] =ItemSpecialInstr;
				if(ItemSpecialInstr !=null && ItemSpecialInstr !='')
				{
					response.sendRedirect('SUITELET', 'customscript_picking_item_instructions', 'customdeploy_ebiz_picking_item_instru_di', false, SOarray);
				}
				else
				{
					//If Lotnumbered item the navigate to batch # scan
					if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
					{
						SOarray["custparam_itemType"] ='lotnumberedinventoryitem';
						response.sendRedirect('SUITELET', 'customscript_rf_picking_batch', 'customdeploy_rf_picking_batch_di', false, SOarray);					  
					}
					else {

						response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
					}
				}
				// case 20123446 end
			}
			else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the location');
			}
		}
	}
}
