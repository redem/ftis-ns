/***************************************************************************
			eBizNET Solutions 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_AutoBreakDownsch_SL.js,v $
 *     	   $Revision: 1.1.2.6.2.2 $
 *     	   $Date: 2015/11/14 11:36:57 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_AutoBreakDownsch_SL.js,v $
 * Revision 1.1.2.6.2.2  2015/11/14 11:36:57  snimmakayala
 * 201415635
 *
 * Revision 1.1.2.6.2.1  2015/09/21 10:09:29  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.2.6  2015/07/10 22:35:29  skreddy
 * Case# 201413009
 * CT Prod  issue fix
 *
 * Revision 1.1.2.5  2015/05/27 13:38:22  schepuri
 * case# 201412923
 *
 * Revision 1.1.2.4  2015/01/05 09:41:43  snimmakayala
 * no message
 *
 * Revision 1.1.2.3  2014/05/27 07:36:59  snimmakayala
 * Case#: 20148135
 * Schedule Script Status
 *
 * Revision 1.1.2.2  2013/11/12 18:11:57  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Auto break down scheduler is fixed.
 *
 * Revision 1.1.2.1  2013/10/07 15:37:33  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * MHP Changes
 *
 * Revision 1.17.2.9.4.3.2.11.2.4  2013/09/03 15:28:49  rmukkera
 * Case# 20124078, 20124153
 *
 * Revision 1.17.2.9.4.3.2.11.2.3  2013/08/30 15:10:06  nneelam
 * Case#.20124153�
 * AutoBreakDown Feature for RMA.
 *
 * Revision 1.17.2.9.4.3.2.11.2.2  2013/08/30 14:15:02  grao
 * Issue Fixes for 20124078
 *
 * Revision 1.17.2.9.4.3.2.11.2.1  2013/08/29 15:59:37  skreddy
 * Case# 20124115
 * issue rellated to LP when different  location in PO line level
 *
 * Revision 1.17.2.9.4.3.2.11  2013/06/27 06:54:44  nneelam
 * Case# 20123032
 * AutoBreak Down.
 *
 * Revision 1.17.2.9.4.3.2.10  2013/06/13 15:57:57  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.17.2.9.4.3.2.9  2013/06/05 22:11:22  spendyala
 * CASE201112/CR201113/LOG201121
 * FIFO field updating with data stamp is removed.
 *
 * Revision 1.17.2.9.4.3.2.8  2013/05/21 15:06:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.17.2.9.4.3.2.7  2013/05/15 14:51:03  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.17.2.9.4.3.2.6  2013/05/14 14:52:02  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.17.2.9.4.3.2.5  2013/05/08 15:21:08  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.17.2.9.4.3.2.4  2013/04/24 15:20:31  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.17.2.9.4.3.2.3  2013/04/09 13:15:53  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to restrict to delete PO line after check in
 *
 * Revision 1.17.2.9.4.3.2.2  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.17.2.9.4.3.2.1  2013/03/01 14:34:59  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.17.2.9.4.3  2013/02/20 23:29:38  kavitha
 * CASE201112/CR201113/LOG201121
 * 20121784  Ryonet SB issue
 *
 * Revision 1.17.2.9.4.2  2013/02/16 12:11:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.17.2.9.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.17.2.9  2012/08/31 07:16:13  spendyala
 * CASE201112/CR201113/LOG201121
 * Moving CHKN Task to Closed task.
 *
 * Revision 1.17.2.8  2012/08/22 14:50:13  schepuri
 * CASE201112/CR201113/LOG201121
 * QC customscript name is corrected
 *
 * Revision 1.17.2.7  2012/07/04 07:44:16  gkalla
 * CASE201112/CR201113/LOG201121
 * Throwaway parent id changed
 *
 * Revision 1.17.2.6  2012/06/22 10:18:51  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned using child-parent relation
 *
 * Revision 1.17.2.5  2012/06/07 13:24:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Add filter for po
 *
 * Revision 1.17.2.4  2012/06/04 12:51:36  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix to display remaning qty based on chkin qty
 *
 * Revision 1.17.2.3  2012/05/31 12:49:33  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.17.2.2  2012/04/20 12:40:13  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.17.2.1  2012/02/21 15:37:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.17  2012/01/06 15:09:04  gkalla
 * CASE201112/CR201113/LOG201121
 * To update ebizuser and task assigned values in open task to get the values in task management screen
 *
 * Revision 1.16  2012/01/05 14:21:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code tunning
 *
 * Revision 1.15  2011/12/28 00:28:04  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zero Qty Task Issues.
 *
 * Revision 1.14  2011/12/26 13:04:40  gkalla
 * CASE201112/CR201113/LOG201121
 * PO Receipt related changes
 *
 * Revision 1.13  2011/12/19 07:08:05  spendyala
 * CASE201112/CR201113/LOG201121
 * added with hidden fields .
 *
 * Revision 1.12  2011/12/16 12:30:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/12/12 07:03:07  rgore
 * CASE201112/CR201113/LOG201121
 * Sanitized error message for spelling and grammar.
 * - Ratnakar
 * 12 Dec 2011
 *
 * Revision 1.10  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/07/25 15:50:41  skota
 * CASE201112/CR201113/LOG201121
 * Issue Fix, code cleaning & fine tuning
 *
 * 
 *****************************************************************************/
function AutoBreakDown(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR', 'Time Stamp at the start of request',TimeStampinSec());		
		// The below code is added from Factory Mation on 28Feb13 by santosh
		var context = nlapiGetContext();
		//upto here
		nlapiLogExecution('ERROR','Remaining usage at the start of get',context.getRemainingUsage());

		var form = nlapiCreateForm('Auto Breakdown');
		form.setScript('customscript_autobreakdownclient');

		nlapiLogExecution('ERROR', 'vMsg', request.getParameter('custpage_msg'));
		nlapiLogExecution('ERROR', 'vorderstatus', request.getParameter('custpage_orderstatus'));
		var poname = request.getParameter('custpage_po');
		//nlapiLogExecution('ERROR', 'vordestatus', request.getParameter('custpage_orderstatus'));

		if (request.getParameter('custpage_orderstatus') != null && request.getParameter('custpage_orderstatus') != "") {
			vorderstatus = request.getParameter('custpage_orderstatus');
			nlapiLogExecution('ERROR', 'vorderstatus', vorderstatus);
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null); 
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', '" + vorderstatus + "', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		}		


		if (request.getParameter('custpage_msg') != null && request.getParameter('custpage_msg') != "") {
			vMsg = request.getParameter('custpage_msg');
			nlapiLogExecution('ERROR', 'vMsg', vMsg);
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null); 
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '" + vMsg + "', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		}

		nlapiLogExecution('ERROR', 'vMsg here', request.getParameter('custpage_msg'));
		if (request.getParameter('custpage_msg') == null || request.getParameter('custpage_msg') == ""){
			//case# 20124153 Start
			var OrderType = form.addField('custpage_ordertype', 'select', 'Transaction Type').setMandatory(true);
			var selectPO = form.addField('custpage_po', 'text', 'PO/TO/RMA #');
			selectPO.setDefaultValue(poname);

			nlapiLogExecution('ERROR', 'selectPO', selectPO);
			//OrderType.addSelectOption("","");
			OrderType.addSelectOption("purchaseorder","PO");
			OrderType.addSelectOption("transferorder","TO");
			OrderType.addSelectOption("returnauthorization","RMA");
			if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
			{
				OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
			}

			//selectPO.addSelectOption('',''); 
			//case# 20124153 End

			var receiptRequired=form.addField('custpage_receiptrequired', 'text', 'Receipt Required').setDisplayType('hidden');
			receiptRequired.setDefaultValue('Y');

			var receiptPO =  form.addField('custpage_receiptfield','text','Receipt#');
			var trailerPO =  form.addField('custpage_trailerfield','text','Trailer Appointment#/Trip#');

			var filterspo = new Array();
			filterspo.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
			filterspo.push(new nlobjSearchFilter('status', null, 'noneof', ['PurchOrd:F','PurchOrd:H']));
			var RoleLocation=getRoledBasedLocation();
			nlapiLogExecution('ERROR', 'RoleLocation', RoleLocation);
			if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
			{
				filterspo.push(new nlobjSearchFilter('location', null, 'anyof', RoleLocation));
			}

			var colspo = new Array();
			colspo[0]=new nlobjSearchColumn('tranid');
			colspo[0].setSort(true);

			var searchresults = nlapiSearchRecord('purchaseorder', null, filterspo, colspo);
//			case# 20124153 Start

//			if (searchresults != null)
//			{
//			for (var i = 0; i < searchresults.length; i++) 
//			{
//			selectPO.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('tranid'));
//			}
//			}
//			case# 20124153 End

			var flag=form.addField('custpage_validationflag', 'text', 'Validation temp flag').setDisplayType('hidden');
			flag.setDefaultValue('GET');
			var button = form.addSubmitButton('Display');
			nlapiLogExecution('ERROR','Remaining usage at the end of get',context.getRemainingUsage());
			nlapiLogExecution('ERROR', 'Time Stamp at the end of request',TimeStampinSec());
		}
		response.writePage(form);

	} 
	else 
	{
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR', 'Time Stamp at the start of response',TimeStampinSec());	
		nlapiLogExecution('ERROR','Remaining usage at the start of post',context.getRemainingUsage());

		var form = nlapiCreateForm('Auto Breakdown');
		var poname=request.getParameter('custpage_po');
		var ordtype=request.getParameter('custpage_ordertype');
		//case# 20124153 Start
		var poid;
		nlapiLogExecution('ERROR','poname', poname);
		if(poname!=null && poname!='')
		{
			poid=GetPOInternalId(poname,ordtype);
		}
		//case# 20124153 End
		var trailerno=request.getParameter('custpage_trailerfield');
		var receiptno=request.getParameter('custpage_receiptfield');
		var receiptRequired=request.getParameter('custpage_receiptrequired');
		var tempflag=request.getParameter('custpage_tempflag');
		nlapiLogExecution('ERROR','PO value', poname);
		nlapiLogExecution('ERROR','trailerno', trailerno);
		nlapiLogExecution('ERROR','receiptno', receiptno);
		nlapiLogExecution('ERROR','receiptRequired', receiptRequired);
		nlapiLogExecution('ERROR','tempflag', tempflag);
		var POid =  form.addField('custpage_poid','text','PO#').setDisplayType('inline');
		var printtype =  form.addField('custpage_print','select','PrinterName #');
		var receiptPO =  form.addField('custpage_receiptfield','text','Receipt#').setDisplayType('inline');
		var trailerPO =  form.addField('custpage_trailerfield','text','Trailer Appointment#/Trip#').setDisplayType('inline');

		POid.setDefaultValue(poid);
		if(receiptno!=""&&receiptno!=null)
			receiptPO.setDefaultValue(receiptno);
		if(trailerno!=""&&trailerno!=null)
			trailerPO.setDefaultValue(trailerno);

		var trantype = '';

		if(poid!=null && poid!='')
			trantype = nlapiLookupField('transaction', poid, 'recordType');
		var validate=CheckOrderStatus(poid,trantype);

		nlapiLogExecution('ERROR','trantype', trantype);
		nlapiLogExecution('ERROR','validate', validate);

		if(validate=="F")
		{

			showInlineMessage(form, 'Error', 'Invalid "'+trantype +'" Status', " ");
			response.writePage(form);

			/*var POarray=new Array();
			POarray ["custpage_orderstatus"] ='INVALID ORDER STATUS';
			POarray ["custpage_poid"] = poname;
			response.sendRedirect('SUITELET', 'customscript_autobreakdownsch_sl', 'customdeploy_autobreakdownsch_dl', false, POarray );*/
		}
		else
		{
			if((receiptRequired=='Y'&& tempflag==null))
			{
				nlapiLogExecution('ERROR','chkpt', 'chkpt');
				if((trailerno!='' || receiptno!=''))
				{
					nlapiLogExecution('ERROR','firstif', 'IF');

					form.setScript('customscript_autobreakdownclient');
					var tempflag=form.addField('custpage_tempflag', 'text', 'Temp Flag').setDisplayType('hidden');
					tempflag.setDefaultValue('true');

					//var poname;
					if(poname != null && poname != ""){
						//poname= nlapiLookupField('purchaseorder',po,'tranid');
						POid.setDefaultValue(poname);
					}
					var filterporeceipt=new Array();
					if(poid!=null&&poid!="")
						filterporeceipt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_pono',null,'anyof',poid));
					if(trailerno!=null&&trailerno!="")
						filterporeceipt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_trailerno',null,'is',trailerno));//Trailer# = Receipt#
					if(receiptno!=null&&receiptno!="")
						filterporeceipt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_receiptno',null,'is',receiptno));
					var columnporeceipt=new Array();
					columnporeceipt[0]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_pono');
					columnporeceipt[1]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_trailerno');
					columnporeceipt[2]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_lineno');
					columnporeceipt[3]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_item');
					columnporeceipt[4]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_polineordqty');
					columnporeceipt[5]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptqty');
					columnporeceipt[6]=new nlobjSearchColumn('custitem_ebizdescriptionitems','custrecord_ebiz_poreceipt_item');

					var searchporeceipt=nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt',null,filterporeceipt,columnporeceipt);

					var button = form.addSubmitButton('Auto Breakdown');            
					var sublist = form.addSubList("custpage_items", "list", "PO List");
					sublist.addMarkAllButtons();//added on 10/1/12 by suman.
					sublist.addField("custpage_brkdwnflag", "checkbox", "Auto Breakdown");
					sublist.addField("custpage_potext", "text", "PO#");
					sublist.addField("custpage_po", "text", "PO internalId#").setDisplayType('hidden');
					sublist.addField("custpage_trailer", "text", "Trailer#");
					sublist.addField("custpage_line", "text", "Line#");
					sublist.addField("custpage_itemname", "text", "Item Name").setDisplayType('hidden');
					sublist.addField("custpage_iteminternalid", "text", "Item internalid").setDisplayType('hidden');
					sublist.addField("custpage_itemid", "select", "Item", "item").setDisplayType('inline');
					sublist.addField("custpage_palletqty", "text", "Pallet qty");
					sublist.addField("custpage_itemdesc", "text", "Item Description");
					sublist.addField("custpage_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDisplayType('inline');
					sublist.addField("custpage_itempackcode", "select", "Pack Code", "customlist_ebiznet_packcode").setDisplayType('inline');
					sublist.addField("custpage_quantity", "text", "Quantity");
					sublist.addField("custpage_rcvquantity", "text", "RCV Quantity").setDisplayType('entry');            
					sublist.addField("custpage_lotbatch", "text", "LOT#").setDisplayType('entry');
					sublist.addField("custpage_lotbatchlink", "text", "Select Lot#");
					sublist.addField("custpage_chkn_binlocation", "select", "Bin Location","customrecord_ebiznet_location");
					sublist.addField("custpage_cartlp", "text", "Cart LP").setDisplayType('entry');
					sublist.addField("custpage_potranid", "text", "PO#").setDisplayType('hidden');
					sublist.addField("custpage_polocationid", "text", "location").setDisplayType('hidden');
					sublist.addField("custpage_pocompanyid", "text", "company").setDisplayType('hidden');
					sublist.addField("custpage_popackcode", "text", "location").setDisplayType('hidden');
					sublist.addField("custpage_poitemstatus", "text", "company").setDisplayType('hidden');
					sublist.addField("custpage_receiptid", "text", "receiptid").setDisplayType('hidden');
					sublist.addField("custpage_autobreakdownqty", "text", "Qty left to auto breakdown").setDisplayType('hidden');
					sublist.addField("custpage_poline", "text", "POline#").setDisplayType('hidden');
					sublist.addField("custpage_poquantity", "text", "POquantity").setDisplayType('hidden');

					var k=1;
					if(searchporeceipt!=null&&searchporeceipt!="")
					{
						var itemslist = new Array();
						var pointrid=0;
						for ( var g = 0; g < searchporeceipt.length; g++) {

							var item = searchporeceipt[g].getValue('custrecord_ebiz_poreceipt_item');
							pointrid = searchporeceipt[g].getValue('custrecord_ebiz_poreceipt_pono');
							itemslist.push(item);					
						}

						nlapiLogExecution('ERROR','pointrid', 'pointrid');

						var Itemdimresults = getItemdimpalletqty(itemslist);

						//*** The below code is added by Satish.N on 02/06/2013 as part of performance tuning. ***//

						var polinedetails = getPODetails(pointrid);

						//*** Upto here. ***//

						for ( var count = 0; count < searchporeceipt.length; count++) {
							var searchresult=searchporeceipt[count];
							var ponum=searchresult.getValue('custrecord_ebiz_poreceipt_pono');
							var potext=searchresult.getText('custrecord_ebiz_poreceipt_pono');
							var trailer=searchresult.getValue('custrecord_ebiz_poreceipt_trailerno');
							var lineno=searchresult.getValue('custrecord_ebiz_poreceipt_lineno');
							var itemtext=searchresult.getText('custrecord_ebiz_poreceipt_item');
							var itemid=searchresult.getValue('custrecord_ebiz_poreceipt_item');
							var receiptqty=searchresult.getValue('custrecord_ebiz_poreceipt_receiptqty');
							var poordqtyRec=searchresult.getValue('custrecord_ebiz_poreceipt_polineordqty');
							var itemdesc=searchresult.getValue('custitem_ebizdescriptionitems','custrecord_ebiz_poreceipt_item');
							var palletqty = getpalletqty(Itemdimresults, itemid);
							var polocationid='';
							var pocompanyid='';
							var itemstatus='';
							var itempackcode='';
							var orderqty=0;
							var polocation = '';

							nlapiLogExecution('ERROR', 'po',ponum);
							nlapiLogExecution('ERROR', 'lineno',lineno);

							//*** The below code is added by Satish.N on 02/06/2013 as part of performance tuning. ***//

							for ( var z = 0; z < polinedetails.length; z++) {

								var polineno=polinedetails[z].getValue('line');
								nlapiLogExecution('ERROR', 'polineno',polineno);

								if(lineno==polineno)
								{
									polocationid = polinedetails[z].getValue('location');
									polocation = polinedetails[z].getText('location');
									pocompanyid = polinedetails[z].getValue('custbody_nswms_company');
									itemstatus = polinedetails[z].getValue('custcol_ebiznet_item_status');
									itempackcode = polinedetails[z].getValue('custcol_nswmspackcode');
									orderqty = polinedetails[z].getValue('quantity');
									break;
								}

							}

							var fields = ['custrecord_ebizwhsite'];
							nlapiLogExecution('ERROR', 'polocationid',polocationid);
							nlapiLogExecution('ERROR', 'polocation',polocation);
							var locationcolumns = nlapiLookupField('Location', polocationid, fields);		
							var Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;
							nlapiLogExecution('ERROR', 'Tomwhsiteflag',Tomwhsiteflag);
							
							if(Tomwhsiteflag =='F')
								{

								var POarray=new Array();
								POarray ["custpage_msg"] =' " '+ polocation +' " is not a Ware house site.';
								POarray ["custpage_poid"] = poname;

								response.sendRedirect('SUITELET', 'customscript_autobreakdownsch_sl', 'customdeploy_autobreakdownsch_dl', false, POarray );
								
								}
							//*** Upto here. ***//

							//*** The below code is commented by Satish.N on 02/06/2013 as part of performance tuning. ***//

//							var poload = nlapiLoadRecord('purchaseorder', ponum);
//							var itemstatus = poload.getLineItemValue('item', 'custcol_ebiznet_item_status', lineno);
//							var itempackcode = poload.getLineItemValue('item', 'custcol_nswmspackcode', lineno);
//							var polocationid=poload.getFieldValue('location');
//							var pocompanyid=poload.getFieldValue('custbody_nswms_company');
//							var orderqty=poload.getLineItemValue('item', 'quantity', lineno);

							//*** Upto here. ***//

							if(receiptqty==null||receiptqty=='')
								receiptqty=0;

							var qtyToAutobreakdown=parseFloat(poordqtyRec)-parseFloat(receiptqty);

							nlapiLogExecution('ERROR', 'polocationid', polocationid);
							nlapiLogExecution('ERROR', 'pocompanyid', pocompanyid);
							nlapiLogExecution('ERROR', 'itemid',itemid);

							var fields = ['custitem_ebizdefpackcode','custitem_ebizdefskustatus',"recordType","custitem_ebizbatchlot"];
							var columns= nlapiLookupField('item',itemid,fields);

							if(itempackcode==null || itempackcode=='')
								itempackcode = columns.custitem_ebizdefpackcode;

							if(itemstatus==null || itemstatus=='')
								itemstatus = columns.custitem_ebizdefskustatus;

							var vItemType=columns.recordType;

							var BatchFlag = columns.custitem_ebizbatchlot;

							nlapiLogExecution('ERROR', 'qtyToAutobreakdown', qtyToAutobreakdown);
							nlapiLogExecution('ERROR', 'lineno', lineno);
							nlapiLogExecution('ERROR', 'poordqtyRec', poordqtyRec);

							if(parseFloat(qtyToAutobreakdown)>0)
							{
								form.getSubList('custpage_items').setLineItemValue('custpage_potext', k, potext);
								form.getSubList('custpage_items').setLineItemValue('custpage_po', k, ponum);
								form.getSubList('custpage_items').setLineItemValue('custpage_trailer', k, trailer);
								form.getSubList('custpage_items').setLineItemValue('custpage_line', k, lineno);
								form.getSubList('custpage_items').setLineItemValue('custpage_itemname', k, itemtext);
								form.getSubList('custpage_items').setLineItemValue('custpage_itemid', k, itemid);
								form.getSubList('custpage_items').setLineItemValue('custpage_iteminternalid', k, itemid);
								form.getSubList('custpage_items').setLineItemValue('custpage_palletqty', k, parseFloat(palletqty).toFixed(5));
								if(itemdesc!=null && itemdesc!='')
									itemdesc=itemdesc.substring(0, 200);
								
								form.getSubList('custpage_items').setLineItemValue('custpage_itemdesc', k, itemdesc);
								form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', k, itemstatus);
								form.getSubList('custpage_items').setLineItemValue('custpage_itempackcode', k, itempackcode);
								form.getSubList('custpage_items').setLineItemValue('custpage_quantity', k, poordqtyRec);
								form.getSubList('custpage_items').setLineItemValue('custpage_rcvquantity', k,qtyToAutobreakdown.toString());	 	              
								form.getSubList('custpage_items').setLineItemValue('custpage_potranid', k, potext);
								form.getSubList('custpage_items').setLineItemValue('custpage_polocationid', k, polocationid);
								form.getSubList('custpage_items').setLineItemValue('custpage_pocompanyid', k, pocompanyid);
								form.getSubList('custpage_items').setLineItemValue('custpage_popackcode', k, itempackcode);
								form.getSubList('custpage_items').setLineItemValue('custpage_poitemstatus', k, itemstatus);
								form.getSubList('custpage_items').setLineItemValue('custpage_receiptid', k, searchresult.getId());
								form.getSubList('custpage_items').setLineItemValue('custpage_autobreakdownqty',k,qtyToAutobreakdown);
								form.getSubList('custpage_items').setLineItemValue('custpage_poline',k,lineno);
								form.getSubList('custpage_items').setLineItemValue('custpage_poquantity',k,poordqtyRec);
								//codechanges for issue# 20121503
								if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || BatchFlag =='T')
								{
									var lotnourl = nlapiResolveURL('SUITELET', 'customscript_ebiz_lot_creation_sl', 'customdeploy_ebiz_lot_creation_sl') + "&custparam_item=" + 
									itemid+"&custparam_itemstatus="+itemstatus+"&custparam_location="+polocationid+"&custparam_qty="+qty+"&custparam_line="+k;
									form.getSubList('custpage_items').setLineItemValue('custpage_lotbatchlink', k, "<a href=\"#\" onclick=\"javascript:window.open('" + lotnourl + "');\" >Multiple Lot Entry</a>");
								}
								//end of the code

								k++;
							}
						}
					}
					response.writePage(form);
				}
				else 
				{	
					nlapiLogExecution('ERROR','firstelse', 'ELSE');
					// Code to populate SubList with PO line details for selected PO
					var form = nlapiCreateForm('Auto Breakdown');
					var tempflag=form.addField('custpage_tempflag', 'text', 'Temp Flag').setDisplayType('hidden');
					tempflag.setDefaultValue('true');

					form.setScript('customscript_autobreakdownclient');
					nlapiLogExecution('ERROR','request.getParameter(custpage_po)',request.getParameter('custpage_po'));
					if (request.getParameter('custpage_po') != null)
					{
						var poNum = "";
						form.setScript('customscript_autobreakdownclient');
						nlapiLogExecution('DEBUG','PO value', request.getParameter('custpage_po'));
						//case# 20124153 Start
						var selectPO = form.addField('custpage_pono', 'text', 'PO/TO/RMA #').setDisplayType('hidden');
						var OrderType = form.addField('custpage_ordertype', 'select', 'Transaction Type').setMandatory(true);
						OrderType.addSelectOption("","");
						OrderType.addSelectOption("purchaseorder","PO");
						OrderType.addSelectOption("transferorder","TO");
						OrderType.addSelectOption("returnauthorization","RMA");
						if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
						{
							OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
						}
//						var filterspo = new Array();
//						filterspo.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

//						var colspo = new Array();
//						colspo[0]=new nlobjSearchColumn('tranid');
//						colspo[0].setSort(true);

//						var searchresults = nlapiSearchRecord('purchaseorder', null, filterspo, colspo);

//						if (searchresults != null)
//						{
//						for (var i = 0; i < searchresults.length; i++) 
//						{
//						selectPO.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('tranid'));
//						}
//						}
						//case# 20124153 End

						selectPO.setDefaultValue(request.getParameter('custpage_po'));

						//selectPO.setDisplayType('inline');

						var headerCartLP = form.addField('custpage_hcartlp', 'text', 'Cart LP');
						headerCartLP.setLayoutType('normal', 'startcol');
						var GenLocFlag= form.addField('custpage_confirmputaway', 'checkbox', 'Create Inventory').setDefaultValue('F');
						var button = form.addSubmitButton('Auto Breakdown');            
						var sublist = form.addSubList("custpage_items", "list", "PO List");
						sublist.addMarkAllButtons();//added on 10/1/12 by suman.
						sublist.addField("custpage_brkdwnflag", "checkbox", "Auto Breakdown");
						sublist.addField("custpage_potext", "text", "PO/TO#");
						sublist.addField("custpage_po", "text", "PO internal id#").setDisplayType('hidden');
						sublist.addField("custpage_trailer", "text", "Trailer#");
						sublist.addField("custpage_line", "text", "Line#");
						sublist.addField("custpage_itemname", "text", "Item Name").setDisplayType('hidden');
						sublist.addField("custpage_iteminternalid", "text", "Item internalid").setDisplayType('hidden');
						sublist.addField("custpage_itemid", "select", "Item", "item").setDisplayType('inline');
						sublist.addField("custpage_palletqty", "text", "Pallet qty");
						sublist.addField("custpage_itemdesc", "text", "Item Description");
						sublist.addField("custpage_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDisplayType('inline');
						sublist.addField("custpage_itempackcode", "select", "Pack Code", "customlist_ebiznet_packcode").setDisplayType('inline');
						sublist.addField("custpage_quantity", "text", "Quantity");
						sublist.addField("custpage_rcvquantity", "text", "RCV Quantity");            
						sublist.addField("custpage_lotbatch", "text", "LOT/Serial#").setDisplayType('entry');
						sublist.addField("custpage_lotbatchlink", "text", "Select Lot#");
						sublist.addField("custpage_chkn_binlocation", "select", "Bin Location","customrecord_ebiznet_location");
						sublist.addField("custpage_cartlp", "text", "Cart LP").setDisplayType('entry');
						sublist.addField("custpage_potranid", "text", "PO#").setDisplayType('hidden');
						sublist.addField("custpage_polocationid", "text", "location").setDisplayType('hidden');
						sublist.addField("custpage_pocompanyid", "text", "company").setDisplayType('hidden');
						sublist.addField("custpage_popackcode", "text", "location").setDisplayType('hidden');
						sublist.addField("custpage_poitemstatus", "text", "company").setDisplayType('hidden');
						sublist.addField("custpage_receiptid", "text", "receiptid").setDisplayType('hidden');
						sublist.addField("custpage_poline", "text", "POline#").setDisplayType('hidden');
						sublist.addField("custpage_poquantity", "text", "POquantity").setDisplayType('hidden');

						// Get PO Details.
						if(poid!=null && poid!='' && poid!=-1)
						{
							trantype = nlapiLookupField('transaction', poid, 'recordType');

							var poload = nlapiLoadRecord(trantype, poid);

//							var vponum= poload.getFieldValue('id');
//							poNum=poload.getFieldValue('tranid');
							nlapiLogExecution('ERROR', 'PO Value', request.getParameter('custpage_po'));
							//nlapiLogExecution('ERROR', 'vponum', vponum);

							var lineCount = poload.getLineItemCount('item');

							nlapiLogExecution('ERROR', 'lineCount', lineCount);
							var  k=1;   

							// Get Transaction Order Line details to find out line wise already checked-in quantity
							var filters = new Array();
							filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto',poid));

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
							columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');

							var searchQtyResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details',null, filters, columns);  
							nlapiLogExecution('ERROR', 'test1', 'test1');
							var itemslist = new Array();
							var LineLevelLocation = new Array();
							for ( var z = 1; z <= lineCount; z++) {
								//nlapiLogExecution('ERROR', 'test2', 'test2');
								var item = poload.getLineItemValue('item', 'item', z);
								var poLinelocationid=poload.getLineItemValue('item', 'location', z);
								itemslist.push(item);					
								if(poLinelocationid !=null && poLinelocationid !='')
								{	//nlapiLogExecution('ERROR', 'poLinelocationid', poLinelocationid);
									if(LineLevelLocation.indexOf(poLinelocationid)== -1)
									{
										//nlapiLogExecution('ERROR', 'poLinelocationid', poLinelocationid);
										//nlapiLogExecution('ERROR', 'LineLevelLocation', LineLevelLocation);
										LineLevelLocation.push(poLinelocationid);
									}
								}
							}
							nlapiLogExecution('ERROR', 'LineLevelLocation', LineLevelLocation);
							var Itemdimresults=getItemdimpalletqty(itemslist);
							var  Tomwhsiteflag=CheckMakeSiteOrNot(LineLevelLocation);

							nlapiLogExecution('ERROR', 'Tomwhsiteflag',Tomwhsiteflag);
							if(Tomwhsiteflag =='F')
							{
								var POarray=new Array();
								POarray ["custpage_msg"] =' Line level location is not a Ware house site.';
								POarray ["custpage_poid"] = poname;

								response.sendRedirect('SUITELET', 'customscript_autobreakdownsch_sl', 'customdeploy_autobreakdownsch_dl', false, POarray );
							}
							for (var i = 1; i <= lineCount; i++) 
							{    
								var vitem = poload.getLineItemValue('item', 'item', i);
								//	nlapiLogExecution('ERROR', 'vitem', vitem);
								var checkInQty = 0;
								var	itemlineno = poload.getLineItemValue('item', 'line', i);
								var polineqty = poload.getLineItemValue('item', 'quantity', i);
								var polinestatus=poload.getLineItemValue('item', 'custcol_transactionstatusflag', i);
								var islineclosed = poload.getLineItemValue('item', 'isclosed', i);
								//	nlapiLogExecution('ERROR', 'islineclosed', islineclosed);
								if(islineclosed!='T')
								{
									if(searchQtyResults != null && searchQtyResults != "")
									{ 
										//nlapiLogExecution('ERROR', 'itemlineno', searchQtyResults.length);
										for(l=0; l < searchQtyResults.length; l++)
										{
											if (itemlineno == searchQtyResults[l].getValue('custrecord_orderlinedetails_orderline_no')) 
											{
												checkInQty = searchQtyResults[l].getValue('custrecord_orderlinedetails_checkin_qty');
												nlapiLogExecution('ERROR', 'Order Line Details Check-In Qty', checkInQty);
												break;
											}
										}
									}      
									if(checkInQty==null || checkInQty=='' )
										checkInQty = 0;   
									if(parseFloat(polineqty)>parseFloat(checkInQty) && polinestatus!='36')
									{
								if(parseInt(vitem)>0)
								{
											var vitemType = nlapiLookupField('item', vitem ,['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

											//nlapiLogExecution('ERROR', 'vitemType', vitemType);


											if(vitemType.recordType != 'serviceitem' && vitemType.recordType != 'noninventoryitem' && vitemType.recordType != 'descriptionitem'&&vitemType.recordType != 'otherchargeitem')
									{
										itemlineno = poload.getLineItemValue('item', 'line', i);
										itemname = poload.getLineItemText('item', 'item', i);
										itemid = poload.getLineItemValue('item', 'item', i);
										itemdesc = poload.getLineItemValue('item', 'description', i);
										itemstatus = poload.getLineItemValue('item', 'custcol_ebiznet_item_status', i);
										itempackcode = poload.getLineItemValue('item', 'custcol_nswmspackcode', i);
										polineqty = poload.getLineItemValue('item', 'quantity', i);
										poserialno = poload.getLineItemValue('item', 'serialnumbers', i);
										polocationid=poload.getLineItemValue('item', 'location', i);

										var polocation = poload.getLineItemText('item', 'location', i);
										var polinestatus=poload.getLineItemValue('item', 'custcol_transactionstatusflag', i);
										if(trantype=='transferorder')
											polocationid=poload.getFieldValue('transferlocation');

										if(trantype=='returnauthorization')
											polocationid=poload.getFieldValue('location');
										
										if(trantype=='purchaseorder' && (polocationid == null || polocationid =='') )// case 201412923
											polocationid=poload.getFieldValue('location');

										pocompanyid=poload.getFieldValue('custbody_nswms_company');
										nlapiLogExecution('ERROR', 'itemlineno', itemlineno);
										nlapiLogExecution('ERROR', 'polocationid', polocationid);
										nlapiLogExecution('ERROR', 'polocation text', polocation);
										nlapiLogExecution('ERROR', 'pocompanyid', pocompanyid);


										var fields = ['custrecord_ebizwhsite'];
										if(trantype=='purchaseorder')
											var locationcolumns = nlapiLookupField('Location', polocationid, fields);
										else
											var locationcolumns = nlapiLookupField('Location', polocationid, fields);

										var Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;

										nlapiLogExecution('ERROR', 'Tomwhsiteflag',Tomwhsiteflag);

										if(Tomwhsiteflag =='F')
										{
											var POarray=new Array();
											POarray ["custpage_msg"] =' " '+ polocation +' " is not a Ware house site.';
											POarray ["custpage_poid"] = poname;

											response.sendRedirect('SUITELET', 'customscript_autobreakdownsch_sl', 'customdeploy_autobreakdownsch_dl', false, POarray );
										}

										var palletqty = getpalletqty(Itemdimresults, itemid);
										var checkInQty = 0;              

										if(searchQtyResults != null && searchQtyResults != "")
										{ 
											nlapiLogExecution('ERROR', 'itemlineno', searchQtyResults.length);
											for(l=0; l < searchQtyResults.length; l++)
											{
												if (itemlineno == searchQtyResults[l].getValue('custrecord_orderlinedetails_orderline_no')) 
												{
													checkInQty = searchQtyResults[l].getValue('custrecord_orderlinedetails_checkin_qty');
													nlapiLogExecution('ERROR', 'Order Line Details Check-In Qty', checkInQty);
													continue;
												}
											}
										}      
										var itemSubtype = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

										if(checkInQty==null || checkInQty=='' )
											checkInQty = 0;      

										rcvquantity = parseFloat(checkInQty);

										nlapiLogExecution('ERROR', 'rcvquantity',rcvquantity);

										var vItemType=columns.recordType;

										var BatchFlag = columns.custitem_ebizbatchlot;
										nlapiLogExecution('ERROR', 'polineqty',polineqty);
										nlapiLogExecution('ERROR', 'checkInQty',checkInQty);
										nlapiLogExecution('ERROR', 'polinestatus',polinestatus);

										// If PO line quantity greater than already checked-in quantity then show the po line details in sub list
										if(parseFloat(polineqty)>parseFloat(checkInQty) && polinestatus!='36')
										{    
											var qty = parseFloat(polineqty)- parseFloat(checkInQty);

											form.getSubList('custpage_items').setLineItemValue('custpage_po', k, poid);
											form.getSubList('custpage_items').setLineItemValue('custpage_potext', k, poname);
											form.getSubList('custpage_items').setLineItemValue('custpage_line', k, itemlineno);
											form.getSubList('custpage_items').setLineItemValue('custpage_itemname', k, itemname);
											form.getSubList('custpage_items').setLineItemValue('custpage_itemid', k, itemid);
											form.getSubList('custpage_items').setLineItemValue('custpage_palletqty', k, parseFloat(palletqty).toFixed(5));	
											form.getSubList('custpage_items').setLineItemValue('custpage_iteminternalid', k, itemid);
											if(itemdesc!=null && itemdesc!='')
												itemdesc=itemdesc.substring(0, 200);

											form.getSubList('custpage_items').setLineItemValue('custpage_itemdesc', k, itemdesc);
											form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', k, itemstatus);
											form.getSubList('custpage_items').setLineItemValue('custpage_itempackcode', k, itempackcode);
											//form.getSubList('custpage_items').setLineItemValue('custpage_quantity', k, polineqty);    
											form.getSubList('custpage_items').setLineItemValue('custpage_quantity', k, qty);    
											form.getSubList('custpage_items').setLineItemValue('custpage_potranid', k, poname);
											if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') 
											{
												form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', k, poserialno);
											}
											else if (itemSubtype.recordType == 'lotnumberedinventoryitem' ||itemSubtype.recordType == 'lotnumberedassemblyitem' || itemSubtype.custitem_ebizbatchlot == 'T')
											{
												form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', k, poserialno);
												var lotnourl = nlapiResolveURL('SUITELET', 'customscript_ebiz_lot_creation_sl', 'customdeploy_ebiz_lot_creation_sl') + "&custparam_item=" + 
												itemid+"&custparam_itemstatus="+itemstatus+"&custparam_location="+polocationid+"&custparam_qty="+qty+"&custparam_line="+k;
												form.getSubList('custpage_items').setLineItemValue('custpage_lotbatchlink', k, "<a href=\"#\" onclick=\"javascript:window.open('" + lotnourl + "');\" >Multiple Lot Entry</a>");
											}
											form.getSubList('custpage_items').setLineItemValue('custpage_polocationid', k, polocationid);
											form.getSubList('custpage_items').setLineItemValue('custpage_pocompanyid', k, pocompanyid);
											form.getSubList('custpage_items').setLineItemValue('custpage_popackcode', k, itempackcode);
											form.getSubList('custpage_items').setLineItemValue('custpage_poitemstatus', k, itemstatus);
											form.getSubList('custpage_items').setLineItemValue('custpage_receiptid', k, '');
											form.getSubList('custpage_items').setLineItemValue('custpage_trailer', k, '');
											form.getSubList('custpage_items').setLineItemValue('custpage_poline',k,itemlineno);
											form.getSubList('custpage_items').setLineItemValue('custpage_poquantity',k,qty);
											//end of the code
											k++;
										} 
									}
								}
									}
								}
							}// end of PO line count loop
						}
					}// if (request.getParameter('custpage_po') != null) 

					nlapiLogExecution('ERROR','Remaining usage at the middel of post 1',context.getRemainingUsage());
					response.writePage(form);
				}
			}
		}
		if(tempflag=='true') 
		{
			var poname= request.getParameter('custpage_pono');
			var ordtype=request.getParameter('custpage_ordertype');
			var putconfirm = request.getParameter('custpage_confirmputaway');

			var lineCount = request.getLineItemCount('custpage_items'); //nlapiGetLineItemCount('custpage_items');
			nlapiLogExecution('ERROR','linecount',lineCount);
			var venteredfields="";
			var vlineno,vbatchnoentered,vbinlocationentered,vcartlpentered;
			var vlinearr = new Array();

			for (var i1 = 1; i1 <= lineCount; i1++)
			{			
				var brkdwnflag = request.getLineItemValue('custpage_items', 'custpage_brkdwnflag', i1);
				if(brkdwnflag == 'T')
				{
					vlineno =request.getLineItemValue('custpage_items', 'custpage_line', i1);
					vlinearr.push(vlineno);
					vbatchnoentered =request.getLineItemValue('custpage_items', 'custpage_lotbatch', i1);
					vbinlocationentered =request.getLineItemValue('custpage_items', 'custpage_chkn_binlocation', i1);
					vcartlpentered =request.getLineItemValue('custpage_items', 'custpage_cartlp', i1);
					if(venteredfields==""||venteredfields==null)
						venteredfields=vlineno+"&"+vbatchnoentered+"&"+vbinlocationentered+"&"+vcartlpentered+"$";
					else
						venteredfields=venteredfields+vlineno+"&"+vbatchnoentered+"&"+vbinlocationentered+"&"+vcartlpentered+"$";
				}
			}


			nlapiLogExecution('ERROR','venteredfields',venteredfields);
			nlapiLogExecution('ERROR','putconfirm',putconfirm);

			//case# 20124153 Start
			if(poname!=null && poname!='')
			{
				poid=GetPOInternalId(poname,ordtype);
			}
			var param = new Array();
			param['custscript_ebiz_autobrkpo'] = poid;
			param['custscript_ebiz_createinvt'] = putconfirm;
			param['custscript_ebiz_enteredvalue'] = venteredfields;

			//nlapiScheduleScript('customscript_ebiz_autobrkdown_sch_sl', 'customdeploy_ebiz_autobrkdown_sch_di',param);

			nlapiScheduleScript('customscript_ebiz_autobrkdown_sch_sl',null,param);
			var curuserId = context.getUser();	
			updateScheduleScriptStatus('AUTO BREAKDOWN',curuserId,'Submitted',poname,ordtype);

			try
			{
				updatePOline(poid,ordtype,vlinearr);
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR', 'Exception in updatePOline', exp);
			}
			var POarray=new Array();
			POarray ["custpage_msg"] ='AutoBreakdown process has been initiated successfully';
			POarray ["custpage_poid"] = poname;

			response.sendRedirect('SUITELET', 'customscript_autobreakdownsch_sl', 'customdeploy_autobreakdownsch_dl', false, POarray );
		}
		//}


	}// end of AutoBreakDown function
}

/**
 * 
 * @param orderQty
 * @param palletQty
 * @returns {Number}
 */
function getSKUDimCount(orderQty, dimQty){
	var retSKUDimCount = 0;

	if(dimQty > 0 && (parseFloat(orderQty) > parseFloat(dimQty)))
	{
		skuDimCount = parseFloat(orderQty)/parseFloat(dimQty);
		retSKUDimCount = Math.floor(skuDimCount);
		if(retSKUDimCount == 0)
			retSKUDimCount = 1;
	}

	return retSKUDimCount;
}

/**
 * 
 * @param itemID
 * @returns
 */
function getItemDimensions(itemID){
	//nlapiLogExecution('ERROR', 'In Item Dimensions', "here1");
	var retItemDimArray = null;

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemID));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomskudim');
	columns[3] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');
	columns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');
	columns[0].setSort(true);

	retItemDimArray = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, columns);
	nlapiLogExecution('ERROR', 'retItemDimArray', retItemDimArray.length);
	return retItemDimArray;
}

/**
 * To fetch inbound staging location
 * @returns {String}
 */
function getStagingLocation()
{
	var retInboundStagingLocn = "";

	/*	 
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}

/**********************************************
 * This function splits the line
 * based on Item Dimensions for each items
 * sublist in a Purchase Order.
 *
 */
function autobreakdown_palletisation(request)
{
	nlapiLogExecution('ERROR', 'Into autobreakdown_palletisation : Start', "here");	
	nlapiLogExecution('ERROR', 'Time Stamp at the start of autobreakdown_palletisation',TimeStampinSec());
	var poReceiptNo = "";
	var result = "T";
	var context = nlapiGetContext();
	try {
		var endloc = "";
		var beginloc = "";
		var stageLocn = "";
		var DockLoc = "";

		// Get inbound stage location		

		// *** The below code is commented by Satish.N on 02/06/2013 as part of performance tuning. *** //
		//stageLocn = getStagingLocation();
		// *** Upto here. *** //
		nlapiLogExecution('ERROR', 'stageLocn', stageLocn);

		// getting order specific information
		var lineCount = request.getLineItemCount('custpage_items'); //nlapiGetLineItemCount('custpage_items');
		nlapiLogExecution('ERROR','linecount',lineCount);

		var selecteditems = new Array();

		for (var s = 1; s <= lineCount; s++)
		{			
			var brkdwnflag = request.getLineItemValue('custpage_items', 'custpage_brkdwnflag', s);
			if(brkdwnflag == 'T')
			{
				selecteditems.push(request.getLineItemValue('custpage_items', 'custpage_itemid', s));
			}
		}

		nlapiLogExecution('ERROR', 'Selected Items Count', selecteditems.length);

		var AllItemDims = getItemDimensions(selecteditems);

		var poValue = "";

		//Creating record in throwaway parent
		nlapiLogExecution('ERROR','Remaining usage 1',context.getRemainingUsage());
		var newValue = new Date().getTime();
		var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record
		//var parentid = nlapiSubmitRecord(newParent); //save parent record
		nlapiLogExecution('ERROR','Remaining usage 2',context.getRemainingUsage());

		// Iterate through all the PO lines and break down
		for (var s = 1; s <= lineCount; s++)
		{		

			var brkdwnflag = request.getLineItemValue('custpage_items', 'custpage_brkdwnflag', s);
			if(brkdwnflag == 'T')
			{
				var poId = request.getLineItemValue('custpage_items','custpage_po',s);//PO Internal number		
				nlapiLogExecution('ERROR','POID',poId);
				var hCartLP = request.getParameter('custpage_hcartlp');
				var putconfirm = request.getParameter('custpage_confirmputaway');
				nlapiLogExecution('ERROR','putconfirm',putconfirm);
				var orderType =  nlapiLookupField('transaction', poId, 'recordType'); //It returns order type as: purchaseorder
				var poloc =  nlapiLookupField('transaction', poId, 'location');
				var expdate=nlapiLookupField('customrecord_ebiznet_batch_entry', poId, 'custrecord_ebizexpirydate');

				var tasktype = "";
				var wmsStsflag = "";
				var RcvQty = "";
				var uomlevel = "";
				var lotwithqty = "";
				var itemId = request.getLineItemValue('custpage_items', 'custpage_itemid', s);
				var ItemName = request.getLineItemValue('custpage_items', 'custpage_itemid', s);
				var lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
				var ItemStatus = request.getLineItemValue('custpage_items', 'custpage_itemstatus', s);
				var PackCode = request.getLineItemValue('custpage_items', 'custpage_itempackcode', s);
				var ItemDesc = request.getLineItemValue('custpage_items', 'custpage_itemdesc', s);
				var LotBatch = request.getLineItemValue('custpage_items', 'custpage_lotbatch', s);
				var binLocn = request.getLineItemValue('custpage_items','custpage_chkn_binlocation', s);
				var CartLP = request.getLineItemValue('custpage_items', 'custpage_cartlp', s);
				var PoLocation = request.getLineItemValue('custpage_items', 'custpage_polocationid', s);

				nlapiLogExecution('ERROR','PoLocation',PoLocation);
				//case 20124115 start
				nlapiLogExecution('ERROR','poloc',poloc);

				if(PoLocation !=null && PoLocation !='')
				{
					if(poloc != PoLocation)
						poloc= PoLocation;
				}

				nlapiLogExecution('ERROR','poloc',poloc);
				//end

				if(LotBatch != "" && LotBatch != null)
				{
					var filtersbatch = new Array();
					filtersbatch[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', LotBatch);

					var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbatch);
					var LotBatchId = "";
					if(SrchRecord != "" && SrchRecord != null)
					{
						LotBatchId = SrchRecord[0].getId();
					}
				}

				var ordQty;
				nlapiLogExecution('ERROR','request.getParameter(custpage_po)',request.getParameter('custpage_po'));
				if (request.getParameter('custpage_pono') != null)
					ordQty = request.getLineItemValue('custpage_items', 'custpage_quantity', s);
				else
					ordQty = request.getLineItemValue('custpage_items', 'custpage_rcvquantity', s);				
				nlapiLogExecution('ERROR','ordQty',ordQty);
				poValue = request.getLineItemValue('custpage_items', 'custpage_potranid', s); // Get PO Number
				var poRecId = request.getLineItemValue('custpage_items', 'custpage_receiptid', s); // Get POReceipt Id
				var poTrailer = request.getLineItemValue('custpage_items', 'custpage_trailer', s); // Get PO Receipt
				var poRec = request.getParameter('custpage_receiptfield');
				nlapiLogExecution('ERROR', 'itemId', itemId);

				if(CartLP == null || CartLP == "")
				{
					CartLP=hCartLP;					
				}
				nlapiLogExecution('ERROR', 'CartLP', CartLP);

				/* To fetch item Sub type.*/
				var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
				var columns = nlapiLookupField('item', itemId, fields);

				var lgItemType = columns.recordType;
				var batchflg = columns.custitem_ebizbatchlot;
				var itemfamId= columns.custitem_item_family;
				var itemgrpId= columns.custitem_item_group;


				// Get item dimentions for the item id					

				var eachQty = 0;
				var caseQty = 0;
				var palletQty = 0;
				var noofPallets = 0;
				var noofCases = 0;
				var remQty = 0;
				var tOrdQty = ordQty;
				var BaseUOMLevel=1;

				if (AllItemDims != null && AllItemDims.length > 0) 
				{							
					for(var p = 0; p < AllItemDims.length; p++)
					{
						var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
						var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');

						if(AllItemDims[p].getValue('custrecord_ebizitemdims') == itemId)
						{
							nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', itemId);

							var BaseUOM = AllItemDims[p].getValue('custrecord_ebizbaseuom');
							var skuDim = AllItemDims[p].getValue('custrecord_ebizuomlevelskudim');
							var skuQty = AllItemDims[p].getValue('custrecord_ebizqty');						
							if(BaseUOM =='T')
							{
								if(skuDim == '1')
								{
									BaseUOMLevel = "1"; 
								}
								else if(skuDim == '2')
								{
									BaseUOMLevel = "2";
								}
								else if(skuDim == '3')
								{
									BaseUOMLevel = "3";
								}

							}
							nlapiLogExecution('ERROR','BaseUOM',BaseUOM);
							nlapiLogExecution('ERROR','BaseUOMLevel',BaseUOMLevel);
							nlapiLogExecution('ERROR','skuDim',skuDim);
							nlapiLogExecution('ERROR','SKUQTY',skuQty);
							if(skuDim == '1')
							{
								eachQty = skuQty;
							}
							else if(skuDim == '2')
							{
								caseQty = skuQty;
							}
							else if(skuDim == '3')
							{
								palletQty = skuQty;
							}

						}									
					}

					if(parseFloat(eachQty) == 0)
					{
						nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim EACH is not configured for SKU: '+ itemId);
					}
					if(parseFloat(caseQty) == 0)
					{
						nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim CASE is Not Configured for SKU: '+ itemId);
					}
					if(parseFloat(palletQty) == 0)
					{
						nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim PALLET is not Configured for SKU: '+ itemId);
					}
					if(eachQty>0 ||caseQty>0 || palletQty>0)  // break down selected PO line quantity provided Item dims are configured
					{					
						//nlapiLogExecution('ERROR', 'Item', itemId);
						nlapiLogExecution('ERROR', 'Each Qty', eachQty);
						nlapiLogExecution('ERROR', 'Case Qty', caseQty);
						nlapiLogExecution('ERROR', 'Pallet Qty', palletQty);
						nlapiLogExecution('ERROR', 'Ord Qty', ordQty);

						// calculate how many LP's are required to break down selected PO line
						var noofLpsRequired = 0;

						// Get the number of pallets required for this item (there will be one LP per pallet)
						noofPallets = getSKUDimCount(ordQty, palletQty);
						remQty = parseFloat(ordQty) - (parseFloat(noofPallets) * parseFloat(palletQty));	

						nlapiLogExecution('ERROR', 'autobreakdown_palletisation:No. of Pallets', noofPallets);
						nlapiLogExecution('ERROR', 'autobreakdown_palletisation:Remaining Quantity', remQty);

						noofLpsRequired = parseFloat(noofPallets);

						// check whether we need to break down into cases or not
						if (parseFloat(remQty) > 0 && parseFloat(caseQty) != 0) 
						{
							// Get the number of cases required for this item (only one LP for all cases)
							noofCases = getSKUDimCount(remQty, caseQty);
							remQty = parseFloat(remQty) - (noofCases * caseQty);
							if(noofCases > 0)
								noofLpsRequired = parseFloat(noofLpsRequired) + 1;
							nlapiLogExecution('ERROR', 'autobreakdown_palletisation:No. of Cases', noofCases);
							nlapiLogExecution('ERROR', 'autobreakdown_palletisation:Remaining Quantity', remQty);
						}

						// check whether we need to further break down into eaches or not (there will be one LP for all eaches)
						if (parseFloat(remQty) > 0)  
							noofLpsRequired = parseFloat(noofLpsRequired) + 1;


						nlapiLogExecution('ERROR', 'No. Of LPs required for line#:' + lineno +'=', noofLpsRequired);

						// In single call we are updating MaxLP in "LP range" custom record with required no of lp's					

						var MultipleLP = GetMultipleLP(1,1,noofLpsRequired,poloc);


						var maxLPPrefix = MultipleLP.split(",")[0];
						var maxLP = MultipleLP.split(",")[1];;
						maxLP = parseFloat(maxLP) - parseFloat(noofLpsRequired); 

						nlapiLogExecution('ERROR', 'MAX LP:', maxLP);

						nlapiLogExecution('ERROR', 'noofPallets', noofPallets);
						var totalrcvqty=0;
						for (var p = 0; p < noofPallets; p++) 
						{
							// Calculate LP
							maxLP = parseFloat(maxLP) + 1;
							var LP = maxLPPrefix + maxLP.toString();
							nlapiLogExecution('ERROR', 'Pallet LP', LP);

							nlapiLogExecution('ERROR', 'RcvQty', RcvQty);
							nlapiLogExecution('ERROR', 'palletQty', palletQty);

							if(RcvQty=='' || RcvQty==null)
								RcvQty=0;

							//RcvQty = parseFloat(RcvQty)+parseFloat(palletQty);
							RcvQty = parseFloat(palletQty);
							totalrcvqty=parseFloat(totalrcvqty)+parseFloat(RcvQty);

							nlapiLogExecution('ERROR', 'RcvQty', RcvQty);

							tasktype = "1"; // Task = CHECKIN
							wmsStsflag = "1"; // WMS Status Flag = INBOUND / CHECK-IN
//							beginloc = "";
							beginloc=stageLocn;
							endloc = stageLocn;
							uomlevel = "3"; // UOM Level = Pallet

							lotwithqty = LotBatch + "(" + RcvQty + ")";

							CreateLPMaster(LP,CartLP,parent);

							// *** The below code is commented by Satish.N on 02/06/2013 as part of performance tuning. *** //

							/*var recID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
									uomlevel, tasktype, beginloc, endloc, poId, poReceiptNo, wmsStsflag, ItemName, ItemDesc, 
									LotBatchId,LotBatch, CartLP, poloc, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,parent);*/

							// *** The below code is commented by Satish.N on 02/06/2013 as part of performance tuning. *** //

							tasktype = "2"; // Task = PUTAWAY
							wmsStsflag = "6"; // WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
							beginloc = "";
							if (binLocn != null)
								beginloc=binLocn;
							endloc = "";
							if(putconfirm!='T')
							{
								var putrecID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
										uomlevel, tasktype, beginloc, endloc, poId, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
										LotBatchId,LotBatch, CartLP, poloc, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,parent);
							}

						}

						nlapiLogExecution('ERROR', 'Total RcvQty', totalrcvqty);

						if(parseFloat(totalrcvqty)>0)
							TrnLineUpdationNew(orderType, "CHKN", poValue, poId, lineno, itemId,ordQty,totalrcvqty,"",ItemStatus,parent);

						//for (var p = 0; p < noofCases; p++)
						nlapiLogExecution('ERROR', 'noofCases', noofCases);
						if(parseFloat(noofCases) > 0)
						{
							// Calculate LP
							maxLP = parseFloat(maxLP) + 1;
							var LP = maxLPPrefix + maxLP.toString();
							nlapiLogExecution('ERROR', 'Case LP', LP);							

							RcvQty = parseFloat(noofCases) * parseFloat(caseQty);

							tasktype = "1"; // Task = CHECKIN
							wmsStsflag = "1"; // WMS Status Flag = INBOUND / CHECK-IN
//							beginloc = "";
							beginloc=stageLocn;
							endloc = stageLocn;
							uomlevel = "2"; // UOM Level = Case

							lotwithqty = LotBatch + "(" + RcvQty + ")";

							CreateLPMaster(LP,CartLP,parent);

							// *** The below code is commented by Satish.N on 02/06/2013 as part of performance tuning. *** //

							/*var recID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
									uomlevel, tasktype, beginloc, endloc, poId, poReceiptNo, wmsStsflag, ItemName, ItemDesc, 
									LotBatchId,LotBatch, CartLP, poloc, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,parent);*/

							// *** Upto here. *** //
							if(putconfirm!='T')
							{
								TrnLineUpdationNew(orderType, "CHKN", poValue, poId, lineno, itemId,ordQty,RcvQty,"",ItemStatus,parent);

								tasktype = "2"; // Task = PUTAWAY
								wmsStsflag = "6"; // WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
								beginloc = "";
								if (binLocn != null)
									beginloc=binLocn;
								endloc = "";

								var putrecID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
										uomlevel, tasktype, beginloc, endloc, poId, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
										LotBatchId,LotBatch,CartLP, poloc, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,parent);
							}
						}

						nlapiLogExecution('ERROR', 'remQty', remQty);
						//alert("Rem Qty After Casses :" +remQty);
						if (parseFloat(remQty) > 0) 
						{
							// Calculate LP
							maxLP = parseFloat(maxLP) + 1;
							var LP = maxLPPrefix + maxLP.toString();
							nlapiLogExecution('ERROR', 'Each LP', LP);

							tasktype = "1"; // Task = CHECKIN
							wmsStsflag = "1"; // WMS Status Flag = INBOUND / CHECK-IN
							RcvQty = remQty;
//							beginloc = "";
							beginloc=stageLocn;
							endloc = stageLocn;
							//uomlevel = "1"; // UOM = EACH
							lotwithqty = LotBatch + "(" + RcvQty + ")";
							uomlevel = BaseUOMLevel; // UOM = EACH
							CreateLPMaster(LP,CartLP,parent);

							// *** The below code is commented by Satish.N on 02/06/2013 as part of performance tuning. *** //

							/*var recID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
									uomlevel, tasktype, beginloc, endloc, poId, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
									LotBatchId,LotBatch,CartLP, poloc, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,parent)*/;

									// *** Upto here. *** //
									if(putconfirm!='T')
									{
										TrnLineUpdationNew(orderType, "CHKN", poValue, poId, lineno, itemId,ordQty,RcvQty,"Y",ItemStatus,parent);

										tasktype = "2"; // Task = PUTAWAY
										wmsStsflag = "6"; // WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
										beginloc = "";
										if (binLocn != null)
											beginloc=binLocn;
										endloc = "";

										var putrecID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
												uomlevel, tasktype, beginloc, endloc, poId, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
												LotBatchId,LotBatch,CartLP, poloc, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,parent);
									}


						}
						var confirmputrecID;
						if(putconfirm=='T')
						{
							tasktype = "2"; // Task = PUTAWAY
							wmsStsflag = "3"; // WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
							beginloc = "";
							if (binLocn != null)
								beginloc=binLocn;
							TrnLineUpdationNew(orderType, "PUTW", poValue, poId, lineno, itemId,ordQty,RcvQty,"",ItemStatus,parent);
							confirmputrecID = CreateOpenTaskRecordwithconfrim(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
									uomlevel, tasktype, beginloc, endloc, poId, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
									LotBatchId,LotBatch,CartLP, poloc, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,parent);
							nlapiLogExecution('ERROR','confirmputrecID',confirmputrecID);

						}
					}// end of auto break down calculation
				} 
				else 
				{ 
					nlapiLogExecution('ERROR', 'UOM Dims', 'For all items, Dimensions are not configured ');
				}			

				if(poRecId!=null&&poRecId!='')
				{
					var poReceiptRec=nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt',poRecId);
					var previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_receiptqty');
					if(previousrcvqty==null||previousrcvqty=='')
						previousrcvqty=0;
					nlapiLogExecution('ERROR','previousrcvqty',previousrcvqty);
					nlapiLogExecution('ERROR','ordQty',ordQty);
					var newrcvQty= parseFloat(previousrcvqty)+parseFloat(ordQty);
					nlapiLogExecution('ERROR','newrcvQty',newrcvQty);
					var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', poRecId, 'custrecord_ebiz_poreceipt_receiptqty',newrcvQty.toString());
					nlapiLogExecution('ERROR','id',id);
				}
				var  printername=request.getParameter('custpage_print');
				nlapiLogExecution('ERROR','poRecId',poRecId);
				nlapiLogExecution('ERROR','poRec',poRec);
				nlapiLogExecution('ERROR','poReceiptNo',poReceiptNo);
				nlapiLogExecution('ERROR','LP',LP);
				//generatePalletLabel(poId,poloc,poTrailer,printername,itemId,poRecId,ItemDesc,LP);
				//generatePalletLabel(poId,poloc,poTrailer,printername);
				var filters=new Array();
				filters.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemId));
				var columns=new Array();
				columns[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
				var list=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filters,columns);
				var expdate="";
				if(list!=null && list!=''){
					for(var count=0;count<list.length;count++)
					{
						var lineNo=count+1;
						var listresult=list[count];
						expdate=listresult.getValue('custrecord_ebizexpirydate');

					}
				}
				
				
				
				generateQALabel(poId,ItemName,ItemDesc,LP,LotBatch,lineCount,expdate);

			}//End of: if(brkdwnflag == 'T')   
		}// End of: for (var s = 1; s <= lineCount; s++)
		nlapiLogExecution('ERROR','Before parent',parent);
		nlapiSubmitRecord(parent); //submit the parent record, all child records will also be updated
		nlapiLogExecution('ERROR',' After parent',parent);
		MovingChknTaskToClosedTask(poId,poReceiptNo,poTrailer);
		nlapiLogExecution('ERROR','Remaining usage 4',context.getRemainingUsage());

	} catch(exp) 
	{
		//alert('Expception'+exp);
		nlapiLogExecution('ERROR', 'Exception in autobreakdown_palletisation:', exp);		
		result = "F";
	}

	nlapiLogExecution('ERROR', 'Time Stamp at the end of autobreakdown_palletisation',TimeStampinSec());
	if(confirmputrecID!='' && confirmputrecID!='')
	{
		nlapiLogExecution('ERROR','test','test');
		nlapiSetRedirectURL('RECORD', 'transferorder', poId, false);
	}
	return result;
}


function CreateLPMaster(LP,CartLP,parent)
{
	/*var lprecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
	lprecord.setFieldValue('name', LP);
	lprecord.setFieldValue('custrecord_ebiz_lpmaster_lp', LP);		
	lprecord.setFieldValue('custrecord_ebiz_lpmaster_masterlp', CartLP);	

	var recid = nlapiSubmitRecord(lprecord);*/

	parent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');


	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', LP);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', LP);		
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_masterlp', CartLP);	

	parent.commitLineItem('recmachcustrecord_ebiz_lp_parent');

}

function CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus, BaseUOM, 
		tasktype, beginloc, endloc, poId, poreceiptno, wmsStsflag, ItemName, ItemDesc, LotBatch,LotBatchtext, CartLP, poloc,
		lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,parent)
{
	nlapiLogExecution('ERROR','create opentask records');
	nlapiLogExecution('ERROR','parent' ,parent);
	nlapiLogExecution('ERROR','NAME',poValue);
	nlapiLogExecution('ERROR','poRec',poRec);
	nlapiLogExecution('ERROR','poTrailer',poTrailer);
	nlapiLogExecution('ERROR','wmsStsflag',wmsStsflag);
	nlapiLogExecution('ERROR','tasktype',tasktype);
	nlapiLogExecution('ERROR','BaseUOM',BaseUOM);
	try{
		var receiptno='';
		parent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', poValue);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_concept_po', poValue);		
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', itemId);	
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', itemId);	
//		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', ordQty);		
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', parseFloat(RcvQty).toFixed(5));
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', LP);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', LP);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', lineno);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', PackCode);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku_status', ItemStatus);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_ot_receipt_no', poRecId);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ot_receipt_no', poRec);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_trailer_no', poTrailer);

		var currentUserID = getCurrentUser();
		nlapiLogExecution('ERROR','currentUserID',currentUserID);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', currentUserID);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizuser', currentUserID);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', currentUserID);
		var uomid="";
		if(BaseUOM == 1)
			uomid = "EACH";
		else if(BaseUOM == 2)
			uomid = "CASE";
		else
			uomid = "PALLET";

		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_id', uomid);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', tasktype);
		if(tasktype == "2")
		{
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
		}

		if (tasktype == "1") // Task = CHECKIN
		{ 
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', parseFloat(RcvQty).toFixed(5));
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', endloc);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date', DateStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());

			//Adding fields to update time zones.
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', TimeStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordtime', TimeStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());			
		}

		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_current_date', DateStamp());
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());		
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', poId);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', parseFloat(poId));
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_receipt_no', poRecId);

		//Status flag .
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag',wmsStsflag );

		//Added for Item name and desc
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_skudesc', ItemDesc);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_transport_lp', CartLP);

		//LotBatch and batch with Quantity.
		if(lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflg == "T"){
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', LotBatchtext);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lotnowithquantity', lotwithqty);
		}

		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', poloc);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_fifodate', DateStamp());

		//Assigning Sku dims level. 
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizuomlevelskudim', uomlevel);

		//commit the record to NetSuite
		//var recid = nlapiSubmitRecord(customrecord,false, true);
		parent.commitLineItem('recmachcustrecord_ebiz_ot_parent');
		//nlapiLogExecution('ERROR','Create Open task record ID',recid);
		//alert('TRN_OPENTASK Success');
		nlapiLogExecution('ERROR','UpdateSerialEntry',"UpdateSerialEntry"); 
		nlapiLogExecution('ERROR','ordQty',ordQty);
		nlapiLogExecution('ERROR','RcvQty',RcvQty);
		//Issue No: 20123032 start
		UpdateSerialEntry(poId,lineno,itemId,LP,RcvQty);
		//Issue No: 20123032 end
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Create Open task record Fail',exp);
	}

	// *** The below code is commented by Satish.N on 02/06/2013 as part of performance tuning. *** //

//	nlapiLogExecution('ERROR', 'Create Inventory Record Insertion', 'start');
//	var invtRecordId = createInvtRecord(tasktype, poId, endloc, LP, itemId,
//	ItemStatus, ItemDesc, RcvQty, poloc, lgItemType, PackCode,batchflg,LotBatch,batchflg,parent);
//	nlapiLogExecution('ERROR', 'Created Inventory Record ID', invtRecordId);

	// *** Upto here. *** //
	return 'success';	
}



function CreateOpenTaskRecordwithconfrim(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus, BaseUOM, 
		tasktype, beginloc, endloc, poId, poreceiptno, wmsStsflag, ItemName, ItemDesc, LotBatch,LotBatchtext, CartLP, poloc,
		lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,parent)
{
	nlapiLogExecution('ERROR','create opentask records test');
	nlapiLogExecution('ERROR','parent' ,parent);
	nlapiLogExecution('ERROR','NAME',poValue);
	nlapiLogExecution('ERROR','poRec',poRec);
	nlapiLogExecution('ERROR','poTrailer',poTrailer);
	nlapiLogExecution('ERROR','wmsStsflag',wmsStsflag);
	nlapiLogExecution('ERROR','tasktype',tasktype);
	nlapiLogExecution('ERROR','BaseUOM',BaseUOM);

	var tranType = nlapiLookupField('transaction', poId, 'recordType');
	//tranType = columns.recordType;
	fromrecord = tranType;


	idl=generateItemReceipt(fromrecord,poId,ordQty,lineno,ItemStatus,itemId,tranType,poId,LP,"","","",LotBatch,"",poloc,"");
	nlapiLogExecution('ERROR','idl',idl);

	if(idl!=null && idl!='')
	{
		try{
			var receiptno='';
			parent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', poValue);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_concept_po', poValue);		
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', itemId);	
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', itemId);	
//			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', ordQty);		
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', parseFloat(RcvQty).toFixed(5));
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', LP);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', LP);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', lineno);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', PackCode);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku_status', ItemStatus);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_ot_receipt_no', poRecId);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ot_receipt_no', poRec);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_trailer_no', poTrailer);

			var currentUserID = getCurrentUser();
			nlapiLogExecution('ERROR','currentUserID',currentUserID);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', currentUserID);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizuser', currentUserID);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', currentUserID);
			var uomid="";
			if(BaseUOM == 1)
				uomid = "EACH";
			else if(BaseUOM == 2)
				uomid = "CASE";
			else
				uomid = "PALLET";

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_id', uomid);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', tasktype);
			if(tasktype == "2")
			{
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', parseFloat(RcvQty).toFixed(5));			
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', beginloc);
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date', DateStamp());
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());
			}

			if (tasktype == "1") // Task = CHECKIN
			{ 
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', parseFloat(RcvQty).toFixed(5));
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', beginloc);
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date', DateStamp());
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());

				//Adding fields to update time zones.
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', TimeStamp());
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordtime', TimeStamp());
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());			
			}

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_current_date', DateStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());		
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', poId);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', parseFloat(poId));
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_receipt_no', poRecId);

			//Status flag .
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag',wmsStsflag );

			//Added for Item name and desc
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_skudesc', ItemDesc);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_transport_lp', CartLP);

			//LotBatch and batch with Quantity.
			if(lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflg == "T"){
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', LotBatchtext);
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lotnowithquantity', lotwithqty);
			}

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', poloc);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_fifodate', DateStamp());

			//Assigning Sku dims level. 
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizuomlevelskudim', uomlevel);

			//commit the record to NetSuite
			//var recid = nlapiSubmitRecord(customrecord,false, true);
			parent.commitLineItem('recmachcustrecord_ebiz_ot_parent');
			//nlapiLogExecution('ERROR','Create Open task record ID',recid);
			//alert('TRN_OPENTASK Success');
			nlapiLogExecution('ERROR','UpdateSerialEntry',"UpdateSerialEntry"); 
			nlapiLogExecution('ERROR','ordQty',ordQty);
			nlapiLogExecution('ERROR','RcvQty',RcvQty);
			//Issue No: 20123032 start
			UpdateSerialEntry(poId,lineno,itemId,LP,RcvQty);
			//Issue No: 20123032 end


			//create inventory

			nlapiLogExecution('ERROR','beginloc',beginloc);


			var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

//			var filters=new Array();
//			filters.push(new nlobjSearchFilter('internalid', null, 'is', beginloc));
//			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
//			filters.push(new nlobjSearchFilter('isinactive', 'custrecord_inboundlocgroupid', 'is', 'F'));
//			filters.push(new nlobjSearchFilter('isinactive', 'custrecord_outboundlocgroupid', 'is', 'F'));
//			var columns=new Array();
//			columns.push(new nlobjSearchColumn('custrecord_inboundlocgroupid'));
//			columns.push(new nlobjSearchColumn('custrecord_outboundlocgroupid'));
//			var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
//			var inblocgroupid='';var outlocgroupid='';
//			if(searchresults != null && searchresults != '' && searchresults.length>0)
//			{	
//			nlapiLogExecution('Error', 'Into Update location groups');
//			inblocgroupid = searchresults[0].getValue('custrecord_inboundlocgroupid');
//			outlocgroupid = searchresults[0].getValue('custrecord_outboundlocgroupid');
//			}
			invtRec.setFieldValue('name', LP);
			invtRec.setFieldValue('custrecord_ebiz_inv_binloc', beginloc);
			//invtRec.setFieldValue('custrecord_inboundinvlocgroupid', inblocgroupid);
			//invtRec.setFieldValue('custrecord_outboundinvlocgroupid', outlocgroupid);

			invtRec.setFieldValue('custrecord_ebiz_inv_lp', LP);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemId);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatus);
			if(PackCode!=null && PackCode!="")
				invtRec.setFieldValue('custrecord_ebiz_inv_packcode', PackCode);
			invtRec.setFieldValue('custrecord_ebiz_inv_qty', RcvQty);

			invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemId);
			invtRec.setFieldValue('custrecord_ebiz_qoh', RcvQty);
			//invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
			invtRec.setFieldValue('custrecord_ebiz_inv_loc', poloc);
			invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
			//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
			invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
			invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
			invtRec.setFieldValue('custrecord_invttasktype', '2');
			invtRec.setFieldValue('custrecord_ebiz_transaction_no', poId);


			var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
			var columns = nlapiLookupField('item', itemId, fields);
			var ItemType = columns.recordType;					
			var batchflg = columns.custitem_ebizbatchlot;
			var itemfamId= columns.custitem_item_family;
			var itemgrpId= columns.custitem_item_group;
			nlapiLogExecution('Error', 'ItemType', ItemType);
			nlapiLogExecution('Error', 'batchflg', batchflg);
			//nlapiLogExecution('Error', 'batchno', batchno);

//			if(LotBatch!=null && LotBatch!='')
//			{
//			var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
//			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
//			}
//			if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
////			if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
//			{ 										

//			//Checking FIFO Policy.					
//			if (LotBatch != "") {
//			fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
//			nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
//			invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
//			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
//			invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
//			}

//			}
//			else
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());


			nlapiLogExecution('Error', 'Before Submitting invtrecid', 'INVTRECORDS');

			var invtrecid = nlapiSubmitRecord(invtRec, false, true);



		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Create Open task record Fail',exp);
		}
	}
	else
	{
		try{
			var receiptno='';
			parent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', poValue);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_concept_po', poValue);		
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', itemId);	
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', itemId);	
//			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', ordQty);		
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', parseFloat(RcvQty).toFixed(5));
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', LP);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', LP);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', lineno);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', PackCode);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku_status', ItemStatus);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_ot_receipt_no', poRecId);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ot_receipt_no', poRec);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_trailer_no', poTrailer);

			var currentUserID = getCurrentUser();
			nlapiLogExecution('ERROR','currentUserID',currentUserID);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', currentUserID);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizuser', currentUserID);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', currentUserID);
			var uomid="";
			if(BaseUOM == 1)
				uomid = "EACH";
			else if(BaseUOM == 2)
				uomid = "CASE";
			else
				uomid = "PALLET";

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_id', uomid);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', tasktype);
			if(tasktype == "2")
			{
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
			}

			if (tasktype == "1") // Task = CHECKIN
			{ 
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', parseFloat(RcvQty).toFixed(5));
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', endloc);
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date', DateStamp());
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());

				//Adding fields to update time zones.
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', TimeStamp());
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordtime', TimeStamp());
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());			
			}

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_current_date', DateStamp());
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());		
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', poId);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', parseFloat(poId));
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_receipt_no', poRecId);

			//Status flag .
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag',wmsStsflag );

			//Added for Item name and desc
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_skudesc', ItemDesc);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_transport_lp', CartLP);

			//LotBatch and batch with Quantity.
			if(lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflg == "T"){
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', LotBatchtext);
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lotnowithquantity', lotwithqty);
			}

			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', poloc);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_fifodate', DateStamp());

			//Assigning Sku dims level. 
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizuomlevelskudim', uomlevel);

			//commit the record to NetSuite
			//var recid = nlapiSubmitRecord(customrecord,false, true);
			parent.commitLineItem('recmachcustrecord_ebiz_ot_parent');
			//nlapiLogExecution('ERROR','Create Open task record ID',recid);
			//alert('TRN_OPENTASK Success');
			nlapiLogExecution('ERROR','UpdateSerialEntry',"UpdateSerialEntry"); 
			nlapiLogExecution('ERROR','ordQty',ordQty);
			nlapiLogExecution('ERROR','RcvQty',RcvQty);
			//Issue No: 20123032 start
			UpdateSerialEntry(poId,lineno,itemId,LP,RcvQty);
			//Issue No: 20123032 end
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Create Open task record Fail',exp);
		}
	}

	// *** The below code is commented by Satish.N on 02/06/2013 as part of performance tuning. *** //

//	nlapiLogExecution('ERROR', 'Create Inventory Record Insertion', 'start');
//	var invtRecordId = createInvtRecord(tasktype, poId, endloc, LP, itemId,
//	ItemStatus, ItemDesc, RcvQty, poloc, lgItemType, PackCode,batchflg,LotBatch,batchflg,parent);
//	nlapiLogExecution('ERROR', 'Created Inventory Record ID', invtRecordId);

	// *** Upto here. *** //
	return 'success';	
}
function generatePalletLabel(poId,poloc,poTrailer,printername,itemId,reciptno,skudesc,LP)
{
	try
	{
		nlapiLogExecution('ERROR', 'cartonlabel', 'PALLETLABEL');
		nlapiLogExecution('ERROR', 'cartonlabelpoValue', poId);
		nlapiLogExecution('ERROR', 'cartonlabelpoTrailer',poTrailer);
		nlapiLogExecution('ERROR', 'cartonlabelpopoloc', poloc);
		nlapiLogExecution('ERROR', 'itemId', itemId);
		nlapiLogExecution('ERROR', 'reciptno', reciptno);
		nlapiLogExecution('ERROR', 'LP', LP);

		nlapiLogExecution('ERROR', 'skudesc', skudesc);
		var username;
		var skuname;
		var palletlabel="";
		var labeltype="PALLETLABEL";
		//var tasktype=1;
		var tasktype=2;
		var filtertempalte = new Array();
		filtertempalte.push(new nlobjSearchFilter('custrecord_labeltemplate_type',null,'is',labeltype));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_labeltemplate_data');
		var searchtemplate= nlapiSearchRecord('customrecord_label_templates', null,filtertempalte,columns);
		if((searchtemplate !=null) &&(searchtemplate!=""))
		{
			palletlabel=searchtemplate[0].getValue('custrecord_labeltemplate_data');
		}
		var print ="F";
		var reprint="F";
		//var tasktype=1;
		var tasktype=2;
		var splitPonumber="";
		var userid = getCurrentUser();
		nlapiLogExecution('ERROR','currentUserID',userid);
		var Emprecord = nlapiLoadRecord('Employee', userid);
		username=Emprecord.getFieldValue('entityid');

		var recevieddate=DateStamp();
		nlapiLogExecution('ERROR', 'recevieddate', recevieddate);
		//var username,skuname,skudesc,exptqty,ponumber,recevieddate;
		//var reciptno;
		/*var filters= new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null,'is',poId));
		//filters.push(new nlobjSearchFilter('custrecord_sku',null,'anyof',itemId));
	//	filters.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no',null,'is',poTrailer));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_sku');
		columns[1] = new nlobjSearchColumn('custrecord_ebizuser');
		columns[2] = new nlobjSearchColumn('custrecord_skudesc');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[5] = new nlobjSearchColumn('custrecord_current_date');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
		if(searchresults!=null && searchresults!='' )
		{
			nlapiLogExecution('ERROR', 'test','test');
			skuname =searchresults[0].getText('custrecord_sku');
			username=searchresults[0].getText('custrecord_ebizuser');
			skudesc=searchresults[0].getValue('custrecord_skudesc');
			reciptno=searchresults[0].getValue('custrecord_ot_receipt_no');
			ponumber=searchresults[0].getText('custrecord_ebiz_order_no');
			recevieddate=searchresults[0].getValue('custrecord_current_date');
			nlapiLogExecution('ERROR', 'cartonlabelponumber',ponumber);
			nlapiLogExecution('ERROR', 'skuname',skuname);
			nlapiLogExecution('ERROR', 'username',username);
			nlapiLogExecution('ERROR', 'skudesc',skudesc);
			nlapiLogExecution('ERROR', 'reciptno',reciptno);
			splitPonumber=ponumber.split('#')[1];
		    nlapiLogExecution('ERROR', 'palletlabelskudesc2', skudesc);

		}*/
		var filteritems = new Array();
		filteritems.push(new nlobjSearchFilter('internalid', null,'is',itemId));
		var columnitems = new Array();
		columnitems[0] = new nlobjSearchColumn('custitem_ebizdescriptionitems');
		columnitems[1] = new nlobjSearchColumn('itemid');
		var searchresultitem = nlapiSearchRecord('item',null,filteritems,columnitems);
		if(searchresultitem!=null)
		{
			skudesc=searchresultitem[0].getValue('custitem_ebizdescriptionitems');
			skuname=searchresultitem[0].getValue('itemid');
		}
		nlapiLogExecution('ERROR', 'palletlabelskudesc', skudesc);
		nlapiLogExecution('ERROR', 'skuname', skuname);
		if((recevieddate!=null)&&(recevieddate!=""))
		{
			palletlabel=palletlabel.replace(/Parameter26/,recevieddate);

		}
		else
		{
			palletlabel=palletlabel.replace(/Parameter26/,recevieddate);
		}
		if((skuname!=null)&&(skuname!=""))
		{
			palletlabel=palletlabel.replace(/Parameter27/g,skuname);
		}
		else
		{
			palletlabel=palletlabel.replace(/Parameter27/g,'');
		}

		if((username!=null)&&(username!=""))
		{
			palletlabel=palletlabel.replace(/Parameter31/,username);
		}
		else
		{
			palletlabel=palletlabel.replace(/Parameter31/,'');
		}
		if((skudesc!=null)&&(skudesc!=""))
		{
			palletlabel=palletlabel.replace(/Parameter28/,skudesc);
		}
		else
		{
			palletlabel=palletlabel.replace(/Parameter28/,'');
		}
		if((reciptno!=null)&&(reciptno!=""))
		{
			palletlabel=palletlabel.replace(/parameter30/,poTrailer);
		}
		else if((splitPonumber!=null)&&(splitPonumber!=""))
		{
			palletlabel=palletlabel.replace(/parameter30/,splitPonumber);
		}
		else
		{
			palletlabel=palletlabel.replace(/parameter30/,'');
		}
		CreateLabelData(palletlabel,labeltype,tasktype,reciptno,print,reprint,"",poloc,poTrailer,"",skuname,"",printername,LP);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','PalletLabelGenerationException',exp);
	}

}
//code for creating QA label
function generateQALabel(poId,ItemName,ItemDesc,LP,LotBatch,linecount,expdate)
{
	try
	{
		nlapiLogExecution('ERROR', 'cartonlabel', 'QALABEL');
		nlapiLogExecution('ERROR', 'cartonlabelpoValue', poId);
		nlapiLogExecution('ERROR', 'ItemName', ItemName);
		nlapiLogExecution('ERROR', 'skudesc', ItemDesc);
		nlapiLogExecution('ERROR', 'linecount', linecount);
		nlapiLogExecution('ERROR', 'LP', LP);
		nlapiLogExecution('ERROR', 'expdate', expdate);
		var recevieddate=DateStamp();
		nlapiLogExecution('ERROR', 'recevieddate', recevieddate);
		var eXtlabelrecord = nlapiCreateRecord('customrecord_ext_labelprinting');
		eXtlabelrecord.setFieldValue('name',poId);
		eXtlabelrecord.setFieldValue('custrecord_label_labeltype','QAlabel');
		eXtlabelrecord.setFieldValue('custrecord_label_item',ItemName);
		eXtlabelrecord.setFieldValue('custrecord_label_itemdesc',ItemDesc);
		eXtlabelrecord.setFieldValue('custrecord_label_reference1',linecount); 
		eXtlabelrecord.setFieldValue('custrecord_label_reference2',recevieddate);
		eXtlabelrecord.setFieldValue('custrecord_label_reference3',LotBatch);
		eXtlabelrecord.setFieldValue('custrecord_label_reference4',expdate);
		var tranid = nlapiSubmitRecord(eXtlabelrecord);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','QAGenerationException',exp);
	}

}
function CreateLabelData(labeldata,labeltype,tasktype,refno,print,reprint,company,location,name,labelcount,skuname,totalqty,printername,LP)
{
	nlapiLogExecution('ERROR','RecordCreation','RecordCreation');
	var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
	labelrecord.setFieldValue('name', LP); 
	labelrecord.setFieldValue('custrecord_labeldata',labeldata);  
	labelrecord.setFieldValue('custrecord_label_refno',refno);     
	labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);//chkn task   
	labelrecord.setFieldValue('custrecord_labeltype',labeltype);                                                                                                                                                                     labelrecord.setFieldValue('custrecord_labeldata', labeldata);
	labelrecord.setFieldValue('custrecord_label_print', print);
	labelrecord.setFieldValue('custrecord_label_reprint', reprint);
	labelrecord.setFieldValue('custrecord_label_company', company);
	labelrecord.setFieldValue('custrecord_label_location', location);
	labelrecord.setFieldValue('custrecord_label_lp', LP);
	labelrecord.setFieldValue('custrecord_label_skuname', skuname);
	labelrecord.setFieldValue('custrecord_label_adresslabel_generation', labelcount);
	labelrecord.setFieldValue('custrecord_label_printername', printername);
	labelrecord.setFieldValue('custrecord_label_itemqty', totalqty);
	var tranid = nlapiSubmitRecord(labelrecord);
} 
function createInvtRecord(taskType, po, endLocn, lp, itemId, itemStatus, itemDesc,
		receiveQty, poLocn, lgItemType, packCode,batchflag,LotBatch,batchflg,parent) 
{
	// Creating inventory for taskType = 1 (CHKN)
	var sysdate=DateStamp();
	if (taskType == "1") 
	{
		try 
		{
			// Creating Inventory Record.		
			parent.selectNewLineItem('recmachcustrecord_ebiz_inv_parent');

			nlapiLogExecution('ERROR', 'Location',poLocn);


			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','name', po);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_binloc', endLocn);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_lp', lp);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_sku', itemId);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_sku_status', itemStatus);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_packcode', packCode);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_qty', parseFloat(receiveQty).toFixed(5));
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_inv_ebizsku_no', itemId);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_qoh', parseFloat(receiveQty).toFixed(5));
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_itemdesc', itemDesc);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_loc', poLocn);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_invttasktype', 1); // CHKN..
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_wms_inv_status_flag', 17); // CHKN
			//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_account_no', varAccountNo);	
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','customrecord_ebiznet_location', endLocn); 
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_callinv', 'N');
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_displayfield', 'N');
			//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_fifo', sysdate);

			nlapiLogExecution('ERROR', 'LotBatch', LotBatch);

			nlapiLogExecution('ERROR', 'LotBatchId', LotBatchId);

			if(lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflg == "T"){
				parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_lot', LotBatch);				
			}			

			//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_company', vCompany);
			parent.commitLineItem('recmachcustrecord_ebiz_inv_parent');
			//var invtRecordId = nlapiSubmitRecord(invtRec, false, true);			

			/*if (invtRecordId != null) {
				nlapiLogExecution('ERROR', 'Inventory Record Creation', invtRecordId);
			} else {
				nlapiLogExecution('ERROR', 'Inventory Record Creation', 'Failed');
			}*/
		} catch (error) {
			nlapiLogExecution('ERROR', 'Check for error msg in create invt', error);
		}
	}//end of if (taskType == "1") 
}
function GetPrinters()
{

	var printercolumn = new Array();
	printercolumn.push(new nlobjSearchColumn('name'));
	var searchresults = nlapiSearchRecord('customrecord_printer_preferences', null, null,printercolumn);
	return searchresults;

}
function getAccountNo(poLocn)
{
	var varAccountNo =1;
	var filtersAccNo = new Array();
	filtersAccNo.push(new nlobjSearchFilter('custrecord_location', null, 'is', poLocn));
	filtersAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var colsAcc = new Array();
	colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
	var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);              

	if(Accsearchresults != null && Accsearchresults.length >0)
		varAccountNo = Accsearchresults[0].getValue('custrecord_accountno');                                

	nlapiLogExecution('ERROR', 'Account ',varAccountNo );

	return varAccountNo;
}

/**
 * 
 * @param orderType
 * @param ttype
 * @param poValue
 * @param poid
 * @param lineno
 * @param ItemId
 * @param quan
 * @param confqty
 * @returns {Boolean}
 */

function TrnLineUpdationNew(orderType, ttype, poValue, poid, lineno, ItemId, quan, confqty,chkAssignputW,ItemStatus,parent){
	nlapiLogExecution('ERROR','orderType',orderType);
	nlapiLogExecution('ERROR','ttype',ttype);
	nlapiLogExecution('ERROR','poValue',poValue);
	nlapiLogExecution('ERROR','poid',poid);
	nlapiLogExecution('ERROR','lineno',lineno);
	nlapiLogExecution('ERROR','ItemId',ItemId);
	nlapiLogExecution('ERROR','quan',quan);
	nlapiLogExecution('ERROR','confqty',confqty);
	nlapiLogExecution('ERROR','chkAssignputW',chkAssignputW);
	nlapiLogExecution('ERROR','ItemStatus',ItemStatus);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poid));
	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));

	// get filter based on order type
	if(orderType!='transferorder')
	{
		filters.push(getFilterForOrderType(orderType));
	}


	nlapiLogExecution('ERROR', 'filters[0]', poid);
	nlapiLogExecution('ERROR', 'filters[1]', lineno);


	nlapiLogExecution('ERROR', 'ItemStatus status', ItemStatus);

	var columns = new Array();
	columns[0] = getColumnForTransactionType(ttype);
	columns[1] = getColumnForTransactionType("ASPW");

	var itemstatus = 'GOOD';//itemRec.getFieldValue('customrecord_ebiznet_sku_status');
	var uomid = 'EACH';

	var t1 = new Date();
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);
	nlapiLogExecution('ERROR','chkpt','sucess');
	nlapiLogExecution('ERROR','searchResults',searchResults);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSearchRecord:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t1, t2));

	if(searchResults != null){
		var trnId = searchResults[0].getId();
		var lastConfQty = 0;// SearchRecd[0].getValue('custrecord_orderlinedetails_checkin_qty');

		var fldArray = new Array();
		fldArray[0] = 'custrecord_orderlinedetails_ebiz_ord_no';
		fldArray[1] = 'custrecord_orderlinedetails_orderline_no';
		fldArray[2] = 'custrecord_orderlinedetails_record_date';
		fldArray[3] = 'custrecord_orderlinedetails_record_time';
		fldArray[4] = getFieldArrayValueForTransType(ttype);
		lastConfQty = searchResults[0].getValue(fldArray[4]);
		if(lastConfQty == null || lastConfQty == "")
			lastConfQty = 0;

		var valArray = new Array();
		valArray[0] = poid;
		valArray[1] = lineno;
		valArray[2] = DateStamp();
		valArray[3] = TimeStamp();
		valArray[4] = parseFloat(lastConfQty) + parseFloat(confqty);

		if(chkAssignputW=="Y")
		{
			ttype="ASPW";
			fldArray[5] = getFieldArrayValueForTransType(ttype);
			nlapiLogExecution("ERROR", "fldArray[5]", fldArray[5]);
			//lastConfQty = searchResults[0].getValue("'"+fldArray[5]+"'");
			lastConfQty = searchResults[0].getValue(fldArray[5]);
			nlapiLogExecution("ERROR", "searchResults[0].getValue(fldArray[5]) ", searchResults[0].getValue(fldArray[5]));
			if(lastConfQty == null || lastConfQty == "")
				lastConfQty = 0;
			valArray[5] = parseFloat(lastConfQty) + parseFloat(confqty);

		}

		nlapiLogExecution("ERROR", "TrnLineUpdation:Submitted Quantity valArray[4]", valArray[4]);//chkin Qty
		nlapiLogExecution("ERROR", "TrnLineUpdation:Submitted Quantity valArray[5]", valArray[5]);//PutGen Qty

		var t3 = new Date();
		nlapiSubmitField('customrecord_ebiznet_order_line_details', searchResults[0].getId(), fldArray, valArray);
		var t4 = new Date();
		nlapiLogExecution('ERROR', 'nlapiSubmitField:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t3, t4));
	}else{
		var t5 = new Date();
		parent.selectNewLineItem('recmachcustrecord_ebiz_toline_parent');
		var t6 = new Date();
		nlapiLogExecution('ERROR', 'nlapiCreateRecord:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t5, t6));
		nlapiLogExecution('ERROR', 'Item Status', itemstatus);
		nlapiLogExecution('ERROR', 'UOM ID', uomid);
		nlapiLogExecution('ERROR', 'PO Value', poValue);
		if(poValue!=''&& poValue!=null)
		{
			nlapiLogExecution('ERROR', 'PO', poValue);
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','name', poValue);		
			parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_order_no', poValue);
		}

		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_ebiz_ord_no', parseFloat(poid));
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_orderline_no', lineno);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_item', ItemId);
//		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_item_status', 1);
		nlapiLogExecution('ERROR', 'Item Status', ItemStatus);
		if(ItemStatus!=null && ItemStatus!="")
			parent.setCurrentLineItemText('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_item_status', ItemStatus);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_uom_id', uomid);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_ebiz_sku_no', ItemId);
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_order_qty', parseFloat(quan).toFixed(5));
		// set order category depending on the order type
		setOrderLineDetailsOrderCategoryNew(parent, orderType);
		setOrderLineDetailsOrderQuantityNew(parent, ttype, confqty);
		if(chkAssignputW=="Y")
		{
			setOrderLineDetailsOrderQuantityNew(parent, "ASPW", confqty);
		}

		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_record_date', DateStamp());
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_record_time', TimeStamp());

		t5 = new Date();
		//var tranRecId = nlapiSubmitRecord(createTranRec);
		parent.commitLineItem('recmachcustrecord_ebiz_toline_parent'); 
		t6 = new Date();
		nlapiLogExecution('ERROR', 'nlapiSubmitRecord:TrnLineUpdation:order_line_details', getElapsedTimeDuration(t5, t6));
	}
	return true;
}
/**
 * 
 * @param transRecord
 * @param orderType
 */
function setOrderLineDetailsOrderCategoryNew(parent, orderType){
	if(orderType == "purchaseorder"||orderType == 'returnauthorization'||orderType == 'transferorder')
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_ord_category', 2);
	else if(orderType == "salesorder")
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_ord_category', 3);
}
/**
 * 
 * @param transRecord
 * @param transType
 * @param confQty
 */
function setOrderLineDetailsOrderQuantityNew(parent, transType, confQty){
	if (transType == "CHKN") {
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_checkin_qty', parseFloat(confQty).toFixed(5));
	}else if (transType == "ASPW") {
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_putgen_qty', parseFloat(confQty).toFixed(5));
	}else if (transType == "PUTW") {
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_putconf_qty', parseFloat(confQty).toFixed(5));
	}else if (transType == "PICKG") {
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_pickgen_qty', parseFloat(confQty).toFixed(5));
	}else if (transType == "PICKC") {
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_pickconf_qty', parseFloat(confQty).toFixed(5));
	}else if (transType == "SHIP") {
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_toline_parent','custrecord_orderlinedetails_ship_qty', parseFloat(confQty).toFixed(5));
	}
}

/**
 * @param poValue
 * @param poReceiptNo
 * @param poTrailer
 */
function MovingChknTaskToClosedTask(poValue,poReceiptNo,poTrailer)
{
	try
	{
		nlapiLogExecution('ERROR','MovingChknTaskToClosedTask');
		nlapiLogExecution('ERROR','poValue',poValue);
		nlapiLogExecution('ERROR','poReceiptNo',poReceiptNo);
		nlapiLogExecution('ERROR','poTrailer',poTrailer);

		var filter=new Array();
		if(poValue!=null&&poValue!="")
			filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no',null,'anyof',poValue));

		if(poReceiptNo!=null&&poReceiptNo!="")
			filter.push(new nlobjSearchFilter('custrecord_ot_receipt_no',null,'is',poReceiptNo));

		if(poTrailer!=null&&poTrailer!="")
			filter.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no',null,'is',poTrailer));

		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',1));//chkn
		filter.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',1));//chkn

		var column=new Array();
		column[0]=new nlobjSearchColumn('internalid');

		var searchrec=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,column);
		if(searchrec!=null&&searchrec!="")
		{
			for ( var x = 0; x < searchrec.length; x++)
			{
				var recid=searchrec[x].getValue('internalid');
				MoveTaskRecord(recid);
				nlapiLogExecution('ERROR','MOved Chkn Task with rec id',recid);
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in MovingChknTaskToClosedTask',exp);
	}


}

function getItemdimpalletqty(itemlist)
{
	nlapiLogExecution('ERROR','itemlist', itemlist);
	var palletqtysearch;
	var filters = new Array();  
	if(itemlist!=null && itemlist!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemlist));

	filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', 3));

	var columns = new Array();    
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizitemdims');					
	palletqtysearch = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns); 

	if(palletqtysearch!=null && palletqtysearch!='')
	{
		nlapiLogExecution('ERROR','palletqtysearch.length', palletqtysearch.length);
	}

	return palletqtysearch;
}

function CheckMakeSiteOrNot(Locationlist)
{
	nlapiLogExecution('ERROR','Locationlist', Locationlist);
	var MHflag='T';
	var filters = new Array();  
	if(Locationlist!=null && Locationlist!='')
	{
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', Locationlist));
		var columns = new Array();    
		columns[0] = new nlobjSearchColumn('custrecord_ebizwhsite');
		var LocationSearchResults = nlapiSearchRecord('Location', null, filters, columns); 
		if(LocationSearchResults!=null && LocationSearchResults!='')
		{
			nlapiLogExecution('ERROR','LocationSearchResults.length', LocationSearchResults.length);
			for(var i=0 ;i<LocationSearchResults.length;i++)
			{
				var Tomwhsiteflag= LocationSearchResults[i].getValue('custrecord_ebizwhsite');
				if(Tomwhsiteflag =='F')
				{
					MHflag='F';
				}
			}
		}
	}
	nlapiLogExecution('ERROR','MHflag', MHflag);
	return MHflag;
}
function getpalletqty(palletqtysearch, itemid){
	var itemdimpalletqty = 0;
	nlapiLogExecution('ERROR','item in new',itemid);

	if(palletqtysearch != null && palletqtysearch != '')
	{
		for(var i = 0; i < palletqtysearch.length; i++){

			if(palletqtysearch[i].getValue('custrecord_ebizitemdims') == itemid ){

				if(palletqtysearch[i].getValue('custrecord_ebizqty')!='')
				{
					nlapiLogExecution('ERROR','qty',palletqtysearch[i].getValue('custrecord_ebizqty'));
					itemdimpalletqty = parseFloat(palletqtysearch[i].getValue('custrecord_ebizqty'));
				}
			}
		}
	}

	return itemdimpalletqty;
}

function getPODetails(poid)
{
	nlapiLogExecution('ERROR','Into getPODetails...',poid);
	var POSearchResults = new Array();
	var POColumns = new Array();
	var POFilters = new Array();

	POFilters.push(new nlobjSearchFilter('internalid', null, 'is', parseFloat(poid))); // Task Type - RPLN	 
	POFilters.push(new nlobjSearchFilter('mainline', null, 'is', 'F')); 
	POFilters.push(new nlobjSearchFilter('taxline', null, 'is', 'F')); 
	POFilters.push(new nlobjSearchFilter('shipping', null, 'is', 'F')); 
	POFilters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0)); 

	POColumns [0] = new nlobjSearchColumn('internalid');
	POColumns [1] = new nlobjSearchColumn('tranid');
	POColumns [2] = new nlobjSearchColumn('item');
	POColumns [3] = new nlobjSearchColumn('line');
	POColumns [4] = new nlobjSearchColumn('quantity');
	POColumns [5] = new nlobjSearchColumn('location');
	POColumns [6] = new nlobjSearchColumn('custbody_nswms_company');
	POColumns [7] = new nlobjSearchColumn('custcol_ebiznet_item_status');
	POColumns [8] = new nlobjSearchColumn('custcol_nswmspackcode');

	POSearchResults = nlapiSearchRecord('purchaseorder', null, POFilters, POColumns );

	nlapiLogExecution('ERROR','Out of getPODetails...',poid);

	return POSearchResults;
}




function UpdateSerialEntry(pointid,lineno,ItemId,LP,RcvQty)
{
	try
	{
		nlapiLogExecution('ERROR','SerialEntry','update');
		nlapiLogExecution('ERROR','pointid',pointid);
		nlapiLogExecution('ERROR','lineno',lineno);
		nlapiLogExecution('ERROR','ItemId',ItemId);
		nlapiLogExecution('ERROR','LP',LP);
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
		filters[1] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', lineno);
		filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
		filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'isempty');
		if(ItemId != null && ItemId!='')
			filters[4] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', ItemId);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		columns[0].setSort();
		var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
		if(serchRec!=null && serchRec!='')
		{
			for(var k=0;k<serchRec.length;k++)
			{
//				Issue No: 20123032 start if(k<RcvQty) condition is added
				if(k<RcvQty){
					var Id=serchRec[k].getId();
					nlapiLogExecution('ERROR','Id',Id);
					if(Id!=null && Id!='')
					{
						var customrecord = nlapiLoadRecord('customrecord_ebiznetserialentry',Id);
						customrecord.setFieldValue('custrecord_serialparentid', LP);
						var rec = nlapiSubmitRecord(customrecord, false, true);
					}

				}
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','Exception in Updating SerialEntry',e);
	}
}


//case# 20124153 start
function GetPOInternalId(POText,ordtype)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',POText);
	//nlapiLogExecution('ERROR','ordertype',ordertype);

	var ActualPOID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',POText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));
	var RoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR', 'RoleLocation', RoleLocation);
	if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
	{
		filter.push(new nlobjSearchFilter('location', null, 'anyof', RoleLocation));
	}
	nlapiLogExecution('ERROR', 'ordtype', ordtype);


	var columns=new Array();
	//columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord(ordtype,null,filter,columns);
	/*if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}*/
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualPOID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualPOID);

	return ActualPOID;
}




function generateItemReceipt(fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,invtlp,systemRule,RecordId,EndLocationId,batchno,qtyexceptionFlag,templocation,getScannedSerialNoinQtyexception)
{
	try {
		nlapiLogExecution('ERROR', "fromrecord", fromrecord);
		nlapiLogExecution('ERROR', "fromid", fromid);
		var tempserialId=0;
		var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
		var polinelength = trecord.getLineItemCount('item');
		nlapiLogExecution('ERROR', "polinelength", polinelength);
		var idl;
		var vAdvBinManagement=true;

		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
		}
		//}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);
		for (var j = 1; j <= polinelength; j++) {
			nlapiLogExecution('ERROR', "Get Item Id", trecord.getLineItemValue('item', 'item', j));
			nlapiLogExecution('ERROR', "Get Item Text", trecord.getLineItemText('item', 'item', j));

			var item_id = trecord.getLineItemValue('item', 'item', j);
			var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = trecord.getLineItemValue('item', 'line', j);
			//var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', j);//text
			var poItemUOM = trecord.getLineItemValue('item', 'units', j);//value

			var commitflag = 'N';
			var quantity=0;
			quantity = ActQuantity;

			nlapiLogExecution('ERROR', 'quantity', quantity);

			var inventorytransfer="";
			nlapiLogExecution('ERROR', "itemLineNo(PO/TO)", itemLineNo);
			nlapiLogExecution('ERROR', "linenum(Task)", linenum);
			nlapiLogExecution('ERROR', "poItemUOM", poItemUOM);

			if(trantype!='transferorder')
			{
				if (itemLineNo == linenum) {

					//This part of the code will fetch the line level location.
					//If line level is empty then header level location is considered.
					//code added by suman on 080513.
					whLocation=trecord.getLineItemValue('item', 'location', j);//value
					if(whLocation==null||whLocation=="")
						whLocation=templocation;
					//End of code as of 080513.

					//var vrcvqty=arrQty[i];
					var vrcvqty=quantity;
					if(poItemUOM!=null && poItemUOM!='')
					{
						var vbaseuomqty=0;
						var vuomqty=0;
						var eBizItemDims=geteBizItemDimensions(item_id);
						if(eBizItemDims!=null&&eBizItemDims.length>0)
						{
							nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
							for(var z=0; z < eBizItemDims.length; z++)
							{
								if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
								{
									vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
								}
								nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
								if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
								{
									vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
								}
							}
							if(vuomqty==null || vuomqty=='')
							{
								vuomqty=vbaseuomqty;
							}
							nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
							nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

							if(quantity==null || quantity=='' || isNaN(quantity))
								quantity=0;
							else
								quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

						}
					}
					//Code Added by Ramana
					var varItemlocation;
					nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
					varItemlocation = getItemStatusMapLoc(itemstatus);
					//upto to here

					commitflag = 'Y';

					var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	


						serialInflg = columns.custitem_ebizserialin;
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					/*if (Itype != "serializedinventoryitem" || serialInflg != "T")
					{*/
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', quantity);

					trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana
					/*}*/

					nlapiLogExecution('ERROR', 'Serialquantity', quantity);
					nlapiLogExecution('ERROR', 'Into SerialNos');
					var totalconfirmedQty=0;
					if (Itype == "serializedinventoryitem" || serialInflg == "T") {
						nlapiLogExecution('ERROR', 'Into SerialNos');
						nlapiLogExecution('ERROR', 'invtlp', invtlp);	
						nlapiLogExecution('ERROR', 'trantype', trantype);		
						var serialline = new Array();
						var serialId = new Array();

						var tempSerial = "";
						var filters = new Array();


//						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
//						filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
//						filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

						if(trantype=='returnauthorization')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							//Commented for RMA, For RMA no need to check  whether status is checkin/storage
							//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						}

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();
						if (serchRec) {
							for (var n = 0; n < serchRec.length; n++) {
								//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
								if (tempserialId == "") {
									tempserialId = serchRec[n].getId();
								}
								else {
									tempserialId = tempserialId + "," + serchRec[n].getId();
								}

								//This is for Serial num loopin with Space separated.
								if (tempSerial == "") {
									tempSerial = serchRec[n].getValue('custrecord_serialnumber');

									serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

								}
								else {

									tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
									if(tempSerial.length>3500)
									{
										tempSerial='';
										trecord.selectLineItem('item', j);
										trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
										var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
										trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

										trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
										trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
										nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);

										trecord.commitLineItem('item');
										idl = nlapiSubmitRecord(trecord);
										serialnumArray=new Array();
										totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
										if(n<serchRec.length)
										{
											var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
											nlapiLogExecution('ERROR', 'tQty1', tQty);
											var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
											var polinelength = trecord.getLineItemCount('item');
											/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
											var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
											var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
											var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
											var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

											/*if (itemLineNo == linenum)
											{*/
											if(poItemUOM!=null && poItemUOM!='')
											{
												var vbaseuomqty=0;
												var vuomqty=0;
												var eBizItemDims=geteBizItemDimensions(item_id);
												if(eBizItemDims!=null&&eBizItemDims.length>0)
												{
													nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
													for(var z=0; z < eBizItemDims.length; z++)
													{
														if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
														{
															vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
														}
														nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
														nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
														nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
														if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
														{
															vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
														}
													}
													if(vuomqty==null || vuomqty=='')
													{
														vuomqty=vbaseuomqty;
													}
													nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
													nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

													if(tQty==null || tQty=='' || isNaN(tQty))
														tQty=0;
													else
														tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

												}
											}
											nlapiLogExecution('ERROR', 'tQtylast', tQty);
											trecord.selectLineItem('item', itemLineNo);
											trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
											var tempqty=parseInt(n)+parseInt(1);
											trecord.setCurrentLineItemValue('item', 'quantity', tQty);

											trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
											//}

											//}
										}






									}
									serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

								}
								if(vAdvBinManagement)
								{
									var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
									compSubRecord.selectNewLineItem('inventoryassignment');
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

									compSubRecord.commitLineItem('inventoryassignment');
									compSubRecord.commit();
								}

								//if(parseInt(n)== parseInt(quantity-1))
								//	break;




							}
							nlapiLogExecution('ERROR', 'qtyexceptionFlag', qtyexceptionFlag);
							if(qtyexceptionFlag == "true")
							{
								serialnumcsv = getScannedSerialNoinQtyexception;
								nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is true', serialnumcsv);
							}
							else
							{
								serialnumcsv = tempSerial;
								nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is false', serialnumcsv);
							}


						}
						tempSerial = "";
						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if ((Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem")  && (trecord!=null && trecord!='')) {
								//trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);
							}
						}


					}
					else 
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
							nlapiLogExecution('ERROR', 'Into LOT/Batch');
							nlapiLogExecution('ERROR', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";
							var confirmLotToNS='Y';

							confirmLotToNS=GetConfirmLotToNS(varItemlocation);
							nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);
							var batfilter = new Array();
							batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
							batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
							batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
							batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
							batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

							var vatColumns=new Array();
							vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
							vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
							/*var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);

							if (batchsearchresults) {
								for (var n = 0; n < batchsearchresults.length; n++) {


									//This is for Batch num loopin with Space separated.
									if (tempBatch == "") {
										if(confirmLotToNS == 'Y')
											tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
										else
										{
											nlapiLogExecution('ERROR', 'item_id:', item_id);
											var filters1 = new Array();          
											filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
											//Changed on 30/4/12 by suman
											filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
											//End of Chages as of 30/4/12

											var columns = new Array();

											columns[0] = new nlobjSearchColumn('itemid');
											var vItemname;
											var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
											if (itemdetails !=null) 
											{
												vItemname=itemdetails[0].getValue('itemid');
												nlapiLogExecution('ERROR', 'vItemname:', vItemname);
												var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
												vItemname=vItemname.replace(/ /g,"-");
												tempBatch=vItemname + '('+ vItQty + ')';


											}
										}
									}
									else {
										if(confirmLotToNS == 'Y')
											tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
										else
										{
											var filters1 = new Array();          
											filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
											//Changed on 30/4/12 by suman
											filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
											//End of Chages as of 30/4/12
											var columns = new Array();

											columns[0] = new nlobjSearchColumn('itemid');
											var vItemname;
											var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
											if (itemdetails !=null) 
											{
												vItemname=itemdetails[0].getValue('itemid');
												nlapiLogExecution('ERROR', 'vItemname:', vItemname);
												var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
												vItemname=vItemname.replace(/ /g,"-");
												tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';


											}
										}

									}

								}

								batchcsv = tempBatch;
								tempBatch = "";
							}*/
							var vItemname;
							if(confirmLotToNS == 'Y')
								tempBatch = batchno + '('+ quantity + ')';
							else
							{
								nlapiLogExecution('ERROR', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}

							if(!vAdvBinManagement)
							{	
								batchcsv = tempBatch;
								nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
								//20123720 start
								nlapiLogExecution('ERROR', 'batchno', batchno);
								var expdate1=getLotExpdate(batchno,item_id);
								if(expdate1 !=null && expdate1 !='')
								{
									trecord.setCurrentLineItemValue('item', 'expirationdate', expdate1);

								}//end
							}
							else
							{
								var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
								compSubRecord.selectNewLineItem('inventoryassignment');
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
								if(confirmLotToNS == 'Y')
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
								else
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
								compSubRecord.commitLineItem('inventoryassignment');
								compSubRecord.commit();
							}


						}
					trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');
				}
			}
			else
			{ 

				nlapiLogExecution('ERROR', 'itemid (Task)', itemid);	
				nlapiLogExecution('ERROR', 'item_id (Line)', item_id);	
				//if(vTORestrictFlag =='F')
				//{	
				itemLineNo=parseFloat(itemLineNo)-2;
				if (itemid == item_id && itemLineNo == linenum) {

					commitflag = 'Y';

					var varItemlocation;				
					varItemlocation = getItemStatusMapLoc(itemstatus);

					var fields = ['recordType', 'custitem_ebizserialin'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	

						//	var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;
						nlapiLogExecution('ERROR', 'Itype', Itype);
						nlapiLogExecution('ERROR', 'columns', columns);
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', quantity);
					if(trantype!='transferorder')
						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

					nlapiLogExecution('ERROR', 'Serialquantity', quantity);
					nlapiLogExecution('ERROR', 'Into SerialNos');
					if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") {
						nlapiLogExecution('ERROR', 'Into SerialNos');
						nlapiLogExecution('ERROR', 'invtlp', invtlp);					
						var serialline = new Array();
						var serialId = new Array();



						var tempSerial = "";
						var filters = new Array();
						/*if(trantype=='returnauthorization' || trantype=='transferorder')
						{*/
						if(trantype=='returnauthorization')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
						}
						else if(trantype=='transferorder')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1']);
							filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
						}
//						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();var totalconfirmedQty=0;
						if (serchRec) {
							for (var n = 0; n < serchRec.length; n++) {
								var tempserialcount=parseInt(n)+1;
								if(parseInt(tempserialcount)<=parseInt(quantity)){
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');

										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									else {

										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
										if(tempSerial.length>3500)
										{
											tempSerial='';
											trecord.selectLineItem('item', j);
											trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
											var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
											trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

											trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
											trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
											nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);

											trecord.commitLineItem('item');
											idl = nlapiSubmitRecord(trecord);
											serialnumArray=new Array();
											totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
											if(n<serchRec.length)
											{
												var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
												nlapiLogExecution('ERROR', 'tQty1', tQty);
												var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
												var polinelength = trecord.getLineItemCount('item');
												/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
												var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
												var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
												//var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
												var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

												/*if (itemLineNo == linenum)
											{*/
												if(poItemUOM!=null && poItemUOM!='')
												{
													var vbaseuomqty=0;
													var vuomqty=0;
													var eBizItemDims=geteBizItemDimensions(item_id);
													if(eBizItemDims!=null&&eBizItemDims.length>0)
													{
														nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
														for(var z=0; z < eBizItemDims.length; z++)
														{
															if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
															{
																vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
															}
															nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
															if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
															{
																vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
															}
														}
														if(vuomqty==null || vuomqty=='')
														{
															vuomqty=vbaseuomqty;
														}
														nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
														nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

														if(tQty==null || tQty=='' || isNaN(tQty))
															tQty=0;
														else
															tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

													}
												}
												nlapiLogExecution('ERROR', 'tQtylast', tQty);
												trecord.selectLineItem('item', itemLineNo);
												trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
												var tempqty=parseInt(n)+parseInt(1);
												trecord.setCurrentLineItemValue('item', 'quantity', tQty);

												trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
												//}

												//}
											}






										}
										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									if(vAdvBinManagement)
									{
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										compSubRecord.commit();
									}	
								}
							}

							serialnumcsv = tempSerial;
							tempSerial = "";
						}

						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem") {
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
							}
						}
					}
					else 
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
							nlapiLogExecution('ERROR', 'Into LOT/Batch');
							nlapiLogExecution('ERROR', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";

							var confirmLotToNS='Y';

							confirmLotToNS=GetConfirmLotToNS(varItemlocation);

							nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);


							/*var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										if (tempBatch == "") {
											if(confirmLotToNS == 'Y')
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('ERROR','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';


												}
											}
										}
										else {
											if(confirmLotToNS == 'Y')
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('ERROR','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';


												}
											}

										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}*/
							var vItemname;

							if(confirmLotToNS == 'Y')
								tempBatch = batchno + '('+ quantity + ')';
							else
							{
								nlapiLogExecution('ERROR', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}
							if(!vAdvBinManagement)
							{	
								batchcsv = tempBatch;
								nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							}
							else
							{
								var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
								compSubRecord.selectNewLineItem('inventoryassignment');
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
								if(confirmLotToNS == 'Y')
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
								else
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
								compSubRecord.commitLineItem('inventoryassignment');
								compSubRecord.commit();
							}	
						}
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');

				}
				//Code Added by Ramana
				/*}
				else
				{
					trecord=null;
					idl=pointid;
				}*/	
			}
			if (commitflag == 'N' && trecord != null && trecord != '') {
				nlapiLogExecution('ERROR', 'commitflag is N', commitflag);
				trecord.selectLineItem('item', j);
				trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
				trecord.commitLineItem('item');
			}
		}
		nlapiLogExecution('ERROR', 'Before Item Receipt---', 'Before');

		if(trecord != null && trecord != '')
			idl = nlapiSubmitRecord(trecord);


		nlapiLogExecution('ERROR', 'Item Receipt idl', idl);

		return idl;
	}
	catch(exp)
	{
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		var POarray = new Array();
		nlapiLogExecution('ERROR', 'DetailsError', functionality);	
		nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('ERROR', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,exp,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('ERROR', 'Exception in TransformRec (PurchaseOrder) : ', exp);

		if (exp instanceof nlobjError) 
			nlapiLogExecution('ERROR', 'system error', exp.getCode() + '\n' + exp.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', exp.toString());

		nlapiLogExecution('ERROR', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('ERROR', 'PUTW task updated with act end location', EndLocationId);

		nlapiLogExecution('ERROR', 'Putaway Failed ');
		POarray["custparam_error"] = exp.getCode() + '\n' + exp.getDetails();
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		return;
	}	
}
//case# 20124153 end


function updatePOline(vPoNo,trantype,vlinearr)
{
	nlapiLogExecution('Debug', 'Into updatePOline', vPoNo);
	nlapiLogExecution('Debug', 'trantype', trantype);

	var poRecord = nlapiLoadRecord(trantype, vPoNo);

	var vPOItemLength = poRecord.getLineItemCount('item');
	for(var vcount=1;vcount<=vPOItemLength;vcount++)
	{
		var vpolineno = poRecord.getLineItemValue('item', 'line',vcount)

		nlapiLogExecution('Debug', 'vpolineno', vpolineno);

		for(var j=0;j<vlinearr.length;j++)
		{
			var arLineno = vlinearr[j];
			nlapiLogExecution('Debug', 'arLineno', arLineno);
			if(parseInt(vpolineno)==parseInt(arLineno))
			{
				poRecord.setLineItemValue('item', 'custcol_transactionstatusflag',vcount,36);
			}
		}		
	}
	nlapiSubmitRecord(poRecord, true); 

	nlapiLogExecution('Debug', 'Out of updatePOline', vPoNo);
}