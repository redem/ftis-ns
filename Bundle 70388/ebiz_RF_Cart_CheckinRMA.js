/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_Cart_CheckinRMA.js,v $
 *     	   $Revision: 1.1.2.5.2.1 $
 *     	   $Date: 2015/09/28 15:14:00 $
 *     	   $Author: sponnaganti $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_17 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Cart_CheckinRMA.js,v $
 * Revision 1.1.2.5.2.1  2015/09/28 15:14:00  sponnaganti
 * 201414626
 * Briggs Issue fix
 *
 * Revision 1.1.2.5  2014/06/27 10:57:17  sponnaganti
 * case# 20149057
 * Compatability Test Acc issue fix
 *
 * Revision 1.1.2.4  2014/06/13 08:00:39  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.3  2014/05/30 00:26:47  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2  2014/05/14 14:07:14  gkalla
 * case#20148381
 * RF RMA Cart checkin issue
 *
 * Revision 1.1.2.1  2014/04/22 14:49:53  grao
 * Leisure Living Enhancement case#:20148087
 *
 * Revision 1.1.2.4.4.5.4.3  2013/10/11 06:08:09  skreddy
 * Case# 20124821
 * GFT prod issue fix
 *
 * Revision 1.1.2.4.4.5.4.2  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.4.4.5.4.1  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.4.4.5  2012/10/01 06:36:25  grao
 * no message
 *
 * Revision 1.1.2.4.4.4  2012/09/27 13:13:26  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.1.2.4.4.3  2012/09/27 10:54:36  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.1.2.4.4.2  2012/09/26 12:44:14  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.4.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.1.2.4  2012/07/31 10:45:50  schepuri
 * CASE201112/CR201113/LOG201121
 * INVALID TO CONTINUE issue
 *
 * Revision 1.1.2.3  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1.2.2  2012/04/25 21:19:27  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF cart Changes
 *
 * Revision 1.1.2.1  2012/04/17 10:38:39  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Cart To Checkin
 *
 * Revision 1.8  2011/12/12 09:06:53  rgore
 * CASE201112/CR201113/LOG201121
 * Modularized code to call method from data access library.
 * - Ratnakar
 * 12 Dec 2011
 *
 * Revision 1.7  2011/09/12 08:24:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/07/07 09:15:42  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/04/29 16:02:38  skota
 * CASE201112/CR201113/LOG201121
 * Added function key functionality
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/19 09:40:19  vpasula
 * CASE2009936/CR200912052/LOG20093196
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function CartCheckInRMA(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INGRESAR / ESCANEAR RMA";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";


		}
		else
		{
			st0 = "";
			st1 = "ENTER/SCAN RMA#";
			st2 = "SEND";
			st3 = "PREV";

		}




		//customscript_rf_general_script  
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript'>";
		html = html + "function OnKeyDown_CL() ";
		html = html + " { ";     
		html = html + "         if (";
		html = html + " event.keyCode == 112 || event.keyCode == 113 || event.keyCode == 114 || event.keyCode == 115 || event.keyCode == 116 || event.keyCode == 117 ||";
		html = html + " event.keyCode == 118 || event.keyCode == 119 || event.keyCode == 120 || event.keyCode == 121 || event.keyCode == 122) {";
		html = html + " var arrElements = document.getElementsByTagName('input');";
		html = html + " var keyFound = false;";
		html = html + " for (i = 0; i < arrElements.length; i++) {";
		html = html + " if (arrElements[i].type == 'submit') {";
		html = html + " switch (event.keyCode) {";
		html = html + " case 112:";
		html = html + " if (arrElements[i].value == 'F1')";	//F7 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 113:";
		html = html + " if (arrElements[i].value == 'F2')";		//F8 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 114:";
		html = html + " if (arrElements[i].value == 'F3')";	//F9 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 115:";
		html = html + " if (arrElements[i].value == 'F4')";		//F10 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 116:";
		html = html + " if (arrElements[i].value == 'F5')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 117:";
		html = html + " if (arrElements[i].value == 'F6')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 118:";
		html = html + " if (arrElements[i].value == 'F7')";		//F7 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 119:";
		html = html + " if (arrElements[i].value == 'F8')";		//F8 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 120:";
		html = html + " if (arrElements[i].value == 'F9')";		//F9 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 121:";
		html = html + " if (arrElements[i].value == 'F10')";		//F10 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 122:";
		html = html + " if (arrElements[i].value == 'F11')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " }";
		html = html + " if (keyFound == true) {";        
		html = html + " eval('document._rf_checkin_po.' + arrElements[i].name + '.click();');";
		html = html + " return false;";
		html = html + " }";
		html = html + " }";
		html = html + " }";
		html = html + " }    ";        
		html = html + "    return true; ";
		html = html + "    }";        
		html = html + " </SCRIPT>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enterpo').focus();";        
		html = html + "</script>";
		html = html + " </head>";
		html = html + "<body onkeydown='OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_po' method='POST'>"; //onkeydown='OnKeyDown_CL()' >";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input id='txtPO' name='enterpo' id='enterpo' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterpo').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Processing RF', 'Validating TO');

		var ActualBeginDate = DateStamp();
		var ActualBeginTime = TimeStamp();
		var TimeArray = new Array();
		TimeArray = ActualBeginTime.split(' ');

		// Determine if 'F7' is clicked, in order to navigate to the previous screen
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the PO# entered.
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			st4 = "NO V&#193;LIDO PARA";
			st5 = "MEN&#218;DEL EMBALAJE";
			st9 = "ESTA CARACTER&#205;STICA SE DISPONIBLE PR&#211;XIMO PARCHE/VERSI&#211;N.";

		}
		else
		{
			st4 = "INVALID RMA";
			st5 = "PACKING MENU";
			st9 = "THIS FEATURE WILL BE AVAILABLE NEXT PATCH/VERSION.";
		}




		POarray["custparam_poid"] = request.getParameter('enterpo');
		POarray["custparam_error"] = st4;
		POarray["custparam_screenno"] = 'RMA1';
		POarray["custparam_option"] = request.getParameter('hdnOptenField');
		POarray["custparam_actualbegindate"] = ActualBeginDate;
		POarray["custparam_actualbegintime"] = TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = TimeArray[1];

		var message = 'PO ID = ' + POarray["custparam_poid"] + '<br>';
		message = message + 'OptedField = ' + POarray["custparam_option"] + '<br>';
		message = message + 'ActualBeginDate = ' + POarray["custparam_actualbegindate"] + '<br>';
		message = message + 'ActualBeginTime = ' + POarray["custparam_actualbegintime"] + '<br>';
		message = message + 'ActualBeginTimeAMPM = ' + POarray["custparam_actualbegintimeampm"] + '<br>';
		message = message + 'OptedEvent = ' + optedEvent;

		nlapiLogExecution('DEBUG', 'RF - TO SCREEN', message);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
		}
		else 
		{
			var getPOid=request.getParameter('enterpo');
			nlapiLogExecution('DEBUG','getPOid',getPOid);
			var POtrantypefilters=new Array();
			POtrantypefilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
			POtrantypefilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

			var POtrantypecols=new Array();
			POtrantypecols[0]=new nlobjSearchColumn('internalid');

			var PORecinternalids=nlapiSearchRecord('returnauthorization',null,POtrantypefilters,POtrantypecols);
			var poid='';
			if(PORecinternalids!=null && PORecinternalids!='')
			{
				poid=PORecinternalids[0].getValue('internalid');
				//Case20124821 start : for Invaild TO
				//}
				//end of Case20124821
				
				var trantype = nlapiLookupField('transaction', poid, 'recordType');
				nlapiLogExecution('DEBUG','trantype',trantype);
				POarray["custparam_trantype"] = trantype;
				var POfilters=new Array();
				POfilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
				POfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
				POfilters.push(new nlobjSearchFilter('recordtype',null,'is',trantype));

				var POcols=new Array();
				POcols[0]=new nlobjSearchColumn('status');
				POcols[1]=new nlobjSearchColumn('location');
				if(trantype=='transferorder')
				POcols[2]= new nlobjSearchColumn('transferlocation');

				var PORec=nlapiSearchRecord('transaction',null,POfilters,POcols);
				if(PORec!=null&&PORec!='')
				{
					var poTransferLocationID='';
					var poStatus=PORec[0].getValue('status');
					nlapiLogExecution('DEBUG','poStatus',poStatus);
					var poToLocationID=PORec[0].getValue('location');
					if(trantype=='transferorder')
					poTransferLocationID=PORec[0].getValue('transferlocation');
					nlapiLogExecution('DEBUG','poToLocation',poToLocationID);
					if(trantype!='transferorder' && (poToLocationID == null || poToLocationID =='' || poToLocationID == 'null'))
					{
						POarray["custparam_error"] = 'Location is not tied up this RMA#'+getPOid;
						nlapiLogExecution('DEBUG', 'Location not tied with order', POarray["custparam_poid"]);
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return;
					}
					var fields = ['custrecord_ebizwhsite'];
					var locationcolumns ="";
					if(trantype=='transferorder')
					{
						if(poTransferLocationID !=null && poTransferLocationID !='' && poTransferLocationID !='null')
							locationcolumns = nlapiLookupField('Location', poTransferLocationID, fields);
					}
					else	
					{
						if(poToLocationID !=null && poToLocationID !='' && poToLocationID !='null')
							locationcolumns = nlapiLookupField('Location', poToLocationID, fields);
					}
					
					var Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;
					nlapiLogExecution('DEBUG','Tomwhsiteflag',Tomwhsiteflag);
					// Call method to retrieve the PO based on the PO ID

					if(poStatus=='pendingReceipt'||poStatus=='partiallyReceived' ||poStatus=='pendingBillPartReceived' || poStatus=='pendingReceiptPartFulfilled'  || poStatus=='pendingRefundPartReceived')
					{
						if(Tomwhsiteflag=='T')
						{
							var poSearchResults = eBiz_RF_GetPOListForTranID(POarray["custparam_poid"],trantype);

							if(poSearchResults != null && poSearchResults.length > 0){
								if (POarray["custparam_poid"] != null) 
								{
									var whLocation= poSearchResults[0].getValue('location');
									var whCompany= poSearchResults[0].getValue('custbody_nswms_company');
									var whtransferLocation= poSearchResults[0].getValue('transferlocation');
									if(trantype=='transferorder')
										POarray["custparam_whlocation"] = whtransferLocation;
									else
										POarray["custparam_whlocation"] = whLocation;

									POarray["custparam_company"] =whCompany;

									message = 'whLocation = ' + whLocation + '<br>';
									message = message + 'whCompany = ' + whCompany;
									nlapiLogExecution('DEBUG', 'PO Details', message);

									// Redirecting the control to the next screen - SCAN Cart
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartscan', 'customdeploy_ebiz_rf_cartscan_di', false, POarray);
								}
								else 
								{
									//	if the 'Send' button is clicked without any option value entered,
									//  it has to remain in the same screen ie., Receiving Menu.
									nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								}
							}
							else
							{
								nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
						}
						else
						{
							nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);			
						}
					}
					else
					{
						POarray["custparam_error"] = st4;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
					}
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
				}


			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
			}
		}  //end of else loop.
	}

}
