/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_loadtrailer_CL.js,v $
 *     	   $Revision: 1.1.2.2.4.3 $
 *     	   $Date: 2013/02/21 14:32:53 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_NSWMS_2013_1_2_21 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_loadtrailer_CL.js,v $
 * Revision 1.1.2.2.4.3  2013/02/21 14:32:53  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.1.2.2.4.2  2013/01/21 15:51:53  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA-BuildShip Unit Chnages.
 *
 * Revision 1.1.2.2.4.1  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.2  2012/09/14 07:21:57  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for null
 *
 * Revision 1.1.2.1  2012/06/21 10:15:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.1  2011/11/23 11:16:13  schepuri
 * CASE201112/CR201113/LOG201121
 * splitting load and depart trailer functionality
 **
 *****************************************************************************/
function onSave(type)
{
	
	var resultsetShipLP,wavenumber,chkVal;
	var prevWaveNo="",prevChkVal="",ProcessVal=true;var Salesord = '';
	var lineCnt = nlapiGetLineItemCount('custpage_loadtrailer');
	var prevSalesord='-1';

	//alert(lineCnt);
	for (var i = 1; i <= lineCnt; i++) 
	{
		resultsetShipLP = nlapiGetLineItemValue('custpage_loadtrailer','custpage_ordernumber',i);
		wavenumber = nlapiGetLineItemValue('custpage_loadtrailer','custpage_wavenumber',i);
		chkVal = nlapiGetLineItemValue('custpage_loadtrailer','custpage_select',i);
		Salesord = nlapiGetLineItemValue('custpage_loadtrailer','custpage_orderno',i);
		
		/*var Filers = new Array;
		Filers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', resultsetShipLP)); 

		var Columns = new Array();
		Columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');


		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filers, Columns);
		var Salesord,closedtasktrailername;
		if(SOSearchResults != ""  &&  SOSearchResults != null){
			Salesord=SOSearchResults[0].getValue('custrecord_ebiz_order_no');

		}*/

		
		if(prevSalesord!=Salesord)
		{
			//alert('Salesord '+Salesord);
			if(Salesord!=null && Salesord!='')
			{
//				if(blnShipComplete(Salesord))
//				{
//					/*alert('clienttrue ');
//			alert('prevWaveNo '+prevWaveNo);
//			alert('wavenumber '+wavenumber);
//			alert('prevChkVal '+prevChkVal);
//			alert('chkVal '+chkVal);*/
//
//					if(prevWaveNo == wavenumber && prevChkVal != chkVal)
//					{
//						alert('Please select other line for the order '+Salesord);
//						return false;
//					}
//
//					prevWaveNo = wavenumber;
//					prevChkVal = chkVal;
//				}
			}
			
			prevSalesord = Salesord;
		}
	}
	return true;

}

function blnShipComplete(vSOInternalId)
{
	var vBlnShipComp=true;
	var filters = new Array();

	filters.push(new nlobjSearchFilter('name', null,
			'is', vSOInternalId)); 


	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
	columns.push(new nlobjSearchColumn('custrecord_pickqty'));
	columns.push(new nlobjSearchColumn('custrecord_shipcomplete'));


	var searchresultsordline = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if(searchresultsordline != null && searchresultsordline != "" && searchresultsordline.length>0)
	{
		nlapiLogExecution('ERROR', 'searchresultsordline.length',searchresultsordline.length);
		for(var v=0;v<searchresultsordline.length;v++)
		{
			var vlinePickQty=0;
			var vlineOrdQty=0;
			if(searchresultsordline[v].getValue('custrecord_ord_qty')!= null && searchresultsordline[v].getValue('custrecord_ord_qty') != "")
				vlineOrdQty=parseFloat(searchresultsordline[v].getValue('custrecord_ord_qty'));

			if(searchresultsordline[v].getValue('custrecord_pickqty') != null && searchresultsordline[v].getValue('custrecord_pickqty') != "")
				vlinePickQty= parseFloat(searchresultsordline[v].getValue('custrecord_pickqty'));

			var vlineShipCompFlag=searchresultsordline[v].getValue('custrecord_shipcomplete');

			nlapiLogExecution('ERROR', 'vlineOrdQty',vlineOrdQty);
			nlapiLogExecution('ERROR', 'vlinePickQty',vlinePickQty);
			nlapiLogExecution('ERROR', 'vlineShipCompFlag',vlineShipCompFlag);

			if(vlineShipCompFlag!= null && vlineShipCompFlag != "" && vlineShipCompFlag =='T' && vlineOrdQty >0)
			{
				if(vlineOrdQty!=vlinePickQty)
				{
					vBlnShipComp=false;

				}
				else
				{
					vBlnShipComp=true;

				}	

			}
			else if(vlineShipCompFlag!= null && vlineShipCompFlag != "" && vlineShipCompFlag =='F' && vlineOrdQty >0)
			{
				vBlnShipComp=false;

			}

		}
	}
	else
		vBlnShipComp=false;
	//alert('vBlnShipComp inclient'+vBlnShipComp);
	return vBlnShipComp;
}
