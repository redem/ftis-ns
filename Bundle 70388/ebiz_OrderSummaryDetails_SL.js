/***************************************************************************
	  		   eBizNET Solutions Inc                
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/ebiz_OrderSummaryDetails_SL.js,v $
 *     	   $Revision: 1.8.2.1.4.1.4.7 $
 *     	   $Date: 2015/09/02 15:48:06 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_OrderSummaryDetails_SL.js,v $
 * Revision 1.8.2.1.4.1.4.7  2015/09/02 15:48:06  nneelam
 * case# 201414138
 *
 * Revision 1.8.2.1.4.1.4.6  2013/09/16 15:54:04  nneelam
 * Case#. 20124422
 * pdf Report  Issue Fix..
 *
 * Revision 1.8.2.1.4.1.4.5  2013/09/11 14:22:28  skreddy
 * Case# 20124295
 * standard bundle issue fix
 *
 * Revision 1.8.2.1.4.1.4.4  2013/09/06 00:26:04  nneelam
 * Case#.20124295�
 * SuiteScript Error  Issue Fix..
 *
 * Revision 1.8.2.1.4.1.4.3  2013/05/08 15:11:46  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.8.2.1.4.1.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.8.2.1.4.1.4.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.8.2.1.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.8.2.1  2012/09/03 13:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.8  2011/11/28 15:30:42  spendyala
 * CASE201112/CR201113/LOG201121
 * added with printdate in the sublist
 *
 * Revision 1.7  2011/11/02 14:16:55  rmukkera
 * CASE201112/CR201113/LOG201121
 * changes done for fetching more than 100o records for dordowns and added print functionality
 *
 * Revision 1.6  2011/11/01 06:08:25  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/10/25 06:47:25  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/10/24 12:44:22  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/10/24 10:37:34  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/10/24 06:15:23  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1  2011/10/20 18:05:08  mbpragada
 * CASE201112/CR201113/LOG201121
 *

 *

 *****************************************************************************/


function fillWaveNumbers(form, WaveField,maxno){
	var waveFilters = new Array();
	waveFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F')); 
	if(maxno!=-1)
	{
		waveFilters.push(new nlobjSearchFilter('id', null, 'greaterthan', parseFloat(maxno)));
	}
	WaveField.addSelectOption("", "");
	var columns = new Array();
	columns[0]=new nlobjSearchColumn('custrecord_ebiz_wave');
	columns[1]=new nlobjSearchColumn('id');
	columns[0].setSort();
	var waveSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, waveFilters,columns );
	nlapiLogExecution('ERROR', 'waveSearchResults', waveSearchResults.length);
	for (var i = 0; waveSearchResults != null && i < waveSearchResults.length; i++) {

		if(waveSearchResults[i].getValue('custrecord_ebiz_wave') !=null && waveSearchResults[i].getValue('custrecord_ebiz_wave') !='')
		{
			var res = form.getField('custpage_wave').getSelectOptions(waveSearchResults[i].getValue('custrecord_ebiz_wave'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}

			WaveField.addSelectOption(waveSearchResults[i].getValue('custrecord_ebiz_wave'), waveSearchResults[i].getValue('custrecord_ebiz_wave'));
		}
	}
	if(waveSearchResults!=null && waveSearchResults.length>=1000)
	{
		nlapiLogExecution('ERROR', 'waveSearchResults more than 1000', waveSearchResults.length);
		//case# 20124295�start
		var column = new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_wave');
		column[1]=new nlobjSearchColumn('id');
		column[1].setSort();
		
		var OrderSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, waveFilters, column);
		var maxno = OrderSearchResults[OrderSearchResults.length-1].getValue(column[1]);
		nlapiLogExecution('ERROR', 'waveSearchResults more than 1000',maxno);
		fillWaveNumbers(form, WaveField,maxno);
		//case# 20124295�end
	
	}
}

/**
 * This function is to fill the fulfillment order numbers in the dropdown of the query block.
 * @param form
 * @param fulfillmentField
 */
function fillordField(form, fulfillmentField,maxno){
	var fulfillmentOrderFilters = new Array();
	fulfillmentOrderFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));  	
	if(maxno!=-1)
		{
		fulfillmentOrderFilters.push(new nlobjSearchFilter('id', null, 'greaterthan', parseFloat(maxno)));
		}
	fulfillmentField.addSelectOption("", "");
	var columns = new Array();
	columns[0]=new nlobjSearchColumn('custrecord_lineord');
	columns[1]=new nlobjSearchColumn('id');
	columns[0].setSort();
	
	var fulfillmentOrderSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, fulfillmentOrderFilters, columns);
     
	for (var i = 0; fulfillmentOrderSearchResults != null && i < fulfillmentOrderSearchResults.length; i++) {
		var res = form.getField('custpage_order').getSelectOptions(fulfillmentOrderSearchResults[i].getValue('custrecord_lineord'), 'is');
		if (res != null) {
			if (res.length > 0) {
				continue;
			}
		}
		fulfillmentField.addSelectOption(fulfillmentOrderSearchResults[i].getValue('custrecord_lineord'), fulfillmentOrderSearchResults[i].getValue('custrecord_lineord'));
	}
	
	if(fulfillmentOrderSearchResults!=null && fulfillmentOrderSearchResults.length>=1000)
	{
		var column = new Array();
		column[0]=new nlobjSearchColumn('custrecord_lineord');
		column[1]=new nlobjSearchColumn('id');
		column[1].setSort();
		
		var OrderSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, fulfillmentOrderFilters, column);
		
		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillordField(form, fulfillmentField,maxno);	
	
	}
}

 
function fillcustomerField(form, customerField,maxno){
	var customerFilers = new Array();
	customerFilers.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(maxno!=-1)
	{
		customerFilers.push(new nlobjSearchFilter('id', null, 'greaterthan', parseFloat(maxno)));
	}
	customerField.addSelectOption("", "");
	var columns=new Array();
	columns[0]=new nlobjSearchColumn('custrecord_do_customer');
	columns[0].setSort();
	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, customerFilers,columns);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {
		var resdo = form.getField('custpage_customer').getSelectOptions(customerSearchResults[i].getText('custrecord_do_customer'), 'is');
		if (resdo != null) {
			if (resdo.length > 0) {
				continue;
			}
		}
		customerField.addSelectOption(customerSearchResults[i].getValue('custrecord_do_customer'), customerSearchResults[i].getText('custrecord_do_customer'));
	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var column = new Array();
		column[0]=new nlobjSearchColumn('custrecord_do_customer');
		column[1]=new nlobjSearchColumn('id');
		column[1].setSort();
		
		var OrderSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, customerFilers, column);
		
		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(column[1]);		
		fillcustomerField(form, customerField,maxno);	
	
	}
}

function fillsalesorderField(form, salesorderField,maxno){
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	if(maxno!=-1)
	{
		salesorderFilers.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', parseFloat(maxno)));
	}
	salesorderField.addSelectOption("", "");
	
	var columns=new Array();
	columns[0]=new nlobjSearchColumn('tranid');
	columns[0].setSort();
	columns[1]=new nlobjSearchColumn('internalid');

	
	var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,columns);

	nlapiLogExecution('ERROR', 'salesorder_count',customerSearchResults.length);
	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {
		
		if(customerSearchResults[i].getValue('tranid') !=null && customerSearchResults[i].getValue('tranid') !='')
		{
			var resdo = form.getField('custpage_salesorder').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
			salesorderField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
		}
	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var column=new Array();
		column[0]=new nlobjSearchColumn('tranid');		
		column[1]=new nlobjSearchColumn('internalid');
		column[1].setSort();
		
		var OrderSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,column);
		
		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillsalesorderField(form, salesorderField,maxno);	
	
	}
}
function ebiznet_OrderSummaryDetails(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Order Summary Details Report');
		var vQbWave = "";
		//case # 20124422�Start
//form.setScript('customscript_inventoryclientvalidations');
		//case # 20124422�End
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');		
		var sysdate=DateStamp();
		fromdate.setDefaultValue(sysdate);

		 var salesorder=form.addField('custpage_salesorder', 'select', 'Order #');
	     salesorder.setLayoutType('startrow', 'none');
	     fillsalesorderField(form, salesorder,-1);
	    
	     
	var ordField = form.addField('custpage_order', 'select', 'Fullfilment Order #');
	ordField.setLayoutType('startrow', 'none');
	fillordField(form, ordField,-1);
	

	
	var WaveField = form.addField('custpage_wave', 'select', 'Wave #');
	WaveField.setLayoutType('startrow', 'none');
	fillWaveNumbers(form, WaveField,-1);
	
	

		var todate = form.addField('custpage_todate', 'date', 'To Date');		 
		todate.setDefaultValue(sysdate);

		var customerField = form.addField('custpage_customer', 'select', 'Customer');
		customerField.setLayoutType('startrow', 'none');
		fillcustomerField(form, customerField,-1);



		var fulfillordfield = form.addField('custpage_fulfillmentorder', 'select', 'Fulfillment Order Status');
		fulfillordfield.setLayoutType('startrow', 'none');
		fulfillordfield.addSelectOption("", "");

		var location = form.addField('custpage_location', 'select', 'Location');

		var filterLocation = new Array();
		filterLocation.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));
		var columnLocation = new Array();
		columnLocation[0] = new nlobjSearchColumn('name');
		location.addSelectOption("", "");
		var searchLocationRecord = nlapiSearchRecord('Location', null, filterLocation, columnLocation);
		for(var count=0;count < searchLocationRecord.length ;count++)
		{
			location.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('name'));

		}
		location.setMandatory(true ); 

		var fulfillordfilters = new Array();
		fulfillordfilters[0] = new nlobjSearchFilter('custrecord_ebiz_process', null, 'anyof', ['2']);  		

		var fulfillordsearchresults = nlapiSearchRecord('customrecord_wms_status_flag', null, fulfillordfilters, new nlobjSearchColumn('name'));		
		if (fulfillordsearchresults != null) {
			for (var i = 0; i < fulfillordsearchresults.length; i++) {

				var res=  form.getField('custpage_fulfillmentorder').getSelectOptions(fulfillordsearchresults[i].getValue('name'), 'is');
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}

				fulfillordfield.addSelectOption(fulfillordsearchresults[i].getId(), fulfillordsearchresults[i].getValue('name'));
			}
		}

		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else //this is the POST block
	{

		var form = nlapiCreateForm('Order Summary Details Report');
		//case # 20124422�Start
		//form.setScript('customscript_inventoryclientvalidations');
		//case # 20124422�End
		var vQbfromdate = request.getParameter('custpage_fromdate');
		var vQbtodate = request.getParameter('custpage_todate');	
		var fulfillordstatus = request.getParameter('custpage_fulfillmentorder');
		var vqblocation = request.getParameter('custpage_location');

		var qborderno = request.getParameter('custpage_order');		
		var qbwaveno = request.getParameter('custpage_wave');
		var qbcustomer = request.getParameter('custpage_customer');
		var qbsalesorderno = request.getParameter('custpage_salesorder');

		nlapiLogExecution('ERROR', 'fulfillordstatus', fulfillordstatus);


		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date').setDefaultValue(vQbfromdate);

		 var salesorder=form.addField('custpage_salesorder', 'select', 'Order #');
	     salesorder.setLayoutType('startrow', 'none');
	     fillsalesorderField(form, salesorder,-1);
	     salesorder.setDefaultValue(qbsalesorderno);
		
		var ordField = form.addField('custpage_order', 'select', 'Fullfilment Order #');
		ordField.setLayoutType('startrow', 'none');
		fillordField(form, ordField,-1);
		ordField.setDefaultValue(qborderno);
		
		var WaveField = form.addField('custpage_wave', 'select', 'Wave #');
		WaveField.setLayoutType('startrow', 'none');
		fillWaveNumbers(form, WaveField,-1);
		WaveField.setDefaultValue(qbwaveno);
		
		
		
		var todate = form.addField('custpage_todate', 'date', 'To Date').setDefaultValue(vQbtodate);
		
		var customerField = form.addField('custpage_customer', 'select', 'Customer');
		customerField.setLayoutType('startrow', 'none');
		fillcustomerField(form, customerField,-1);
		customerField.setDefaultValue(qbcustomer);

		var fulfillordfield = form.addField('custpage_fulfillmentorder', 'select', 'Fulfillment Order Status');
		fulfillordfield.setLayoutType('startrow', 'none');
		fulfillordfield.addSelectOption("", "");


		var location = form.addField('custpage_location', 'select', 'Location');

		var filterLocation = new Array();
		filterLocation.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));
		var columnLocation = new Array();
		columnLocation[0] = new nlobjSearchColumn('name');
		location.addSelectOption("", "");
		var searchLocationRecord = nlapiSearchRecord('Location', null, filterLocation, columnLocation);
		for(var count=0;count < searchLocationRecord.length ;count++)
		{
			location.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('name'));

		}
		location.setMandatory(true ); 
		location.setDefaultValue(vqblocation);
		var fulfillordfilters = new Array();
		fulfillordfilters[0] = new nlobjSearchFilter('custrecord_ebiz_process', null, 'anyof', ['2']);  		

		var fulfillordsearchresults = nlapiSearchRecord('customrecord_wms_status_flag', null, fulfillordfilters, new nlobjSearchColumn('name'));		
		if (fulfillordsearchresults != null) {
			for (var i = 0; i < fulfillordsearchresults.length; i++) {

				var res=  form.getField('custpage_fulfillmentorder').getSelectOptions(fulfillordsearchresults[i].getValue('name'), 'is');
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}

				fulfillordfield.addSelectOption(fulfillordsearchresults[i].getId(), fulfillordsearchresults[i].getValue('name'));
			}
		}
		fulfillordfield.setDefaultValue(fulfillordstatus);
		form.addSubmitButton('Display');

		var sublist = form.addSubList("custpage_items", "list", "Order Summary List");

		sublist.addField("custpage_ordno", "text", "Ord#");
		sublist.addField("custpage_customerlist", "text", "Customer");
		sublist.addField("custpage_orderdate", "text", "Order Date");	
		sublist.addField("custpage_priority", "text", "Order Priority");

		sublist.addField("custpage_waveno", "text", "Wave #");
		sublist.addField("custpage_item", "text", "Item");

		sublist.addField("custpage_ordqty", "text", "Order Qty");
		sublist.addField("custpage_pickedqty", "text", "Picked Qty");
		sublist.addField("custpage_shipqty", "text", "Ship Qty");
		//sublist.addField("custpage_location", "text", "Location");
		sublist.addField("custpage_print", "text", "Printed?");
		sublist.addField("custpage_printdate", "date", "Printed Date");
		sublist.addField("custpage_fulfillordno", "text", "Fulfillment Order #");
		sublist.addField("custpage_fulfillordnostatus", "text", "Fulfillment Order Status");


		//for sales order
		var salessearchfilters = new Array();
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {

			salessearchfilters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));			 
		} 
		if ((request.getParameter('custpage_fulfillmentorder') != "" && request.getParameter('custpage_fulfillmentorder') != null)){
			nlapiLogExecution('ERROR', 'custpage_fulfillmentorder', fulfillordstatus);
			salessearchfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'is', fulfillordstatus));
		}
		else
		{
			salessearchfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['1','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','24','25','26','27','28','29','30'])); 
		}
		if (request.getParameter('custpage_order') !=null && request.getParameter('custpage_order') !=""){
			nlapiLogExecution('ERROR', 'custpage_order', qborderno);
			salessearchfilters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', request.getParameter('custpage_order')));
		}
		if (request.getParameter('custpage_salesorder') !=null && request.getParameter('custpage_salesorder') !=""){
			nlapiLogExecution('ERROR', 'custpage_salesorder', qbsalesorderno);
			salessearchfilters.push(new nlobjSearchFilter('name', null, 'is', request.getParameter('custpage_salesorder')));
		}
		if (request.getParameter('custpage_wave') !=null && request.getParameter('custpage_wave') !=""){
			nlapiLogExecution('ERROR', 'custpage_wave', qbwaveno);
			salessearchfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto', request.getParameter('custpage_wave')));
		}

		if (request.getParameter('custpage_customer') !=null && request.getParameter('custpage_customer') !=""){
			nlapiLogExecution('ERROR', 'custpage_customer', qbcustomer);
			salessearchfilters.push(new nlobjSearchFilter('custrecord_do_customer', null, 'anyof', [request.getParameter('custpage_customer')]));
		}
		if (request.getParameter('custpage_location') !=null && request.getParameter('custpage_location') !=""){
			nlapiLogExecution('ERROR', 'custpage_location', qborderno);
			salessearchfilters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', request.getParameter('custpage_location')));
		}
		
		var ctx = nlapiGetContext();
		var vassemblies=ctx.getFeature('assemblies');

		var salesColumns = new Array();
		salesColumns[0] = new nlobjSearchColumn('custrecord_ns_ord');
		salesColumns[1] = new nlobjSearchColumn('custrecord_do_customer');
		salesColumns[2] = new nlobjSearchColumn('custrecord_record_linedate');

		salesColumns[3] = new nlobjSearchColumn('custrecord_lineord');
		salesColumns[4] = new nlobjSearchColumn('custrecord_linestatus_flag');
		salesColumns[5] = new nlobjSearchColumn('custrecord_ebiz_wave');
		salesColumns[6] = new nlobjSearchColumn('custrecord_ebiz_linesku');

		salesColumns[7] = new nlobjSearchColumn('custrecord_ord_qty');
		salesColumns[8] = new nlobjSearchColumn('custrecord_pickqty');		
		salesColumns[9] = new nlobjSearchColumn('custrecord_linestatus_flag');
		

		salesColumns[10] = new nlobjSearchColumn('custrecord_do_order_priority');
		salesColumns[11] = new nlobjSearchColumn('custrecord_ordline_wms_location');
		salesColumns[12] = new nlobjSearchColumn('custrecord_lineord');
		salesColumns[13] = new nlobjSearchColumn('custrecord_ship_qty');
		salesColumns[14] = new nlobjSearchColumn('custrecord_printflag');
		salesColumns[15] = new nlobjSearchColumn('custrecord_ebiz_pr_dateprinted');
		if(vassemblies == true)
			salesColumns[16] = new nlobjSearchColumn('buildentireassembly', 'custrecord_ebiz_linesku');
		
		var salessearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, salessearchfilters, salesColumns);
		if(salessearchresults != null)
		{
			form.setScript('customscript_ordersummary_details');
			form.addButton('custpage_print','Print','Printreport()');
		}
		var createdsoord=0,createdsolines=0,fulfillsoord=0,fulfillsolines=0,inpickssoord=0,inpickssolines=0,pickconfirmedsoord=0,pickconfirmedsolines=0,Shippedsoord=0,Shippedsolines=0;

		for (var i = 0; salessearchresults != null && i < salessearchresults.length; i++) {


			var dono=salessearchresults[i].getValue('custrecord_lineord');
			var temparray=dono.split('.');

			var ordno = temparray[0];
			var customer= salessearchresults[i].getText('custrecord_do_customer');
			var orddate = salessearchresults[i].getValue('custrecord_record_linedate');


			var fulfillordno = salessearchresults[i].getValue('custrecord_lineord');
			var fulfillordstatus= salessearchresults[i].getText('custrecord_linestatus_flag');
			var fulfillordstatusvalue= salessearchresults[i].getValue('custrecord_linestatus_flag');
			var waveno = salessearchresults[i].getValue('custrecord_ebiz_wave');
			var item = salessearchresults[i].getText('custrecord_ebiz_linesku');
			var assembly = "";
			if(vassemblies == true)
			 assembly = salessearchresults[i].getValue('buildentireassembly', 'custrecord_ebiz_linesku');
			var Priority = salessearchresults[i].getValue('custrecord_do_order_priority');
			var location = salessearchresults[i].getText('custrecord_ordline_wms_location');
			var shipqty = salessearchresults[i].getValue('custrecord_ship_qty');
			nlapiLogExecution('ERROR', 'fulfillordstatusvalue', fulfillordstatusvalue);

			if(assembly == 'T')
				assembly='YES';
			else
				assembly='NO';

			var fulfillflag = salessearchresults[i].getValue('custrecord_linestatus_flag');

			if(fulfillflag == '25')
				fulfillflag='YES';
			else
				fulfillflag='NO';
			var ordqty = salessearchresults[i].getValue('custrecord_ord_qty');
			var pickedqty = salessearchresults[i].getValue('custrecord_pickqty');
			var pickeddate = salessearchresults[i].getValue('custrecord_ebiz_pr_dateprinted');
			
			var printdate="";
			if(pickeddate!=null&&pickeddate!="")
				{
//				nlapiLogExecution('ERROR','PICKEDDATE1',pickeddate);
				printdate=pickeddate;
				
				}
			var print= salessearchresults[i].getValue('custrecord_printflag');
			var printflag="";
			if(print=='T')
				{
				printflag="Yes";
				}
			else
				{
				printflag="No";
				}

			form.getSubList('custpage_items').setLineItemValue('custpage_ordno', i + 1, ordno);
			form.getSubList('custpage_items').setLineItemValue('custpage_customerlist', i + 1, customer);
			form.getSubList('custpage_items').setLineItemValue('custpage_orderdate', i + 1, orddate);			
			form.getSubList('custpage_items').setLineItemValue('custpage_ufillmentflag', i + 1, fulfillflag);			 
			form.getSubList('custpage_items').setLineItemValue('custpage_fulfillordno', i + 1, fulfillordno);
			form.getSubList('custpage_items').setLineItemValue('custpage_fulfillordnostatus', i + 1, fulfillordstatus);
			form.getSubList('custpage_items').setLineItemValue('custpage_waveno', i + 1, waveno);
			form.getSubList('custpage_items').setLineItemValue('custpage_item', i + 1, item);
			form.getSubList('custpage_items').setLineItemValue('custpage_assembly', i + 1, assembly);			 
			form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, ordqty);
			form.getSubList('custpage_items').setLineItemValue('custpage_pickedqty', i + 1, pickedqty);	
			form.getSubList('custpage_items').setLineItemValue('custpage_priority', i + 1, Priority);
			//form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, location);
			form.getSubList('custpage_items').setLineItemValue('custpage_print', i + 1, printflag);
			form.getSubList('custpage_items').setLineItemValue('custpage_printdate', i + 1, printdate);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipqty', i + 1, shipqty);



		}

		response.writePage(form);
	}
}

function Printreport(){

	var salessearchfilters = new Array();
	var tempstring;
	if ((nlapiGetFieldValue('custpage_fromdate') != "" && nlapiGetFieldValue('custpage_fromdate') != null) && (nlapiGetFieldValue('custpage_todate') != "" && nlapiGetFieldValue('custpage_todate') != null) ) {

		tempstring="&custpage_fromdate="+nlapiGetFieldValue('custpage_fromdate')+"&custpage_todate="+nlapiGetFieldValue('custpage_todate');
	} 
	if ((nlapiGetFieldValue('custpage_fulfillmentorder') != "" && nlapiGetFieldValue('custpage_fulfillmentorder') != null)){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_fulfillmentorder="+nlapiGetFieldValue('custpage_fulfillmentorder');
		}
		else
		{
			tempstring="&custpage_fulfillmentorder="+nlapiGetFieldValue('custpage_fulfillmentorder');
		}

	}	
	if (nlapiGetFieldValue('custpage_order') !=null && nlapiGetFieldValue('custpage_order') !=""){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_order="+nlapiGetFieldValue('custpage_order');
		}
		else
		{
			tempstring="&custpage_order="+nlapiGetFieldValue('custpage_order');
		}

	}
	if (nlapiGetFieldValue('custpage_wave') !=null && nlapiGetFieldValue('custpage_wave') !=""){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_wave="+nlapiGetFieldValue('custpage_wave');
		}
		else
		{
			tempstring="&custpage_wave="+nlapiGetFieldValue('custpage_wave');
		}

	}

	if (nlapiGetFieldValue('custpage_customer') !=null && nlapiGetFieldValue('custpage_customer') !=""){

		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_customer="+nlapiGetFieldValue('custpage_customer');
		}
		else
		{
			tempstring="&custpage_customer="+nlapiGetFieldValue('custpage_customer');
		}

	}
	if (nlapiGetFieldValue('custpage_location') !=null && nlapiGetFieldValue('custpage_location') !=""){
		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_location="+nlapiGetFieldValue('custpage_location');
		}
		else
		{
			tempstring="&custpage_location="+nlapiGetFieldValue('custpage_location');
		}

	}     
	if (nlapiGetFieldValue('custpage_salesorder') !=null && nlapiGetFieldValue('custpage_salesorder') !=""){
		if(tempstring!=null)
		{
			tempstring=tempstring+"&custpage_salesorder="+nlapiGetFieldValue('custpage_salesorder');
		}
		else
		{
			tempstring="&custpage_salesorder="+nlapiGetFieldValue('custpage_salesorder');
		}

	}    
	var BolPDFURL = nlapiResolveURL('SUITELET', 'customscript_ordsummarydetailsreport', 'customdeploy_ordsummarydetailsreport');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		BolPDFURL = 'https://system.netsuite.com' + BolPDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			BolPDFURL = 'https://system.sandbox.netsuite.com' + BolPDFURL;				
		}	*/
	BolPDFURL=BolPDFURL+tempstring;
	window.open(BolPDFURL);
}
/**
 * Function to return the current date in mm/dd/yyyy format
 * @author Phani
 * @returns Current system date
 */
/*function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}*/
