
/***************************************************************************
��������������������������
���������������������eBizNET Solutions
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountFIFODate.js,v $
*� $Revision: 1.1.2.1.4.5.2.4.4.1 $
*� $Date: 2015/11/16 15:34:11 $
*� $Author: rmukkera $
*� $Name: t_WMS_2015_2_StdBundle_1_152 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_CycleCountFIFODate.js,v $
*� Revision 1.1.2.1.4.5.2.4.4.1  2015/11/16 15:34:11  rmukkera
*� case # 201415115
*�
*� Revision 1.1.2.1.4.5.2.4  2014/05/30 00:34:20  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.1.2.1.4.5.2.3  2014/04/16 07:57:30  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue fixed related to case#20148006
*�
*� Revision 1.1.2.1.4.5.2.2  2013/04/17 16:02:37  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.1.2.1.4.5.2.1  2013/03/05 13:35:38  rmukkera
*� Merging of lexjet Bundle files to Standard bundle
*�
*� Revision 1.1.2.1.4.5  2013/02/18 06:28:30  skreddy
*� CASE201112/CR201113/LOG201121
*�  RF Lot auto generating FIFO enhancement
*�
*� Revision 1.1.2.1.4.4  2013/02/14 01:35:04  kavitha
*� CASE201112/CR201113/LOG201121
*� Cycle count process - Issue fixes
*�
*� Revision 1.1.2.1.4.3  2012/10/05 06:34:12  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting Multiple language into given suggestions
*�
*� Revision 1.1.2.1.4.2  2012/09/26 12:33:26  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.1.2.1.4.1  2012/09/21 15:01:30  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.1.2.1  2012/03/21 15:11:00  spendyala
*� CASE201112/CR201113/LOG201121
*� New Screen added to create BatchNo.
*�
*
****************************************************************************/


/**
 * @param request
 * @param response
 */
function CycleCountFIFODate(request, response){
	if (request.getMethod() == 'GET') {

		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var getRecordId = request.getParameter('custparam_recordid');
		nlapiLogExecution('ERROR','getRecprdId',getRecordId);
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName =  request.getParameter('custparam_begin_location_name');
		var getPackCode =  request.getParameter('custparam_packcode');
		var getExpectedQuantity =  request.getParameter('custparam_expqty');
		var getExpLPNo = request.getParameter('custparam_lpno');
		var getActLPNo = request.getParameter('custparam_actlp');
		var getExpItem = request.getParameter('custparam_expitem');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');

		var getActualItem = request.getParameter('custparam_actitem');
		var itemInternalid = request.getParameter('custparam_expiteminternalid');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var getActualPackCode = request.getParameter('custparam_actpackcode');	
		var getBatchNo = request.getParameter('custparam_actbatch');
		var getMfgDate = request.getParameter('custparam_mfgdate');
		var getExpDate = request.getParameter('custparam_expdate');
		var getBestBeforeDate = request.getParameter('custparam_bestbeforedate');
		
		var getLanguage = request.getParameter('custparam_language');
		var ItemShelfLife = request.getParameter('custparam_shelflife');
		var CaptureExpiryDate = request.getParameter('custparam_captureexpirydate');
		var CaptureFifoDate = request.getParameter('custparam_capturefifodate');
		var planitem=request.getParameter('custparam_planitem');

	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "CHECKIN FECHA DE CADUCIDAD";
			st1 = "&#218;LTIMA FECHA DISPONIBLE";
			st2 = "FECHA FIFO";
			st3 = "C&#211;DIGO FIFO";
			st4 = "FORMATO: MMDDAA";			
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
							
		}
		else
		{
			st0 = "CHECKIN EXPIRY DATE";
			st1 = "LAST AVL DATE";
			st2 = "FIFO DATE";
			st3 = "FIFO CODE";
			st4 = "FORMAT : MMDDYY";
			st5 = "SEND";
			st6 = "PREV";

		}
		
		
		
		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecount_fifo_date'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecount_fifo_date' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":</td>";
		html = html + "				<td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";		
		html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";	
		html = html + "				<input type='hidden' name='hdniteminternalid' value=" + itemInternalid + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
        html = html + "				<input type='hidden' name='hdnItemShelfLife' value=" + ItemShelfLife + ">";
		html = html + "				<input type='hidden' name='hdnCaptureExpiryDate' value=" + CaptureExpiryDate + ">";
		html = html + "				<input type='hidden' name='hdnCaptureFifoDate' value=" + CaptureFifoDate + ">";	
		html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";	

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteravldate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ":</td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfifodate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + ": </td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfifocode' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		try {
			var optedEvent = request.getParameter('cmdPrevious');
			var getLastAvlDate = request.getParameter('enteravldate');
			var getFifoDate = request.getParameter('enterfifodate');
			var getFifoCode = request.getParameter('enterfifocode');
			var error='';
			var errorflag='F';
			var CCarray = new Array();
			var getLanguage = request.getParameter('hdngetLanguage');
			CCarray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', CCarray["custparam_language"]);


			var st7,st8,st9;
			if( getLanguage == 'es_ES')
			{
				st7 = "ERROR EN LA FECHA FIFO";
				st8 = "&#218;LTIMA FECHA DEBE SER B / W MFG FECHA Y FECHA EXP";
				st9 = "FECHAFIFO DEBEN SER B / W MFG FECHA Y FECHA EXP";

			}
			else
			{
				st7 = "ERROR IN FIFO DATE";
				st8 = "LASTDATE SHLD BE B/W MFGDATE AND EXPDATE";
				st9 = "FIFODATE SHLD BE B/W MFGDATE AND EXPDATE";

			}



			CCarray["custparam_error"] = st7;
			CCarray["custparam_screenno"] = '35B';
			CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
			CCarray["custparam_planno"] = request.getParameter('custparam_planno');
			CCarray["custparam_begin_location_id"] = request.getParameter('custparam_begin_location_id');
			CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
			CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
			CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
			CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
			CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
			CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
			CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
			CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');

			CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
			CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
			CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
			CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
			CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
			CCarray["custparam_actbatch"] = request.getParameter('hdnBatchNo');
			CCarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
			CCarray["custparam_expdate"] = request.getParameter('hdnExpDate');
			CCarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');			
			CCarray["custparam_shelflife"] = request.getParameter('hdnItemShelfLife');
			CCarray["custparam_captureexpirydate"] = request.getParameter('hdnCaptureExpiryDate');
			CCarray["custparam_capturefifodate"] = request.getParameter('hdnCaptureFifoDate');
			var itemShelflife = request.getParameter('hdnItemShelfLife');
			var CaptureExpirydate = request.getParameter('hdnCaptureExpiryDate');
			var CaptureFifodate = request.getParameter('hdnCaptureFifoDate');
			CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');

			if (optedEvent == 'F7') {
				nlapiLogExecution('DEBUG', 'CHECK IN FIFO DATE F7 Pressed');
				if(CaptureExpirydate=='T')	
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_exp_date', 'customdeploy_rf_cyclecount_exp_date_di', false, CCarray);
				else
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecountbatch', 'customdeploy_rf_cyclecountbatch_di', false, CCarray);
			}
			else {
				var ValueLastDate="";
				var ValueFiFoDate="";
				if (getFifoDate != '' || getFifoCode != '' || getLastAvlDate != '') 
				{
					var getFifoln=getFifoDate.length;
					var getLastAvlln=getLastAvlDate.length;
					nlapiLogExecution('ERROR', 'getLastAvlln', getFifoln+"/"+getLastAvlln);
					if(getFifoln>6||getLastAvlln>6)
						errorflag="T";
				}
				nlapiLogExecution('ERROR','Test2',errorflag);
				if(errorflag=="F")
				{
					if (getLastAvlDate != '' && getLastAvlDate !=null)
					{
						var Lastdate= RFDateFormat(getLastAvlDate);

						if(Lastdate[0]=='true')
						{
							ValueLastDate=Lastdate[1];
							CCarray["custparam_lastdate"]=Lastdate[1];
						}
						else {
							errorflag='T';
							error='LastDate:'+Lastdate[1];
						}
					}
					else
					{
						CCarray["custparam_lastdate"]='';
					}

					if (getFifoDate == null || getFifoDate == "") {

						CCarray["custparam_fifodate"] = DateStamp();
						ValueFiFoDate=DateStamp();
						/*if (request.getParameter('hdnExpDate') != null || request.getParameter('hdnExpDate') != "") {
						CCarray["custparam_fifodate"] = request.getParameter('hdnExpDate');
						ValueFiFoDate=request.getParameter('hdnExpDate');
					}
					else 
						if (request.getParameter('hdnMfgDate') != null || request.getParameter('hdnMfgDate') != "") {
							CCarray["custparam_fifodate"] = request.getParameter('hdnMfgDate');
							ValueFiFoDate=request.getParameter('hdnMfgDate');
						}*/
					}
					else {
						nlapiLogExecution('ERROR','Befor','Before');
						var Fifodate= RFDateFormat(getFifoDate);
						if(Fifodate[0]=='true')
						{
							nlapiLogExecution('ERROR','Fifodate',Fifodate[1]);
							CCarray["custparam_fifodate"]=Fifodate[1];
							ValueFiFoDate=Fifodate[1];
						}
						else {
							errorflag='T';
							error='Fifodate:'+Fifodate[1];
						}
					}
					nlapiLogExecution('ERROR','error',error);
					if(errorflag=='F')
					{
						//Validating the condn LastDate and FIFODate shld be in b/w MfgDate and ExpDate.
						nlapiLogExecution('ERROR','Expdate',request.getParameter('hdnExpDate'));
						nlapiLogExecution('ERROR','Mfgdate',request.getParameter('hdnMfgDate'));
						nlapiLogExecution('ERROR','ValueLastDate',ValueLastDate);
						nlapiLogExecution('ERROR','ValueFiFoDate',ValueFiFoDate);
						if (Date.parse(request.getParameter('hdnMfgDate'))>= Date.parse(ValueLastDate)|| Date.parse(ValueLastDate)> Date.parse(request.getParameter('hdnExpDate'))) 
						{
							nlapiLogExecution('ERROR','chkpt1','chkpt1');
							CCarray["custparam_error"] = st8;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						}
						else if (Date.parse(request.getParameter('hdnMfgDate'))>= Date.parse(ValueFiFoDate)|| Date.parse(ValueLastDate) > Date.parse(request.getParameter('hdnExpDate'))) 
						{
							nlapiLogExecution('ERROR','chkpt2','chkpt2');
							CCarray["custparam_error"] = st9;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						} 
						else if (Date.parse(ValueFiFoDate) >= Date.parse(request.getParameter('hdnExpDate'))) 
						{
							nlapiLogExecution('ERROR','FifoDate greater than Exp Date','sucess');
							CCarray["custparam_error"] = "FIFO Date is greater than ExpDate";
							response.sendRedirect('SUITELET', 'customscript_ebiz_cyclecount_fifo_confir', 'customdeploy_ebiz_cyclecnt_fifo_conf_di', false, CCarray);
						} 	
						else
						{
							nlapiLogExecution('ERROR','chkpt3','chkpt3');
							CCarray["custparam_fifocode"] = getFifoCode;
							var rec=CreateBatchEntryRec(CCarray);
							response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
						}
					}
					else
					{
						nlapiLogExecution('ERROR','chkpt4','chkpt4');
						CCarray["custparam_error"] = error;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					}
				}
				else
				{
					nlapiLogExecution('ERROR','chkpt4','chkpt4');
					CCarray["custparam_error"] = "INVALID DATE FORMATE";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
				}
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			nlapiLogExecution('DEBUG', 'Catch: Location not found');

		}
	}
}



/**
 * @param CCarray
 */
function CreateBatchEntryRec(CCarray)
{
	try {
		var site="";
		nlapiLogExecution('ERROR', 'in');

		nlapiLogExecution('ERROR', 'BATCH NOT FOUND');
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

		customrecord.setFieldValue('name', CCarray["custparam_actbatch"]);
		customrecord.setFieldValue('custrecord_ebizlotbatch', CCarray["custparam_actbatch"]);
		customrecord.setFieldValue('custrecord_ebizmfgdate', CCarray["custparam_mfgdate"]);
		customrecord.setFieldValue('custrecord_ebizbestbeforedate', CCarray["custparam_bestbeforedate"]);
		customrecord.setFieldValue('custrecord_ebizexpirydate', CCarray["custparam_expdate"]);
		customrecord.setFieldValue('custrecord_ebizfifodate', CCarray["custparam_fifodate"]);
		customrecord.setFieldValue('custrecord_ebizfifocode', CCarray["custparam_fifocode"]);
		customrecord.setFieldValue('custrecord_ebizlastavldate', CCarray["custparam_lastdate"]);
		nlapiLogExecution('ERROR', '1');
		
		//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
		customrecord.setFieldValue('custrecord_ebizsku', CCarray["custparam_actitem"]);
		customrecord.setFieldValue('custrecord_ebizpackcode',CCarray["custparam_packcode"]);


		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', CCarray["custparam_planno"]);

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_cyclesite_id');

		nlapiLogExecution('ERROR', 'Before Search Results Id', 'getSearchResultsId');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, column);
		if(searchresults)
			site=searchresults[0].getValue('custrecord_cyclesite_id');
		if(site!=null&&site!="")
			//customrecord.setFieldValue('custrecord_ebizsitebatch',site);
		var rec = nlapiSubmitRecord(customrecord, false, true);
		nlapiLogExecution('ERROR', 'rec',rec);

	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into BATCH ENTRY Record',e);
	}
}

