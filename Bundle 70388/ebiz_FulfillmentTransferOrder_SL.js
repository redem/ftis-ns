/**
 * 
 */

function backOrderFulfillmentforTO(type)
{
	nlapiLogExecution('ERROR','Into  backOrderFulfillmentforTO','');
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());
	
	
	var vConfig=nlapiLoadConfiguration('companyfeatures');
	var UOMruleValue=vConfig.getFieldValue('unitsofmeasure');

	nlapiLogExecution('DEBUG','UOMruleValue',UOMruleValue);
	
	//if ( type != 'scheduled') return; 
//	var vintrid=0;
//	var vinternalid=0;

	//get the seq no from wave record.
//	vintrid = GetMaxTransactionNo('TO');

	var vInternalIdArray=GetAllTOInternalID_BackOrder();

	if(vInternalIdArray!=null&&vInternalIdArray!="")
	{		
		var filters = new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
		//filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0
		filters.push(new nlobjSearchFilter('transactionlinetype', null, 'anyof', 'ITEM'));

		// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['TrnfrOrd:B','TrnfrOrd:D','TrnfrOrd:E']));
		filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
		filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
//		if(parseFloat(vintrid)>1)
//		{
//		nlapiLogExecution('ERROR','In side internalid in search filter',vintrid);
//		filters.push(new nlobjSearchFilter('internalidnumber', null, 'lessthanorequalto', vintrid));
//		}

		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', vInternalIdArray));
		//filters.push(new nlobjSearchFilter('transactionlinetype', null, 'anyof', 'ITEM'));

		var columns = new Array();

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');	
		columns[1] = new nlobjSearchColumn('line');
		columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
		columns[3] = new nlobjSearchColumn('quantitycommitted');
		columns[4]=new nlobjSearchColumn('formulanumeric');
		columns[4].setFormula('ABS({quantity})');
		columns[5] = new nlobjSearchColumn('trandate');
		columns[6] = new nlobjSearchColumn('entity');
		columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
		columns[8] = new nlobjSearchColumn('shipmethod');
		columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
		columns[10] = new nlobjSearchColumn('custbody_nswms_company');
		columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
		columns[12] = new nlobjSearchColumn('location');
		columns[13] = new nlobjSearchColumn('item');
		columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
		columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
		columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');
		
		columns[17] = new nlobjSearchColumn('tranid');
		columns[18] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
		columns[19] = new nlobjSearchColumn('quantityshiprecv');
		columns[20] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
		columns[21] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
		columns[22] = new nlobjSearchColumn('custbody_shipment_no');
		columns[23] = new nlobjSearchColumn('custbody_nswmssoordertype');
		columns[24] = new nlobjSearchColumn('transferorderquantitycommitted');
		if(UOMruleValue=='T')
		{
			columns[25] = new nlobjSearchColumn('unit');
		}

		//true - Discending false - Ascending
		columns[0].setSort(true);
		columns[1].setSort();

		var salesOrderLineDetails = new nlapiSearchRecord('transferorder', null, filters, columns);
		nlapiLogExecution('ERROR','Remaining Usage 2',context.getRemainingUsage());
		if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
		{	
			var createfo = 'T'; //Added 08-Nov-2011
			var sonumber = 0; //Added 08-Nov-2011
			var oldsoname='';
			var fullFillmentorderno='';

			nlapiLogExecution('ERROR','salesOrderLineDetails.length ',salesOrderLineDetails.length);
			for(var i=0;i<salesOrderLineDetails.length;i++)
			{
				nlapiLogExecution('ERROR','# Loop',i);
				vinternalid=salesOrderLineDetails[i].getValue('internalid');
				//nlapiLogExecution('ERROR','vinternalid',vinternalid);

				var soname = salesOrderLineDetails[i].getValue('tranid');
				var solineshipmethod=salesOrderLineDetails[i].getValue('shipmethod');
				var solocation = salesOrderLineDetails[i].getValue('location');
				var orderqty = salesOrderLineDetails[i].getValue('formulanumeric');
				if(orderqty < 0)
					orderqty=orderqty*(-1);
				var create_fulfillment_order= salesOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
				var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
				//var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
				var commetedQty = salesOrderLineDetails[i].getValue('transferorderquantitycommitted');
				if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
					commetedQty=0;

//				var trcommetedQty = salesOrderLineDetails[i].getValue('transferorderquantitycommitted');
//				if(trcommetedQty==null || trcommetedQty=='' || isNaN(trcommetedQty))
//				trcommetedQty=0;

				var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
				var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');

				// Assuming search results will return the data in the descending order of SO#

				if(soname != null)
				{
					if(sonumber != soname)
					{
						sonumber = soname;
						createfo = 'T';
						nlapiLogExecution('ERROR','SO Number',sonumber);

						if(shpcompleteflag != null && shpcompleteflag == 'T')
						{						
							for(var j=0;j<salesOrderLineDetails.length;j++)
							{
								if(sonumber == salesOrderLineDetails[j].getValue('tranid'))
								{
									var solineordqty = salesOrderLineDetails[j].getValue('quantity');
									if(solineordqty < 0)
										solineordqty=solineordqty*(-1);
									var solinecmmtdqty = salesOrderLineDetails[j].getValue('quantitycommitted');
									var lineitem=salesOrderLineDetails[j].getValue('item');
									var shipmethod=salesOrderLineDetails[j].getValue('shipmethod');
									if(solinecmmtdqty==null || solinecmmtdqty=='' || isNaN(solinecmmtdqty))
										solinecmmtdqty=0;

									var solinefulfildqty = salesOrderLineDetails[j].getValue('quantityshiprecv');

									if(solinefulfildqty == null || solinefulfildqty == '' || isNaN(solinefulfildqty))
										solinefulfildqty = 0;

									var totalcomtdqty=parseFloat(solinecmmtdqty)+parseFloat(solinefulfildqty);
									if(lineitem!=shipmethod)//Temporary fix to avoid unnecessary lines
									{
										if(parseFloat(solineordqty) > parseFloat(totalcomtdqty))
										{
											createfo = 'F';
											continue;
										}
									}
								}

							}

							nlapiLogExecution('ERROR','Create FO',createfo);
						}						
					}				
				}
				if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T')
				{
					try
					{
						if(soname != null && soname != '')
						{	
							//To lock the sales order
							nlapiLogExecution('DEBUG','soname before Locking',soname);
							nlapiLogExecution('DEBUG','trantype before Locking','transferorder');
							var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
							fulfillRec.setFieldValue('name', soname);					
							fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'transferorder');
							fulfillRec.setFieldValue('externalid', soname);
							fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by Scheduler');
							nlapiSubmitRecord(fulfillRec,false,true );
							nlapiLogExecution('DEBUG','Locked successfully');
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
						nlapiLogExecution('DEBUG', 'DEBUG', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.');
						//var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
						//throw wmsE;
						continue;
					}
					try
					{
					var variance=0;

					var totalQuantity = getFulfilmentqty(salesOrderLineDetails[i].getId(), salesOrderLineNo);
					nlapiLogExecution('ERROR','Remaining Usage 3',context.getRemainingUsage());
					nlapiLogExecution('ERROR','SO Name',soname);
					nlapiLogExecution('ERROR','SO Line',salesOrderLineNo);
					nlapiLogExecution('ERROR','Order Qty',orderqty);
					nlapiLogExecution('ERROR','Commetted Qty',commetedQty);
					nlapiLogExecution('ERROR','Created FO Qty',totalQuantity);
					nlapiLogExecution('ERROR','Shipped Qty',shippedqty);
					//nlapiLogExecution('ERROR','TO Commetted Qty',trcommetedQty);

					var totalcomtdqty=parseFloat(commetedQty)+parseFloat(shippedqty);

					if(totalQuantity == 0)
						variance=commetedQty;
					else
					{
						if(parseFloat(totalQuantity) < parseFloat(totalcomtdqty))
						{
							variance=parseFloat(totalcomtdqty)-parseFloat(totalQuantity);
						}
					}

					nlapiLogExecution('ERROR','Variance',variance);

					if(variance > 0)
					{
						if(oldsoname!=soname)
						{
							//generate new fullfillment order no.
							//fullFillmentorderno=fulfillmentOrderDetails(salesOrderLineDetails[i].getId(),soname);
							fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod);
							oldsoname=soname;
						}
						nlapiLogExecution('ERROR','Remaining Usage 4',context.getRemainingUsage());
						nlapiLogExecution('ERROR','fullFillmentorderno',fullFillmentorderno);

						createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,salesOrderLineDetails[i],soname,salesOrderLineDetails[i].getId());
						nlapiLogExecution('ERROR','Remaining Usage 5',context.getRemainingUsage());							
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG','exception in auto fulfillment creation',e.message);
					}
					finally
					{
						nlapiLogExecution('DEBUG','soname before unlocking',soname);
						nlapiLogExecution('DEBUG','trantype before unlocking','transferorder');

						if(soname != null && soname != '')
						{	
							var ExternalIdFilters = new Array();
							ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
							ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'transferorder'));
							var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
							if(searchresultsExt != null && searchresultsExt != '')
							{
								nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
								nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
								nlapiLogExecution('DEBUG','Unlocked successfully');
							}	
						}
					}

				}			
				//Update seq no in wave table if the remaining use is less than 500
				if(context.getRemainingUsage()<500)
				{				
					//nlapiLogExecution('ERROR','vinternalid',vinternalid);	
					//updateSeqNo('TO',vinternalid);
					nlapiLogExecution('ERROR','break');
					break;
				}
			}
			nlapiLogExecution('ERROR','salesOrderLineDetails.length2 ',salesOrderLineDetails.length);

//			if(salesOrderLineDetails.length<1000)
//			{
//			updateSeqNo('TO',0);
//			}
//			else
//			{
//			nlapiLogExecution('ERROR','vinternalid',vinternalid);
//			updateSeqNo('TO',vinternalid);			
//			}

		}
	}

	nlapiLogExecution('ERROR','Remaining Usage at the end ',context.getRemainingUsage());	

	nlapiLogExecution('ERROR','Out of  backOrderFulfillmentforTO','');
}

/**
 * To get the sum of all the order qty of the particular line no and SO, 
 * from fullfillment order table
 * @param soid
 * @param lineno
 * @returns Totalqty
 */
function getFulfilmentqty(soid,lineno)
{
	var vqty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', soid.toString());
	filters[1] = new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineno);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	var searchresultsRcpts = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	if (searchresultsRcpts != null) 
	{
		var val = searchresultsRcpts[0];
		vqty=val.getValue('custrecord_ord_qty',null, 'sum');
		//nlapiLogExecution('ERROR','vqtysum',vqty);
		if(vqty==null)
			return 0;
		else
			return vqty;
	}
}


/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
function fulfillmentOrderDetailsforSchedulescript(salesOrderId,salesordername,solocation,solineshipmethod)
{
	/***Upto here ***/
	var fulfillmentNumber =  '';
	var salesOrderArray = new Array();
	var searchFulfillment =  getFullFillment(salesOrderId);
	//nlapiLogExecution('DEBUG','searchFulfillment :', searchFulfillment);

	//If searchfulfillment is null it indicates Its a New SO. 
	if(searchFulfillment == null){
		var fulfillmentNumber=salesordername +".1";
	}
	else{
		for(var i=0; i< searchFulfillment.length;i++)
		{
			var fullfillmentlineord = searchFulfillment[i].getValue('custrecord_lineord');
			var folinestatus = searchFulfillment[i].getValue('custrecord_linestatus_flag');
			/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
			var folocation = searchFulfillment[i].getValue('custrecord_ordline_wms_location'); //
			var foshipmethod = searchFulfillment[i].getValue('custrecord_do_carrier'); //
			nlapiLogExecution("ERROR","INTO fulfillment details",folinestatus+"/"+folocation+"-"+solocation+"/"+foshipmethod+"-"+solineshipmethod);
			/***Upto here***/
			var lineordsplit = fullfillmentlineord.split('.');
			salesOrderArray[i] = lineordsplit[1];
			/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
			if(folinestatus==25 && solocation==folocation && solineshipmethod==foshipmethod )
			{/***upto here ***/
				fulfillmentNumber=fullfillmentlineord;
				return fulfillmentNumber;
			}			
		}
		//MaxValue returns the max fullfillment order number
		var maximumValue = MaxValue(salesOrderArray);

		//adding 1 to the max fullfillment order number to get the new fullfillment order no.
		var fulfillmentValue = parseInt(maximumValue) + parseInt(1);
		nlapiLogExecution('DEBUG','fulfillment Value :', fulfillmentValue);

		//concatinating the fulfillmentvalue to salesorderid to get new fullfillment no.
		fulfillmentNumber =  salesordername + "." + fulfillmentValue;
		//nlapiLogExecution('DEBUG','fulfillmentNumber :', fulfillmentNumber);
	}
	return fulfillmentNumber;
}


/**
 *  Generate the fulfillment order number based on the existing fulfillment orders
 *  	by concatenating the sales order number with the maximum value of the fulfillment order number.
 * @param salesOrderId
 * @returns {String}
 */
function fulfillmentOrderDetails(salesOrderId,salesordername)
{
	var salesOrderArray = new Array();
	var searchFulfillment =  getFullFillment(salesOrderId);
	//nlapiLogExecution('ERROR','searchFulfillment :', searchFulfillment);

	//If searchfulfillment is null it indicates Its a New SO. 
	if(searchFulfillment == null){
		var fulfillmentNumber=salesordername +".1";
	}
	else{
		for(var i=0; i< searchFulfillment.length;i++)
		{
			var fullfillmentlineord = searchFulfillment[i].getValue('custrecord_lineord');
			var lineordsplit = fullfillmentlineord.split('.');
			salesOrderArray[i] = lineordsplit[1];
		}
		//MaxValue returns the max fullfillment order number
		var maximumValue = MaxValue(salesOrderArray);

		//adding 1 to the max fullfillment order number to get the new fullfillment order no.
		var fulfillmentValue = parseFloat(maximumValue) + parseFloat(1);
		nlapiLogExecution('ERROR','fulfillment Value :', fulfillmentValue);

		//concatinating the fulfillmentvalue to salesorderid to get new fullfillment no.
		var fulfillmentNumber =  salesordername + "." + fulfillmentValue;
		//nlapiLogExecution('ERROR','fulfillmentNumber :', fulfillmentNumber);
	}
	return fulfillmentNumber;
}

/**
 * Getting the maximum value for the fulfillment order.
 * @param array
 * @returns maximumNumber
 */
function MaxValue(array)
{
	var maximumNumber = array[0];
	for (var i = 0; i < array.length; i++) 
	{
		if (array[i] > maximumNumber) 
		{
			maximumNumber = array[i];
		}
	}
	return maximumNumber;
}

/**
 * searching for records in fullfillment order line
 * @param soId
 * @returns ordlinesearchresults
 */

function getFullFillment(soId)
{
	var ordlinefilters = new Array();
	ordlinefilters[0] = new nlobjSearchFilter('name', null, 'is', soId);
	var ordlinecolumns = new Array();  
	ordlinecolumns[0] = new nlobjSearchColumn('custrecord_lineord').setSort();        
	ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ordline');
	ordlinecolumns[2] = new nlobjSearchColumn('custrecord_ord_qty'); // fulfillment ordered quantity
	ordlinecolumns[3] = new nlobjSearchColumn('custrecord_linestatus_flag');
	ordlinecolumns[4] = new nlobjSearchColumn('custrecord_ordline_wms_location');
	ordlinecolumns[5] = new nlobjSearchColumn('custrecord_do_carrier');
	var ordlinesearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);
	return ordlinesearchresults;
}

function createbackorderFulfillmentOrdernew(lineno,variance,fullFillmentorderno,searchresults,tranid,salesOrderId)
{
	try
	{
		nlapiLogExecution('ERROR','Into createbackorderFulfillmentOrdernew','');
		//nlapiLogExecution('ERROR','variance',variance);
		var salesOrderNo = tranid;
		//nlapiLogExecution('ERROR','salesOrderNo',salesOrderNo);
		var salesOrderInternalId = salesOrderId;
		//nlapiLogExecution('ERROR','salesOrderInternalId',salesOrderInternalId);

		var salesOrderDate =searchresults.getValue('trandate');
		var customer = searchresults.getValue('entity');
		var priority = searchresults.getValue('custbody_nswmspriority');
		var orderType = searchresults.getValue('custbody_nswmssoordertype');
		var shipCarrierMethod = searchresults.getValue('shipmethod');	
		var company = searchresults.getValue('custbody_nswms_company');
		var route = searchresults.getValue('custbody_nswmssoroute');
		var location=searchresults.getValue('location');
		var shipcomplete = searchresults.getValue('shipcomplete');

		var locationname=searchresults.getText('location');
		nlapiLogExecution('ERROR','locationname',locationname);

		var statusbyloc='';

		//The following line of code is only for TNT
		//statusbyloc=getItemStatus(locationname);	

		nlapiLogExecution('ERROR','statusbyloc',statusbyloc);

		//*********header information************
		var orderLineRecord = nlapiCreateRecord('customrecord_ebiznet_ordline');
		orderLineRecord.setFieldValue('name', salesOrderInternalId);
		orderLineRecord.setFieldValue('custrecord_ns_ord', parseFloat(salesOrderInternalId));
		//orderLineRecord.setFieldValue('custrecord_ordline',parseFloat(salesOrderLineNo));
		orderLineRecord.setFieldValue('custrecord_do_order_priority', priority);
		orderLineRecord.setFieldValue('custrecord_do_order_type', orderType);
		orderLineRecord.setFieldValue('custrecord_do_customer', customer);
		orderLineRecord.setFieldValue('custrecord_do_carrier', shipCarrierMethod);	
		orderLineRecord.setFieldValue('custrecord_linestatus_flag', 25);
		orderLineRecord.setFieldValue('custrecord_route_no',route);
		orderLineRecord.setFieldValue('custrecord_lineord', fullFillmentorderno);
		orderLineRecord.setFieldValue('custrecord_ordline_company',company);
		orderLineRecord.setFieldValue('custrecord_ordline_wms_location',location);
		orderLineRecord.setFieldValue('custrecord_record_linetime',TimeStamp());
		orderLineRecord.setFieldValue('custrecord_shipcomplete',shipcomplete);

		//*******Item/Line information*************

		//Sales Order Line #
		var salesOrderLineNo =searchresults.getValue('line');
		orderLineRecord.setFieldValue('custrecord_ordline', salesOrderLineNo);
		//nlapiLogExecution('ERROR','salesOrderLineNo',salesOrderLineNo);	
		//nlapiLogExecution('ERROR','fullFillmentorderno',fullFillmentorderno);

		// Sales Order Item Id
		var itemId =searchresults.getValue('item');
		orderLineRecord.setFieldValue('custrecord_ebiz_linesku', itemId);
		//nlapiLogExecution('ERROR','itemId',itemId);

		var itemStatus =searchresults.getValue('custcol_ebiznet_item_status');

		var defpackcode = '1';

		if(statusbyloc!=null && statusbyloc!='')
		{
			orderLineRecord.setFieldText('custrecord_linesku_status', statusbyloc);
			nlapiLogExecution('ERROR','itemStatus',statusbyloc);
		}
		else
		{

			if(itemStatus==null || itemStatus=='')
			{
				var fields = ['custitem_ebizdefskustatus','custitem_ebizdefpackcode'];
				var columns= nlapiLookupField('item',itemId,fields);
				if(columns!=null)
				{
					itemStatus = columns.custitem_ebizdefskustatus;
					defpackcode = columns.custitem_ebizdefpackcode;
				}
			}
			orderLineRecord.setFieldValue('custrecord_linesku_status', itemStatus);
			//nlapiLogExecution('ERROR','itemStatus',itemStatus);
		}
		
		var lineordsplit = fullFillmentorderno.split('.');
		var backordline = lineordsplit[1];

		// Sales Order Item Pack Code
		orderLineRecord.setFieldValue('custrecord_ebiz_backorderno', backordline);
		var itemPackCode =searchresults.getValue('custcol_nswmspackcode');
		if(itemPackCode==null || itemPackCode=='')
		{
			itemPackCode=defpackcode;
		}
		orderLineRecord.setFieldValue('custrecord_linepackcode', itemPackCode);
		//nlapiLogExecution('ERROR','itemPackCode',itemPackCode);


		// Sales Order Item Uom
		var baseUom = searchresults.getValue('custcol_nswmssobaseuom');
		if(baseUom != null && baseUom != '')
			orderLineRecord.setFieldValue('custrecord_lineuom_id', parseFloat(baseUom));
		//nlapiLogExecution('ERROR','baseUom',baseUom);

		//var vNSUOM = searchresults.getLineItemValue('item','units', lineno);

		var vNSUOM = searchresults.getValue('unit');
		//nlapiLogExecution('ERROR', 'Order UOM', vNSUOM);

		var veBizUOMQty=0;
		var vBaseUOMQty=0;

		if(vNSUOM!=null && vNSUOM!="")
		{
//			if(vNSUOM=="EA")
//			vNSUOM="Each";
//			else if(vNSUOM=="CA") 
//			vNSUOM="Case";
//			else if(vNSUOM=="PLT") 
//			vNSUOM="Pallet";

			var eBizItemDims=geteBizItemDimensions(itemId);
			if(eBizItemDims!=null&&eBizItemDims.length>0)
			{
				for(z=0; z < eBizItemDims.length; z++)
				{
					//nlapiLogExecution('ERROR', 'Base UOM Flag', eBizItemDims[z].getValue('custrecord_ebizbaseuom'));
					if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
					{
						vBaseUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
						//nlapiLogExecution('ERROR', 'vBaseUOMQty', vBaseUOMQty);
					}

					if(vNSUOM == eBizItemDims[z].getValue('custrecord_ebiznsuom'))
					{
						//nlapiLogExecution('ERROR', 'SKU Dim UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
						veBizUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
						//nlapiLogExecution('ERROR', 'veBizUOMQty', veBizUOMQty);
					}
				}
				if(veBizUOMQty==null || veBizUOMQty=='')
				{
					veBizUOMQty=vBaseUOMQty;
				}
				//nlapiLogExecution('ERROR', 'veBizUOMQty', veBizUOMQty);
				//nlapiLogExecution('ERROR', 'variance', variance);
				//nlapiLogExecution('ERROR', 'vBaseUOMQty', vBaseUOMQty);


				if(veBizUOMQty!=0)
					variance = parseFloat(variance)*parseFloat(veBizUOMQty)/parseFloat(vBaseUOMQty);
				nlapiLogExecution('ERROR', 'variance', variance);
			}
		}

		// Sales Order Item Committed Quantity
		orderLineRecord.setFieldValue('custrecord_ord_qty', parseFloat(variance).toFixed(5));
		// Item Info for a particular Item
		var itemSubtype = nlapiLookupField('item', itemId, 'recordType');
		var fields = ['custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
		var columnsval = nlapiLookupField(itemSubtype, itemId, fields);
		var skuinfo1val = columnsval.custitem_item_info_1;
		var skuinfo2val = columnsval.custitem_item_info_2;
		var skuinfo3val = columnsval.custitem_item_info_3;

		orderLineRecord.setFieldValue('custrecord_fulfilmentiteminfo1',skuinfo1val);
		orderLineRecord.setFieldValue('custrecord_fulfilmentiteminfo2',skuinfo2val);
		orderLineRecord.setFieldValue('custrecord_fulfilmentiteminfo3',skuinfo3val);
		orderLineRecord.setFieldValue('custrecord_linenotes2','Created by Schedule Script for TO');

		try
		{
			var wmsCarrier = nlapiGetFieldValue('custbody_salesorder_carrier');
			var wmscarrierlinelevel = searchresults.getValue('custcol_salesorder_carrier_linelevel');
			nlapiLogExecution('ERROR', 'wmscarrier', wmsCarrier);
			nlapiLogExecution('ERROR', 'wmscarrierlinelevel', wmscarrierlinelevel);

			if (wmscarrierlinelevel != null && wmscarrierlinelevel != "")
			{

				orderLineRecord.setFieldValue('custrecord_do_wmscarrier', wmscarrierlinelevel);
			}
			else
			{

				orderLineRecord.setFieldValue('custrecord_do_wmscarrier', wmsCarrier);
			}


		}
		catch(exp) {
			nlapiLogExecution('ERROR', 'Exception in wmsCarrier : ', exp);		
		}


		//submit the record 
		if(parseFloat(variance) != 0 ){
			var recordId = nlapiSubmitRecord(orderLineRecord);
			//updatesalesorderline(salesOrderInternalId,salesOrderLineNo,variance,'E');
			nlapiLogExecution('ERROR','recordstatus','recordsubmited'+lineno);
		}		
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in createbackorderFulfillmentOrdernew : ', exp);		
	}

	nlapiLogExecution('ERROR','Out of createbackorderFulfillmentOrdernew','');
}

/**
 * 
 * @param itemid
 */
function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter.push(new nlobjSearchFilter('custrecord_ebiznsuom', null, 'noneof', '@NONE@'));
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}

function updateSeqNo(transType,seqno){
	nlapiLogExecution('ERROR', 'Into updateSeqNo', seqno);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_seqno');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_trantype', null, 'is', transType);

	// Search in ebiznet_wave custom record for all rows for the given transaction type
	var results = nlapiSearchRecord('customrecord_ebiznet_wave', null, filters, columns);
	if(results != null){
		for (var t = 0; t < results.length; t++) {
			nlapiSubmitField('customrecord_ebiznet_wave', results[t].getId(), 'custrecord_seqno', parseFloat(seqno));
		}
	}

	nlapiLogExecution('ERROR', 'Out of updateSeqNo', seqno);
}


function GetAllTOInternalID_BackOrder()
{
	try
	{
		nlapiLogExecution('DEBUG','Into GetAllTOInternalID_BackOrder');
		var SOInternalIdArray=new Array();
// case# 201417912
		//var resultSet=nlapiSearchRecord('Transaction','customsearch_ebiz_tofobackorderrelease',null,null);
		
		var resultSet=nlapiSearchRecord('Transaction','customsearch_ebiz_fo_backorder',null,null);
		if(resultSet!=null&&resultSet!="")
		{
			for(var i=0;i<resultSet.length;i++)
			{
				SOInternalIdArray.push(resultSet[i].getValue('internalid',null,'group'));
			}
		}
		nlapiLogExecution('DEBUG','Out of GetAllTOInternalID_BackOrder',SOInternalIdArray);
		return SOInternalIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetAllTOInternalID_BackOrder',exp);
	}
}


function backOrderFulfillmentforTO1s(type)
{
	nlapiLogExecution('ERROR','Into  backOrderFulfillmentforTO1s','');
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());
	
	var vConfig=nlapiLoadConfiguration('companyfeatures');
	var UOMruleValue=vConfig.getFieldValue('unitsofmeasure');

	nlapiLogExecution('DEBUG','UOMruleValue',UOMruleValue);
	
	
	//if ( type != 'scheduled') return; 
//	var vintrid=0;
//	var vinternalid=0;

	//get the seq no from wave record.
//	vintrid = GetMaxTransactionNo('TO');

	var vInternalIdArray=GetAllTOInternalID_1s();

	if(vInternalIdArray!=null&&vInternalIdArray!="")
	{		
		var filters = new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
		filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0

		// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['TrnfrOrd:B','TrnfrOrd:D','TrnfrOrd:E']));
		filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
		filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
		filters.push(new nlobjSearchFilter('transactionlinetype', null, 'anyof', 'ITEM'));
//		if(parseFloat(vintrid)>1)
//		{
//		nlapiLogExecution('ERROR','In side internalid in search filter',vintrid);
//		filters.push(new nlobjSearchFilter('internalidnumber', null, 'lessthanorequalto', vintrid));
//		}

		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', vInternalIdArray));
		//filters.push(new nlobjSearchFilter('transactionlinetype', null, 'anyof', 'ITEM'));

		var columns = new Array();

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');	
		columns[1] = new nlobjSearchColumn('line');
		columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
		columns[3] = new nlobjSearchColumn('quantitycommitted');
		columns[4]=new nlobjSearchColumn('formulanumeric');
		columns[4].setFormula('ABS({quantity})');
		columns[5] = new nlobjSearchColumn('trandate');
		columns[6] = new nlobjSearchColumn('entity');
		columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
		columns[8] = new nlobjSearchColumn('shipmethod');
		columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
		columns[10] = new nlobjSearchColumn('custbody_nswms_company');
		columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
		columns[12] = new nlobjSearchColumn('location');
		columns[13] = new nlobjSearchColumn('item');
		columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
		columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
		columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');		
		columns[17] = new nlobjSearchColumn('tranid');
		columns[18] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
		columns[19] = new nlobjSearchColumn('quantityshiprecv');
		columns[20] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
		columns[21] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
		columns[22] = new nlobjSearchColumn('custbody_shipment_no');
		columns[23] = new nlobjSearchColumn('custbody_nswmssoordertype');
		columns[24] = new nlobjSearchColumn('transferorderquantitycommitted');		
		if(UOMruleValue=='T')
		{
			columns[25] = new nlobjSearchColumn('unit');
		}

		//true - Discending false - Ascending
		columns[0].setSort(true);
		columns[1].setSort();


		var salesOrderLineDetails = new nlapiSearchRecord('transferorder', null, filters, columns);
		nlapiLogExecution('ERROR','Remaining Usage 2',context.getRemainingUsage());
		if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
		{	
			var createfo = 'T'; //Added 08-Nov-2011
			var sonumber = 0; //Added 08-Nov-2011
			var oldsoname='';
			var fullFillmentorderno='';

			nlapiLogExecution('ERROR','salesOrderLineDetails.length ',salesOrderLineDetails.length);
			for(var i=0;i<salesOrderLineDetails.length;i++)
			{
				nlapiLogExecution('ERROR','# Loop',i);
				vinternalid=salesOrderLineDetails[i].getValue('internalid');
				//nlapiLogExecution('ERROR','vinternalid',vinternalid);

				var soname = salesOrderLineDetails[i].getValue('tranid');
				var solineshipmethod=salesOrderLineDetails[i].getValue('shipmethod');
				var solocation = salesOrderLineDetails[i].getValue('location');
				var orderqty = salesOrderLineDetails[i].getValue('formulanumeric');
				if(orderqty < 0)
					orderqty=orderqty*(-1);
				var create_fulfillment_order= salesOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
				var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
				//var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
				var commetedQty = salesOrderLineDetails[i].getValue('transferorderquantitycommitted');
				if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
					commetedQty=0;

				var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
				var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');

				// Assuming search results will return the data in the descending order of SO#

				if(soname != null)
				{
					if(sonumber != soname)
					{
						sonumber = soname;
						createfo = 'T';
						nlapiLogExecution('ERROR','SO Number',sonumber);

						if(shpcompleteflag != null && shpcompleteflag == 'T')
						{						
							for(var j=0;j<salesOrderLineDetails.length;j++)
							{
								if(sonumber == salesOrderLineDetails[j].getValue('tranid'))
								{
									var solineordqty = salesOrderLineDetails[j].getValue('quantity');
									if(solineordqty < 0)
										solineordqty=solineordqty*(-1);
									var solinecmmtdqty = salesOrderLineDetails[j].getValue('quantitycommitted');
									var lineitem=salesOrderLineDetails[j].getValue('item');
									var shipmethod=salesOrderLineDetails[j].getValue('shipmethod');
									if(solinecmmtdqty==null || solinecmmtdqty=='' || isNaN(solinecmmtdqty))
										solinecmmtdqty=0;

									var solinefulfildqty = salesOrderLineDetails[j].getValue('quantityshiprecv');

									if(solinefulfildqty == null || solinefulfildqty == '' || isNaN(solinefulfildqty))
										solinefulfildqty = 0;

									var totalcomtdqty=parseFloat(solinecmmtdqty)+parseFloat(solinefulfildqty);
									if(lineitem!=shipmethod)//Temporary fix to avoid unnecessary lines
									{
										if(parseFloat(solineordqty) > parseFloat(totalcomtdqty))
										{
											createfo = 'F';
											continue;
										}
									}
								}

							}

							nlapiLogExecution('ERROR','Create FO',createfo);
						}						
					}				
				}
				if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T')
				{
					try
					{
						// To lock sales order
						if(soname != null && soname != '')
						{	
							nlapiLogExecution('DEBUG','soname before Locking',soname);
							nlapiLogExecution('DEBUG','trantype before Locking',trantype);
							var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
							fulfillRec.setFieldValue('name', soname);					
							fulfillRec.setFieldValue('custrecord_ebiznet_trantype', trantype);
							fulfillRec.setFieldValue('externalid', soname);
							fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by UE');
							nlapiSubmitRecord(fulfillRec,false,true );
							nlapiLogExecution('DEBUG','Locked successfully');
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
						nlapiLogExecution('DEBUG', 'DEBUG', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.');
						//var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
						//throw wmsE;
						continue;
					}

					try
					{
					var variance=0;

					var totalQuantity = getFulfilmentqty(salesOrderLineDetails[i].getId(), salesOrderLineNo);
					nlapiLogExecution('ERROR','Remaining Usage 3',context.getRemainingUsage());
					nlapiLogExecution('ERROR','SO Name',soname);
					nlapiLogExecution('ERROR','SO Line',salesOrderLineNo);
					nlapiLogExecution('ERROR','Order Qty',orderqty);
					nlapiLogExecution('ERROR','Commetted Qty',commetedQty);
					nlapiLogExecution('ERROR','Created FO Qty',totalQuantity);
					nlapiLogExecution('ERROR','Shipped Qty',shippedqty);
					var totalcomtdqty=parseFloat(commetedQty)+parseFloat(shippedqty);

					if(totalQuantity == 0)
						variance=commetedQty;
					else
					{
						if(parseFloat(totalQuantity) < parseFloat(totalcomtdqty))
						{
							variance=parseFloat(totalcomtdqty)-parseFloat(totalQuantity);
						}
					}

					nlapiLogExecution('ERROR','Variance',variance);

					if(variance > 0)
					{
						if(oldsoname!=soname)
						{
							//generate new fullfillment order no.
							//fullFillmentorderno=fulfillmentOrderDetails(salesOrderLineDetails[i].getId(),soname);
							fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname,solocation,solineshipmethod);
							oldsoname=soname;
						}
						nlapiLogExecution('ERROR','Remaining Usage 4',context.getRemainingUsage());
						nlapiLogExecution('ERROR','fullFillmentorderno',fullFillmentorderno);

						createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,salesOrderLineDetails[i],soname,salesOrderLineDetails[i].getId());
						nlapiLogExecution('ERROR','Remaining Usage 5',context.getRemainingUsage());							
					}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG','exception in auto fulfillment creation',e.message);
					}
					finally
					{
						nlapiLogExecution('DEBUG','soname before unlocking',soname);
						nlapiLogExecution('DEBUG','trantype before unlocking','transferorder');

						if(soname != null && soname != '')
						{	
							var ExternalIdFilters = new Array();
							ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
							ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'transferorder'));
							var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
							if(searchresultsExt != null && searchresultsExt != '')
							{
								nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
								nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
								nlapiLogExecution('DEBUG','Unlocked successfully');
							}	
						}
					}
				}			
				//Update seq no in wave table if the remaining use is less than 500
				if(context.getRemainingUsage()<500)
				{				
					//nlapiLogExecution('ERROR','vinternalid',vinternalid);	
					//updateSeqNo('TO',vinternalid);
					nlapiLogExecution('ERROR','break');
					break;
				}
			}
			nlapiLogExecution('ERROR','salesOrderLineDetails.length2 ',salesOrderLineDetails.length);

//			if(salesOrderLineDetails.length<1000)
//			{
//			updateSeqNo('TO',0);
//			}
//			else
//			{
//			nlapiLogExecution('ERROR','vinternalid',vinternalid);
//			updateSeqNo('TO',vinternalid);			
//			}

		}
	}

	nlapiLogExecution('ERROR','Remaining Usage at the end ',context.getRemainingUsage());	

	nlapiLogExecution('ERROR','Out of  backOrderFulfillmentforTO1s','');
}

function GetAllTOInternalID_1s()
{
	try
	{
		nlapiLogExecution('DEBUG','Into GetAllTOInternalID_1s');
		var SOInternalIdArray=new Array();

		var resultSet=nlapiSearchRecord('Transaction','customsearch_ebiz_tofocreated1s',null,null);

		if(resultSet!=null&&resultSet!="")
		{
			for(var i=0;i<resultSet.length;i++)
			{
				SOInternalIdArray.push(resultSet[i].getValue('internalid'));
			}
		}
		nlapiLogExecution('DEBUG','Out of GetAllTOInternalID_1s',SOInternalIdArray);
		return SOInternalIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetAllTOInternalID_1s',exp);
	}
}