function AutoBrkDownSheduler(type)
{
	nlapiLogExecution('ERROR','Into  AutobreakdownScheduler','');
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());

	var curuserId = context.getUser();

	updateScheduleScriptStatus('AUTO BREAKDOWN',curuserId,'In Progress',null,null);

	/*if ( type != 'scheduled') return; 
	else*/
	{
		nlapiLogExecution('ERROR','hi');
		var poid = context.getSetting('SCRIPT', 'custscript_ebiz_autobrkpo');
		var createinvtflag = context.getSetting('SCRIPT', 'custscript_ebiz_createinvt');
		var venteredfieldvalues = context.getSetting('SCRIPT', 'custscript_ebiz_enteredvalue');
		//var poid = nlapiGetFieldValue('custscript_ebiz_autobrkpo');
		nlapiLogExecution('ERROR','Remaining Usage 2',context.getRemainingUsage());
		nlapiLogExecution('ERROR','poid',poid);
		nlapiLogExecution('ERROR','createinvtflag',createinvtflag);
		//nlapiLogExecution('ERROR','test',context.getSetting('SCRIPT', 'custscript_ebiz_sample'));
		fnAutoBrkdwn(poid,createinvtflag,venteredfieldvalues);
	}	

	updateScheduleScriptStatus('AUTO BREAKDOWN',curuserId,'Completed',null,null);
}
function fnAutoBrkdwn(poNum,Invtflag,venteredfieldvalues)
{
	// Get PO Details.
	try {
		var labelArray=new Array();
		var templabelArray=new Array();

		var poReceiptNo = "";
		nlapiLogExecution('ERROR','poNum',poNum);
		var tranType = nlapiLookupField('transaction', poNum, 'recordType');
		var poload = nlapiLoadRecord(tranType, poNum);
		var vponum= poload.getFieldValue('id');
		poNum=poload.getFieldValue('tranid');

		var receiptdate=poload.getFieldValue('trandate');

		nlapiLogExecution('ERROR', 'vponum', vponum);
		nlapiLogExecution('ERROR', 'poNum', poNum);
		nlapiLogExecution('ERROR', 'venteredfieldvalues', venteredfieldvalues);
		//nlapiLogExecution('ERROR', 'oType', poload.getFieldValue('recordType'));

		var systemRule= GetSystemRuleForPostItemReceiptby();

		var lineCount = poload.getLineItemCount('item');

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'is',poNum));
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto',parseInt(vponum)));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
		columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');

		var searchQtyResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details',null, filters, columns);
		var endloc = "";
		var beginloc = "";
		var stageLocn = "";
		var DockLoc = "";
		var batchno="";

		// Get inbound stage location
		stageLocn = getStagingLocation();
		nlapiLogExecution('ERROR', 'stageLocn', stageLocn);



		var selecteditems = new Array();

		for (var p = 1; p <= lineCount; p++) 
		{ 
			selecteditems.push(poload.getLineItemValue('item', 'item', p));
		}
		nlapiLogExecution('ERROR', 'Selected Items Count', selecteditems.length);

		var AllItemDims = getItemDimensions(selecteditems);

		var poValue = "";
		nlapiLogExecution('ERROR','vponum',vponum);
		var orderType =  nlapiLookupField('transaction', vponum, 'recordType');
		nlapiLogExecution('ERROR','orderType',orderType);
		nlapiLogExecution('ERROR', 'lineCount new', lineCount);

		var LineLevelEnteredValueSet=venteredfieldvalues.split("$");
		nlapiLogExecution('ERROR', 'LineLevelEnteredValueSet', LineLevelEnteredValueSet.length);



		for(var venteredlinecount=0;venteredlinecount<LineLevelEnteredValueSet.length;venteredlinecount++)
		{

			//labelArray[0][1]=poNum;
			templabelArray.push(vponum);

			nlapiLogExecution('ERROR', 'SetLineLevelEnteredValue[venteredlinecount]', LineLevelEnteredValueSet[venteredlinecount]);
			var LineLevelEnteredValue=LineLevelEnteredValueSet[venteredlinecount].split("&");
			nlapiLogExecution('ERROR', 'LineLevelEnteredValue', LineLevelEnteredValue.length);
			nlapiLogExecution('ERROR', 'LineLevelEnteredValue', LineLevelEnteredValue);
			var lineselectedForAutobreakdwn=LineLevelEnteredValue[0];
			nlapiLogExecution('ERROR', 'lineselectedForAutobreakdwn', lineselectedForAutobreakdwn);
			for (var i = 1; i <= lineCount; i++) 
			{     
				itemlineno = poload.getLineItemValue('item', 'line', i);
				itemname = poload.getLineItemText('item', 'item', i);
				itemid = poload.getLineItemValue('item', 'item', i);
				nlapiLogExecution('ERROR', 'itemid new', itemid);
				itemdesc = poload.getLineItemValue('item', 'description', i);
				itemstatus = poload.getLineItemValue('item', 'custcol_ebiznet_item_status', i);
				itempackcode = poload.getLineItemValue('item', 'custcol_nswmspackcode', i);
				polineqty = poload.getLineItemValue('item', 'quantity', i);
				polocationid=poload.getFieldValue('location');
				if(tranType=='transferorder')
					polocationid=poload.getFieldValue('transferlocation');
				pocompanyid=poload.getFieldValue('custbody_nswms_company');
				nlapiLogExecution('ERROR', 'itemlineno', itemlineno);
				nlapiLogExecution('ERROR', 'polocationid', polocationid);
				nlapiLogExecution('ERROR', 'pocompanyid', pocompanyid);
				var polineqtyMain=polineqty;

				if(lineselectedForAutobreakdwn==itemlineno)
				{
					var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizdefskustatus'];
					nlapiLogExecution('ERROR', 'fields', fields);
					
					if(poload.getLineItemValue('item', 'location', i) != null && poload.getLineItemValue('item', 'location', i) != '')
						polocationid=poload.getLineItemValue('item', 'location', i);
					if(tranType=='transferorder' && poload.getLineItemValue('item', 'transferlocation', i) != null && poload.getLineItemValue('item', 'transferlocation', i) != '')
						polocationid=poload.getLineItemValue('item', 'transferlocation', i);
					nlapiLogExecution('ERROR', 'polocationid', polocationid);
					var columns = nlapiLookupField('item', itemid, fields);
					var lgItemType = columns.recordType;
					var batchflg = columns.custitem_ebizbatchlot;
					var itemfamId= columns.custitem_item_family;
					var itemgrpId= columns.custitem_item_group;
					var itemstat = columns.custitem_ebizdefskustatus;
					nlapiLogExecution('ERROR', 'lgItemType', lgItemType);
					nlapiLogExecution('ERROR', 'batchflg', batchflg);
					if(lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflg == "T")
					{
						var linebatchselected=LineLevelEnteredValue[1].split(",");
						nlapiLogExecution('ERROR', 'linebatchselected', linebatchselected);
						for(var noofbatchselectedCount=0;noofbatchselectedCount<linebatchselected.length;noofbatchselectedCount++)
						{
							nlapiLogExecution('ERROR', 'noofbatchselectedCount',noofbatchselectedCount);

							var tempBatch=linebatchselected[noofbatchselectedCount]
							nlapiLogExecution('ERROR', 'tempBatch',tempBatch);
							if(tempBatch.indexOf("(")==-1)
							{


								var checkInQty = 0;              

								if(searchQtyResults != null && searchQtyResults != "")
								{ 
									nlapiLogExecution('ERROR', 'itemlineno', searchQtyResults.length);
									for(l=0; l < searchQtyResults.length; l++)
									{
										if (itemlineno == searchQtyResults[l].getValue('custrecord_orderlinedetails_orderline_no')) 
										{
											checkInQty = searchQtyResults[l].getValue('custrecord_orderlinedetails_checkin_qty');
											nlapiLogExecution('ERROR', 'Order Line Details Check-In Qty', checkInQty);
											continue;
										}
									}
								} 
								if(checkInQty==null || checkInQty=='' )
									checkInQty = 0;  
								var qty = parseFloat(polineqty)- parseFloat(checkInQty);
								tempBatch=tempBatch+"("+qty+")";
								nlapiLogExecution('ERROR', 'tempBatch',tempBatch);
								nlapiLogExecution('ERROR', 'linebatchselected[noofbatchselectedCount]',linebatchselected[noofbatchselectedCount]);
								linebatchselected[noofbatchselectedCount]=tempBatch;
								nlapiLogExecution('ERROR', 'linebatchselected[noofbatchselectedCount]',linebatchselected[noofbatchselectedCount]);
							}

							var batchselected=linebatchselected[noofbatchselectedCount].split("(");
							var batchno=batchselected[0];
							nlapiLogExecution('ERROR', 'batchno', batchno);

							var batchqty=batchselected[1].split(")")[0];
							nlapiLogExecution('ERROR', 'batchqty', batchqty);
							polineqty=batchqty;
							var checkInQty = 0;
							if(searchQtyResults != null && searchQtyResults != "")
							{ 
								nlapiLogExecution('ERROR', 'itemlineno', searchQtyResults.length);
								for(l=0; l < searchQtyResults.length; l++)
								{
									if (itemlineno == searchQtyResults[l].getValue('custrecord_orderlinedetails_orderline_no')) 
									{
										checkInQty = searchQtyResults[l].getValue('custrecord_orderlinedetails_checkin_qty');
										nlapiLogExecution('ERROR', 'Order Line Details Check-In Qty', checkInQty);
										continue;
									}
								}
							}      

							if(checkInQty==null || checkInQty=='' )
								checkInQty = 0;      

							rcvquantity = parseInt(polineqtyMain)-parseInt(checkInQty);

							nlapiLogExecution('ERROR', 'rcvquantity',rcvquantity);

							// If PO line quantity greater than already checked-in quantity then show the po line details in sub list
							if(parseInt(polineqtyMain)> parseInt(checkInQty))
							{
								nlapiLogExecution('ERROR', 'INTO auto',polineqty);

								var qty = parseInt(polineqty);
								nlapiLogExecution('ERROR','polocationid',polocationid);
								var tasktype = "";
								var wmsStsflag = "";
								var RcvQty = "";
								var uomlevel = "";
								var lotwithqty = "";
								var itemId = itemid;
								var ItemName = itemid;
								var lineno = itemlineno;
								var ItemStatus = itemstatus;
								var PackCode = itempackcode;
								var ItemDesc = itemdesc;
								var LotBatch = '';
								var Lotid = '';
								var binLocn = LineLevelEnteredValue[2];
								nlapiLogExecution('ERROR','binLocn',binLocn);
								var CartLP = LineLevelEnteredValue[3];
								nlapiLogExecution('ERROR','CartLP',CartLP);
								var ordQty;


								if(ItemDesc!=null && ItemDesc!='')
									ItemDesc = ItemDesc.substring(0, 299);								ordQty = qty;

									nlapiLogExecution('ERROR','ordQty',ordQty);
									poValue = poNum; // Get PO Number
									//var poRecId = request.getLineItemValue('custpage_items', 'custpage_receiptid', s); // Get POReceipt Id
									var poRecId = ''; // Get POReceipt Id
									//var poTrailer = request.getLineItemValue('custpage_items', 'custpage_trailer', s); // Get PO Receipt
									var poTrailer = ''; // Get PO Receipt
									//var poRec = request.getParameter('custpage_receiptfield');
									var poRec = '';
									nlapiLogExecution('ERROR', 'itemId', itemId);

									if(CartLP == null || CartLP == "")
									{
										CartLP=hCartLP;					
									}
									nlapiLogExecution('ERROR', 'CartLP', CartLP);

									nlapiLogExecution('ERROR', 'batchflg', batchflg);
									nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
									nlapiLogExecution('ERROR', 'itemId', itemId);
									// Get item dimentions for the item id				
									nlapiLogExecution('ERROR', 'polocationid', polocationid);

									nlapiLogExecution('ERROR', 'venteredfieldvalues', venteredfieldvalues);
									nlapiLogExecution('ERROR', 'itemstat', itemstat);

									if(ItemStatus==null || ItemStatus=='')
										ItemStatus = itemstat;

									nlapiLogExecution('ERROR', 'ItemStatus is null ', ItemStatus);
									nlapiLogExecution('ERROR', 'venteredfieldvalues ', venteredfieldvalues);


									var filters = new Array();          
									if(itemId!=null && itemId!='')
										filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',itemId));
									if(batchno!=null && batchno!='')
										filters.push(new nlobjSearchFilter('name', null, 'is',batchno));

									filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
									var columns = new Array();
									columns[0] = new nlobjSearchColumn('name');
									columns[1] = new nlobjSearchColumn('internalid');
									//columns[1].setSort(true);
									var batchdetails = new nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
									if (batchdetails !=null && batchdetails !='' && batchdetails.length>0) 
									{
										LotBatch=batchdetails[0].getValue('name');
										Lotid=batchdetails[0].getValue('internalid');
										nlapiLogExecution('ERROR', 'LotBatch', LotBatch);
										nlapiLogExecution('ERROR', 'Lotid', Lotid);



									}

									nlapiLogExecution('ERROR', 'LotBatch outside', LotBatch);

									var eachQty = 0;
									var caseQty = 0;
									var palletQty = 0;
									var noofPallets = 0;
									var noofCases = 0;
									var remQty = 0;
									var tOrdQty = ordQty;
									if (AllItemDims != null && AllItemDims.length > 0) 
									{

										for(var p = 0; p < AllItemDims.length; p++)
										{
											var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
											var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');

											if(AllItemDims[p].getValue('custrecord_ebizitemdims') == itemId)
											{
												nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', itemId);

												var skuDim = AllItemDims[p].getValue('custrecord_ebizuomlevelskudim');
												var skuQty = AllItemDims[p].getValue('custrecord_ebizqty');						
												nlapiLogExecution('ERROR','skuDim',skuDim);
												nlapiLogExecution('ERROR','SKUQTY',skuQty);
												if(skuDim == '1')
												{
													eachQty = skuQty;
												}
												else if(skuDim == '2')
												{
													caseQty = skuQty;
												}
												else if(skuDim == '3')
												{
													palletQty = skuQty;
												}

											}									
										}

										if(parseInt(eachQty) == 0)
										{
											nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim EACH is not configured for SKU: '+ itemId);
										}
										if(parseInt(caseQty) == 0)
										{
											nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim CASE is Not Configured for SKU: '+ itemId);
										}
										if(parseInt(palletQty) == 0)
										{
											nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim PALLET is not Configured for SKU: '+ itemId);
										}

										var filters1 = new Array();
										filters1[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemId);            
										filters1[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

										var columns1 = new Array();
										columns1[0] = new nlobjSearchColumn('custrecord_ebizcube');
										columns1[1] = new nlobjSearchColumn('custrecord_ebizqty');
										columns1[2] = new nlobjSearchColumn('custrecord_ebizitemdims');
										var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters1, columns1);

										var filters2 = new Array();

										var columns2 = new Array();
										columns2[0] = new nlobjSearchColumn('custitem_item_family');
										columns2[1] = new nlobjSearchColumn('custitem_item_group');
										columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
										columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
										columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
										columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
										columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

										filters2.push(new nlobjSearchFilter('internalid', null, 'is', itemId));

										var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);
										var filters3=new Array();
										filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
										var columns3=new Array();
										columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
										columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
										columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
										columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
										columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
										columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
										columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
										columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
										columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
										columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
										columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
										columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
										columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
										columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
										columns3[14] = new nlobjSearchColumn('formulanumeric');
										columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
										columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
										columns3[14].setSort();



										var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);


										if(eachQty>0 ||caseQty>0 || palletQty>0)  // break down selected PO line quantity provided Item dims are configured
										{					
											//nlapiLogExecution('ERROR', 'Item', itemId);
											nlapiLogExecution('ERROR', 'Each Qty', eachQty);
											nlapiLogExecution('ERROR', 'Case Qty', caseQty);
											nlapiLogExecution('ERROR', 'Pallet Qty', palletQty);
											nlapiLogExecution('ERROR', 'Ord Qty', ordQty);

											// calculate how many LP's are required to break down selected PO line
											var noofLpsRequired = 0;

											// Get the number of pallets required for this item (there will be one LP per pallet)
											noofPallets = getSKUDimCount(ordQty, palletQty);
											remQty = parseInt(ordQty) - (parseInt(noofPallets) * parseInt(palletQty));	

											nlapiLogExecution('ERROR', 'autobreakdown_palletisation:No. of Pallets', noofPallets);
											nlapiLogExecution('ERROR', 'autobreakdown_palletisation:Remaining Quantity', remQty);

											noofLpsRequired = parseInt(noofPallets);

											// check whether we need to break down into cases or not
											if (parseInt(remQty) > 0 && parseInt(caseQty) != 0) 
											{
												// Get the number of cases required for this item (only one LP for all cases)
												noofCases = getSKUDimCount(remQty, caseQty);
												remQty = parseInt(remQty) - (noofCases * caseQty);
												if(noofCases > 0)
													noofLpsRequired = parseInt(noofLpsRequired) + 1;
												nlapiLogExecution('ERROR', 'autobreakdown_palletisation:No. of Cases', noofCases);
												nlapiLogExecution('ERROR', 'autobreakdown_palletisation:Remaining Quantity', remQty);
											}

											// check whether we need to further break down into eaches or not (there will be one LP for all eaches)
											if (parseInt(remQty) > 0)  
												noofLpsRequired = parseInt(noofLpsRequired) + 1;


											nlapiLogExecution('ERROR', 'No. Of LPs required for line#:' + lineno +'=', noofLpsRequired);

											// In single call we are updating MaxLP in "LP range" custom record with required no of lp's					

											var MultipleLP = GetMultipleLP(1,1,noofLpsRequired,polocationid);


											var maxLPPrefix = MultipleLP.split(",")[0];
											var maxLP = MultipleLP.split(",")[1];;
											maxLP = parseInt(maxLP) - parseInt(noofLpsRequired); 

											nlapiLogExecution('ERROR', 'MAX LP:', maxLP);

											nlapiLogExecution('ERROR', 'noofPallets', noofPallets);
											var totalrcvqty=0;
											var putmethod="";
											for (var p = 0; p < noofPallets; p++) 
											{
												// Calculate LP
												maxLP = parseInt(maxLP) + 1;
												var LP = maxLPPrefix + maxLP.toString();
												nlapiLogExecution('ERROR', 'Pallet LP', LP);

												nlapiLogExecution('ERROR', 'RcvQty', RcvQty);
												nlapiLogExecution('ERROR', 'palletQty', palletQty);

												if(RcvQty=='' || RcvQty==null)
													RcvQty=0;

												//RcvQty = parseInt(RcvQty)+parseInt(palletQty);
												RcvQty = parseInt(palletQty);
												totalrcvqty=parseInt(totalrcvqty)+parseInt(RcvQty);

												nlapiLogExecution('ERROR', 'RcvQty', RcvQty);

												tasktype = "1"; // Task = CHECKIN
												wmsStsflag = "1"; // WMS Status Flag = INBOUND / CHECK-IN
//												beginloc = "";
												beginloc=stageLocn;
												endloc = stageLocn;
												uomlevel = "3"; // UOM Level = Pallet

												lotwithqty = LotBatch + "(" + RcvQty + ")";
												nlapiLogExecution('ERROR', 'lotwithqty', lotwithqty);
												CreateLPMaster(LP,CartLP);

												var recID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
														uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc, 
														LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,'',systemRule);

												tasktype = "2"; // Task = PUTAWAY
												wmsStsflag = "6"; // WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
												beginloc = "";

												if (binLocn != null)
													beginloc=binLocn;

												var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;

												Loctdims = GetPutawayLocationNew(itemId, null, ItemStatus, null, null,null,polocationid,ItemInfoResults,putrulesearchresults);
												nlapiLogExecution('ERROR', 'Loctdims', Loctdims);
												if (Loctdims.length > 0) 
												{
													wmsStsflag = "3";
													if (Loctdims[0] != "") 
													{
														LocName = Loctdims[0];
														nlapiLogExecution('ERROR', 'LocName', LocName);
													}
													else 
													{
														LocName = "";
														nlapiLogExecution('ERROR', 'in else LocName', LocName);
													}
													if (!isNaN(Loctdims[1])) 
													{
														RemCube = Loctdims[1];
													}
													else 
													{
														RemCube = 0;
													}
													if (!isNaN(Loctdims[2])) 
													{
														LocId = Loctdims[2];
														nlapiLogExecution('ERROR', 'LocId', LocId);
													}
													else 
													{
														LocId = "";
														nlapiLogExecution('ERROR', 'in else LocId', LocId);
													}
													if (!isNaN(Loctdims[3])) 
													{
														OubLocId = Loctdims[3];
													}
													else 
													{
														OubLocId = "";
													}
													if (!isNaN(Loctdims[4])) 
													{
														PickSeq = Loctdims[4];
													}
													else 
													{
														PickSeq = "";
													}
													if (!isNaN(Loctdims[5])) 
													{
														InbLocId = Loctdims[5];
													}
													else 
													{
														InbLocId = "";
													}
													if (!isNaN(Loctdims[6])) {
														PutSeq = Loctdims[6];
													}
													else {
														PutSeq = "";
													}
													if (!isNaN(Loctdims[7])) {
														LocRemCube = Loctdims[7];
													}
													else {
														LocRemCube = "";
													}

													putmethod=Loctdims[9];
													nlapiLogExecution('ERROR', 'putmethod', putmethod);
													putstrategy=Loctdims[10];
													nlapiLogExecution('ERROR', 'putstrategy', putstrategy);
												}
												nlapiLogExecution('ERROR', 'binLocn', binLocn);
												if(binLocn==null || binLocn==''||binLocn=="null")
												{
													beginloc=LocId;
													endloc =LocId;
												}
												nlapiLogExecution('ERROR', 'beginlocPUT', binLocn);
												nlapiLogExecution('ERROR', 'endlocPUT', endloc);
												nlapiLogExecution('ERROR','LotBatchPALLET',LotBatch);
												nlapiLogExecution('ERROR','lotwithqtyPallet',lotwithqty);
												nlapiLogExecution('ERROR','batchflg',batchflg);

												var putrecID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
														uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
														LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,putmethod,systemRule);

											}

											nlapiLogExecution('ERROR', 'Total RcvQty', totalrcvqty);

											if(parseInt(totalrcvqty)>0)
											{
												TrnLineUpdation(orderType, "CHKN", poValue, vponum, lineno, itemId,ordQty,totalrcvqty,"Y",ItemStatus);
												TrnLineUpdation(orderType, "PUTW", poValue, vponum, lineno, itemId,ordQty,totalrcvqty,"",ItemStatus,putmethod);
											}

											//for (var p = 0; p < noofCases; p++)
											nlapiLogExecution('ERROR', 'noofCases', noofCases);
											if(parseInt(noofCases) > 0)
											{
												// Calculate LP
												maxLP = parseInt(maxLP) + 1;
												var LP = maxLPPrefix + maxLP.toString();
												nlapiLogExecution('ERROR', 'Case LP', LP);							

												RcvQty = parseInt(noofCases) * parseInt(caseQty);

												tasktype = "1"; // Task = CHECKIN
												wmsStsflag = "1"; // WMS Status Flag = INBOUND / CHECK-IN
//												beginloc = "";
												beginloc=stageLocn;
												endloc = stageLocn;
												uomlevel = "2"; // UOM Level = Case

												lotwithqty = LotBatch + "(" + RcvQty + ")";

												CreateLPMaster(LP,CartLP);

												var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;

												Loctdims = GetPutawayLocationNew(itemId, null, ItemStatus, null, null,null,polocationid,ItemInfoResults,putrulesearchresults);
												nlapiLogExecution('ERROR', 'LoctdimsCASE', Loctdims);
												if (Loctdims.length > 0) 
												{
													if (Loctdims[0] != "") 
													{
														LocName = Loctdims[0];
														nlapiLogExecution('ERROR', 'LocName', LocName);
													}
													else 
													{
														LocName = "";
														nlapiLogExecution('ERROR', 'in else LocName', LocName);
													}
													if (!isNaN(Loctdims[1])) 
													{
														RemCube = Loctdims[1];
													}
													else 
													{
														RemCube = 0;
													}
													if (!isNaN(Loctdims[2])) 
													{
														LocId = Loctdims[2];
														nlapiLogExecution('ERROR', 'LocId', LocId);
													}
													else 
													{
														LocId = "";
														nlapiLogExecution('ERROR', 'in else LocId', LocId);
													}
													if (!isNaN(Loctdims[3])) 
													{
														OubLocId = Loctdims[3];
													}
													else 
													{
														OubLocId = "";
													}
													if (!isNaN(Loctdims[4])) 
													{
														PickSeq = Loctdims[4];
													}
													else 
													{
														PickSeq = "";
													}
													if (!isNaN(Loctdims[5])) 
													{
														InbLocId = Loctdims[5];
													}
													else 
													{
														InbLocId = "";
													}
													if (!isNaN(Loctdims[6])) {
														PutSeq = Loctdims[6];
													}
													else {
														PutSeq = "";
													}
													if (!isNaN(Loctdims[7])) {
														LocRemCube = Loctdims[7];
													}
													else {
														LocRemCube = "";
													}

													putmethod=Loctdims[9];
													nlapiLogExecution('ERROR', 'putmethod', putmethod);
													putstrategy=Loctdims[10];
													nlapiLogExecution('ERROR', 'putstrategy', putstrategy);
												}



												var recID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
														uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc, 
														LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,putmethod,systemRule);

												TrnLineUpdation(orderType, "CHKN", poValue, vponum, lineno, itemId,ordQty,RcvQty,"Y",ItemStatus);

												tasktype = "2"; // Task = PUTAWAY
												if (Loctdims.length > 0) 
													wmsStsflag = "3"; 
												else
													wmsStsflag = "6";// WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
												beginloc = "";
												if (binLocn != null)
													beginloc=binLocn;

												if(binLocn==null || binLocn==''||binLocn=="null")
												{
													beginloc=LocId;
													endloc =LocId;
												}
												var putrecID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
														uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
														LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,putmethod,systemRule);
												
												TrnLineUpdation(orderType, "PUTW", poValue, vponum, lineno, itemId,ordQty,RcvQty,"",ItemStatus,putmethod);
											}

											nlapiLogExecution('ERROR', 'remQty', remQty);
											//alert("Rem Qty After Casses :" +remQty);
											if (parseInt(remQty) > 0) 
											{
												// Calculate LP
												maxLP = parseInt(maxLP) + 1;
												var LP = maxLPPrefix + maxLP.toString();
												nlapiLogExecution('ERROR', 'Each LP', LP);

												tasktype = "1"; // Task = CHECKIN
												wmsStsflag = "1"; // WMS Status Flag = INBOUND / CHECK-IN
												RcvQty = remQty;
//												beginloc = "";
												beginloc=stageLocn;
												endloc = stageLocn;
												uomlevel = "1"; // UOM = EACH
												lotwithqty = LotBatch + "(" + RcvQty + ")";

												CreateLPMaster(LP,CartLP);

												var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;

												Loctdims = GetPutawayLocationNew(itemId, null, ItemStatus, null, null,null,polocationid,ItemInfoResults,putrulesearchresults);
												nlapiLogExecution('ERROR', 'LoctdimsEACH', Loctdims);
												if (Loctdims.length > 0) 
												{
													if (Loctdims[0] != "") 
													{
														LocName = Loctdims[0];
														nlapiLogExecution('ERROR', 'LocName', LocName);
													}
													else 
													{
														LocName = "";
														nlapiLogExecution('ERROR', 'in else LocName', LocName);
													}
													if (!isNaN(Loctdims[1])) 
													{
														RemCube = Loctdims[1];
													}
													else 
													{
														RemCube = 0;
													}
													if (!isNaN(Loctdims[2])) 
													{
														LocId = Loctdims[2];
														nlapiLogExecution('ERROR', 'LocId', LocId);
													}
													else 
													{
														LocId = "";
														nlapiLogExecution('ERROR', 'in else LocId', LocId);
													}
													if (!isNaN(Loctdims[3])) 
													{
														OubLocId = Loctdims[3];
													}
													else 
													{
														OubLocId = "";
													}
													if (!isNaN(Loctdims[4])) 
													{
														PickSeq = Loctdims[4];
													}
													else 
													{
														PickSeq = "";
													}
													if (!isNaN(Loctdims[5])) 
													{
														InbLocId = Loctdims[5];
													}
													else 
													{
														InbLocId = "";
													}
													if (!isNaN(Loctdims[6])) {
														PutSeq = Loctdims[6];
													}
													else {
														PutSeq = "";
													}
													if (!isNaN(Loctdims[7])) {
														LocRemCube = Loctdims[7];
													}
													else {
														LocRemCube = "";
													}

													putmethod=Loctdims[9];
													nlapiLogExecution('ERROR', 'putmethod', putmethod);
													putstrategy=Loctdims[10];
													nlapiLogExecution('ERROR', 'putstrategy', putstrategy);
												}




												var recID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
														uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
														LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,putmethod,systemRule);

												TrnLineUpdation(orderType, "CHKN", poValue, vponum, lineno, itemId,ordQty,RcvQty,"Y",ItemStatus);

												tasktype = "2"; // Task = PUTAWAY
												if (Loctdims.length > 0) 
													wmsStsflag = "3"; // WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
												else
													wmsStsflag = "6";
												nlapiLogExecution('ERROR', 'beginlocf', beginloc);
												beginloc = "";

												if (binLocn != null)
													beginloc=binLocn;
												nlapiLogExecution('ERROR', 'beginlocf1', beginloc);
												if(binLocn==null || binLocn==''||binLocn=="null")
												{
													beginloc=LocId;
													endloc =LocId;
												}
												nlapiLogExecution('ERROR', 'beginlocf2', beginloc);
												nlapiLogExecution('ERROR', 'beginloc/endloc', LocId);
												var putrecID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
														uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
														LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,'',systemRule);
												TrnLineUpdation(orderType, "PUTW", poValue, vponum, lineno, itemId,ordQty,RcvQty,"",ItemStatus,putmethod);
											}
										}// end of auto break down calculation
										else
										{
											var curuserId = context.getUser();
											updateScheduleScriptStatus('AUTO BREAKDOWN',curuserId,'Completed',null,null,'No Dimensions');
										}
									}
							}
							else 
							{ 
								nlapiLogExecution('ERROR', 'UOM Dims', 'For all items, Dimensions are not configured ');
							}			

							if(poRecId!=null&&poRecId!='')
							{
								var poReceiptRec=nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt',poRecId);
								var previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_receiptqty');
								if(previousrcvqty==null||previousrcvqty=='')
									previousrcvqty=0;
								nlapiLogExecution('ERROR','previousrcvqty',previousrcvqty);
								nlapiLogExecution('ERROR','ordQty',ordQty);
								var newrcvQty= parseInt(previousrcvqty)+parseInt(ordQty);
								nlapiLogExecution('ERROR','newrcvQty',newrcvQty);
								var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', poRecId, 'custrecord_ebiz_poreceipt_receiptqty',newrcvQty.toString());
								nlapiLogExecution('ERROR','id',id);
							}
							var expdate="";
							//generatePalletLabel(vponum,polocationid,poTrailer);
							nlapiLogExecution('ERROR','intoQAlabel','intoQAlabel');

							//Start of Getting QC label data from Opentask

							//Getopentaskdetails(vponum,LotBatch,itemId);
							//generateQALabel(vponum,itemname,ItemDesc,LP,LotBatch,polineqty,receiptdate,poNum);
						}
					}
					else
					{
						var checkInQty = 0;
						if(searchQtyResults != null && searchQtyResults != "")
						{ 
							nlapiLogExecution('ERROR', 'itemlineno', searchQtyResults.length);
							for(l=0; l < searchQtyResults.length; l++)
							{
								if (itemlineno == searchQtyResults[l].getValue('custrecord_orderlinedetails_orderline_no')) 
								{
									checkInQty = searchQtyResults[l].getValue('custrecord_orderlinedetails_checkin_qty');
									nlapiLogExecution('ERROR', 'Order Line Details Check-In Qty', checkInQty);
									continue;
								}
							}
						}      

						if(checkInQty==null || checkInQty=='' )
							checkInQty = 0;      

						rcvquantity = parseInt(checkInQty);

						nlapiLogExecution('ERROR', 'rcvquantity',rcvquantity);

						// If PO line quantity greater than already checked-in quantity then show the po line details in sub list
						if(parseInt(polineqty)> parseInt(checkInQty))
						{    
							var qty = parseInt(polineqty)- parseInt(checkInQty);
							nlapiLogExecution('ERROR','polocationid',polocationid);
							nlapiLogExecution('ERROR','ItemDesc',ItemDesc);
							var tasktype = "";
							var wmsStsflag = "";
							var RcvQty = "";
							var uomlevel = "";
							var lotwithqty = "";
							var itemId = itemid;
							var ItemName = itemid;
							var lineno = itemlineno;
							var ItemStatus = itemstatus;
							var PackCode = itempackcode;
							var ItemDesc = itemdesc;
							var LotBatch = '';
							var Lotid = '';
							var binLocn = '';
							var CartLP = '';
							var ordQty;
							if(ItemDesc!=null && ItemDesc!='')
							{
								ItemDesc = ItemDesc.substring(0, 299);
							}


							ordQty = qty;

							nlapiLogExecution('ERROR','ordQty',ordQty);
							poValue = poNum; // Get PO Number
							//var poRecId = request.getLineItemValue('custpage_items', 'custpage_receiptid', s); // Get POReceipt Id
							var poRecId = ''; // Get POReceipt Id
							//var poTrailer = request.getLineItemValue('custpage_items', 'custpage_trailer', s); // Get PO Receipt
							var poTrailer = ''; // Get PO Receipt
							//var poRec = request.getParameter('custpage_receiptfield');
							var poRec = '';
							nlapiLogExecution('ERROR', 'itemId', itemId);

							/*if(CartLP == null || CartLP == "")
			{
				CartLP=hCartLP;					
			}*/
							nlapiLogExecution('ERROR', 'CartLP', CartLP);


							var batchflg = columns.custitem_ebizbatchlot;
							var itemfamId= columns.custitem_item_family;
							var itemgrpId= columns.custitem_item_group;
							var itemstat = columns.custitem_ebizdefskustatus;
							nlapiLogExecution('ERROR', 'batchflg', batchflg);
							nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
							nlapiLogExecution('ERROR', 'itemId', itemId);
							// Get item dimentions for the item id				
							nlapiLogExecution('ERROR', 'polocationid', polocationid);

							nlapiLogExecution('ERROR', 'venteredfieldvalues', venteredfieldvalues);
							nlapiLogExecution('ERROR', 'itemstat', itemstat);

							if(ItemStatus==null || ItemStatus=='')
								ItemStatus = itemstat;

							nlapiLogExecution('ERROR', 'ItemStatus is null ', ItemStatus);
							nlapiLogExecution('ERROR', 'venteredfieldvalues ', venteredfieldvalues);


							var filters = new Array();          
							/*if(itemId!=null && itemId!='')
				filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',itemId));*/
							if(batchno!=null && batchno!='')
								filters.push(new nlobjSearchFilter('name', null, 'is',batchno));

							filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('name');
							columns[1] = new nlobjSearchColumn('internalid');
							//columns[1].setSort(true);
							var batchdetails = new nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
							if (batchdetails !=null && batchdetails !='' && batchdetails.length>0) 
							{
								LotBatch=batchdetails[0].getValue('name');
								Lotid=batchdetails[0].getValue('internalid');
								nlapiLogExecution('ERROR', 'LotBatch', LotBatch);
								nlapiLogExecution('ERROR', 'Lotid', Lotid);



							}

							nlapiLogExecution('ERROR', 'LotBatch outside', LotBatch);

							var eachQty = 0;
							var caseQty = 0;
							var palletQty = 0;
							var noofPallets = 0;
							var noofCases = 0;
							var remQty = 0;
							var tOrdQty = ordQty;
							if (AllItemDims != null && AllItemDims.length > 0) 
							{

								for(var p = 0; p < AllItemDims.length; p++)
								{
									var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
									var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');

									if(AllItemDims[p].getValue('custrecord_ebizitemdims') == itemId)
									{
										nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', itemId);

										var skuDim = AllItemDims[p].getValue('custrecord_ebizuomlevelskudim');
										var skuQty = AllItemDims[p].getValue('custrecord_ebizqty');						
										nlapiLogExecution('ERROR','skuDim',skuDim);
										nlapiLogExecution('ERROR','SKUQTY',skuQty);
										if(skuDim == '1')
										{
											eachQty = skuQty;
										}
										else if(skuDim == '2')
										{
											caseQty = skuQty;
										}
										else if(skuDim == '3')
										{
											palletQty = skuQty;
										}

									}									
								}

								if(parseInt(eachQty) == 0)
								{
									nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim EACH is not configured for SKU: '+ itemId);
								}
								if(parseInt(caseQty) == 0)
								{
									nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim CASE is Not Configured for SKU: '+ itemId);
								}
								if(parseInt(palletQty) == 0)
								{
									nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim PALLET is not Configured for SKU: '+ itemId);
								}

								var filters1 = new Array();
								filters1[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemId);            
								filters1[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

								var columns1 = new Array();
								columns1[0] = new nlobjSearchColumn('custrecord_ebizcube');
								columns1[1] = new nlobjSearchColumn('custrecord_ebizqty');
								columns1[2] = new nlobjSearchColumn('custrecord_ebizitemdims');
								var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters1, columns1);

								var filters2 = new Array();

								var columns2 = new Array();
								columns2[0] = new nlobjSearchColumn('custitem_item_family');
								columns2[1] = new nlobjSearchColumn('custitem_item_group');
								columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
								columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
								columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
								columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
								columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

								filters2.push(new nlobjSearchFilter('internalid', null, 'is', itemId));

								var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);
								var filters3=new Array();
								filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
								var columns3=new Array();
								columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
								columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
								columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
								columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
								columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
								columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
								columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
								columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
								columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
								columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
								columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
								columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
								columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
								columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
								columns3[14] = new nlobjSearchColumn('formulanumeric');
								columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
								columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
								columns3[14].setSort();



								var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);


								if(eachQty>0 ||caseQty>0 || palletQty>0)  // break down selected PO line quantity provided Item dims are configured
								{					
									//nlapiLogExecution('ERROR', 'Item', itemId);
									nlapiLogExecution('ERROR', 'Each Qty', eachQty);
									nlapiLogExecution('ERROR', 'Case Qty', caseQty);
									nlapiLogExecution('ERROR', 'Pallet Qty', palletQty);
									nlapiLogExecution('ERROR', 'Ord Qty', ordQty);

									// calculate how many LP's are required to break down selected PO line
									var noofLpsRequired = 0;

									// Get the number of pallets required for this item (there will be one LP per pallet)
									noofPallets = getSKUDimCount(ordQty, palletQty);
									remQty = parseInt(ordQty) - (parseInt(noofPallets) * parseInt(palletQty));	

									nlapiLogExecution('ERROR', 'autobreakdown_palletisation:No. of Pallets', noofPallets);
									nlapiLogExecution('ERROR', 'autobreakdown_palletisation:Remaining Quantity', remQty);

									noofLpsRequired = parseInt(noofPallets);

									// check whether we need to break down into cases or not
									if (parseInt(remQty) > 0 && parseInt(caseQty) != 0) 
									{
										// Get the number of cases required for this item (only one LP for all cases)
										noofCases = getSKUDimCount(remQty, caseQty);
										remQty = parseInt(remQty) - (noofCases * caseQty);
										if(noofCases > 0)
											noofLpsRequired = parseInt(noofLpsRequired) + 1;
										nlapiLogExecution('ERROR', 'autobreakdown_palletisation:No. of Cases', noofCases);
										nlapiLogExecution('ERROR', 'autobreakdown_palletisation:Remaining Quantity', remQty);
									}

									// check whether we need to further break down into eaches or not (there will be one LP for all eaches)
									if (parseInt(remQty) > 0)  
										noofLpsRequired = parseInt(noofLpsRequired) + 1;


									nlapiLogExecution('ERROR', 'No. Of LPs required for line#:' + lineno +'=', noofLpsRequired);

									// In single call we are updating MaxLP in "LP range" custom record with required no of lp's					

									var MultipleLP = GetMultipleLP(1,1,noofLpsRequired,polocationid);


									var maxLPPrefix = MultipleLP.split(",")[0];
									var maxLP = MultipleLP.split(",")[1];;
									maxLP = parseInt(maxLP) - parseInt(noofLpsRequired); 

									nlapiLogExecution('ERROR', 'MAX LP:', maxLP);

									nlapiLogExecution('ERROR', 'noofPallets', noofPallets);
									var totalrcvqty=0;
									for (var p = 0; p < noofPallets; p++) 
									{
										// Calculate LP
										maxLP = parseInt(maxLP) + 1;
										var LP = maxLPPrefix + maxLP.toString();
										nlapiLogExecution('ERROR', 'Pallet LP', LP);

										nlapiLogExecution('ERROR', 'RcvQty', RcvQty);
										nlapiLogExecution('ERROR', 'palletQty', palletQty);

										if(RcvQty=='' || RcvQty==null)
											RcvQty=0;

										//RcvQty = parseInt(RcvQty)+parseInt(palletQty);
										RcvQty = parseInt(palletQty);
										totalrcvqty=parseInt(totalrcvqty)+parseInt(RcvQty);

										nlapiLogExecution('ERROR', 'RcvQty', RcvQty);

										tasktype = "1"; // Task = CHECKIN
										wmsStsflag = "1"; // WMS Status Flag = INBOUND / CHECK-IN
//										beginloc = "";
										beginloc=stageLocn;
										endloc = stageLocn;
										uomlevel = "3"; // UOM Level = Pallet

										lotwithqty = LotBatch + "(" + RcvQty + ")";

										CreateLPMaster(LP,CartLP);

										var recID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
												uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc, 
												LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,'',systemRule);

										tasktype = "2"; // Task = PUTAWAY
										wmsStsflag = "6"; // WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
										beginloc = "";
										if (binLocn != null)
											beginloc=binLocn;

										var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;

										Loctdims = GetPutawayLocationNew(itemId, null, ItemStatus, null, null,null,polocationid,ItemInfoResults,putrulesearchresults);

										if (Loctdims.length > 0) 
										{
											wmsStsflag = "3";
											if (Loctdims[0] != "") 
											{
												LocName = Loctdims[0];
												nlapiLogExecution('ERROR', 'LocName', LocName);
											}
											else 
											{
												LocName = "";
												nlapiLogExecution('ERROR', 'in else LocName', LocName);
											}
											if (!isNaN(Loctdims[1])) 
											{
												RemCube = Loctdims[1];
											}
											else 
											{
												RemCube = 0;
											}
											if (!isNaN(Loctdims[2])) 
											{
												LocId = Loctdims[2];
												nlapiLogExecution('ERROR', 'LocId', LocId);
											}
											else 
											{
												LocId = "";
												nlapiLogExecution('ERROR', 'in else LocId', LocId);
											}
											if (!isNaN(Loctdims[3])) 
											{
												OubLocId = Loctdims[3];
											}
											else 
											{
												OubLocId = "";
											}
											if (!isNaN(Loctdims[4])) 
											{
												PickSeq = Loctdims[4];
											}
											else 
											{
												PickSeq = "";
											}
											if (!isNaN(Loctdims[5])) 
											{
												InbLocId = Loctdims[5];
											}
											else 
											{
												InbLocId = "";
											}
											if (!isNaN(Loctdims[6])) {
												PutSeq = Loctdims[6];
											}
											else {
												PutSeq = "";
											}
											if (!isNaN(Loctdims[7])) {
												LocRemCube = Loctdims[7];
											}
											else {
												LocRemCube = "";
											}

											putmethod=Loctdims[9];
											putstrategy=Loctdims[10];
										}

										if(binLocn==null || binLocn==''|| binLocn=='null')
										{
											beginloc=LocId;
											endloc =LocId;
										}


										var putrecID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
												uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
												LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,putmethod,systemRule);

									}

									nlapiLogExecution('ERROR', 'Total RcvQty', totalrcvqty);

									if(parseInt(totalrcvqty)>0)
									{
										TrnLineUpdation(orderType, "CHKN", poValue, vponum, lineno, itemId,ordQty,totalrcvqty,"Y",ItemStatus);
										TrnLineUpdation(orderType, "PUTW", poValue, vponum, lineno, itemId,ordQty,totalrcvqty,"",ItemStatus,putmethod);
									}

									//for (var p = 0; p < noofCases; p++)
									nlapiLogExecution('ERROR', 'noofCases', noofCases);
									if(parseInt(noofCases) > 0)
									{
										// Calculate LP
										maxLP = parseInt(maxLP) + 1;
										var LP = maxLPPrefix + maxLP.toString();
										nlapiLogExecution('ERROR', 'Case LP', LP);							

										RcvQty = parseInt(noofCases) * parseInt(caseQty);

										tasktype = "1"; // Task = CHECKIN
										wmsStsflag = "1"; // WMS Status Flag = INBOUND / CHECK-IN
//										beginloc = "";
										beginloc=stageLocn;
										endloc = stageLocn;
										uomlevel = "2"; // UOM Level = Case

										lotwithqty = LotBatch + "(" + RcvQty + ")";

										CreateLPMaster(LP,CartLP);

										var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;

										Loctdims = GetPutawayLocationNew(itemId, null, ItemStatus, null, null,null,polocationid,ItemInfoResults,putrulesearchresults);

										if (Loctdims.length > 0) 
										{
											if (Loctdims[0] != "") 
											{
												LocName = Loctdims[0];
												nlapiLogExecution('ERROR', 'LocName', LocName);
											}
											else 
											{
												LocName = "";
												nlapiLogExecution('ERROR', 'in else LocName', LocName);
											}
											if (!isNaN(Loctdims[1])) 
											{
												RemCube = Loctdims[1];
											}
											else 
											{
												RemCube = 0;
											}
											if (!isNaN(Loctdims[2])) 
											{
												LocId = Loctdims[2];
												nlapiLogExecution('ERROR', 'LocId', LocId);
											}
											else 
											{
												LocId = "";
												nlapiLogExecution('ERROR', 'in else LocId', LocId);
											}
											if (!isNaN(Loctdims[3])) 
											{
												OubLocId = Loctdims[3];
											}
											else 
											{
												OubLocId = "";
											}
											if (!isNaN(Loctdims[4])) 
											{
												PickSeq = Loctdims[4];
											}
											else 
											{
												PickSeq = "";
											}
											if (!isNaN(Loctdims[5])) 
											{
												InbLocId = Loctdims[5];
											}
											else 
											{
												InbLocId = "";
											}
											if (!isNaN(Loctdims[6])) {
												PutSeq = Loctdims[6];
											}
											else {
												PutSeq = "";
											}
											if (!isNaN(Loctdims[7])) {
												LocRemCube = Loctdims[7];
											}
											else {
												LocRemCube = "";
											}

											putmethod=Loctdims[9];
											putstrategy=Loctdims[10];
										}



										var recID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
												uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc, 
												LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,putmethod,systemRule);

										TrnLineUpdation(orderType, "CHKN", poValue, vponum, lineno, itemId,ordQty,RcvQty,"Y",ItemStatus);

										tasktype = "2"; // Task = PUTAWAY
										if (Loctdims.length > 0) 
											wmsStsflag = "3"; 
										else
											wmsStsflag = "6"; // WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
										beginloc = "";
										if (binLocn != null)
											beginloc=binLocn;

										if(binLocn==null || binLocn==''|| binLocn=='null')
										{
											beginloc=LocId;
											endloc =LocId;
										}
										var putrecID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
												uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
												LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,putmethod,systemRule);
										TrnLineUpdation(orderType, "PUTW", poValue, vponum, lineno, itemId,ordQty,RcvQty,"",ItemStatus,putmethod);
									}

									nlapiLogExecution('ERROR', 'remQty', remQty);
									//alert("Rem Qty After Casses :" +remQty);
									if (parseInt(remQty) > 0) 
									{
										// Calculate LP
										maxLP = parseInt(maxLP) + 1;
										var LP = maxLPPrefix + maxLP.toString();
										nlapiLogExecution('ERROR', 'Each LP', LP);

										tasktype = "1"; // Task = CHECKIN
										wmsStsflag = "1"; // WMS Status Flag = INBOUND / CHECK-IN
										RcvQty = remQty;
//										beginloc = "";
										beginloc=stageLocn;
										endloc = stageLocn;
										uomlevel = "1"; // UOM = EACH
										lotwithqty = LotBatch + "(" + RcvQty + ")";

										CreateLPMaster(LP,CartLP);

										var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;

										Loctdims = GetPutawayLocationNew(itemId, null, ItemStatus, null, null,null,polocationid,ItemInfoResults,putrulesearchresults);

										if (Loctdims.length > 0) 
										{
											if (Loctdims[0] != "") 
											{
												LocName = Loctdims[0];
												nlapiLogExecution('ERROR', 'LocName', LocName);
											}
											else 
											{
												LocName = "";
												nlapiLogExecution('ERROR', 'in else LocName', LocName);
											}
											if (!isNaN(Loctdims[1])) 
											{
												RemCube = Loctdims[1];
											}
											else 
											{
												RemCube = 0;
											}
											if (!isNaN(Loctdims[2])) 
											{
												LocId = Loctdims[2];
												nlapiLogExecution('ERROR', 'LocId', LocId);
											}
											else 
											{
												LocId = "";
												nlapiLogExecution('ERROR', 'in else LocId', LocId);
											}
											if (!isNaN(Loctdims[3])) 
											{
												OubLocId = Loctdims[3];
											}
											else 
											{
												OubLocId = "";
											}
											if (!isNaN(Loctdims[4])) 
											{
												PickSeq = Loctdims[4];
											}
											else 
											{
												PickSeq = "";
											}
											if (!isNaN(Loctdims[5])) 
											{
												InbLocId = Loctdims[5];
											}
											else 
											{
												InbLocId = "";
											}
											if (!isNaN(Loctdims[6])) {
												PutSeq = Loctdims[6];
											}
											else {
												PutSeq = "";
											}
											if (!isNaN(Loctdims[7])) {
												LocRemCube = Loctdims[7];
											}
											else {
												LocRemCube = "";
											}

											putmethod=Loctdims[9];
											putstrategy=Loctdims[10];
										}



										var recID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
												uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
												LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,putmethod,systemRule);

										TrnLineUpdation(orderType, "CHKN", poValue, vponum, lineno, itemId,ordQty,RcvQty,"Y",ItemStatus);

										tasktype = "2"; // Task = PUTAWAY
										if (Loctdims.length > 0)
											wmsStsflag = "3"; // WMS Status Flag = INBOUND / NO LOCATIONS ASSIGNED
										else
											wmsStsflag = "6"; 
										beginloc = "";
										if (binLocn != null)
											beginloc=binLocn;
										if(binLocn==null || binLocn==''|| binLocn=='null')
										{
											beginloc=LocId;
											endloc =LocId;
										}
										var putrecID = CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus,
												uomlevel, tasktype, beginloc, endloc, vponum, poReceiptNo, wmsStsflag, ItemName, ItemDesc,
												LotBatch, CartLP, polocationid, lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,tranType,Lotid,Invtflag,'',systemRule);
										TrnLineUpdation(orderType, "PUTW", poValue, vponum, lineno, itemId,ordQty,RcvQty,"",ItemStatus,putmethod);
									}
								}// end of auto break down calculation

							}
						}
						else 
						{ 
							nlapiLogExecution('ERROR', 'UOM Dims', 'For all items, Dimensions are not configured ');
						}			

						if(poRecId!=null&&poRecId!='')
						{
							var poReceiptRec=nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt',poRecId);
							var previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_receiptqty');
							if(previousrcvqty==null||previousrcvqty=='')
								previousrcvqty=0;
							nlapiLogExecution('ERROR','previousrcvqty',previousrcvqty);
							nlapiLogExecution('ERROR','ordQty',ordQty);
							var newrcvQty= parseInt(previousrcvqty)+parseInt(ordQty);
							nlapiLogExecution('ERROR','newrcvQty',newrcvQty);
							var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', poRecId, 'custrecord_ebiz_poreceipt_receiptqty',newrcvQty.toString());
							nlapiLogExecution('ERROR','id',id);
						}
						var filters=new Array();
						filters.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemId));
						var columns=new Array();
						columns[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
						var list=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filters,columns);
						var expdate="";
						if(list!=null && list!=''){
							for(var count=0;count<list.length;count++)
							{
								var lineNo=count+1;
								var listresult=list[count];
								expdate=listresult.getValue('custrecord_ebizexpirydate');

							}
						}
						//generatePalletLabel(vponum,polocationid,poTrailer);


					}
					//labelArray[0][1]=LotBatch;
					//labelArray[0][2]=itemid;

					templabelArray.push(LotBatch);
					templabelArray.push(itemid);					
					break;
				}
			}
			labelArray.push(templabelArray);
		}

		Getopentaskdetails(vponum);
		/*	//Code changed by suman as on 28th Jan
		if(labelArray!=null&&labelArray!="")
		{
			nlapiLogExecution('ERROR', 'labelArray.length', labelArray.length);
			for(var i=0;i<labelArray.length;i++)
			{
				var vponum=labelArray[i][0];
				var LotBatch=labelArray[i][1];
				var itemid=labelArray[i][2];

			}
		}*/
		//End of code as on 28th JAn
	}
	catch(exp) 
	{
		//alert('Expception'+exp);
		nlapiLogExecution('ERROR', 'Exception in autobreakdown_palletisation:', exp);		

	}
}


function Getopentaskdetails(poId,LotBatch,itemId)
{

	nlapiLogExecution('ERROR','Into GetOpentaskdetails','Into GetOpentaskdetails');
	nlapiLogExecution('ERROR','poId',poId);
	nlapiLogExecution('ERROR','LotBatch',LotBatch);
	nlapiLogExecution('ERROR','itemId',itemId);
	//get qty from opentask against to containerlp

	var qtyfilters = new Array();


	qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [poId]));	
	qtyfilters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]));  
	qtyfilters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
	qtyfilters.push( new nlobjSearchFilter('custrecord_batch_no', null, 'is', LotBatch));
	qtyfilters.push( new nlobjSearchFilter('custrecord_sku', null, 'anyof', [itemId]));


	var qtycolumns = new Array();
	qtycolumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	qtycolumns[1] = new nlobjSearchColumn('custrecord_expirydate');
	qtycolumns[2] = new nlobjSearchColumn('custrecordact_begin_date');
	qtycolumns[3] = new nlobjSearchColumn('custrecord_act_qty');
	qtycolumns[4] = new nlobjSearchColumn('custrecord_lpno');
	qtycolumns[5] = new nlobjSearchColumn('custrecord_sku');
	qtycolumns[6] = new nlobjSearchColumn('custrecord_skudesc');
	qtycolumns[7] = new nlobjSearchColumn('custrecord_batch_no');




	var qtysearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, qtyfilters, qtycolumns);

	if(qtysearchresults !=null)
	{

		for(var g=0;g<qtysearchresults.length;g++)
		{		
			var orderno = qtysearchresults[g].getText('custrecord_ebiz_order_no');
			var itemText = qtysearchresults[g].getText('custrecord_sku');
			var itemdesc = qtysearchresults[g].getValue('custrecord_skudesc');
			var expdate = qtysearchresults[g].getValue('custrecord_expirydate');
			var actbegindate = qtysearchresults[g].getValue('custrecordact_begin_date');
			var quantity = qtysearchresults[g].getValue('custrecord_act_qty');
			var lpnumber = qtysearchresults[g].getValue('custrecord_lpno');
			var lotnumber = qtysearchresults[g].getValue('custrecord_batch_no');		

			generateQALabel(itemText,itemdesc,lpnumber,lotnumber,quantity,actbegindate,expdate,orderno)
		}


	}


}


function CreateOpenTaskRecord(poValue, itemId, ordQty, RcvQty, LP, lineno, PackCode, ItemStatus, BaseUOM, 
		tasktype, beginloc, endloc, poId, poreceiptno, wmsStsflag, ItemName, ItemDesc, LotBatch, CartLP, poloc,
		lgItemType, lotwithqty, uomlevel,poRecId,poRec,poTrailer,batchflg,trantype,Lotid,Invtflag,putmethod,systemRule)
{
	nlapiLogExecution('ERROR','create opentask records');
	nlapiLogExecution('ERROR','NAME',poValue);
	nlapiLogExecution('ERROR','poRec',poRec);
	nlapiLogExecution('ERROR','poTrailer',poTrailer);
	nlapiLogExecution('ERROR','Lotid',Lotid);
	nlapiLogExecution('ERROR','Invtflag',Invtflag);
	nlapiLogExecution('ERROR','LotBatch',LotBatch);
	nlapiLogExecution('ERROR','lotwithqty',lotwithqty);

	if(Invtflag=='T')
	{
		try{
			var receiptno='';
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
			customrecord.setFieldValue('name', poValue);
			customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);		
			customrecord.setFieldValue('custrecord_ebiz_sku_no', itemId);	
			customrecord.setFieldValue('custrecord_sku', itemId);	
			customrecord.setFieldValue('custrecord_act_qty', RcvQty);		
			customrecord.setFieldValue('custrecord_expe_qty', RcvQty);
			customrecord.setFieldValue('custrecord_lpno', LP);
			customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
			customrecord.setFieldValue('custrecord_line_no', lineno);
			customrecord.setFieldValue('custrecord_packcode', PackCode);
			customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
			customrecord.setFieldValue('custrecord_ebiz_ot_receipt_no', poRecId);
			customrecord.setFieldValue('custrecord_ot_receipt_no', poRec);
			customrecord.setFieldValue('custrecord_ebiz_trailer_no', poTrailer);

			var currentUserID = getCurrentUser();
			nlapiLogExecution('ERROR','currentUserID',currentUserID);
			customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
			customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
			customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
			var uomid="";
			if(BaseUOM == 1)
				uomid = "EACH";
			else if(BaseUOM == 2)
				uomid = "CASE";
			else
				uomid = "PALLET";

			customrecord.setFieldValue('custrecord_uom_id', uomid);
			customrecord.setFieldValue('custrecord_tasktype', tasktype);
			if(tasktype == "2")
			{
				if(beginloc!=null && beginloc!='')
				customrecord.setFieldValue('custrecord_actbeginloc', beginloc);
				if(endloc!=null && endloc!='')
				customrecord.setFieldValue('custrecord_actendloc', endloc);
				customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
				customrecord.setFieldValue('custrecord_act_end_date', DateStamp());
			}

			if (tasktype == "1") // Task = CHECKIN
			{ 
				customrecord.setFieldValue('custrecord_actbeginloc', beginloc);
				customrecord.setFieldValue('custrecord_actendloc', endloc);
				customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
				customrecord.setFieldValue('custrecord_act_end_date', DateStamp());

				//Adding fields to update time zones.
				customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
				customrecord.setFieldValue('custrecord_actualendtime', TimeStamp());
				customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
				customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());			
			}

			customrecord.setFieldValue('custrecord_current_date', DateStamp());
			customrecord.setFieldValue('custrecord_upd_date', DateStamp());		
			customrecord.setFieldValue('custrecord_ebiz_cntrl_no', poId);
			customrecord.setFieldValue('custrecord_ebiz_order_no', parseInt(poId));
			customrecord.setFieldValue('custrecord_ebiz_receipt_no', poRecId);

			//Status flag .
			customrecord.setFieldValue('custrecord_wms_status_flag',wmsStsflag );

			//Added for Item name and desc
			customrecord.setFieldValue('custrecord_skudesc', ItemDesc);
			customrecord.setFieldValue('custrecord_transport_lp', CartLP);

			//LotBatch and batch with Quantity.
			if(lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflg == "T"){
				customrecord.setFieldValue('custrecord_batch_no', LotBatch);
				customrecord.setFieldValue('custrecord_lotnowithquantity', lotwithqty);
			}

			var expdate;
			if(LotBatch!="" && LotBatch!=null)
			{

				var filterspor = new Array();
				filterspor.push(new nlobjSearchFilter('name', null, 'is', LotBatch));
				/*if(whLocation!=null && whLocation!="")
				filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);*/
				if(itemId!=null && itemId!="")
					filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemId));

				var column=new Array();
				column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
				column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

				var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
				if(receiptsearchresults!=null)
				{

					expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
					//vfifoDate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				}
				//nlapiLogExecution('DEBUG', 'expdate', expdate);
				//nlapiLogExecution('DEBUG', 'vfifoDate', vfifoDate);
			}
			if(expdate!=null && expdate!='')
				customrecord.setFieldValue('custrecord_expirydate', expdate);
			customrecord.setFieldValue('custrecord_wms_location', poloc);
			customrecord.setFieldValue('custrecord_fifodate', DateStamp());

			//Assigning Sku dims level. 
			customrecord.setFieldValue('custrecord_ebizuomlevelskudim', uomlevel);
			customrecord.setFieldValue('custrecord_ebizmethod_no',putmethod);
			//commit the record to NetSuite
			var recid = nlapiSubmitRecord(customrecord,false, true);
			nlapiLogExecution('ERROR','Create Open task record ID',recid);
			//alert('TRN_OPENTASK Success');
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Create Open task record Fail',exp);
		}
		if (tasktype == "2" && wmsStsflag=="3") // Task = CHECKIN
		{
			nlapiLogExecution('ERROR','Invtflag',Invtflag);
			var idl='';
			if(Invtflag=='T'){
				nlapiLogExecution('ERROR','systemRule',systemRule);
				if(systemRule!='PO')
					idl=generateItemReceipt(trantype,poId,RcvQty,lineno,ItemStatus,itemId,trantype,poId,LP,"","","",LotBatch,"",poloc,"",Lotid);

				nlapiLogExecution('ERROR', 'Create Inventory Record Insertion', 'start');
				var varMergeLP = 'F';
				nlapiLogExecution('DEBUG', 'putmethod', putmethod);
				nlapiLogExecution('DEBUG', 'poloc', poloc);
				//if(methodid!=null && methodid!='')
				varMergeLP = isMergeLP(poloc,putmethod);
				var invtRecordId = createInvtRecord(tasktype, poId, endloc, LP, itemId,
						ItemStatus, ItemDesc, RcvQty, poloc, lgItemType, PackCode,batchflg,LotBatch,batchflg,Lotid,varMergeLP);

				updatePOline(poId,trantype);
				nlapiLogExecution('ERROR', 'Created Inventory Record ID', invtRecordId);
			}
		}
	}
	else
	{


		try{
			var receiptno='';
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
			customrecord.setFieldValue('name', poValue);
			customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);		
			customrecord.setFieldValue('custrecord_ebiz_sku_no', itemId);	
			customrecord.setFieldValue('custrecord_sku', itemId);	

			customrecord.setFieldValue('custrecord_expe_qty', RcvQty);
			customrecord.setFieldValue('custrecord_lpno', LP);
			customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
			customrecord.setFieldValue('custrecord_line_no', lineno);
			customrecord.setFieldValue('custrecord_packcode', PackCode);
			customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
			customrecord.setFieldValue('custrecord_ebiz_ot_receipt_no', poRecId);
			customrecord.setFieldValue('custrecord_ot_receipt_no', poRec);
			customrecord.setFieldValue('custrecord_ebiz_trailer_no', poTrailer);

			var currentUserID = getCurrentUser();
			nlapiLogExecution('ERROR','currentUserID',currentUserID);
			customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
			customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
			customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
			var uomid="";
			if(BaseUOM == 1)
				uomid = "EACH";
			else if(BaseUOM == 2)
				uomid = "CASE";
			else
				uomid = "PALLET";

			customrecord.setFieldValue('custrecord_uom_id', uomid);
			customrecord.setFieldValue('custrecord_tasktype', tasktype);
			if(tasktype == "2")
			{
				if(beginloc!=null && beginloc!='')
				customrecord.setFieldValue('custrecord_actbeginloc', beginloc);
				//customrecord.setFieldValue('custrecord_actendloc', endloc);
				customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
				//customrecord.setFieldValue('custrecord_act_end_date', DateStamp());
				customrecord.setFieldValue('custrecord_wms_status_flag','2' );
			}

			if (tasktype == "1") // Task = CHECKIN
			{ 
				customrecord.setFieldValue('custrecord_actbeginloc', beginloc);
				customrecord.setFieldValue('custrecord_actendloc', endloc);
				customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
				customrecord.setFieldValue('custrecord_act_end_date', DateStamp());
				customrecord.setFieldValue('custrecord_act_qty', RcvQty);

				//Adding fields to update time zones.
				customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
				customrecord.setFieldValue('custrecord_actualendtime', TimeStamp());
				customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
				customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());	
				customrecord.setFieldValue('custrecord_wms_status_flag',wmsStsflag );
			}

			customrecord.setFieldValue('custrecord_current_date', DateStamp());
			customrecord.setFieldValue('custrecord_upd_date', DateStamp());		
			customrecord.setFieldValue('custrecord_ebiz_cntrl_no', poId);
			customrecord.setFieldValue('custrecord_ebiz_order_no', parseInt(poId));
			customrecord.setFieldValue('custrecord_ebiz_receipt_no', poRecId);

			//Status flag .


			//Added for Item name and desc
			customrecord.setFieldValue('custrecord_skudesc', ItemDesc);
			customrecord.setFieldValue('custrecord_transport_lp', CartLP);

			//LotBatch and batch with Quantity.
			if(lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflg == "T"){
				customrecord.setFieldValue('custrecord_batch_no', LotBatch);
				customrecord.setFieldValue('custrecord_lotnowithquantity', lotwithqty);
			}

			var expdate;
			if(LotBatch!="" && LotBatch!=null)
			{

				var filterspor = new Array();
				filterspor.push(new nlobjSearchFilter('name', null, 'is', LotBatch));
				/*if(whLocation!=null && whLocation!="")
				filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);*/
				if(itemId!=null && itemId!="")
					filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemId));

				var column=new Array();
				column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
				column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

				var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
				if(receiptsearchresults!=null)
				{

					expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
					//vfifoDate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				}
				//nlapiLogExecution('DEBUG', 'expdate', expdate);
				//nlapiLogExecution('DEBUG', 'vfifoDate', vfifoDate);
			}
			customrecord.setFieldValue('custrecord_expirydate', expdate);
			customrecord.setFieldValue('custrecord_wms_location', poloc);
			customrecord.setFieldValue('custrecord_fifodate', DateStamp());

			//Assigning Sku dims level. 
			customrecord.setFieldValue('custrecord_ebizuomlevelskudim', uomlevel);

			//commit the record to NetSuite
			var recid = nlapiSubmitRecord(customrecord,false, true);
			nlapiLogExecution('ERROR','Create Open task record ID',recid);
			//alert('TRN_OPENTASK Success');
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Create Open task record Fail',exp);
		}
		if (tasktype == "2"&&wmsStsflag=="3") // Task = CHECKIN
		{
			nlapiLogExecution('ERROR','Invtflag',Invtflag);
			var idl='';
			if(Invtflag=='T'){

				nlapiLogExecution('ERROR','systemRule',systemRule);
				if(systemRule!='PO')
					idl=generateItemReceipt(trantype,poId,RcvQty,lineno,ItemStatus,itemId,trantype,poId,LP,"","","",LotBatch,"",poloc,"",Lotid);

				nlapiLogExecution('ERROR', 'Create Inventory Record Insertion', 'start');
				var varMergeLP = 'F';
				nlapiLogExecution('DEBUG', 'putmethod', putmethod);
				nlapiLogExecution('DEBUG', 'poloc', whloc);
				//if(methodid!=null && methodid!='')
				varMergeLP = isMergeLP(poloc,putmethod);
				var invtRecordId = createInvtRecord(tasktype, poId, endloc, LP, itemId,
						ItemStatus, ItemDesc, RcvQty, poloc, lgItemType, PackCode,batchflg,LotBatch,batchflg,Lotid,varMergeLP);
				updatePOline(poId,trantype);
				nlapiLogExecution('ERROR', 'Created Inventory Record ID', invtRecordId);


			}
		}

	}

	return recid;	
}

function createInvtRecord(taskType, po, endLocn, lp, itemId, itemStatus, itemDesc,
		receiveQty, poLocn, lgItemType, packCode,batchflag,LotBatch,batchflg,Lotid,varMergeLP) 
{

	var invtrecid;
	// Creating inventory for taskType = 1 (CHKN)
	nlapiLogExecution('ERROR', 'taskType', taskType);
	var sysdate=DateStamp();
	if (taskType == "2") 
	{
		try 
		{
			// Creating Inventory Record.	


			var filters = new Array();

			nlapiLogExecution('ERROR', 'ItemId',itemId);
			if (itemId != null && itemId != '') {		
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemId));			
			}

			nlapiLogExecution('ERROR', 'itemStatus',itemStatus);


			if(itemStatus!=null && itemStatus!='')
			{
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemStatus));
			}

			nlapiLogExecution('ERROR', 'packcode',packCode);
			if(packCode !=null && packCode!="")
			{
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'is', packCode));
			}

			nlapiLogExecution('ERROR', 'endLocn',endLocn);
			if(endLocn!=null&&endLocn!="")
			{
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', endLocn));
			}

			nlapiLogExecution('ERROR', 'Lotid',Lotid);
			if(Lotid!=null&& Lotid!="")
			{
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', [Lotid]));
			}



			//3-PUTAWAY COMPLETE, 19-STORAGE
			filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh'); 
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp'); 
			var inventorysearch = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

			var vid=0;
			var invExistLp='';
			var invExistQty=0;
			if(inventorysearch != null && inventorysearch != '')
			{
				//nlapiLogExecution('ERROR', 'Inventory found in same location for same item',ItemId);

				invExistQty  =  inventorysearch[0].getValue('custrecord_ebiz_qoh');	
				invExistLp = inventorysearch[0].getValue('custrecord_ebiz_inv_lp');	
				vid = inventorysearch[0].getId();
			}

			nlapiLogExecution('ERROR', 'invExistQty',invExistQty);
			nlapiLogExecution('ERROR', 'invExistLp',invExistLp);




			var expdate;
			/*if(Lotid!="" && Lotid!=null)
			{*/

			var filterspor = new Array();
			nlapiLogExecution('ERROR', 'LotBatch', LotBatch);
			filterspor.push(new nlobjSearchFilter('name', null, 'is', LotBatch));
			/*if(whLocation!=null && whLocation!="")
					filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);*/
			if(itemId!=null && itemId!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemId));

			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

			var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
			if(receiptsearchresults!=null)
			{
				//getlotnoid= receiptsearchresults[0].getId();

				expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
				//vfifoDate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				nlapiLogExecution('ERROR', 'expdate',expdate);
			}
			nlapiLogExecution('ERROR', 'varMergeLP', varMergeLP);

			if(varMergeLP!='F')
			{
				if(parseInt(invExistQty) > 0){  
					nlapiLogExecution('ERROR', 'If invExistQty > 0',invExistQty);


					var scount=1;
					LABL1: for(var i=0;i<scount;i++)
					{	
						nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
						try
						{
							nlapiLogExecution('ERROR', 'Created Inventory Record ID new', vid);
							var invLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', vid);
							invExistQty = invLoad.getFieldValue('custrecord_ebiz_qoh');
							var updatedInventory = parseInt(invExistQty) + parseInt(receiveQty);
							nlapiLogExecution('ERROR', 'updatedInventory', updatedInventory);
							nlapiLogExecution('ERROR', 'nlapiGetContext().getUser()', nlapiGetContext().getUser());
							invLoad.setFieldValue('custrecord_ebiz_qoh', updatedInventory);		
							invLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
							invLoad.setFieldValue('custrecord_ebiz_displayfield','N');	
							nlapiLogExecution('ERROR', 'expdate', expdate);
							if(expdate!=null && expdate!='' || expdate!='null')
								invLoad.setFieldValue('custrecord_ebiz_expdate', expdate);	

							//invLoad.setFieldValue('custrecord_ebiz_callinv', CallFlag);

							invtrecid = nlapiSubmitRecord(invLoad, false, true);
							nlapiLogExecution('ERROR', 'Created Inventory Record ID submit', invtrecid);
						}
						catch(ex)
						{
							var exCode='CUSTOM_RECORD_COLLISION'; 
							var wmsE='Inventory record being updated by another user. Please try again...';
							if (ex instanceof nlobjError) 
							{	
								wmsE=ex.getCode() + '\n' + ex.getDetails();
								exCode=ex.getCode();
							}
							else
							{
								wmsE=ex.toString();
								exCode=ex.toString();
							}  


							if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
							{
								scount=scount+1;
								continue LABL1;
							}
							else break LABL1;
						}
					}
					nlapiLogExecution('ERROR', 'Inventory Merged to the existing LP',invExistLp);


				}
				else
				{
					var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');			

					nlapiLogExecution('ERROR', 'Location',poLocn);


					invtRec.setFieldValue('name', po);
					invtRec.setFieldValue('custrecord_ebiz_inv_binloc', endLocn);
					invtRec.setFieldValue('custrecord_ebiz_inv_lp', lp);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemId);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
					invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packCode);
					invtRec.setFieldValue('custrecord_ebiz_inv_qty', receiveQty);
					invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemId);
					invtRec.setFieldValue('custrecord_ebiz_qoh', receiveQty);
					invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemDesc);
					invtRec.setFieldValue('custrecord_ebiz_inv_loc', poLocn);
					invtRec.setFieldValue('custrecord_invttasktype', 2); // CHKN..
					invtRec.setFieldValue('custrecord_wms_inv_status_flag', 19); // CHKN
					//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);	
					invtRec.setFieldValue('customrecord_ebiznet_location', endLocn); 
					invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
					invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
					if(vfifoDate!=null && vfifoDate!='')
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifoDate);

					invtRec.setFieldValue('custrecord_ebiz_transaction_no', po);
					nlapiLogExecution('ERROR', 'Lotid', Lotid);

					if(lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflg == "T"){
						invtRec.setFieldValue('custrecord_ebiz_inv_lot', Lotid);				
					}
					if(expdate!=null && expdate!='')
						invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
					//nlapiLogExecution('DEBUG', 'expdate', expdate);
					//nlapiLogExecution('DEBUG', 'vfifoDate', vfifoDate);
				}
			}
			else
			{
				var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');			

				nlapiLogExecution('ERROR', 'Location',poLocn);


				invtRec.setFieldValue('name', po);
				invtRec.setFieldValue('custrecord_ebiz_inv_binloc', endLocn);
				invtRec.setFieldValue('custrecord_ebiz_inv_lp', lp);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemId);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
				invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packCode);
				invtRec.setFieldValue('custrecord_ebiz_inv_qty', receiveQty);
				invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemId);
				invtRec.setFieldValue('custrecord_ebiz_qoh', receiveQty);
				invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemDesc);
				invtRec.setFieldValue('custrecord_ebiz_inv_loc', poLocn);
				invtRec.setFieldValue('custrecord_invttasktype', 2); // CHKN..
				invtRec.setFieldValue('custrecord_wms_inv_status_flag', 19); // CHKN
				//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);	
				invtRec.setFieldValue('customrecord_ebiznet_location', endLocn); 
				invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
				invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', sysdate);
				invtRec.setFieldValue('custrecord_ebiz_transaction_no', po);
				nlapiLogExecution('ERROR', 'Lotid', Lotid);

				if(lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflg == "T"){
					invtRec.setFieldValue('custrecord_ebiz_inv_lot', Lotid);				
				}
				if(expdate!=null && expdate!='')
					invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
				//nlapiLogExecution('DEBUG', 'expdate', expdate);
				//nlapiLogExecution('DEBUG', 'vfifoDate', vfifoDate);
			}


			//invtRec.setFieldValue('custrecord_ebiz_inv_company', vCompany);

			var invtRecordId = nlapiSubmitRecord(invtRec, false, true);			

			if (invtRecordId != null) {
				var type='create';
				var	AvalCubeCheck=	UpdateCube(receiveQty, parseFloat(itemId), endLocn,type,receiveQty,receiveQty);

				nlapiLogExecution('ERROR', 'Inventory Record Creation', invtRecordId);
			} else {
				nlapiLogExecution('ERROR', 'Inventory Record Creation', 'Failed');
			}
			//	} 
		}
		catch(e)
		{
			nlapiLogExecution('ERROR','exception in RF Inventory move',e.message);
		}//end of if (taskType == "1") 
	}

}
function UpdateCube(quantity, itemvalue ,binloc,type,oldqty,newqty){
	var cube;
	var BaseUOMQty;
	var RemCube;

	var resultqty = parseFloat(quantity);
	nlapiLogExecution('DEBUG', 'resultqty', resultqty);

	if(parseFloat(resultqty) != 0 && (type == 'create' || type == 'edit'))
	{
		var vCube = 0;
		var vweight = 0;
		// Item dimensions
		nlapiLogExecution('DEBUG', 'Create Inventory:item ID', itemvalue);
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemvalue);		    
		filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
		columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
		var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
		if(skuDimsSearchResults != null){
			for (var i = 0; i < skuDimsSearchResults.length; i++) {
				var skuDim = skuDimsSearchResults[i];
				cube = skuDim.getValue('custrecord_ebizcube');
				BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
			}
		}

		var itemCube = 0;
		if ((!isNaN(cube))){
			nlapiLogExecution('DEBUG', 'parseFloat(resultqty)', parseFloat(resultqty));
			var uomqty = ((parseFloat(resultqty))/(parseFloat(BaseUOMQty)));			
			itemCube = (parseFloat(uomqty) * parseFloat(cube));
			nlapiLogExecution('DEBUG', 'Create Inventory:itemCube', itemCube);
		} else {
			itemCube = 0;
		}
		nlapiLogExecution('DEBUG', 'itemCube', itemCube);
		var LocRemCube = GeteLocCube(binloc);
		nlapiLogExecution('DEBUG', 'Create Inventory:LocRemCube', LocRemCube);
		if(type=='edit')
		{
			if(parseFloat(oldqty)>parseFloat(newqty)){
				RemCube = parseFloat(LocRemCube) + parseFloat(itemCube);

			}
			else if(parseFloat(oldqty)< parseFloat(newqty))
			{

				RemCube = parseFloat(LocRemCube) - parseFloat(itemCube);
			}
		}
		else
		{
			RemCube = parseFloat(LocRemCube) - parseFloat(itemCube);
		}

		nlapiLogExecution('DEBUG', 'LocId in RemCube', RemCube);
		nlapiLogExecution('DEBUG', 'LocId in UpdateLocCube', binloc);
		if (binloc != null && binloc != "" && RemCube >= 0){
			nlapiLogExecution('DEBUG', 'RemCube', RemCube);
			var retValue = UpdateLocCube(binloc, RemCube);
			nlapiLogExecution('DEBUG', 'LocId in UpdateLocCube', retValue);
			return  true;
		}
		else
		{
			return  false;
		}
	}	
}


function generatePalletLabel(poValue,poloc,poTrailer)
{
	try
	{
		nlapiLogExecution('ERROR', 'poValue', poValue);
		nlapiLogExecution('ERROR', 'poloc', poloc);
		nlapiLogExecution('ERROR', 'cartonlabel', 'PALLETLABEL');

		var palletlabel="";
		var labeltype="PALLETLABEL";
		var tasktype=1;
		var filtertempalte = new Array();
		filtertempalte.push(new nlobjSearchFilter('custrecord_labeltemplate_type',null,'is',labeltype));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_labeltemplate_data');
		var searchtemplate= nlapiSearchRecord('customrecord_label_templates', null,filtertempalte,columns);
		if((searchtemplate !=null) &&(searchtemplate!=""))
		{
			palletlabel=searchtemplate[0].getValue('custrecord_labeltemplate_data');
		}
		var print ="F";
		var reprint="F";
		var tasktype=1;
		var username,skuname,skudesc,exptqty,ponumber,recevieddate,reciptno;
		var filters= new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null,'is',poValue));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_sku');
		columns[1] = new nlobjSearchColumn('custrecord_ebizuser');
		columns[2] = new nlobjSearchColumn('custrecord_skudesc');
		columns[3] = new nlobjSearchColumn('custrecord_ot_receipt_no');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[5] = new nlobjSearchColumn('custrecord_current_date');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
		if(searchresults!=null)
		{
			skuname =searchresults[0].getText('custrecord_sku');
			username=searchresults[0].getText('custrecord_ebizuser');
			skudesc=searchresults[0].getValue('custrecord_skudesc');
			reciptno=searchresults[0].getValue('custrecord_ot_receipt_no');
			ponumber=searchresults[0].getText('custrecord_ebiz_order_no');
			recevieddate=searchresults[0].getValue('custrecord_current_date');
		}
		var splitPonumber=null;
		if(ponumber!=null && ponumber!='')
		{
			splitPonumber=ponumber.split('#')[1];
		}
		if((recevieddate!=null)&&(recevieddate!=""))
		{
			palletlabel=palletlabel.replace(/Parameter26/,recevieddate);

		}
		else
		{
			palletlabel=palletlabel.replace(/Parameter26/,recevieddate);
		}
		if((skuname!=null)&&(skuname!=""))
		{
			palletlabel=palletlabel.replace(/Parameter27/g,skuname);
		}
		else
		{
			palletlabel=palletlabel.replace(/Parameter27/g,'');
		}

		if((username!=null)&&(username!=""))
		{
			palletlabel=palletlabel.replace(/Parameter31/,username);
		}
		else
		{
			palletlabel=palletlabel.replace(/Parameter31/,'');
		}
		if((skudesc!=null)&&(skudesc!=""))
		{
			palletlabel=palletlabel.replace(/Parameter28/,skudesc);
		}
		else
		{
			palletlabel=palletlabel.replace(/Parameter28/,'');
		}
		if((reciptno!=null)&&(reciptno!=""))
		{
			palletlabel=palletlabel.replace(/parameter30/,reciptno);
		}
		else if((splitPonumber!=null)&&(splitPonumber!=""))
		{
			palletlabel=palletlabel.replace(/parameter30/,splitPonumber);
		}
		else
		{
			palletlabel=palletlabel.replace(/parameter30/,'');
		}
		//CreateLabelData(palletlabel,labeltype,tasktype,poValue,print,reprint,"",poloc,poTrailer,"",skuname,"","");
		CreateLabelData(palletlabel,labeltype,tasktype,poValue,print,reprint,"",poloc,poValue,"",skuname,"","");
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','PalletLabelGenerationException',exp);
	}

}



function IsContLpExist(vContLpNo)
{

	nlapiLogExecution('DEBUG', 'Into IsContLpExist',vContLpNo);	
	var IsContLpExist='F';

	try
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_label_reference5',null,'is',vContLpNo));
		filter.push(new nlobjSearchFilter('custrecord_label_labeltype',null,'is','QAlabel'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_label_order');
		var eXtrecordlist= nlapiSearchRecord('customrecord_ext_labelprinting',null,filter,columns);
		if(eXtrecordlist!=null)
		{
			IsContLpExist='T';		
		}
	}
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'unexpected error in IsContLpExist');	
	}
	nlapiLogExecution('DEBUG', 'Out of IsContLpExist',IsContLpExist);	
	return IsContLpExist;	
}



//code for creating QA label
function generateQALabel(ItemName,ItemDesc,LP,LotBatch,linecount,actbegindate,expdate,poNum)
{
	try
	{
		nlapiLogExecution('ERROR', 'cartonlabel', 'QALABEL');

		if(IsContLpExist(LP)!='T')
		{
			//nlapiLogExecution('ERROR', 'cartonlabelpoValue', poId);
			nlapiLogExecution('ERROR', 'ItemName', ItemName);
			nlapiLogExecution('ERROR', 'skudesc', ItemDesc);
			nlapiLogExecution('ERROR', 'linecount', linecount);
			nlapiLogExecution('ERROR', 'LP', LP);
			nlapiLogExecution('ERROR', 'expdate', expdate);
			nlapiLogExecution('ERROR', 'poNum', poNum);
			var recevieddate=DateStamp();
			nlapiLogExecution('ERROR', 'recevieddate', recevieddate);
			var eXtlabelrecord = nlapiCreateRecord('customrecord_ext_labelprinting');
			eXtlabelrecord.setFieldValue('name',poNum);
			eXtlabelrecord.setFieldValue('custrecord_label_labeltype','QAlabel');
			eXtlabelrecord.setFieldValue('custrecord_label_item',ItemName);
			eXtlabelrecord.setFieldValue('custrecord_label_itemdesc',ItemDesc);
			eXtlabelrecord.setFieldValue('custrecord_label_reference1',linecount); 
			eXtlabelrecord.setFieldValue('custrecord_label_reference2',actbegindate);
			eXtlabelrecord.setFieldValue('custrecord_label_reference3',LotBatch);
			eXtlabelrecord.setFieldValue('custrecord_label_reference4',expdate);
			eXtlabelrecord.setFieldValue('custrecord_label_reference5',LP);
			eXtlabelrecord.setFieldValue('custrecord_label_order',poNum);
			var tranid = nlapiSubmitRecord(eXtlabelrecord);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','QAGenerationException',exp);
	}
}
function CreateLPMaster(LP,CartLP)
{
	var lprecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
	lprecord.setFieldValue('name', LP);
	lprecord.setFieldValue('custrecord_ebiz_lpmaster_lp', LP);		
	lprecord.setFieldValue('custrecord_ebiz_lpmaster_masterlp', CartLP);	

	var recid = nlapiSubmitRecord(lprecord);	
}

/**
 * 
 * @param itemID
 * @returns
 */
function getItemDimensions(itemID){
	//nlapiLogExecution('ERROR', 'In Item Dimensions', "here1");
	var retItemDimArray = null;

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemID));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomskudim');
	columns[3] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');
	columns[0].setSort(true);

	retItemDimArray = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, columns);
	if(retItemDimArray != null && retItemDimArray != '')
		nlapiLogExecution('ERROR', 'retItemDimArray', retItemDimArray.length);
	return retItemDimArray;
}

/**
 * To fetch inbound staging location
 * @returns {String}
 */
function getStagingLocation()
{
	var retInboundStagingLocn = "";

	/*	 
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}

/**
 * 
 * @param orderQty
 * @param palletQty
 * @returns {Number}
 */
function getSKUDimCount(orderQty, dimQty){
	var retSKUDimCount = 0;

	if(dimQty > 0 && (parseInt(orderQty) > parseInt(dimQty)))
	{
		skuDimCount = parseInt(orderQty)/parseInt(dimQty);
		retSKUDimCount = Math.floor(skuDimCount);
		if(retSKUDimCount == 0)
			retSKUDimCount = 1;
	}

	return retSKUDimCount;
}


function generateItemReceipt(fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,invtlp,systemRule,RecordId,EndLocationId,batchno,qtyexceptionFlag,templocation,getScannedSerialNoinQtyexception)
{
	try {
		nlapiLogExecution('ERROR', "fromrecord", fromrecord);
		nlapiLogExecution('ERROR', "fromid", fromid);
		var tempserialId=0;
		var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
		var polinelength = trecord.getLineItemCount('item');
		nlapiLogExecution('ERROR', "polinelength", polinelength);
		var idl;
		var vAdvBinManagement=true;

		var itemidarray=new Array();
		var itemstatusarray=new Array();
		var templocationarray=new Array();
		var varItemlocationarray=new Array();
		var quantityarray=new Array();
		var lotnotransferarray=new Array();
		var serialnotransferarray=new Array();



		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
		}
		//}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);
		for (var j = 1; j <= polinelength; j++) {
			nlapiLogExecution('ERROR', "Get Item Id", trecord.getLineItemValue('item', 'item', j));
			nlapiLogExecution('ERROR', "Get Item Text", trecord.getLineItemText('item', 'item', j));

			var item_id = trecord.getLineItemValue('item', 'item', j);
			var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = trecord.getLineItemValue('item', 'line', j);
			//var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', j);//text
			var poItemUOM = trecord.getLineItemValue('item', 'units', j);//value

			var commitflag = 'N';
			var quantity=0;
			quantity = ActQuantity;

			nlapiLogExecution('ERROR', 'quantity', quantity);

			var serialnotransfer;
			var lotnotransfer;



			var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
			var columns = nlapiLookupField('item', itemid, fields);
			var ItemType = columns.recordType;					
			var batchflg = columns.custitem_ebizbatchlot;
			var itemfamId= columns.custitem_item_family;
			var itemgrpId= columns.custitem_item_group;
			var serialInflg = columns.custitem_ebizserialin;

			nlapiLogExecution('ERROR', 'ItemType', ItemType);
			nlapiLogExecution('ERROR', 'batchflg', batchflg);
			nlapiLogExecution('ERROR', 'batchno', batchno);



			var inventorytransfer="";
			nlapiLogExecution('ERROR', "itemLineNo(PO/TO)", itemLineNo);
			nlapiLogExecution('ERROR', "linenum(Task)", linenum);
			nlapiLogExecution('ERROR', "poItemUOM", poItemUOM);

			if(trantype!='transferorder')
			{
				if (itemLineNo == linenum) {

					//This part of the code will fetch the line level location.
					//If line level is empty then header level location is considered.
					//code added by suman on 080513.
					whLocation=trecord.getLineItemValue('item', 'location', j);//value
					if(whLocation==null||whLocation=="")
						whLocation=templocation;
					//End of code as of 080513.

					//var vrcvqty=arrQty[i];
					var vrcvqty=quantity;
					if(poItemUOM!=null && poItemUOM!='')
					{
						var vbaseuomqty=0;
						var vuomqty=0;
						var eBizItemDims=geteBizItemDimensions(item_id);
						if(eBizItemDims!=null&&eBizItemDims.length>0)
						{
							nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
							for(var z=0; z < eBizItemDims.length; z++)
							{
								if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
								{
									vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
								}
								nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
								if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
								{
									vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
								}
							}
							if(vuomqty==null || vuomqty=='')
							{
								vuomqty=vbaseuomqty;
							}
							nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
							nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

							if(quantity==null || quantity=='' || isNaN(quantity))
								quantity=0;
							else
								quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

						}
					}
					//Code Added by Ramana
					var varItemlocation;
					nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
					if(itemstatus!=null && itemstatus!='')
						varItemlocation = getItemStatusMapLoc(itemstatus);
					//upto to here

					commitflag = 'Y';

					var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	


						serialInflg = columns.custitem_ebizserialin;
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					/*if (Itype != "serializedinventoryitem" || serialInflg != "T")
					{*/
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', quantity);
					nlapiLogExecution('ERROR', 'varItemlocation', varItemlocation);

					if(varItemlocation!=null && varItemlocation!=''){
						trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
					}
					else
					{
						trecord.setCurrentLineItemValue('item', 'location', templocation);
					}//Added by ramana
					/*}*/

					nlapiLogExecution('ERROR', 'Serialquantity', quantity);
					nlapiLogExecution('ERROR', 'Into SerialNos');
					var totalconfirmedQty=0;
					if (Itype == "serializedinventoryitem" || serialInflg == "T") {
						nlapiLogExecution('ERROR', 'Into SerialNos');
						nlapiLogExecution('ERROR', 'invtlp', invtlp);	
						nlapiLogExecution('ERROR', 'trantype', trantype);		
						var serialline = new Array();
						var serialId = new Array();

						var tempSerial = "";
						var filters = new Array();


//						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
//						filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
//						filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

						if(trantype=='returnauthorization')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							//Commented for RMA, For RMA no need to check  whether status is checkin/storage
							//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						}

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();
						if (serchRec) {
							for (var n = 0; n < serchRec.length; n++) {
								//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
								if (tempserialId == "") {
									tempserialId = serchRec[n].getId();
								}
								else {
									tempserialId = tempserialId + "," + serchRec[n].getId();
								}

								//This is for Serial num loopin with Space separated.
								if (tempSerial == "") {
									tempSerial = serchRec[n].getValue('custrecord_serialnumber');

									serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

								}
								else {

									tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
									if(tempSerial.length>3500)
									{
										tempSerial='';
										trecord.selectLineItem('item', j);
										trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
										var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
										trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

										trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
										trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
										nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);

										trecord.commitLineItem('item');
										idl = nlapiSubmitRecord(trecord);
										serialnumArray=new Array();
										totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
										if(n<serchRec.length)
										{
											var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
											nlapiLogExecution('ERROR', 'tQty1', tQty);
											var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
											var polinelength = trecord.getLineItemCount('item');
											/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
											var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
											var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
											var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
											var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

											/*if (itemLineNo == linenum)
											{*/
											if(poItemUOM!=null && poItemUOM!='')
											{
												var vbaseuomqty=0;
												var vuomqty=0;
												var eBizItemDims=geteBizItemDimensions(item_id);
												if(eBizItemDims!=null&&eBizItemDims.length>0)
												{
													nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
													for(var z=0; z < eBizItemDims.length; z++)
													{
														if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
														{
															vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
														}
														nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
														nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
														nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
														if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
														{
															vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
														}
													}
													if(vuomqty==null || vuomqty=='')
													{
														vuomqty=vbaseuomqty;
													}
													nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
													nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

													if(tQty==null || tQty=='' || isNaN(tQty))
														tQty=0;
													else
														tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

												}
											}
											nlapiLogExecution('ERROR', 'tQtylast', tQty);
											trecord.selectLineItem('item', itemLineNo);
											trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
											var tempqty=parseInt(n)+parseInt(1);
											trecord.setCurrentLineItemValue('item', 'quantity', tQty);

											trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
											//}

											//}
										}






									}
									serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

								}
								if(vAdvBinManagement)
								{
									var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
									compSubRecord.selectNewLineItem('inventoryassignment');
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

									compSubRecord.commitLineItem('inventoryassignment');
									compSubRecord.commit();
								}

								//if(parseInt(n)== parseInt(quantity-1))
								//	break;




							}
							nlapiLogExecution('ERROR', 'qtyexceptionFlag', qtyexceptionFlag);
							if(qtyexceptionFlag == "true")
							{
								serialnumcsv = getScannedSerialNoinQtyexception;
								nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is true', serialnumcsv);
							}
							else
							{
								serialnumcsv = tempSerial;
								nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is false', serialnumcsv);
							}


						}
						tempSerial = "";
						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if ((Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem")  && (trecord!=null && trecord!='')) {
								//trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);
							}
						}


					}
					else 
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem"   || batchflg == "T") {
							nlapiLogExecution('ERROR', 'Into LOT/Batch');
							nlapiLogExecution('ERROR', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";
							var confirmLotToNS='Y';

							confirmLotToNS=GetConfirmLotToNS(varItemlocation);
							nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);
							var batfilter = new Array();
							batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
							batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
							batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
							batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
							batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

							var vatColumns=new Array();
							vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
							vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
							/*var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);

							if (batchsearchresults) {
								for (var n = 0; n < batchsearchresults.length; n++) {


									//This is for Batch num loopin with Space separated.
									if (tempBatch == "") {
										if(confirmLotToNS == 'Y')
											tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
										else
										{
											nlapiLogExecution('ERROR', 'item_id:', item_id);
											var filters1 = new Array();          
											filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
											//Changed on 30/4/12 by suman
											filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
											//End of Chages as of 30/4/12

											var columns = new Array();

											columns[0] = new nlobjSearchColumn('itemid');
											var vItemname;
											var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
											if (itemdetails !=null) 
											{
												vItemname=itemdetails[0].getValue('itemid');
												nlapiLogExecution('ERROR', 'vItemname:', vItemname);
												var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
												vItemname=vItemname.replace(/ /g,"-");
												tempBatch=vItemname + '('+ vItQty + ')';


											}
										}
									}
									else {
										if(confirmLotToNS == 'Y')
											tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
										else
										{
											var filters1 = new Array();          
											filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
											//Changed on 30/4/12 by suman
											filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
											//End of Chages as of 30/4/12
											var columns = new Array();

											columns[0] = new nlobjSearchColumn('itemid');
											var vItemname;
											var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
											if (itemdetails !=null) 
											{
												vItemname=itemdetails[0].getValue('itemid');
												nlapiLogExecution('ERROR', 'vItemname:', vItemname);
												var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
												vItemname=vItemname.replace(/ /g,"-");
												tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';


											}
										}

									}

								}

								batchcsv = tempBatch;
								tempBatch = "";
							}*/
							var vItemname;
							if(confirmLotToNS == 'Y')
								tempBatch = batchno + '('+ quantity + ')';
							else
							{
								nlapiLogExecution('ERROR', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}

							if(!vAdvBinManagement)
							{	
								batchcsv = tempBatch;
								nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
								//20123720 start
								nlapiLogExecution('ERROR', 'batchno', batchno);
								var expdate1=getLotExpdate(batchno,item_id);
								if(expdate1 !=null && expdate1 !='')
								{
									trecord.setCurrentLineItemValue('item', 'expirationdate', expdate1);

								}//end
							}
							else
							{
								var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
								compSubRecord.selectNewLineItem('inventoryassignment');
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
								if(confirmLotToNS == 'Y')
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
								else
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
								compSubRecord.commitLineItem('inventoryassignment');
								compSubRecord.commit();
							}


						}
					trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');
				}
			}
			else
			{ 

				nlapiLogExecution('ERROR', 'itemid (Task)', itemid);	
				nlapiLogExecution('ERROR', 'item_id (Line)', item_id);	
				//if(vTORestrictFlag =='F')
				//{	



				itemLineNo=parseFloat(itemLineNo)-2;
				if (itemid == item_id && itemLineNo == linenum) {

					commitflag = 'Y';

					var varItemlocation;				
					varItemlocation = getItemStatusMapLoc(itemstatus);

					var fields = ['recordType', 'custitem_ebizserialin'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	

						//	var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;
						nlapiLogExecution('ERROR', 'Itype', Itype);
						nlapiLogExecution('ERROR', 'columns', columns);
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', quantity);
					/*if(trantype!='transferorder')
					nlapiLogExecution('ERROR', 'varItemlocation', varItemlocation);
						trecord.setCurrentLineItemValue('item', 'location', varItemlocation);*/ //Added by ramana

					nlapiLogExecution('ERROR', 'templocation', templocation);
					trecord.setCurrentLineItemValue('item', 'location', templocation);

					nlapiLogExecution('ERROR', 'Serialquantity', quantity);
					nlapiLogExecution('ERROR', 'Into SerialNos');
					if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") {
						nlapiLogExecution('ERROR', 'Into SerialNos');
						nlapiLogExecution('ERROR', 'invtlp', invtlp);					
						var serialline = new Array();
						var serialId = new Array();



						var tempSerial = "";
						var filters = new Array();
						/*if(trantype=='returnauthorization' || trantype=='transferorder')
						{*/
						if(trantype=='returnauthorization')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
						}
						else if(trantype=='transferorder')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1']);
							filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
						}
//						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();var totalconfirmedQty=0;
						if (serchRec) {
							for (var n = 0; n < serchRec.length; n++) {
								var tempserialcount=parseInt(n)+1;
								if(parseInt(tempserialcount)<=parseInt(quantity)){
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');

										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									else {

										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
										if(tempSerial.length>3500)
										{
											tempSerial='';
											trecord.selectLineItem('item', j);
											trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
											var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
											trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

											trecord.setCurrentLineItemValue('item', 'location', templocation);
											trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
											nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);

											trecord.commitLineItem('item');
											idl = nlapiSubmitRecord(trecord);
											serialnumArray=new Array();
											totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
											if(n<serchRec.length)
											{
												var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
												nlapiLogExecution('ERROR', 'tQty1', tQty);
												var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
												var polinelength = trecord.getLineItemCount('item');
												/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
												var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
												var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
												//var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
												var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

												/*if (itemLineNo == linenum)
											{*/
												if(poItemUOM!=null && poItemUOM!='')
												{
													var vbaseuomqty=0;
													var vuomqty=0;
													var eBizItemDims=geteBizItemDimensions(item_id);
													if(eBizItemDims!=null&&eBizItemDims.length>0)
													{
														nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
														for(var z=0; z < eBizItemDims.length; z++)
														{
															if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
															{
																vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
															}
															nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
															if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
															{
																vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
															}
														}
														if(vuomqty==null || vuomqty=='')
														{
															vuomqty=vbaseuomqty;
														}
														nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
														nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

														if(tQty==null || tQty=='' || isNaN(tQty))
															tQty=0;
														else
															tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

													}
												}
												nlapiLogExecution('ERROR', 'tQtylast', tQty);
												trecord.selectLineItem('item', itemLineNo);
												trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
												var tempqty=parseInt(n)+parseInt(1);
												trecord.setCurrentLineItemValue('item', 'quantity', tQty);

												trecord.setCurrentLineItemValue('item', 'location', templocation);
												//}

												//}
											}






										}
										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									if(vAdvBinManagement)
									{
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										compSubRecord.commit();
									}	
								}
							}

							serialnumcsv = tempSerial;
							tempSerial = "";
						}

						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem") {
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
							}
						}
					}
					else 
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem"  || batchflg == "T") {
							nlapiLogExecution('ERROR', 'Into LOT/Batch');
							nlapiLogExecution('ERROR', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";

							var confirmLotToNS='Y';

							confirmLotToNS=GetConfirmLotToNS(templocation);

							nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);


							/*var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										if (tempBatch == "") {
											if(confirmLotToNS == 'Y')
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('ERROR','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';


												}
											}
										}
										else {
											if(confirmLotToNS == 'Y')
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('ERROR','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';


												}
											}

										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}*/
							var vItemname;

							if(confirmLotToNS == 'Y')
								tempBatch = batchno + '('+ quantity + ')';
							else
							{
								nlapiLogExecution('ERROR', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}
							if(!vAdvBinManagement)
							{	
								batchcsv = tempBatch;
								nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							}
							else
							{
								var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
								compSubRecord.selectNewLineItem('inventoryassignment');
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
								if(confirmLotToNS == 'Y')
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
								else
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
								compSubRecord.commitLineItem('inventoryassignment');
								compSubRecord.commit();
							}	
						}
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');

					serialnotransfer = serialnumArray;
					lotnotransfer = batchcsv;				 

					var Invtrymveloc;
					if(templocation!= varItemlocation){

						itemidarray.push(item_id);
						itemstatusarray.push(itemstatus);
						templocationarray.push(templocation);
						varItemlocationarray.push(varItemlocation);
						quantityarray.push(quantity);
						lotnotransferarray.push(lotnotransfer);
						serialnotransferarray.push(serialnotransfer);

						nlapiLogExecution('ERROR', 'Inventory transfer to Item status mapped location');
					}
				}
				//Code Added by Ramana
				/*}
				else
				{
					trecord=null;
					idl=pointid;
				}*/	
			}
			if (commitflag == 'N' && trecord != null && trecord != '') {
				nlapiLogExecution('ERROR', 'commitflag is N', commitflag);
				trecord.selectLineItem('item', j);
				trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
				trecord.commitLineItem('item');
			}
		}
		nlapiLogExecution('ERROR', 'Before Item Receipt---', 'Before');

		if(trecord != null && trecord != '')
			idl = nlapiSubmitRecord(trecord);

		if(idl!=null && idl!='')
		{
			for(var p=0; p<itemidarray.length;p++ )
			{
				Invtrymveloc =  InvokeNSInventoryTransfer(itemidarray[p],itemstatusarray[p],templocationarray[p],varItemlocationarray[p],quantityarray[p],lotnotransferarray[p],serialnotransferarray[p]);
			}
		}
		nlapiLogExecution('ERROR', 'Item Receipt idl', idl);

		return idl;
	}
	catch(exp)
	{
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		var POarray = new Array();
		nlapiLogExecution('ERROR', 'DetailsError', functionality);	
		nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('ERROR', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,exp,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('ERROR', 'Exception in TransformRec (PurchaseOrder) : ', exp);

		if (exp instanceof nlobjError) 
			nlapiLogExecution('ERROR', 'system error', exp.getCode() + '\n' + exp.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', exp.toString());

		nlapiLogExecution('ERROR', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('ERROR', 'PUTW task updated with act end location', EndLocationId);

		nlapiLogExecution('ERROR', 'Putaway Failed ');
		POarray["custparam_error"] = exp.getCode() + '\n' + exp.getDetails();
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		return;
	}	
}


function getLotExpdate(vbatchno,itemid)
{
	try{
		nlapiLogExecution('ERROR', 'vbatchno', vbatchno);
		nlapiLogExecution('ERROR', 'itemid', itemid);
		var expdate='';
		var filterspor = new Array();
		filterspor.push(new nlobjSearchFilter('name', null, 'is', vbatchno));
		if(itemid!=null && itemid!="")
			filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
		column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

		var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
		if(receiptsearchresults!=null)
		{
			var getlotnoid= receiptsearchresults[0].getId();
			nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
			expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');

		}
		nlapiLogExecution('ERROR', 'expdate', expdate);
		return expdate;
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'expection in getLotExpdate', e);
	}

}


function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot,serialno)
{
	try{
		nlapiLogExecution('ERROR', 'Into InvokeNSInventoryTransfer');		
		nlapiLogExecution('ERROR', 'item', item);		
		nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
		nlapiLogExecution('ERROR', 'loc', loc);
		nlapiLogExecution('ERROR', 'toloc', toloc);
		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'lot', lot);
		nlapiLogExecution('ERROR', 'test1', 'test1');

		var ItemType='';

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType' ]);
			ItemType = columns.recordType;
		}

		nlapiLogExecution('ERROR', 'ItemType', ItemType);
		var confirmLotToNS='N';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;	

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		//Changed on 30/4/12 by suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		//End of changes as on 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}

		var invttransfer = nlapiCreateRecord('inventorytransfer');			

		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
		invttransfer.setCurrentLineItemValue('inventory', 'unitconversionrate', '1');


		var subs = nlapiGetContext().getFeature('subsidiaries');
		nlapiLogExecution('ERROR', 'subs', subs);
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);			
		}


		if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			if(confirmLotToNS=='N')
				lot=vItemname;	

			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				nlapiLogExecution('ERROR', 'test1', 'test1');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					nlapiLogExecution('ERROR', 'serialnoNowithQty', serialno);
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  serialno);
				}	
				else
				{
					nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
				}
			}
		}

		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);

		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}
}

function isMergeLP(poloc,methodid)
{
	var varMergeLP = 'F';
	var filtersputmethod = new Array();
	filtersputmethod.push(new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof', ['@NONE@',poloc]));
	if(methodid!=null && methodid!='')
		filtersputmethod.push(new nlobjSearchFilter('custrecord_methodid', null, 'is', methodid));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            

	var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	if(putwmethodsearchresults != null &&putwmethodsearchresults != ''&& putwmethodsearchresults.length > 0)
	{

		varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
	}

	return varMergeLP;
}
function updatePOline(vPoNo,trantype)
{
	try
	{
		nlapiLogExecution('ERROR', 'Into updatePOline', vPoNo);
		nlapiLogExecution('ERROR', 'Into updatePOline', trantype);


		var poRecord = nlapiLoadRecord(trantype, vPoNo);

		var vPOItemLength = poRecord.getLineItemCount('item');
		for(var vcount=1;vcount<=vPOItemLength;vcount++)
		{
			var poline= poRecord.getLineItemValue('item', 'line', vcount);


			var ItemQuantity = poRecord.getLineItemValue('item', 'quantity', poline);
			if (ItemQuantity == null || ItemQuantity == '') {
				ItemQuantity = '0';
			}

			var ItemQuantityReceived = poRecord.getLineItemValue('item', 'quantityreceived', poline);
			if (ItemQuantityReceived == null || ItemQuantityReceived == '') {
				ItemQuantityReceived = '0';
			}

			nlapiLogExecution('ERROR', 'Into ItemQuantity', ItemQuantity);
			nlapiLogExecution('ERROR', 'Into ItemQuantityReceived', ItemQuantityReceived);
			nlapiLogExecution('ERROR', 'Into poline', poline);
			nlapiLogExecution('ERROR', 'Into vcount', vcount);

			if(trantype=="transferorder")
				poline=vcount;
			if(ItemQuantityReceived != 0)
			{
				if(parseFloat(ItemQuantity) == parseFloat(ItemQuantityReceived))
				{
					poRecord.setLineItemValue('item', 'custcol_transactionstatusflag',poline,3);//putaway completed
				}
				else
				{
					poRecord.setLineItemValue('item', 'custcol_transactionstatusflag',poline,4);//partially received
				}
			}
			else
			{
				poRecord.setLineItemValue('item', 'custcol_transactionstatusflag',poline,4);
			}


		}
		nlapiSubmitRecord(poRecord, true); 
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception",exp);
	}
	nlapiLogExecution('ERROR', 'Out of updatePOline', vPoNo);
}
