/***************************************************************************
�eBizNET Solutions Inc  
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_PutawayLocation.js,v $
 *
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_Cart_PutawayLocation.js,v $
 *� Revision 1.2.2.24.4.16.2.81.2.3  2015/11/19 15:25:20  grao
 *� 2015.2 Issue Fixes 201414649
 *�
 *� Revision 1.2.2.24.4.16.2.81.2.2  2015/11/06 14:13:09  snimmakayala
 *� 201415040
 *�
 *� Revision 1.2.2.24.4.16.2.81.2.1  2015/10/01 12:44:55  schepuri
 *� case# 201412136
 *�
 *� Revision 1.2.2.24.4.16.2.81  2015/09/02 15:30:39  grao
 *� 2015.2   issue fixes 201414232
 *�
 *� Revision 1.2.2.24.4.16.2.80  2015/07/15 13:30:47  schepuri
 *� case# 201413383
 *�
 *� Revision 1.2.2.24.4.16.2.79  2015/05/27 07:59:28  grao
 *� SB issue fixes  201412879
 *�
 *� Revision 1.2.2.24.4.16.2.78  2015/01/06 06:34:51  schepuri
 *� issue#   201411275
 *�
 *� Revision 1.2.2.24.4.16.2.77  2014/11/03 16:33:37  gkalla
 *� case # 201410852
 *� CT item receipt issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.76  2014/10/15 10:42:19  skavuri
 *� Case# 201410710 Std bundle issue fixed
 *�
 *� Revision 1.2.2.24.4.16.2.75  2014/09/30 15:16:58  sponnaganti
 *� case# 201410518
 *� stnd bundle issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.74  2014/09/23 14:31:23  snimmakayala
 *� Case: 20149505
 *� Merge FIFO dates
 *�
 *� Revision 1.2.2.24.4.16.2.73  2014/09/15 15:56:06  nneelam
 *� case# 201410314
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.2.2.24.4.16.2.72  2014/08/08 15:01:15  nneelam
 *� case# 20149885
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.2.2.24.4.16.2.71  2014/08/07 06:08:09  skreddy
 *� case # 20149853
 *� True Fabrications SB issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.70  2014/08/04 20:03:07  grao
 *� Case#: 20148527 Standard bundel  issue fixes
 *�
 *� Revision 1.2.2.24.4.16.2.69  2014/08/01 14:57:52  rmukkera
 *� Case # 20149553
 *�
 *� Revision 1.2.2.24.4.16.2.68  2014/07/30 15:33:24  sponnaganti
 *� Case# 20149771
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.67  2014/07/23 07:08:09  snimmakayala
 *� Case: 20149505
 *� FIFO merge changes
 *�
 *� Revision 1.2.2.24.4.16.2.66  2014/07/21 16:03:14  sponnaganti
 *� Case# 20148231
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.65  2014/07/15 16:35:20  sponnaganti
 *� Case# 20149411
 *� Stnd Bundle issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.64  2014/07/10 09:52:18  sponnaganti
 *� Case# 20149358
 *� Compatibility issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.63  2014/06/27 10:21:31  sponnaganti
 *� case# 20149024 20149027
 *� New UI Compatability issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.62  2014/06/13 15:01:27  grao
 *� Case#: 20148623
 *� Standard bundle issue fixes
 *�
 *� Revision 1.2.2.24.4.16.2.61  2014/06/13 07:44:38  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.2.2.24.4.16.2.60  2014/06/06 06:45:11  skavuri
 *� Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *�
 *� Revision 1.2.2.24.4.16.2.59  2014/05/30 00:26:48  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.2.2.24.4.16.2.58  2014/05/20 15:47:54  skavuri
 *� Case # 20148450 SB Issue Fixed
 *�
 *� Revision 1.2.2.24.4.16.2.57  2014/03/24 14:38:08  rmukkera
 *� Case # 20127751�
 *�
 *� Revision 1.2.2.24.4.16.2.56  2014/03/12 06:31:18  skreddy
 *� case 20127629
 *� NLS SB issue fixs
 *�
 *� Revision 1.2.2.24.4.16.2.55  2014/02/27 15:36:49  skavuri
 *� Case # 20127407 (to show the serialNo# in opentask after doing cart putaway ) issue fixed
 *�
 *� Revision 1.2.2.24.4.16.2.54  2014/02/24 15:36:05  sponnaganti
 *� case# 20127265
 *� (createCurrentLineItemSubrecord should create only once)
 *�
 *� Revision 1.2.2.24.4.16.2.53  2014/02/19 13:33:17  sponnaganti
 *� case# 20126998
 *� Standard Bundle Issue (Code added from RYONET SB Acc)
 *�
 *� Revision 1.2.2.24.4.16.2.52  2014/02/17 16:28:16  sponnaganti
 *� case 20127154
 *� (allow only for serial item for that Itype added to if)
 *�
 *� Revision 1.2.2.24.4.16.2.51  2014/02/13 15:05:05  rmukkera
 *� Case # 20126900
 *�
 *� Revision 1.2.2.24.4.16.2.50  2014/01/31 13:32:06  schepuri
 *� 20126968
 *� standard bundle issue
 *�
 *� Revision 1.2.2.24.4.16.2.49  2014/01/20 13:52:04  schepuri
 *� 20126856
 *� standard bundle issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.48  2014/01/09 12:35:17  schepuri
 *� 20126720
 *�
 *� Revision 1.2.2.24.4.16.2.47  2014/01/08 13:52:59  schepuri
 *� 20126691
 *�
 *� Revision 1.2.2.24.4.16.2.46  2014/01/06 13:12:59  grao
 *� Case# 20126579 related issue fixes in Sb issue fixes
 *�
 *� Revision 1.2.2.24.4.16.2.45  2013/12/24 15:13:44  rmukkera
 *� Case # 20126458
 *�
 *� Revision 1.2.2.24.4.16.2.44  2013/12/16 13:57:37  schepuri
 *� 20125978 , 20126403
 *�
 *� Revision 1.2.2.24.4.16.2.43  2013/12/11 22:19:55  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue fixed related to case#201214046
 *�
 *� Revision 1.2.2.24.4.16.2.42  2013/12/11 13:53:41  schepuri
 *� 20126278
 *�
 *� Revision 1.2.2.24.4.16.2.41  2013/12/02 10:29:57  snimmakayala
 *� CASE#:20125764
 *� MHP: PO2TO Conversion.
 *�
 *� Revision 1.2.2.24.4.16.2.40  2013/12/02 10:09:00  snimmakayala
 *� CASE#:20125764
 *� MHP: PO2TO Conversion.
 *�
 *� Revision 1.2.2.24.4.16.2.39  2013/11/29 15:03:19  grao
 *� Case# 20125978  related issue fixes in SB 2014.1
 *�
 *� Revision 1.2.2.24.4.16.2.38  2013/11/25 15:22:23  grao
 *� Case# 20125870  &&  20125921 related issue fixes in SB 2014.1
 *�
 *� Revision 1.2.2.24.4.16.2.37  2013/11/22 14:44:05  grao
 *� Case# 20125870 related issue fixes in SB 2014.1
 *�
 *� Revision 1.2.2.24.4.16.2.36  2013/11/22 14:43:39  grao
 *� Case# 20125870 related issue fixes in SB 2014.1
 *�
 *� Revision 1.2.2.24.4.16.2.35  2013/11/21 13:57:50  skreddy
 *� Case# 20125737
 *� Afosa SB issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.34  2013/11/18 15:00:59  skreddy
 *� Case# 20125737
 *� Afosa SB issue fix
 *�
 *� Revision 1.2.2.24.4.16.2.33  2013/11/15 12:15:57  vmandala
 *� Case# 20125778
 *� undefinedname  boombah  Ffx
 *�
 *� Revision 1.2.2.24.4.16.2.32  2013/10/31 16:57:14  gkalla
 *� Case# 20125414
 *� Item receipt failed for large serial numbers
 *�
 *� Revision 1.2.2.24.4.16.2.31  2013/10/03 13:02:44  rmukkera
 *� Case# 20124743�
 *�
 *� Revision 1.2.2.24.4.16.2.30  2013/09/30 15:58:50  rmukkera
 *� Case#  20124534
 *�
 *� Revision 1.2.2.24.4.16.2.29  2013/09/25 15:49:42  rmukkera
 *� Case# 20124536
 *�
 *� Revision 1.2.2.24.4.16.2.28  2013/09/25 06:54:22  schepuri
 *� LP# is not getting displayed in Item receipt screen.
 *�
 *� Revision 1.2.2.24.4.16.2.27  2013/09/24 14:39:13  grao
 *� SB issue fixes 20124535 fixed
 *�
 *� Revision 1.2.2.24.4.16.2.26  2013/09/23 15:59:35  grao
 *� sb issue fixesL20124532 , 20124533
 *�
 *� Revision 1.2.2.24.4.16.2.25  2013/09/02 07:12:20  schepuri
 *� item Receipt system displayed invalid item status
 *�
 *� Revision 1.2.2.24.4.16.2.24  2013/08/23 07:24:06  schepuri
 *� case no 20122739
 *�
 *� Revision 1.2.2.24.4.16.2.23  2013/08/19 16:12:47  rmukkera
 *� Issue Fix related to 20123915�,20123912�
 *�
 *� Revision 1.2.2.24.4.16.2.22  2013/08/05 17:01:37  skreddy
 *� Case# 20123721
 *� issue rellated to location exception
 *�
 *� Revision 1.2.2.24.4.16.2.21  2013/07/26 21:13:03  grao
 *� Case# 20123592
 *� Issue fixes for Inventory record update with specific item level location(site) in TSG SB
 *�
 *� Revision 1.2.2.24.4.16.2.20  2013/07/04 18:47:16  spendyala
 *� case# 20120121
 *� Issue fixed against the case#201215876
 *�
 *� Revision 1.2.2.24.4.16.2.19  2013/06/24 20:13:33  grao
 *� CASE201112/CR201113/LOG201121
 *� Surftech SB Issues fixes
 *�  Merge Lp issue fixes
 *�
 *� Revision 1.2.2.24.4.16.2.18  2013/06/24 15:21:14  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�  Case# 20123159
 *� Surftech serial#issue
 *�
 *� Revision 1.2.2.24.4.16.2.17  2013/06/11 14:30:41  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.2.2.24.4.16.2.16  2013/06/05 22:11:22  spendyala
 *� CASE201112/CR201113/LOG201121
 *� FIFO field updating with data stamp is removed.
 *�
 *� Revision 1.2.2.24.4.16.2.15  2013/05/16 11:25:03  spendyala
 *� CASE201112/CR201113/LOG201121
 *� InvtRef# field of opentask record will be updated
 *� to putaway task, once inbound process in completed
 *�
 *� Revision 1.2.2.24.4.16.2.14  2013/05/14 14:17:28  schepuri
 *� Surftech issues of serial status update
 *�
 *� Revision 1.2.2.24.4.16.2.13  2013/05/10 11:01:53  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.2.2.24.4.16.2.12  2013/05/10 00:33:08  kavitha
 *� CASE201112/CR201113/LOG201121
 *� Issue fixes
 *�
 *� Revision 1.2.2.24.4.16.2.11  2013/05/01 15:31:46  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.2.2.24.4.16.2.10  2013/04/19 11:55:56  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.2.2.24.4.16.2.9  2013/04/17 16:04:02  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.2.2.24.4.16.2.8  2013/04/03 01:46:05  kavitha
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.2.2.24.4.16.2.7  2013/03/26 13:27:42  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.2.2.24.4.16.2.6  2013/03/19 11:53:20  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production and UAT issue fixes.
 *�
 *� Revision 1.2.2.24.4.16.2.5  2013/03/13 13:57:16  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Time Stamp related changes.
 *�
 *� Revision 1.2.2.24.4.16.2.4  2013/03/08 14:35:02  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Code merged from Endochoice as part of Standard bundle
 *�
 *� Revision 1.2.2.24.4.16.2.3  2013/03/05 13:35:38  rmukkera
 *� Merging of lexjet Bundle files to Standard bundle
 *�
 *� Revision 1.2.2.24.4.16.2.2  2013/03/01 15:01:12  rmukkera
 *� code is merged from FactoryMation production as part of Standard bundle
 *�
 *� Revision 1.2.2.24.4.16.2.1  2013/02/26 13:02:23  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Marged from Boombah.
 *�
 *� Revision 1.2.2.24.4.16  2013/02/19 15:38:50  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Revision 1.2.2.24.4.15  2013/02/07 15:10:34  schepuri
 *� CASE201112/CR201113/LOG201121
 *� disabling ENTER Button func added
 *�
 *� Revision 1.2.2.24.4.14  2013/02/07 08:41:36  skreddy
 *� CASE201112/CR201113/LOG201121
 *�  RF Lot auto generating FIFO enhancement
 *�
 *� Revision 1.2.2.24.4.13  2013/01/25 06:55:04  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue related to wrong location
 *�
 *� Revision 1.2.2.24.4.12  2013/01/08 15:41:28  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Lexjet issue for batch entry irrespective of location
 *�
 *� Revision 1.2.2.24.4.11  2012/12/20 07:48:48  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Merged code form 2012.2 branch.
 *�
 *� Revision 1.2.2.24.4.10  2012/12/12 16:13:37  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Actual Bin management enhancement
 *�
 *� Revision 1.2.2.24.4.9  2012/12/11 14:52:43  schepuri
 *� CASE201112/CR201113/LOG201121
 *� ebiznet inv was not updating according to UOM conversions
 *�
 *� Revision 1.2.2.24.4.8  2012/11/20 22:29:33  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Issue fix for case# 201212441, put confirm qty updating more than order qty
 *�
 *� Revision 1.2.2.24.4.7  2012/11/11 03:40:30  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� GSUSA UAT issue fixes.
 *�
 *� Revision 1.2.2.24.4.6  2012/11/01 14:55:35  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.2.2.24.4.5  2012/10/23 17:08:38  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Merged code form branch.
 *�
 *� Revision 1.2.2.24.4.4  2012/10/01 08:42:59  grao
 *� no message
 *�
 *� Revision 1.2.2.24.4.3  2012/09/27 10:54:36  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.2.2.24.4.2  2012/09/26 12:45:35  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.2.2.24.4.1  2012/09/21 14:57:16  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.2.2.24  2012/09/04 07:18:48  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to creating inventory is resolved.
 *�
 *� Revision 1.2.2.23  2012/09/03 13:45:29  schepuri
 *� CASE201112/CR201113/LOG201121
 *� added date stamp
 *�
 *� Revision 1.2.2.22  2012/08/24 18:13:58  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� NSUOMfield code changes
 *�
 *� Revision 1.2.2.21  2012/08/10 14:44:59  gkalla
 *� CASE201112/CR201113/LOG201121
 *� To create inventory if merged qty greater than pallet qty
 *�
 *� Revision 1.2.2.20  2012/07/30 23:17:32  gkalla
 *� CASE201112/CR201113/LOG201121
 *� NSUOM issue
 *�
 *� Revision 1.2.2.19  2012/07/10 23:15:21  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Alert Mails
 *�
 *� Revision 1.2.2.18  2012/06/26 06:22:23  spendyala
 *� CASE201112/CR201113/LOG201121
 *� While doing Qty exception if qty entered is Zero then we are bypassing Item Receipt.
 *�
 *� Revision 1.2.2.17  2012/06/04 14:51:32  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Missing parameters while passing to Query string.
 *�
 *� Revision 1.2.2.16  2012/05/21 15:05:57  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to Invt creation.
 *�
 *� Revision 1.2.2.15  2012/05/16 13:35:34  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Multi UOM changes
 *�
 *� Revision 1.2.2.14  2012/05/03 07:26:50  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Fixed the issue of sending all the lines to item receipt
 *�
 *� Revision 1.2.2.13  2012/04/30 22:36:43  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Production issue fixes
 *�
 *� Revision 1.2.2.12  2012/04/30 10:00:47  spendyala
 *� CASE201112/CR201113/LOG201121
 *� While Searching of Item in ItemMaster,
 *� 'name' filter is changed to 'nameinternal'
 *�
 *� Revision 1.2.2.11  2012/04/25 21:29:19  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� RF cart Changes
 *�
 *� Revision 1.2.2.10  2012/04/13 23:12:46  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� RF CART PUTAWAY issues.
 *�
 *� Revision 1.2.2.9  2012/04/03 11:07:15  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Added WMS status filter for Merge LP
 *�
 *� Revision 1.2.2.8  2012/04/02 12:46:26  spendyala
 *� CASE201112/CR201113/LOG201121
 *� set true lot Value in our custom records depending upon Rule value.
 *�
 *� Revision 1.2.2.7  2012/03/20 20:42:32  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Moved from Trunk to Branch
 *�
 *� Revision 1.5  2012/03/20 16:15:43  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Open Task to Closed Task movement
 *�
 *� Revision 1.4  2012/03/09 21:52:20  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Added record id in poarray
 *�
 *� Revision 1.3  2012/02/24 00:14:12  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� buildshipunits
 *�
 *� Revision 1.2  2012/02/16 10:35:10  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Added FunctionkeyScript
 *�
 *� Revision 1.1  2012/02/02 08:59:38  rmukkera
 *� CASE201112/CR201113/LOG201121
 *�   cartlp new file
 *�
 *
 ****************************************************************************/


function PutawayLocation(request, response){

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);


	if (request.getMethod() == 'GET') {
		var getRecordCount = request.getParameter('custparam_recordcount');
		//nlapiLogExecution('DEBUG', 'getFetchedLocationId ', getFetchedLocation);

		var getconfirmedLPCount = request.getParameter('custparam_confirmedLpCount');
		var getlpCount = request.getParameter('custparam_lpCount');
		var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
		nlapiLogExecution('DEBUG', 'confirmedLPCount ', getconfirmedLPCount);
		nlapiLogExecution('DEBUG', 'lpCount ', getlpCount);
		nlapiLogExecution('DEBUG', 'TempLPNoArray ', TempLPNoArray);
		//	Get the LP#, Quantity, Location 
		//  from the previous screen, which is passed as a parameter	
		var getLPNo = request.getParameter('custparam_lpno');
		var getQuantity = request.getParameter('custparam_quantity');
		var getFetchedLocation = request.getParameter('custparam_beginlocation');
		var getFetchedItem = request.getParameter('custparam_item');
		var getFetchedItemText = request.getParameter('custparam_itemtext');
		var getFetchedItemDescription = request.getParameter('custparam_itemDescription');
		var getFetchedLocationId = request.getParameter('custparam_location');
		var getPONo = request.getParameter('custparam_pono');
		var getLineNo = request.getParameter('custparam_lineno');

		var getRecordId = request.getParameter('custparam_recordid');

		var getCartLPNo = request.getParameter('custparam_cartno');
		nlapiLogExecution('DEBUG', 'getCartLPNo ', getCartLPNo);
		nlapiLogExecution('DEBUG', 'getFetchedItem ', getFetchedItem);

		var getOptedField = request.getParameter('custparam_option');
		//case# 20148231 starts
		var vremqty=request.getParameter('custparam_remainingqty');
		nlapiLogExecution('DEBUG', 'vremqty',vremqty);
		//case# 20148231 ends
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INGRESAR / ESCANEAR UBICACI&#211;N";
			st2 = "ART&#205;CULO";
			st3 = "PLACA";
			st4 = "CONT";
			st5 = "ANTERIOR";
			st6 = "IR A PUNTO DE:";


		}
		else
		{
			st0 = "";
			st1 = "ENTER/SCAN LOCATION ";
			st2 = "ITEM";
			st3 = "LP";
			st4 = "CONT";
			st5 = "PREV";
			st6 = "GO TO LOCATION:";

		}



		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";		
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st6 + " <label>" + getFetchedLocation + "</label>";
		html = html + "				<input type='hidden' name='hdnLPNo' value=" + getLPNo + "></td>";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocation' value='" + getFetchedLocation + "'></td>";// Case# 20148450
		html = html + "				<input type='hidden' name='hdnFetchedItem' value='" + getFetchedItem + "'></td>";//Case# 20148450
		html = html + "				<input type='hidden' name='hdnFetchedItemDescription' value='" + getFetchedItemDescription + "'></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItemText' value='" + getFetchedItemText + "'></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocationId' value=" + getFetchedLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnRecordCount' value=" + getRecordCount + "></td>";
		html = html + "				<input type='hidden' name='hdnconfirmedLPCount' value=" + getconfirmedLPCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpCount' value=" + getlpCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpNumbersArray' value=" + TempLPNoArray + "></td>";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + "></td>";
		html = html + "				<input type='hidden' name='hdnCartLPNo' value='" + getCartLPNo + "'>";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnLineNo' value=" + getLineNo + ">";
		html = html + "				<input type='hidden' name='hdnPONo' value=" + getPONo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnremQuantity' value=" + vremqty + ">";//case# 20148231
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " : <label>" + getFetchedItemText + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 +  " : <label>" + getLPNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" +  st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st5 +  "  <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'INTO Response');

		// This variable is to hold the LP entered.
		var getLocation = request.getParameter('enterlocation');
		nlapiLogExecution('DEBUG', 'getLocation', getLocation);

		var getLPNo = request.getParameter('hdnLPNo');
		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);

		var getActQuantity = request.getParameter('custparam_quantity');
		nlapiLogExecution('DEBUG', 'getActQuantity', getActQuantity);

		var exceptionqty=request.getParameter('custparam_exceptionquantity');
		nlapiLogExecution('DEBUG', 'exceptionqty', exceptionqty);

		var exceptionqtyflag=request.getParameter('custparam_exceptionQuantityflag');
		nlapiLogExecution('DEBUG', 'exceptionqtyflag', exceptionqtyflag);
		//case# 20148231 starts
		var vremqty=request.getParameter('hdnremQuantity');
		//case# 20148231 ends
		getRecordCount = request.getParameter('hdnRecordCount');
		nlapiLogExecution('DEBUG', 'getRecordCount', getRecordCount);

		getRecordId = request.getParameter('hdnRecordId');
		nlapiLogExecution('DEBUG', 'getRecordId', getRecordId);

		var getFetchedLocation=request.getParameter('hdnFetchedLocation');
		nlapiLogExecution('DEBUG', 'getFetchedLocation', getFetchedLocation);
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st6;
		if( getLanguage == 'es_ES')
		{
			st6 = "UBICACI&#211;N NO V&#193;LIDA";

		}
		else
		{
			st6 = "INVALID LOCATION";

		}


		var getOptedField = request.getParameter('hdnOptedField');
		nlapiLogExecution('DEBUG', 'getOptedField ', getOptedField);
		POarray["custparam_option"] = getOptedField;
		POarray["custparam_recordid"] = getRecordId;
		POarray["custparam_lpno"] = getLPNo;
		POarray["custparam_recordcount"] = getRecordCount;
		POarray["custparam_exceptionquantity"] = exceptionqty;
		POarray["custparam_exceptionQuantityflag"] = exceptionqtyflag;

		var filters = new Array();
		// 2 - Location Assigned, 6 - No Location Assigned
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6])); 
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_lpno', null, 'is', getLPNo));
		if(getRecordId!=null && getRecordId!='' && getRecordId!='null' )
			filters.push(new nlobjSearchFilter('internalid', null, 'is', getRecordId));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lpno');
		columns[1] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[3] = new nlobjSearchColumn('custrecord_sku');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[5] = new nlobjSearchColumn('custrecord_skudesc');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[7] = new nlobjSearchColumn('custrecord_line_no');
		columns[8] = new nlobjSearchColumn('custrecord_sku_status');
		columns[9] = new nlobjSearchColumn('custrecord_packcode');
		columns[10] = new nlobjSearchColumn('custrecord_batch_no');
		columns[11] = new nlobjSearchColumn('custrecord_wms_location');
		columns[12] = new nlobjSearchColumn('custrecord_ebizmethod_no');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		POarray["custparam_error"] = st6;
		POarray["custparam_screenno"] = 'CRT11';
		nlapiLogExecution('DEBUG', 'Screen #', POarray["custparam_screenno"]);

		POarray["custparam_cartno"] = request.getParameter('hdnCartLPNo');
		getCartLPNo = request.getParameter('hdnCartLPNo');
		nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);

		POarray["custparam_beginlocation"] = request.getParameter('custparam_beginlocation');
		POarray["custparam_item"] = request.getParameter('hdnFetchedItem');
		POarray["custparam_itemtext"] = request.getParameter('hdnFetchedItemText');
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

				if (optedEvent == 'F7') {
					//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayloc', 'customdeploy_ebiz_rf_cart_putawayloc_di', false, POarray);
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
					return;
				}
				else {
					// try {
					if (getLocation != null&&getLocation!="" && getFetchedLocation!=null && getFetchedLocation!='' && getFetchedLocation.trim()==getLocation.trim()) 
					{
						if (searchresults != null && searchresults.length > 0) {
							nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');

							var getLPId = searchresults[0].getId();
							nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);

							POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
							POarray["custparam_quantity"] = searchresults[0].getValue('custrecord_expe_qty');
							POarray["custparam_location"] = searchresults[0].getValue('custrecord_actbeginloc');
							POarray["custparam_item"] = searchresults[0].getValue('custrecord_sku');
							POarray["custparam_itemid"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
							POarray["custparam_itemDescription"] = searchresults[0].getValue('custrecord_skudesc');
							POarray["custparam_pointernalid"] = searchresults[0].getValue('custrecord_ebiz_cntrl_no');
							POarray["custparam_polinenumber"] = searchresults[0].getValue('custrecord_line_no');
							POarray["custparam_poitemstatus"] = searchresults[0].getValue('custrecord_sku_status');
							POarray["custparam_popackcode"] = searchresults[0].getValue('custrecord_packcode');
							POarray["custparam_polotbatchno"] = searchresults[0].getValue('custrecord_batch_no');
							POarray["custparam_pono"] = request.getParameter('hdnPONo');
							POarray["custparam_cartno"] = searchresults[0].getValue('custrecord_transport_lp');

							POarray["custparam_lpNumbersArray"] = request.getParameter('hdnlpNumbersArray');
							POarray["custparam_confirmedLpCount"] = request.getParameter('hdnconfirmedLPCount');
							POarray["custparam_lpCount"] = request.getParameter('hdnlpCount');
							POarray["custparam_itemtext"] = request.getParameter('hdnFetchedItemText');
							POarray["custparam_recordid"] = getLPId;
							nlapiLogExecution('DEBUG', 'getLPId', getLPId);
							var whloc=searchresults[0].getValue('custrecord_wms_location');
							var methodid=searchresults[0].getValue('custrecord_ebizmethod_no');

							nlapiLogExecution('DEBUG', 'geteBizControlNo', POarray["custparam_pointernalid"]);
							nlapiLogExecution('DEBUG', 'geteBizSKUNo', POarray["custparam_itemid"]);
							nlapiLogExecution('DEBUG', 'itemStatus', POarray["custparam_poitemstatus"]);
							nlapiLogExecution('DEBUG', 'custparam_confirmedLpCount', POarray["custparam_confirmedLpCount"]);
							nlapiLogExecution('DEBUG', 'custparam_lpCount', POarray["custparam_lpCount"]);
							nlapiLogExecution('DEBUG', 'custparam_optedEvent', POarray["custparam_optedEvent"]);

//							if (getOptedField == 4) {
							POarray["custparam_location"] = getLocation;
							//}
							nlapiLogExecution('ERROR', 'getLocation', getLocation);
//							var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getLocation),new nlobjSearchColumn('name'));
							// case# 201417281
							/*var filter=new Array();
							filter.push(new nlobjSearchFilter('name', null, 'is', getLocation));
							filter.push(new nlobjSearchFilter('isinactive', null, 'is', "F"));
							var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, filter,new nlobjSearchColumn('name'));*/

							var filters = new Array();					
							filters[0] = new nlobjSearchFilter('name', null, 'is', getLocation.toUpperCase());
							if(whloc!=null && whloc!='')
								filters[1] = new nlobjSearchFilter('custrecord_ebizsitelocf',null, 'anyof',[whloc]);
							filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));
							filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

							var columns = new Array();
							columns.push(new nlobjSearchColumn('name'));
							var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null,filters,columns);

							if (BinLocationSearch != null && BinLocationSearch != "")
							{
								POarray["custparam_beginlocation"] = BinLocationSearch[0].getValue('name');
								nlapiLogExecution('DEBUG', 'Location Name is', POarray["custparam_beginlocation"]);
								POarray["custparam_beginlocationinternalid"] = BinLocationSearch[0].getId();
								nlapiLogExecution('DEBUG', 'Begin Location Internal Id', POarray["custparam_beginlocationinternalid"]);

								var getBeginLocationInternalId = POarray["custparam_beginlocationinternalid"];

//								if (getOptedField != 4) {
//								if (getLocation != POarray["custparam_beginlocation"]) {
//								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
//								nlapiLogExecution('DEBUG', 'Entered Bin Location', 'Does not match with the actual bin location');
//								}
//								}
								// case# 201417281
								//var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getLocation));

								var filters = new Array();					
								filters[0] = new nlobjSearchFilter('name', null, 'is', getLocation.toUpperCase());
								if(whloc!=null && whloc!='')
									filters[1] = new nlobjSearchFilter('custrecord_ebizsitelocf',null, 'anyof',[whloc]);
								filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));
								filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

								var columns = new Array();
								columns.push(new nlobjSearchColumn('name'));
								var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null,filters,columns);

								if (LocationSearch!=null && LocationSearch.length != 0) {
									nlapiLogExecution('DEBUG', 'Length of Location Search', LocationSearch.length);

									for (var s = 0; s < LocationSearch.length; s++) {
										var EndLocationId = LocationSearch[s].getId();
										nlapiLogExecution('DEBUG', 'End Location Id', EndLocationId);
									}
								}
								var trantype = nlapiLookupField('transaction', POarray["custparam_pointernalid"], 'recordType');

								var qty;
								if(exceptionqtyflag=='true')
								{
									qty=exceptionqty;;
								}
								else
									qty=POarray["custparam_quantity"];
								var vputwrecid=-1;
								var vConfirmStatus=rf_confirmputaway(POarray["custparam_pointernalid"], POarray["custparam_polinenumber"], POarray["custparam_itemid"], POarray["custparam_itemDescription"], POarray["custparam_poitemstatus"], POarray["custparam_popackcode"], qty, getLocation, getBeginLocationInternalId, POarray["custparam_lpno"], EndLocationId,exceptionqty, getRecordId,trantype,POarray["custparam_polotbatchno"],whloc,methodid,exceptionqtyflag,response);
								//                        rf_confirmputaway(PORec.getFieldValue('custrecord_ebiz_cntrl_no'), POarray["custparam_polinenumber"], PORec.getFieldValue('custrecord_ebiz_sku_no'), PORec.getFieldValue('custrecord_skudesc'), PORec.getFieldValue('custrecord_sku_status'), PORec.getFieldValue('custrecord_packcode'), PORec.getFieldValue('custrecord_expe_qty'), getLocation, getBeginLocationInternalId, POarray["custparam_lpno"], EndLocationId, getActQuantity, getRecordId);
								if(vConfirmStatus!= null && vConfirmStatus != '' && vConfirmStatus != 'DEBUG')
								{
									TrnLineUpdation(trantype, 'PUTW', POarray["custparam_pono"], 
											POarray["custparam_pointernalid"], POarray["custparam_polinenumber"], 
											POarray["custparam_itemid"], null, qty,"",
											POarray["custparam_poitemstatus"]);

									nlapiLogExecution('DEBUG', 'getCartLPNo after confirming', getCartLPNo);
									var Remqty=0;
									if(exceptionqtyflag=='true')
									{
										itemDimensions = getSKUCubeAndWeight(POarray["custparam_itemid"],"");
										var itemCube = itemDimensions[0];
										//case# 20148231 starts
										//Remqty=parseFloat(POarray["custparam_quantity"])-parseFloat(qty);
										Remqty=vremqty;
										nlapiLogExecution('DEBUG', 'itemCube ', itemCube);	
										nlapiLogExecution('DEBUG', 'Remqty ', Remqty);
										//case# 20148231 ends
										var TotalItemCube = parseFloat(itemCube) * parseFloat(Remqty);
										nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );						

										var binLocationRemainingCube = GeteLocCube(getBeginLocationInternalId);
										nlapiLogExecution('DEBUG', 'binLocationRemainingCube ', binLocationRemainingCube );
										var remainingCube = parseFloat(binLocationRemainingCube) + parseFloat(TotalItemCube);
										UpdateLocCube(getBeginLocationInternalId, parseFloat(remainingCube));

										nlapiLogExecution('DEBUG', 'remainingCube ', remainingCube );



									}
									if (getRecordCount > 1) {
										var filters = new Array();

										// 2 - Location Assigned, 6 - No Location Assigned
										filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]));
										filters.push(new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo));

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
										columns[1] = new nlobjSearchColumn('custrecord_lpno');

										var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

										if (searchresults != null && searchresults.length > 0) {
											nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');
											POarray["custparam_cartno"] = searchresults[0].getValue('custrecord_transport_lp');
											POarray["custparam_lpno"] = getLPNo;
											POarray["custparam_recordcount"] = searchresults.length;

											if (POarray["custparam_beginlocation"] == null) {
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaysku', 'customdeploy_ebiz_rf_cart_putawaysku_di', false, POarray);
												nlapiLogExecution('DEBUG', 'Back to Putaway SKU', 'Record count is not zero');
											}
											else {
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
												nlapiLogExecution('DEBUG', 'Back to Putaway confirmation', 'Record count is not zero');
											}
										}
										else {
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											nlapiLogExecution('DEBUG', 'Here the Search Results ', 'Length is null');
										}
									}						
									else {
										var filtersmlp = new Array(); 
										filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartno'));

										var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

										if (SrchRecord != null && SrchRecord.length > 0) {

											nlapiSubmitField('customrecord_ebiznet_master_lp', SrchRecord[0].getId(), 'custrecord_ebiz_cart_closeflag', 'F'); //	2 UNITS
//											var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
//											rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'F');
//											var recid = nlapiSubmitRecord(rec, false, true);
										}
										else {
											nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

										}

										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaycmplete', 'customdeploy_ebiz_rf_cart_putawaycmplete', false, POarray);
									}

									/*
									 * 	This is to delete the check-in transactions from the inventory record while putaway confirmation. 
									 */						
//									DeleteInvtRecCreatedforCHKNTask(POarray["custparam_pointernalid"], POarray["custparam_lpno"]);
									DeleteInvtRecCreatedforCHKNTask(POarray["custparam_pointernalid"], getLPNo);
								}
								else
								{
									POarray["custparam_error"] = 'CART PUTAWAY FAILED';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									nlapiLogExecution('DEBUG', 'CART Putaway Failed', 'CART Putaway Failed');
								}
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Entered Bin Location', 'Scanned bin location is wrong');
							}
						}
						else {
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'Here the Search Results ', 'Length is null');
						}
					}
					else {
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Location is not entered / scanned', getLocation);
					}

					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			POarray["custparam_screenno"] = 'CRT9';
			POarray["custparam_error"] = 'LOCATION ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		}
	}
}

function rf_confirmputaway(pointid, linenum, itemid, itemdesc, itemstatus, itempackcode, quantityineach, binlocationid, getBeginLocationInternalId, invtlp, EndLocationId, ActQuantity, RecordId,trantype,batchno,whloc,methodid,exceptionqtyflag,response){
	nlapiLogExecution('DEBUG', 'inside rf_confirmputaway', 'Success');
	//Creating Item Receipt.
	var fromrecord;
	var fromid;
	var torecord;
	var qty;
	var vputwrecid=-1;
	var tempserialId = "";
	nlapiLogExecution('DEBUG', 'binlocationid is', binlocationid);
	nlapiLogExecution('DEBUG', 'Actual Quantity is', ActQuantity);
	nlapiLogExecution('DEBUG', 'binlocation internalid is', getBeginLocationInternalId);
	nlapiLogExecution('DEBUG', 'trantype', trantype);
	//To get WH Location.
	var whLocation= '';
	var serialnumcsv = "";
	if(trantype!='transferorder')
	{
		whLocation= nlapiLookupField('transaction',pointid,'location');
	}
	else
	{
		//whLocation= nlapiLookupField('transferorder',pointid,'transferlocation');

		/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/

		var columns= nlapiLookupField('transferorder',pointid,[ 'transferlocation','department']);
		if(columns != null && columns != '')
		{
			whLocation = columns.transferlocation;
			vdepartment = columns.department;


			nlapiLogExecution('DEBUG', 'whLocation internalid in TO', whLocation);
			nlapiLogExecution('DEBUG', 'vdepartment internalid in TO', vdepartment);
			//nlapiLogExecution('DEBUG', 'vunits internalid in TO', vunits);
		}
		/*** up to here ***/
	}
	nlapiLogExecution('DEBUG', 'whLocation internalid is', whLocation);
	var templocation=whLocation;
	fromrecord = trantype;
	var qtyexceptionFlag;
	fromid = pointid; // Transform PO with ID = 26 ;
	torecord = 'itemreceipt'; // Transform a record with given id to a different record type.
	var Itype = "";
	/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
	var TranferInvFlag = 'F';
	var getScannedSerialNoinQtyexception;
	/*** up to here ***/
	// Get the object of the transformed record.       
	try {
		//case 20125737 start 
		//case# 20149358 starts
		//var systemRule= GetSystemRuleForPostItemReceiptby();
		var systemRule= GetSystemRuleForPostItemReceiptby(templocation);
		//case# 20149358 ends
		if(systemRule=='LP')
		{
			idl=generateItemReceipt(fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,invtlp,systemRule,RecordId,EndLocationId,batchno,exceptionqtyflag,templocation,getScannedSerialNoinQtyexception,quantityineach,response);
		}
		else
		{
			if(systemRule=='PO')
			{
				idl='-1';
			}
		}
		//case 20125737 end
		//case no start 20126856� 

		quantity=quantityineach;
		//case no end 20126856� 
		/*
		 * //if(parseFloat(quantity)!=0){
		var inventorytransfer;
		if(parseFloat(quantityineach)!=0){
			var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
			var polinelength = trecord.getLineItemCount('item');
			nlapiLogExecution('DEBUG', "polinelength", polinelength);
			var vAdvBinManagement=false;

			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('DEBUG', 'vAdvBinManagement', vAdvBinManagement);
			for (var j = 1; j <= polinelength; j++) {
				//nlapiLogExecution('DEBUG', "Get Line Value" + trecord.getLineItemValue('item', 'line', j));

				var item_id = trecord.getLineItemValue('item', 'item', j);
				var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
				var itemLineNo = trecord.getLineItemValue('item', 'line', j);
				//var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', j);
				var poItemUOM = trecord.getLineItemValue('item', 'units', j);
				var commitflag = 'N';
				var str = 'PO Item Line No. = ' + itemLineNo + '<br>';
				str = str + 'Task linenum. = ' + linenum + '<br>';	
				str = str + 'Transaction Type. = ' + trantype + '<br>';	
				str = str + 'poItemUOM. = ' + poItemUOM + '<br>';	

				nlapiLogExecution('DEBUG', 'Line Details', str);

				if(trantype!='transferorder')
				{
					if (itemLineNo == linenum) {

						whLocation=trecord.getLineItemValue('item', 'location', j);//value
						if(whLocation==null||whLocation=="")
							whLocation=templocation;

						if(poItemUOM!=null && poItemUOM!='')
						{
							var vbaseuomqty=0;
							var vuomqty=0;
							var eBizItemDims=geteBizItemDimensions(item_id);
							if(eBizItemDims!=null&&eBizItemDims.length>0)
							{
								nlapiLogExecution('DEBUG', 'Dims Length', eBizItemDims.length);
								for(z=0; z < eBizItemDims.length; z++)
								{
									if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
									}
									nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
									if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
									{
										vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
									}
								}
								if(vuomqty==null || vuomqty=='')
								{
									vuomqty=vbaseuomqty;
								}
								nlapiLogExecution('DEBUG', 'vbaseuomqty', vbaseuomqty);
								nlapiLogExecution('DEBUG', 'vuomqty', vuomqty);
								nlapiLogExecution('DEBUG', 'quantity tstbefore', quantityineach);

								if(quantityineach==null || quantityineach=='' || isNaN(quantityineach))
									quantity=0;
								else
									quantity = (parseFloat(quantityineach)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

								nlapiLogExecution('DEBUG', 'quantity tstafter', quantity);
							}
						}

						//Code Added by Ramana
						var varItemlocation;
						nlapiLogExecution('DEBUG', 'itemstatus', itemstatus);
						varItemlocation = getItemStatusMapLoc(itemstatus);
						//upto to here

						commitflag = 'Y';

						//Itype = nlapiLookupField('item', itemid, 'recordType');
						var columns = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
						if(columns != null && columns != '')
						{
							Itype = columns.recordType;
							serialInflg = columns.custitem_ebizserialin;
							nlapiLogExecution('DEBUG', 'Itype', Itype);
							nlapiLogExecution('DEBUG', 'columns', columns);
						}

						nlapiLogExecution('DEBUG', 'Value of J', j);
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', quantity);

						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

						nlapiLogExecution('DEBUG', 'Serialquantity', quantity);
						nlapiLogExecution('DEBUG', 'Into SerialNos');
						//if (Itype == "serializedinventoryitem") {
						//if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") {
						if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem") {
							nlapiLogExecution('DEBUG', 'Into SerialNos');
							nlapiLogExecution('DEBUG', 'invtlp', invtlp);					
							var serialline = new Array();
							var serialId = new Array();

							var tempSerial = "";
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							filters[3] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'doesnotstartwith', 'H');

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
							//var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

							if (serchRec) {
								for (var n = 0; n < serchRec.length; n++) {
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');
									}
									else {
										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

									}
									if(vAdvBinManagement && (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem"))
									{
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										// case no 20126278
										//compSubRecord.commit();
									}
								}

                                                                   if(vAdvBinManagement)
									compSubRecord.commit();
								serialnumcsv = tempSerial;
								tempSerial = "";
							}

							nlapiLogExecution('DEBUG', 'Serial nums assigned', serialnumcsv);
							nlapiLogExecution('DEBUG', 'Serial nums tempserialId', tempserialId);
							if(!vAdvBinManagement && (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem"))
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);



						}
						else 
							if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {

							nlapiLogExecution('DEBUG', 'Into LOT/Batch');
							nlapiLogExecution('DEBUG', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";
							var batfilter = new Array();
							batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
							batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
							batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
							batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
							batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);
							var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, new nlobjSearchColumn('custrecord_lotnowithquantity'));
							if (batchsearchresults) {
								for (var n = 0; n < batchsearchresults.length; n++) {
									//This is for Batch num loopin with Space separated.
									if (tempBatch == "") {
										tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
									}
									else {
										tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');

									}
								}

								batchcsv = tempBatch;
								tempBatch = "";
							}
							nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned', batchcsv);
							trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);



								nlapiLogExecution('DEBUG', 'Into LOT/Batch');
								nlapiLogExecution('DEBUG', 'LP:', invtlp);
								var tempBatch = "";
								var batchcsv = "";

								var confirmLotToNS='Y';

								confirmLotToNS=GetConfirmLotToNS(varItemlocation);

								nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);


								var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								 The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle								
								vatColumns[2] = new nlobjSearchColumn('custrecord_batch_no');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										nlapiLogExecution('DEBUG', 'Inside For:', 'Inside batchresults');
										if (tempBatch == "") {
											var vItemname;
											 The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle
											var tempbatchnew;
											var vItQty;     //Line Added by Ganapathi on 7th Dec 2012
											 Up to here  
											if(confirmLotToNS == 'Y')
											{	
												nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												 The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle												
												tempbatchnew = batchsearchresults[n].getValue('custrecord_batch_no');
												//  Code Added by Ganapathi on 7th Dec 2012
												vItQty = batchsearchresults[n].getValue('custrecord_expe_qty');
												nlapiLogExecution('DEBUG', 'vItQty:', vItQty);
												nlapiLogExecution('DEBUG', 'tempBatch:', tempBatch);
												// case 20123720 start 
												var expdate1=getLotExpdate(tempbatchnew,item_id);
												if(expdate1 !=null && expdate1 !='')
												{
													trecord.setCurrentLineItemValue('item', 'expirationdate', expdate1);

												}//end

												// End of the Code Added by Ganapathi on 7th Dec 2012
												 Up to here  
											}	
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();
												columns[0] = new nlobjSearchColumn('itemid');

												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												nlapiLogExecution('DEBUG','itemdetails',itemdetails);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												 The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle												
												if(vItQty !=quantity)
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);	
												 Up to here  
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
												nlapiLogExecution('DEBUG', 'compSubRecord:', compSubRecord);
											}
										}
										else {
											var vItemname;
											 The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle	
											var vItQty;                   //Line Added by Ganapathi on 7th Dec 2012
											if(confirmLotToNS == 'Y'){
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												//  Code Added by Ganapathi on 7th Dec 2012
												vItQty = batchsearchresults[n].getValue('custrecord_expe_qty');
												nlapiLogExecution('DEBUG', 'else vItQty:', vItQty);
												// End of the Code Added by Ganapathi on 7th Dec 2012
												 Up to here  
											}
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');

												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempBatch);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}
										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}
								var vItemname;
								if(confirmLotToNS == 'Y')
									tempBatch = batchno + '('+ quantity + ')';
								else
								{
									nlapiLogExecution('DEBUG', 'item_id:', item_id);
									var filters1 = new Array();          
									filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
									var columns = new Array();

									columns[0] = new nlobjSearchColumn('itemid');

									var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
									if (itemdetails !=null) 
									{
										vItemname=itemdetails[0].getValue('itemid');
										nlapiLogExecution('DEBUG', 'vItemname:', vItemname);
										//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
										vItemname=vItemname.replace(/ /g,"-");
										tempBatch=vItemname + '('+ quantity + ')';


									}
								}
								if(vAdvBinManagement)											 
								{
									var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
									compSubRecord.selectNewLineItem('inventoryassignment');
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
									if(confirmLotToNS == 'Y')
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
									else
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
									compSubRecord.commitLineItem('inventoryassignment');
									compSubRecord.commit();
								}
								batchcsv = tempBatch;
								nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned new', batchcsv);
								if(!vAdvBinManagement)
								{
									trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
								}

							}
								// case 20122739 start 
						trecord.setCurrentLineItemValue('item', 'custcol_lp', invtlp);
						//end
						trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
						trecord.commitLineItem('item');
					}
				}
				else
				{
					nlapiLogExecution('DEBUG', 'itemid (Task)', itemid);	
					nlapiLogExecution('DEBUG', 'item_id (Line)', item_id);	

					itemLineNo=parseFloat(itemLineNo)-2;
					if (itemid == item_id && itemLineNo == linenum) {
		 *//*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***//*
						//UOM conversions
						if(poItemUOM!=null && poItemUOM!='')
						{
							var vbaseuomqty=0;
							var vuomqty=0;
							var eBizItemDims=geteBizItemDimensions(item_id);
							if(eBizItemDims!=null&&eBizItemDims.length>0)
							{
								nlapiLogExecution('DEBUG', 'Dims Length', eBizItemDims.length);
								for(z=0; z < eBizItemDims.length; z++)
								{
									if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
									}
									nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
									if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
									{
										vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
									}
								}
								if(vuomqty==null || vuomqty=='')
								{
									vuomqty=vbaseuomqty;
								}
								nlapiLogExecution('DEBUG', 'vbaseuomqty', vbaseuomqty);
								nlapiLogExecution('DEBUG', 'vuomqty', vuomqty);
								nlapiLogExecution('DEBUG', 'quantity tstbefore', quantityineach);

								if(quantityineach==null || quantityineach=='' || isNaN(quantityineach))
									quantity=0;
								else
									quantity = (parseFloat(quantityineach)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

								nlapiLogExecution('DEBUG', 'quantity tstafter', quantity);
							}
						}

		  *//*** up to here ***//*

						commitflag = 'Y';

						var varItemlocation;				
						varItemlocation = getItemStatusMapLoc(itemstatus);

						//Itype = nlapiLookupField('item', itemid, 'recordType');
						var columns = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
						if(columns != null && columns != '')
						{
							Itype = columns.recordType;
							serialInflg = columns.custitem_ebizserialin;
							nlapiLogExecution('DEBUG', 'Itype', Itype);
							nlapiLogExecution('DEBUG', 'columns', columns);
						}

						nlapiLogExecution('DEBUG', 'Value of J', j);
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', quantity);
						if(trantype!='transferorder')
							trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

		   *//*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***//*
						nlapiLogExecution('DEBUG', 'whLocation tst ', whLocation);
						nlapiLogExecution('DEBUG', 'itemstatus tst', itemstatus);

						if(whLocation == varItemlocation)
						{
							trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
						}
						else
						{
							TranferInvFlag = 'T';
							trecord.setCurrentLineItemValue('item', 'location', whLocation);
						}
		    *//*** up to here ***//*
						trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
						nlapiLogExecution('DEBUG', 'Serialquantity', quantity);
						nlapiLogExecution('DEBUG', 'Into SerialNos');
						//if (Itype == "serializedinventoryitem" ) {
						//if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") {
						if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem") {
							nlapiLogExecution('DEBUG', 'Into SerialNos');
							nlapiLogExecution('DEBUG', 'invtlp', invtlp);					
							var serialline = new Array();
							var serialId = new Array();

							var tempSerial = "";
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']);
							filters[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
							//var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

							if (serchRec) {
								for (var n = 0; n < serchRec.length; n++) {
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');
									}
									else {
										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

									}
									if(vAdvBinManagement && (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem"))
									{
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										compSubRecord.commit();
									}
								}

								serialnumcsv = tempSerial;
								tempSerial = "";
							}

							nlapiLogExecution('DEBUG', 'Serial nums assigned', serialnumcsv);
							nlapiLogExecution('DEBUG', 'Serial nums tempserialId', tempserialId);
							if(!vAdvBinManagement && (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem"))
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);


						}
						else 
							if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {

						nlapiLogExecution('DEBUG', 'Into LOT/Batch');
						nlapiLogExecution('DEBUG', 'LP:', invtlp);
						var tempBatch = "";
						var batchcsv = "";
						var batfilter = new Array();
						batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
						batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
						batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
						batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
						batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);
						var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, new nlobjSearchColumn('custrecord_lotnowithquantity'));
						if (batchsearchresults) {
							for (var n = 0; n < batchsearchresults.length; n++) {
								//This is for Batch num loopin with Space separated.
								if (tempBatch == "") {
									tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
								}
								else {
									tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');

								}
							}

							batchcsv = tempBatch;
							tempBatch = "";
						}
						nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned', batchcsv);
						trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);


								nlapiLogExecution('DEBUG', 'Into LOT/Batch');
								nlapiLogExecution('DEBUG', 'LP:', invtlp);
								var tempBatch = "";
								var batchcsv = "";

								var confirmLotToNS='Y';

								confirmLotToNS=GetConfirmLotToNS(varItemlocation);

								nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);


								var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								 The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle									
								vatColumns[2] = new nlobjSearchColumn('custrecord_batch_no');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										 The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle	
										var vItemname;
										if (tempBatch == "") {
											 The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle	
											var vtempbatchnew;
											var vItQty;
											 Up to here  
											if(confirmLotToNS == 'Y')
											{
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												 The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle	
												vtempbatchnew = batchsearchresults[n].getValue('custrecord_batch_no');
												//  Code Added by Ganapathi on 7th Dec 2012
												vItQty = batchsearchresults[n].getValue('custrecord_expe_qty');
												nlapiLogExecution('DEBUG', 'else vItQty:', vItQty);
												// End of the Code Added by Ganapathi on 7th Dec 2012
												 Up to here  
											}
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';


												}
											}
											if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vtempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}
										}
										else {
											var vItemname;
											var vItQty;
											if(confirmLotToNS == 'Y')
											{	
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												vtempbatchnew = batchsearchresults[n].getValue('custrecord_batch_no');
												//  Code Added by Ganapathi on 7th Dec 2012
												vItQty = batchsearchresults[n].getValue('custrecord_expe_qty');
												nlapiLogExecution('DEBUG', 'else vItQty:', vItQty);
												// End of the Code Added by Ganapathi on 7th Dec 2012
											}	
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vtempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}
										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}


								if(confirmLotToNS == 'Y')
									tempBatch = batchno + '('+ quantity + ')';
								else
								{
									nlapiLogExecution('DEBUG', 'item_id:', item_id);
									var filters1 = new Array();          
									filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
									var columns = new Array();

									columns[0] = new nlobjSearchColumn('itemid');
									var vItemname;
									var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
									if (itemdetails !=null) 
									{
										vItemname=itemdetails[0].getValue('itemid');
										nlapiLogExecution('DEBUG', 'vItemname:', vItemname);
										//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
										vItemname=vItemname.replace(/ /g,"-");
										tempBatch=vItemname + '('+ quantity + ')';


									}
								}
								batchcsv = tempBatch;
								nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned', batchcsv);
								if(!vAdvBinManagement)
									trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							}
						trecord.setCurrentLineItemValue('item', 'custcol_lp', invtlp);
						nlapiLogExecution('DEBUG', 'itemstatus insert new', itemstatus);
						trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
						trecord.commitLineItem('item');
		     *//*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***//*
						if(TranferInvFlag == 'T')
							inventorytransfer = InvokeNSInventoryTransfer(itemid,itemstatus,whLocation,varItemlocation,quantity,batchno,vdepartment,poItemUOM);
		      *//*** up to here ***//*
					}
				}
				if (commitflag == 'N') {
					nlapiLogExecution('DEBUG', 'commitflag is N', commitflag);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
					trecord.commitLineItem('item');
				}
			}
		}
		nlapiLogExecution('ERROR', 'Before Item Receipt---', 'Before');
		//Case 20125778  start         
		var idl='';
		//Case 20125778 end      
		if (parseInt(quantity)>0)
		 idl = nlapiSubmitRecord(trecord);
		nlapiLogExecution('ERROR', 'Item Receipt idl', idl);*/

		if (idl != null||parseInt(quantity)==0) {
			nlapiLogExecution('ERROR', 'RecordId', RecordId);
			nlapiLogExecution('ERROR', 'pointid', pointid);

			if(idl != null && idl != '' && idl != '-1')
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_ebiz_nsconfirm_ref_no', idl);

			var vbatchno='';
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid));
			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '2'));
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]));
			filters.push(new nlobjSearchFilter('internalid', null, 'is', RecordId));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_actendloc');
			columns[2] = new nlobjSearchColumn('custrecordact_begin_date');
			columns[3] = new nlobjSearchColumn('custrecord_act_end_date');
			columns[4] = new nlobjSearchColumn('custrecord_wms_status_flag');
			columns[5] = new nlobjSearchColumn('custrecord_actualbegintime');
			columns[6] = new nlobjSearchColumn('custrecord_actualendtime');
			columns[7] = new nlobjSearchColumn('custrecord_upd_date');
			columns[8] = new nlobjSearchColumn('custrecord_recordupdatetime');
			columns[9] = new nlobjSearchColumn('custrecord_batch_no');
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[11] = new nlobjSearchColumn('custrecord_sku_status');
			columns[12] = new nlobjSearchColumn('custrecord_packcode');
			columns[13] = new nlobjSearchColumn('custrecord_ebiz_lpno');
			columns[14] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[15] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
			columns[16] = new nlobjSearchColumn('custrecord_ebiz_ot_receipt_no');

			//a Date object to be used for a random value
			var now = new Date();
			//now= now.getHours();
			//Getting time in hh:mm tt format.
			var a_p = "";
			var d = new Date();
			var curr_hour = now.getHours();
			if (curr_hour < 12) {
				a_p = "am";
			}
			else {
				a_p = "pm";
			}
			if (curr_hour == 0) {
				curr_hour = 12;
			}
			if (curr_hour > 12) {
				curr_hour = curr_hour - 12;
			}

			var curr_min = now.getMinutes();

			curr_min = curr_min + "";

			if (curr_min.length == 1) {
				curr_min = "0" + curr_min;
			}
			nlapiLogExecution("ERROR", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p));
			// case# 201414340
			var mergeputmethods = getMergeLPMethods(whloc);	
			var varMergeLP = 'F';
			nlapiLogExecution('DEBUG', 'methodid', methodid);
			nlapiLogExecution('DEBUG', 'whloc', whloc);
			if(methodid!=null && methodid!='')
				varMergeLP = isMergeLPEnabled(whloc,methodid,mergeputmethods);
			//varMergeLP = isMergeLP(whloc,methodid);

			//var priorityflag = priorityPutawayflag(itemid,whloc);
			var priorityPutawayLP = priorityPutawayfixedLP(itemid,whloc,itemstatus,EndLocationId);

			// execute the  search, passing null filter and return columns

			nlapiLogExecution('DEBUG', 'varMergeLP', varMergeLP);
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			var vEbizTrailerNo='';			
			var vEbizReceiptNo='';
			var opentaskintid='';
			var expdate='';

			var InvtRefNo="";

			var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group',
			              'custitem_ebizserialin','custitem_ebiz_merge_fifodates'];

			var columns = nlapiLookupField('item', itemid, fields);
			var ItemType='';					
			var batchflg ='F';
			var itemfamId='';
			var itemgrpId='';
			var serialInflg ='F';
			var mergefifodates='F';
			var fifovalue='';

			if(columns != null && columns != '')
			{
				ItemType = columns.recordType;					
				batchflg = columns.custitem_ebizbatchlot;
				itemfamId= columns.custitem_item_family;
				itemgrpId= columns.custitem_item_group;
				serialInflg = columns.custitem_ebizserialin;
				mergefifodates = columns.custitem_ebiz_merge_fifodates;
			}


			if(varMergeLP=="T" || (priorityPutawayLP!=null && priorityPutawayLP!=''))
			{
				if(priorityPutawayLP!=null && priorityPutawayLP!='')
					invtlp=priorityPutawayLP;

				var putItemId,putItemStatus,putItemPC,putBinLoc,putLP,putQty,putoldBinLoc;
				for (var i = 0; i < searchresults.length; i++)
				{
					nlapiLogExecution('DEBUG', 'test1', 'test1');
					nlapiLogExecution('DEBUG', 'searchresults.length', searchresults.length);
					var searchresult = searchresults[i];
					opentaskintid=searchresult.getId();
					vbatchno=searchresult.getValue('custrecord_batch_no');
					nlapiLogExecution('DEBUG', 'opentaskintid', opentaskintid);
					putItemId=searchresult.getValue('custrecord_ebiz_sku_no');
					putItemStatus=searchresult.getValue('custrecord_sku_status');
					putItemPC=searchresult.getValue('custrecord_packcode');
					putBinLoc=searchresult.getValue('custrecord_actbeginloc');
					putLP=searchresult.getValue('custrecord_ebiz_lpno');
//					putQty = searchresult.getValue('custrecord_expe_qty');
					putQty = quantity;
					putoldBinLoc= searchresult.getValue('custrecord_actbeginloc');
					if(searchresult.getValue('custrecord_ebiz_trailer_no') != null && searchresult.getValue('custrecord_ebiz_trailer_no') != '')
					{
						vEbizTrailerNo=searchresult.getValue('custrecord_ebiz_trailer_no');
					}
					if(searchresult.getValue('custrecord_ebiz_ot_receipt_no') != null && searchresult.getValue('custrecord_ebiz_ot_receipt_no') != '')
					{
						vEbizReceiptNo=searchresult.getValue('custrecord_ebiz_ot_receipt_no');
					}

					nlapiLogExecution('DEBUG', 'End', 'END');
				}
				var fifodate='';//case# 20149771
				//commented because declared under lotnumberedinventoryitem)
				/*var filtersfifo = new Array();
				filtersfifo.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', putLP));
				var columnsfifo = new Array();
				columnsfifo[0] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');    
				var invttransactionforfifodate = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersfifo, columnsfifo);
				if(invttransactionforfifodate!=null && invttransactionforfifodate!='')
				{
					fifodate=invttransactionforfifodate[0].getValue('custrecord_ebiz_inv_fifo');
				}*/

				if(ItemType == "lotnumberedinventoryitem" || batchflg == "T")
				{
					nlapiLogExecution('DEBUG', 'vbatchno', vbatchno);
					if(vbatchno!=""&&vbatchno!=null)
					{
						var filterspor = new Array();
						filterspor[0] = new nlobjSearchFilter('name', null, 'is', vbatchno);
						if(itemid!=null && itemid!="")
							filterspor[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid);
						var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor);
						if(receiptsearchresults!=null)
						{
							getlotnoid= receiptsearchresults[0].getId();
							nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
						}
					}
				}


				var filtersfifo = new Array();
				filtersfifo.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', putLP));
				var columnsfifo = new Array();
				columnsfifo[0] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');    
				var invttransactionforfifodate = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersfifo, columnsfifo);
				if(invttransactionforfifodate!=null && invttransactionforfifodate!='')
				{
					fifodate=invttransactionforfifodate[0].getValue('custrecord_ebiz_inv_fifo');
				}

				if(fifodate==null || fifodate=='')
				{
					fifodate = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
					fifovalue = fifodate;
				}
				else
				{
					//fifovalue = fifoDate;
					fifovalue = fifodate;
				}

				nlapiLogExecution('DEBUG', 'putItemId', putItemId);
				nlapiLogExecution('DEBUG', 'putItemStatus', putItemStatus);
				nlapiLogExecution('DEBUG', 'putItemPC', putItemPC);
				nlapiLogExecution('DEBUG', 'putBinLoc', putBinLoc);
				nlapiLogExecution('DEBUG', 'putLP', putLP);
				nlapiLogExecution('DEBUG', 'fifodate', fifodate);
				var varPaltQty = getMaxUOMQty(putItemId,putItemPC);

				nlapiLogExecution('DEBUG', 'varPaltQty', varPaltQty);

				var filtersinvt = new Array();

				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', putItemId));
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', putItemStatus));
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', putBinLoc));
				filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', '19'));
				if((mergefifodates!='T') && (fifodate!=null && fifodate!=''))
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', fifodate));

				if(putItemPC!=null && putItemPC!='')
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', putItemPC));							

				if(getlotnoid!=null && getlotnoid!='')
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', getlotnoid));							

				var columnsinvt = new Array();
				columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
				columnsinvt[0].setSort();

				var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
				if(invtsearchresults != null && invtsearchresults != ''  && invtsearchresults.length > 0)
				{
					nlapiLogExecution('DEBUG', 'invtsearchresults.length', invtsearchresults.length);
					var newputqty=putQty;
					var BoolInvMerged=false;
					for (var i = 0; i < invtsearchresults.length; i++) 
					{
						var qoh=invtsearchresults[i].getValue('custrecord_ebiz_qoh');

						nlapiLogExecution('DEBUG', 'exiting qoh', qoh);
						nlapiLogExecution('DEBUG', 'putQty', putQty);
						nlapiLogExecution('DEBUG', 'varPaltQty', varPaltQty);

						if(newputqty>0 && (parseFloat(putQty)+parseFloat(qoh))<=parseFloat(varPaltQty))
						{
							nlapiLogExecution('DEBUG', 'Inventory Record ID', invtsearchresults[i].getId());
							BoolInvMerged=true;
							nlapiLogExecution('DEBUG', 'BoolInvMerged', BoolInvMerged);

							var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[i].getId());											

							var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
							var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
							var varExistLP=invttransaction.getFieldValue('custrecord_ebiz_inv_lp');

							nlapiLogExecution('DEBUG', 'varExistQOHQty', varExistQOHQty);
							nlapiLogExecution('DEBUG', 'varExistInvQty', varExistInvQty);

							var varupdatedQOHqty = parseFloat(varExistQOHQty) + parseFloat(putQty);
							var varupdatedInvqty = parseFloat(varExistInvQty) + parseFloat(putQty);


							putLP = varExistLP;
							if(parseFloat(varExistQOHQty)<parseFloat(putQty))
							{
								if(fifodate!=null && fifodate!='')
									invttransaction.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
							}

							invttransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(varupdatedQOHqty).toFixed(4));
							invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varupdatedInvqty).toFixed(4));
							invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

							var invtrecid = nlapiSubmitRecord(invttransaction, true);
							InvtRefNo=invtsearchresults[i].getId();
							nlapiLogExecution('DEBUG', 'nlapiSubmitRecord', 'Record Submitted');
							nlapiLogExecution('DEBUG', 'Inventory is merged to the LP '+varExistLP);
							newputqty=0;

							if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);

							UpdateSerialNumLpForInvtMerge(trantype,pointid,linenum,invtlp,varExistLP,exceptionqtyflag);
							// case 20125737 end
						}
					}
					if(BoolInvMerged==false)
					{
						var accountNumber = "";

						//Creating Inventory Record.
						var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

						nlapiLogExecution('ERROR', 'Creating INVT  Record', 'INVT');
						// code added to update name if undefine to binlocation
						if (idl != null && idl!='')
						{
							invtRec.setFieldValue('name', idl);
						}
						//Case 20125778  start         
						else
						{
							invtRec.setFieldValue('name', EndLocationId);	
						}
						////Case 20125778  end         
						invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
						invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
						invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
						invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
						if(itempackcode!=null&&itempackcode!="")
							invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
						invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
						//invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
						//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
						//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
						invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
						invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
						invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
						invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
						invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
						invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
						invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
						invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
						invtRec.setFieldValue('custrecord_invttasktype', '2');
						invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
						if(getuomlevel!=null && getuomlevel!='')
							invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);

						var getlotnoid="";
						var expdate='';
						var vfifodate='';
						nlapiLogExecution('DEBUG', 'ItemType', ItemType);
						nlapiLogExecution('DEBUG', 'batchflg', batchflg);
						nlapiLogExecution('DEBUG', 'batchno', batchno);
						if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
						{
							nlapiLogExecution('DEBUG', 'batchno', batchno);
							if(batchno!="" && batchno!=null)
							{

								var filterspor = new Array();
								filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
								/*if(whLocation!=null && whLocation!="")
									filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));*/
								if(itemid!=null && itemid!="")
									filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

								var column=new Array();
								column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
								column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

								var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
								if(receiptsearchresults!=null)
								{
									getlotnoid= receiptsearchresults[0].getId();
									nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
									expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
									vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
								}
								nlapiLogExecution('DEBUG', 'expdate', expdate);
							}
						}

						if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//							if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
						{ 										
							try
							{
								//Checking FIFO Policy.					
								if (getlotnoid != "") {
									if(vfifodate == null || vfifodate =='')
									{
										fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
										nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
									}
									else
									{
										fifovalue=vfifodate;
									}
									invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
									invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
									invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
								}
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
							}
						}
						else
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);


						nlapiLogExecution('DEBUG', 'Before Submitting invtrecid', 'INVTRECORDS');

						var invtrecid = nlapiSubmitRecord(invtRec, false, true);
						InvtRefNo=invtrecid;
						nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtrecid);
						if (invtrecid != null) {
							nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
						}
						else {
							nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Fail ', 'Fail');
						}

						//Added for updation of Cube information by ramana on 10th june 2011

						nlapiLogExecution('DEBUG', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
						nlapiLogExecution('DEBUG', 'NEW BIN LOCATION DETAILS ', putBinLoc);
						nlapiLogExecution('DEBUG', 'ITEMID', putItemId);
						nlapiLogExecution('DEBUG', 'LP', putLP);

						if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);
					}
					//Added for updation of Cube information by ramana on 10th june 2011

					nlapiLogExecution('DEBUG', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
					nlapiLogExecution('DEBUG', 'NEW BIN LOCATION DETAILS ', putBinLoc);
					nlapiLogExecution('DEBUG', 'ITEMID', putItemId);
					nlapiLogExecution('DEBUG', 'LP', putLP);

					if(putBinLoc!=putoldBinLoc)
					{   
						/*
								//For the OLD Location Updation Purpose.    											 											
						 */

						var arrDims = getSKUCubeAndWeight(putItemId, 1);
						var itemCube = 0;
						if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
						{
							var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims["BaseUOMQty"])));			
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
							nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
						} 
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var vOldRemainingCube = GeteLocCube(putoldBinLoc);
						}
						nlapiLogExecution('DEBUG', 'vOldRemainingCube', vOldRemainingCube);

						var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
						nlapiLogExecution('DEBUG', 'vOldLocationRemainingCube', vTotalCubeValue);
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
						}
						//upto to here old Location Updation purpose

						//For the New Location updation

						var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

						nlapiLogExecution('DEBUG', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

						//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
						var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

						nlapiLogExecution('DEBUG', 'vTotalNewLocation', vTotalNewLocationCube);

						if(vTotalNewLocationCube<0)
						{
							nlapiLogExecution('DEBUG', 'inside PO Overage Validation', 'inside loop');					  
							var form = nlapiCreateForm('Confirm Putaway');
							nlapiLogExecution('DEBUG', 'Form Called', 'form');					  
							var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
							msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'Qty exceeds location capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
							nlapiLogExecution('DEBUG', 'message', msg);					  
							response.writePage(form);                  						
							return;			
						}
						else
						{
							var retValue1 = UpdateLocCube(putBinLoc,vTotalNewLocationCube);
						}
						//upto to here new location

					}						 

					for (var i = 0; i < searchresults.length; i++) {
						vbatchno=searchresults[i].getValue('custrecord_batch_no');
						var searchresult = searchresults[i];
						opentaskintid = searchresult.getId();
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

						nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

						if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
							transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
							transaction.setFieldValue('custrecordact_begin_date', DateStamp());
							//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						}

						transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_upd_date', DateStamp());
						transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_wms_status_flag', 3);
						transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
						nlapiLogExecution("ERROR","InvtRefNo",InvtRefNo);
						transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
						transaction.setFieldValue('custrecord_lpno', putLP);//
						if(idl== '-1' || idl== null || idl==-1 )
						{
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', '');
						}
						else
						{
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
						}
						nlapiLogExecution("ERROR","serialnumcsv",serialnumcsv);
						transaction.setFieldValue('custrecord_serial_no', serialnumcsv);
						vputwrecid=nlapiSubmitRecord(transaction, false, true);
						/*if(systemRule=='LP')
						{
							MoveTaskRecord(vputwrecid);
						}*/

					} 

					//MoveTaskRecord(opentaskintid);
					if(vEbizTrailerNo != null && vEbizTrailerNo != '')
					{
						if(vEbizReceiptNo != null && vEbizReceiptNo != '')
						{
							nlapiLogExecution('DEBUG', 'vEbizReceiptNo', vEbizReceiptNo);
							nlapiLogExecution('DEBUG', 'vEbizTrailerNo', vEbizTrailerNo);
							var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);

							if(poReceiptRec != null && poReceiptRec != '')
							{
								var previousrcvqty=0;
								if(poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != null && poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != '')
								{
									previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
								}	
								var newrcvQty= parseFloat(previousrcvqty)+parseFloat(putQty);

								nlapiLogExecution('DEBUG', 'previousrcvqty', previousrcvqty);
								nlapiLogExecution('DEBUG', 'qty', qty);
								nlapiLogExecution('DEBUG', 'newrcvQty', newrcvQty);

								var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
								nlapiLogExecution('DEBUG','id',id);
							}	
						}	
					}
				}
				else
				{
					var accountNumber = "";
					var getlotnoid="";
					var vfifodate='';
					var expdate='';
					//Creating Inventory Record.
					var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

					nlapiLogExecution('ERROR', 'Creating INVT  Record', 'INVT');
					//code added to avoid undefined in create inventory name field
					if (idl != null && idl!='')
					{
						invtRec.setFieldValue('name', idl);
					}
					else
					{
						invtRec.setFieldValue('name', EndLocationId);	
					}

					invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
					invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
					invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
					invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
					//invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
					//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
					//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
					invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
					invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
					invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
					invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
					invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
					invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
					invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
					invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
					invtRec.setFieldValue('custrecord_invttasktype', '2');
					invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
					if(getuomlevel!=null && getuomlevel!='')
						invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);
					/*if(getlotnoid!=null && getlotnoid!='')
					{
						var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
					}*/
					if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//						if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
					{ 										
						try
						{
							nlapiLogExecution('DEBUG', 'batchno', batchno);
							if(batchno!="" && batchno!=null)
							{

								var filterspor = new Array();
								filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
								/*if(whLocation!=null && whLocation!="")
									filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));*/
								if(itemid!=null && itemid!="")
									filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

								var column=new Array();
								column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
								column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

								var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
								if(receiptsearchresults!=null)
								{
									getlotnoid= receiptsearchresults[0].getId();
									nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
									expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
									vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
									invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
								}
								nlapiLogExecution('DEBUG', 'expdate', expdate);
								nlapiLogExecution('DEBUG', 'vfifodate', vfifodate);
							}

							//Checking FIFO Policy.					
							if (getlotnoid != "" && getlotnoid != null) {
								if(vfifodate ==null || vfifodate =='')
								{
									fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
									nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
								}
								else
								{
									fifovalue =vfifodate;
								}
								invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
								//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
							}
						}
						catch(exp)
						{
							nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
						}
					}
					/*	else
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());*/


					nlapiLogExecution('DEBUG', 'Before Submitting invtrecid', 'INVTRECORDS');
					if(pointid!=null && pointid!="")
						invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);

					/*if(fifovalue != null && fifovalue != '')
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);*/


					if(fifovalue == null || fifovalue == '')
					{
						fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
					}

					if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
					else
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());

					var invtrecid = nlapiSubmitRecord(invtRec, false, true);
					InvtRefNo=invtrecid;
					nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtrecid);
					if (invtrecid != null) {
						nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
					}
					else {
						nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Fail ', 'Fail');
					}

					//Added for updation of Cube information by ramana on 10th june 2011

					nlapiLogExecution('DEBUG', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
					nlapiLogExecution('DEBUG', 'NEW BIN LOCATION DETAILS ', putBinLoc);
					nlapiLogExecution('DEBUG', 'ITEMID', putItemId);
					nlapiLogExecution('DEBUG', 'LP', putLP);

					if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);

					if(putBinLoc!=putoldBinLoc)
					{   
						/*
								//For the OLD Location Updation Purpose.    											 											
						 */

						var arrDims = getSKUCubeAndWeight(putItemId, 1);
						var itemCube = 0;
						if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
						{
							var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims["BaseUOMQty"])));			
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
							nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
						} 
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var vOldRemainingCube = GeteLocCube(putoldBinLoc);
						}
						nlapiLogExecution('DEBUG', 'vOldRemainingCube', vOldRemainingCube);

						var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
						nlapiLogExecution('DEBUG', 'vOldLocationRemainingCube', vTotalCubeValue);
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
						}
						//upto to here old Location Updation purpose

						//For the New Location updation

						var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

						nlapiLogExecution('DEBUG', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

						//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
						var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

						nlapiLogExecution('DEBUG', 'vTotalNewLocation', vTotalNewLocationCube);

						if(vTotalNewLocationCube<0)
						{
							nlapiLogExecution('DEBUG', 'inside PO Overage Validation', 'inside loop');					  
							var form = nlapiCreateForm('Confirm Putaway');
							nlapiLogExecution('DEBUG', 'Form Called', 'form');					  
							var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
							msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'Qty exceeds location capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
							nlapiLogExecution('DEBUG', 'message', msg);					  
							response.writePage(form);                  						
							return;			
						}
						else
						{
							var retValue1 = UpdateLocCube(putBinLoc,vTotalNewLocationCube);
						}
						//upto to here new location

					}                     
					//upto to here on 10th june 2011

					for (var i = 0; i < searchresults.length; i++) {
						vbatchno=searchresults[i].getValue('custrecord_batch_no');
						var searchresult = searchresults[i];
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

						nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

						if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
							transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
							transaction.setFieldValue('custrecordact_begin_date', DateStamp());
							//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						}

						transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_upd_date', DateStamp());
						transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_wms_status_flag', 3);
						transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
						nlapiLogExecution("ERROR","InvtRefNo",InvtRefNo);
						transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
						transaction.setFieldValue('custrecord_lpno', putLP);//
						if(idl== '-1' || idl== null || idl==-1 )
						{
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', '');
						}
						else
						{
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
						}
						//transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
						nlapiLogExecution("ERROR","serialnumcsv",serialnumcsv);

						// case#20127407 starts (to show the serialNo# in opentask after doing cart putaway )
						if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
							try {
								var filters = new Array();
								if(trantype=='returnauthorization' || trantype=='transferorder' )
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
									//Commented for RMA, For RMA no need to check  whether status is checkin/storage
									//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
									filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

								}
								else
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
									filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
									filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
									filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

									if(exceptionqtyflag == 'true')
									{
										nlapiLogExecution('DEBUG', 'exceptionqtyflag new log1', exceptionqtyflag);
										filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'P');// if we have total 5 serial# with putaway and if we do qty exception for 2/3 as actual/remaning then in serial entry er updating 3 remaing qty related serial# serial status 'P',here we have send 2 serial# whose serialstatus is empty.for this reason we have kept this filter
									}

								}

								var columns = new Array();

								columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
								var serialnumcsv = "";
								var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
								var serialnumArray=new Array();
								if (serchRec !=null && serchRec !='' && serchRec.length>0) {
									for (var n1 = 0; n1 < serchRec.length; n1++) {
										//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
										if (tempserialId == "") {
											tempserialId = serchRec[n1].getId();
											tempserial=serchRec[n1].getValue('custrecord_serialnumber');
										}
										else {
											tempserialId = tempserialId + "," + serchRec[n1].getId();
											tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
										}
									}
									serialnumcsv=tempserial;
								}//end
							}
							catch (myexp) {
								nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
							}
						}
						// case # 20127407 end
						transaction.setFieldValue('custrecord_serial_no', serialnumcsv);
						vputwrecid=nlapiSubmitRecord(transaction, false, true);
						//vputwrecid[1]='';
						/*if(systemRule=='LP')
						{
							MoveTaskRecord(vputwrecid[0]);
						}*/

					} 

					//MoveTaskRecord(opentaskintid);
					if(vEbizTrailerNo != null && vEbizTrailerNo != '')
					{
						if(vEbizReceiptNo != null && vEbizReceiptNo != '')
						{
							nlapiLogExecution('DEBUG', 'vEbizReceiptNo', vEbizReceiptNo);
							nlapiLogExecution('DEBUG', 'vEbizTrailerNo', vEbizTrailerNo);
							var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);

							if(poReceiptRec != null && poReceiptRec != '')
							{
								var previousrcvqty=0;
								if(poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != null && poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != '')
								{
									previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
								}	
								var newrcvQty= parseFloat(previousrcvqty)+parseFloat(putQty);

								nlapiLogExecution('DEBUG', 'previousrcvqty', previousrcvqty);
								nlapiLogExecution('DEBUG', 'qty', qty);
								nlapiLogExecution('DEBUG', 'newrcvQty', newrcvQty);

								var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
								nlapiLogExecution('DEBUG','id',id);
							}	
						}	
					}
				}
				if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
					try {
						var filters = new Array();
						if(trantype=='returnauthorization' || trantype=='transferorder' )
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							//Commented for RMA, For RMA no need to check  whether status is checkin/storage
							//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							if(exceptionqtyflag == 'true')
							{
								nlapiLogExecution('DEBUG', 'exceptionqtyflag new log1', exceptionqtyflag);
								filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'P');// if we have total 5 serial# with putaway and if we do qty exception for 2/3 as actual/remaning then in serial entry er updating 3 remaing qty related serial# serial status 'P',here we have send 2 serial# whose serialstatus is empty.for this reason we have kept this filter
							}

						}

						var columns = new Array();

						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();
						if (serchRec !=null && serchRec !='' && serchRec.length>0) {
							for (var n1 = 0; n1 < serchRec.length; n1++) {
								//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
								if (tempserialId == "") {
									tempserialId = serchRec[n1].getId();
									tempserial=serchRec[n1].getValue('custrecord_serialnumber');
								}
								else {
									tempserialId = tempserialId + "," + serchRec[n1].getId();
									tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
								}
							}
						}//end


						if (tempserialId != null && tempserialId !='')  {
							var temSeriIdArr = new Array();
							temSeriIdArr = tempserialId.split(',');
							nlapiLogExecution('DEBUG', 'updating Serial num Records to', 'Storage Status');
							for (var p = 0; p < temSeriIdArr.length; p++) {
								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialwmsstatus';
								values[0] = '3';
								var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
								nlapiLogExecution('DEBUG', 'Inside loop serialId ', temSeriIdArr[p]);

								nlapiLogExecution('DEBUG', 'Serial num Records to', 'Storage Status Success');
							}
						}
					} 
					catch (myexp) {
						nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
					}
				}

			}
			else
			{

				/*for (var i = 0; i < searchresults.length; i++) {
					vbatchno=searchresults[i].getValue('custrecord_batch_no');
					var searchresult = searchresults[i];
					opentaskintid=searchresult.getId();
					var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

					nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

					if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
						transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
						transaction.setFieldValue('custrecordact_begin_date', DateStamp());
						//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					}

					transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					transaction.setFieldValue('custrecord_upd_date', DateStamp());
					transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
					transaction.setFieldValue('custrecord_act_end_date', DateStamp());
					nlapiLogExecution('DEBUG', 'EndDate', DateStamp());
					transaction.setFieldValue('custrecord_wms_status_flag', 3);
					transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
					transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
					vputwrecid=nlapiSubmitRecord(transaction, false, true);

					MoveTaskRecord(vputwrecid);

				} //for loop closing.
				 */
				for (var i = 0; i < searchresults.length; i++){

					vbatchno=searchresults[i].getValue('custrecord_batch_no');
					var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
					var columns = nlapiLookupField('item', itemid, fields);
					var ItemType = columns.recordType;					
					var batchflg = columns.custitem_ebizbatchlot;
					var itemfamId= columns.custitem_item_family;
					var itemgrpId= columns.custitem_item_group;
					var getlotnoid="";
					var expdate='';
					var vfifodate='';
					if(ItemType == "lotnumberedinventoryitem" || batchflg == "T")
					{

						//vbatchno=searchresults[0].getValue('custrecord_batch_no');
						nlapiLogExecution('DEBUG', 'vbatchno', vbatchno);
						if(vbatchno!=""&&vbatchno!=null)
						{
							var filterspor = new Array();
							filterspor[0] = new nlobjSearchFilter('name', null, 'is', vbatchno);
							if(itemid!=null && itemid!="")
								filterspor[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid);
							var column=new Array();
							column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
							column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');
							var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
							if(receiptsearchresults!=null)
							{
								getlotnoid= receiptsearchresults[0].getId();
								nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
								expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
								vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
							}
							nlapiLogExecution('DEBUG', 'expdate', expdate);
							nlapiLogExecution('DEBUG', 'vfifodate', vfifodate);
						}
					}

					var getBaseUOM='';
					var getuomlevel='';

					var eBizItemDims=geteBizItemDimensions(itemid);
					if(eBizItemDims!=null&&eBizItemDims.length>0)
					{
						for(z=0; z < eBizItemDims.length; z++)
						{
							if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
							{
								getBaseUOM = eBizItemDims[z].getText('custrecord_ebizuomskudim');
								getuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
							}
						}
					}

					nlapiLogExecution('DEBUG', 'getBaseUOM', getBaseUOM);
					nlapiLogExecution('DEBUG', 'getuomlevel', getuomlevel);

					///Getting Records to insert in to inventory table.
					nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation ', 'Record Creation');
					var accountNumber = "";

					//Creating Inventory Record.
					var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

					nlapiLogExecution('ERROR', 'Creating INVT  Record', 'INVT');
					//code added to avoid undefined in create inventory name field
					if (idl != null && idl!='')
					{
						invtRec.setFieldValue('name', idl);
					}
					else
					{
						invtRec.setFieldValue('name', EndLocationId);	
					}
					invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
					invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
					invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
					invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
					//invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
					//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
					//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
					invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
					invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
					invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
					invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
					invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
					invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
					invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
					invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
					invtRec.setFieldValue('custrecord_invttasktype', '2');
					invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
					if(getuomlevel!=null && getuomlevel!='')
						invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);
					if(getlotnoid!=null && getlotnoid!='')
					{
						var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
					}
					if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//						if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
					{ 										
						try
						{
							//Checking FIFO Policy.					
							if(getlotnoid!=null && getlotnoid!='')
							{
								if(vfifodate ==null || vfifodate =='')
								{
									fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
									nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
								}
								else
								{
									fifovalue=vfifodate;
								}
								invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
								//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
								invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
							}
						}
						catch(exp)
						{
							nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
						}
					}

					if(fifodate==null || fifodate=='')
					{
						//Case# 201410710 starts
						//fifodate = FifovalueCheck(ItemType,putItemId,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
						fifodate = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
						//Case# 201410710 ends
						fifovalue=fifodate;
					}
					else
					{
						fifovalue=fifodate;
					}

					//if(fifodate != null && fifodate != '')
					//	invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);

					if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
					else
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
					nlapiLogExecution('DEBUG', 'Before Submitting invtrecid', 'INVTRECORDS');

					var invtrecid = nlapiSubmitRecord(invtRec, false, true);
					InvtRefNo=invtrecid;
					nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtrecid);
					if (invtrecid != null) {
						nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
					}
					else {
						nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Fail ', 'Fail');
					}

					if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);

					for (var i = 0; i < searchresults.length; i++) {
						vbatchno=searchresults[i].getValue('custrecord_batch_no');
						var searchresult = searchresults[i];
						opentaskintid=searchresult.getId();
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

						nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

						if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
							transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
							transaction.setFieldValue('custrecordact_begin_date', DateStamp());
							//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						}

						transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
						transaction.setFieldValue('custrecord_recordupdatetime', TimeStamp());
						transaction.setFieldValue('custrecord_upd_date', DateStamp());
						transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						nlapiLogExecution('DEBUG', 'EndDate', DateStamp());
						transaction.setFieldValue('custrecord_wms_status_flag', 3);
						transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
						nlapiLogExecution("ERROR","InvtRefNo",InvtRefNo);
						transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
						nlapiLogExecution('DEBUG', 'idl', idl);
						//transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
						//case start 20125870
						if(idl== '-1' || idl== null || idl==-1 )//end
						{

							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', '');
						}
						else
						{
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
						}

						// case#20127407 starts (to show the serialNo# in opentask after doing cart putaway )
						if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
							try {
								var filters = new Array();
								if(trantype=='returnauthorization' || trantype=='transferorder' )
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
									//Commented for RMA, For RMA no need to check  whether status is checkin/storage
									//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
									filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

								}
								else
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
									filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
									filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
									filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
									nlapiLogExecution('DEBUG', 'exceptionqtyflag new log', exceptionqtyflag);

									if(exceptionqtyflag == 'true')
									{
										nlapiLogExecution('DEBUG', 'exceptionqtyflag new log1', exceptionqtyflag);
										filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'P');// if we have total 5 serial# with putaway and if we do qty exception for 2/3 as actual/remaning then in serial entry er updating 3 remaing qty related serial# serial status 'P',here we have send 2 serial# whose serialstatus is empty.for this reason we have kept this filter
									}

								}

								var columns = new Array();

								columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
								var serialnumcsv = "";
								var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
								var serialnumArray=new Array();
								if (serchRec !=null && serchRec !='' && serchRec.length>0) {
									for (var n1 = 0; n1 < serchRec.length; n1++) {
										nlapiLogExecution('DEBUG', 'serchRec.length new log', serchRec.length);
										//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
										if (tempserialId == "") {
											tempserialId = serchRec[n1].getId();
											tempserial=serchRec[n1].getValue('custrecord_serialnumber');
										}
										else {
											tempserialId = tempserialId + "," + serchRec[n1].getId();
											tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
										}
									}
									serialnumcsv=tempserial;
								}//end
							}
							catch (myexp) {
								nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
							}
						}
						//case # 20127407 end


						nlapiLogExecution("ERROR","serialnumcsv",serialnumcsv);
						transaction.setFieldValue('custrecord_serial_no', serialnumcsv);
						vputwrecid=nlapiSubmitRecord(transaction, false, true);
						nlapiLogExecution("ERROR","vputwrecid",vputwrecid);
						//vputwrecid[1]='';

						// case no 20125978  

						/*if(systemRule=='LP')
						{
							MoveTaskRecord(vputwrecid[0]);
						}*/

					} //for loop closing.


					//serial Entry status updation to S
					//if (Itype == "serializedinventoryitem") {
					if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
						try {
							var filters = new Array();
							if(trantype=='returnauthorization' || trantype=='transferorder' )
							{
								filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
								//Commented for RMA, For RMA no need to check  whether status is checkin/storage
								//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
								filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

							}
							else
							{
								filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
								filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
								filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
								filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

								if(exceptionqtyflag == 'true')
								{
									nlapiLogExecution('DEBUG', 'exceptionqtyflag new log1', exceptionqtyflag);
									filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'P');// if we have total 5 serial# with putaway and if we do qty exception for 2/3 as actual/remaning then in serial entry er updating 3 remaing qty related serial# serial status 'P',here we have send 2 serial# whose serialstatus is empty.for this reason we have kept this filter
								}


							}

							var columns = new Array();

							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
							var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
							var serialnumArray=new Array();
							if (serchRec !=null && serchRec !='' && serchRec.length>0) {
								for (var n1 = 0; n1 < serchRec.length; n1++) {
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n1].getId();
										tempserial=serchRec[n1].getValue('custrecord_serialnumber');
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n1].getId();
										tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
									}
								}
							}//end

							if (tempserialId != null && tempserialId != '') {
								var temSeriIdArr = new Array();
								temSeriIdArr = tempserialId.split(',');
								nlapiLogExecution('DEBUG', 'updating Serial num Records to', 'Storage Status');
								for (var p = 0; p < temSeriIdArr.length; p++) {
									var fields = new Array();
									var values = new Array();
									fields[0] = 'custrecord_serialwmsstatus';
									fields[1] = 'custrecord_serial_location';
									fields[2] = 'custrecord_serialbinlocation';
									values[0] = '3';
									values[1] = whLocation;
									values[2] = EndLocationId;
									var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
									nlapiLogExecution('DEBUG', 'Inside loop serialId ', temSeriIdArr[p]);

									nlapiLogExecution('DEBUG', 'Serial num Records to', 'Storage Status Success');
								}
							}
						} 
						catch (myexp) {
							nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
						}
					}
				}
			}
		} //if Item receipt is not null.
		else {
			nlapiLogExecution('DEBUG', 'If Item Receipt Failed', 'Error in Item Receipt');
			nlapiLogExecution('DEBUG', 'Updating PUTW task with end location - Record Id', RecordId);
			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
			transaction.setFieldValue('custrecord_actendloc', EndLocationId);
			nlapiSubmitRecord(transaction, false, true);
			nlapiLogExecution('DEBUG', 'PUTW task updated with act end location', EndLocationId);
		}
	} 
	catch (e) {
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
		nlapiLogExecution('DEBUG', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('DEBUG', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('DEBUG', 'Exception in TransformRec (PurchaseOrder) : ', e);

		if (e instanceof nlobjError) 
			nlapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());

		nlapiLogExecution('DEBUG', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		transaction.setFieldValue('custrecord_ebiz_error_log', e);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('DEBUG', 'PUTW task updated with act end location', EndLocationId);
		return 'DEBUG';
	}

	return vputwrecid;
}

function isMergeLP(poloc,methodid)
{
	var varMergeLP = 'F';
	var filtersputmethod = new Array();
	filtersputmethod.push(new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof', ['@NONE@',poloc]));
	if(methodid!=null && methodid!='')
		filtersputmethod.push(new nlobjSearchFilter('name', null, 'is', methodid));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            

	var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	if(putwmethodsearchresults != null &&putwmethodsearchresults != ''&& putwmethodsearchresults.length > 0)
	{

		varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
	}

	return varMergeLP;
}
function getMergeLPMethods(poloc)
{
	nlapiLogExecution('ERROR','Into  getMergeLPMethods');

	var putwmethodsearchresults = new Array();
	var filtersputmethod = new Array();
	filtersputmethod.push(new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof', ['@NONE@',poloc]));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filtersputmethod.push(new nlobjSearchFilter('custrecord_mergelp', null, 'is', 'T'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_methodid');  
	colsputmethod[1] = new nlobjSearchColumn('name');  

	putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	nlapiLogExecution('ERROR','Out of  getMergeLPMethods');

	return putwmethodsearchresults;
}
function isMergeLPEnabled(poloc,methodid,mergeputmethods)
{
	nlapiLogExecution('ERROR','Into  isMergeLPEnabled');

	var varMergeLP = 'F';

	if(mergeputmethods!=null && mergeputmethods!='' && mergeputmethods.length>0)
	{
		nlapiLogExecution('ERROR','Merge Methods Length',mergeputmethods.length);

		for (var s = 0; s < mergeputmethods.length; s++) {

			var mergemethodid = mergeputmethods[s].getValue('custrecord_methodid');
			var mergemethodname = mergeputmethods[s].getValue('name');
			nlapiLogExecution('ERROR','mergemethodid',mergemethodid);
			nlapiLogExecution('ERROR','mergemethodname',mergemethodname);
			nlapiLogExecution('ERROR','task methodid',methodid);
			if(mergemethodid==methodid || mergemethodname==methodid)
			{
				varMergeLP='T';
			}
		}
	}
	else
	{
		varMergeLP='F';
	}

	nlapiLogExecution('ERROR','Out of  isMergeLPEnabled');
	return varMergeLP;
}
function getMaxUOMQty(putItemId,putItemPC)
{
	var varPaltQty=0;
	var searchresults = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', putItemId));
	if(putItemPC!=null && putItemPC!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', putItemPC));

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	columns[1].setSort(true);

	searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	if(searchresults != null && searchresults != '' && searchresults.length > 0)
	{
		varPaltQty = searchresults[0].getValue('custrecord_ebizqty');
	}

	return varPaltQty;
}

function DeleteInvtRecCreatedforCHKNTask(pointid, lp)
{
	nlapiLogExecution('DEBUG','Inside DeleteInvtRecCreatedforCHKNTask ','Funciton');
	var Ifilters = new Array();
	Ifilters[0] = new nlobjSearchFilter('name', null, 'is', pointid);
	Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [17]);
	Ifilters[2] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [1]);
	Ifilters[3] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp);

	var invtId="";
	var invtType="";
	var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (serchInvtRec) 
	{
		for (var s = 0; s < serchInvtRec.length; s++) {
			nlapiLogExecution('DEBUG','Inside loop','loop');
			var searchresult = serchInvtRec[ s ];
			nlapiLogExecution('DEBUG','need to print this','print');
			invtId = serchInvtRec[s].getId();
			invtType= serchInvtRec[s].getRecordType();
			nlapiLogExecution('DEBUG','CHKN INVT Id',invtId);
			nlapiLogExecution('DEBUG','CHKN invtType ',invtType);
			nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
			nlapiLogExecution('DEBUG','Invt Deleted record Id',invtId);

		}
	}
}

function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}




/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
//Inventory Transfer

function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot,vdepartment,vunits)
{
	try{
		nlapiLogExecution('DEBUG', 'Into InvokeNSInventoryTransfer');  
		nlapiLogExecution('DEBUG', 'item', item);  
		nlapiLogExecution('DEBUG', 'itemstatus', itemstatus);
		nlapiLogExecution('DEBUG', 'loc', loc);
		nlapiLogExecution('DEBUG', 'toloc', toloc);
		nlapiLogExecution('DEBUG', 'qty', qty);
		nlapiLogExecution('DEBUG', 'lot', lot);
		nlapiLogExecution('DEBUG', 'vunits', vunits);

		var ItemType='';

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType','custitem_ebizserialout' ]);
			ItemType = columns.recordType;
		}

		nlapiLogExecution('DEBUG', 'ItemType', ItemType);
		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname; 

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();  
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}

		var invttransfer = nlapiCreateRecord('inventorytransfer');   

		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);  
		invttransfer.setCurrentLineItemValue('inventory', 'units', vunits);  
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));

		var subs = nlapiGetContext().getFeature('subsidiaries');
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			{
				nlapiLogExecution('DEBUG', 'vSubsidiaryVal', vSubsidiaryVal);
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);
			}

		}
		nlapiLogExecution('DEBUG', 'vdepartment tst', vdepartment);

		invttransfer.setFieldValue('department', vdepartment);


		if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem") 
			//if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			if(confirmLotToNS=='N')
				lot=vItemname;  

			nlapiLogExecution('DEBUG', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
			invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  lot + "(" + parseFloat(qty) + ")");
		}

		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);
		nlapiLogExecution('DEBUG', 'Out of InvokeNSInventoryTransfer');
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in InvokeNSInventoryTransferNew ', exp); 

	}
}
/*** up to here ***/




//Case# 20123720  start , to get expiry date for lot
function getLotExpdate(vbatchno,itemid)
{
	try{
		nlapiLogExecution('ERROR', 'vbatchno', vbatchno);
		nlapiLogExecution('ERROR', 'itemid', itemid);
		var expdate='';
		var filterspor = new Array();
		filterspor.push(new nlobjSearchFilter('name', null, 'is', vbatchno));
		if(itemid!=null && itemid!="")
			filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
		column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

		var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
		if(receiptsearchresults!=null)
		{
			var getlotnoid= receiptsearchresults[0].getId();
			nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
			expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');

		}
		nlapiLogExecution('ERROR', 'expdate', expdate);
		return expdate;
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'expection in getLotExpdate', e);
	}

}

function GetSystemRuleForPostItemReceiptby(whloc) //Case# 20149154
{
	try
	{
		var rulevalue='';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Post Item Receipt by'));
		//filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whLocation]));

		// case no start 20126968
		var vRoleLocation=getRoledBasedLocation();
		var resloc=new Array();
		resloc.push("@NONE@");

		nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
		//case# 20149824
		if(whloc!=null && whloc!='')
		{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
		}
		else if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			for(var count=0;count<vRoleLocation.length;count++)
				resloc.push(vRoleLocation[count]);
			filter.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', resloc));
		}
		// Case# 20149154 starts
		/*else
			{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
			}*/
		// Case# 20149154 ends
		// case no end 20126968
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		column[1]=new nlobjSearchColumn('custrecord_ebizsite');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);
		var siteval = '';
		if(searchresult!=null && searchresult!="")
		{
			for(var k = 0;k<searchresult.length;k++)
			{
				siteval = searchresult[k].getValue('custrecord_ebizsite');
				nlapiLogExecution('ERROR','whloc',whloc);
				nlapiLogExecution('ERROR','siteval',siteval);

				if(whloc!=null && whloc!='')
				{
					if(whloc == siteval)
					{
						rulevalue=searchresult[k].getValue('custrecord_ebizrulevalue');
						break;
					}
				}
			}

			if(rulevalue==null || rulevalue=='')
			{
				for(var z = 0;z<searchresult.length;z++)
				{
					siteval = searchresult[z].getValue('custrecord_ebizsite');

					if(siteval==null || siteval=='')
					{
						rulevalue=searchresult[z].getValue('custrecord_ebizrulevalue');
						break;
					}
				}

			}
		}

		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		if(rulevalue!=null && rulevalue!='undefined' && rulevalue!='null' && rulevalue!='')
		{
			if(rulevalue.trim()!='LP' && rulevalue.trim()!='PO')
			{
				rulevalue='LP';
			}
		}
		else
		{
			rulevalue='LP';
		}
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}

function generateItemReceipt(fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,invtlp,systemRule,RecordId,
		EndLocationId,batchno,qtyexceptionFlag,templocation,getScannedSerialNoinQtyexception,quantityineach,response)
{
	try{
		// case no 20126278
		var TranferInvFlag='F';
		var whLocation=templocation;
		nlapiLogExecution('DEBUG', "whLocation", whLocation);
		var tempserialId="";
		quantity=quantityineach;
		//if(parseFloat(quantity)!=0){
		var inventorytransfer;
		if(parseFloat(quantityineach)!=0){
			var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
			var polinelength = trecord.getLineItemCount('item');
			nlapiLogExecution('DEBUG', "polinelength", polinelength);
			var vAdvBinManagement=false;

			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('DEBUG', 'vAdvBinManagement', vAdvBinManagement);
			for (var j = 1; j <= polinelength; j++) {
				//nlapiLogExecution('DEBUG', "Get Line Value" + trecord.getLineItemValue('item', 'line', j));

				var item_id = trecord.getLineItemValue('item', 'item', j);
				var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
				var itemLineNo = trecord.getLineItemValue('item', 'line', j);
				//var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', j);
				var poItemUOM = trecord.getLineItemValue('item', 'units', j);
				var commitflag = 'N';
				var str = 'PO Item Line No. = ' + itemLineNo + '<br>';
				str = str + 'Task linenum. = ' + linenum + '<br>';	
				str = str + 'Transaction Type. = ' + trantype + '<br>';	
				str = str + 'poItemUOM. = ' + poItemUOM + '<br>';	
				str = str + 'qtyexceptionFlag. = ' + qtyexceptionFlag + '<br>';	

				nlapiLogExecution('DEBUG', 'Line Details', str);

				if(trantype!='transferorder')
				{
					if (itemLineNo == linenum) {

						whLocation=trecord.getLineItemValue('item', 'location', j);//value
						if(whLocation==null||whLocation=="")
							whLocation=templocation;

						if(poItemUOM!=null && poItemUOM!='')
						{
							var vbaseuomqty=0;
							var vuomqty=0;
							var eBizItemDims=geteBizItemDimensions(item_id);
							if(eBizItemDims!=null&&eBizItemDims.length>0)
							{
								nlapiLogExecution('DEBUG', 'Dims Length', eBizItemDims.length);
								for(z=0; z < eBizItemDims.length; z++)
								{
									if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
									}
									nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
									if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
									{
										vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
									}
								}
								if(vuomqty==null || vuomqty=='')
								{
									vuomqty=vbaseuomqty;
								}
								nlapiLogExecution('DEBUG', 'vbaseuomqty', vbaseuomqty);
								nlapiLogExecution('DEBUG', 'vuomqty', vuomqty);
								nlapiLogExecution('DEBUG', 'quantity tstbefore', quantityineach);

								if(quantityineach==null || quantityineach=='' || isNaN(quantityineach))
									quantity=0;
								else
									quantity = (parseFloat(quantityineach)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

								nlapiLogExecution('DEBUG', 'quantity tstafter', quantity);
							}
						}

						//Code Added by Ramana
						var varItemlocation;
						nlapiLogExecution('DEBUG', 'itemstatus', itemstatus);
						varItemlocation = getItemStatusMapLoc(itemstatus);
						//upto to here

						commitflag = 'Y';

						//Itype = nlapiLookupField('item', itemid, 'recordType');
						var columns = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
						if(columns != null && columns != '')
						{
							Itype = columns.recordType;
							serialInflg = columns.custitem_ebizserialin;
							nlapiLogExecution('DEBUG', 'Itype', Itype);
							nlapiLogExecution('DEBUG', 'columns', columns);
						}

						nlapiLogExecution('DEBUG', 'Value of J', j);
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', quantity);

						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

						nlapiLogExecution('DEBUG', 'Serialquantity', quantity);
						nlapiLogExecution('DEBUG', 'Into SerialNos');
						//if (Itype == "serializedinventoryitem") {
						if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") {
							nlapiLogExecution('DEBUG', 'Into SerialNos');
							nlapiLogExecution('DEBUG', 'invtlp', invtlp);					
							var serialline = new Array();
							var serialId = new Array();

							var tempSerial = "";
							var filters = new Array();
							/*filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							filters[3] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'doesnotstartwith', 'H');*/
							var filters = new Array();
							//case# 20149411 starts
							if(trantype=='returnauthorization')
							{
								nlapiLogExecution('ERROR', 'returnauthorization');
								filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
								filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
								filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
								filters[3] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
							}
							/*else if(trantype=='transferorder')
							{
								nlapiLogExecution('ERROR', 'transferorder');
								filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
								filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']);
								filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
								filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							}*/
							else
							{

								nlapiLogExecution('ERROR', 'purchasesorder');
								filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);							
								filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
								filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
								filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
								if(qtyexceptionFlag == "true")
								{
									filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'P');// if we have total 5 serial# with putaway and if we do qty exception for 2/3 as actual/remaning then in serial entry er updating 3 remaing qty related serial# serial status 'P',here we have send 2 serial# whose serialstatus is empty.for this reason we have kept this filter
								}
							}
							//case# 20149411 ends
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
							//var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

							if (serchRec) {
								for (var n = 0; n < serchRec.length; n++) {
									nlapiLogExecution('DEBUG', 'serchRec.length', serchRec.length);
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');
									}
									else {
										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

									}
									//case 20127154 start (allow only for serial item for that Itype added to if)
									//case# 201410518 (Allowing to create subrecord for serializedassemblyitem)
									//if(vAdvBinManagement && Itype == "serializedinventoryitem")
									if(vAdvBinManagement && (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem"))
									{
										//case# 20127265 starts (createCurrentLineItemSubrecord should create only once)
										if(n==0)
											var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										//case# 20127265
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');

									}
								}
								//case# 201410518 (Allowing to create subrecord for serializedassemblyitem)
								//if(vAdvBinManagement && Itype == "serializedinventoryitem")
								if(vAdvBinManagement && (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem"))
									compSubRecord.commit();
								//case 20127154 end
								serialnumcsv = tempSerial;
								tempSerial = "";
							}

							nlapiLogExecution('DEBUG', 'Serial nums assigned', serialnumcsv);
							nlapiLogExecution('DEBUG', 'Serial nums tempserialId', tempserialId);
							if(!vAdvBinManagement)
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);



						}
						else 
							//if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
							if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem" ) {

								nlapiLogExecution('DEBUG', 'Into LOT/Batch');
								nlapiLogExecution('DEBUG', 'LP:', invtlp);
								var tempBatch = "";
								var batchcsv = "";

								var confirmLotToNS='Y';

								confirmLotToNS=GetConfirmLotToNS(varItemlocation);

								nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);


								var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								/* The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/								
								vatColumns[2] = new nlobjSearchColumn('custrecord_batch_no');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										nlapiLogExecution('DEBUG', 'Inside For:', 'Inside batchresults');
										if (tempBatch == "") {
											var vItemname;
											/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/
											var tempbatchnew;
											var vItQty;     //Line Added by Ganapathi on 7th Dec 2012
											/* Up to here */ 
											if(confirmLotToNS == 'Y')
											{	
												nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/												
												tempbatchnew = batchsearchresults[n].getValue('custrecord_batch_no');
												//  Code Added by Ganapathi on 7th Dec 2012
												vItQty = batchsearchresults[n].getValue('custrecord_expe_qty');
												nlapiLogExecution('DEBUG', 'vItQty:', vItQty);
												nlapiLogExecution('DEBUG', 'tempBatch:', tempBatch);
												// case 20123720 start 
												var expdate1=getLotExpdate(tempbatchnew,item_id);
												if(expdate1 !=null && expdate1 !='')
												{
													trecord.setCurrentLineItemValue('item', 'expirationdate', expdate1);

												}//end

												// End of the Code Added by Ganapathi on 7th Dec 2012
												/* Up to here */ 
											}	
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();
												columns[0] = new nlobjSearchColumn('itemid');

												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												nlapiLogExecution('DEBUG','itemdetails',itemdetails);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/												
												if(vItQty !=quantity)
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);	
												/* Up to here */ 
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
												nlapiLogExecution('DEBUG', 'compSubRecord:', compSubRecord);
											}
										}
										else {
											var vItemname;
											/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
											var vItQty;                   //Line Added by Ganapathi on 7th Dec 2012
											if(confirmLotToNS == 'Y'){
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												//  Code Added by Ganapathi on 7th Dec 2012
												vItQty = batchsearchresults[n].getValue('custrecord_expe_qty');
												nlapiLogExecution('DEBUG', 'else vItQty:', vItQty);
												// End of the Code Added by Ganapathi on 7th Dec 2012
												/* Up to here */ 
											}
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');

												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempBatch);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}
										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}
								var vItemname;
								if(confirmLotToNS == 'Y')
									tempBatch = batchno + '('+ quantity + ')';
								else
								{
									nlapiLogExecution('DEBUG', 'item_id:', item_id);
									var filters1 = new Array();          
									filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
									var columns = new Array();

									columns[0] = new nlobjSearchColumn('itemid');

									var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
									if (itemdetails !=null) 
									{
										vItemname=itemdetails[0].getValue('itemid');
										nlapiLogExecution('DEBUG', 'vItemname:', vItemname);
										//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
										vItemname=vItemname.replace(/ /g,"-");
										tempBatch=vItemname + '('+ quantity + ')';


									}
								}								
								batchcsv = tempBatch;
								nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned new', batchcsv);
								if(!vAdvBinManagement)
									trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);

							}
						// case no 20126691
						trecord.setCurrentLineItemValue('item', 'custcol_lp', invtlp);
						trecord.commitLineItem('item');
					}
				}
				else
				{
					nlapiLogExecution('DEBUG', 'itemid (Task)', itemid);	
					nlapiLogExecution('DEBUG', 'item_id (Line)', item_id);	

					itemLineNo=parseFloat(itemLineNo)-2;
					if (itemid == item_id && itemLineNo == linenum) {
						/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
						//UOM conversions
						if(poItemUOM!=null && poItemUOM!='')
						{
							var vbaseuomqty=0;
							var vuomqty=0;
							var eBizItemDims=geteBizItemDimensions(item_id);
							if(eBizItemDims!=null&&eBizItemDims.length>0)
							{
								nlapiLogExecution('DEBUG', 'Dims Length', eBizItemDims.length);
								for(z=0; z < eBizItemDims.length; z++)
								{
									if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
									}
									nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
									if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
									{
										vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
									}
								}
								if(vuomqty==null || vuomqty=='')
								{
									vuomqty=vbaseuomqty;
								}
								nlapiLogExecution('DEBUG', 'vbaseuomqty', vbaseuomqty);
								nlapiLogExecution('DEBUG', 'vuomqty', vuomqty);
								nlapiLogExecution('DEBUG', 'quantity tstbefore', quantityineach);

								if(quantityineach==null || quantityineach=='' || isNaN(quantityineach))
									quantity=0;
								else
									quantity = (parseFloat(quantityineach)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

								nlapiLogExecution('DEBUG', 'quantity tstafter', quantity);
							}
						}

						/*** up to here ***/

						commitflag = 'Y';

						var varItemlocation;				
						varItemlocation = getItemStatusMapLoc(itemstatus);

						//Itype = nlapiLookupField('item', itemid, 'recordType');
						var columns = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
						if(columns != null && columns != '')
						{
							Itype = columns.recordType;
							serialInflg = columns.custitem_ebizserialin;
							nlapiLogExecution('DEBUG', 'Itype', Itype);
							nlapiLogExecution('DEBUG', 'columns', columns);
						}

						nlapiLogExecution('DEBUG', 'Value of J', j);
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', quantity);
						if(trantype!='transferorder')
							trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

						/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
						nlapiLogExecution('DEBUG', 'whLocation tst ', whLocation);
						nlapiLogExecution('DEBUG', 'itemstatus tst', itemstatus);

						if(whLocation == varItemlocation)
						{
							trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
						}
						else
						{
							TranferInvFlag = 'T';
							trecord.setCurrentLineItemValue('item', 'location', whLocation);
						}
						/*** up to here ***/
						trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
						nlapiLogExecution('DEBUG', 'Serialquantity', quantity);
						nlapiLogExecution('DEBUG', 'Into SerialNos');
						//if (Itype == "serializedinventoryitem" ) {
						if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T")						
						{
							nlapiLogExecution('DEBUG', 'Into SerialNos');
							nlapiLogExecution('DEBUG', 'invtlp', invtlp);					
							var serialline = new Array();
							var serialId = new Array();

							var tempSerial = "";
							//case # 20149885 uncommented filters array
							var filters = new Array();
							/*filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1,8]);
							filters[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							 */

							nlapiLogExecution('ERROR', 'transferorder');
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']);
							filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							if(qtyexceptionFlag == 'true')
							{
								nlapiLogExecution('DEBUG', 'exceptionqtyflag new log1', exceptionqtyflag);
								filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'P');// if we have total 5 serial# with putaway and if we do qty exception for 2/3 as actual/remaning then in serial entry er updating 3 remaing qty related serial# serial status 'P',here we have send 2 serial# whose serialstatus is empty.for this reason we have kept this filter
							}							

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
							var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

							if (serchRec) {
								for (var n = 0; n < serchRec.length; n++) {
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');
									}
									else {
										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

									}
									//Case # 20126900? Start
									if(vAdvBinManagement && Itype == "serializedinventoryitem")
									{
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										compSubRecord.commit();
									}
									//Case # 20126900? End
								}

								serialnumcsv = tempSerial;
								tempSerial = "";
							}

							nlapiLogExecution('DEBUG', 'Serial nums assigned', serialnumcsv);
							nlapiLogExecution('DEBUG', 'Serial nums tempserialId', tempserialId);
							if(!vAdvBinManagement)
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);


						}
						else 
							if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem" ){
								//if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
								/*
					nlapiLogExecution('DEBUG', 'Into LOT/Batch');
					nlapiLogExecution('DEBUG', 'LP:', invtlp);
					var tempBatch = "";
					var batchcsv = "";
					var batfilter = new Array();
					batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
					batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
					batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
					batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
					batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);
					var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, new nlobjSearchColumn('custrecord_lotnowithquantity'));
					if (batchsearchresults) {
						for (var n = 0; n < batchsearchresults.length; n++) {
							//This is for Batch num loopin with Space separated.
							if (tempBatch == "") {
								tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
							}
							else {
								tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');

							}
						}

						batchcsv = tempBatch;
						tempBatch = "";
					}
					nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned', batchcsv);
					trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
								 */

								nlapiLogExecution('DEBUG', 'Into LOT/Batch');
								nlapiLogExecution('DEBUG', 'LP:', invtlp);
								var tempBatch = "";
								var batchcsv = "";

								var confirmLotToNS='Y';

								confirmLotToNS=GetConfirmLotToNS(varItemlocation);

								nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);


								var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								/* The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/									
								vatColumns[2] = new nlobjSearchColumn('custrecord_batch_no');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										/* The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
										var vItemname;
										if (tempBatch == "") {
											/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
											var vtempbatchnew;
											var vItQty;
											/* Up to here */ 
											if(confirmLotToNS == 'Y')
											{
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
												vtempbatchnew = batchsearchresults[n].getValue('custrecord_batch_no');
												//  Code Added by Ganapathi on 7th Dec 2012
												vItQty = batchsearchresults[n].getValue('custrecord_expe_qty');
												nlapiLogExecution('DEBUG', 'else vItQty:', vItQty);
												// End of the Code Added by Ganapathi on 7th Dec 2012
												/* Up to here */ 
											}
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';


												}
											}
											//Case # 20126900� Start											
											/*if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vtempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}*/
											//Case # 20126900� End
										}
										else {
											var vItemname;
											var vItQty;
											if(confirmLotToNS == 'Y')
											{	
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												vtempbatchnew = batchsearchresults[n].getValue('custrecord_batch_no');
												//  Code Added by Ganapathi on 7th Dec 2012
												vItQty = batchsearchresults[n].getValue('custrecord_expe_qty');
												nlapiLogExecution('DEBUG', 'else vItQty:', vItQty);
												// End of the Code Added by Ganapathi on 7th Dec 2012
											}	
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';
												}
											}
											//Case # 20126900� Start
											/*if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vtempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}*/
											//Case # 20126900� End
										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}


								/*if(confirmLotToNS == 'Y')
								tempBatch = batchno + '('+ quantity + ')';
							else
							{
								nlapiLogExecution('DEBUG', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');
								var vItemname;
								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('DEBUG', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}
							batchcsv = tempBatch;*/
								nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned', batchcsv);
								if(!vAdvBinManagement)
									trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							}
						// case no 20126691
						trecord.setCurrentLineItemValue('item', 'custcol_lp', invtlp);
						trecord.commitLineItem('item');
						/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
						if(TranferInvFlag == 'T')
							inventorytransfer = InvokeNSInventoryTransfer(itemid,itemstatus,whLocation,varItemlocation,quantity,batchno,vdepartment,poItemUOM);
						/*** up to here ***/
					}
				}
				if (commitflag == 'N') {
					nlapiLogExecution('DEBUG', 'commitflag is N', commitflag);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
					trecord.commitLineItem('item');
				}
			}
		}
		nlapiLogExecution('DEBUG', 'Before Item Receipt---', 'Before');
		if (parseFloat(quantity)>0)
			var idl = nlapiSubmitRecord(trecord);
		nlapiLogExecution('DEBUG', 'Item Receipt idl', idl);
		/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/	
		if(inventorytransfer!=null&&inventorytransfer!="")
		{
			var transferinvRecId = nlapiSubmitRecord(inventorytransfer, false, true);
			nlapiLogExecution('DEBUG', 'Inventory inventorytransfer tst',
					transferinvRecId + ' is Success');
		}
		return idl;

	}	
	catch(exp)
	{
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		var POarray = new Array();
		nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
		nlapiLogExecution('DEBUG', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('DEBUG', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,exp,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('DEBUG', 'Exception in TransformRec (PurchaseOrder) : ', exp);

		if (exp instanceof nlobjError) 
			nlapiLogExecution('DEBUG', 'system error', exp.getCode() + '\n' + exp.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', exp.toString());

		nlapiLogExecution('DEBUG', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		transaction.setFieldValue('custrecord_ebiz_error_log', exp);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('DEBUG', 'PUTW task updated with act end location', EndLocationId);

		nlapiLogExecution('ERROR', 'Putaway Failed ');
		POarray["custparam_error"] = exp.getCode() + '\n' + exp.getDetails();
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		return;

	}
}
//Update SerialNumbers LP value when Invt is Merge
//case 20125737 start 
function UpdateSerialNumLpForInvtMerge(trantype,pointid,linenum,actlp,InvtLP,exceptionqtyflag)
{
	nlapiLogExecution('ERROR', 'into UpdateSerialNumLpForInvtMerge', 'done');
	nlapiLogExecution('ERROR', 'trantype', trantype);
	nlapiLogExecution('ERROR', 'pointid', pointid);
	nlapiLogExecution('ERROR', 'linenum', linenum);
	nlapiLogExecution('ERROR', 'actlp', actlp);
	nlapiLogExecution('ERROR', 'InvtLP', InvtLP);

	try {						

		var tempserialId='';
		var filters = new Array();
		if(trantype=='returnauthorization' || trantype=='transferorder' )
		{
			filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
			//Commented for RMA, For RMA no need to check  whether status is checkin/storage
			//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
			filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

		}
		else
		{
			filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
			filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
			filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
			filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', actlp);
			if(exceptionqtyflag == 'true')
			{
				nlapiLogExecution('DEBUG', 'exceptionqtyflag new log1', exceptionqtyflag);
				filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'P');// if we have total 5 serial# with putaway and if we do qty exception for 2/3 as actual/remaning then in serial entry er updating 3 remaing qty related serial# serial status 'P',here we have send 2 serial# whose serialstatus is empty.for this reason we have kept this filter
			}
		}

		var columns = new Array();

		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var serialnumcsv = "";
		var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
		var tempserialId=new Array();
		if (serchRec !=null && serchRec !='' && serchRec.length>0) {
			for (var n1 = 0; n1 < serchRec.length; n1++) {
				//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
				if (tempserialId == "") {
					tempserialId = serchRec[n1].getId();
					tempserial=serchRec[n1].getValue('custrecord_serialnumber');
				}
				else {
					tempserialId = tempserialId + "," + serchRec[n1].getId();
					tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
				}
			}
		}//end
		nlapiLogExecution('ERROR', 'tempserialId', tempserialId);
		if (tempserialId != null && tempserialId !='') {
			var temSeriIdArr = new Array();
			temSeriIdArr = tempserialId.split(',');
			nlapiLogExecution('ERROR', 'updating Serial num Records to', 'Storage Status');
			for (var p = 0; p < temSeriIdArr.length; p++) {
				nlapiLogExecution('ERROR', 'temSeriIdArr.length ', temSeriIdArr.length);
				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialparentid';
				fields[1] = 'custrecord_serialwmsstatus';
				values[0] = InvtLP;
				values[1] = '3';


				nlapiLogExecution('ERROR', 'Inside loop serialId ', temSeriIdArr[p]);
				var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
				nlapiLogExecution('ERROR', 'Inside loop serialId ', temSeriIdArr[p]);

				nlapiLogExecution('ERROR', 'UpdateSerialNumLpForInvtMerge', 'LP Merge Status Success');
			}
		}
	} 
	catch (exp) {
		nlapiLogExecution('ERROR', 'UpdateSerialNumLpForInvtMerge exeption', exp);
	}

}
//case 20125737 end
