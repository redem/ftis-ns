/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_Replenishment_Location.js,v $
 *     	   $Revision: 1.8.2.9.4.2.4.24.2.1 $
 *     	   $Date: 2015/11/12 15:02:35 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Replenishment_Location.js,v $
 * Revision 1.8.2.9.4.2.4.24.2.1  2015/11/12 15:02:35  skreddy
 * case 201415498/201415499
 * Boombah SB  issue fix
 *
 * Revision 1.8.2.9.4.2.4.24  2015/08/21 15:34:43  grao
 * 2015.2   issue fixes 201414080
 *
 * Revision 1.8.2.9.4.2.4.23  2015/07/01 13:11:17  schepuri
 * case# 201413297
 *
 * Revision 1.8.2.9.4.2.4.22  2015/02/04 08:23:05  sponnaganti
 * Case# 201411507
 * True Fab Prod  Next/Skip Issue
 *
 * Revision 1.8.2.9.4.2.4.21  2014/09/19 11:52:00  skavuri
 * case # 201410209
 *
 * Revision 1.8.2.9.4.2.4.20  2014/07/01 06:39:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Alphacommchanges
 *
 * Revision 1.8.2.9.4.2.4.19  2014/06/13 09:37:26  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.8.2.9.4.2.4.18  2014/06/06 12:18:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.8.2.9.4.2.4.17  2014/05/30 00:34:23  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.8.2.9.4.2.4.16  2014/04/29 06:01:33  skreddy
 * case # 20148180
 * Sonic SB  issue fix
 *
 * Revision 1.8.2.9.4.2.4.15  2013/11/20 20:45:48  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20125767
 * Replen Exceptions
 *
 * Revision 1.8.2.9.4.2.4.14  2013/11/14 15:41:55  skreddy
 * Case# 20125719 & 20125658
 * Afosa SB issue fix
 *
 * Revision 1.8.2.9.4.2.4.13  2013/11/14 14:34:37  rmukkera
 * Case # 20125742
 *
 * Revision 1.8.2.9.4.2.4.12  2013/11/12 06:40:59  skreddy
 * Case# 20125658 & 20125602
 * Afosa SB issue fix
 *
 * Revision 1.8.2.9.4.2.4.11  2013/10/23 16:13:43  nneelam
 * Case# 20125250
 * Next button not working properly when Rep Report no given..
 *
 * Revision 1.8.2.9.4.2.4.10  2013/09/17 06:30:11  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.8.2.9.4.2.4.9  2013/09/06 14:55:04  rmukkera
 * Case# 20124286
 *
 * Revision 1.8.2.9.4.2.4.8  2013/09/02 15:38:06  rmukkera
 * Case# 20124176
 *
 * Revision 1.8.2.9.4.2.4.7  2013/08/27 16:34:09  rrpulicherla
 * case# 20123755
 * issue fix for 20123755
 *
 * Revision 1.8.2.9.4.2.4.6  2013/07/18 15:16:43  schepuri
 * Addeed item descrption
 *
 * Revision 1.8.2.9.4.2.4.5  2013/05/14 14:16:37  schepuri
 * RF Replen confirm based on filters
 *
 * Revision 1.8.2.9.4.2.4.4  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.8.2.9.4.2.4.3  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.8.2.9.4.2.4.2  2013/03/13 13:57:18  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.8.2.9.4.2.4.1  2013/03/05 14:46:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.8.2.9.4.2  2012/09/26 12:37:08  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.8.2.9.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.8.2.9  2012/08/24 16:09:57  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Replen confirm
 *
 * Revision 1.8.2.8  2012/08/17 22:55:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *  RF Replen
 *
 * Revision 1.8.2.7  2012/08/06 06:51:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Item Description.
 *
 * Revision 1.8.2.6  2012/07/12 14:47:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.8.2.5  2012/06/04 11:47:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * HUBPEN - REPLEN ISSUE FIXES
 *
 * Revision 1.8.2.4  2012/03/27 13:45:08  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.8.2.3  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.8.2.2  2012/02/21 13:26:01  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.8.2.1  2012/02/07 12:40:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Replen Issue Fixes
 *
 * Revision 1.10  2012/01/17 13:53:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.9  2012/01/09 13:15:23  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.8  2012/01/05 17:24:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.6  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *****************************************************************************/

function Replenishment_Location(request, response){
	if (request.getMethod() == 'GET') {
		var reportNo = request.getParameter('custparam_repno');
		var beginLoc = request.getParameter('custparam_repbegloc');
		var sku = request.getParameter('custparam_repsku');
		var skuNo = request.getParameter('custparam_repskuno');
		var expQty = request.getParameter('custparam_repexpqty');
		var repInternalId = request.getParameter('custparam_repid');
		var clustNo = request.getParameter('custparam_clustno');
		var whlocation = request.getParameter('custparam_whlocation');
		var actEndLocationId = request.getParameter('custparam_reppicklocno');
		var actEndLocation = request.getParameter('custparam_reppickloc');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var fromlpno=request.getParameter('custparam_fromlpno');
		var tolpno=request.getParameter('custparam_tolpno');
		var batchno=request.getParameter('custparam_batchno');
		var nextitem=request.getParameter('custparam_nextitem');
		var nextitemno=request.getParameter('custparam_nextitemno');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var nextlocation=request.getParameter('custparam_nextlocation');
		var nextqty=request.getParameter('custparam_nextqty');
		var getreportNo=request.getParameter('custparam_enterrepno');

		var taskpriority = request.getParameter('custparam_entertaskpriority');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');
		var getLanguage = request.getParameter('custparam_language');


		nlapiLogExecution('ERROR', 'fromlpno', fromlpno);
		nlapiLogExecution('ERROR', 'tolpno', tolpno);
		nlapiLogExecution('ERROR', 'entereditem', getitem);
		nlapiLogExecution('ERROR', 'entereditemid', getitemid);
		nlapiLogExecution('ERROR', 'enteredloc', getloc);
		nlapiLogExecution('ERROR', 'enteredlocid', getlocid);		
		nlapiLogExecution('ERROR', 'taskpriority', taskpriority);
		nlapiLogExecution('ERROR', 'RecordCount', RecordCount);
		nlapiLogExecution('ERROR', 'replenType in GET', replenType);
		nlapiLogExecution('ERROR', 'skuNo in GET', skuNo);
		nlapiLogExecution('ERROR', 'entered reportNo in GET', getreportNo);
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);


		var Itype = nlapiLookupField('item', skuNo, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, skuNo);

		var Itemdescription='';


		var getItemName = ItemRec.getFieldValue('itemid');

		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('description');
		}
		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('salesdescription');
		}	
		Itemdescription = Itemdescription.substring(0, 100);

		nlapiLogExecution('ERROR', 'Itemdescription', Itemdescription);

		//var html=getLocationScan(beginLoc, reportNo, sku, expQty, skuNo, repInternalId, 
		//	clustNo, whlocation, actEndLocationId, actEndLocation, beginLocId,RecordCount,fromlpno,tolpno,batchno,nextitem,nextlocation,nextitemno,nextqty,Itemdescription,taskpriority);


		var html=getLocationScan(beginLoc, reportNo, sku, expQty, skuNo, repInternalId,clustNo, whlocation, actEndLocationId, actEndLocation, 
				beginLocId,RecordCount,fromlpno,tolpno,batchno,nextitem,nextlocation,nextitemno,nextqty,Itemdescription,getitemid,getlocid,
				taskpriority,replenType,getitem,getitemid,getloc,getlocid,getreportNo,getLanguage);
		response.write(html);
	}
	else {
		try {
			var Reparray = new Array();

			var getLanguage = request.getParameter('hdngetLanguage');
			Reparray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);
			nlapiLogExecution('ERROR', 'hdnReplenType', request.getParameter('hdnReplenType'));


			var st8,st9;
			if( getLanguage == 'es_ES' || getLanguage =='es_AR')
			{

				st8 = "UBICACI&#211;N NO COINCIDEN";
				st9 = "LA UBICACI&#211;N ES NULL";
			}
			else
			{

				st8 = "LOCATION DO NOT MATCH";
				st9 = "LOCATION IS NULL";

			}

			var optedEvent = request.getParameter('cmdPrevious');
			var optedNext=request.getParameter('cmdNEXT');

			var getNumber = request.getParameter('hdngetnumber');
			nlapiLogExecution('Error', 'in response getNumber', getNumber);

			var fromLoc = request.getParameter('enterfromloc');
			var hdnBeginLocation = request.getParameter('hdnBeginLocation');
			var clusterNo = request.getParameter('hdnclustno');	
			var reportNo = request.getParameter('hdnNo');	
			var toloc = request.getParameter('hdnactendlocation');	
			nlapiLogExecution('ERROR', 'getfromloc', fromLoc);
			nlapiLogExecution('ERROR', 'gethdnBeginLocation', hdnBeginLocation);
			Reparray["custparam_screenno"] = 'R2';
			Reparray["custparam_repno"] = request.getParameter('hdnNo');
			Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
			Reparray["custparam_repsku"] = request.getParameter('hdnSku');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnExpQty');
			Reparray["custparam_repid"] = request.getParameter('hdnid');
			Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
			Reparray["custparam_clustno"] = clusterNo;	
			Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnendlocationid');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnendlocation');
			Reparray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
			Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
			Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
			Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');		
			Reparray["custparam_nextitem"] = request.getParameter('hdnnitem');
			Reparray["custparam_nextitemno"] = request.getParameter('hdnSkuNo');
			//Reparray["custparam_taskpriority"] = request.getParameter('hdntaskpriority');
			Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');
			Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
			Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
			Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
			Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
			Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
			Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
			Reparray["custparam_enterfromloc"] = request.getParameter('hdnbeginLocId');
			Reparray["custparam_Itemdescription"] = request.getParameter('hdnItemdescription');
			nlapiLogExecution('ERROR', 'hdnactendlocationid', request.getParameter('hdnactendlocationid'));  
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnactendlocationid');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnactendlocation');

			var recid=request.getParameter('hdnid');



			nlapiLogExecution('ERROR', 'test2', 'test2');  
			nlapiLogExecution('ERROR', 'ot internal which is being processed further', Reparray["custparam_repid"]);  

			if (optedEvent == 'F7') {
				nlapiLogExecution('ERROR', 'REPLENISHMENT LOCATION F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false, Reparray);
				//response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);
			}
			else if(optedNext=='F6')
			{
				var nooftotalRecord=request.getParameter('hdnRecCount');
				if(nooftotalRecord=='' || nooftotalRecord=='')
				{
					var nooftotalRecordlength=getReplenTasks(reportNo,null,null,null,null);
					if(nooftotalRecordlength!=null && nooftotalRecordlength!='')
					{
						Reparray["custparam_noofrecords"] = nooftotalRecordlength.length;
						nooftotalRecord=nooftotalRecordlength.length;
					}

				}
				var tempgetNumber=parseInt(getNumber)+1;
				nlapiLogExecution('ERROR', 'nooftotalRecord', nooftotalRecord);  
				nlapiLogExecution('ERROR', 'tempgetNumber', tempgetNumber);  
				if(parseInt(tempgetNumber)<parseInt(nooftotalRecord))
				{
					getNumber=parseInt(getNumber) + 1;
					Reparray["custparam_number"] = getNumber;
					response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);

				}
				else
				{
					Reparray["custparam_error"] = 'No More Records';
					Reparray["custparam_screenno"] = 'R2';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					return;
				}
				//response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
			}
			else {
				var systemRule=GetSystemRuleForReplen();
				nlapiLogExecution('ERROR', 'systemRule', systemRule);  


				if(toloc!=null && toloc!='' && toloc!='null')
				{
					var locationvalid=getLocation(toloc);
					nlapiLogExecution('ERROR', 'locationvalid', locationvalid);
					if(locationvalid==false)
					{
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
						var expectedqty = transaction.getFieldValue('custrecord_expe_qty');
						var Invrecid = transaction.getFieldValue('custrecord_invref_no');
						transaction.setFieldValue('custrecord_wms_status_flag', 32);
						transaction.setFieldValue('custrecord_act_qty', expectedqty);
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());					

						var result = nlapiSubmitRecord(transaction);

						var invtrecord=nlapiLoadRecord('customrecord_ebiznet_createinv', Invrecid);
						var Invallocqty = invtrecord.getFieldValue('custrecord_ebiz_alloc_qty');
						nlapiLogExecution('ERROR', 'Invallocqty',Invallocqty);
						nlapiLogExecution('ERROR', 'expectedqty',expectedqty);
						if((Invallocqty!=null && Invallocqty!='' && Invallocqty>0) && (expectedqty!=null && expectedqty!='' && expectedqty>0))
							invtrecord.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(Invallocqty)- parseInt(expectedqty));

						var invtrecid = nlapiSubmitRecord(invtrecord, false, true);

						Reparray["custparam_error"] = 'END LOCATION IS INACTIVE STATUS';
						Reparray["custparam_screenno"] = 'R1';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						nlapiLogExecution('ERROR', 'Location is null');
						return;
					}
				}

				if (fromLoc != '') {
					if (fromLoc == hdnBeginLocation) {

						var fromlocationvalid=getfromLocation(fromLoc);
						updateTaskBeginTime(reportNo,request.getParameter('hdnSkuNo'),request.getParameter('hdngetlocid'),
								request.getParameter('hdnSkuNo'),request.getParameter('hdngetlocid'),
								request.getParameter('hdntaskpriority'),request.getParameter('hdnNo'),request.getParameter('hdnReplenType'));
						nlapiLogExecution('ERROR', 'Location Found and Matched', fromLoc);  
						nlapiLogExecution('ERROR', 'End Location Id', Reparray["custparam_reppicklocno"]);
						nlapiLogExecution('ERROR', 'End Location', Reparray["custparam_reppickloc"]);

						if(fromlocationvalid==false)
						{
							var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
							var expectedqty = transaction.getFieldValue('custrecord_expe_qty');
							var Invrecid = transaction.getFieldValue('custrecord_invref_no');
							transaction.setFieldValue('custrecord_wms_status_flag', 32);
							transaction.setFieldValue('custrecord_act_qty', expectedqty);
							transaction.setFieldValue('custrecord_act_end_date', DateStamp());		
							var result = nlapiSubmitRecord(transaction);

							var invtrecord=nlapiLoadRecord('customrecord_ebiznet_createinv', Invrecid);
							var Invallocqty = invtrecord.getFieldValue('custrecord_ebiz_alloc_qty');
							nlapiLogExecution('ERROR', 'Invallocqty',Invallocqty);
							nlapiLogExecution('ERROR', 'expectedqty',expectedqty);
							if((Invallocqty!=null && Invallocqty!='' && Invallocqty>0) && (expectedqty!=null && expectedqty!='' && expectedqty>0))
								invtrecord.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(Invallocqty)- parseInt(expectedqty));

							var invtrecid = nlapiSubmitRecord(invtrecord, false, true);

							Reparray["custparam_error"] = 'FROM LOCATION IS INACTIVE STATUS';
							Reparray["custparam_screenno"] = 'R1';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
							nlapiLogExecution('ERROR', 'Location is null');
							return;
						}
						else
						{
							Reparray["custparam_number"] = getNumber;
							response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);
						}


						/*
						var ItemType = nlapiLookupField('item', request.getParameter('hdnSkuNo'), ['recordType','custitem_ebizbatchlot']);
						//If Lotnumbered item the navigate to batch # scan
						if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.custitem_ebizbatchlot=='T' ) {
							response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);
							return;
						}

						var searchresult = getPickFaceLoc(request.getParameter('hdnSkuNo'));

						if (searchresult != null && searchresult.length > 0) {
							Reparray["custparam_reppicklocno"] = searchresult[0].getValue('custrecord_pickbinloc');
							Reparray["custparam_reppickloc"] = searchresult[0].getText('custrecord_pickbinloc');
							nlapiLogExecution('ERROR', 'Reparray["custparam_reppicklocno"]', Reparray["custparam_reppicklocno"]);
							nlapiLogExecution('ERROR', 'Reparray["custparam_reppickloc"]', Reparray["custparam_reppickloc"]);
							nlapiLogExecution('ERROR', 'Reparray["custparam_repid"]', Reparray["custparam_repid"]);
						}
						updateScanedLoc(request.getParameter('hdnid'));                        
						var searchresults =getReplenTasks(request.getParameter('hdnNo'));

						if (searchresults != null && searchresults.length > 0) {

							Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
							Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku');
							Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
							Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
							Reparray["custparam_repid"] = searchresults[0].getId();

							response.sendRedirect('SUITELET', 'customscript_rf_replen_item', 'customdeploy_rf_replen_item_id', false, Reparray);
						}
						else
						{
							nlapiLogExecution('ERROR', 'getIntransitReplenTasks');
							var searchresults = getIntransitReplenTasks(request.getParameter('hdnNo'));
							nlapiLogExecution('ERROR', 'After getIntransitReplenTasks');

							if (searchresults != null && searchresults.length > 0) {
								nlapiLogExecution('ERROR', 'In getIntransitReplenTasks');	
								Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
								Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku');
								Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
								Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
								Reparray["custparam_repid"] = searchresults[0].getId();

								Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc');
								Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc');                     

								//	if the 'Send' button is clicked without any option value entered,
								//  it has to remain in the same screen ie., Receiving Menu.
								response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
								return;
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false);
								return;
							}
						}
						 */					}
					else {
						Reparray["custparam_error"] = 'LOCATION DO NOT MATCH';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						nlapiLogExecution('ERROR', 'Location not Matched');
					}
				}
				else {
					Reparray["custparam_error"] = 'PLEASE ENTER VALID FROM LOCATION';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					nlapiLogExecution('ERROR', 'Location is null');
				}
			}
		} 
		catch (e) {
			nlapiLogExecution('ERROR', 'exception',e);
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			nlapiLogExecution('ERROR', 'Catch: Location not found');
		}
	}
}


function getReplenTasks(reportNo,getWaveNo,getItemId,getBinLocationId,getPriority)
{
	/*var filters = new Array();
	if(reportNo!=null && reportNo!='')
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	if(getWaveNo!=null && getWaveNo!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));
	 */
	var filters = new Array();
	var vOrdFormat='R';
	var vOrdFormat;
	if(reportNo!=null && reportNo!='')
	{
		nlapiLogExecution('ERROR','replenreportNo',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
		vOrdFormat='R';
	}
	if(getItemId!=null && getItemId!='')
	{
		nlapiLogExecution('ERROR','Replenitem',getItemId);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getItemId));
		vOrdFormat=vOrdFormat + 'I';
	}

	if(getBinLocationId!=null && getBinLocationId!='')
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getBinLocationId);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getBinLocationId));
		vOrdFormat=vOrdFormat + 'L';
	}

	if(getPriority!=null && getPriority!='')
	{
		nlapiLogExecution('ERROR','Replenpriority',getPriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', getPriority));
		vOrdFormat=vOrdFormat + 'P';
	}

	if(reportNo != null && reportNo != "" && getItemId!=null && getItemId!="" && getBinLocationId!=null && getBinLocationId!="" && getPriority!=null && getPriority!="")
	{
		vOrdFormat='ALL';
	}

	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('DEBUG','vRoleLocation',vRoleLocation);

	if(vRoleLocation!=null&&vRoleLocation!="")
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
	//filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));
	try
	{
		var systemRule=GetSystemRuleForReplen();
		nlapiLogExecution('DEBUG','systemRule',systemRule);
		var columns = new Array();
		if(systemRule=='N')
		{

			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
			columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
			/*columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	*/
		}
		else
		{

			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			columns[2] = new nlobjSearchColumn('custrecord_sku');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc');
			columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[9] = new nlobjSearchColumn('name');	
			columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	
		}

		columns[1].setSort(false);
		columns[2].setSort(false);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		if(searchresults != null && searchresults != '')
			nlapiLogExecution('ERROR','searchresults tssssst',searchresults.length);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','searchresults tssssst',exp);
	}
	nlapiLogExecution('DEBUG','searchresults',searchresults);
	return searchresults;
}

//function getReplenTasks(reportNo)
//{
//var filters = new Array();
//filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
//filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
//filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);
//filters[3] = new nlobjSearchFilter('custrecord_act_qty', null, 'isempty');


//var columns = new Array();
//columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
//columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
//columns[2] = new nlobjSearchColumn('custrecord_sku');
//columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
//columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
//columns[5] = new nlobjSearchColumn('custrecord_wms_location');
//columns[6] = new nlobjSearchColumn('custrecord_actendloc');
//columns[7] = new nlobjSearchColumn('custrecord_lpno');
//columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
//columns[9] = new nlobjSearchColumn('name');	
//columns[10] = new nlobjSearchColumn('custrecord_batch_no');

//columns[1].setSort(false);
//columns[2].setSort(false);



//var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
//return searchresults;
//}

function getLocation(gettoloc)
{
	var validlocation=false;
	nlapiLogExecution('ERROR', 'gettoloc', gettoloc);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', gettoloc);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns=new Array();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

	if(searchresults!=null && searchresults!='')
		validlocation=true;

	nlapiLogExecution('ERROR', 'validlocation', validlocation);

	return validlocation;

}

function getfromLocation(fromLoc)
{
	var fromvalidlocation=false;
	nlapiLogExecution('ERROR', 'fromLoc', fromLoc);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', fromLoc);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns=new Array();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

	if(searchresults!=null && searchresults!='')
		fromvalidlocation=true;

	nlapiLogExecution('ERROR', 'fromvalidlocation', fromvalidlocation);

	return fromvalidlocation;
}
function getLocationScan(beginLoc,reportNo,sku,expQty,skuNo,repInternalId, 
		clustNo, whlocation, actEndLocationId, actEndLocation, beginLocId,RecordCount,fromlpno,tolpno,batchno,nextitem,nextlocation,nextitemno,
		nextqty,Itemdescription,getitemid,getlocid,taskpriority,replenType,getitem,getitemid,getloc,getlocid,getreportNo,getLanguage)
{
	var total;

	nlapiLogExecution('ERROR', 'nextlocation in  before1',nextlocation);	
	nlapiLogExecution('ERROR', 'nextqty in  before1',nextqty);	
	nlapiLogExecution('ERROR', 'nextitemno in  before1',nextitemno);	
	nlapiLogExecution('ERROR', 'nextitem in  before1',nextitem);	
	nlapiLogExecution('ERROR', 'replenType in  before1',replenType);	
	nlapiLogExecution('ERROR', 'getitem in  before1',getitem);	
	nlapiLogExecution('ERROR', 'getitemid in  before1',getitemid);	
	nlapiLogExecution('ERROR', 'getloc in  before1',getloc);	
	nlapiLogExecution('ERROR', 'getlocid in  before1',getlocid);	
	nlapiLogExecution('ERROR', 'getreportNo in  before1',getreportNo);	
	nlapiLogExecution('ERROR', 'request.getParameter(custparam_number) in  before1',request.getParameter('custparam_number'));	
	nlapiLogExecution('ERROR', 'getLanguage',getLanguage);	

	if(nextlocation!=null && nextlocation!='')
	{
		beginLoc=nextlocation;
		//total = RecordCount;
	}
	if(nextqty!=null && nextqty!='')
	{
		expQty=nextqty;
	}
	if(nextitemno!=null && nextitemno!='')
	{
		//sku=nextitem;
		var Itype = nlapiLookupField('item', nextitemno, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, nextitemno);		

		//sku = ItemRec.getFieldValue('itemid');
		//sku=nextitem;
		sku=nlapiLookupField('item', nextitemno, 'name');
		nlapiLogExecution('ERROR', 'sku',sku);

	}
	if(nextitem!=null && nextitem!='')
	{
		skuNo=nextitemno;
		//total = RecordCount;
	}

	var getNumber;
	if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
		getNumber = request.getParameter('custparam_number');
	else
		getNumber=0;	
	total = RecordCount;



	nlapiLogExecution('ERROR', 'beginLoc in taskpriority before',beginLoc);	
	nlapiLogExecution('ERROR', 'reportNo in taskpriority before',reportNo);	
	nlapiLogExecution('ERROR', 'sku in taskpriority before',sku);	
	nlapiLogExecution('ERROR', 'expQty in taskpriority before',expQty);	
	nlapiLogExecution('ERROR', 'skuNo in taskpriority before',skuNo);	
	//case# 20125250,if condition added if only report no is given.
	if(reportNo != null && reportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		var itemfilters = new Array();		
		nlapiLogExecution('ERROR', 'skuNo',skuNo);

		itemfilters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
		if(getitemid !=null && getitemid!='' )
			itemfilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
		itemfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
		itemfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
		//itemfilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));

		/*var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc');
		columns[7] = new nlobjSearchColumn('custrecord_lpno');
		columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[9] = new nlobjSearchColumn('name');	
		columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	*/
		var systemRule=GetSystemRuleForReplen();
		if(systemRule=='N')
		{
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
			columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
			/*columns[7] = new nlobjSearchColumn('custrecord_lpno');
		columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[9] = new nlobjSearchColumn('name');	
		columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	*/
		}
		else
		{
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			columns[2] = new nlobjSearchColumn('custrecord_sku');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc');
			columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[9] = new nlobjSearchColumn('name');	
			columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	
		}

		columns[1].setSort(false);
		columns[2].setSort(false);


		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, itemfilters, columns);
		if (searchresults != null) 
		{
			nlapiLogExecution('ERROR', 'Itemsearchresults.length', searchresults.length);  
			total = searchresults.length;

			if(searchresults.length==getNumber)
			{
				getNumber=0;
			}

			if(parseFloat(getNumber)<parseFloat(total))
			{
				var recNo=0;
				if(getNumber!=0)
					recNo=parseFloat(getNumber);

				var searchresult = searchresults[recNo];			 

				/*sku = searchresult.getText('custrecord_sku');
				skuNo = searchresult.getValue('custrecord_ebiz_sku_no');
				beginLoc = 	searchresult.getText('custrecord_actbeginloc');
				expQty = searchresult.getValue('custrecord_expe_qty');

				whlocation = searchresult.getValue('custrecord_wms_location');
				actEndLocationId = searchresult.getValue('custrecord_actendloc');
				actEndLocation = searchresult.getText('custrecord_actendloc');
				beginLocId = searchresult.getValue('custrecord_actbeginloc');						
				repInternalId = searchresult.getId();*/
				//taskpriority = searchresult.getValue('custrecord_taskpriority');
				if(systemRule=='N')
				{
					sku = searchresult.getText('custrecord_sku',null,'group');
					skuNo = searchresult.getValue('custrecord_ebiz_sku_no',null,'group');
					beginLoc = 	searchresult.getText('custrecord_actbeginloc',null,'group');
					expQty = searchresult.getValue('custrecord_expe_qty',null,'sum');

					whlocation = searchresult.getValue('custrecord_wms_location',null,'group');
					actEndLocationId = searchresult.getValue('custrecord_actendloc',null,'group');
					actEndLocation = searchresult.getText('custrecord_actendloc',null,'group');
					beginLocId = searchresult.getValue('custrecord_actbeginloc',null,'group');						


				}
				else
				{


					sku = searchresult.getText('custrecord_sku');
					skuNo = searchresult.getValue('custrecord_ebiz_sku_no');
					beginLoc = 	searchresult.getText('custrecord_actbeginloc');
					expQty = searchresult.getValue('custrecord_expe_qty');

					whlocation = searchresult.getValue('custrecord_wms_location');
					actEndLocationId = searchresult.getValue('custrecord_actendloc');
					actEndLocation = searchresult.getText('custrecord_actendloc');
					beginLocId = searchresult.getValue('custrecord_actbeginloc');						
					repInternalId = searchresult.getId();
				}
				var Itype = nlapiLookupField('item', skuNo, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, skuNo);			
				var Itemdescription='';			

				var getItemName = ItemRec.getFieldValue('itemid');


				if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
				{	
					Itemdescription = ItemRec.getFieldValue('description');
				}
				else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
				{	
					Itemdescription = ItemRec.getFieldValue('salesdescription');
				}	
				Itemdescription = Itemdescription.substring(0, 100);

			}
			else
			{
				var Reparray=new Array();
				Reparray["custparam_error"] = 'No More Records';
				Reparray["custparam_screenno"] = 'R2';
				//Reparray["custparam_reppicklocno"]=actEndLocationId;
				Reparray["custparam_repskuno"]=skuNo;
				//Reparray["custparam_taskpriority"]=taskpriority;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				return;
			}
		}
	}	
	//End
	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		var itemfilters = new Array();		
		nlapiLogExecution('ERROR', 'skuNo',skuNo);
		if(getreportNo != null && getreportNo != '')
			itemfilters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));
		itemfilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
		itemfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
		itemfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
		//itemfilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));

		var systemRule=GetSystemRuleForReplen();
		if(systemRule=='N')
		{
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
			columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
			/*columns[7] = new nlobjSearchColumn('custrecord_lpno');
		columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[9] = new nlobjSearchColumn('name');	
		columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	*/
		}
		else
		{
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			columns[2] = new nlobjSearchColumn('custrecord_sku');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc');
			columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[9] = new nlobjSearchColumn('name');	
			columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	
		}

		columns[1].setSort(false);
		columns[2].setSort(false);
		//columns[3].setSort(false);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, itemfilters, columns);
		if (searchresults != null) 
		{
			nlapiLogExecution('ERROR', 'Itemsearchresults.length', searchresults.length);  
			total = searchresults.length;

			if(searchresults.length==getNumber)
			{
				getNumber=0;
			}

			if(parseFloat(getNumber)<parseFloat(total))
			{
				var recNo=0;
				if(getNumber!=0)
					recNo=parseFloat(getNumber);

				var searchresult = searchresults[recNo];			 
				if(systemRule=='N')
				{
					sku = searchresult.getText('custrecord_sku',null,'group');
					skuNo = searchresult.getValue('custrecord_ebiz_sku_no',null,'group');
					beginLoc = 	searchresult.getText('custrecord_actbeginloc',null,'group');
					expQty = searchresult.getValue('custrecord_expe_qty',null,'sum');

					whlocation = searchresult.getValue('custrecord_wms_location',null,'group');
					actEndLocationId = searchresult.getValue('custrecord_actendloc',null,'group');
					actEndLocation = searchresult.getText('custrecord_actendloc',null,'group');
					beginLocId = searchresult.getValue('custrecord_actbeginloc',null,'group');						


				}
				else
				{


					sku = searchresult.getText('custrecord_sku');
					skuNo = searchresult.getValue('custrecord_ebiz_sku_no');
					beginLoc = 	searchresult.getText('custrecord_actbeginloc');
					expQty = searchresult.getValue('custrecord_expe_qty');

					whlocation = searchresult.getValue('custrecord_wms_location');
					actEndLocationId = searchresult.getValue('custrecord_actendloc');
					actEndLocation = searchresult.getText('custrecord_actendloc');
					beginLocId = searchresult.getValue('custrecord_actbeginloc');						
					repInternalId = searchresult.getId();
				}
				//taskpriority = searchresult.getValue('custrecord_taskpriority');

				var Itype = nlapiLookupField('item', skuNo, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, skuNo);			
				var Itemdescription='';			

				var getItemName = ItemRec.getFieldValue('itemid');


				if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
				{	
					Itemdescription = ItemRec.getFieldValue('description');
				}
				else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
				{	
					Itemdescription = ItemRec.getFieldValue('salesdescription');
				}	
				Itemdescription = Itemdescription.substring(0, 100);

			}
			else
			{
				var Reparray=new Array();
				Reparray["custparam_error"] = 'No More Records';
				Reparray["custparam_screenno"] = 'R2';
				//Reparray["custparam_reppicklocno"]=actEndLocationId;
				Reparray["custparam_repskuno"]=skuNo;
				//Reparray["custparam_taskpriority"]=taskpriority;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				return;
			}
		}
	}
	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		var primarylocfilters = new Array();		

		nlapiLogExecution('ERROR', 'actEndLocationId',actEndLocationId);
		if(getreportNo != null && getreportNo != '')
			primarylocfilters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));
		primarylocfilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
		primarylocfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
		primarylocfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
		//primarylocfilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));
		if(getitemid != null && getitemid != '')
			primarylocfilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));


		var systemRule=GetSystemRuleForReplen();
		if(systemRule=='N')
		{
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
			columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
			/*columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[9] = new nlobjSearchColumn('name');	
			columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	*/
		}
		else
		{


			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			columns[2] = new nlobjSearchColumn('custrecord_sku');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc');
			columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[9] = new nlobjSearchColumn('name');	
			columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	
			//columns[11] = new nlobjSearchColumn('id');
		}

		columns[1].setSort(false);
		columns[2].setSort(false);



		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, primarylocfilters, columns);
		if (searchresults != null) 
		{
			nlapiLogExecution('ERROR', 'PrimaryLocsearchresults.length', searchresults.length);  
			total = searchresults.length;

			if(searchresults.length==getNumber)
			{
				getNumber=0;
			}

			if(parseFloat(getNumber)<parseFloat(total))
			{
				var recNo=0;
				if(getNumber!=0)
					recNo=parseFloat(getNumber);

				var searchresult = searchresults[recNo];			 
				nlapiLogExecution('ERROR', 'recNo in item filter', recNo); 

				if(systemRule=='N')
				{
					sku = searchresult.getText('custrecord_sku',null,'group');
					skuNo = searchresult.getValue('custrecord_ebiz_sku_no',null,'group');
					beginLoc = 	searchresult.getText('custrecord_actbeginloc',null,'group');
					expQty = searchresult.getValue('custrecord_expe_qty',null,'sum');

					whlocation = searchresult.getValue('custrecord_wms_location',null,'group');
					//Case# 201410209
					//actEndLocationId = searchresult.getValue('custparam_reppicklocno',null,'group');
					//actEndLocation = searchresult.getText('custparam_reppickloc',null,'group');
					actEndLocationId = searchresult.getValue('custrecord_actendloc',null,'group');
					actEndLocation = searchresult.getText('custrecord_actendloc',null,'group');
					
					beginLocId = searchresult.getValue('custrecord_actbeginloc',null,'group');	
					


				}
				else
				{


					sku = searchresult.getText('custrecord_sku');
					skuNo = searchresult.getValue('custrecord_ebiz_sku_no');
					beginLoc = 	searchresult.getText('custrecord_actbeginloc');
					expQty = searchresult.getValue('custrecord_expe_qty');

					whlocation = searchresult.getValue('custrecord_wms_location');
					actEndLocationId = searchresult.getValue('custrecord_actendloc');
					actEndLocation = searchresult.getText('custrecord_actendloc');
					beginLocId = searchresult.getValue('custrecord_actbeginloc');						
					repInternalId = searchresult.getId();
					//taskpriority = searchresult.getValue('custrecord_taskpriority');
				}
				var Itype = nlapiLookupField('item', skuNo, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, skuNo);			
				var Itemdescription='';			

				var getItemName = ItemRec.getFieldValue('itemid');


				if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
				{	
					Itemdescription = ItemRec.getFieldValue('description');
				}
				else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
				{	
					Itemdescription = ItemRec.getFieldValue('salesdescription');
				}	
				Itemdescription = Itemdescription.substring(0, 100);

			}
			else
			{
				var Reparray=new Array();
				Reparray["custparam_error"] = 'No More Records';
				Reparray["custparam_screenno"] = 'R2';
				Reparray["custparam_reppicklocno"]=actEndLocationId;
				//Reparray["custparam_repskuno"]=skuNo;
				//Reparray["custparam_taskpriority"]=taskpriority;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				return;
			}
		}
	}
	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))

	{			

		var priorityfilters = new Array();		
		if(getreportNo != null && getreportNo != '')
			priorityfilters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));
		priorityfilters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

		priorityfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
		priorityfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
		//priorityfilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));
		if(getitemid != null && getitemid != '')
			priorityfilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

		if(getlocid != null && getlocid != '')
			priorityfilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));



		var systemRule=GetSystemRuleForReplen();
		if(systemRule=='N')
		{
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
			columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
			/*columns[7] = new nlobjSearchColumn('custrecord_lpno');
				columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
				columns[9] = new nlobjSearchColumn('name');	
				columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	*/
		}
		else
		{
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			columns[2] = new nlobjSearchColumn('custrecord_sku');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc');
			columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[9] = new nlobjSearchColumn('name');	
			columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	
			//columns[11] = new nlobjSearchColumn('id');
		}

		columns[1].setSort(false);
		columns[2].setSort(false);


		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, priorityfilters, columns);
		if (searchresults != null) 
		{
			nlapiLogExecution('ERROR', 'Prioritysearchresults.length', searchresults.length);  
			total = searchresults.length;

			if(searchresults.length==getNumber)
			{
				getNumber=0;
			}
			nlapiLogExecution('ERROR', 'getNumber in taskpriority ',getNumber);
			if(parseFloat(getNumber)<parseFloat(total))
			{
				var recNo=0;
				if(getNumber!=0)
					recNo=parseFloat(getNumber);
				nlapiLogExecution('ERROR', 'recNo in taskpriority ',recNo);	
				var searchresult = searchresults[recNo];			 
				if(systemRule=='N')
				{
					sku = searchresult.getText('custrecord_sku',null,'group');
					skuNo = searchresult.getValue('custrecord_ebiz_sku_no',null,'group');
					beginLoc = 	searchresult.getText('custrecord_actbeginloc',null,'group');
					expQty = searchresult.getValue('custrecord_expe_qty',null,'sum');

					whlocation = searchresult.getValue('custrecord_wms_location',null,'group');
					actEndLocationId = searchresult.getValue('custrecord_actendloc',null,'group');
					actEndLocation = searchresult.getText('custrecord_actendloc',null,'group');
					beginLocId = searchresult.getValue('custrecord_actbeginloc',null,'group');						


				}
				else
				{
					sku = searchresult.getText('custrecord_sku');
					skuNo = searchresult.getValue('custrecord_ebiz_sku_no');
					beginLoc = 	searchresult.getText('custrecord_actbeginloc');
					expQty = searchresult.getValue('custrecord_expe_qty');

					whlocation = searchresult.getValue('custrecord_wms_location');
					actEndLocationId = searchresult.getValue('custrecord_actendloc');
					actEndLocation = searchresult.getText('custrecord_actendloc');
					beginLocId = searchresult.getValue('custrecord_actbeginloc');						
					repInternalId = searchresult.getId();
					taskpriority = searchresult.getValue('custrecord_taskpriority');
					reportNo = searchresult.getValue('name');
				}

				var Itype = nlapiLookupField('item', skuNo, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, skuNo);			
				var Itemdescription='';			

				var getItemName = ItemRec.getFieldValue('itemid');


				if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
				{	
					Itemdescription = ItemRec.getFieldValue('description');
				}
				else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
				{	
					Itemdescription = ItemRec.getFieldValue('salesdescription');
				}	
				Itemdescription = Itemdescription.substring(0, 100);

			}
			else
			{
				var Reparray=new Array();
				Reparray["custparam_error"] = 'No More Records';
				Reparray["custparam_screenno"] = 'R2';
				//Reparray["custparam_reppicklocno"]=actEndLocationId;
				//Reparray["custparam_repskuno"]=skuNo;
				//Reparray["custparam_taskpriority"]=taskpriority;
				Reparray["custparam_entertaskpriority"]=taskpriority;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				return;
			}

		}
	}



	/*var filters = new Array();

	if(skuNo!=null && skuNo!='')
		{
		nlapiLogExecution('ERROR', 'skuNo',skuNo);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', skuNo));
		}

	if(actEndLocationId!=null && actEndLocationId!='')
		{
		nlapiLogExecution('ERROR', 'actEndLocationId',actEndLocationId);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', actEndLocationId));
		}

	if(taskpriority!=null && taskpriority!='')
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	

	columns[1].setSort(false);
	columns[2].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if (searchresults != null) 
	{
		nlapiLogExecution('ERROR', 'searchresults.length', searchresults.length);  
		total = searchresults.length;

		if(searchresults.length==getNumber)
		{
			getNumber=0;
		}

		if(parseFloat(getNumber)<parseFloat(total))
		{
			var recNo=0;
			if(getNumber!=0)
				recNo=parseFloat(getNumber);

			var searchresult = searchresults[recNo];			 

			sku = searchresult.getText('custrecord_sku');
			skuNo = searchresult.getValue('custrecord_ebiz_sku_no');
			beginLoc = 	searchresult.getText('custrecord_actbeginloc');
			expQty = searchresult.getValue('custrecord_expe_qty');

			whlocation = searchresult.getValue('custrecord_wms_location');
			actEndLocationId = searchresult.getValue('custparam_reppicklocno');
			actEndLocation = searchresult.getValue('custparam_reppickloc');
			beginLocId = searchresult.getValue('custrecord_actbeginloc');						

			taskpriority = searchresult.getValue('custrecord_taskpriority');

			var Itype = nlapiLookupField('item', skuNo, 'recordType');
			var ItemRec = nlapiLoadRecord(Itype, skuNo);			
			var Itemdescription='';			

			var getItemName = ItemRec.getFieldValue('itemid');


			if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
			{	
				Itemdescription = ItemRec.getFieldValue('description');
			}
			else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
			{	
				Itemdescription = ItemRec.getFieldValue('salesdescription');
			}	
			Itemdescription = Itemdescription.substring(0, 20);

		}
		else
		{
			var Reparray=new Array();
			Reparray["custparam_error"] = 'No More Records';
			Reparray["custparam_screenno"] = 'R2';
			Reparray["custparam_reppicklocno"]=actEndLocationId;
			Reparray["custparam_repskuno"]=skuNo;
			Reparray["custparam_taskpriority"]=taskpriority;
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			return;
		}
	}*/

	nlapiLogExecution('ERROR', 'actEndLocationId',actEndLocationId);	
	nlapiLogExecution('ERROR', 'beginLoc in taskpriority after',beginLoc);	
	nlapiLogExecution('ERROR', 'reportNo in taskpriority after',reportNo);	
	nlapiLogExecution('ERROR', 'sku in taskpriority after',sku);	
	nlapiLogExecution('ERROR', 'expQty in taskpriority after',expQty);	


	//case20125658 and 20125602 start :spanish conversion

	var st0,st1,st2,st3,st4,st5,st6,st7,st8;
	if( getLanguage == 'es_ES' || getLanguage =='es_AR')
	{
		st0 = "DE UBICACI&#211;N :";
		st1 = "ART&#205;CULO :";
		st2 = "DESCRIPCI&#211;N DE ART&#205;CULO :";
		st3 = "CANTIDAD :";
		st4 = "ENTRAR / SCAN DE UBICACI&#211;N :";
		st5 = "ENVIAR";
		st6 = "ANTERIOR";
		st7 = "SIGUIENTE";
		st8 = "DE";

	}
	else
	{
		st0 = "FROM LOCATION :";
		st1 = "ITEM:";
		st2 = "ITEM DESCRIPTION: ";
		st3 = "QUANTITY: ";
		st4 = "ENTER/SCAN FROM LOCATION:";
		st5 = "SEND";
		st6 = "PREV";
		st7 = "NEXT";
		st8 = "OF";

	}


	//case20125658 and 20125602 end

	var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_location'); 
	var html = "<html><head><title>REPLENISHMENT LOCATION</title>";
	html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
	html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
	
	//html = html + " document.getElementById('enterfromloc').focus();";   
	//Case# 20148749 Refresh Functionality starts
	html = html + "var version = navigator.appVersion;";
	html = html + "document.onkeydown = function (e) {";
	html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
	html = html + "if ((version.indexOf('MSIE') != -1)) { ";
	html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
	html = html + "else {if (keycode == 116)return false;}";
	html = html + "};";
	//Case# 20148749 Refresh Functionality ends
	html = html + "function stopRKey(evt) { ";
	//html = html + "	  alert('evt');";
	html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
	html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
	html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
	//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

	html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
	html = html + "	  alert('System Processing, Please wait...');";
	html = html + "	  return false;}} ";
	html = html + "	} ";

	html = html + "	document.onkeypress = stopRKey; ";


	html = html + "</script>";
	html = html +functionkeyHtml;
	html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
	html = html + "	<form name='_rf_replenishment_location' method='POST'>";
	html = html + "		<table>";

	html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " "+st8+" <label>" + parseFloat(total) + "</label>";
	html = html + "			</tr>";

	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st0+"  <label>" + beginLoc + "</label></td>";
	html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
	html = html + "				<input type='hidden' name='hdnBeginLocation' value='" + beginLoc + "'>";// case# 201413297
	html = html + "				<input type='hidden' name='hdnSku' value='" + sku + "'>";
	html = html + "				<input type='hidden' name='hdnExpQty' value=" + expQty + ">";
	html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
	html = html + "				<input type='hidden' name='hdnid' value=" + repInternalId + ">";
	html = html + "				<input type='hidden' name='hdnclustno' value=" + clustNo + ">";
	html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
	html = html + "				<input type='hidden' name='hdnactendlocationid' value=" + actEndLocationId + ">";
	html = html + "				<input type='hidden' name='hdnactendlocation' value=" + actEndLocation + ">";
	html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
	html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
	html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
	html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
	html = html + "				<input type='hidden' name='hdnbatchno' value=" + batchno + ">";
	html = html + "				<input type='hidden' name='hdnnitem' value=" + nextitem + ">";
	html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
	html = html + "				<input type='hidden' name='hdngetnumber' value=" + getNumber + ">";
	html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";


	html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
	html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
	html = html + "				<input type='hidden' name='hdngetloc' value='" + getloc + "'>";
	html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
	html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
	html = html + "				<input type='hidden' name='hdnItemdescription' value='" + Itemdescription + "'>";
	html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	

	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st1+"  <label>" + sku + "</label></td>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st2+" <label>" + Itemdescription + "</label></td>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+ st3+ " <label>" + expQty + "</label></td>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>"+st4;
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input name='enterfromloc' id='enterfromloc' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>"+st5+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdNEXT.disabled=true; return false'/>";
	if(parseFloat(total)>1)
		html = html + "					"+st7+" <input name='cmdNEXT' type='submit' value='F6'/>"+st6+" <input name='cmdPrevious' type='submit' value='F7'/>";
	else
		html = html + "					"+st6+" <input name='cmdPrevious' type='submit' value='F7'/>";
	//html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "		 </table>";
	html = html + "	</form>";
	//Case# 20148882 (added Focus Functionality for Textbox)
	html = html + "<script type='text/javascript'>document.getElementById('enterfromloc').focus();</script>";
	html = html + "</body>";
	html = html + "</html>";
	return html;
}

/*
//Get Pick Face Location
function getPickFaceLoc(skuNo)
{
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skuNo);
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_pickbinloc');

	var searchresult = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filter, column);
	return searchresult;
}

function updateScanedLoc(recId)
{
	try {
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
		transaction.setFieldValue('custrecord_wms_status_flag', 24);
		var result = nlapiSubmitRecord(transaction);
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed', e);
	}
}

function getReplenTasks(reportNo)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

function getIntransitReplenTasks(reportNo)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [24]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_actendloc');    
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;    
}
 */

function GetSystemRuleForReplen()
{
	try
	{
		var rulevalue='Y';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for Replen?'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null && searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);


		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}

function updateTaskBeginTime(reportNo,sku,endlocation,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('Debug', 'Into updateTaskBeginTime', reportNo);

	nlapiLogExecution('Debug', 'reportNo', reportNo);
	nlapiLogExecution('Debug', 'sku', sku);
	nlapiLogExecution('Debug', 'endlocation', endlocation);
	nlapiLogExecution('Debug', 'getitemid in replen task',getitemid);
	nlapiLogExecution('Debug', 'getlocid in replen task',getlocid);
	nlapiLogExecution('Debug', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('Debug', 'getreportNo in replen task',getreportNo);
	nlapiLogExecution('Debug', 'replenType',replenType);

	var filters = new Array();

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}

	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	//filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isnotempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_batch_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_sku_status');
	columns[5] = new nlobjSearchColumn('custrecord_act_qty');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_reversalqty');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!='')
	{
		nlapiLogExecution('Debug', 'searchresults length ',searchresults.length);

		for (var s = 0; s < searchresults.length; s++) 
		{
			//*** The following code is added by Satish.N on 12-Mar-2013 to update task begin time and user name***//

			var currentContext = nlapiGetContext();  
			var currentUserID = currentContext.getUser();
			nlapiLogExecution('ERROR', 'currentUserID', currentUserID);

			var fields=new Array();
			fields[0]='custrecord_taskassignedto';		
			fields[1]='custrecord_actualbegintime';	

			var Values=new Array();
			Values[0]=currentUserID;	
			Values[1]=TimeStamp();	

			var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',searchresults[s].getId(),fields,Values);

			//*** Upto here ***//
		}
	}

	nlapiLogExecution('Debug', 'Out of updateTaskBeginTime', reportNo);

}
