/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/ebiz_Replenishment_Report_SL.js,v $
 *     	   $Revision: 1.2.4.3.8.4 $
 *     	   $Date: 2015/07/27 13:45:40 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Replenishment_Report_SL.js,v $
 * Revision 1.2.4.3.8.4  2015/07/27 13:45:40  schepuri
 * case# 201413592
 *
 * Revision 1.2.4.3.8.3  2015/06/10 14:27:12  schepuri
 * case# 201413048
 *
 * Revision 1.2.4.3.8.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.2.4.3.8.1  2013/02/26 12:52:09  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.2.4.3  2012/08/27 16:29:24  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * fiskreplenissues
 *
 * Revision 1.2.4.2  2012/08/06 07:00:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Print PDF functionality.
 *
 * Revision 1.2.4.1  2012/07/30 16:52:09  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed the caption
 *
 * Revision 1.2  2011/07/20 14:40:01  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.10  2011/07/20 12:57:48  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.4  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function ebiznet_replensihment_report(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Replenishment Report');

		//$LN, LOT/Batch # Validation...
		//form.setScript('customscript_chknlpvalidation');
		//form.setScript('customscript_chknlpputwvalidation');		

		//Calling Client Script for validating Location
		// form.setScript('customscript_cpclientvalidations');
		//To get PO location and recordType.       


		var sublist = form.addSubList("custpage_items", "list", "Replen Tasks");
		//sublist.addField("custpage_polocgen", "checkbox", "Confirm").setDefaultValue('T');
		sublist.addField("custpage_slno", "text", "SL No");
		sublist.addField("custpage_line", "text", "Report #");
		sublist.addField("custpage_chkn_fromlp", "text", "From LP#");
		sublist.addField("custpage_chkn_lp", "text", "TO LP#");
		sublist.addField("custpage_iteminternalid", "text", "Item", "item");//.setDisplayType('hidden');
		//sublist.addField("custpage_itemstatus", "text", "Item Status", "customrecord_ebiznet_sku_status").setDefaultValue('1');
		//sublist.addField("custpage_item_pc", "text", "Pack Code").setDefaultValue('1').setDisplayType('hidden');
		sublist.addField("custpage_lotbatch", "text", "Lot/Batch #");
		sublist.addField("custpage_quantity", "text", "Quantity");
		sublist.addField("custpage_location", "text", "Actual Begin Location", "customrecord_ebiznet_location");
		sublist.addField("custpage_endlocation", "text", "Actual End Location", "customrecord_ebiznet_location");
		sublist.addField("custpage_tasktype", "text", "Task Type", "customrecord_ebiznet_tasktype");

		sublist.addField("custpage_pointid", "text", "poIntid").setDisplayType('hidden');
		sublist.addField("custpage_poskuid", "text", "skuIntid").setDisplayType('hidden');
		sublist.addField("custpage_poskudesc", "text", "skudesc").setDisplayType('hidden');
		sublist.addField("custpage_serialno", "text", "SerialNo").setDisplayType('hidden');
		//sublist.addField("custpage_serialnos", "textarea", "Serial #");//.setDisplayType('entry');
		sublist.addField("custpage_serialnos", "textarea", "Serial #").setDisplayType('hidden');

		sublist.addField("custpage_batchwithqty", "textarea", "Batch with quantiy").setDisplayType('hidden');
		sublist.addField("custpage_line_item", "text", "LineItem").setDisplayType('hidden');
		sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');


		//var autobutton = form.addButton('custpage_confirmputaway', 'Save');
		//var button = form.addSubmitButton('Confirm');



		//LN, define search filters
		var filters = new Array();
		//var status = new Array("N", "L");
		//if (pointid != "" && pointid != null) {
		nlapiLogExecution('Error', 'custparam_poid', request.getParameter('custparam_poid'));
		filters[0] = new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_poid'));
		//}
		//else {
		//    filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', 1527);
		//}

		filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
		filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9,20]);
		//filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', [3]);
		//filters[2] = new nlobjSearchFilter('custrecord_actendloc', null, 'is', isNaN);
		// filters[3] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

		var columns = new Array();
		var systemRule=GetSystemRuleForReplen();
		if(systemRule=='N')
		{

			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group'); 
			columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group'); //Retreive SKU Details   
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
			columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
			columns[8] = new nlobjSearchColumn('custrecord_tasktype',null,'group');
			columns[9] = new nlobjSearchColumn('custrecord_from_lp_no',null,'group');
			columns[10] = new nlobjSearchColumn('custrecord_lpno',null,'group');


			columns[1].setSort(false);
			columns[2].setSort(false);
		}
		else
		{
			columns[0] = new nlobjSearchColumn('custrecord_line_no');
			columns[1] = new nlobjSearchColumn('custrecord_lpno');
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[3] = new nlobjSearchColumn('custrecord_sku_status');
			columns[4] = new nlobjSearchColumn('custrecord_batch_no');
			columns[5] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[6] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[7] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');    	
			columns[8] = new nlobjSearchColumn('custrecord_sku'); //Retreive SKU Details
			columns[9] = new nlobjSearchColumn('custrecord_skudesc'); //Retreive SKU desc Details
			columns[10] = new nlobjSearchColumn('custrecord_packcode');
			//Batch with qty.
			columns[11] = new nlobjSearchColumn('custrecord_lotnowithquantity');
			columns[12] = new nlobjSearchColumn('custrecord_comp_id');
			columns[13] = new nlobjSearchColumn('custrecord_actendloc');
			columns[14] = new nlobjSearchColumn('custrecord_tasktype');
			columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[7].setSort(); // case# 201413592
			columns[8].setSort();
		}

		//LN, execute the  search, passing null filter and return columns
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		var lp, line, itemname, itemstatus, lotbatch, qty, loc, itemid, itemdesc, itempackcode,batchwithqty, endloc, tasktype;
		var rcptqty = new Array();

		/*
         LN, Searching records from custom record and dynamically adding to the sublist lines
		 */
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			nlapiLogExecution('Error', 'searchresults.length', searchresults.length);
			var searchresult = searchresults[i];
			/*
             if (searchresult.getValue('custrecord_line_no') == i) {
             rcptqty[i] += searchresult.getValue('custrecord_expe_qty');
             }
			 */
			if(systemRule=='N')
			{

				line = searchresult.getValue('custrecord_line_no');
				lp = searchresult.getValue('custrecord_lpno',null,'group');
				itemid = searchresult.getText('custrecord_sku',null,'group');
				//itemstatus = searchresult.getValue('custrecord_sku_status');
				lotbatch = searchresult.getValue('custrecord_batch_no',null,'group');
				qty = searchresult.getValue('custrecord_expe_qty',null,'sum');
				loc = searchresult.getText('custrecord_actbeginloc',null,'group');
				itemname = searchresult.getText('custrecord_sku',null,'group');
				//itemdesc = searchresult.getValue('custrecord_skudesc');
				//itempackcode = searchresult.getValue('custrecord_packcode');
				//batchwithqty =  searchresult.getValue('custrecord_lotnowithquantity');
				endloc =  searchresult.getText('custrecord_actendloc',null,'group');
				tasktype =  searchresult.getText('custrecord_tasktype',null,'group');
				var fromlp =  searchresult.getValue('custrecord_from_lp_no',null,'group');
			}
			else
			{
				line = searchresult.getValue('custrecord_line_no');
				lp = searchresult.getValue('custrecord_lpno');
				itemid = searchresult.getText('custrecord_ebiz_sku_no');
				//itemstatus = searchresult.getValue('custrecord_sku_status');
				lotbatch = searchresult.getValue('custrecord_batch_no');
				qty = searchresult.getValue('custrecord_expe_qty');
				loc = searchresult.getText('custrecord_actbeginloc');
				itemname = searchresult.getText('custrecord_sku');
				//itemdesc = searchresult.getValue('custrecord_skudesc');
				//itempackcode = searchresult.getValue('custrecord_packcode');
				//batchwithqty =  searchresult.getValue('custrecord_lotnowithquantity');
				endloc =  searchresult.getText('custrecord_actendloc');
				tasktype =  searchresult.getText('custrecord_tasktype');
				var fromlp =  searchresult.getValue('custrecord_from_lp_no');
			}

			//retserialcsv = getSerialNoCSV(arrayLP, lp);
			//nlapiLogExecution('DEBUG','LOC',searchresult.getValue('custrecord_actbeginloc'));
			vCompany =  searchresult.getValue('custrecord_comp_id');
			form.getSubList('custpage_items').setLineItemValue('custpage_slno', i + 1, ""+(i +1));
			form.getSubList('custpage_items').setLineItemValue('custpage_line', i + 1, request.getParameter('custparam_poid'));
			form.getSubList('custpage_items').setLineItemValue('custpage_chkn_fromlp', i + 1, fromlp);
			form.getSubList('custpage_items').setLineItemValue('custpage_chkn_lp', i + 1, lp);
			form.getSubList('custpage_items').setLineItemValue('custpage_iteminternalid', i + 1, itemname);            
			form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, lotbatch);
			form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, qty);
			form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, loc);			
			form.getSubList('custpage_items').setLineItemValue('custpage_endlocation', i + 1, endloc);
			form.getSubList('custpage_items').setLineItemValue('custpage_tasktype', i + 1, tasktype);
		}
		//nlapiSelectLineItem('custpage_items', 1);
		// form.addPageLink('crosslink', 'Back to PO Check-In', nlapiResolveURL('RECORD', 'purchaseorder', request.getParameter('custparam_poid')))
		//form.addPageLink('crosslink', 'Back to PO', nlapiResolveURL('RECORD', tranType, request.getParameter('custparam_poid')))
		var Replenid=request.getParameter('custparam_poid');
		form.setScript('customscript_replenishmnt_report');
		form.addButton('custpage_print','Print','Printreport('+Replenid+')');
		response.writePage(form);
	}
    
	
}

function Printreport(Replenid){

	nlapiLogExecution('ERROR', 'into Printreport repno',Replenid);
	var REPPDFURL = nlapiResolveURL('SUITELET', 'customscript_replenishment_report_pdf', 'customdeploy_replenishment_report_pdf_di');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		REPPDFURL = 'https://system.netsuite.com' + REPPDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			REPPDFURL = 'https://system.sandbox.netsuite.com' + REPPDFURL;				
		}*/
	nlapiLogExecution('ERROR', 'WO PDF URL',REPPDFURL);					
	REPPDFURL = REPPDFURL + '&custparam_repid='+ Replenid;
	nlapiLogExecution('ERROR', 'REPPDFURL',REPPDFURL);
//	alert(WavePDFURL);
	window.open(REPPDFURL);

}

function GetSystemRuleForReplen()
{
	try
	{
		var rulevalue='Y';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for Replen?'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null && searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);


		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}



