/***************************************************************************
�����������������������
������������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CreateInventoryFIFODate.js,v $
*� $Revision: 1.1.2.2.4.4.2.3.2.2 $
*� $Date: 2015/02/23 17:09:37 $
*� $Author: rrpulicherla $
*� $Name: t_eBN_2015_1_StdBundle_1_1 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_CreateInventoryFIFODate.js,v $
*� Revision 1.1.2.2.4.4.2.3.2.2  2015/02/23 17:09:37  rrpulicherla
*� Case#201411317
*�
*� Revision 1.1.2.2.4.4.2.3.2.1  2014/10/15 16:09:07  skavuri
*� Case# 201410633 Std bundle issue fixed
*�
*� Revision 1.1.2.2.4.4.2.3  2014/05/30 00:34:19  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.1.2.2.4.4.2.2  2013/04/17 16:02:35  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.1.2.2.4.4.2.1  2013/03/05 13:35:38  rmukkera
*� Merging of lexjet Bundle files to Standard bundle
*�
*� Revision 1.1.2.2.4.4  2013/02/07 08:48:07  skreddy
*� CASE201112/CR201113/LOG201121
*�  RF Lot auto generating FIFO enhancement
*�
*� Revision 1.1.2.2.4.3  2012/10/05 06:34:12  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting Multiple language into given suggestions
*�
*� Revision 1.1.2.2.4.2  2012/09/26 12:33:02  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.1.2.2.4.1  2012/09/21 15:01:30  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.1.2.2  2012/05/17 14:14:01  spendyala
*� CASE201112/CR201113/LOG201121
*� parameter passing to query string  missing issue resolved.
*�
*� Revision 1.1.2.1  2012/05/17 13:26:37  spendyala
*� CASE201112/CR201113/LOG201121
*� New script for FIFO date.
*�
*
****************************************************************************/


/**
 * @param request
 * @param response
 */
function CreateInventoryFIFODate(request, response){
	if (request.getMethod() == 'GET') {

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getLocationId = request.getParameter('custparam_locationId');
		var getBinLocationId = request.getParameter('custparam_binlocationid');
		var getBinLocationName = request.getParameter('custparam_binlocationname');    
		var getItemId = request.getParameter('custparam_itemid');
		var getItem = request.getParameter('custparam_item');
		var getItemType = request.getParameter('custparam_ItemType');
		var getQuantity = request.getParameter('custparam_quantity');
		var getBatchNo = request.getParameter('custparam_BatchNo');
		var getMfgDate = request.getParameter('custparam_mfgdate');
		var getExpDate = request.getParameter('custparam_expdate');
		var getBestBeforeDate = request.getParameter('custparam_bestbeforedate');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var ItemShelfLife = request.getParameter('custparam_shelflife');
		var CaptureExpiryDate = request.getParameter('custparam_captureexpirydate');
		var CaptureFifoDate = request.getParameter('custparam_capturefifodate');
		nlapiLogExecution('ERROR','getWHLocation',getWHLocation);
		nlapiLogExecution('ERROR','getBatchNo',getBatchNo);	
		
		
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "CHECKIN FECHA DE CADUCIDAD";
			st1 = "&#218;LTIMA FECHA DISPONIBLE";
			st2 = "FECHA FIFO";
			st3 = "C&#211;DIGO FIFO";
			st4 = "FORMATO: MMDDAA";			
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
							
		}
		else
		{
			st0 = "CHECKIN EXPIRY DATE";
			st1 = "LAST AVL DATE";
			st2 = "FIFO DATE";
			st3 = "FIFO CODE";
			st4 = "FORMAT : MMDDYY";
			st5 = "SEND";
			st6 = "PREV";

		}
		
		var functionkeyHtml=getFunctionkeyScript('_rf_createinventory'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_createinventory' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":</td>";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value=" + getBinLocationName + ">";
		html = html + "					<input type='hidden' name='hdnLocationId' value=" + getLocationId + ">";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem+ ">";
		html = html + "					<input type='hidden' name='hdnQty' value=" + getQuantity + ">";
		html = html + "					<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + getItem + "'>";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnItemShelfLife' value=" + ItemShelfLife + ">";
		html = html + "				<input type='hidden' name='hdnCaptureExpiryDate' value=" + CaptureExpiryDate + ">";
		html = html + "				<input type='hidden' name='hdnCaptureFifoDate' value=" + CaptureFifoDate + ">";
		
		
		
		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteravldate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ":</td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfifodate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + ": </td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfifocode' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		try {
			var optedEvent = request.getParameter('cmdPrevious');
			var getLastAvlDate = request.getParameter('enteravldate');
			var getFifoDate = request.getParameter('enterfifodate');
			var getFifoCode = request.getParameter('enterfifocode');
			var getBatchNo=request.getParameter('hdnBatchNo');
			nlapiLogExecution('ERROR','getBatchNo',getBatchNo);
			var ItemId= request.getParameter('hdnItemId');
			var locationId = request.getParameter('hdnLocationId');
			nlapiLogExecution('ERROR', 'locationId tst ', locationId);
			var getActualBeginDate = request.getParameter('custparam_actualbegindate');
			var getActualBeginTime = request.getParameter('custparam_actualbegintime');
			var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
			var Itemtype= request.getParameter('hdnitemType');
			var error='';
			var errorflag='F';

			var CIarray = new Array();
			
			var getLanguage = request.getParameter('hdngetLanguage');
			CIarray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);
	    	
			
			var st7,st8,st9;
			if( getLanguage == 'es_ES')
			{
				st7 = "ERROR EN LA FECHA FIFO";
				st8 = "&#218;LTIMA FECHA DEBE SER B / W MFG FECHA Y FECHA EXP";
				st9 = "FIFODATE SHLD SER B / W MFGDATE Y EXPDATE...";
				
			}
			else
			{
				st7 = "ERROR IN FIFO DATE";
				st8 = "LASTDATE SHLD BE B/W MFGDATE AND EXPDATE";
				st9 = "FIFODATE SHLD BE B/W MFGDATE AND EXPDATE";
				
			}

			CIarray["custparam_binlocationid"] = request.getParameter('hdnBinLocationId');
			CIarray["custparam_binlocationname"] = request.getParameter('hdnBinLocation');
			CIarray["custparam_itemid"] = request.getParameter('hdnItemId');
			CIarray["custparam_item"] = request.getParameter('hdnItem');
//			CIarray["custparam_item"] = Item;
			CIarray["custparam_quantity"] = request.getParameter('hdnQty');
			CIarray["custparam_itemtype"]=Itemtype;
			CIarray["custparam_actualbegindate"] = getActualBeginDate;
			CIarray["custparam_locationId"] = locationId;
			CIarray["custparam_actualbegintime"] = getActualBeginTime;
			CIarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
			CIarray["custparam_screenno"] = '15C';
			CIarray["custparam_BatchNo"] = request.getParameter('hdnBatchNo');
			CIarray["custparam_error"] = st7;
			CIarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
			CIarray["custparam_expdate"] = request.getParameter('hdnExpDate');
			CIarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
			CIarray["custparam_lastdate"]='';
			CIarray["custparam_shelflife"] = request.getParameter('hdnItemShelfLife');
			CIarray["custparam_captureexpirydate"] = request.getParameter('hdnCaptureExpiryDate');
			CIarray["custparam_capturefifodate"] = request.getParameter('hdnCaptureFifoDate');
			var itemShelflife = request.getParameter('hdnItemShelfLife');
			var CaptureExpirydate = request.getParameter('hdnCaptureExpiryDate');
			var CaptureFifodate = request.getParameter('hdnCaptureFifoDate');
			nlapiLogExecution('ERROR','hdnBatchNo',request.getParameter('hdnBatchNo'));
			
			if (optedEvent == 'F7') {
				nlapiLogExecution('ERROR', 'CHECK IN FIFO DATE F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_create_invt_expdate', 'customdeploy_rf_create_invt_expdate_di', false, CIarray);
			}
			else {
				var ValueLastDate="";
				var ValueFiFoDate="";
				nlapiLogExecution('ERROR','Test2','Test2');
				if (getLastAvlDate != '' && getLastAvlDate !=null)
				{
					var Lastdate= RFDateFormat(getLastAvlDate);

					if(Lastdate[0]=='true')
					{
						ValueLastDate=Lastdate[1];
						CIarray["custparam_lastdate"]=Lastdate[1];
					}
					else {
						errorflag='T';
						error='LastDate:'+Lastdate[1];
					}
				}
				if (getFifoDate == null || getFifoDate == "") {
					if (request.getParameter('hdnExpDate') != null || request.getParameter('hdnExpDate') != "") {
						CIarray["custparam_fifodate"] = request.getParameter('hdnExpDate');
						ValueFiFoDate=request.getParameter('hdnExpDate');
					}
					else 
						if (request.getParameter('hdnMfgDate') != null || request.getParameter('hdnMfgDate') != "") {
							CIarray["custparam_fifodate"] = request.getParameter('hdnMfgDate');
							ValueFiFoDate=request.getParameter('hdnMfgDate');
						}
				}
				else {
					nlapiLogExecution('ERROR','Befor','Before');
					var Fifodate= RFDateFormat(getFifoDate);
					if(Fifodate[0]=='true')
					{
						nlapiLogExecution('ERROR','Fifodate',Fifodate[1]);
						CIarray["custparam_fifodate"]=Fifodate[1];
						ValueFiFoDate=Fifodate[1];
					}
					else {
						errorflag='T';
						error='Fifodate:'+Fifodate[1];
					}
				}
				nlapiLogExecution('ERROR','error',error);
				if(errorflag=='F')
				{
					//Validating the condn LastDate and FIFODate shld be in b/w MfgDate and ExpDate.
					nlapiLogExecution('ERROR','Expdate',request.getParameter('hdnExpDate'));
					nlapiLogExecution('ERROR','Mfgdate',request.getParameter('hdnMfgDate'));
					nlapiLogExecution('ERROR','ValueLastDate',ValueLastDate);
					nlapiLogExecution('ERROR','ValueFiFoDate',ValueFiFoDate);
					if (Date.parse(request.getParameter('hdnMfgDate'))>= Date.parse(ValueLastDate)|| Date.parse(ValueLastDate)> Date.parse(request.getParameter('hdnExpDate'))) 
					{
						CIarray["custparam_error"] = st8;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					}
					else if (Date.parse(request.getParameter('hdnMfgDate'))>= Date.parse(ValueFiFoDate)|| Date.parse(ValueLastDate) > Date.parse(request.getParameter('hdnExpDate'))) 
					{
						CIarray["custparam_error"] = st9;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					}
					//Case# 201410633 starts
					//else if (Date.parse(ValueFiFoDate) >= Date.parse(request.getParameter('hdnExpDate'))) 
					else if (Date.parse(ValueFiFoDate) > Date.parse(request.getParameter('hdnExpDate')))
					// Case# 201410633 ends
					{
						CIarray["custparam_error"] = "FIFO Date is greater than ExpDate";
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_create_invt_fifocon', 'customdeploy_ebiz_rf_cret_invt_fifocon_d', false, CIarray);
					} 
					else
					{
						CIarray["custparam_fifocode"] = getFifoCode;
						var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

						/*customrecord.setFieldValue('name', getBatchNo);
						customrecord.setFieldValue('custrecord_ebizlotbatch', getBatchNo);
						nlapiLogExecution('ERROR', '1');
						nlapiLogExecution('ERROR', 'Item Id',ItemId);
						//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
						customrecord.setFieldValue('custrecord_ebizsku', ItemId);
						customrecord.setFieldValue('custrecord_ebizsitebatch',locationId);

						customrecord.setFieldValue('custrecord_ebizmfgdate', CIarray["custparam_mfgdate"]);
						customrecord.setFieldValue('custrecord_ebizbestbeforedate', CIarray["custparam_bestbeforedate"]);
						customrecord.setFieldValue('custrecord_ebizexpirydate', CIarray["custparam_expdate"]);
						customrecord.setFieldValue('custrecord_ebizfifodate', CIarray["custparam_fifodate"]);
						customrecord.setFieldValue('custrecord_ebizfifocode', CIarray["custparam_fifocode"]);
						customrecord.setFieldValue('custrecord_ebizlastavldate', CIarray["custparam_lastdate"]);

						var rec = nlapiSubmitRecord(customrecord, false, true);*/
						
						nlapiLogExecution('ERROR', 'BATCH NOT FOUND');
						var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
						customrecord.setFieldValue('name', CIarray["custparam_BatchNo"]);
						customrecord.setFieldValue('custrecord_ebizlotbatch', CIarray["custparam_BatchNo"]);
						customrecord.setFieldValue('custrecord_ebizmfgdate', CIarray["custparam_mfgdate"]);
						customrecord.setFieldValue('custrecord_ebizbestbeforedate', CIarray["custparam_bestbeforedate"]);
						customrecord.setFieldValue('custrecord_ebizexpirydate', CIarray["custparam_expdate"]);
						customrecord.setFieldValue('custrecord_ebizfifodate', CIarray["custparam_fifodate"]);
						customrecord.setFieldValue('custrecord_ebizfifocode', CIarray["custparam_fifocode"]);
						customrecord.setFieldValue('custrecord_ebizlastavldate', CIarray["custparam_lastdate"]);
						nlapiLogExecution('ERROR', '1');
						nlapiLogExecution('ERROR', 'Item Id',ItemId);
						customrecord.setFieldValue('custrecord_ebizsku', ItemId);
//						customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);
					if(CIarray["custparam_locationId"] != null  && CIarray["custparam_locationId"] != '')
							customrecord.setFieldValue('custrecord_ebizsitebatch',CIarray["custparam_locationId"]);
						var rec = nlapiSubmitRecord(customrecord, false, true);
						
						nlapiLogExecution('ERROR', 'rec',rec);
						nlapiLogExecution('ERROR', 'getBatchNo',getBatchNo);
						CIarray["custparam_BatchId"] = rec;
						CIarray["custparam_BatchNo"] = getBatchNo;
						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_itemstat', 'customdeploy_rf_create_invt_itemstat_di', false, CIarray);
					}
				}
				else
				{
					CIarray["custparam_error"] = error;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);

				}
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
			nlapiLogExecution('ERROR', 'Catch: Location not found',e);

		}
	}
}
