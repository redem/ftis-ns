/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/*******************************************************************************
 * ***************************************************************************
 * 
 * $Source:
 * /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_PickConfirmation_SL.js,v $
 * $Revision: 1.3.2.9.4.4.2.12.2.3 $ $Date: 2015/07/07 15:41:41 $ $Author: snimmakayala $ $Name: t_eBN_2015_1_StdBundle_1_184 $
 * 
 * eBizNET version and checksum stamp. Do not remove. $eBiznet_VER:
 * .............. $eBizNET_SUM: ..... PRAMETERS
 * 
 * 
 * DESCRIPTION
 * 
 * Default Data for Interfaces
 * 
 * NOTES AND WARNINGS
 * 
 * INITATED FROM
 * 
 * 
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingClusterNo.js,v $
 * Revision 1.3.2.9.4.4.2.12.2.3  2015/07/07 15:41:41  snimmakayala
 * Case#: 201413392
 *
 * Revision 1.3.2.9.4.4.2.12.2.2  2014/11/05 13:21:08  rmukkera
 * Case # 201410947
 *
 * Revision 1.3.2.9.4.4.2.12.2.1  2014/07/22 07:29:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed moved from enhancment branch
 *
 * Revision 1.3.2.9.4.4.2.12  2014/06/16 07:19:32  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148916
 *
 * Revision 1.3.2.9.4.4.2.11  2014/06/13 12:38:05  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.3.2.9.4.4.2.10  2014/06/06 08:16:40  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.3.2.9.4.4.2.9  2014/05/30 00:41:04  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.2.9.4.4.2.8  2014/05/26 07:08:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.3.2.9.4.4.2.7  2014/05/19 06:29:01  skreddy
 * case # 20148192
 * Sonic SB issue fix
 *
 * Revision 1.3.2.9.4.4.2.6  2013/09/17 06:09:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.3.2.9.4.4.2.5  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.3.2.9.4.4.2.4  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.2.9.4.4.2.3  2013/04/10 15:52:46  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to Invalid Cluster no
 *
 * Revision 1.3.2.9.4.4.2.2  2013/04/03 02:02:02  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.3.2.9.4.4.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.3.2.9.4.4  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.3.2.9.4.3  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.2.9.4.2  2012/10/04 10:28:44  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.3.2.9.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.3.2.9  2012/09/04 20:46:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Incorporate SKIP option in Cluster Picking.
 *
 * Revision 1.3.2.8  2012/08/09 07:30:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Driving user to new screen i.e cluster summary report.
 *
 * Revision 1.3.2.7  2012/08/07 12:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Missing Parameter to query string.
 *
 * Revision 1.3.2.6  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.3.2.5  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.2.4  2012/03/12 08:05:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.8  2012/03/12 07:56:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.7  2012/02/28 01:22:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.6  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.5  2012/02/23 13:08:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.4  2012/02/17 13:29:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 ******************************************************************************/
function PickingClusterNo(request, response)
{
	if (request.getMethod() == 'GET') 
	{

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4;

		if( getLanguage == 'es_ES')
		{
			st1 = "INGRESAR / ESCANEARL N&#218;MERO DE BLOQUE : ";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";

		}
		else
		{
			st1 = "ENTER / SCAN CLUSTER # :";
			st2 = "SEND";
			st3 = "PREV";

		}

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends    
		//html = html + " document.getElementById('enterclusterno').focus();";      

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st1;//ENTER/SCAN CLUSTER NO 
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterclusterno' id='enterclusterno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//	html = html + "				<td align = 'left'>"+ st2 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>"+ st2 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st3 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterclusterno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st4;

		if( getLanguage == 'es_ES')
		{

			st4 = "GRUPO DE VALIDEZ #";


		}
		else
		{
			st4 = "INVALID CLUSTER #";
		}

		var getClusterNo = request.getParameter('enterclusterno');

		nlapiLogExecution('DEBUG', 'Cluter # ', getClusterNo);

		var SOarray = new Array();
		SOarray["custparam_skipid"] = getLanguage;

		SOarray["custparam_error"] = st4;//'INVALID CLUSTER #';
		SOarray["custparam_screenno"] = 'CL1';
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');

		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di', false, SOarray);
		}
		else 
		{
			if (getClusterNo != null && getClusterNo != '' ) 
			{
				var taskassignedto;
				var SOFilters = new Array();

				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
				SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', getClusterNo));
				var vRoleLocation=getRoledBasedLocation();
				nlapiLogExecution('DEBUG', 'vRoleLocation', vRoleLocation);
				if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
				{
					SOFilters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

				}

				var SOColumns = new Array();
//				SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
				//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
				SOColumns[0]=new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
				//Code end as on 290414
				SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
				SOColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
				SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
				SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
				SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
				SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
				SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');
				SOColumns[11] = new nlobjSearchColumn('custrecord_wms_location');
				SOColumns[12] = new nlobjSearchColumn('custrecord_comp_id');
				SOColumns[13] = new nlobjSearchColumn('name');
				SOColumns[14] = new nlobjSearchColumn('custrecord_container');
				SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				SOColumns[16] = new nlobjSearchColumn('custrecord_container_lp_no');
				SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
				SOColumns[18] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
				SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');
				SOColumns[20] = new nlobjSearchColumn('custrecord_taskassignedto');

				SOColumns[0].setSort();	
				SOColumns[1].setSort();		//	Location Pick Sequence
				SOColumns[3].setSort();		//	SKU
				//SOColumns[16].setSort();	//	Container LP	

				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

				if (SOSearchResults != null && SOSearchResults.length > 0) {

					var vorderno;
					var HoldStatus;
					var vordername;
					//var HoldStatusFlag = 'T';
					var OrdeNoArray = new Array();
					for(var f=0; f < SOSearchResults.length; f++)
					{
						var currrow=[SOSearchResults[f].getValue('custrecord_ebiz_order_no'),SOSearchResults[f].getText('custrecord_ebiz_order_no')];
						OrdeNoArray.push(currrow);

					}
					if(OrdeNoArray != null && OrdeNoArray != '')
					{
						OrdeNoArray = removeDuplicateElement(OrdeNoArray); 

						for(var u=0;u<OrdeNoArray.length;u++)
						{
							//nlapiLogExecution('DEBUG', 'OrdeNoArray[u][0] before tst',OrdeNoArray[u][0] );
							//nlapiLogExecution('DEBUG', 'OrdeNoArray[u][1] before tst',OrdeNoArray[u][1] );
							HoldStatus = fnCreateFo(OrdeNoArray[u][0]);
							//nlapiLogExecution('DEBUG', 'HoldStatus before tst',HoldStatus );

							//HoldStatus = 'F';
							//nlapiLogExecution('DEBUG', 'OrdeNoArray tst',OrdeNoArray );
							//nlapiLogExecution('DEBUG', 'HoldStatus after tst',HoldStatus );

							if(HoldStatus == 'F')
							{
								nlapiLogExecution('DEBUG', 'OrdeNoArray[u][0] after tst',OrdeNoArray[u][0] );
								nlapiLogExecution('DEBUG', 'OrdeNoArray[u][1] after tst',OrdeNoArray[u][1] );
								vordername = OrdeNoArray[u][1];
								break;
							}

						}
					}


					if(SOSearchResults.length <= parseFloat(vSkipId))
					{
						vSkipId=0;
					}
					taskassignedto=SOSearchResults[0].getValue('custrecord_taskassignedto');
					nlapiLogExecution('DEBUG', 'taskassignedto', taskassignedto);
					var currentContext = nlapiGetContext();  
					var currentUserID = currentContext.getUser();
					nlapiLogExecution('DEBUG', 'currentUserID', currentUserID);
					if(taskassignedto==null || taskassignedto=='' )
					{
						for ( var b = 0; b < SOSearchResults.length; b++)
						{
							var RecID=SOSearchResults[b].getId();

							var fields=new Array();
							fields[0]='custrecord_taskassignedto';
							fields[1]='custrecord_actualbegintime';
							var Values=new Array();
							Values[0]=currentUserID;
							Values[1]=TimeStamp();
							var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',RecID,fields,Values);
						}
					}
					else if(currentUserID!=taskassignedto)
					{
						nlapiLogExecution('DEBUG', 'taskassignedto1', taskassignedto);

						var msg1='Picking Is In-Progress for the Cluster#';
						var msg2='. Contact Supervisor.';
						SOarray["custparam_error"] = msg1+getClusterNo+msg2;
						nlapiLogExecution('DEBUG', 'errorMessage', msg1+getClusterNo+msg2);
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}

					var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];

					SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no');
					SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
					SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
					SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
					SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
					SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
					SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
					SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
					SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
					SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
					SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no');
					SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
					SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
					SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
					SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
					SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
					SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
					SOarray["name"] =  SOSearchResult.getValue('name');
					SOarray["custparam_picktype"] =  'CL';
					SOarray["custparam_nextlocation"] = '';
					SOarray["custparam_nextiteminternalid"] = '';
					SOarray["custparam_nextitem"] = '';
					//code added by santosh on 7Aug2012
					SOarray["custparam_type"] ="Cluster";
					//end of the code on 7Aug2012
					nlapiLogExecution('DEBUG', 'Begin Location Name',SOarray["custparam_beginlocationname"]);
					nlapiLogExecution('DEBUG', 'Redirecting to Location Screen');


					if(HoldStatus == "F")
					{
						SOarray["custparam_error"] = "Either SO is Closed/Cancelled or Payment is HOLD/Days Over Due/Credit Limit Exceed for  " + vordername;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					else
					{
						var orderArray=ISOrderPartiallyPickedForShipCompleteOrders(SOarray["custparam_waveno"]);

						if(orderArray!=null&&orderArray!="")
						{
							var msg="";
							for(var j=0;j<orderArray.length;j++)
							{
								if(j==0)
									msg=orderArray[j];
								else
									msg=msg+","+orderArray[j];
							}
							SOarray["custparam_error"] = 'Picks generated partially for an orders# "' + msg + '" with ship complete';
							nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
						}
						else
						{


							//Case # 201410947 Start

							var varSystemRuleValue='N' ;
							varSystemRuleValue = getSystemRuleValue(SOarray["custparam_whlocation"]);
							if(varSystemRuleValue == 'N')
							{
								nlapiLogExecution('DEBUG', 'inside varSystemRuleValue');


								try
								{
									var SOFilters = new Array();


									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
									SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', getClusterNo));

									var SOColumns = new Array();
									SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
									SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
									SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
									SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
									SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
									SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
									SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
									SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
									SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
									SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
									SOColumns[11] = new nlobjSearchColumn('custrecord_line_no',null,'group');
									SOColumns[12] = new nlobjSearchColumn('internalid',null,'group');
									SOColumns[13] = new nlobjSearchColumn('name',null,'group');
									SOColumns[14] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
									SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no',null,'group');
									SOColumns[16] = new nlobjSearchColumn('custrecord_invref_no',null,'group');

									SOColumns[0].setSort(); 	//Location Pick Sequence
									SOColumns[1].setSort();		//SKU
									SOColumns[2].setSort();		//Location
									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									nlapiLogExecution('DEBUG', 'SOSearchResults' ,SOSearchResults);
									if (SOSearchResults != null && SOSearchResults.length > 0) {


										var SOSearchResult = SOSearchResults[0];
										nlapiLogExecution('DEBUG', 'SOSearchResults[0].getId()',SOSearchResults[0].getId());
										SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no',null,'group');
										SOarray["custparam_recordinternalid"] = SOSearchResult.getValue('internalid',null,'group');
										SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
										SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
										SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku',null,'group');
										SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc',null,'group');
										SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
										SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no',null,'group');
										SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no',null,'group');
										SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no',null,'group');
										SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
										SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no',null,'group');
										SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no',null,'group');
										//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no',null,'group');
										SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
										SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id',null,'group');
										//SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container',null,'group');
										//SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('internalid','custrecord_ebiz_order_no','group');
										SOarray["name"] =  SOSearchResult.getValue('name',null,'group');
										SOarray["custparam_picktype"] =  'CL';
										SOarray["custparam_nextlocation"] = '';
										SOarray["custparam_nextiteminternalid"] = '';
										SOarray["custparam_nextitem"] = '';
										SOarray["custparam_type"] ="Cluster";

										if(SOarray["custparam_recordinternalid"] != null && SOarray["custparam_recordinternalid"] != "")
										{
											var Opentaskrec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', SOarray["custparam_recordinternalid"]);
											SOarray["custparam_ebizordno"] =  Opentaskrec.getFieldValue('custrecord_ebiz_order_no');
											nlapiLogExecution('DEBUG', 'custparam_ebizordno',SOarray["custparam_ebizordno"]);
										}

										nlapiLogExecution('DEBUG', 'Begin Location Name',SOarray["custparam_beginlocationname"]);
										nlapiLogExecution('DEBUG', 'Redirecting to Location Screen');
										response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);						
										return;
									}
									else 
									{
										nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
										return;
									}
								}
								catch(e)
								{
									nlapiLogExecution('DEBUG', 'Error: ', e);
								}


							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusterwave_summary', 'customdeploy_ebiz_rf_clusterwave_summary', false, SOarray);
								return;
							}



							/*response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusterwave_summary', 'customdeploy_ebiz_rf_clusterwave_summary', false, SOarray);
							return;*/

							//Case # 201410947 End
						}
					}
					//	response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
					//code added by santosh on 7Aug2012
					//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusterwave_summary', 'customdeploy_ebiz_rf_clusterwave_summary', false, SOarray);
					//end of the code on 7Aug2012
				}
				else {
					nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

				}
			}
			else
			{
				nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

			}
		}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}


function ISOrderPartiallyPickedForShipCompleteOrders(getWaveNo)
{
	try
	{

		var SOFilters = new Array();

		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9,26]));	//	Status - Picks Generated/pickfailed
		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));

		var SOColumns = new Array();
		SOColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

		var SOSearchResult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

		var SOArray=new Array();
		var OrderArray=new Array();
		if(SOSearchResult!=null&&SOSearchResult!="")
		{
			for(var i=0;i<SOSearchResult.length;i++)
			{
				SOArray.push(SOSearchResult[i].getValue('custrecord_ebiz_order_no'));
			}

			nlapiLogExecution('DEBUG', 'SOArray', SOArray);

			var filter = new Array();
			filter[0]= new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', SOArray);
			filter[1]= new nlobjSearchFilter('custrecord_shipcomplete', null, 'is', "T"); 
			filter[2]= new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickgen_qty}),0)");

			var column = new Array();
			column[0] = new nlobjSearchColumn('custrecord_lineord');
			column[1] = new nlobjSearchColumn('custrecord_shipcomplete');
			column[2] = new nlobjSearchColumn('custrecord_pickgen_qty');
			column[3] = new nlobjSearchColumn('custrecord_ebiz_linesku');

			var searchRec = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);

			if(searchRec!=null&&searchRec!="")
			{
				for(var j=0;j<searchRec.length;j++)
					OrderArray.push(searchRec[j].getValue("custrecord_lineord"));			
			}
			nlapiLogExecution('DEBUG', 'OrderArray', OrderArray);
		}
		return OrderArray;
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception in ISOrderPartiallyPickedForShipCompleteOrders",exp);
	}
}

function getSystemRuleValue(site)
{
	var getSystemRuleValue='N' ;

	var SystemRuleFilters = new Array();

	SystemRuleFilters.push(new nlobjSearchFilter('name', null, 'is', 'IS Summary Screen Required?'));
	if(site != null && site !='')
	{
		SystemRuleFilters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', site));
	}
	var cols= new Array();
	cols[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');

	var SystemRuleSearchResults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, SystemRuleFilters, cols);
	nlapiLogExecution('DEBUG', 'SystemRuleSearchResults', 'SystemRuleSearchResults');
	if (SystemRuleSearchResults != null && SystemRuleSearchResults.length > 0) {
		nlapiLogExecution('DEBUG', 'SystemRuleSearchResults', SystemRuleSearchResults.length);
		nlapiLogExecution('DEBUG', 'SystemRuleSearchResults', SystemRuleSearchResults[0].getId());
		getSystemRuleValue = SystemRuleSearchResults[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('DEBUG', 'getSystemRuleValue', getSystemRuleValue);

	}

	return getSystemRuleValue;

}
