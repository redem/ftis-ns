/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_ClusPickingStageLocation.js,v $
 *     	   $Revision: 1.1.2.1.2.1 $
 *     	   $Date: 2015/11/12 15:42:03 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_ClusPickingStageLocation.js,v $
 * Revision 1.1.2.1.2.1  2015/11/12 15:42:03  rmukkera
 * case # 201414366
 *
 * Revision 1.1.2.1  2015/02/13 14:34:38  sponnaganti
 * Jawbone Cluster Picking CR Changes
 * CR#  CR14US0323
 *
 * Revision 1.12.2.42.4.11.2.17  2014/03/11 14:47:29  sponnaganti
 * case# 20127645
 * (filter added for inactive)
 *
 * Revision 1.12.2.42.4.11.2.16  2013/12/17 15:36:29  nneelam
 * Case# 20126364
 * Display Invalid Msg.
 *
 * Revision 1.12.2.42.4.11.2.15  2013/11/22 14:30:14  rmukkera
 * Case# 20125591
 *
 * Revision 1.12.2.42.4.11.2.14  2013/11/22 08:40:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201216680
 *
 * Revision 1.12.2.42.4.11.2.13  2013/11/14 06:32:04  skreddy
 * Case# 201215000
 * Fast Picking issue fix
 *
 * Revision 1.12.2.42.4.11.2.12  2013/10/24 14:28:09  schepuri
 * 20125163
 *
 * Revision 1.12.2.42.4.11.2.11  2013/10/22 14:06:08  schepuri
 * 20125049
 *
 * Revision 1.12.2.42.4.11.2.10  2013/09/03 15:44:02  skreddy
 * Case# 20124235
 * standard bundle issue fix
 *
 * Revision 1.12.2.42.4.11.2.9  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.12.2.42.4.11.2.8  2013/07/15 11:35:41  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215550
 *
 * Revision 1.12.2.42.4.11.2.7  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.12.2.42.4.11.2.6  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.12.2.42.4.11.2.5  2013/04/12 11:04:20  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.12.2.42.4.11.2.4  2013/04/08 08:47:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.12.2.42.4.11.2.3  2013/04/03 02:24:45  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.12.2.42.4.11.2.2  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.12.2.42.4.11.2.1  2013/02/26 12:56:03  skreddy
 * CASE201112/CR201113/LOG201121
 * merged boombah changes
 *
 * Revision 1.12.2.42.4.11  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.12.2.42.4.10  2012/12/21 09:29:26  mbpragada
 * 20120490
 *
 * Revision 1.12.2.42.4.9  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.12.2.42.4.8  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.12.2.42.4.7  2012/11/11 03:40:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * GSUSA UAT issue fixes.
 *
 * Revision 1.12.2.42.4.6  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.12.2.42.4.5  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.12.2.42.4.4  2012/10/11 10:27:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick confirmation and other inventory issues
 *
 * Revision 1.12.2.42.4.3  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.12.2.42.4.2  2012/10/04 10:27:54  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.12.2.42.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.12.2.42  2012/09/07 03:47:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.12.2.41  2012/09/06 06:35:37  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related to fetching stage location.
 *
 * Revision 1.12.2.40  2012/09/03 13:52:07  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.12.2.39  2012/08/31 07:13:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Parenthesis Missing.
 *
 * Revision 1.12.2.38  2012/08/28 16:02:05  spendyala
 * CASE201112/CR201113/LOG201121
 * Changed F8 to ENT.
 *
 * Revision 1.12.2.37  2012/08/27 15:22:39  schepuri
 * CASE201112/CR201113/LOG201121
 * if user clicks on Enter it goes to blank screen
 *
 * Revision 1.12.2.36  2012/08/27 04:04:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Auto Packlist for FISK
 * NS Case # : 20120739
 *
 * Revision 1.12.2.35  2012/08/26 17:55:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Pick Confirm issues for FISK
 *
 * Revision 1.12.2.34  2012/08/26 00:49:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * To show carton # in stage location screen for wave picking
 * Case # 20120728
 *
 * Revision 1.12.2.33  2012/08/24 17:37:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Picking wrong staging location issue
 *
 * Revision 1.12.2.32  2012/08/22 13:58:18  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stage Location determination for Boombah and Fisk.
 *
 * Revision 1.12.2.31  2012/08/17 08:35:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating Stage Location scanned by user.
 *
 * Revision 1.12.2.30  2012/08/16 07:53:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah UAT Issue Fixes.
 * Cartonization and Customization
 *
 * Revision 1.12.2.29  2012/08/11 00:01:09  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.12.2.28  2012/08/08 17:15:18  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Location override issue for Dynacraft
 *
 * Revision 1.12.2.27  2012/08/07 21:18:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.12.2.26  2012/07/19 05:22:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.12.2.25  2012/07/17 13:03:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stg Rule determination by Carrier Type.
 *
 * Revision 1.12.2.24  2012/07/09 06:50:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.12.2.23  2012/06/22 10:10:40  gkalla
 * CASE201112/CR201113/LOG201121
 * blank page issue resolved
 *
 * Revision 1.12.2.22  2012/05/23 14:16:17  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12.2.21  2012/05/17 16:13:44  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate next location
 *
 * Revision 1.12.2.20  2012/05/11 15:47:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Stageloc
 *
 * Revision 1.12.2.19  2012/04/24 13:49:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.12.2.18  2012/04/19 15:00:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.12.2.17  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.12.2.16  2012/04/17 00:11:45  gkalla
 * CASE201112/CR201113/LOG201121
 * To pass actual begin location value to next screen
 *
 * Revision 1.12.2.15  2012/04/10 14:50:17  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.12.2.14  2012/03/29 06:30:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * STG Locatoion update issue for outbound inventory records.
 *
 * Revision 1.28  2012/03/29 06:11:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * STG Locatoion update issue for outbound inventory records.
 *
 * Revision 1.27  2012/03/20 19:01:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * F8 to ENT
 *
 * Revision 1.26  2012/03/12 07:56:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.19  2012/03/09 16:38:16  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed the operator any to anyof  while fetching generate location
 *
 * Revision 1.4  2011/10/03 09:02:42  jrnarsupalli
 * CASE201112/CR201113/LOG201121
 * RM Comments: Corrected CVS header to have CVS key words.
 *
 *
 *****************************************************************************/

function WOPickingStageLocation(request, response){
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('DEBUG', 'Into Page Load');
		var fastpick = request.getParameter('custparam_fastpick');
		nlapiLogExecution('ERROR', 'fastpick',fastpick);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR AL MONTAJE";
			st2 = "N&#218;MERO DE EMPAQUE:";
			st3 = "UBICACI&#211;N:";
			st4 = "INGRESAR / ESCANEAR ETAPA:";
			st5 = "CONF";
		}
		else
		{
			st1 = "GO TO STAGING ";
			st2 = "CARTON NO : ";
			st3 = "LOCATION : ";
			st4 = "ENTER/SCAN STAGE :";
			st5 = "CONF";
		}

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_newcontainerlp');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');		
		var getSerialNo = request.getParameter('custparam_serialno');
		var vClusterNo = request.getParameter('custparam_clusterno');		
		var name = request.getParameter('name');
		var pickType=request.getParameter('custparam_picktype');	
		var vSOId=request.getParameter('custparam_ebizordno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var whLocation = request.getParameter('custparam_whlocation');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var venterzone=request.getParameter('custparam_venterzone');
		
		var vBatchno = request.getParameter('custparam_batchno');
		var getItem = '';
		var vondemandstage = request.getParameter('custparam_ondemandstage');
		//case 20124235 start
		var Itemdescription='';
		//end
		if(getItemName==null || getItemName=='')
		{
			if(getItemInternalId!=null && getItemInternalId!='')
			{
				//var Itemtype = nlapiLookupField('item', getItemInternalId, 'recordType');

				var filtersitem = new Array();
				var columnsitem = new Array();

				filtersitem.push(new nlobjSearchFilter('internalid', null, 'is',getItemInternalId));
				filtersitem.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

				columnsitem[0] = new nlobjSearchColumn('description');    
				columnsitem[1] = new nlobjSearchColumn('salesdescription');
				columnsitem[2] = new nlobjSearchColumn('itemid');

				var itemRecord = nlapiSearchRecord("item", null, filtersitem, columnsitem);
				if(itemRecord!=null && itemRecord!='' && itemRecord.length>0)
				{
					if(itemRecord[0].getValue('description') != null && itemRecord[0].getValue('description') != '')
					{
						Itemdescription = itemRecord[0].getValue('description');
					}
					else if(itemRecord[0].getValue('salesdescription') != null && itemRecord[0].getValue('salesdescription') != "")
					{	
						Itemdescription = itemRecord[0].getValue('salesdescription');
					}

					getItem = itemRecord[0].getValue('itemid');
				}

				if(Itemdescription !=null && Itemdescription !='')
				Itemdescription = Itemdescription.substring(0, 20);			
			}
		}
		else
		{
			getItem=getItemName;
		}

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		//Code Added for Displaying the Location Name
		var Carrier;
		var Site;
		var Company;
		var stgOutDirection="OUB";
		var carriertype='';
		var wmscarrier='';
		var customizetype='';
		var ordertype='';
		var customer='';

		nlapiLogExecution('DEBUG', 'getDOLineId',getDOLineId); 
		nlapiLogExecution('DEBUG', 'WaveNo ', getWaveno);

		if(getDOLineId!=null && getDOLineId!='')
		{
			var columns = new Array();
			var filters = new Array();

			filters.push(new nlobjSearchFilter('internalid', null, 'is',getDOLineId));

			columns[0] = new nlobjSearchColumn('custrecord_do_wmscarrier');	
			columns[1] = new nlobjSearchColumn('custrecord_do_carrier');				
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_wave');
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_ordline_customizetype');
			columns[4] = new nlobjSearchColumn('custrecord_do_order_type');
			columns[5] = new nlobjSearchColumn('custrecord_do_customer');

			var searchresultsord = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
			if(searchresultsord!=null && searchresultsord!='' && searchresultsord.length>0)
			{
				Carrier = searchresultsord[0].getValue('custrecord_do_carrier');
				customizetype = searchresultsord[0].getValue('custrecord_ebiz_ordline_customizetype');
				ordertype = searchresultsord[0].getValue('custrecord_do_order_type');
				customer = searchresultsord[0].getValue('custrecord_do_customer');
				nlapiLogExecution('DEBUG', 'Carrier',Carrier); 
				nlapiLogExecution('DEBUG', 'customizetype',customizetype); 
				nlapiLogExecution('DEBUG', 'ordertype',ordertype); 
				if(Carrier!=null && Carrier!='')
				{
					var filterscarr = new Array();
					var columnscarr = new Array();

					filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',Carrier));
					filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

					columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
					columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
					columnscarr[2] = new nlobjSearchColumn('id');

					var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
					if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
					{
						carriertype = searchresultscarr[0].getValue('custrecord_carrier_type');
						wmscarrier = searchresultscarr[0].getId();
					}
				}

				if(Carrier==null || Carrier=='')
					wmscarrier=searchresultsord[0].getValue('custrecord_do_wmscarrier');
				if(getWaveno==null || getWaveno=='')
					getWaveno=searchresultsord[0].getValue('custrecord_ebiz_wave');
			}
		}

		nlapiLogExecution('DEBUG', 'carriertype',carriertype); 
		nlapiLogExecution('DEBUG', 'wmscarrier',wmscarrier); 
		nlapiLogExecution('DEBUG', 'WaveNo ', getWaveno);

		var stgLocation = '';
		var FetchBinLocation='';
		var tranordertype = nlapiLookupField('workorder', getOrderNo, 'custbody_nswmssoordertype');

		nlapiLogExecution('ERROR', 'tranordertype',tranordertype); 
		if(stgLocation==null || stgLocation=='')
		{

			stgLocation = GetPickStageLocation(getItemInternalId, wmscarrier, whLocation, Company,
					stgOutDirection,carriertype,customizetype,null,tranordertype);
			


			nlapiLogExecution('DEBUG', 'After Getting StagingLocation in page load::',stgLocation);
			var FetchBinLocation;
			if(stgLocation!=null && stgLocation!=-1)
			{
				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('name');
				filtersLocGroup.push(new nlobjSearchFilter('internalid', null, 'is',stgLocation));
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				if(searchresultsloc != null && searchresultsloc != '')
					FetchBinLocation=searchresultsloc[0].getValue('name');
			}

			nlapiLogExecution('DEBUG', 'After Getting StagingLocation Name in page load::',FetchBinLocation);
		}

		//var stageScanRequiredRuleValue = getSystemRuleValue('STAGESCANREQD');

		//upto to here

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('enterstagelocation').focus();";        

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
//		if(pickType!='CL')
//		{
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>"+ st2 +" " + getContainerLpNo;
//		html = html + "				</td>";
//		html = html + "			</tr>";

//		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"</td></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" " + FetchBinLocation;
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnserialno' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnstgloc' value=" + stgLocation + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnsoid' value=" + vSOId + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwmscarrier' value=" + Carrier + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='vhdnstageloctext' value='" + FetchBinLocation + "'>";
		html = html + "				<input type='hidden' name='vhdncarrier' value=" + wmscarrier + ">";
		html = html + "				<input type='hidden' name='vhdncarriertype' value=" + carriertype + ">";
		html = html + "				<input type='hidden' name='vhdncustomizetype' value=" + customizetype + ">";
		html = html + "				<input type='hidden' name='hdnondemandstage' value=" + vondemandstage + ">";
		html = html + "				<input type='hidden' name='hdnStageScanReqRuleValue' value=" + stageScanRequiredRuleValue + ">";
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";
		html = html + "				<input type='hidden' name='hdnenterzone' value=" + venterzone + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		if(pickType!='CL')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>CARTON NO : " + getContainerLpNo;
			html = html + "				</td>";
			html = html + "			</tr>";

		}
		//if(stageScanRequiredRuleValue != 'N')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>"+st4;//ENTER/SCAN STAGE :
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterstagelocation' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st5 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>"+ st5 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st6,st7;

		if( getLanguage == 'es_ES')
		{
			st6 = "LOCALIZACI&#211;N ETAPA NO V&#193;LIDO";
			st7 = "OPCI#211;N V&#193;LIDA";

		}
		else
		{
			//Case # 20126364
			st6 = "ENTER VALID STAGE LOCATION";
			st7 = "INVALID OPTION";

		}
		nlapiLogExecution('DEBUG', 'Pick Type', request.getParameter('hdnpicktype'));

		var fastpick = request.getParameter('hdnfastpick');
		nlapiLogExecution('ERROR', 'fastpick', fastpick);

		var optedEvent = request.getParameter('hdnflag');
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
		var SOarray = new Array();
		var stageScanRequiredRuleValue = request.getParameter('hdnStageScanReqRuleValue');
		SOarray["custparam_language"] = getLanguage;
		var WaveNo = request.getParameter('hdnWaveNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');			
		var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var ItemDescription = request.getParameter('hdnItemDescription');
		var ItemInternalId = request.getParameter('hdnItemInternalId');
		var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var EndLocation = request.getParameter('hdnEnteredLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var SerialNo = request.getParameter('hdnserialno');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchno = request.getParameter('hdnbatchno');
		var vPickType = request.getParameter('hdnpicktype');
		var vSOId=request.getParameter('hdnsoid');
		SOarray["custparam_ebizordno"] = vSOId;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		var vCarrier = request.getParameter('hdnwmscarrier');
		var vSite;
		var vCompany;
		var stgDirection="OUB";
		var vDoname=request.getParameter('hdnName');
		var vdono=request.getParameter('hdnDOLineId');
		var vPrevinvrefno=request.getParameter('hdnInvoiceRefNo');
		var vstageLoc=request.getParameter('hdnstgloc');
		var voldcontainer='';
		var vZoneId=request.getParameter('hdnebizzoneno');
		var vhdncarrier=request.getParameter('vhdncarrier');
		var vhdncarriertype=request.getParameter('vhdncarriertype');
		var vhdncustomizetype=request.getParameter('vhdncustomizetype');
		var vondemandstage = request.getParameter('hdnondemandstage');
		var venterzone = request.getParameter('hdnenterzone');
		nlapiLogExecution('DEBUG', 'venterzone ', venterzone);
		nlapiLogExecution('DEBUG', 'WaveNo ', WaveNo);
		nlapiLogExecution('DEBUG', 'ContainerLPNo ', ContainerLPNo);
		nlapiLogExecution('DEBUG', 'vPickType', vPickType);
		nlapiLogExecution('DEBUG', 'vZoneId', vZoneId);
		nlapiLogExecution('DEBUG', 'vondemandstage', vondemandstage);
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		//nlapiLogExecution('DEBUG', 'SOarray["custparam_waveno"]', SOarray["custparam_waveno"]);
		//nlapiLogExecution('DEBUG', 'SOarray["custparam_recordinternalid"]', SOarray["custparam_recordinternalid"]);
		//nlapiLogExecution('DEBUG', 'SOarray["custparam_containerlpno"]', SOarray["custparam_containerlpno"]);
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');;
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');		
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');;
		SOarray["custparam_picktype"] = vPickType;
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_newcontainerlp"] = ContainerLPNo;
		SOarray["custparam_fastpick"] = fastpick;
		SOarray["custparam_venterzone"]=request.getParameter('hdnenterzone');
		var WHLocation = request.getParameter('hdnwhlocation');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');

		if (optedEvent == 'ENT') 
		{
			//code added on 170812 by suman.
			//To validate user entered Stage Location.
			var stagelocation = '';
			//case no 20125163Ã¯Â¿Â½ 

			if(stageScanRequiredRuleValue == 'Y' || stageScanRequiredRuleValue == '' || stageScanRequiredRuleValue == null)
				stagelocation=request.getParameter('enterstagelocation');
			else
				stagelocation=request.getParameter('vhdnstageloctext');
			var hdnstagelocation=request.getParameter('vhdnstageloctext');

			nlapiLogExecution('DEBUG', 'Scanned Stage Location', stagelocation);
			nlapiLogExecution('DEBUG', 'Actual Stage Location', hdnstagelocation);
			if(stagelocation!=hdnstagelocation)
			{
				var resArray=ValidateStageLocation(ItemInternalId,vhdncarrier,WHLocation,vCompany,
						"OUB",vhdncarriertype,vhdncustomizetype,stagelocation);

				var Isvalidestage=resArray[0];
				if(Isvalidestage==true)
				{
					vstageLoc=resArray[1];
				}
				else
				{
					SOarray["custparam_error"] = st6;//'INVALID STAGE LOCATION';
					nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Stage Location');
					SOarray["custparam_screenno"] = 'CLWO5';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
			}
			//end of code as on 170812.

			nlapiLogExecution('DEBUG', 'ContainerLPNo (Pack Complete)', ContainerLPNo);
			nlapiLogExecution('DEBUG', 'WaveNo (Pack Complete)', WaveNo);

			//To find stage determination
			var Stagedet=3;//Default Carrier level
			if(WHLocation != null && WHLocation != '')
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', WHLocation));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebizpickmethod');
				columns[1] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
				columns[2] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');

				var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

				var pickMethodId ;

				if(pickRuleSearchResult != null){
					for (var i=0; i<pickRuleSearchResult.length; i++){			
						pickMethodId = pickRuleSearchResult[0].getValue('custrecord_ebizpickmethod');

						Stagedet = pickRuleSearchResult[i].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
					}
				}
				nlapiLogExecution('ERROR', 'pickMethodId', pickMethodId);

			}

			//The below changes done by Satish.N
			//If stage determination is not defined,take carton level as default.

			if(Stagedet==null || Stagedet=='')
				Stagedet=3;

			//Upto here.

			nlapiLogExecution('ERROR', 'Stagedet', Stagedet);

			var taskfilters = new Array();

			taskfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));//Task Type - PICK
			taskfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','28']));// Pick Confirm and Pack Complete
			if(WaveNo!=null && WaveNo!='')
				taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));	
			if (vPickType!='CL') {
				if(Stagedet==3)//Carrier level
				{
					if(ContainerLPNo!=null && ContainerLPNo!='' && ContainerLPNo!='null')
						taskfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ContainerLPNo));
				}

				//The below changes done by Satish.N, Wave filter is already there.

//				else if(Stagedet==2)//Wave Level
//				{
//				if(WaveNo!=null && WaveNo!='')
//				taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
//				}

				//Upto here
			}
			if (vPickType=='CL') {
				taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));	
			}

			if(venterzone != null && venterzone != "" && venterzone != 'null')
			{
				taskfilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));
			}
			var taskcolumns = new Array();
			taskcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
			taskcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			taskcolumns[2] = new nlobjSearchColumn('custrecord_invref_no');
			taskcolumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
			taskcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			taskcolumns[5] = new nlobjSearchColumn('name');
			taskcolumns[0].setSort();

			var tasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskfilters, taskcolumns);

			if(tasksearchresults!=null)
			{
				for (var i = 0; i < tasksearchresults.length; i++)
				{
					ContainerLPNo = tasksearchresults[i].getValue('custrecord_container_lp_no');
					Item = tasksearchresults[i].getValue('custrecord_ebiz_sku_no');
					var salesorderintrid = tasksearchresults[i].getValue('custrecord_ebiz_order_no');
					vDoname = tasksearchresults[i].getValue('name');

					nlapiLogExecution('DEBUG', 'voldcontainer', voldcontainer);
					nlapiLogExecution('DEBUG', 'ContainerLPNo', ContainerLPNo);
					nlapiLogExecution('DEBUG', 'salesorderintrid', salesorderintrid);
					nlapiLogExecution('DEBUG', 'vondemandstage', vondemandstage);

					if(voldcontainer!=ContainerLPNo)
					{
						CreateSTGMRecord(ContainerLPNo, WaveNo, RecordInternalId, vDoname, vCompany, WHLocation, vdono, 
								Item, stgDirection, vCarrier, vSite,vstageLoc,vClusterNo,salesorderintrid,vondemandstage);

						voldcontainer=ContainerLPNo;
					}
				}
			}	

			if(vondemandstage == 'Y')
			{
				var SOarray = new Array();
				SOarray["custparam_fastpick"] = fastpick;
				response.sendRedirect('SUITELET', 'customscript_rf_wopicking_cluster_no', 'customdeploy_rf_wopicking_cluster_no', false, SOarray);
			}
			else
			{
				var vFootPrintFlag='N'; // Put Y to navigate to Dynacraft specific screen for scanning 'No. of foot prints/Pallets'
				if(vFootPrintFlag != 'Y')
				{
					if(vPickType != null && vPickType != '' && vPickType!='CL')
					{

						nlapiLogExecution('DEBUG', 'Inside Both Wave No condition', parseFloat(WaveNo));
						nlapiLogExecution('DEBUG', 'Inside Both Order No condition', DoLineId);

						var SOFilters = new Array();


						SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated					
						SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

						if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
						{
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
						} 
						if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && vDoname!=null && vDoname!="" && vDoname!= "null")
						{
							nlapiLogExecution('DEBUG', 'OrdNo inside If', vDoname);
							SOFilters.push(new nlobjSearchFilter('name', null, 'is', vDoname));
						}
						if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
						{
							nlapiLogExecution('DEBUG', 'SO Id inside If', vSOId);
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
						}
						if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
						{
							nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
							SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
						}

						var SOColumns = new Array();
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
						SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
						SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
						SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
						SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
						SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
						SOColumns.push(new nlobjSearchColumn('name'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
						SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));

						SOColumns[0].setSort();
						SOColumns[1].setSort();
						SOColumns[2].setSort();
						SOColumns[3].setSort();
						SOColumns[4].setSort();
						SOColumns[5].setSort(true);

						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
						nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults);
						if (SOSearchResults != null && SOSearchResults.length > 0 ) {
							nlapiLogExecution('DEBUG', 'into Search Results by Wave No', WaveNo);
							nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);
							vSkipId=0;
							if(SOSearchResults.length <= parseFloat(vSkipId))
							{
								vSkipId=0;
							}
							//nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);

							var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
							getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

							nlapiLogExecution('DEBUG', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
							nlapiLogExecution('DEBUG', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
							nlapiLogExecution('DEBUG', 'Wave # of given Wave', getWaveNo);

							SOarray["custparam_waveno"] = getWaveNo;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
							SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
							SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
							SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
							SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
							SOarray["name"] = SOSearchResult.getValue('name');
							SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
							SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
							SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');	
							SOarray["custparam_clusterno"] = vClusterNo;
							SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
							SOarray["custparam_ebizordno"] = SOSearchResult.getValue('custrecord_ebiz_order_no');
							SOarray["custparam_fastpick"] = fastpick;
							if(vZoneId!=null && vZoneId!="")
							{
								SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
							}
							else
								SOarray["custparam_ebizzoneno"] = '';
							if(fastpick!='Y')
							{
								response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
							}							
							return;

						}
						else {
							nlapiLogExecution('DEBUG', 'Search Results by Wave No is NULL', WaveNo);
							var SOarray = new Array();
							SOarray["custparam_fastpick"] = fastpick;
							response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
						}


					}	
					else if (vPickType=='CL') {

						nlapiLogExecution('DEBUG', 'Inside Cluster No condition', vClusterNo);

						var SOFilters = new Array();

						SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
						SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
						SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
						SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
						if(venterzone != null && venterzone != "" && venterzone != 'null')
						{
							SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));
						}

						var SOColumns = new Array();
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
						SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
						SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
						SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
						SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
						SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
						SOColumns.push(new nlobjSearchColumn('name'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
						SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_clus_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));

						SOColumns[0].setSort();
						SOColumns[1].setSort();
						SOColumns[2].setSort();
						SOColumns[3].setSort();
						SOColumns[4].setSort();
						SOColumns[5].setSort(true);
						SOColumns[19].setSort();	//	Container LP
						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

						if (SOSearchResults != null && SOSearchResults.length > 0) {
							vSkipId=0;
							if(SOSearchResults.length <= parseFloat(vSkipId))
							{
								vSkipId=0;
							}

							var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];

							SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no');
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
							SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
							SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
							SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
							SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
							SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
							SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');						
							SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no');
							SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
							SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
							SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
							SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
							SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
							SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
							SOarray["custparam_picktype"] =  'CL';
							SOarray["name"] =  SOSearchResult.getValue('name');

							nlapiLogExecution('DEBUG', 'Redirecting to Location Screen');
							response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_location', 'customdeploy_rf_wocluspicking_location', false, SOarray);					

						}
						else {
							response.sendRedirect('SUITELET', 'customscript_rf_wopicking_cluster_no', 'customdeploy_rf_wopicking_cluster_no', false, null);
						}
					}
					else if (WaveNo != null && WaveNo != "" && vPickType!='CL') {
						nlapiLogExecution('DEBUG', 'Inside Wave No condition', parseFloat(WaveNo));

						var SOFilters = new Array();

						SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
						SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

						var SOColumns = new Array();

						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
						SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
						SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
						SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
						SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
						SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
						SOColumns.push(new nlobjSearchColumn('name'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
						SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
						SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');

						SOColumns[0].setSort();
						SOColumns[1].setSort();
						SOColumns[2].setSort();
						SOColumns[3].setSort();
						SOColumns[4].setSort();
						SOColumns[5].setSort(true);

						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
						nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults);
						if (SOSearchResults != null && SOSearchResults.length > 0) {
							nlapiLogExecution('DEBUG', 'into Search Results by Wave No', WaveNo);
							vSkipId=0;
							if(SOSearchResults.length <= parseFloat(vSkipId))
							{
								vSkipId=0;
							}
							//nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
							var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
							getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

							nlapiLogExecution('DEBUG', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
							nlapiLogExecution('DEBUG', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
							nlapiLogExecution('DEBUG', 'Wave # of given Wave', getWaveNo);

							SOarray["custparam_waveno"] = getWaveNo;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
							SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
							SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
							SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
							SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
							SOarray["name"] = SOSearchResult.getValue('name');
							SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
							SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
							SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');	
							SOarray["custparam_clusterno"] = vClusterNo;
							SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
							SOarray["custparam_ebizordno"] = SOSearchResult.getValue('custrecord_ebiz_order_no');
							SOarray["custparam_fastpick"] = fastpick;

							if(fastpick!='Y')
							{
								response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);

							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
							}
							return;

						}
						else {
							nlapiLogExecution('DEBUG', 'Search Results by Wave No is NULL', WaveNo);
							var SOarray = new Array();
							SOarray["custparam_fastpick"] = fastpick;
							response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
						}
					}
					else
					{
						var SOarray = new Array();
						SOarray["custparam_fastpick"] = fastpick;
						response.sendRedirect('SUITELET', 'customscript_rf_wopicking_cluster_no', 'customdeploy_rf_wopicking_cluster_no', false, SOarray);
					}

				}
				else
				{
					nlapiLogExecution('DEBUG', 'Navigating To1', 'Foot print');
					response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, SOarray);
				}
			}
			nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
		}
		else
		{
			SOarray["custparam_error"] = st7;//'INVALID OPTION';
			nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Option');
			SOarray["custparam_screenno"] = 'CLWO5';
			SOarray["custparam_fastpick"] = fastpick;
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}


function ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo,SerialNo,vBatchno){
	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');

	nlapiLogExecution('DEBUG', "Line Count" + lineCnt);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	/*    
     for (var k = 1; k <= lineCnt; k++)
     {
     var chkVal = request.getLineItemValue('custpage_items', 'custpage_so', k);
     var SONo
     var ItemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
     var itemno = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
     var Lineno = request.getLineItemValue('custpage_items', 'custpage_lineno', k);
     var pickqty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
     var vinvrefno = request.getLineItemValue('custpage_items', 'custpage_invrefno', k);

     var WaveNo = request.getParameter('hdnWaveNo');
     var RecordInternalId = request.getParameter('hdnRecordInternalId');
     var ContainerLPNo = request.getParameter('hdnContainerLpNo');
     var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
     var BeginLocation = request.getParameter('hdnBeginLocationId');
     var Item = request.getParameter('hdnItem');
     var ItemDescription = request.getParameter('hdnItemDescription');
     var ItemInternalId = request.getParameter('hdnItemInternalId');
     var DoLineId = request.getParameter('hdnDOLineId');	//
     var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
     var BeginLocationName = request.getParameter('hdnBeginBinLocation');
     var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
     var EndLocation = getEnteredLocation;

     //        var Recid = request.getLineItemValue('custpage_items', 'custpage_recid', k);
     //        nlapiLogExecution('DEBUG', "Recid " + Recid)
	 */
	//updating allocqty in opentask
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
	OpenTaskTransaction.setFieldValue('custrecord_act_qty', parseFloat(ExpectedQuantity).toFixed(4));
	OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
	OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);

	nlapiLogExecution('DEBUG', 'Done PICK Record Insertion :', 'Success');

	//Updating DO 

	var DOLine = nlapiLoadRecord('customrecord_ebiznet_ordline', DoLineId);
	DOLine.setFieldValue('custrecord_linestatus_flag', '1'); //Statusflag='C'
	DOLine.setFieldValue('custrecord_pickqty', parseFloat(ExpectedQuantity).toFixed(4));
	var SalesOrderInternalId = DOLine.getFieldValue('name');

	try {
		nlapiLogExecution('DEBUG', 'Main Sales Order Internal Id', SalesOrderInternalId);
		var TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');

		var SOLength = TransformRec.getLineItemCount('item');
		nlapiLogExecution('DEBUG', "SO Length", SOLength);

		/*
         for (var j = 0; j < SOLength; j++)
         {
         nlapiLogExecution('DEBUG', "Get Line Value", TransformRec.getLineItemValue('item', 'line', j + 1));
         TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
         }
         TransformRec.setLineItemValue('item', 'itemfulfillment', OrderLineNo, 'T');
         TransformRec.setLineItemValue('item', 'quantity', OrderLineNo, ExpectedQuantity);
         var TransformRecId = nlapiSubmitRecord(TransformRec, true);
		 */
		for (var j = 1; j <= SOLength; j++) {
			nlapiLogExecution('DEBUG', "Get Line Value" + TransformRec.getLineItemValue('item', 'line', j ));

			var item_id = TransformRec.getLineItemValue('item', 'item', j);
			var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);

			if (itemLineNo == OrderLineNo) {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
				TransformRec.setCurrentLineItemValue('item', 'quantity', ExpectedQuantity);
				//TransformRec.setCurrentLineItemValue('item', 'location', 1);
				var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
				var serialsspaces="";
				if (itemSubtype.recordType == 'serializedinventoryitem') {

					arrSerial= SerialNo.split(',');
					nlapiLogExecution('DEBUG', "arrSerial", arrSerial);	
					for (var n = 0; n < arrSerial.length; n++) {
						if (n == 0) {
							serialsspaces = arrSerial[n];
						}
						else
						{
							serialsspaces += " " +arrSerial[n];
						}
					}					
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', serialsspaces);
				}
				//vBatchno
				if (itemSubtype.recordType == 'lotnumberedinventoryitem'){
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', vBatchno+'('+ExpectedQuantity+')');
				}


				TransformRec.commitLineItem('item');
			}
			else {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');
				TransformRec.commitLineItem('item');

			}
			//TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
		}
		//			invtRec.setFieldValue('custrecord_ebiz_inv_loc', '1');//WH Location.

		//			TransformRec.setFieldValue('custbody_ebiz_fulfillmentord',vdono) 
		var TransformRecId = nlapiSubmitRecord(TransformRec, true);
	} 

	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}

	///Updating qty,qoh in inventory
	nlapiLogExecution('DEBUG', 'Invoice Ref No', InvoiceRefNo);
	var InventoryTranaction = nlapiLoadRecord('customrecord_ebiznet_createinv', InvoiceRefNo);

	var InvAllocQty = InventoryTranaction.getFieldValue('custrecord_ebiz_alloc_qty');
	var InvQOH = InventoryTranaction.getFieldValue('custrecord_ebiz_qoh');

	if (isNaN(InvAllocQty)) {
		InvAllocQty = 0;
	}
	InventoryTranaction.setFieldValue('custrecord_ebiz_qoh', (parseFloat(InvQOH) - parseFloat(ExpectedQuantity)).toFixed(4));
	InventoryTranaction.setFieldValue('custrecord_ebiz_alloc_qty', (parseFloat(InvAllocQty) - parseFloat(ExpectedQuantity)).toFixed(4));
	var InventoryTranactionId = nlapiSubmitRecord(InventoryTranaction, false, true);
	OpenTaskTransaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
	nlapiSubmitRecord(OpenTaskTransaction, true);
	nlapiSubmitRecord(DOLine, true);

	//    }

	//    var form = nlapiCreateForm('Pick  Confirmation');
	//    form.addField('custpage_label', 'label', '<h2>Pick Confirmation Successful</h2>');

	//    response.writePage(form);
}

/**
 * 
 * @param vContLp
 * @param vebizWaveNo
 * @param vebizOrdNo
 * @param vDoname
 * @param vCompany
 * @param vlocation
 * @param vdono
 * @param Item
 * @param stgDirection
 * @param vCarrier
 * @param vSite
 */
function CreateSTGMRecord(vContLp, vebizWaveNo, vebizOrdNo, vDoname, vCompany,vlocation,vdono,
		Item,stgDirection,vCarrier,vSite,stageLocation,vClusterNo,salesorderintrid,vondemandstage) {

	nlapiLogExecution('DEBUG', 'into CreateSTGMRecord');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,
			'is', '9')); //STATUS.OUTBOUND.PICK_GENERATED('G') 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',
			vebizWaveNo));
	if(vContLp!=null && vContLp!='' && vContLp!='null')
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,
				'is', vContLp));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'isempty'));
	}

	if(vClusterNo!=null && vClusterNo!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null,
				'is', vClusterNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',
			null, filters, columns);
	if (searchresults == null || vondemandstage=='Y') {
		if(stageLocation==null || stageLocation=='')
			stageLocation = GetPickStageLocation(Item, vCarrier, vSite, vCompany,
					stgDirection,null,null,null,null,null);
		var STGMtask = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'stageLocation',stageLocation);
		nlapiLogExecution('DEBUG', 'Name',vDoname);
		nlapiLogExecution('DEBUG', 'DO NO',vdono);
		if(vDoname==null || vDoname=='')
			vDoname = vdono;
		STGMtask.setFieldValue('name', vDoname);
		STGMtask.setFieldValue('custrecord_ebiz_concept_po', vebizOrdNo);
		STGMtask.setFieldValue('custrecord_tasktype', '13');//STG
		STGMtask.setFieldValue('custrecord_wms_location', vlocation);
		STGMtask.setFieldValue('custrecord_actbeginloc', stageLocation);
		STGMtask.setFieldValue('custrecord_actendloc', stageLocation);
		STGMtask.setFieldValue('custrecord_container_lp_no', vContLp);
		STGMtask.setFieldValue('custrecord_ebiz_cntrl_no', vdono);
		STGMtask.setFieldValue('custrecord_ebiz_receipt_no', vdono);
		STGMtask.setFieldValue('custrecord_ebiz_wave_no', vebizWaveNo);
		STGMtask.setFieldValue('custrecordact_begin_date', DateStamp());
		STGMtask.setFieldValue('custrecordact_end_date', DateStamp());
		STGMtask.setFieldValue('custrecord_actualendtime', TimeStamp());
		STGMtask.setFieldValue('custrecord_actualbegintime', TimeStamp());
		STGMtask.setFieldValue('custrecord_sku', Item);
		STGMtask.setFieldValue('custrecord_ebiz_sku_no', Item);
		STGMtask.setFieldValue('custrecord_ebiz_order_no', salesorderintrid);
		STGMtask.setFieldValue('custrecord_ebiz_order_no', salesorderintrid);
		//STGMtask.setFieldValue('custrecord_comp_id', vCompany);
		var currentContext = nlapiGetContext();
		var currentUserID = currentContext.getUser();
		STGMtask.setFieldValue('custrecord_ebizuser', currentUserID);
		var retSubmitLP= nlapiSubmitRecord(STGMtask);
		nlapiLogExecution('DEBUG', 'out of CreateSTGMRecord');

		nlapiLogExecution('DEBUG', 'updating outbound inventory with satge location',stageLocation);
		updateStageInventorywithStageLoc(vContLp,stageLocation);
	}
}


function updateStageInventorywithStageLoc(contlpno,stageLocation)
{
	nlapiLogExecution('DEBUG', 'into updateStageInventorywithStageLoc');
	nlapiLogExecution('DEBUG', 'contlpno',contlpno);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null,'anyof', ['18','36'])); //FLAG.INVENTORY.OUTBOUND('O') ,Inventory in WIP
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is', contlpno));
	filters.push(new nlobjSearchFilter('custrecord_invttasktype', null,'is', '3')); ////Task Type - PICK

	var columns = new Array();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null, filters, columns);
	if (invtsearchresults != null && invtsearchresults != '' && invtsearchresults.length > 0) 
	{
		for (var i = 0; i < invtsearchresults.length; i++)
		{
			var invtid=invtsearchresults[i].getId();

			nlapiLogExecution('DEBUG', 'Deleting Outbound Inventory....',invtid);
			nlapiSubmitField('customrecord_ebiznet_createinv', invtid, 'custrecord_ebiz_inv_binloc', stageLocation);

//			var stgmInvtRec = nlapiLoadRecord('customrecord_ebiznet_createinv',invtid);
//			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', stageLocation);
//			var invtrecid = nlapiSubmitRecord(stgmInvtRec, false, true);

			//nlapiLogExecution('DEBUG', 'updateStageInventorywithStageLoc is done',invtrecid);
		}		
	}

	nlapiLogExecution('DEBUG', 'out of updateStageInventorywithStageLoc');
}

/**
 * 
 * @param invtrecid
 * @param vContLp
 * @param Item
 * @param vCarrier
 * @param vSite
 * @param vCompany
 * @param stgDirection
 * @param vqty
 */
function CreateSTGInvtRecord(invtrecid, vContLp, Item, vCarrier, vSite,
		vCompany, stgDirection, vqty,vstageLoc) {
	try {
		nlapiLogExecution('DEBUG', 'into CreateSTGInvtRecord');
		if(vstageLoc==null || vstageLoc=='')
			vstageLoc = GetPickStageLocation(Item, vCarrier, vSite, vCompany,
					stgDirection,null,null,null,null,null);
		nlapiLogExecution('DEBUG', 'vstagLoc', vstageLoc);
		if (vstageLoc != null && vstageLoc != "" && vstageLoc != "-1") {

			var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',
					invtrecid);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', vstageLoc);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
			stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
			stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
			stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
			stgmInvtRec.setFieldValue('custrecord_invttasktype', '3');//STATUS.INBOUND.PUTAWAY_COMPLETE('S')
			stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');			
			nlapiSubmitRecord(stgmInvtRec, false, true);

			nlapiLogExecution('DEBUG', 'out of CreateSTGInvtRecord');
		}
	} catch (e) {
		if (e instanceof nlobjError)
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		else
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}
}


function GetStageLocationFromSTGM(fono,waveno)
{
	nlapiLogExecution('DEBUG', 'Into GetStageLocationFromSTGM');
	nlapiLogExecution('DEBUG', 'fono', fono);
	nlapiLogExecution('DEBUG', 'waveno', waveno);

	var SOSearchResults = new Array();

	var SOFilters = new Array();

	if(fono!=null && fono!='')
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', fono));
	if(waveno!=null && waveno!='')
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveno));
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [13])); // Task Type - STGM

	var SOColumns = new Array();

	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('internalid'));	
	SOColumns[1].setSort(true);

	SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	nlapiLogExecution('DEBUG', 'Out of GetStageLocationFromSTGM',SOSearchResults);

	return SOSearchResults;	
}



function GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,customizetype,
		StageBinInternalId,ordertype){

	nlapiLogExecution('Error', 'into GetPickStageLocation', Item);
	nlapiLogExecution('Error', 'Item', Item);
	nlapiLogExecution('Error', 'vSite', vSite);
	nlapiLogExecution('Error', 'vCompany', vCompany);
	nlapiLogExecution('Error', 'stgDirection', stgDirection);
	nlapiLogExecution('Error', 'vCarrier', vCarrier);
	nlapiLogExecution('Error', 'vCarrierType', vCarrierType);
	nlapiLogExecution('Error', 'ordertype', ordertype);
	nlapiLogExecution('Error', 'customizetype', customizetype);

	//var ordertype='5'; //Order Type ="WORK ORDER";
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();

	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 = "";
	var ItemInfo3 = "";
	var columns = nlapiLookupField('item', Item, fields);
	if(columns!=null){
		ItemFamily = columns.custitem_item_family;
		ItemGroup = columns.custitem_item_group;
		ItemStatus = columns.custitem_ebizdefskustatus;
		ItemInfo1 = columns.custitem_item_info_1;
		ItemInfo2 = columns.custitem_item_info_2;
		ItemInfo3 = columns.custitem_item_info_3;
	}

	nlapiLogExecution('Error', 'ItemFamily', ItemFamily);
	nlapiLogExecution('Error', 'ItemGroup', ItemGroup);
	nlapiLogExecution('Error', 'ItemStatus', ItemStatus);
	nlapiLogExecution('Error', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('Error', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('Error', 'ItemInfo3', ItemInfo3);

	nlapiLogExecution('Error', 'Before Variable declaration');

	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}

	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');
	columns[5] = new nlobjSearchColumn('custrecord_stagerule_ordtype');

	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
	if (vCarrier != null && vCarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

	}
	if (vCarrierType != null && vCarrierType != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}
	if (ordertype != null && ordertype != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@',ordertype]));

	}
	else
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@']));

	if (customizetype != null && customizetype != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@', customizetype]));

	}
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction]));
	filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'anyof', ['3']));

	/*if (StageBinInternalId != null && StageBinInternalId != "")
		//filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
	filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_outboundlocationgroup', 'anyof', StageBinInternalId));*/
	nlapiLogExecution('Error', 'Before Searching of Stageing Rules');

	var vstgLocation,vstgLocationText;
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < searchresults.length; i++) {
			//GetPickStagLpCount    
			//fectch stage location for Ordertype = WORK ORDER		  
			var ordertype = searchresults[i].getText('custrecord_stagerule_ordtype');
			nlapiLogExecution('Error', 'ordertype::', ordertype);
//			if(ordertype == 'WORK ORDER'){
//			vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');
//			vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');
//			}

			vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');
			vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');


			nlapiLogExecution('Error', 'Stageing Location Value::', vstgLocation);
			nlapiLogExecution('Error', 'Stageing Location Text::', vstgLocationText);


			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');

			nlapiLogExecution('Error', 'vnoofFootPrints::', vnoofFootPrints);


			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetPickStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}


			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);
			nlapiLogExecution('Error', 'LocGroup::', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('Error', 'searchresultsloc::', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();
//					var vLocid=searchresultsloc[0].getId();

					nlapiLogExecution('Error', 'vLocid::', vLocid);

					var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');

					//--
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}
			}
		}
	}
	return -1;
}






/**
 * 
 * @param location
 * @param site
 * @param company
 * @returns {Number}
 */
function GetPickStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');

	var outLPCount = 0;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));

	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));

	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');

	return outLPCount;
}

function ValidateStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,vcustomizetype,stagelocation)
{
	try
	{
		nlapiLogExecution('DEBUG','Into ValidateStageLocation',stagelocation);
		nlapiLogExecution('DEBUG','WHLocation',vSite);
		var StageBinInternalIdArray=new Array();
		StageBinInternalIdArray[0]=false;
		var StageBinInternalId=fetchStageInternalId(stagelocation,vSite);
		if(StageBinInternalId!=null&&StageBinInternalId!="")
		{

			//var stagebinloc=GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,vcustomizetype,StageBinInternalId,null);
//			nlapiLogExecution('DEBUG','stagebinloc',stagebinloc);
//			if(stagebinloc!=-1)
//			{
			StageBinInternalIdArray[0]=true;
			StageBinInternalIdArray[1]=StageBinInternalId;
//			}
			/*var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', ['2', '3']));
			filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'noneof', ['14']));
			filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
			if(WHLocation!=null&&WHLocation!="")
				filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', WHLocation]));

			if (vCarrier != null && vCarrier != "") 
				filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

			if (vCarrierType != null && vCarrierType != "")
				filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
			columns[0].setSort();
			columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
			columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
			columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
			columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

			nlapiLogExecution('DEBUG', 'Before Searching of Stageing Rules');


			var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, null);
			if(searchresults!=null&&searchresults!="")
			{
				StageBinInternalIdArray[0]=true;
				StageBinInternalIdArray[1]=StageBinInternalId;
			}*/
		}
		nlapiLogExecution('DEBUG','StageBinInternalIdArray',StageBinInternalIdArray);
		return StageBinInternalIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in ValidateStageLocation',exp);
	}
}

function fetchStageInternalId(stageLoc,vSite)
{
	try
	{
		nlapiLogExecution('DEBUG','Into fetchStageInternalId',stageLoc);
		var FetchBinLocation="";
		var collocGroup = new Array();
		collocGroup[0] = new nlobjSearchColumn('internalid');

		var filtersLocGroup = new Array();
		filtersLocGroup.push(new nlobjSearchFilter('name', null, 'is',stageLoc));
		filtersLocGroup.push(new nlobjSearchFilter('custrecord_ebizlocationtype',null,'anyof',8));//8=Stage
		if(vSite!=null&&vSite!="")
			filtersLocGroup.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null,'anyof',vSite));
		var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
		if(searchresultsloc != null && searchresultsloc != '')
			FetchBinLocation=searchresultsloc[0].getValue('internalid');
		nlapiLogExecution('DEBUG','FetchBinLocation',FetchBinLocation);
		return FetchBinLocation;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in fetchStageInternalId',exp);
	}
}

function getSystemRuleValue(RuleId)
{
	nlapiLogExecution('DEBUG', 'Into getSystemRuleValue... ', RuleId);

	var systemrulevalue='';

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', RuleId);
		filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				systemrulevalue = searchresults[0].getValue('custrecord_ebizrulevalue');
				return systemrulevalue;
			}
			else
				return systemrulevalue;
		}
		else
			return systemrulevalue;
	}
	catch (exp) 
	{
		nlapiLogExecution('DEBUG', 'Exception in GetSystemRules: ', exp);
		return systemrulevalue;
	}	
}
