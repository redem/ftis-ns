/***************************************************************************
     eBizNET Solutions Inc             
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_Menu.js,v $
 *     	   $Revision: 1.1.2.2.4.5.2.1 $
 *     	   $Date: 2015/03/02 14:46:39 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_Menu.js,v $
 * Revision 1.1.2.2.4.5.2.1  2015/03/02 14:46:39  snimmakayala
 * 201410541
 *
 * Revision 1.1.2.2.4.6  2015/02/13 14:51:45  sponnaganti
 * Jawbone Cluster Picking CR Changes
 * CR#  CR14US0323
 *
 * Revision 1.1.2.2.4.5  2014/06/13 08:59:47  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2.4.4  2014/06/12 15:32:21  skavuri
 * Case # 20148880 (LinkButton functionality added to options in RF Screens)
 *
 * Revision 1.1.2.2.4.3  2014/05/30 00:34:23  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.2  2012/12/03 16:06:55  skreddy
 * CASE201112/CR201113/LOG201121
 * added one more menu item
 *
 * Revision 1.1.2.1  2012/11/23 09:10:12  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 *
 */

function WorkorderMenu(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		//Case# 20148880 (LinkButton Functionality)
		var st1,st2,st3,st4;
		st1 = "WO PICKING";
		st2 = "BUILD ASSEMBLY";
		st3 = "WO PUTAWAY";
		st4 = "WO CLUSTER PICKING";
		st5 = "RETURN TO MAIN MENU";
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_wo_picking', 'customdeploy_ebiz_rf_wo_picking_di');
		var linkURL_1 = checkInURL_1; 
		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_wo_putaway_gen', 'customdeploy_ebiz_rf_wo_putaway_gen_di');
		var linkURL_2 = checkInURL_2; 
		var checkInURL_3 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_wo_put_lp', 'customdeploy_ebiz_rf_wo_put_lp_di');
		var linkURL_3 = checkInURL_3; 
		var checkInURL_4 = nlapiResolveURL('SUITELET', 'customscript_rf_wopicking_cluster_no', 'customdeploy_rf_wopicking_cluster_no');
		var linkURL_4 = checkInURL_4; 
		var checkInURL_5 = nlapiResolveURL('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di');
		var linkURL_5 = checkInURL_5;
		var functionkeyHtml=getFunctionkeyScript('_rfpickingmenu'); 
		var html = "<html><head><title>PICKING MENU</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfpickingmenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1. WO PICKING";
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 2. BUILD ASSEMBLY";
		html = html + "				<td align = 'left'> 2. <a href='" + linkURL_2 + "' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 3. WO PUTAWAY";
		html = html + "				<td align = 'left'> 3. <a href='" + linkURL_3 + "' style='text-decoration: none'>" + st3 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 4. WO CLUSTER PICKING";
		html = html + "				<td align = 'left'> 4. <a href='" + linkURL_4 + "' style='text-decoration: none'>" + st4 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 5. RETURN TO MAIN MENU";
		html = html + "				<td align = 'left'> 5. <a href='" + linkURL_5 + "' style='text-decoration: none'>" + st5 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var optedField = request.getParameter('selectoption');
		var optedEvent = request.getParameter('cmdPrevious');


		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_rf_production_menu', 'customdeploy_rf_production_menu_di', false, optedEvent);
		}
		else 
			if (optedField == '1') {
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking', 'customdeploy_ebiz_rf_wo_picking_di', false, optedField);
			}
			else 
				if (optedField == '2') {
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_gen', 'customdeploy_ebiz_rf_wo_putaway_gen_di', false, optedField);
					//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_gen', 'customdeploy_ebiz_rf_wo_putaway_gen_di', false, optedField);
				}

				else
					if (optedField == '3') {
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_put_lp', 'customdeploy_ebiz_rf_wo_put_lp_di', false, optedField);
						//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_gen', 'customdeploy_ebiz_rf_wo_putaway_gen_di', false, optedField);
					}
					else
						if (optedField == '4') {
							response.sendRedirect('SUITELET', 'customscript_rf_wopicking_cluster_no', 'customdeploy_rf_wopicking_cluster_no', false, optedField);

						}
						else
							if (optedField == '5') {
								response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedField);
								//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_gen', 'customdeploy_ebiz_rf_wo_putaway_gen_di', false, optedField);
							}
							else
							{
								var SOarray=new Array();
								SOarray["custparam_screenno"] = 'WOmenu';
								SOarray["custparam_error"] = "Invalid Option";
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}