/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_BuildShipUnits_SL.js,v $
 *     	   $Revision: 1.15.2.17.4.3.2.16.2.1 $
 *     	   $Date: 2015/09/23 14:57:47 $
 *     	   $Author: deepshikha $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_BuildShipUnits_SL.js,v $
 * Revision 1.15.2.17.4.3.2.16.2.1  2015/09/23 14:57:47  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.15.2.17.4.3.2.16  2015/09/04 15:39:25  nneelam
 * case# 201414217
 *
 * Revision 1.15.2.17.4.3.2.15  2014/07/10 06:55:51  skavuri
 * Case# 20149315 Compatibility Issue Fixed
 *
 * Revision 1.15.2.17.4.3.2.14  2014/05/05 14:49:02  sponnaganti
 * case# 20148253
 * (wave number updation)
 *
 * Revision 1.15.2.17.4.3.2.13  2014/03/12 15:47:18  sponnaganti
 * case# 20127593
 * (carton weight zero issue)
 *
 * Revision 1.15.2.17.4.3.2.12  2014/03/06 15:32:49  sponnaganti
 * case# 20127378
 * (Wave filter added due to allow one wave to complete buildship or not)
 *
 * Revision 1.15.2.17.4.3.2.11  2014/02/21 15:16:11  rmukkera
 * no message
 *
 * Revision 1.15.2.17.4.3.2.10  2014/02/10 15:01:25  sponnaganti
 * case# 20127104
 * (following code commented because we are already taken ship lp.here again going for ship lp)
 *
 * Revision 1.15.2.17.4.3.2.9  2014/01/06 13:33:05  snimmakayala
 * Case# : 20125731
 * Productivity Report Related Changes
 *
 * Revision 1.15.2.17.4.3.2.8  2013/12/12 16:42:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * prodissue fixes
 *
 * Revision 1.15.2.17.4.3.2.7  2013/11/21 06:40:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to DJN
 *
 * Revision 1.15.2.17.4.3.2.6  2013/09/12 15:35:21  rmukkera
 * Case# 20124318
 *
 * Revision 1.15.2.17.4.3.2.5  2013/08/22 15:05:43  rmukkera
 * Case# 20123991
 *
 * Revision 1.15.2.17.4.3.2.4  2013/06/14 15:58:00  nneelam
 * CASE201112/CR201113/LOG201121
 * Standard Bundle, BuildShip Units:Issue No. 20122622
 *
 * Revision 1.15.2.17.4.3.2.3  2013/05/02 15:37:15  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.15.2.17.4.3.2.2  2013/04/09 13:21:36  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to display results  with Container Lp
 *
 * Revision 1.15.2.17.4.3.2.1  2013/04/02 16:02:08  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * TSg Issue fixes
 *
 * Revision 1.15.2.17.4.3  2013/02/21 07:20:39  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.15.2.17.4.2  2012/11/23 06:34:07  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue related to Duplicate rows
 *
 * Revision 1.15.2.17.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.15.2.17  2012/09/04 07:24:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Restricting user not to perform buildship unit if any open picks present for the SO under the given wave.
 *
 * Revision 1.15.2.16  2012/08/31 11:48:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Build ship units
 *
 * Revision 1.15.2.15  2012/08/24 12:03:16  mbpragada
 * 20120490
 * GUI Build ship unit screen is overwriting the user assigned ship
 * LP with system generated one. Fisk will be having pre printed ship lp's
 *  that need to be assigned for every pallet/skid
 *
 * Revision 1.15.2.14  2012/07/23 06:30:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Dynacraft UAT issue fixes
 *
 * Revision 1.15.2.13  2012/07/10 23:35:16  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuning
 *
 * Revision 1.15.2.12  2012/06/21 10:15:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.15.2.11  2012/05/31 12:49:33  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.15.2.10  2012/04/20 13:42:40  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.15.2.9  2012/03/13 15:45:07  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to displaying Location is resolved.
 *
 * Revision 1.15.2.8  2012/03/06 10:55:51  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to fetching record from LpMaster is resolved.
 *
 * Revision 1.15.2.7  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.22  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.21  2012/02/20 12:56:04  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.20  2012/02/17 16:22:27  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * buildshipunits
 *
 * Revision 1.19  2012/02/16 00:52:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.18  2012/02/15 13:24:47  gkalla
 * CASE201112/CR201113/LOG201121
 * Round the weight value
 *
 * Revision 1.17  2012/02/07 06:42:09  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.15.2.4
 *
 * Revision 1.16  2012/02/01 06:19:26  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.15.2.3
 *
 * Revision 1.15  2012/01/17 23:41:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * BuildShipUnit Issue fixes
 *
 * Revision 1.14  2011/12/12 13:07:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * spelling corrections in success message
 *
 * Revision 1.13  2011/12/12 13:04:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * spelling corrections in success message
 *
 * Revision 1.12  2011/11/23 11:16:34  schepuri
 * CASE201112/CR201113/LOG201121
 * splitting load and depart trailer functionality
 *
 * Revision 1.11  2011/11/04 12:15:32  spendyala
 * CASE201112/CR201113/LOG201121
 * opentask record is been updated with containerLPNo. before moving rec to closed task.
 *
 * Revision 1.10  2011/10/05 09:35:30  gkalla
 * CASE201112/CR201113/LOG201121
 * For Lot# entry updation
 *
 * Revision 1.9  2011/09/24 08:46:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/09/23 13:31:43  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/08/26 11:16:15  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/08/26 10:50:49  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/08/25 15:01:14  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/07/21 07:01:02  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
//Common Variables
var lpErrorMessage="";
var vSiteId;
var vCompId,vwhlocation="";
var vGetIdLPRange;
var dataNotFoundMsg="";


/**
 * Function to fill all Wave numbers in the dropdownlist and to add dropdownlist to Form
 * 
 * @param form
 * @param VQbwave
 *
 */
function addWaveNo(form,VQbwave,WaveField){
	//var WaveField = form.addField('custpage_wave', 'select', 'Wave');
	//WaveField.setLayoutType('startrow', 'none');
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); //PICK 
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28'])); //statusflag='C' and STATUS.OUTBOUND.PACK_COMPLETE(Close)
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty'));
	WaveField.addSelectOption("", "");

	var Columns = new Array();
	Columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');	 
	Columns[0].setSort(true);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, Columns);	

	if(searchresults != null)
		nlapiLogExecution('DEBUG', 'SO count', searchresults.length);
	else
		nlapiLogExecution('DEBUG', 'SO count', "null");
	for (var i = 0; i < searchresults.length; i++) {
		WaveField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_wave_no'), searchresults[i].getValue('custrecord_ebiz_wave_no'));
	}
	WaveField.setDefaultValue(VQbwave);	
}

/**
 * Function to fill Container Size Ids in the dropdownlist and to add dropdownlist to Form
 * 
 * @param form
 *
 */
function addSizeId(form){ 
	form.addField('custpage_sizeid', 'select', 'Size ID','customrecord_ebiznet_container').setMandatory(true); 
}

/**
 * Function to get Data from Opentask and related data from MasterLP
 * 
 * @param VQbwave
 * @param VOrdNo
 * @param VShipLP
 * @param VConsignee
 * @param searchresultsTemp
 * @returns
 * 
 */
function GetWaveOrdDetails(VQbwave,VOrdNo,VShipLP,VConsignee,searchresultsTemp)
{
	nlapiLogExecution('ERROR','Into GetWaveOrdDetails');
	nlapiLogExecution('ERROR',"VQbwave ", VQbwave );
	nlapiLogExecution('ERROR',"OrdNo ", VOrdNo );
	nlapiLogExecution('ERROR',"VShipLP ", VShipLP );

	var filters = new Array();
	var searchresults =new Array();

	if(VQbwave!="")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', [VQbwave]));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));  //Pick Task
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28'])); //statusflag='C' and STATUS.OUTBOUND.PACK_COMPLETE
	if(VOrdNo!="")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [VOrdNo])); //for OrdNo'
	if(VShipLP!="")
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', VShipLP)); //for Ship LP No'	

	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_line_no');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_act_qty');
	columns[3] = new nlobjSearchColumn('custrecord_tasktype');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_ship_lp_no');
	columns[5] = new nlobjSearchColumn('custrecord_actendloc');
	columns[6] = new nlobjSearchColumn('custrecord_sku');
	columns[7] = new nlobjSearchColumn('name');    	
	columns[8] = new nlobjSearchColumn('custrecord_container_lp_no');    	 
	columns[9] = new nlobjSearchColumn('custrecord_sku_status');
	columns[10] = new nlobjSearchColumn('custrecord_packcode');
	columns[11] = new nlobjSearchColumn('custrecord_uom_id');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');		
	columns[14] = new nlobjSearchColumn('custrecord_totalcube');
	columns[15] = new nlobjSearchColumn('custrecord_total_weight');
	columns[16] = new nlobjSearchColumn('custrecord_comp_id');
	columns[17] = new nlobjSearchColumn('custrecord_site_id');
	columns[18] = new nlobjSearchColumn('custrecord_batch_no');
	columns[19] = new nlobjSearchColumn('custrecord_wms_location');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[8].setSort();

	// To get the data from Opentask based on selection criteria
	searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults !=null && searchresults != "" && searchresults.length>0)
	{
		nlapiLogExecution('ERROR',"OpenTask Record Count", searchresults.length );

		/*columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');


		// To get the Container Details from master lp
		var searchresultsLP = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, null, columns);
		 */

		//var searchresultsLP=GetLP(null);


		searchresultsTemp=searchresults;
		var searchData=new Array();


		// TO map container lp# of opentask with lp# of mast lp
//		if(searchresultsLP != null && searchresultsLP != "" && searchresultsLP.length>0)
//		{
		//vCompId=searchresults[0].getValue('custrecord_comp_id');
		//vSiteId=searchresults[0].getValue('custrecord_site_id');

		for(var i=0;i<searchresults.length;i++)
		{
			vCompId=searchresults[i].getValue('custrecord_comp_id');
			vSiteId=searchresults[i].getValue('custrecord_site_id');
			vwhlocation=searchresults[i].getValue('custrecord_wms_location');

			var containerlpno=searchresults[i].getValue('custrecord_container_lp_no') ;
//			nlapiLogExecution('ERROR',"Container LP in Opentask",containerlpno );
//			nlapiLogExecution('ERROR',"vwhlocation in Opentask",vwhlocation );

			var searchresultsLP=GetLPdetails(containerlpno,vwhlocation);				



			if(searchresultsLP != null && searchresultsLP != "" && searchresultsLP.length>0)
			{
				nlapiLogExecution('ERROR',"LP Master Record Count  ",searchresultsLP.length );

				for(var j=0;j<searchresultsLP.length;j++)
				{
//					nlapiLogExecution('ERROR',"Container LP in Opentask",searchresults[i].getValue('custrecord_container_lp_no') );
//					nlapiLogExecution('ERROR',"Container LP in LP Master",searchresultsLP[j].getValue('custrecord_ebiz_lpmaster_lp') );

					if(searchresults[i].getValue('custrecord_container_lp_no') == searchresultsLP[j].getValue('custrecord_ebiz_lpmaster_lp')){

//						nlapiLogExecution('ERROR',"searchresultsLP[j].getId() ",searchresultsLP[j].getId()); 
//						nlapiLogExecution('ERROR',"searchresults[i].getValue('custrecord_container_lp_no') ",searchresults[i].getValue('custrecord_container_lp_no'));
//						nlapiLogExecution('ERROR',"searchresultsLP[j].getValue('custrecord_ebiz_lpmaster_lp') ",searchresultsLP[j].getValue('custrecord_ebiz_lpmaster_lp'));

						searchData.push(new openTaskRec(searchresults[i].getValue('custrecord_line_no'),searchresults[i].getValue('custrecord_ebiz_sku_no'),
								searchresults[i].getValue('custrecord_act_qty'),searchresults[i].getValue('custrecord_tasktype'),searchresults[i].getValue('custrecord_ebiz_ship_lp_no')
								,searchresults[i].getText('custrecord_actendloc'),searchresults[i].getText('custrecord_sku'),searchresults[i].getValue('name')
								,searchresults[i].getValue('custrecord_container_lp_no'),searchresults[i].getText('custrecord_sku_status'),searchresults[i].getValue('custrecord_packcode')
								,searchresults[i].getValue('custrecord_uom_id'),searchresults[i].getValue('custrecord_ebiz_sku_no'),searchresults[i].getValue('custrecord_ebiz_cntrl_no')
								,searchresultsLP[j].getValue('custrecord_ebiz_lpmaster_lp'),searchresultsLP[j].getValue('custrecord_ebiz_lpmaster_sizeid')
								,searchresultsLP[j].getValue('custrecord_ebiz_lpmaster_totwght'),searchresultsLP[j].getValue('custrecord_ebiz_lpmaster_totcube')
								,searchresults[i].getValue('custrecord_total_weight'),searchresults[i].getValue('custrecord_totalcube'),searchresults[i].getValue('custrecord_batch_no'),searchresults[i].getValue('custrecord_site_id')
								, searchresults[i].getId(), searchresultsLP[j].getId(),searchresults[i].getValue('custrecord_wms_location'),searchresults[i].getValue('custrecord_sku'),searchresults[i].getValue('custrecord_ebiz_order_no'),searchresults[i].getValue('custrecord_ebiz_wave_no')));


						break;
					}

				}	
			}
			else
			{
				nlapiLogExecution('ERROR',"LP Master Record Count is zero");
				dataNotFoundMsg="No records found for the given search criteria";
			}
		}
		searchresults=searchData;
		//nlapiLogExecution('ERROR',"searchresults Data ",searchresults.length );
//		}
//		else
//		{
//		dataNotFoundMsg="No records found for the given search criteria";
//		}
		if(searchresults == null || searchresults == "")
		{
			dataNotFoundMsg="No records found for the given search criteria";
		}
	}
	else
	{
		dataNotFoundMsg="No records found for the given search criteria";
	}
	return searchresults;
}
var SResults=new Array();
function GetLP(val)
{
	// To get the Container Details from master lp


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
	columns[4] = new nlobjSearchColumn('internalid');
	columns[0].setSort();
	var filters = new Array();
	if(val!= null && val!="")
	{			
		filters[0] = new nlobjSearchFilter('id', null, 'greaterthan', val);
	}
	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	if(SearchResults!= null && SearchResults!="" && SearchResults .length>0)
	{
		nlapiLogExecution('ERROR',"searchresults MasterLP ",SearchResults.length );
		for(var i=0;i<SearchResults.length;i++)
		{
			SResults.push(SearchResults[i]);
		}
		if(SearchResults.length>=1000)
		{
			var maxno=SearchResults[SearchResults.length-1].getId();
			nlapiLogExecution('ERROR',"maxno MasterLP ",maxno );
			var searchresult2=GetLP(maxno);
		}
	}
	return SResults;
}

function GetLPdetails(contlp,vwhlocation)
{
	// To get the Container Details from master lp

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
	columns[4] = new nlobjSearchColumn('internalid');
	columns[0].setSort();
	//case# 20127593 starts (carton weight zero issue)
	columns[4].setSort(true);
	//case# 20127593 end
	var filters = new Array();
	if(contlp!= null && contlp!="")
	{			
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', contlp));
	}
	if(vwhlocation!= null && vwhlocation!="")
	{			
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', ["@NONE@",vwhlocation]));
	}
	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);

	return SearchResults;


}
/**
 * Function to get auto generated Ship LP# from lp range
 *  
 * @returns Auto generated ShipLP
 * 
 */
function getAutoShipLP(vWhLoc)
{
	var AutoShipLPNo="1";	 
	AutoShipLPNo=GetMaxLPNo('1', '3',vWhLoc);// for Auto generated with SHIP LPType
	return AutoShipLPNo;
}

/**
 * Function to Trim a string
 * @param string 
 * @returns trimmed string
 * 
 */
function Trim(s) 
{	  
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r'))
	{
		s = s.substring(1,s.length);
	}

	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length) == '\n') || (s.substring(s.length-1,s.length) == '\r'))
	{
		s = s.substring(0,s.length-1);
	}
	return s;
}
/**
 * To check manually generated Ship LP# exists in master LP or not
 * 
 * @param manualShipLP   
 * @returns message with proper error message
 */
function MastLPExists(manualShipLP,whloc)
{
	nlapiLogExecution('ERROR',"into MastLPExists", 'done');
	var message="";
	var filters =new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', manualShipLP));

	if(whloc!=null && whloc!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', ['@NONE@',whloc]));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag'));

	// To get the data from Master LP based on selection criteria
	searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	if(searchresults!= null && searchresults.length>0)
	{
		var blnClosedLP=false;
		for(var i=0;i<searchresults.length;i++)
		{
			if(searchresults[i].getValue('custrecord_ebiz_lpmaster_wmsstatusflag')=='28') //STATUS.OUTBOUND.PACK_COMPLETE
			{
				blnClosedLP=true; 
				break;
			}					
		}
		if(blnClosedLP==true)
		{ 
			message="Ship LP# already exists, Please enter another Ship LP#";
			nlapiLogExecution('ERROR',"message ",message );
			return message;

		}
		else
		{
			nlapiLogExecution('ERROR',"message ",message );
			return message;
		}

	}
	else
	{
		if(ebiznet_LPRange_CL_withLPType(manualShipLP,'2','3'))// for User Defined with SHIP LPType
		{
			nlapiLogExecution('ERROR',"message ",message );
			return message;
		}
		else
		{
			message="Ship LP# is outof range";
			nlapiLogExecution('ERROR',"message ",message );
			return message;
			//showInlineMessage(form, 'Error', 'Ship LP# is outof range', "");
			//response.writePage(form); 
		}
	}
}


/**
 * 
 * @param request
 * @param response
 * @param form
 */
//function processSelectedOrders(request,response,form)
//{
//var uompackflag="";
//var vWhLoc='';
//var now = new Date();		
//var lineCnt = request.getLineItemCount('custpage_items');
//for (var n = 1; n <= lineCnt; n++) {
//vWhLoc=request.getLineItemValue('custpage_items', 'custpage_wmslocation', n);
//}
////var vshipLp= request.getParameter('custpage_shiplp');
//var matchFound = true;

//var actualDate = DateStamp();
//var actualTime = TimeStamp();
//var sizeId;
//var item;
//if (request.getParameter('custpage_sizeid')!=null && request.getParameter('custpage_sizeid')!="" )		{
//sizeId=request.getParameter('custpage_sizeid');		}

//var vDIms=getContainerCubeAndTarWeight(sizeId,"");// To get Container weight and volume from container to calculate tar weight and cube

//var TotWeight=0,TotVolume=0,inventoryInout;
//var vTareWeight=0,vTareVol=0;
//if(vDIms.length >0)			{
//inventoryInout=vDIms[2];
//if(vDIms[1] != null && vDIms[1]!="")
//vTareWeight=parseFloat(vDIms[1]);
//if(vDIms[0] != null && vDIms[0]!="")
//vTareVol=parseFloat(vDIms[0]);
//}
//var NewShipLPNo;
//if( request.getParameter('custpage_shiplp')== null || Trim(request.getParameter('custpage_shiplp'))=="")
//{		
////To calulate auto Buildship# from LP Range
//var AutoShipLPNo=getAutoShipLP(vWhLoc);
//NewShipLPNo=AutoShipLPNo;
//}
//else
//{
//var manualShipLP=Trim(request.getParameter('custpage_shiplp'));

//lpErrorMessage= MastLPExists(manualShipLP,vWhLoc);//check manual ship lp exists
//if(lpErrorMessage=="")
//{
//NewShipLPNo=manualShipLP;
//} 
//else
//matchFound = false; 
//}
//if(matchFound){
//nlapiLogExecution('ERROR',"if: matchFound ", matchFound );
//nlapiLogExecution('ERROR',"AutoShipLPNo ", NewShipLPNo );

//var picktaskid;
//var LpMasterId;
//var SONo;
//var item;
//var vBoolAnythingChecked=false;
//for (var k = 1; k <= lineCnt; k++) {
//var chkVal = request.getLineItemValue('custpage_items', 'custpage_so', k);
//picktaskid = request.getLineItemValue('custpage_items', 'custpage_pickid', k);
//LpMasterId = request.getLineItemValue('custpage_items', 'custpage_lpgid', k);
//uompackflag = request.getLineItemValue('custpage_items', 'custpage_uomid', k);
//nlapiLogExecution('ERROR', "uompackflag  " , uompackflag);
////SONo = request.getLineItemValue('custpage_items', 'custpage_sono', k);

//var vCartWeight = request.getLineItemValue('custpage_items', 'custpage_cartwt', k);
//var vCartVol = request.getLineItemValue('custpage_items', 'custpage_cartvol', k);
//var vContainerLp=request.getLineItemValue('custpage_items','custpage_cartonid',k);
//nlapiLogExecution('ERROR','vConatierLp',vContainerLp);
//if (chkVal == 'T') {//If it is checked
////To calculate Total weight and volume
//vBoolAnythingChecked=true;
//if(vCartWeight!=null && vCartWeight !="")
//TotWeight+=parseFloat(vCartWeight);
//if(vCartVol!=null && vCartVol !="")
//TotVolume+=parseFloat(vCartVol);


//nlapiLogExecution('ERROR', "Line Count " , k);
//nlapiLogExecution('ERROR', "picktaskid" , picktaskid);
//nlapiLogExecution('ERROR', "LpMasterId" , LpMasterId);

////To update ShipLP# in opentask
//var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', picktaskid);
//if(transaction!=null && transaction!='')
//{
//transaction.setFieldValue('custrecord_wms_status_flag', "7"); // for status B(Built onto Ship Unit)
//transaction.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
//transaction.setFieldValue('custrecord_container_lp_no',vContainerLp);//code added by suman for updating record with conatinerlp#
//SONo = transaction.getFieldValue('custrecord_ebiz_order_no');
//item = transaction.getFieldValue('custrecord_sku');
//nlapiSubmitRecord(transaction, true);
//nlapiLogExecution('ERROR', 'SONo', SONo);
//nlapiLogExecution('ERROR', 'opentask updated ', "");
//}

////To update ShipLP# in mast LP

//var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', LpMasterId); 			 
//transaction.setFieldValue('custrecord_ebiz_lpmaster_masterlp', NewShipLPNo);
//nlapiSubmitRecord(transaction, true);

//nlapiLogExecution('ERROR', 'LP Master updated ', ""); 
//MoveTaskRecord(picktaskid);

////Moving Pick Task to EbizTask
//nlapiLogExecution('ERROR', 'eBiztask updated ', "");

//}
//}
////If atleast one item checked need to update checked items in opentask and LP Master
//if(vBoolAnythingChecked)
//{
//var blnClosedFlag;
//var ebizWaveNo='';
//if (request.getParameter('custpage_closeflag')!=null && request.getParameter('custpage_closeflag')!="" )
//{
//blnClosedFlag=request.getParameter('custpage_closeflag');
//}
//nlapiLogExecution('ERROR', 'blnClosedFlag ', blnClosedFlag);

////var vCompId="";
////var vSiteId="";
////If inventory Inour is IN then 
////total weight=tot containers weight + tare weight and tot cube=tot containers cube + tare cube
//if(inventoryInout=="1" || inventoryInout =="IN")
//{
//TotWeight+=vTareWeight;
//TotVolume=vTareVol;
//}
//if (request.getParameter('custpage_wave')!=null && request.getParameter('custpage_wave')!="" )
//{
//ebizWaveNo=request.getParameter('custpage_wave');
//}
//var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
////populating the fields
//customrecord.setFieldValue('custrecord_tasktype', '4'); //Ship Task
//customrecord.setFieldValue('custrecord_container', sizeId); //Ship Task 
//customrecord.setFieldValue('custrecord_ebiz_wave_no', ebizWaveNo);
//customrecord.setFieldValue('custrecord_totalcube', TotVolume);
//customrecord.setFieldValue('custrecord_total_weight', TotWeight);
//customrecord.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
//customrecord.setFieldValue('name', NewShipLPNo);
//customrecord.setFieldValue('custrecord_sku', item);
//nlapiLogExecution('ERROR', 'item 1', item);
//customrecord.setFieldValue('custrecordact_begin_date', actualDate);
//if(SONo != null && SONo != '')
//customrecord.setFieldValue('custrecord_ebiz_order_no', SONo);

////customrecord.setFieldValue('custrecord_ebiz_order_no', SONo);//added by shylaja
////customrecord.setFieldValue('custrecord_container_lp_no',request.getLineItemValue('custpage_items','custpage_cartonid',1));
//nlapiLogExecution('ERROR', 'blnClosedFlag ', blnClosedFlag);
//if(blnClosedFlag=='T')
//{
//customrecord.setFieldValue('custrecord_wms_status_flag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
//nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 28);
//}
//else
//{
//customrecord.setFieldValue('custrecord_wms_status_flag', '7');// For Build ship
//nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 7);
//}

////customrecord.setFieldValue('custrecord_wms_status_flag', '7');// for status B(Built onto Ship Unit)
//customrecord.setFieldValue('custrecord_actualbegintime', actualTime);
////customrecord.setFieldValue('custrecord_comp_id', vCompId); //For Compid
//customrecord.setFieldValue('custrecord_site_id', vSiteId);//For SiteId 
//nlapiLogExecution('ERROR', 'Submitting SHIP record', 'TRN_OPENTASK');

////commit the record to NetSuite
//var recid = nlapiSubmitRecord(customrecord, true);

//nlapiLogExecution('ERROR', 'Submitted SHIP record', 'TRN_OPENTASK');
///* 
//var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', picktaskid);
//transaction.setFieldValue('custrecord_wms_status_flag', "7");
//transaction.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
//nlapiSubmitRecord(transaction, true);
//customrecord=null;
//*/ 
////To Create record in LP Master
////code added by mahesh

//var finaltext="";
//var duns="";
//var label="",uom="0",ResultText="";


//var filters = new Array();
//filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', 5));
//filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', 1));
//if(vWhLoc!=null && vWhLoc!='')
//filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', [vWhLoc]));

//var columns = new Array();
//columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
//columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_begin');
//columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_end');
//columns[3] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype');
//columns[4] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype');
//columns[5] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

//var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);
////nlapiLogExecution('ERROR', 'Lp Range Searchresults', searchresults.length);
//if (searchresults !=null && searchresults !="") 
//{
////for (var i = 0; i < searchresults.length; i++) 
////{
//try 
//{
//var company = searchresults[0].getText('custrecord_ebiznet_lprange_company');	
//var getLPrefix = searchresults[0].getValue('custrecord_ebiznet_lprange_lpprefix');					
//var varBeginLPRange = searchresults[0].getValue('custrecord_ebiznet_lprange_begin');
//var varEndRange = searchresults[0].getValue('custrecord_ebiznet_lprange_end');
//var getLPGenerationTypeValue = searchresults[0].getValue('custrecord_ebiznet_lprange_lpgentype');
//var getLPTypeValue = searchresults[0].getText('custrecord_ebiznet_lprange_lptype');

//var lpMaxValue=GetMaxLPNo('1', '5',vWhLoc);
//nlapiLogExecution('ERROR', 'lpMaxValue', lpMaxValue);

//var prefixlength=lpMaxValue.length;
//nlapiLogExecution('ERROR', 'prefixlength', prefixlength);

//if(prefixlength==0)
//label="000000000";
//else if(prefixlength==1)
//label="00000000"+lpMaxValue;
//else if(prefixlength==2)
//label="0000000"+lpMaxValue;
//else if(prefixlength==3)
//label="000000"+lpMaxValue;
//else if(prefixlength==4)
//label="00000"+lpMaxValue;
//else if(prefixlength==5)
//label="0000"+lpMaxValue;
//else if(prefixlength==6)
//label="000"+lpMaxValue;
//else if(prefixlength==7)
//label="00"+lpMaxValue;
//else if(prefixlength==8)
//label="0"+lpMaxValue;
//else if(prefixlength==9)
//label=lpMaxValue;
//nlapiLogExecution('ERROR', 'label', label);

//var dunsfilters = new Array();
//dunsfilters[0] = new nlobjSearchFilter('custrecord_company', null, 'is', company);
//var dunscolumns = new Array();
//dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

//var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
//if (dunssearchresults !=null && dunssearchresults != "") 
//{
//duns = dunssearchresults[0].getValue('custrecord_compduns');
//nlapiLogExecution('ERROR', 'duns', duns);
//}

//if(uompackflag == "1" ) 
//uom="0"; 
//else if(uompackflag == "3") 
//uom="2";
//else
//uom="0";
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord uom', uom);
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
//finaltext=uom+duns+label;
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);



////to get chk digit
//var checkStr=finaltext;
//var ARL=0;
//var BRL=0;var CheckDigitValue="";
//for (i = checkStr.length-1;  i > 0;  i--)
//{
//ARL = ARL+parseFloat(checkStr.charAt(i));
//i--;
//}
////alert(ARL);
//ARL=ARL*3;
//for (i = checkStr.length-2;  i > 0;  i--)
//{
//BRL = BRL+parseFloat(checkStr.charAt(i));
//i--;
//}
////alert(BRL);
//var sumOfARLBRL=ARL+BRL;
//var CheckDigit=0;

//while(CheckDigit<10)
//{
//if(sumOfARLBRL%10==0)
//{ 
//CheckDigitValue=CheckDigit; 
//break; 
//} 
//sumOfARLBRL++;
//CheckDigit++;
//}
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
//ResultText="00"+finaltext+CheckDigitValue.toString();
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord ResultText', ResultText);
//} 
//catch (err) 
//{

//}
////}
//} 
//var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp'); 
////populating the fields
////customrecord.setFieldValue('custrecord_ebiz_lpmaster_masterlp', NewShipLPNo);
//customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', NewShipLPNo);
//customrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', sizeId);
//customrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotWeight);
//customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', '3');//SHIP
//customrecord.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
//customrecord.setFieldValue('name', NewShipLPNo);
////customrecord.setFieldValue('custrecord_sku', itemname);
//customrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', TotVolume);
//nlapiLogExecution('ERROR', 'blnClosedFlag ', blnClosedFlag);
//var status;
//if(blnClosedFlag=='T')
//{
//customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
//nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 28);
//}
//else
//{
//customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '7');// For Build ship
//nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 7);
//}

////commit the record to NetSuite
//var recid = nlapiSubmitRecord(customrecord, true);

//var vWave = request.getParameter('custpage_wave');
//var SOarray = new Array();
//SOarray["custpage_wave"] = vWave;
//var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
//var BuildShipUnitCompletedBoolValue = blnBuildShipUnitCompleted(NewShipLPNo);
//nlapiLogExecution('ERROR', "BuildShipUnitCompletedBoolValue start", BuildShipUnitCompletedBoolValue);




//if(BuildShipUnitCompletedBoolValue == true)
//{nlapiLogExecution('ERROR', 'if ', 'shipcompleted');
//response.sendRedirect('SUITELET', 'customscript_loadunloadtrailer', 'customdeploy_loadunloadtrailer', false, SOarray);
//}
//else
//{nlapiLogExecution('ERROR', 'else ', 'shipnotcompleted');
////showInlineMessage(form, 'Error', lpErrorMessage, null);
//msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Ship LP Generated :" + NewShipLPNo + "(" + NewShipLPNo + ",)', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
//response.writePage(form);

//}

///*commented code is moved to above condition
//* var vWave = request.getParameter('custpage_wave');
//var SOarray = new Array();
//SOarray["custpage_wave"] = vWave;
//var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
//var BuildShipUnitCompletedBoolValue = blnBuildShipUnitCompleted(NewShipLPNo);
//nlapiLogExecution('ERROR', "BuildShipUnitCompletedBoolValue start", BuildShipUnitCompletedBoolValue);




//if(BuildShipUnitCompletedBoolValue == true)
//{nlapiLogExecution('ERROR', 'if ', 'if');
//response.sendRedirect('SUITELET', 'customscript_loadunloadtrailer', 'customdeploy_loadunloadtrailer', false, SOarray);
//}
//else
//{nlapiLogExecution('ERROR', 'else ', 'else');
////showInlineMessage(form, 'Error', lpErrorMessage, null);
//msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Ship LP Generated :" + NewShipLPNo + "(" + NewShipLPNo + ",)', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
//response.writePage(form);*/


//}
//else
//{
//var lpErrorMessage1 = 'Please select atleast one line ';
//showInlineMessage(form, 'Error', lpErrorMessage1, null);
//response.writePage(form);
//}
//}
//else
//{
//showInlineMessage(form, 'Error', lpErrorMessage, null);
//response.writePage(form);
//}

////return matchFound;
//}


/**
 * 
 * @param request
 * @param response
 * @param form
 */
function processSelectedOrders(request,response,form,msg)
{
	try {
		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();

		var uompackflag="";
		var vWhLoc='';
		var now = new Date();		
		var lineCnt = request.getLineItemCount('custpage_items');
		for (var n = 1; n <= lineCnt; n++) {
			vWhLoc=request.getLineItemValue('custpage_items', 'custpage_wmslocation', n);
		}
		//var vshipLp= request.getParameter('custpage_shiplp');
		var matchFound = true;

		var actualDate = DateStamp();
		var actualTime = TimeStamp();
		var sizeId;
		var item;
		if (request.getParameter('custpage_sizeid')!=null && request.getParameter('custpage_sizeid')!="" )		{
			sizeId=request.getParameter('custpage_sizeid');		}

		var vDIms=getContainerCubeAndTarWeight(sizeId,"");// To get Container weight and volume from container to calculate tar weight and cube

		var TotWeight=0,TotVolume=0,inventoryInout;
		var vTareWeight=0,vTareVol=0;
		if(vDIms.length >0)			{
			inventoryInout=vDIms[2];
			if(vDIms[1] != null && vDIms[1]!="")
				vTareWeight=parseFloat(vDIms[1]);
			if(vDIms[0] != null && vDIms[0]!="")
				vTareVol=parseFloat(vDIms[0]);
		}
		var NewShipLPNo;var voldContainerLp="";	
		nlapiLogExecution('ERROR',"custpage_shiplp ", request.getParameter('custpage_shiplp') );
		if( request.getParameter('custpage_shiplp')== null || Trim(request.getParameter('custpage_shiplp'))=="")
		{		
			nlapiLogExecution('ERROR',"into null", 'done');
			//To calulate auto Buildship# from LP Range
			var AutoShipLPNo=getAutoShipLP(vWhLoc);
			NewShipLPNo=AutoShipLPNo;
		}
		else
		{
			nlapiLogExecution('ERROR',"into manual", 'done');
			var manualShipLP=Trim(request.getParameter('custpage_shiplp'));

			lpErrorMessage= MastLPExists(manualShipLP,vWhLoc);//check manual ship lp exists
			nlapiLogExecution('ERROR',"lpErrorMessage", lpErrorMessage);
			if(lpErrorMessage=="")
			{
				NewShipLPNo=manualShipLP;
			} 
			else
				matchFound = false; 
		}


		if(matchFound){
			nlapiLogExecution('ERROR',"if: matchFound ", matchFound );
			nlapiLogExecution('ERROR',"AutoShipLPNo ", NewShipLPNo );

			var picktaskid;
			var LpMasterId;
			var SONo;
			var item;
			var vBoolAnythingChecked=false;
			var wavenum;
			var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record

			for (var k = 1; k <= lineCnt; k++) {
				var chkVal = request.getLineItemValue('custpage_items', 'custpage_so', k);
				picktaskid = request.getLineItemValue('custpage_items', 'custpage_pickid', k);
				LpMasterId = request.getLineItemValue('custpage_items', 'custpage_lpgid', k);
				uompackflag = request.getLineItemValue('custpage_items', 'custpage_uomid', k);
				SONo = request.getLineItemValue('custpage_items', 'custpage_orderno', k);
				item = request.getLineItemValue('custpage_items', 'custpage_skuval', k);
				nlapiLogExecution('ERROR', "uompackflag  " , uompackflag);
				var foorderno = request.getLineItemValue('custpage_items', 'custpage_dono', k);
				//SONo = request.getLineItemValue('custpage_items', 'custpage_sono', k);
				//case# 20148253 starts(wave number updation)
				wavenum = request.getLineItemValue('custpage_items', 'custpage_waveno', k);
				//case# 20148253 end
				var vCartWeight = request.getLineItemValue('custpage_items', 'custpage_cartwt', k);
				var vCartVol = request.getLineItemValue('custpage_items', 'custpage_cartvol', k);
				var vContainerLp=request.getLineItemValue('custpage_items','custpage_cartonid',k);
				nlapiLogExecution('ERROR','voldContainerLp',voldContainerLp);
				nlapiLogExecution('ERROR','vConatierLp',vContainerLp);


				if (chkVal == 'T') {//If it is checked

					nlapiLogExecution('ERROR', "k1", k);

					//To calculate Total weight and volume
					vBoolAnythingChecked=true;
					//nlapiLogExecution('ERROR','vCartWeight',vCartWeight);
					if(vCartWeight!=null && vCartWeight !="")
						TotWeight+=parseFloat(vCartWeight);
					//nlapiLogExecution('ERROR','TotWeight',TotWeight);
					if(vCartVol!=null && vCartVol !="")
						TotVolume+=parseFloat(vCartVol);


//					nlapiLogExecution('ERROR', "Line Count " , k);
//					nlapiLogExecution('ERROR', "picktaskid" , picktaskid);
//					nlapiLogExecution('ERROR', "LpMasterId" , LpMasterId);
					//case# 20127104 starts (following code commented because we are already taken ship lp.here again going for ship lp)
					/*if( request.getParameter('custpage_shiplp')== null || Trim(request.getParameter('custpage_shiplp'))==""){
						if(voldContainerLp != vContainerLp)	
						{
							//To calulate auto Buildship# from LP Range
							var AutoShipLPNo=getAutoShipLP(vWhLoc);
							NewShipLPNo=AutoShipLPNo;
						}
					}*/
					//case# 20127104 end
					// To update ShipLP# in opentask
					var fieldNames = new Array(); 
					fieldNames.push('custrecord_wms_status_flag');  	
					fieldNames.push('custrecord_ship_lp_no');
					fieldNames.push('custrecord_container_lp_no');

					var newValues = new Array(); 
					newValues.push('7');
					newValues.push(NewShipLPNo);
					newValues.push(vContainerLp);


					nlapiSubmitField('customrecord_ebiznet_trn_opentask', picktaskid, fieldNames, newValues);
//					nlapiLogExecution('ERROR', 'SONo', SONo);
//					nlapiLogExecution('ERROR', 'item', item);
					nlapiLogExecution('ERROR', 'opentask updated ', "");
					/*var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', picktaskid);
					if(transaction!=null && transaction!='')
					{
						nlapiLogExecution('ERROR', "vContainerLp in update opentask" , vContainerLp);
						nlapiLogExecution('ERROR', "NewShipLPNo in update opentask" , NewShipLPNo);
						transaction.setFieldValue('custrecord_wms_status_flag', "7"); // for status B(Built onto Ship Unit)
						transaction.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
						transaction.setFieldValue('custrecord_container_lp_no',vContainerLp);//code added by suman for updating record with conatinerlp#
						SONo = transaction.getFieldValue('custrecord_ebiz_order_no');
						item = transaction.getFieldValue('custrecord_sku');
						nlapiSubmitRecord(transaction, true);
						nlapiLogExecution('ERROR', 'SONo', SONo);
						nlapiLogExecution('ERROR', 'opentask updated ', "");
					}*/

					// To update ShipLP# in mast LP
					var fieldNames2 = new Array(); 
					fieldNames2.push('custrecord_ebiz_lpmaster_masterlp');  	

					var newValues2 = new Array(); 					 
					newValues2.push(NewShipLPNo);

					nlapiSubmitField('customrecord_ebiznet_master_lp', LpMasterId, fieldNames2, newValues2);

					/*var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', LpMasterId); 			 
					transaction.setFieldValue('custrecord_ebiz_lpmaster_masterlp', NewShipLPNo);
					nlapiSubmitRecord(transaction, true);*/

					nlapiLogExecution('ERROR', 'LP Master updated ', ""); 

					//Commented on 06/18/2012 by Satish.N. Task will be moved to closed task at after departing the trailer
					//MoveTaskRecord(picktaskid);

					//Moving Pick Task to EbizTask
					//nlapiLogExecution('ERROR', 'eBiztask updated ', "");

				}
				if(voldContainerLp != vContainerLp)	
				{				//new
					if(vBoolAnythingChecked)
					{
						var blnClosedFlag;
						var ebizWaveNo='';
						if (request.getParameter('custpage_closeflag')!=null && request.getParameter('custpage_closeflag')!="" )
						{
							blnClosedFlag=request.getParameter('custpage_closeflag');
						}
						//nlapiLogExecution('ERROR', 'blnClosedFlag ', blnClosedFlag);


						var Filers = new Array;
						Filers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SONo)); 
						Filers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '4')); 
						Filers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'isnotempty'));
						Filers.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', [item]));

						if(blnClosedFlag=='T')
							Filers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', '28')); //pack complete
						else
							Filers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', '7'));
						var Columns = new Array();
						Columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');

						var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filers, Columns);

						if(ContainerSearchResults == "" || ContainerSearchResults == null){

							//var vCompId="";
							//var vSiteId="";
							//If inventory Inour is IN then 
							//total weight=tot containers weight + tare weight and tot cube=tot containers cube + tare cube

//							nlapiLogExecution('ERROR','inventoryInout',inventoryInout);
//							nlapiLogExecution('ERROR','inventoryInout',inventoryInout);
//							nlapiLogExecution('ERROR','vTareWeight',vTareWeight);
							if(inventoryInout=="1" || inventoryInout =="IN")
							{
								TotWeight+=vTareWeight;
								TotVolume=vTareVol;
							}
							if (request.getParameter('custpage_wave')!=null && request.getParameter('custpage_wave')!="" )
							{
								ebizWaveNo=request.getParameter('custpage_wave');
							}

							parent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
//							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', NewShipLPNo);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', foorderno);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', '4');
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_current_date', actualDate);
							if(sizeId != null && sizeId != '')
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', sizeId);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', wavenum);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_totalcube', parseFloat(TotVolume).toFixed(5));
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', parseFloat(TotWeight).toFixed(5));
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ship_lp_no', NewShipLPNo);
							if(item != null && item != '')
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', item);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date', actualDate);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', actualDate);
							if(SONo != null && SONo != '')
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', SONo);
							if(blnClosedFlag=='T')
							{
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '28');
								//customrecord.setFieldValue('custrecord_wms_status_flag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
								//nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 28);
							}
							else
							{
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '7');
								//customrecord.setFieldValue('custrecord_wms_status_flag', '7');// For Build ship
								//nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 7);
							}

							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', actualTime);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', actualTime);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', currentUserID);
							//
							if(vWhLoc != null && vWhLoc != '')
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', vWhLoc);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ship_lp_no', NewShipLPNo);
							parent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ship_lp_no', NewShipLPNo);
							parent.commitLineItem('recmachcustrecord_ebiz_ot_parent');
							/*var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
						//populating the fields
						customrecord.setFieldValue('custrecord_tasktype', '4'); //Ship Task
						customrecord.setFieldValue('custrecord_container', sizeId); //Ship Task 
						customrecord.setFieldValue('custrecord_ebiz_wave_no', ebizWaveNo);
						customrecord.setFieldValue('custrecord_totalcube', TotVolume);
						customrecord.setFieldValue('custrecord_total_weight', TotWeight);
						customrecord.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
						customrecord.setFieldValue('name', NewShipLPNo);
						customrecord.setFieldValue('custrecord_sku', item);
						nlapiLogExecution('ERROR', 'item 1', item);
						customrecord.setFieldValue('custrecordact_begin_date', actualDate);
						if(SONo != null && SONo != '')
							customrecord.setFieldValue('custrecord_ebiz_order_no', SONo);

						//customrecord.setFieldValue('custrecord_ebiz_order_no', SONo);//added by shylaja
//						customrecord.setFieldValue('custrecord_container_lp_no',request.getLineItemValue('custpage_items','custpage_cartonid',1));
						nlapiLogExecution('ERROR', 'blnClosedFlag ', blnClosedFlag);
						if(blnClosedFlag=='T')
						{
							customrecord.setFieldValue('custrecord_wms_status_flag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
							nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 28);
						}
						else
						{
							customrecord.setFieldValue('custrecord_wms_status_flag', '7');// For Build ship
							nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 7);
						}
						//customrecord.setFieldValue('custrecord_wms_status_flag', '7');// for status B(Built onto Ship Unit)
						customrecord.setFieldValue('custrecord_actualbegintime', actualTime);
						//customrecord.setFieldValue('custrecord_comp_id', vCompId); //For Compid
						customrecord.setFieldValue('custrecord_site_id', vSiteId);//For SiteId 
							 */						nlapiLogExecution('ERROR', 'Submitting SHIP record', 'TRN_OPENTASK');

							 //commit the record to NetSuite
							 //var recid = nlapiSubmitRecord(customrecord, true);
							 // nlapiLogExecution('ERROR', 'Submitted SHIP record', 'TRN_OPENTASK');					
							 // To Create record in LP Master
							 //code added by mahesh
							 //nlapiLogExecution('ERROR', 'before  ResultText', vWhLoc);
							 var ResultText=GenerateSucc(vWhLoc,uompackflag);
							 nlapiLogExecution('ERROR', 'GenerateSucc result', ResultText);

							 parent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent'); 
							 parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', NewShipLPNo);
							 parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', NewShipLPNo);
							 if(sizeId != null && sizeId != '')
								 parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sizeid', sizeId);
							 parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(5));
							 parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', '3');
							 parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', ResultText);
							 parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totcube', parseFloat(TotVolume).toFixed(5));
							 // nlapiLogExecution('ERROR', 'blnClosedFlag ', blnClosedFlag);
							 var status;
							 if(blnClosedFlag=='T')
							 {
								 parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_wmsstatusflag', '28');
								 //customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
								 //nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 28);
							 }
							 else
							 {
								 parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_wmsstatusflag', '7');
								 //customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '7');// For Build ship
								 //nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 7);
							 }
							 parent.commitLineItem('recmachcustrecord_ebiz_lp_parent');
							 /*						var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp'); 
						//populating the fields
						//customrecord.setFieldValue('custrecord_ebiz_lpmaster_masterlp', NewShipLPNo);
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', NewShipLPNo);
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', sizeId);
						nlapiLogExecution('ERROR','TotWeight',TotWeight);
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotWeight);
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', '3');//SHIP
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
						customrecord.setFieldValue('name', NewShipLPNo);
						//customrecord.setFieldValue('custrecord_sku', itemname);
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', TotVolume);
						nlapiLogExecution('ERROR', 'blnClosedFlag ', blnClosedFlag);
						var status;
						if(blnClosedFlag=='T')
						{
							customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
							nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 28);
						}
						else
						{
							customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '7');// For Build ship
							nlapiLogExecution('ERROR', 'custrecord_wms_status_flag ', 7);
						}

						//commit the record to NetSuite
						var recid = nlapiSubmitRecord(customrecord, true);*/

							 var vWave = request.getParameter('custpage_wave');
							 var vOrdNo = request.getParameter('custpage_ordno');
							 var SOarray = new Array();
							 SOarray["custpage_wave"] = vWave;
							 SOarray["custpage_fulfillmentorder"] = vOrdNo;
							 SOarray["custpage_shiplp"] = NewShipLPNo;	 
							 //var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
							 var BuildShipUnitCompletedBoolValue = blnBuildShipUnitCompleted(NewShipLPNo);
							 nlapiLogExecution('ERROR', "BuildShipUnitCompletedBoolValue start", BuildShipUnitCompletedBoolValue);


							 nlapiLogExecution('ERROR', "k2", k);

							 if(BuildShipUnitCompletedBoolValue == true)
							 {
								 nlapiLogExecution('ERROR', 'if ', 'shipcompleted');
								 //case # skid label 
								 nlapiLogExecution('ERROR', 'NewShipLPNo', NewShipLPNo);
								 nlapiLogExecution('ERROR', 'vOrdNo', SONo);
								 nlapiLogExecution('ERROR', 'vConatierLp', voldContainerLp);
								 // case# 201417314
								 try
								 {
									 var SKIDLabelRule=getSystemRule('Use SKID Label');
									 if(SKIDLabelRule!=null && SKIDLabelRule!='')
									 { 
										 SKIDLabelgeneration(NewShipLPNo,SONo,voldContainerLp);
									 }
								 }
								 catch(e)
								 {
									 nlapiLogExecution('ERROR', 'e', e);
								 }
								 //End of the skid label
								 response.sendRedirect('SUITELET', 'customscript_loadunloadtrailer', 'customdeploy_loadunloadtrailer', false, SOarray);
							 }
							 else
							 {
								 nlapiLogExecution('ERROR', 'else ', 'shipnotcompleted');
								 //showInlineMessage(form, 'Error', lpErrorMessage, null);
								 msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Ship LP Generated :" + NewShipLPNo + "(" + NewShipLPNo + ",)', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
								 response.writePage(form);
							 }					
						}
					}
					else
					{
						var lpErrorMessage1 = 'Please select atleast one line ';
						//showInlineMessage(form, 'Error', lpErrorMessage1, null);
						msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error','"+lpErrorMessage1+"' , NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						response.writePage(form);
					}
				}
				voldContainerLp=vContainerLp;
				nlapiLogExecution('ERROR','NewShipLPNo after',NewShipLPNo);
			}
//			If atleast one item checked need to update checked items in opentask and LP Master
			nlapiSubmitRecord(parent); //submit the parent record, all child records will also be updated
			var context = nlapiGetContext();
			nlapiLogExecution('ERROR','Remaining usage 4',context.getRemainingUsage());
		}
		else
		{
			//showInlineMessage(form, 'Error', lpErrorMessage, '');
			msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error','"+lpErrorMessage+"' , NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			response.writePage(form);
		}
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in Buildshipunit ', exp);

	}

	//return matchFound;
}

/**
 * This is the main function
 * 
 * @param request
 * @param response
 *  
 */

function BuildshipUnitsSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Build Ship Units');

		//To get the values Wave#, Order#, Consignee and Ship LP from Query block		
		var VQbwave = "";
		if (request.getParameter('custpage_wave')!=null && request.getParameter('custpage_wave')!="" )
		{
			VQbwave=request.getParameter('custpage_wave');
		}
		var VOrdNo = "";

		if (request.getParameter('custpage_ordno')!=null && request.getParameter('custpage_ordno')!="" )
		{
			VOrdNo=request.getParameter('custpage_ordno');
		}
		var VConsignee = "";

		if (request.getParameter('custpage_customer')!=null && request.getParameter('custpage_customer')!="" )
		{
			VConsignee=request.getParameter('custpage_customer');
		}
		var VShipLP = "";

		if (request.getParameter('custpage_shiplp')!=null && request.getParameter('custpage_shiplp')!="" )
		{
			VShipLP=request.getParameter('custpage_shiplp');
		}

		//code added to check weather the given wave/order has any open pickes or not
		//added by suman on 310812.
//		case# 201416913
		var results ='F';
		var SOArray=IsOpenPicksPresent(VQbwave,VOrdNo,VShipLP);
		if(SOArray!=null&&SOArray!="")
		{
			var filter = new Array();
			filter[0]= new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', SOArray);
			filter[1]= new nlobjSearchFilter('custrecord_shipcomplete', null, 'is', "T"); 
			//filter[2]= new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickgen_qty}),0)");

			var column = new Array();
			column[0] = new nlobjSearchColumn('custrecord_lineord');
			column[1] = new nlobjSearchColumn('custrecord_shipcomplete');
			column[2] = new nlobjSearchColumn('custrecord_pickgen_qty');
			column[3] = new nlobjSearchColumn('custrecord_ebiz_linesku');

			var searchRec = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);
			if(searchresults!=null&&searchresults!="")
			{
				results='T';
				nlapiLogExecution('ERROR','IsOpenPicks',IsOpenPicks);
			}

		}


		if(results=='T')
		{
			nlapiLogExecution('ERROR','Request cant be processed as there are still Open Picks for the Wave');
			dataNotFoundMsg="Request cant be processed as there are still Open Picks for the Wave";
			showInlineMessage(form, 'Error', dataNotFoundMsg, "");
			response.writePage(form);
		}
		else{
			//end of code as of 31012.
			var searchresultsTemp=new Array();
			// To get the Wave details based on selection criteria
			var searchresults=GetWaveOrdDetails(VQbwave,VOrdNo,VShipLP,VConsignee,searchresultsTemp);
			nlapiLogExecution('ERROR', 'dataNotFoundMsg', 'dataNotFoundMsg');
			nlapiLogExecution('ERROR', 'VQbwave', 'VQbwave');
			if(dataNotFoundMsg=="")
			{
				var WaveField = form.addField('custpage_wave', 'select', 'Wave');
				WaveField.setLayoutType('startrow', 'none');
				addWaveNo(form,VQbwave,WaveField);//To add Wave No dropdown and to fill with wave#s

				var shipLp = form.addField('custpage_shiplp', 'text', 'Ship LP#'); // To add Ship LP Text box to form


				// To set default value to shiplp text box
				if(VShipLP != null && VShipLP != "")
				{
					//shipLp.setDefaultValue(VShipLP);

				}

				addSizeId(form); // To add SIze id ddl and to fill with Size ids

				form.addField("custpage_closeflag", "checkbox", "Close ship unit?");
				// To add Grid Items Header to the form 
				var sublist = form.addSubList("custpage_items", "list", "Item List");	//Case# 20149315	
				sublist.addField("custpage_so", "checkbox", "Confirm").setDefaultValue('T');
				sublist.addField("custpage_waveno", "text", "Wave #");
				sublist.addField("custpage_sono", "text", "SO #");	//--
				sublist.addField("custpage_dono", "text", "Fulfillment Order #");
				sublist.addField("custpage_lineno", "text", "Line #");
				sublist.addField("custpage_tasktype", "text", "TaskType").setDisplayType('hidden');
				sublist.addField("custpage_itemname", "text", "Item");
				sublist.addField("custpage_packcode", "text", "Pack Code");
				sublist.addField("custpage_lot", "text", "LOT#");
				sublist.addField("custpage_itemstatus", "text", "Item Status"); //--
				sublist.addField("custpage_container", "text", "Container").setDisplayType('hidden');
				sublist.addField("custpage_ordqty", "text", "Qty");
				sublist.addField("custpage_weight", "text", "Weight");
				sublist.addField("custpage_volume", "text", "Volume");		
				sublist.addField("custpage_actbeginloc", "text", "Location"); 
				sublist.addField("custpage_cartonid", "text", "Carton ID");		 
				sublist.addField("custpage_cartwt", "text", "Carton Weight");		 
				sublist.addField("custpage_cartvol", "text", "Carton Volume");	 
				//sublist.addField("custpage_contlp", "text", "Container LP");		 
				//sublist.addField("custpage_actlp", "text", "LP");		 
				//sublist.addField("custpage_lpno", "text", "Ship LP #").setDisplayType('entry');
				sublist.addField("custpage_lpno", "text", "Ship LP #").setDisplayType('hidden');
				sublist.addField("custpage_ebizskuno", "text", "eizskuno").setDisplayType('hidden');
				sublist.addField("custpage_ebizdono", "text", "eizdono").setDisplayType('hidden');
				sublist.addField("custpage_pickid", "text", "Pick task Id").setDisplayType('hidden');
				sublist.addField("custpage_uomid", "text", "uom Id").setDisplayType('hidden');
				sublist.addField("custpage_skuno", "text", "SKUNO").setDisplayType('hidden');
				sublist.addField("custpage_skustatus", "text", "SKUStaus").setDisplayType('hidden');
				sublist.addField("custpage_lpgid", "text", "LPId").setDisplayType('hidden');
				sublist.addField("custpage_wmslocation", "text", "WH Location").setDisplayType('hidden');
				sublist.addField("custpage_orderno", "text", "OrderNo").setDisplayType('hidden');
				sublist.addField("custpage_skuval", "text", "SKU Value").setDisplayType('hidden');
				//sublist.addField("custpage_img", "text", "Build Ship");


				//nlapiLogExecution('ERROR', "searchresultsTemp " , searchresultsTemp.length);


				var vline, vitem, vqty, vTaskType, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno,vskustatus,
				vpackcode,vdono,vSOno,vuomid,vbatchNo,vcontLP,vLP,vSizeId,vTotWt,vTotCube,vSiteId;

				for (var i = 0; searchresults != null && i < searchresults.length; i++) {
					var searchresult = searchresults[i];
					//var searchresultTemp = searchresultsTemp[i];	// To get custpage_pickid		 
					vline = searchresult.custrecord_Line_no; 
					vitem = searchresult.custrecord_Ebiz_sku_no;
					//vitem = searchresult.getText('custrecord_ebiz_sku_no');


					if(searchresult.custrecord_Act_qty==null || searchresult.custrecord_Act_qty== "")
						vqty=0;
					else
						vqty= parseFloat(searchresult.custrecord_Act_qty);

					//nlapiLogExecution("ERROR", "vqty ", vqty);

					vTaskType = searchresult.custrecord_Tasktype;
					vLpno = searchresult.custrecord_Ebiz_ship_lp_no;
					vlocationid = searchresult.custrecord_Actendloc;

					vlocation = searchresult.custrecord_Actendloc;
					vskustatusValue= searchresult.custrecord_Sku_status;
					vskustatus= searchresult.custrecord_Sku_status;
					vpackcode= searchresult.custrecord_Packcode;
					vdono=searchresult.Name;
					if(vdono != null && vdono != "")
						vSOno=vdono.split('.')[0];
					vSKUNo = searchresult.custrecord_Sku;
					vSKU = searchresult.custrecord_Sku; 
					vcontLP = searchresult.custrecord_Container_lp_no;
					vLP = searchresult.custrecord_Ebiz_lpmaster_lp;
					vSizeId = searchresult.custrecord_Ebiz_lpmaster_sizeid;
					vTotWt = searchresult.custrecord_Ebiz_lpmaster_totwght;
					vTotCube = searchresult.custrecord_Ebiz_lpmaster_totcube; 
					vuomid=searchresult.custrecord_Uom_id; 
					vbatchNo=searchresult.custrecord_batch_no; 
					vSiteId = searchresult.wmslocation;
					var vOrderNo = searchresult.ordNo;
					var vSKUVal = searchresult.SkuVal;
					if(vTotCube!=null && vTotCube!='')
						vTotCube=Math.round(vTotCube*Math.pow(10,2))/Math.pow(10,2);

					VQbwave = searchresult.custrecord_ebiz_wave_no;
					//nlapiLogExecution('ERROR', "vuomid grid " , vuomid);
					//nlapiLogExecution('ERROR', "vbatchNo grid " , vbatchNo);
					form.getSubList('custpage_items').setLineItemValue('custpage_waveno', i + 1, VQbwave);
					form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1, vline);
					form.getSubList('custpage_items').setLineItemValue('custpage_tasktype', i + 1, vTaskType);
					form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, vSKU);
					form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, parseFloat(vqty).toFixed(0));
					form.getSubList('custpage_items').setLineItemValue('custpage_lpno', i + 1, vLpno);
					form.getSubList('custpage_items').setLineItemValue('custpage_actbeginloc', i + 1, vlocation);
					form.getSubList('custpage_items').setLineItemValue('custpage_ebizdono', i + 1, searchresult.custrecord_Ebiz_cntrl_no);
					form.getSubList('custpage_items').setLineItemValue('custpage_dono', i + 1, searchresult.Name);
					form.getSubList('custpage_items').setLineItemValue('custpage_pickid', i + 1, searchresult.gId);		
					nlapiLogExecution('ERROR', "gId " , searchresult.gId);
					form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, vskustatus);
					form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, vpackcode);
					form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1, vSOno);
					var vRoundWeight=0;
					if(searchresult.custrecord_Total_weight != null && searchresult.custrecord_Total_weight != '')
					{
						vRoundWeight= Math.round(searchresult.custrecord_Total_weight*100)/100;
					}	

					var vRoundTotCube=0;
					if(searchresult.custrecord_Totalcube != null && searchresult.custrecord_Totalcube != '')
					{
						vRoundTotCube=Math.round(searchresult.custrecord_Totalcube*Math.pow(10,2))/Math.pow(10,2);
					}	

					form.getSubList('custpage_items').setLineItemValue('custpage_weight', i + 1, vRoundWeight);
					form.getSubList('custpage_items').setLineItemValue('custpage_volume', i + 1, vRoundTotCube);
					form.getSubList('custpage_items').setLineItemValue('custpage_uomid', i + 1, vuomid);
					form.getSubList('custpage_items').setLineItemValue('custpage_skuno', i + 1, vSKUNo);
					form.getSubList('custpage_items').setLineItemValue('custpage_skustatus', i + 1, vskustatusValue);
					form.getSubList('custpage_items').setLineItemValue('custpage_lot', i + 1, vbatchNo);

					form.getSubList('custpage_items').setLineItemValue('custpage_cartonid', i + 1, vcontLP);
					form.getSubList('custpage_items').setLineItemValue('custpage_cartwt', i + 1, vTotWt);
					form.getSubList('custpage_items').setLineItemValue('custpage_cartvol', i + 1, vTotCube);

					form.getSubList('custpage_items').setLineItemValue('custpage_lpgid', i + 1, searchresult.gidLp);
					form.getSubList('custpage_items').setLineItemValue('custpage_wmslocation', i + 1, vSiteId);
					form.getSubList('custpage_items').setLineItemValue('custpage_orderno', i + 1, vOrderNo);
					form.getSubList('custpage_items').setLineItemValue('custpage_skuval', i + 1, vSKUVal);
					//form.getSubList('custpage_items').setLineItemValue('custpage_img', i + 1, "<img id='is1' src='BS2.png'></img>");
					//form.getSubList('custpage_items').setLineItemValue('custpage_img', i + 1, "<img id='is1' src='https://system.netsuite.com/c.TSTDRV840430/suitebundle14109/BS2.png'></img>");
					//nlapiLogExecution('ERROR', "gidLp " , searchresult.gidLp);
					//nlapiLogExecution('ERROR', "vSiteId " , vSiteId);
					//form.getSubList('custpage_items').setLineItemValue('custpage_contlp', i + 1, vskustatusValue);
					//form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i + 1, vLP);


				}
				nlapiLogExecution('ERROR', "VQbwave new" , VQbwave);
				WaveField.setDefaultValue(VQbwave);
				form.addSubmitButton('Build Ship Units');

				response.writePage(form);
			}
			else
			{
				showInlineMessage(form, 'Error', dataNotFoundMsg, null);
				response.writePage(form);
			}
		}
	}
	else //this is the POST block
	{
		//To process selected items
		var form = nlapiCreateForm('Build Ship Units');	
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		processSelectedOrders(request,response,form,msg);
		//var matchFound = processSelectedOrders(request,response,form);
		/*if(!matchFound){

			showInlineMessage(form, 'Error', lpErrorMessage, null);
			response.writePage(form);
		}*/
	}
}


function blnBuildShipUnitCompleted(newShipLP)
{
	nlapiLogExecution('ERROR', "blnBuildShipUnitCompleted start", newShipLP);
	var vBlnShipCompleted=true;
	var Filers = new Array;
	//The below code is commnted by Satish.N on 06/18/2012 
//	Filers.push(new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', newShipLP)); 

//	var Columns = new Array();
//	Columns[0] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
//	Columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_trailer_no');
//	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, Filers, Columns);

	Filers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', newShipLP)); 

	var Columns = new Array();
	Columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	Columns[1] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filers, Columns);

	var Salesord,closedtasktrailername;
	//if(SOSearchResults != "" || SOSearchResults != null){
	if(SOSearchResults != "" && SOSearchResults != null){
		Salesord=SOSearchResults[0].getValue('custrecord_ebiz_trailer_no');
		closedtasktrailername=SOSearchResults[0].getValue('custrecord_ebiz_trailer_no');
		nlapiLogExecution('ERROR', "Salesordp", Salesord);			 
	}
	if(blnShipComplete(Salesord)){
		nlapiLogExecution('ERROR', "Salesordpshipcompletetrue", '');	
		var Filers = new Array;
		Filers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', Salesord)); 
		Filers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); 
		Filers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', '28')); //pack complete

		var Columns = new Array();
		Columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');

		var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filers, Columns);
		if(ContainerSearchResults == "" || ContainerSearchResults == null){
			nlapiLogExecution('ERROR', "ContainerSearchResults is empty", '');	
			vBlnShipCompleted = true;

		}
		else
		{
			for (var k = 0; k < ContainerSearchResults.length; k++) 
			{
				nlapiLogExecution('ERROR', "ContainerSearchResults",ContainerSearchResults.length );		
				var containerlp = ContainerSearchResults[k].getValue('custrecord_container_lp_no');

			}
			vBlnShipCompleted = false;
		}



	}
	nlapiLogExecution('ERROR', 'vBlnShipCompleted returning value',vBlnShipCompleted);
	return vBlnShipCompleted;
}


/**
 * To create openTaskRec for multidimentional array
 *  
 */
function openTaskRec(custrecord_line_no, custrecord_ebiz_sku_no, custrecord_act_qty,custrecord_tasktype
		,custrecord_ebiz_ship_lp_no,custrecord_actendloc,custrecord_sku,name
		,custrecord_container_lp_no,custrecord_sku_status,custrecord_packcode,custrecord_uom_id
		,custrecord_ebiz_sku_no,custrecord_ebiz_cntrl_no,custrecord_ebiz_lpmaster_lp,custrecord_ebiz_lpmaster_sizeid
		,custrecord_ebiz_lpmaster_totwght,custrecord_ebiz_lpmaster_totcube,custrecord_total_weight,custrecord_totalcube,custrecord_batch_no,custrecord_site_id,gid,gidlp,custrecord_wms_location,custrecord_sku_val,custrecord_ord,custrecord_ebiz_wave_no) {
	this.custrecord_Line_no = custrecord_line_no;
	this.custrecord_Ebiz_sku_no = custrecord_ebiz_sku_no;
	this.custrecord_Act_qty = custrecord_act_qty;
	this.custrecord_Tasktype = custrecord_tasktype;
	this.custrecord_Ebiz_ship_lp_no = custrecord_ebiz_ship_lp_no;
	this.custrecord_Actendloc = custrecord_actendloc;
	this.custrecord_Sku = custrecord_sku;
	this.Name = name;
	this.custrecord_Container_lp_no = custrecord_container_lp_no;
	this.custrecord_Sku_status = custrecord_sku_status;
	this.custrecord_Packcode = custrecord_packcode;
	this.custrecord_Uom_id = custrecord_uom_id;
	this.custrecord_Ebiz_sku_no = custrecord_ebiz_sku_no;
	this.custrecord_Ebiz_cntrl_no = custrecord_ebiz_cntrl_no;
	this.custrecord_Ebiz_lpmaster_lp = custrecord_ebiz_lpmaster_lp;
	this.custrecord_Ebiz_lpmaster_sizeid = custrecord_ebiz_lpmaster_sizeid;
	this.custrecord_Ebiz_lpmaster_totwght = custrecord_ebiz_lpmaster_totwght;
	this.custrecord_Ebiz_lpmaster_totcube = custrecord_ebiz_lpmaster_totcube;
	this.custrecord_Total_weight = custrecord_total_weight;
	this.custrecord_Totalcube = custrecord_totalcube;
	this.custrecord_batch_no = custrecord_batch_no;
	this.custrecord_site_id = custrecord_site_id;
	this.gId = gid;
	this.gidLp = gidlp;
	this.wmslocation=custrecord_wms_location;
	this.ordNo=custrecord_ord;
	this.SkuVal=custrecord_sku_val;
	this.custrecord_ebiz_wave_no=custrecord_ebiz_wave_no;


}

/*
 * To check Ship Complete or not
 * @param vSOInternalId
 * returns true if Ship complete and all qty picked
 * returns true if ship complete flag is false
 * returns false if ship not complete 
 */
function blnShipComplete(vSOInternalId)
{
	var vBlnShipComp=true;
	var filters = new Array();

	filters.push(new nlobjSearchFilter('name', null,
			'is', vSOInternalId)); 


	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
	columns.push(new nlobjSearchColumn('custrecord_pickqty'));
	columns.push(new nlobjSearchColumn('custrecord_shipcomplete'));


	var searchresultsordline = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if(searchresultsordline != null && searchresultsordline != "" && searchresultsordline.length>0)
	{
		nlapiLogExecution('ERROR', 'searchresultsordline.length',searchresultsordline.length);
		for(var v=0;v<searchresultsordline.length;v++)
		{
			var vlinePickQty=0;
			var vlineOrdQty=0;
			if(searchresultsordline[v].getValue('custrecord_ord_qty')!= null && searchresultsordline[v].getValue('custrecord_ord_qty') != "")
				vlineOrdQty=parseFloat(searchresultsordline[v].getValue('custrecord_ord_qty'));

			if(searchresultsordline[v].getValue('custrecord_pickqty') != null && searchresultsordline[v].getValue('custrecord_pickqty') != "")
				vlinePickQty= parseFloat(searchresultsordline[v].getValue('custrecord_pickqty'));

			var vlineShipCompFlag=searchresultsordline[v].getValue('custrecord_shipcomplete');

			nlapiLogExecution('ERROR', 'vlineOrdQty',vlineOrdQty);
			nlapiLogExecution('ERROR', 'vlinePickQty',vlinePickQty);
			nlapiLogExecution('ERROR', 'vlineShipCompFlag',vlineShipCompFlag);

			if(vlineShipCompFlag!= null && vlineShipCompFlag != "" && vlineShipCompFlag =='T' && vlineOrdQty >0)
			{
				if(vlineOrdQty!=vlinePickQty)
				{
					vBlnShipComp=false;

				}
				else
				{
					vBlnShipComp=true;

				}	

			}
			else if(vlineShipCompFlag!= null && vlineShipCompFlag != "" && vlineShipCompFlag =='F' && vlineOrdQty >0)
			{
				vBlnShipComp=false;

			}

		}
	}
	else
		vBlnShipComp=false;
	nlapiLogExecution('ERROR', 'vBlnShipComp',vBlnShipComp);
	return vBlnShipComp;
}



function GenerateSucc(vWhLoc,uompackflag)
{
	var finaltext="";
	var duns="";
	var label="",uom="0",ResultText="";
	nlapiLogExecution('ERROR', 'GenerateSucc',vWhLoc);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', 5));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', 1));
	if(vWhLoc!=null && vWhLoc!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', [vWhLoc]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_begin');
	columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_end');
	columns[3] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype');
	columns[4] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype');
	columns[5] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);
//	nlapiLogExecution('ERROR', 'Lp Range Searchresults', searchresults.length);
	if (searchresults !=null && searchresults !="") 
	{nlapiLogExecution('ERROR', 'Lp Range Searchresults', searchresults.length);
//	for (var i = 0; i < searchresults.length; i++) 
//	{
	try 
	{
		var company = searchresults[0].getText('custrecord_ebiznet_lprange_company');	
		var getLPrefix = searchresults[0].getValue('custrecord_ebiznet_lprange_lpprefix');					
		var varBeginLPRange = searchresults[0].getValue('custrecord_ebiznet_lprange_begin');
		var varEndRange = searchresults[0].getValue('custrecord_ebiznet_lprange_end');
		var getLPGenerationTypeValue = searchresults[0].getValue('custrecord_ebiznet_lprange_lpgentype');
		var getLPTypeValue = searchresults[0].getText('custrecord_ebiznet_lprange_lptype');

		var lpMaxValue=GetMaxLPNo('1', '5',vWhLoc);
		nlapiLogExecution('ERROR', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('ERROR', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;
		nlapiLogExecution('ERROR', 'label', label);

		var dunsfilters = new Array();
		dunsfilters[0] = new nlobjSearchFilter('custrecord_company', null, 'is', company);
		var dunscolumns = new Array();
		dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

		var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
		if (dunssearchresults !=null && dunssearchresults != "") 
		{
			duns = dunssearchresults[0].getValue('custrecord_compduns');
			nlapiLogExecution('ERROR', 'duns', duns);
		}

		if(uompackflag == "1" ) 
			uom="0"; 
		else if(uompackflag == "3") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord uom', uom);
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseFloat(checkStr.charAt(i));
			i--;
		}
		//alert(ARL);
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseFloat(checkStr.charAt(i));
			i--;
		}
		// alert(BRL);
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 
			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	//}
	}
	return  ResultText;
}


/**
 * @param VQbwave
 * @param VOrdNo
 * @returns {String}
 */
function IsOpenPicksPresent(VQbwave,VOrdNo,VShipLP)
{
	try
	{
		nlapiLogExecution('ERROR','Into IsOpenPicksPresent');
		nlapiLogExecution('ERROR','VQbwave',VQbwave);
		nlapiLogExecution('ERROR','VOrdNo',VOrdNo);
		nlapiLogExecution('ERROR','VShipLP',VShipLP);
		var IsOpenPicks='F';
		var SoIdArray=new Array();
		var TotalSO;
		if(VQbwave!=null&&VQbwave!="")
			TotalSO=GetAlltheOrders(VQbwave);

		nlapiLogExecution('ERROR','TotalSO',TotalSO);

		var filters=new Array();
		//case# 20127378 starts (Wave filter added due to allow one wave to complete buildship or not)
		if(VQbwave!=null && VQbwave!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',VQbwave));
		//case# 20127378 end
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));  //Pick Task
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','9'])); //8=pick confirmed ;9=pick generated; 30=Short picks
		filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan',0));
		if(TotalSO!=null && TotalSO!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', TotalSO)); //for OrdNo'
		else if(VOrdNo!=null && VOrdNo!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', VOrdNo)); //for OrdNo'

		if(VShipLP !=null && VShipLP!='')
			filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', VShipLP)); //for ContLP'

		var SOColumns = new Array();
		SOColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, SOColumns);
		if(searchresults!=null&&searchresults!="")
		{
			/*	IsOpenPicks='T';
			nlapiLogExecution('ERROR','IsOpenPicks',IsOpenPicks);*/

			for(var i=0;i<searchresults.length;i++)
			{
				SoIdArray.push(searchresults[i].getValue('custrecord_ebiz_order_no'));
			}
		}



		return SoIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in IsOpenPicksPresent',exp);
	}
}


function GetAlltheOrders(wave)
{
	try
	{
		var SoIdArray=new Array();
		nlapiLogExecution('ERROR','Into GetAlltheOrders');
		nlapiLogExecution('ERROR','VQbwave',wave);
		var filters=new Array();
		if(wave!=null && wave!="")
		{
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',wave));
			filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan',0));
		}

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_order_no');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, column);
		if(searchresults!=null&&searchresults!="")
		{
			for ( var x = 0; x < searchresults.length; x++)
			{
				var soid=searchresults[x].getValue('custrecord_ebiz_order_no');
				SoIdArray.push(soid);
			}
		}
		nlapiLogExecution('ERROR','SoIdArray',SoIdArray);
		return SoIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetAlltheOrders',exp);
	}
}
