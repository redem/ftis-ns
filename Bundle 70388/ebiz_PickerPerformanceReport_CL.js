/* 
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Common/Suitelet/GeneralFunctions.js,v $
 *     	   $Revision: 1.86 $
 *     	   $Date: 2011/11/25 10:17:48 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: GeneralFunctions.js,v $ */

function onchange(type,field,linenum)
{

	if(field=='custrecord_endtime')
		{
//	var date=DateStamp();var time =TimeStamp();
//	var datetime=date+" "+time;alert(datetime);
	//var index=nlapiGetCurrentLineItemIndex('custpage_pickerperformance');
	//var field1 = nlapiGetLineItemField('custpage_pickerperformance', 'custrecord_endtime',linenum);  
	//nlapiSetLineItemValue('custpage_pickerperformance','custrecord_endtime',linenum,datetime);
	var starttime=nlapiGetLineItemValue('custpage_pickerperformance','custrecord_starttime',linenum);
	var endtime=nlapiGetLineItemValue('custpage_pickerperformance','custrecord_endtime',linenum);
	
	if (starttime > endtime)
	{
	alert ("Finish Time is less then Start Time");
	return false;
	}
	else
	{
	return true;
	}
		}
}

function ValidatePickerPerformanceSublist()
{
	
	var lineCnt = nlapiGetLineItemCount('custpage_pickerperformance');//to get total line count of sublist	
	var checkselect=false;
	//loop through the sublist items to get the total quantity entered.and check wheter qty is empty
	for (var s = 1; s <= lineCnt; s++) {
		var chk = nlapiGetLineItemValue('custpage_pickerperformance','custrecord_select',s);
		
		if(chk=='T')
			{
			checkselect=true;
			var loadno = nlapiGetLineItemValue('custpage_pickerperformance','custrecord_loadno',s);
			var pullerid = nlapiGetLineItemValue('custpage_pickerperformance','custrecord_pullerid',s);
			var pages=nlapiGetLineItemValue('custpage_pickerperformance','custrecord_pagesno',s);
			var pallets=nlapiGetLineItemValue('custpage_pickerperformance','custrecord_palletsno',s);
			var cartons=nlapiGetLineItemValue('custpage_pickerperformance','custrecord_cartons',s);
			var starttime=nlapiGetLineItemValue('custpage_pickerperformance','custrecord_starttime',s);
			var endtime=nlapiGetLineItemValue('custpage_pickerperformance','custrecord_endtime',s);
			if(pullerid=='')
			{
				alert('Please enter pullerid for Load #:'+loadno);				
				return false;
			}
			
			else if(isNaN(pages) == true)
				{
				alert('Please enter #Pages as number for Load #:'+loadno);					
				return false;
				}
			
			else if(isNaN(pallets) == true)
				{
				alert('Please enter #Pallets as number for Load #:'+loadno);			
				return false;
				}
			
			else if(isNaN(cartons) == true)
				{
				alert('Please enter #cartons as number for Load #:'+loadno);				
				return false;
				}
			else if(starttime!=''&&starttime!=null&&endtime!=''&&endtime!=null)
				{
				if (starttime > endtime)
				{
				alert ("Finish Time is less then Start Time for Load #:"+loadno);
				return false;
				}
				}
			else
				{
				checkselect=false;
				}
			
			}	
		

	}	
	if(checkselect==false )
		{
		return true;
		}
	
}