/***************************************************************************
����������������������������eBizNET Solutions
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_ReplenishmentReport_pdf.js,v $
*� $Revision: 1.1.2.1.4.1.4.4.2.2 $
*� $Date: 2015/11/12 13:15:50 $
*� $Author: schepuri $
*� $Name: b_WMS_2015_2_StdBundle_Issues $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_ReplenishmentReport_pdf.js,v $
*� Revision 1.1.2.1.4.1.4.4.2.2  2015/11/12 13:15:50  schepuri
*� case# 201415503
*�
*� Revision 1.1.2.1.4.1.4.4.2.1  2015/09/24 12:59:08  schepuri
*� case# 201414581
*�
*� Revision 1.1.2.1.4.1.4.4  2015/08/24 09:29:45  rrpulicherla
*� Case#201413343
*�
*� Revision 1.1.2.1.4.1.4.3  2014/11/13 06:07:21  vmandala
*� case#  201411009 Stdbundle issue fixed
*�
*� Revision 1.1.2.1.4.1.4.2  2014/02/11 15:51:27  grao
*� Case# 20127136 related issue fixes in MHP SB issue fixes
*�
*� Revision 1.1.2.1.4.1.4.1  2013/03/19 12:08:11  schepuri
*� CASE201112/CR201113/LOG201121
*� change url path
*�
*� Revision 1.1.2.1.4.1  2012/11/01 14:55:15  schepuri
*� CASE201112/CR201113/LOG201121
*� Decimal Qty Conversions
*�
*� Revision 1.1.2.1  2012/08/06 07:02:27  spendyala
*� CASE201112/CR201113/LOG201121
*� New script for print replenishment pdf
*�
*
****************************************************************************/

/**
 * This is the main function to print the BOM LIST against the given WO number  
 */
function ReplenishmentReportPDFSuitelet(request, response)
{
	if (request.getMethod() == 'GET') 
	{

		var form = nlapiCreateForm('Replenishment Report'); 
		var repid=request.getParameter('custparam_repid');

		nlapiLogExecution('ERROR','repid',repid);

		if(repid!=null && repid!='')
		{
			nlapiLogExecution('ERROR','INTOrepid1',repid);

			var wonumber;

			var filters = new Array();
			filters[0] = new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repid'));
			filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
			filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9,20]);

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_line_no');
			columns[1] = new nlobjSearchColumn('custrecord_lpno');
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[3] = new nlobjSearchColumn('custrecord_sku_status');
			columns[4] = new nlobjSearchColumn('custrecord_batch_no');
			columns[5] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[6] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[7] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');    	
			columns[8] = new nlobjSearchColumn('custrecord_sku'); //Retreive SKU Details
			columns[9] = new nlobjSearchColumn('custrecord_skudesc'); //Retreive SKU desc Details
			columns[10] = new nlobjSearchColumn('custrecord_packcode');
			columns[11] = new nlobjSearchColumn('custrecord_lotnowithquantity');
			columns[12] = new nlobjSearchColumn('custrecord_comp_id');
			columns[13] = new nlobjSearchColumn('custrecord_actendloc');
			columns[14] = new nlobjSearchColumn('custrecord_tasktype');
			columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');

			columns[7].setSort(); // case# 201413592
	        columns[8].setSort();

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);


			var url;
			/*var ctx = nlapiGetContext();
			nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
			if (ctx.getEnvironment() == 'PRODUCTION') 
			{
				url = 'https://system.netsuite.com';			
			}
			else if (ctx.getEnvironment() == 'SANDBOX') 
			{
				url = 'https://system.sandbox.netsuite.com';				
			}*/
			nlapiLogExecution('ERROR', 'PDF URL',url);	
			var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
			if (filefound) 
			{ 
				nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
				var imageurl = filefound.getURL();
				nlapiLogExecution('ERROR','imageurl',imageurl);
				//var finalimageurl = url + imageurl;//+';';
				var finalimageurl = imageurl;//+';';
				//finalimageurl=finalimageurl+ '&expurl=T;';
				nlapiLogExecution('ERROR','imageurl',finalimageurl);
				finalimageurl=finalimageurl.replace(/&/g,"&amp;");

			} 
			else 
			{
				nlapiLogExecution('ERROR', 'Event', 'No file;');
			}

			//date
			var sysdate=DateStamp();
			var systime=TimeStamp();
			var Timez=calcTime('-5.00');
			nlapiLogExecution('ERROR','TimeZone',Timez);
			var datetime= new Date();
			datetime=datetime.toLocaleTimeString() ;

			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
			//var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
//			nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">\n");

			//var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g; 
		
				
			var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;	
			var strxml= "";


			if(searchresults!=null && searchresults!="")
			{
				var rowcount=0;//added by santosh
				var totalcount=searchresults.length;
				while(0<totalcount)//added by santosh
				{ 
					var count=0;
					nlapiLogExecution('ERROR','totalcount',totalcount);
					nlapiLogExecution('ERROR','count',count);

				strxml += "<table width='100%'>";
				strxml += "<tr><td><table width='100%' >";
				strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
				strxml += "Replenishment Report ";
				strxml += "</td><td align='right'>&nbsp;</td></tr></table></td></tr>";
				strxml += "<tr><td><table width='100%' >";
				strxml += "<tr><td style='width:41px;font-size: 12px;'>";
				strxml += "<p align='right'>Date/Time:"+Timez+"</p>"; 
				strxml += "</td></tr>";
				strxml += "</table>";

				strxml += "</td></tr>";
				strxml += "<tr><td><table width='100%' >";
				strxml += "<tr><td>";
				strxml +="<table align='left'>";

				strxml +="<tr><td align='left' style='width:20px; font-size: 16px;'>Replenishment# :</td>"; 
				strxml +="<td align='right' style='width:41px; font-size: 14px;'>";
				strxml += repid;
				strxml +="</td>";
				strxml +="</tr>";

				strxml +="</table>";
				strxml += "</td></tr>";
				strxml += "</table>";

				strxml += "</td></tr>";

				strxml += "<tr><td><table width='1000px' >";
				strxml += "<tr><td>";

					//strxml +="<table  width='980px'>";
					strxml +="<table>";
					strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Report#";
				strxml += "</td>";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "From LP#";
				strxml += "</td>";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "TO LP#";
				strxml += "</td>";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Item";
				strxml += "</td>";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "LOT#";
				strxml += "</td>";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Quantity";
				strxml += "</td>";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Act Begin Location";
				strxml += "</td>";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Act End Location";
				strxml += "</td>";

				strxml += "<td width='16%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px; '>";
				strxml += "Task Type";
				strxml += "</td>";

				/*strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Serial #";
				strxml += "</td>";*/

				strxml =strxml+  "</tr>";

				for(var m=rowcount; m< searchresults.length; m++)
				{	
					count++;
					rowcount++;
					searchresult=searchresults[m];
					var line = request.getParameter('custparam_repid');
					if(line== null || line =="")
						line="";

					var  lp = searchresult.getValue('custrecord_lpno');
					if(lp== null || lp =="")	
						lp="&nbsp;";

					var  itemname = searchresult.getText('custrecord_sku');
					if(itemname== null || itemname =="")
						itemname="&nbsp;";

					var lotbatch = searchresult.getValue('custrecord_batch_no');
					if(lotbatch== null || lotbatch =="")
						lotbatch="&nbsp;";

					var qty = searchresult.getValue('custrecord_expe_qty');
					if(qty== null || qty =="")
						qty="";

					var  loc = searchresult.getText('custrecord_actbeginloc');
					if(loc== null || loc =="")	
						loc="&nbsp;";

					var endloc =  searchresult.getText('custrecord_actendloc');
					if(endloc== null || endloc =="")
						endloc="&nbsp;";

					var tasktype =  searchresult.getText('custrecord_tasktype');
					if(tasktype== null || tasktype =="")
						tasktype="&nbsp;";

					var FromLp =  searchresult.getValue('custrecord_from_lp_no');
					if(FromLp== null || FromLp =="")
						FromLp="&nbsp;";

						var itemnamenew = '';
					strxml =strxml+  "<tr>";
					strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					strxml += line;
					strxml += "</td>";

					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					strxml += FromLp;
					strxml += "</td>";

					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					strxml += lp;
					strxml += "</td>";

					strxml += "<td width='6%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					//strxml += itemname;
					if(itemname != null && itemname!= "")
					{
							var itemnameArray = itemname.split(':');// case# 201414581  if have matrix items then we have : in itemname then getting a script error
							
							if(itemnameArray.length > 1)
								itemnamenew = itemnameArray[1].replace(replaceChar,'');
							else
								itemnamenew = itemnameArray[0].replace(replaceChar,'');
						strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
							strxml += itemnamenew;
						strxml += "\"/>";
					}

					strxml += "</td>";


					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					strxml += lotbatch;
					strxml += "</td>";

					strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					strxml += qty;
					strxml += "</td>";

					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					strxml += loc;
					strxml += "</td>";

					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					strxml += endloc;
					strxml += "</td>";

					strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					strxml += tasktype;
					strxml += "</td>";

					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
					strxml += '';
					strxml += "</td></tr>";
               //case 201411009
					if(count==8)//added by santosh
					{
						break;
					}
				}
				strxml =strxml+"</table>";
          
				strxml += "</td></tr>";
				strxml += "</table>";

				strxml += "</td></tr>";

				strxml += "</table>"; 

				totalcount=parseFloat(totalcount)-parseFloat(count);
				if(rowcount==searchresult.length)
				{
					strxml +="<p style=\" page-break-after:avoid\"></p>";
				}
				else
				{
					strxml +="<p style=\" page-break-after:always\"></p>";
				}
				}
				strxml =strxml+ "\n</body>\n</pdf>";		
				xml=xml +strxml;
				//nlapiLogExecution('ERROR','xml',xml);
				var file = nlapiXMLToPDF(xml);	
				response.setContentType('PDF','ReplenishmentReport.pdf');
				response.write( file.getValue() );


			}
		}
	}
	else 
	{

	}
}




function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}

function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}


function TimeStamp(){
	var timestamp;
	var now = new Date();
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = now.getHours();
	var curr_min = now.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return timestamp;
}
