/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_Shipping_CartonLP.js,v $
 *     	   $Revision: 1.4.4.4.4.6.2.15.2.1 $
 *     	   $Date: 2015/11/14 09:42:58 $
 *     	   $Author: sponnaganti $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Shipping_CartonLP.js,v $
 * Revision 1.4.4.4.4.6.2.15.2.1  2015/11/14 09:42:58  sponnaganti
 * case# 201415643
 * 2015.2 issue fix
 *
 * Revision 1.4.4.4.4.6.2.15  2015/05/11 21:05:48  rrpulicherla
 * Case#201412277
 *
 * Revision 1.4.4.4.4.6.2.14  2015/05/08 15:58:00  rrpulicherla
 * Case#201412277
 *
 * Revision 1.4.4.4.4.6.2.13  2014/07/23 15:43:18  skavuri
 * Case # 20149567 Compatibility Issue Fixed
 *
 * Revision 1.4.4.4.4.6.2.12  2014/07/11 07:24:59  gkalla
 * case#20149124
 * Sonic In RF Build ship we are not updating FO# in ship task of open
 * task
 *
 * Revision 1.4.4.4.4.6.2.11  2014/06/23 07:56:03  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.4.4.4.4.6.2.10  2014/06/06 08:06:35  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.4.4.4.4.6.2.9  2014/05/30 00:41:06  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.4.4.6.2.8  2014/05/09 14:33:35  rmukkera
 * Case# 20148335
 *
 * Revision 1.4.4.4.4.6.2.7  2014/01/31 15:50:58  rmukkera
 * Case # 20127020
 *
 * Revision 1.4.4.4.4.6.2.6  2013/08/21 02:38:28  snimmakayala
 * Case# 20123983
 * GSUSA - PICKGEN ISSUE
 *
 * Revision 1.4.4.4.4.6.2.5  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.4.4.4.6.2.4  2013/05/08 15:08:23  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.4.4.4.4.6.2.3  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.4.4.4.6.2.2  2013/04/16 14:55:56  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.4.4.4.4.6.2.1  2013/04/03 00:55:22  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.4.4.4.4.6  2013/02/15 14:59:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.4.4.4.4.5  2013/01/24 07:17:46  mbpragada
 * 20120490
 *
 * Revision 1.4.4.4.4.4  2013/01/21 15:51:53  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA-BuildShip Unit Chnages.
 *
 * Revision 1.4.4.4.4.3  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.4.4.4.4.2  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4.4.4.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.4.4.4  2012/06/21 10:15:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.4.4.3  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.4.2  2012/02/22 13:00:54  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4.4.1  2012/02/06 05:56:54  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing related to fetch details from opentask,added tasktype filter
 *
 * Revision 1.4  2011/10/03 08:25:05  snimmakayala
 * CASE201112/CR201113/LOG20112
 *
 * Revision 1.1  2011/09/23 16:22:25  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/21 07:42:42  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.1  2011/04/19 14:19:06  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 *
 *****************************************************************************/
function EnterCartonLP(request, response)
{
	if (request.getMethod() == 'GET') 
	{    
		var getShipLP,getItemCount,getTotWgt,getTotCube,getOrderNo,getSiteId,getCompId;
		getShipLP = request.getParameter('custparam_shiplpno');
		nlapiLogExecution('DEBUG', 'getShipLP ', getShipLP);

		if(request.getParameter('custparam_GWgt')!= null && request.getParameter('custparam_GWgt') != '')
			getTotWgt = request.getParameter('custparam_GWgt');

		if(request.getParameter('custparam_GCube')!= null && request.getParameter('custparam_GCube') != '')
			getTotCube = request.getParameter('custparam_GCube'); 

		if(request.getParameter('custparam_itemcount')!= null && request.getParameter('custparam_itemcount') != '')
			getItemCount = request.getParameter('custparam_itemcount');
		if(request.getParameter('custparam_OrderNo')!= null && request.getParameter('custparam_OrderNo') != '')
			getOrderNo = request.getParameter('custparam_OrderNo');

		if(request.getParameter('custparam_site_id')!= null && request.getParameter('custparam_site_id') != '')
			getSiteId = request.getParameter('custparam_site_id');

		if(request.getParameter('custparam_comp_id')!= null && request.getParameter('custparam_comp_id') != '')
			getCompId = request.getParameter('custparam_comp_id');
		var getEbizWave;
		if(request.getParameter('custparam_waveno')!= null && request.getParameter('custparam_waveno') != '')
			getEbizWave = request.getParameter('custparam_waveno');

		var vShipCity;
		if(request.getParameter('custparam_shipcity')!= null && request.getParameter('custparam_shipcity') != '')
			vShipCity = request.getParameter('custparam_shipcity');

		var getFOId = request.getParameter('custparam_foname');
		nlapiLogExecution('DEBUG', 'getFOId', getFOId);
		 
		var getLanguage = request.getParameter('custparam_language');	
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		
		var whLocation = request.getParameter('custparam_locationId');

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "ENV&#205;O LP #";
			st2 = "ENTER / SCAN CARTON #:";
			st3 = "ADJUNTE";
			st4 = "COMPLETE";
		}
		else
		{
			st0 = "";
			st1 = "SHIP LP#";
			st2 = "ENTER/SCAN CARTON#:";
			st3 = "ATTACH";
			st4 = "COMPLETE";
		}    	



		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		/*html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends    
		html = html + "document.getElementById('entercartonno').focus();";
		html = html + " if (document.getElementById('entercartonno').value.length >=1) ";
		html = html + " document.getElementById('cmdSend').disabled = false;";
		html = html + " else";
		html = html + " document.getElementById('cmdSend').disabled = true; }";	




		html = html + "</script>";*/

		html = html + "<SCRIPT LANGUAGE='javascript'>";
		html = html + "function validate(frm){";
		html = html + " if (document.getElementById('entercartonno').value.length >=1) ";
		html = html + " document.getElementById('cmdSend').disabled = false;";
		html = html + " else";
		html = html + " document.getElementById('cmdSend').disabled = true; }";			

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";

		html = html +functionkeyHtml;
		html = html + "</head><body onload='return validate(this)'; onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getShipLP + "</label>"; 
		html = html + "				<input type='hidden' name='hdnShipLP' value=" + getShipLP + ">";
		html = html + "				<input type='hidden' name='hdnitemcount' value=" + getItemCount + ">";
		html = html + "				<input type='hidden' name='hdntotwgt' value=" + getTotWgt + ">";
		html = html + "				<input type='hidden' name='hdntotcube' value=" + getTotCube + ">";
		html = html + "				<input type='hidden' name='hdnOrd' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnwave' value=" + getEbizWave + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnwhLocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnfoid' value=" + getFOId + ">";
		
		html = html + "				</td>";
		html = html + "			</tr>";  
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;		
		html = html + "				</td>";
		html = html + "			</tr>";        
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercartonno' id='entercartonno' type='text' onkeyup='return validate(this)';/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdExit.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		//if(getItemCount>0)
		html = html + "					" + st4 + " <input name='cmdExit' type='submit' value='F10'/> PREV <input name='cmdPrevious' type='submit' value='F7'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercartonno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		var getShipLP;
		getShipLP = request.getParameter('hdnShipLP');
		nlapiLogExecution('DEBUG', 'getShipLP ', getShipLP);
		var getCartonLP = request.getParameter('entercartonno');
		var getItemCount = request.getParameter('custparam_itemcount');
		// This variable is to hold the SO# entered.

		var getTotWgt = request.getParameter('custparam_GWgt');
		var getTotCube = request.getParameter('custparam_GCube');
		var getOrdNo = request.getParameter('hdnOrd');
		var getSiteId = request.getParameter('custparam_site_id');
		var getCompId = request.getParameter('custparam_comp_id');
		var getWave = request.getParameter('hdnwave');
		var getShipCity = request.getParameter('hdnshipcity');
		var getFOId = request.getParameter('hdnfoid');
		
		nlapiLogExecution('DEBUG', 'SOarray["custparam_waveno"] ', getWave);
		var SOarray = new Array();
		SOarray["custparam_foname"]=getFOId;
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]);    	
		var whLocation = request.getParameter('hdnwhLocation');
		//nlapiLogExecution('DEBUG', 'Entered SHIP LP', varEnteredLP);
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);
		var st9,st10;
		if( getLanguage == 'es_ES')
		{			
			st9 = "CAJA NO V&#193;LIDO #";
			st10 = "CAJA # pertenece a otro orden #";
		}
		else
		{			
			st9 = "INVALID CARTON#";
			st10 ="CARTON# belongs to another Order#";
		}

		SOarray["custparam_waveno"]=getWave;
		SOarray["custparam_error"] = st9;
		SOarray["custparam_screenno"] = '39';
		SOarray["custparam_shiplpno"] = getShipLP;
		SOarray["custparam_shipcity"] = getShipCity;
		SOarray["custparam_locationId"]=whLocation;
		//Case# 20149567 starts
		SOarray["custparam_orderintid"]=getOrdNo;
		//Case# 20149567 ends
		SOarray["custparam_site_id"]=getSiteId;
		nlapiLogExecution('DEBUG', 'Order #', getCartonLP);

		// This variable is to get the value when the EXIT 'F10' button is clicked, in order to navigate
		// to complete the scanning process of Cartons.
		var optedEvent;
		if(request.getParameter('cmdExit')!=null)
			optedEvent = request.getParameter('cmdExit');

		//	if the EXIT  button 'F10' is clicked, it has to go to complete the contaner attachment
		//  ie., it has to go to accept Size id and tare weights.
		/*if (optedEvent == 'F10') 
		{    
			SOarray["custparam_shiplpno"] = getShipLP;
			response.sendRedirect('SUITELET', 'customscript_rf_shipping_sizeid', 'customdeploy_rf_shipping_sizeid_di', false, SOarray);			 
		}
		else */
		{
			if (request.getParameter('cmdPrevious') == 'F7') {
				nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
				response.sendRedirect('SUITELET', 'customscript_rf_shipping_shiplp', 'customdeploy_rf_shipping_shiplp_di', false, SOarray);
			}
			else if ((getCartonLP != null && getCartonLP != "") || optedEvent == 'F10') 
			{
				if (getCartonLP != null && getCartonLP != "")
				{
					nlapiLogExecution('DEBUG', 'Into If', 'hi1');
					SOarray["custparam_shiplpno"] = getShipLP;
					var vShiptoCityRule='T';
					nlapiLogExecution('DEBUG', 'GetOrdDetails getCartonLP',getCartonLP); 
					var searchrecords = GetOrdDetails(getCartonLP);

					if(searchrecords != null && searchrecords != "")
					{
						var voldsoid=-1;
						var ShipCity;
						for (var b = 0; searchrecords != null && b < searchrecords.length; b++) 
						{
							var searchresult = searchrecords[b];
							if(getOrdNo== null || getOrdNo == "" || getOrdNo=='undefined' )
							{
								SOarray["custparam_OrderNo"]=searchresult.custrecord_Ebiz_order_no;
								getOrdNo=searchresult.custrecord_Ebiz_order_no;
							}
							nlapiLogExecution('DEBUG', 'Old Order', getOrdNo);
							nlapiLogExecution('DEBUG', 'New Order', searchresult.custrecord_Ebiz_order_no);
							nlapiLogExecution('DEBUG', 'Fulfillment Order no', searchresult.Name);
							

							if(getOrdNo!= null && getOrdNo != "" && getOrdNo != 'undefined')
							{	
								nlapiLogExecution('DEBUG', 'NS Ref #', searchresult.custrecord_ebiz_nsconfirm_ref_no);

								if(searchresult.custrecord_ebiz_nsconfirm_ref_no == null || searchresult.custrecord_ebiz_nsconfirm_ref_no == "")
								{
									SOarray["custparam_error"] = 'This Order is not yet picked in Netsuite';
									nlapiLogExecution('DEBUG', 'into Error', 'into error');
									nlapiLogExecution('DEBUG', 'Error: ', 'This Order is not yet picked in Netsuite');
									response.write("<script language=javascript>alert('This Order is not yet picked in Netsuite');return false;</script>");
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);									
									return false;

								}

								nlapiLogExecution('DEBUG', 'searchresult[custrecord_ebiz_order_no]', searchresult.custrecord_Ebiz_order_no);

								if(searchresult.custrecord_Ebiz_order_no != null && searchresult.custrecord_Ebiz_order_no != "")
								{
									if(getOrdNo!=searchresult.custrecord_Ebiz_order_no)
									{
										SOarray["custparam_error"] = 'CARTON# belongs to another Order#';
										nlapiLogExecution('DEBUG', 'into Error', 'into error');
										response.write("<script language=javascript>alert('CARTON# belongs to another Order#');return false;</script>");
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
										nlapiLogExecution('DEBUG', 'Error: ', 'CARTON# belongs to another Order#');
										return false;
									}
								}
							}

							var vsalesOrderId = searchresult.custrecord_Ebiz_order_no;

							nlapiLogExecution('DEBUG', 'vsalesOrderId', vsalesOrderId);
							nlapiLogExecution('DEBUG', 'voldsoid', voldsoid);
							SOarray["custparam_orderintid"]=vsalesOrderId;

							if((vsalesOrderId!=null && vsalesOrderId!='') && (voldsoid!=vsalesOrderId))
							{
								var sostatus='';
								var paymentstatus='';

								var sostatusdet = new Array();

								sostatusdet = isOrderClosed(vsalesOrderId);
								if(sostatusdet!=null && sostatusdet!='' && sostatusdet.length>0)
								{
									sostatus=sostatusdet[0].getValue('status');
									paymentstatus=sostatusdet[0].getValue('paymenteventresult');
									ShipCity = sostatusdet[0].getValue('shipcity');
								}

								var str = 'sostatus. = ' + sostatus + '<br>';
								str += 'paymentstatus. = ' + paymentstatus + '<br>';
								str += 'ShipCity. = ' + ShipCity + '<br>';

								nlapiLogExecution('DEBUG', 'SO Status Log', str);

								if(sostatus == 'closed')
								{
									SOarray["custparam_error"] = "Order is already closed in Netsuite.So shipping is not allowed in eBizNET";
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
									return;
								}

								if(paymentstatus == 'HOLD')
								{
									SOarray["custparam_error"] = "A payment hold has been placed on this Sales Order.So shipping is not allowed in eBizNET";
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
									return;
								}

								voldsoid = vsalesOrderId;
							}

//							if(searchresult.custrecord_Ebiz_order_no != null && searchresult.custrecord_Ebiz_order_no != "")
//							ShipCity=fnGetShipcity(searchresult.custrecord_Ebiz_order_no);
							nlapiLogExecution('DEBUG', 'New ShipCity', ShipCity);
							nlapiLogExecution('DEBUG', 'Old getShipCity', getShipCity);
							if(vShiptoCityRule =='T' && getShipCity!=ShipCity && getShipCity != null && getShipCity != '' &&   getShipCity != 'undefined')
							{
								SOarray["custparam_shipcity"]=ShipCity;
								SOarray["custparam_error"] = 'CARTON# belongs to another shipping city';
								nlapiLogExecution('DEBUG', 'into Error', 'into error');
								response.write("<script language=javascript>alert('CARTON# belongs to another shipping city');return false;</script>");
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('DEBUG', 'Error: ', 'CARTON# belongs to another shipping city');
								return false;
							}
							SOarray["custparam_shipcity"]=ShipCity;

							if(getOrdNo== null || getOrdNo == "" )
								SOarray["custparam_OrderNo"]=searchresult.custrecord_Ebiz_order_no;
							nlapiLogExecution('DEBUG', 'searchresult.custrecord_Ebiz_order_no', searchresult.custrecord_Ebiz_order_no);
							if(getCompId!= null || getCompId != "" )
								SOarray["custparam_comp_id"]=searchresult.custrecord_Comp_id;
							nlapiLogExecution('DEBUG', 'searchresult.custrecord_Comp_id', searchresult.custrecord_Comp_id);
						//	if(getSiteId!= null || getSiteId != "" )
							if(getSiteId== null || getSiteId == "" )
								SOarray["custparam_site_id"]=searchresult.custrecord_Site_id;
//							nlapiLogExecution('DEBUG', 'searchresult.custrecord_Site_id', searchresult.custrecord_Site_id);
//							nlapiLogExecution('DEBUG', 'searchresult.custrecord_Ebiz_lpmaster_totwght', searchresult.custrecord_Ebiz_lpmaster_totwght);
//							nlapiLogExecution('DEBUG', 'searchresult.custrecord_Ebiz_lpmaster_totcube', searchresult.custrecord_Ebiz_lpmaster_totcube);

							SOarray["custparam_waveno"]=searchresult.custrecord_Ebizwaveno;
							SOarray["custparam_foname"]=searchresult.Name;
//							nlapiLogExecution('DEBUG', 'SOarray["custparam_waveno"] ', searchresult.custrecord_Ebizwaveno);
//							nlapiLogExecution('DEBUG', 'SOarray["custparam_GWgt"] before adding', getTotWgt);
//							nlapiLogExecution('DEBUG', 'SOarray["custparam_GCube"] before adding', getTotCube);
//							nlapiLogExecution('DEBUG', 'searchresult.custrecord_Ebiz_lpmaster_totwght', searchresult.custrecord_Ebiz_lpmaster_totwght);
//							nlapiLogExecution('DEBUG', 'searchresult.custrecord_Ebiz_lpmaster_totcube', searchresult.custrecord_Ebiz_lpmaster_totcube);
							if(getTotWgt!= null && getTotWgt != "")
							{
								if(searchresult.custrecord_Ebiz_lpmaster_totwght != null && searchresult.custrecord_Ebiz_lpmaster_totwght != "")
								{
									SOarray["custparam_GWgt"]=parseFloat(getTotWgt) + parseFloat(searchresult.custrecord_Ebiz_lpmaster_totwght);
								}
							}
							else
							{
								if(searchresult.custrecord_Ebiz_lpmaster_totwght != null && searchresult.custrecord_Ebiz_lpmaster_totwght != "")
								{
									SOarray["custparam_GWgt"]=parseFloat(searchresult.custrecord_Ebiz_lpmaster_totwght);
								}
								else
								{
									SOarray["custparam_GWgt"]=0;
								}
							}
							if(getTotCube!= null && getTotCube != "")
							{
								if(searchresult.custrecord_Ebiz_lpmaster_totcube != null && searchresult.custrecord_Ebiz_lpmaster_totcube != "")
								{
									SOarray["custparam_GCube"]=parseFloat(SOarray["custparam_GCube"]) + parseFloat(searchresult.custrecord_Ebiz_lpmaster_totcube);
								}
							}
							else
							{
								if(searchresult.custrecord_Ebiz_lpmaster_totcube != null && searchresult.custrecord_Ebiz_lpmaster_totcube != "")
								{
									SOarray["custparam_GCube"]=parseFloat(searchresult.custrecord_Ebiz_lpmaster_totcube);
								} 
								else
								{
									SOarray["custparam_GCube"]=0;
								}
							}
//							nlapiLogExecution('DEBUG', 'searchresult.custrecord_Opentask_id1', searchresult.custrecord_Opentask_id);
//							nlapiLogExecution('DEBUG', 'searchresult.custrecord_Lp_id1', searchresult.custrecord_Lp_id);


							processEnteredOrder(getShipLP,getCartonLP,searchresult.custrecord_Opentask_id,searchresult.custrecord_Lp_id,
									searchresult.custrecord_Ebiz_order_no);
							if(getItemCount!=null && getItemCount != '' && getItemCount != 'null' && getItemCount != 'undefined')
								SOarray["custparam_itemcount"]=parseInt(getItemCount)+1;
							else
								SOarray["custparam_itemcount"]=1;

//							nlapiLogExecution('DEBUG', 'SOarray["custparam_GWgt"]', SOarray["custparam_GWgt"]);
//							nlapiLogExecution('DEBUG', 'SOarray["custparam_GCube"]', SOarray["custparam_GCube"]);
							/*if(SOarray["custparam_GWgt"]!= null && SOarray["custparam_GWgt"] != "")
					{
						if(searchresult.getValue('custrecord_ebiz_lpmaster_totwght') != null && searchresult.getValue('custrecord_ebiz_lpmaster_totwght') != "")
						{
							SOarray["custparam_GWgt"]=parseFloat(SOarray["custparam_GWgt"]) + parseFloat(searchresult.getValue('custrecord_ebiz_lpmaster_totwght'));
						}
					}*/
							
						}
						if (optedEvent == 'F10') 
						{    
							SOarray["custparam_shiplpno"] = getShipLP;
							nlapiLogExecution('DEBUG', 'SOarray["custparam_GWgt"] before size id', SOarray["custparam_GWgt"]);
							nlapiLogExecution('DEBUG', 'SOarray["custparam_GCube"] before size id', SOarray["custparam_GCube"]);
							response.sendRedirect('SUITELET', 'customscript_rf_shipping_sizeid', 'customdeploy_rf_shipping_sizeid_di', false, SOarray);			 
						}
						else
							response.sendRedirect('SUITELET', 'customscript_rf_shipping_cartonlp', 'customdeploy_rf_shipping_cartonlp_di', false, SOarray);
					}
					else
					{
						nlapiLogExecution('ERROR', 'getItemCount :: ', getItemCount);
						if (optedEvent == 'F10' && ( getCartonLP == null || getCartonLP == '') && (getItemCount!= null && getItemCount != "" &&  getItemCount != 'null' && getItemCount != 'undefined' && parseInt(getItemCount)>0)) 
						{    
							SOarray["custparam_shiplpno"] = getShipLP;
							nlapiLogExecution('DEBUG', 'SOarray["custparam_GWgt"] before size id', SOarray["custparam_GWgt"]);
							nlapiLogExecution('DEBUG', 'SOarray["custparam_GCube"] before size id', SOarray["custparam_GCube"]);
							response.sendRedirect('SUITELET', 'customscript_rf_shipping_sizeid', 'customdeploy_rf_shipping_sizeid_di', false, SOarray);	
							return;
						}
						else
						{
							SOarray["custparam_error"] = 'No carton# is attached to ship lp#';
							nlapiLogExecution('DEBUG', 'SOarray["custparam_shiplpno"] ', SOarray["custparam_shiplpno"]);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'CARTON# not found');
						}
					}
				}
				else//if F10 button clicked without entering CartonLP redirect it to size id screen
				{
					nlapiLogExecution('ERROR', 'getItemCount: ', getItemCount);
					if (optedEvent == 'F10' && ( getCartonLP == null || getCartonLP == '') && (getItemCount!= null && getItemCount != "" &&  getItemCount != 'null' && getItemCount != 'undefined' && parseInt(getItemCount)>0)) 
					{    
						SOarray["custparam_shiplpno"] = getShipLP;
						nlapiLogExecution('DEBUG', 'SOarray["custparam_GWgt"] before size id', SOarray["custparam_GWgt"]);
						nlapiLogExecution('DEBUG', 'SOarray["custparam_GCube"] before size id', SOarray["custparam_GCube"]);
						response.sendRedirect('SUITELET', 'customscript_rf_shipping_sizeid', 'customdeploy_rf_shipping_sizeid_di', false, SOarray);	
						return;
					}
					if(getItemCount!= null && getItemCount != "" &&  getItemCount != 'null' && getItemCount != 'undefined' && parseInt(getItemCount)>0)
					{
						SOarray["custparam_shiplpno"] = getShipLP;
						nlapiLogExecution('DEBUG', 'SOarray["custparam_GWgt"] before size id', SOarray["custparam_GWgt"]);
						nlapiLogExecution('DEBUG', 'SOarray["custparam_GCube"] before size id', SOarray["custparam_GCube"]);
						response.sendRedirect('SUITELET', 'customscript_rf_shipping_sizeid', 'customdeploy_rf_shipping_sizeid_di', false, SOarray);
					}
					else//if no Carton LP line was entered 
					{
						SOarray["custparam_error"] = 'No carton# is attached to ship lp#';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'CARTON# not entered');
					}
				}
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'CARTON# not entered');
			}
		}
	}
}

/**
 * Function to get Data from Opentask and related data from MasterLP using Carton LP
 * 
 * @param VCartonLP 
 * @returns
 * 
 */
function GetOrdDetails(VCartonLP)
{
	var filters = new Array();
	var searchresults =new Array();
	var filtersLP = new Array(); 
	filtersLP.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', VCartonLP));

	columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
	searchresultsTemp=searchresults;

	// To get the Container Details from master lp
	var searchresultsLP = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersLP, columns);
	//nlapiLogExecution('DEBUG',"searchresults MasterLP ",searchresultsLP.length );
	if(searchresultsLP != null && searchresultsLP != "" && searchresultsLP.length>0)
	{

		// TO map container lp# of opentask with lp# of mast lp

		var dataNotFoundMsg="";
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', VCartonLP));	 
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
		//Case # 20127020ï¿½Start
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28','7']));
		//Case # 20127020ï¿½End
		nlapiLogExecution('DEBUG',"VCartonLP ", VCartonLP );
		//nlapiLogExecution('DEBUG',"tasktype ", pick );

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_act_qty');
		columns[3] = new nlobjSearchColumn('custrecord_tasktype');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_ship_lp_no');
		columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[6] = new nlobjSearchColumn('custrecord_sku');
		columns[7] = new nlobjSearchColumn('name');    	
		columns[8] = new nlobjSearchColumn('custrecord_container_lp_no');    	 
		columns[9] = new nlobjSearchColumn('custrecord_sku_status');
		columns[10] = new nlobjSearchColumn('custrecord_packcode');
		columns[11] = new nlobjSearchColumn('custrecord_uom_id');
		columns[12] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[13] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');		
		columns[14] = new nlobjSearchColumn('custrecord_totalcube');
		columns[15] = new nlobjSearchColumn('custrecord_total_weight');
		columns[16] = new nlobjSearchColumn('custrecord_comp_id');
	//	columns[17] = new nlobjSearchColumn('custrecord_site_id');
		columns[17] = new nlobjSearchColumn('custrecord_wms_location');
		columns[18] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[19] = new nlobjSearchColumn('custrecord_ship_lp_no');
		columns[20] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[21] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');


		// To get the data from Opentask based on selection criteria
		searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		var searchData=new Array();
		if(searchresults !=null && searchresults != "" && searchresults.length>0)
		{	
			for (var q = 0; searchresults !=null && q < searchresults.length; q++) 
			{
				nlapiLogExecution('DEBUG',"searchresults.length ", searchresults.length );
				nlapiLogExecution('DEBUG',"searchresults OpenTask ", searchresults.length );


				vCompId=searchresults[0].getValue('custrecord_comp_id');
				vSiteId=searchresults[0].getValue('custrecord_wms_location');

				nlapiLogExecution('DEBUG',"searchresultsLP[0].getId() ",searchresultsLP[0].getId() );
				nlapiLogExecution('DEBUG',"searchresults value ", searchresults[0].getValue('custrecord_container_lp_no') );


				nlapiLogExecution('DEBUG',"searchresultsLP[0].getId() ",searchresultsLP[0].getId()); 
				nlapiLogExecution('DEBUG',"searchresults[0].getValue('custrecord_container_lp_no') ",searchresults[0].getValue('custrecord_container_lp_no'));
				nlapiLogExecution('DEBUG',"searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_lp') ",searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_lp'));


				searchData.push(new openTaskRec(searchresults[q].getValue('custrecord_line_no'),searchresults[q].getValue('custrecord_ebiz_sku_no'),
						searchresults[q].getValue('custrecord_act_qty'),searchresults[q].getValue('custrecord_tasktype'),searchresults[q].getValue('custrecord_ebiz_ship_lp_no')
						,searchresults[q].getText('custrecord_actbeginloc'),searchresults[q].getText('custrecord_sku'),searchresults[q].getValue('name')
						,searchresults[q].getValue('custrecord_container_lp_no'),searchresults[q].getText('custrecord_sku_status'),searchresults[q].getValue('custrecord_packcode')
						,searchresults[q].getValue('custrecord_uom_id'),searchresults[q].getValue('custrecord_ebiz_sku_no'),searchresults[q].getValue('custrecord_ebiz_cntrl_no')
						,searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_lp'),searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_sizeid')
						,searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_totwght'),searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_totcube'),searchresults[q].getValue('custrecord_total_weight')
						,searchresults[q].getValue('custrecord_totalcube'), searchresults[q].getId(), searchresultsLP[0].getId()
						,searchresults[q].getValue('custrecord_comp_id'),searchresults[q].getValue('custrecord_wms_location'),searchresults[q].getValue('custrecord_ebiz_order_no')
						,searchresults[q].getValue('custrecord_ship_lp_no'),searchresults[q].getValue('custrecord_ebiz_wave_no'),searchresults[q].getValue('custrecord_ebiz_nsconfirm_ref_no')));

				nlapiLogExecution('DEBUG',"searchresults[0].getValue('custrecord_ebiz_order_no') ",searchresults[0].getValue('custrecord_ebiz_order_no') );
				nlapiLogExecution('DEBUG',"searchData['custrecord_ebiz_order_no'] ",searchData["custrecord_ebiz_order_no"] );
				nlapiLogExecution('DEBUG',"searchData['custrecord_opentask_id'] ",searchData["custrecord_opentask_id"] );

				nlapiLogExecution('DEBUG',"searchData['custrecord_total_weight'] ",searchData["custrecord_total_weight"] );

				nlapiLogExecution('DEBUG',"searchresults Data ",searchData.length );
			}

			if(searchData == null || searchData == "")
			{
				dataNotFoundMsg="Invalid Carton#";
			}
		}
		else
		{
			dataNotFoundMsg="Invalid Carton#";
		}
	}
	else
	{
		dataNotFoundMsg="Invalid Carton#";
	}

	return searchData;
}


/**
 * 
 * @param request
 * @param response
 * @param form
 */
function processEnteredOrder(shipLP,cartonLP,picktaskid,LPMastID,ebizorderno)
{ 
	nlapiLogExecution('DEBUG', 'into processEnteredOrder', '');
	nlapiLogExecution('DEBUG', 'shipLP', shipLP);
	nlapiLogExecution('DEBUG', 'cartonLP', cartonLP);
	nlapiLogExecution('DEBUG', 'picktaskid', picktaskid);
	nlapiLogExecution('DEBUG', 'LPMastID', LPMastID);
	var vBoolAnythingChecked=false;

	// To update ShipLP# in opentask
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', picktaskid);
	transaction.setFieldValue('custrecord_wms_status_flag', "7"); // for status B(Built onto Ship Unit)
	transaction.setFieldValue('custrecord_ship_lp_no', shipLP);
	nlapiSubmitRecord(transaction, true);
	nlapiLogExecution('DEBUG', 'opentask updated ', "");

	// To update ShipLP# in mast LP Cust Record
	var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', LPMastID); 			 
	transaction.setFieldValue('custrecord_ebiz_lpmaster_masterlp', shipLP);
	nlapiSubmitRecord(transaction, true);

	var ruleid = 'Creation of Ship Manifest for LTL';

	var vShippingRule=getSystemRuleValue(ruleid);

	if(vShippingRule == 'BuildShip')
	{
		var vStgCarrierName='';
		var vStageAutoPackFlag = getAutoPackFlagforStage();

		if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){ 
			vStgCarrierName = vStageAutoPackFlag[0][2];
			nlapiLogExecution('DEBUG', 'vStgCarrierName(Stage)',vStgCarrierName); 
		}

		CreateShippingManifestRecordforShipLP(ebizorderno,cartonLP,vStgCarrierName,null,null,null,null,vShippingRule,shipLP);
	}

	nlapiLogExecution('DEBUG', 'LP Master updated ', ""); 	
}

/**
 * To create openTaskRec for multidimentional array
 *  
 */
function openTaskRec(custrecord_line_no, custrecord_ebiz_sku_no, custrecord_act_qty,custrecord_tasktype
		,custrecord_ebiz_ship_lp_no,custrecord_actbeginloc,custrecord_sku,name
		,custrecord_container_lp_no,custrecord_sku_status,custrecord_packcode,custrecord_uom_id
		,custrecord_ebiz_sku_no,custrecord_ebiz_cntrl_no,custrecord_ebiz_lpmaster_lp,custrecord_ebiz_lpmaster_sizeid
		,custrecord_ebiz_lpmaster_totwght,custrecord_ebiz_lpmaster_totcube,custrecord_total_weight,custrecord_totalcube,gid,gidlp,custrecord_comp_id,custrecord_site_id,custrecord_ebiz_order_no,custrecord_ship_lp_no,ebizwaveno,custrecord_ebiz_nsconfirm_ref_no) {
	this.custrecord_Line_no = custrecord_line_no;
	this.custrecord_Ebiz_sku_no = custrecord_ebiz_sku_no;
	this.custrecord_Act_qty = custrecord_act_qty;
	this.custrecord_Tasktype = custrecord_tasktype;
	this.custrecord_Ebiz_ship_lp_no = custrecord_ebiz_ship_lp_no;
	this.custrecord_Actbeginloc = custrecord_actbeginloc;
	this.custrecord_Sku = custrecord_sku;
	this.Name = name;
	this.custrecord_Container_lp_no = custrecord_container_lp_no;
	this.custrecord_Sku_status = custrecord_sku_status;
	this.custrecord_Packcode = custrecord_packcode;
	this.custrecord_Uom_id = custrecord_uom_id;
	this.custrecord_Ebiz_sku_no = custrecord_ebiz_sku_no;
	this.custrecord_Ebiz_cntrl_no = custrecord_ebiz_cntrl_no;
	this.custrecord_Ebiz_lpmaster_lp = custrecord_ebiz_lpmaster_lp;
	this.custrecord_Ebiz_lpmaster_sizeid = custrecord_ebiz_lpmaster_sizeid;
	this.custrecord_Ebiz_lpmaster_totwght = custrecord_ebiz_lpmaster_totwght;
	this.custrecord_Ebiz_lpmaster_totcube = custrecord_ebiz_lpmaster_totcube;
	this.custrecord_Total_weight = custrecord_total_weight;
	this.custrecord_Totalcube = custrecord_totalcube;
	this.custrecord_Opentask_id = gid;
	this.custrecord_Lp_id = gidlp;
	this.custrecord_Comp_id = custrecord_comp_id;
	this.custrecord_Site_id = custrecord_site_id;
	this.custrecord_Ebiz_order_no = custrecord_ebiz_order_no;
	this.custrecord_Ship_lp_no = custrecord_ship_lp_no;
	this.custrecord_Ebizwaveno = ebizwaveno;
	this.custrecord_ebiz_nsconfirm_ref_no=custrecord_ebiz_nsconfirm_ref_no;


	nlapiLogExecution('DEBUG', 'searchresult.custrecord_Opentask_id2', gid);
	nlapiLogExecution('DEBUG', 'searchresult.custrecord_Lp_id2', gidlp);

	nlapiLogExecution('DEBUG', 'searchresult.custrecord_Ebiz_lpmaster_totwght', custrecord_ebiz_lpmaster_totwght);
	nlapiLogExecution('DEBUG', 'searchresult.custrecord_Ebiz_lpmaster_totcube', custrecord_ebiz_lpmaster_totcube);

}
function fnGetShipcity(OrdIntId)
{
	nlapiLogExecution('DEBUG', 'OrdIntId', OrdIntId);
	var trantype = nlapiLookupField('transaction', OrdIntId, 'recordType');
	var searchresults = nlapiLoadRecord(trantype, OrdIntId);
	var vShipCity;
	if(searchresults != null && searchresults != '')
	{
		vShipCity=searchresults.getFieldValue('shipcity');
	}	
	return vShipCity;

}

function getAutoPackFlagforStage(whlocation,vorderType){
	nlapiLogExecution('DEBUG', 'into getAutoPackFlagforStage');
	nlapiLogExecution('DEBUG', 'whlocation',whlocation);
	nlapiLogExecution('DEBUG', 'vorderType',vorderType);
	var vStgRule = new Array();
	//getStageRule(Item, vCarrier, vSite, vCompany, stgDirection,ordertype)
	vStgRule = getStageRule('', '', whlocation, '', 'OUB',vorderType);
	nlapiLogExecution('DEBUG', 'Stage Rule', vStgRule);

	nlapiLogExecution('DEBUG', 'out of getAutoPackFlagforStage');
	return vStgRule;
}
