/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_AutoBreakDown_CL.js,v $
 *     	   $Revision: 1.7.2.3.4.2.4.32.2.2 $
 *     	   $Date: 2015/11/24 11:19:24 $
 *     	   $Author: aanchal $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_AutoBreakDown_CL.js,v $
 * Revision 1.7.2.3.4.2.4.32.2.2  2015/11/24 11:19:24  aanchal
 * 2015.2 issue fix
 * 201415679
 *
 * Revision 1.7.2.3.4.2.4.32.2.1  2015/11/16 16:09:15  sponnaganti
 * 201415678
 * 2015.2 issue fix
 *
 * Revision 1.7.2.3.4.2.4.32  2015/07/24 15:26:49  grao
 * 2015.2   issue fixes  201413061   and 201413096
 *
 * Revision 1.7.2.3.4.2.4.31  2015/07/15 15:15:41  grao
 * 2015.2 ssue fixes  201413061
 *
 * Revision 1.7.2.3.4.2.4.30  2015/06/12 15:47:47  grao
 * SB issue fixes 201413063
 *
 * Revision 1.7.2.3.4.2.4.29  2015/05/22 13:24:52  schepuri
 * case# 201412882
 *
 * Revision 1.7.2.3.4.2.4.28  2015/04/06 12:39:25  grao
 * CT Prod issue fixes  201412258
 *
 * Revision 1.7.2.3.4.2.4.27  2014/11/05 15:40:54  skavuri
 * Case# 201410937 Std bundle issue fixed
 *
 * Revision 1.7.2.3.4.2.4.26  2014/10/20 15:45:27  skavuri
 * Case# 201410739 std bundle issue fixed
 *
 * Revision 1.7.2.3.4.2.4.25  2014/10/17 14:03:58  skavuri
 * Case# 201410635 Std bundle Issue fixed
 *
 * Revision 1.7.2.3.4.2.4.24  2014/10/17 13:51:08  skavuri
 * Case# 201410635 Std bundle Issue fixed
 *
 * Revision 1.7.2.3.4.2.4.23  2014/06/12 07:16:12  skavuri
 * Case # 20148802 SB Issue Fixed
 *
 * Revision 1.7.2.3.4.2.4.22  2014/06/09 15:28:55  skavuri
 * Case # 20148802 SB Issue Fixed
 *
 * Revision 1.7.2.3.4.2.4.21  2014/05/28 15:22:04  skavuri
 * Case# 20148594 SB Issue Fixed
 *
 * Revision 1.7.2.3.4.2.4.20  2014/05/27 07:36:59  snimmakayala
 * Case#: 20148135
 * Schedule Script Status
 *
 * Revision 1.7.2.3.4.2.4.19  2014/05/23 15:37:24  skavuri
 * Case # 20148487 SB Issue Fixed
 *
 * Revision 1.7.2.3.4.2.4.18  2014/05/16 13:25:16  sponnaganti
 * case# 20148001
 * standard BUndle issue fix
 *
 * Revision 1.7.2.3.4.2.4.17  2014/05/14 15:29:55  sponnaganti
 * case# 20148387
 *  (serial nums dispaying after entering in serial entry )
 *
 * Revision 1.7.2.3.4.2.4.16  2014/05/05 14:45:24  sponnaganti
 * case# 20148260
 * (serial numbers asking again)
 *
 * Revision 1.7.2.3.4.2.4.15  2014/04/29 06:04:04  skreddy
 * case # 20148184
 * MHP  issue fix
 *
 * Revision 1.7.2.3.4.2.4.14  2014/01/06 08:01:06  schepuri
 * 20126620
 *
 * Revision 1.7.2.3.4.2.4.13  2013/12/23 14:10:07  schepuri
 * 20126456
 *
 * Revision 1.7.2.3.4.2.4.12  2013/12/16 13:58:01  schepuri
 * 20125978 , 20126403
 *
 * Revision 1.7.2.3.4.2.4.11  2013/12/11 13:52:59  schepuri
 * 20126234 ,20126235
 *
 * Revision 1.7.2.3.4.2.4.10  2013/11/12 18:10:48  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Auto break down scheduler is fixed.
 *
 * Revision 1.7.2.3.4.2.4.9  2013/10/23 16:14:56  nneelam
 * Case# 20124847
 * Pop Up Serial Screen when Lot/Serial Textbox is changed.
 *
 * Revision 1.7.2.3.4.2.4.8  2013/09/23 15:57:49  rmukkera
 * Case# 20124545
 *
 * Revision 1.7.2.3.4.2.4.7  2013/09/11 15:23:51  rmukkera
 * Case# 20124376
 *
 * Revision 1.7.2.3.4.2.4.6  2013/08/19 16:20:27  rmukkera
 * Issue Fix related to FIXED--->20123785�,20123902�,20123903,20123906�,20123907�,20123910�,�
 *
 * Revision 1.7.2.3.4.2.4.5  2013/06/27 06:50:21  nneelam
 * Case# 20123112
 * Multiple Item PO.
 *
 * Revision 1.7.2.3.4.2.4.4  2013/04/30 18:44:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.7.2.3.4.2.4.3  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.7.2.3.4.2.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.7.2.3.4.2.4.1  2013/03/05 14:51:26  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.7.2.3.4.2  2013/01/23 04:05:57  kavitha
 * CASE201112/CR201113/LOG201121
 * Dynacraft - Inbound Reversal CR
 *
 * Revision 1.7.2.3.4.1  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.7.2.3  2012/08/22 14:47:57  schepuri
 * no message
 *
 * Revision 1.7.2.2  2012/04/20 12:23:59  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.7.2.1  2012/03/16 07:23:31  spendyala
 * CASE201112/CR201113/LOG201121
 * code sync from trunk.
 *
 * Revision 1.7  2011/12/19 06:53:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Added functionality to validate and drive the user to new batch entry screen .
 *
 * Revision 1.6  2011/07/25 15:51:31  skota
 * CASE201112/CR201113/LOG201121
 * Added function to do client side validation
 *
 * 
 *****************************************************************************/



function OnAutobreakdown()
{
	var flag=nlapiGetFieldValue('custpage_validationflag');
//	alert(flag);
	if(flag=='GET')
	{
		var receiptvalue=nlapiGetFieldValue('custpage_receiptfield');
		var trailervalue=nlapiGetFieldValue('custpage_trailerfield');
		var povalue=nlapiGetFieldValue('custpage_po');
// case no 20126456� 

		var ordertype=nlapiGetFieldValue('custpage_ordertype');
		//alert(ordertype);
		if(receiptvalue==''&&trailervalue==''&&povalue=='')
		{
			alert('Fields Cant be left empty,need atleast one value to process');
			return false;
		}
// case no 20126620
		if(ordertype != null && ordertype != '')
		{
			var filterspo = new Array();
			filterspo.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
			filterspo.push(new nlobjSearchFilter('status', null, 'noneof', ['PurchOrd:F','PurchOrd:H']));
			filterspo.push(new nlobjSearchFilter('tranid',null,'is',povalue));

			var colspo = new Array();
			colspo[0]=new nlobjSearchColumn('internalid');
			colspo[0].setSort(true);

			//var searchresults = nlapiSearchRecord('purchaseorder', null, filterspo, colspo);
			var searchresults = nlapiSearchRecord(ordertype, null, filterspo, colspo);

			if(searchresults == null || searchresults == '')
			{
				alert('Please select a valid Order#/Transaction Type');
				return false;
			}
			//Case# 201410635 starts
			//alert(ordertype);
			if(ordertype=='returnauthorization')
			{
				var POfilters=new Array();
				POfilters.push(new nlobjSearchFilter('tranid',null,'is',povalue));
				POfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
				POfilters.push(new nlobjSearchFilter('recordtype',null,'is','returnauthorization'));

				var POcols=new Array();
				POcols[0]=new nlobjSearchColumn('status');
				POcols[1]=new nlobjSearchColumn('location');
				POcols[2]= new nlobjSearchColumn('custbody_nswms_company');

				var PORec=nlapiSearchRecord('transaction',null,POfilters,POcols);

				if(PORec!=null&&PORec!='')
				{
					var poStatus=PORec[0].getValue('status');
					nlapiLogExecution('DEBUG','poStatus',poStatus);
					var poToLocationID=PORec[0].getValue('location');
					nlapiLogExecution('DEBUG','poToLocation',poToLocationID);
					var whCompany=PORec[0].getValue('custbody_nswms_company');
					nlapiLogExecution('DEBUG','poToLocation',whCompany);
					var Tomwhsiteflag = 'F';
				}
				//alert('poStatus'+poStatus);
				if(poStatus!='pendingReceipt'&& poStatus!='partiallyReceived' && poStatus!='pendingReceiptPartFulfilled' && poStatus!='pendingRefundPartReceived') // Case# 201410937
				{
					alert("INVALID RMA");
					return false;
				}
			}
			//Case# 201410635 ends
		}

		return true;
	}
	else{
		//alert('In OnAutobreakdown');
		var POlinecount = nlapiGetLineItemCount('custpage_items');
		var userselection = false;	
		var selectedcount = 0;
		var continuebrkdown = "N";
		var ruleValue = 0;
// case no 20126234� ,20126235� 


		var hcartLP=nlapiGetFieldValue('custpage_hcartlp');
		var cartLP='';
		var SysRuleFilters = new Array();//
		SysRuleFilters.push(new nlobjSearchFilter('name', null, 'is', 'Max # of pallets allowed per trailer'));
		SysRuleFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var SysRuleColumns = new Array();
		SysRuleColumns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		var SystemRulesSearchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, SysRuleFilters, SysRuleColumns);
		if(SystemRulesSearchresults!=null && SystemRulesSearchresults.length>0)
		{
			ruleValue = SystemRulesSearchresults[0].getValue('custrecord_ebizrulevalue');
		}
		
		var arrItems = new Array();
		var arrLocs = new Array();
		
		for (var p1 = 1; p1 <= POlinecount; p1++) 
		{
			var brkdwnflag1 = nlapiGetLineItemValue('custpage_items', 'custpage_brkdwnflag', p1);
			
			if (brkdwnflag1 == 'T') 
			{
				arrItems.push(nlapiGetLineItemValue('custpage_items','custpage_iteminternalid', p1));
				if(nlapiGetLineItemValue('custpage_items','custpage_polocationid', p1)!=null && nlapiGetLineItemValue('custpage_items','custpage_polocationid', p1)!='')
				arrLocs.push(nlapiGetLineItemValue('custpage_items','custpage_polocationid', p1));				
			}
		}
		
		var ItemDimens=CheckItemDimensall(arrItems, arrLocs);//

		for (var p = 1; p <= POlinecount; p++) 
		{
			var brkdwnflag = nlapiGetLineItemValue('custpage_items', 'custpage_brkdwnflag', p);
			if (brkdwnflag == 'T') 
			{
				userselection = true;

				var palletqty = nlapiGetLineItemValue('custpage_items', 'custpage_palletqty', p);
				var RCVquantity = nlapiGetLineItemValue('custpage_items','custpage_rcvquantity', p);
				var Quantity = nlapiGetLineItemValue('custpage_items','custpage_quantity', p);
				
				var ItemId = nlapiGetLineItemValue('custpage_items','custpage_iteminternalid', p);
				var LocationID = nlapiGetLineItemValue('custpage_items','custpage_polocationid', p);
				var ItemName = nlapiGetLineItemValue('custpage_items','custpage_itemname', p);
				
				//Case# 201410635 starts
				var poItemPackcode = nlapiGetLineItemValue('custpage_items','custpage_popackcode', p);
				
				var boolDimsfound='F';
				for (var p2 = 0; ItemDimens != null && p2 < ItemDimens.length; p2++) //case# 201412882
				{
					//alert("ItemDimens.length:" + ItemDimens.length);

					var vitem = ItemDimens[p2].getValue('custrecord_ebizitemdims');
							
					
					if(vitem == ItemId)
					{
						boolDimsfound='T';
						break;
					}
					

				}
				
				if(boolDimsfound=='F')
				{
					alert('Item Dimensions are not Configured for Item#'+ItemName);
					return false;
				}


				//alert("hcartLP:" + hcartLP);
				var vcartLP= '';
				
				var whloc=nlapiGetLineItemValue('custpage_items','custpage_polocationid',p);
				
				vcartLP = nlapiGetLineItemValue('custpage_items','custpage_cartlp', p);
				if(vcartLP!=null && vcartLP!='')
				{
					var isValidcartLP=MastLPExists(cartLP,whloc);
					if(isValidcartLP!='')
					{
						if(vcartLP != null && vcartLP != '')
						{
							alert('Invalid Cart LP!');
							nlapiSetLineItemValue('custpage_items','custpage_cartlp',p,'');
							nlapiSetLineItemValue('custpage_items','custpage_brkdwnflag',p,'F');
						}
						return false;
					}
				}
				
				if(hcartLP == null || hcartLP == '')
				{
					cartLP = nlapiGetLineItemValue('custpage_items','custpage_cartlp', p);

				}

				else
					cartLP = hcartLP;

				if(cartLP!=null && cartLP!='')
				{
					
					var isValidcartLP=MastLPExists(cartLP,whloc);
					if(isValidcartLP!='')
					{
						//alert(hcartLP);
						//alert(vcartLP);
						//alert(isValidcartLP);



						if(hcartLP != null && hcartLP != '')
						{
							alert('Invalid Cart LP!');
							
							nlapiSetFieldValue('custpage_hcartlp','',false);
							nlapiSetLineItemValue('custpage_items','custpage_brkdwnflag',p,'F');
							nlapiSetLineItemValue('custpage_items','custpage_hcartlp',p,'F');

						}
						else if(vcartLP != null && vcartLP != '')
						{
							nlapiSetLineItemValue('custpage_items','custpage_cartlp',p,'');
							nlapiSetLineItemValue('custpage_items','custpage_brkdwnflag',p,'F');
						}
						return false;
					}
				}


				var noofpallets = 0;
				var totalqty = 0;
				if(palletqty != null && palletqty != "")
				{
					if(parseInt(palletqty) == 1 || parseInt(palletqty) == 999)
					{
						if(confirm('Pallet qty is 1 or 999. Do you want to Submit?'))
						{
							continuebrkdown = "Y"; 

						}
						else
						{
							return false;
						}
					}
				}


				if((palletqty != null && palletqty != "") && (Quantity != null && Quantity != ""))
				{
					if(RCVquantity != null && RCVquantity != "")
					{
						totalqty = RCVquantity;
					}
					else
					{
						totalqty = Quantity;
					}
					//noofpallets = parseInt(totalqty) / parseInt(palletqty);

					if(parseInt(palletqty) > 0 && (parseInt(totalqty) > parseInt(palletqty)))
					{
						noofpallets = parseInt(totalqty)/parseInt(palletqty);
						noofpallets = Math.floor(noofpallets);
						if(noofpallets == 0)
							noofpallets = 1;
					}

					if(parseInt(noofpallets) > parseInt(ruleValue))
					{
						alert("Total number of pallets exceeding the maximum limit. Please contact warehouse manager.");
						return false;
					}
				}


				var itmid = nlapiGetLineItemValue('custpage_items', 'custpage_iteminternalid', p);
				//var itmid = nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');
				var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
				var columns = nlapiLookupField('item', itmid, fields);
				var ItemType = columns.recordType;	

				var serialInflg="F";		
				serialInflg = columns.custitem_ebizserialin;
				var batchflag="F";
				batchflag= columns.custitem_ebizbatchlot;

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {


					var POid = nlapiGetLineItemValue('custpage_items','custpage_po',p);
					var POlineno = nlapiGetLineItemValue('custpage_items','custpage_poline',p);
					var ItemId = nlapiGetLineItemValue('custpage_items','custpage_iteminternalid',p);
					var vqty = nlapiGetLineItemValue('custpage_items', 'custpage_totalqtyforserqty',p);

					var serialCustRecChk = SerialCustRecordChk(ItemId,POid,POlineno);

					//var SerialNo = nlapiGetLineItemValue('custpage_items', 'custpage_lotbatch',p);
					
					if(serialCustRecChk == 'undefined'  || serialCustRecChk == '' || serialCustRecChk == null)
					{
						serialCustRecChk = 0;
					}
					
				//	alert(vqty);
					//alert(serialCustRecChk);

					if (parseInt(vqty)<=parseInt(serialCustRecChk)) 
					{

						// return true;
						/*var serialCustRecChk = SerialCustRecordChk(SerialNo);
						if (serialCustRecChk) 
						{

							//return serialCustRecChk;
						}
						 */		
					}
					else 
					{
						alert("Please enter value(s)for: Serial Items in Serial Entry");
						var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
						/*var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') {
								linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') {
									linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}*/
						linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_items','custpage_polocationid',p);
						linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_items','custpage_pocompanyid',p);
						linkURL = linkURL + '&custparam_serialskuid=' + itmid;
						linkURL = linkURL + '&custparam_lot=' + BatchNo;
						linkURL = linkURL + '&custparam_name=' + BatchNo;
						linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_items','custpage_poitemstatus',p);
						linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_items','custpage_popackcode',p);
						linkURL = linkURL + '&custparam_serialpoid=' + nlapiGetLineItemValue('custpage_items','custpage_po',p);
						linkURL = linkURL + '&custparam_serialskulineno=' + nlapiGetLineItemValue('custpage_items','custpage_poline',p);
						linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetLineItemValue('custpage_items', 'custpage_cartlp',p);
						linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetLineItemValue('custpage_items', 'custpage_poquantity',p);

						window.open(linkURL);

						
						//nlapiSetLineItemValue('custpage_items','custpage_brkdwnflag',p,'F');//  Case# 20148487
						return false;
					}
				}
				/*else
					{

						alert("Please enter value(s)for: Serial Items in Serial Entry");
						nlapiSetLineItemValue('custpage_items','custpage_brkdwnflag',p,'F');
						return false;
					}

				}*/
				else if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
				{
//					alert(nlapiGetCurrentLineItemValue('custpage_items','custpage_poitemstatus'));
					try {
						var BatchNoentered = nlapiGetLineItemValue('custpage_items', 'custpage_lotbatch',p);
						if (BatchNoentered) {
							var flag="F";
							var batcharray=BatchNoentered.split(",");

							if(batcharray!=null&&batcharray!="")
							{
								for(var count=0;count<batcharray.length;count++)
								{
									var BatchNo=batcharray[count];

									var batchCustRecChk = BatchCustRecordChk(BatchNo,itmid);
									if (batchCustRecChk) 
									{
										//return batchCustRecChk;
									}
									else 
									{
										var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
										/*var ctx = nlapiGetContext();
								if (ctx.getEnvironment() == 'PRODUCTION') 
								{
									linkURL = 'https://system.netsuite.com' + linkURL;			
								}
								else 
									if (ctx.getEnvironment() == 'SANDBOX') 
									{
										linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
									}*/
								linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_items','custpage_polocationid',p);
								linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_items','custpage_pocompanyid',p);
								linkURL = linkURL + '&custparam_serialskuid=' + itmid;
								linkURL = linkURL + '&custparam_lot=' + BatchNo;
								linkURL = linkURL + '&custparam_name=' + BatchNo;
								linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_items','custpage_poitemstatus',p);
								linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_items','custpage_popackcode',p);

								window.open(linkURL);

										alert("Please enter valid value(s)for: Lot / Batch Items in Lot / Batch Entry");
										nlapiSetLineItemValue('custpage_items','custpage_brkdwnflag',p,'F');
										return false;
									}
								}
							}
						}
						else
						{
							alert("Please enter valid value(s)for: Lot / Batch Items in Lot / Batch Entry");
							nlapiSetLineItemValue('custpage_items','custpage_brkdwnflag',p,'F');
							return false;
						}
					}
					catch(exps)
					{
						alert("Error: "+exps);
					}
				}
				else
				{
				}



			}
		}

		if(userselection == false)
		{
			alert('Atleast one line item should be selected');
			return false;
		}
		return true;
	}
}


/**
 * @param type
 * @param name
 * @returns
 */
function validateLOTBatch(type,name)
{

	if(name=='custpage_brkdwnflag')
	{
		var flagstatus=nlapiGetCurrentLineItemValue('custpage_items','custpage_brkdwnflag');
		if(flagstatus=='T')
		{
			var itmid = nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');

			// Case# 20148487
			var POid = nlapiGetCurrentLineItemValue('custpage_items','custpage_po');
			var POlineno = nlapiGetCurrentLineItemValue('custpage_items','custpage_poline');
			// Case# 20148487
			var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', itmid, fields);
			var ItemType = columns.recordType;	
			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;
			var batchflag="F";
			batchflag= columns.custitem_ebizbatchlot;

			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
				//case# 20148260 starts (serial numbers asking again)
				//alert("Please enter value(s)for: Serial Items in Serial Entry");
				// Case# 20148487 starts
				/*var filterSerialEntry=new Array();
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid')));
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizpono',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_po')));
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialpolineno',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_poline')));
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialwmsstatus',null,'anyof',1));

				var SearchRec=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filterSerialEntry,null);*/
				// Case# 20148487 starts
				var serialCustRecChk = SerialCustRecordChk(itmid,POid,POlineno);
				//if(SearchRec!=null&&SearchRec!=""){
					//case# 20148387 starts (serial nums dispaying after entering in serial entry ) 
					/*var linkURL = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial_no', 'customdeploy_lprelated_serial_no');
					linkURL = linkURL + '&custparam_locationid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_polocationid');
					linkURL = linkURL + '&custparam_companyid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_pocompanyid');
					linkURL = linkURL + '&custparam_serialskuid=' + itmid;
					linkURL = linkURL + '&custparam_lot=' + BatchNo;
					linkURL = linkURL + '&custparam_name=' + BatchNo;
					linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poitemstatus');
					linkURL = linkURL + '&custparam_packcode=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_popackcode');
					linkURL = linkURL + '&custparam_serialpoid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_po');
					linkURL = linkURL + '&custparam_serialskulineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poline');
					linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_cartlp');
					linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_poquantity');
					window.open(linkURL);
					return true;*/
					//case# 20148387 ends
					if(serialCustRecChk){
				}
				else{
					//alert('hiiiiiii');
					alert("Please enter value(s)for: Serial Items in Serial Entry");
					var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
					//var linkURL = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial_no', 'customdeploy_lprelated_serial_no');
					linkURL = linkURL + '&custparam_locationid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_polocationid');
					linkURL = linkURL + '&custparam_companyid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_pocompanyid');
					linkURL = linkURL + '&custparam_serialskuid=' + itmid;
					linkURL = linkURL + '&custparam_lot=' + BatchNo;
					linkURL = linkURL + '&custparam_name=' + BatchNo;
					linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poitemstatus');
					linkURL = linkURL + '&custparam_packcode=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_popackcode');
					linkURL = linkURL + '&custparam_serialpoid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_po');
					linkURL = linkURL + '&custparam_serialskulineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poline');
					linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_cartlp');
					linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_poquantity');
					window.open(linkURL);
					return false;
				}
				//case# 20148260 end
				/*var ctx = nlapiGetContext();
				if (ctx.getEnvironment() == 'PRODUCTION') {
					linkURL = 'https://system.netsuite.com' + linkURL;			
				}
				else 
					if (ctx.getEnvironment() == 'SANDBOX') {
						linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
					}*/


			}
			else if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
			{/*
				// Case# 20148802
//				alert(nlapiGetCurrentLineItemValue('custpage_items','custpage_poitemstatus'));
				try {
					var BatchNoentered = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lotbatch');
					if (BatchNoentered) {
						var flag="F";
						var batcharray=BatchNoentered.split(",");

						if(batcharray!=null&&batcharray!="")
						{
							for(var count=0;count<batcharray.length;count++)
							{
								var BatchNo=batcharray[count];
								
								var batchCustRecChk = BatchCustRecordChk(BatchNo,itmid);
								if (batchCustRecChk) 
								{
									//return batchCustRecChk;
									flag="T";
								}
								else 
								{
									var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
									var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') 
							{
								linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') 
								{
									linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}
							linkURL = linkURL + '&custparam_locationid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_polocationid');
							linkURL = linkURL + '&custparam_companyid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_pocompanyid');
							linkURL = linkURL + '&custparam_serialskuid=' + itmid;
							linkURL = linkURL + '&custparam_lot=' + BatchNo;
							linkURL = linkURL + '&custparam_name=' + BatchNo;
							linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poitemstatus');
							linkURL = linkURL + '&custparam_packcode=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_popackcode');

							window.open(linkURL);

									alert("Please enter valid value(s)for: LOT# Items in LOT# Entry");
									nlapiSetCurrentLineItemValue('custpage_items','custpage_brkdwnflag','F');
									return false;
								}
							}
						}
					}
					else
					{
						alert("Please enter value(s)for: LOT# Items in LOT# Entry");
						nlapiSetCurrentLineItemValue('custpage_items','custpage_brkdwnflag','F');
						return false;
					}
				}
				catch(exps)
				{
					alert("Error "+exps);
				}*/
				// Case# 20148802 ends
			}
			else
			{
			}



			var cartLP;
			/*if(name=='custpage_hcartlp')
				{
					cartLP=nlapiGetFieldValue('custpage_hcartlp');
				}*/
			
			if((nlapiGetCurrentLineItemValue('custpage_items','custpage_cartlp') != null && nlapiGetCurrentLineItemValue('custpage_items','custpage_cartlp') != '') && nlapiGetCurrentLineItemValue('custpage_items','custpage_brkdwnflag')=='T')
			{
				cartLP=nlapiGetCurrentLineItemValue('custpage_items','custpage_cartlp');
			}
			else
			{
				cartLP=null;
			}
			
			if(cartLP!=null)
			{
				var whloc=nlapiGetCurrentLineItemValue('custpage_items','custpage_polocationid');
				var isValidcartLP=MastLPExists(cartLP,whloc);
				if(isValidcartLP!='')
				{
					alert(isValidcartLP);
					/*if(name=='custpage_hcartlp')
						{
							nlapiSetCurrentLineItemValue('custpage_items','custpage_brkdwnflag','F');
							nlapiSetFieldValue('custpage_hcartlp','',false);


						}*/

					nlapiSetCurrentLineItemValue('custpage_items','custpage_brkdwnflag','F');
					nlapiSetCurrentLineItemValue('custpage_items','custpage_cartlp','',false);


					return false;
				}
			}



		}
	}

	//case # 20124847, Added the below if condition to ask for serial/ Lot # values.
	if(name=='custpage_lotbatch')
	{
		var flagstatus=nlapiGetCurrentLineItemValue('custpage_items','custpage_brkdwnflag');
		/*if(flagstatus=='T')
		{*/
			var itmid = nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');
			// Case# 20148487
			var POid = nlapiGetCurrentLineItemValue('custpage_items','custpage_po');
			var POlineno = nlapiGetCurrentLineItemValue('custpage_items','custpage_poline');
			// Case# 20148487
			var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', itmid, fields);
			var ItemType = columns.recordType;	
			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;
			var batchflag="F";
			batchflag= columns.custitem_ebizbatchlot;

			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
				//alert('hi');
				//alert("Please enter value(s)for: Serial Items in Serial Entry");// Case# 20148487
				/*var filterSerialEntry=new Array();
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid')));
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizpono',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_po')));
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialpolineno',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_poline')));
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialwmsstatus',null,'anyof',1));

				var SearchRec=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filterSerialEntry,null);*/
				// Case# 20148487 starts
				var serialCustRecChk = SerialCustRecordChk(itmid,POid,POlineno);
				//if(SearchRec!=null&&SearchRec!=""){
					if(serialCustRecChk){
					//  Case# 20148487
					/*var linkURL = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial_no', 'customdeploy_lprelated_serial_no');
					linkURL = linkURL + '&custparam_locationid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_polocationid');
					linkURL = linkURL + '&custparam_companyid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_pocompanyid');
					linkURL = linkURL + '&custparam_serialskuid=' + itmid;
					linkURL = linkURL + '&custparam_lot=' + BatchNo;
					linkURL = linkURL + '&custparam_name=' + BatchNo;
					linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poitemstatus');
					linkURL = linkURL + '&custparam_packcode=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_popackcode');
					linkURL = linkURL + '&custparam_serialpoid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_po');
					linkURL = linkURL + '&custparam_serialskulineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poline');
					linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_cartlp');
					linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_poquantity');
					window.open(linkURL);*/
					//
					//return true;

				}
				else{
					alert("Please enter value(s)for: Serial Items in Serial Entry");
					//Case# 201410739 starts
					var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
					//var linkURL = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial_no', 'customdeploy_lprelated_serial_no');
					//Case# 201410739 ends
					linkURL = linkURL + '&custparam_locationid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_polocationid');
					linkURL = linkURL + '&custparam_companyid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_pocompanyid');
					linkURL = linkURL + '&custparam_serialskuid=' + itmid;
					linkURL = linkURL + '&custparam_lot=' + BatchNo;
					linkURL = linkURL + '&custparam_name=' + BatchNo;
					linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poitemstatus');
					linkURL = linkURL + '&custparam_packcode=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_popackcode');
					linkURL = linkURL + '&custparam_serialpoid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_po');
					linkURL = linkURL + '&custparam_serialskulineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poline');
					linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_cartlp');
					linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_poquantity');
					window.open(linkURL);
					return false;
				}
				/*var ctx = nlapiGetContext();
				if (ctx.getEnvironment() == 'PRODUCTION') {
					linkURL = 'https://system.netsuite.com' + linkURL;			
				}
				else 
					if (ctx.getEnvironment() == 'SANDBOX') {
						linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
					}*/


			}
			else if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
			{
//				alert(nlapiGetCurrentLineItemValue('custpage_items','custpage_poitemstatus'));
				try {
					var BatchNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lotbatch');
					if (BatchNo) {

						var batchCustRecChk = BatchCustRecordChk(BatchNo);
						if (batchCustRecChk) 
						{
							return batchCustRecChk;
						}
						else 
						{
							var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
							/*var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') 
							{
								linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') 
								{
									linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}*/
							linkURL = linkURL + '&custparam_locationid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_polocationid');
							linkURL = linkURL + '&custparam_companyid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_pocompanyid');
							linkURL = linkURL + '&custparam_serialskuid=' + itmid;
							linkURL = linkURL + '&custparam_lot=' + BatchNo;
							linkURL = linkURL + '&custparam_name=' + BatchNo;
							linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_poitemstatus');
							linkURL = linkURL + '&custparam_packcode=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_popackcode');

							window.open(linkURL);

							alert("Please enter valid value(s)for: LOT# Items in LOT# Entry");
							nlapiSetCurrentLineItemValue('custpage_items','custpage_brkdwnflag','F');
							return false;
						}

					}
					else
					{
						alert("Please enter value(s)for: LOT# Items in LOT# Entry");
						nlapiSetCurrentLineItemValue('custpage_items','custpage_brkdwnflag','F');
						return false;
					}
				}
				catch(exps)
				{
					alert("Error "+exps);
				}
			}
			else
			{
				
				var BatchNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lotbatch');
				if(BatchNo!=null && BatchNo!='')
				{
					alert("Item not elligible for Lot entry/not a Lot item");
					nlapiSetCurrentLineItemValue('custpage_items','custpage_lotbatch','');
					nlapiSetCurrentLineItemValue('custpage_items','custpage_brkdwnflag','F');
					return false;
				}
				
			}
		//}
	}
	
	//End
	
	
	if(name=='custpage_rcvquantity')
	{
		var qtyentered=nlapiGetCurrentLineItemValue('custpage_items','custpage_rcvquantity');
		var qtyleftToAutoBreakDown=nlapiGetCurrentLineItemValue('custpage_items','custpage_autobreakdownqty');
		if(parseFloat(qtyentered)>parseFloat(qtyleftToAutoBreakDown))
		{
			alert('Qty Exceed the PO Qty');
			nlapiSetCurrentLineItemValue('custpage_items','custpage_rcvquantity',qtyleftToAutoBreakDown);
			return false;
		}
	}

	//if(name=='custpage_hcartlp' || name=='custpage_cartlp')
	/*if(name=='custpage_cartlp')
	{
		var cartLP;
		if(name=='custpage_hcartlp')
		{
			cartLP=nlapiGetFieldValue('custpage_hcartlp');
		}
		
		if(name=='custpage_cartlp' && nlapiGetCurrentLineItemValue('custpage_items','custpage_brkdwnflag')=='T')
		{
			cartLP=nlapiGetCurrentLineItemValue('custpage_items','custpage_cartlp');
		}
		else
		{
			cartLP=null;
		}
		
		if(cartLP!=null)
		{
			var whloc=nlapiGetCurrentLineItemValue('custpage_items','custpage_polocationid');
			var isValidcartLP=MastLPExists(cartLP,whloc);
			if(isValidcartLP!='')
			{
				alert(isValidcartLP);
				if(name=='custpage_hcartlp')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_brkdwnflag','F');
					nlapiSetFieldValue('custpage_hcartlp','',false);


				}
				if(name=='custpage_cartlp')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_brkdwnflag','F');
					nlapiSetCurrentLineItemValue('custpage_items','custpage_cartlp','',false);

				}
				return false;
			}
		}


	}*/
}

function MastLPExists(manualCartLP,whloc)
{
	var message="";
	var filters =new Array();
	var vResult=new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', manualCartLP));
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_cart_closeflag', null, 'is', 'T'));
	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_cart_closeflag'));
	// To get the data from Master LP based on selection criteria
	searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	if(searchresults!= null && searchresults.length>0)
	{
		if(searchresults[0].getValue('custrecord_ebiz_cart_closeflag')=='T')
		{
			message="Cart LP# already closed, Please enter another Cart LP#";
			nlapiLogExecution('DEBUG',"message ",message );

		}
		return message="";
//		else
//		{
//		message="Cart LP# already exists, Please enter another Cart LP#";
//		nlapiLogExecution('DEBUG',"message ",message );
//		return message;
//		}
	}
	else
	{
		//if(ebiznet_LPRange_CL_withLPType1(manualCartLP,'2','4',whloc))// for User Defined  with Cart LPType
		//{
		vResult=ebiznet_LPRange_CL_withLPType1(manualCartLP,'2','4',whloc)// for User Defined  with Cart LPType
		if (vResult[0] == 'Y')
		{
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', manualCartLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', manualCartLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
			return message;
		}
		/*else
		{
			message="CART LP# IS OUT OF RANGE";
			return message;
		}*/
		else if (vResult[1] == 'O')
		{
			message="CART LP# IS OUT OF RANGE";
			//case 20124932 start
			return message;
			//case end
		}
		else if(vResult[1] == 'I')
		{
			message="Please enter valid cart no";
			//case 20124932 start
			return message;
			//case end

		}
		else if(vResult[1] == 'R')
		{
			
			message="PLs give Range along with LpPrefix";
			//case 20124932 start
			return message;
			//case end

		}
	}
}


/**
 * @param ChknBatchNo
 * @returns {Boolean}
 */
function BatchCustRecordChk(ChknBatchNo,item)
{
//	alert(ChknBatchNo);
	var batch=ChknBatchNo.split("(")[0];
//	alert(batch);
	var filtersbat = new Array();
	filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', batch);
	if(item !='' && item!='null' && item!=null) // Case# 20148594
	filtersbat[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', item);

	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);
//	alert(SrchRecord);
	if (SrchRecord) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}

/*function SerialCustRecordChk(ChknSerialNo)
{
	var filtersbat = new Array();
	filtersbat[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', ChknSerialNo);

	var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersbat);

	if (SrchRecord) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}*/


function SerialCustRecordChk(ItemId,POid,POlineno)
{
	var trantype;
	var filterSerialEntry=new Array();
	if(POid!=null && POid!='' && POid!=-1)
	{
		trantype = nlapiLookupField('transaction', POid, 'recordType');
	}
	filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',ItemId));
	//case# 20148001 starts 
	if(trantype=='purchaseorder' || trantype=='transferorder')
	{//case# 20148001 ends
		filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizpono',null,'is',POid));
		filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialpolineno',null,'is',POlineno));
	}
	else if(trantype=='returnauthorization')
		{
		filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizrmano',null,'is',POid));
		filterSerialEntry.push(new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno',null,'is',POlineno));
		}
	filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialwmsstatus',null,'anyof',1));

 	var searchres = 0;
	var SearchRec=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filterSerialEntry,null);
	if(SearchRec!=null&&SearchRec!=""){
		searchres = SearchRec.length;
		return searchres;
	}
	/*else 
	{
		return false;
	}*/
}

function CheckItemDimens(itemId, location,poItemPackcode)//Case# 201410635
{
	
	var itemSearchResults='';
	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));
	//Case# 201410635 starts
	if(poItemPackcode!=null && poItemPackcode!='' && poItemPackcode!='null')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',poItemPackcode));
	//Case# 201410635 ends
	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 	
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true); 	

	itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);
	return itemSearchResults;
}


function CheckItemDimensall(arritems, arrlocs)
{

	var itemSearchResults='';
	var itemFilters = new Array();
	if(arritems!=null && arritems!='')
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', arritems));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(arrlocs!=null&&arrlocs!='')
	{
		arrlocs.push('@NONE@');
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',arrlocs));
	}
	
	

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 	
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true); 	
	itemColumns[5] = new nlobjSearchColumn('custrecord_ebizsiteskudim'); 	

	itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);
	return itemSearchResults;
}