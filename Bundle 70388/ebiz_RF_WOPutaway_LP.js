/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WOPutaway_LP.js,v $
 *     	   $Revision: 1.1.2.2.4.5 $
 *     	   $Date: 2014/06/13 08:44:13 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WOPutaway_LP.js,v $
 * Revision 1.1.2.2.4.5  2014/06/13 08:44:13  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2.4.4  2014/05/30 00:34:25  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.3  2013/06/18 14:58:13  skreddy
 * CASE201112/CR201113/LOG201121
 * added filter
 *
 * Revision 1.1.2.2.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.2  2012/12/03 16:04:35  skreddy
 * CASE201112/CR201113/LOG201121
 * Restrict to Approve the WO before confirm
 *
 * Revision 1.1.2.1  2012/11/23 09:40:18  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 * Revision 1.7.4.4.4.2  2012/09/27 10:58:09  grao
 * CASE201112/CR201113/LOG201121
 *
 *
 *
 *****************************************************************************/

function WOPutawayLP(request, response){
	if (request.getMethod() == 'GET') 
	{
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('ERROR', 'getOptedField', getOptedField);

		var getLP;
		var noofrecords=0;
		var lpcount = request.getParameter('custparam_lpcount');
		if (lpcount == null)
			lpcount = 0;

		var filters = new Array();

		//			filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', 'PUTW');
		//			filters[1] = new nlobjSearchFilter('custrecord_status_flag', null, 'is', 'L');
		filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '5');
		filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lpno');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			var searchresult = searchresults[i];
			nlapiLogExecution('ERROR', 'searchresults.length', searchresults.length);
			noofrecords = parseInt(searchresults.length);
			getLP = searchresult.getValue(columns[0]);

		}
		nlapiLogExecution('ERROR', 'Retrieved LP', getLP);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4,st5;

		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "PLACA"; 
			st2 = "INGRESAR / ESCANEAR DEL N&#218;MERO DE MATR&#205;CULA";
			st3 = "";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";


		}
		else
		{
			st0 = "";
			st1 = "LP"; 
			st2 = "ENTER/SCAN LICENSE";
			st3 = "PLATE NUMBER";
			st4 = "SEND";
			st5 = "PREV";

		}




		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlp').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " : <label>" + getLP + "</label>";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnlpcount' value=" + lpcount + ">";
		//html = html + "				<input type='hidden' name='hdnRecordCount' value=" + noofrecords + "></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label id='LPNo'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " </td></tr>";
		html = html + "				<td align = 'left'>" + st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp'; type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					"+ st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'SearchResults ', 'Length is not null');

		// This variable is to hold the LP entered.
		var getLPNo = request.getParameter('enterlp');
		nlapiLogExecution('ERROR', 'getLPNo', getLPNo);
		var getLPCount = request.getParameter('hdnlpcount');
		nlapiLogExecution('ERROR', 'getLPCount', getLPCount);

		var filters = new Array();
		//		filters[0] = new nlobjSearchFilter('custrecord_status_flag', null, 'is', 'L');
		//filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
		filters.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '5'));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lpno');
		columns[1] = new nlobjSearchColumn('custrecord_wms_status_flag');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);

		//var getRecordCount = request.getParameter('hdnRecordCount');
		var st6;
		if( getLanguage == 'es_ES')
		{
			st6 = "LP NO V&#193;LIDO";
		}
		else
		{
			st6 = "INVALID LP";
		}



		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		POarray["custparam_error"] = st6;
		POarray["custparam_screenno"] = 'WOPutScanLp';
		var LPNoArray = new Array();
		var lpListString;

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di', false, POarray);
		}
		else {
			try { 
				var vhdnNumber = 0;
				var sysRulefilters = new Array();

				sysRulefilters[0] = new nlobjSearchFilter('custrecord_ebiztasktype', null, 'is','2');

				var sysRulecolumns = new Array();
				sysRulecolumns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');

				var sysRulesearchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, sysRulefilters, sysRulecolumns);

				if (sysRulesearchresults != null){
					var vhdnNumber = sysRulesearchresults[0].getValue('custrecord_ebizrulevalue');
					nlapiLogExecution('ERROR', 'sysRulesearchresults vhdnNumber', vhdnNumber);
				}
				if (getLPNo != null && getLPNo != "") {
					if (searchresults != null){
						//var vhdnNumber = "3";
						/*if (request.getParameter('custparam_lpno') != null){
							LPNoArray = request.getParameter('custparam_lpno').split(',');
							for (var t = 0; t < LPNoArray.length; t++){
								if (getLPNo == LPNoArray[t]){
									POarray["custparam_error"] = "LP No. Already Scanned";
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									return;
								}
							}
						}*/

						var WMSStatus = searchresults[0].getValue('custrecord_wms_status_flag');
						nlapiLogExecution('ERROR', 'WMSStatus', WMSStatus);
						if(WMSStatus == 3)
						{
							POarray["custparam_error"] = "PUTAWAY ALREADY COMPLETED FOR THIS WO";
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						}
						else
						{
							if (request.getParameter('custparam_lpno') == null || request.getParameter('custparam_lpno') == ""){	
								POarray["custparam_lpno"] = getLPNo; 
							}
							else{
								POarray["custparam_lpno"] = request.getParameter('custparam_lpno') + ',' + getLPNo;
							}
							lpListString = POarray["custparam_lpno"];
							POarray["custparam_lpcount"] = parseFloat(request.getParameter('hdnlpcount')) + 1;
							nlapiLogExecution('ERROR', 'lpcount', POarray["custparam_lpcount"]);

							LPNoArray.push(getLPNo);
							/*if ((parseFloat(getLPCount) + 1) < parseFloat(vhdnNumber)){
							nlapiLogExecution('ERROR', 'Scanning LP No.');
							response.sendRedirect('SUITELET', 'customscript_rf_putaway_lp', 'customdeploy_rf_putaway_lp_di', false, POarray);
							return;
						}
							 */
							var getLPId = searchresults[0].getId();
							POarray["custparam_recordcount"] = parseInt(searchresults.length);
							nlapiLogExecution('ERROR', 'get LP Internal Id', getLPId);

							// Load a record into a variable from opentask table for the LP# entered
							var PORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getLPId);

							POarray["custparam_lpno"] = PORec.getFieldValue('custrecord_lpno');
							POarray["custparam_quantity"] = PORec.getFieldValue('custrecord_expe_qty');
							POarray["custparam_location"] = PORec.getFieldValue('custrecord_actbeginloc');
							nlapiLogExecution('ERROR', 'Location', POarray["custparam_location"]);
							POarray["custparam_item"] = PORec.getFieldValue('custrecord_sku');
							POarray["custparam_itemtext"] = PORec.getFieldText('custrecord_sku');
							POarray["custparam_itemDescription"] = PORec.getFieldValue('custrecord_skudesc');
							POarray["custparam_itemstatus"] = PORec.getFieldValue('custrecord_sku_status');
							POarray["custparam_lpNumbersArray"] = lpListString;
							POarray["custparam_enteredLPNo"] = getLPNo;
							POarray["custparam_batchno"] = PORec.getFieldValue('custrecord_batch_no');
							POarray["custparam_ebizordno"] = PORec.getFieldValue('custrecord_ebiz_order_no');
							POarray["custparam_recordid"] = PORec.getId();
							POarray["custparam_whlocation"] = PORec.getFieldValue('custrecord_wms_location');
							POarray["custparam_woname"] =PORec.getFieldValue('name');


							if(POarray["custparam_location"] !=null && POarray["custparam_location"] !='' )
							{
							var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', POarray["custparam_location"]);

							POarray["custparam_beginlocation"] = BinLocationRec.getFieldValue('name');
							nlapiLogExecution('ERROR', 'Location Name is', POarray["custparam_beginlocation"]);
							}	

							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putconfirm_loc', 'customdeploy_ebiz_rf_wo_putconfirm_loc_d', false, POarray);
						}
					}
					else {
						//POarray["custparam_error"] = "BUILD CONFIRM";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('ERROR', 'SearchResults ', 'Length is null');
					}
				}
				else {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('ERROR', 'getLPNo is null ', getLPNo);
				}
			} 
			catch (e) {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('ERROR', 'In error catch block ', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}
	}
}
