/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplen_PickingStageLocation.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2015/01/02 15:04:44 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplen_PickingStageLocation.js,v $
 * Revision 1.1.2.1  2015/01/02 15:04:44  skreddy
 * Case# 201411349
 * Two step replen process
 *
 * Revision 1.12.2.42.4.11.2.21.2.1  2014/09/22 07:31:15  gkalla
 *
 *
 *****************************************************************************/

function TwoStepReplenPickingStageLocation(request, response){
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('DEBUG', 'Into Page Load');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR AL MONTAJE";
			st2 = "N&#218;MERO DE EMPAQUE:";
			st3 = "UBICACI&#211;N:";
			st4 = "INGRESAR / ESCANEAR ETAPA:";
			st5 = "CONF";
		}
		else
		{
			st1 = "GO TO STAGING ";
			st2 = "CARTON NO : ";
			st3 = "LOCATION : ";
			st4 = "ENTER/SCAN STAGE :";
			st5 = "CONF";
		}

		var reportNo = request.getParameter('custparam_repno');
		var beginLoc = request.getParameter('custparam_repbegloc');
		var sku = request.getParameter('custparam_repsku');
		var skuNo = request.getParameter('custparam_repskuno');
		var expQty = request.getParameter('custparam_repexpqty');
		var repInternalId = request.getParameter('custparam_repid');
		var clustNo = request.getParameter('custparam_clustno');
		var whlocation = request.getParameter('custparam_whlocation');
		var actEndLocationId = request.getParameter('custparam_reppicklocno');
		var actEndLocation = request.getParameter('custparam_reppickloc');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var nextlocation=request.getParameter('custparam_nextlocation');
		var nextqty=request.getParameter('custparam_nextqty');
		var batchno=request.getParameter('custparam_batchno');
		var fromlpno=request.getParameter('custparam_fromlpno');
		var tolpno=request.getParameter('custparam_tolpno');
		var nextitem=request.getParameter('custparam_nextitem');
		var nextitemno=request.getParameter('custparam_nextitemno');
		var taskpriority = request.getParameter('custparam_entertaskpriority');
		var getreportNo=request.getParameter('custparam_enterrepno');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');
		var getNumber = request.getParameter('custparam_number');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var enterfromloc = request.getParameter('custparam_enterfromloc');
		var getLanguage = request.getParameter('custparam_language');
		var cartlp = request.getParameter('custparam_cartlpno');
		var vzone = request.getParameter('custparam_zoneno');
		//case 20124235 start
		var Itemdescription='';
		//end
	
		var stgOutDirection="OUB";
		

			var stgLocation = GetPickStageLocation(skuNo, '', whlocation, '',
					stgOutDirection,'','',null,'','');


			nlapiLogExecution('DEBUG', 'After Getting StagingLocation in page load::',stgLocation);
			var FetchBinLocation;
			if(stgLocation!=null && stgLocation!=-1)
			{
				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('name');
				filtersLocGroup.push(new nlobjSearchFilter('internalid', null, 'is',stgLocation));
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				if(searchresultsloc != null && searchresultsloc != '')
					FetchBinLocation=searchresultsloc[0].getValue('name');
			}

			nlapiLogExecution('DEBUG', 'After Getting StagingLocation Name in page load::',FetchBinLocation);
		

		var stageScanRequiredRuleValue = getSystemRuleValue('STAGESCANREQD');

		//upto to here

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterstagelocation').focus();";        

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"</td></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" " + FetchBinLocation;
		html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + beginLoc + ">";
		html = html + "				<input type='hidden' name='hdnSku' value='" + sku + "'>";
		html = html + "				<input type='hidden' name='hdnExpQty' value=" + expQty + ">";
		html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
		html = html + "				<input type='hidden' name='hdnid' value=" + repInternalId + ">";
		html = html + "				<input type='hidden' name='hdnclustno' value=" + clustNo + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
		html = html + "				<input type='hidden' name='hdnactendlocationid' value=" + actEndLocationId + ">";
		html = html + "				<input type='hidden' name='hdnactendlocation' value=" + actEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
		html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
		html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
		html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + batchno + ">";
		html = html + "				<input type='hidden' name='hdnnitem' value='" + nextitem + "'>";
		html = html + "				<input type='hidden' name='hdnnextitemno' value=" + nextitemno + ">";
		html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
		html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
		html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
		html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
		html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
		html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
		html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
		html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
		html = html + "				<input type='hidden' name='hdnenterfromloc' value=" + enterfromloc + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnCartlpno' value=" + cartlp + ">";
		html = html + "				<input type='hidden' name='hdnvzone' value=" + vzone + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
	
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>"+st4;//ENTER/SCAN STAGE :
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterstagelocation' id='enterstagelocation' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
	
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st5 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>"+ st5 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterstagelocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		try
		{
			nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

			var getLanguage = request.getParameter('hdngetLanguage');

			var st6,st7;

			if( getLanguage == 'es_ES')
			{
				st6 = "LOCALIZACI&#211;N ETAPA NO V&#193;LIDO";
				st7 = "OPCI#211;N V&#193;LIDA";

			}
			else
			{
				//Case # 20126364
				st6 = "ENTER VALID STAGE LOCATION";
				st7 = "INVALID OPTION";

			}
			nlapiLogExecution('DEBUG', 'Pick Type', request.getParameter('hdnpicktype'));


			var optedEvent = request.getParameter('cmdSend');
			nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);

			var vZoneId=request.getParameter('hdnvzone');
			var Reparray =new Array();		
			Reparray["custparam_repno"] = request.getParameter('hdnNo');
			Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');
			Reparray["custparam_repsku"] = request.getParameter('custparam_repskuno');
			Reparray["custparam_torepexpqty"] = request.getParameter('hdnExpQty');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_repid"] = request.getParameter('hdnid');
			Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
			Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
			Reparray["custparam_error"] =st6;
			Reparray["custparam_screenno"] = '2stepSTG';
//			Reparray["custparam_clustno"] = clusterNo;	
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnactendlocationid');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnactendlocation');
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnBeginLocationId');
			Reparray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
			Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
			Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
			Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_batchno"] = request.getParameter('hdnbatchno');
			Reparray["custparam_nextitem"] = request.getParameter('hdnnitem');
			Reparray["custparam_nextitemno"] = request.getParameter('hdnnextitemno');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnExpQty');
			Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');
			Reparray["custparam_number"] = request.getParameter('hdngetNumber');
			Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
			Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
			Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
			Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
			Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
			Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
			Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');
			Reparray["custparam_cartlpno"] = request.getParameter('hdnCartlpno');
			Reparray["custparam_zoneno"] = vZoneId;	

			var reportNo =request.getParameter('hdnNo');
			if (optedEvent == 'ENT') 
			{		
				var	stagelocation=request.getParameter('enterstagelocation');

				var hdnstagelocation=request.getParameter('vhdnstageloctext');

				nlapiLogExecution('DEBUG', 'Scanned Stage Location', stagelocation);
				nlapiLogExecution('DEBUG', 'Actual Stage Location', hdnstagelocation);
				if(stagelocation!=hdnstagelocation)
				{
					var resArray=ValidateStageLocation(request.getParameter('custparam_repskuno'),'',request.getParameter('hdnwhlocation'),'',
							"OUB",'','',stagelocation);

					var Isvalidestage=resArray[0];
					if(Isvalidestage==true)
					{
						vstageLoc=resArray[1];
					}
					else
					{
						Reparray["custparam_error"] = st6;//'INVALID STAGE LOCATION';
						nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Stage Location');
						Reparray["custparam_screenno"] = '2stepSTG';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}
				}
				//end of code as on 170812.



				var taskfilters = new Array();

				taskfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '8'));//Task Type - PICK
				taskfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['24']));// Pick Confirm and Pack Complete

				if(reportNo!=null && reportNo!='' && reportNo!='null')
					taskfilters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
				if(vZoneId !=null && vZoneId !='' && vZoneId !='null')
				{
					nlapiLogExecution('ERROR','vZoneId',vZoneId);
					taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));

				}

				var taskcolumns = new Array();
				taskcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
				taskcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				taskcolumns[2] = new nlobjSearchColumn('custrecord_invref_no');
				taskcolumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
				taskcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				taskcolumns[5] = new nlobjSearchColumn('name');
				taskcolumns[6] = new nlobjSearchColumn('custrecord_wms_location');
				taskcolumns[0].setSort();

				var tasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskfilters, taskcolumns);
				var voldcontainer ='';
				if(tasksearchresults!=null)
				{
					for (var i = 0; i < tasksearchresults.length; i++)
					{
						ContainerLPNo = tasksearchresults[i].getValue('custrecord_container_lp_no');
						Item = tasksearchresults[i].getValue('custrecord_ebiz_sku_no');
						var salesorderintrid = tasksearchresults[i].getValue('custrecord_ebiz_order_no');
						vDoname = tasksearchresults[i].getValue('name');
						var WHLocation = tasksearchresults[i].getValue('custrecord_wms_location');
						var Item =	tasksearchresults[i].getValue('custrecord_ebiz_sku_no');

						nlapiLogExecution('DEBUG', 'voldcontainer', voldcontainer);
						nlapiLogExecution('DEBUG', 'ContainerLPNo', ContainerLPNo);
						nlapiLogExecution('DEBUG', 'salesorderintrid', salesorderintrid);


						var fields = new Array();
						fields[0]=['custrecord_ebiz_stage_location'];

						var values = new Array();
						values[0]= vstageLoc;

						nlapiSubmitField('customrecord_ebiznet_trn_opentask',tasksearchresults[i].getId(),fields,values);

						if(voldcontainer!=ContainerLPNo)
						{
							CreateSTGMRecord(ContainerLPNo, '', salesorderintrid, vDoname, null, WHLocation, '', 
									Item, "OUB", '', '',vstageLoc,'',salesorderintrid,null);

							voldcontainer=ContainerLPNo;
						}

					}

				}	
				response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenreportno', 'customdeploy_ebiz_twostepreplenreportno', false, Reparray);
				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			}
			else
			{
				Reparray["custparam_error"] = st7;//'INVALID OPTION';
				nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Option');
				Reparray["custparam_screenno"] = '2stepSTG';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			}
		}
		catch(exp)
		{
			Reparray["custparam_error"] = st7;//'INVALID OPTION';
			nlapiLogExecution('DEBUG', 'Exception: ', exp);
			Reparray["custparam_screenno"] = '2stepSTG';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
		}
	}
}


function ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo,SerialNo,vBatchno){
	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');

	nlapiLogExecution('DEBUG', "Line Count" + lineCnt);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	/*    
     for (var k = 1; k <= lineCnt; k++)
     {
     var chkVal = request.getLineItemValue('custpage_items', 'custpage_so', k);
     var SONo
     var ItemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
     var itemno = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
     var Lineno = request.getLineItemValue('custpage_items', 'custpage_lineno', k);
     var pickqty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
     var vinvrefno = request.getLineItemValue('custpage_items', 'custpage_invrefno', k);

     var WaveNo = request.getParameter('hdnWaveNo');
     var RecordInternalId = request.getParameter('hdnRecordInternalId');
     var ContainerLPNo = request.getParameter('hdnContainerLpNo');
     var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
     var BeginLocation = request.getParameter('hdnBeginLocationId');
     var Item = request.getParameter('hdnItem');
     var ItemDescription = request.getParameter('hdnItemDescription');
     var ItemInternalId = request.getParameter('hdnItemInternalId');
     var DoLineId = request.getParameter('hdnDOLineId');	//
     var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
     var BeginLocationName = request.getParameter('hdnBeginBinLocation');
     var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
     var EndLocation = getEnteredLocation;

     //        var Recid = request.getLineItemValue('custpage_items', 'custpage_recid', k);
     //        nlapiLogExecution('DEBUG', "Recid " + Recid)
	 */
	//updating allocqty in opentask
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
	OpenTaskTransaction.setFieldValue('custrecord_act_qty', parseFloat(ExpectedQuantity).toFixed(4));
	OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
	OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);

	nlapiLogExecution('DEBUG', 'Done PICK Record Insertion :', 'Success');

	//Updating DO 

	var DOLine = nlapiLoadRecord('customrecord_ebiznet_ordline', DoLineId);
	DOLine.setFieldValue('custrecord_linestatus_flag', '1'); //Statusflag='C'
	DOLine.setFieldValue('custrecord_pickqty', parseFloat(ExpectedQuantity).toFixed(4));
	var SalesOrderInternalId = DOLine.getFieldValue('name');

	try {
		nlapiLogExecution('DEBUG', 'Main Sales Order Internal Id', SalesOrderInternalId);
		var TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');

		var SOLength = TransformRec.getLineItemCount('item');
		nlapiLogExecution('DEBUG', "SO Length", SOLength);

		/*
         for (var j = 0; j < SOLength; j++)
         {
         nlapiLogExecution('DEBUG', "Get Line Value", TransformRec.getLineItemValue('item', 'line', j + 1));
         TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
         }
         TransformRec.setLineItemValue('item', 'itemfulfillment', OrderLineNo, 'T');
         TransformRec.setLineItemValue('item', 'quantity', OrderLineNo, ExpectedQuantity);
         var TransformRecId = nlapiSubmitRecord(TransformRec, true);
		 */
		for (var j = 1; j <= SOLength; j++) {
			nlapiLogExecution('DEBUG', "Get Line Value" + TransformRec.getLineItemValue('item', 'line', j ));

			var item_id = TransformRec.getLineItemValue('item', 'item', j);
			var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);

			if (itemLineNo == OrderLineNo) {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
				TransformRec.setCurrentLineItemValue('item', 'quantity', ExpectedQuantity);
				//TransformRec.setCurrentLineItemValue('item', 'location', 1);
				var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
				var serialsspaces="";
				if (itemSubtype.recordType == 'serializedinventoryitem') {

					arrSerial= SerialNo.split(',');
					nlapiLogExecution('DEBUG', "arrSerial", arrSerial);	
					for (var n = 0; n < arrSerial.length; n++) {
						if (n == 0) {
							serialsspaces = arrSerial[n];
						}
						else
						{
							serialsspaces += " " +arrSerial[n];
						}
					}					
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', serialsspaces);
				}
				//vBatchno
				if (itemSubtype.recordType == 'lotnumberedinventoryitem'){
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', vBatchno+'('+ExpectedQuantity+')');
				}


				TransformRec.commitLineItem('item');
			}
			else {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');
				TransformRec.commitLineItem('item');

			}
			//TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
		}
		//			invtRec.setFieldValue('custrecord_ebiz_inv_loc', '1');//WH Location.

		//			TransformRec.setFieldValue('custbody_ebiz_fulfillmentord',vdono) 
		var TransformRecId = nlapiSubmitRecord(TransformRec, true);
	} 

	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}

	///Updating qty,qoh in inventory
	nlapiLogExecution('DEBUG', 'Invoice Ref No', InvoiceRefNo);
	var InventoryTranaction = nlapiLoadRecord('customrecord_ebiznet_createinv', InvoiceRefNo);

	var InvAllocQty = InventoryTranaction.getFieldValue('custrecord_ebiz_alloc_qty');
	var InvQOH = InventoryTranaction.getFieldValue('custrecord_ebiz_qoh');

	if (isNaN(InvAllocQty)) {
		InvAllocQty = 0;
	}
	InventoryTranaction.setFieldValue('custrecord_ebiz_qoh', (parseFloat(InvQOH) - parseFloat(ExpectedQuantity)).toFixed(4));
	InventoryTranaction.setFieldValue('custrecord_ebiz_alloc_qty', (parseFloat(InvAllocQty) - parseFloat(ExpectedQuantity)).toFixed(4));
	var InventoryTranactionId = nlapiSubmitRecord(InventoryTranaction, false, true);
	OpenTaskTransaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
	nlapiSubmitRecord(OpenTaskTransaction, true);
	nlapiSubmitRecord(DOLine, true);

	//    }

	//    var form = nlapiCreateForm('Pick  Confirmation');
	//    form.addField('custpage_label', 'label', '<h2>Pick Confirmation Successful</h2>');

	//    response.writePage(form);
}

/**
 * 
 * @param vContLp
 * @param vebizWaveNo
 * @param vebizOrdNo
 * @param vDoname
 * @param vCompany
 * @param vlocation
 * @param vdono
 * @param Item
 * @param stgDirection
 * @param vCarrier
 * @param vSite
 */
function CreateSTGMRecord(vContLp, vebizWaveNo, vebizOrdNo, vDoname, vCompany,vlocation,vdono,
		Item,stgDirection,vCarrier,vSite,stageLocation,vClusterNo,salesorderintrid,vondemandstage) {

	nlapiLogExecution('DEBUG', 'into CreateSTGMRecord');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,
			'is', '9')); //STATUS.OUTBOUND.PICK_GENERATED('G') 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',
			vebizWaveNo));
	if(vContLp!=null && vContLp!='' && vContLp!='null')
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,
				'is', vContLp));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'isempty'));
	}

	if(vClusterNo!=null && vClusterNo!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null,
				'is', vClusterNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',
			null, filters, columns);
	if (searchresults == null || vondemandstage=='Y') {
		if(stageLocation==null || stageLocation=='')
			stageLocation = GetPickStageLocation(Item, vCarrier, vSite, vCompany,
					stgDirection,null,null,null,null,null);
		var STGMtask = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'stageLocation',stageLocation);
		nlapiLogExecution('DEBUG', 'Name',vDoname);
		nlapiLogExecution('DEBUG', 'DO NO',vdono);
		if(vDoname==null || vDoname=='')
			vDoname = vdono;
		STGMtask.setFieldValue('name', vDoname);
		STGMtask.setFieldValue('custrecord_ebiz_concept_po', vebizOrdNo);
		STGMtask.setFieldValue('custrecord_tasktype', '13');//STATUS.OUTBOUND.ORDERLINE_PARTIALLY_SHIPPED('PS')
		STGMtask.setFieldValue('custrecord_wms_location', vlocation);
		STGMtask.setFieldValue('custrecord_actbeginloc', stageLocation);
		STGMtask.setFieldValue('custrecord_actendloc', stageLocation);
		STGMtask.setFieldValue('custrecord_container_lp_no', vContLp);
		STGMtask.setFieldValue('custrecord_ebiz_cntrl_no', vdono);
		STGMtask.setFieldValue('custrecord_ebiz_receipt_no', vdono);
		STGMtask.setFieldValue('custrecord_ebiz_wave_no', vebizWaveNo);
		STGMtask.setFieldValue('custrecordact_begin_date', DateStamp());
		STGMtask.setFieldValue('custrecordact_end_date', DateStamp());
		STGMtask.setFieldValue('custrecord_actualendtime', TimeStamp());
		STGMtask.setFieldValue('custrecord_actualbegintime', TimeStamp());
		STGMtask.setFieldValue('custrecord_sku', Item);
		STGMtask.setFieldValue('custrecord_ebiz_sku_no', Item);
		STGMtask.setFieldValue('custrecord_ebiz_order_no', salesorderintrid);
		//STGMtask.setFieldValue('custrecord_comp_id', vCompany);
		var currentContext = nlapiGetContext();
		var currentUserID = currentContext.getUser();
		STGMtask.setFieldValue('custrecord_ebizuser', currentUserID);
		var retSubmitLP= nlapiSubmitRecord(STGMtask);
		nlapiLogExecution('DEBUG', 'out of CreateSTGMRecord');

		nlapiLogExecution('DEBUG', 'updating outbound inventory with satge location',stageLocation);
		// below code is commented beacuse outbound inventory is not creating earily screens
		//updateStageInventorywithStageLoc(vContLp,stageLocation);
	}
}


function updateStageInventorywithStageLoc(contlpno,stageLocation)
{
	nlapiLogExecution('DEBUG', 'into updateStageInventorywithStageLoc');
	nlapiLogExecution('DEBUG', 'contlpno',contlpno);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null,'is', '18')); //FLAG.INVENTORY.OUTBOUND('O') 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is', contlpno));
	filters.push(new nlobjSearchFilter('custrecord_invttasktype', null,'is', '3')); ////Task Type - PICK

	var columns = new Array();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null, filters, columns);
	if (invtsearchresults != null && invtsearchresults != '' && invtsearchresults.length > 0) 
	{
		for (var i = 0; i < invtsearchresults.length; i++)
		{
			var invtid=invtsearchresults[i].getId();

			nlapiLogExecution('DEBUG', 'Deleting Outbound Inventory....',invtid);
			nlapiSubmitField('customrecord_ebiznet_createinv', invtid, 'custrecord_ebiz_inv_binloc', stageLocation);

//			var stgmInvtRec = nlapiLoadRecord('customrecord_ebiznet_createinv',invtid);
//			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', stageLocation);
//			var invtrecid = nlapiSubmitRecord(stgmInvtRec, false, true);

			//nlapiLogExecution('DEBUG', 'updateStageInventorywithStageLoc is done',invtrecid);
		}		
	}

	nlapiLogExecution('DEBUG', 'out of updateStageInventorywithStageLoc');
}

/**
 * 
 * @param invtrecid
 * @param vContLp
 * @param Item
 * @param vCarrier
 * @param vSite
 * @param vCompany
 * @param stgDirection
 * @param vqty
 */
function CreateSTGInvtRecord(invtrecid, vContLp, Item, vCarrier, vSite,
		vCompany, stgDirection, vqty,vstageLoc) {
	try {
		nlapiLogExecution('DEBUG', 'into CreateSTGInvtRecord');
		if(vstageLoc==null || vstageLoc=='')
			vstageLoc = GetPickStageLocation(Item, vCarrier, vSite, vCompany,
					stgDirection,null,null,null,null,null);
		nlapiLogExecution('DEBUG', 'vstagLoc', vstageLoc);
		if (vstageLoc != null && vstageLoc != "" && vstageLoc != "-1") {

			var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',
					invtrecid);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', vstageLoc);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
			stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
			stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
			stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
			stgmInvtRec.setFieldValue('custrecord_invttasktype', '3');//STATUS.INBOUND.PUTAWAY_COMPLETE('S')
			stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');			
			nlapiSubmitRecord(stgmInvtRec, false, true);

			nlapiLogExecution('DEBUG', 'out of CreateSTGInvtRecord');
		}
	} catch (e) {
		if (e instanceof nlobjError)
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		else
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}
}


function GetStageLocationFromSTGM(fono,waveno)
{
	nlapiLogExecution('DEBUG', 'Into GetStageLocationFromSTGM');
	nlapiLogExecution('DEBUG', 'fono', fono);
	nlapiLogExecution('DEBUG', 'waveno', waveno);

	var SOSearchResults = new Array();

	var SOFilters = new Array();

	if(fono!=null && fono!='')
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', fono));
	if(waveno!=null && waveno!='')
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveno));
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [13])); // Task Type - STGM

	var SOColumns = new Array();

	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('internalid'));	
	SOColumns[1].setSort(true);

	SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	nlapiLogExecution('DEBUG', 'Out of GetStageLocationFromSTGM',SOSearchResults);

	return SOSearchResults;	
}


function GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,customizetype,StageBinInternalId,ordertype,vcustomer){

	nlapiLogExecution('DEBUG', 'into GetPickStageLocation', Item);
	nlapiLogExecution('DEBUG', 'Item', Item);
	nlapiLogExecution('DEBUG', 'vSite', vSite);
	nlapiLogExecution('DEBUG', 'vCompany', vCompany);
	nlapiLogExecution('DEBUG', 'stgDirection', stgDirection);
	nlapiLogExecution('DEBUG', 'vCarrier', vCarrier);
	nlapiLogExecution('DEBUG', 'vCarrierType', vCarrierType);
	nlapiLogExecution('DEBUG', 'ordertype', ordertype);
	nlapiLogExecution('DEBUG', 'customizetype', customizetype);
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();

	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 = "";
	var ItemInfo3 = "";
	var columns = nlapiLookupField('item', Item, fields);
	if(columns!=null){
		ItemFamily = columns.custitem_item_family;
		ItemGroup = columns.custitem_item_group;
		ItemStatus = columns.custitem_ebizdefskustatus;
		ItemInfo1 = columns.custitem_item_info_1;
		ItemInfo2 = columns.custitem_item_info_2;
		ItemInfo3 = columns.custitem_item_info_3;
	}

	nlapiLogExecution('DEBUG', 'ItemFamily', ItemFamily);
	nlapiLogExecution('DEBUG', 'ItemGroup', ItemGroup);
	nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);
	nlapiLogExecution('DEBUG', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('DEBUG', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('DEBUG', 'ItemInfo3', ItemInfo3);

	nlapiLogExecution('DEBUG', 'Before Variable declaration');

	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}

	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
	if (vCarrier != null && vCarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

	} // code added from Boombah account on 25Feb13 by santosh
	else
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@']));
	// upto here
	if (vCarrierType != null && vCarrierType != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}
	if (ordertype != null && ordertype != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@', ordertype]));

	}
	else
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@']));
	if (customizetype != null && customizetype != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@', customizetype]));

	} // code added from Boombah account on 25Feb13 by santosh
	else
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@']));
	//upto here
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	//filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'anyof', ['8']));

	if (vcustomer != null && vcustomer != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_customer', null, 'anyof', ['@NONE@', vcustomer]));
	}
	
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	nlapiLogExecution('DEBUG', 'Before Searching of Stageing Rules');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	nlapiLogExecution('DEBUG', 'searchresults',searchresults);
	if (searchresults != null) {
		for (var i = 0; i < Math.min(10, searchresults.length); i++) {
			//GetPickStagLpCount                
			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			var vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');

			nlapiLogExecution('DEBUG', 'Stageing Location Value::', vstgLocation);
			nlapiLogExecution('DEBUG', 'Stageing Location Text::', vstgLocationText);


			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');

			nlapiLogExecution('DEBUG', 'vnoofFootPrints::', vnoofFootPrints);


			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetPickStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}


		

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

		
			nlapiLogExecution('DEBUG', 'LocGroup::', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');


				if (stgDirection == 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}
			
				filtersLocGroup.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('DEBUG', 'searchresultsloc::', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();


					nlapiLogExecution('DEBUG', 'vLocid::', vLocid);

					var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');

					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}
			}
		}
	}
	return -1;
}

/**
 * 
 * @param location
 * @param site
 * @param company
 * @returns {Number}
 */
function GetPickStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');

	var outLPCount = 0;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));

	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));

	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');

	return outLPCount;
}

function ValidateStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,vcustomizetype,stagelocation)
{
	try
	{
		nlapiLogExecution('DEBUG','Into ValidateStageLocation',stagelocation);
		nlapiLogExecution('DEBUG','WHLocation',vSite);
		var StageBinInternalIdArray=new Array();
		StageBinInternalIdArray[0]=false;
		var StageBinInternalId=fetchStageInternalId(stagelocation,vSite);
		if(StageBinInternalId!=null&&StageBinInternalId!="")
		{

			//var stagebinloc=GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,vcustomizetype,StageBinInternalId,null);
//			nlapiLogExecution('DEBUG','stagebinloc',stagebinloc);
//			if(stagebinloc!=-1)
//			{
			StageBinInternalIdArray[0]=true;
			StageBinInternalIdArray[1]=StageBinInternalId;
//			}
			/*var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', ['2', '3']));
			filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'noneof', ['14']));
			filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
			if(WHLocation!=null&&WHLocation!="")
				filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', WHLocation]));

			if (vCarrier != null && vCarrier != "") 
				filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

			if (vCarrierType != null && vCarrierType != "")
				filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
			columns[0].setSort();
			columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
			columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
			columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
			columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

			nlapiLogExecution('DEBUG', 'Before Searching of Stageing Rules');


			var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, null);
			if(searchresults!=null&&searchresults!="")
			{
				StageBinInternalIdArray[0]=true;
				StageBinInternalIdArray[1]=StageBinInternalId;
			}*/
		}
		nlapiLogExecution('DEBUG','StageBinInternalIdArray',StageBinInternalIdArray);
		return StageBinInternalIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in ValidateStageLocation',exp);
	}
}

function fetchStageInternalId(stageLoc,vSite)
{
	try
	{
		nlapiLogExecution('DEBUG','Into fetchStageInternalId',stageLoc);
		var FetchBinLocation="";
		var collocGroup = new Array();
		collocGroup[0] = new nlobjSearchColumn('internalid');

		var filtersLocGroup = new Array();
		filtersLocGroup.push(new nlobjSearchFilter('name', null, 'is',stageLoc));
		filtersLocGroup.push(new nlobjSearchFilter('custrecord_ebizlocationtype',null,'anyof',8));//8=Stage
		// Case# 20148566 starts
		filtersLocGroup.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		// Case# 20148566 ends
		if(vSite!=null&&vSite!="")
			filtersLocGroup.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null,'anyof',vSite));
		var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
		if(searchresultsloc != null && searchresultsloc != '')
			FetchBinLocation=searchresultsloc[0].getValue('internalid');
		nlapiLogExecution('DEBUG','FetchBinLocation',FetchBinLocation);
		return FetchBinLocation;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in fetchStageInternalId',exp);
	}
}

function getSystemRuleValue(RuleId)
{
	nlapiLogExecution('DEBUG', 'Into getSystemRuleValue... ', RuleId);

	var systemrulevalue='';

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', RuleId);
		filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				systemrulevalue = searchresults[0].getValue('custrecord_ebizrulevalue');
				return systemrulevalue;
			}
			else
				return systemrulevalue;
		}
		else
			return systemrulevalue;
	}
	catch (exp) 
	{
		nlapiLogExecution('DEBUG', 'Exception in GetSystemRules: ', exp);
		return systemrulevalue;
	}	
}
