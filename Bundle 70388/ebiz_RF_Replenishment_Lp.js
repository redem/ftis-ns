/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_Replenishment_Lp.js,v $
<<<<<<< ebiz_RF_Replenishment_Lp.js
 *     	   $Revision: 1.1.2.10.4.4.4.26.2.3 $
 *     	   $Date: 2015/11/14 11:48:22 $
 *     	   $Author: snimmakayala $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.1.2.10.4.4.4.26.2.3 $
 *     	   $Date: 2015/11/14 11:48:22 $
 *     	   $Author: snimmakayala $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.1.2.10.4.4.4.25
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Replenishment_Lp.js,v $
 * Revision 1.1.2.10.4.4.4.26.2.3  2015/11/14 11:48:22  snimmakayala
 * 201415660
 *
 * Revision 1.1.2.10.4.4.4.26.2.2  2015/11/10 16:41:27  snimmakayala
 * 201415474
 * 2015,2 Issues
 *
 * Revision 1.1.2.10.4.4.4.26.2.1  2015/11/03 15:33:12  grao
 * 2015.2 Issue Fixes 201413874
 *
 * Revision 1.1.2.10.4.4.4.26  2015/08/21 15:34:44  grao
 * 2015.2   issue fixes 201414080
 *
 * Revision 1.1.2.10.4.4.4.25  2015/08/21 11:01:00  nneelam
 * case# 201414102
 *
 * Revision 1.1.2.10.4.4.4.24  2015/07/03 15:40:44  skreddy
 * Case# 201412907 & 201412906
 * Signwarehouse SB issue fix
 *
 * Revision 1.1.2.10.4.4.4.23  2014/07/01 06:39:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Alphacommchanges
 *
 * Revision 1.1.2.10.4.4.4.22  2014/06/23 16:23:13  skavuri
 * Case# 20149021 New Ui Issue Fixed
 *
 * Revision 1.1.2.10.4.4.4.21  2014/06/13 09:35:49  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.10.4.4.4.20  2014/06/06 12:18:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.1.2.10.4.4.4.19  2014/05/30 00:34:23  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.10.4.4.4.18  2014/04/16 06:43:56  skreddy
 * case # 20147969
 * Sonic sb  issue fix
 *
 * Revision 1.1.2.10.4.4.4.17  2014/03/11 15:36:14  skavuri
 * Case# 20127635 issue fixed
 *
 * Revision 1.1.2.10.4.4.4.16  2013/11/20 20:45:48  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20125767
 * Replen Exceptions
 *
 * Revision 1.1.2.10.4.4.4.15  2013/11/14 15:41:55  skreddy
 * Case# 20125719 & 20125658
 * Afosa SB issue fix
 *
 * Revision 1.1.2.10.4.4.4.14  2013/11/14 14:34:57  rmukkera
 * Case # 20125742
 *
 * Revision 1.1.2.10.4.4.4.13  2013/11/12 06:40:59  skreddy
 * Case# 20125658 & 20125602
 * Afosa SB issue fix
 *
 * Revision 1.1.2.10.4.4.4.12  2013/11/01 13:47:29  schepuri
 * 20125477
 *
 * Revision 1.1.2.10.4.4.4.11  2013/10/25 16:57:37  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * MHPissue fixes
 *
 * Revision 1.1.2.10.4.4.4.7.2.5  2013/10/25 16:47:42  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * MHP issue fixes
 *
 * Revision 1.1.2.10.4.4.4.7.2.4  2013/10/22 14:26:24  schepuri
 * Case# 20125087
 * DC Dental SB issue fix
 *
 * Revision 1.1.2.10.4.4.4.7.2.3  2013/09/03 15:29:20  rmukkera
 * Case# 20123992
 *
 * Revision 1.1.2.10.4.4.4.7.2.2  2013/08/27 17:03:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdissuefixes
 * case#20123755
 *
 * Revision 1.1.2.10.4.4.4.7.2.1  2013/08/25 10:35:58  grao
 * SB Issue Fixes  20123992
 *
 * Revision 1.1.2.10.4.4.4.7  2013/06/04 07:45:40  rmukkera
 * Issue Fix For
 * RF replenishment screen displaying batch for ordinary Items
 *
 * Revision 1.1.2.10.4.4.4.6  2013/05/14 14:16:37  schepuri
 * RF Replen confirm based on filters
 *
 * Revision 1.1.2.10.4.4.4.5  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.10.4.4.4.4  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.2.10.4.4.4.3  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.10.4.4.4.2  2013/03/12 15:34:02  skreddy
 * CASE201112/CR201113/LOG201121
 * Qty Exception in  RF Replensiment Process
 *
 * Revision 1.1.2.10.4.4.4.1  2013/03/05 14:46:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.1.2.10.4.4  2012/12/14 07:42:53  spendyala
 * CASE201112/CR201113/LOG201121
 * moved from 2012.2 branch
 *
 * Revision 1.1.2.10.4.3  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.10.4.2  2012/10/11 14:50:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * REPLEN CHANGES
 *
 * Revision 1.1.2.10  2012/08/29 18:20:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * replens confirm
 *
 * Revision 1.1.2.9  2012/08/24 16:09:57  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Replen confirm
 *
 * Revision 1.1.2.8  2012/08/09 07:05:31  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Round turn in scanning other Items.
 *
 * Revision 1.1.2.7  2012/08/06 06:51:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Item Description.
 *
 * Revision 1.1.2.6  2012/07/12 14:47:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.1.2.5  2012/06/04 11:47:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * HUBPEN - REPLEN ISSUE FIXES
 *
 * Revision 1.1.2.4  2012/05/07 12:01:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.1.2.3  2012/04/20 11:19:27  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.1.2.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.1.2.1  2012/02/07 12:40:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Replen Issue Fixes
 *
 * Revision 1.3  2012/01/17 13:53:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.2  2012/01/09 13:15:23  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.1  2012/01/05 17:24:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.6  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *****************************************************************************/
function Replenishment_LP(request, response){
	if (request.getMethod() == 'GET') {
		var reportNo = request.getParameter('custparam_repno');
		var beginLoc = request.getParameter('custparam_repbegloc');
		var sku = request.getParameter('custparam_repsku');
		var skuNo = request.getParameter('custparam_repskuno');
		var expQty = request.getParameter('custparam_repexpqty');
		var repInternalId = request.getParameter('custparam_repid');
		var clustNo = request.getParameter('custparam_clustno');
		var whlocation = request.getParameter('custparam_whlocation');
		var actEndLocationId = request.getParameter('custparam_reppicklocno');
		var actEndLocation = request.getParameter('custparam_reppickloc');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var nextlocation=request.getParameter('custparam_nextlocation');
		var nextqty=request.getParameter('custparam_nextqty');
		var batchno=request.getParameter('custparam_batchno');
		var fromlpno=request.getParameter('custparam_fromlpno');
		var tolpno=request.getParameter('custparam_tolpno');
		var nextitem=request.getParameter('custparam_nextitem');
		var nextitemno=request.getParameter('custparam_nextitemno');
		var taskpriority = request.getParameter('custparam_entertaskpriority');
		var getreportNo=request.getParameter('custparam_enterrepno');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');
		var getNumber = request.getParameter('custparam_number');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var enterfromloc = request.getParameter('custparam_enterfromloc');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'fromlpno', fromlpno);
		nlapiLogExecution('ERROR', 'tolpno', tolpno);
		nlapiLogExecution('ERROR', 'expQty', expQty);
		nlapiLogExecution('ERROR', 'nextitem', nextitem);
		nlapiLogExecution('ERROR', 'nextitemno', nextitemno);
		nlapiLogExecution('ERROR', 'taskpriority in GET', taskpriority);
		nlapiLogExecution('ERROR', 'RecordCount in GET', RecordCount);
		nlapiLogExecution('ERROR', 'repInternalId of OT in GET', repInternalId);
		nlapiLogExecution('ERROR', 'entereditem', getitem);
		nlapiLogExecution('ERROR', 'entereditemid', getitemid);
		nlapiLogExecution('ERROR', 'enteredloc', getloc);
		nlapiLogExecution('ERROR', 'enteredlocid', getlocid);		
		nlapiLogExecution('ERROR', 'taskpriority', taskpriority);
		nlapiLogExecution('ERROR', 'getreportNo', getreportNo);
		nlapiLogExecution('ERROR', 'getNumber', getNumber);
		nlapiLogExecution('ERROR', 'enterfromloc', enterfromloc);
		nlapiLogExecution('ERROR', 'actEndLocationId', actEndLocationId);
		nlapiLogExecution('ERROR', 'replenType', replenType);

		var html=getLPScan(beginLoc, reportNo, sku, expQty, skuNo, repInternalId, 
				clustNo, whlocation, actEndLocationId, actEndLocation, beginLocId,RecordCount,fromlpno,tolpno,
				nextlocation,nextqty,batchno,nextitem,nextitemno,taskpriority,replenType,getitem,getitemid,getloc,
				getlocid,getreportNo,getNumber,enterfromloc,getLanguage);        
		response.write(html);
	}
	else
	{

		try {

			var Reparray = new Array();

			var optedEvent = request.getParameter('cmdPrevious');

			var fromlp = request.getParameter('enterreplenlp');		
			var hdnBeginLocation = request.getParameter('hdnBeginLocation');
			var hdnBeginLocationId = request.getParameter('hdnBeginLocationId');
			var hdnrepLpno=request.getParameter('hdnfromLpno');			
			var clusterNo = request.getParameter('hdnclustno');		
			var RecordCount = request.getParameter('hdnRecCount');			
			var ExpQty=request.getParameter('hdnExpQty');
			var ReportNo=request.getParameter('hdnNo');
			var recid=request.getParameter('hdnid');
			var sku=request.getParameter('hdnSku');
			var skuno=request.getParameter('hdnSkuNo');
			var replnQty=request.getParameter('enterreplenqty');
//			if(replnQty==null || replnQty=='')
//			replnQty=0;

			var remainingreplenqty = request.getParameter('enterremainingreplenqty');
			var reportNo = request.getParameter('custparam_repno');
			//case20125658 and 20125602 start:spanish conversion
			var getLanguage = request.getParameter('hdngetLanguage');
			Reparray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);
			//case20125658 and 20125602 end
			var st0,st1,st2,st3;
			if( getLanguage == 'es_ES' || getLanguage =='es_AR')
			{
				st0 = "LP NO V&#193;LIDA";
				st1 = "LP no coinciden";
				st2 = "LP ES NULO";
				st3 = "Cantidad entr&#243; no debe ser mayor que Replen genera Cantidad";


			}
			else
			{
				st0 = "INVALID LP";
				st1 = "LP DO NOT MATCH";
				st2 = "LP IS NULL ";
				st3 = "Quantity entered should not be greater than Replen generated qty";


			}
			if(request.getParameter('hdngetNumber')!= null && request.getParameter('hdngetNumber') != "")
				getNumber = request.getParameter('hdngetNumber');
			else
				getNumber=0;	

			if(fromlp==null || fromlp=='')
				fromlp=hdnrepLpno;

			nlapiLogExecution('ERROR', 'gethdnBeginLocation', hdnBeginLocation);
			nlapiLogExecution('ERROR', 'hdnBeginLocationId', hdnBeginLocationId);
			nlapiLogExecution('ERROR', 'recid', recid);
			nlapiLogExecution('ERROR', 'ReportNo', ReportNo);
			nlapiLogExecution('ERROR', 'sku', sku);
			nlapiLogExecution('ERROR', 'EnterQty', replnQty);
			nlapiLogExecution('ERROR', 'Remainingreplenqty', remainingreplenqty);

			Reparray["custparam_repno"] = request.getParameter('hdnNo');
			Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');
			//Reparray["custparam_repsku"] = request.getParameter('hdnSku');
			Reparray["custparam_repsku"] = request.getParameter('custparam_repskuno');
			Reparray["custparam_torepexpqty"] = request.getParameter('hdnExpQty');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_repid"] = request.getParameter('hdnid');
			Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
			Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
			Reparray["custparam_error"] =st0; //'INVALID LP';
			Reparray["custparam_screenno"] = 'R5';
			Reparray["custparam_clustno"] = clusterNo;	
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnactendlocationid');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnactendlocation');
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnBeginLocationId');
			//Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
			Reparray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
			Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
			Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
			Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_batchno"] = request.getParameter('hdnbatchno');
			Reparray["custparam_nextitem"] = request.getParameter('hdnnitem');
			Reparray["custparam_nextitemno"] = request.getParameter('hdnnextitemno');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnExpQty');
			Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');
			Reparray["custparam_number"] = request.getParameter('hdngetNumber');
			Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
			Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
			Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
			Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
			Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
			Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
			Reparray["custparam_remainingreplenqty"] = remainingreplenqty;
			Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

			var getitemid = request.getParameter('hdngetitemid');
			var getlocid = request.getParameter('hdngetlocid');
			var taskpriority = request.getParameter('hdntaskpriority');
			var taskpriority = request.getParameter('hdntaskpriority');
			var getreportNo = request.getParameter('hdngetreportno');
			var replenType = request.getParameter('hdnReplenType');
			var getBinLocationId=request.getParameter('hdngetlocid');

			nlapiLogExecution('ERROR','beginLocationId', Reparray["custparam_repbeglocId"]);
			nlapiLogExecution('ERROR','Reparray["custparam_nextitem"]', Reparray["custparam_nextitem"]);
			nlapiLogExecution('ERROR','Reparray["custparam_nextitemno"]', Reparray["custparam_nextitemno"]);
			nlapiLogExecution('ERROR','Reparray["custparam_nextlocation"]', Reparray["custparam_nextlocation"]);
			nlapiLogExecution('ERROR','Reparray["custparam_enterfromloc"]', Reparray["custparam_enterfromloc"]);


			if (optedEvent == 'F7') {
				nlapiLogExecution('ERROR', 'REPLENISHMENT LP F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);

			}
			else {

				if(replnQty==''|| replnQty==null)
				{
					Reparray["custparam_error"] = 'Please Enter Quantity';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					return;
				}

				if((isNaN(replnQty)) || (isNaN(remainingreplenqty)))
				{
					Reparray["custparam_error"] = 'Please Enter Valid Qty';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					return;
				}

				if(replnQty != null && replnQty!='')
				{
					if(parseInt(replnQty)<0)
					{
						Reparray["custparam_error"] = 'ENTERED QTY SHOULD BE GREATER THAN ZERO.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}

					if(parseInt(replnQty)>parseInt(ExpQty))
					{
						Reparray["custparam_error"] = 'ENTERED QTY SHOULD NOT BE GREATER THAN REPLEN QTY.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}
				}

				// case# 20127635 starts
				if(remainingreplenqty != null && remainingreplenqty!='')
				{

					if(parseInt(remainingreplenqty)<0)
					{
						Reparray["custparam_error"] = 'PLS ENTER VALID QTY';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}

					if(parseInt(remainingreplenqty)>parseInt(ExpQty))
					{
						Reparray["custparam_error"] = 'ENTERED QTY SHOULD NOT BE GREATER THAN REPLEN QTY.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}
				}
				var TotalEnteredQty=parseInt(replnQty) + parseInt(remainingreplenqty);
				if(TotalEnteredQty != null && TotalEnteredQty!='')
				{
					if(parseInt(TotalEnteredQty)>parseInt(ExpQty))
					{
						Reparray["custparam_error"] = 'ENTERED QTY SHOULD NOT BE GREATER THAN REPLEN QTY.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}
				}
				// case# 20127635 ends

				var systemRule=GetSystemRuleForReplen();


				if (fromlp != '' || systemRule=='N') {
					if (fromlp == hdnrepLpno || systemRule=='N') {
						nlapiLogExecution('ERROR', 'LP Found and Matched', fromlp);
						var opentaskcount=0;
						if(systemRule=='Y')
						{
							var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
							var InvtRef = transaction.getFieldValue('custrecord_invref_no');
							var vSkuStatus = transaction.getFieldValue('custrecord_sku_status');
							var tasktype = transaction.getFieldValue('custrecord_tasktype');

							nlapiLogExecution('ERROR', 'InvtRef', InvtRef);
							if(replnQty == null || replnQty =='' || isNaN(replnQty))
								replnQty=0;

							nlapiLogExecution('ERROR', 'replnQty',replnQty);
							nlapiLogExecution('ERROR', 'ExpQty',ExpQty);

							//if((parseInt(replnQty)< parseInt(ExpQty)) && replnQty !=0)
							if(parseInt(replnQty)< parseInt(ExpQty))
							{

								var RemQty = parseInt(ExpQty)-parseInt(replnQty);
								//DeallocateRemainingInvt(RemQty,InvtRef);
								transaction.setFieldValue('custrecord_act_qty', replnQty);
							}	
							else if((parseInt(replnQty) == parseInt(ExpQty)) && replnQty !=0)
							{
								transaction.setFieldValue('custrecord_act_qty', ExpQty);
							}
							else if((parseInt(replnQty)> parseInt(ExpQty)) && replnQty !=0)
							{
								nlapiLogExecution('ERROR', 'replen qty is greater than gen qty');
								Reparray["custparam_error"] = 'Quantity entered should not be greater than Replen generated qty';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
								return;
							}
							else
								transaction.setFieldValue('custrecord_act_qty', ExpQty);

							if(remainingreplenqty != null && remainingreplenqty !='')
								transaction.setFieldValue('custrecord_reversalqty', remainingreplenqty);//temporarily remaning qty is updated in this field,to fetch value for calculation in TO LP screen
//							else
//							transaction.setFieldValue('custrecord_reversalqty', '0');

							var result = nlapiSubmitRecord(transaction);

							opentaskcount=getOpenTasksCount(ReportNo,sku,getitemid,getlocid,taskpriority,getreportNo,replenType);
						}
						else
						{
							var filters = new Array();
							var vOrdFormat='';
							var vOrdFormat;
							if(ReportNo!=null && ReportNo!='')
							{
								nlapiLogExecution('ERROR','replenreportNo',ReportNo);
								filters.push(new nlobjSearchFilter('name', null, 'is', ReportNo));
								vOrdFormat='R';
							}
							if(request.getParameter('custparam_repskuno')!=null && request.getParameter('custparam_repskuno')!='')
							{
								nlapiLogExecution('ERROR','Replenitem',request.getParameter('custparam_repskuno'));
								filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', request.getParameter('custparam_repskuno')));
								vOrdFormat=vOrdFormat + 'I';
							}

							if(request.getParameter('custparam_repbeglocId')!=null && request.getParameter('custparam_repbeglocId')!='')
							{
								nlapiLogExecution('ERROR','Replenprimaryloc',request.getParameter('custparam_repbeglocId'));
								filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('custparam_repbeglocId')));
								vOrdFormat=vOrdFormat + 'L';
							}

							if(taskpriority!=null && taskpriority!='')
							{
								nlapiLogExecution('ERROR','Replenpriority',taskpriority);
								filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
								vOrdFormat=vOrdFormat + 'P';
							}
							if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='')
							{
								nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
								filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));

							}

							if(request.getParameter('hdnenterfromloc')!=null && request.getParameter('hdnenterfromloc')!='')
							{
								nlapiLogExecution('ERROR','custrecord_actbeginloc',request.getParameter('hdnenterfromloc'));
								filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('hdnenterfromloc')));

							}

							if(request.getParameter('hdnbatchno')!=null && request.getParameter('hdnbatchno')!='' && request.getParameter('hdnbatchno')!='null')
							{
								nlapiLogExecution('ERROR','batchno',request.getParameter('hdnbatchno'));
								filters.push(new nlobjSearchFilter('custrecord_batch_no', null, 'is', request.getParameter('hdnbatchno')));

							}

							filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
							filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
							filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty')); // case# 201416817
							//filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
							columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
							columns[2] = new nlobjSearchColumn('custrecord_sku');
							columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
							columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
							columns[5] = new nlobjSearchColumn('custrecord_wms_location');
							columns[6] = new nlobjSearchColumn('custrecord_actendloc');
							columns[7] = new nlobjSearchColumn('custrecord_lpno');
							columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
							columns[9] = new nlobjSearchColumn('name');	
							columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	


							columns[1].setSort(false);
							columns[2].setSort(false);
							columns[3].setSort(false);

							var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
							if(searchresults != null && searchresults != '')
							{
								nlapiLogExecution('DEBUG', 'searchresultststt1', searchresults.length);
								var tempqty=request.getParameter('enterreplenqty');

								var tcols=new Array();
								tcols[0]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
								var searchresultstemp = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, tcols);

								var QTY1=searchresultstemp[0].getValue('custrecord_expe_qty',null,'sum');

								if(parseInt(tempqty)>parseInt(QTY1))
								{
									nlapiLogExecution('ERROR', 'replen qty is greater than gen qty');
									Reparray["custparam_error"] = 'Quantity entered should not be greater than Replen generated qty';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
									return;
								}

								for(var m1=0;m1<searchresults.length;m1++)
								{

									var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresults[m1].getId());
									var InvtRef = transaction.getFieldValue('custrecord_invref_no');
									var vSkuStatus = transaction.getFieldValue('custrecord_sku_status');
									var tasktype = transaction.getFieldValue('custrecord_tasktype');
									var expqty = transaction.getFieldValue('custrecord_expe_qty');
									nlapiLogExecution('ERROR', 'InvtRef', InvtRef);

									if(parseInt(expqty)<=parseInt(tempqty))
									{
										nlapiLogExecution('ERROR', 'tempqty', tempqty);
										tempqty=parseInt(tempqty)-parseInt(expqty);
										transaction.setFieldValue('custrecord_act_qty', expqty);
									}
									else
									{
										nlapiLogExecution('ERROR', 'tempqty', tempqty);

										transaction.setFieldValue('custrecord_act_qty', tempqty.toString());
									}

									if(remainingreplenqty != null && remainingreplenqty !='')
										transaction.setFieldValue('custrecord_reversalqty', remainingreplenqty);//temporarily remaning qty is updated in this field,to fetch value for calculation in TO LP screen
//									else
//									transaction.setFieldValue('custrecord_reversalqty', '0');

									var result = nlapiSubmitRecord(transaction);

									nlapiLogExecution('ERROR', 'result', result);
									nlapiLogExecution('ERROR', 'expqty', expqty);
									nlapiLogExecution('ERROR', 'tempqty', tempqty);

								}
							}
						}
						//checking for serial items
						var rectype="";
						var serin="";
						var serout="";
						var vbatchlot="";

						nlapiLogExecution('ERROR', 'skuno', skuno);
						var ItemFilter=new Array();
						ItemFilter[0]=new nlobjSearchFilter("internalid",null,"anyof",skuno);

						var Itemcolumn=new Array();
						Itemcolumn[0]=new nlobjSearchColumn("type");
						Itemcolumn[1]=new nlobjSearchColumn("custitem_ebizserialin");
						Itemcolumn[2]=new nlobjSearchColumn("custitem_ebizserialout");
						Itemcolumn[3]=new nlobjSearchColumn("custitem_ebizbatchlot");

						var ItemSearchResults=nlapiSearchRecord("item",null,ItemFilter,Itemcolumn);
						if(ItemSearchResults!=null && ItemSearchResults!="")
						{

							rectype=ItemSearchResults[0].recordType;
							serin=ItemSearchResults[0].getValue("custitem_ebizserialin");
							serout=ItemSearchResults[0].getValue("custitem_ebizserialout");
							vbatchlot=ItemSearchResults[0].getValue("custitem_ebizbatchlot");
						}

						nlapiLogExecution('ERROR', 'rectype', rectype);
						nlapiLogExecution('ERROR', 'serout', serout);
						if (rectype == 'serializedinventoryitem' || serout == 'T') {

							Reparray["custparam_number"] = 0;
							Reparray["custparam_RecType"] = rectype;
							Reparray["custparam_SerOut"] = serout;
							Reparray["custparam_SerIn"] = serin;
							Reparray["custparam_totalqtyscan"] = replnQty;
							Reparray["custparam_systemrule"] = systemRule;
							response.sendRedirect('SUITELET', 'customscript_rf_replen_serialscan', 'customdeploy_rf_replen_serialscan_di', false, Reparray);
							return;
						}
						nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);
						opentaskcount=0;									
						var	opentasksearchresults=getConsolidatedopentaskcount(ReportNo,getitemid,getlocid,taskpriority,getreportNo,replenType,request);//Case# 20149021
						if(opentasksearchresults!=null && opentasksearchresults.length>0)
						{
							opentaskcount=opentasksearchresults.length;
						}
						nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);
						nlapiLogExecution('ERROR', 'hdnBeginLocation', hdnBeginLocation);
						nlapiLogExecution('ERROR', 'Reparray["custparam_nextlocation"]', Reparray["custparam_nextlocation"]);

						Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

						if(opentaskcount >= 1)
						{
							if(systemRule=='Y')
							{	
								if(hdnBeginLocation==Reparray["custparam_nextlocation"])
								{
									var vbeginloc = request.getParameter('hdnenterfromloc');

									var searchresults = getReplenTasks(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType,request,vbeginloc);

									nlapiLogExecution('ERROR', 'searchresults - Ns', searchresults);
									if(searchresults!=null && searchresults!='')
									{
										nlapiLogExecution('ERROR', 'searchresults Length - Ns', searchresults.length);

										nlapiLogExecution('ERROR', 'replenType - Ns', replenType);

										Reparray["custparam_replentype"] = replenType;
										Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
										Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc');
										Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku');
										Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
										Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
										Reparray["custparam_repid"] = searchresults[0].getId();
										Reparray["custparam_whlocation"] = searchresults[0].getValue('custrecord_wms_location');
										Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc');
										Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc');
										Reparray["custparam_fromlpno"] = searchresults[0].getValue('custrecord_from_lp_no');
										Reparray["custparam_tolpno"] = searchresults[0].getValue('custrecord_lpno');
										Reparray["custparam_repbatchno"] = searchresults[0].getValue('custrecord_batch_no');

										Reparray["custparam_noofrecords"] = searchresults.length;

										nlapiLogExecution('ERROR', 'hdnBeginLocation', hdnBeginLocation);
										nlapiLogExecution('ERROR', 'currBeginLocation', Reparray["custparam_nextlocation"]);

										if(hdnBeginLocation!=Reparray["custparam_repbegloc"])
										{
											response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
										}
										else
										{
											response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);
										}
									}
									else
									{
										response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
										return
									}

									//response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);

								}
								else
									response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);

							}
							else
							{
								var item=opentasksearchresults[0].getValue('custrecord_sku',null,'group');
								var binloc=opentasksearchresults[0].getText('custrecord_actbeginloc',null,'group');
								var ItemType = nlapiLookupField('item', skuno, ['recordType','custitem_ebizbatchlot']);
								if(item==skuno && hdnBeginLocation==binloc && (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ))
								{
									Reparray['custparam_repbatchno']=opentasksearchresults[0].getValue('custrecord_batch_no',null,'group');
									Reparray['custparam_repexpqty']=opentasksearchresults[0].getValue('custrecord_expe_qty',null,'sum');
//									if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
//									{
									response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);
									return;
//									}
								}
								else if(item!=skuno && hdnBeginLocation==binloc )
								{
									response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);
									return;
								}
								else
								{
									response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
									return;
								}
							}
						}
						else
						{
							Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

							var searchresults=getOpenTasksCountlp(ReportNo,sku,getitemid,getlocid,taskpriority,getreportNo,replenType);
							if (searchresults != null && searchresults!='') {

								var recNo=0;
								if(getNumber!=0)
									recNo=parseFloat(getNumber);
								nlapiLogExecution('ERROR', 'recNo in taskpriority ',recNo);	

								var SOSearchResult = searchresults[recNo];
								if(systemRule=='Y')
								{
									nlapiLogExecution('ERROR', 'searchresults', searchresults.length);
									nlapiLogExecution('ERROR', 'test2', 'test2');
									Reparray["custparam_repbegloc"] = SOSearchResult.getText('custrecord_actbeginloc');
									Reparray["custparam_repbeglocId"] = SOSearchResult.getValue('custrecord_actbeginloc');
									Reparray["custparam_repsku"] = SOSearchResult.getText('custrecord_sku');
									Reparray["custparam_repexpqty"] = SOSearchResult.getValue('custrecord_expe_qty');
									Reparray["custparam_repskuno"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
									Reparray["custparam_repid"] =SOSearchResult.getId();
									Reparray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
									Reparray["custparam_reppicklocno"] = SOSearchResult.getValue('custrecord_actendloc');
									Reparray["custparam_reppickloc"] = SOSearchResult.getText('custrecord_actendloc');
									Reparray["custparam_fromlpno"] = SOSearchResult.getValue('custrecord_from_lp_no');
									Reparray["custparam_tolpno"] = SOSearchResult.getValue('custrecord_lpno');
									Reparray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
									Reparray["custparam_noofrecords"] = SOSearchResult.length;
									Reparray["custparam_clustno"] = clusterNo;
									Reparray["custparam_repno"] = ReportNo;
								}
								else
								{
									Reparray["custparam_repbegloc"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
									Reparray["custparam_repbeglocId"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
									Reparray["custparam_repsku"] = SOSearchResult.getText('custrecord_sku',null,'group');
									Reparray["custparam_repexpqty"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
									Reparray["custparam_repskuno"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
									Reparray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
									Reparray["custparam_reppicklocno"] = SOSearchResult.getValue('custrecord_actendloc',null,'group');
									Reparray["custparam_reppickloc"] = SOSearchResult.getText('custrecord_actendloc',null,'group');
									Reparray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no',null,'group');
									Reparray["custparam_noofrecords"] = SOSearchResult.length;
									Reparray["custparam_clustno"] = clusterNo;
									Reparray["custparam_repno"] = ReportNo;
								}
							}

							response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
						}
					}
					else {
						Reparray["custparam_error"] = st1;//'LP DO NOT MATCH';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						nlapiLogExecution('ERROR', 'LP not Matched');
					}
				}
				else {
					Reparray["custparam_error"] = st2;//'LP IS NULL';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					nlapiLogExecution('ERROR', 'LP is null');
				}
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			nlapiLogExecution('ERROR', 'Catch: LP not found',e);
		}
	}
}

function getOpenTasksCount(ReportNo,sku,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('ERROR', 'ReportNo', ReportNo);
	nlapiLogExecution('ERROR', 'sku', sku);

	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);
	nlapiLogExecution('ERROR', 'replenType in replen task',replenType);

	var openreccount=0;
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('name', null, 'is', ReportNo);

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}

	//if(getBinLocationId!=null && getBinLocationId!='')
	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

	}

	//if(getPriority!=null && getPriority!='')
	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));
	//filters[4] = new nlobjSearchFilter('custrecord_sku', null,'anyof', sku);


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_batch_no');
	columns[11] = new nlobjSearchColumn('custrecord_taskpriority');	
	//columns[11] = new nlobjSearchColumn('id');	

	columns[1].setSort(false);
	columns[2].setSort(false);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!=''&& searchresults.length>0)
		openreccount=searchresults.length;		


	return openreccount;

}
function getOpenTasksCountlp(ReportNo,sku,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('ERROR', 'ReportNo', ReportNo);
	nlapiLogExecution('ERROR', 'sku', sku);

	nlapiLogExecution('ERROR', 'getitemid in replen task count lp',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task count lp',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task count lp',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task count lp',getreportNo);
	nlapiLogExecution('ERROR', 'replenType in replen task count lp',replenType);

	var openreccount=0;
	var filters = new Array();

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));
	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
	}

	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
	}

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	var systemRule=GetSystemRuleForReplen();
	var columns = new Array();

	if(systemRule=='N')
	{
		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
		columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
		var fields=new Array();
		nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
		fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
		var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
		if(sresult!=null && sresult.length>0)
			filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', sresult[0].getId()));
		if(getitemid!=null && getitemid!='')
		{
			var ItemType = nlapiLookupField('item', getitemid, ['recordType','custitem_ebizbatchlot']);
			if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
			{
				columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
			}
		}	
	}
	else
	{

		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc');
		columns[7] = new nlobjSearchColumn('custrecord_lpno');
		columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[9] = new nlobjSearchColumn('name');	
		columns[10] = new nlobjSearchColumn('custrecord_batch_no');

		columns[11] = new nlobjSearchColumn('custrecord_taskpriority');	
	}

	columns[1].setSort(false);
	columns[2].setSort(false);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR','searchresults',searchresults.length);
	return searchresults;

}

function getLPScan(beginLoc,reportNo,sku,expQty,skuNo,repInternalId, 
		clustNo, whlocation, actEndLocationId, actEndLocation, beginLocId,RecordCount,fromlpno,tolpno,nextlocation,nextqty,
		batchno,nextitem,nextitemno,taskpriority,replenType,getitem,getitemid,getloc,getlocid,getreportNo,getNumber,enterfromloc,getLanguage)
{
	if(batchno=='null'||batchno==null)
		batchno="";
	var fields=new Array();
	fields[0]='name';
	fields[1]='custitem_ebizdescriptionitems';
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
	fields[2]='description';
	/*** upto here ***/
	var skudesc='';

	nlapiLogExecution('ERROR', 'sku',sku);
	nlapiLogExecution('ERROR', 'skuNo',skuNo);
	nlapiLogExecution('ERROR', 'getLanguage',getLanguage);

	var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;
	if( getLanguage == 'es_ES' || getLanguage =='es_AR')
	{
		st0 = "LP REPOSICI&#211;N";
		st1 = "ART&#205;CULO :";
		st2 = "DESCRIPCI&#211;N DE ART&#205;CULO :";
		st3 = "CANTIDAD :";
		st4 = "ENTER / SCAN DE LP";
		st5 = "ENVIAR";
		st6 = "ANTERIOR";
		st7 = "SIGUIENTE";
		st8 = "LOTE";
		st9 = "Introduzca la cantidad";

	}
	else
	{
		st0 = "REPLENISHMENT LP";
		st1 = "ITEM:";
		st2 = "ITEM DESCRIPTION: ";
		st3 = "QUANTITY: ";
		st4 = "ENTER/SCAN FROM LP:";
		st5 = "SEND";
		st6 = "PREV";
		st7 = "NEXT";
		st8 = "BATCH";
		st9 = "ENTER QUANTITY";

	}

	var rec=nlapiLookupField('item', skuNo, fields);

	//case no 20125087
	if(rec != null && rec != '')
	{
		sku=rec.name;
		skudesc=rec.custitem_ebizdescriptionitems;
		/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
		if(skudesc==null || skudesc=='')
		{
			skudesc=rec.description;
			skudesc=skudesc.substring(0, 100)
		}
	}

	/*** upto here ***/
	nlapiLogExecution('ERROR', 'sku',sku);

	var caseQty = fetchCaseQuantity(skuNo,whlocation,null);
	var systemRule=GetSystemRuleForReplen();
	var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_LP');
	var html = "<html><head><title>"+st0+"</title>";
	html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
	html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
	//Case# 20148749 Refresh Functionality starts
	html = html + "var version = navigator.appVersion;";
	html = html + "document.onkeydown = function (e) {";
	html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
	html = html + "if ((version.indexOf('MSIE') != -1)) { ";
	html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
	html = html + "else {if (keycode == 116)return false;}";
	html = html + "};";
	//Case# 20148749 Refresh Functionality ends
	html = html + "nextPage = new String(history.forward());";          
	html = html + "if (nextPage == 'undefined')";     
	html = html + "{}";     
	html = html + "else";     
	html = html + "{  location.href = window.history.forward();"; 
	html = html + "} ";

	//html = html + " document.getElementById('enterreplenlp').focus();";    

	html = html + "function stopRKey(evt) { ";
	//html = html + "	  alert('evt');";
	html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
	html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
	html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
	//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

	html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
	html = html + "	  alert('System Processing, Please wait...');";
	html = html + "	  return false;}} ";
	html = html + "	} ";

	html = html + "	document.onkeypress = stopRKey; ";


	html = html + "</script>";
	html = html +functionkeyHtml;
	html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
	html = html + "<body>";
	html = html + "	<form name='_rf_replenishment_LP' method='POST'>";
	html = html + "		<table>";
	html = html + "			<tr>";
	if(systemRule=='Y')
	{
		html = html + "				<td align = 'left'>LP:  <label>" + fromlpno + "</label></td>";
	}
	html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
	html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + beginLoc + ">";
	html = html + "				<input type='hidden' name='hdnSku' value='" + sku + "'>";
	html = html + "				<input type='hidden' name='hdnExpQty' value=" + expQty + ">";
	html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
	html = html + "				<input type='hidden' name='hdnid' value=" + repInternalId + ">";
	html = html + "				<input type='hidden' name='hdnclustno' value=" + clustNo + ">";
	html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
	html = html + "				<input type='hidden' name='hdnactendlocationid' value=" + actEndLocationId + ">";
	html = html + "				<input type='hidden' name='hdnactendlocation' value=" + actEndLocation + ">";
	html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
	html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
//	html = html + "				<input type='hidden' name='hdnLpno' value=" + lpno + ">";
	html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
	html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
	html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
	html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
	html = html + "				<input type='hidden' name='hdnbatchno' value=" + batchno + ">";
	html = html + "				<input type='hidden' name='hdnnitem' value='" + nextitem + "'>";
	html = html + "				<input type='hidden' name='hdnnextitemno' value=" + nextitemno + ">";
	html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
	html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
	html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
	html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
	html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
	html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
	html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
	html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
	html = html + "				<input type='hidden' name='hdnenterfromloc' value=" + enterfromloc + ">";
	html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st1+" <label>" + sku + "</label></td>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st2+":  <label>" + skudesc + "</label></td>";
	html = html + "				</td>";
	html = html + "			</tr>";
	if(batchno!="" && batchno!=null){
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st8+":  <label>" + batchno + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
	}
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st3+" :  <label>" + expQty + " Each</label></td>";
	var pikQtyBreakup = getQuantityBreakdown(caseQty,expQty);
	html = html + "	("+caseQty+"/Case:"+pikQtyBreakup+")";
	html = html + "				</td>";
	html = html + "			</tr>";
	if(systemRule=='Y')
	{
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st4+ ":";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterreplenlp' id='enterreplenlp' type='text' value='"+fromlpno+"'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
	}
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st9+" :";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input name='enterreplenqty' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";	



	html = html + "			<tr>";
	html = html + "				<td align = 'left'>ENTER REMAINING QUANTITY:";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input name='enterremainingreplenqty' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";	


	html = html + "			<tr>";
	html = html + "				<td align = 'left'>"+st5+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
	html = html + "					"+st6+" <input name='cmdPrevious' type='submit' value='F7'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "		 </table>";
	html = html + "	</form>";
	//Case# 20148882 (added Focus Functionality for Textbox)
	html = html + "<script type='text/javascript'>document.getElementById('enterreplenlp').focus();</script>";
	html = html + "</body>";
	html = html + "</html>";
	return html;
}

function DeallocateRemainingInvt(RemQty,InvtRef)
{
	try{
		nlapiLogExecution('ERROR', 'Deallocation of Inventory', 'CreateInvt');
		nlapiLogExecution('ERROR', 'RemQty', RemQty);
		nlapiLogExecution('ERROR', 'InvtRef', InvtRef);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',InvtRef);
		var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
		var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
		var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');

		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'allocqty', allocqty);
		nlapiLogExecution('ERROR', 'RemQty', RemQty);
		transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(RemQty)));
		transaction.setFieldValue('custrecord_ebiz_qoh',(parseFloat(qty)- parseFloat(RemQty)));
		nlapiSubmitRecord(transaction, false, true); 
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'Exception in Deallocation', e);
	}
}

function GetSystemRuleForReplen()
{
	try
	{
		var rulevalue='Y';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for Replen?'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null && searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);


		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}

function getConsolidatedopentaskcount(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType,request)
{

	nlapiLogExecution('ERROR', 'reportNo',reportNo);

	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);
	nlapiLogExecution('ERROR', 'replenType in replen task',replenType);

	var filters = new Array();

	if(reportNo != null && reportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
	}

	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='')
	{
		nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));
	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
	}

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	//filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
	columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
//	var fields=new Array();
//	nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
//	fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
//	var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
//	if(sresult!=null && sresult.length>0)
//	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', sresult[0].getId()));
	if(getitemid!=null && getitemid!='')
	{
		var ItemType = nlapiLookupField('item', getitemid, ['recordType','custitem_ebizbatchlot']);
		if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
		{
			columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
		}
	}

	columns[1].setSort(false);
	columns[2].setSort(false);
	columns[3].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR', 'searchresults.length ingetreplen',searchresults.length);
	return searchresults;

}

function getReplenTasks(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType,request,vbeginloc)
{

	nlapiLogExecution('ERROR', 'Into getReplenTasks - reportNo',reportNo);

	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);
	nlapiLogExecution('ERROR', 'replenType in replen task',replenType);

	var filters = new Array();

	if(reportNo != null && reportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
	}

	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='')
	{
		nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));
	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
	}

	if(vbeginloc!=null && vbeginloc!='')
	{
		nlapiLogExecution('ERROR','vbeginloc',vbeginloc);
		filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', vbeginloc));
	}

	//vbeginloc

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_taskpriority');
	columns[11] = new nlobjSearchColumn('custrecord_batch_no');

	columns[1].setSort(false);
	columns[2].setSort(false);
	columns[3].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR', 'searchresults.length ingetreplen',searchresults.length);
	return searchresults;

}