
/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Client/Attic/ebiz_emptybinloc_CL.js,v $
 *     	   $Revision: 1.1.2.1.8.2 $
 *     	   $Date: 2013/03/19 12:08:11 $
 *     	   $Author: schepuri $
 *     	   $Name: t_NSWMS_2013_1_3_9 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_emptybinloc_CL.js,v $
 * Revision 1.1.2.1.8.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.1.2.1.8.1  2013/02/26 12:56:04  skreddy
 * CASE201112/CR201113/LOG201121
 * merged boombah changes
 *
 * Revision 1.1.2.1  2012/08/03 13:19:19  schepuri
 * CASE201112/CR201113/LOG201121
 * new report
 *
 * Revision 1.1.4.5  2012/06/25 22:24:37  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned using child-parent relation
 *
 * Revision 1.1.4.4  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.6  2012/03/20 18:05:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation by sales order header
 *
 * Revision 1.5  2012/03/19 05:37:24  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2012/03/16 09:59:37  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.1.4.2
 *
 * Revision 1.3  2012/03/15 07:27:20  rmukkera
 * CASE201112/CR201113/LOG201121
 *  condition added for wave cancelation
 *
 * Revision 1.2  2012/03/09 07:25:15  rmukkera
 * CASE201112/CR201113/LOG201121
 * Printreport2 new  meihod added for wavecreation
 *
 * Revision 1.1  2011/11/02 12:34:45  rmukkera
 * CASE201112/CR201113/LOG201121
 *
 * new file
 *  */

function changePageValues(type,name)
{	
	if(name=='custpage_selectpage'||name=='custpage_pagesize')
	{

		NLDoMainFormButtonAction("submitter",true);	
	}
	else
	{
		return true;
	}
}

function Printreport(id)
{		


	var WOPDFURL = nlapiResolveURL('SUITELET', 'customscript_emptybinlocexcel_sl', 'customdeploy_emptybinlocexcel_sl');
	//var ctx = nlapiGetContext();
	//code added from Boombah account on 25Feb13 by santosh
	var vbinlocGroup=nlapiGetFieldValue('custpage_binlocgroup');
	//up to here
	/*nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WOPDFURL = 'https://system.netsuite.com' + WOPDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WOPDFURL = 'https://system.sandbox.netsuite.com' + WOPDFURL;				
		}
*/
	nlapiLogExecution('ERROR', 'WO PDF URL',WOPDFURL);					
	WOPDFURL = WOPDFURL + '&custparam_id='+ id;
	//code added from Boombah account on 25Feb13 by santosh
	WOPDFURL = WOPDFURL + '&custparam_binlocgroup='+ vbinlocGroup;
	//up to here


//	alert(WavePDFURL);
	window.open(WOPDFURL);
}

