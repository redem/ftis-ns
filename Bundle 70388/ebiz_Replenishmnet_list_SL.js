/***************************************************************************
	  		  eBizNET Solutions Inc .
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_Replenishmnet_list_SL.js,v $
 *     	   $Revision: 1.2.14.1 $
 *     	   $Date: 2013/09/11 12:17:32 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *****************************************************************************/
function ebiznet_replenishment_list_SL(request, response){
	var list = nlapiCreateList('Replenishment List');
	list.setStyle('normal');
	var column = list.addColumn('name', 'text', 'Report#', 'LEFT');
	column.setURL(nlapiResolveURL('SUITELET', 'customscript_replenishmnt_report', 'customdeploy_replenishmnt_report_di'));
	column.addParamToURL('custparam_poid', 'name', true);
	list.addColumn('custrecord_sku', 'text', 'Item', 'LEFT');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);	//	8 = RPLN Task
	// WMS Status Flag: G(9)/R(20)/I(21)
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9, 20, 21]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_sku_status');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	var po = new Object();
	var dupliarray = new Array();
	var redarray = new Array();
	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		redarray[i] = searchresults[i].getValue('name');
	}

	var redarray = removeDuplicateElement(redarray);
	for (var i = 0; i < redarray.length; i++) {
		for (var j = 0; searchresults != null && j < searchresults.length; j++) {
			if (redarray[i] == searchresults[j].getValue('name')) {
				po["name"] = searchresults[j].getValue('name');
				po["custrecord_sku"] = searchresults[j].getText('custrecord_sku');
				po["custrecord_sku_status"] = searchresults[j].getText('custrecord_sku_status');
				po["custrecord_ebiz_cntrl_no"] = searchresults[j].getValue('custrecord_ebiz_cntrl_no');

			}
		}
		list.addRow(po);
	}
	response.writePage(list);
}

function removeDuplicateElement(arrayName){
	var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < newArray.length; j++) {
			if (newArray[j] == arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}
