/***************************************************************************
				eBizNET Solutions Inc 
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_PickByLp_SL.js,v $
*� $Revision: 1.1.2.2.4.1.4.1.8.1 $
*� $Date: 2015/09/23 14:57:59 $
*� $Author: deepshikha $
*� $Name: t_WMS_2015_2_StdBundle_1_13 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_PickByLp_SL.js,v $
*� Revision 1.1.2.2.4.1.4.1.8.1  2015/09/23 14:57:59  deepshikha
*� 2015.2 issueFix
*� 201414466
*�
*� Revision 1.1.2.2.4.1.4.1  2013/03/01 14:34:54  skreddy
*� CASE201112/CR201113/LOG201121
*� Merged from FactoryMation and change the Company name
*�
*� Revision 1.1.2.2.4.1  2012/11/01 14:55:02  schepuri
*� CASE201112/CR201113/LOG201121
*� Decimal Qty Conversions
*�
*� Revision 1.1.2.2  2012/04/20 14:12:36  schepuri
*� CASE201112/CR201113/LOG201121
*� changing the Label of Batch #  field to Lot#
*�
*� Revision 1.1.2.1  2012/04/17 09:20:27  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� Pick By LP
*�
*
****************************************************************************/

function PickByLp_Sl(request,response)
{
	if (request.getMethod() == 'GET') {
		
		var form = nlapiCreateForm('Pick By Lp');
		var soField = form.addField('custpage_qbso', 'select', 'Fulfillment Ord#');
		soField.setLayoutType('startrow', 'none');  
		soField.setMandatory(true); 
	//	var hdnlotno = form.addField('custpage_hdnlotnumber', 'text', 'hdnlblotno');
	//	var hdnbinloc = form.addField('custpage_hdnbinloc', 'text', 'hdnlbinloc');
		fillfulfillorderField(form, soField,-1);
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else
		{
		var form = nlapiCreateForm('Pick By Lp');
		
		 form.setScript('customscript_ebiz_pickbylp_cl');
		       var lineCnt=request.getLineItemCount('custpage_items');
		       nlapiLogExecution('ERROR', 'lineCnt', lineCnt); 
		       if(lineCnt>0)
		    	   {
		    
		    		var fulfillOrderList = getSelectedFulfillmentOrderInfo(request);
		    		var eBizWaveNo ;var eBizReportNo;
		    		// Proceed only if there is atleast 1 fulfillment order selected
		    		if(fulfillOrderList!=null && fulfillOrderList.length > 0){	
		    			nlapiLogExecution('ERROR', 'fulfillOrderList.length', fulfillOrderList.length);
		    			// Get the next WAVE and REPORT sequences
				   		 eBizWaveNo = GetMaxTransactionNo('WAVE');
				   		 eBizReportNo = GetMaxTransactionNo('REPORT');
				   		nlapiLogExecution('ERROR', 'Wave & Report No.', eBizWaveNo + '|', eBizReportNo);
		    			nlapiLogExecution('ERROR', 'eBizWaveNo', eBizWaveNo);	
		    			for(var i=0;i<fulfillOrderList.length;i++)
		    				{
		    				createRecordInOpenTask(fulfillOrderList[i],'', eBizWaveNo, '', '', '','','', '', '', '', '', '','','','','','','','');				
			    			updateFulfilmentOrder(fulfillOrderList[i], eBizWaveNo);
			    			updateInventoryWithAllocation(fulfillOrderList[i]);
		    				}
		    		

		    		}
		    		
		    	   }
		
		
		var soField = form.addField('custpage_qbso', 'select', 'Fulfillment Ord#');
	////	var hdnlotno = form.addField('custpage_hdnlotnumber', 'text', 'hdnlotno');
	//	var hdnbinloc = form.addField('custpage_hdnbinloc', 'text', 'hdnlbinloc');
		soField.setLayoutType('startrow', 'none'); 
		soField.setMandatory(true); 
		fillfulfillorderField(form, soField,-1);
		var filters = new Array();
		if (request.getParameter('custpage_qbso') != null && request.getParameter('custpage_qbso') != "") {
			filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', request.getParameter('custpage_qbso')));
		}
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_linesku_status');
		columns[1] = new nlobjSearchColumn('custrecord_do_customer');
		columns[2] = new nlobjSearchColumn('custrecord_do_carrier');
		columns[3] = new nlobjSearchColumn('custrecord_do_order_type');
		columns[4] = new nlobjSearchColumn('custrecord_do_order_priority');
		columns[5] = new nlobjSearchColumn('custrecord_lineord');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_linesku');
		columns[7] = new nlobjSearchColumn('custrecord_ord_qty');
		columns[8] = new nlobjSearchColumn('name');
		//columns[8].setSort();
		columns[9] = new nlobjSearchColumn('custrecord_lineord');
		columns[9].setSort();		
		columns[10] = new nlobjSearchColumn('custrecord_ordline');
		columns[10].setSort();
		columns[11] = new nlobjSearchColumn('custrecord_linepackcode');
		columns[12] = new nlobjSearchColumn('custrecord_lineuom_id');
		columns[13] = new nlobjSearchColumn('custrecord_batch');
		columns[14] = new nlobjSearchColumn('custrecord_ordline_wms_location');
		columns[15] = new nlobjSearchColumn('custrecord_ordline_company');
		columns[16] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo1');
		columns[17] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo2');
		columns[18] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo3');	
		columns[19] = new nlobjSearchColumn('custrecord_pickgen_qty');	
		columns[20] = new nlobjSearchColumn('custrecord_linestatus_flag');
		columns[21] = new nlobjSearchColumn('id');
		columns[21].setSort();
		columns[22] = new nlobjSearchColumn('custrecord_ns_ord');
		columns[22].setSort();
		columns[23] = new nlobjSearchColumn('custrecord_do_wmscarrier');
		columns[24]=new nlobjSearchColumn('custrecord_pickqty');
		var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
		
		var sublist = form.addSubList("custpage_items", "list", "ItemList");
		
		
		//sublist.addMarkAllButtons();
		sublist.addField("custpage_so", "checkbox", "Select").setDefaultValue('F');
		sublist.addField("custpage_sono", "text", "Order No");
		sublist.addField("custpage_lineno", "text", "Line No");	 		
		sublist.addField("custpage_itemname", "text", "SKU");
		sublist.addField("custpage_skustatus", "text", "Item Status").setDisplayType('hidden');
		sublist.addField("custpage_skustatustext", "text", "Status");
		var lot=sublist.addField("custpage_lotbatch", "select", "Lot/Batch #");
		var binloc=	sublist.addField("custpage_binlocations", "select", "Bin Location");
		sublist.addField("custpage_packcode", "text", "Packcode").setDisplayType('hidden');
		sublist.addField("custpage_uomid", "text", "Base<br>UOM").setDisplayType('hidden');
		sublist.addField("custpage_orderqty", "text", "Remaining<br>Order Qty");
		sublist.addField("custpage_pickgenqty", "text", "PickGen <br>Qty");
		sublist.addField("custpage_pickqty", "text", "Pick<br>Qty");
		sublist.addField("custpage_doinernno", "text", "fointernalid").setDisplayType('hidden');
		sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
		sublist.addField("custpage_location", "text", "location").setDisplayType('hidden');
		sublist.addField("custpage_itemno", "text", "Item No").setDisplayType('hidden');
		sublist.addField("custpage_ordqty", "integer", "Fulfilment Order Qty").setDisplayType('hidden');
		sublist.addField("custpage_soinernno", "text", "SONo").setDisplayType('hidden');
		sublist.addField("custpage_iteminfo1", "text", "Item Info1").setDisplayType('hidden');
		sublist.addField("custpage_iteminfo2", "text", "Item Info2").setDisplayType('hidden');
		sublist.addField("custpage_iteminfo3", "text", "Item Info3").setDisplayType('hidden');
		sublist.addField("custpage_ordtype", "text", "Order Type").setDisplayType('hidden');
		sublist.addField("custpage_ordpriority", "text", "Order Priority").setDisplayType('hidden');
		sublist.addField("custpage_createinvntid", "text", "recordid").setDisplayType('hidden');
		sublist.addField("custpage_lpno", "text", "LpNo").setDisplayType('hidden');
		sublist.addField("custpage_beginloc", "text", "BeginLoc").setDisplayType('hidden');
		var lp=	sublist.addField("custpage_lp", "select", " Lp").setDisplaySize('150', '50');
		sublist.addField("custpage_lpavailqty", "text", "Available Qty<br>In Lp").setDisplayType('entry');
//	lp.setMandatory(true); 
	var qty=	sublist.addField("custpage_qty", "text", "Qty").setDisplayType('entry');
	//qty.setMandatory(true); 
		form.addSubmitButton('Display');
		var itemslist=new Array;
		if(orderList!=null && orderList.length > 0){
			nlapiLogExecution('ERROR', 'orderList.length', orderList.length); 
			for(var i=0;i<orderList.length;i++)
				{
				
				var currentOrder=orderList[i];
				form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1,
						currentOrder.getValue('custrecord_ordline'));
			
				form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1, 
						currentOrder.getValue('custrecord_lineord'));
				form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, 
						currentOrder.getText('custrecord_ebiz_linesku'));
				form.getSubList('custpage_items').setLineItemValue('custpage_itemno', i + 1, 
						currentOrder.getValue('custrecord_ebiz_linesku'));
				form.getSubList('custpage_items').setLineItemValue('custpage_skustatustext', i + 1, 
						currentOrder.getText('custrecord_linesku_status'));			
				form.getSubList('custpage_items').setLineItemValue('custpage_skustatus', i + 1, 
						currentOrder.getValue('custrecord_linesku_status'));
				form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, 
						currentOrder.getValue('custrecord_batch'));
				form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, 
						currentOrder.getText('custrecord_linepackcode'));
				form.getSubList('custpage_items').setLineItemValue('custpage_uomid', i + 1, 
						currentOrder.getText('custrecord_lineuom_id'));
				form.getSubList('custpage_items').setLineItemValue('custpage_doinernno', i + 1, currentOrder.getId());
				form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1, 
						currentOrder.getValue('custrecord_ordline_company'));	
				form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, 
						currentOrder.getValue('custrecord_ordline_wms_location'));
				
				var vOrdQty = parseFloat(currentOrder.getValue('custrecord_ord_qty'));


				var vPickGenQty=0;

				if(currentOrder.getValue('custrecord_pickgen_qty')!=null && currentOrder.getValue('custrecord_pickgen_qty')!='')	
					vPickGenQty = parseFloat(currentOrder.getValue('custrecord_pickgen_qty'));
				vOrdQty = parseFloat(vOrdQty)-parseFloat(vPickGenQty);
				form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, 
						vOrdQty.toString());
				form.getSubList('custpage_items').setLineItemValue('custpage_soinernno', i + 1, 
						currentOrder.getValue('name'));
				form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo1', i + 1, 
						currentOrder.getText('custrecord_fulfilmentiteminfo1'));
				form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo2', i + 1, 
						currentOrder.getText('custrecord_fulfilmentiteminfo2'));
				form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo3', i + 1, 
						currentOrder.getText('custrecord_fulfilmentiteminfo3'));
				form.getSubList('custpage_items').setLineItemValue('custpage_ordtype', i + 1,
						currentOrder.getText('custrecord_do_order_type'));
				form.getSubList('custpage_items').setLineItemValue('custpage_ordpriority', i + 1, 
						currentOrder.getText('custrecord_do_order_priority'));
				form.getSubList('custpage_items').setLineItemValue('custpage_lpavailqty', i + 1, 
						'0');
				var vOrdQty = parseFloat(currentOrder.getValue('custrecord_ord_qty'));
				var vPickGenQty=0;

				if(currentOrder.getValue('custrecord_pickgen_qty')!=null && currentOrder.getValue('custrecord_pickgen_qty')!='')	
					vPickGenQty = parseFloat(currentOrder.getValue('custrecord_pickgen_qty'));
				
				vOrdQty = parseFloat(vOrdQty)-parseFloat(vPickGenQty);
				nlapiLogExecution('ERROR', 'vOrdQty', vOrdQty); 
				form.getSubList('custpage_items').setLineItemValue('custpage_orderqty', i + 1, 
						vOrdQty.toString());
				
				form.getSubList('custpage_items').setLineItemValue('custpage_pickgenqty', i + 1, 
						currentOrder.getValue('custrecord_pickgen_qty'));
				
				form.getSubList('custpage_items').setLineItemValue('custpage_pickqty', i + 1, 
						currentOrder.getValue('custrecord_pickqty'));
				if(currentOrder.getValue('custrecord_ebiz_linesku')!=null && currentOrder.getValue('custrecord_ebiz_linesku')!='')
				{
					var sku=currentOrder.getValue('custrecord_ebiz_linesku');
					itemslist[itemslist.length]=sku;
				}
				
				nlapiLogExecution('ERROR', 'itemslist', itemslist); 
				if(i==orderList.length-1)
				{
					nlapiLogExecution('ERROR', 'request custpage_lotbatch', request.getParameter('custpage_lotbatch'));
					var createInventoryFilters = new Array();
					if(itemslist!=null && itemslist.length>0)
					{
						
					createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemslist));
					createInventoryFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']));
					
//					if(currentOrder.getText('custrecord_linepackcode')!=null && currentOrder.getText('custrecord_linepackcode')!='')
//					createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'is', currentOrder.getText('custrecord_linepackcode')));
//					if(currentOrder.getValue('custrecord_linesku_status')!=null && currentOrder.getValue('custrecord_linesku_status')!='')
//					createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', currentOrder.getValue('custrecord_linesku_status')));
					}
					var createInventoryCols=new Array();
					createInventoryCols[0]= new nlobjSearchColumn('custrecord_ebiz_inv_lp');
					createInventoryCols[1]= new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
					createInventoryCols[2]=  new nlobjSearchColumn('custrecord_ebiz_inv_sku').setSort();
					createInventoryCols[3]=  new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
					createInventoryCols[4]=  new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
					createInventoryCols[5]=  new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
					createInventoryCols[6]=  new nlobjSearchColumn('custrecord_ebiz_qoh');
					createInventoryCols[7]=  new nlobjSearchColumn('custrecord_ebiz_inv_lot');
					createInventoryCols[8]=  new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
					
					var createInventoryList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, createInventoryFilters, createInventoryCols);
					
					
					for (var k = 0; createInventoryList != null && k < createInventoryList.length; k++) {
                            if(k==0)
                            	{
                            	lp.addSelectOption('Select', 'Select');
                            	}
                            var qoh=0;var allocqty=0;
                            if(createInventoryList[k].getValue('custrecord_ebiz_qoh')!=null && createInventoryList[k].getValue('custrecord_ebiz_qoh')!='')
                            	{
                            	qoh=createInventoryList[k].getValue('custrecord_ebiz_qoh');
                            	}
                            if(createInventoryList[k].getValue('custrecord_ebiz_alloc_qty')!=null && createInventoryList[k].getValue('custrecord_ebiz_alloc_qty')!='')
                        	{
                            	allocqty=createInventoryList[k].getValue('custrecord_ebiz_alloc_qty');
                        	}
                                  var quantity=parseFloat(qoh)-parseFloat(allocqty);
                                  //nlapiLogExecution('ERROR', 'createInventoryList[k].getValue(custrecord_ebiz_qoh)', createInventoryList[k].getValue('custrecord_ebiz_qoh')); 
                                 // nlapiLogExecution('ERROR', 'createInventoryList[k].getValue(custrecord_ebiz_alloc_qty)', createInventoryList[k].getValue('custrecord_ebiz_alloc_qty')); 
                                //  nlapiLogExecution('ERROR', 'createInventoryList[k].getText(custrecord_ebiz_inv_sku_status)', createInventoryList[k].getText('custrecord_ebiz_inv_sku_status')); 
                                  lp.addSelectOption(quantity+","+createInventoryList[k].getText('custrecord_ebiz_inv_sku_status')+","+createInventoryList[k].getText('custrecord_ebiz_inv_packcode')+","+createInventoryList[k].getText('custrecord_ebiz_inv_sku')+","+createInventoryList[k].getValue('custrecord_ebiz_inv_lp')+","+createInventoryList[k].getValue('custrecord_ebiz_inv_binloc')+","+createInventoryList[k].getValue('custrecord_ebiz_inv_lot')+","+createInventoryList[k].getId(), createInventoryList[k].getValue('custrecord_ebiz_inv_lp')+"-"+createInventoryList[k].getText('custrecord_ebiz_inv_sku'));
                            	
						
					}
					
							createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_cycl_count_hldflag', null, 'is', 'F'));
							createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_invholdflg', null, 'is', 'F'));
							createInventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
							
							var lotarray=new Array();var binllocarray=new Array();
							var createInventoryLotList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, createInventoryFilters, createInventoryCols);
							nlapiLogExecution('ERROR', 'createInventoryLotList', createInventoryLotList);	
							
							
							
							for (var k = 0; createInventoryLotList != null && k < createInventoryLotList.length; k++) {
								if(k==0)
								{
									lot.addSelectOption('Select', 'Select');
									binloc.addSelectOption('Select', 'Select');
								}
//								if((request.getParameter('custpage_hdnbinloc')==null || request.getParameter('custpage_hdnbinloc')==''))
//								{
									if(createInventoryLotList[k].getValue('custrecord_ebiz_inv_lot') != null  && createInventoryLotList[k].getValue('custrecord_ebiz_inv_lot') != "")
									{

										if (lotarray != null) {
											if (lotarray.indexOf(createInventoryLotList[k].getValue('custrecord_ebiz_inv_lot'))==-1 ) {
												lotarray[lotarray.length]=createInventoryLotList[k].getValue('custrecord_ebiz_inv_lot');

												lot.addSelectOption(createInventoryLotList[k].getValue('custrecord_ebiz_inv_lot'), createInventoryLotList[k].getText('custrecord_ebiz_inv_lot')+"-"+createInventoryLotList[k].getText('custrecord_ebiz_inv_sku'));
											}
										}
									}
//									lot.setDisplayType('normal');
//								}
//								else
//								{
//									lot.setDisplayType('disabled');
//								}
//								if(request.getParameter('custpage_hdnlotnumber')==null || request.getParameter('custpage_hdnlotnumber')=='')
//								{
									if(createInventoryLotList[k].getValue('custrecord_ebiz_inv_binloc') != null  && createInventoryLotList[k].getValue('custrecord_ebiz_inv_binloc') != " ")
									{

										if (lotarray != null) {
											if (binllocarray.indexOf(createInventoryLotList[k].getValue('custrecord_ebiz_inv_binloc'))==-1) {
												binllocarray[lotarray.length]=createInventoryLotList[k].getValue('custrecord_ebiz_inv_binloc');

												binloc.addSelectOption(createInventoryLotList[k].getValue('custrecord_ebiz_inv_binloc'), createInventoryLotList[k].getText('custrecord_ebiz_inv_binloc')+"-"+createInventoryLotList[k].getText('custrecord_ebiz_inv_sku'));
											}
										}
									}
//									binloc.setDisplayType('normal');
//								}
//								else
//								{
//									binloc.setDisplayType('disabled');
//								}


							}
				}
				
				}
		}
		response.writePage(form);
		}
}

if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}
/**
 * Return the fulfillment order information to be used for wave generation
 * NOTE: WILL NEVER RETURN NULL; WILL ALWAYS RETURN ATLEAST A BLANK ARRAY
 * 
 * @param request
 * @returns {Array}
 */
function getSelectedFulfillmentOrderInfo(request){
	nlapiLogExecution('ERROR','into getSelectedFulfillmentOrderInfo', '');
	var fulfillOrderInfoArray = new Array();
	var orderInfoCount = 0;

	// Retrieve the number of items for which wave needs to be created
	var lineCount = request.getLineItemCount('custpage_items');

	for(var k = 1; k <= lineCount; k++){
		var selectValue = request.getLineItemValue('custpage_items', 'custpage_so', k);
		//nlapiLogExecution('ERROR','selectValue', selectValue);

		if(isItemSelected(request,'custpage_items', 'custpage_so', k)){
			var itemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
			var itemNo = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
			var availQty = 0;
			var orderQty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
			var doNo = request.getLineItemValue('custpage_items', 'custpage_doinernno', k);
			var lineNo = request.getLineItemValue('custpage_items', 'custpage_lineno', k);
			var doName = request.getLineItemValue('custpage_items', 'custpage_sono', k);
			var soInternalID = request.getLineItemValue('custpage_items', 'custpage_soinernno', k);
			var itemStatus = request.getLineItemValue('custpage_items', 'custpage_skustatus', k);
			var packCode = request.getLineItemValue('custpage_items', 'custpage_packcode', k);

			var uom = request.getLineItemValue('custpage_items', 'custpage_uomid', k);
			var lotBatchNo = request.getLineItemValue('custpage_items', 'custpage_lotbatch', k);
			var location = request.getLineItemValue('custpage_items', 'custpage_location', k);
			var company = request.getLineItemValue('custpage_items', 'custpage_company', k);
			var itemInfo1 = request.getLineItemValue('custpage_items', 'custpage_iteminfo1', k);
			var itemInfo2 = request.getLineItemValue('custpage_items', 'custpage_iteminfo2', k);
			var itemInfo3 = request.getLineItemValue('custpage_items', 'custpage_iteminfo2', k);
			var orderType = request.getLineItemValue('custpage_items', 'custpage_ordtype', k);
			var orderPriority = request.getLineItemValue('custpage_items', 'custpage_ordpriority', k);
			//var wmsCarrier = request.getLineItemValue('custpage_items','custrecord_do_wmscarrier',k);
			var lpno=request.getLineItemValue('custpage_items', 'custpage_lpno', k);
			var createInvtId=request.getLineItemValue('custpage_items', 'custpage_createinvntid', k);
			var beginloc=request.getLineItemValue('custpage_items', 'custpage_beginloc', k);
			nlapiLogExecution('ERROR','lpno',lpno);
			var pickgenqty=request.getLineItemValue('custpage_items', 'custpage_qty', k);
			var currentRow = [k, soInternalID, lineNo, doNo, doName, itemName, itemNo, itemStatus, 
			                  availQty, orderQty, packCode, uom, lotBatchNo, company, 
			                  itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority,location,lpno,createInvtId,beginloc,pickgenqty];

			fulfillOrderInfoArray[orderInfoCount++] = currentRow;
		}
	}
	nlapiLogExecution('ERROR','fulfillOrderInfoArray', fulfillOrderInfoArray.length);

	nlapiLogExecution('ERROR','out of getSelectedFulfillmentOrderInfo', '');

	return fulfillOrderInfoArray;

}

/**
 * 
 */
function isItemSelected(request, subList, element, index){
	var retVal = false;
	if(request.getLineItemValue(subList, element, index) == 'T')
		retVal = true;

	return retVal;
}



/**
 * Returns the new maximum wave number for wave generation
 * @param transType
 * @returns cluster number
 */
function GetMaxTransactionNo(transType){
	var clusterNo = 1;

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_seqno');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_trantype', null, 'is', transType);

	// Search in ebiznet_wave custom record for all rows for the given transaction type
	var results = nlapiSearchRecord('customrecord_ebiznet_wave', null, filters, columns);
	if(results != null){
		for (var t = 0; t < results.length; t++) {
			clusterNo = results[t].getValue('custrecord_seqno');
			// Increment cluster number
			clusterNo = parseFloat(clusterNo) + 1;
			nlapiSubmitField('customrecord_ebiznet_wave', results[t].getId(), 'custrecord_seqno', parseFloat(clusterNo));
		}
	}

	nlapiLogExecution('ERROR', 'GetMaxTransactionNo:New Cluster No.', clusterNo);
	return parseFloat(clusterNo);
}
function fillfulfillorderField(form, FulfillOrderField,maxno){
	var filtersso = new Array();		
	filtersso[0] = new nlobjSearchFilter( 'custrecord_ord_qty', null, 'greaterthan', '0' );
	//15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filtersso[1] = new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [15,25,26]);
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('id');	 
	columnsinvt[1] = new nlobjSearchColumn('custrecord_lineord');
	columnsinvt[0].setSort(true);
	columnsinvt[2] = new nlobjSearchColumn('custrecord_shipment_no');
	//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');
	if(request.getParameter('custpage_qbso')!=null && request.getParameter('custpage_qbso')!='')
	{
		FulfillOrderField.addSelectOption(request.getParameter('custpage_qbso'), request.getParameter('custpage_qbso'));
	}
	else
	{
		FulfillOrderField.addSelectOption("", "");
	}



	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersso,columnsinvt);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('custrecord_lineord') != null && customerSearchResults[i].getValue('custrecord_lineord') != "" && customerSearchResults[i].getValue('custrecord_lineord') != " ")
		{
			var resdo = form.getField('custpage_qbso').getSelectOptions(customerSearchResults[i].getValue('custrecord_lineord'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		FulfillOrderField.addSelectOption(customerSearchResults[i].getValue('custrecord_lineord'), customerSearchResults[i].getValue('custrecord_lineord'));
	}


	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		fillfulfillorderField(form, FulfillOrderField,maxno);	
	}
}
function createRecordInOpenTask(fulfillOrderList,expectedQuantity, waveNo, containerLP, beginLocationId, recordId,packCode,
		itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, LP,uomlevel,totalweight,totalcube,containersize,fromLot,vnotes,wmsCarrier){
	nlapiLogExecution('ERROR', 'into createRecordInOpenTask with container lp : ',containerLP);
	nlapiLogExecution('ERROR', 'into createRecordInOpenTask with beginLocationId : ',beginLocationId);



	var openTaskRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
 var containerLP = GetMaxLPNo('1', '2',fulfillOrderList[19]);
	nlapiLogExecution('ERROR', 'into createRecordInOpenTask with container lp : ',containerLP);
if(fulfillOrderList[19]!= null && fulfillOrderList[19]!='')
		openTaskRecord.setFieldValue('custrecord_wms_location', fulfillOrderList[19]);
	openTaskRecord.setFieldValue('name', fulfillOrderList[4]);							// DELIVERY ORDER NAME
	openTaskRecord.setFieldValue('custrecord_ebiz_sku_no', fulfillOrderList[6]);		// ITEM INTERNAL ID
	openTaskRecord.setFieldValue('custrecord_sku', fulfillOrderList[6]);				// ITEM NAME
	openTaskRecord.setFieldValue('custrecord_expe_qty', parseFloat(fulfillOrderList[9]).toFixed(5));				// FULFILMENT ORDER QUANTITY
	openTaskRecord.setFieldValue('custrecord_container_lp_no', containerLP);
	openTaskRecord.setFieldValue('custrecord_ebiz_lpno', containerLP);
	openTaskRecord.setFieldValue('custrecord_line_no', fulfillOrderList[2]);
	openTaskRecord.setFieldValue('custrecord_tasktype', '3'); 							// TASK TYPE = PICK

	openTaskRecord.setFieldValue('custrecord_actbeginloc', fulfillOrderList[22]);
	openTaskRecord.setFieldValue('custrecord_ebiz_cntrl_no', fulfillOrderList[3]);
	openTaskRecord.setFieldValue('custrecord_ebiz_order_no', parseFloat(fulfillOrderList[1]));
	openTaskRecord.setFieldValue('custrecord_ebiz_receipt_no', fulfillOrderList[3]);
	openTaskRecord.setFieldValue('custrecord_ebiz_wave_no',  waveNo);
	openTaskRecord.setFieldValue('custrecordact_begin_date',DateStamp());
//	if(wmsCarrier != null && wmsCarrier != "")
//		openTaskRecord.setFieldValue('custrecord_ebizwmscarrier',  wmsCarrier);
	// Setting for failed pick
	if(fulfillOrderList[22] == "")
		openTaskRecord.setFieldValue('custrecord_wms_status_flag', '26'); 					// STATUS FLAG = F
	else
		openTaskRecord.setFieldValue('custrecord_wms_status_flag', '9'); 					// STATUS FLAG = G

	openTaskRecord.setFieldValue('custrecord_from_lp_no', fulfillOrderList[20]);
	openTaskRecord.setFieldValue('custrecord_lpno', fulfillOrderList[20]);
	openTaskRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
	openTaskRecord.setFieldValue('custrecord_invref_no', fulfillOrderList[21]);
	openTaskRecord.setFieldValue('custrecord_packcode', fulfillOrderList[10]);
	openTaskRecord.setFieldValue('custrecord_sku_status', fulfillOrderList[7]);
	openTaskRecord.setFieldValue('custrecord_uom_id', fulfillOrderList[11]);
	openTaskRecord.setFieldValue('custrecord_batch_no', fulfillOrderList[12]);
	//openTaskRecord.setFieldValue('custrecord_ebizrule_no', pickStrategyId);
	//openTaskRecord.setFieldValue('custrecord_ebizmethod_no', pickMethod);
	//openTaskRecord.setFieldValue('custrecord_ebizzone_no', pickZone);
	openTaskRecord.setFieldValue('custrecord_comp_id', fulfillOrderList[13]);
	nlapiLogExecution('ERROR', 'whLocation',whLocation);

//	if(whLocation != null && whLocation!='')
//		openTaskRecord.setFieldValue('custrecord_wms_location', whLocation);	

	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();		
	openTaskRecord.setFieldValue('custrecord_ebizuser', currentUserID);		
	openTaskRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);						
	openTaskRecord.setFieldValue('custrecord_taskassignedto', currentUserID);
	openTaskRecord.setFieldValue('custrecord_uom_level', uomlevel);
	openTaskRecord.setFieldValue('custrecord_total_weight', parseFloat(totalweight).toFixed(5));
	openTaskRecord.setFieldValue('custrecord_totalcube', parseFloat(totalcube).toFixed(5));
	openTaskRecord.setFieldValue('custrecord_container', containersize);
	openTaskRecord.setFieldValue('custrecord_batch_no', fromLot);
	openTaskRecord.setFieldValue('custrecord_notes', vnotes);

	nlapiLogExecution('ERROR', 'fromLot',fromLot);
	nlapiSubmitRecord(openTaskRecord,false,true);
	nlapiLogExecution('ERROR', 'Opentask updation Completed');
	nlapiLogExecution('ERROR', 'out of createRecordInOpenTask with container lp : ',containerLP);
}

function updateFulfilmentOrder(fulfillOrderList, eBizWaveNo){

	nlapiLogExecution('ERROR', 'Into updateFulfilmentOrder ',fulfillOrderList[3]);
	var pickgenqty=0;
	if (fulfillOrderList[23] == "" || isNaN(fulfillOrderList[23])) {
		pickgenqty = 0;
	}
	else
		{
		pickgenqty=fulfillOrderList[23];
		}
	var fulfilmentOrderline = nlapiLoadRecord('customrecord_ebiznet_ordline', fulfillOrderList[3]);
	var prevpickgenqty=fulfilmentOrderline.getFieldValue('custrecord_pickgen_qty');
	var totalPickGeneratedQuantity = parseFloat(prevpickgenqty) + parseFloat(pickgenqty);
	nlapiLogExecution('ERROR', 'totalPickGeneratedQuantity ', totalPickGeneratedQuantity);
	nlapiLogExecution('ERROR', 'pickgenqty', pickgenqty);
	fulfilmentOrderline.setFieldValue('custrecord_ebiz_wave', parseFloat(eBizWaveNo));		

	if(parseFloat(pickgenqty)>0)
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '9'); //status flag--'G' (Picks Generated)	
	else
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '26'); //status flag--'F' (Picks Failed)

	fulfilmentOrderline.setFieldValue('custrecord_pickgen_qty', parseFloat(totalPickGeneratedQuantity).toFixed(5));

	nlapiSubmitRecord(fulfilmentOrderline, false, true);
	nlapiLogExecution('ERROR', 'updateFulfilmentOrder ', 'Success');
	nlapiLogExecution('ERROR', 'Out of updateFulfilmentOrder ');
}
function updateInventoryWithAllocation(fulfillOrderList){
	var allocationQuantity=fulfillOrderList[23];
	nlapiLogExecution('ERROR', 'Into updateInventoryWithAllocation - Qty', fulfillOrderList[23]);
	var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', fulfillOrderList[21]);
	var alreadyallocqty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
	if (isNaN(alreadyallocqty))
	{
		alreadyallocqty = 0;
	}
	var totalallocqty=parseFloat(allocationQuantity)+parseFloat(alreadyallocqty);
	inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(totalallocqty).toFixed(5));
	inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
	inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

	nlapiSubmitRecord(inventoryTransaction,false,true);
	//nlapiLogExecution('ERROR', 'Out of updateInventoryWithAllocation - Record ID',recordId);

}

