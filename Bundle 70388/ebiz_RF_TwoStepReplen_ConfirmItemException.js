
/***************************************************************************
 eBizNET Solutions Inc
****************************************************************************
*
*  $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplen_ConfirmItemException.js,v $
*  $Revision: 1.1.2.2 $
*  $Date: 2015/04/30 15:24:44 $
*  $Author: grao $
*  $Name: b_WMS_2015_2_StdBundle_Issues $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_RF_TwoStepReplen_ConfirmItemException.js,v $
*  Revision 1.1.2.2  2015/04/30 15:24:44  grao
*  SB issue fixes  201412579
* 
*  Revision 1.1.2.1  2015/01/02 15:04:38  skreddy
*  Case# 201411349
*  Two step replen process
* 
*  Revision 1.2.2.2.4.2.4.7  2014/06/13 07:53:40  skavuri
*  Case# 20148882 (added Focus Functionality for Textbox)
* 
*  Revision 1.2.2.2.4.2.4.6  2014/06/06 06:21:21  skavuri
*  Case# 20148749 (Refresh Functionality ) SB Issue Fixed
* 
*  Revision 1.2.2.2.4.2.4.5  2014/05/30 00:26:47  nneelam
*  case#  20148622
*  Stanadard Bundle Issue Fix.
* 
*  Revision 1.2.2.2.4.2.4.4  2013/07/15 14:26:38  rrpulicherla
*  case# 20123391
*  GFT Issue fixes
* 
*  Revision 1.2.2.2.4.2.4.3  2013/06/11 14:30:41  schepuri
*  Error Code Change ERROR to DEBUG
* 
*  Revision 1.2.2.2.4.2.4.2  2013/04/17 15:53:17  skreddy
*  CASE201112/CR201113/LOG201121
*  issue fixes
* 
*  Revision 1.2.2.2.4.2.4.1  2013/03/19 11:53:20  snimmakayala
*  CASE201112/CR201113/LOG2012392
*  Production and UAT issue fixes.
* 
*  Revision 1.2.2.2.4.2  2012/11/11 03:40:30  snimmakayala
*  CASE201112/CR201113/LOG201121
*  GSUSA UAT issue fixes.
* 
*  Revision 1.2.2.2.4.1  2012/09/21 14:57:16  grao
*  CASE201112/CR201113/LOG201121
*  Converting Multilanguage
* 
*  Revision 1.2.2.2  2012/03/15 12:57:10  schepuri
*  CASE201112/CR201113/LOG201121
*  new file
* 
*  Revision 1.2  2012/03/14 13:41:45  rmukkera
*  CASE201112/CR201113/LOG201121
*  cartno added in the message
* 
*  Revision 1.1  2012/03/13 14:08:41  rmukkera
*  CASE201112/CR201113/LOG201121
*  new file
* 
*
****************************************************************************/

function ConfirmItemException(request, response)
{
	if (request.getMethod() == 'GET') 
	{    

		    var getCartLP = request.getParameter('custparam_cartlpno');	    
		    
		    var getLanguage = request.getParameter('custparam_language');
			/*POarray["custparam_language"] = getLanguage;*/
			nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    	
			
			var st0,st1,st2;
			if( getLanguage == 'es_ES')
			{
				st0 = "";
				st1 = "SI";
				st2 = "NO";
			}
			else
			{
				st0 = "";
				st1 = "YES";
				st2 = "NO";
			}
			
			var reportNo = request.getParameter('custparam_repno');
			var beginLoc = request.getParameter('custparam_repbegloc');
			var sku = request.getParameter('custparam_repsku');
			var skuNo = request.getParameter('custparam_repskuno');
			var expQty = request.getParameter('custparam_repexpqty');
			var repInternalId = request.getParameter('custparam_repid');
			var clustNo = request.getParameter('custparam_clustno');
			var whlocation = request.getParameter('custparam_whlocation');
			var actEndLocationId = request.getParameter('custparam_reppicklocno');
			var actEndLocation = request.getParameter('custparam_reppickloc');
			var RecordCount=request.getParameter('custparam_noofrecords');
			var nextlocation=request.getParameter('custparam_nextlocation');
			var nextqty=request.getParameter('custparam_nextqty');
			var batchno=request.getParameter('custparam_batchno');
			var fromlpno=request.getParameter('custparam_fromlpno');
			var tolpno=request.getParameter('custparam_tolpno');
			var nextitem=request.getParameter('custparam_nextitem');
			var nextitemno=request.getParameter('custparam_nextitemno');
			var taskpriority = request.getParameter('custparam_entertaskpriority');
			var getreportNo=request.getParameter('custparam_enterrepno');
			var getitem = request.getParameter('custparam_entersku');
			var getitemid = request.getParameter('custparam_enterskuid');
			var getloc = request.getParameter('custparam_enterpickloc');
			var getlocid = request.getParameter('custparam_enterpicklocid');
			var replenType = request.getParameter('custparam_replentype');
			var getNumber = request.getParameter('custparam_number');
			var beginLocId = request.getParameter('custparam_repbeglocId');
			var enterfromloc = request.getParameter('custparam_enterfromloc');
			var getLanguage = request.getParameter('custparam_language');
			var cartlp = request.getParameter('custparam_cartlpno');
			var vzone = request.getParameter('custparam_zoneno');
			
			
			var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
			var html = "<html><head><title>" + st0 + "</title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
			//Case# 20148749 Refresh Functionality starts
			html = html + "var version = navigator.appVersion;";
			html = html + "document.onkeydown = function (e) {";
			html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
			html = html + "if ((version.indexOf('MSIE') != -1)) { ";
			html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
			html = html + "else {if (keycode == 116)return false;}";
			html = html + "};";
			//Case# 20148749 Refresh Functionality ends
			//html = html + " document.getElementById('cmdSend').focus();";        
			html = html + "</script>";     
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "	<form name='_rf_cluster_no' method='POST'>";
			html = html + "		<table>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>ARE YOU SURE TO DO ITEM EXCEPTION ?";	 
		
			html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
			html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + beginLoc + ">";
			html = html + "				<input type='hidden' name='hdnSku' value='" + sku + "'>";
			html = html + "				<input type='hidden' name='hdnExpQty' value=" + expQty + ">";
			html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
			html = html + "				<input type='hidden' name='hdnid' value=" + repInternalId + ">";
			html = html + "				<input type='hidden' name='hdnclustno' value=" + clustNo + ">";
			html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
			html = html + "				<input type='hidden' name='hdnactendlocationid' value=" + actEndLocationId + ">";
			html = html + "				<input type='hidden' name='hdnactendlocation' value=" + actEndLocation + ">";
			html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
			html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
			html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
			html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
			html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
			html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
			html = html + "				<input type='hidden' name='hdnbatchno' value=" + batchno + ">";
			html = html + "				<input type='hidden' name='hdnnitem' value='" + nextitem + "'>";
			html = html + "				<input type='hidden' name='hdnnextitemno' value=" + nextitemno + ">";
			html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
			html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
			html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
			html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
			html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
			html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
			html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
			html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
			html = html + "				<input type='hidden' name='hdnenterfromloc' value=" + enterfromloc + ">";
			html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
			html = html + "				<input type='hidden' name='hdnCartlpno' value=" + cartlp + ">";
			html = html + "				<input type='hidden' name='hdnvzone' value=" + vzone + ">";		
			
			html = html + "				</td>";
			html = html + "			</tr>"; 
			html = html + "			<tr><td></td></tr>"; 
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st1 + " <input name='cmdSend' id='cmdSend' type='submit' value='ENT'/>";
			html = html + "				" + st2 + "	 <input name='cmdPrevious' type='submit' value='F7'/>";    
			html = html + "				<input type='hidden' name='hdncartlp' value='" + getCartLP + "'>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "		 </table>";
			html = html + "	</form>";
			//Case# 20148882 (added Focus Functionality for Textbox)
			html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
			html = html + "</body>";
			html = html + "</html>";

			response.write(html);
		
	}
	else 
	{
		var optedEvent = request.getParameter('cmdPrevious');
		var Reparray =new Array();
		
		
		Reparray["custparam_repno"] = request.getParameter('hdnNo');
		Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');
		Reparray["custparam_repsku"] = request.getParameter('custparam_repskuno');
		Reparray["custparam_torepexpqty"] = request.getParameter('hdnExpQty');
		Reparray["custparam_repexpqty"] = request.getParameter('hdnnextqty');
		Reparray["custparam_repid"] = request.getParameter('hdnid');
		Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
		Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		Reparray["custparam_error"] =st0; //'INVALID LP';
		
		Reparray["custparam_screenno"] = 'R5';
//		Reparray["custparam_clustno"] = clusterNo;	
		Reparray["custparam_reppicklocno"] = request.getParameter('hdnactendlocationid');
		Reparray["custparam_reppickloc"] = request.getParameter('hdnactendlocation');
		Reparray["custparam_repbeglocId"] = request.getParameter('hdnBeginLocationId');
		Reparray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
		Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
		Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
		Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
		Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');
		Reparray["custparam_batchno"] = request.getParameter('hdnbatchno');
		Reparray["custparam_nextitem"] = request.getParameter('hdnnitem');
		Reparray["custparam_nextitemno"] = request.getParameter('hdnnextitemno');
		Reparray["custparam_repexpqty"] = request.getParameter('hdnExpQty');
		Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');
		Reparray["custparam_number"] = request.getParameter('hdngetNumber');
		Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
		Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
		Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
		Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
		Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
		Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
		Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');
		Reparray["custparam_cartlpno"] = request.getParameter('hdncartlp');
		Reparray["custparam_zoneno"] = request.getParameter('hdnvzone');	
		
		nlapiLogExecution('ERROR','zone',Reparray["custparam_zoneno"]);
		
		var vZoneId=request.getParameter('hdnvzone');
		var getCartLP = request.getParameter('hdncartlp');
		
		var whLocation = request.getParameter('hdnwhlocation');
		var expqty = request.getParameter('hdnnextqty');
		var actQty=0;
		
		if (optedEvent == 'F7') {
			Reparray["custparam_cartlpno"] =  request.getParameter('hdncartlp');			
			response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenitem', 'customdeploy_ebiz_twostepreplenitem', false, Reparray);
		}
		else
		{
			//Zero quantity exception
			var vOrdFormat='';
			var filters = new Array();

			if(request.getParameter('hdnNo')!=null && request.getParameter('hdnNo')!='' && request.getParameter('hdnNo')!='null')
			{
				nlapiLogExecution('ERROR','replenreportNo',request.getParameter('hdnNo'));
				filters.push(new nlobjSearchFilter('name', null, 'is', request.getParameter('hdnNo')));
				vOrdFormat='R';
			}
			if(request.getParameter('custparam_repskuno')!=null && request.getParameter('custparam_repskuno')!='' && request.getParameter('custparam_repskuno')!='null')
			{
				nlapiLogExecution('ERROR','Replenitem',request.getParameter('custparam_repskuno'));
				filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', request.getParameter('custparam_repskuno')));
				vOrdFormat=vOrdFormat + 'I';
			}

			if(request.getParameter('custparam_repbeglocId')!=null && request.getParameter('custparam_repbeglocId')!='' && request.getParameter('custparam_repbeglocId')!='null')
			{
				nlapiLogExecution('ERROR','Replenprimaryloc',request.getParameter('custparam_repbeglocId'));
				filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('custparam_repbeglocId')));
				vOrdFormat=vOrdFormat + 'L';
			}

			if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='' && request.getParameter('custparam_whlocation')!='null')
			{
				nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
				filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));

			}

			if(request.getParameter('hdnenterfromloc')!=null && request.getParameter('hdnenterfromloc')!='' && request.getParameter('hdnenterfromloc')!='null')
			{
				nlapiLogExecution('ERROR','custrecord_actbeginloc',request.getParameter('hdnenterfromloc'));
				filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('hdnenterfromloc')));

			}
			
			if(vZoneId !=null && vZoneId !='' && vZoneId !='null')
			{
				nlapiLogExecution('ERROR','vZoneId',vZoneId);
				filters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));

			}

			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			columns[2] = new nlobjSearchColumn('custrecord_sku');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc');
			columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[9] = new nlobjSearchColumn('name');	
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');	
			columns[11] = new nlobjSearchColumn('custrecord_batch_no');
			
			columns[1].setSort(false);
			columns[2].setSort(false);
			columns[3].setSort(false);
			var TotalWeight =0;

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			if(searchresults != null && searchresults != '')
			{
				
				
				for(var m1=0;m1<searchresults.length;m1++)
				{

					var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresults[m1].getId());
					var InvtRef = transaction.getFieldValue('custrecord_invref_no');
					var vSkuStatus = transaction.getFieldValue('custrecord_sku_status');
					var tasktype = transaction.getFieldValue('custrecord_tasktype');
					var expqty = transaction.getFieldValue('custrecord_expe_qty');
					nlapiLogExecution('ERROR', 'InvtRef', InvtRef);
					transaction.setFieldValue('custrecord_act_qty', 0);
					transaction.setFieldValue('custrecord_wms_status_flag', '24');
					transaction.setFieldValue('custrecord_transport_lp', getCartLP);
					transaction.setFieldValue('custrecord_act_end_date', DateStamp());
					transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
					var result = nlapiSubmitRecord(transaction);
					nlapiLogExecution('ERROR', 'result', result);					

					//}								

					/*	var vContLpNo = searchresults[0].getValue('custrecord_container_lp_no');
				var dorefno =searchresults[0].getValue('custrecord_ebiz_cntrl_no');
				//var accountNumber = getAccountNumber(whLocation);
				var vBatchno = searchresults[0].getValue('custrecord_batch_no');*/

					var vContLpNo = searchresults[m1].getValue('custrecord_container_lp_no');
					//var dorefno =searchresults[m1].getValue('custrecord_ebiz_cntrl_no');
					var dorefno='';
					//var accountNumber = getAccountNumber(whLocation);
					var vBatchno = searchresults[m1].getValue('custrecord_batch_no')

					var qty = parseFloat(expqty) - parseFloat(0);			
					var Systemrules = SystemRuleForStockAdjustment(whLocation);						
					nlapiLogExecution('ERROR', 'Systemrules', Systemrules);

					if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
						Systemrules=null;		

					CreateNewShortPick(searchresults[m1].getId(),expqty,actQty,dorefno,0,vContLpNo,TotalWeight,vBatchno,qty,Systemrules,request);//have to check for wt and cube calculation
				}
				
				var opentaskcount=0;									
				var	opentasksearchresults=getConsolidatedopentaskcount(request.getParameter('hdnNo'),request.getParameter('custparam_repskuno'),request.getParameter('custparam_repbeglocId'));
				if(opentasksearchresults!=null && opentasksearchresults.length>0)
				{
					opentaskcount=opentasksearchresults.length;
				}
				nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);

				Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

				if(opentaskcount >= 1)
				{

					var item=opentasksearchresults[0].getValue('custrecord_sku',null,'group');
					var binloc=opentasksearchresults[0].getText('custrecord_actbeginloc',null,'group');
					/*var ItemType = nlapiLookupField('item', skuno, ['recordType','custitem_ebizbatchlot']);
								if(item==skuno && hdnBeginLocation==binloc && (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ))
								{
									Reparray['custparam_repbatchno']=opentasksearchresults[0].getValue('custrecord_batch_no',null,'group');
									Reparray['custparam_repexpqty']=opentasksearchresults[0].getValue('custrecord_expe_qty',null,'sum');

									response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);
									return;
								}*/
					if(item!=skuno && hdnBeginLocation==binloc )
					{
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenitem', 'customdeploy_ebiz_twostepreplenitem', false, Reparray);
						return;
					}
					else
					{
						response.sendRedirect('SUITELET', ' customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
						return;
					}

				}
				else
				{
					Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

					var searchresults=getOpenTasksCountlp(request.getParameter('hdnNo'));
					if (searchresults != null && searchresults!='') {

						var recNo=0;
						if(getNumber!=0)
							recNo=parseFloat(getNumber);
						nlapiLogExecution('ERROR', 'recNo in taskpriority ',recNo);	

						var SOSearchResult = searchresults[recNo];

						Reparray["custparam_repbegloc"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repbeglocId"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repsku"] = SOSearchResult.getText('custrecord_sku',null,'group');
						Reparray["custparam_repexpqty"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
						Reparray["custparam_repskuno"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
						Reparray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
						Reparray["custparam_reppicklocno"] = SOSearchResult.getValue('custrecord_actendloc',null,'group');
						Reparray["custparam_reppickloc"] = SOSearchResult.getText('custrecord_actendloc',null,'group');
						Reparray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no',null,'group');
						Reparray["custparam_noofrecords"] = SOSearchResult.length;
						Reparray["custparam_clustno"] = clusterNo;
						Reparray["custparam_repno"] = request.getParameter('hdnNo');
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
						return;

					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostep_stageloc', 'customdeploy_ebiz_rf_twostep_stageloc', false, Reparray);
						//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_3', 'customdeploy_ebiz_hook_sl_3', false, Reparray);
					}
				}
				
			}
			
			
			
		}
	}
}

function getConsolidatedopentaskcount(reportNo,getitemid,getlocid)
{

	nlapiLogExecution('ERROR', 'reportNo',reportNo);
	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);


	var filters = new Array();

	if(reportNo != null && reportNo != "" && reportNo !='null')
	{
		nlapiLogExecution('ERROR','name',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	}

	if(getitemid != null && getitemid != ""  && getitemid !='null')
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
	}

	if(getlocid!=null && getlocid!='' && getlocid !='null')
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='')
	{
		nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));
	}



	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
	columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');

	if(getitemid!=null && getitemid!='' && getitemid !='null')
	{
		var ItemType = nlapiLookupField('item', getitemid, ['recordType','custitem_ebizbatchlot']);
		if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
		{
			columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
		}
	}

	columns[1].setSort(false);
	columns[2].setSort(false);
	columns[3].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR', 'searchresults.length ingetreplen',searchresults.length);
	return searchresults;
}
function getOpenTasksCountlp(ReportNo,request)
{
	nlapiLogExecution('ERROR', 'ReportNo', ReportNo);

try
{
	var openreccount=0;
	var filters = new Array();

	if(ReportNo != null && ReportNo != "" && ReportNo !='null')
	{
		nlapiLogExecution('ERROR','name',ReportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', ReportNo));
	}


	filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
	columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
	var fields=new Array();
	//nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
	if(request.getParameter('custparam_repbegloc') != null && request.getParameter('custparam_repbegloc') !='' && request.getParameter('custparam_repbegloc') !='null')
	{
		fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
	}
	var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
	if(sresult!=null && sresult.length>0)
		filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', sresult[0].getId()));
	if(request.getParameter('custparam_repsku') != null && request.getParameter('custparam_repsku') !='' && request.getParameter('custparam_repsku') !='null')
	{
		var ItemType = nlapiLookupField('item', request.getParameter('custparam_repsku'), ['recordType','custitem_ebizbatchlot']);
		if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
		{
			columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
		}
	}	


	columns[1].setSort(false);
	columns[2].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR','searchresults',searchresults.length);
	return searchresults;
}
catch(e)
{
	nlapiLogExecution('ERROR','exception',e);
}

}