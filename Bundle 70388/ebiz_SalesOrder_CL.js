/***************************************************************************
eBizNET Solutions
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_SalesOrder_CL.js,v $
 *     	   $Revision: 1.18.2.15.4.6.2.39.2.4 $
 *     	   $Date: 2015/11/26 16:01:05 $
 *     	   $Author: deepshikha $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *$Log: ebiz_SalesOrder_CL.js,v $
 *Revision 1.18.2.15.4.6.2.39.2.4  2015/11/26 16:01:05  deepshikha
 *2015.2 Issue Fix
 *201415639
 *
 *Revision 1.18.2.15.4.6.2.39.2.3  2015/11/03 12:50:19  schepuri
 *case# 201415334
 *
 *Revision 1.18.2.15.4.6.2.39.2.2  2015/09/24 15:31:06  deepshikha
 *2015.2 issue fixes
 *201414548
 *
 *Revision 1.18.2.15.4.6.2.39.2.1  2015/09/18 13:50:17  snimmakayala
 *Case#: 201414307
 *
 *Revision 1.18.2.15.4.6.2.39  2015/09/02 15:47:36  nneelam
 *case# 201413984
 *
 *Revision 1.18.2.15.4.6.2.38  2015/08/31 15:03:36  deepshikha
 *2015.2 issue fixes
 *201414106
 *
 *Revision 1.18.2.15.4.6.2.37  2015/05/26 19:32:25  sponnaganti
 *Case# 201412922
 *Getting itemtype as "paymentitem" . If item as paymentitem no need to check item dimensions
 *
 *Revision 1.18.2.15.4.6.2.36  2015/05/07 13:27:05  schepuri
 *case #201412600
 *
 *Revision 1.18.2.15.4.6.2.35  2015/05/04 14:06:08  schepuri
 *case# 201412600
 *
 *Revision 1.18.2.15.4.6.2.34  2015/04/13 12:48:17  vmandala
 *case# 201412283
 *packcode validation
 *
 *Revision 1.18.2.15.4.6.2.33  2014/12/24 12:18:23  snimmakayala
 *Case#: 201411313
 *
 *Revision 1.18.2.15.4.6.2.24.2.10  2014/12/23 12:33:35  snimmakayala
 *Case#: 201411313
 *
 *Revision 1.18.2.15.4.6.2.24.2.9  2014/11/13 05:17:27  vmandala
 *case#  201411046 Stdbundle issue fixed
 *
 *Revision 1.18.2.15.4.6.2.24.2.8  2014/09/23 12:27:23  snimmakayala
 *no message
 *
 *Revision 1.18.2.15.4.6.2.24.2.7  2014/09/18 14:57:51  snimmakayala
 *no message
 *
 *Revision 1.18.2.15.4.6.2.24.2.6  2014/09/16 15:54:21  sponnaganti
 *Case# 201410346
 *DCD SB Issue fix
 *
 *Revision 1.18.2.15.4.6.2.24.2.5  2014/09/02 11:08:03  gkalla
 *case#201410058
 *CSV import issue and pending approval issue
 *
 *Revision 1.18.2.15.4.6.2.24.2.4  2014/08/29 19:49:01  grao
 *Case#: 201410171�Nautilus sb  issue fixes
 *
 *Revision 1.18.2.15.4.6.2.24.2.3  2014/07/28 09:25:17  skavuri
 *Case# 20149728 SB Issue Fixed
 *
 *Revision 1.18.2.15.4.6.2.24.2.2  2014/07/24 15:19:34  nneelam
 *case#  20149689
 *New UI Issue Fix.
 *
 *Revision 1.18.2.15.4.6.2.24.2.1  2014/07/11 21:26:05  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 *Alphacommchanges
 *
 *Revision 1.18.2.15.4.6.2.24  2014/05/26 15:05:31  skreddy
 *case # 20148494
 *Sonic SB issue fix
 *
 *Revision 1.18.2.15.4.6.2.23  2014/05/01 15:38:14  skavuri
 *Case# 20148218,20148028 issue fixed
 *
 *Revision 1.18.2.15.4.6.2.22  2014/04/14 15:52:59  nneelam
 *case#  20147990
 *Stanadard Bundle Issue Fix.
 *
 *Revision 1.18.2.15.4.6.2.21  2014/04/08 15:52:20  nneelam
 *case#  20127686
 *Stanadard Bundle Issue Fix.
 *
 *Revision 1.18.2.15.4.6.2.20  2014/03/29 21:32:27  spendyala
 *CASE201112/CR201113/LOG201121
 *Issue fixed related to 20127873
 *
 *Revision 1.18.2.15.4.6.2.19  2014/03/28 15:27:54  skavuri
 *Case # 20127768 issue fixed
 *
 *Revision 1.18.2.15.4.6.2.18  2014/03/27 16:15:41  skavuri
 *Case # 20127686 issue fixed
 *
 *Revision 1.18.2.15.4.6.2.17  2014/03/26 09:46:17  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 *LL issue fix
 *
 *Revision 1.18.2.15.4.6.2.16  2014/03/18 07:23:28  spendyala
 *CASE201112/CR201113/LOG201121
 *Issue fixed related to variable declaration
 *
 *Revision 1.18.2.15.4.6.2.15  2014/01/07 09:40:37  rmukkera
 *Case # 20126322
 *
 *Revision 1.18.2.15.4.6.2.14  2013/12/27 19:19:27  grao
 *Case# 20126322  related issue fixes in Sb issue fixes
 *
 *Revision 1.18.2.15.4.6.2.13  2013/11/14 16:21:42  snimmakayala
 *Standard Bundle Fix.
 *Case# : 20120128
 *FO Creation FIxes
 *
 *Revision 1.18.2.15.4.6.2.12  2013/10/25 20:21:00  snimmakayala
 *GSUSA PROD ISSUE
 *Case# : 20125337
 *
 *Revision 1.18.2.15.4.6.2.11  2013/09/16 15:40:04  rmukkera
 *Case# 20124315
 *
 *Revision 1.18.2.15.4.6.2.10  2013/08/27 16:34:09  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 *stdchanges
 *case#20123755
 *
 *Revision 1.18.2.15.4.6.2.9  2013/05/31 15:02:12  grao
 *CASE201112/CR201113/LOG201121
 *Standard bundle issues fixes
 *
 *Revision 1.18.2.15.4.6.2.8  2013/05/22 08:33:06  grao
 *PO Closed related Validation
 *Case#:20122717 issues fixes in PMM Prod account.
 *
 *Revision 1.18.2.15.4.6.2.7  2013/05/16 14:41:43  schepuri
 *vidation removed on shipdate
 *
 *Revision 1.18.2.15.4.6.2.6  2013/05/14 14:14:55  schepuri
 *Validation after wave generation
 *
 *Revision 1.18.2.15.4.6.2.5  2013/05/01 15:22:27  skreddy
 *CASE201112/CR201113/LOG201121
 *Standard bundle issue fixes
 *
 *Revision 1.18.2.15.4.6.2.4  2013/04/10 05:37:39  snimmakayala
 *CASE201112/CR201113/LOG2012392
 *Prod and UAT issue fixes.
 *
 *Revision 1.18.2.15.4.6.2.3  2013/03/05 13:35:46  rmukkera
 *Merging of lexjet Bundle files to Standard bundle
 *
 *Revision 1.18.2.15.4.6.2.2  2013/03/01 14:34:59  skreddy
 *CASE201112/CR201113/LOG201121
 *Merged from FactoryMation and change the Company name
 *
 *Revision 1.18.2.15.4.6.2.1  2013/02/26 13:02:23  snimmakayala
 *CASE201112/CR201113/LOG2012392
 *Marged from Boombah.
 *
 *Revision 1.18.2.15.4.6  2013/02/19 00:51:39  gkalla
 *CASE201112/CR201113/LOG201121
 *Issue fixing
 *
 *Revision 1.18.2.15.4.5  2013/01/15 02:07:15  kavitha
 *CASE201112/CR201113/LOG201121
 *CASE20121294 - Endochoice SB issue fix
 *
 *Revision 1.18.2.15.4.4  2012/11/01 14:55:22  schepuri
 *CASE201112/CR201113/LOG201121
 *Decimal Qty Conversions
 *
 *Revision 1.18.2.15.4.3  2012/10/30 06:10:07  spendyala
 *CASE201112/CR201113/LOG201121
 *Merged code form 2012.2 branch.
 *
 *Revision 1.18.2.15.4.2  2012/10/11 10:27:04  gkalla
 *CASE201112/CR201113/LOG201121
 *Pick confirmation and other inventory issues
 *
 *Revision 1.18.2.15.4.1  2012/10/02 22:45:03  snimmakayala
 *CASE201112/CR201113/LOG2012392
 *Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 *Revision 1.18.2.15  2012/09/05 14:28:23  schepuri
 *CASE201112/CR201113/LOG201121
 *added date stamp
 *
 *Revision 1.18.2.14  2012/08/30 09:21:36  skreddy
 *CASE201112/CR201113/LOG201121
 *Restricted to changed the item after Wave Generation.
 *
 *Revision 1.18.2.13  2012/08/08 16:56:34  spendyala
 *CASE201112/CR201113/LOG201121
 *Issue related to delete Line while creating new SO is resolved.
 *
 *Revision 1.18.2.12  2012/06/04 14:53:52  spendyala
 *CASE201112/CR201113/LOG201121
 *issue related to parsing the qty.
 *
 *Revision 1.18.2.11  2012/05/25 06:44:58  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 *Subtotaol item validations
 *
 *Revision 1.18.2.10  2012/05/22 07:02:45  rrpulicherla
 *Bypass qty validation
 *
 *Revision 1.18.2.7  2012/04/11 12:28:29  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 *default sku status
 *
 *Revision 1.18.2.6  2012/04/03 14:49:30  spendyala
 *CASE201112/CR201113/LOG201121
 *Issue Related to Delete Fulfillment order is fixed.
 *
 *Revision 1.18.2.5  2012/02/20 15:24:24  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Stable bundle issue fixes
 *
 *Revision 1.18.2.4  2012/02/09 13:06:35  schepuri
 *CASE201112/CR201113/LOG201121
 *issue fixing  related memo textbox validation removed
 *
 *Revision 1.18.2.3  2012/02/07 12:32:38  snimmakayala
 *CASE201112/CR201113/LOG201121
 *UOM Conversion
 *
 *Revision 1.18.2.2  2012/02/02 13:44:21  schepuri
 *CASE201112/CR201113/LOG201121
 *issue fixing related to validation on textbox
 *
 *Revision 1.18.2.1  2012/01/31 13:12:11  schepuri
 *CASE201112/CR201113/LOG201121
 *issue fixing related to validation on textbox
 *
 *Revision 1.18  2011/12/05 14:58:59  snimmakayala
 *CASE201112/CR201113/LOG201121
 *
 *Revision 1.17  2011/11/22 15:25:35  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Code Fine Tunning
 *
 *Revision 1.16  2011/11/22 08:42:55  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Default Lot Functionality. (Item as Lot)
 *
 *Revision 1.15  2011/11/13 00:16:15  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Outbound Inventory Deletion
 *
 *Revision 1.14  2011/11/09 14:47:15  snimmakayala
 *CASE201112/CR201113/LOG201121
 *Ship Complete Functionality
 *
 *Revision 1.13  2011/11/02 19:18:27  gkalla
 *CASE201112/CR201113/LOG201121
 *To populate SKU status and packcode while change the item in SO Line
 *
 *Revision 1.12  2011/11/02 16:17:23  gkalla
 *CASE201112/CR201113/LOG201121
 *To populate SKU status and packcode while change the item in SO Line
 *
 *Revision 1.10  2011/09/30 09:00:33  skdokka
 *CASE201112/CR201113/LOG201121
 *In the onChange function, moved load record in side the if condition
 *
 *Revision 1.9  2011/09/26 11:35:57  rmukkera
 *CASE201112/CR201113/LOG201121
 *hold flag message was deleted
 *
 *Revision 1.8  2011/09/23 14:20:50  rmukkera
 *CASE201112/CR201113/LOG201121
 *Hold Flags condition message added
 *
 *Revision 1.7  2011/09/19 14:03:03  snimmakayala
 *CASE201112/CR201113/LOG201121
 *
 *Revision 1.6  2011/09/09 15:12:14  spendyala
 *Added On Item change  function for auto fulfillment
 *
 *Revision 1.5  2011/09/09 07:08:33  spendyala
 *CASE201112/CR201113/LOG201121
 *functionality added related to auto creation of fulfillment order.
 *
 *Revision 1.4  2011/09/06 22:07:28  skota
 *CASE201112/CR201113/LOG201121
 *
 *Revision 1.3  2011/06/17 14:46:46  skota
 *CASE201112/CR201113/LOG201121
 *code changes in function 'CheckFulfillmentqty' for not allowing the user to enter -ve values etc.,
 *
 *****************************************************************************/

/**
 * To empty the custombody field
 */

var eventtype;
function setDefaultValue(type)
{
	eventtype=type;
	if(type=='edit')
		nlapiSetFieldValue('custbody_ebiz_lines_deleted', '');	
	/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
	/*if(type=='copy')
	{
		dropshipcheck();
	}*/
}

function dropshipcheck()
{
	var  entityId=nlapiGetFieldValue('entity');		

	if(entityId!='')
	{

		var returncheckboxvalue=CheckCustomerDropShip(entityId);
		//	alert('returncheckboxvalue'+returncheckboxvalue);
		nlapiSetFieldValue('custbody_ebiz_blind_dropship', returncheckboxvalue);
		return true;
	}
}
/* Up to here */ 
function onSave()
{
	try {
		//Added on 27th Oct by suman.
		var site=nlapiGetFieldValue('location');
		//alert('site ' + site);
		var vmwhsiteflag='F';
		if(site!=null && site!='')
		{
			var fields = ['custrecord_ebizwhsite'];

			var locationcolumns = nlapiLookupField('Location', site, fields);
			if(locationcolumns!=null)
				vmwhsiteflag = locationcolumns.custrecord_ebizwhsite;
		}
		if(vmwhsiteflag =="T")
		{
		var company=nlapiGetFieldValue('custbody_nswms_company');
		//alert('company ' + company);
		var status=nlapiGetFieldValue('status');
		var Soid=nlapiGetFieldValue('id');
		var lineCnt = nlapiGetLineItemCount('item');
		var orderstatus=nlapiGetFieldValue('orderstatus');
		var orderstatustext=nlapiGetFieldText('orderstatus');
		var orderType =nlapiGetFieldValue('custbody_nswmssoordertype');




		if(orderType!=null && orderType!='')
		{
			//	alert('orderType ' + orderType);
			var filters = new Array();
			filters.push(new nlobjSearchFilter('internalid', null, 'anyof', orderType));
			//alert('site ' + site);
			if(site!=null && site!='')
				filters.push(new nlobjSearchFilter('custrecord_ebizsiteordtyp', null, 'anyof', site));

			var rcsearchresults=nlapiSearchRecord('customrecord_ebiznet_order_type', null,filters,null);

			if(rcsearchresults==null)
			{
				alert('Order type not matched with From location');	
				return false;
			}

		}

		//case 201411046 start
		for (var k = 1; k <= lineCnt; k++) 
		{
			var vPackcode=nlapiGetLineItemValue('item','custcol_nswmspackcode',k);
			var getitem=nlapiGetLineItemValue('item','item',k);
			//var lineloc=nlapiGetLineItemValue('item','location_display',k);
			var vlocation=nlapiGetLineItemValue('item','location',k);// case# 201415334
			//case 201412283
			//alert(lineloc);
			//alert(vlocation);
// case# 201417181
			/*if(vlocation==null || vlocation=='')
			{

				alert("please enter location at the line level" );
				return false;
			}*/

			/*if(vlocation!=site)
			{
				alert("Please select the same location at header and line level");
				return false;
			}*/

//			if(getitem!=0 && parseInt(getitem)>0)// case# 201412600
//			{
////			case# 201412600
//			itemtype = nlapiLookupField('item', getitem, 'recordType');

//			if(itemtype!='descriptionitem' && itemtype!='discountitem' && itemtype!='Payment' && itemtype!='Markup' 
//			&& itemtype!='Subtotal'  && itemtype!='otherchargeitem' && itemtype!='serviceitem' && itemtype!='noninventoryitem'
//			&& itemtype!='NonInvtPart'  && itemtype!='itemgroup' && itemtype!='paymentitem') // 201412922 added paymentitem to if condition
//			{
//			var ItemDimens=CheckItemDimens(getitem,location,vPackcode);

//			if(ItemDimens == '' ||ItemDimens == null)
//			{								
//			alert("packcode not Configured for Item" );							
//			return false;
//			}
//			}
//			}
		}


		var vOldOrdStatus="";
		var sorec;
		if(Soid!=null && Soid>=1)
		{

			sorec=nlapiLoadRecord('salesorder',Soid);
			vOldOrdStatus=sorec.getFieldValue('orderstatus');

		}
		if(eventtype =='edit' && vOldOrdStatus!= null && vOldOrdStatus != '' &&  vOldOrdStatus != 'A' && orderstatus=='A')
		{
			var searchRecordsapprove = getRecordDetailsforapprove(Soid,'noneof');
			if(searchRecordsapprove != null && searchRecordsapprove != '' && searchRecordsapprove.length>0){
				alert("Order is being processed in Main Warehouse.Contact Warehouse Manager." );							
				return false;
			}
		}

		if(company!=null && company!=''){

			var filters = new Array();
			if(site!=null && site!='')
				filters.push(new nlobjSearchFilter('internalid',null,'is',site));
			filters.push(new nlobjSearchFilter('custrecordcompany',null,'anyof',['@NONE@',company]));
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

			var locresult = nlapiSearchRecord('location', null, filters, null);

			//alert('locresult ' + locresult);
			if(locresult == null || locresult == '')
			{
				alert('Entered Company is not related to the location');
				return false;
			}

		}

		/*// Case # 20127768 starts
		if(eventtype =='edit')
		{
			//if(orderstatustext=='Pending Fulfillment')
			//alert(" orderstatustext is"+orderstatustext);
			if(orderstatustext=='Pending Approval')
				{
				alert("Order is being processed in Main Warehouse.You can't modify the Status");
				return false;
				}
		}
		//Case # 20127768 end
		 */

		var itemarray=new Array();
		for (var s = 1; s <= lineCnt; s++) 
		{
			var qty=nlapiGetLineItemValue('item','quantity',s);
			var itemtype=nlapiGetLineItemValue('item','itemtype',s);
			var islineclosed = nlapiGetLineItemValue('item','isclosed',s);
			// Case# 20148218/20148028 starts
			var getitem=nlapiGetLineItemValue('item','item',s);
			var CommitedQty=nlapiGetLineItemValue('item','quantitycommitted',s);
			itemarray.push(getitem);
			//alert ('getitem is'+getitem);
//			var filters = new Array();
//			filters[0] = new nlobjSearchFilter('internalid', null, 'is', getitem);
//			var columns = new Array();
//			columns[0]=new nlobjSearchColumn("isinactive");
//			var searchres=nlapiSearchRecord("item",null,filters,columns);
//			if(searchres!=''&& searchres!=null&& searchres!='null')
//			{
//			var p1=searchres[0].getValue("isinactive");
//			if(p1=='T')
//			{
//			alert("Inactive Items are Not Taken For Order Process" );							
//			return false;
//			}
//			}
			// Case# 20148218/20148028 ends
			//alert(itemtype );	
			if(eventtype=='create' || eventtype=='copy' || eventtype =='edit')
			{
				if(qty == 0 && itemtype!='Description' && itemtype!='Discount' && itemtype!='Payment' && 
						itemtype!='Markup' && itemtype!='Subtotal' && itemtype!='EndGroup' && islineclosed!='T' && CommitedQty !=0)
				{
					alert("Order Quantity should be greaterthan ZERO." );							
					return false;
				}
			}
		}

		if(itemarray!=null && itemarray!='' && itemarray.length>0)
		{
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', itemarray);
			filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'T');
			var columns = new Array();
			columns[0]=new nlobjSearchColumn("isinactive");
			columns[1]=new nlobjSearchColumn("itemid");
			var searchres=nlapiSearchRecord("item",null,filters,columns);
			if(searchres!=null && searchres!='')

			{
				var item=searchres[0].getValue('itemid')
				alert("Inactive Items are Not Taken For Order Process" + item );							
				return false;
			}
		}


		if(Soid!=null && Soid>=1)
		{
			try{
				var sorec=nlapiLoadRecord('salesorder',Soid);

				//var currentLineno=nlapiGetCurrentLineItemValue('item','line');

				//alert(currentLineno);
				for (var s = 1; s <= lineCnt; s++) {
					var actlocation=0;
					var actqty=0;
					var ordqty=nlapiGetLineItemValue('item','quantity',s);
					var location=nlapiGetLineItemValue('item','location',s);
					var Lineno=nlapiGetLineItemValue('item','line',s);
					var LineCommitedQty=nlapiGetLineItemValue('item','quantitycommitted',s);
					for(var actSOcount=1;actSOcount<=sorec.getLineItemCount('item');actSOcount++)
					{
						var actLineno=sorec.getLineItemValue('item','line',actSOcount);
						if(Lineno==actLineno)
						{
							var actqty=sorec.getLineItemValue('item','quantity',actSOcount);
							actlocation=sorec.getLineItemValue('item','location',actSOcount);
							break;
						}
					}
					var actLineNo=sorec.getLineItemValue('item','line',s);
					var itemtype=nlapiGetLineItemValue('item','itemtype',s);
					var islineclosed = nlapiGetLineItemValue('item','isclosed',s);
					//alert(itemtype );	
					if(ordqty == 0 && itemtype!='Description' && itemtype!='Discount' && itemtype!='Payment' && 
							itemtype!='Markup' && itemtype!='Subtotal' && itemtype!='EndGroup' && islineclosed!='T' && LineCommitedQty !=0)
					{

						alert("Order Qunatity should be greaterthan ZERO." );							
						return false;
					}

					if((parseFloat(ordqty) < parseFloat(actqty)) && itemtype!='Description' 
						&& itemtype!='Discount' && itemtype!='Payment' && itemtype!='Markup' && itemtype!='Subtotal' && itemtype!='EndGroup' )
					{
//						var searchRecords = getRecordDetails(Soid,Lineno,'noneof');
						var searchRecords=GetPickGeneratedQty(Soid,Lineno);
						if(searchRecords != null && searchRecords != ''){
							var vqty=searchRecords[0].getValue('custrecord_pickgen_qty',null, 'sum');

							if(vqty==null||vqty=="")
								vqty=0;

							//alert('ordqty' + ordqty);
							//alert('vqty' + vqty);
							if(parseFloat(ordqty) < parseFloat(vqty))
							{
								alert("Order is being processed in Main Warehouse. Do not make quantity changes at Line "+ Lineno+", Contact Warehouse Manager." );							
								return false;
							}
						}
					}
					if(Lineno!=null && Lineno!='' && actLineNo != null && actLineNo != '' && actLineNo == Lineno)//code added to check the condn that the line# is already existed then only allow to search rec or else exit.    
					{
						if(location != actlocation)
						{
							var searchRecords = getRecordDetails(Soid,Lineno,'noneof');
							if(searchRecords != null && searchRecords != '' && searchRecords.length>0){
								alert("Order is being processed in Main Warehouse. Do not make location changes at Line "+ Lineno+", Contact Warehouse Manager." );							
								return false;
							}
						}
					}

				}		
			}
			catch(exp)
			{

			}
		}
		var shipdate = nlapiGetFieldValue('shipdate');
		var currdate = DateStamp();

		if(shipdate != null && shipdate != "")
		{
			shipdate=ConvertToDate(shipdate);
			//commented because this validation doesn't require
			/*var strDatediff = CompareDates(currdate,shipdate);		

			if(strDatediff == false)
			{
				//Added on 27th Oct by suman
				//If the staus is not Fulfilled and Billed we need to check the condn.
				if(status!='Fulfilled'&&status!='Billed')
					alert("Ship Date cannot be lessthan current date");
				return false;
			}*/
		}

		var expshipdate=nlapiGetFieldValue('custbody_nswmspoexpshipdate');
		//alert('expshipdate'+expshipdate);
		if(expshipdate != null && expshipdate != "")
		{
			expshipdate=ConvertToDate(expshipdate);
			var strDatediff = CompareDates(currdate,expshipdate);	

			if(strDatediff == false)
			{
//				alert("Exp Ship Date cannot be lessthan current date");
//				return false;
			}
		}

		var ActualArrivaldate=nlapiGetFieldValue('custbody_nswmsactualarrivaldate');
		//alert('expshipdate'+expshipdate);
		if(ActualArrivaldate != null && ActualArrivaldate != "")
		{
			ActualArrivaldate=ConvertToDate(ActualArrivaldate);
			var strDatediff = CompareDates(currdate,ActualArrivaldate);	

//			if(strDatediff == false)
//			{
//			alert("Actual Arrival Date cannot be lessthan current date");
//			return false;
//			}
		}
		var str=nlapiGetFieldValue('tranid');
		if(str == "")
		{
			alert('Please enter Order #');
			return false;
		}
		var result=ValidateSplCharacter(str,'Order #');
		if(result == false)
		{
			alert("Special Character are not allowed in Order #");
			return false;
		}


		var str=nlapiGetFieldValue('otherrefnum');
		var result=ValidateSplCharacter(str,'PO#');
		if(result == false)
		{
			//alert("Special Character are not alloweed in \n PO #");
			alert("Special Character are not alloweed in PO #");
			return false;
		}

		var str=nlapiGetFieldValue('custbody_shipment_no');
		var result=ValidateSplCharacter(str,'Shipment #');
		if(result == false)
		{
			alert("Special Character are not alloweed in Shipment #");
			return false;
		}

		//case # 20127686
		var itemlevelshipping=nlapiGetFieldValue('ismultishipto');
		if(itemlevelshipping!='T'){
			var shipCarrierMethod = nlapiGetFieldValue('shipmethod');
			if(shipCarrierMethod == null || shipCarrierMethod == ''){
				alert('Please select ship method');
				return false;
			}

		}
		}
	}
	catch (exps) {
		alert('Error ' + JSON.stringify(exps));
		//return false;
	}

	return true;
}

/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/
function CheckCustomerDropShip(entityId)
{
	var returncheckboxvalue="F";
	var entityrecord = nlapiLoadRecord('customer', entityId);

//	var customercheckdropshipflag;
//	if(entityrecord!=null && entityrecord!='')
//	{
//	customercheckdropshipflag=entityrecord.getFieldValue('custentity_ebiz_cust_dropship');
//	}
	var customercheckdropshipflag=entityrecord.getFieldValue('custentity_ebiz_cust_dropship');
	var custlineitemcount=entityrecord.getLineItemCount('addressbook');
	var adressdropshipflag="F";
	for(var customerline=1;customerline<=custlineitemcount;customerline++)
	{
		var custline=parseInt(customerline).toString();
		var dropshiplabel = entityrecord.getLineItemValue('addressbook','label',custline);

		if(dropshiplabel=="DropShip")
		{

			adressdropshipflag="T";

			break;
		}
	} 

	if(customercheckdropshipflag=="F")
	{
		returncheckboxvalue="F";

	}	 
	else if((adressdropshipflag=="T")&&(customercheckdropshipflag=="T"))
	{
		returncheckboxvalue="T";

	}		

	return returncheckboxvalue;
}
/* Up to here */ 
function ValidateSplCharacter(string,name)
{
	var iChars = "*|,\":<>[]{}`\';()@&$#%";
	if(string==null || string=='')
		return true;
	var length=string.length;
	var flag = 'N';
	for(var i=0;i<length;i++)
	{
		if(iChars.indexOf(string.charAt(i))!=-1)
		{
			flag='Y';
			break;
		}
	}
	if(flag == 'Y')
	{
		return false;
	}
	else
	{
		return true;
	}

}

/**
 * This function is to validate for  
 * not entering character into the mobile no ,a/c no and land line no fields. 
 * @param string
 * @returns {Boolean}
 */
function ValidateNumeric(string)
{
	var iChars = "0123456789";
	if(string==null || string=='')
		return true;
	var length=string.length;
	for(var i=0;i<=length;i++)
	{
		if(iChars.indexOf(string.charAt(i))==-1)
		{
			alert("you may only enter number into this field\n");
			break;
			return false;
		}
	}
	return true;
}

function getFulfilmentqty(soid,lineno){
	var vqty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', soid);
	filters[1] = new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineno);


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');

	var searchresultsRcpts = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if (searchresultsRcpts != null) {
		var val = searchresultsRcpts[0];
		vqty=val.getValue('custrecord_ord_qty',null, 'sum');
		return vqty;
	}
	return vqty;
}

//This function is called when the user edits the SO lines in fulfillment order screen
function CheckFulfillmentqty(fld)
{		
	var location = nlapiGetFieldValue('location');
	var linelocation=nlapiGetCurrentLineItemValue('item','location');
	var vmwhsiteflag='F';
	if(linelocation!=null && linelocation!='')
	{
		var fields = ['custrecord_ebizwhsite'];

		var locationcolumns = nlapiLookupField('Location', linelocation, fields);
		if(locationcolumns!=null)
			vmwhsiteflag = locationcolumns.custrecord_ebizwhsite;
	}
	else if(location!=null && location!='')
	{
		var fields = ['custrecord_ebizwhsite'];

		var locationcolumns = nlapiLookupField('Location', location, fields);
		if(locationcolumns!=null)
			vmwhsiteflag = locationcolumns.custrecord_ebizwhsite;
	}
	if(vmwhsiteflag == "T")
	{
		if (nlapiGetCurrentLineItemValue('custpage_deliveryord_items','custpage_deliveryord_expqty') != null)
		{
			//alert('Inside if condition');
			var solineordqty = nlapiGetCurrentLineItemValue('custpage_deliveryord_items','custpage_deliveryord_expqty');
			var solinefulfillmentcreatedqty = nlapiGetCurrentLineItemValue('custpage_deliveryord_items','custpage_deliveryord_fulfillmentcreatedqty');
			var solinefulfillmententeredqty = nlapiGetCurrentLineItemValue('custpage_deliveryord_items','custpage_deliveryord_rcvngqty');
			var soshipcompleteFlag = nlapiGetCurrentLineItemValue('custpage_deliveryord_items','custpage_deliveryord_shipcomplete');

		if(isNaN(solinefulfillmententeredqty) == true)
		{
			alert('Please enter Fulfillment Quantity as a number');		
			return false;
		}
		else if(solinefulfillmententeredqty < 0)
		{
			alert('Fulfillment Quantity cannot be -ve');		
			return false;
		}
		else if(solinefulfillmententeredqty == 0)
		{
			alert('Fulfillment Quantity cannot be 0');		
			return false;
		}

		if(parseFloat(solineordqty) != parseFloat(solinefulfillmententeredqty) && soshipcompleteFlag == 'T')
		{
			alert('This SO has created with Ship Complete flag set ON, partial Qty is not generated');
			return false;
		}


		var totfulfillmentqty = parseFloat(solinefulfillmentcreatedqty) + parseFloat(solinefulfillmententeredqty);

			if(totfulfillmentqty > parseFloat(solineordqty))
			{
				if(parseFloat(solinefulfillmentcreatedqty) > 0)
				{
					solinefulfillmentcreatedqty = solinefulfillmentcreatedqty+"";
					alert('Fulfillment order is already created for qty:' + solinefulfillmentcreatedqty + ', total Fulfillment quantity exceeding Ordered quantity');
				}
				else
					alert('Fulfillment quantity exceeding Ordered quantity');
				return false;
			}
		}
		//case # 20127686
		//case # 20147990
		if(fld=='item'){
//			case # 20149689.........Checking item status againt location.

			var itemstatus=nlapiGetCurrentLineItemValue('item','custcol_ebiznet_item_status'); //Case# 20149728
			var itemStatusFilters = new Array();
			itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
			itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
			if(linelocation != null && linelocation != '')
				itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',linelocation);
			else if(location != null && location != '')
				itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',location);
			var itemStatusColumns = new Array();
			itemStatusColumns[0] = new nlobjSearchColumn('internalid');
			itemStatusColumns[1] = new nlobjSearchColumn('name');
			itemStatusColumns[0].setSort();
			itemStatusColumns[1].setSort();
			var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);
			if (itemStatusSearchResult != null){
				var errorflag='F';
				for (var i = 0; i < itemStatusSearchResult.length; i++) 
				{		
					//if(itemstatus !=itemStatusSearchResult[i].getValue('name'))
					if(itemstatus ==itemStatusSearchResult[i].getId())
					{
						errorflag='T';
					}
				}
				if(errorflag=='F' && vmwhsiteflag == "T"){
					alert('Item Status not match with Location');
					return false;
				}
			}
			var itemlevelshipping=nlapiGetFieldValue('ismultishipto');
			if(itemlevelshipping=='T'){
				var linelevelshipmethod = nlapiGetCurrentLineItemValue('item','shipmethod_display');
				if(linelevelshipmethod==null || linelevelshipmethod == ''){
					alert('Please select ship via');
					return false;
				}
			}
		}



	var Soid=nlapiGetFieldValue('id');
	var currentLineno=nlapiGetCurrentLineItemValue('item','line');

	//alert(currentLineno);	

	if(eventtype =='edit')
	{
		if(Soid!=null && Soid!='' && currentLineno!=null && currentLineno!='' ) 
		{
			if(currentLineno != null && currentLineno != '')
			{
				var ordersearchResult = getRecordDetails(Soid,currentLineno,'noneof');
				if(ordersearchResult != null && ordersearchResult != '')
				{
					//Case # 20126322� Start
					var oldOrderQty=  ordersearchResult[0].getValue('custrecord_ord_qty',null,'sum');
					var orderQty=nlapiGetCurrentLineItemValue('item','quantity');

					if(parseFloat(oldOrderQty)> parseFloat(orderQty))
					{
						//Case # 20126322� End

						alert("order is being proceesed,cannot edit");
						return false;
						//Case # 20126322� Start
					}
					//Case # 20126322� End
				}
			}
		}
	}
	}
	return true;
}

function onInsertLine(type)
{
	var Soid=nlapiGetFieldValue('id');
	//alert('Soid : '+Soid);

	if(Soid!=null && Soid!='')
	{
		try
		{
			var lineindex=nlapiGetCurrentLineItemIndex('item');
			//alert('lineindex : '+lineindex);
			var seletedline = nlapiGetCurrentLineItemValue('item', 'line');	
			//alert('seletedline : '+seletedline);
			var sorec=nlapiLoadRecord('salesorder',Soid);
			var seletedlocation = nlapiGetCurrentLineItemValue('item', 'location');	
			//alert('seletedlocation : '+seletedlocation);
			var actlocation=sorec.getLineItemValue('item','location',seletedline);
			//alert('actlocation : '+actlocation);
			var searchRecords = getRecordDetails(Soid,seletedline,'noneof');

			if(searchRecords!=null && searchRecords!='' && searchRecords.length>0)
			{
				if(seletedlocation != actlocation)
				{
					alert("Order is being processed in Main Warehouse. Do not make location changes, Contact Warehouse Manager." );	
					nlapiSetCurrentLineItemValue('item','location',actlocation,false);	
					return true;
				}
			}
		}
		catch(exp)
		{

		}
	}
}



/**
 * client event when user removes line in an SO
 * @param type
 * @returns {Boolean}
 */

function DeleteSoLine(type)
{
	var soId=nlapiGetFieldValue('id');
	var lineNo =nlapiGetCurrentLineItemValue('item','line');

	/*alert("soId :" + soId);
	alert("lineNo :" + lineNo);*/
	if(soId!=null && soId!='' && lineNo!=null && lineNo!='' ) //added by santosh on 27july2012 (if and else condition)
	{
		var searchRecords = getRecordDetails(soId,lineNo,'noneof');
		var getLineNo=nlapiGetFieldValue('custbody_ebiz_lines_deleted');
		var afterconcatinate;

		if(searchRecords != null && searchRecords!='' )
		{
			alert("This Line Item is already considered for outbound process in WMS, so you cannot delete it.");
			return false;		
		}
		else
		{
			if(getLineNo==null||getLineNo=='')
				afterconcatinate=lineNo.toString()+',';
			else
				afterconcatinate=getLineNo+lineNo.toString()+',';
			nlapiSetFieldValue('custbody_ebiz_lines_deleted', afterconcatinate, 'F', true);		
			return true;
		}
	}
	else
	{
		return true;
	}
}



/**
 * searching for records
 * @param soId
 * @param lineNo
 * @returns
 */
function getRecordDetails(soId,lineNo,condn){
	var filter= new Array();

	filter.push(new nlobjSearchFilter('name', null, 'is', soId));
	filter.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineNo));
	filter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, condn, ['25','29']));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_ordline',null,'group');
	columns[2] = new nlobjSearchColumn('name',null,'group');

	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, columns);
	return searchRecords;
}



function getRecordDetailsforapprove(soId,condn){
	var filter= new Array();

	filter.push(new nlobjSearchFilter('name', null, 'is', soId));	
	filter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, condn, [25]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_ordline',null,'group');
	columns[2] = new nlobjSearchColumn('name',null,'group');

	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, columns);
	return searchRecords;
}

/**
 * This will trigger when user change the Item in the item sublist and 
 * checks for the status flag.If the status is not 'E' then it will not allow user to change the item.
 * @param type
 * @param name
 * @param line
 * @returns {Boolean}
 */
function onChange(type,name,line)
{
	try
	{
		var soId=nlapiGetFieldValue('id');
		if(soId != null && soId >= 1)
		{
			if(name=='custbody_create_fulfillment_order')
			{
				var searchresults = nlapiLoadRecord('salesorder', soId);
				var chkflag = nlapiGetFieldValue('custbody_create_fulfillment_order');
				var checkedStatus=searchresults.getFieldValue('custbody_create_fulfillment_order');
				if(checkedStatus=='T' && chkflag=='F')
				{
					var result=confirm('This SO has created with Create Fulfillment Order flag set ON, if you deselect this flag it will impact the fulfillment orders.');
					if(result==false)
					{
						nlapiSetFieldValue('custbody_create_fulfillment_order','T');
					}
				}

			}


			if(name=='quantity')
			{
				var lineNo = nlapiGetCurrentLineItemValue('item','line');
				var lineCnt = nlapiGetLineItemCount('item');
				//alert("lineNo:" + lineNo);
				//alert("soId:" + soId);
				if(soId!=null && soId!='' && lineNo!=null && lineNo!='' ) 
				{
					var searchRecords = getRecordDetails(soId,lineNo,'noneof');

					if(searchRecords != null && searchRecords != '' && searchRecords.length>0)
					{				
						var searchresults = nlapiLoadRecord('salesorder', soId);
						for (var s = 1; s <=lineCnt; s++) {
							var vLineno=searchresults.getLineItemValue('item','line',s);
							var vOrderQty=searchresults.getLineItemValue('item','quantity',s);
							var vlinechangedQty=nlapiGetCurrentLineItemValue('item','quantity');

							//alert("vlinechangedQty:" + vlinechangedQty);
							//alert("vOrderQty:" + vOrderQty);

							if(vlinechangedQty < vOrderQty ){
								var Itemid=searchresults.getLineItemValue('item','item',s);
								alert("Order is being processed in Main Warehouse. Do not make Qty changes, Contact Warehouse Manager." );							
								nlapiSetCurrentLineItemValue('item','quantity',vOrderQty, false,true);
								return false;
							}

						}
					}
				}

			}
			if(name=='item'||name=="custcol_ebiznet_item_status")
			{
				var lineNo = nlapiGetCurrentLineItemValue('item','line');
				var lineCnt = nlapiGetLineItemCount('item');

				var searchRecords = getRecordDetails(soId,lineNo,'noneof');

				if(searchRecords != null && searchRecords != '' && searchRecords.length>0)
				{				
					var searchresults = nlapiLoadRecord('salesorder', soId);
					for (var s = 1; s <=lineCnt; s++) {
						var vLineno=searchresults.getLineItemValue('item','line',s);
						var vOldItem=searchresults.getLineItemValue('item','item',s);
						var vOldItemstatus=searchresults.getLineItemValue('item','custcol_ebiznet_item_status',s);
						var vItem=nlapiGetCurrentLineItemValue('item','item');
						var vItemstatus=nlapiGetCurrentLineItemValue('item','custcol_ebiznet_item_status');
						if((vLineno==lineNo)&&(vOldItem!=vItem)){
							var Itemid=searchresults.getLineItemValue('item','item',s);
							alert("Order is being processed in Main Warehouse. Do not make Item changes, Contact Warehouse Manager." );							
							nlapiSetCurrentLineItemValue('item','item',Itemid, false,true);
							return false;
						}
						if((vLineno==lineNo)&&(vOldItemstatus!=vItemstatus))
						{
							alert("Order is being processed in Main Warehouse. Do not make ItemStatus changes, Contact Warehouse Manager." );							
							nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',vOldItemstatus, false,true);
							return false;
						}
					}
				}
				/*			else
				{
					var seleteditem = nlapiGetCurrentLineItemValue('item', 'item');		

					if(seleteditem != null && seleteditem != '')
					{		
						var fields = ['custitem_ebizdefpackcode','custitem_ebizdefskustatus','custitem_ebizoutbounddefskustatus'];
						var columns = nlapiLookupField('item', seleteditem, fields);

						if(columns != null && columns != '')
						{
							var skuStatus = columns.custitem_ebizdefskustatus;
							var ouboundskuStatus = columns.custitem_ebizoutbounddefskustatus;
							var packCode= columns.custitem_ebizdefpackcode; 


							if(ouboundskuStatus != null && ouboundskuStatus != '')
							{
								nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',ouboundskuStatus,false);
							}
							else
							{
								if(skuStatus != null && skuStatus != '')
									nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',skuStatus,false);	
							}


							if(packCode != null && packCode != '')
								nlapiSetCurrentLineItemValue('item','custcol_nswmspackcode',packCode,false);				 
						}			
					}
					return true;
			}*/
			}

			if(name=='shipcomplete')
			{
				var sorec=nlapiLoadRecord('salesorder',soId);
				var shipcomplete = nlapiGetFieldValue('shipcomplete');
				var checkedshipcomplete=sorec.getFieldValue('shipcomplete');
				if(checkedshipcomplete=='T' && shipcomplete=='F')
				{
					var result=confirm('This SO has created with Ship Complete flag set ON, if you deselect this flag it will impact the fulfillment orders.');
					if(result==false)
					{
						nlapiSetFieldValue('shipcomplete','T');
					}
				}
				else if(checkedshipcomplete=='F' && shipcomplete=='T')
				{
					var result=confirm('This SO has created with Ship Complete flag set OFF, if you select this flag it will impact the fulfillment orders.');
					if(result==false)
					{
						nlapiSetFieldValue('shipcomplete','F');
					}
				}
			}
		}


		if (name == 'isclosed') 
		{		
			var closedflag = nlapiGetCurrentLineItemValue('item', 'isclosed');			
			var lineNo = nlapiGetCurrentLineItemValue('item','line');
			var lineCnt = nlapiGetLineItemCount('item');

			var searchRecords = getRecordDetailsForClose(soId,lineNo,'noneof');

			if(searchRecords != null && searchRecords != '' && searchRecords.length>0)
			{	
				var vOrdQty = searchRecords[0].getValue('custrecord_ord_qty',null,'sum');

				var searchresults = nlapiLoadRecord('salesorder', soId);
				for (var s = 1; s <=lineCnt; s++) {
					var vLineno=searchresults.getLineItemValue('item','line',s);
					var vOldClosedFalg=searchresults.getLineItemValue('item','isclosed',s);

					if((vLineno==lineNo)&&(vOldClosedFalg=='F' && closedflag=='T') && (vOrdQty!=0)){
						alert("Order is being processed in Main Warehouse. Do not close the line, Contact Warehouse Manager." );							
						nlapiSetCurrentLineItemValue('item','isclosed',vOldClosedFalg, false,true);
						return false;
					}
				}
			}
		}
		//alert(name);
		if (name == 'void') 
		{		
			var closedflag = nlapiGetCurrentLineItemValue('item', 'voided');			
			var lineNo = nlapiGetCurrentLineItemValue('item','line');
			var lineCnt = nlapiGetLineItemCount('item');

			var searchRecords = getRecordDetailsForClose(soId,lineNo,'noneof');

			if(searchRecords != null && searchRecords != '' && searchRecords.length>0)
			{		
				var vOrdQty = searchRecords[0].getValue('custrecord_ord_qty',null,'sum');

				var searchresults = nlapiLoadRecord('salesorder', soId);
				for (var s = 1; s <=lineCnt; s++) {
					var vLineno=searchresults.getLineItemValue('item','line',s);
					var vOldClosedFalg=searchresults.getLineItemValue('item','isclosed',s);

					if((vLineno==lineNo)&&(vOldClosedFalg=='F' && closedflag=='T') && (vOrdQty!=0)){
						alert("Order is being processed in Main Warehouse. Do not close the line, Contact Warehouse Manager." );							
						nlapiSetCurrentLineItemValue('item','isclosed',vOldClosedFalg, false,true);
						return false;
					}
				}
			}
		}

		// Code to set default item status and packcode
		if (name == 'item' || name=='location') 
		{
			var seleteditem = nlapiGetCurrentLineItemValue('item', 'item');
			var fromLocation = nlapiGetCurrentLineItemValue('item','location');//To get line level location

			if(fromLocation == null || fromLocation =='')
			{
				fromLocation = nlapiGetFieldValue('location');	//To get Header line level location
			}        
			if(seleteditem != null && seleteditem != '' && fromLocation != null && fromLocation != '')
			{	
				var searchresult = SetItemStatus("SalesOrder", seleteditem, fromLocation, null);

				if (searchresult != null && searchresult != '') 
				{
					nlapiSetCurrentLineItemValue('item', 'custcol_ebiznet_item_status', searchresult[0], false);
					if(searchresult[1] != null && searchresult[1] != '')
						nlapiSetCurrentLineItemValue('item', 'custcol_nswmspackcode', searchresult[1], false);
				}

				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', seleteditem));
				filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
				filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiznsuom');
				columns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');
				//alert('item' + seleteditem);
				var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
				if (skuDimsSearchResults != null && skuDimsSearchResults != '') 
				{
					var vDefNSUOM = skuDimsSearchResults[0].getText('custrecord_ebizuomskudim');
					//alert(vDefNSUOM);
					if (vDefNSUOM != null && vDefNSUOM != '')
						nlapiSetCurrentLineItemValue('item', 'custcol_nswmssobaseuom', vDefNSUOM, false);
				}
			}
		}
		if(name=="custcol_ebiznet_item_status")
		{
			var seleteditemstatus = nlapiGetCurrentLineItemValue('item', 'custcol_ebiznet_item_status');
		}
	}
	catch(exp)
	{
		return true;
	}
}

/**
 * Get date from string
 * @param strDate
 * @returns {Date}
 */
function getDateForString(strDate){
	var array = new Array();
	array = strDate.split('/');

	var dtSettings = DateSetting();

	if(dtSettings == 'DD')
	{
		return new Date(array[2], parseFloat(array[1] -1),array[0]);//YYYY/MM/DD
	}
	else
	{
		return new Date(array[2], parseFloat(array[0] - 1), array[1]);//YYYY/MM/DD
	}
}

/**
 * Compare dates
 * @param str1
 * @param str2
 * @returns TRUE:if str1 is before str2 / FALSE: if str1 is after str2
 */
function CompareDates(str1, str2){
	var retVal = false;

	var date1 = getDateForString(str1);
	var date2 = getDateForString(str2);
	if (date1 <= date2)
		retVal = true;
	else
		retVal = false;

	return retVal;
}


function GetPickGeneratedQty(soId,lineNo)
{
	var pickgenQty=0;
	var searchRecords = new Array();
	try
	{		
		var filter= new Array();
		filter.push(new nlobjSearchFilter('name', null, 'is', soId));
		filter.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineNo));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_pickgen_qty',null,'sum');
		columns[1] = new nlobjSearchColumn('custrecord_ordline',null,'group');
		columns[2] = new nlobjSearchColumn('name',null,'group');

		searchRecords = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, columns);
	}	
	catch(exp)
	{

	}
	return searchRecords;
}

function ConvertToDate(vdate)
{
	if(vdate != null && vdate != '')
	{	
		vdate=nlapiStringToDate(vdate);
		//alert('shipdate'+shipdate);
		var vReturnDate = (parseFloat(vdate.getMonth()) + 1) + '/' + (parseFloat(vdate.getDate())) + '/' + vdate.getFullYear();
		//alert('shipdate'+shipdate);
		return vReturnDate;
	}
	else
		return null;
} 

function DateSetting()
{
	var flag = 'MM';
	return flag;

}

function DateStamp(){
	var now = new Date();

	var dtsettingFlag = DateSetting();
	if(dtsettingFlag == 'DD')
	{
		return ((parseFloat(now.getDate())) + '/' + (parseFloat(now.getMonth()) + 1) + '/' +now.getFullYear());
	}
	else
	{
		return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
	}
}
/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
function onItemchangeinTO(type,name,line)
{
	if(name=='item')
	{
		var seleteditem = nlapiGetCurrentLineItemValue('item', 'item');		
		var fromLocation = nlapiGetFieldValue('location');
		var toLocation = nlapiGetFieldValue('transferlocation');
		var searchresult=SetItemStatus("TransferOrder",seleteditem,fromLocation,toLocation);

		nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',searchresult[0],false);
		nlapiSetCurrentLineItemValue('item','custcol_nswmspackcode',searchresult[1],false);	
	}
}
/* Up to here */ 

/**
 * searching for records
 * @param soId
 * @param lineNo
 * @returns
 */
function getRecordDetailsForClose(soId,lineNo,condn){
	var filter= new Array();

	filter.push(new nlobjSearchFilter('name', null, 'is', soId));
	filter.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineNo));
	filter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, condn, [25,14]));
	filter.push(new nlobjSearchFilter('custrecord_ord_qty', null, 'greaterthan', 0));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_ordline',null,'group');
	columns[2] = new nlobjSearchColumn('name',null,'group');

	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, columns);
	return searchRecords;
}