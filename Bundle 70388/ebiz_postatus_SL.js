/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/ebiz_postatus_SL.js,v $
 *     	   $Revision: 1.8.4.1.8.13.2.1 $
 *     	   $Date: 2015/11/24 11:23:05 $
 *     	   $Author: aanchal $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_179 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_postatus_SL.js,v $
 * Revision 1.8.4.1.8.13.2.1  2015/11/24 11:23:05  aanchal
 * 2015.2 issue fix
 * 201415681
 *
 * Revision 1.8.4.1.8.13  2015/04/10 21:28:10  skreddy
 * Case# 201412323�
 * changed the url path which was hard coded
 *
 * Revision 1.8.4.1.8.12  2014/03/07 16:05:24  skavuri
 * Case# 20127588 issue fixed
 *
 * Revision 1.8.4.1.8.11  2014/02/14 00:06:43  nneelam
 * case#  20127155
 * Fetching item status from open or closed task
 *
 * Revision 1.8.4.1.8.10  2014/02/11 15:27:56  sponnaganti
 * case# 20127116
 * (tempexceptionqty variable added)
 *
 * Revision 1.8.4.1.8.9  2014/02/07 15:19:02  sponnaganti
 * case# 20127102
 * (if condition added for checking exceptionqty null or empty)
 *
 * Revision 1.8.4.1.8.8  2013/11/29 15:33:48  grao
 * Case# 20125981 related issue fixes in SB 2014.1
 *
 * Revision 1.8.4.1.8.7  2013/11/27 15:54:11  grao
 * Case# 20125950 related issue fixes in SB 2014.1
 *
 * Revision 1.8.4.1.8.6  2013/09/12 15:35:34  rmukkera
 * Case# 20124353
 *
 * Revision 1.8.4.1.8.5  2013/09/06 00:26:37  nneelam
 * Case#.20124298�
 * Put Conf Qty  Issue Fix..
 *
 * Revision 1.8.4.1.8.4  2013/08/29 15:49:17  skreddy
 * Case# 20124116,20124117
 * PO status report checkin qty
 *
 * Revision 1.8.4.1.8.3  2013/08/27 16:06:40  nneelam
 * Case#.  20123968
 * PO Status Report
 *
 * Revision 1.8.4.1.8.2  2013/08/21 16:48:35  grao
 * Issue Fix related to
 * 20123968
 *
 * Revision 1.8.4.1.8.1  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.8.4.1.4.3  2013/03/07 13:27:15  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8.4.1.4.2  2013/03/07 11:36:04  grao
 * no message
 *
 * Revision 1.8.4.1.4.1  2013/03/06 15:27:17  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8.4.1  2012/04/11 12:42:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * po status report
 *
 * Revision 1.8  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/10 17:59:45  skota
 * CASE201112/CR201113/LOG201121
 * Added filter for not retrieving Tax related records
 *
 * Revision 1.6  2011/06/06 15:20:34  skota
 * CASE201112/CR201113/LOG201121
 * Fixed the issue of displaying blank record
 *
 * Revision 1.5  2011/05/30 13:46:04  skota
 * CASE201112/CR201113/LOG201121
 * Retrieving SKU STATUS from purchaseorder
 *
 * Revision 1.4  2011/05/30 12:09:59  skota
 * CASE201112/CR201113/LOG201121
 * Fixed the issue of showing first line check-in qty even though remaining PO lines are not checked-in
 *
 * Revision 1.3  2011/05/30 11:34:50  skota
 * CASE201112/CR201113/LOG201121
 * To retrieve data for specific PO line from tables: PurchaseOrder and customrecord_ebiznet_order_line_details
 * 
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function ebiznet_POStatus_SL(request, response){
    if (request.getMethod() == 'GET') {
    //    var form = nlapiCreateForm('PO / TO Status');     
       
        // add PO drop down
      //  var selectPO = form.addField('custpage_po', 'select', 'PO / ASN #').setLayoutType('endrow');
        // Populate all PO's and its IDs into drop down
//        var filterspo = new Array();
//        filterspo[0] = new nlobjSearchFilter('mainline', null, 'is', 'T');
//        var colspo = new Array();
//        colspo[0]=new nlobjSearchColumn('tranid');
//        colspo[0].setSort(true);
//        var searchresults = nlapiSearchRecord('purchaseorder', null, filterspo, colspo);
//        if (searchresults != null)
//        {
//	        //for (var i = 0; i < Math.min(500, searchresults.length); i++) 
//        	for (var i = 0; i < searchresults.length; i++)
//	        {	        
//	            selectPO.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('tranid'));
//	        }
//        }
        
        
        var form = nlapiCreateForm('PO Status');
		var purchaseorder = form.addField('custpage_po', 'text', 'PO/TO/RMA #');

		purchaseorder.setMandatory(true);

		var button = form.addSubmitButton('Display');

		response.writePage(form);
        
        
    }
    else {
    
        var form = nlapiCreateForm('PO Status');
        
        //form.addPageLink('crosslink', 'Go Back', 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=68&deploy=1');
        var linkURL = nlapiResolveURL('SUITELET', 'customscript_postatus', 'customdeploy_postatus_di');
        form.addPageLink('crosslink', 'Go Back',linkURL);
        
        // add PO drop down
//        var selectPO = form.addField('custpage_po', 'select', 'PO / ASN #').setLayoutType('startrow');
//        selectPO.setDefaultValue(request.getParameter('custpage_po'));
//        // Populate all PO's and its IDs into drop down
//        var filterspo = new Array();
//        filterspo[0] = new nlobjSearchFilter('mainline', null, 'is', 'T');
//        var colspo = new Array();
//        colspo[0]=new nlobjSearchColumn('tranid');
//        colspo[0].setSort(true);
//        var searchresults = nlapiSearchRecord('purchaseorder', null, filterspo, colspo);
//        if (searchresults != null)
//        {
//	        //for (var i = 0; i < Math.min(500, searchresults.length); i++) 
//        	for (var i = 0; i < searchresults.length; i++)        	
//        	{	        
//	            selectPO.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('tranid'));
//	        }
//        }
        
        
		var poidno = request.getParameter('custpage_po');
		var purchaseorder = form.addField('custpage_po', 'text', 'PO / TO #');
		purchaseorder.setMandatory(true);
		purchaseorder.setDefaultValue(poidno);

		var poid = GetPOInternalId(poidno);
		
		if(poid != null && poid != "")
		{
			nlapiLogExecution('ERROR', 'poid',poid);
			var button = form.addSubmitButton('Display');

			var potext = form.addField('custpage_textpo', 'text', 'PO').setLayoutType('startrow');
			potext.setDisplayType('inline');
			var RTypetext = form.addField('custpage_textrtype', 'text', 'Receipt Type');
			RTypetext.setDisplayType('inline');
			var statustext = form.addField('custpage_textstatus', 'text', 'Status');
			statustext.setDisplayType('inline');
			var carriertext = form.addField('custpage_textcarrier', 'select', 'Carrier', 'Temp S');
			carriertext.setDisplayType('inline');
			var suppliertext = form.addField('custpage_textsupplier', 'select', 'Supplier', 'vendor');
			suppliertext.setDisplayType('inline');
			var sonumbertext = form.addField('custpage_textsonumber', 'text', 'Supplier Order Number');
			sonumbertext.setDisplayType('inline');

			var podatetext = form.addField('custpage_textpodate', 'text', 'Purchase Order Date');
			podatetext.setDisplayType('inline');
			var exparrdatetext = form.addField('custpage_textexparrdate', 'text', 'Expected Arrival Date');
			exparrdatetext.setDisplayType('inline');
			var expshpdatetext = form.addField('custpage_textexpshpdate', 'text', 'Expected Ship Date');
			expshpdatetext.setDisplayType('inline');

			//        var lmethodtext = form.addField('custpage_textlmethod', 'text', 'Load Method');
			//        lmethodtext.setDisplayType('inline');
			//        var spointtext = form.addField('custpage_textspoint', 'text', 'Ship Point');
			//        spointtext.setDisplayType('inline');
			//        var plisttext = form.addField('custpage_textplist', 'text', 'Packing List');
			//        plisttext.setDisplayType('inline');
			var instext = form.addField('custpage_textins', 'text', 'Instructions');
			instext.setDisplayType('inline');
			//        var closedtext = form.addField('custpage_textclosed', 'text', 'Closed');
			//        closedtext.setDisplayType('inline');
			//        var trlnotext = form.addField('custpage_texttrlno', 'text', 'Trailer Number');
			//        trlnotext.setDisplayType('inline');
			//        var trlarrdatetext = form.addField('custpage_texttrlarrdate', 'text', 'Trailer Arrival Date');
			//        trlarrdatetext.setDisplayType('inline');
			//        var trldepdatetext = form.addField('custpage_texttrldepdate', 'text', 'Trailer Departure Date');
			//        trldepdatetext.setDisplayType('inline');
			//        var snotext = form.addField('custpage_textsno', 'text', 'Seal Number');
			//        snotext.setDisplayType('inline');

			var sublist = form.addSubList("custpage_items", "list", "PO Status Report List");
			sublist.addField("custpage_line", "text", "Line#");
			sublist.addField("custpage_itemname", "select", "Item", "item").setDisplayType('inline');
			sublist.addField("custpage_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDisplayType('inline');
			//sublist.addField("custpage_lotbatch", "text", "Lot/Batch#");
			//sublist.addField("custpage_lp", "text", "LP#");
			sublist.addField("custpage_uom", "text", "UOM");
			sublist.addField("custpage_quantity", "text", "Order Qty");
			sublist.addField("custpage_rcvquantity", "text", "Check-In Qty");
			sublist.addField("custpage_genquantity", "text", "Put Gen Qty");
			sublist.addField("custpage_confquantity", "text", "Put Conf Qty");
			sublist.addField("custpage_exceptionquantity", "text", "Exception Qty");

			nlapiLogExecution('DEBUG', 'PO', request.getParameter('custpage_po'));

						
			
			nlapiLogExecution('Error', 'poid', poid);

			var trantype = nlapiLookupField('transaction', poid, 'recordType');
			nlapiLogExecution('ERROR', 'trantype', trantype);
			
			var transaction;
			if(trantype!='purchaseorder')
			{
				transaction = nlapiLoadRecord('transferorder', poid);
			}
			else
			{
				transaction = nlapiLoadRecord('purchaseorder', poid);
			}
			
			//var transaction = nlapiLoadRecord('purchaseorder', request.getParameter('custpage_po'));
			potext.setDefaultValue(transaction.getFieldValue('tranid'));
			RTypetext.setDefaultValue(transaction.getFieldText('custbody_nswmsporeceipttype'));
			nlapiLogExecution('ERROR', 'PO Status', transaction.getFieldValue('status'));
			statustext.setDefaultValue(transaction.getFieldValue('status'));
			carriertext.setDefaultValue(transaction.getFieldValue('custbody_poshippingitem'));
			suppliertext.setDefaultValue(transaction.getFieldValue('entity'));
			sonumbertext.setDefaultValue(transaction.getFieldValue('otherrefnum'));
			podatetext.setDefaultValue(transaction.getFieldValue('trandate'));
			exparrdatetext.setDefaultValue(transaction.getFieldValue('custbody_nswmspoarrdate'));
			expshpdatetext.setDefaultValue(transaction.getFieldValue('custbody_nswmspoexpshipdate'));
			instext.setDefaultValue(transaction.getFieldValue('custbody_nswmspoinstructions'));

			nlapiLogExecution('ERROR', 'request.getParameter(' + 'custpage_po' + ')', request.getParameter('custpage_po'));
			// loading sublist  from transaction ord details


			// To retrieve data for specific PO line from tables: PurchaseOrder and customrecord_ebiznet_order_line_details 

			var polinefilters = new Array();
			//polinefilters[0] = new nlobjSearchFilter('internalid', null, 'is', request.getParameter('custpage_po'));
			polinefilters[0] = new nlobjSearchFilter('internalid', null, 'is', poid);			
			polinefilters[1] = new nlobjSearchFilter('billable', null, 'is', 'false');
			polinefilters[2] = new nlobjSearchFilter('item', null, 'noneof','@NONE@');
			//case start 20125951  
			polinefilters[3] = new nlobjSearchFilter('taxline', null, 'is', 'F');
		  //case end 20125951
			// To filter null value records from the search results
//			polinefilters[3] = new nlobjSearchFilter('taxcode', 'item', 'noneof','@NONE@'); // For not retrieving Tax related records

			var polinecolumns = new Array();
			polinecolumns[0] = new nlobjSearchColumn('line');
			polinecolumns[1] = new nlobjSearchColumn('item');
			polinecolumns[2] = new nlobjSearchColumn('quantity');
			polinecolumns[3] = new nlobjSearchColumn('custcol_ebiznet_item_status');
			polinecolumns[4] = new nlobjSearchColumn('formulanumeric');
			polinecolumns[4].setFormula('ABS({quantity})');
			
			var posearchresults = nlapiSearchRecord('purchaseorder', null, polinefilters, polinecolumns);
			if(posearchresults==null)
			{
				nlapiLogExecution('ERROR', 'transferorder', 'transferorder');
				polinefilters[3] = new nlobjSearchFilter('transactionlinetype', null, 'anyof', 'ITEM');
				//polinefilters[4] = new nlobjSearchFilter('mainline', null, 'is', 'F');
						
				posearchresults=nlapiSearchRecord('transferorder',null,polinefilters,polinecolumns);
			}
			
			var line, itemname,  uom, ordqty, pconfqty,exceptionqty;
			var itemstatus='';
			//case # 20127155, getting item status from open / closed task 
			
			nlapiLogExecution('ERROR', 'posearchresults.length', posearchresults.length);
			if (posearchresults != null)
			{            	
				for (var i = 0; i < posearchresults.length; i++)  // Even though PO is having one line, search results are giving two records, assumtion is first record might be expense record 
				{     		

					line = posearchresults[i].getValue('line');
					itemname = posearchresults[i].getValue('item');
					ordqty = posearchresults[i].getValue('formulanumeric');	
					
					nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
					nlapiLogExecution('ERROR', 'ordqty', ordqty);
					nlapiLogExecution('ERROR', 'line', line);
					nlapiLogExecution('ERROR', 'itemname', itemname);
					nlapiLogExecution('ERROR', 'poidno', poidno);
					
			
					
					
					var itemstatusopfilters = new Array();
					itemstatusopfilters[0] = new nlobjSearchFilter('custrecord_line_no', null, 'equalto', line);
					itemstatusopfilters[1] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', itemname);
					itemstatusopfilters[2] = new nlobjSearchFilter('name', null, 'is', poidno);
					
					var itemstatusopcolumns = new Array();
					itemstatusopcolumns[0] = new nlobjSearchColumn('custrecord_sku_status');
					
					
					var itemstatussearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, itemstatusopfilters, itemstatusopcolumns);
					
					if(itemstatussearchresults!=null && itemstatussearchresults!=''){
						itemstatus=itemstatussearchresults[0].getValue('custrecord_sku_status');
					}
					if(itemstatus==''||itemstatus=='null'||itemstatus==null){// Case# 20127588
						nlapiLogExecution('ERROR', 'Inside Closed search for item status');
						var itemstatuscofilters = new Array();
						itemstatuscofilters[0] = new nlobjSearchFilter('custrecord_ebiztask_line_no', null, 'equalto', line);
						itemstatuscofilters[1] = new nlobjSearchFilter('custrecord_ebiztask_sku', null, 'anyof', itemname);
						itemstatuscofilters[2] = new nlobjSearchFilter('name', null, 'is', poidno);

						var itemstatuscocolumns = new Array();
						itemstatuscocolumns[0] = new nlobjSearchColumn('custrecord_ebiztask_sku_status');


						var itemstatuscosearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, itemstatuscofilters, itemstatuscocolumns);

						if(itemstatuscosearchresults!=null && itemstatuscosearchresults!=''){
							itemstatus=itemstatuscosearchresults[0].getValue('custrecord_ebiztask_sku_status');
						}

					}
					
					if(itemstatus==''||itemstatus=='null'||itemstatus==null){// Case# 20127588
						nlapiLogExecution('ERROR', 'SKU Status1', itemstatus);
						itemstatus = posearchresults[i].getValue('custcol_ebiznet_item_status');
					}
					nlapiLogExecution('ERROR', 'SKU Status', itemstatus);


					var filters = new Array();
					//filters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', request.getParameter('custpage_po'));
					filters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poid);
					// Order Category is list of vales and its internal id's will not change across different deployments
					// Order-Category   Internal-Id
					//     PO				2
					//     SO				3
					//     WO				4

					filters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ord_category', null, 'is', '2');
					filters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', line);


					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
					columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_item');
					columns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_item_status');
					columns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_uom_id');
					columns[4] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');
					columns[5] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
					columns[6] = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');
					columns[7] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');
					columns[8] = new nlobjSearchColumn('custrecord_orderlinedetails_putexcep_qty');



					// LN, execute the search, passing null filter and return columns
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);
				//20124116 start
				var	cqty=0;
				var	pgenqty=0;
				var pconfqty=0;
				var exceptionqty=0;
				//end

					if (searchresults !=null )
					{
						for (var j = 0; j < searchresults.length; j++) 
						{
							
							nlapiLogExecution('ERROR', 'searchresults.length', searchresults.length);
							
							var searchresult = searchresults[j];

							//line = searchresult.getValue('custrecord_orderlinedetails_orderline_no');
							//itemname = searchresult.getValue('custrecord_orderlinedetails_item');
							//itemstatus = searchresult.getValue('custrecord_orderlinedetails_item_status');
							uom = searchresult.getValue('custrecord_orderlinedetails_uom_id');
							ordqty = searchresult.getValue('custrecord_orderlinedetails_order_qty');
							//case #20123968 start
							cqty = parseFloat(cqty) + parseFloat(searchresult.getValue('custrecord_orderlinedetails_checkin_qty'));
							//case #20123968 end
							//case 20124116,20124117� start
							nlapiLogExecution('ERROR', 'cqty' + j, cqty);
							//case# 20124353� Start�
							var tempputgenqty=searchresult.getValue('custrecord_orderlinedetails_putgen_qty');
							if(tempputgenqty=='' || tempputgenqty==null)
								tempputgenqty=0;
							//case# 20124353�  End
							
							pgenqty = parseFloat(pgenqty) + parseFloat(tempputgenqty);
							//case# 20124298 Start?
							var temppconfqty=searchresult.getValue('custrecord_orderlinedetails_putconf_qty');
							if(temppconfqty=='' || temppconfqty==null)
								temppconfqty=0;
							//case# 20124298 End
							pconfqty = parseFloat(pconfqty) + parseFloat(temppconfqty);
							//pconfqty = searchresult.getValue('custrecord_orderlinedetails_putconf_qty');
							//end	
							//case# 20127102 starts (if condition added for checking exceptionqty null or empty)
							//case# 20127116 starts (tempexceptionqty variable added)
							var tempexceptionqty=searchresult.getValue('custrecord_orderlinedetails_putexcep_qty');
							if(tempexceptionqty=='' || tempexceptionqty==null)
								tempexceptionqty=0;
							//case# 20127116 end
							exceptionqty=parseFloat(exceptionqty)+parseFloat(tempexceptionqty);
							//case# 20127102 end
						}
					}

					form.getSubList('custpage_items').setLineItemValue('custpage_line', i+1, line);
					// form.getSubList('custpage_items').setLineItemValue('custpage_lp', i + 1, lp);
					form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i+1, itemname);
					if(itemstatus != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i+1, itemstatus);
					//form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, lotbatch);
					if(ordqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i+1, ordqty);
					if(uom != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_uom', i+1, uom);
					if(cqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_rcvquantity', i+1, (cqty.toString()));
					if(pgenqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_genquantity', i+1, pgenqty.toString());
					if(pconfqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_confquantity', i+1, pconfqty.toString());
					if(exceptionqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_exceptionquantity', i+1, exceptionqty.toString());

					line = null; itemname = null; itemstatus = null; uom = null; ordqty = null; cqty= null; pgenqty = null; pconfqty = null;exceptionqty=null;

				}

			}
		}
		else
			{
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
			var msgtext = "Invalid Order# : "+poidno;
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+ msgtext + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			}
        
        response.writePage(form);
             
        
    }
    
}


function GetPOInternalId(POid)
{
	var purchaseorderFilers = new Array();
	purchaseorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	purchaseorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', POid)); 
	//salesorderFilers.push(new nlobjSearchFilter('line', null, 'is', ordline));
	var customerSearchResults = nlapiSearchRecord('purchaseorder', null, purchaseorderFilers,null);	
	if(customerSearchResults==null)
	{
		nlapiLogExecution('ERROR', 'transferorder','transferorder');
		customerSearchResults=nlapiSearchRecord('transferorder',null,purchaseorderFilers,null);
	}
	
	if(customerSearchResults != null && customerSearchResults != "")
	{
		nlapiLogExecution('ERROR', 'customerSearchResults.length ',customerSearchResults.length);
		return customerSearchResults[0].getId();
	}
	else
		return null;
}
