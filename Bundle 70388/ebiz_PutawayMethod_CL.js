/***************************************************************************
????????????????????     ?eBizNET
???????????????????? eBizNET Solutions Inc
 ****************************************************************************
 *
 *? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_PutawayMethod_CL.js,v $
 *? $Revision: 1.4.2.2 $
 *? $Date: 2012/02/09 16:10:57 $
 *? $Author: snimmakayala $
 *? $Name: t_NSWMS_LOG201121_26 $
 *
 * DESCRIPTION
 *? Functionality
 *
 * REVISION HISTORY
 *? $Log: ebiz_PutawayMethod_CL.js,v $
 *? Revision 1.4.2.2  2012/02/09 16:10:57  snimmakayala
 *? CASE201112/CR201113/LOG201121
 *? Physical and Virtual Location changes
 *?
 *? Revision 1.6  2012/02/09 14:43:43  snimmakayala
 *? CASE201112/CR201113/LOG201121
 *? Physical and Virtual Location changes
 *?
 *? Revision 1.5  2012/01/23 09:58:46  spendyala
 *? CASE201112/CR201113/LOG201121
 *?  Added code to trim the space at right(ifany)  on 'name' field.
 *?
 ****************************************************************************/

/**
 * @param type
 * @param name
 * @returns {Boolean}
 */
function mergeFlagOnchange(type,name)
{
	if(name=='custrecord_mergeputaway')
	{
		var mergeFlag = nlapiGetFieldValue('custrecord_mergeputaway');  
		var mixlots = nlapiGetFieldValue('custrecord_mixlotsputaway');  
		var mixpackcode = nlapiGetFieldValue('custrecord_mixpackcode');  
		var mixsku = nlapiGetFieldValue('custrecord_mixsku');  
		var mergelp = nlapiGetFieldValue('custrecord_mergelp'); 
		if(mergeFlag !=null)
		{		
			if(mergeFlag == 'F')
			{
				nlapiSetFieldValue('custrecord_mixlotsputaway','F');
				nlapiSetFieldValue('custrecord_mixpackcode','F');
				nlapiSetFieldValue('custrecord_mixsku','F');
				nlapiSetFieldValue('custrecord_mergelp','F');

				return true;
			}
		}
	}
}


/**
 * @param type
 * @returns {Boolean}
 */
function fnCheckPUTWMethod(type)
{
	var varMethodID =  nlapiGetFieldValue('name');  
	var vComapny	= nlapiGetFieldValue('custrecord_ebizcompanyputaway');  	 
	var vSite	= nlapiGetFieldValue('custrecord_ebizsiteputaway');  
	var vId = nlapiGetFieldValue('id');  

	if(varMethodID != "" && varMethodID != null && vSite != "" && vSite != null && vComapny != ""  && vComapny != null)
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is',varMethodID.replace(/\s+$/, ""));//Added by suman on 09/01/12 to trim spaces at right.
		filters[1] = new nlobjSearchFilter('custrecord_ebizcompanyputaway', null, 'anyof',['@NONE@',vComapny]);	 
		filters[2] = new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof',['@NONE@',vSite]);

		if(vId!=null && vId!="")
		{
			filters[3] = new nlobjSearchFilter('internalid', null, 'noneof',vId);	
		}

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filters);


		if (searchresults != null && searchresults.length > 0) 
		{
			alert("This Record already exists.");
			return false;
		}
		else
		{
			return true;
		} 
	}
	else
	{
		return true;
	}

}