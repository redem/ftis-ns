/***************************************************************************
 eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_Rf_LoadTrlr.js,v $
 *     	   $Revision: 1.2.4.2.4.2.4.6 $
 *     	   $Date: 2014/06/13 13:11:52 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Rf_LoadTrlr.js,v $
 * Revision 1.2.4.2.4.2.4.6  2014/06/13 13:11:52  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.4.2.4.2.4.5  2014/06/06 07:10:57  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.2.4.2.4.2.4.4  2014/05/30 00:41:01  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.2.4.2.4.3  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.4.2.4.2.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.2.4.2.4.1  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.4.2.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.4.2.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.4.2  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.1  2012/02/22 12:47:19  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2  2011/09/28 13:23:13  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 *
 *****************************************************************************/
function LoadTrailerMenu(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
	
	var getLanguage = request.getParameter('custparam_language');	
    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
    
	var st0,st1,st2,st3;
	if( getLanguage == 'es_ES')
	{
		st0 = "TRAILER DE CARGA";
		st1 = "ENTER TRAILER:";
		st2 = "ENVIAR";
		st3 = "ANTERIOR";		
	}
	else
	{
		st0 = "TRAILER LOADING";
		st1 = "ENTER TRAILER :";
		st2 = "SEND";
		st3 = "PREV";

	}
		var functionkeyHtml=getFunctionkeyScript('_rfload'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('selecttrailer').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfload' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selecttrailer' id='selecttrailer' type='text'/>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selecttrailer').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');		 
		var optedField = request.getParameter('selecttrailer');
		var optedEvent = request.getParameter('cmdPrevious');
		var loadtrlrarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		loadtrlrarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', loadtrlrarray["custparam_language"]);
    	
		
		var st5;
		if( getLanguage == 'es_ES')
		{
			
			st5 = "N&#218;MERO NO V&#193;LIDO TRAILER TRY AGAIN";
			
		}
		else
		{
			
			st5 = "INVALID TRAILER NUMBER TRY AGAIN";
		}
		
		loadtrlrarray["custparam_trlrno"]=request.getParameter('selecttrailer');

		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_rf_loadunloadtrlr', 'customdeploy_rf_loadunloadtrlr_di', false, loadtrlrarray);
		}
		else 
		{
			var loadtrlrfilters = new Array();
			loadtrlrfilters.push(new nlobjSearchFilter('name', null, 'is', optedField)); 

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, loadtrlrfilters, null);
			if(searchresults != null){
				if(searchresults.length > 0){
					response.sendRedirect('SUITELET', 'customscript_rf_scanlp', 'customdeploy_rf_scanlp_di', false, loadtrlrarray);
				}
			}
			else
			{
				loadtrlrarray["custparam_error"] = st5;
				loadtrlrarray["custparam_screenno"] = '36';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, loadtrlrarray);
			}
		}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}