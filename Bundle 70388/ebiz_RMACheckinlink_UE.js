/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/UserEvents/ebiz_RMACheckinlink_UE.js,v $
 *     	   $Revision: 1.2.4.2.4.1.4.6.2.1 $
 *     	   $Date: 2015/10/20 10:49:32 $
 *     	   $Author: grao $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_44 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RMACheckinlink_UE.js,v $
 * Revision 1.2.4.2.4.1.4.6.2.1  2015/10/20 10:49:32  grao
 *  CT issue fixes 201415135
 *
 * Revision 1.2.4.2.4.1.4.6  2014/10/16 13:51:51  vmandala
 * Case# 201410722
 * Std bundle issue fixed
 *
 * Revision 1.2.4.2.4.1.4.5  2014/05/29 15:38:50  sponnaganti
 * case# 20148585
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.4.2.4.1.4.4  2013/12/30 15:33:20  rmukkera
 * Case # 20126587
 *
 * Revision 1.2.4.2.4.1.4.3  2013/04/09 13:22:46  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to check in link
 *
 * Revision 1.2.4.2.4.1.4.2  2013/02/26 13:45:24  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Boombah.
 *
 *
 *****************************************************************************/

/***************************************************
 * This is a User Event Script that generates a
 * link for each line item that points to the
 * Suitelet page above.  It is meant to be deployed
 * on the Purchase Order record.
 */
function rmaAddCheckInLinkUE(type, form, request){
	//obtain the context object
	var ctx = nlapiGetContext();

	//only execute when the exec context is web browser and in view mode of the PO
	if (ctx.getExecutionContext() == 'userinterface' &&
			type == 'view') {
		//Add the "ebiznet Check In" field

		form.getSubList('item').addField('custpage_checkin_link', 'text', 'Check-In', null);
		
		//Case # 20126587 Start
		//Get the receive button
		var receivebutton = form.getButton('receive');  
		//case# 20148585 starts 
		//if(receivebutton!=null)
			//receivebutton.setVisible(false);
		if(receivebutton!=null)
			receivebutton.setVisible(true);
		//case# 20148585 ends
		//Case # 20126587 Start
		//obtain the PO internal ID
		var poid = nlapiGetRecordId();
		var poname = nlapiGetFieldValue('tranid');
		var OrdStatus = nlapiGetFieldValue('orderstatus');
		var OrdStatustext = nlapiGetFieldText('orderstatus');
		var location = nlapiGetFieldValue('location');
		var postatus = nlapiGetFieldValue('status');
		nlapiLogExecution('ERROR', 'postatus', postatus);
		//resolve the generic relative URL for the Suitelet
		var checkInURL = nlapiResolveURL('SUITELET', 'customscript_rma_checkin_sl', 'customdeploy_ebiznetrmacheckin_di');
		nlapiLogExecution('Error', 'check in url', checkInURL);
		nlapiLogExecution('Error', 'poid', poid);
		nlapiLogExecution('Error', 'OrdStatus', OrdStatus);
		nlapiLogExecution('Error', 'OrdStatustext', OrdStatustext);

		//checkInURL = getFQDNForHost(ctx.getEnvironment()) + checkInURL;
		nlapiLogExecution('Error', 'check in url', checkInURL);
		//complete the URL as either for production or sandbox environment
//		if (ctx.getEnvironment() == 'PRODUCTION') {
//		checkInURL = 'https://system.netsuite.com' + checkInURL;
//		}
//		else 
//		if (ctx.getEnvironment() == 'SANDBOX') {
//		checkInURL = 'https://system.sandbox.netsuite.com' + checkInURL;
//		}

		//loop through the item sublist
		for (var i = 0; i < form.getSubList('item').getLineItemCount(); i++) {

			var quantity = parseFloat(form.getSubList('item').getLineItemValue('quantity', i + 1));
			var quantityRcv = parseFloat(form.getSubList('item').getLineItemValue('quantityreceived', i + 1));
			var RemQty = (parseFloat(quantity) - parseFloat(quantityRcv));
			var IsClosed=nlapiGetLineItemValue('item','isclosed',i + 1);


			nlapiLogExecution('DEBUG', 'quantity info', 'Quantity: ' + quantity + '<br>' + 'Quantity Received: ' + quantityRcv);
			/* code merged from boombah on 25th feb 2013 by Radhika */
			var lineCnt=i+1;

			if(IsClosed!='T') //If line is closed then check in link is not populated for the selected line.
			{



				var checkinqty=getCheckinqty(poid,lineCnt);

			if(checkinqty==null || checkinqty=='')
				checkinqty=0;

			if(postatus=='Pending Receipt'||postatus=='Partially Received' ||postatus=='Pending Billing/Partially Received' || postatus=='Pending Refund/Partially Received')
			{
				//generate the hyperlink only when the line has not been fully received
				if ((quantityRcv < quantity) && (quantity > checkinqty)) {
					var linkURL;
					linkURL = checkInURL + '&poID=' + poid;
					linkURL = linkURL + '&poValue=' + poname;
					linkURL = linkURL + '&QtyChkn=' + quantityRcv;
					linkURL = linkURL + '&QtyRem=' + RemQty;
					linkURL = linkURL + '&poLineItem=' + nlapiGetLineItemValue('item', 'item', i + 1);
					linkURL = linkURL + '&poLineQty=' + nlapiGetLineItemValue('item', 'quantity', i + 1);
					linkURL = linkURL + '&poLineNum=' + nlapiGetLineItemValue('item', 'line', i + 1);
					linkURL = linkURL + '&poSerialNos=' + nlapiGetLineItemValue('item', 'serialnumbers', i + 1);
					linkURL = linkURL + '&poItemStatus=' + nlapiGetLineItemValue('item', 'custcol_ebiznet_item_status', i + 1);
					//case 201410722 case start
					linkURL = linkURL + '&Packcode=' + nlapiGetLineItemText('item', 'custcol_nswmspackcode', i + 1);;
					//case 201410722 case end
					//add the PO ID, item, and quantity to the URL
//					linkURL = checkInURL + '&custparam_poid=' + poid;
//					linkURL = linkURL + '&custparam_povalue=' + poname;
//					linkURL = linkURL + '&custparam_po_line_item=' + nlapiGetLineItemValue('item', 'item', i + 1);
//					linkURL = linkURL + '&custparam_po_line_quantity=' + nlapiGetLineItemValue('item', 'quantity', i + 1);
//					linkURL = linkURL + '&custparam_po_line_num=' + nlapiGetLineItemValue('item', 'line', i + 1);
//					linkURL = linkURL + '&custparam_po_serialnos=' + nlapiGetLineItemValue('item', 'serialnumbers', i + 1);
					//linkURL = linkURL + '&custparam_po_loc_id=' + nlapiGetFieldValue('location');
					//LN,Added on Nov15th 2010. Sending remaining qty parameter into QueryString
					//linkURL = linkURL + '&custparam_remainingqty='+parseFloat(nlapiGetLineItemValue('item', 'quantity', i + 1) - nlapiGetLineItemValue('item', 'received', i + 1));



					//log the URL
					nlapiLogExecution('Error', 'check in url', linkURL);

					//populate the URL into the check in field as a hyperlink to the Suitelet
					//A for Pending Approval
					//B for Pending Receipt
					if(OrdStatus!="A")
					{
						form.getSubList('item').setLineItemValue('custpage_checkin_link', i + 1, '<a href="' + linkURL + '">Check In</a>');
					}

					}
				}
			}
		}
	}
}

/* function added from boombah acc  to fetch the checkin quantity on 25th feb 2013 by Radhika */
function getCheckinqty(poID,lineCnt)
{
	var checkinQuantity=0; 
	var transactionFilters = new Array();
	transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poID);
	//transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', itemId);
	transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineCnt);

	var transactionColumns = new Array();
	transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
	transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	transactionColumns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	transactionColumns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');

	var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);

	if (transactionSearchresults != null && transactionSearchresults.length > 0)
	{
		for(var i = 0; i <= transactionSearchresults.length; i++)
		{
			checkinQuantity = transactionSearchresults[0].getValue('custrecord_orderlinedetails_checkin_qty');

		}
	}
	return checkinQuantity;
}
