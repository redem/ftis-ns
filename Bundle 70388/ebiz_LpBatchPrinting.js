/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_LpBatchPrinting.js,v $
 *     	   $Revision: 1.1.4.1.4.1 $
 *     	   $Date: 2012/11/01 14:55:15 $
 *     	   $Author: schepuri $
 *     	   $Name: t_NSWMS_2013_1_2_7 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_LpBatchPrinting.js,v $
 * Revision 1.1.4.1.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.4.1  2012/04/20 12:53:18  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.1  2011/09/29 13:20:20  rmukkera
 * CASE201112/CR201113/LOG201121
 * new file
 *  */

function LpBatchPrinting(request, response)
{
	if (request.getMethod() == 'GET') {

		createForm();
	}
	else
	{
		createForm();
		var varlptype = request.getParameterValues('custpage_lptype');
		// var varlptypetext=varlptype.getText;
		// nlapiLogExecution('ERROR', 'varlptypetext', varlptypetext);
		nlapiLogExecution('ERROR', 'varlptype', varlptype);
		var varfromlp = request.getParameter('custpage_lpfrom');
		var vartolp = request.getParameter('custpage_lpto');
		var item=request.getParameter('custpage_sku');
		
		
		var lp="";
	
		var itemdesc="";
		var from=parseFloat(varfromlp);
		var to=parseFloat(vartolp);
		var p_nolabels="";
		for(var i=from;i<=to;i++)
		{
			var labeldata="^XA^PH^XZ";
			labeldata+="^XA^PR3^XZ";
			labeldata+="^XA";
			labeldata+="^LH0,0";
			labeldata+="^FO100,300^A0,50,50^FD'||'LP'||'^FS";
			labeldata+="^FO200,300^BCN,300,Y,N,N^FD'||"+i+"||'^FS'";
			if(item!=""&&item!=null)
			{
				
				
				labeldata+="^FO100,650^A0,50,50^FD'||'SKU :'||'^FS";
				labeldata+="^FO200,650^BCN,300,Y,N,N^FD'||RTRIM("+item+")||'^FS";
				labeldata+="^FO500,650^A0,50,50^FD'||RTRIM("+itemdesc+")||'^FS";

			}
			labeldata+="^POI";
			labeldata+="^PQ'||LTRIM("+p_nolabels+")";
			labeldata+="^XB";
			labeldata+="^ISR:LABEL.GRF,N";
			labeldata+="^XZ";
			labeldata+=" ";
			labeldata+="^XA^ILR:LABEL.GRF^XZ";

			CreateLabelData(labeldata,varlptype,varlptype,'','T','F','','',i);
		}
	}
}          

function createForm()
{
	var form = nlapiCreateForm('Lp LOT Printing');
   
	var lptype = form.addField('custpage_lptype', 'select', 'Lp Type', 'customlist_ebiznet_lp_type');   
	
	lptype.setDisplaySize(120, 9);	
	lptype.setMandatory(true);
	
	var sku = form.addField('custpage_sku', 'select', 'Sku', 'inventoryitem');  
	sku.setDisplaySize(20, 9);	
	
	var Fromlp = form.addField('custpage_lpfrom', 'text', 'From Lp'); 
	Fromlp.setDisplaySize(20, 9);	
	Fromlp.setMandatory(true);

	//Fromlp.setPadding(1);
	
	var Tolp = form.addField('custpage_lpto', 'text', 'To Lp'); 
	Tolp.setDisplaySize(20, 9);	
	Tolp.setMandatory(true);
	form.addSubmitButton('Generate');
	
	response.writePage(form);
}

function validatelprange(varlptype,varfromlp,vartolp)
{
	var filters = new Array();          
	filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof',varlptype);
	filters[1] = new nlobjSearchFilter('custrecord_ebiznet_lprange_begin', null, 'greaterthanorequalto',varfromlp);
	filters[2] = new nlobjSearchFilter('custrecord_ebiznet_lprange_end', null, 'between',varfromlp,vartolp);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_begin');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_end');
	
	var searchQtyresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details',null, filters, columns);
	
}
function CreateLabelData(labeldata,labeltype,tasktype,refno,print,reprint,company,location,name)
{

	var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
	labelrecord.setFieldValue('name', name); 
	labelrecord.setFieldValue('custrecord_labeldata',labeldata);  
	labelrecord.setFieldValue('custrecord_label_refno',refno);     
	labelrecord.setFieldValue('custrecord_label_tasktype',1);//chkn task   
	labelrecord.setFieldValue('custrecord_labeltype',labeltype);                                                                                                                                                                     labelrecord.setFieldValue('custrecord_labeldata', labeldata);
	labelrecord.setFieldValue('custrecord_label_print', print);
	labelrecord.setFieldValue('custrecord_label_reprint', reprint);
	labelrecord.setFieldValue('custrecord_label_company', company);
	labelrecord.setFieldValue('custrecord_label_location', location);
	labelrecord.setFieldValue('custrecord_label_lp', name);
	var tranid = nlapiSubmitRecord(labelrecord);


}

function generateShipLAbel()
{

var labeldata="^XA";
labeldata+="^LH0,0";
labeldata+="^FO700,30^AVR,55,22^FDShip From :^FS";
labeldata+="^FO700,350^AVR,36,30^FDAlpha Comm Enterprises, Inc.^FS";
labeldata+="^FO630,350^AVR,36,30^FD1500 Lakes Parkway, ,^FS";
labeldata+="^FO570,350^AVR,36,30^FDLawrenceville, GA 30047^FS";
labeldata+="^FO550,2^AVR,20,1190^FD____________________________________________________________^FS";
labeldata+="^FO450,30^AVR,35,20^FDShip to :^FS";
labeldata+="^FO450,350^AVR,35,20^FD MetroPCS HO#7008-MetroPCS wireless, Inc.^FS";
labeldata+="^FO380,350^AVR,35,20^FD7440 W. Vernor Hwy^FS";
labeldata+="^FO310,350^AVR,35,20^FD,^FS";
labeldata+="^FO250,350^AVR,35,20^FDDetroit,MI 48209^FS";
labeldata+="^FO220,2^AVR,20,1190^FD___________________________________________________________^FS";
labeldata+="^FO140,30^AVR,37,17^FDCustomer PO # : 226881^FS";
labeldata+="^FO110,2^AVR,20,1190^FD___________________________________________________________^FS";
labeldata+="^FO30,30^AVR,29,26^FDCarton # : 1568751^FS";
labeldata+="^FO30,850^AVR,35,40^FDBox 2 of 2^FS";
labeldata+="^PQ1";
labeldata+="^XB";
labeldata+="^ISR:LABEL.GRF,N";
labeldata+="^XZ";
labeldata+="^XA^ILR:LABEL.GRF^XZ"; 

	
}



