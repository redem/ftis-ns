/***************************************************************************
 eBizNET  Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/ebiz_BOM_ReportPDF_CL.js,v $
 *     	   $Revision: 1.18.2.1.4.2.4.3.2.4 $
 *     	   $Date: 2015/05/05 15:22:56 $
 *     	   $Author: grao $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_88 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_BOM_ReportPDF_CL.js,v $
 * Revision 1.18.2.1.4.2.4.3.2.4  2015/05/05 15:22:56  grao
 * SB issue fixes  201412646
 *
 * Revision 1.18.2.1.4.2.4.3.2.3  2014/08/18 15:26:38  sponnaganti
 * Case# 20149993
 * Stnd Bundle Issue fix
 *
 * Revision 1.18.2.1.4.2.4.3.2.2  2014/07/04 15:18:11  sponnaganti
 * case# 20149081
 * Compatibility Issue Fix
 *
 * Revision 1.18.2.1.4.2.4.3.2.1  2014/06/27 15:36:40  skreddy
 * case # 20148991
 * Dylan SB  issue fix
 *
 * Revision 1.18.2.1.4.2.4.3  2014/06/13 15:31:23  sponnaganti
 * case# 20148583
 * Stnd Bundle Issue Fix
 *
 * Revision 1.18.2.1.4.2.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.18.2.1.4.2.4.1  2013/03/08 14:47:08  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.18.2.1.4.2  2012/11/01 14:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.18.2.1.4.1  2012/10/26 08:02:57  gkalla
 * CASE201112/CR201113/LOG201121
 * Lot# exception enhancement in work orders
 *
 * Revision 1.18.2.1  2012/02/08 14:40:17  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * kitting
 *
 * Revision 1.21  2012/02/01 09:41:18  gkalla
 * CASE201112/CR201113/LOG201121
 * Work order Bin location issues fixed
 *
 * Revision 1.20  2012/01/31 12:43:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Work order Bin location issues fixed
 *
 * Revision 1.17  2011/12/30 20:37:02  gkalla
 * CASE201112/CR201113/LOG201121
 * For WO allocation
 *
 * Revision 1.16  2011/12/15 17:20:20  gkalla
 * CASE201112/CR201113/LOG201121
 * To get the inventory based on selected site
 *
 * Revision 1.15  2011/11/13 16:03:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Assembly instruction field is added
 *
 * Revision 1.14  2011/11/04 18:21:07  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate multiple lot# in WO screen
 *
 * Revision 1.13  2011/11/01 23:18:03  gkalla
 * CASE201112/CR201113/LOG201121
 * To get inventory which has more than zero qty
 *
 * Revision 1.12  2011/11/01 23:06:19  gkalla
 * CASE201112/CR201113/LOG201121
 * To get inventory which has more than zero qty
 *
 * Revision 1.11  2011/10/27 13:46:09  gkalla
 * CASE201112/CR201113/LOG201121
 * To remove the alert messages and fine tuned
 *
 * Revision 1.10  2011/10/25 11:23:24  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Report issue fixed in 1.9 ver. Issue is if we have multiple batches while parsing we got issue
 * 1.10 ver contains the code fix for un necessary alerts
 *
 * Revision 1.9  2011/10/24 07:32:06  gkalla
 * CASE201112/CR201113/LOG201121
 * To display alert message to track lot no
 *
 * Revision 1.5  2011/10/13 07:35:46  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Two changes are done.
 * change1 :
 * if the compoent is itself is sub assembly item and its family is null , system is throwing java script error.
 * This is resolved and code is generalised  for any kind of item
 *
 * Change2:
 * if the component items doesn't have inventory at all , the code is fixing and showing zero qty in the message box
 *
 * Revision 1.4  2011/10/11 12:09:43  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Chages to work order screen
 * 1. Location as made mandatory.
 * 2. BOM details report was showing the location internalid. It is fixed now and showing the location
 *
 * Revision 1.3  2011/10/11 06:42:35  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Line feed is added for insufficiant inventory message.
 *
 * Revision 1.4  2011/09/12 10:47:02  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the print button issue in Pick Report screen
 *
 * Revision 1.3  2011/07/21 07:40:01  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
/**
 * This is the main function to print the WO details  
 */

function BOMDetails(type, name)
{
	//alert("1");
	var ctx = nlapiGetContext();  
	var tranId= nlapiGetFieldValue('tranid');
	var woid= nlapiGetFieldValue('id');
//	alert(ctx);
//	alert(ctx.response);
	var searchresults = nlapiLoadRecord('workorder', woid);

	var qty,location,woid,tranid,item,itemid;
	if(searchresults!=null && searchresults!='') 
	{
		//alert("hi1 ");
		//alert('Hi2');
		qty = searchresults.getFieldValue('quantity'); 
		//alert(qty);
		itemid=searchresults.getFieldValue('assemblyitem');
		var vMemo=searchresults.getFieldValue('memo');
		var itemrec=nlapiLoadRecord('assemblyitem',itemid);
		var assmblyinstruction=itemrec.getFieldValue('custitem_assmblyinstr');
		if(assmblyinstruction==null||assmblyinstruction=="")
			assmblyinstruction="";

		var itemText = nlapiGetFieldText('assemblyitem');
		var WoCreatedDate=searchresults.getFieldValue('trandate');
		var WoDueDate=searchresults.getFieldValue('custbody_ebiz_woduedate');
		if(WoDueDate== null || WoDueDate=="")
			WoDueDate="&nbsp;";
		var WoExpcompletionDate=searchresults.getFieldValue('custbody_ebiz_expcompletiondate');
		if(WoExpcompletionDate== null || WoExpcompletionDate=="")
			WoExpcompletionDate="&nbsp;";
		//alert(itemText);
		//var itemText=searchresults.getFieldText('assemblyitem'); 
		var itemQty=searchresults.getFieldValue('quantity');

		var itemStatus=searchresults.getFieldValue('status');
		if(itemStatus== null || itemStatus=="")
			itemStatus="&nbsp;";
		var itemLocation=searchresults.getFieldValue('location');
		var itemLocationtext="";
		if(itemLocation == null || itemLocation =="")
		{
			itemLocation="&nbsp;";
		}
		else
		{

			var searchresultslocation = nlapiLoadRecord('location', itemLocation);
			if(searchresultslocation!=null && searchresultslocation!='') 
			{
				itemLocationtext=searchresultslocation.getFieldValue('name');
			}
		}

		//alert(nlapiGetFieldText('assemblyitem'));
		//alert('Item ' +itemid);
		var filterOpentask = new Array();
		filterOpentask[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid);
		filterOpentask[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', 9);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no', null, 'group');
		columns[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
		columns[2] = new nlobjSearchColumn('custrecord_expe_qty', null ,'sum');
		columns[3] = new nlobjSearchColumn('custrecord_lpno', null, 'group');
		columns[4] = new nlobjSearchColumn('custrecord_batch_no', null, 'group');
		columns[5] = new nlobjSearchColumn('custrecord_actbeginloc', null, 'group');
		columns[6] = new nlobjSearchColumn('custrecord_act_qty', null, 'group');                                    
		columns[0].setSort();
		var Opentaskresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,columns);

		
		if(Opentaskresults!=null && Opentaskresults!='') 
		{
			var vLineCount=searchresults.getLineItemCount('item');
			var pp = window.open();
			pp.document.writeln('<HTML><HEAD><title>Print Preview</title>');
			pp.document.writeln('<style  media=print>#PRINT ,#CLOSE ,#btnPrint{visibility:hidden;}</style>');
			pp.document.writeln('<script language="javascript">function fnRevoke(strPrint){alert(strPrint);}</script>');
			pp.document.writeln('<base target="_self"></HEAD>');				 
			pp.document.writeln('<body MS_POSITIONING="GridLayout" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">');
			pp.document.writeln('<form  method="post">');
			pp.document.writeln('<table width="90%" align=center><tr><td align=right>');
			pp.document.writeln('<INPUT ID="PRINT" type="button" Class="btn" value="Print" onclick="javascript:location.reload(true); window.print();window.close();">&nbsp;');
			pp.document.writeln('<INPUT ID="CLOSE" type="button" value="Close" Class="btn"  onclick="window.close();"></TD></TR>');
			pp.document.writeln('<tr><td>');
			pp.document.writeln('<TABLE width="100%" align="center"><TR><TD align=left><b>BOM Details for WO#: '+ tranId +'</b></td></tr>');
			pp.document.writeln('<tr><td>&nbsp;</td></tr>');
			pp.document.writeln('<tr><td><table><tr><td align=left><b>Assembly</b></td><td width=50px></td><td align=left>'+ itemText +'</td><td width=250px></td><td align=left><b>Workorder created date</b></td><td width=50px></td><td align=left>'+ WoCreatedDate +'</td></tr>');
			pp.document.writeln('<tr><td align=left><b>Quantity</b></td><td width=50px></td><td align=left>'+ itemQty +'</td><td width=250px></td><td align=left><b>Due date</b></td><td width=50px></td><td align=left>'+ WoDueDate +'</td></tr>');
			pp.document.writeln('<tr><td align=left><b>Status</b></td><td width=50px></td><td align=left>'+ itemStatus +'</td><td width=250px></td><td align=left><b>Expected Completion date</b></td><td width=50px></td><td align=left>'+ WoExpcompletionDate +'</td></tr>');
			if(vMemo == null || vMemo == '')
				vMemo='';
			pp.document.writeln('<tr><td align=left><b>Location</b></td><td width=50px></td><td align=left>'+ itemLocationtext +'</td><td width=250px></td><td align=left><b>Assembly Instructions</b></td><td width=50px></td><td align=left>'+ assmblyinstruction +'</td></tr><tr><td align=left><b>Memo</b></td><td width=50px></td><td align=left>'+ vMemo +'</td></tr></table></td></tr>');
			//pp.document.writeln('<tr><td align=left><b>Location</b></td><td width=50px></td><td align=left>'+ itemLocationtext +'</td></tr></table></td></tr>');
			pp.document.writeln('<tr><td>&nbsp;</td></tr>');
			pp.document.writeln('<tr><td><table border=1 cellSpacing="0" width="100%" cellPadding="0"><tr><td align=center><b>Item</b></td><td align=center><b>Quantity</b></td><td align=center><b>Serial/Lot Numbers</b></td><td align=center><b>LP#</b></td><td align=center><b>Bin Location</b></td><td align=center><b>Available Qty</b></td><td align=center><b>BOM Qty</b></td></tr>');

			


					var vCount=1;



					var newarray=new Array();
					for(k=0; k<Opentaskresults.length; k++)
					{
						
						
						var LineNo =Opentaskresults[k].getValue('custrecord_line_no', null, 'group');
						newarray[k]=LineNo;


					}

						LineArray=removeDuplicateElement(newarray);
					//alert("LineArrayLength"+LineArray.length);
					//alert("LineCount"+LineCount.length);
					

					var previtem1="";
					var previtem2="";
					
					for (m=0; m<Opentaskresults.length; m++) 
					{
						var WOBackOrdQty="";
						 var WOCommittedQty="";
						 var WOBomQty="";
						 var WOItemDesc="";
						 var WOLineQty=0;
							
						var lineItem=Opentaskresults[m].getValue('custrecord_sku', null, 'group');
						var itemtext=Opentaskresults[m].getText('custrecord_sku', null, 'group');
						var LineNo =Opentaskresults[m].getValue('custrecord_line_no', null, 'group');
						var LineQty=Opentaskresults[m].getValue('custrecord_expe_qty', null ,'sum');
						var LineBinLOcation=Opentaskresults[m].getText('custrecord_actbeginloc', null, 'group');
						var LineBinLOcationVal=Opentaskresults[m].getValue('custrecord_actbeginloc', null, 'group');
						var LineLP=Opentaskresults[m].getValue('custrecord_lpno', null, 'group');
						var LineQuantity=Opentaskresults[m].getValue('custrecord_act_qty', null, 'group');
						var LineLotNo=Opentaskresults[m].getValue('custrecord_batch_no', null, 'group');
						if(LineLotNo== null || LineLotNo ==""||LineLotNo =="- None -")
							LineLotNo="";
						
						
						
						for(var i=1;i<=vLineCount;i++)
						{	//alert('forloop');
						var WOLineNo =searchresults.getLineItemValue('item','line',i)+".0";
						 if(WOLineNo== null || WOLineNo =="")
							 WOLineNo="&nbsp;";
						
						 
						 var WOlineItemvalue = searchresults.getLineItemValue('item', 'item', i);
						 		 
											
							
							//if(WOLineNo==LineNo && WOlineItemvalue==lineItem)
							if(parseFloat(WOLineNo)==parseFloat(LineNo) && parseFloat(WOlineItemvalue)==parseFloat(lineItem))
							{ 
								WOLineQty = searchresults.getLineItemValue('item','quantity',i);
								if(WOLineQty== null || WOLineQty =="")
									WOLineQty="&nbsp;";
								
								var BomQty =WOLineQty/itemQty;
								if(BomQty== null || BomQty =="")
									BomQty="&nbsp;";
								
								//WOBackOrdQty=BackOrdQty;
								//WOCommittedQty=CommittedQty;
								WOBomQty=BomQty;
											
										
							}
						}
						
						
						//pp.document.writeln('<tr><td>'+ lineItem +'</td><td align=right>'+ WOLineQty +'</td><td>'+ LineLotNo +'</td><td>'+ LineLP +'</td><td>'+ LineBinLOcation +'</td><td align=right>'+ LineAvailQty +'</td><td align=right>'+ WOBomQty +'</td></tr>');
						
						pp.document.writeln('<tr><td>'+ itemtext +'</td>');
						
						var count=0;
						for(var z=0;z<LineArray.length;z++)
						{
							if(LineArray[z]==LineNo)
								{
								count=LineCount[z];
								break;
								}


						}
						//var index=LineArray.indexOf(LineNo);
						//var count=parseFloat(LineCount[index]);
					//alert("LineNo"+LineNo);
					//alert("count"+count);
						if(count>1 && previtem1!=lineItem)
						{
							
							pp.document.writeln('<td align=right rowspan=' + count + '>'+ WOLineQty +'</td>');
							previtem1=lineItem;
						}
						else if(count==1 && previtem1!=lineItem)
						{
							pp.document.writeln('<td align=right>'+ WOLineQty +'</td>');
							previtem1=lineItem;
						}
						pp.document.writeln('<td>'+ LineLotNo +'</td><td>'+ LineLP +'</td><td>'+ LineBinLOcation +'</td><td align=right>'+ LineQty +'</td>');	
						
						
						if(count>1 && previtem2!=lineItem)
						{
						pp.document.writeln('<td align=right rowspan=' + count+ '>'+ WOBomQty +'</td>');
						previtem2=lineItem;
						}
						else if(count==1 && previtem2!=lineItem)
						{
							pp.document.writeln('<td align=right rowspan=' + count+ '>'+ WOBomQty +'</td>');
							previtem2=lineItem;
						}			
						pp.document.writeln('</tr>');
										
					}





					//var CommittedId = nlapiSubmitRecord(searchresults, true);
					//nlapiLogExecution('ERROR', 'CommittedId ', CommittedId);	 


					pp.document.writeln('</table></td></tr>');
					pp.document.writeln('</table>');
					pp.document.writeln('</td></tr></TABLE>');
					pp.document.writeln('</form></body></HTML>');
					
					LineArray.length=0;
					LineCount.length=0;
		}
		else 
		{
			if(itemStatus=="Pending Build")
			{
				alert('Bin locations are not generated for this Work Order '+tranId);
				return false;
			}
			else
			{
				alert('Built confirmed for this Work Order '+tranId);
				return false;
			}

		}
	}
}


function ValidateWOLine(type, name)
{
	//alert("2");
	var qty1=nlapiGetFieldValue('quantity');
	//alert("qty11 :"+qty1 );
	var locationid=nlapiGetFieldValue('location');
	//alert("locationid :"+locationid );

	if(locationid == null || locationid == "")
	{
		alert("Please Select Location ");
		return false;
	}
	var qty=0;
	if(qty1!= null && qty1!="" && parseFloat(qty1)>0)
		qty=qty1;//maincomenet qty
	var itemcount=0;
	var itemcount1=nlapiGetLineItemCount('item');
	if(itemcount1!= null && itemcount1!="" && parseFloat(itemcount1)>0)
		itemcount=itemcount1;//no of member items
	var assemItem= nlapiGetFieldValue('assemblyitem');
	//alert(name);
	//if((name=='assemblyitem' || name=='quantity') && qty>0 && itemcount1>0)
	if(qty>0 && itemcount1>0 && assemItem != null && assemItem != "")
		fnGetDetails(assemItem,qty,itemcount,locationid);


	/*if(qty>0 && itemcount1>0 && assemItem != null && assemItem != "")
	{
		fnGetDetails(assemItem,qty,itemcount);
//		fnGetDetails(assemItem,qty,itemcount);

		//code added by suman
		//this will navigate to generateBinLocation_sl screen for displaying all the location, batch#
		var URL = nlapiResolveURL('SUITELET', 'customscript_workorder_generate_location', 'customdeploy_workorder_generate_loc_di');
		var ctx = nlapiGetContext();
		if (ctx.getEnvironment() == 'PRODUCTION') {
			URL = 'https://system.netsuite.com' + URL;			
		}
		else 
			if (ctx.getEnvironment() == 'SANDBOX') {
				URL = 'https://system.sandbox.netsuite.com' + URL;				
			}

		URL = URL + '&custparam_ebiz_assemItem='+ assemItem+'&custparam_ebiz_qty='+qty+'&custparam_ebiz_itemcount='+ itemcount+'&custparam_ebiz_items='+items;
//		alert(URL);
		window.open(URL);

	}*/

	//alert(nlapiGetFieldText('assemblyitem'));
//	alert(name);

}
function fnGetDetails(vitem,varAssemblyQty,itemcount,locationid)
{
	//alert("3");
	var strItem="",strQty=0,vcomponent;
	var ItemType = nlapiLookupField('item', vitem, 'recordType');
	nlapiLogExecution('ERROR','ItemType',ItemType);
	var searchresultsitem;
	if(ItemType!=null && ItemType !='')

	{
		nlapiLogExecution('ERROR','Item Type',ItemType);
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);

		searchresultsitem = nlapiLoadRecord(ItemType, vitem); //1020
	}

	else
	{
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
	}
	//alert('Location ' + locationid);
	var SkuNo=searchresultsitem.getFieldValue('itemid');
	var recCount=0; 
	//Written by Ganesh
	//alert("ItemCount : "+ itemcount);
	var vAlert='';
	for(var m=1; m<=itemcount;m++) 
	{
		//alert("SkuNo" + SkuNo);
		strItem = nlapiGetLineItemValue('item', 'item', m);
		//alert("strItem" + strItem);
		strQty = nlapiGetLineItemValue('item', 'quantity', m);
		//alert("strQty" + strQty);
		//alert("varAssemblyQty" + varAssemblyQty);
		var fields = ['custitem_item_family', 'custitem_item_group'];
		var ComponentItemType = nlapiLookupField('item', strItem, 'recordType');
		if (ComponentItemType != null && ComponentItemType !='')
		{
			var columns = nlapiLookupField(ComponentItemType, strItem, fields);


			var itemfamily = columns.custitem_item_family;
			var itemgroup = columns.custitem_item_group;
		}
		//avlqty= (parseFloat(strQty)*parseFloat(varAssemblyQty));
		//alert("avlqty " + avlqty);
		//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, avlqty);
		var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, strQty,locationid);
		var vitemqty=0;
		var vrecid="" ;
		var vlp="",vlocation="",vqty="",vlotnoWithQty="",vremainqty="",location;
		vitemText=nlapiGetLineItemText('item', 'item', m);
		if(arryinvt.length>0)
		{
			var vLot="";
			var vPrevLot="";
			var vLotQty=0;
			var vTempLot="";
			var vBoolFound=false;
			
			//alert("vitemText" + vitemText);
			for (j=0; j < arryinvt.length; j++) 
			{			
				var invtarray= arryinvt[j];  	

				//vitem =searchresults[i].getValue('memberitem');		
				//vitemText =searchresults[i].getText('memberitem');
				

				if(vlp == "" || vlp == null)
					vlp = invtarray[2];
				else
					vlp = vlp+","+ invtarray[2];
				if(vTempLot == "" || vTempLot == null)
					vTempLot = invtarray[4];
				else
					vTempLot = vTempLot+","+ invtarray[4];
				if(vPrevLot==null || vPrevLot== "")
				{
					vPrevLot=invtarray[4];
					vLotQty=parseFloat(invtarray[0]);
					vLot=invtarray[4];
					vBoolFound=true;
				}
				else if(vPrevLot==invtarray[4])
				{
					vLotQty=vLotQty+ parseFloat(invtarray[0]);						
				}
				else if(vPrevLot!=invtarray[4])
				{
					if(vlotnoWithQty == "" || vlotnoWithQty == null)
						vlotnoWithQty = vPrevLot+"(" + vLotQty +")";//invtarray[7] is lotno id
					else
						vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + vPrevLot +"(" + vLotQty +")";

					if(vLot == "" || vLot == null)
						vLot = invtarray[4];
					else
						vLot = vLot+","+ invtarray[4];

					vPrevLot=invtarray[4];
					vLotQty=parseFloat(invtarray[0]);

				}
				if(vqty == "" || vqty == null)
					vqty = invtarray[0];
				else
					vqty = vqty+","+ invtarray[0];

				var qty1 = invtarray[0];

				if(vlocation == "" || vlocation == null)
					vlocation = invtarray[6];
				else
					vlocation = vlocation+","+ invtarray[6];




				if(vrecid == "" || vrecid == null)
					vrecid = invtarray[3];
				else
					vrecid = vrecid+","+ invtarray[3];




				if(vremainqty == "" || vremainqty == null)
					vremainqty = invtarray[5];
				else
					vremainqty = vremainqty+","+ invtarray[5];
				location = invtarray[1];
				vitemqty=parseFloat(vitemqty)+parseFloat(qty1);
			}
			if(vBoolFound==true)
			{
				/*if(vlotnoWithQty == "" || vlotnoWithQty == null)
					vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";//invtarray[7] is lotno id
				else
					vlotnoWithQty = vlotnoWithQty+","+ invtarray[4]+"(" + vLotQty +")";*/
				
				if(vlotnoWithQty == "" || vlotnoWithQty == null)
					vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";
				else
					vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + invtarray[4]+"(" + vLotQty +")";


				//alert("vTempLot" + vTempLot);
				//alert("vLot.split(',').length" + vLot.split(',').length);
				// alert("vLotQty " + vlotnoWithQty);				

				if(vLot.split(',').length>1)
					vLot=vlotnoWithQty;
			}
			//if(avlqty > vitemqty )
			if(strQty > vitemqty )			
			{ 
				vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";
				 
			}
			else 
			{
				/*alert("vlp" + vlp);
				alert("location" + location);
				alert("vrecid" + vrecid);
				alert("vLot" + vLot);
				alert("vTempLot" + vTempLot);
				alert("vqty" + vqty);
				alert("vlocation" + vlocation);*/
				
				nlapiSelectLineItem('item',m);
				try {
					nlapiSetCurrentLineItemValue('item','custcol_ebiz_lp',vlp);
					nlapiSetCurrentLineItemValue('item','custcol_locationsitemreceipt', location);//previously it is vlocation
					nlapiSetCurrentLineItemValue('item','custcol_ebizwoinventoryref', vrecid);
                                        // For default Lot
					vLot=vitemText.replace(/ /g,"-");
					//alert("vLot "+ vLot);
					nlapiSetCurrentLineItemValue('item','serialnumbers', vLot);
					
					/*var newLot = vLot.split(','); 
					newLot = newLot.join(String.fromCharCode(5)); 
					nlapiSetCurrentLineItemValue('item','serialnumbers', newLot);*/
					
					if(vTempLot != null && vTempLot != "")
						nlapiSetCurrentLineItemValue('item','custcol_ebizwolotinfo', vTempLot);
					nlapiSetCurrentLineItemValue('item','custcol_ebizwoavalqty', vqty);
					nlapiSetCurrentLineItemValue('item','custcol_ebizwobinloc', vlocation);
					nlapiSetCurrentLineItemValue('item','custcol_ebizwolotlineno', m);
					nlapiCommitLineItem('item');
					 
				}
				catch (e) {

					if (e instanceof nlobjError) 
					{
						 
						alert('system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
					}

					else 
					{ 
						alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
					}
					nlapiCancelLineItem('item');
					break;
					 
				}
			}
		}
		else
		{
			vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";
			nlapiSelectLineItem('item',m);
				try {
					nlapiSetCurrentLineItemValue('item','custcol_ebiz_lp','');
					nlapiSetCurrentLineItemValue('item','custcol_locationsitemreceipt', '');//previously it is vlocation
					nlapiSetCurrentLineItemValue('item','custcol_ebizwoinventoryref', '');
					 
					nlapiSetCurrentLineItemValue('item','serialnumbers', '');
 
						nlapiSetCurrentLineItemValue('item','custcol_ebizwolotinfo', '');
					nlapiSetCurrentLineItemValue('item','custcol_ebizwoavalqty', '');
					nlapiSetCurrentLineItemValue('item','custcol_ebizwobinloc', vlocation);
					nlapiSetCurrentLineItemValue('item','custcol_ebizwolotlineno', '');
					nlapiCommitLineItem('item');

				}
				catch (e) {

					if (e instanceof nlobjError) 
					{

						alert('system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
					}

					else 
					{ 
						alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
					}
					nlapiCancelLineItem('item');
					break;
								 
				}
			 
		}
	}
	if(vAlert!=null && vAlert!='')
	{//alert("vAlertelse");
		alert(vAlert);
		return false;
	}
}
	 

function PickStrategieKittoStock(item,itemfamily,itemgroup,avlqty,locationid){
	//alert("5");
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','itemgroup',itemgroup);
	nlapiLogExecution('ERROR','itemfamily',itemfamily);
//alert('locationid pickrule ' + locationid);
	var filters = new Array();	
	filters[0] = new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]);
	var k=1;
	if (itemgroup != "" && itemgroup != null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskugrouppickrul',null,  'anyof', ['@NONE@',itemgroup]);
		k=k+1;
	}
	if (itemfamily!= "" && itemfamily!= null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@',itemfamily]);
	}
	if (locationid!= "" && locationid!= null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', ['@NONE@',locationid]);
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	//Fetching pick rule
	nlapiLogExecution('ERROR','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

//alert('locationid pickrule into results ' + locationid);
		var searchresultpick = searchresults[i];				
		var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');			

		nlapiLogExecution('ERROR', "LP: " + LP);
		nlapiLogExecution('ERROR','PickZone',vpickzone);			
		var filterszone = new Array();	
		filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', vpickzone,null);

		var columnzone = new Array();
		columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');

		var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
		nlapiLogExecution('ERROR','Loc Group Fetching');

		for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {

//alert('locationid locgroup results ' + locationid);
			var searchresultzone = searchresultszone[j];
			var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');

			nlapiLogExecution('ERROR', 'LocGroup'+ searchresultzone.getValue('custrecord_locgroup_no'), searchresultzone.getText('custrecord_locgroup_no'));

			var filtersinvt = new Array();

			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
			filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));
			filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',locationid));
			
			nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno');
			columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
			columnsinvt[7] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
			columnsinvt[5].setSort();
			columnsinvt[4].setSort(false);

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

//alert('locationid createInv ' + locationid);
				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
				var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
				var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
				var Recid = searchresult.getId(); 
//alert('create inv locaton : ' + searchresult.getValue('custrecord_ebiz_inv_loc'));

				//alert(" 664 actqty:" +actqty);
				//alert(" 664 allocqty:" +allocqty);
				//alert(" 664 LP:" +LP);
				//alert(" 664 vactLocation:" +vactLocation);
				//alert(" 664 vactLocationtext:" +vactLocationtext);
				//alert(" 664 vlotno:" +vlotno);
				//alert(" 664 vlotText:" +vlotText);
				//alert(" 664 Recid:" +Recid);


				nlapiLogExecution('ERROR', 'Recid1', Recid);
				if (isNaN(allocqty)) {
					allocqty = 0;
				}
				if (allocqty == "" ||  parseFloat(allocqty)<0) {
					allocqty = 0;
				}
				if( parseFloat(actqty)<0)
				{
					actqty=0;
				}
				nlapiLogExecution('ERROR', "allocqty: " + allocqty);
				nlapiLogExecution('ERROR', "LP: " + LP);
				var remainqty=0;
				
				//Added by Ganesh not to allow negative or zero Qtys 
				if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0)
					remainqty = parseFloat(actqty) - parseFloat(allocqty);


				var cnfmqty;

////				alert("remainqty"+remainqty );
////				alert("avlqty"+avlqty );
				if (parseFloat(remainqty) > 0) {

					////alert("parseFloat(avlqty) "+parseFloat(avlqty));

					if ((parseFloat(avlqty) - parseFloat(actallocqty)) <= parseFloat(remainqty)) {
						cnfmqty = parseFloat(avlqty) - parseFloat(actallocqty);
						actallocqty = avlqty;
						////alert('cnfmqty in if '+cnfmqty);
					}
					else {
						cnfmqty = remainqty;
						////alert('cnfmqty2 ' +cnfmqty);
						actallocqty = parseFloat(actallocqty) + parseFloat(remainqty);
					}

					//alert(" 708 cnfmqty:" +cnfmqty);
					//alert(" 709 actallocqty:" +actallocqty);

					////alert('BeforeIf ' +cnfmqty);
					if (parseFloat(cnfmqty) > 0) {
						////alert('AfterIf ' +cnfmqty);
						//Resultarray
						var invtarray = new Array();
						////alert('cnfmqty3 ' +cnfmqty);
						invtarray[0]= cnfmqty;
						////alert('cnfmqty4 ' +cnfmqty);
						invtarray[1]= vactLocation;
						invtarray[2] = LP;
						invtarray[3] = Recid;
						invtarray[4] = vlotText;
						invtarray[5] =remainqty;
						invtarray[6] =vactLocationtext;
						invtarray[7] =vlotno;
						//alert("cnfmqty "+cnfmqty);
						//alert("cnfmqty " + cnfmqty);
						//alert(vlotno);
						//alert(vlotText);
						nlapiLogExecution('ERROR', 'RecId:',Recid);
						//invtarray[3] = vpackcode;
						//invtarray[4] = vskustatus;
						//invtarray[5] = vuomid;
						Resultarray.push(invtarray);								
					}


				}

				if ((avlqty - actallocqty) == 0) {

					return Resultarray;

				}

			} 
		}
	}

	return Resultarray;  
}
/**
 * Remove duplicates from an array
 * @param arrayName
 * @returns {Array}
 */
var LineArray=new Array();
var LineCount=new Array();
function removeDuplicateElement(arrayName){
	//var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < LineArray.length; j++) {
			if (LineArray[j] == arrayName[i]){
				var c=LineArray[j];
				LineCount[j]=parseFloat(LineCount[j]+1);
				continue label;
			} 
				
		}
		LineArray[j] = arrayName[i];
		LineCount[j]=1;
	}
	return LineArray;
}

/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
function GenerateWOlocations()
{
	//var WOId= nlapiGetFieldValue('tranid');
	var WOId = nlapiGetRecordId();
	nlapiLogExecution('ERROR','WOId','WOId');
	var linkURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_wo_generatelocation_sl', 'customdeploy_ebiz_wo_generatelocation_di');

	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		linkURL = 'https://system.netsuite.com' + linkURL;			
	}
	else if (ctx.getEnvironment() == 'SANDBOX') {
		linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
	}*/
	linkURL = linkURL + '&custpage_workorder=' + WOId;
	window.open(linkURL);
}
/*** up to here ***/
//case# 20148583 starts
function OnSave()
{
//  	if(nlapiGetRecordType() == 'workorder'){
//      	nlapiGetContext().getSubsidiary = function(){
//          return 1;
//        }
//    }
  
	var locationId=nlapiGetFieldValue('location');
	var rolelocation=getRoledBasedLocationNew();
	//alert("eventtype :" + eventtype);
	//alert("name :" + name);
	if(eventtype =='edit')
	{
		var WOId=nlapiGetFieldValue('id');
			var ordersearchResult = getRecordDetails(WOId);
			if(ordersearchResult != null && ordersearchResult != '')
			{

				alert("order is being proceesed,cannot edit");
				return false;
			}
		
	}
		
	if(locationId!=null && locationId !=''&& rolelocation!='null' && rolelocation!=null && rolelocation!='' && rolelocation!=0)
	{
		
		if(rolelocation.indexOf(parseInt((locationId)))== -1)
		{
			alert("INVALID LOCATION");
			return false;
		}
		else
		{
			return true;
		}
		
	}
	return true;
}
//case# 20148583 ends





/**
 * client event when user removes line in an WO
 * @param type
 * @returns {Boolean}
 */

function DeleteWoLine(type)
{
	var WOId=nlapiGetFieldValue('id');
	var lineNo =nlapiGetCurrentLineItemValue('item','line');
	//alert("type :" + type);
	//alert("WOId :" + WOId);
	//alert("lineNo :" + lineNo);
	//alert("lineNo :" + lineNo);
	if(WOId!=null && WOId!='')
	{
		//getting search results from open task with workorder Id
		var searchRecords = getRecordDetails(WOId);
		//alert("searchRecords :" + searchRecords);
		if(searchRecords != null && searchRecords!='' && searchRecords.length>0)
		{
			alert("This Line Item is already considered for Workorder process in WMS, so you cannot delete it.");
			return false;		
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}



function onChange(type,name,line)
{
	
	try
	{
		var WOId=nlapiGetFieldValue('id');
		var lineNo =nlapiGetCurrentLineItemValue('item','line');
		//alert("type :" + type);
		//alert("WOId :" + WOId);
		//alert("lineNo :" + lineNo);
		//alert("name :" + name);
		if(WOId!=null && WOId!='')
		{
			if(((name=='item')||(name=='quantity')) && (lineNo !=null && lineNo!=''))
			{
				var searchRecords = getRecordDetails(WOId);
				if(searchRecords != null && searchRecords!='' && searchRecords.length>0)
				{
					alert("This Line Item is already considered for Workorder process in WMS, so you cannot change it.");
					return false;		
				}
				else
				{
					return true;
				}
			}
		
		}
		else
		{
			return true;
		}
		
	}
	catch(e)
	{
		//return true;
	}
}

function onInsertLine(type)
{
	//alert('HI');
	var WOId=nlapiGetFieldValue('id');
	var seletedline = nlapiGetCurrentLineItemValue('item', 'line');	
	//alert('seletedline'+seletedline);
	if(WOId!=null && WOId!='' && (seletedline=='' || seletedline==null))
	{
		//getting search results from open task with workorder Id
		var searchRecords = getRecordDetails(WOId);
		//alert("searchRecords :" + searchRecords);
		if(searchRecords != null && searchRecords!='' && searchRecords.length>0)
		{
			alert("This Line Item is already considered for Workorder process in WMS, so you cannot add items.");
			return false;		
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}


/*function OnSave(eventtype)
{
	//alert('eventtype'+eventtype);
	if(eventtype =='edit')
	{
		var WOId=nlapiGetFieldValue('id');
			var ordersearchResult = getRecordDetails(WOId);
			if(ordersearchResult != null && ordersearchResult != '')
			{

				alert("order is being proceesed,cannot edit");
				return false;
			}
		
	}	
}*/



/**
 * searching for records
 * @param WOId
  * @returns
 */
function getRecordDetails(WOId){
	var filter= new Array();

	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', WOId));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_line_no');

	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, columns);
	return searchRecords;
}
//case# 20149993
var eventtype;
function setDefaultValue(type)
{
	eventtype=type;
	//alert("type "+type)
}