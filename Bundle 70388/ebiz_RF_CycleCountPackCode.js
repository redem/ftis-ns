/***************************************************************************
eBizNET Solutions Inc 
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountPackCode.js,v $
 *  $Revision: 1.10.2.2.4.6.2.11.2.2 $
 *  $Date: 2015/11/26 13:38:47 $
 *  $Author: schepuri $
 *  $Name: t_WMS_2015_2_StdBundle_1_189 $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: ebiz_RF_CycleCountPackCode.js,v $
 *  Revision 1.10.2.2.4.6.2.11.2.2  2015/11/26 13:38:47  schepuri
 *  case# 201415820
 *
 *  Revision 1.10.2.2.4.6.2.11.2.1  2015/11/16 15:36:14  rmukkera
 *  case # 201415115
 *
 *  Revision 1.10.2.2.4.6.2.11  2015/04/30 13:09:41  schepuri
 *  case# 201412516
 *
 *  Revision 1.10.2.2.4.6.2.10  2014/06/13 10:07:56  skavuri
 *  Case# 20148882 (added Focus Functionality for Textbox)
 *
 *  Revision 1.10.2.2.4.6.2.9  2014/05/30 00:34:20  nneelam
 *  case#  20148622
 *  Stanadard Bundle Issue Fix.
 *
 *  Revision 1.10.2.2.4.6.2.8  2014/01/07 09:39:15  rmukkera
 *  Case # 20126425
 *
 *  Revision 1.10.2.2.4.6.2.7  2013/12/13 14:01:13  schepuri
 *  20126193
 *
 *  Revision 1.10.2.2.4.6.2.6  2013/10/19 13:19:05  rmukkera
 *  Case # 20125161
 *
 *  Revision 1.10.2.2.4.6.2.5  2013/07/11 06:29:00  schepuri
 *  Case# 20123348
 *  validation on scanning packcode based on displayed packcode or base uom packcode
 *
 *  Revision 1.10.2.2.4.6.2.4  2013/05/01 01:03:06  kavitha
 *  CASE201112/CR201113/LOG201121
 *  TSG Issue fixes
 *
 *  Revision 1.10.2.2.4.6.2.3  2013/04/17 16:02:37  skreddy
 *  CASE201112/CR201113/LOG201121
 *  added meta tag
 *
 *  Revision 1.10.2.2.4.6.2.2  2013/03/19 11:48:12  snimmakayala
 *  CASE201112/CR201113/LOG2012392
 *  Production and UAT issue fixes.
 *
 *  Revision 1.10.2.2.4.6.2.1  2013/03/05 13:35:38  rmukkera
 *  Merging of lexjet Bundle files to Standard bundle
 * 
 *  Revision 1.10.2.2.4.6  2013/02/20 22:30:57  kavitha
 *  CASE201112/CR201113/LOG201121
 *  Cycle count process - Issue fixes
 * 
 *  Revision 1.10.2.2.4.5  2013/02/01 07:58:15  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 *  Cyclecountlatestfixed
 * 
 *  Revision 1.10.2.2.4.3  2012/10/05 06:34:12  grao
 *  CASE201112/CR201113/LOG201121
 * 
 *  Converting Multiple language into given suggestions
 * 
 *  Revision 1.10.2.2.4.2  2012/09/26 12:33:26  grao
 *  CASE201112/CR201113/LOG201121
 *  Converting Multi language without small characters
 * 
 *  Revision 1.10.2.2.4.1  2012/09/24 10:08:56  grao
 *  CASE201112/CR201113/LOG201121
 *  Converting Multi Lnaguage
 * 
 *  Revision 1.10.2.2  2012/03/16 14:08:54  spendyala
 *  CASE201112/CR201113/LOG201121
 *  Disable-button functionality is been added.
 * 
 *  Revision 1.10.2.1  2012/02/21 13:23:19  schepuri
 *  CASE201112/CR201113/LOG201121
 *  function Key Script code merged
 * 
 *  Revision 1.10  2011/12/08 07:27:16  spendyala
 *  CASE201112/CR201113/LOG201121
 *  solved issue related to actual item display.
 * 
 *
 ****************************************************************************/

/**
 * @author LN
 *@version
 *@date
 */
function CycleCountPackCode(request, response){
	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		var st1,st2,st3,st4,st5,st6,st8;

		if( getLanguage == 'es_ES')
		{		
			st1 = "INVENTARIO C&#205;CLICO";
			st2 = "INGRESAR / ESCANEAR C&#211;DIGO DEL PAQUETE";
			st3 = "";
			st4 = "CONT";
			st5 = "ANTERIOR";
			st6 = "INVENTARIO C&#205;CLICO C&#211;DIGO DE PAQUETE";
			st8 = "C&#211;DIGO DE PAQUETE";

		}
		else
		{   
			st1 = "CYCLE COUNT";
			st2 = "ENTER/SCAN";			
			st3 = "ACTUAL PACK CODE";
			st4 = "CONT";
			st5 = "PREV";
			st6 = "CYCLE COUNT PACK CODE";
			st8 = "PACK CODE";

		}
		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);
		
		var rulevalue =  request.getParameter('custparam_ruleValue');
		nlapiLogExecution('ERROR', 'rulevalue in get', rulevalue);
		var getRecordId = request.getParameter('custparam_recordid');
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName =  request.getParameter('custparam_begin_location_name');
		var getPackCode =  request.getParameter('custparam_packcode');
		var getExpectedQuantity =  request.getParameter('custparam_expqty');
		var getExpLPNo = request.getParameter('custparam_lpno');
		var getActLPNo = request.getParameter('custparam_actlp');
		var getExpItem = request.getParameter('custparam_expitem');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');
		var getActualItem = request.getParameter('custparam_actitem');
		var itemInternalid = request.getParameter('custparam_expiteminternalid');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var planitem=request.getParameter('custparam_planitem');
		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountpc'); 
		var html = "<html><head><title>" + st6 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterpackcode').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountpc' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + "</td></tr>";
		html = html + "				<tr><td align = 'left'>" + st8 + " :<label>" + getPackCode+ "</label></td></tr>";
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";		
		html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnActualItem' value='" + getActualItem + "'>";
		html = html + "				<input type='hidden' name='hdniteminternalid' value=" + itemInternalid + ">";   
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";
		html = html + "				<input type='hidden' name='hdnrulevalue' value=" + rulevalue + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + "</td></tr>";
		html = html + "				<tr><td align = 'left'>"+ st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterpackcode' id='enterpackcode' type='text' value=''/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st4+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false' style='width:40;height:40;'/>";
		html = html + "					"+st5+" <input name='cmdPrevious' type='submit' value='F7' style='width:40;height:40;'/>";
		html = html + "					SKIP <input name='cmdSkip' type='submit' value='F8' style='width:40;height:40;'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterpackcode').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var CCarray = new Array();

		var getActualPackCode = request.getParameter('enterpackcode');
		
		// case no 20126193� 

		//if(getActualPackCode == "")
		//	getActualPackCode = request.getParameter('hdnPackCode');

		var optedEvent = request.getParameter('cmdPrevious');
		nlapiLogExecution('DEBUG', 'Key pressed', optedEvent);

		var CCarray = new Array();
		//var optedEvent = request.getParameter('cmdPrevious');
		var optedEvent1 = request.getParameter('cmdSkip');
		var getLanguage = request.getParameter('hdngetLanguage');
		CCarray["custparam_language"] = getLanguage;
		CCarray["custparam_ruleValue"] = request.getParameter('hdnrulevalue');
		nlapiLogExecution('ERROR', 'getLanguage', CCarray["custparam_language"]);
	

		var st6,st7;
		if( getLanguage == 'es_ES')
		{

			st7= "C&#211;DIGO DE PAQUETE NO V&#193;LIDO";
			//Case # 20126425� Start
			st8="POR FAVOR ENTRAR C�DIGO PAQUETE";
			//Case # 20126425� End
		}
		else
		{

			st7="INVALID PACK CODE";
			//Case # 20126425� Start
			st8="PLEASE ENTER PACK CODE";
			//Case # 20126425� End
		}
		CCarray["custparam_error"] = st7;
		CCarray["custparam_screenno"] = '32';
		CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
		CCarray["custparam_planno"] = request.getParameter('custparam_planno');
		CCarray["custparam_begin_location_id"] = request.getParameter('custparam_begin_location_id');
		CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
		CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
		CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
		CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
		CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
		CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');
		CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
		CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
		CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
		CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
		//CCarray["custparam_iteminternalid"] = request.getParameter('hdniteminternalid');
		CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
		CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');
		CCarray["custparam_actbatch"]='';
//case 201412516		
CCarray["custparam_siteid"] =request.getParameter('custparam_siteid');

		if (optedEvent == 'F7') 
		{ 
			var planitem = request.getParameter('hdnplanitem');
			CCarray["custparam_expitem"] = planitem;
			var cycexeplanId=request.getParameter('custparam_recordid');
		// Case # 20125161�Start
			if(cycexeplanId !=null && cycexeplanId!='')
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('internalid', null, 'is', cycexeplanId));
				filters.push(new nlobjSearchFilter('custrecord_cycle_notes', null, 'is', 'fromaddnewitem'));
				filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				

				var cycleCountExecutionRecords = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);

				if(cycleCountExecutionRecords != null && cycleCountExecutionRecords != '')
				{
					nlapiLogExecution('DEBUG', 'Cycle Count PackCode F7 Pressed');
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cyc_addnewitem', 'customdeploy_ebiz_rf_cyc_addnewitem_di', false, CCarray);
				}
				else
				{
					nlapiLogExecution('DEBUG', 'Cycle Count PackCode F7 Pressed');
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_sku', 'customdeploy_rf_cyclecount_sku_di', false, CCarray);
				}
			}
			else
				{
				nlapiLogExecution('DEBUG', 'Cycle Count PackCode F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_sku', 'customdeploy_rf_cyclecount_sku_di', false, CCarray);
			}
		//Case # 20125161�end	
		}
		else if(optedEvent1=='F8')
		{
			nlapiLogExecution('ERROR','SKIP','SKIPthetask');
			nlapiLogExecution('ERROR', 'getRecordId', CCarray["custparam_recordid"]);
			var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'));
			var skipcount=CCRec.getFieldValue('custrecord_cyc_skip_task');
			nlapiLogExecution('ERROR','skipcount',skipcount);

			if(skipcount=='' || skipcount==null)
			{
				skipcount=0;
			}
			skipcount=parseInt(skipcount)+1;
			CCRec.setFieldValue('custrecord_cyc_skip_task',skipcount);
			var id=	nlapiSubmitRecord(CCRec,true);
			nlapiLogExecution('ERROR','skipid',id);


			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
		}
		else 
		{
			if (getActualPackCode != null && getActualPackCode != '') 
			{
				var baseUOMPackcode='';
				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', CCarray["custparam_actitem"]));//internal id of actual item
				filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
				filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebizpackcodeskudim');

				var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

				if(skuDimsSearchResults != null && skuDimsSearchResults != '')
				{
					baseUOMPackcode = skuDimsSearchResults[0].getValue('custrecord_ebizpackcodeskudim');
				}
				
				nlapiLogExecution('ERROR', 'packcode actual',getActualPackCode );
				nlapiLogExecution('ERROR', 'packcode base uom',baseUOMPackcode );
				nlapiLogExecution('ERROR', 'packcode hdn varaible',request.getParameter('hdnPackCode') );
				
				if((getActualPackCode == baseUOMPackcode) || getActualPackCode == request.getParameter('hdnPackCode'))
				{
					CCarray["custparam_actpackcode"] = getActualPackCode ;
					nlapiLogExecution('ERROR', 'Item Internal Id',request.getParameter('custparam_actitem') );
					var ItemType = nlapiLookupField('item', request.getParameter('custparam_actitem'), ['recordType','custitem_ebizbatchlot']);
					nlapiLogExecution('ERROR', 'Cycle Count PackCode1',getActualPackCode );
					nlapiLogExecution('ERROR', 'ItemType',ItemType.recordType );
					nlapiLogExecution('ERROR', 'Batch/Lot',ItemType.custitem_ebizbatchlot );
					if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.custitem_ebizbatchlot=='T' ) {
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecountbatch', 'customdeploy_rf_cyclecountbatch_di', false, CCarray);
						return;
					}
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					return;
				}


			}
			else 
			{ 
				//Case # 20126425� Start
				CCarray["custparam_error"] = st8;
				//Case # 20126425� End
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
				nlapiLogExecution('DEBUG', 'Catch : Cycle Count PackCode not found');
			}
		}
	}
}
