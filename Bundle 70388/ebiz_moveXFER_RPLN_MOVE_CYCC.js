/***************************************************************************
                          eBizNET Solution Inc
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Scheduled/Attic/ebiz_moveXFER_RPLN_MOVE_CYCC.js,v $
*  $Revision: 1.1.2.1 $
*  $Date: 2013/10/25 15:53:56 $
*  $Author: spendyala $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_moveXFER_RPLN_MOVE_CYCC.js,v $
*  Revision 1.1.2.1  2013/10/25 15:53:56  spendyala
*  CASE201112/CR201113/LOG201121
*  New Files for the case#0125362
*
*
****************************************************************************/

function closeMoveXferTasks()
{
	var context = nlapiGetContext();

	//TO get the date which is 60 days past from now.
	var currentdate = new Date();
	var requiredDate='';
	currentdate.setDate(currentdate.getDate()-parseInt(60));
	requiredDate=((currentdate.getMonth()+1)+"/"+(currentdate .getDate())+"/"+(currentdate.getFullYear()));
	nlapiLogExecution('ERROR', 'RequiredDate', requiredDate);
	//End of date conversion.

	var searchrecord=GetOpenTaskRecordsIDs(-1,requiredDate);
	if(searchrecord!=null && searchrecord!='')
	{
		for ( var count = 0; count < searchrecord.length; count++)
		{
			var tempSearchRecord=searchrecord[count];
			if(tempSearchRecord!=null && tempSearchRecord.length>0)
			{
				for ( var tempcount = 0; tempcount < tempSearchRecord.length; tempcount++)
				{
					if(context.getRemainingUsage()> 30)
					{
						var id=MoveTaskRecord(tempSearchRecord[tempcount].getId());
					}
					else
						break;
				}
			}
		}
	}
}


var TempArray=new Array();
function GetOpenTaskRecordsIDs(maxno,requiredDate)
{
	try
	{
		nlapiLogExecution('Error', 'maxno',maxno);
		var vDate=requiredDate;
		var SOFilters = new Array();
		if(maxno!=-1)
		{
			SOFilters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
		}
		//8-RPLN, 9-MOVE, 18-XFER  7-CYCC
		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8,9,18,7]));
		
		//19 Inventory in Storage ,21 Cycle Count Ignored ,22 Cycle Count Resolved ,32 Transaction Closed 
		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [19,21,22,32]));
		
		SOFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'before', vDate));
		SOFilters.push(new nlobjSearchFilter('isinactive',"custrecord_sku","is","F"));
		SOFilters.push(new nlobjSearchFilter('isinactive',"custrecord_actbeginloc","is","F"));
		SOFilters.push(new nlobjSearchFilter('isinactive',"custrecord_actendloc","is","F"));
		SOFilters.push(new nlobjSearchFilter("custrecord_sku",null,"noneof",["@NONE@"]));
		SOFilters.push(new nlobjSearchFilter('isinactive',"custrecord_upd_ebiz_user_no","is","F"));
		SOFilters.push(new nlobjSearchFilter('isinactive',"custrecord_taskassignedto","is","F"));

		var SOColumns = new Array();
		SOColumns[0] = new nlobjSearchColumn('internalid');
		SOColumns[0].setSort(false);

		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns );	

		if(SearchResults!=null)
		{
			if(SearchResults.length>=1000)
			{
				var maxno1=SearchResults[SearchResults.length-1].getValue(columns[0]);
				TempArray.push(SearchResults);
				GetOpenTaskRecordsIDs(maxno1,vDate);
			}
			else
			{
				TempArray.push(SearchResults);
			}
		}
		return TempArray;
	}
	catch(exp)
	{
		nlapiLogExecution("Debug","Exception in GetOpenTaskRecordsIDs",exp);
	}
}