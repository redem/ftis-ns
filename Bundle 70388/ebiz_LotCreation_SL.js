function CreateLot(request,response)
{
	if(request.getMethod()=='GET')
	{
		var vItemId="";
		var vItemStatus="";
		var vLocationId="";
		var ReqQty="";	
		var sFlag="";
		var totalQty = 0;
		var vavailableQty=-1;
		try
		{
			nlapiLogExecution('ERROR', 'request.getParameter(custparam_item)',request.getParameter('custparam_item') );
			var LOTArray=request.getParameter("custparam_lotarray");
			var LOTcount=request.getParameter("custparam_lotcount");
			vItemId=request.getParameter('custparam_item');
			vItemStatus=request.getParameter('custparam_itemstatus');
			vLocationId=request.getParameter('custparam_location');
			ReqQty=request.getParameter('custparam_qty');
			var vCntrl=request.getParameter('cus_sl');
			sFlag=request.getParameter('cus_flag');
			var vlineno=request.getParameter('custparam_line');

			vavailableQty=request.getParameter('custparam_availqty');
			nlapiLogExecution('ERROR', 'vavailableQty',vavailableQty );
			if(vavailableQty==null||vavailableQty=="")
				vavailableQty=ReqQty;

			nlapiLogExecution('ERROR', 'ItemId',vItemId );
			nlapiLogExecution('ERROR', 'ItemStatus',vItemStatus);
			nlapiLogExecution('ERROR', 'vLocationId',vLocationId);
			nlapiLogExecution('ERROR', 'ReqQty',ReqQty);
			nlapiLogExecution('ERROR', 'sFlag',sFlag);
			nlapiLogExecution('ERROR', 'LOTArray1',LOTArray);
			nlapiLogExecution('ERROR', 'LOTcount',LOTcount);
			var Lotresarray="";
			if(LOTArray!=null&&LOTArray!="")
				Lotresarray=LOTArray.split("$");
			nlapiLogExecution('ERROR', 'Lotresarray',Lotresarray);

			var ItemType = nlapiLookupField('item', vItemId, 'recordType');
			nlapiLogExecution('ERROR','ItemType1',ItemType);

			var ItemRecord = nlapiLookupField('item',vItemId, ['recordType', 'itemid' ]);
			var vItemName=ItemRecord.itemid;

			var form = nlapiCreateForm('Select/Create Lot#');
			form.setScript('customscript_ebiz_lot_creation_cl');
			var sublist = form.addSubList("custpage_items", "inlineeditor", "PO List");
			sublist.addField("custpage_item", "text", "Item").setDisabled(true)
			.setDefaultValue(vItemName);
			sublist.addField("custpage_lot", "text", "LOT#");
			sublist.addField("custpage_qty", "text", "Qty");
			sublist.addField("custpage_expdate", "date", "ExpiryDate").setMandatory(true);
			sublist.addField("custpage_fifodate", "date", "FifoDate");
			sublist.addField("custpage_loc", "text", "Location").setDisplayType('hidden').setDefaultValue(vLocationId);
			sublist.addField("custpage_lineno", "text", "LineNo").setDisplayType('hidden').setDefaultValue(vlineno);
			sublist.addField("custpage_itemid", "text", "ItemId").setDisplayType('hidden').setDefaultValue(vItemId);
			sublist.addField("custpage_recqty", "text", "Recommeded Qty").setDisplayType('hidden').setDefaultValue(ReqQty);
			sublist.addField("custpage_isalreadyexist", "text", "IsBatch AlreadyExist").setDisplayType('hidden');
			var button = form.addSubmitButton('Submit'); 

		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'exp',exp );
		}
		response.writePage(form);
	}
	else
	{

	}
}






















function CreateLot1(request,response)
{
	if(request.getMethod()=='GET')
	{
		var vItemId="";
		var vItemStatus="";
		var vLocationId="";
		var ReqQty="";	
		var sFlag="";
		var totalQty = 0;
		var vavailableQty=-1;
		try
		{
			nlapiLogExecution('ERROR', 'request.getParameter(custparam_item)',request.getParameter('custparam_item') );
			if(request.getParameter('custparam_item')!=null&& request.getParameter('custparam_item')!="")
			{
				var LOTArray=request.getParameter("custparam_lotarray");
				var LOTcount=request.getParameter("custparam_lotcount");
				vItemId=request.getParameter('custparam_item');
				vItemStatus=request.getParameter('custparam_itemstatus');
				vLocationId=request.getParameter('custparam_location');
				ReqQty=request.getParameter('custparam_qty');
				var vCntrl=request.getParameter('cus_sl');
				sFlag=request.getParameter('cus_flag');

				vavailableQty=request.getParameter('custparam_availqty');
				nlapiLogExecution('ERROR', 'vavailableQty',vavailableQty );
				if(vavailableQty==null||vavailableQty=="")
					vavailableQty=ReqQty;

				nlapiLogExecution('ERROR', 'ItemId',vItemId );
				nlapiLogExecution('ERROR', 'ItemStatus',vItemStatus);
				nlapiLogExecution('ERROR', 'vLocationId',vLocationId);
				nlapiLogExecution('ERROR', 'ReqQty',ReqQty);
				nlapiLogExecution('ERROR', 'sFlag',sFlag);
				nlapiLogExecution('ERROR', 'LOTArray1',LOTArray);
				nlapiLogExecution('ERROR', 'LOTcount',LOTcount);
				var Lotresarray="";
				if(LOTArray!=null&&LOTArray!="")
					Lotresarray=LOTArray.split("$");
				nlapiLogExecution('ERROR', 'Lotresarray',Lotresarray);

				var ItemType = nlapiLookupField('item', vItemId, 'recordType');
				nlapiLogExecution('ERROR','ItemType1',ItemType);

				var ItemRecord = nlapiLookupField('item',vItemId, ['recordType', 'itemid' ]);
				var vItemName=ItemRecord.itemid;

				var functionkeyHtml=getsubmitvalue('_rf_cluster_no',vCntrl,ReqQty,sFlag);  
				var html = "<html><head>";
				html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
				html = html + " document.getElementById('enterquantity').focus();";        
				html = html + "</script>";   	

				html = html +functionkeyHtml;
				//html = html + "</head><body onkeydown='return OnSubmit_CL();'>";
				html = html + "</head><body>";
				html = html + "	<form name='_rf_cluster_no' method='POST'>";
				if(vavailableQty!=0&&vavailableQty!=-1)
				{
					html = html + "		<table cellspacing=0 cellpadding=0>";
					html = html + "			<tr>";
					html = html + "				<td align = 'Center'><h2> Create Lot Numbers For the ITEM:"+vItemName+"</h2> ";
					html = html + "				</td>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					html = html + "		<table id='tbl' border=1 cellspacing=0 cellpadding=0  >";
					html = html + "			<tr style=\"font-face=Tahoma;font-weight:bold;background-color:gray;color:white;\">";
					html = html + "				<td align = 'Center' style='font-size: 14px;'>Item#";
					html = html + "				</td>";
					html = html + "				<td align = 'Center' style='font-size: 14px;'>Lot#";
					html = html + "				</td>";
					html = html + "				<td align = 'Center' style='font-size: 14px;'>Quantity";
					html = html + "				</td>";
					html = html + "				<td align = 'Center' style='font-size: 14px;'>QuantityRem";
//					html = html + "				<td style = 'display:none'>";
					html = html + "				</td>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					html = html + "				<td>";
					html = html+ "				<label id='lblitemid'>" + vItemName + "</label>";
					html = html + "				</td>";
					html = html + "				<td>";
					html = html +"				<input id='enterlot' name='enterlot' type='text' style='text-align: right'/>";
					html = html + "				</td>";
					html = html + "				<td>";
					html = html +"				<input id='enterquantity' name='enterquantity' type='text' style='text-align: right'/>";
					html = html + "				</td>";
					html = html + "				<td>";
					html = html+ "				<label id='lblitemid'>" + vavailableQty + "</label>";
					html = html + "				<input type='hidden' name='hdnavailableqty' value=" + vavailableQty + ">";
					html = html + "				<input type='hidden' name='hdnLOTArray' value=" + LOTArray + ">";
					html = html + "				</td>";
					html = html + "			</tr>";
					html = html + "		 </table>";
					html = html + "			</tr>";
					html = html + "			<tr><td>";
					html = html + "			<input name='btnAdd' type='submit' value='ADD' onclick='return OnSubmit_CL();'/>";
					html = html + "			</td>";
					html = html + "			</tr>";
					html = html + "		 </table>";
					html = html + "		<table id='tb2' border=1 cellspacing=0 cellpadding=0  >";
					html = html + "			<tr>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					html = html + "			</tr>";
					html = html + "		 </table>";
				}
				if(LOTArray!=null&&LOTArray!="")
				{
					html = html + "		<table id='tb3' border=1 cellspacing=0 cellpadding=0  >";
					html = html + "			<tr>";
					html = html + "				<td align = 'Center' colspan='3'><h2> Selected Lot# for the ITEM:"+vItemName+"</h2> ";
					html = html + "				</td>";
					html = html + "			</tr>";
					html = html + "			<tr style=\"font-face=Tahoma;font-weight:bold;background-color:gray;color:white;\">";
					html = html + "				<td align = 'Center' style='font-size: 14px;'>Item#";
					html = html + "				</td>";
					html = html + "				<td align = 'Center' style='font-size: 14px;'>Lot#";
					html = html + "				</td>";
					html = html + "				<td align = 'Center' style='font-size: 14px;'>Quantity";
					html = html + "				</td>";
					html = html + "				<td style = 'display:none'>";
					html = html + "				</td>";
					html = html + "			</tr>";
					for(var i=0;i<Lotresarray.length-1;i++)
					{
						nlapiLogExecution('ERROR', 'Lotresarray.length',Lotresarray.length);
						var Res1=Lotresarray[i];
						nlapiLogExecution('ERROR', 'Res1',Res1);
						var Res=Res1.split(",");
						nlapiLogExecution('ERROR', 'Res',Res);
						html = html + "			<tr>";
						html = html + "				<td>";
						html = html+ "				<label id='lblitemid'>" + vItemName + "</label>";
						html = html + "				</td>";
						html = html + "				<td>";
						html = html+ "				<label id='lbllotid'>" + Res[0] + "</label>";
						html = html + "				</td>";
						html = html + "				<td>";
						html = html+ "				<label id='lblqtyid'>" + Res[1] + "</label>";
						html = html + "				</td>";
						html = html + "				<td>";
						html = html + "				<input type='hidden' name='hdnavailableqty' value=" + vavailableQty + ">";
						html = html + "				</td>";
						html = html + "			</tr>";
					}
					if(vavailableQty==0)
					{
						html = html + "			<tr>";
						html = html + "				<td>";
						html = html + "			<input name='btnsubmit' type='submit' value='SUBMIT' onclick='return OnSubmit_CL();'/>";
						html = html + "				</td>";
						html = html + "			</tr>";
					}
					html = html + "		 </table>";

				}
				html = html + "	</form>";
				html = html + "</body>";
				html = html + "</html>";
				response.write(html);
			}
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception ',exp);
		}
		//response.writePage(form);
	}
	else
	{
		nlapiLogExecution('ERROR','ELSE ',"ELSE");
		
		var lot= request.getParameter('enterlot');
		nlapiLogExecution('ERROR','lot ',lot);

		var qty= request.getParameter('enterquantity');
		nlapiLogExecution('ERROR','qty ',qty);

		var operation= request.getParameter('btnAdd');
		nlapiLogExecution('ERROR','operation ',operation);
		
		var item= request.getParameter('custparam_item');
		nlapiLogExecution('ERROR','item ',item);
		
		var location= request.getParameter('custparam_location');
		nlapiLogExecution('ERROR','location ',location);
		
		var TotalQty= request.getParameter('custparam_qty');
		nlapiLogExecution('ERROR','TotalQty ',TotalQty);
		
		var availableqty= request.getParameter('hdnavailableqty');
		nlapiLogExecution('ERROR','availableqty ',availableqty);
		
		
		var POarray=new Array();
		
		var LOTArray=request.getParameter('hdnLOTArray');
		nlapiLogExecution('ERROR','LOTArrayELSE ',LOTArray);
		
		var LOTcount=request.getParameter("custparam_lotcount");
		
		if(LOTcount==""||LOTcount==null)
			LOTcount=0;
		else
			LOTcount=parseInt(LOTcount)+1;
		nlapiLogExecution('ERROR','LOTcount ',LOTcount);
		
		if(operation=="ADD")
		{
			
			var IsBatchAlreadyExist="F";
			var filter=new Array();
			filter[0]=new nlobjSearchFilter("custrecord_ebizlotbatch",null,"is",lot);
			filter[1]=new nlobjSearchFilter("custrecord_ebizsitebatch",null,"anyof",location);
			
			var res=nlapiSearchRecord("customrecord_ebiznet_batch_entry",null,filter,null);
			if(res!=null&&res!="")
				IsBatchAlreadyExist="T";
			
			LOTArray=lot+','+qty+','+IsBatchAlreadyExist+'$'+LOTArray;
			
			
			POarray["custparam_item"]=request.getParameter('custparam_item');
			POarray["custparam_itemstatus"]=request.getParameter('custparam_itemstatus');
			POarray["custparam_location"]=request.getParameter('custparam_location');
			POarray["custparam_qty"]=request.getParameter('custparam_qty');
			POarray["cus_sl"]=request.getParameter('cus_sl');
			POarray["cus_flag"]=request.getParameter('cus_flag');
			POarray["custparam_lotarray"]=LOTArray;
			POarray["custparam_lotcount"]=LOTcount;
			POarray["custparam_availqty"]=parseInt(availableqty)-parseInt(qty);
			nlapiLogExecution('ERROR','IsBatchAlreadyExist ',IsBatchAlreadyExist);
			nlapiLogExecution('ERROR','LOTArray ',LOTArray);
			response.sendRedirect('SUITELET', 'customscript_ebiz_lot_creation_sl', 'customdeploy_ebiz_lot_creation_sl', false, POarray);
		}
		else
		{

		}
	}
}

function getsubmitvalue(formid,vCntrl,ReqQty,sFlag)
{
	var html=""; 
	html = html + "<SCRIPT LANGUAGE='javascript'>";
	html = html + "function OnSubmit_CL() ";
	html = html + " { ";     
	html = html +" var rows = document.getElementById('tbl').getElementsByTagName('tr').length;";
	html = html + "var vtable = document.getElementById('tbl');";
	html = html + "var varqty=0;";
	html = html + "var rowLength = rows;";
	html = html + "var vText;";
	html = html + "var vLotid;";
	html = html + "var vcount=0;";
//	html = html +"alert(rows);";
//	html = html +"alert(navigator.userAgent.indexOf('Firefox'));";
	html = html + "if (navigator.userAgent.indexOf('Firefox') !=-1)";//detecting browser
		html = html + "{ ";  
			html = html + "for (var i = 1; i < rows; i++){";
				html = html + "var row = vtable.rows[i];";
//				html = html + "alert(row);";
				html = html + "var vTableCells = row.getElementsByTagName('td');";
//				html = html + "alert(vTableCells[4].firstChild.value);";
				html = html + "if ((vTableCells[4].firstChild.value != null) && (!isNaN(vTableCells[4].firstChild.value))){";
				html = html + "}";
				html = html + "else{";
				html = html + "alert('Please Enter Numbers Only');";
				html = html + " return false; ";
				html = html + "}";
				html = html + "if(parseFloat(vTableCells[4].firstChild.value)<1){";
				html = html + "alert('Entered Quantity Should not be Less than 1');";
				html = html + " return false; ";
				html = html + "}";
			html = html + "if(vTableCells[0].firstChild.checked){";
				html = html + "if(parseFloat(vTableCells[4].firstChild.value)>parseFloat(vTableCells[3].textContent)){";
				html = html + "alert('Entered Quantity should not be greater than Available Qty for the Lot# :'+vTableCells[2].textContent);";
				html = html + " return false; ";
				html = html + "}";
				html = html + "if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!=''){";
					html = html + "vcount= parseFloat(vcount)+1;";
					html = html + "varqty=varqty+parseFloat(vTableCells[4].firstChild.value);";        
					html = html +"if(vText != null && vText != ''){";
					html = html +"vText=vText+','+vTableCells[2].textContent.trim()+'('+vTableCells[4].firstChild.value+')';";
					html = html +"vLotid=vLotid+','+vTableCells[5].firstChild.value;}";
					html = html +"else { vText=vTableCells[2].textContent.trim()+'('+vTableCells[4].firstChild.value+')';";
					html = html +" vLotid=vTableCells[5].firstChild.value;}";
				html = html +"}";
			html = html +"}";
				html = html +"else{";
					html = html +"if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!=''){";
					html = html +"alert('Entered Quantity for unselected Lot# '+vTableCells[2].textContent);";
					html = html + " return false; }";
				html = html + " }";
	        html = html + "}"; 
	html = html + "}";
	html = html + " else";
	html = html + "{";
//	html = html + "alert(navigator.userAgent.indexOf('Firefox'));";
//	html = html + "alert(rows);";
	html = html + "for (var i = 1; i < rows; i++){";
	html = html + "var row = vtable.rows[i];";
	html = html +"var vTableCells = row.getElementsByTagName('td');";
//	html = html + "alert(vTableCells);";
//	html = html + "alert(vTableCells[3].firstChild.value);";
	
	html = html +" if ((vTableCells[1].firstChild.value != '')&&(vTableCells[1].firstChild.value != null)){";
	html = html +"}";
	html = html +"else{";
	html = html + "alert('Please Enter LOT#');";
	html = html + " return false; ";
	html = html + "}";
	
	html = html +" if ((vTableCells[2].firstChild.value != '')&&(vTableCells[2].firstChild.value != null)){";
	html = html +"}";
	html = html +"else{";
	html = html + "alert('Please Enter Qty');";
	html = html + " return false; ";
	html = html + "}";
	
	html = html + "if(parseFloat(vTableCells[2].firstChild.value)<=0){";
	html = html + "alert('Entered Quantity Should not be less than or equal to 0');";
	html = html + " return false; ";
	html = html + "}";
	
	html = html + "if(vTableCells[0].firstChild.checked){";
	html = html +"if(parseFloat(vTableCells[4].firstChild.value)>parseFloat(vTableCells[3].innerText)){";
	html = html +"alert('Entered Quantity should not be greater than Available Qty for the Lot# :'+vTableCells[2].innerText);";
	html = html + " return false; ";
	html = html +"}";

	html = html +"if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!=''){";
	html = html +"vcount= parseFloat(vcount)+1;";
	html = html +"varqty=varqty+parseFloat(vTableCells[4].firstChild.value);";        
	html = html +"if(vText != null && vText != ''){";
	html = html +"vText=vText+','+vTableCells[2].innerText+'('+vTableCells[4].firstChild.value+')';";
	html = html +"vLotid=vLotid+','+vTableCells[5].firstChild.value;}";
	html = html +"else { vText=vTableCells[2].innerText+'('+vTableCells[4].firstChild.value+')';";
	html = html +" vLotid=vTableCells[5].firstChild.value;}";
	html = html +"}";
	html = html +"}";


	html = html +"else{";
	html = html +"if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!=''){";
	html = html +"alert('Entered Quantity for unselected Lot# '+vTableCells[2].innerText);";
	html = html + " return false; }";
	html = html + " }";

	html = html + "}";

	html = html + "}";

	html = html + "if(vText == null || vText == ''){";
	html = html + "vText='';";
	html = html + "vLotid='';";
	html = html + " }";

	html = html + "if((parseFloat(varqty)>parseFloat('"+ReqQty+"')|| parseFloat(varqty)<parseFloat('"+ReqQty+"'))&& varqty>0){";
	html = html + "alert('Entered Quantity must be equal to Required quantity:'+'"+ReqQty+"');";
	html = html + " return false; ";
	html = html + " }";

	html = html +" if(('"+sFlag+"')=='T')";
	html = html +"{";
	html = html +"if(vcount!=1 && vcount!=0)";
	html = html +"{";
	html = html +"alert('NotToSplit Lot# Flag is checked for the Customer, So Select Quantity from One Lot# ');";
	html = html + " return false; ";
	html = html +"}";
	html = html +"}";
	var vCntrlId= "custpage_lotbatch" + vCntrl;
	var vLotId= "custpage_lotnoid" + vCntrl;
	html = html + "if (navigator.userAgent.indexOf('Firefox') !=-1)";
	html = html +"{";
	html = html + "window.opener.document.getElementById('"+ vCntrlId +"').value = vText;window.opener.focus();target='_self';";
	html = html + "window.opener.document.getElementById('"+ vLotId +"').value = vLotid;window.opener.focus();target='_self';self.close();";
	html = html + "return false;";
	html = html +"}";
	html = html +"else";
	html = html +"{";
	html = html + "window.opener.document.getElementById('"+ vCntrlId +"').value = vText;window.opener.focus();target='_self';self.close();";
	html = html + "window.opener.document.getElementById('"+ vLotId +"').value = vLotid;window.opener.focus();target='_self';self.close();";
	html = html + "return false;";
	html = html +"}";
	html = html + "}";
	
	function Validate()
	{
		
	}
	html = html + " </SCRIPT>";
	return html;
}



function getclose(formid){
	var html=""; 
	html = html + "<SCRIPT LANGUAGE='javascript'>";
	html = html + " var Qty='REC6544'; "; 
	html = html + "function Onclose_CL() ";
	html = html + " { ";   
	html = html + " alert('hi'); "; 
	//html = html + "window.opener.document._rf_cluster_no.custpage_items_custpage_lotbatch1_fs = 0;";
	//html = html + "window.opener.document.'main_form'.custpage_items_custpage_lotbatch1_fs.value = 0;";

	//html = html + "window.opener.document.getElementById('custpage_items_custpage_lotbatch1_fs').value = '0';self.close();";
	html = html + "window.opener.document.getElementById('custpage_lotbatch1').value = 'RVC1235';self.close();";
	//html = html + "window.close();";
	html = html + "return false;";
	html = html + "}";		
	html = html + " </SCRIPT>";
	return html;

}

function scrap()
{
	if (navigator.userAgent.indexOf('Firefox') !=-1)//detecting browser
	{   
		for (var i = 1; i < rows; i++)
		{
			var row = vtable.rows[i];
			var vTableCells = row.getElementsByTagName('td');
			if ((vTableCells[4].firstChild.value != null) && (!isNaN(vTableCells[4].firstChild.value)))
			{
			}
			else
			{
				alert('Please Enter Numbers Only');
				return false; 
			}
			if(parseFloat(vTableCells[4].firstChild.value)<1)
			{
				alert('Entered Quantity Should not be Less than 1');
				return false; 
			}
			if(vTableCells[0].firstChild.checked)
			{
				if(parseFloat(vTableCells[4].firstChild.value)>parseFloat(vTableCells[3].textContent))
				{
					alert('Entered Quantity should not be greater than Available Qty for the Lot# :'+vTableCells[2].textContent);
					return false; 
				}
				if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!='')
				{
					vcount= parseFloat(vcount)+1;
					varqty=varqty+parseFloat(vTableCells[4].firstChild.value);        
					if(vText != null && vText != '')
					{
						vText=vText+','+vTableCells[2].textContent.trim()+'('+vTableCells[4].firstChild.value+')';
						vLotid=vLotid+','+vTableCells[5].firstChild.value;
					}
					else 
					{ 
						vText=vTableCells[2].textContent.trim()+'('+vTableCells[4].firstChild.value+')';
						vLotid=vTableCells[5].firstChild.value;
					}
				}
			}
			else{
				if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!='')
				{
					alert('Entered Quantity for unselected Lot# '+vTableCells[2].textContent);
					return false;
				}
			}
		} 
	}
	else
	{
		for (var i = 1; i < rows; i++){
			var row = vtable.rows[i];
			var vTableCells = row.getElementsByTagName('td');
			if ((vTableCells[4].firstChild.value != null) && (!isNaN(vTableCells[4].firstChild.value)))
			{
			}
			else
			{
				alert('Please Enter Numeric values Only');
				return false; 
			}
			if(parseFloat(vTableCells[4].firstChild.value)<=0)
			{
				alert('Entered Quantity Should not be less than or equal to 0');
				return false; 
			}
			if(vTableCells[0].firstChild.checked)
			{
				if(parseFloat(vTableCells[4].firstChild.value)>parseFloat(vTableCells[3].innerText))
				{
					alert('Entered Quantity should not be greater than Available Qty for the Lot# :'+vTableCells[2].innerText);
					return false; 
				}
				if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!='')
				{
					vcount= parseFloat(vcount)+1;
					varqty=varqty+parseFloat(vTableCells[4].firstChild.value);        
					if(vText != null && vText != '')
					{
						vText=vText+','+vTableCells[2].innerText+'('+vTableCells[4].firstChild.value+')';
						vLotid=vLotid+','+vTableCells[5].firstChild.value;
					}
					else 
					{ 
						vText=vTableCells[2].innerText+'('+vTableCells[4].firstChild.value+')';
						vLotid=vTableCells[5].firstChild.value;
					}
				}
			}
			else
			{
				if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!='')
				{
					alert('Entered Quantity for unselected Lot# '+vTableCells[2].innerText);
					return false; 
				}
			}
		}
	}
	if(vText == null || vText == ''){
		vText='';
		vLotid='';
	}

	if((parseFloat(varqty)>parseFloat('"+ReqQty+"')|| parseFloat(varqty)<parseFloat('"+ReqQty+"'))&& varqty>0)
	{
		alert('Entered Quantity must be equal to Required quantity:'+'"+ReqQty+"');
		return false; 
	}

	if(('"+sFlag+"')=='T')
	{
		if(vcount!=1 && vcount!=0)
		{
			alert('NotToSplit Lot# Flag is checked for the Customer, So Select Quantity from One Lot# ');
			return false; 
		}
	}
	var vCntrlId= "custpage_lotbatch" + vCntrl;
	var vLotId= "custpage_lotnoid" + vCntrl;
	if (navigator.userAgent.indexOf('Firefox') !=-1)
	{
		window.opener.document.getElementById('"+ vCntrlId +"').value = vText;window.opener.focus();target='_self';
		window.opener.document.getElementById('"+ vLotId +"').value = vLotid;window.opener.focus();target='_self';self.close();
		return false;
	}
	else
	{
		window.opener.document.getElementById('"+ vCntrlId +"').value = vText;window.opener.focus();target='_self';self.close();
		window.opener.document.getElementById('"+ vLotId +"').value = vLotid;window.opener.focus();target='_self';self.close();
		return false;
	}
}

