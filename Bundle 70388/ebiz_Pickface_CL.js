/***************************************************************************

   eBizNET SOLUTIONS 
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Masters/Client/ebiz_Pickface_CL.js,v $
 *  $Revision: 1.2.10.1.4.4 $
 *  $Date: 2014/07/08 15:07:22 $
 *  $Author: rmukkera $
 *  $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: ebiz_Pickface_CL.js,v $
 *  Revision 1.2.10.1.4.4  2014/07/08 15:07:22  rmukkera
 *  Case # 20149281
 *
 *  Revision 1.2.10.1.4.3  2014/05/23 06:43:06  gkalla
 *  case#20148398
 *  Sonic Pick face location validation issue
 *
 *  Revision 1.2.10.1.4.2  2013/09/11 15:23:51  rmukkera
 *  Case# 20124376
 *
 *
 ****************************************************************************/
function fnCheckPickFaceEntry(type)
{
	var vpickfaceSku = nlapiGetFieldValue('custrecord_pickfacesku');        
	//  var vSite = nlapiGetFieldValue('custrecord_ebizsiteput');
	var vSite = nlapiGetFieldValue('custrecord_pickface_location');

	// var vCompany = nlapiGetFieldValue('custrecord_ebizcompanyput');
	var vId = nlapiGetFieldValue('id');

	var vminqty= nlapiGetFieldValue('custrecord_minqty');
	var vmaxqty=  nlapiGetFieldValue('custrecord_maxqty');
	var vmaxpickqty=  nlapiGetFieldValue('custrecord_maxpickqty');
	var vreplenqty=  nlapiGetFieldValue('custrecord_replenqty');
	var vroundqty= nlapiGetFieldValue('custrecord_roundqty');
	//var vPackcode = nlapiGetFieldValue('custrecord_pickfacepackcode');


	/*alert("vMaxQty :" + vmaxqty);
	alert("vMinQty :" + vminqty);
	alert("vMaxPickQty :" + vmaxpickqty);
	alert("vRoundQty :" + vroundqty);
	alert("vpickfaceSku :" + vpickfaceSku);
	alert("vId :" + vId);
	alert("vSite :" + vSite);*/
	//alert("vCompany :" + vCompany);
    var filters = new Array();
    filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', vpickfaceSku));
    
    if(vSite !=null && vSite!='')
    	{
    	filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', ['@NONE@', vSite]));
    	}
    //filters[1] = new nlobjSearchFilter('custrecord_ebizcompanyput', null, 'anyof', [vCompany]);
    
    var binLocation = nlapiGetFieldValue('custrecord_pickbinloc');
    filters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', binLocation));
   
    	//filters[1] = new nlobjSearchFilter('internalid', null, 'noneof',vId);	
    	
    var searchresults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters);
    if(vId==null || vId=="")
    {
    	

    	if (searchresults != null && searchresults.length > 0) 
    	{
    		alert("The Put/Pick Zone # already exists.");
    		return false;
    	}
    }
    
    if(vpickfaceSku != "" && vpickfaceSku != null &&  vSite != "" && vSite != null && 
    		vminqty != "" && vminqty != null && vmaxqty != "" && vmaxqty != null && vmaxpickqty != "" && vmaxpickqty != null && vreplenqty != ""  && vreplenqty != null &&
    		vroundqty != "" && vroundqty != null)
	{
	
		if(parseFloat(vmaxqty)<parseFloat(vminqty))    
		{
			alert("Max Qty should be greater than Min Qty ");
			return false;
		}
		if(parseFloat(vmaxqty)<parseFloat(vreplenqty))
		{
			alert("Repln Qty should be less than Max Qty ");
			return false;
		}

		if(parseFloat(vreplenqty)<parseFloat(vroundqty))
		{
			alert("Round Qty should be less than Repln Qty ");
			return false;
		}

		if(parseFloat(vmaxqty)<parseFloat(vmaxpickqty))
		{
			alert("Max Pick Qty should be less than Max Qty ");
			return false;
		}

		if(parseFloat(vmaxpickqty)<parseFloat(vminqty))
		{
			alert("Max Pick Qty should be greater than Min Qty ");
			return false;
		}    

		/* var itemQuantity = getPalletQuantityFromItemDims(vpickfaceSku, vPackcode);
	if(parseFloat(itemQuantity)==0)
	{
		alert("Please Select valid packcode");
		return false; 
	}

	if(parseFloat(vroundqty) > parseFloat(itemQuantity)){
		alert("Round Qty is greater than Pallet Qty");
		return false;    		
	}*/




		return true;
	}
	else
	{
		return true;
	}
}

/*function getPalletQuantityFromItemDims(itemId, packCode){
	var itemQuantity = 0;
	var filters = new Array();          
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is',itemId);
//	filters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', [3]);		  
	filters[1] = new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',packCode);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true);//it fetches the records with highest uomlevel first.

	var searchItemResults = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);
	if(searchItemResults!=null){    		
		itemQuantity = searchItemResults[0].getValue('custrecord_ebizqty');  
	}    	

	return itemQuantity;	
}*/
function onChange(type,name)
{
	//alert('name'+name);
	var vRecordId=nlapiGetFieldValue('id');
	if(name=='custrecord_pickbinloc')
	{
		if(vRecordId !=null && vRecordId !='' && vRecordId !='null')
		{
			var fields = ['custrecord_pickfacesku','custrecord_pickbinloc'];
			var columns = nlapiLookupField('customrecord_ebiznet_pickfaceloc', vRecordId, fields);
			//alert('vRecordId'+vRecordId);
			if(columns != null && columns != '')
			{
				var vItem=columns.custrecord_pickfacesku;
				var vBinLoc=columns.custrecord_pickbinloc;
				//alert('vBinLoc'+vBinLoc);
				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9,20]));//Status Flag - 9 (Pick Locations Assigned), 20 - 	Cycle Count Released
				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3,8]));// 3 -PICK, 8 - RPLN
				filters.push(new nlobjSearchFilter('custrecord_invref_no', null, 'isempty'));
				if(vItem !=null && vItem!='')
				{
					filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', vItem));
				}
				if(vBinLoc !=null && vBinLoc!='')
				{
					filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', vBinLoc));
				}
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('name');
				columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');

				var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
				//alert('SearchResults'+SearchResults);
				if(SearchResults !=null && SearchResults !='' && SearchResults !='null' && SearchResults !='undefined')
				{
					alert('Bin Location is having open picks/replens. Please do not make any changes');
					nlapiSetFieldValue('custrecord_pickbinloc',vBinLoc,false);
					return false;
				}
			}
		}
	}
	return true;
}