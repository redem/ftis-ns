/***************************************************************************
	   eBizNET Solutions Inc
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_PickingStageLocation.js,v $
 *     	   $Revision: 1.1.2.2.4.6.2.6 $
 *     	   $Date: 2015/04/28 15:54:23 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PickingStageLocation.js,v $
 * Revision 1.1.2.2.4.6.2.6  2015/04/28 15:54:23  grao
 * SB issue fixes  201411264
 *
 * Revision 1.1.2.2.4.6.2.5  2014/08/18 12:28:24  snimmakayala
 * Case: 20149972 & 20149973
 * JAWBONE WO RF PICKING ISSUES
 *
 * Revision 1.1.2.2.4.6.2.4  2014/08/04 15:21:49  skreddy
 * case # 20149765/20149766
 * jawbone SB issue fix
 *
 * Revision 1.1.2.2.4.6.2.3  2014/07/31 15:52:39  sponnaganti
 * Case# 20147959
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.2.4.6.2.2  2014/07/25 13:50:24  skreddy
 * case # 20149507
 * jawbone SB issue fix
 *
 * Revision 1.1.2.2.4.6.2.1  2014/07/18 15:21:07  skreddy
 * case # 20149504
 * jawbone SB issue fix
 *
 * Revision 1.1.2.2.4.6  2014/06/20 14:48:01  rmukkera
 * no message
 *
 * Revision 1.1.2.2.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.8.4.3.4.9.2.1  2013/02/26 13:02:23  skreddy

 *
 *
 *****************************************************************************/


function PickingStageLocation(request, response){
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');

	var user=context.getUser();
	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('ERROR', 'Into Page Load');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR AL MONTAJE";
			st2 = "N&#218;MERO DE EMPAQUE:";
			st3 = "UBICACI&#211;N:";
			st4 = "INGRESAR / ESCANEAR ETAPA:";
			st5 = "CONF";
		}
		else
		{
			st1 = "GO TO STAGING ";
			st2 = "CARTON NO : ";
			st3 = "LOCATION : ";
			st4 = "ENTER/SCAN STAGE :";
			st5 = "CONF";
		}

		var getWOid = request.getParameter('custparam_woid');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_newcontainerlp');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');		
		var getSerialNo = request.getParameter('custparam_serialno');
		var vClusterNo = request.getParameter('custparam_clusterno');		
		var name = request.getParameter('name');
		var pickType=request.getParameter('custparam_picktype');	
		var vSOId=request.getParameter('custparam_ebizordno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var whLocation = request.getParameter('custparam_whlocation');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var vBatchno = request.getParameter('custparam_batchno');
		var getItem = '';

		if(getItemName==null || getItemName=='')
		{			
			var filter=new Array();
			if(getItemInternalId!=null&&getItemInternalId!="")
				filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

			var column=new Array();
			column[0]=new nlobjSearchColumn("itemid");
			column[1]=new nlobjSearchColumn("description");

			var searchres=nlapiSearchRecord("item",null,filter,column);
			if(searchres!=null&&searchres!="")
			{
				getItem=searchres[0].getValue("itemid");
				Itemdescription=searchres[0].getValue("description");
			}
		}
		else
		{
			getItem=getItemName;
		}

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		//Code Added for Displaying the Location Name
		var Carrier;
		var Site;
		var Company;
		var stgOutDirection="OUB";
		var carriertype='';
		var wmscarrier='';
		var customizetype='';
		var ordertype='';

		nlapiLogExecution('ERROR', 'getDOLineId',getDOLineId); 
		//nlapiLogExecution('Error', 'WaveNo ', getWaveno);

		if(getDOLineId!=null && getDOLineId!='')
		{
			var columns = new Array();
			var filters = new Array();

			filters.push(new nlobjSearchFilter('internalid', null, 'is',getDOLineId));

			columns[0] = new nlobjSearchColumn('custrecord_do_wmscarrier');	
			columns[1] = new nlobjSearchColumn('custrecord_do_carrier');				
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_wave');
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_ordline_customizetype');
			columns[4] = new nlobjSearchColumn('custrecord_do_order_type');

			var searchresultsord = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
			if(searchresultsord!=null && searchresultsord!='' && searchresultsord.length>0)
			{
				Carrier = searchresultsord[0].getValue('custrecord_do_carrier');
				customizetype = searchresultsord[0].getValue('custrecord_ebiz_ordline_customizetype');
				ordertype = searchresultsord[0].getValue('custrecord_do_order_type');

				if(Carrier!=null && Carrier!='')
				{
					var filterscarr = new Array();
					var columnscarr = new Array();

					filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',Carrier));
					filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

					columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
					columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
					columnscarr[2] = new nlobjSearchColumn('id');

					var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
					if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
					{
						carriertype = searchresultscarr[0].getValue('custrecord_carrier_type');
						wmscarrier = searchresultscarr[0].getId();
					}
				}

				if(Carrier==null || Carrier=='')
					wmscarrier=searchresultsord[0].getValue('custrecord_do_wmscarrier');
				/*if(getWaveno==null || getWaveno=='')
					getWaveno=searchresultsord[0].getValue('custrecord_ebiz_wave');*/
			}
		}

		nlapiLogExecution('ERROR', 'carriertype',carriertype); 
		nlapiLogExecution('ERROR', 'wmscarrier',wmscarrier); 
		nlapiLogExecution('ERROR', 'ordertype',ordertype); 

		var tranordertype = nlapiLookupField('workorder', getOrderNo, 'custbody_nswmssoordertype');

		nlapiLogExecution('ERROR', 'tranordertype',tranordertype); 

		//nlapiLogExecution('Error', 'WaveNo ', getWaveno);

		//var stgLocation = GetPickStageLocation(getItem, Carrier, Site, Company,stgOutDirection);
		var stgLocation = GetPickStageLocation(getItemInternalId, wmscarrier, whLocation, Company,
				stgOutDirection,carriertype,customizetype,null,tranordertype);


		nlapiLogExecution('ERROR', 'After Getting StagingLocation in page load::',stgLocation);
		var FetchBinLocation;
		if(stgLocation!=null && stgLocation!=-1)
		{
			var collocGroup = new Array();
			var filtersLocGroup = new Array();
			collocGroup[0] = new nlobjSearchColumn('name');
			filtersLocGroup.push(new nlobjSearchFilter('internalid', null, 'is',stgLocation));
			var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
			if(searchresultsloc != null && searchresultsloc != '')
				FetchBinLocation=searchresultsloc[0].getValue('name');
		}

		nlapiLogExecution('ERROR', 'After Getting StagingLocation Name in page load::',FetchBinLocation);


		//upto to here

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";
		/*html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('enterstagelocation').focus();";        
		html = html + "</script>";*/
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterstagelocation').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"</td></tr>";
		if(pickType!='CL')
		{
			html = html + "			<tr>";
			/*html = html + "				<td align = 'left'>"+ st2 +" " + getContainerLpNo;
			html = html + "				</td>";*/
			html = html + "			</tr>";

		}
		html = html + "			<tr>";
		if(FetchBinLocation!=null && FetchBinLocation!='' && FetchBinLocation!='undefined' && FetchBinLocation!='null')
		html = html + "				<td align = 'left'>"+ st3 +" " + FetchBinLocation;
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getWOid + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnserialno' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnstgloc' value=" + stgLocation + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnsoid' value=" + vSOId + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwmscarrier' value=" + Carrier + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='vhdnstageloctext' value='" + FetchBinLocation + "'>";
		html = html + "				<input type='hidden' name='vhdncarrier' value=" + wmscarrier + ">";
		html = html + "				<input type='hidden' name='vhdncarriertype' value=" + carriertype + ">";
		html = html + "				<input type='hidden' name='vhdncustomizetype' value=" + customizetype + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st4;//ENTER/SCAN STAGE :
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterstagelocation' id='enterstagelocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +"  <input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterstagelocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st6,st7;

		if( getLanguage == 'es_ES')
		{
			st6 = "LOCALIZACI&#211;N ETAPA NO V&#193;LIDO";
			st7 = "OPCI#211;N V&#193;LIDA";

		}
		else
		{
			st6 = "INVALID STAGE LOCATION";
			st7 = "INVALID OPTION";

		}
		nlapiLogExecution('ERROR', 'Pick Type', request.getParameter('hdnpicktype'));
		var optedEvent = request.getParameter('hdnflag');
		nlapiLogExecution('ERROR', 'optedEvent', optedEvent);
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		var getwoid = request.getParameter('hdnWOid');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');			
		var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var ItemDescription = request.getParameter('hdnItemDescription');
		var ItemInternalId = request.getParameter('hdnItemInternalId');
		var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var EndLocation = request.getParameter('hdnEnteredLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var SerialNo = request.getParameter('hdnserialno');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchno = request.getParameter('hdnbatchno');
		var vPickType = request.getParameter('hdnpicktype');
		var vSOId=request.getParameter('hdnsoid');
		SOarray["custparam_ebizordno"] = vSOId;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		var vCarrier = request.getParameter('hdnwmscarrier');
		var vSite;
		var vCompany;
		var stgDirection="OUB";
		var vDoname=request.getParameter('hdnName');
		var vdono=request.getParameter('hdnDOLineId');
		var vPrevinvrefno=request.getParameter('hdnInvoiceRefNo');
		var vstageLoc=request.getParameter('hdnstgloc');
		var voldcontainer='';
		var vZoneId=request.getParameter('hdnebizzoneno');
		var vhdncarrier=request.getParameter('vhdncarrier');
		var vhdncarriertype=request.getParameter('vhdncarriertype');
		var vhdncustomizetype=request.getParameter('vhdncustomizetype');

		nlapiLogExecution('Error', 'getwoid ', getwoid);
		nlapiLogExecution('Error', 'ContainerLPNo ', ContainerLPNo);
		nlapiLogExecution('ERROR', 'vPickType', vPickType);
		nlapiLogExecution('ERROR', 'vZoneId', vZoneId);
		SOarray["custparam_woid"] = request.getParameter('hdnWOid');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		//nlapiLogExecution('ERROR', 'SOarray["custparam_waveno"]', SOarray["custparam_waveno"]);
		nlapiLogExecution('ERROR', 'SOarray["custparam_recordinternalid"]', SOarray["custparam_recordinternalid"]);
		nlapiLogExecution('ERROR', 'SOarray["custparam_containerlpno"]', SOarray["custparam_containerlpno"]);
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');;
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');		
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');;
		SOarray["custparam_picktype"] = vPickType;
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_newcontainerlp"] = ContainerLPNo;
		var WHLocation = request.getParameter('hdnwhlocation');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');

		if (sessionobj!=context.getUser()) {
			try{
				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (optedEvent == 'ENT') 
				{
					//code added on 170812 by suman.
					//To validate user entered Stage Location.
					var stagelocation=request.getParameter('enterstagelocation');
					var hdnstagelocation=request.getParameter('vhdnstageloctext');

					nlapiLogExecution('ERROR', 'Scanned Stage Location', stagelocation);
					nlapiLogExecution('ERROR', 'Actual Stage Location', hdnstagelocation);
					SOarray["custparam_screenno"] = 'WOstagelocation';
					if(stagelocation!=hdnstagelocation)
					{
						/*var resArray=ValidateStageLocation(ItemInternalId,vhdncarrier,WHLocation,vCompany,
						"OUB",vhdncarriertype,vhdncustomizetype,stagelocation);

				var Isvalidestage=resArray[0];
				if(Isvalidestage==true)
				{
					vstageLoc=resArray[1];
				}
				else
				{*/
						SOarray["custparam_error"] = st6;//'INVALID STAGE LOCATION';
						nlapiLogExecution('ERROR', 'Error: ', 'Invalid Stage Location');
						SOarray["custparam_screenno"] = 'WOstagelocation';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
						//}
					}
					//end of code as on 170812.

					nlapiLogExecution('ERROR', 'ContainerLPNo (Pack Complete)', ContainerLPNo);
					nlapiLogExecution('ERROR', 'WOId (Pack Complete)', getwoid);
					var taskfilters = new Array();

					taskfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));//Task Type - PICK
					taskfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','28']));// Pick Confirm and Pack Complete
					if(getwoid != null && getwoid != "")
					{
						taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getwoid));
					}

					if(vZoneId != null && vZoneId != "")
					{
						taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
					}

					/*if (vPickType!='CL') {
						if(ContainerLPNo!=null && ContainerLPNo!='' && ContainerLPNo!='null')
							taskfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ContainerLPNo));	
						else
							taskfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));

			}*/
//					if (vPickType=='CL') {
//					taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));	
//					}

					var taskcolumns = new Array();
					taskcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
					taskcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
					taskcolumns[2] = new nlobjSearchColumn('custrecord_invref_no');
					taskcolumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
					taskcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					taskcolumns[5] = new nlobjSearchColumn('name');
					taskcolumns[6] = new nlobjSearchColumn('custrecord_packcode');
					taskcolumns[7] = new nlobjSearchColumn('custrecord_wms_location');
					taskcolumns[8] = new nlobjSearchColumn('name');
					taskcolumns[9] = new nlobjSearchColumn('custrecord_skudesc');
					taskcolumns[10] = new nlobjSearchColumn('custrecord_sku_status');
					taskcolumns[11] = new nlobjSearchColumn('custrecord_lpno');
					//Case # 20148227  Start
					taskcolumns[12] = new nlobjSearchColumn('custrecord_batch_no');
					//Case # 20148227  end
					taskcolumns[0].setSort();

					var tasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskfilters, taskcolumns);

					if(tasksearchresults!=null)
					{
						if(vstageLoc==null || vstageLoc=='')
							vstageLoc = GetPickStageLocation(Item, vCarrier, WHLocation, vCompany,stgDirection,null,null,null,null);
						nlapiLogExecution('ERROR', 'vstagLoc', vstageLoc);
						if (vstageLoc != null && vstageLoc != "" && vstageLoc != "-1") 
						{

							for (var i = 0; i < tasksearchresults.length; i++)
							{

								var itemid = tasksearchresults[i].getValue('custrecord_ebiz_sku_no');
								var orderintrid = tasksearchresults[i].getValue('custrecord_ebiz_order_no');
								var quantity = tasksearchresults[i].getValue('custrecord_expe_qty');
								var itempackcode = tasksearchresults[i].getValue('custrecord_packcode');
								var vDoname = tasksearchresults[i].getValue('name');
								var whLocation = tasksearchresults[i].getValue('custrecord_wms_location');
								var itemdesc = tasksearchresults[i].getValue('custrecord_skudesc');
								var itemstatus = tasksearchresults[i].getValue('custrecord_sku_status');
								var Lp = tasksearchresults[i].getValue('custrecord_lpno');
								//Case # 20148227  Start
								var batchno = tasksearchresults[i].getValue('custrecord_batch_no');
								var fifoDate=null;
								var expiryDate=null;


								if(itemid != null && itemid != '')
								{
									var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
									var columns = nlapiLookupField('item', itemid, fields);
									//var columns = nlapiLookupField('item', POarray["custparam_fetcheditemid"], 'recordType');

									if(columns != null && columns != '')
									{				
										itemSubtype = columns.recordType;
										serialInflg = columns.custitem_ebizserialin;
										batchflag= columns.custitem_ebizbatchlot;
										nlapiLogExecution('ERROR', 'citem subtype is =', itemSubtype);
									}

									if (itemSubtype == 'lotnumberedinventoryitem' || itemSubtype=="lotnumberedassemblyitem" || batchflag=="T") 
									{

										var batchfilters = new Array();

										batchfilters.push(new nlobjSearchFilter('name', null, 'is', batchno));//Task Type - PICK
										batchfilters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));
										var columns = new Array();
										columns.push(new nlobjSearchColumn('custrecord_ebizfifodate'));
										columns.push(new nlobjSearchColumn('custrecord_ebizexpirydate'));
										var WObatchResults = nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,batchfilters,columns);
										if(WObatchResults!=null && WObatchResults!='')
										{
											fifoDate=WObatchResults[0].getValue('custrecord_ebizfifodate');
											expiryDate=WObatchResults[0].getValue('custrecord_ebizexpirydate');
										}
									}
								}
								nlapiLogExecution('ERROR', 'itemid', itemid);
								nlapiLogExecution('ERROR', 'fifoDate', fifoDate);
								updateStageInventorywithStageLoc(Lp,vstageLoc,orderintrid);
								//Case # 20148227  end
								/*var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
								invtRec.setFieldValue('name', itemid);
								invtRec.setFieldValue('custrecord_ebiz_inv_binloc', vstageLoc);
								invtRec.setFieldValue('custrecord_ebiz_inv_lp', Lp);
								invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
								invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
								if(itempackcode!=null&&itempackcode!="")
									invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);

								invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
								//invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
								invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
								invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
								invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
								invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
								invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
								//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
								invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
//								invtRec.setFieldValue('custrecord_wms_inv_status_flag','18');//19=FLAG.INVENTORY.STORAGE
								invtRec.setFieldValue('custrecord_wms_inv_status_flag','36');
								invtRec.setFieldValue('custrecord_invttasktype', '3');
								invtRec.setFieldValue('custrecord_ebiz_transaction_no', orderintrid);
								//Case # 20148227  Start
								if(fifoDate !=null && fifoDate!='')
									invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifoDate);

								if(expiryDate !=null && expiryDate!='')
									invtRec.setFieldValue('custrecord_ebiz_expdate', expiryDate);
								//Case # 20148227  end
								var invtrecid = nlapiSubmitRecord(invtRec, false, true);

*/
							}

						}



						/*var woresults = nlapiLoadRecord('workorder', getwoid);
				if(woresults !=null && woresults !='')
				{
					ContainerLPNo='';
					var Item = woresults.getFieldValue('assemblyitem');
					var vDoname = woresults.getFieldValue('tranid');

					nlapiLogExecution('ERROR', 'Item', Item);
					nlapiLogExecution('ERROR', 'vDoname', vDoname);
					CreateSTGMRecord(ContainerLPNo, getwoid, RecordInternalId, vDoname, vCompany, WHLocation, vdono,Item, stgDirection, vCarrier, vSite,vstageLoc,vClusterNo,getwoid);
				}*/




					}			
					//nlapiLogExecution('ERROR', 'vPickType', vPickType);
					var vFootPrintFlag='N'; // Put Y to navigate to Dynacraft specific screen for scanning 'No. of foot prints/Pallets'
					if(vFootPrintFlag != 'Y')
					{
						if(vPickType != null && vPickType != '' && vPickType!='CL')
						{

							nlapiLogExecution('ERROR', 'Inside Both Wave No condition', parseFloat(WaveNo));
							nlapiLogExecution('ERROR', 'Inside Both Order No condition', DoLineId);

							var SOFilters = new Array();


							SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated					
							SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

							if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
							{
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
							} 
							if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && vDoname!=null && vDoname!="" && vDoname!= "null")
							{
								nlapiLogExecution('ERROR', 'OrdNo inside If', vDoname);
								SOFilters.push(new nlobjSearchFilter('name', null, 'is', vDoname));
							}
							if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
							{
								nlapiLogExecution('ERROR', 'SO Id inside If', vSOId);
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
							}
							if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
							{
								nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));			
							}

							var SOColumns = new Array();
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
							SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
							SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
							SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
							SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
							SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
							SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
							SOColumns.push(new nlobjSearchColumn('name'));
							SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
							SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));

							SOColumns[0].setSort();
							SOColumns[1].setSort();
							SOColumns[2].setSort();
							SOColumns[3].setSort();
							SOColumns[4].setSort();
							SOColumns[5].setSort(true);

							var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
							nlapiLogExecution('ERROR', 'SOSearchResults', SOSearchResults);
							if (SOSearchResults != null && SOSearchResults.length > 0) {
								nlapiLogExecution('ERROR', 'into Search Results by Wave No', WaveNo);
								nlapiLogExecution('ERROR', 'vSkipId', vSkipId);
								if(SOSearchResults.length <= parseFloat(vSkipId))
								{
									vSkipId=0;
								}
								//nlapiLogExecution('ERROR', 'SearchResults of given Wave', getWaveNo);
								var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
								getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

								nlapiLogExecution('ERROR', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
								nlapiLogExecution('ERROR', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
								nlapiLogExecution('ERROR', 'Wave # of given Wave', getWaveNo);

								SOarray["custparam_waveno"] = getWaveNo;
								SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
								SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
								SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
								SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
								SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
								SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
								SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
								SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
								SOarray["name"] = SOSearchResult.getValue('name');
								SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
								SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
								SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
								SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');	
								SOarray["custparam_clusterno"] = vClusterNo;
								SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
								SOarray["custparam_ebizordno"] = SOSearchResult.getValue('custrecord_ebiz_order_no');
								if(vZoneId!=null && vZoneId!="")
								{
									SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
								}
								else
									SOarray["custparam_ebizzoneno"] = '';
								response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
								nlapiLogExecution('ERROR', 'Done customrecord', 'Success');

							}
							else {
								nlapiLogExecution('ERROR', 'Search Results by Wave No is NULL', WaveNo);
								response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, null);
							}


						}	

						else if(getwoid != null && getwoid != "")
						{				

							nlapiLogExecution('ERROR', 'Inside condition', parseFloat(getwoid));

							var filterOpentask = new Array();
							filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
							filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

							if(getwoid != null && getwoid != "")
							{
								filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getwoid));
							}
							if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
							{
								filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));

							}
							var SOColumns=new Array();
							SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
							SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
							SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
							SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
							SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
							SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
							SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
							SOColumns.push(new nlobjSearchColumn('name'));
							SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
							SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
							SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');
							SOColumns[0].setSort();


							var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask, SOColumns);
							nlapiLogExecution('ERROR', 'SOSearchResults', SOSearchResults);
							if (SOSearchResults != null && SOSearchResults.length > 0) {
								//nlapiLogExecution('ERROR', 'into Search Results by Wave No', WaveNo);
								if(SOSearchResults.length <= parseFloat(vSkipId))
								{
									vSkipId=0;
								}
								//nlapiLogExecution('ERROR', 'SearchResults of given Wave', getWaveNo);
								var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
								getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

								nlapiLogExecution('ERROR', 'LP# of given WO', SOSearchResult.getValue('custrecord_lpno'));
								nlapiLogExecution('ERROR', 'Exp.Qty of given WO', SOSearchResult.getValue('custrecord_expe_qty'));
								nlapiLogExecution('ERROR', 'WO # of given Wave', getwoid);

								SOarray["custparam_waveno"] = getwoid;
								SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
								SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
								SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
								SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
								SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
								SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
								SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
								SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
								SOarray["name"] = SOSearchResult.getValue('name');
								SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
								SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
								SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
								SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');	
								SOarray["custparam_clusterno"] = vClusterNo;
								SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
								SOarray["custparam_ebizordno"] = SOSearchResult.getValue('custrecord_ebiz_order_no');
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, SOarray);
								nlapiLogExecution('ERROR', 'Done customrecord', 'Success');

							}
							else {
								nlapiLogExecution('ERROR', 'Search Results by ', '');
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di', false, null);
							}
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking', 'customdeploy_ebiz_rf_wo_picking_di', false, null);
						}

					}
					else
					{
						nlapiLogExecution('ERROR', 'Navigating To1', 'Foot print');
						response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, SOarray);
					}	
					nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
				}
				else
				{
					SOarray["custparam_error"] = st7;//'INVALID OPTION';
					nlapiLogExecution('ERROR', 'Error: ', 'Invalid Option');
					SOarray["custparam_screenno"] = 'WOstagelocation';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				}
			}
			catch (e)  {
				nlapiLogExecution('DEBUG', 'e',e);
				SOarray["custparam_error"] = st6;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');
				//case 20125952 start :
				//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, POarray);
				//case 20125952 end
			}
		}
		else
		{
			SOarray["custparam_screenno"] = 'WOmenu';
			SOarray["custparam_error"] = 'ITEM ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}


function updateStageInventorywithStageLoc(vlpno,stageLocation,orderintrid)
{
	nlapiLogExecution('ERROR', 'into updateStageInventorywithStageLoc');
	nlapiLogExecution('ERROR', 'vlpno',vlpno);
	nlapiLogExecution('ERROR', 'orderintrid',orderintrid);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null,'is', '36')); //FLAG.INVENTORY.WIP 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is', vlpno));
	filters.push(new nlobjSearchFilter('custrecord_invttasktype', null,'is', '3')); ////Task Type - PICK
	if(orderintrid !=null && orderintrid !='' && orderintrid !='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_transaction_no', null,'anyof',orderintrid )); 

	var columns = new Array();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null, filters, columns);
	if (invtsearchresults != null && invtsearchresults != '' && invtsearchresults.length > 0) 
	{
		for (var i = 0; i < invtsearchresults.length; i++)
		{
			var invtid=invtsearchresults[i].getId();

			nlapiLogExecution('ERROR', 'Update Stage location....',invtid);
			nlapiSubmitField('customrecord_ebiznet_createinv', invtid, 'custrecord_ebiz_inv_binloc', stageLocation);


		}		
	}

	nlapiLogExecution('ERROR', 'out of updateStageInventorywithStageLoc');
}

/**
 * 
 * @param invtrecid
 * @param vContLp
 * @param Item
 * @param vCarrier
 * @param vSite
 * @param vCompany
 * @param stgDirection
 * @param vqty
 */
function CreateSTGInvtRecord(invtrecid, vContLp, Item, vCarrier, vSite,vCompany, stgDirection, vqty,vstageLoc) {
	try {
		nlapiLogExecution('ERROR', 'into CreateSTGInvtRecord');
		if(vstageLoc==null || vstageLoc=='')
			vstageLoc = GetPickStageLocation(Item, vCarrier, vSite, vCompany,
					stgDirection,null,null,null,null);
		nlapiLogExecution('ERROR', 'vstagLoc', vstageLoc);
		if (vstageLoc != null && vstageLoc != "" && vstageLoc != "-1") {

			var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', vstageLoc);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
			stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
			stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
			stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
			stgmInvtRec.setFieldValue('custrecord_invttasktype', '3');//STATUS.INBOUND.PUTAWAY_COMPLETE('S')
			stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');			
			nlapiSubmitRecord(stgmInvtRec, false, true);

			nlapiLogExecution('ERROR', 'out of CreateSTGInvtRecord');
		}
	} catch (e) {
		if (e instanceof nlobjError)
			lapiLogExecution('ERROR', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		else
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
	}
}



function GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,customizetype,
		StageBinInternalId,ordertype){

	nlapiLogExecution('Error', 'into GetPickStageLocation', Item);
	nlapiLogExecution('Error', 'Item', Item);
	nlapiLogExecution('Error', 'vSite', vSite);
	nlapiLogExecution('Error', 'vCompany', vCompany);
	nlapiLogExecution('Error', 'stgDirection', stgDirection);
	nlapiLogExecution('Error', 'vCarrier', vCarrier);
	nlapiLogExecution('Error', 'vCarrierType', vCarrierType);
	nlapiLogExecution('Error', 'ordertype', ordertype);
	nlapiLogExecution('Error', 'customizetype', customizetype);

	//var ordertype='5'; //Order Type ="WORK ORDER";
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();

	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 = "";
	var ItemInfo3 = "";
	var columns = nlapiLookupField('item', Item, fields);
	if(columns!=null){
		ItemFamily = columns.custitem_item_family;
		ItemGroup = columns.custitem_item_group;
		ItemStatus = columns.custitem_ebizdefskustatus;
		ItemInfo1 = columns.custitem_item_info_1;
		ItemInfo2 = columns.custitem_item_info_2;
		ItemInfo3 = columns.custitem_item_info_3;
	}

	nlapiLogExecution('Error', 'ItemFamily', ItemFamily);
	nlapiLogExecution('Error', 'ItemGroup', ItemGroup);
	nlapiLogExecution('Error', 'ItemStatus', ItemStatus);
	nlapiLogExecution('Error', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('Error', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('Error', 'ItemInfo3', ItemInfo3);

	nlapiLogExecution('Error', 'Before Variable declaration');

	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}

	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');
	columns[5] = new nlobjSearchColumn('custrecord_stagerule_ordtype');

	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
	if (vCarrier != null && vCarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

	}
	if (vCarrierType != null && vCarrierType != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}
	
	var ordertypearray = new Array();
	
	if (ordertype != null && ordertype != "") {
		ordertypearray.push('@NONE@');
		ordertypearray.push(ordertype);		
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ordertypearray));

	}
	/*else
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@']));*/

	if (customizetype != null && customizetype != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@', customizetype]));

	}
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction]));
	filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'anyof', ['3']));

	/*if (StageBinInternalId != null && StageBinInternalId != "")
		//filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
	filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_outboundlocationgroup', 'anyof', StageBinInternalId));*/
	nlapiLogExecution('Error', 'Before Searching of Stageing Rules');

	var vstgLocation,vstgLocationText;
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < searchresults.length; i++) {
			//GetPickStagLpCount    
			//fectch stage location for Ordertype = WORK ORDER		  
			var ordertype = searchresults[i].getText('custrecord_stagerule_ordtype');
			nlapiLogExecution('Error', 'ordertype::', ordertype);
//			if(ordertype == 'WORK ORDER'){
//			vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');
//			vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');
//			}

			vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');
			vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');


			nlapiLogExecution('Error', 'Stageing Location Value::', vstgLocation);
			nlapiLogExecution('Error', 'Stageing Location Text::', vstgLocationText);


			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');

			nlapiLogExecution('Error', 'vnoofFootPrints::', vnoofFootPrints);


			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetPickStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}


			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);
			nlapiLogExecution('Error', 'LocGroup::', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('Error', 'searchresultsloc::', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();
//					var vLocid=searchresultsloc[0].getId();

					nlapiLogExecution('Error', 'vLocid::', vLocid);

					var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');

					//--
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}
			}
		}
	}
	return -1;
}

/**
 * 
 * @param location
 * @param site
 * @param company
 * @returns {Number}
 */
function GetPickStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');

	var outLPCount = 0;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));

	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));

	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');

	return outLPCount;
}

function ValidateStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,vcustomizetype,stagelocation)
{
	try
	{
		nlapiLogExecution('ERROR','Into ValidateStageLocation',stagelocation);
		nlapiLogExecution('ERROR','WHLocation',vSite);
		var StageBinInternalIdArray=new Array();
		StageBinInternalIdArray[0]=false;
		var StageBinInternalId=fetchStageInternalId(stagelocation,vSite);
		if(StageBinInternalId!=null&&StageBinInternalId!="")
		{

			//var stagebinloc=GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,vcustomizetype,StageBinInternalId,null);
//			nlapiLogExecution('ERROR','stagebinloc',stagebinloc);
//			if(stagebinloc!=-1)
//			{
			StageBinInternalIdArray[0]=true;
			StageBinInternalIdArray[1]=StageBinInternalId;
//			}
			/*var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', ['2', '3']));
			filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'noneof', ['14']));
			filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
			if(WHLocation!=null&&WHLocation!="")
				filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', WHLocation]));

			if (vCarrier != null && vCarrier != "") 
				filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

			if (vCarrierType != null && vCarrierType != "")
				filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
			columns[0].setSort();
			columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
			columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
			columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
			columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

			nlapiLogExecution('Error', 'Before Searching of Stageing Rules');


			var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, null);
			if(searchresults!=null&&searchresults!="")
			{
				StageBinInternalIdArray[0]=true;
				StageBinInternalIdArray[1]=StageBinInternalId;
			}*/
		}
		nlapiLogExecution('ERROR','StageBinInternalIdArray',StageBinInternalIdArray);
		return StageBinInternalIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in ValidateStageLocation',exp);
	}
}

function fetchStageInternalId(stageLoc,vSite)
{
	try
	{
		nlapiLogExecution('ERROR','Into fetchStageInternalId',stageLoc);
		var FetchBinLocation="";
		var collocGroup = new Array();
		collocGroup[0] = new nlobjSearchColumn('internalid');

		var filtersLocGroup = new Array();
		filtersLocGroup.push(new nlobjSearchFilter('name', null, 'is',stageLoc));
		filtersLocGroup.push(new nlobjSearchFilter('custrecord_ebizlocationtype',null,'anyof',8));//8=Stage
		if(vSite!=null&&vSite!="")
			filtersLocGroup.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null,'anyof',vSite));
		var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
		if(searchresultsloc != null && searchresultsloc != '')
			FetchBinLocation=searchresultsloc[0].getValue('internalid');
		nlapiLogExecution('ERROR','FetchBinLocation',FetchBinLocation);
		return FetchBinLocation;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in fetchStageInternalId',exp);
	}
}
