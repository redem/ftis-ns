/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Client/Attic/ebiz_ReturnAutorization_CL.js,v $
 *     	   $Revision: 1.1.4.13.2.1 $
 *     	   $Date: 2015/10/20 10:49:07 $
 *     	   $Author: grao $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_44 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *

 *****************************************************************************/

/**
 * @param type
 * @returns {Boolean}
 */

function onDropChangeType(type,name)
{  

	var itemId;

	if((type =='item') && name == 'item')
	{ 


		itemId=nlapiGetCurrentLineItemValue('item','item');

		//Case# 20127379 starts
		var fromLocation = nlapiGetCurrentLineItemValue('item','location');//To get line level location

		if(fromLocation == null || fromLocation =='')
		{
			fromLocation = nlapiGetFieldValue('location');	//To get Header line level location
		}        
		if(itemId != null && itemId != '' && fromLocation != null && fromLocation != '')
		{	
			var searchresult = SetItemStatus("ReturnAuthorization", itemId, fromLocation, null);

			if (searchresult != null && searchresult != '') 
			{
				nlapiSetCurrentLineItemValue('item', 'custcol_ebiznet_item_status', searchresult[0], false);
				if(searchresult[1] != null && searchresult[1] != '')
					nlapiSetCurrentLineItemValue('item', 'custcol_nswmspackcode', searchresult[1], false);
			}
		}
		//Case# 20127379 ends

		if(itemId != null && itemId!='')
		{

			var itemtype=nlapiGetCurrentLineItemValue('item','itemtype');
			if( itemtype!='Description' && itemtype!='Discount' && itemtype!='Payment' && itemtype!='Markup' && itemtype!='Subtotal' )
			{

				var fields = ['custitem_ebizdefskustatus'];
				var columns = nlapiLookupField('item', itemId, fields);

				if(columns != null && columns != '')
				{
					var skuStatus = columns.custitem_ebizdefskustatus;
					//var defskustatus=columns.custitem_ebizdefskustatus;

					if(skuStatus != null && skuStatus != '')
						nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',skuStatus,false);
//					if((skuStatus=='' || skuStatus=='') && (defskustatus!=null && defskustatus!=''))
//					nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',defskustatus,false);	 
				}
			}
		}
	}
	/*	if(name == 'quantity'){

		var Poid = nlapiGetFieldValue('id');
		//alert('Poid'+Poid);

		var lineNo = nlapiGetCurrentLineItemValue('item','line');
		//alert('lineNo'+lineNo);
		var lineCnt = nlapiGetLineItemCount('item');
		if(Poid!=null && Poid!=''){
		var searchRecords = getRecordDetails(Poid,lineNo,'noneof');
		if(searchRecords != null && searchRecords != '' && searchRecords.length>0)
		{	
			for (var s = 1; s <=lineCnt; s++) {
				var searchresults = nlapiLoadRecord('returnauthorization', Poid);
				var vLineno=searchresults.getLineItemValue('item','line',s);
				var vOrderQty=searchresults.getLineItemValue('item','quantity',s);
				var vlinechangedQty=nlapiGetCurrentLineItemValue('item','quantity');

			//	alert("vlinechangedQty:" + vlinechangedQty);
			//	

				if(parseInt(vlinechangedQty) < parseInt(vOrderQty))
				{
					alert("This Line Item is already considered for Inbound process in WMS, so you cannot Change it.");
					//nlapiSetLineItemValue('item','quantity',s,vOrderQty);					
					nlapiSetCurrentLineItemValue('item','quantity',vOrderQty,false);	
					//alert("vOrderQty:" + vOrderQty);
					return false;	
				}

			}
		}
		/*else
		{
			return true;
		}
		}

	}

	//case# 20148441 starts
	if((type == 'item')&&(name == 'item'))
	{ 
		//alert(type);
		//alert(name);
		var Poid = nlapiGetFieldValue('id');
		var lineNo = nlapiGetCurrentLineItemValue('item','line');
		var lineCnt = nlapiGetLineItemCount('item');
		//alert(Poid);
		//alert(lineNo);
		if(lineNo!=null&&lineNo!=""&&lineNo!="null"&&Poid!=null&&Poid!=""&&Poid!="null") 
		{
			//alert(Poid);
			//alert(lineNo);
			var searchRecords = getRecordDetails(Poid,lineNo,'noneof');
			if(searchRecords != null && searchRecords != '')
			{
				for (var s = 1; s <=lineCnt; s++) {
					var searchresults = nlapiLoadRecord('returnauthorization', Poid);
					var vLineno=searchresults.getLineItemValue('item','line',s);
					var vitem=searchresults.getLineItemValue('item','item',s);
					var vlinechangeditem=nlapiGetCurrentLineItemValue('item','item');
					if(vitem!=vlinechangeditem)
					{
						//alert('if');
						alert("This Line Item is already considered for Inbound process in WMS, so you cannot Change it.");
						nlapiSetCurrentLineItemValue('item','item',vitem,false);
						return false;
					}
				}
			}
			else
			{
				//alert('else');
				return true;
			}
		}
		else
		{
			//alert('main else');
			return true;
		}
	}*/
	//case# 20148441 ends
	
	if(name == 'isclosed'){
		
		//var itemid = itemId=nlapiGetCurrentLineItemValue('item','item');
		var lineNo = nlapiGetCurrentLineItemValue('item','line');
		var Poid = nlapiGetFieldValue('id');
		
		var isClosed=nlapiGetCurrentLineItemValue('item','isclosed');
		
		if(isClosed == 'T'){
			
			var transactionFilters = new Array();
			transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', Poid);
			transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineNo);

			var transactionColumns = new Array();
			transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
			transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');

			var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);
			
			if(transactionSearchresults!=null && transactionSearchresults!=''){
				alert('The Selected Line# is Processing, So cannot Close it.');
				nlapiSetCurrentLineItemValue('item', 'isclosed','F', false);
				return false;
			}

			
		}
		
		
		
		//alert("isClosed" + isClosed);
		
		
		
		
	}


}
function getRecordDetails(poId,lineNo){

//	alert(poId);
//	alert(lineNo);

	var filter= new Array();
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poId));
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineNo));

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty',null,'sum');
	columns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty',null,'sum');
	columns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no',null,'group');


	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filter, columns);
	return searchRecords;
}



function DeleteRMALine(type)
{

	var PoId=nlapiGetFieldValue('id');
	var lineNo = nlapiGetCurrentLineItemValue('item','line');
	//alert("lineNo;"+lineNo);
	if(lineNo!=null&&lineNo!=""&&lineNo!="null"&&PoId!=null&&PoId!=""&&PoId!="null") 
	{

		//alert("lineNo:" + lineNo);
		var searchRecords = getRecordDetails(PoId,lineNo);
		//var RecExist = GetRecord(PoId,lineNo);

		//var getLineNo=nlapiGetFieldValue('custbody_ebiz_lines_deleted');
		var afterconcatinate;

		if(searchRecords != null && searchRecords != '')
		{

			alert("This Line Item is already considered for Inbound process in WMS, so you cannot delete it.");
			return false;		
		}
		else
		{

			/*if(getLineNo==null||getLineNo=='')
				afterconcatinate=lineNo.toString()+',';
			else
				afterconcatinate=getLineNo+lineNo.toString()+',';
			nlapiSetFieldValue('custbody_ebiz_lines_deleted', afterconcatinate, 'F', true);	*/	
			return true;
		}
	}
	else
	{
		return true;
	}

}

function OnSave() {

	//case# 20127167 starts (Checking receipt type matches with given location or not)
	var receipttype=nlapiGetFieldText('custbody_nswmsporeceipttype');
	var vloc1=nlapiGetFieldValue('location');
	//alert(receipttype);
	//alert(vloc1);

	//Case# 201410582 starts
	if(vloc1==''||vloc1==null||vloc1=='null')
	{
		alert('Please Select Location');
		return false;
	}
	/*else {
		return true;
	}*/
	//Case# 201410582 ends
	var mwhsiteflag;// case# 201412927
	if(vloc1!=null && vloc1!='')
	{
		var fields = ['custrecord_ebizwhsite'];

		var locationcolumns = nlapiLookupField('Location', vloc1, fields);
		if(locationcolumns!=null)
			mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
	}
	if(mwhsiteflag=='T')
	{	
		var rcfilters=new Array();
		if(receipttype!=null && receipttype!='')
			rcfilters.push(new nlobjSearchFilter('custrecord_ebizreceipttype', null, 'is', receipttype));
		if(vloc1!=null && vloc1!='')
			rcfilters.push(new nlobjSearchFilter('custrecord_ebizsiterecpt', null, 'anyof', vloc1));

		var rcsearchresults=nlapiSearchRecord('customrecord_ebiznet_receipt_type', null, rcfilters,null);
		if(rcsearchresults==null)
		{
			alert('Receipt type not matched with given location');
			return false;
		}
		else {
			return true;
		}
	}
	//case# 20127167 end
	return true;

}
//case# 20127439 starts (onAddLine function added)
function onAddLine(type,name)
{	
	//alert(name);
	//alert(type);
	var itemstatus=nlapiGetCurrentLineItemValue('item','custcol_ebiznet_item_status');
	var location = nlapiGetFieldValue('location');
	var itemStatusFilters = new Array();
	itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
	itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
	if(location != null && location != '')
		itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',location);
	var itemStatusColumns = new Array();
	itemStatusColumns[0] = new nlobjSearchColumn('internalid');
	itemStatusColumns[1] = new nlobjSearchColumn('name');
	itemStatusColumns[0].setSort();
	itemStatusColumns[1].setSort();
	var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);
	if (itemStatusSearchResult != null){
		var errorflag='F';
		for (var i = 0; i < itemStatusSearchResult.length; i++) 
		{		
			//if(itemstatus !=itemStatusSearchResult[i].getValue('name'))
			if(itemstatus ==itemStatusSearchResult[i].getId())
			{
				errorflag='T';
			}
		}
		if(errorflag=='F'){
			alert('Item Status not match with Location');
			return false;
		}
	}
	if(type == 'item')
	{ 
		//alert(type);
		var Poid = nlapiGetFieldValue('id');
		var lineNo = nlapiGetCurrentLineItemValue('item','line');
		//alert(Poid);
		//alert(lineNo);
		if(lineNo!=null&&lineNo!=""&&lineNo!="null"&&Poid!=null&&Poid!=""&&Poid!="null") 
		{
			//alert(Poid);
			//alert(lineNo);
			var searchRecords = getRecordDetails(Poid,lineNo,'noneof');
			if(searchRecords != null && searchRecords != '')
			{
				var OrdQty = nlapiGetCurrentLineItemValue('item', 'quantity');	
				var vChknQty = searchRecords[0].getValue('custrecord_orderlinedetails_checkin_qty',null,'sum');
				if(parseInt(OrdQty) < parseInt(vChknQty)) {
					//alert('if');
					alert("This Line Item is already considered for Inbound process in WMS, so you cannot Change it.");
					return false;		
				}
				else
				{


					if(searchRecords != null && searchRecords != '')
					{
						var lineCnt = nlapiGetLineItemCount('item');
						for (var s = 1; s <=lineCnt; s++) {
							var searchresults = nlapiLoadRecord('returnauthorization', Poid);
							var vLineno=searchresults.getLineItemValue('item','line',s);
							var vitem=searchresults.getLineItemValue('item','item',s);
							var vlinechangeditem=nlapiGetCurrentLineItemValue('item','item');
							var vline=nlapiGetCurrentLineItemValue('item','line');
							//alert(vline);alert(vLineno);alert(vlinechangeditem);alert(vitem);
							if(vitem!=vlinechangeditem && vline==vLineno)
							{
								//alert('if');
								alert("This Line Item is already considered for Inbound process in WMS, so you cannot Change it2.");
								nlapiSetCurrentLineItemValue('item','item',vitem,false);
								return false;
							}
						}
					}
					else
					{
						//alert('else');
						return true;
					}

				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			//alert('main else');
			return true;
		}
	}

	return true;
}


function getRecordDetails1(poId,lineNo){	

	var filter= new Array();
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poId));
	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineNo));

	filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_putconf_qty', null, 'isnotempty'));


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');		

	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filter, columns);
	return searchRecords;
}
//case# 20127439 end
