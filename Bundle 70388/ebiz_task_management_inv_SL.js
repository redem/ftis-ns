/***************************************************************************
 eBizNET Solutions Inc .
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_task_management_inv_SL.js,v $
 *     	   $Revision: 1.11.4.2.8.3.2.1 $
 *     	   $Date: 2015/04/10 21:36:25 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * 
 *  *$Log: ebiz_task_management_inv_SL.js,v $
 *  *Revision 1.11.4.2.8.3.2.1  2015/04/10 21:36:25  skreddy
 *  *Case# 201412323�
 *  *changed the url path which was hard coded
 *  *
 *  *Revision 1.11.4.2.8.3  2014/05/15 15:47:33  sponnaganti
 *  *Case# 20148145
 *  *Standard Bundle Issue fix
 *  *
 *  *Revision 1.11.4.2.8.2  2013/09/19 15:35:36  nneelam
 *  *Case#. 20124449
 *  *All the available LOT Values are not populated in the drop down.
 *  *
 *  *Revision 1.11.4.2.8.1  2013/09/11 12:18:09  rmukkera
 *  *Case# 20124374
 *  *
 *  *Revision 1.11.4.2  2012/05/18 07:27:37  mbpragada
 *  *CASE201112/CR201113/LOG201121
 *  *
 *  *Revision 1.11.4.1  2012/04/20 13:40:04  schepuri
 *  *CASE201112/CR201113/LOG201121
 *  *changing the Label of Batch #  field to Lot#
 *  *
 *  *Revision 1.11  2011/10/21 09:08:08  snimmakayala
 *  *CASE201112/CR201113/LOG201121
 *  *Code changes to show only respective tasks for the respective screens
 *  *i.e Inbound Task Management screen should display only inbound task related data when querying the report without selecting task type.
 *  *
 *  *Revision 1.10  2011/10/17 12:58:25  mbpragada
 *  *CASE201112/CR201113/LOG201121
 *  *set as var opentaskserchresultcount=0;
 *  *
 *  *Revision 1.9  2011/10/17 08:15:36  mbpragada
 *  *CASE201112/CR201113/LOG201121
 *  *Missed the cvs tag , I have added it
 *  *
 *
 *****************************************************************************/


function ebiznet_TaskManagement_inv(request, response){

	if (request.getMethod() == 'GET') 
	{

		var form = nlapiCreateForm('Inventory Task Management');                      
		var tasktype = form.addField('custpage_tasktype', 'multiselect', 'Task Type');
		var tempflag=form.addField('custpage_tempflag','text','tempory flag').setDisplayType('hidden').setDefaultValue('true');
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_process_direction', null, 'is', [3]);
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_task_desc');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, filters, columns);
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {

			tasktype.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name') + ' - ' + searchresults[i].getValue('custrecord_ebiz_task_desc'));
		}

		var taskstatus = form.addField('custpage_taskstatus', 'select', 'Task Status');
		taskstatus.addSelectOption('ALL', 'ALL');
		taskstatus.addSelectOption('C', 'COMPLETE');
		taskstatus.addSelectOption('I', 'INCOMPLETE');
		//taskstatus.addSelectOption('S', 'SUSPEND');

		//  var priority = form.addField('custpage_priority', 'select', 'Priority');
		//  priority.addSelectOption('', '');
		//  priority.addSelectOption('HIGH', 'HIGH');
		//  priority.addSelectOption('LOW', 'LOW');

		var sku = form.addField('custpage_sku', 'select', 'ITEM', 'item');

		var lotbatch = form.addField('custpage_lotbatch', 'select', 'LOT#');
		lotbatch.addSelectOption('', '');
		//case # 20124449 Start
		var filterslot = new Array();
		filterslot[0] = new nlobjSearchFilter('custrecord_batch_no', null, 'isnot', '');
		filterslot[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [5,6,7]);
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslot, new nlobjSearchColumn('custrecord_batch_no'));



		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			var res = form.getField('custpage_lotbatch').getSelectOptions(searchresults[i].getValue('custrecord_batch_no'), 'is');
			if (res != null) {
				 nlapiLogExecution('DEBUG', 'res.length',res.length +  searchresults[i].getValue('custpage_lotbatch'));
				if (res.length > 0) {
					continue;
				}
			}

			lotbatch.addSelectOption(searchresults[i].getValue('custrecord_batch_no'), searchresults[i].getValue('custrecord_batch_no'));
		}

		//case # 20124449 End
		
		//lotbatch.setLayoutType('startrow','startrow');

		var beginlocation = form.addField('custpage_beginlocation', 'select', 'Begin Location', 'customrecord_ebiznet_location');
		// priority.addSelectOption('H','HIGH');  
		//beginlocation.setLayoutType('endrow');

		var endlocation = form.addField('custpage_endlocation', 'select', 'End Location', 'customrecord_ebiznet_location');
		//  priority.addSelectOption('H','HIGH');  
		// endlocation.setLayoutType('startrow','startrow');

		//var lp = form.addField('custpage_lp', 'select', 'LP','customrecord_ebiznet_master_lp');
		var lp = form.addField('custpage_lp', 'select', 'LP');
		lp.addSelectOption('', '');

		var filterslp = new Array();
		filterslp[0] = new nlobjSearchFilter('name', null, 'isnot', '');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filterslp, new nlobjSearchColumn('name').setSort());

		if (searchresults != null)
		{
			for (var i = 0; i < searchresults.length; i++)
			{
				lp.addSelectOption(searchresults[i].getValue('name'), searchresults[i].getValue('name'));
			}
		}

		var employee = form.addField('custpage_employee', 'select', 'Employee', 'employee');

		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
		var todate = form.addField('custpage_todate', 'date', 'To Date');
		form.setScript('customscript_ebiz_cancel_fullorder_cl');
		form.addSubmitButton('Display');		 

		response.writePage(form);
	}
	else 
	{
		var form = nlapiCreateForm("Inventory Task Management");
		//form.addPageLink('crosslink', 'Go Back', 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=139&deploy=1');
		var linkURL = nlapiResolveURL('SUITELET', 'customscript_task_management_inv', 'customdeploy_task_management_inv_di');
    		   form.addPageLink('crosslink', 'Go Back',linkURL);
		var tempflag=request.getParameter('custpage_tempflag');
		//add the task type field and make it a hyperlink

		var tasktype = form.addField('custpage_tasktype', 'multiselect', 'Task Type');

		//new nlobjSearchFilter('mainline', null, 'is', 'T')
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_process_direction', null, 'is', [3]);
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_task_desc');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, filters, columns);
		for (var i = 0; i < Math.min(500, searchresults.length); i++) {

			tasktype.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name') + ' - ' + searchresults[i].getValue('custrecord_ebiz_task_desc'));
		}
		tasktype.setDefaultValue(request.getParameter('custpage_tasktype'));
		//tasktype.setLayoutType('startrow');

		var taskstatus = form.addField('custpage_taskstatus', 'select', 'Task Status');
		taskstatus.addSelectOption('ALL', 'ALL');
		taskstatus.addSelectOption('C', 'COMPLETE');
		taskstatus.addSelectOption('I', 'INCOMPLETE');
		//taskstatus.addSelectOption('S', 'SUSPEND');
		taskstatus.setDefaultValue(request.getParameter('custpage_taskstatus'));
		//taskstatus.setLayoutType('endrow');

		//var priority = form.addField('custpage_priority', 'select', 'Priority');
		//priority.addSelectOption('', '');
		//priority.addSelectOption('HIGH', 'HIGH');
		//priority.addSelectOption('LOW', 'LOW');
		//priority.setDefaultValue(request.getParameter('custpage_priority'));


		var sku = form.addField('custpage_sku', 'select', 'ITEM', 'item');
		sku.setDefaultValue(request.getParameter('custpage_sku'));

		var lotbatch = form.addField('custpage_lotbatch', 'select', 'LOT#');
		lotbatch.addSelectOption('', '');
		//case # 20124449 Start
		var filterslot = new Array();
		filterslot[0] = new nlobjSearchFilter('custrecord_batch_no', null, 'isnot', '');
		filterslot[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [5,6,7]);
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslot, new nlobjSearchColumn('custrecord_batch_no'));



		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			var res = form.getField('custpage_lotbatch').getSelectOptions(searchresults[i].getValue('custrecord_batch_no'), 'is');
			if (res != null) {
				 nlapiLogExecution('DEBUG', 'res.length',res.length +  searchresults[i].getValue('custpage_lotbatch'));
				if (res.length > 0) {
					continue;
				}
			}

			lotbatch.addSelectOption(searchresults[i].getValue('custrecord_batch_no'), searchresults[i].getValue('custrecord_batch_no'));
		}
		if(request.getParameter('custpage_lotbatch')!=null && request.getParameter('custpage_lotbatch')!='')
		lotbatch.setDefaultValue(request.getParameter('custpage_lotbatch'));
		//case # 20124449 End
		//lotbatch.setLayoutType('startrow','startrow');

		var beginlocation = form.addField('custpage_beginlocation', 'select', 'Begin Location', 'customrecord_ebiznet_location');
		beginlocation.setDefaultValue(request.getParameter('custpage_beginlocation'));


		var endlocation = form.addField('custpage_endlocation', 'select', 'End Location', 'customrecord_ebiznet_location');
		endlocation.setDefaultValue(request.getParameter('custpage_endlocation'));


		var lp = form.addField('custpage_lp', 'select', 'LP');
		lp.addSelectOption('', '');

		var filterslp = new Array();
		filterslp[0] = new nlobjSearchFilter('name', null, 'isnot', '');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filterslp, new nlobjSearchColumn('name').setSort());

		if (searchresults != null)
		{
			for (var i = 0; i < searchresults.length; i++)
			{
				lp.addSelectOption(searchresults[i].getValue('name'), searchresults[i].getValue('name'));
			}
		}
		lp.setDefaultValue(request.getParameter('custpage_lp'));

		var employee = form.addField('custpage_employee', 'select', 'Employee', 'employee');
		employee.setDefaultValue(request.getParameter('custpage_employee'));

		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
		var todate = form.addField('custpage_todate', 'date', 'To Date');
		
		tasktype.setDefaultValue(request.getParameter('custpage_tasktype'));        
		taskstatus.setDefaultValue(request.getParameter('custpage_taskstatus'));        
		sku.setDefaultValue(request.getParameter('custpage_sku'));        
		lotbatch.setDefaultValue(request.getParameter('custpage_lotbatch'));       
		beginlocation.setDefaultValue(request.getParameter('custpage_beginlocation'));        
		endlocation.setDefaultValue(request.getParameter('custpage_endlocation'));       
		lp.setDefaultValue(request.getParameter('custpage_lp'));       
		employee.setDefaultValue(request.getParameter('custpage_employee'));
		fromdate.setDefaultValue(request.getParameter('custpage_fromdate'));
		todate.setDefaultValue(request.getParameter('custpage_todate'));
		
		//form.addSubmitButton('Display');

		//create a sublist to display values.
		var sublist = form.addSubList("custpage_results", "list", "Result List");
		//var sublist = form.addSubList("custpage_items", "list", "ItemList");
		sublist.addMarkAllButtons();
		 form.setScript('customscript_ebiz_cancel_fullorder_cl');
		 var temporyflag=form.addField('custpage_tempflag','text','tempory flag').setDisplayType('hidden');
		 sublist.addField("custpage_select", "checkbox", "Confirm");
		sublist.addField("custpage_slno", "text", "Sl.No");
		sublist.addField("custpage_taskno", "text", "Ref #");
		sublist.addField("custpage_tasktype", "text", "Task Type");
		
		sublist.addField("custpage_status", "text", "Status");
		sublist.addField("custpage_begintime", "text", "Begin Time");
		sublist.addField("custpage_endtime", "text", "End Time").setDisplayType('hidden');
		sublist.addField("custpage_beginlocation", "select", "Begin Location", "customrecord_ebiznet_location").setDisplayType('inline');
		sublist.addField("custpage_endlocation", "select", "End Location", "customrecord_ebiznet_location").setDisplayType('inline');
		sublist.addField("custpage_putzone", "text", "Put Zone");
		sublist.addField("custpage_expqty", "text", "Exp Qty");
		sublist.addField("custpage_actqty", "text", "Actual Qty");
		// sublist.addField("custpage_refno", "text", "Ref #");
		sublist.addField("custpage_item", "select", "Item", "item").setDisplayType('inline');
		sublist.addField("custpage_lotbat", "text", "LOT#");
		sublist.addField("custpage_lp", "text", "LP");
		sublist.addField("custpage_user", "text", "User");
		sublist.addField("custpage_taskpriority", "integer", "Task Priority").setDisplayType('entry');
		//sublist.addField("custpage_taskpriority", "text", "Task Priority").setDisplayType('entry');
		sublist.addField("custpage_assignedto", "select", "Assigned To",'employee');
		sublist.addField("custpage_internalid", "text", "Internal Id").setDisplayType('hidden');


		// define search filters
		var value = request.getParameter('custpage_tasktype');
		nlapiLogExecution('ERROR', 'Value:', value);
		var taskarray = new Array();
		taskarray = value.split('');
		for (var p = 0; p < taskarray.length; p++) {
			nlapiLogExecution('ERROR', 'TaskArray:', taskarray[p]);
		}
        var opentaskserchresultcount=0;

		//nlapiLogExecution('ERROR', 'request.getParameter(custpage_fromdate)', request.getParameter('custpage_fromdate'));
		try {
			var i = 0;
			var filters = new Array();
			var param = request.getParameter('custpage_sku');
			nlapiLogExecution('ERROR', 'Length:', taskarray.length);
			if (taskarray.length != 0 && value != '') {
				filters[i] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', taskarray);
				nlapiLogExecution('ERROR', 'filters[i]', taskarray);
				i++;
			}
			if (request.getParameter('custpage_taskstatus') == "I") {

				nlapiLogExecution('ERROR', 'Inside Task Status', request.getParameter('custpage_taskstatus'));
				 filters[i] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
				 i++;
			}
			else if(request.getParameter('custpage_taskstatus') == "C")
			{
				nlapiLogExecution('ERROR', 'Inside Task Status', request.getParameter('custpage_taskstatus'));
				filters[i] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty');
				i++;
			}

			if (request.getParameter('custpage_sku') != "") {
				nlapiLogExecution('ERROR', 'Inside SKU Param', request.getParameter('custpage_sku'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', request.getParameter('custpage_sku'));
				i++;
			}

			if (request.getParameter('custpage_lotbatch') != "") {
				nlapiLogExecution('ERROR', 'Inside Lot/Batch', request.getParameter('custpage_lotbatch'));
				filters[i] = new nlobjSearchFilter('custrecord_batch_no', null, 'is', request.getParameter('custpage_lotbatch'));
				i++;
			}

			if (request.getParameter('custpage_beginlocation') != "") {
				nlapiLogExecution('ERROR', 'Inside Begin Loc', request.getParameter('custpage_beginlocation'));
				filters[i] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', request.getParameter('custpage_beginlocation'));
				i++;
			}
			if (request.getParameter('custpage_endlocation') != "") {
				nlapiLogExecution('ERROR', 'Inside End Loc', request.getParameter('custpage_endlocation'));
				filters[i] = new nlobjSearchFilter('custrecord_actendloc', null, 'is', request.getParameter('custpage_endlocation'));
				i++;
			}
			if (request.getParameter('custpage_lp') != "") {
				nlapiLogExecution('ERROR', 'Inside LP', request.getParameter('custpage_lp'));
				filters[i] = new nlobjSearchFilter('custrecord_lpno', null, 'is', request.getParameter('custpage_lp'));
				i++;
			}

			if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {
				nlapiLogExecution('ERROR', 'fromdate', request.getParameter('custpage_fromdate'));
				nlapiLogExecution('ERROR', 'todate', request.getParameter('custpage_todate'));

				filters[i] = new nlobjSearchFilter('custrecordact_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate') );
				i++;
			}  

			/*
             if (request.getParameter('custpage_employee')!= "")
             {
             nlapiLogExecution('ERROR', 'Inside Employee', request.getParameter('custpage_employee'));
             filters[i] = new nlobjSearchFilter('custrecord_lpno', null, 'is', request.getParameter('custpage_employee'));
             i++;
             }
			 */

			if(request.getParameter('custpage_employee')!= "")
			{
				filters[i] = new nlobjSearchFilter('custrecord_taskassignedto', null, 'is', request.getParameter('custpage_employee'));
			}

			nlapiLogExecution('ERROR', 'Employee info', request.getParameter('custpage_employee'));

			filters.push(new nlobjSearchFilter('custrecord_ebiz_process_direction', 'custrecord_tasktype', 'is', [3]));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_tasktype');
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpno');
			columns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[3] = new nlobjSearchColumn('custrecord_actendloc');
			columns[4] = new nlobjSearchColumn('custrecordact_begin_date');
			columns[5] = new nlobjSearchColumn('custrecord_act_end_date');
			columns[6] = new nlobjSearchColumn('custrecord_actualbegintime');
			columns[7] = new nlobjSearchColumn('custrecord_actualendtime');
			columns[8] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[9] = new nlobjSearchColumn('custrecord_ebiz_task_no');
			columns[10] = new nlobjSearchColumn('custrecord_batch_no');
			columns[11] = new nlobjSearchColumn('custrecord_taskassignedto');
			columns[12] = new nlobjSearchColumn('custrecord_ebizzone_no');
			columns[13] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[14] = new nlobjSearchColumn('custrecord_act_qty');


			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			
			if(searchresults !=null && searchresults != ""){
				opentaskserchresultcount=searchresults.length;
				nlapiLogExecution('ERROR', 'open task searchresults', searchresults.length);
			}
		} 
		catch (error) {
			nlapiLogExecution('ERROR', 'Into Catch Block', error);
		}
		var intcnt=0;
		var tasktype, lp, beginloc, endloc, begindte, enddate, begintime, endtime, itemname, dstatus, taskno, lotbat, taskassigned,putzone,expqty,actqty;
		var ZoneName = "";
		/*Searching records from custom record and dynamically adding to the sublist lines */
		for (var i = 0; searchresults != null && searchresults != "" && i < searchresults.length; i++) 
		{
			intcnt=i;
			var searchresult = searchresults[i];
			tasktype = searchresult.getText('custrecord_tasktype');
			lp = searchresult.getValue('custrecord_ebiz_lpno');
			beginloc = searchresult.getValue('custrecord_actbeginloc');
			endloc = searchresult.getValue('custrecord_actendloc');
			begindte = searchresult.getValue('custrecordact_begin_date');
			enddate = searchresult.getValue('custrecord_act_end_date');
			begintime = searchresult.getValue('custrecord_actualbegintime');
			endtime = searchresult.getValue('custrecord_actualendtime');
			itemname = searchresult.getValue('custrecord_ebiz_sku_no');
			taskno = searchresult.getValue('custrecord_ebiz_task_no');
			lotbat = searchresult.getValue('custrecord_batch_no');
			taskassigned= searchresult.getText('custrecord_taskassignedto');
			putzone= searchresult.getValue('custrecord_ebizzone_no');
			expqty= searchresult.getValue('custrecord_expe_qty');
			actqty= searchresult.getValue('custrecord_act_qty');
			var fields = ['name'];
			if(putzone != "" && putzone != null)
			{
				var columns = nlapiLookupField('customrecord_ebiznet_putpick_zone', putzone, fields);
				ZoneName= columns.name;
			}
			if (enddate != null && enddate != '') 
				dstatus = 'COMPLETED';
			else 
				dstatus = 'INCOMPLETED';

			form.getSubList('custpage_results').setLineItemValue('custpage_slno', i + 1, ""+(i + 1));
			form.getSubList('custpage_results').setLineItemValue('custpage_taskno', i + 1, taskno);
			form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', i + 1, tasktype);
			//form.getSubList('custpage_results').setLineItemValue('custpage_taskpriority', i + 1, 'HIGH');
			form.getSubList('custpage_results').setLineItemValue('custpage_status', i + 1, dstatus);
			form.getSubList('custpage_results').setLineItemValue('custpage_begintime', i + 1, (begindte + " " + begintime));
			form.getSubList('custpage_results').setLineItemValue('custpage_endtime', i + 1, (enddate + " " + endtime));
			form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', i + 1, beginloc);
			form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', i + 1, endloc);
			form.getSubList('custpage_results').setLineItemValue('custpage_putzone', i + 1, ZoneName);
			//form.getSubList('custpage_results').setLineItemValue('custpage_refno', i + 1, '888');
			form.getSubList('custpage_results').setLineItemValue('custpage_item', i + 1, itemname);
			form.getSubList('custpage_results').setLineItemValue('custpage_lp', i + 1, lp);
			form.getSubList('custpage_results').setLineItemValue('custpage_user', i + 1, taskassigned);
			form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', i + 1, lotbat);
			form.getSubList('custpage_results').setLineItemValue('custpage_expqty', i + 1, expqty);
			form.getSubList('custpage_results').setLineItemValue('custpage_actqty', i + 1, actqty);
			form.getSubList('custpage_results').setLineItemValue('custpage_internalid', i + 1, searchresults[i].getId());
		}
		nlapiLogExecution('ERROR', 'request.getParameter(custpage_taskstatus)', request.getParameter('custpage_taskstatus'));
		//closed tas details
		try {

			var i = 0;
			var filters = new Array();
			var param = request.getParameter('custpage_sku');
			nlapiLogExecution('ERROR', 'Length:', taskarray.length);
			if (taskarray.length != 0 && value != '') {
				filters[i] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', taskarray);
				nlapiLogExecution('ERROR', 'filters[i]', taskarray);
				i++;
			}
			if (request.getParameter('custpage_taskstatus') == "I") {

				nlapiLogExecution('ERROR', 'Inside Task Status', request.getParameter('custpage_taskstatus'));
				 filters[i] = new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'isempty');
				i++;
			}
			else if(request.getParameter('custpage_taskstatus') == "C")
			{
				nlapiLogExecution('ERROR', 'Inside Task Status', request.getParameter('custpage_taskstatus'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'isnotempty');
				i++;
			}

			if (request.getParameter('custpage_sku') != "") {
				nlapiLogExecution('ERROR', 'Inside SKU Param', request.getParameter('custpage_sku'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_sku_no', null, 'is', request.getParameter('custpage_sku'));
				i++;
			}

			if (request.getParameter('custpage_lotbatch') != "") {
				nlapiLogExecution('ERROR', 'Inside Lot/Batch', request.getParameter('custpage_lotbatch'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiztask_batch_no', null, 'is', request.getParameter('custpage_lotbatch'));
				i++;
			}

			if (request.getParameter('custpage_beginlocation') != "") {
				nlapiLogExecution('ERROR', 'Inside Begin Loc', request.getParameter('custpage_beginlocation'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiztask_actbeginloc', null, 'is', request.getParameter('custpage_beginlocation'));
				i++;
			}
			if (request.getParameter('custpage_endlocation') != "") {
				nlapiLogExecution('ERROR', 'Inside End Loc', request.getParameter('custpage_endlocation'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiztask_actendloc', null, 'is', request.getParameter('custpage_endlocation'));
				i++;
			}
			if (request.getParameter('custpage_lp') != "") {
				nlapiLogExecution('ERROR', 'Inside LP', request.getParameter('custpage_lp'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiztask_lpno', null, 'is', request.getParameter('custpage_lp'));
				i++;
			}

			if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {
				nlapiLogExecution('ERROR', 'fromdate', request.getParameter('custpage_fromdate'));
				nlapiLogExecution('ERROR', 'todate', request.getParameter('custpage_todate'));

				filters[i] = new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate') );
				i++;
			}  

			/*
         if (request.getParameter('custpage_employee')!= "")
         {
         nlapiLogExecution('ERROR', 'Inside Employee', request.getParameter('custpage_employee'));
         filters[i] = new nlobjSearchFilter('custrecord_lpno', null, 'is', request.getParameter('custpage_employee'));
         i++;
         }
			 */

			if(request.getParameter('custpage_employee')!= "")
			{
				filters[i] = new nlobjSearchFilter('custrecord_ebiztask_taskassgndid', null, 'is', request.getParameter('custpage_employee'));
			}

			nlapiLogExecution('ERROR', 'Employee info', request.getParameter('custpage_employee'));

			filters.push(new nlobjSearchFilter('custrecord_ebiz_process_direction', 'custrecord_ebiztask_tasktype', 'is', [3]));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiztask_tasktype');
			columns[1] = new nlobjSearchColumn('custrecord_ebiztask_lpno');
			columns[2] = new nlobjSearchColumn('custrecord_ebiztask_actbeginloc');
			columns[3] = new nlobjSearchColumn('custrecord_ebiztask_actendloc');
			columns[4] = new nlobjSearchColumn('custrecord_ebiztask_act_begin_date');
			columns[5] = new nlobjSearchColumn('custrecord_ebiztask_act_end_date');
			columns[6] = new nlobjSearchColumn('custrecord_ebiztask_actualbegintime');
			columns[7] = new nlobjSearchColumn('custrecord_ebiztask_actualendtime');
			columns[8] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_sku_no');
			columns[9] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_task_no');
			columns[10] = new nlobjSearchColumn('custrecord_ebiztask_batch_no');
			columns[11] = new nlobjSearchColumn('custrecord_ebiztask_taskassgndid');
			columns[12] = new nlobjSearchColumn('custrecord_ebiztask_zone_no');
			columns[13] = new nlobjSearchColumn('custrecord_ebiztask_expe_qty');
			columns[14] = new nlobjSearchColumn('custrecord_ebiztask_act_qty');


			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);
			if(searchresults !=null && searchresults != "" )
				nlapiLogExecution('ERROR', 'closed task searchresults', searchresults.length);
		} 
		catch (error) {
			nlapiLogExecution('ERROR', 'Into Catch Block', error);
		}
		var intcnt=0;
		var tasktype, lp, beginloc, endloc, begindte, enddate, begintime, endtime, itemname, dstatus, taskno, lotbat, taskassigned,putzone,expqty,actqty;
		var ZoneName = "";
		/*Searching records from custom record and dynamically adding to the sublist lines */
		for (var i = 0; searchresults != null && searchresults != "" && i < searchresults.length; i++) 
		{
			intcnt=i;
			var searchresult = searchresults[i];
			tasktype = searchresult.getText('custrecord_ebiztask_tasktype');
			lp = searchresult.getValue('custrecord_ebiztask_lpno');
			beginloc = searchresult.getValue('custrecord_ebiztask_actbeginloc');
			endloc = searchresult.getValue('custrecord_ebiztask_actendloc');
			begindte = searchresult.getValue('custrecord_ebiztask_act_begin_date');
			enddate = searchresult.getValue('custrecord_ebiztask_act_end_date');
			begintime = searchresult.getValue('custrecord_ebiztask_actualbegintime');
			endtime = searchresult.getValue('custrecord_ebiztask_actualendtime');
			itemname = searchresult.getValue('custrecord_ebiztask_ebiz_sku_no');
			taskno = searchresult.getText('custrecord_ebiztask_ebiz_task_no');
			lotbat = searchresult.getValue('custrecord_ebiztask_batch_no');
			taskassigned= searchresult.getText('custrecord_ebiztask_taskassgndid');
			putzone= searchresult.getValue('custrecord_ebiztask_zone_no');
			expqty= searchresult.getValue('custrecord_ebiztask_expe_qty');
			actqty= searchresult.getValue('custrecord_ebiztask_act_qty');
			var fields = ['name'];
			if(putzone != "" && putzone != null)
			{
				var columns = nlapiLookupField('customrecord_ebiznet_putpick_zone', putzone, fields);
				ZoneName= columns.name;
			}
			if (enddate != null && enddate != '') 
				dstatus = 'COMPLETED';
			else 
				dstatus = 'INCOMPLETED';

			form.getSubList('custpage_results').setLineItemValue('custpage_slno', opentaskserchresultcount + 1, ""+(opentaskserchresultcount + 1));
			form.getSubList('custpage_results').setLineItemValue('custpage_taskno', opentaskserchresultcount + 1, taskno);
			form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', opentaskserchresultcount + 1, tasktype);
			//form.getSubList('custpage_results').setLineItemValue('custpage_taskpriority', opentaskserchresultcount + 1, 'HIGH');
			form.getSubList('custpage_results').setLineItemValue('custpage_status', opentaskserchresultcount + 1, dstatus);
			form.getSubList('custpage_results').setLineItemValue('custpage_begintime', opentaskserchresultcount + 1, (begindte + " " + begintime));
			form.getSubList('custpage_results').setLineItemValue('custpage_endtime', opentaskserchresultcount + 1, (enddate + " " + endtime));
			form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', opentaskserchresultcount + 1, beginloc);
			form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', opentaskserchresultcount + 1, endloc);
			form.getSubList('custpage_results').setLineItemValue('custpage_putzone', opentaskserchresultcount + 1, ZoneName);
			//form.getSubList('custpage_results').setLineItemValue('custpage_refno', i + 1, '888');
			form.getSubList('custpage_results').setLineItemValue('custpage_item', opentaskserchresultcount + 1, itemname);
			form.getSubList('custpage_results').setLineItemValue('custpage_lp', opentaskserchresultcount + 1, lp);
			form.getSubList('custpage_results').setLineItemValue('custpage_user',opentaskserchresultcount + 1, taskassigned);
			form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', opentaskserchresultcount + 1, lotbat);
			form.getSubList('custpage_results').setLineItemValue('custpage_expqty', opentaskserchresultcount + 1, expqty);
			form.getSubList('custpage_results').setLineItemValue('custpage_actqty', opentaskserchresultcount + 1, actqty);
			form.getSubList('custpage_results').setLineItemValue('custpage_internalid', opentaskserchresultcount + 1, searchresults[i].getId());
			opentaskserchresultcount=opentaskserchresultcount+1;
		}
		nlapiLogExecution('ERROR','tempflag',tempflag);
		if(tempflag == null || tempflag==""){
			performUpdateOperations(request,form);
		}
		form.addButton('custpage_displaybtn','Display','Display()');
		var button = form.addSubmitButton('Submit');

		response.writePage(form);

	}
}


function performUpdateOperations(request,form)
{
	nlapiLogExecution('ERROR','into performUpdateOperations','done');
	try
	{
		var LineCount = request.getLineItemCount('custpage_results');		
		for(var s=1; s <= LineCount; s++){			
			flag = request.getLineItemValue('custpage_results', 'custpage_select', s);			
			if(flag == "T"){
				var internalId = request.getLineItemValue('custpage_results', 'custpage_internalid', s);
				var taskpriority  = request.getLineItemValue('custpage_results', 'custpage_taskpriority', s);
				var taskassignedto  = request.getLineItemValue('custpage_results', 'custpage_assignedto', s);				 

				updateRecords(internalId,taskpriority,taskassignedto);
			}
		}
	}	
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception',exp);
	}

}

function updateRecords(recordId,taskpriority,taskassignedto){
	nlapiLogExecution('ERROR','into update record','done');
	nlapiLogExecution('ERROR','recordId',recordId);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);

	transaction.setFieldValue('custrecord_taskpriority', taskpriority);
	transaction.setFieldValue('custrecord_taskassignedto', taskassignedto);

	nlapiSubmitRecord(transaction, false, true);
}