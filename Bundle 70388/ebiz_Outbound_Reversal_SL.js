/***************************************************************************
�eBizNET Solutions
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_Outbound_Reversal_SL.js,v $
 *� $Revision: 1.1.2.4.4.3.4.8.2.2 $
 *� $Date: 2015/11/25 22:18:41 $
 *� $Author: sponnaganti $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_Outbound_Reversal_SL.js,v $
 *� Revision 1.1.2.4.4.3.4.8.2.2  2015/11/25 22:18:41  sponnaganti
 *� Case# 201415704
 *� Nautilus Prod Issue fix
 *�
 *� Revision 1.1.2.4.4.3.4.8.2.1  2015/09/23 14:57:55  deepshikha
 *� 2015.2 issueFix
 *� 201414466
 *�
 *� Revision 1.1.2.4.4.3.4.8  2015/07/30 13:27:17  schepuri
 *� case# 201413685
 *�
 *� Revision 1.1.2.4.4.3.4.7  2014/05/20 14:35:24  snimmakayala
 *� no message
 *�
 *� Revision 1.1.2.4.4.3.4.6  2014/04/16 15:58:00  gkalla
 *� case#20148020
 *� Cesium outbound reversal stage inventory clear issue
 *�
 *� Revision 1.1.2.4.4.3.4.5  2013/12/23 16:45:39  gkalla
 *� case#20126348
 *� Standard bundle issue
 *�
 *� Revision 1.1.2.4.4.3.4.4  2013/07/04 18:55:51  spendyala
 *� CASE201112/CR201113/LOG2012392
 *� Issue fixed against Optimistic Locking
 *�
 *� Revision 1.1.2.4.4.3.4.3  2013/06/25 08:43:26  mbpragada
 *� Case# 201215762
 *� more than 1000 records isse fix while displaying tasks
 *�
 *� Revision 1.1.2.4.4.3.4.2  2013/04/04 15:07:34  rmukkera
 *�
 *� Issue Fix related to Outbound Pick reversal serial no updation issue GUI(serial no was not updating back to storage)
 *�
 *� Revision 1.1.2.4.4.3.4.1  2013/03/19 11:46:05  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production and UAT issue fixes.
 *�
 *� Revision 1.1.2.4.4.3  2012/12/03 16:41:00  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Additional text fields order field ,serial no# and carton no#
 *�
 *� Revision 1.1.2.4.4.2  2012/11/01 14:55:02  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.1.2.4.4.1  2012/10/07 23:06:03  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production Issue Fixes for FISK,BOOMBAH and TDG.
 *�
 *� Revision 1.1.2.4  2012/08/03 21:40:36  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� UAT Issue Fixes
 *�
 *� Revision 1.1.2.3  2012/07/26 10:26:11  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� UAT Issue Fixes
 *�
 *� Revision 1.1.2.2  2012/07/23 06:32:09  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Issue fixes
 *�
 *� Revision 1.1.2.1  2012/06/22 12:28:57  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Outbound Reversal
 *�

 *
 ****************************************************************************/

function outboundReversal(request, response){
	if(request.getMethod() == 'GET'){
		nlapiLogExecution('ERROR', 'outboundReversal - GET', 'Start');
		var form = nlapiCreateForm('Outbound Reversal');
		form.setScript('customscript_ebiz_outboundreversal_cl');
		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');
		var tempflag=form.addField('custpage_flag','text','tempory flag').setDisplayType('hidden').setDefaultValue("nonpaging");
		createOrderSublist(form, request, response);

		var taskList = getTasksForReversal(request,0,form);

		if(taskList != null && taskList.length > 0){
			setPagingForSublist(taskList,form);
		}

		form.addSubmitButton('Submit');

		response.writePage(form);

		nlapiLogExecution('ERROR', 'outboundReversal - GET', 'End');
	} else if (request.getMethod() == 'POST'){

		nlapiLogExecution('ERROR', 'outboundReversal - POST', 'Start');
		try
		{
			var pagingFlag='';
			var form = nlapiCreateForm('Outbound Reversal');
			form.setScript('customscript_ebiz_outboundreversal_cl');
			var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
			var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
			hiddenField_selectpage.setDefaultValue('F');
			nlapiLogExecution('ERROR','custpage_hiddenfieldselectpage',request.getParameter('custpage_hiddenfieldselectpage'));
			var tempflag=form.addField('custpage_flag','text','tempory flag').setDisplayType('hidden').setDefaultValue("nonpaging");
			if(request.getParameter('custpage_qeryparams')!=null)
			{
				hiddenQueryParams.setDefaultValue(request.getParameter('custpage_qeryparams'));
			}
			if (request.getParameter('custpage_flag') != null && request.getParameter('custpage_flag') != "")
			{
				pagingFlag = request.getParameter('custpage_flag');
				 nlapiLogExecution('ERROR', 'pagingFlag', pagingFlag);
				if(pagingFlag == 'paging')
				{
					createOrderSublist(form, request, response);

					var taskList = getTasksForReversal(request,0,form);

					if(taskList != null && taskList.length > 0){
						setPagingForSublist(taskList,form);
					}

					form.addSubmitButton('Submit');

				}
				else
				{

				// Retrieve the number of items for which wave needs to be created
				var lineCount = request.getLineItemCount('custpage_items');
				var contlparray = new Array();
				var vSuccessMsg='N';
				for(var k = 1; k <= lineCount; k++){
					var selectValue = request.getLineItemValue('custpage_items', 'custpage_so', k);
					if(selectValue=='T')
					{
						var waveno=request.getLineItemValue('custpage_items', 'custpage_waveno', k);
						var ordid=request.getLineItemValue('custpage_items', 'custpage_soinernno', k);
						var foid=request.getLineItemValue('custpage_items', 'custpage_doinernno', k);
						var nsrefno=request.getLineItemValue('custpage_items', 'custpage_nsrefno', k);
						var invrefno=request.getLineItemValue('custpage_items', 'custpage_invrefno', k);
						var taskid=request.getLineItemValue('custpage_items', 'custpage_taskid', k);
						var taskqty=request.getLineItemValue('custpage_items', 'custpage_qty', k);
						var ordlineno=request.getLineItemValue('custpage_items', 'custpage_lineno', k);
						var uomlevel=request.getLineItemValue('custpage_items', 'custpage_uomlevel', k);
						var taskweight=request.getLineItemValue('custpage_items', 'custpage_taskweight', k);
						var taskcube=request.getLineItemValue('custpage_items', 'custpage_taskcube', k);
						var itemid=request.getLineItemValue('custpage_items', 'custpage_itemintrid', k);
						var contlp = request.getLineItemValue('custpage_items', 'custpage_containerlp', k);
						var fromlp = request.getLineItemValue('custpage_items', 'custpage_fromlp', k);
						var fono = request.getLineItemValue('custpage_items', 'custpage_fono', k);
						var vMasterlp = request.getLineItemValue('custpage_items', 'custpage_masterlp', k);
							var vBatchno = request.getLineItemValue('custpage_items', 'custpage_lotbatch', k);						
						nlapiLogExecution('ERROR', 'vMasterlp', vMasterlp);
						contlparray.push(contlp);

					performTaskReversal(waveno,ordid,foid,ordlineno,taskid,taskqty,nsrefno,invrefno,taskweight,
									taskcube,uomlevel,itemid,vBatchno);	//60 units.
					vSuccessMsg='T';
					
					ClearOutBoundInventory(contlp,taskqty,taskweight,itemid,vMasterlp);
					if(itemid!=null && itemid!='' && itemid!='null')
					{
						var col=new Array();
						col[0]='recordType';
						col[1]='custitem_ebizserialout';
						
						var res= nlapiLookupField('item', itemid,col);
						var itemTypesku=res.recordType;
						var outbdserial=res.custitem_ebizserialout;
						
						nlapiLogExecution('ERROR', 'itemTypesku', itemTypesku);
						nlapiLogExecution('ERROR', 'outbdserial', outbdserial);
						if (itemTypesku == "serializedinventoryitem"||outbdserial=="T")
						{
							updateSerialEntry(contlp,itemid,taskqty,ordlineno,ordid,fromlp,fono);
						}
					}
				

					}
				}
				if(vSuccessMsg=='T')
				{
					var vDistinctContainerLp = removeDuplicateElement(contlparray);				
					nlapiLogExecution('ERROR', 'vDistinctContainerLp ', vDistinctContainerLp);	
					RemovePackTask(vDistinctContainerLp);
					showInlineMessage(form, 'Confirmation', 'Outbound Reversal Completed Successfully ');
				}
				}
			}
		}
		catch(exp) {
			nlapiLogExecution('ERROR', 'Exception in Outbound Reversal ', exp);	
			showInlineMessage(form, 'Error', 'Outbound Reversal Failed', "");
			response.writePage(form);
		}
		response.writePage(form);
	}

}

function performTaskReversal(waveno,ordid,foid,ordlineno,taskid,taskqty,nsrefno,invrefno,taskweight,taskcube,taskuomlevel,itemid,vBatchno)
{
	var str = 'waveno. = ' + waveno + '<br>';
	str = str + 'ordid. = ' + ordid + '<br>';	
	str = str + 'foid. = ' + foid + '<br>';	
	str = str + 'ordlineno. = ' + ordlineno + '<br>';
	str = str + 'taskid. = ' + taskid + '<br>';
	str = str + 'taskqty. = ' + taskqty + '<br>';
	str = str + 'nsrefno. = ' + nsrefno + '<br>';
	str = str + 'invrefno. = ' + invrefno + '<br>';
	str = str + 'taskweight. = ' + taskweight + '<br>';
	str = str + 'taskcube. = ' + taskcube + '<br>';
	str = str + 'taskuomlevel. = ' + taskuomlevel + '<br>';
	str = str + 'itemid. = ' + itemid + '<br>';

	nlapiLogExecution('ERROR', 'performTaskReversal Parameters', str);

	updateItemFulfillment(nsrefno,ordlineno,taskqty,vBatchno); //22
	updateOpentaskandInventory(invrefno,taskqty,taskid,taskqty,taskweight,taskcube,taskuomlevel,itemid);//26
	updateFulfillmentOrder(foid,ordlineno,taskqty);	//6
}
function updateSerialEntry(contlp,itemid,taskqty,ordlineno,ordid,fromlp,ordno)
{
	var temptaskqty=taskqty;
	var filters = new Array();
	//filters.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', contlp));
	filters.push(new nlobjSearchFilter('custrecord_serialcontlpno', null, 'is', contlp));
	filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itemid));
	//filters.push(new nlobjSearchFilter('custrecord_serialebizsono', null, 'is', ordid));
	filters.push(new nlobjSearchFilter('custrecord_serialsono', null, 'is', ordno));
	filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null, 'is', ordlineno));
	filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	var serialEntryResults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
	if(serialEntryResults!=null && serialEntryResults!='' && serialEntryResults!='null')
	{
		for(var k1=0;k1<serialEntryResults.length;k1++)
		{
			var  count=parseInt(k1)+1;
			if(parseInt(count)<=parseInt(temptaskqty))
			{
				var SerialEntryRec=nlapiLoadRecord('customrecord_ebiznetserialentry',serialEntryResults[k1].getId());
				if(SerialEntryRec!=null && SerialEntryRec!='' && SerialEntryRec!='null')
				{
					SerialEntryRec.setFieldValue('custrecord_serialwmsstatus',['19']);
					SerialEntryRec.setFieldValue('custrecord_serialparentid',fromlp);
					nlapiSubmitRecord(SerialEntryRec);
				}
			}
		}
	}

}
function RemovePackTask(vDistinctContainerLp)
{
	nlapiLogExecution('ERROR', 'Into RemovePackTask', vDistinctContainerLp);

	try
	{

		for ( var vcount = 0; vDistinctContainerLp!=null && vcount < vDistinctContainerLp.length; vcount++) 
		{
			nlapiLogExecution('ERROR', 'Container LP', vDistinctContainerLp[vcount]);

			if(vDistinctContainerLp[vcount]!= "" && vDistinctContainerLp[vcount]!= null)
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vDistinctContainerLp[vcount]));

				//26-Picks Failed, 29-Closed, 30-Short Pick
				filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'noneof', [26,29,30]));

				//3 - PICK
				filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[0].setSort();

				var opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

				nlapiLogExecution('ERROR', 'opentasks', opentasks);
				if(opentasks==null || opentasks=='')
				{				
					var packfilters = new Array();

					packfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vDistinctContainerLp[vcount]));

					//28-Pack Complete
					packfilters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [28]));

					//14 - PACK
					packfilters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [14]));

					var packcolumns = new Array();
					packcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
					packcolumns[0].setSort();

					var packtasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, packfilters, packcolumns);	
					nlapiLogExecution('ERROR', 'packtasks', packtasks);
					if(packtasks!=null && packtasks!='' && packtasks.length>0)
					{
						for(var j=0;j < packtasks.length; j++)
						{
							nlapiLogExecution('ERROR', 'Deleting Pack Task...');
							nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', packtasks[j].getId());
						}
					}

					// Deleting ShipManifest Record
					var smfilters = new Array();

					smfilters.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'is', vDistinctContainerLp[vcount]));

					var smcolumns = new Array();
					smcolumns[0] = new nlobjSearchColumn('custrecord_ship_ref5');
					smcolumns[0].setSort();

					var shipmanifesttasks = nlapiSearchRecord('customrecord_ship_manifest', null, smfilters, smcolumns);	
					nlapiLogExecution('ERROR', 'shipmanifesttasks', shipmanifesttasks);
					if(shipmanifesttasks!=null && shipmanifesttasks!='' && shipmanifesttasks.length>0)
					{
						for(var k=0;k < shipmanifesttasks.length; k++)
						{
							nlapiLogExecution('ERROR', 'Deleting Ship Manifest Record...');
							nlapiDeleteRecord('customrecord_ship_manifest', shipmanifesttasks[k].getId());
						}
					}
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in RemovePackTask', exp);
	}

	nlapiLogExecution('ERROR', 'Out of RemovePackTask', vDistinctContainerLp);
}

function ClearOutBoundInventory(contlp,taskqty,taskweight,itemid,vMasterlp)
{
	nlapiLogExecution('ERROR', 'Into ClearOutBoundInventory');

	var str = 'contlp. = ' + contlp + '<br>';
	str = str + 'taskqty. = ' + taskqty + '<br>';	
	str = str + 'taskweight. = ' + taskweight + '<br>';	
	str = str + 'itemid. = ' + itemid + '<br>';	
	str = str + 'vMasterlp. = ' + vMasterlp + '<br>';	
	nlapiLogExecution('ERROR', 'ClearOutBoundInventory Parameters', str);

	try
	{
		if(contlp!=null && contlp!='')
		{
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', contlp));
			//26-Outbound
			filters.push(new nlobjSearchFilter( 'custrecord_wms_inv_status_flag', null, 'anyof', [18]));
			filters.push(new nlobjSearchFilter( 'custrecord_ebiz_inv_sku', null, 'anyof', itemid));

			var columns=new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

			var inventorysearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filters,columns);
			if(inventorysearchresults!=null && inventorysearchresults!='' && inventorysearchresults.length>0)
			{
				var remtaskqty=taskqty;
				for(var k=0;k < inventorysearchresults.length; k++)
				{
					var obqoh = inventorysearchresults[k].getValue('custrecord_ebiz_qoh');

					//if(obqoh>=taskqty && remtaskqty>0)
					if(remtaskqty>0)
					{
						if(isNaN(obqoh))
							obqoh=0;
						var newqty=0;
						if(parseFloat(obqoh) <= parseFloat(remtaskqty))
						{
							newqty = 0;
							remtaskqty=parseInt(remtaskqty)-parseInt(obqoh);
						}
						else
						{
							newqty = parseFloat(obqoh)-parseFloat(taskqty);
							remtaskqty=parseInt(remtaskqty)-parseInt(taskqty);
						}

						//var newqty = parseFloat(obqoh)-parseFloat(taskqty);
						//var delqty = parseFloat(obqoh)-parseFloat(taskqty);

						if(isNaN(newqty))
							newqty=0;

						nlapiLogExecution('ERROR', 'newqty', newqty);

						if(newqty<=0)
							nlapiDeleteRecord('customrecord_ebiznet_createinv', inventorysearchresults[k].getId());
						else
						{
							nlapiSubmitField('customrecord_ebiznet_createinv', inventorysearchresults[k].getId(), 'custrecord_ebiz_qoh', parseFloat(newqty).toFixed(5));
						}

					}
				}
			}
			else if(vMasterlp !=null && vMasterlp !='' && vMasterlp!='null')
			{
				nlapiLogExecution('ERROR', 'Inside else', vMasterlp);
				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', vMasterlp));
				//26-Outbound
				filters.push(new nlobjSearchFilter( 'custrecord_wms_inv_status_flag', null, 'anyof', [18]));
				filters.push(new nlobjSearchFilter( 'custrecord_ebiz_inv_sku', null, 'anyof', itemid));

				var columns=new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

				var inventorysearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filters,columns);
				if(inventorysearchresults!=null && inventorysearchresults!='' && inventorysearchresults.length>0)
				{
					var remtaskqty=taskqty;
					for(var k=0;k < inventorysearchresults.length; k++)
					{
						var obqoh = inventorysearchresults[k].getValue('custrecord_ebiz_qoh');

						//if(obqoh>=taskqty && remtaskqty>0)
						if(remtaskqty>0)
						{
							if(isNaN(obqoh))
								obqoh=0;
							var newqty=0;
							if(parseFloat(obqoh) <= parseFloat(remtaskqty))
							{
								newqty = 0;
								remtaskqty=parseInt(remtaskqty)-parseInt(obqoh);
							}
							else
							{
								newqty = parseFloat(obqoh)-parseFloat(taskqty);
								remtaskqty=parseInt(remtaskqty)-parseInt(taskqty);
							}

							//var newqty = parseFloat(obqoh)-parseFloat(taskqty);
							//var delqty = parseFloat(obqoh)-parseFloat(taskqty);

							if(isNaN(newqty))
								newqty=0;

							nlapiLogExecution('ERROR', 'newqty', newqty);

							if(newqty<=0)
								nlapiDeleteRecord('customrecord_ebiznet_createinv', inventorysearchresults[k].getId());
							else
							{
								nlapiSubmitField('customrecord_ebiznet_createinv', inventorysearchresults[k].getId(), 'custrecord_ebiz_qoh', parseFloat(newqty).toFixed(5));
							}

						}
					}
				}
			}
			//Shipmanifest Update
			var smfilters = new Array();

			smfilters.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'is', contlp));

			var smcolumns = new Array();
			smcolumns[0] = new nlobjSearchColumn('custrecord_ship_pkgwght');
			smcolumns[0].setSort();

			var shipmanifesttasks = nlapiSearchRecord('customrecord_ship_manifest', null, smfilters, smcolumns);	
			if(shipmanifesttasks!=null && shipmanifesttasks!='' && shipmanifesttasks.length>0)
			{
				for(var l=0;l < shipmanifesttasks.length; l++)
				{
					var actweight = shipmanifesttasks[l].getValue('custrecord_ship_pkgwght');

					if(isNaN(actweight))
						actweight=0;

					var newweight = parseFloat(actweight)-parseFloat(taskweight);

					nlapiLogExecution('ERROR', 'newweight', newweight);

					if(parseFloat(newweight)>0)
						nlapiSubmitField('customrecord_ship_manifest', shipmanifesttasks[l].getId(), 'custrecord_ship_pkgwght', parseFloat(newweight).toFixed(5));
				}				
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in ClearOutBoundInventory', exp);
	}

	nlapiLogExecution('ERROR', 'Out of of ClearOutBoundInventory');
}

function createOrderSublist(form, request, response){
	var sublist = form.addSubList("custpage_items", "list", "ItemList");
	sublist.addMarkAllButtons();
	sublist.addField("custpage_so", "checkbox", "Confirm").setDefaultValue('F');
	sublist.addField("custpage_waveno", "text", "Wave #");
	sublist.addField("custpage_sono", "text", "Order #");
	sublist.addField("custpage_lineno", "text", "Line #");
	sublist.addField("custpage_item", "text", "Item");
	sublist.addField("custpage_qty", "text", "Qty");
	sublist.addField("custpage_itemstatus", "text", "Item Status");
	sublist.addField("custpage_packcode", "text", "Packcode");
	sublist.addField("custpage_lotbatch", "text", "Lot #");
	sublist.addField("custpage_binloc", "text", "Bin Location");
	sublist.addField("custpage_lp", "text", "LP");
	sublist.addField("custpage_containerlp", "text", "Carton #");
	sublist.addField("custpage_serailno", "text", "Serial #");
	sublist.addField("custpage_status", "text", "Status");
	sublist.addField("custpage_doinernno", "text", "DONo").setDisplayType('hidden');
	sublist.addField("custpage_soinernno", "text", "SONo").setDisplayType('hidden');	
	sublist.addField("custpage_location", "text", "location").setDisplayType('hidden');
	sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
	sublist.addField("custpage_invrefno", "text", "Inv Ref #").setDisplayType('hidden');
	sublist.addField("custpage_nsrefno", "text", "NS Ref #").setDisplayType('hidden');
	sublist.addField("custpage_taskid", "text", "Task Id").setDisplayType('hidden');
	sublist.addField("custpage_uomlevel", "text", "UOM Level").setDisplayType('hidden');
	sublist.addField("custpage_taskweight", "text", "TASK WEIGHT").setDisplayType('hidden');
	sublist.addField("custpage_taskcube", "text", "TASK CUBE").setDisplayType('hidden');
	sublist.addField("custpage_itemintrid", "text", "ITEM ID").setDisplayType('hidden');
	sublist.addField("custpage_fromlp", "text", "ITEM ID").setDisplayType('hidden');
	sublist.addField("custpage_fono", "text", "FONo").setDisplayType('hidden');
	sublist.addField("custpage_masterlp", "text", "Master Lp").setDisplayType('hidden');
	
	sublist.addField("custpage_kititem", "text", "kititem").setDisplayType('hidden');// case# 201413685
	sublist.addField("custpage_kititemtype", "text", "kititemtype").setDisplayType('hidden');
	sublist.addField("custpage_kititemtext", "text", "kititemtext").setDisplayType('hidden');
}

function addTaskListToSublist(form, currentTask, i){

	form.getSubList('custpage_items').setLineItemValue('custpage_waveno', i + 1,
			currentTask.getValue('custrecord_ebiz_wave_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1,
			currentTask.getText('custrecord_ebiz_order_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1,
			currentTask.getValue('custrecord_line_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_item', i + 1,
			currentTask.getText('custrecord_sku'));
	form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, 
			currentTask.getValue('custrecord_act_qty'));
	form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, 
			currentTask.getText('custrecord_sku_status'));
	form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, 
			currentTask.getValue('custrecord_packcode'));
	form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, 
			currentTask.getValue('custrecord_batch_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_binloc', i + 1, 
			currentTask.getText('custrecord_actendloc'));
	form.getSubList('custpage_items').setLineItemValue('custpage_lp', i + 1, 
			currentTask.getValue('custrecord_lpno'));
	form.getSubList('custpage_items').setLineItemValue('custpage_containerlp', i + 1, 
			currentTask.getValue('custrecord_container_lp_no'));			
	form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, 
			currentTask.getText('custrecord_wms_status_flag'));
	form.getSubList('custpage_items').setLineItemValue('custpage_doinernno', i + 1, 
			currentTask.getValue('custrecord_ebiz_cntrl_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_soinernno', i + 1, 
			currentTask.getValue('custrecord_ebiz_order_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, 
			currentTask.getValue('custrecord_site_id'));
	form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1, 
			currentTask.getValue('custrecord_comp_id'));
	form.getSubList('custpage_items').setLineItemValue('custpage_invrefno', i + 1, 
			currentTask.getValue('custrecord_invref_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_nsrefno', i + 1, 
			currentTask.getValue('custrecord_ebiz_nsconfirm_ref_no'));	
	form.getSubList('custpage_items').setLineItemValue('custpage_taskid', i + 1, 
			currentTask.getId());
	form.getSubList('custpage_items').setLineItemValue('custpage_uomlevel', i + 1, 
			currentTask.getValue('custrecord_uom_level'));
	form.getSubList('custpage_items').setLineItemValue('custpage_taskweight', i + 1, 
			currentTask.getValue('custrecord_total_weight'));
	form.getSubList('custpage_items').setLineItemValue('custpage_taskcube', i + 1, 
			currentTask.getValue('custrecord_totalcube'));
	form.getSubList('custpage_items').setLineItemValue('custpage_itemintrid', i + 1, 
			currentTask.getValue('custrecord_sku'));
	form.getSubList('custpage_items').setLineItemValue('custpage_serailno', i + 1, 
			currentTask.getValue('custrecord_serial_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_fromlp', i + 1, 
			currentTask.getValue('custrecord_from_lp_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_fono', i + 1, 
			currentTask.getValue('name'));
	
	form.getSubList('custpage_items').setLineItemValue('custpage_kititem', i + 1, 
			currentTask.getValue('custrecord_parent_sku_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_kititemtype', i + 1, 
			currentTask.getValue('type','custrecord_parent_sku_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_kititemtext', i + 1, 
			currentTask.getText('custrecord_parent_sku_no'));
	var vnsrefno = currentTask.getValue('custrecord_ebiz_nsconfirm_ref_no');
	form.getSubList('custpage_items').setLineItemValue('custpage_masterlp', i + 1, 
			currentTask.getValue('custrecord_mast_ebizlp_no'));
	nlapiLogExecution('Error', 'vnsrefno', vnsrefno);
}

var searchResultArray=new Array();

function getTasksForReversal(request,maxno,form){

	// Validating all the request parameters and pushing to a local array
	var localVarArray;
	//this is to maintain the querystring parameters while refreshing the paging drop down
	if (request.getMethod() == 'POST') {
		var queryparams=request.getParameter('custpage_qeryparams');
		nlapiLogExecution('Error', 'queryparams', queryparams);
		var tempArray=new Array();
		tempArray.push(queryparams.split(','));
		localVarArray=tempArray;
		nlapiLogExecution('Error', 'tempArray', tempArray.length);
	}
	else
	{
		localVarArray = validateRequestParams(request,form);
		nlapiLogExecution('Error', 'localVarArray', localVarArray);
	}

	// Get all the search filters for wave generation
	var filters = new Array();   
	filters = specifyTaskFilters(localVarArray,maxno);

	// Get all the columns that the search should return
	var columns = new Array();
	columns = getTaskColumns();

	var taskList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if( taskList!=null && taskList.length>=1000)
	{ 	
		searchResultArray.push(taskList); 
		var maxno=taskList[taskList.length-1].getValue(columns[21]);
		getTasksForReversal(request,maxno,form);	
	}
	else
	{
		searchResultArray.push(taskList); 
	}
	return searchResultArray;
}

function validateRequestParams(request,form){
	var soNo = "";
	var cartonNo = "";
	var serialNo = "";

	var localVarArray = new Array();

	if (request.getParameter('custpage_orderlist') != null && request.getParameter('custpage_orderlist') != "") {
		soNo = request.getParameter('custpage_orderlist');
	}


	if (request.getParameter('custpage_cartonlist') != null && request.getParameter('custpage_cartonlist') != "") {
		cartonNo = request.getParameter('custpage_cartonlist');
	}

	if (request.getParameter('custpage_seriallist') != null && request.getParameter('custpage_seriallist') != "") {
		serialNo = request.getParameter('custpage_seriallist');
	}

	var currentRow = [soNo,cartonNo,serialNo];
	localVarArray.push(currentRow);
	if (request.getParameter('custpage_qeryparams')==null )
	{
		var hiddenQueryParams=form.getField('custpage_qeryparams');
		hiddenQueryParams.setDefaultValue(localVarArray.toString());
	}
	return localVarArray;
}

function specifyTaskFilters(localVarArray,maxno){
	var filters = new Array();

	// Sales Order No
	if(localVarArray[0][0] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', localVarArray[0][0]));

	if(localVarArray[0][1] != "")
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', localVarArray[0][1]));

	if(localVarArray[0][2] != "")
		filters.push(new nlobjSearchFilter('custrecord_serial_no', null, 'is', localVarArray[0][2]));

	//9-Picks Generated, 15-Selected Into Wave, 25-Edit, 26-Picks Failed, 29-Closed, 30-Short Pick
	filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'noneof', [9,14,15,25,26,29,30]));

	//3 - PICK
	filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]));

	filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));

	return filters;
}

/**
 * 
 * @returns {Array}
 */
function getTaskColumns(){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[2] = new nlobjSearchColumn('custrecord_line_no');
	columns[3] = new nlobjSearchColumn('custrecord_sku');
	columns[4] = new nlobjSearchColumn('custrecord_act_qty');
	columns[5] = new nlobjSearchColumn('custrecord_batch_no');
	columns[6] = new nlobjSearchColumn('custrecord_sku_status');
	columns[7] = new nlobjSearchColumn('custrecord_packcode');
	columns[8] = new nlobjSearchColumn('custrecord_actendloc');
	columns[9] = new nlobjSearchColumn('custrecord_lpno');	
	columns[10] = new nlobjSearchColumn('custrecord_container_lp_no');	
	columns[11] = new nlobjSearchColumn('custrecord_wms_status_flag');	
	columns[12] = new nlobjSearchColumn('custrecord_ship_lp_no');	
	columns[13] = new nlobjSearchColumn('name');	
	columns[14] = new nlobjSearchColumn('custrecord_container');
	columns[15] = new nlobjSearchColumn('custrecord_comp_id');
	columns[16] = new nlobjSearchColumn('custrecord_site_id');
	columns[17] = new nlobjSearchColumn('custrecord_invref_no');
	columns[18] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');	
	columns[19] = new nlobjSearchColumn('custrecord_total_weight');	
	columns[20] = new nlobjSearchColumn('custrecord_totalcube');	
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
	columns[22] = new nlobjSearchColumn('id');
	columns[23] = new nlobjSearchColumn('custrecord_uom_level');
	columns[24] = new nlobjSearchColumn('custrecord_serial_no');
	columns[25] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[26] = new nlobjSearchColumn('custrecord_parent_sku_no');
	columns[27] = new nlobjSearchColumn("type",'custrecord_parent_sku_no');
	columns[28] = new nlobjSearchColumn('custrecord_mast_ebizlp_no');
	columns[22].setSort();
	

	return columns;
}

function setPagingForSublist(taskList,form)
{
	if(taskList != null && taskList.length > 0){

		nlapiLogExecution('Error', 'taskList length ', taskList.length);
		var taskListArray=new Array();		

		for(k=0;k<taskList.length;k++)
		{
			var tasksearchresult = taskList[k];

			if(tasksearchresult!=null)
			{					
				for(var j=0;j<tasksearchresult.length;j++)
				{	
					var actqty=tasksearchresult[j].getValue('custrecord_act_qty');

					if(actqty==null || actqty=='' || isNaN(actqty))
						actqty=0;

					if(parseFloat(actqty)>0)						
						taskListArray[taskListArray.length]=tasksearchresult[j];
				}
			}
		}

		var test='';

		if(taskListArray.length>0)
		{
			if(taskListArray.length>50)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("50");
					pagesizevalue=50;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue= 50;
						pagesize.setDefaultValue("50");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=taskListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{
					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>taskListArray.length)
					{
						to=taskListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(taskListArray.length);
			}

			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>taskListArray.length)
			{
				maxval=orderListArray.length;
			}

			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{
					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						//nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						//nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						//nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}


			var c=0;
			var minvalue;
			minvalue=minval;

			for(var j = minvalue; j < maxval; j++){		

				var currentTask = taskListArray[j];
				addTaskListToSublist(form, currentTask, c);
				c=c+1;
			}
		}
	}
}

