/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_CheckInSKU.js,v $
 *     	   $Revision: 1.11.2.13.4.4.4.41 $
 *     	   $Date: 2015/07/16 15:22:02 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInSKU.js,v $
 * Revision 1.11.2.13.4.4.4.41  2015/07/16 15:22:02  grao
 * 2015.2   issue fixes  201413509
 *
 * Revision 1.11.2.13.4.4.4.40  2015/05/05 07:53:23  schepuri
 * case#201412636
 *
 * Revision 1.11.2.13.4.4.4.39  2015/05/04 14:00:26  nneelam
 * case# 201412610
 *
 * Revision 1.11.2.13.4.4.4.38  2015/05/04 13:32:08  nneelam
 * case# 201412610
 *
 * Revision 1.11.2.13.4.4.4.37  2015/04/30 15:33:30  nneelam
 * case# 201412565
 *
 * Revision 1.11.2.13.4.4.4.36  2015/04/17 15:28:08  grao
 * SB issue fixes  201412295
 *
 * Revision 1.11.2.13.4.4.4.35  2015/04/02 13:32:13  schepuri
 * case# 201412231
 *
 * Revision 1.11.2.13.4.4.4.34  2015/01/30 20:08:39  gkalla
 * Case# 201411440
 * CT post item receipt issue
 *
 * Revision 1.11.2.13.4.4.4.33  2014/11/27 14:04:30  sponnaganti
 * case# 201411102
 * True Fab Prod issue fix
 *
 * Revision 1.11.2.13.4.4.4.32  2014/10/31 13:28:05  vmandala
 * case# 201410846 Stdbundle issue fixed
 *
 * Revision 1.11.2.13.4.4.4.31  2014/10/28 09:40:45  vmandala
 * Case# 201410846 Stdbundle issue fixed
 *
 * Revision 1.11.2.13.4.4.4.30  2014/10/28 09:32:37  vmandala
 * Case# 201410846 Stdbundle issue fixed
 *
 * Revision 1.11.2.13.4.4.4.29  2014/10/16 15:07:42  skavuri
 * Case# 201410723 Std Bundle issue fixed
 *
 * Revision 1.11.2.13.4.4.4.28  2014/09/17 15:46:29  sponnaganti
 * Case# 201410428
 * Stnd Bundle Issue fix
 *
 * Revision 1.11.2.13.4.4.4.27  2014/09/05 15:12:07  sponnaganti
 * case# 201410253
 * Stnd Bundle issue fix
 *
 * Revision 1.11.2.13.4.4.4.26  2014/08/29 20:02:57  grao
 * Case#: 201410178 ï¿½Dc Dental sb  issue fixes
 *
 * Revision 1.11.2.13.4.4.4.25  2014/07/10 09:46:02  sponnaganti
 * Case# 20149365
 * Compatibility issue fix
 *
 * Revision 1.11.2.13.4.4.4.24  2014/07/04 23:06:05  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149188
 *
 * Revision 1.11.2.13.4.4.4.23  2014/07/02 15:17:15  skavuri
 * Case# 20149155 Compatibility Issue Fixed
 *
 * Revision 1.11.2.13.4.4.4.22  2014/06/20 12:05:59  skavuri
 * Case # 20148882 SB Issue Fixed
 *
 * Revision 1.11.2.13.4.4.4.21  2014/06/13 07:01:02  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.11.2.13.4.4.4.20  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.11.2.13.4.4.4.19  2014/04/15 15:48:56  skavuri
 * Case # 20148012 SB issue fixed
 *
 * Revision 1.11.2.13.4.4.4.18  2014/02/12 16:11:53  skavuri
 * Case # 20127101 (now, System allow to Item's Next Screen in rf cart checkin)
 *
 * Revision 1.11.2.13.4.4.4.17  2014/01/13 16:47:03  grao
 * Case# 20126606 related issue fixes in Sb issue fixes
 *
 * Revision 1.11.2.13.4.4.4.16  2014/01/07 06:42:53  grao
 * Case# 20126606 related issue fixes in Sb issue fixes
 *
 * Revision 1.11.2.13.4.4.4.15  2013/12/11 23:14:48  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20126355
 *
 * Revision 1.11.2.13.4.4.4.14  2013/11/25 15:20:27  grao
 * Case# 20125917  related issue fixes in SB 2014.1
 *
 * Revision 1.11.2.13.4.4.4.13  2013/11/01 13:38:00  schepuri
 * 20125388
 *
 * Revision 1.11.2.13.4.4.4.12  2013/10/31 15:29:57  rmukkera
 * no message
 *
 * Revision 1.11.2.13.4.4.4.11  2013/10/24 14:35:46  schepuri
 * 20125259
 *
 * Revision 1.11.2.13.4.4.4.10  2013/09/19 15:58:48  skreddy
 * Case# 20124372
 * Afosa SB  issue fix
 *
 * Revision 1.11.2.13.4.4.4.9  2013/09/17 18:49:51  grao
 * CWD issue fixes 20124469
 *
 * Revision 1.11.2.13.4.4.4.8  2013/09/05 00:04:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Afosa changes
 * case#20123755
 *
 * Revision 1.11.2.13.4.4.4.7  2013/09/02 07:44:58  schepuri
 * location is generating
 *
 * Revision 1.11.2.13.4.4.4.6  2013/08/05 17:00:12  skreddy
 * Case# 20123722
 * issue rellated to rolebased location
 *
 * Revision 1.11.2.13.4.4.4.5  2013/07/15 07:51:29  spendyala
 * case# 201216039
 * Validating user not to process non-inventory item
 *
 * Revision 1.11.2.13.4.4.4.4  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.11.2.13.4.4.4.3  2013/06/04 10:54:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.11.2.13.4.4.4.2  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.11.2.13.4.4.4.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.11.2.13.4.4  2012/12/12 07:43:51  spendyala
 * CASE201112/CR201113/LOG201121
 * moved from 2012.2 branch
 *
 * Revision 1.11.2.13.4.3  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.11.2.13.4.2  2012/09/26 12:48:01  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.11.2.13.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.11.2.13  2012/06/02 09:06:46  spendyala
 * CASE201112/CR201113/LOG201121
 * Driving user to update the ItemDim when Prompt for Dimensions(Check box)
 * or  Prompt for Weight (Check box) are checked in item master.
 *
 * Revision 1.11.2.12  2012/05/22 15:42:53  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.11.2.11  2012/05/17 22:19:23  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Checkin sku
 *
 * Revision 1.11.2.10  2012/05/16 06:02:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Monobind UAT issue fixes
 *
 * Revision 1.11.2.9  2012/05/14 14:45:50  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating the sku which is valid but not present in respective PO.
 *
 * Revision 1.11.2.8  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.11.2.7  2012/05/02 12:36:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Checkin changes
 *
 * Revision 1.11.2.6  2012/04/13 23:12:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF CART PUTAWAY issues.
 *
 * Revision 1.11.2.5  2012/04/09 14:35:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * DUplicate line items in Transactions
 *
 * Revision 1.11.2.4  2012/04/06 15:00:35  spendyala
 * CASE201112/CR201113/LOG201121
 * fetching header level site Id  if line level site id is null.
 *
 * Revision 1.11.2.3  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.11.2.2  2012/02/22 12:34:06  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.11.2.1  2012/02/13 13:48:14  spendyala
 * CASE201112/CR201113/LOG201121
 * Added BaseUomQty parameter to the query string.
 *
 * Revision 1.11  2011/12/22 11:37:57  rgore
 * CASE201112/CR201113/LOG201121
 * Modularized code to call method from data access library.
 * - Ratnakar
 * 22 Dec 2011
 *
 * Revision 1.10  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/09/12 08:24:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/07/12 05:35:41  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/07/07 09:15:42  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.5  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/19 09:40:19  vpasula
 * CASE2009936/CR200912052/LOG20093196
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

/**
 * 
 * @param itemNo
 * @param location
 * @param company
 * @returns
 */
function validateSKU(itemNo, location, company,poid){
	nlapiLogExecution('DEBUG', 'validateSKU', 'Start');

	var inputParams = 'Item = ' + itemNo + '<br>';
	inputParams = inputParams + 'Location = ' + location + '<br>';
	inputParams = inputParams + 'Company = ' + company + '<br>';;
	inputParams = inputParams + 'PO Id = ' + poid;
	nlapiLogExecution('DEBUG', 'Input Parameters', inputParams);

	var currItem = eBiz_RF_GetItemForItemNo(itemNo, location);

	if(currItem == ""){
		currItem = eBiz_RF_GetItemBasedOnUPCCode(itemNo, location);
	}

	if(currItem==""){

		var skuAliasFilters = new Array();
		skuAliasFilters.push(new nlobjSearchFilter('name',null, 'is',itemNo));
		skuAliasFilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_location',null, 'is',location));


		var skuAliasCols = new Array();
		skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

		var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);
		if(skuAliasResults!=null && skuAliasResults!='')
			currItem = skuAliasResults[0].getText('custrecord_ebiz_item');
	}



	if(currItem == ""){
		var vendor='';
		if(poid!=null && poid!='')
		{
			var po  = nlapiLoadRecord('purchaseorder',poid);
			vendor=po.getFieldValue('entity');
		}
		currItem = eBiz_RF_GetItemFromSKUDims(itemNo, location,vendor);
	}

	var logMsg = "";
	if(currItem == ""){
		logMsg = 'Unable to retrieve item';
		currItem = null;
	} else {
		logMsg = 'Item = ' + currItem;
	}

	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'validateSKU', 'End');
	return currItem;
}


function CheckInSKU(request, response){
	if (request.getMethod() == 'GET') 
	{

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4;

		if( getLanguage == 'es_ES')
		{
			st0 = "RECEPCI&#211;N DE MEN&#218;";
			st1 = "ORDEN DE COMPRA";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULO";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";			

		}
		else
		{
			st0 = "RECEIVING MENU";
			st1 = "PO";
			st2 = "ENTER/SCAN ITEM";
			st3 = "SEND";
			st4 = "PREV";

		}



		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		//Variable Declaration
		var html = '';

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		nlapiLogExecution('DEBUG', 'Into Request', getPONo);

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var whLocation = request.getParameter('custparam_whlocation');
		var trantype= request.getParameter('custparam_trantype');
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);
		var whCompany= request.getParameter('custparam_company');
		var packcode= request.getParameter('custparam_packcode');//Case# 201410723

		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_sku'); 
		html = "<html><head><title>" + st0 + "</title>" ;
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enteritem').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_checkin_sku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " : <label>" + getPONo + "</label>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnWhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		html = html + "				<input type='hidden' name='hdnpackcode' value=" + packcode + ">";//Case# 201410723
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Processing RF', 'Validating SKU/ITEM');

		// Forming the temporary array POarray
		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st5,st6;
		if( getLanguage == 'es_ES')
		{
			st5 = "ART&#205;CULO INV&#193;LIDO";
			st6="INTRODUZCA TEMA";
		}
		else
		{
			st5 = "INVALID ITEM";
			st6="PLEASE ENTER ITEM";
		}



		var tempflag='F';
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('enteritem');
		POarray["custparam_error"] = st5;
		POarray["custparam_screenno"] = '2';
		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');	
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_company"] = request.getParameter('hdnWhCompany');
		POarray["custparam_option"] = request.getParameter('hdnOptedField');
		POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
		POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		POarray["custparam_packcode"] = request.getParameter('hdnpackcode'); //Case# 201410723
		var trantype=request.getParameter('hdntrantype');
		nlapiLogExecution('DEBUG', 'trantype', trantype);
		var optedEvent = request.getParameter('cmdPrevious');	// To trap the previous button

		var logMsg = 'PO = ' + POarray["custparam_poid"] + '<br>';
		logMsg = logMsg + 'Item = ' + POarray["custparam_poitem"] + '<br>';
		nlapiLogExecution('DEBUG', 'Processing RF - SKU', logMsg);

		// Processing only if the 'Previous' button is not pressed
		if(optedEvent != 'F7'){
			if (POarray["custparam_poitem"] != "") {

				var filters = new Array();	
				var poid;
				filters.push(new nlobjSearchFilter('tranid', null, 'is', POarray["custparam_poid"]));	
				//case# 20149365 starts (if line level location is not given)
				filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
				var columns=new Array();
				columns[0] = new nlobjSearchColumn('location');
				var posearch = nlapiSearchRecord(trantype, null, filters, null);
				nlapiLogExecution('Debug', 'Time Stamp after posearch',TimeStampinSec());
				var vlinelocation='';
				if(posearch!=null && posearch!='')
				{
					poid = posearch[0].getId();
					nlapiLogExecution('DEBUG', 'poid', poid);
					vlinelocation=posearch[0].getValue('location');
					nlapiLogExecution('DEBUG', 'vlinelocation', vlinelocation);
				}
			
					var itemRecord;
					//nlapiLogExecution('Debug', 'Time Stamp before itemRecordArr',TimeStampinSec());
					var itemRecordArr = eBiz_RF_GetItemForItemIdWithArrNew(POarray["custparam_poitem"],POarray["custparam_whlocation"]);
					nlapiLogExecution('Debug', 'Time Stamp after itemRecordArr',TimeStampinSec());
					if(itemRecordArr != null && itemRecordArr.length>0){
						//nlapiLogExecution('Debug', 'Time Stamp before poLineDetails',TimeStampinSec());
						var poLineDetails =
							eBiz_RF_GetPOLineDetailsForItemArr(POarray["custparam_poid"], itemRecordArr,trantype,vlinelocation);
						nlapiLogExecution('Debug', 'Time Stamp after poLineDetails',TimeStampinSec());
						if(poLineDetails != null && poLineDetails != '')
						{
							var itemIntId=poLineDetails[0].getValue('item');
							if(itemIntId != null && itemIntId != '')
							{
								nlapiLogExecution('ERROR', 'Internal Id', itemIntId);
								var itemcolumns = nlapiLookupField('item', itemIntId, ['recordType']);
								var itemType = itemcolumns.recordType;

								nlapiLogExecution('ERROR', 'itemType', itemType);

								itemRecord = nlapiLoadRecord(itemType, itemIntId);
							}	
						}				

						nlapiLogExecution('Debug', 'Time Stamp after LookupField',TimeStampinSec());
						if(itemRecord != null&& itemRecord.getFieldValue("baserecordtype")!="noninventoryitem" && itemRecord.getFieldValue("isdropshipitem")!="T"){
							
							//var currItems = eBiz_RF_GetItemForItemInternalId(itemRecord, POarray["custparam_whlocation"]);
							//nlapiLogExecution('Debug', 'Time Stamp before ItemDimens',TimeStampinSec());
							var ItemDimens=CheckItemDimens(itemIntId, POarray["custparam_whlocation"],POarray["custparam_packcode"]);
							nlapiLogExecution('Debug', 'Time Stamp after ItemDimens',TimeStampinSec());
							if(ItemDimens == '' ||ItemDimens == null)
							{
								POarray["custparam_error"] ="Item Dimensions are not Configured for Item# "+POarray["custparam_poitem"];
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return false;
								nlapiLogExecution('DEBUG', 'ItemDimens ', 'Item Dimensions are not Configured ');
							}
							
							
							
							
							var itemDesc = itemRecord.getFieldValue('purchasedescription');
							if(itemDesc!=null && itemDesc!='')
								itemDesc = itemDesc.replace(/(\r\n|\n|\r)/gm,"");
							var Transactionlinedetails=eBiz_RF_GetTransactionlinedetails(POarray["custparam_poid"], itemRecord.getId());
							nlapiLogExecution('Debug', 'Time Stamp after Transactionlinedetails',TimeStampinSec());
							// Retrieving the PO Line Details
							var poLineDetails =
								eBiz_RF_GetPOLineDetailsForItem(POarray["custparam_poid"], itemRecord.getId(),trantype);
							nlapiLogExecution('Debug', 'Time Stamp after poLineDetails1',TimeStampinSec());
							//nlapiLogExecution('DEBUG','poLineDetails',poLineDetails);
							//nlapiLogExecution('DEBUG','Transactionlinedetails',Transactionlinedetails);
							var vIndex=0;
							var vBoolFount=false;
							if(Transactionlinedetails!=null && Transactionlinedetails!='')
							{
								/* The below code is merged from Tekind production account on 03-04-2013 by Radhika as part of Standard bundle*/
								tempflag='T';
								/*for(var i=0; i<Transactionlinedetails.length; i++)
							{
								var tranlineno=Transactionlinedetails[i].getValue('custrecord_orderlinedetails_orderline_no');
								var trancheckinqty=Transactionlinedetails[i].getValue('custrecord_orderlinedetails_checkin_qty');

								for(var j=0; j<poLineDetails.length; j++)
								{
									var polineno=poLineDetails[j].getValue('line');
									var pocheckinqty=poLineDetails[j].getValue('quantity');
									if(polineno==tranlineno)
									{
										//templinearray[templinearray.length]=polineno;
										if(parseFloat(pocheckinqty)>parseFloat(trancheckinqty))
										{
											if(!vBoolFount){
												vIndex=j;
												nlapiLogExecution('DEBUG', 'POInternalId', poLineDetails[j].getValue('line'));
												vBoolFount=true;
											}
											//goto Skiphere;
										}
										else
										{
											break;
										}
									}
									else
									{
										if(!vBoolFount){
											vIndex=j;
											vBoolFount=true;
										}
										//goto Skiphere;
									}	

								}
							}*/
								//if(vBoolFount==false && Transactionlinedetails.length>0)
								nlapiLogExecution('DEBUG', 'vBoolFount1', vBoolFount);
								for(var j=0;vBoolFount==false && j<poLineDetails.length; j++)
								{
									var vTempFlag=false;
									var polineno=poLineDetails[j].getValue('line');
									//nlapiLogExecution('DEBUG', 'polinenoStart',polineno);
									for(var k=0; k<Transactionlinedetails.length;k++)
									{
										var tranlineno=Transactionlinedetails[k].getValue('custrecord_orderlinedetails_orderline_no');
										nlapiLogExecution('DEBUG', 'tranlineno', tranlineno);
										nlapiLogExecution('DEBUG', 'polineno', polineno);
										if(tranlineno==polineno)
										{
											var pocheckinqty=poLineDetails[j].getValue('quantity');
											if(trantype=='returnauthorization' && pocheckinqty != null && pocheckinqty != '' && parseFloat(pocheckinqty)<0)
												pocheckinqty=parseFloat(pocheckinqty)*-1;
											var trancheckinqty=Transactionlinedetails[k].getValue('custrecord_orderlinedetails_checkin_qty');
											if(parseFloat(pocheckinqty)>parseFloat(trancheckinqty))
											{
												if(!vBoolFount){
													vIndex=j;
													nlapiLogExecution('DEBUG', 'POInternalId', poLineDetails[j].getValue('line'));
													vBoolFount=true;
												}
												//goto Skiphere;
											}
											vTempFlag=true;
											break; 
										}									 	
									}	
									if(vTempFlag==false)
									{
										vIndex=j;
										vBoolFount=true;
										break;
									}	

								}  
							}
							nlapiLogExecution('DEBUG', 'vBoolFount2', vBoolFount);
							nlapiLogExecution('DEBUG', 'vIndex', vIndex);
							/* Up to here */ 						
							if(poLineDetails != null && poLineDetails.length > 0){
								tempflag='T';
								var poInternalId = poLineDetails[vIndex].getId();
								nlapiLogExecution('DEBUG', 'POInternalId', poInternalId);

								POarray["custparam_lineno"] = poLineDetails[vIndex].getValue('line');
								POarray["custparam_fetcheditemid"] = itemRecord.getId();;
								POarray["custparam_pointernalid"] = poInternalId;
								POarray["custparam_fetcheditemname"] = POarray["custparam_poitem"];
								POarray["custparam_itemdescription"] = itemDesc;
								if(poLineDetails[vIndex].getValue('location') != null && poLineDetails[vIndex].getValue('location') != '')
									POarray["custparam_whlocation"] = poLineDetails[vIndex].getValue('location');
								//code modified on 06 Apr
								//If Po LineLevel Location(site) is null then choose the location(site) from header value.
								/*var poLineLevelLoc=poLineDetails[vIndex].getValue('location');
							if(poLineLevelLoc!=null && poLineLevelLoc!="")
								POarray["custparam_whlocation"] = poLineDetails[vIndex].getValue('location');
							else
								POarray["custparam_whlocation"]= nlapiLookupField('purchaseorder', poInternalId, 'location');*/
								//end of code as of 06 Apr.

								if(trantype=='purchaseorder')
								{       //#CASE NO 20123722	//

									var RoleLocation=getRoledBasedLocation();
									nlapiLogExecution('ERROR', 'RoleLocation', RoleLocation);
									if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
									{
										if(poLineDetails[vIndex].getValue('location') != null && poLineDetails[vIndex].getValue('location') != '')
										{
											//this is to validate the po site level


											//nlapiLogExecution('ERROR', 'poLineDetails[vIndex].getValue(location)', poLineDetails[vIndex].getValue('location'));
											//case no 20125259,20125388
											nlapiLogExecution('ERROR', 'RoleLocation.indexOf((poLineDetails[vIndex].getValue(location))) != -1', RoleLocation.indexOf(parseInt(poLineDetails[vIndex].getValue('location'))));
											//if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0 && RoleLocation==poLineDetails[vIndex].getValue('location'))
											//case# 201410253 (parseint removed from if condition)
											if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0 && RoleLocation.indexOf(parseInt(poLineDetails[vIndex].getValue('location'))) != -1) // case# 201412231
												//if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0 && RoleLocation.indexOf(poLineDetails[vIndex].getValue('location')))
											{
												POarray["custparam_whlocation"] = poLineDetails[vIndex].getValue('location');
												//nlapiLogExecution('ERROR', 'Location', POarray["custparam_whlocation"]);
											}
											else
											{
												nlapiLogExecution('ERROR', 'Error Page', 'Invalid Location');

												response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
												return false;

											}
										}
										else
										{
											//this is to validate the po site level

											nlapiLogExecution('ERROR', 'RoleLocationelse', RoleLocation);
											var headerlocation=getOrderHeaderLocation(POarray["custparam_poid"],trantype);
											nlapiLogExecution('ERROR', 'headerlocation', headerlocation);
											//case # 20127101 starts
											//if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0 && RoleLocation==headerlocation)
											if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0 && RoleLocation.indexOf(parseInt(headerlocation))!=-1)//case # 20127101 ends,201412636
											{
												POarray["custparam_whlocation"] = headerlocation ;
											}
											else
											{

												response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
												return false;

											}
										}
									}
									else
									{
										if(poLineDetails[vIndex].getValue('location') != null && poLineDetails[vIndex].getValue('location') != '')
										{
											POarray["custparam_whlocation"] = poLineDetails[vIndex].getValue('location');
										}
										else
										{
											var headerlocation=getOrderHeaderLocation(POarray["custparam_poid"],trantype);
											POarray["custparam_whlocation"] = headerlocation;
										}
									}
									//end of case no 20123722//
								}

								nlapiLogExecution('DEBUG','POarray["custparam_fetcheditemid"]',POarray["custparam_fetcheditemid"]);
								if(POarray["custparam_fetcheditemid"]!=null&&POarray["custparam_fetcheditemid"]!="")
								{
									// Retrieve the item cube for the fetched item id
									var ItemInfo = eBiz_RF_GetItemCubeForItem(POarray["custparam_fetcheditemid"]);

									//code added on 13 feb 2012 by suman
									//To get the baseuom qty .
									//nlapiLogExecution('DEBUG','ITEMINFO[0]',ItemInfo[0]);
									//nlapiLogExecution('DEBUG','ITEMINFO[1]',ItemInfo[1]);

									POarray["custparam_itemcube"]=ItemInfo[0];
									POarray["custparam_baseuomqty"]=ItemInfo[1];

									//end of code as of 13 Feb 2012.

									logMsg = 'PO = ' + poInternalId + '<br>';
									logMsg = logMsg + 'Line = ' + POarray["custparam_lineno"] + '<br>';
									logMsg = logMsg + 'Fetched Item = ' + POarray["custparam_fetcheditemid"] + '<br>';
									logMsg = logMsg + 'Location = ' + POarray["custparam_whlocation"] + '<br>';
									logMsg = logMsg + 'Item Cube = ' + POarray["custparam_itemcube"];
									nlapiLogExecution('DEBUG', 'Line Details', logMsg);

									if(POarray["custparam_poitem"] != ""){
										var poItemFields = ['custitem_ebizdimprompt','custitem_ebizweightprompt'];
										var rec = nlapiLookupField('item', POarray["custparam_fetcheditemid"], poItemFields);
										var promtDim=rec.custitem_ebizdimprompt;
										var promtWt=rec.custitem_ebizweightprompt;
										nlapiLogExecution('DEBUG','promtDim',promtDim);
										nlapiLogExecution('DEBUG','promtWt',promtWt);
										if(promtDim=='T'||promtWt=='T')
										{
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_defaultdim', 'customdeploy_ebiz_rf_defaultdim_di', false, POarray);
										}
										else
										{
											var context = nlapiGetContext();
											var userAccountId = context.getCompany();
											if(userAccountId=='1285441')
											{
												nlapiLogExecution('DEBUG','userAccountId',userAccountId);
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_countryoforigin', 'customdeploy_ebiz_rf_countryoforigin_di', false, POarray);
											}
											else
											{
												//nlapiLogExecution('DEBUG','userAccountId','NotNautilus');
												response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty','customdeploy_rf_checkin_qty_di', false, POarray);
											}
										}
									} else {
										nlapiLogExecution('DEBUG', 'No SKU scanned or entered', POarray["custparam_poitem"]);
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									}
								}
							}
							if(tempflag=='F')
							{
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'itemRecord1', 'Invalid Item Specified');
							}
						} else {
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'itemRecord', 'Invalid Item Specified');
						}
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'itemRecord', 'Invalid Item Specified');
					}
				//} 
					/*else {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'poLineDetails ', 'No order lines retrieved for this SKU');
				}*/
			} 
			//Case# 20149155 starts
			else
			{
				POarray["custparam_error"] = st6;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Item  ', 'Please ENTER ITEM');
			}
			//Case# 20149155 ends
		} //end of first if condition//	Case # 20148012
		else {
			response.sendRedirect('SUITELET', 'customscript_rf_checkin_po', 'customdeploy_rf_checkin_po_di', false, POarray);
		}
		//}// Case # 20148012
	} //post else end
} //end of function.

//case 20123722 start

//Case# 201410723 starts
function CheckItemDimens(itemId, location,poItemPackcode)
{
	//nlapiLogExecution('DEBUG', 'CheckItemDimens-itemId ',itemId);
	//nlapiLogExecution('DEBUG', 'CheckItemDimens-location ',location);
	//nlapiLogExecution('DEBUG', 'CheckItemDimens-poItemPackcode ',poItemPackcode);
	var itemSearchResults='';
	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//case# 201410846
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));

	if(poItemPackcode!=null && poItemPackcode!='' && poItemPackcode!='null')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',poItemPackcode));

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 	
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true); 	

	itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);
	return itemSearchResults;
}
//Case# 201410723 ends
function getOrderHeaderLocation(orderId,trantype)
{
	var site='';
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('tranid', null, 'is', orderId));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('location');

	var poLineSearchResults = nlapiSearchRecord(trantype, null, filters, columns);

	if(poLineSearchResults != null && poLineSearchResults.length > 0)
	{
		site=poLineSearchResults[0].getValue('location');

		nlapiLogExecution('ERROR', 'No. of PO Lines Retrieved', poLineSearchResults.length);
	}

	return site;
}
//end

if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}

function eBiz_RF_GetItemForItemInternalId(itemNo,location){
	//nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo', 'Start');
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo, Input Item No', itemNo);

	var currItem = "";

	var sitemno = "";
	var context = nlapiGetContext();
	var userAccountId = context.getCompany();
	if(userAccountId=='1285441')
	{
		sitemno = checkserialritem(itemNo);
	}
	if(sitemno != "")
	{
		itemNo = sitemno;
	}	

	var filters = new Array();
	filters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemNo));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null && location!='' && location!='null')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('itemid');
	columns[0].setSort(true);


	var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);

	if(itemSearchResults != null)
		currItem = itemSearchResults[0].getId();

	var logMsg = 'Item = ' + currItem;
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo, Item Retrieved', logMsg);
	//nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo', 'End');

	return currItem;
}


