
/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_Autoship_Scheduler.js,v $
*  $Revision: 1.1.2.1.4.2.4.1 $
*  $Date: 2015/04/29 13:48:37 $
*  $Author: skreddy $
*  $Name: t_eBN_2015_1_StdBundle_1_73 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_Autoship_Scheduler.js,v $
*  Revision 1.1.2.1.4.2.4.1  2015/04/29 13:48:37  skreddy
*  Case# 20147994
*  Stnadard bundle issue fix
*
*  Revision 1.1.2.1.4.2  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function AutoShipOrders()
{
	var context = nlapiGetContext();
	var col = new Array();
	col[0] = new nlobjSearchColumn('custrecord_ship_order').setSort(true);
	col[1] = new nlobjSearchColumn('custrecord_ship_void');
	col[2] = new nlobjSearchColumn('entity','custrecord_ship_order');   

	var filters=new Array();
	filters[0]=new nlobjSearchFilter('custrecord_ship_custom4',null,'isempty');	
	filters[1]=new nlobjSearchFilter('custrecord_ship_pkgtype',null,'doesnotstartwith','SHIP');	
	filters[2]=new nlobjSearchFilter('custrecord_ship_order', null, 'noneof', '@NONE@');
	filters[3]=new nlobjSearchFilter('mainline','custrecord_ship_order', 'is', 'T');

	var results = nlapiSearchRecord('customrecord_ship_manifest', null, filters, col);
	if(results!=null)
	{
		var distinctArray=removeDuplicateElement(results);


		for ( var i = 0; i < distinctArray.length; i++ )
		{
			AutoShipMent(distinctArray[i]);


		}
	}
	else
		return;
}






function removeDuplicateElement(arrayName)
{
	var newArray=new Array();
	var tempArray=new Array();   
	var skiporderArray=new Array();
	label:for(var i=0;i<arrayName.length;i++)
	{
		var Orderno=arrayName[i].getValue('custrecord_ship_order');
		var entityid=arrayName[i].getValue('entity','custrecord_ship_order');
		if(entityid!=null && entityid!='' && skiporderArray.indexOf(Orderno)==-1)
		{
			var isAsnrequiredforcustomer = nlapiLookupField('customer',entityid ,'custentity_ebiz_asn_required');
			if( isAsnrequiredforcustomer!=null && isAsnrequiredforcustomer=='T' && skiporderArray.indexOf(Orderno)==-1)
			{
				var voidflag=null;
				if(Orderno!='undefined' && tempArray.indexOf(Orderno)==-1 && skiporderArray.indexOf(Orderno)==-1)
				{
					for(var j=0;j<arrayName.length;j++)
					{
						var tOrderno=arrayName[j].getValue('custrecord_ship_order');
						if(tOrderno!='undefined')
						{
							if(Orderno==tOrderno)
							{
								var tvoidflag=arrayName[j].getValue('custrecord_ship_void');
								if(tvoidflag!='U')
								{
									voidflag ='F';
									skiporderArray[skiporderArray.length]=Orderno;
									continue label;
								}
								else
								{
									voidflag ='T';
								}
							}
						}
					}

					if(voidflag!=null && voidflag=='T')
					{
						tempArray[tempArray.length]=Orderno;
						newArray.push(Orderno);
					}
				}
			}
			else
			{
				skiporderArray[skiporderArray.length]=Orderno;
			}
		}
		else
		{
			skiporderArray[skiporderArray.length]=Orderno;
		}

	}	

	return newArray;
}  

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
		"use strict";

		if (this === void 0 || this === null) throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (len === 0) return -1;

		var n = 0;
		if (arguments.length > 0) {
			n = Number(arguments[1]);
			if (n !== n) // shortcut for verifying if it's NaN
				n = 0;
			else if (n !== 0 && n !== (1 / 0) && n !== -(1 / 0)) n = (n > 0 || -1) * Math.floor(Math.abs(n));
		}

		if (n >= len) return -1;

		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);

		for (; k < len; k++) {
			if (k in t && t[k] === searchElement) return k;
		}
		return -1;
	};
}

function AutoShipMent(VOrdNo)
{
	var searchresultsTemp=new Array();
	// To get the Wave details based on selection criteria

	var searchresults=GetWaveOrdDetails(null,VOrdNo,null,null,null);
	if(searchresults!=null)
	{
		nlapiLogExecution('ERROR', "dataNotFoundMsg " , dataNotFoundMsg);
		if(dataNotFoundMsg=="")
		{
			nlapiLogExecution('ERROR', "searchresultsTemp " , searchresultsTemp.length);
			var vline, vitem, vqty, vTaskType, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno,vskustatus,vpackcode,vdono,vSOno,vuomid;
			var vcontLP,vLP,vSizeId,vTotWt,vTotCube;
			var now = new Date();		
			var lineCnt = searchresults.length;	
			var matchFound = true;
			var actualDate = DateStamp();
			var actualTime = TimeStamp();
			var sizeId;
			var TotWeight=0,TotVolume=0,inventoryInout;
			var vTareWeight=0,vTareVol=0;
			var NewShipLPNo;

			//To calulate auto Buildship# from LP Range
			var AutoShipLPNo=getAutoShipLP();
			NewShipLPNo=AutoShipLPNo;

			if(matchFound){
				nlapiLogExecution('ERROR',"if: matchFound ", matchFound );
				nlapiLogExecution('ERROR',"AutoShipLPNo ", NewShipLPNo );
				var picktaskid;
				var LpMasterId;

				var vBoolAnythingChecked=false;
				for (var k = 0; k < lineCnt; k++) {

					var searchresult = searchresults[k];	
					if(searchresult!=null)
					{
						vline = searchresult.custrecord_line_no; 
						vitem = searchresult.custrecord_Ebiz_sku_no;
						if(searchresult.custrecord_Act_qty==null || searchresult.custrecord_Act_qty== "")
							vqty=0;
						else
							vqty= parseInt(searchresult.custrecord_Act_qty);

						nlapiLogExecution("ERROR", "vqty ", vqty);
						vTaskType = searchresult.custrecord_Tasktype;
						vLpno = searchresult.custrecord_ebiz_sku_no;
						vlocationid = searchresult.custrecord_Actbeginloc;
						vlocation = searchresult.custrecord_Actbeginloc;
						vskustatusValue= searchresult.custrecord_Sku_status;
						vskustatus= searchresult.custrecord_Sku_status;
						vpackcode= searchresult.custrecord_Packcode;
						vdono=searchresult.Name;
						if(vdono != null && vdono != "")
							vSOno=vdono.split('.')[0];
						vSKUNo = searchresult.custrecord_Sku;
						vSKU = searchresult.custrecord_Sku; 
						vcontLP = searchresult.custrecord_Container_lp_no;
						vLP = searchresult.custrecord_Ebiz_lpmaster_lp;
						vSizeId = searchresult.custrecord_Ebiz_lpmaster_sizeid;
						vTotWt = searchresult.custrecord_Ebiz_lpmaster_totwght;
						vTotCube = searchresult.custrecord_Ebiz_lpmaster_totcube; 
						vuomid=searchresult.custrecord_Uom_id; 	
						picktaskid = searchresult.gId; 
						LpMasterId = searchresult.gidLp;
						var vCartWeight = vTotWt; 
						var vCartVol = vTotCube;


						//To calculate Total weight and volume

						if(vCartWeight!=null && vCartWeight !="")
							TotWeight+=parseFloat(vCartWeight);
						if(vCartVol!=null && vCartVol !="")
							TotVolume+=parseFloat(vCartVol);


						nlapiLogExecution('ERROR', "Line Count " , k);
						nlapiLogExecution('ERROR', "picktaskid" , picktaskid);
						nlapiLogExecution('ERROR', "LpMasterId" , LpMasterId);

						// To update ShipLP# in opentask
//						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', picktaskid);
//						transaction.setFieldValue('custrecord_wms_status_flag', "7"); // for status B(Built onto Ship Unit)
//						transaction.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
//						nlapiSubmitRecord(transaction, true);

						var fields = new Array();
						var values = new Array();

						fields[1] = 'custrecord_ebiztask_wms_status_flag';
						values[1] = '7';
						fields[2] ='custrecord_ebiztask_ship_lp_no';
						values[2] = NewShipLPNo;
						var updateclosetaskfields = nlapiSubmitField('customrecord_ebiznet_trn_ebiztask',searchresult.gId,fields,values);


						nlapiLogExecution('ERROR', 'opentask updated ', "");

						// To update ShipLP# in mast LP

//						var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', LpMasterId); 			 
//						transaction.setFieldValue('custrecord_ebiz_lpmaster_masterlp', NewShipLPNo);
//						nlapiSubmitRecord(transaction, true);

						var updatemasterlpfields = nlapiSubmitField('customrecord_ebiznet_master_lp',LpMasterId,'custrecord_ebiz_lpmaster_masterlp',NewShipLPNo);
						nlapiLogExecution('ERROR', 'LP Master updated ', ""); 

						if(k==0)
						{
							//Moving Pick Task to EbizTask
							nlapiLogExecution('ERROR', 'eBiztask updated ', "");

							var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
							//populating the fields
							customrecord.setFieldValue('custrecord_tasktype', '4'); //Ship Task
							//customrecord.setFieldValue('custrecord_container', sizeId); //Ship Task 
							customrecord.setFieldValue('custrecord_ebiz_wave_no', VQbwaveno);
							customrecord.setFieldValue('custrecord_totalcube', TotVolume);
							customrecord.setFieldValue('custrecord_total_weight', TotWeight);
							customrecord.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
							customrecord.setFieldValue('name', NewShipLPNo);
							customrecord.setFieldValue('custrecordact_begin_date', actualDate);
							customrecord.setFieldValue('custrecord_wms_status_flag', '14');// for status B(Built onto Ship Unit)
							customrecord.setFieldValue('custrecord_actualbegintime', actualTime);
							customrecord.setFieldValue('custrecord_ebiz_cntrl_no', searchresult.custrecord_ebiz_cntrl_no);
							customrecord.setFieldValue('custrecord_site_id', vSiteId);//For SiteId 
							nlapiLogExecution('ERROR', 'Submitting SHIP record', 'TRN_OPENTASK');
							//commit the record to NetSuite
							var opentaskrecid = nlapiSubmitRecord(customrecord, true);
							nlapiLogExecution('ERROR', 'Submitted SHIP record', 'TRN_OPENTASK');


							// To Create record in LP Master
							var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp'); 
							customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', NewShipLPNo);
							//customrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', sizeId);
							customrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotWeight);
							customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', '3');//SHIP
							customrecord.setFieldValue('name', NewShipLPNo);
							customrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', TotVolume);
							customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
							//commit the record to NetSuite
							var masterlprecid = nlapiSubmitRecord(customrecord, true);

						}
						//Load Depart trailer  code.


						/*	var filters = new Array();		

						filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['4']); //ship task
						var j = 1;
						//nlapiLogExecution('ERROR', "VQbwave " + VQbwave);
						if (VQbwaveno != "") {
							filters[j] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', VQbwaveno);
							j = j + 1;
						}
						if (NewShipLPNo != "") {
							filters[j] = new nlobjSearchFilter('custrecord_ebiz_ship_lp_no', null, 'starts with', NewShipLPNo);
							j = j + 1;
						}
						filters[j] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7']);


						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_line_no');
						columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
						columns[2] = new nlobjSearchColumn('custrecord_act_qty');
						columns[3] = new nlobjSearchColumn('custrecord_tasktype');
						columns[4] = new nlobjSearchColumn('custrecord_ebiz_ship_lp_no');
						columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
						columns[6] = new nlobjSearchColumn('custrecord_sku');
						columns[7] = new nlobjSearchColumn('name');			
						columns[8] = new nlobjSearchColumn('custrecord_container_lp_no'); 
						columns[9] = new nlobjSearchColumn('custrecord_sku_status');
						columns[10] = new nlobjSearchColumn('custrecord_packcode');
						columns[11] = new nlobjSearchColumn('custrecord_uom_id');
						columns[12] = new nlobjSearchColumn('custrecord_total_weight');
						columns[13] = new nlobjSearchColumn('custrecord_totalcube');
						columns[14] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');


						var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);


						var vline, vitem, vqty, vTaskType, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vitemstatus, vpackcode;
						var vtotcube = 0;
						var vtotwght=0;
						for (var i = 0; searchresults != null && i < searchresults.length; i++) {

							nlapiLogExecution('ERROR', "searchresults" + searchresults.length);
							var searchresult = searchresults[i];

							vline = searchresult.getValue('custrecord_line_no');
							vitem = searchresult.getValue('custrecord_ebiz_sku_no');

							vqty = searchresult.getValue('custrecord_act_qty');
							vTaskType = searchresult.getValue('custrecord_tasktype');
							vLpno = searchresult.getValue('custrecord_ebiz_ship_lp_no');
							vlocationid = searchresult.getValue('custrecord_actbeginloc');

							vlocation = searchresult.getText('custrecord_actbeginloc');
							vitemstatus = searchresult.getValue('custrecord_sku_status');
							vpackcode = searchresult.getValue('custrecord_packcode');

							nlapiLogExecution('ERROR', "vlocationid" + vlocationid);
							nlapiLogExecution('ERROR', "vlocation" + vlocation);


							vSKU = searchresult.getText('custrecord_sku');

							var vlinecube = searchresult.getValue('custrecord_totalcube');
							var vlinewght = searchresult.getValue('custrecord_total_weight');
							if (!isNaN(vlinecube)) {
								vtotcube = parseFloat(vtotcube) + parseFloat(vlinecube);
							}

							if (!isNaN(vlinewght)) {
								vtotwght = parseFloat(vtotwght) + parseFloat(vlinewght);
							}
							var now = new Date();					
							var itemname= vSKU;
							var itemNo= "";
							var avlqty=vqty; 	 
							var ebizDono=searchresult.getValue('custrecord_ebiz_cntrl_no'); 
							var Lineno=vline; 
							var vDoname= searchresult.getValue('name');
							var ebizWaveNo = VQbwaveno;
							var vactLocation = vlocation;
							var picktaskid= searchresult.getId();					

							var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', picktaskid);
							transaction.setFieldValue('custrecord_wms_status_flag', "14"); //14-S(shipped)				
							nlapiSubmitRecord(transaction, true);
							nlapiLogExecution('ERROR', "ebizDono" + ebizDono);

							var docols = nlapiLookupField('customrecord_ebiznet_ordline',ebizDono,['name','custrecord_ns_ord']);
							var salsordInternid=docols.name;
							var vsono=docols.custrecord_ns_ord;
							nlapiLogExecution('ERROR', "salsordInternid" + salsordInternid);
							var type = "salesorder";					
							var putgenqty = 0;
							var putconfqty = 0;
							var ttype = "SHIP";
							var confqty = avlqty;				
							var tranValue = TrnLineUpdation(type, ttype, salsordInternid, vsono, Lineno, itemNo, avlqty, confqty);*/
					}
				}
			}
		}
	}
}


function GetWaveOrdDetails(VQbwave,VOrdNo,VShipLP,VConsignee,searchresultsTemp)
{

	nlapiLogExecution('ERROR',"Into GetWaveOrdDetails (VOrdNo) ", VOrdNo );

	var filters = new Array();
	var searchresults =new Array();
//case 20147994  
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', ['3']));  //Pick Task
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', ['8','28'])); //statusflag='C'and STATUS.OUTBOUND.PACK_COMPLETE

	filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', [VOrdNo])); //for OrdNo'
	//end

	nlapiLogExecution('ERROR',"OrdNo ", VOrdNo );

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiztask_line_no');
	columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiztask_act_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiztask_tasktype');
	columns[4] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_ship_lp_no');
	columns[5] = new nlobjSearchColumn('custrecord_ebiztask_actbeginloc');
	columns[6] = new nlobjSearchColumn('custrecord_ebiztask_sku');
	columns[7] = new nlobjSearchColumn('name');    	
	columns[8] = new nlobjSearchColumn('custrecord_ebiztask_contlp_no');    	 
	columns[9] = new nlobjSearchColumn('custrecord_ebiztask_sku_status');
	columns[10] = new nlobjSearchColumn('custrecord_ebiztask_packcode');
	columns[11] = new nlobjSearchColumn('custrecord_ebiztask_uom_id');
	columns[12] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_sku_no');
	columns[13] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_cntrl_no');		
	columns[14] = new nlobjSearchColumn('custrecord_ebiztask_totalcube');
	columns[15] = new nlobjSearchColumn('custrecord_ebiztask_total_weight');
	columns[16] = new nlobjSearchColumn('custrecord_ebiztask_comp_id');
	columns[17] = new nlobjSearchColumn('custrecord_ebiztask_site_id');
	columns[18] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_wave_no');
	// To get the data from Opentask based on selection criteria
	searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);
	if(searchresults !=null && searchresults != "" )
	{
		nlapiLogExecution('ERROR',"close Task Records Found ", searchresults );
		searchresultsTemp=searchresults;	var searchData=new Array();
		if(searchresults.length>0)
		{
			VQbwaveno=searchresults[0].getValue('custrecord_ebiztask_ebiz_wave_no') ;
			nlapiLogExecution('ERROR',"VQbwaveno ", VQbwaveno );
			for(var i=0;i<searchresults.length;i++)
			{		
				columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
				columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
				columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
				columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');

				filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', searchresults[i].getValue('custrecord_ebiztask_contlp_no'))); //for OrdNo'

				// To get the Container Details from master lp
				var searchresultsLP = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
				//nlapiLogExecution('ERROR',"searchresults MasterLP ",searchresultsLP.length );

				// TO map container lp# of opentask with lp# of mast lp
				nlapiLogExecution('ERROR',"searchresultsLP", searchresultsLP );
				if(searchresultsLP != null && searchresultsLP != "" )
				{
					if(searchresultsLP.length>0)
					{
						vCompId=searchresults[i].getValue('custrecord_ebiztask_comp_id');
						vSiteId=searchresults[i].getValue('custrecord_ebiztask_site_id');

						nlapiLogExecution('ERROR',"searchresults value ", searchresults[i].getValue('custrecord_ebiztask_contlp_no') );

						searchData.push(new closeTaskRec(searchresults[i].getValue('custrecord_ebiztask_line_no'),searchresults[i].getValue('custrecord_ebiztask_ebiz_sku_no'),
								searchresults[i].getValue('custrecord_ebiztask_act_qty'),searchresults[i].getValue('custrecord_ebiztask_tasktype'),searchresults[i].getValue('custrecord_ebiztask_ebiz_ship_lp_no')
								,searchresults[i].getText('custrecord_ebiztask_actbeginloc'),searchresults[i].getText('custrecord_ebiztask_sku'),searchresults[i].getValue('name')
								,searchresults[i].getValue('custrecord_ebiztask_contlp_no'),searchresults[i].getText('custrecord_ebiztask_sku_status'),searchresults[i].getValue('custrecord_ebiztask_packcode')
								,searchresults[i].getValue('custrecord_ebiztask_uom_id'),searchresults[i].getValue('custrecord_ebiztask_ebiz_sku_no'),searchresults[i].getValue('custrecord_ebiztask_ebiz_cntrl_no')
								,searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_lp'),searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_sizeid')
								,searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_totwght'),searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_totcube'),searchresults[i].getValue('custrecord_ebiztask_total_weight'),searchresults[i].getValue('custrecord_ebiztask_totalcube'), searchresults[i].getId(), searchresultsLP[0].getId()));

						nlapiLogExecution('ERROR',"searchresults Data ",searchresults.length );
					}
				}

				else
				{
					dataNotFoundMsg="No records found for the given search criteria";
				}
				if(searchresults == null || searchresults == "")
				{
					dataNotFoundMsg="No records found for the given search criteria";
				}
			}
			searchresults=searchData;
		}
	}
	else
	{
		dataNotFoundMsg="No records found for the given search criteria";
	}
	return searchresults;
}


function getAutoShipLP()
{
	var AutoShipLPNo="1";	 
	AutoShipLPNo=GetMaxLPNo('1', '3');// for Auto generated with SHIP LPType
	return AutoShipLPNo;
}

function closeTaskRec(custrecord_ebiztask_line_no, custrecord_ebiztask_ebiz_sku_no, custrecord_ebiztask_act_qty,custrecord_ebiztask_tasktype
		,custrecord_ebiz_ship_lp_no,custrecord_ebiztask_actbeginloc,custrecord_ebiztask_sku,name
		,custrecord_ebiztask_contlp_no,custrecord_ebiztask_sku_status,custrecord_ebiztask_packcode,custrecord_ebiztask_uom_id
		,custrecord_ebiztask_ebiz_sku_no,custrecord_ebiztask_ebiz_cntrl_no,custrecord_ebiz_lpmaster_lp,custrecord_ebiz_lpmaster_sizeid
		,custrecord_ebiz_lpmaster_totwght,custrecord_ebiz_lpmaster_totcube,custrecord_ebiztask_total_weight,custrecord_ebiztask_totalcube,gid,gidlp) {
	this.custrecord_Line_no = custrecord_ebiztask_line_no;
	this.custrecord_Ebiz_sku_no = custrecord_ebiztask_ebiz_sku_no;
	this.custrecord_Act_qty = custrecord_act_qty;
	this.custrecord_Tasktype = custrecord_ebiztask_tasktype;
	this.custrecord_Ebiz_ship_lp_no = custrecord_ebiztask_ebiz_ship_lp_no;
	this.custrecord_Actbeginloc = custrecord_actbeginloc;
	this.custrecord_Sku = custrecord_ebiztask_sku;
	this.Name = name;
	this.custrecord_Container_lp_no = custrecord_ebiztask_contlp_no;
	this.custrecord_Sku_status = custrecord_ebiztask_sku_status;
	this.custrecord_Packcode = custrecord_ebiztask_packcode;
	this.custrecord_Uom_id = custrecord_uom_id;
	this.custrecord_Ebiz_sku_no = custrecord_ebiztask_ebiz_sku_no;
	this.custrecord_Ebiz_cntrl_no = custrecord_ebiztask_ebiz_cntrl_no;
	this.custrecord_Ebiz_lpmaster_lp = custrecord_ebiz_lpmaster_lp;
	this.custrecord_Ebiz_lpmaster_sizeid = custrecord_ebiz_lpmaster_sizeid;
	this.custrecord_Ebiz_lpmaster_totwght = custrecord_ebiz_lpmaster_totwght;
	this.custrecord_Ebiz_lpmaster_totcube = custrecord_ebiz_lpmaster_totcube;
	this.custrecord_Total_weight = custrecord_ebiztask_total_weight;
	this.custrecord_Totalcube = custrecord_ebiztask_totalcube;
	this.gId = gid;
	this.gidLp = gidlp;
}