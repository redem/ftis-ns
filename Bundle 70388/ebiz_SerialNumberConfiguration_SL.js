/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_SerialNumberConfiguration_SL.js,v $
 *     	   $Revision: 1.4.10.1.4.16.2.2 $
 *     	   $Date: 2015/11/04 15:46:30 $
 *     	   $Author: nneelam $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_73 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_SerialNumberConfiguration_SL.js,v $
 * Revision 1.4.10.1.4.16.2.2  2015/11/04 15:46:30  nneelam
 * case# 201415204
 *
 * Revision 1.4.10.1.4.16.2.1  2015/11/02 23:37:27  sponnaganti
 * case# 201415243
 * Issue is while getting serial numbers from item fullfilment we are checking
 * with link type is 'Item Fulfillment' but it is returning 'Item Shipment'.
 *
 * Revision 1.4.10.1.4.16  2015/06/29 13:18:22  schepuri
 * case# 201413271
 *
 * Revision 1.4.10.1.4.15  2015/06/22 15:42:28  grao
 * SW issue fixes  201413180   and 201413146
 *
 * Revision 1.4.10.1.4.14  2015/06/17 12:34:40  schepuri
 * case# 201413146
 *
 * Revision 1.4.10.1.4.13  2015/06/12 15:23:35  nneelam
 * case# 201413006
 *
 * Revision 1.4.10.1.4.12  2015/05/19 13:13:16  schepuri
 * case# 201412842
 *
 * Revision 1.4.10.1.4.11  2014/11/18 15:32:47  skavuri
 * Case# 201411083 Std bundle issue fixed
 *
 * Revision 1.4.10.1.4.10  2014/11/11 06:11:56  vmandala
 * case#  201411005 Stdbundle issue fixed
 *
 * Revision 1.4.10.1.4.9  2014/08/18 15:24:21  sponnaganti
 * Case# 201410030
 * Stnd Bundle Issue fix
 *
 * Revision 1.4.10.1.4.8  2014/05/27 15:37:43  sponnaganti
 * case# 20148547
 * Stnd Bundle Issue fix
 *
 * Revision 1.4.10.1.4.7  2014/02/07 14:49:23  rmukkera
 * Case # 20127054
 *
 * Revision 1.4.10.1.4.6  2014/01/10 13:50:48  schepuri
 * 20126723
 *
 * Revision 1.4.10.1.4.5  2014/01/09 14:07:06  grao
 * Case# 20126703 related issue fixes in Sb issue fixes
 *
 * Revision 1.4.10.1.4.4  2014/01/09 09:24:13  grao
 * no message
 *
 * Revision 1.4.10.1.4.3  2014/01/08 13:51:36  schepuri
 * 20126701
 *
 * Revision 1.4.10.1.4.2  2014/01/07 14:40:47  grao
 * Case# 20126632 related issue fixes in Sb issue fixes
 *
 * Revision 1.4.10.1.4.1  2013/04/03 20:40:48  kavitha
 * CASE201112/CR201113/LOG2012392
 * TSG Issue fixes
 *
 * Revision 1.4.10.1  2012/12/11 03:21:00  kavitha
 * CASE201112/CR201113/LOG201121
 * Incorporated Serial # functionality
 *
 * Revision 1.4  2011/12/12 07:03:07  rgore
 * CASE201112/CR201113/LOG201121
 * Sanitized error message for spelling and grammar.
 * - Ratnakar
 * 12 Dec 2011
 *
 * Revision 1.3  2011/07/21 08:11:35  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function SerialNumberConfig(request, response)
{
	if (request.getMethod() == 'GET') 
	{   
		var form = nlapiCreateForm('Serial Entry');
		var getPOid = request.getParameter('custparam_serialpoid');
		nlapiLogExecution('ERROR', 'getPOid', getPOid);		



		var getSKU= request.getParameter('custparam_serialskuid');
		nlapiLogExecution('ERROR', 'getSKU', getSKU);

		var getSKUline= request.getParameter('custparam_serialskulineno');
		nlapiLogExecution('ERROR', 'getSKULine num', getSKUline);

		var getLP= request.getParameter('custparam_serialskulp');
		nlapiLogExecution('ERROR', 'getLP', getLP);

		var getChknQty= request.getParameter('custparam_serialskuchknqty');
		nlapiLogExecution('ERROR', 'getChknQty', getChknQty);

		var getFrmname= "";
		if(request.getParameter('custparam_serialformname') != null && request.getParameter('custparam_serialformname') != "")
		{
			getFrmname= request.getParameter('custparam_serialformname');
		}
		nlapiLogExecution('ERROR', 'getFrmname', getFrmname);

		//custparam_seriallocation
		var getLocation = "";
		if(request.getParameter('custparam_seriallocation') != null && request.getParameter('custparam_seriallocation') != "")
		{
			getLocation= request.getParameter('custparam_seriallocation');
		}
		nlapiLogExecution('ERROR', 'getLocation', getLocation);

		//custparam_serialbinlocation
		var getBinLocation = "";
		if(request.getParameter('custparam_serialbinlocation') != null && request.getParameter('custparam_serialbinlocation') != "")
		{
			getBinLocation = request.getParameter('custparam_serialbinlocation');
		}
		nlapiLogExecution('ERROR', 'getBinLocation', getBinLocation);

		var ordField = form.addField('custpage_serialordno', 'select', 'Ord No#', 'transaction');
		if (getPOid != null || getPOid != "") {
			ordField.setDefaultValue(request.getParameter('custparam_serialpoid'));
		}
		ordField.setLayoutType('startrow');

		var skuField= form.addField('custpage_serialsku', 'select', 'Item', 'item').setDisabled(true);
		if(getSKU!=null || getSKU!="")
		{
			skuField.setDefaultValue(request.getParameter('custparam_serialskuid'));
		}

		var skuLineField= form.addField('custpage_serialskuline', 'text', 'Item Line No');
		if(getSKUline!=null || getSKUline!="")
		{
			skuLineField.setDefaultValue(getSKUline);
		}

		var lpField= form.addField('custpage_seriallp', 'text', 'LP');
		if(getLP!=null || getLP!="")
		{
			lpField.setDefaultValue(request.getParameter('custparam_serialskulp'));
		}
		 //case 201411005
		var chknQtyField= form.addField('custpage_serialchknqty', 'text', 'Quantity').setDisabled(true);
		var chknhiddenQtyField= form.addField('custpage_serialhiddenchknqty', 'text', 'Quantity').setDisplayType('hidden');
		
		if(getChknQty!=null || getChknQty!="")
		{
			chknQtyField.setDefaultValue(getChknQty);
			chknhiddenQtyField.setDefaultValue(getChknQty);//Case# 201411083
		}

		var FrmNameField= form.addField('custpage_serialfrmname', 'text', 'form name');
		if(getFrmname!=null && getFrmname!="")
		{
			FrmNameField.setDefaultValue(getFrmname);
		}
		FrmNameField.setDisplayType('hidden');

		var LocationField= form.addField('custpage_seriallocation', 'text', 'Location');
		if(getLocation!=null && getLocation!="")
		{
			LocationField.setDefaultValue(getLocation);
		}
		LocationField.setDisplayType('hidden');

		var BinLocationField= form.addField('custpage_serialbinlocation', 'text', 'Bin Location');
		if(getBinLocation != null && getBinLocation != "")
		{
			BinLocationField.setDefaultValue(getBinLocation);
		}
		BinLocationField.setDisplayType('hidden');

		var autobutton = form.addSubmitButton('Submit');
		response.writePage(form);
	}
	else
	{
		try {
			var ordid = request.getParameter('custpage_serialordno');
			nlapiLogExecution('ERROR', 'ordid', ordid);

			//var linechknQty = request.getParameter('custpage_serialchknqty');
			 //case 201411005
			var linechknQty = request.getParameter('custpage_serialhiddenchknqty');
			
			nlapiLogExecution('ERROR', 'linechknQty', linechknQty);

			//Form Creation.
			var form = nlapiCreateForm('Serial Entry');
			if(request.getParameter('custpage_hiddenfieldselectpage') != 'F'){
				
				var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
				hiddenfieldselectpage.setDefaultValue('F');
				
				
				var ordField = form.addField('custpage_serialordno', 'select', 'Ord No#', 'transaction').setDisplayType('hidden');;
				if (ordField != null && ordField != "") {
					ordField.setDefaultValue(request.getParameter('custpage_serialordno'));
				}
			//if (linechknQty) {

				var skuid = request.getParameter('custpage_serialsku');
				nlapiLogExecution('ERROR', 'skuid', skuid);

				var skuLineField = request.getParameter('custpage_serialskuline');
				nlapiLogExecution('ERROR', 'skuLineField', skuLineField);

				var serLP = request.getParameter('custpage_seriallp');
				var frmName = request.getParameter('custpage_serialfrmname');
				var seriallocation = request.getParameter('custpage_seriallocation');
				var serialbinlocation = request.getParameter('custpage_serialbinlocation');

				var lineQty = 0;

				if(ordid != null && ordid !=''){
					var tranType= nlapiLookupField('transaction',ordid,'recordType');
					nlapiLogExecution('ERROR', 'tranType', tranType);
					var ordLoadrec = nlapiLoadRecord(tranType, ordid);
					if (linechknQty == null || linechknQty == "") {
						lineQty = ordLoadrec.getLineItemValue('item', 'quantity', skuLineField);
						nlapiLogExecution('ERROR', 'Quantity', lineQty);
					}
					else {
						lineQty = linechknQty;
					}
				}	
				else
				{
					lineQty = linechknQty;
				}

				//Calling Client Script.
				form.setScript('customscript_serialnumclientsave');

				var resordField = form.addField('custpage_respserialordno', 'select', 'Ord No#', 'transaction');
				// case no 20126723

				//if (ordid != null || ordid != "") {
				if (ordid != null && ordid != "") {
					resordField.setDefaultValue(ordid);
				}
				resordField.setLayoutType('startrow');
				resordField.setDisplayType('inline');
				var resTranType=form.addField("custpage_trantype", "text", "Tran Type").setDisplayType('hidden');
				if(tranType!=null && tranType!="")
				{
					resTranType.setDefaultValue(tranType);
				}

				var respskuField = form.addField('custpage_serialsku', 'select', 'Item', 'item');
				if (skuid != null && skuid != "") {
					respskuField.setDefaultValue(skuid);
				}
				respskuField.setDisplayType('inline');

				var respskuLineField = form.addField('custpage_serialskuline', 'text', 'Item Line No');
				if (skuLineField != null && skuLineField != "") {
					respskuLineField.setDefaultValue(skuLineField);
				}
				respskuLineField.setDisplayType('inline');
				
				var chknhiddenQtyField= form.addField('custpage_serialhiddenchknqty', 'text', 'Quantity').setDisplayType('hidden');
				chknhiddenQtyField.setDefaultValue(linechknQty);


				var resplpField = form.addField('custpage_seriallp', 'text', 'LP');
				if (serLP != null && serLP != "") {
					resplpField.setDefaultValue(serLP);
				}
				resplpField.setDisplayType('inline');

				//custparam_serialformname
				var respFrmNameField = form.addField('custpage_serialfrmname', 'text', 'Frm Name');
				if (frmName != null && frmName != "") {
					respFrmNameField.setDefaultValue(frmName);
				}
				respFrmNameField.setDisplayType('hidden');

				var respLocationField = form.addField('custpage_seriallocation', 'text', 'Location');
				if (seriallocation != null && seriallocation != "") {
					respLocationField.setDefaultValue(seriallocation);
				}
				respLocationField.setDisplayType('hidden');

				var respBinLocationField = form.addField('custpage_serialbinlocation', 'text', 'Bin Location');
				if (serialbinlocation != null && serialbinlocation != "") {
					respBinLocationField.setDefaultValue(serialbinlocation);
				}
				respBinLocationField.setDisplayType('hidden');

				var sublist = form.addSubList("custpage_items", "list", "ItemList");
				sublist.addField("custpage_serialrec_no", "text", "Rec.No").setDisplayType('inline');
				sublist.addField("custpage_serial_uom", "text", "UOM").setDisplayType('inline');
				sublist.addField("custpage_serialparentid", "text", "Parent ID").setDisplayType('inline');
				sublist.addField("custpage_serial_serialno", "text", "Serial #").setDisplayType('entry');
				sublist.addField("custpage_serial_fulfillserialno", "text", "FulfillSerial #").setDisplayType('hidden');

				nlapiLogExecution('ERROR', 'tranType', tranType);
				//for (var s = 1; s <= lineQty; s++) {

				var vAdvBinManagement=false;

					var ctx = nlapiGetContext();
					if(ctx != null && ctx != '')
					{
						if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
							vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
						nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
					}
					if(ordid != null && ordid != ''){
						nlapiLogExecution('ERROR', 'ordid', ordid);
						//var trantype = nlapiLookupField('transaction', ordid, 'recordType');
						var trantype =tranType;
						// case no 20126701
						if(trantype=='transferorder')
						{
							/*if(vAdvBinManagement)
							{*/										
								var trecord = nlapiLoadRecord(trantype, ordid);
								var links=trecord.getLineItemCount('links');
								/*var id=trecord.getLineItemValue('links','id',1);
								nlapiLogExecution('ERROR', 'id', id);
								var frecord = nlapiLoadRecord('itemfulfillment', id);
								frecord.selectLineItem('item', 1);
								var compSubRecord = frecord.viewCurrentLineItemSubrecord('item','inventorydetail');

								var polinelength = compSubRecord.getLineItemCount('inventoryassignment');
								nlapiLogExecution('ERROR', 'polinelength', polinelength);
								var itemfulfilserialno;
								var itemTempfulfilserialno;
								for(var j=1;j<=polinelength ;j++)
								{
//									itemfulfilserialno=compSubRecord.getLineItemValue('inventoryassignment','issueinventorynumber_display',j);
									itemfulfilserialno=compSubRecord.getLineItemValue('inventoryassignment','issueinventorynumber',j);
									nlapiLogExecution('ERROR', 'itemfulfilserialno', itemfulfilserialno);
									if(itemfulfilserialno!=null && itemfulfilserialno!='')
									{
										if(itemTempfulfilserialno == null || itemTempfulfilserialno == '')
											itemTempfulfilserialno=itemfulfilserialno;
										else										 
											itemTempfulfilserialno= itemTempfulfilserialno + ',' + itemfulfilserialno;

									}
									nlapiLogExecution('ERROR', 'itemTempfulfilserialno', itemTempfulfilserialno);
								}*/


								var itemTempfulfilserialno='';
								nlapiLogExecution('ERROR', 'links', links);
								if(links !=null && links!='')
								{

									for(var j1=1;j1<=links;j1++)
									{
										nlapiLogExecution('DEBUG', 'inside links',links);
										var id=trecord.getLineItemValue('links','id',(parseInt(j1)));
										var linktype = trecord.getLineItemValue('links','type',(parseInt(j1)));

										nlapiLogExecution('DEBUG', 'linktype',linktype);
										nlapiLogExecution('DEBUG', 'id',id);

										if(linktype=='Item Fulfillment' || linktype=='Item Shipment')
										{
											var frecord = nlapiLoadRecord('itemfulfillment', id);
//											case no start 20126901

											if(vAdvBinManagement)
											{		
												//case# 20148547 starts
												var itemcount=frecord.getLineItemCount('item');
												for(var h=1;h<=itemcount;h++)
												{
													//frecord.selectLineItem('item', 1);
													frecord.selectLineItem('item', h);
													var item=frecord.getLineItemValue('item','item',h);
													nlapiLogExecution('ERROR', 'item : skuid', item +" : "+skuid);
													if(item==skuid)
													{
														var compSubRecord = frecord.viewCurrentLineItemSubrecord('item','inventorydetail');

														var polinelength = compSubRecord.getLineItemCount('inventoryassignment');
														nlapiLogExecution('ERROR', 'polinelength', polinelength);
														var itemfulfilserialno;

														for(var k=1;k<=polinelength ;k++)
														{
															itemfulfilserialno=compSubRecord.getLineItemText('inventoryassignment','issueinventorynumber',k);
															nlapiLogExecution('ERROR', 'itemfulfilserialno', itemfulfilserialno);
															if(itemfulfilserialno!=null && itemfulfilserialno!='')
															{
																if(itemTempfulfilserialno == null || itemTempfulfilserialno == '')
																	itemTempfulfilserialno=itemfulfilserialno;
																else										 
																	itemTempfulfilserialno= itemTempfulfilserialno + ',' + itemfulfilserialno;

															}
															nlapiLogExecution('ERROR', 'itemTempfulfilserialno', itemTempfulfilserialno);
														}

														// case no end 20126901
													}
												}//case# 20148547 ends
											}

											else
											{

												var fitemcount=frecord.getLineItemCount('item');

												for(var f=1;f<=fitemcount;f++)
												{
													var fitem=frecord.getLineItemValue('item','item',f);
													var fline=frecord.getLineItemValue('item','orderline',f);
													var pofline= fline-1;

													nlapiLogExecution('DEBUG', 'fitem',fitem);
													//nlapiLogExecution('DEBUG', 'POarray[custparam_fetcheditemid]',POarray["custparam_fetcheditemid"]);// case# 201412842

													if(fitem==skuid) //&& parseInt(getPOLineNo)==parseInt(pofline))
													{
														itemfulfilserialno=frecord.getLineItemValue('item','serialnumbers',(parseInt(f)));
														if(itemfulfilserialno!=null && itemfulfilserialno!='')
														{
															if(itemTempfulfilserialno == null || itemTempfulfilserialno == '')
																itemTempfulfilserialno=itemfulfilserialno;
															else										 
																itemTempfulfilserialno= itemTempfulfilserialno + ',' + itemfulfilserialno;

														}
														nlapiLogExecution('ERROR', 'itemTempfulfilserialno', itemTempfulfilserialno);									

													}
												}
											}//advanced bin management false
										}
									}


								}





								/*if(itemTempfulfilserialno != null && itemTempfulfilserialno != '')
									form.getSubList('custpage_items').setLineItemValue('custpage_serial_fulfillserialno', s, itemTempfulfilserialno);*/
							/*}*/
						}
					}
					//case# 201410030
					for (var s = 1; s <= lineQty; s++) {

						if(itemTempfulfilserialno != null && itemTempfulfilserialno != '')
							form.getSubList('custpage_items').setLineItemValue('custpage_serial_fulfillserialno', s, itemTempfulfilserialno);	
					form.getSubList('custpage_items').setLineItemValue('custpage_serialrec_no', s, s);
					form.getSubList('custpage_items').setLineItemValue('custpage_serial_uom', s, 'EACH');
					form.getSubList('custpage_items').setLineItemValue('custpage_serialparentid', s, serLP);
					//form.getSubList('custpage_items').setLineItemValue('custpage_serial_serialno', s , serLP);

				}
				var autobutton = form.addSubmitButton('Save');
				var resetbtn = form.addResetButton('Reset');
			}
			else
			{
				var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
				hiddenfieldselectpage.setDefaultValue('F');
				var linechknQty = request.getParameter('custpage_serialhiddenchknqty');

				var poId = request.getParameter('custpage_respserialordno');
				
				/*var tranType= nlapiLookupField('transaction',poId,'recordType');
				nlapiLogExecution('ERROR', 'tranType', tranType);
				var ordLoadrec1 = nlapiLoadRecord(tranType, poId);
				
				var poText = ordLoadrec1.getFieldValue('tranid');*/
				var poText='';
				var tranType='';
				if(poId != null && poId != '')
				{
					tranType= nlapiLookupField('transaction',poId,'recordType');
					nlapiLogExecution('ERROR', 'tranType', tranType);

					var ordLoadrec1 = nlapiLoadRecord(tranType, poId);

					poText = ordLoadrec1.getFieldValue('tranid');
				}
				var serialskuline = request.getParameter('custpage_serialskuline');
				var serialskid = request.getParameter('custpage_serialsku');
				var serialskulp = request.getParameter('custpage_seriallp');
				var seriallocation = request.getParameter('custpage_seriallocation');
				var serialbinlocation = request.getParameter('custpage_serialbinlocation');
				
				
				/*var skuid = request.getParameter('custpage_serialsku');
				nlapiLogExecution('ERROR', 'skuid', skuid);

				var skuLineField = request.getParameter('custpage_serialskuline');
				nlapiLogExecution('ERROR', 'skuLineField', skuLineField);

				var serLP = request.getParameter('custpage_seriallp');
				var frmName = request.getParameter('custpage_serialfrmname');
				var seriallocation = request.getParameter('custpage_seriallocation');
				var serialbinlocation = request.getParameter('custpage_serialbinlocation');*/
				var frmName = request.getParameter('custpage_serialfrmname');// case# 201413271
				nlapiLogExecution('ERROR', 'linechknQty', linechknQty);
				nlapiLogExecution('ERROR', 'poId', poId);
				nlapiLogExecution('ERROR', 'poText', poText);
				nlapiLogExecution('ERROR', 'serialskuline', serialskuline);
				nlapiLogExecution('ERROR', 'serialskid', serialskid);
				nlapiLogExecution('ERROR', 'serialskulp', serialskulp);
				nlapiLogExecution('ERROR', 'seriallocation', seriallocation);
				nlapiLogExecution('ERROR', 'serialbinlocation', serialbinlocation);
				nlapiLogExecution('ERROR', 'frmName', frmName);
				for (var p = 1; p <= linechknQty; p++) {
					//var lineUom = form.getSubList('custpage_items').getLineItemValue('custpage_serial_uom', p);
					var lineUom = request.getLineItemValue('custpage_items','custpage_serial_uom', p);

					//	alert('lineUom. '+ lineUom);

					//var lineParentId= nlapiGetLineItemValue('custpage_items','custpage_serialparentid',p);
					var lineSerialNo = request.getLineItemValue('custpage_items','custpage_serial_serialno', p);


					var createSerialEntry = nlapiCreateRecord('customrecord_ebiznetserialentry');

					if(poText != null && poText != "")
					{
						createSerialEntry.setFieldValue('name', poText);
					}
					else
					{
						createSerialEntry.setFieldValue('name', lineSerialNo);
					}

					if(tranType != undefined){	
						if(tranType=='returnauthorization')
						{
							createSerialEntry.setFieldValue('custrecord_ebiz_serial_rmano', poText);
							createSerialEntry.setFieldValue('custrecord_ebiz_serial_rmalineno', serialskuline);
							createSerialEntry.setFieldValue('custrecord_serialebizrmano', poId);
						}//Case #20126899�start
						else if(tranType=='purchaseorder' || tranType=='transferorder' )
						{//Case #20126899�end
							createSerialEntry.setFieldValue('custrecord_serialpono', poText);
							createSerialEntry.setFieldValue('custrecord_serialpolineno', serialskuline);
							createSerialEntry.setFieldValue('custrecord_serialebizpono', poId);

						}
					}

					createSerialEntry.setFieldValue('custrecord_serialitem', serialskid);
					createSerialEntry.setFieldValue('custrecord_serialiteminternalid', serialskid);
					createSerialEntry.setFieldValue('custrecord_serialparentid', serialskulp);
					createSerialEntry.setFieldValue('custrecord_serialqty', '1');
					createSerialEntry.setFieldValue('custrecord_serialnumber', lineSerialNo);
					createSerialEntry.setFieldValue('custrecord_serialuomid', lineUom);
					if(frmName != null && (frmName == "InvtAdjustment" || frmName == "CyclecountRecord"))
					{
						createSerialEntry.setFieldValue('custrecord_serialwmsstatus', 3);
						createSerialEntry.setFieldValue('custrecord_serialstatus', 'I');
					}
					else if(frmName != null && frmName == "CreateInvt")
					{
						createSerialEntry.setFieldValue('custrecord_serialwmsstatus', 19);
					}
					else
					{
						createSerialEntry.setFieldValue('custrecord_serialwmsstatus', 1);
					}
					if(seriallocation != null && seriallocation != "")
						createSerialEntry.setFieldValue('custrecord_serial_location', seriallocation);
					if(serialbinlocation != null && serialbinlocation != "")
						createSerialEntry.setFieldValue('custrecord_serialbinlocation', serialbinlocation);

					//	alert('p. '+ p);

					var recid = nlapiSubmitRecord(createSerialEntry);
				}

				showInlineMessage(form, 'Confirmation', 'Serial Number(s) entered successfully.', null);
			}
			response.writePage(form);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception', exp);
		}

	}
}
