/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/UserEvents/Attic/ebiz_POValidations_UE.js,v $
*  $Revision: 1.1.4.3 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_POValidations_UE.js,v $
*  Revision 1.1.4.3  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function PODelete(type, form, request)
{	
	nlapiLogExecution('ERROR','Into PO Delete function',type);

	if(type ==  'delete')
	{
		//var status=nlapiGetFieldValue('status');		
		var PoId=nlapiGetFieldValue('id');
		nlapiLogExecution('ERROR','PoId ',PoId);
		var PODelStatusFlag=true;

		var OpenTaskfilters = new Array();

		OpenTaskfilters.push(new nlobjSearchFilter("custrecord_ebiz_cntrl_no", null, 'is', PoId));
		OpenTaskfilters.push(new nlobjSearchFilter("custrecord_tasktype", null, 'anyof', ['2','1']));
		OpenTaskfilters.push(new nlobjSearchFilter("custrecord_wms_status_flag", null, 'anyof', ['2','3','6','1']));

		var OpenTaskColumns = new Array();
		OpenTaskColumns[0] = new nlobjSearchColumn('custrecord_wms_status_flag'); 
		var OpenTaskSearchreults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskfilters, OpenTaskColumns);

		nlapiLogExecution('ERROR','OpenTaskSearchreults ',OpenTaskSearchreults);
		if(OpenTaskSearchreults != null && OpenTaskSearchreults != '')
		{
					
			nlapiLogExecution('ERROR','searchRec is not null so rec cannot  be deleted','');
			var ErrorMsg = nlapiCreateError('CannotDelete',' Order is being processed in Main Warehouse. Do not delete', true);
			throw ErrorMsg;
			
		}
		else
		{
			nlapiLogExecution('ERROR','searchRec is null so rec can be deleted ',OpenTaskSearchreults);
		}

		var ClosedTaskfilters = new Array();

		ClosedTaskfilters.push(new nlobjSearchFilter("custrecord_ebiztask_ebiz_cntrl_no", null, 'is', PoId));
		ClosedTaskfilters.push(new nlobjSearchFilter("custrecord_ebiztask_tasktype", null, 'anyof', [2]));
		ClosedTaskfilters.push(new nlobjSearchFilter("custrecord_ebiztask_wms_status_flag", null, 'anyof', [2,3]));

		var ClosedTaskColumns = new Array();
		ClosedTaskColumns[0] = new nlobjSearchColumn('custrecord_ebiztask_wms_status_flag'); 
		var ClosedTaskSearchreults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, ClosedTaskfilters, ClosedTaskColumns);

		if(ClosedTaskSearchreults != null && ClosedTaskSearchreults != '')
		{
			nlapiLogExecution('ERROR','searchRec is not null so rec cannot  be deleted','');
			var ErrorMsg = nlapiCreateError('CannotDelete','Order is being processed in Main Warehouse. Do not delete', true);
			throw ErrorMsg;
		}	
		else
		{
			nlapiLogExecution('ERROR','searchRec is null so rec can be deleted ',ClosedTaskSearchreults);
		}

	}

}