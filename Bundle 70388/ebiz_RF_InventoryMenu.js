/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMenu.js,v $
 *     	   $Revision: 1.4.4.8.4.5.2.11.2.1 $
 *     	   $Date: 2015/10/01 12:45:37 $
 *     	   $Author: schepuri $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_18 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_InventoryMenu.js,v $
 * Revision 1.4.4.8.4.5.2.11.2.1  2015/10/01 12:45:37  schepuri
 * case# 201414577
 *
 * Revision 1.4.4.8.4.5.2.11  2015/01/02 15:04:10  skreddy
 * Case# 201411349
 * Two step replen process
 *
 * Revision 1.4.4.8.4.5.2.10  2014/06/13 10:01:12  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.4.8.4.5.2.9  2014/06/12 15:27:49  skavuri
 * Case # 20148880 (LinkButton functionality added to options in RF Screens)
 *
 * Revision 1.4.4.8.4.5.2.8  2014/05/30 00:34:21  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.8.4.5.2.7  2014/04/10 15:51:08  nneelam
 * case#  20140649
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.8.4.5.2.6  2014/01/07 09:47:08  rmukkera
 * Case # 20126322
 *
 * Revision 1.4.4.8.4.5.2.5  2014/01/07 09:37:12  rmukkera
 * Case # 20126233
 *
 * Revision 1.4.4.8.4.5.2.4  2013/12/20 15:37:18  rmukkera
 * Case # 20126437
 *
 * Revision 1.4.4.8.4.5.2.3  2013/09/12 15:35:47  nneelam
 * Case#. 20124383���
 * Error msg display when no option selected  Issue Fix..
 *
 * Revision 1.4.4.8.4.5.2.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.4.8.4.5.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.4.4.8.4.5  2013/02/07 08:47:03  skreddy
 * CASE201112/CR201113/LOG201121
 *  RF Lot auto generating FIFO enhancement
 *
 * Revision 1.4.4.8.4.4  2012/10/05 06:34:12  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting Multiple language into given suggestions
 *
 * Revision 1.4.4.8.4.3  2012/09/28 10:41:06  grao
 * no message
 *
 * Revision 1.4.4.8.4.2  2012/09/26 12:34:15  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.4.4.8.4.1  2012/09/21 15:06:44  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.4.4.8  2012/09/14 09:21:48  grao
 * no message
 *
 * Revision 1.4.4.4  2012/08/14 15:09:48  spendyala
 * CASE201112/CR201113/LOG201121
 * Changed Create Inventory Label to Start Up Inventory.
 *
 * Revision 1.4.4.3  2012/07/12 14:47:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.4.4.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.4.1  2012/02/21 13:23:58  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.5  2012/02/21 11:42:54  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */
function InventoryMenu(request, response){
	if (request.getMethod() == 'GET') {
		
		
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
		
		
		var st0,st1,st2,st3,st4,st5,st6,st7,st8;
		
		if( getLanguage == 'es_ES')
		{
		
			st0 = "INVENTARIO DE MEN&#218;";
			st1 = "INVENTARIO C&#205;CLICO";
			st2 = "PUESTA EN MARCHA DE INVENTARIO";
			st3 = "REPOSICI&#211;N";
			st4 = "INVENTARIO DE MOVIMIENTO";
			st5 = "ACTUALIZACI&#211;N DEL C&#211;DIGO UPC";
			//Case # 20126233 Start
			st9 = "INGRESAR SELECCI&#211;N";
			st7 = "ENVIAR";
			st8 = "ANTERIOR";
			//Case # 20126437 Start
			st6="Buscar en el inventario";
			//Case # 20126437 End
			//Case # 20126233 End
			st10 = "TWO STEP REPLENISHMENT";

		}
		else
		{
			st0 = "INVENTORY MENU";
			st1 = "CYCLE COUNT";
			st2 = "START UP INVENTORY";
			st3 = "REPLENISHMENT";
			st4 = "INVENTORY MOVE";
			st5 = "UPDATE UPC CODE";
			//Case # 20126233� Start
			//st6 = "ENTER SELECTION";
			//Case # 20126233� End
			st6="INVENTORY SEARCH";
			st7 = "SEND";
			st8 = "PREV";
			//Case # 20126233� Start
			//st9="INVENTORY SEARCH";
			st9 = "ENTER SELECTION";
			st10 = "TWO STEP REPLENISHMENT";
			//st10="INVENTORY ADJUSTMENT"
		}

		//Case# 20148880 (LinkButton Functionality)
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di');
		var linkURL_1 = checkInURL_1; 
		
		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_rf_location_company', 'customdeploy_rf_location_company_di');
		var linkURL_2 = checkInURL_2; 
		var checkInURL_3 = nlapiResolveURL('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no');
		var linkURL_3 = checkInURL_3; 
		var checkInURL_4 = nlapiResolveURL('SUITELET', 'customscript_rf_inventory_move_main', 'customdeploy_rf_inventory_move_main_di');
		var linkURL_4 = checkInURL_4; 
		var checkInURL_5 = nlapiResolveURL('SUITELET', 'customscript_rf_upccode_plan_no', 'customdeploy_rf_upccode_plan_no_di');
		var linkURL_5 = checkInURL_5; 
		var checkInURL_6 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_inventory_search', 'customdeploy_ebiz_rf_inventory_search_di');
		var linkURL_6 = checkInURL_6; 
		// case# 201414577
		var checkInURL_7 = nlapiResolveURL('SUITELET', 'customscript_ebiz_twostepreplenmenu', 'customdeploy_ebiz_twostepreplenmenu');
		var linkURL_7 = checkInURL_7; 
		
		
		var functionkeyHtml=getFunctionkeyScript('_rfinventorymenu'); 
		var html = "<html><head><title>" + st0 +  "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfinventorymenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1. " + st1;
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 2. " + st2;
		html = html + "				<td align = 'left'> 2. <a href='" + linkURL_2 + "' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 3. " + st3;
		html = html + "				<td align = 'left'> 3. <a href='" + linkURL_3 + "' style='text-decoration: none'>" + st3 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 4. " + st4;
		html = html + "				<td align = 'left'> 4. <a href='" + linkURL_4 + "' style='text-decoration: none'>" + st4 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 5. " + st5;
		html = html + "				<td align = 'left'> 5. <a href='" + linkURL_5 + "' style='text-decoration: none'>" + st5 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 6. "+st6;
		html = html + "				<td align = 'left'> 6. <a href='" + linkURL_6 + "' style='text-decoration: none'>" + st6 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";	
		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'> 7. "+st10;
		html = html + "				</td>";
		html = html + "			</tr> ";	*/
		
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 6. "+st6;
		html = html + "				<td align = 'left'> 7. <a href='" + linkURL_7 + "' style='text-decoration: none'>" + st10 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";	
		
		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"  + st9;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st7 + "<input id='cmdSend' type='submit' value='ENT'/>";
		html = html + "				"	+ st8 + "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		// This variable is to get the option selected. If option selected is 1, it navigates to
		// the check-in screen. If the option selected is 2, it navigates to the putaway screen.
		var optedField = request.getParameter('selectoption');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		//Case # 20126437 Start
		var getLanguage = request.getParameter('hdngetLanguage');
		//Case # 20126437 End
		 //case # 20124383� Start  
		var st11,st12;
		if( getLanguage == 'es_ES')
		{
			st11 = "OPCI&#211;N V&#193;LIDA";
			st12 = "InventoryMenu";
		}
		else
		{
			st11 = "INVALID OPTION";
			st12 = "InventoryMenu";
		}
	
		 //case # 20124383� End
		
		

		var POarray = new Array();  
	
		POarray["custparam_language"] = getLanguage;
		
		 //case # 20124383� Start  
		POarray["custparam_error"] = st11;
		POarray["custparam_screenno"] = st12;
		 //case # 20124383� End
		
		nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);
		
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, POarray);
		}
		else 
			//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
			//            if (optedEvent != '' && optedEvent != null) {
			if (optedField == '1') {
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di', false, POarray);
			}
			else 
				if (optedField == '2') {
//					response.sendRedirect('SUITELET', 'customscript_rf_create_invt_binloc', 'customdeploy_rf_create_invt_binloc_di', false, optedField);
					response.sendRedirect('SUITELET', 'customscript_rf_location_company', 'customdeploy_rf_location_company_di', false, POarray);
				}
				else 
					if (optedField == '3') {
						response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false, POarray);
						//response.sendRedirect('SUITELET', 'customscript_rf_replenishmentmenu', 'customdeploy_rf_replenishmentmenu', false, optedField);

					}
					else 
						if (optedField == '4') {
							//response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false, optedField);
							response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_main', 'customdeploy_rf_inventory_move_main_di', false, POarray);

						}
						else 
							if (optedField == '5') {
								//response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false, optedField);
								response.sendRedirect('SUITELET', 'customscript_rf_upccode_plan_no', 'customdeploy_rf_upccode_plan_no_di', false, POarray);

							}
							else
								if (optedField == '6') {
									//response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false, optedField);
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_inventory_search', 'customdeploy_ebiz_rf_inventory_search_di', false, optedField);

							}	
								else
									if (optedField == '7') {
										response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenmenu', 'customdeploy_ebiz_twostepreplenmenu', false, optedField);

									}	
		//case # 20124383? Start  
									else{
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									}
		//case # 20124383� End
							/*	else
									if (optedField == '7') {
									//	response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false, optedField);
									//	response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_invtadjustment', 'customdeploy_ebiz_rf_wo_invtadjustment', false, optedField);

									}*/
		 //case # 20124383? Start  
								/*else{
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								}*/
		 //case # 20124383� End
		
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
