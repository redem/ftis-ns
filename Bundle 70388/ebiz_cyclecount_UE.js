/*************************************************************************
	  		  eBizNET Solutions Inc .
 ****************************************************************************
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/UserEvents/ebiz_cyclecount_UE.js,v $
 *     	   $Revision: 1.2.2.2.4.1.4.2 $
 *     	   $Date: 2013/09/11 15:23:51 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_cyclecount_UE.js,v $
 * Revision 1.2.2.2.4.1.4.2  2013/09/11 15:23:51  rmukkera
 * Case# 20124376
 *
 * Revision 1.2.2.2.4.1.4.1  2013/09/11 12:03:35  rmukkera
 * Case# 20124374
 *
 * Revision 1.2.2.2.4.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.2  2012/03/27 23:40:20  gkalla
 * CASE201112/CR201113/LOG201121
 * To restrict the user not to delete the plan after processed
 *
 * Revision 1.2  2012/03/27 23:33:11  gkalla
 * CASE201112/CR201113/LOG201121
 * To restrict the user not to delete the plan after processed
 *
 * Revision 1.1  2012/03/27 15:49:42  gkalla
 * CASE201112/CR201113/LOG201121
 * To restrict the user not to delete the plan after processed
 * 
 * 
 *
 *****************************************************************************/
function CustomRecordsBeforeSubmit(type, form, request){ 

	nlapiLogExecution('ERROR','type',type);
	 
	
	// Trap only the 'delete' event
	if(type == 'delete'){
		// Further check to ensure that only requests from the user interface and userevent
		// are caught
		var CycleCountPlan=nlapiGetFieldValue('id');
		nlapiLogExecution('ERROR','CycleCountPlan',CycleCountPlan);
		var CycleCountExecutionFilter=new Array();
		CycleCountExecutionFilter.push(new nlobjSearchFilter('custrecord_cycle_count_plan',null,'equalto',parseFloat(CycleCountPlan)));
		CycleCountExecutionFilter.push(new nlobjSearchFilter('custrecord_cyclestatus_flag',null,'anyof',[20,22,31]));//20=FLAG.INVENTORY.CYCLE_COUNT.RELEASE;22=FLAG.INVENTORY.CYCLE_COUNT.RESOLVE;31=FLAG.INVENTORY.CYCLE_COUNT.RECORD   

		var CycleCountExecutionRec=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe',null,CycleCountExecutionFilter,null);
		if(CycleCountExecutionRec!=null)
		{
			nlapiLogExecution('ERROR', 'Creating Error Object', 'Creating Error Object');
			nlapiLogExecution('ERROR','searchRec is not null so rec can be deleted','');
			var cannotDelError = nlapiCreateError('CannotDelete',
					'You cannot delete cycle count plan# ' + CycleCountPlan + ' as it has associate transactions.', true);
			throw cannotDelError;  // Netsuite Easter Egg: throw this error object, do not (never) catch it
		}
	}
}
