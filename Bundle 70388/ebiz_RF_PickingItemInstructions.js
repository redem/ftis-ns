/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_PickingItemInstructions.js,v $ 
 *     	   $Revision: 1.1.2.3.2.1 $
 *     	   $Date: 2015/07/02 13:56:49 $
 *     	   $Author: schepuri $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_172 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *
 *****************************************************************************/
function PickingItemInstuctions(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8;

		if( getLanguage == 'es_ES')
		{
			st1 = "ART&#205;CULO:";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULO";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "OVERRIDE";
			st6 = "SKIP";
			st8 ="DESCRIPCI&#211;N DEL ART&#205;CULO";
		}
		else
		{
			st1 = "ITEM: ";
			st2 = "ENTER/SCAN ITEM ";
			st3 = "CONT";
			st4 = "PREV";
			st5 = "OVERRIDE";
			st6 = "SKIP";
			st8 = "ITEM INSTRUCTIONS : ";
		}

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var vClusterno = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');

		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		var ItemInstructions = request.getParameter('custparam_itemInstructions');
		var serialscanned = request.getParameter('custparam_serialscanned');
		nlapiLogExecution('ERROR', 'pickType', pickType);
		nlapiLogExecution('ERROR', 'getFetchedLocation', getFetchedLocation);
		if(pickType !='CL')
		{
			getFetchedLocation = request.getParameter('custparam_beginLocationname');
		}
		else
		{
			getFetchedLocation = request.getParameter('custparam_beginlocationname');
		}
		nlapiLogExecution('ERROR', 'getnextExpectedQuantity', getnextExpectedQuantity);
		nlapiLogExecution('ERROR', 'getBeginLocation', NextLocation);
		nlapiLogExecution('ERROR', 'NextItemId', NextItemId);
		nlapiLogExecution('ERROR', 'getFetchedLocation', getFetchedLocation);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		if(getFetchedLocation==null)
		{
			getFetchedLocation=NextLocation;
		}
		if(getItemInternalId==null)
		{
			getItemInternalId=NextItemId;
		}

		var Itemdescription='';
		var ItemInstructions='';
		
		if(getItemInternalId !=null && getItemInternalId!='')
		{
			var Itemtype = nlapiLookupField('item', getItemInternalId, 'recordType');

			var filtersitem = new Array();
			var columnsitem = new Array();

			filtersitem.push(new nlobjSearchFilter('internalid', null, 'is',getItemInternalId));
			filtersitem.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

			columnsitem[0] = new nlobjSearchColumn('description');    
			columnsitem[1] = new nlobjSearchColumn('salesdescription');
			columnsitem[2] = new nlobjSearchColumn('itemid');
			columnsitem[3] = new nlobjSearchColumn('custitem_ebizmodelno');

			var itemRecord = nlapiSearchRecord(Itemtype, null, filtersitem, columnsitem);
			if(itemRecord!=null && itemRecord!='' && itemRecord.length>0)
			{
				if(itemRecord[0].getValue('description') != null && itemRecord[0].getValue('description') != '')
				{
					Itemdescription = itemRecord[0].getValue('description');
				}
				else if(itemRecord[0].getValue('salesdescription') != null && itemRecord[0].getValue('salesdescription') != "")
				{	
					Itemdescription = itemRecord[0].getValue('salesdescription');
				}
				getItemName=itemRecord[0].getValue('itemid');
				ItemInstructions =itemRecord[0].getValue('custitem_ebizmodelno');
			}

			Itemdescription = Itemdescription.substring(0, 40);
			if(ItemInstructions !=null && ItemInstructions !='')
			ItemInstructions = ItemInstructions.substring(0, 30);
		}
		

		nlapiLogExecution('ERROR', 'getItemName', getItemName);	
		nlapiLogExecution('ERROR', 'ItemInstructions', ItemInstructions);
		

		
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');

		nlapiLogExecution('ERROR', 'getItem', getItem);	
		nlapiLogExecution('ERROR', 'Itemdescription', Itemdescription);
		nlapiLogExecution('ERROR', 'Next Location', NextLocation);
		nlapiLogExecution('ERROR', 'getOrderNo', getOrderNo);

		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getFetchedBeginLocation);

		//  var getFetchedLocation = BinLocationRec.getFieldValue('custrecord_ebizlocname');
		//   nlapiLogExecution('DEBUG', 'Location Name is', getFetchedLocation);

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		html = html + " document.getElementById('enteritem').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		//html = html + "	  alert(node.type);";
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>FO#:<label>" + name + "</label> ## WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";		
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getItemName + "</label><br>"+ st8 +"<label>" + ItemInstructions + "</label>";//<br>REMAINING QTY: <label>" + remqty + "</label>";

		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterno + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value='" + vBatchno + "'>";// case# 201413320
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationid' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnItemInstructions' value='" + ItemInstructions + "'>";
		html = html + "				<input type='hidden' name='hdnserialscanned' value=" + serialscanned + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"; 
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnnextitem' value=" + NextItemId + ">";	
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";	
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='F8' onclick='this.form.submit();this.focus();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdOverride.disabled=true; this.form.cmdSKIP.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					"+ st5 +" <input name='cmdOverride' type='submit' value='F11'/>";
		//html = html + "					"+ st6 +" <input name='cmdSKIP' type='submit' value='F12'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st7,st8;

		if( getLanguage == 'es_ES')
		{
			st7 = "ART&#205;CULO INV&#193;LIDO";
			st8 = "DESCRIPCI&#211;N DEL ART&#205;CULO:";

		}
		else
		{
			st7 = "INVALID ITEM";
			st8 = "ITEM DESCRIPTION : ";
		}
		var getEnteredItem = request.getParameter('enteritem');
		nlapiLogExecution('ERROR', 'Entered Item', getEnteredItem);
		nlapiLogExecution('ERROR', 'hdnnext', request.getParameter('hdnnext'));
		nlapiLogExecution('ERROR', 'hdnnextItemId', request.getParameter('hdnnextitem'));

		var getWaveNo = request.getParameter('hdnWaveNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getBeginLocationid = request.getParameter('hdnBeginLocationid');
		var getItem = request.getParameter('hdnItem');
		var getItemName = request.getParameter('hdnItemName');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		nlapiLogExecution('ERROR', 'hdnItemName', getItemName);
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');	
		var whLocation = request.getParameter('hdnwhlocation');
		var vZoneId=request.getParameter('hdnebizzoneno');
		var nextexpqty=request.getParameter('hdnextExpectedQuantity');
		var whCompany = request.getParameter('hdnwhCompany');
		var RecCount=request.getParameter('hdnRecCount');
		var getNext=request.getParameter('hdnnext');
		var getNextItemId=request.getParameter('hdnnextitem');
		var OrdName=request.getParameter('hdnName');
		var ContainerSize=request.getParameter('hdnContainerSize');
		var ebizOrdNo=request.getParameter('hdnebizOrdNo');

		nlapiLogExecution('ERROR', 'Name', OrdName);
		nlapiLogExecution('ERROR', 'getNext', getNext);
		nlapiLogExecution('ERROR', 'getBeginLocation', getBeginLocation);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optsend = request.getParameter('cmdSend');
		var pickType=request.getParameter('hdnpicktype');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_error"] = st7;//'INVALID ITEM';
		SOarray["custparam_screenno"] = '14A';
		SOarray["custparam_whlocation"] = whLocation;
		SOarray["custparam_waveno"] = getWaveNo;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_beginLocation"] = getBeginLocation;
		SOarray["custparam_beginLocationname"] = getBeginLocation;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemname"] = getItemName;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_dolineid"] = getDOLineId;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_batchno"] = vBatchNo;
		SOarray["custparam_noofrecords"] = RecCount;
		SOarray["custparam_nextlocation"] = getNext;
		SOarray["custparam_nextiteminternalid"] = getNextItemId;
		SOarray["name"] = OrdName;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = ebizOrdNo;
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_nextexpectedquantity"] = nextexpqty;
		SOarray["custparam_Actbatchno"] = "";
		SOarray["custparam_Expbatchno"] = "";
		SOarray["custparam_itemInstructions"] = request.getParameter('hdnItemInstructions');
		SOarray["custparam_serialscanned"]='';
		SOarray["custparam_prevscreen"] = request.getParameter('custparam_prevscreen');
		SOarray["custparam_serialscanned"] = request.getParameter('hdnserialscanned');
		SOarray["custparam_detailtask"] = request.getParameter('custparam_detailtask');
		
		if(pickType !='CL')
		{
			SOarray["custparam_beginLocationname"] = getBeginLocation;
		}
		else
		{
			SOarray["custparam_beginlocationname"] = getBeginLocation;
		}
	
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		nlapiLogExecution('DEBUG', 'Order LineNo is', getOrderLineNo);

		nlapiLogExecution('ERROR', 'getEnteredItem',getEnteredItem);
		nlapiLogExecution('ERROR', 'getItemName',getItemName);
		nlapiLogExecution('ERROR', 'getItem',getItem);

		var str = 'getEnteredItem. = ' + getEnteredItem + '<br>';			
		str = str + 'getItemName. = ' + getItemName + '<br>';
		str = str + 'getItem. = ' + getItem + '<br>';
		str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';
		str = str + 'optsend. = ' + optsend + '<br>';

		nlapiLogExecution('ERROR', 'Item Details1', str);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {

			if(pickType !='CL')
			{

				response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
			}
		}
		else 
		{
			var batchflag='';
			var ItemType='';
			var ItemSpecialInstr='';
			if(getItemInternalId !='' && getItemInternalId !=null)
			{
				var ItemTypeRec = nlapiLookupField('item', getItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot','custitem_ebizmodelno']);				
				batchflag = ItemTypeRec.custitem_ebizbatchlot;
				ItemType = ItemTypeRec.recordType;
				ItemSpecialInstr = ItemTypeRec.custitem_ebizmodelno;
			}

			if(pickType !='CL')
			{
				if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
				{
					SOarray["custparam_itemType"] ='lotnumberedinventoryitem';
					response.sendRedirect('SUITELET', 'customscript_rf_picking_batch', 'customdeploy_rf_picking_batch_di', false, SOarray);					  
				}
				else {

					response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
			}
			else
			{
				if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
				{
					response.sendRedirect('SUITELET', 'customscript_rf_picking_batch', 'customdeploy_rf_picking_batch_di', false, SOarray);
				}
				else
				{
					nlapiLogExecution('ERROR', 'SOarray["custparam_beginLocation"] ', SOarray["custparam_beginLocation"] );
					nlapiLogExecution('ERROR', 'Navigating to Summary Task');
					//SOarray["custparam_detailtask"] = detailTask;
					response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
				}
			}

		}

	}
}
