/***************************************************************************
 eBizNET Solutions               
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_ClusPickingLocation.js,v $
 *     	   $Revision: 1.1.4.3 $
 *     	   $Date: 2015/04/21 13:42:44 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_ClusPickingLocation.js,v $
 * Revision 1.1.4.3  2015/04/21 13:42:44  schepuri
 * case# 201412444
 *
 * Revision 1.1.4.2  2015/03/02 14:46:39  snimmakayala
 * 201410541
 *
 * Revision 1.1.2.1  2015/02/13 14:34:38  sponnaganti
 * Jawbone Cluster Picking CR Changes
 * CR#  CR14US0323
 *
 * Revision 1.3.2.10.4.5.2.17  2014/04/23 15:52:37  skavuri
 * Case# 20148127 issue fixed
 *
 * Revision 1.3.2.10.4.5.2.16  2014/04/16 07:54:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to20140026
 *
 * Revision 1.3.2.10.4.5.2.15  2014/02/11 15:30:25  skreddy
 * case 20126958
 * Standard Bundle issue fix
 *
 * Revision 1.3.2.10.4.5.2.14  2014/01/13 13:56:33  schepuri
 * 20126737
 * Standard bundle issue fix
 *
 * Revision 1.3.2.10.4.5.2.13  2013/11/22 14:48:40  grao
 * Case# 20125872  related issue fixes in SB 2014.1
 *
 * Revision 1.3.2.10.4.5.2.12  2013/10/23 10:13:25  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related case#201217053 was fixed
 *
 * Revision 1.3.2.10.4.5.2.11  2013/09/27 16:17:17  skreddy
 * Case# 20124612
 * nucourse issue fix
 *
 * Revision 1.3.2.10.4.5.2.10  2013/08/01 06:31:18  grao
 * Case# 20123668
 * TSG SB :Qty Mismatching related issues fixes
 *
 * Revision 1.3.2.10.4.5.2.9  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.3.2.10.4.5.2.8  2013/04/30 18:45:25  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.3.2.10.4.5.2.7  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.2.10.4.5.2.6  2013/04/04 20:20:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixes
 *
 * Revision 1.3.2.10.4.5.2.5  2013/04/03 02:04:51  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.3.2.10.4.5.2.4  2013/04/02 14:15:06  rmukkera
 * issue Fix related to fetching the location paramater
 *
 * Revision 1.3.2.10.4.5.2.3  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.3.2.10.4.5.2.2  2013/03/13 13:57:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.3.2.10.4.5.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.3.2.10.4.5  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.3.2.10.4.4  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.2.10.4.3  2012/10/04 10:31:18  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.3.2.10.4.2  2012/09/26 12:28:38  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.3.2.10.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.3.2.10  2012/09/04 20:46:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Incorporate SKIP option in Cluster Picking.
 *
 * Revision 1.3.2.9  2012/08/14 06:43:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Validate the Location enter by the user.
 *
 * Revision 1.3.2.8  2012/08/09 07:30:17  spendyala
 * CASE201112/CR201113/LOG201121
 * Driving user to new screen i.e cluster summary report.
 *
 * Revision 1.3.2.7  2012/08/07 12:00:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing Parameter to query string.
 *
 * Revision 1.3.2.6  2012/04/27 13:15:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Cluster picking
 *
 * Revision 1.3.2.5  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.2.4  2012/03/12 08:05:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.5  2012/03/12 07:56:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.4  2012/02/28 01:22:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.3  2012/02/23 18:19:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.2  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.1  2012/02/23 13:08:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.14  2012/02/21 15:04:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.13  2012/02/20 14:25:46  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Loc Exception
 *
 * Revision 1.12  2012/02/17 13:31:34  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.11  2012/01/23 23:21:17  gkalla
 * CASE201112/CR201113/LOG201121
 * RF Picking customized
 *
 * Revision 1.10  2011/12/16 12:30:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/12/12 12:58:54  schepuri
 * CASE201112/CR201113/LOG201121
 * adding new button for RF PICK Exception
 *
 * Revision 1.7  2011/09/29 16:17:23  gkalla
 * CASE201112/CR201113/LOG201121
 * To integrate Serial no entry in RF outbound
 *
 * Revision 1.6  2011/09/10 07:24:50  skota
 * CASE201112/CR201113/LOG201121
 * code changes to redirect to right suitelet for capturing serial nos
 *
 * Revision 1.5  2011/09/09 18:08:24  skota
 * CASE201112/CR201113/LOG201121
 * If condition change to consider serial out flag
 *
 * 
 *****************************************************************************/
function WOClusterPickingLocation(request, response){
	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR A PUNTO DE: ";
			st2 = "INGRESAR / ESCANEAR UBICACI&#211;N";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";



		}
		else
		{
			st1 = "GO TO LOCATION: ";
			st2 = "ENTER/SCAN LOCATION ";
			st3 = "SEND";
			st4 = "PREV";

		}


		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');		
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginlocationname');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchNo = request.getParameter('custparam_batchno');	
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_nooflocrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var NextItem=request.getParameter('custparam_nextitem');
		var pickType=request.getParameter('custparam_picktype');
		var OrdNo=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var vSOId=request.getParameter('custparam_ebizordno');
		nlapiLogExecution('DEBUG', 'getFetchedLocation', getFetchedLocation);
		if(getFetchedLocation==null || getFetchedLocation=='')
		{
			getFetchedLocation=NextLocation;
		}

		if(getItemInternalId==null || getItemInternalId=='')
		{
			getItemInternalId=NextItemInternalId;
		}

		var venterzone=request.getParameter('custparam_venterzone');
		var str = 'getWaveno. = ' + getWaveno + '<br>';
		str = str + 'vClusterNo. = ' + vClusterNo + '<br>';		
		str = str + 'getFetchedBeginLocation. = ' + getFetchedBeginLocation + '<br>';	
		str = str + 'getFetchedLocation. = ' + getFetchedLocation + '<br>';	
		str = str + 'NextItemInternalId. = ' + NextItemInternalId + '<br>';	
		str = str + 'NextItem. = ' + NextItem + '<br>';	

		nlapiLogExecution('DEBUG', 'Parameter Details', str);

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('enterlocation').focus();";   

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getFetchedLocation + "</label>";

		html = html + "				<input type='hidden' name='hdnlocation' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationname' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo  + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + vBatchNo  + ">";	
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";	
		html = html + "				<input type='hidden' name='hdnOrdNo' value=" + OrdNo + ">";
		html = html + "				<input type='hidden' name='hdnsoid' value=" + vSOId + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";	
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnenterzone' value=" + venterzone + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +" ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					OVERRIDE <input name='cmdOverride' type='submit' value='F11'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');	


		var getLanguage = request.getParameter('hdngetLanguage');

		var st5;

		if( getLanguage == 'es_ES')
		{

			st5 = "UBICACI&#211;N NO V&#193;LIDA";
		}
		else
		{
			st5 = "INVALID LOCATION";
		}

		var picktype = request.getParameter('hdnpicktype');
		var getEnteredLocation = request.getParameter('enterlocation');
		//code added by santosh on 7Aug2012
		var Type=request.getParameter('hdntype');
		//end of the code on 7Aug2012

		var getBeginLocation='';	
		var WaveNo=request.getParameter('hdnWaveNo');
		var OrdNo=request.getParameter('hdnOrdNo');
		var ClusNo = request.getParameter('hdnClusterNo');
		var NextShowLocation = request.getParameter('hdnBeginLocationname');
		var NextShowItemId = request.getParameter('hdnItemInternalId');
		var vSOId = request.getParameter('hdnsoid');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var venterzone = request.getParameter('hdnenterzone');
		var vitemid='';
		var vbinlocid='';

		var str = 'WaveNo. = ' + WaveNo + '<br>';
		str = str + 'ClusNo. = ' + ClusNo + '<br>';		
		str = str + 'OrdNo. = ' + OrdNo + '<br>';	
		str = str + 'NextShowItemId. = ' + NextShowItemId + '<br>';	
		str = str + 'NextShowLocation. = ' + NextShowLocation + '<br>';	
		str = str + 'picktype. = ' + picktype + '<br>';
		str = str + 'getRecordInternalId. = ' + getRecordInternalId + '<br>';
		str = str + 'venterzone. = ' + venterzone + '<br>';

		nlapiLogExecution('DEBUG', 'Parameter Details', str);

		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_ebizordno"] = vSOId;
		SOarray["custparam_venterzone"]=request.getParameter('hdnenterzone');
		var RecCount;

		var SOFilters = new Array();

		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
		SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
		SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
		if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
		{
			nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);

			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
		}
		if(WaveNo != null && WaveNo != "")
		{
			nlapiLogExecution('DEBUG', 'WaveNo inside If', WaveNo);

			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
		}


		//SOFilters.push(new nlobjSearchFilter('internalid', null, 'is', getRecordInternalId));

		if(venterzone!=null && venterzone!="" && venterzone!='null')
		{
			nlapiLogExecution('DEBUG', 'getZoneNo inside if', venterzone);
			nlapiLogExecution('DEBUG', 'vZoneId inside if', venterzone);
			SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));

		}

		var SOColumns = new Array();

		SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
		SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
		SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
		SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
		SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
		SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
		SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
		SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
		SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
		SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
		SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
		SOColumns[11] = new nlobjSearchColumn('custrecord_line_no',null,'group');
		SOColumns[12] = new nlobjSearchColumn('internalid',null,'group');
		SOColumns[13] = new nlobjSearchColumn('name',null,'group');
		SOColumns[14] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
		SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no',null,'group');
		SOColumns[16] = new nlobjSearchColumn('custrecord_invref_no',null,'group');
		//SOColumns[10] = new nlobjSearchColumn('custrecord_container',null,'group');
		//SOColumns[11] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');


		SOColumns[0].setSort(); 	//Location Pick Sequence// case# 201412444
		SOColumns[1].setSort();		//SKU
		SOColumns[2].setSort();		//Location
		//SOColumns[11].setSort();		//Container LP

		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

		if (SOSearchResults != null && SOSearchResults.length > 0) {
			nlapiLogExecution('DEBUG', 'SearchResults Length', SOSearchResults.length);
			var SOSearchResult = SOSearchResults[0];

			if(SOSearchResults.length > 1)
			{
				var SOSearchnextResult = SOSearchResults[1];
				SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
				SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no',null,'group');
				SOarray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');
				nlapiLogExecution('DEBUG', 'Next Location', SOarray["custparam_nextlocation"]);
			}

			SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku',null,'group');
			SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
			SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');	
			SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc',null,'group');
			SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
			SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
			SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id',null,'group');			
			SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no',null,'group');
			SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no',null,'group');
			SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
			SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no',null,'group');
			SOarray["custparam_nooflocrecords"] = SOSearchResults.length;
			SOarray["custparam_recordinternalid"] =  SOSearchResult.getValue('internalid',null,'group');
			//SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container',null,'group');
			//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no',null,'group');
			SOarray["custparam_ebizordno"] =  request.getParameter('hdnsoid');
			SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
			SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
			//SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
			SOarray["custparam_batchno"] = request.getParameter('hdnBatchNo');	
			//SOarray["name"] =  request.getParameter('hdnsoid');
			SOarray["name"] = OrdNo;
			SOarray["custparam_recordinternalid"] =  request.getParameter('hdnRecordInternalId');
			SOarray["custparam_containersize"] =  request.getParameter('hdnContainerSize');
			SOarray["custparam_containerlpno"] =  request.getParameter('hdnContainerLpNo');

			nlapiLogExecution('DEBUG', 'SOSearchResult.getValue(custrecord_ebiz_sku_no,null,group)', SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group'));
			getBeginLocation = SOarray["custparam_beginlocationname"];

			vitemid = SOarray["custparam_item"];
			vbinlocid = SOarray["custparam_beginLocation"];
		}		
		getBeginLocation = request.getParameter('hdnlocation');
		var str = 'Entered Location. = ' + getEnteredLocation + '<br>';
		str = str + 'Location. = ' + getBeginLocation + '<br>';	

		nlapiLogExecution('DEBUG', 'Location Details', str);	

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');


		SOarray["custparam_error"] = st5;//'INVALID LOCATION';
		SOarray["custparam_screenno"] = 'CLWO2';
		SOarray["custparam_type"] ="Cluster";//santosh

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
			var prevscreen='';
			nlapiLogExecution('DEBUG', 'SOarray["custparam_prevscreen"]', request.getParameter('custparam_prevscreen'));
			if(request.getParameter('custparam_prevscreen')!=null && request.getParameter('custparam_prevscreen')!='')
			{
				prevscreen=request.getParameter('custparam_prevscreen');
			}
			nlapiLogExecution('DEBUG', 'prevscreen', prevscreen);
			if(prevscreen=='ItemScan')
			{
				response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
			}
			else if(prevscreen=='summtask')
			{
				response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_summtask', 'customdeploy_rf_wocluspicking_summtask', false, SOarray);
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_wopicking_cluster_no', 'customdeploy_rf_wopicking_cluster_no', false, SOarray);
			}
		}

		else 
		{
			if(getEnteredLocation != null && getEnteredLocation != ''){
				if (getEnteredLocation.toUpperCase() == getBeginLocation.toUpperCase()) {

					var getEndLocationInternalId='';
					var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getEnteredLocation));

					nlapiLogExecution('DEBUG', 'Length of Location Search', LocationSearch.length);

					if (LocationSearch.length != 0) {
						for (var s = 0; s < LocationSearch.length; s++) {
							getEndLocationInternalId = LocationSearch[s].getId();
						}

						nlapiLogExecution('DEBUG', 'End Location Id', getEndLocationInternalId);

						updatetaskbegintime(WaveNo,ClusNo,getEndLocationInternalId,vitemid);					

						SOarray["custparam_beginlocationname"] = getBeginLocation;
						SOarray["custparam_endlocinternalid"] = getEndLocationInternalId;
						SOarray["custparam_endlocation"] = getBeginLocation;
						SOarray["custparam_clusterno"] = vClusterNo;					 
						nlapiLogExecution('Debug', 'getBeginLocation',SOarray["custparam_beginLocation"]);
						var getBeginLocationid=	SOarray["custparam_beginLocation"];
						var wave=request.getParameter('hdnWaveNo');
						SOarray["custparam_remqty"]=0;

						//Code added by Suman on 011013
						//Instead of checking weather the item has qty in pick face location and prompting user
						//to confirm replenishment at the end of the transaction .We are checking here only.
						var invtqty=0;

						var IsPickFaceLoc = 'F';
						var ItemInternalId=SOarray["custparam_iteminternalid"];
						var EndLocInternalId=getEndLocationInternalId;
						var ExpectedQuantity=SOarray["custparam_expectedquantity"];
						var InvoiceRefNo=SOarray["custparam_invoicerefno"];
						var RecordInternalId=SOarray["custparam_recordinternalid"];
						var ClusNo=SOarray["custparam_clusterno"];
						var OrdName=SOarray["name"];
						var ebizOrdNo=SOarray["custparam_ebizordno"];
						var getZoneNo=SOarray["custparam_ebizzoneno"];
						var whLocation=SOarray["custparam_whlocation"];
						nlapiLogExecution('Debug', 'ItemInternalId to pf', ItemInternalId);
						nlapiLogExecution('Debug', 'SOarray["custparam_iteminternalid"]', SOarray["custparam_iteminternalid"]);
						IsPickFaceLoc = isPickFaceLocationNew(ItemInternalId,EndLocInternalId);
						nlapiLogExecution('Debug', 'ItemInternalId', ItemInternalId);
						nlapiLogExecution('Debug', 'EndLocInternalId', EndLocInternalId);
						nlapiLogExecution('Debug', 'ExpectedQuantity', ExpectedQuantity);
						nlapiLogExecution('Debug', 'IsPickFaceLoc', IsPickFaceLoc);
						nlapiLogExecution('Debug', 'InvoiceRefNo', InvoiceRefNo);
						nlapiLogExecution('Debug', 'RecordInternalId', RecordInternalId);
						nlapiLogExecution('Debug', 'WaveNo', WaveNo);
						nlapiLogExecution('Debug', 'ClusNo', ClusNo);
						nlapiLogExecution('Debug', 'OrdName', OrdName);
						nlapiLogExecution('Debug', 'ebizOrdNo', ebizOrdNo);
						nlapiLogExecution('Debug', 'whLocation', whLocation);
						var invtqty=0;
						var openreplns = new Array();
						if(IsPickFaceLoc=='T')
						{
							openreplns=getOpenReplns(ItemInternalId,EndLocInternalId);
							var openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemInternalId);
							if(openinventory!=null && openinventory!='')
							{//case 20126958ï¿½ added for loop condition
								for(var x=0; x< openinventory.length;x++)
								{
//									var vinvtqty=openinventory[x].getValue('custrecord_ebiz_alloc_qty',null,'sum');
									// Case# 20148127 starts
									//var invtqty=openinventory[0].getValue('custrecord_ebiz_qoh',null,'sum');
									var vinvtqty=openinventory[0].getValue('custrecord_ebiz_qoh',null,'sum');
									// Case# 20148127 ends
									if(vinvtqty==null || vinvtqty=='' || isNaN(vinvtqty))
									{
										vinvtqty=0;
									}

									invtqty=parseInt(invtqty)+parseInt(vinvtqty);
								}
							}
						}

						nlapiLogExecution('Debug', 'invtqty', invtqty);
						if(invtqty==null || invtqty=='' || isNaN(invtqty))
						{
							invtqty=0;
						}
						nlapiLogExecution('ERROR', 'ExpectedQuantity', ExpectedQuantity);
						nlapiLogExecution('ERROR', 'invtqty', invtqty);

						if(IsPickFaceLoc=='T' && (parseInt(ExpectedQuantity)>parseInt(invtqty)) && (openreplns!=null && openreplns!=''))
						{
							for(var i=0; i< openreplns.length;i++)
							{
								var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
								var expqty=openreplns[i].getValue('custrecord_expe_qty');
								if(expqty == null || expqty == '')
								{
									expqty=0;
								}	
								nlapiLogExecution('Debug', 'open repln qty',expqty);
								nlapiLogExecution('Debug', 'getQuantity',ExpectedQuantity);

								var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
								if(skiptask==null || skiptask=='')
								{
									skiptask=1;
								}
								else
								{
									skiptask=parseInt(skiptask)+1;
								}

								nlapiLogExecution('Debug', 'skiptask',skiptask);

								nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

								var vPickType = request.getParameter('hdnpicktype');
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');

							}

							var SOarray=new Array();
							// case no 20126737

							//SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);
							SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo);
							SOarray["custparam_skipid"] = skiptask;
							if(NextShowLocation!=null && NextShowLocation!='')
								SOarray["custparam_screenno"] = 'CL4EXP';
							else
								SOarray["custparam_screenno"] = 'CL4LOCEXP';	
							SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;

						}				
						else
						{
							if(IsPickFaceLoc=='T' && (InvoiceRefNo==null || InvoiceRefNo==''))
							{
								if(openinventory!=null && openinventory!='')
								{
									var openinvtrecid = openinventory[0].getId();
									nlapiLogExecution('Debug', 'openinvtrecid', openinvtrecid);

									try
									{
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_invref_no', openinvtrecid);
									}
									catch(exp)
									{
										nlapiLogExecution('Debug', 'Exception in updating inventory reference number', exp);
									}
								}
							}
						}
						//End of code as on 011013
						nlapiLogExecution('Error', 'Navigating to Item Screen');
						response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);					

					}
					else {
						SOarray["custparam_beginlocationname"]=NextShowLocation;
						SOarray["custparam_nextlocation"]=NextShowLocation;	
						SOarray["custparam_nextiteminternalid"]=NextShowItemId;	
						nlapiLogExecution('DEBUG', 'NextShowLocation : ', NextShowLocation);
						nlapiLogExecution('DEBUG', 'Error: ', 'Scanned a wrong location');
						nlapiLogExecution('DEBUG', 'NextShowItemId : ', NextShowItemId);
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

					}
				}
				else if (getEnteredLocation != '' && getEnteredLocation.toUpperCase() != getBeginLocation.toUpperCase()) {

					SOarray["custparam_beginlocationname"]=NextShowLocation;
					SOarray["custparam_nextlocation"]=NextShowLocation;	
					SOarray["custparam_nextiteminternalid"]=NextShowItemId;
					nlapiLogExecution('DEBUG', 'NextShowLocation : ', NextShowLocation);
					nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the location');
					nlapiLogExecution('DEBUG', 'NextShowItemId : ', NextShowItemId);
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);				
				}
				else 
				{
					SOarray["custparam_beginlocationname"]=NextShowLocation;
					SOarray["custparam_nextlocation"]=NextShowLocation;	
					SOarray["custparam_nextiteminternalid"]=NextShowItemId;
					nlapiLogExecution('DEBUG', 'NextShowLocation : ', NextShowLocation);
					nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the location');
					nlapiLogExecution('DEBUG', 'NextShowItemId : ', NextShowItemId);
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);				
				}
			}
			else 
			{
				SOarray["custparam_beginlocationname"]=NextShowLocation;
				SOarray["custparam_nextlocation"]=NextShowLocation;	
				SOarray["custparam_nextiteminternalid"]=NextShowItemId;
				nlapiLogExecution('DEBUG', 'NextShowLocation : ', NextShowLocation);
				nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the location');
				nlapiLogExecution('DEBUG', 'NextShowItemId : ', NextShowItemId);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);				
			}
		}
	}
}

function updatetaskbegintime(WaveNo,ClusNo,getEndLocationInternalId,vitemid)
{
	nlapiLogExecution('DEBUG', 'Into updatetaskbegintime');

	var SOFilters = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
	SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
	SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
	if(WaveNo != null && WaveNo != "")
	{
		nlapiLogExecution('DEBUG', 'WaveNo inside If', WaveNo);

		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
	}

	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);

		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}

	if(getEndLocationInternalId!= null && getEndLocationInternalId!="" && getEndLocationInternalId!= "null")
	{
		nlapiLogExecution('DEBUG', 'getEndLocationInternalId inside If', getEndLocationInternalId);

		SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', getEndLocationInternalId));
	}

	if(vitemid!= null && vitemid!="" && vitemid!= "null")
	{
		nlapiLogExecution('DEBUG', 'vitemid inside If', vitemid);

		SOFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', vitemid));
	}

	var SOColumns = new Array();

	SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
	SOColumns[1] = new nlobjSearchColumn('custrecord_actbeginloc');

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!='')
	{
		for (var s = 0; s < SOSearchResults.length; s++) {

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[s].getId(), 'custrecord_actualbegintime', TimeStamp());

		}
	}	

	nlapiLogExecution('DEBUG', 'Out of updatetaskbegintime');
}

//case start:20125872
function getOpenReplns(item,location)
{
	nlapiLogExecution('ERROR', 'Into getOpenReplns');
	var PickFaceSearchResults= new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Task Type - RPLN	 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'is', location));	
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_taskpriority');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, PickFaceFilters, PickFaceColumns);



	return PickFaceSearchResults;

}


function isPickFaceLocationNew(item,location)
{
	nlapiLogExecution('DEBUG', 'Into isPickFaceLocationNew',item+","+location);
	var isPickFace='F';
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_autoreplen');
	var PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);
	if(PickFaceSearchResults!=null && PickFaceSearchResults!=''&& PickFaceSearchResults.length>0)
		isPickFace="T";

	nlapiLogExecution('DEBUG', 'Return Value',isPickFace);
	nlapiLogExecution('DEBUG', 'Out of isPickFaceLocation');

	return isPickFace;
}
//end

function buildTaskArray(WaveNo,ClusNo,OrdNo,vSOId,vZoneId)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if(WaveNo != null && WaveNo != "")
	{
		nlapiLogExecution('DEBUG', 'WaveNo inside If', WaveNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
	}

	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);

		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}

	if(OrdNo!=null && OrdNo!="" && OrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdNo);

		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
	}

	if(vSOId!=null && vSOId!="" && vSOId!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', vSOId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
	}

	if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	SOarray["custparam_ebizordno"] = vSOId;
	var SOColumns = new Array();
	SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
	SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
	SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
	SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
	SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
	SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
	SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
	SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
	SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
	SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
	SOColumns[14] = new nlobjSearchColumn('name');
	SOColumns[15] = new nlobjSearchColumn('custrecord_container');
	SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	SOColumns[17] = new nlobjSearchColumn('custrecord_ebizzone_no');
	SOColumns[18] = new nlobjSearchColumn('custrecord_lpno');

	SOColumns[0].setSort(true);
	SOColumns[1].setSort(false);
	SOColumns[2].setSort(false);

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}