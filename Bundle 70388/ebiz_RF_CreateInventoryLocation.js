/***************************************************************************
   eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CreateInventoryLocation.js,v $
 *     	   $Revision: 1.3.4.8.4.7.2.15.2.4 $
 *     	   $Date: 2015/11/26 11:18:39 $
 *     	   $Author: aanchal $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_188 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_CreateInventoryLocation.js,v $
 * Revision 1.3.4.8.4.7.2.15.2.4  2015/11/26 11:18:39  aanchal
 * 2015.2 Issue fix
 * 201415615
 *
 * Revision 1.3.4.8.4.7.2.15.2.3  2015/11/25 15:35:03  aanchal
 * 2015.2 Issue fix
 * 201415615
 *
 * Revision 1.3.4.8.4.7.2.15.2.2  2015/10/05 07:47:59  sponnaganti
 * 201414825
 * Briggs Sb1 issue fix
 *
 * Revision 1.3.4.8.4.7.2.15.2.1  2015/09/22 13:49:11  aanchal
 * 201414297
 * 2015.2 Issue FIx
 *
 * Revision 1.3.4.8.4.7.2.15  2015/07/06 16:45:24  rrpulicherla
 * Case#201413343
 *
 * Revision 1.3.4.8.4.7.2.14  2015/03/16 07:51:39  snimmakayala
 * 201223355
 *
 * Revision 1.3.4.8.4.7.2.13  2014/10/17 13:39:10  skavuri
 * Case# 201410632 Std bundle Issue fixed
 *
 * Revision 1.3.4.8.4.7.2.12  2014/06/13 10:22:41  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.3.4.8.4.7.2.11  2014/05/30 00:34:19  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.8.4.7.2.10  2014/05/26 15:43:22  skavuri
 * Case # 20148545 SB Issue Fixed
 *
 * Revision 1.3.4.8.4.7.2.9  2014/01/07 09:38:38  rmukkera
 * Case # 20126238
 *
 * Revision 1.3.4.8.4.7.2.8  2013/10/25 20:08:04  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20125294
 *
 * Revision 1.3.4.8.4.7.2.7  2013/09/02 15:48:26  skreddy
 * Case# 20124210,20124209
 * standard bundle issue fix
 *
 * Revision 1.3.4.8.4.7.2.6  2013/08/12 15:39:20  nneelam
 * Case# 20123866
 * Standard Bundle Issue Fixed..
 *
 * Revision 1.3.4.8.4.7.2.5  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.4.8.4.7.2.4  2013/04/16 15:05:48  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.3.4.8.4.7.2.3  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.3.4.8.4.7.2.2  2013/02/28 06:58:28  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Monobind.
 *
 * Revision 1.3.4.8.4.7.2.1  2013/02/27 13:22:58  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from Monobind
 *
 * Revision 1.3.4.8.4.7  2013/02/22 06:41:05  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to  showing invalid location
 *
 * Revision 1.3.4.8.4.6  2012/11/20 21:52:24  gkalla
 * CASE201112/CR201113/LOG201121
 * We are getting lot# scanning screen for assembly item also
 *
 * Revision 1.3.4.8.4.5  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.8.4.4  2012/10/23 17:08:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.3.4.8.4.3  2012/10/05 06:34:12  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting Multiple language into given suggestions
 *
 * Revision 1.3.4.8.4.2  2012/09/26 12:33:02  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.3.4.8.4.1  2012/09/24 10:08:56  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.3.4.8  2012/07/04 07:10:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to creating inventory for Monobind SB is resolved.
 *
 * Revision 1.3.4.7  2012/07/02 06:21:59  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Searching Item From Item Master is resolved.
 *
 * Revision 1.3.4.6  2012/05/25 06:42:05  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * item allias
 *
 * Revision 1.3.4.3  2012/02/21 13:22:12  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.3.4.2  2012/02/14 07:43:45  schepuri
 * CASE201112/CR201113/LOG201121
 * CVS Keyword $Log is added
 *
 *****************************************************************************/
function CreateInventoryLocation(request, response){
	if (request.getMethod() == 'GET') 
	{
		var getLanguage = request.getParameter('custparam_language');
		var st1,st2,st3,st4,st5;

		if( getLanguage == 'es_ES')
		{		
			st1 = "INGRESAR / ESCANEAR UBICACI&#211;N";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULO";
			st3 = "INGRESAR / ESCANEAR CANTIDAD";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";
			st6 = "CREAR INVENTARIO";


		}
		else
		{
			st1 = "ENTER/SCAN LOC";
			st2 = "ENTER/SCAN ITEM";	
			st3 = "ENTER/SCAN QTY";	
			st4 = "SEND";
			st5 = "PREV";
//			case 20124210,20124209 start
			st6 = "CREATE INVENTORY";
//			end


		}
		locationId = request.getParameter('custparam_locationId');

		var functionkeyHtml=getFunctionkeyScript('_rf_createinventory'); 
		var html = "<html><head><title>" + st6 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";     
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_createinventory' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 ;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				<input type='hidden' name='hdnLocationName' value=" + locationId+ ">";
		html = html + "				<input type='hidden' name='hdnLocationInternalid' value=" + locationId+ ">";
		html = html + "				<input type='hidden' name='hdnCompname' value=" + locationId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 ;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 ;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterquantity' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "				<td align = 'left'>" + st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var getBinLocation = request.getParameter('enterlocation');
		nlapiLogExecution('ERROR', 'getBinLocation', getBinLocation); 

		var getItem = request.getParameter('enteritem');
		nlapiLogExecution('ERROR', 'getItem', getItem); 

		var getQuantity = request.getParameter('enterquantity');
		nlapiLogExecution('ERROR', 'getQuantity', getQuantity); 

		var locationId = request.getParameter('hdnLocationInternalid');
		var ActualBeginDate = DateStamp();
		var ActualBeginTime = TimeStamp();

		var CIarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		CIarray["custparam_language"] = getLanguage;

		nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);
		nlapiLogExecution('ERROR', 'locationId', locationId); 


		var st6,st7,st8,st9,st10;
		if( getLanguage == 'es_ES')
		{
			st6 = "CREAR INVENTARIO";
			st7 = "UBICACI&#211;N NO V&#193;LIDA";
			st8 = "ART&#205;CULO INV&#193;LIDO ";
			st9 = "CANTIDAD NO PUEDE SER-VE VALOR";
			st10 = "CANTIDAD INV&#193;LIDA";

		}
		else
		{
			st6 = "CREATE INVENTORY";
			st7= "INVALID WAREHOUSE/BIN LOCATION";
			st8= "INVALID ITEM";
			st9= "QUANTITY CANNOT BE -VE VAlUE";
			st10= "INVALID QUANTITY";
		}

		CIarray["custparam_actualbegindate"] = ActualBeginDate;

		var TimeArray = new Array();
		TimeArray = ActualBeginTime.split(' ');

		CIarray["custparam_actualbegintime"] = TimeArray[0];
		CIarray["custparam_actualbegintimeampm"] = TimeArray[1];

		CIarray["custparam_screenno"] = '15';
		CIarray["custparam_locationId"] = locationId;

		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') {
			//            response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, CIarray);
			response.sendRedirect('SUITELET', 'customscript_rf_location_company', 'customdeploy_rf_location_company_di', false, CIarray);
		}
		else 
		{         


			var Error="Y";
			var Results="Y";
			var ErrorDetails=""; 
			var binremcube=0;
			var BinLocationFilters = new Array();
			
			//Case # 20126238 Start
			if(getBinLocation !=null && getBinLocation!='')
			{
		// Case# 20148545 starts
				//Case 20125294 starts
				//BinLocationFilters[0] = new nlobjSearchFilter('name', null, 'is', getBinLocation);
				BinLocationFilters.push(new nlobjSearchFilter('name', null, 'is', getBinLocation));
//				case 20124210,20124209 start
				//BinLocationFilters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');	
				BinLocationFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				if(locationId!=null && locationId!='' && locationId!='undefined')
					//BinLocationFilters[2] = new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof',locationId);	
					BinLocationFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof',locationId));
				//end
				//BinLocationFilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'noneof', ['8']));//8 stage //Case# 201410632
				BinLocationFilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'noneof', ['8','3']));//8-stage 3-Dock
		// Case# 20148545 ends
				
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_remainingcube');
				var LocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, BinLocationFilters, columns);
				nlapiLogExecution('DEBUG', 'LocationSearchResults', LocationSearchResults);
				if(LocationSearchResults!=null)
				{					
					var getBinLocationId = LocationSearchResults[0].getId();
					binremcube=LocationSearchResults[0].getValue('custrecord_remainingcube');
					nlapiLogExecution('DEBUG', 'binremcube', binremcube);
					nlapiLogExecution('ERROR', 'Location Id', getBinLocationId );
					CIarray["custparam_binlocationid"] = getBinLocationId;
					CIarray["custparam_binlocationname"] = getBinLocation;					
				}
				else 
				{					
					if(ErrorDetails!='')
					{
						ErrorDetails = ErrorDetails + "/ " + st7;
					}
					else
					{
						
							ErrorDetails= st7;
						
					}
								
					Error="N";	
					CIarray["custparam_error"] = ErrorDetails;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					return false;					
				}
			}
			else
			{					
				ErrorDetails= "PLEASE ENTER LOCATION";
				Error="N";	
				CIarray["custparam_error"] = ErrorDetails;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				return false;
			}
			//Case # 20126238 Start
			if(getItem!=null && getItem!='' && getItem!='null')
			{
				//Case # 20126238 end
				//Case 20125294 ends
				var getItemId;    	
				var ItemFilters = new Array();
				ItemFilters[0] = new nlobjSearchFilter('nameinternal', null, 'is', getItem);	
				ItemFilters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
				var ItemSearchResults = nlapiSearchRecord('item', null, ItemFilters, null);
				if(ItemSearchResults==null)
				{
					var filters = new Array();
					filters.push(new nlobjSearchFilter('upccode', null, 'is', getItem));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('itemid');
				columns[0].setSort(true);

				ItemSearchResults = nlapiSearchRecord('item', null, filters, columns);
				nlapiLogExecution('ERROR', ' itemSearchResultsupccode', ItemSearchResults);
				if(ItemSearchResults == null){



					var filters = new Array();
					filters.push(new nlobjSearchFilter('name', null, 'is', getItem));


					var columns = new Array();
					columns.push(new nlobjSearchColumn('custrecord_ebiz_item'));

					ItemSearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, filters, columns);
					nlapiLogExecution('ERROR', ' itemSearchResultsalias', ItemSearchResults);

					nlapiLogExecution('ERROR', 'ItemSearchResults', ItemSearchResults);
					if (ItemSearchResults != null)
					{
						//getItemId = ItemSearchResults[0].getId();
						getItemId = ItemSearchResults[0].getValue('custrecord_ebiz_item');
						nlapiLogExecution('ERROR', 'Item Id', getItemId);
						CIarray["custparam_itemid"] = getItemId;
						CIarray["custparam_item"] = getItem;


					}
					else 
					{

							if(ErrorDetails!='')
							{
								ErrorDetails = ErrorDetails + "/ " + st8;
							}
							else
							{
								ErrorDetails= st8;
							}				
							Error="N";
						}


				}
				else
				{
					if (ItemSearchResults != null)
					{
						getItemId = ItemSearchResults[0].getId();						
						nlapiLogExecution('ERROR', 'Item Id', getItemId);
						CIarray["custparam_itemid"] = getItemId;
						CIarray["custparam_item"] = getItem;


					}
					else 
					{

							if(ErrorDetails!='')
							{
								ErrorDetails = ErrorDetails + "/ " + st8;
							}
							else
							{
								ErrorDetails= st8;
							}					
							Error="N";
						}
					}
				}
				else
				{
					nlapiLogExecution('ERROR', 'ItemSearchResults', ItemSearchResults);
					if (ItemSearchResults != null)
					{
						getItemId = ItemSearchResults[0].getId();					
						nlapiLogExecution('ERROR', 'Item Id', getItemId);
						CIarray["custparam_itemid"] = getItemId;
						CIarray["custparam_item"] = getItem;


					}
					else 
					{
						nlapiLogExecution('ERROR', 'ErrorDetails', ErrorDetails);
						if(ErrorDetails!='')
						{
							ErrorDetails = ErrorDetails + "/ " + st8;
						}
						else
						{
							ErrorDetails= st8;
						}					
						Error="N";
					}
				}
				
				var itemCube = 0;
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', getItem);
				filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', 1);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
				var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);


				if(skuDimsSearchResults != null){
					nlapiLogExecution('ERROR', 'Search Length of SKU Dims in inventory adjustment',skuDimsSearchResults.length);
					for (var i = 0; i < skuDimsSearchResults.length; i++) {
						var skuDim = skuDimsSearchResults[i];
						itemCube = skuDim.getValue('custrecord_ebizcube');		    		
					}
				}
			}//Case # 20126238 Start
			else
			{
				ErrorDetails= "PLEASE ENTER ITEM";
				Error="N";	
				CIarray["custparam_error"] = ErrorDetails;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				return false;
				
			}
			//Case # 20126238 End
			if (getQuantity != "")
			{	
				//case 20123866.  isNaN, if condition is added.
				if(!(isNaN(getQuantity)))
				{

					if(parseFloat(getQuantity)>0)
					{
						CIarray["custparam_quantity"] = getQuantity;
						nlapiLogExecution('ERROR', 'Quantity', getQuantity);
					}
					else
					{
						if(ErrorDetails!='')
						{
							ErrorDetails = ErrorDetails + "/ " + st9;
						}
						else
						{
							ErrorDetails= st9;
						}	
						Error="N";
					}
				}

				else
				{
					if(ErrorDetails!='')
					{
						ErrorDetails = ErrorDetails + "/ " + "QUANTITY SHOULD BE NUMERICAL";
					}
					else
					{
						ErrorDetails= "QUANTITY SHOULD BE NUMERICAL";
					}

					Error="N";
				}


			}
			else 
			{
				ErrorDetails= "PLEASE ENTER QUANTITY";
				Error="N";	
				CIarray["custparam_error"] = ErrorDetails;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				return false;
			}
			//Case # 20126238 End
			CIarray["custparam_error"] = ErrorDetails;
			if(getItemId != null && getItemId!=''&& Error!="N")
				//if(Error!="N")
			{
				var itemSubtype = nlapiLookupField('item', getItemId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
				nlapiLogExecution('ERROR', 'itemSubtype =', itemSubtype);
				if(itemSubtype!=null && itemSubtype!='')
				{
					nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype.recordType);
					nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
					nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);
					CIarray["custparam_ItemType"] = itemSubtype.recordType;


					//if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == 'lotnumberedassemblyitem' || itemSubtype.recordType == 'assemblyitem' || itemSubtype.custitem_ebizbatchlot == 'T')
					if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == 'lotnumberedassemblyitem' || itemSubtype.custitem_ebizbatchlot == 'T')
					{
						nlapiLogExecution('ERROR', 'Hi2');	
						Results="B";	
						nlapiLogExecution('ERROR', 'Results',Results);

					}
						else
							if(itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.recordType == 'serializedassemblyitem' || itemSubtype.custitem_ebizserialin == 'T')
							{
							var qty1= getQuantity-parseInt(getQuantity);
							nlapiLogExecution('ERROR', 'qty1',qty1);
							nlapiLogExecution('ERROR', 'getQuantity',getQuantity);
							nlapiLogExecution('ERROR', 'parseInt(getQuantity)',parseInt(getQuantity));
							
							if(qty1 != 0)
								{
								ErrorDetails= "SERIAL NUMBERS CANNOT BE IN DECIMAL";	
								CIarray["custparam_error"] = ErrorDetails;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
								return false;
								}
							}
					}
				}
				
				
				var LocTotalCube = (getQuantity * parseFloat(itemCube));

				nlapiLogExecution('ERROR', 'LocTotalCube', LocTotalCube);
				nlapiLogExecution('ERROR', 'binremcube', binremcube);

				if(parseFloat(LocTotalCube)>parseFloat(binremcube))
				{
					nlapiLogExecution('ERROR', 'text', 'text');
					CIarray["custparam_error"] = "Entered qty cube greater than Bin location remaining cube";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					return;
				}
				if(Error!="N")
				{
				if(Results=="Y")
				{
					response.sendRedirect('SUITELET', 'customscript_rf_create_invt_itemstat', 'customdeploy_rf_create_invt_itemstat_di', false, CIarray);
				}
				else if(Results=="B")
				{
					response.sendRedirect('SUITELET', 'customscript_rf_createinventory_batchno', 'customdeploy_rf_createinventory_batchno', false, CIarray);
					nlapiLogExecution('ERROR', 'Navigating to Batch Confirm', 'Success');   
				}
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
			}

			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}
	}
}
