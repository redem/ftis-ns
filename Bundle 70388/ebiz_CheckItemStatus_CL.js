/***************************************************************************
	  		   eBizNET Solutions Inc               
****************************************************************************
/* 
*     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_CheckItemStatus_CL.js,v $
*     	   $Revision: 1.3.4.1.8.1 $
*     	   $Date: 2013/09/11 15:23:51 $
*     	   $Author: rmukkera $
*
*   eBizNET version and checksum stamp.  Do not remove.
*   $eBiznet_VER: .............. $eBizNET_SUM: .....
* 
* PRAMETERS
*
* DESCRIPTION
* 
* REVISION HISTORY
* $Log: ebiz_CheckItemStatus_CL.js,v $
* Revision 1.3.4.1.8.1  2013/09/11 15:23:51  rmukkera
* Case# 20124376
*
* Revision 1.3.4.1  2012/02/09 16:10:57  snimmakayala
* CASE201112/CR201113/LOG201121
* Physical and Virtual Location changes
*
* Revision 1.4  2012/02/09 14:43:42  snimmakayala
* CASE201112/CR201113/LOG201121
* Physical and Virtual Location changes
*
* Revision 1.3  2011/08/20 15:57:29  skota
* CASE201112/CR201113/LOG201121
* Fixed the issue while updating values and not allowing the user to create duplicate records
*
*   
*
*****************************************************************************/


function fnCheckItemStatus(type)
{
	var varItemStatusName =  nlapiGetFieldValue('name');  
	var vSite	= nlapiGetFieldValue('custrecord_ebizsiteskus');  
	var vCompany	= nlapiGetFieldValue('custrecord_ebizcompanyskus'); 
	var vIntrId = nlapiGetFieldValue('id');
	
	if(varItemStatusName != "" && varItemStatusName != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is',varItemStatusName);
		filters[1] = new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof',['@NONE@',vSite]);
		filters[2] = new nlobjSearchFilter('custrecord_ebizcompanyskus', null, 'anyof',['@NONE@',vCompany]);
	
		if (vIntrId != null && vIntrId != "") 
		{
			filters[3] = new nlobjSearchFilter('internalid', null, 'noneof', vIntrId);
		}	
	
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filters);
		if(searchresults)
		{
			alert("This Record already exists.");
			return false;
		}
		else
		{
			return true;
		}
	}
	else	
	{
		return true;
	}
}