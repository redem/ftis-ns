
/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_ClusPickingItem.js,v $
 *     	   $Revision: 1.1.2.3 $
 *     	   $Date: 2015/08/04 14:05:21 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_ClusPickingItem.js,v $
 * Revision 1.1.2.3  2015/08/04 14:05:21  grao
 * 2015.2   issue fixes  20143669
 *
 * Revision 1.1.2.2  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.1  2015/02/13 14:34:37  sponnaganti
 * Jawbone Cluster Picking CR Changes
 * CR#  CR14US0323
 *
 * Revision 1.3.2.10.4.7.2.14  2014/03/12 06:33:34  skreddy
 * case 20127626
 * Deal med SB issue fixs
 *
 * Revision 1.3.2.10.4.7.2.13  2014/01/24 14:46:44  rmukkera
 * Case # 20126897
 *
 * Revision 1.3.2.10.4.7.2.12  2014/01/08 15:01:14  grao
 * Case# 20126692 related issue fixes in Sb issue fixes
 *
 * Revision 1.3.2.10.4.7.2.11  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.3.2.10.4.7.2.10  2013/08/29 15:01:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Given NEW SKIP option when there is an invt hold flag
 * against the inventory record
 *
 * Revision 1.3.2.10.4.7.2.9  2013/07/15 17:05:07  skreddy
 * Case# 20123446
 * Item info display CR
 *
 * Revision 1.3.2.10.4.7.2.8  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.3.2.10.4.7.2.7  2013/04/30 18:44:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.3.2.10.4.7.2.6  2013/04/26 15:49:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.3.2.10.4.7.2.5  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.2.10.4.7.2.4  2013/04/15 14:57:19  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.3.2.10.4.7.2.3  2013/04/12 11:02:38  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.3.2.10.4.7.2.2  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.3.2.10.4.7.2.1  2013/03/01 14:34:59  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.3.2.10.4.7  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.3.2.10.4.6  2012/12/28 16:32:14  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Exception in Cluster Picking
 *
 * Revision 1.3.2.10.4.5  2012/12/17 15:07:20  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for null
 *
 * Revision 1.3.2.10.4.4  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.2.10.4.3  2012/10/04 10:31:18  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.3.2.10.4.2  2012/09/26 12:28:38  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.3.2.10.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.3.2.10  2012/09/14 20:12:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.3.2.9  2012/08/28 15:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to UPC scan.
 *
 * Revision 1.3.2.8  2012/05/10 07:11:31  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * shipping changes
 *
 * Revision 1.3.2.7  2012/04/27 13:15:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Cluster picking
 *
 * Revision 1.3.2.6  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.2.5  2012/03/12 08:05:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.6  2012/03/12 07:56:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.5  2012/02/28 01:22:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.4  2012/02/24 11:22:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.3  2012/02/23 18:19:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.2  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.1  2012/02/23 13:08:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.21  2012/02/20 14:25:46  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Loc Exception
 *
 * Revision 1.20  2012/02/20 12:56:04  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.19  2012/02/17 13:31:09  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.18  2012/01/23 23:21:17  gkalla
 * CASE201112/CR201113/LOG201121
 * RF Picking customized
 *
 * Revision 1.17  2012/01/12 17:56:58  gkalla
 * CASE201112/CR201113/LOG201121
 * To add item description
 *
 * Revision 1.16  2012/01/12 11:00:39  gkalla
 * CASE201112/CR201113/LOG201121
 * Added calling upc code function from generat functions
 *
 * Revision 1.5  2011/07/12 06:39:08  pattili
 * CASE201112/CR201113/LOG201121
 * 1. Changes to RF Putaway SKU and Putaway Location.
 *
 * Revision 1.4  2011/07/12 05:35:41  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function WOClusterPickingItem(request, response){

	var context = nlapiGetContext();
	var sessionobj = null;
	sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	context.setSessionObject('session', null);
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES')
		{
			st1 = "ART&#205;CULO:";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULO";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
//			st5 = "Art&#237;culo Inv&#225;lido";
			st6 = "Descripci&#243;n del art&#237;culo:";

		}
		else
		{
			st1 = "ITEM: ";
			st2 = "ENTER/SCAN ITEM ";
			st3 = "SEND";
			st4 = "PREV";
//			st5 = "INVALID ITEM";
			st6 = "ITEM DESCRIPTION: ";
		}

		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginlocationname');
		var vClusterno = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');
		var vBatchno = request.getParameter('custparam_batchno');
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_nooflocrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemId=request.getParameter('custparam_nextiteminternalid');
		var NextItem=request.getParameter('custparam_nextitem');
		var pickType=request.getParameter('custparam_picktype');

		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var venterzone=request.getParameter('custparam_venterzone');
		nlapiLogExecution('DEBUG', 'getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('DEBUG', 'NextLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'NextItemId', NextItemId);
		nlapiLogExecution('DEBUG', 'NextItem', NextItem);
		nlapiLogExecution('DEBUG', 'venterzone', venterzone);

		if(getFetchedLocation==null)
		{
			getFetchedLocation=NextLocation;
		}
		if(getItemInternalId==null)
		{
			getItemInternalId=NextItemId;
		}
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');

		/*var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var Itemdescription='';

		var getItemName = ItemRec.getFieldValue('itemid');

		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('description');
		}
		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('salesdescription');
		}*/	
		var getItem="";
		var Itemdescription='';
		if(getItemInternalId!=null&& getItemInternalId!="")
		{
			var filter=new Array();
			filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

			var column=new Array();
			column[0]=new nlobjSearchColumn("type");
			column[1]=new nlobjSearchColumn("description");
			column[2]=new nlobjSearchColumn("itemid");

			var searchres=nlapiSearchRecord("item",null,filter,column);
			if(searchres!=null&&searchres!="")
			{
				getItem=searchres[0].getValue("itemid");
				Itemdescription=searchres[0].getValue("description");

			}
		}

		nlapiLogExecution('DEBUG', 'getItem', getItem);
		nlapiLogExecution('DEBUG', 'getItemdescription', Itemdescription);
		nlapiLogExecution('DEBUG', 'Next Location', NextLocation);


		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    

		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		html = html + " document.getElementById('enteritem').focus();";     

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";		
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getItem + "</label><br>"+ st6 +"<label>" + Itemdescription + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocId' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value='" + Itemdescription + "'>";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterno + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnenterzone' value=" + venterzone + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +" ";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnnextitem' value=" + NextItemId + ">";	
		html = html + "				<input type='hidden' name='hdnnextitemname' value=" + NextItem + ">";	
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";	
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					OVERRIDE <input name='cmdOverride' type='submit' value='F11'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st5,st6;

		if( getLanguage == 'es_ES')
		{

			st5 = "ART&#205;CULO INV&#193;LIDO";
			st6 = "ELEMENTO DESCRIPCI&#211;N";

		}
		else
		{

			st5 = "INVALID ITEM";
			st6 = "ITEM DESCRIPTION: ";
		}

		var picktype = request.getParameter('hdnpicktype');

		var getEnteredItem = request.getParameter('enteritem');
		nlapiLogExecution('DEBUG', 'Entered Item', getEnteredItem);
		nlapiLogExecution('DEBUG', 'hdnnext', request.getParameter('hdnnext'));
		nlapiLogExecution('DEBUG', 'hdnnextItemId', request.getParameter('hdnnextitem'));

		var getWaveNo = request.getParameter('hdnWaveNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getBeginLocIntrId = request.getParameter('hdnBeginLocId');
		var getItem = request.getParameter('hdnItem');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		var getItemName = request.getParameter('hdnItemName');
		nlapiLogExecution('DEBUG', 'hdnItemName', getItemName);
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');	
		var whLocation = request.getParameter('hdnwhlocation');

		var whCompany = request.getParameter('hdnwhCompany');
		var RecCount=request.getParameter('hdnRecCount');
		var getNext=request.getParameter('hdnnext');
		var getNextItemId=request.getParameter('hdnnextitem');
		var getNextItem=request.getParameter('hdnnextitemname');


		var OrdName=request.getParameter('hdnName');
		var ContainerSize=request.getParameter('hdnContainerSize');
		var ebizOrdNo=request.getParameter('hdnebizOrdNo');
		var venterzone = request.getParameter('hdnenterzone');
		nlapiLogExecution('DEBUG', 'Name', OrdName);
		nlapiLogExecution('DEBUG', 'Begin Location', getBeginLocation);
		nlapiLogExecution('DEBUG', 'Next Location', getNext);
		nlapiLogExecution('DEBUG', 'Next Item', getNextItemId);
		nlapiLogExecution('DEBUG', 'Next Item Name', getNextItem);
		nlapiLogExecution('DEBUG', 'venterzone', venterzone);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_error"] = st5;//'INVALID ITEM';
		SOarray["custparam_screenno"] = 'CLWO3';
		SOarray["custparam_whlocation"] = whLocation;
		SOarray["custparam_waveno"] = getWaveNo;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_beginlocationname"] = getBeginLocation;

		SOarray["custparam_beginLocation"] = getBeginLocIntrId;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_dolineid"] = getDOLineId;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_batchno"] = vBatchNo;
		SOarray["custparam_nooflocrecords"] = RecCount;
		SOarray["custparam_nextlocation"] = getNext;
		SOarray["custparam_nextiteminternalid"] = getNextItemId;
		SOarray["name"] = OrdName;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = ebizOrdNo;
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_nextitem"] = getNextItem;
		SOarray["custparam_serialscanned"]='';
		SOarray["custparam_istaskskipped"] = "";//this parameter is used in clusterconf skip task
		SOarray["custparam_venterzone"]=request.getParameter('hdnenterzone');
		nlapiLogExecution('DEBUG', 'Order LineNo is', getOrderLineNo);
		nlapiLogExecution('DEBUG', 'getEnteredItem',getEnteredItem);
		nlapiLogExecution('DEBUG', 'getItemName',getItemName);
		nlapiLogExecution('DEBUG', 'SOarray["custparam_beginLocation"] ', SOarray["custparam_beginLocation"] );

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		
		/*nlapiLogExecution('DEBUG', 'sessionobj11',sessionobj);
		nlapiLogExecution('DEBUG', 'User11',context.getUser());*/

		/*if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}*/
				if (optedEvent == 'F7') {
					var prevscreen='';
					nlapiLogExecution('DEBUG', 'SOarray["custparam_prevscreen"]', request.getParameter('custparam_prevscreen'));
					if(request.getParameter('custparam_prevscreen')!=null && request.getParameter('custparam_prevscreen')!='')
					{
						prevscreen=request.getParameter('custparam_prevscreen');
					}
					nlapiLogExecution('DEBUG', 'prevscreen', prevscreen);
					if(prevscreen=='ItemScan')
					{
						response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
					}
					else if(prevscreen=='summtask')
					{
						response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_summtask', 'customdeploy_rf_wocluspicking_summtask', false, SOarray);
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_location', 'customdeploy_rf_wocluspicking_location', false, SOarray);
					}
				}
				else {
					if (getEnteredItem != '' && getEnteredItem != getItemName) {

//						var actItemid=validateSKU(getEnteredItem);

						var actItemidArray=validateSKUId(getEnteredItem,whLocation,'');
						nlapiLogExecution('DEBUG', 'After validateSKU1',actItemidArray);
						var actItemid = actItemidArray[1];
						nlapiLogExecution('DEBUG', 'After validateSKU11',actItemid);
						if(actItemid!=null && actItemid!="")
						{
							getEnteredItem=actItemid;

							nlapiLogExecution('DEBUG', 'getEnteredItem ', getEnteredItem);
							nlapiLogExecution('DEBUG', 'getItem ', getItem);
							nlapiLogExecution('DEBUG', 'getItemName ', getItemName);

							var SOFilters = new Array();
							SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
							SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
							SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));

							if(getWaveNo != null && getWaveNo != "")
							{

								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
							}

							if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
							{
								nlapiLogExecution('DEBUG', 'ClusNo inside If', vClusterNo);

								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
							}

							if(actItemid != null && actItemid != "")
							{

								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', actItemid));
							}

							if(venterzone!=null && venterzone!="" && venterzone!='null')
							{
								nlapiLogExecution('DEBUG', 'getZoneNo inside if', venterzone);
								nlapiLogExecution('DEBUG', 'vZoneId inside if', venterzone);
								SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));
							
							}
							SOarray["custparam_ebizordno"] =ebizOrdNo;
							var SOColumns = new Array();
							SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
							SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
							SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
							SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
							SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
							SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
							SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
							SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
							SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
							SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
							SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
							SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
							SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
							SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
							SOColumns[14] = new nlobjSearchColumn('name');
							SOColumns[15] = new nlobjSearchColumn('custrecord_container');
							SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');
							SOColumns[17] = new nlobjSearchColumn('custrecord_sku_status');
							
							SOColumns[1].setSort();
							SOColumns[2].setSort();

							//SOColumns[1].setSort(false);
							//SOColumns[2].setSort(false);
							//SOColumns[3].setSort(true);

							var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

							if (SOSearchResults != null && SOSearchResults.length > 0) {
								SOarray["custparam_recordinternalid"] = SOSearchResults[0].getId();
								SOarray["custparam_expectedquantity"] = SOSearchResults[0].getValue('custrecord_expe_qty');
								SOarray["custparam_invoicerefno"] = SOSearchResults[0].getValue('custrecord_invref_no');
								SOarray["custparam_batchno"] = SOSearchResults[0].getValue('custrecord_batch_no');
								SOarray["custparam_ebizordno"] =  SOSearchResults[0].getValue('custrecord_ebiz_order_no');						
								SOarray["custparam_itemstatus"] = SOSearchResults[0].getValue('custrecord_sku_status');
							}
						}
					}  
					else if(getEnteredItem == getItemName)
					{
						nlapiLogExecution('DEBUG', 'getEnteredItem', getEnteredItem);
						nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
						getEnteredItem = getItemInternalId;
					}	
					nlapiLogExecution('DEBUG', 'getEnteredItem ', getEnteredItem);
					nlapiLogExecution('DEBUG', 'getItem ', getItem);
					nlapiLogExecution('DEBUG', 'getItemName ', getItemName);

					if (getEnteredItem != '' && getEnteredItem == getItemInternalId) {

						var ItemType = '';
						var batchflag = '';
						var ItemSpecialInstr='';
						if(getItemInternalId != null && getItemInternalId != "")
						{
							// case 20123446 start: added custitem_ebizmodelno
							var ItemTypeRec = nlapiLookupField('item', getItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot','custitem_ebizmodelno']);				
							batchflag = ItemTypeRec.custitem_ebizbatchlot;
							ItemType = ItemTypeRec.recordType;
							ItemSpecialInstr = ItemTypeRec.custitem_ebizmodelno;
							// case 20123446 end
						}

						SOarray["custparam_itemType"] = ItemType;
						SOarray["custparam_itemInstructions"] =ItemSpecialInstr;
						nlapiLogExecution('DEBUG', 'ItemType', ItemType);
						nlapiLogExecution('DEBUG', 'getWaveNo', getWaveNo);
						nlapiLogExecution('DEBUG', 'vClusterNo', vClusterNo);
						nlapiLogExecution('DEBUG', 'ebizOrdNo', ebizOrdNo);
						nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
						nlapiLogExecution('DEBUG', 'getBeginLocIntrId', getBeginLocIntrId);
						nlapiLogExecution('ERROR', 'ItemSpecialInstr', ItemSpecialInstr);


						var SOFilters = new Array();

						SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
						SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
						SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
						SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
						if(getWaveNo != null && getWaveNo != "")
						{
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
						}

						if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
						{
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
						}

						if(getItemInternalId != null && getItemInternalId != "")
						{
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
						}

						if(getBeginLocIntrId != null && getBeginLocIntrId != "")
						{
							SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', getBeginLocIntrId));
						}

						if(venterzone!=null && venterzone!="" && venterzone!='null')
						{
							nlapiLogExecution('DEBUG', 'vZoneId inside if', venterzone);
							SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));
						}
						
						
						var SOColumns = new Array();
						SOColumns[0] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
						SOColumns[1] = new nlobjSearchColumn('custrecord_sku',null,'group');
						SOColumns[2] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
						SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
						SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
						SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');					
						SOColumns[6] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
						SOColumns[7] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
						SOColumns[8] = new nlobjSearchColumn('custrecord_ebizmethod_no',null,'group');
						SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
						SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
						SOColumns[11] = new nlobjSearchColumn('custrecord_sku_status',null,'group');
						SOColumns[0].setSort();//SKU
						SOColumns[1].setSort();//Location

						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

						var summaryTask='F';  var detailTask='F';

						if (SOSearchResults != null && SOSearchResults.length > 0) {

							nlapiLogExecution('DEBUG', 'SOSearchResults count', SOSearchResults.length);
							SOarray["custparam_expectedquantity"] = SOSearchResults[0].getValue('custrecord_expe_qty',null,'sum');
							SOarray["custparam_item"] = SOSearchResults[0].getValue('custrecord_sku',null,'group');
							SOarray["custparam_itemdescription"] = SOSearchResults[0].getValue('custrecord_skudesc',null,'group');
							SOarray["custparam_iteminternalid"] = SOSearchResults[0].getValue('custrecord_ebiz_sku_no',null,'group');
							SOarray["custparam_batchno"] = SOSearchResults[0].getValue('custrecord_batch_no',null,'group');
							SOarray["custparam_itemstatus"] = SOSearchResults[0].getValue('custrecord_sku_status',null,'group');

							var clusterNo =  SOSearchResults[0].getValue('custrecord_ebiz_clus_no',null,'group');
							var pickMethodNo =  SOSearchResults[0].getValue('custrecord_ebizmethod_no',null,'group');

							nlapiLogExecution('DEBUG', 'pickMethodNo', pickMethodNo);
							if(pickMethodNo!=null && pickMethodNo!='')
							{
								//	

								var pickMethodFilters = new Array();
								pickMethodFilters.push(new nlobjSearchFilter('internalid', null, 'is', pickMethodNo));
								var pickMethodColumns = new Array();				
								pickMethodColumns[0] = new nlobjSearchColumn('custrecord_ebiz_summary_task');
								pickMethodColumns[1] = new nlobjSearchColumn('custrecord_ebiz_detail_task');
								var pickMethodSearchResults=nlapiSearchRecord('customrecord_ebiznet_pick_method', null, pickMethodFilters, pickMethodColumns);

								nlapiLogExecution('DEBUG', 'pickMethodSearchResults', pickMethodSearchResults);

								if(pickMethodSearchResults!=null && pickMethodSearchResults.length>0)
								{
									nlapiLogExecution('DEBUG', 'summaryTask', pickMethodSearchResults[0].getValue('custrecord_ebiz_summary_task'));
									summaryTask=pickMethodSearchResults[0].getValue('custrecord_ebiz_summary_task');
									detailTask=pickMethodSearchResults[0].getValue('custrecord_ebiz_detail_task');
									nlapiLogExecution('DEBUG', 'detailTask', detailTask);
								}
							}


						}


						if(summaryTask=='F')
						{
							nlapiLogExecution('DEBUG', 'Confirm Pick');

							var BeginLocation = request.getParameter('hdnBeginLocId');

							var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
							var EndLocation = request.getParameter('hdnEnteredLocation');

							var NextShowLocation=getNext;


							var NextItemInternalId=request.getParameter('hdnNextItemId');

							nlapiLogExecution('DEBUG', 'OrdName', OrdName);
							nlapiLogExecution('DEBUG', 'Next Location', NextShowLocation);
							nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);

							nlapiLogExecution('DEBUG', 'Before Item Fulfillment', 'Before Item Fulfillment');

							//var itemSubtype = nlapiLookupField('item', getItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);
							var rectype="";
							var serin="";
							var serout="";
							var vbatchlot="";

							var filter=new Array();
							filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

							var column=new Array();
							column[0]=new nlobjSearchColumn("type");
							column[1]=new nlobjSearchColumn("custitem_ebizserialin");
							column[2]=new nlobjSearchColumn("custitem_ebizserialout");
							column[3]=new nlobjSearchColumn("custitem_ebizbatchlot");

							var searchres=nlapiSearchRecord("item",null,filter,column);
							if(searchres!=null&&searchres!="")
							{
								rectype=searchres[0].getText("type").toLowerCase();
								rectype=rectype.replace(/\s/g, "");
								serin=searchres[0].getValue("custitem_ebizserialin");
								serout=searchres[0].getValue("custitem_ebizserialout");
								vbatchlot=searchres[0].getValue("custitem_ebizbatchlot");
							}

							if (rectype == 'serializedinventoryitem' || serout == 'T') {
								SOarray["custparam_number"] = 0;
								SOarray["custparam_RecType"] = rectype;
								SOarray["custparam_SerOut"] = serout;
								SOarray["custparam_SerIn"] = serin;
								SOarray["custparam_prevscreen"] =  "ItemScan";
								response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
							}
							else
							{		

								//nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
								//Changed the code for auto containerization
								var vAutoContainerFlag='N'; // Set 'Y' to skip RF dialog for scanning container LP
								SOarray["custparam_autocont"] = vAutoContainerFlag;
								if(vAutoContainerFlag=='Y')
								{
									var RcId=getRecordInternalId;
									var EndLocation = EndLocInternalId;
									var TotalWeight=0;
									var getReason=request.getParameter('custparam_enteredReason');
									var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
									var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
									getLPContainerSize= SORec.getFieldText('custrecord_container');

									SOarray["custparam_newcontainerlp"] = getContainerLPNo;
									var vPickType = request.getParameter('hdnpicktype');
									var vBatchno = request.getParameter('hdnbatchno');
									var PickQty = getExpectedQuantity;
									var vActqty = ExpectedQuantity;
									var SalesOrderInternalId = ebizOrdNo;
									var getContainerSize = getLPContainerSize;
									var  vdono = getDOLineId;
									nlapiLogExecution('DEBUG', 'WaveNo', getWaveNo);
									nlapiLogExecution('DEBUG', 'getContainerLPNo', getContainerLPNo);
									var opentaskcount=0;

									nlapiLogExecution('DEBUG', 'opentaskcount', getWaveNo);
									nlapiLogExecution('DEBUG', 'opentaskcount', getContainerLPNo);
									nlapiLogExecution('DEBUG', 'opentaskcount', vPickType);
									nlapiLogExecution('DEBUG', 'opentaskcount', SalesOrderInternalId);
									nlapiLogExecution('DEBUG', 'opentaskcount', vdono);
									opentaskcount=getOpenTasksCount(getWaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,vdono);
									nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
									//For fine tuning pick confirmation and doing autopacking in user event after last item
									var IsitLastPick='F';
									if(opentaskcount > 1)
										IsitLastPick='F';
									else
										IsitLastPick='T';

									nlapiLogExecution('DEBUG', 'getItem', getItemInternalId);

									ConfirmPickTask(getItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,vBatchno,IsitLastPick);
									//if(RecCount > 1)
									if(opentaskcount > 1)
									{
										var SOFilters = new Array();
										SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

										if(getWaveNo != null && getWaveNo != "")
										{

											SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
										}

										if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
										{
											nlapiLogExecution('DEBUG', 'ClusNo inside If', vClusterNo);

											SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
										}

										if(OrdName!=null && OrdName!="" && OrdName!= "null")
										{
											nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);

											SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
										}

										if(ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
										{
											nlapiLogExecution('DEBUG', 'OrdNo inside If', ebizOrdNo);

											SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
										}

										var SOColumns = new Array();
										SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
										SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
										SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
										SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
										SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
										SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
										SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
										SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
										SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
										SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
										SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
										SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
										SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
										SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
										SOColumns[14] = new nlobjSearchColumn('name');
										SOColumns[15] = new nlobjSearchColumn('custrecord_container');
										SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');

										SOColumns[1].setSort(false);
										SOColumns[2].setSort(false);
										SOColumns[3].setSort(true);

										var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
										nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
										if (SOSearchResults != null && SOSearchResults.length > 0) 
										{
											var SOSearchResult = SOSearchResults[0];
											if(SOSearchResults.length > 1)
											{
												var SOSearchnextResult = SOSearchResults[1];
												SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
												SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
												nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
											}
											SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
											SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
											SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
											SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
											SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
											SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
											SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
											SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
											SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
											SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
											SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
											SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
											SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
											SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
											SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
											SOarray["custparam_nooflocrecords"] = SOSearchResults.length;		
											SOarray["name"] =  SOSearchResult.getValue('name');
											SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
											SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');

										}
										if(BeginLocation != NextShowLocation)
										{ 
											SOarray["custparam_prevscreen"] =  "ItemScan";
											response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_location', 'customdeploy_rf_wocluspicking_location', false, SOarray);								
											nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
										}
										else if(getItemInternalId != NextItemInternalId)
										{ 
											SOarray["custparam_prevscreen"] =  "ItemScan";
											response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);								
											nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
										}
										else if(getItemInternalId == NextItemInternalId)
										{ 
											SOarray["custparam_prevscreen"] =  "ItemScan";
											response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								
											nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
										}
									}
									else{

										nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
										//For fine tuning pick confirmation and doing autopacking in user event after last item
										//AutoPacking(SalesOrderInternalId);
										/*nlapiLogExecution('DEBUG', 'Navigating To1', 'Foot print');
								response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, SOarray);*/

										//nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
										//For fine tuning pick confirmation and doing autopacking in user event after last item
										//AutoPacking(SalesOrderInternalId);
										SOarray["custparam_prevscreen"] =  "ItemScan";
										nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
										response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_stage_loc', 'customdeploy_rf_wocluspicking_stage_loc', false, SOarray);					
									}

								}


								if(detailTask=='F')
								{
									nlapiLogExecution('DEBUG', 'intodetailTask', '');
									//detail task functionality

									var SOFilters = new Array();

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
									SOFilters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
									SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
									if(getWaveno != null && getWaveno != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
									}

									if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
									}

									/*if(getItemInternalId != null && getItemInternalId != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
									}

									if(BeginLocation != null && BeginLocation != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', BeginLocation));
									}

									var SOColumns = new Array();				
									SOColumns[0] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
									SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
									SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
									SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');					
									SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
									SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
									SOColumns[7] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
									SOColumns[8] = new nlobjSearchColumn('custrecord_container',null,'group');

									SOColumns[0].setSort();	//SKU
									SOColumns[2].setSort();	//Location
									SOColumns[7].setSort();//Container LP
*/									
									var SOColumns = new Array();				
									//SOColumns[0] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
									SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
									SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
									SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
									SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
									SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');					
									SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
									SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
									SOColumns[9] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
									SOColumns[10] = new nlobjSearchColumn('custrecord_container',null,'group');
									SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no',null,'group');

									SOColumns[0].setSort();	//SKU
									SOColumns[1].setSort();	//Location
									SOColumns[2].setSort();//Container LP
									var locreccount=request.getParameter('hdnRecCount');
									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									if (SOSearchResults != null && SOSearchResults.length > 0) {
										nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults.length);
										for(var k=0;k<SOSearchResults.length;k++)
										{

											getContainerLpNo = SOSearchResults[k].getValue('custrecord_container_lp_no',null,'group');
											getLPContainerSize = SOSearchResults[k].getText('custrecord_container',null,'group');
											getQtybyContainer = SOSearchResults[k].getValue('custrecord_expe_qty',null,'sum');
											vRemainQty=parseFloat(getExpectedQuantity)-parseFloat(getQtybyContainer);
											BeginLocationName=SOSearchResults[k].getText('custrecord_actbeginloc',null,'group');
											var 	getBeginLocationId=SOSearchResults[k].getValue('custrecord_actbeginloc',null,'group');
											var 	getEndLocInternalId='';
											getItemInternalId = SOSearchResults[k].getValue('custrecord_ebiz_sku_no',null,'group');
											var nextlocation=SOSearchResults[k+1];var NextShowLocation='';var NextShowItem='';
											if(nextlocation!=null)
											{
												nlapiLogExecution('DEBUG', 'nextlocation1', nextlocation);
												NextShowLocation=nextlocation.getText('custrecord_actbeginloc',null,'group');
												NextShowItem=nextlocation.getValue('custrecord_ebiz_sku_no',null,'group');
											}
											getExpectedQuantity=vRemainQty;
											nlapiLogExecution('DEBUG', 'vRemainQty', vRemainQty);
											nlapiLogExecution('DEBUG', 'vClusterNo', vClusterNo);
											nlapiLogExecution('DEBUG', 'getExpectedQuantity', getExpectedQuantity);
											nlapiLogExecution('DEBUG', 'getContainerLpNo', getContainerLpNo);
											nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
											nlapiLogExecution('DEBUG', 'NextShowLocation', NextShowLocation);
											nlapiLogExecution('DEBUG', 'NextShowItem', NextShowItem);
											ConfirmPickTasks(vClusterNo,getContainerLpNo,getItemInternalId,getBeginLocationId,getEndLocInternalId,getContainerLpNo);
											if(NextShowLocation!='' )
											{					
												if(BeginLocationName != NextShowLocation )
												{ 
													SOarray["custparam_beginlocationname"]= NextShowLocation;
													SOarray["custparam_prevscreen"] =  "ItemScan";
													nlapiLogExecution('DEBUG', 'Navigating To', 'Picking Location');
													response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_location', 'customdeploy_rf_wocluspicking_location', false, SOarray);
													return;
												}
												else if((BeginLocationName == NextShowLocation) && getItemInternalId != NextShowItem)
												{ 
													SOarray["custparam_item"] = NextShowItem;
													SOarray["custparam_iteminternalid"] = NextShowItem;
													SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
													SOarray["custparam_prevscreen"] =  "ItemScan";

													nlapiLogExecution('DEBUG', 'Navigating To', 'Picking Item');
													response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_item', 'customdeploy_rf_wocluspicking_item', false, SOarray);
													return;
												}


											}
											else{
												SOarray["custparam_prevscreen"] =  "ItemScan";

												nlapiLogExecution('DEBUG', 'Navigating To', 'Stage Location');
												response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_stage_loc', 'customdeploy_rf_wocluspicking_stage_loc', false, SOarray);
												return;
											}

										}
										//
									}

								}			
								else
								{
									SOarray["custparam_prevscreen"] =  "ItemScan";
									nlapiLogExecution('DEBUG', 'Navigating To', 'Detail Task');
									response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_dettask', 'customdeploy_rf_wocluspicking_dettask', false, SOarray);
									return;
								}
							}

						}//end of summary task
						// case 20123446 start:
						else if(ItemSpecialInstr !=null && ItemSpecialInstr !='')
						{
							response.sendRedirect('SUITELET', 'customscript_picking_item_instructions', 'customdeploy_ebiz_picking_item_instru_di', false, SOarray);
						}// case 20123446 end
						else if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_batch', 'customdeploy_ebiz_rf_wo_picking_batch_di', false, SOarray);
						}
						else
						{
							/*var invtholdflag = IsInvtonHold(getItemInternalId,SOarray["custparam_endlocinternalid"]);

					if(invtholdflag=='T')
					{
						//case # 20126897Ã¯Â¿Â½start
						SOarray["custparam_screenno"] = 'CL3';	
						//case # 20126897Ã¯Â¿Â½end
						SOarray["custparam_error"]='CYCLE COUNT IS IN PROGRESS FOR THIS LOCATION. PLEASE DO THE LOCATION EXCEPTION TO COMPLETE THIS PICK';
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clustercnf_skiptask', 'customdeploy_rf_clustercnf_skiptask', false, SOarray);
						return;
					}
					else*/
							{
								nlapiLogExecution('DEBUG', 'SOarray["custparam_beginLocation"] ', SOarray["custparam_beginLocation"] );
								nlapiLogExecution('DEBUG', 'Navigating to Summary Task');
								SOarray["custparam_detailtask"] = detailTask;
								response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_summtask', 'customdeploy_rf_wocluspicking_summtask', false, SOarray);
							}
						}
					}			
					else 
					{
						nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the Item');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

					}
				}
			/*}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}*/
		/*}
		else
		{
			SOarray["custparam_screenno"] = 'CLWO2';
			SOarray["custparam_error"] = 'ITEM ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}*/
	}
}
function deleteOldAllocations(invrefno,pickqty,contlpno,ActPickQty)
{ 

	try
	{
		var scount=1;
		var invtrecid;

		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {

					if(parseFloat(Invallocqty)- parseFloat(ActPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(Invallocqty)- parseFloat(ActPickQty));
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid); 


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		} 

	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
	}

	nlapiLogExecution('Debug', 'Out of deleteOldAllocations (Inv Ref NO)',invrefno);
}
function CreateSTGInvtRecord(invtrecid, vContLp,vqty) 
{
	nlapiLogExecution('DEBUG', 'Into CreateSTGInvtRecord (Container LP)',vContLp);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');			
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of CreateSTGInvtRecord');
}
function getOpenTasksCount(clusno,containerlpno)
{
	nlapiLogExecution('DEBUG', 'into getOpenTasksCount');
	nlapiLogExecution('DEBUG', 'cluster no',clusno);
	nlapiLogExecution('DEBUG', 'containerlp no',containerlpno);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK		
	SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	
	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', clusno));

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	return openreccount;

}
function deleteAllocations(invrefno,pickqty,contlpno,ActPickQty)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	try
	{
		var scount=1;
		var invtrecid;
		var InvQOH = 0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(ActPickQty)).toFixed(4));
				}

				Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH) - parseFloat(ActPickQty)).toFixed(4)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Clust Picking : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}

	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
}

function ConfirmPickTasks(vClusterNo,getFetchedContainerNo,ItemNo,BeginLocation,EndLocation)
{
	nlapiLogExecution('DEBUG', 'into ConfirmPickTasks function');
	nlapiLogExecution('DEBUG', 'vClusterNo',vClusterNo);
	nlapiLogExecution('DEBUG', 'getFetchedContainerNo',getFetchedContainerNo);
	nlapiLogExecution('DEBUG', 'ItemNo',ItemNo);
	nlapiLogExecution('DEBUG', 'BeginLocation',BeginLocation);
	nlapiLogExecution('DEBUG', 'EndLocation',EndLocation);

	var SOFilters = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

	if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
	}

	if(ItemNo != null && ItemNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', ItemNo));
	}

	if(BeginLocation != null && BeginLocation != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', BeginLocation));
	}

	if(getFetchedContainerNo != null && getFetchedContainerNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));
	}

	var SOColumns = new Array();				
	SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
	SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
	SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
	SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc');
	SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');					
	SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location');
	SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id',null);
	SOColumns[7] = new nlobjSearchColumn('custrecord_container_lp_no');
	SOColumns[8] = new nlobjSearchColumn('custrecord_container');
	SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');

	SOColumns[0].setSort();	//SKU
	SOColumns[2].setSort();	//Location
	SOColumns[7].setSort();//Container LP

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if (SOSearchResults != null && SOSearchResults.length > 0) 
	{	
		for (var i = 0; i < SOSearchResults.length; i++)
		{
			var RcId=SOSearchResults[i].getId();
			var PickQty = SOSearchResults[i].getValue('custrecord_expe_qty');
			var EndLocation=SOSearchResults[i].getValue('custrecord_actbeginloc');
			var vdono=SOSearchResults[i].getValue('custrecord_ebiz_cntrl_no');

			var opentaskcount=0;

			opentaskcount=getOpenTasksCount(vClusterNo,getFetchedContainerNo);

			nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);

			//For fine tuning pick confirmation and doing autopacking in user event after last item
			var IsitLastPick='F';
			if(opentaskcount > 1)
				IsitLastPick='F';
			else
				IsitLastPick='T';

			UpdateRFOpenTask(RcId, PickQty, '', getFetchedContainerNo, '','', EndLocation,'',
					PickQty,IsitLastPick,'','');

			UpdateRFFulfillOrdLine(vdono,PickQty);
		}		
	}

	nlapiLogExecution('DEBUG', 'out of ConfirmPickTasks function');
}
function UpdateRFOpenTask(RcId, PickQty, containerInternalId, getEnteredContainerNo, itemCube,
		itemWeight, EndLocation,vReason,ActPickQty,IsitLastPick,ExceptionFlag,NewRecId)
{
	nlapiLogExecution('DEBUG', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	nlapiLogExecution('DEBUG', 'IsitLastPick: ', IsitLastPick);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	if(PickQty!= null && PickQty !="")
		transaction.setFieldValue('custrecord_act_qty', parseFloat(PickQty).toFixed(4));
	if(containerInternalId!= null && containerInternalId !="")
		transaction.setFieldValue('custrecord_container', containerInternalId);
	if(EndLocation!=null && EndLocation!="")
		transaction.setFieldValue('custrecord_actendloc', EndLocation);	
	if(itemWeight!=null && itemWeight!="")
		transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
	if(itemCube!=null && itemCube!="")
		transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
	if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
		transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
	if(vReason!=null && vReason!="")
		transaction.setFieldValue('custrecord_notes', vReason);	
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	nlapiLogExecution('DEBUG', 'IsitLastPick: ', IsitLastPick);
	transaction.setFieldValue('custrecord_device_upload_flag',IsitLastPick);
	var vemployee = request.getParameter('custpage_employee');
	nlapiLogExecution('DEBUG', 'vemployee :', vemployee);
	nlapiLogExecution('DEBUG', 'currentUserID :', currentUserID);

	nlapiLogExecution('DEBUG', 'Updating RF Open Task Record Id: ', RcId);
	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('DEBUG', 'Updated RF Open Task successfully');

	//code added on 15Feb by suman.
	if(ExceptionFlag!='L')
		deleteAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty);
	else
	{
		deleteOldAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty);
		deleteNewAllocations(NewRecId,PickQty,getEnteredContainerNo,ActPickQty);
	}
	//End of code as of 15Feb.
}
function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	var pickqtyfinal =0;
	nlapiLogExecution('DEBUG', 'into UpdateRFFulfillOrdLine : ', vdono);

	var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);
	doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	doline.setFieldValue('custrecord_upddate', DateStamp());

	var oldpickQty=doline.getFieldValue('custrecord_pickqty');

	if(isNaN(oldpickQty) || oldpickQty == null || oldpickQty == '')
		oldpickQty=0;

	nlapiLogExecution('DEBUG', 'parseFloat(oldpickQty) in UpdateRFFulfillOrdLine', parseFloat(oldpickQty));	

	pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);
	nlapiLogExecution('DEBUG', 'pickqtyfinal in blocklevel', pickqtyfinal);	
	doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal).toFixed(4));
	doline.setFieldValue('custrecord_pickgen_qty', parseFloat(pickqtyfinal).toFixed(4));

	nlapiSubmitRecord(doline, false, true);
}
function deleteNewAllocations(invrefno,pickqty,contlpno,ActPickQty)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	try
	{
		var scount=1;
		var invtrecid;
		var InvQOH =0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');



				Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH) - parseFloat(ActPickQty)).toFixed(4)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
}

function IsInvtonHold(ItemInternalId,EndLocInternalId)
{
	nlapiLogExecution('DEBUG', 'Into IsInvtonHold');
	nlapiLogExecution('DEBUG', 'ItemInternalId', ItemInternalId);
	nlapiLogExecution('DEBUG', 'EndLocInternalId', EndLocInternalId);

	var holdflag='F';

	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', ItemInternalId)); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', EndLocInternalId)); // Task Type - PICK	 
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));

	SOColumns[0] = new nlobjSearchColumn('custrecord_ebiz_cycl_count_hldflag' ); 
	SOColumns[1] = new nlobjSearchColumn('custrecord_ebiz_invholdflg' ); 

	var searchresults = nlapiSearchRecord( 'customrecord_ebiznet_createinv', null, SOFilters, SOColumns ); 
	if(searchresults!=null && searchresults!='')
	{
		for(var z=0; z<searchresults.length;z++) 
		{										
			var cyclholdflag=searchresults[z].getValue('custrecord_ebiz_cycl_count_hldflag');
			var invtholdflag=searchresults[z].getValue('custrecord_ebiz_invholdflg');

			if(invtholdflag=='T')
			{
				holdflag='T';
				nlapiLogExecution('DEBUG', 'Out of IsInvtonHold',holdflag);
				return holdflag;
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of IsInvtonHold',holdflag);

	return holdflag;	
}
