/***************************************************************************
   eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_inventoryMoveReportQB_SL.js,v $
 *     	   $Revision: 1.3.14.9.2.1 $
 *     	   $Date: 2014/08/14 10:15:17 $
 *     	   $Author: sponnaganti $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_50 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_inventoryMoveReportQB_SL.js,v $
 * Revision 1.3.14.9.2.1  2014/08/14 10:15:17  sponnaganti
 * case# 20149950
 * stnd bundle issue fix
 *
 * Revision 1.3.14.9  2014/05/06 15:09:17  sponnaganti
 * case# 20148234
 * (LP is not showing issue)
 *
 * Revision 1.3.14.8  2013/12/02 15:14:24  skreddy
 * Case# 20126112
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.3.14.7  2013/10/29 06:27:49  grao
 * Issue fix related to the 20124841
 *
 * Revision 1.3.14.6  2013/10/28 15:41:12  grao
 * Issue fix related to the 20124841
 *
 * Revision 1.3.14.5  2013/10/25 16:13:13  skreddy
 * Case# 20124841
 * standard bundle  issue fix
 *
 * Revision 1.3.14.4  2013/10/24 13:05:33  rmukkera
 * Case# 20124841
 *
 * Revision 1.3.14.3  2013/10/09 15:40:54  rmukkera
 * Case# 20124841
 *
 * Revision 1.3.14.2  2013/05/08 15:12:00  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.3.14.1  2013/03/08 14:41:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.3  2011/12/14 18:42:03  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed Deployment ids
 *
 * Revision 1.2  2011/12/14 16:24:57  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed Deployment ids
 *
 * Revision 1.1  2011/12/14 14:18:00  mbpragada
 * CASE201112/CR201113/LOG201121
 * Inventory Move report  and pdf new files
 *
 
 *****************************************************************************/
function inventoryMoveReportQbSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		var form = nlapiCreateForm('Inventory Move Report');
		
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
		var todate = form.addField('custpage_todate', 'date', 'To Date');
		var invtlp = form.addField('custpage_invtlp', 'select', 'LP');
		invtlp.addSelectOption('', '');
		
		form.setScript('customscript_inventoryclientvalidations');
		
		var binLocationField = form.addField('custpage_binlocation', 'select', 'Bin Location');
		binLocationField.addSelectOption('', '');
		 
//		var itemField = form.addField('custpage_item', 'multiselect', 'Item','item');	
		
			
		var itemField = form.addField('custpage_item', 'multiselect', 'Item','item');
//		var invtlp = form.addField('custpage_invtlp', 'select', 'LP');
//		invtlp.addSelectOption('', '');

		var reportno = form.addField('custpage_reportno', 'text', 'Report #');

		//case 20126112 start : commented location because it is duplicate one
		//#case 20124841
		//var varLoc = form.addField('custpage_location', 'select', 'Location','location');
		//#case 20124841 end
		//varLoc.addSelectOption('', '');
		//case 20126112 end
		

		//case 20124841 start : to fetch all locations
		var varLoc = form.addField('custpage_location', 'select', 'Location','location');
		//case 20124841 end
		//varLoc.addSelectOption('', '');

		

		var varComp = form.addField('custpage_company', 'select', 'Company');
		varComp.addSelectOption('', '');


		/*var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, null, new nlobjSearchColumn('custrecord_ebiz_inv_loc').setSort());

//=======
        //Case #  20124841� Start
		/*var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, null, new nlobjSearchColumn('custrecord_ebiz_inv_loc').setSort());
		//Case #  20124841� END
//>>>>>>> 1.3.14.5
		if (searchlocresults != null)
		{
			nlapiLogExecution('ERROR', 'Inventory count', searchlocresults.length);

			for (var i = 0; i < searchlocresults.length; i++) 
			{
				var res = form.getField('custpage_location').getSelectOptions(searchlocresults[i].getText('custrecord_ebiz_inv_loc',null,'group'), 'is');
				if (res != null) 
				{
					if (res.length > 0)	                
						continue;	                
				}
				varLoc.addSelectOption(searchlocresults[i].getValue('custrecord_ebiz_inv_loc',null,'group'), searchlocresults[i].getText('custrecord_ebiz_inv_loc',null,'group'));
			}
		} */     

		var searchcompresults = nlapiSearchRecord('customrecord_ebiznet_company', null, new nlobjSearchFilter('isinactive',null,'is','F'), new nlobjSearchColumn('Name').setSort());

		if (searchcompresults != null)
		{
			for (var i = 0; i < searchcompresults.length; i++) 
			{
				var res = form.getField('custpage_company').getSelectOptions(searchcompresults[i].getValue('Name'), 'is');
				if (res != null) 
				{
					if (res.length > 0) 
						continue;                
				}
				if(searchcompresults[i].getValue('Name') != "")
					varComp.addSelectOption(searchcompresults[i].getValue('Name'), searchcompresults[i].getValue('Name'));
			}
		}

		//3 - In bound Storage
		//19 - Inventory Storage
		var filtersinv = new Array();
		filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		
		var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);

		if (searchInventory != null) 
		{
			for (var i = 0; i < searchInventory.length; i++) 
			{
				var res = form.getField('custpage_binlocation').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_binloc'), 'is');
				if (res != null) 
				{
					if (res.length > 0) 
						continue;                    
				}
				binLocationField.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_binloc'), searchInventory[i].getText('custrecord_ebiz_inv_binloc'));
			}
			//case# 20148234 starts (LP is not showing issue)
			FillLP(form,invtlp,-1);
			/*for (var i = 0; i < searchInventory.length; i++) 
			{
				invtlp.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_lp'), searchInventory[i].getValue('custrecord_ebiz_inv_lp'));
			}*/
			//case# 20148234 end
			var vItemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
			vItemStatus.addSelectOption('', '');
			var filtersStatus = new Array();
			filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			 

			var columnsstatus = new Array();
			columnsstatus.push(new nlobjSearchColumn('custrecord_statusdescriptionskustatus'));
			 
			var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersStatus, columnsstatus);

			if (searchStatus != null) 
			{
				for (var i = 0; i < searchStatus.length; i++) 
				{
					var res = form.getField('custpage_itemstatus').getSelectOptions(searchStatus[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
					if (res != null) 
					{
						if (res.length > 0) 
							continue;                    
					}
					vItemStatus.addSelectOption(searchStatus[i].getId(), searchStatus[i].getValue('custrecord_statusdescriptionskustatus'));
				}
			}
			var button = form.addSubmitButton('Display');
		}
		else 
		{
			var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'No Records', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		}

		response.writePage(form);
	} 
	else 
	{  
		var invArray = new Array();
		invArray["custpage_checkfield"] = request.getParameter('custpage_checkfield');
		invArray["custpage_binlocation"] = request.getParameter('custpage_binlocation');
		invArray["custpage_item"] = request.getParameter('custpage_item');
		invArray["custpage_invtlp"] = request.getParameter('custpage_invtlp'); 
		invArray["custpage_location"] = request.getParameter('custpage_location');
		invArray["custpage_company"] = request.getParameter('custpage_company');
		invArray["custpage_itemstatus"] = request.getParameter('custpage_itemstatus');
		invArray["custpage_fromdate"] = request.getParameter('custpage_fromdate');
		invArray["custpage_todate"] = request.getParameter('custpage_todate');
		invArray["custpage_reportno"] = request.getParameter('custpage_reportno');
		
		response.sendRedirect('SUITELET', 'customscript_inv_move_report',
				'customdeploy_inv_move_report_di', false, invArray);
		
 
	}
}

//case# 20148234 starts (LP is not showing issue)
function FillLP(form,invtlp,maxno)
{
	var filtersinv = new Array();
	filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);
	if(maxno!=-1)
	{
		nlapiLogExecution('ERROR', 'maxno', maxno);
		filtersinv.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[3] = new nlobjSearchColumn('internalid').setSort(true);
	var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);
	nlapiLogExecution('ERROR', 'searchInventory', searchInventory);
	for (var i = 0; i < searchInventory.length; i++) 
	{
		/*var res = form.getField('custpage_invtlp').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_lp'), 'is');
		if (res != null) 
		{
			if (res.length > 0) 
				continue;                    
		}*/
		invtlp.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_lp'), searchInventory[i].getValue('custrecord_ebiz_inv_lp'));
	}
	if(searchInventory!=null && searchInventory.length>=1000)
	{
		var maxno=searchInventory[searchInventory.length-1].getValue('internalid');	
		nlapiLogExecution('ERROR', 'maxno', maxno);
		FillLP(form, invtlp,maxno);	
	}

}
//case# 20148234 end

