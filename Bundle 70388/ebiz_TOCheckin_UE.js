/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/UserEvents/ebiz_TOCheckin_UE.js,v $
 *     	   $Revision: 1.2.2.3.4.2.4.3.2.1 $
 *     	   $Date: 2015/10/20 10:49:33 $
 *     	   $Author: grao $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_44 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_TOCheckin_UE.js,v $
 * Revision 1.2.2.3.4.2.4.3.2.1  2015/10/20 10:49:33  grao
 *  CT issue fixes 201415135
 *
 * Revision 1.2.2.3.4.2.4.3  2015/04/13 09:26:10  skreddy
 * Case# 201412323
 * changed the url path which was hard coded
 *
 * Revision 1.2.2.3.4.2.4.2  2015/03/30 13:48:18  schepuri
 * case# 201412196
 *
 * Revision 1.2.2.3.4.2.4.1  2014/03/04 08:58:22  rmukkera
 * Case # 20127196
 *
 * Revision 1.2.2.3.4.2  2012/12/03 15:44:52  rmukkera
 * CASE201112/CR201113/LOG2012392
 * UOM conversions code
 * fix
 *
 * Revision 1.2.2.3.4.1  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.3  2012/08/17 13:50:22  schepuri
 * CASE201112/CR201113/LOG201121
 * commented calling fulfillment
 *
 * Revision 1.2.2.2  2012/05/28 15:42:45  spendyala
 * CASE201112/CR201113/LOG201121
 * wbc issue related to provide links when the status is partial fulfillment /receive status.
 *
 * Revision 1.2.2.1  2012/05/15 22:50:54  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Receive button diable in TO
 *
 * Revision 1.2  2011/11/25 10:15:00  rmukkera
 * CASE201112/CR201113/LOG201121
 * new file
 *
 * 
 * 
 *****************************************************************************/



/***************************************************
 * This is a User Event Script that generates a
 * link for each line item that points to the
 * Suitelet page above.  It is meant to be deployed
 * on the Purchase Order record.
 */
function TOAddCheckInLinkUE(type, form, request){
	//obtain the context object
	var ctx = nlapiGetContext();

	//only execute when the exec context is web browser and in view mode of the PO
	if (ctx.getExecutionContext() == 'userinterface' &&
			type == 'view') {
		var poid = nlapiGetRecordId();
		var checkinqty=0;
		var poname = nlapiGetFieldValue('tranid');
		var OrdStatus = nlapiGetFieldValue('orderstatus');
		var OrdStatustext = nlapiGetFieldText('orderstatus');
		var status=nlapiGetFieldText('status');
		var vLocation=nlapiGetFieldValue('location');
		var ToLocation=nlapiGetFieldValue('transferlocation');
		//resolve the generic relative URL for the Suitelet
		var URL;var URLText;var mwhsiteflag='F';var Tomwhsiteflag='F';
		nlapiLogExecution('Error', 'status', status);
		var checkInURL = nlapiResolveURL('SUITELET', 'customscript_to_checkin_sl', 'customdeploy_ebiznettocheckin_di');
		var dolinkURL = nlapiResolveURL('SUITELET', 'customscript_ebiznet_deliveryorder', 'customdeploy_deliveryorder');
		nlapiLogExecution('Error', 'vLocation', vLocation);
                  nlapiLogExecution('Error', 'OrdStatus ', OrdStatus);
		var boolfound=false;

		if(vLocation!=null && vLocation!='')
		{
			var fields = ['custrecord_ebizwhsite'];

			var locationcolumns = nlapiLookupField('Location', vLocation, fields);
			if(locationcolumns!=null)
				mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
		}
		if(ToLocation!=null && ToLocation!='')
		{
			var fields = ['custrecord_ebizwhsite'];

			var locationcolumns = nlapiLookupField('Location', ToLocation, fields);
			if(locationcolumns!=null)
				Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;
		}
		nlapiLogExecution('Error', 'mwhsiteflag', mwhsiteflag);
		nlapiLogExecution('Error', 'Tomwhsiteflag', Tomwhsiteflag);
		
		try
		{ 

				if(Tomwhsiteflag=='T')
				{
					var receivebtn= form.getButton('receive');  
					nlapiLogExecution('ERROR', 'receivebtn', receivebtn);
					if(receivebtn!=null)
						receivebtn.setDisabled(true);
				}
			
		}
		catch (exp) 
		{
			nlapiLogExecution('ERROR', 'Exception in hiding receive button : ', exp);		
		}
          //OrdStatus=>'B'Pending Fulfillment 
		if(OrdStatus=='B' && mwhsiteflag=='T')
		{
			boolfound=true;
			URL=dolinkURL;
			URLText='FulfillmentOrder';
			//form.getSubList('item').addField('custpage_checkin_link', 'text', 'CreateFullfilment Order', null);
		}
		else 
		{
			//OrdStatus=>'B'Pending Receipt ordStatus=>'E' partially fulfilled/received.
			if(Tomwhsiteflag=='T' && (OrdStatus=='F'||OrdStatus=='E'))
			{
				boolfound=true;
				URL=checkInURL;
				URLText='Check In';
				form.getSubList('item').addField('custpage_checkin_link', 'text', 'Check-In', null);
			}
			
		}
		if(boolfound==true)
		{
			nlapiLogExecution('Error', 'URL', URL);
			nlapiLogExecution('Error', 'check in url', checkInURL);
			nlapiLogExecution('Error', 'poid', poid);
			nlapiLogExecution('Error', 'OrdStatus', OrdStatus);
			nlapiLogExecution('Error', 'OrdStatustext', OrdStatustext);


			//URL = getFQDNForHost(ctx.getEnvironment()) + URL;
			nlapiLogExecution('Error', 'check in url', URL);

			for (var i = 0; i < form.getSubList('item').getLineItemCount(); i++) {

				var quantity = parseFloat(form.getSubList('item').getLineItemValue('quantity', i + 1));
				var quantityRcv = parseFloat(form.getSubList('item').getLineItemValue('quantityreceived', i + 1));
				var quantityFulfill = parseFloat(form.getSubList('item').getLineItemValue('quantityfulfilled', i + 1));
				var quantitycommitted = parseFloat(form.getSubList('item').getLineItemValue('quantitycommitted', i + 1));

				var IsClosed=nlapiGetLineItemValue('item','isclosed',i + 1);

				var RemQty = (parseFloat(quantity) - parseFloat(quantityRcv));
				nlapiLogExecution('ERROR', 'quantity info', 'Quantity: ' + quantity + '<br>' + 'Quantity Received: ' + quantityRcv);
				//Case # 20127196 Start
				var poName = nlapiGetFieldValue('tranid');
				var lineno = form.getSubList('item').getLineItemValue('line', i + 1);
				var item = form.getSubList('item').getLineItemValue('item', i + 1);
				var checkinqty=GetPOcheckinqty(poName,lineno,item,poid);// case# 201412196
				if(checkinqty==null || checkinqty=='')
					checkinqty=0;
				nlapiLogExecution('DEBUG', 'checkinqty', checkinqty);

				if(IsClosed!='T') //If line is closed then check in link is not populated for the selected line.
				{

					//generate the hyperlink only when the line has not been fully received
					if (quantityRcv < quantity && (parseFloat(quantity)> parseFloat(checkinqty))) {
						//Case # 20127196 End
						var linkURL;

					if((OrdStatus=='B')&&(quantitycommitted !=0))
					{
						//add the SO ID, item, and line to the URL
						linkURL = dolinkURL + '&custparam_soid=' + poid;
						linkURL = linkURL + '&custparam_sovalue=' + poname;
						linkURL = linkURL + '&custparam_ordtype=' + 'TRANSFER';
						linkURL = linkURL + '&custparam_so_line_item=' + nlapiGetLineItemValue('item', 'item', i + 1);
						linkURL = linkURL + '&custparam_so_line_num=' + nlapiGetLineItemValue('item', 'line', i + 1);
						linkURL = linkURL + '&custparam_ordqty=' + nlapiGetLineItemValue('item', 'quantity', i + 1);
						linkURL = linkURL + '&custparam_comtdqty=' + nlapiGetLineItemValue('item', 'quantitycommitted', i + 1);
form.getSubList('item').setLineItemValue('custpage_checkin_link', i + 1, '<a href="' + linkURL + '">'+URLText+'</a>');
					}
					else if(quantityFulfill!=0)
					{             	
						linkURL = checkInURL + '&poID=' + poid;
						linkURL = linkURL + '&poValue=' + poname;
						linkURL = linkURL + '&QtyChkn=' + quantityRcv;
						linkURL = linkURL + '&QtyRem=' + RemQty;
						linkURL = linkURL + '&poLineItem=' + nlapiGetLineItemValue('item', 'item', i + 1);
//						linkURL = linkURL + '&poLineQty=' + nlapiGetLineItemValue('item', 'quantity', i + 1);
						linkURL = linkURL + '&poLineQty=' + nlapiGetLineItemValue('item', 'quantityfulfilled', i + 1);
						linkURL = linkURL + '&poLineNum=' + nlapiGetLineItemValue('item', 'line', i + 1);
						linkURL = linkURL + '&poSerialNos=' + nlapiGetLineItemValue('item', 'serialnumbers', i + 1);
						linkURL = linkURL + '&poItemStatus=' + nlapiGetLineItemValue('item', 'custcol_ebiznet_item_status', i + 1);
						linkURL = linkURL + '&poItemUOM=' + nlapiGetLineItemValue('item', 'units', i + 1);
form.getSubList('item').setLineItemValue('custpage_checkin_link', i + 1, '<a href="' + linkURL + '">'+URLText+'</a>');


						}
						//log the URL
						nlapiLogExecution('DEBUG', 'check in url', linkURL);


					}

				}
			}
		}
	}
}
//Case # 20127196 Start
function GetPOcheckinqty(poName,lineno,item,poid)
{

	var POcheckinqty;

	var filter=new Array();
//case # 20124143 
	if(poName!=null && poName!='')
	{
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_order_no',null,'is',poName));
filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no',null,'equalto',poid));
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no',null,'equalto',lineno));
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_item',null,'anyof',item));
		nlapiLogExecution('ERROR','poName', poName);
		nlapiLogExecution('ERROR','lineno', lineno);
		nlapiLogExecution('ERROR','item', item);

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');

		var searchrecord=nlapiSearchRecord('customrecord_ebiznet_order_line_details',null,filter,column);
		if(searchrecord!=null && searchrecord!='')
		{
			POcheckinqty=searchrecord[0].getValue('custrecord_orderlinedetails_checkin_qty');
		}
	}
// case #  20124143 end
	return POcheckinqty;
}
//Case # 20127196 End
