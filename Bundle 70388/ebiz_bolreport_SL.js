
/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_bolreport_SL.js,v $
 *     	   $Revision: 1.4.10.2.4.19.2.1 $
 *     	   $Date: 2015/10/20 08:08:32 $
 *     	   $Author: nneelam $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_43 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_bolreport_SL.js,v $
 * Revision 1.4.10.2.4.19.2.1  2015/10/20 08:08:32  nneelam
 * case# 201415059
 *
 * Revision 1.4.10.2.4.19  2014/08/18 15:25:37  sponnaganti
 * Case# 20149994
 * Stnd Bundle Issue fix
 *
 * Revision 1.4.10.2.4.18  2014/04/23 15:40:23  nneelam
 * case#  20148126
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.10.2.4.17  2014/04/17 14:23:46  rmukkera
 * Case # 20141467
 *
 * Revision 1.4.10.2.4.16  2014/03/26 15:34:18  skavuri
 * Case # 20127833 issue fixed
 *
 * Revision 1.4.10.2.4.15  2014/03/20 15:44:20  nneelam
 * case#  20127754
 * Standard Bundle Issue Fix.
 *
 * Revision 1.4.10.2.4.14  2014/03/11 14:53:56  sponnaganti
 * case# 20127634
 * (Item name not displaying issue)
 *
 * Revision 1.4.10.2.4.13  2014/03/11 14:51:23  sponnaganti
 * case# 20127634
 * (Item name not displaying issue)
 *
 * Revision 1.4.10.2.4.12  2014/02/21 15:15:38  rmukkera
 * no message
 *
 * Revision 1.4.10.2.4.11  2014/02/17 14:47:42  rmukkera
 * Case # 20127183
 *
 * Revision 1.4.10.2.4.10  2014/02/13 15:31:00  skavuri
 * case # 20127121 (we can not edit the item after checkin in po) is fixed
 *
 * Revision 1.4.10.2.4.9  2014/02/11 15:49:40  skavuri
 * CAse # 20127099 (bolreport working)
 *
 * Revision 1.4.10.2.4.8  2014/02/10 15:05:51  sponnaganti
 * case# 20127100
 * (if condition removed)
 *
 * Revision 1.4.10.2.4.7  2014/01/10 16:07:09  skreddy
 * case# 20126590
 * LL Prod issue
 *
 * Revision 1.4.10.2.4.6  2013/11/12 05:45:33  vmandala
 * Case# 20124760
 * ClicktoShop sb Ffx
 *
 * Revision 1.4.10.2.4.5  2013/10/28 15:44:50  grao
 * Issue fix related to the 20125355
 *
 * Revision 1.4.10.2.4.4  2013/10/04 13:10:50  grao
 * issue fixes related to the Case#:20124760
 *
 * Revision 1.4.10.2.4.3  2013/05/14 14:53:39  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.4.10.2.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.4.10.2.4.1  2013/03/08 14:45:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.4.10.2  2012/11/01 14:55:01  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4.10.1  2012/09/26 22:43:33  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.4  2011/12/13 09:45:38  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/10/05 11:55:21  schepuri
 * CASE201112/CR201113/LOG201121
 * BOL Report PDF
 *
 * Revision 1.2  2011/10/03 06:53:20  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1  2011/09/30 14:25:41  schepuri
 * CASE201112/CR201113/LOG201121
 * BOL Report
 *
 **********************************************************************************************************************/
function BolReportSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('BOL Report');		
		form.setScript('customscript_bolreport');

		//Appointment/Trip#
		var bolField = form.addField('custpage_trailer', 'select', 'Appointment/Trip#').setMandatory(true);
		bolField.setLayoutType('startrow', 'none');
		var shipLPFilers = new Array();
//		var searchResultArray=new Array();
		var shipLPColumns = new Array();
		shipLPColumns[0] = new nlobjSearchColumn('name');
		shipLPColumns[1] = new nlobjSearchColumn('internalid');
		shipLPColumns[1].setSort();
		//var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, null, shipLPColumns);
		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, null, shipLPColumns);
		bolField.addSelectOption("", "");
		if(SearchResults !=null && SearchResults !=""){
			for (var i = 0; i < SearchResults.length; i++) {
				//shipLPId = shipLPSearchResults[0].getId();
				if(SearchResults[i].getValue('name')!=null && SearchResults[i].getValue('name')!="")
				{
					var res=  form.getField('custpage_trailer').getSelectOptions(SearchResults[i].getId(), 'is');
					//var res=  form.getField('custpage_trailer').getSelectOptions(SearchResults[i].getValue('name'), 'is');
					nlapiLogExecution('ERROR','res', +res);
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					bolField.addSelectOption(SearchResults[i].getId(), SearchResults[i].getValue('name'));
					//bolField.addSelectOption(SearchResults[i].getValue('name'), SearchResults[i].getValue('name'));
				}
			}
			if(SearchResults.length>=1000)
			{
				var maxno=SearchResults[SearchResults.length-1].getValue(shipLPColumns[1]);
				var searchresult=getAppointmentSearchResults(maxno);
//				if(searchResult!=null)
//				{
				for (var i = 0; i < searchresult.length; i++) {
					//shipLPId = shipLPSearchResults[0].getId();
					var search=searchresult[i];
					for (var k1 = 0; k1 < search.length; k1++) {
						if(search[k1].getValue('name')!=null && search[k1].getValue('name')!="")
						{
							var res=  form.getField('custpage_trailer').getSelectOptions(search[k1].getId(), 'is');
							//var res=  form.getField('custpage_trailer').getSelectOptions(SearchResults[i].getValue('name'), 'is');
							nlapiLogExecution('ERROR','res', +res);
							if (res != null) {				
								if (res.length > 0) {
									continue;
								}
							}

							bolField.addSelectOption(search[k1].getId(), search[k1].getValue('name'));
							//bolField.addSelectOption(SearchResults[i].getValue('name'), SearchResults[i].getValue('name'));
						}
					}
				}
				//}
			}
		}


		//Fulfillment Order 
		/*var DoField = form.addField('custpage_ord', 'select', 'Fulfillment Ord#');
		DoField.setLayoutType('startrow', 'none');
		var filtersdo = new Array();
		filtersdo[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3');   //Pick Task
//		filtersdo[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9,26']); //statusFalg='G'
		filtersdo[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['14']);
		DoField.addSelectOption("", "");

		var searchresultsdo = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersdo, new nlobjSearchColumn('name'));		

		if (searchresultsdo != null && searchresultsdo != "") {
			for (var i = 0; i < searchresultsdo.length; i++) {

				var res=  form.getField('custpage_ord').getSelectOptions(searchresultsdo[i].getValue('name'), 'is');
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}

				DoField.addSelectOption(searchresultsdo[i].getValue('name'), searchresultsdo[i].getValue('name'));
			}
		}*/
		
		
		var DoField = form.addField('custpage_ord', 'select', 'Fulfillment Ord#');
		DoField.setLayoutType('startrow', 'none'); 
		DoField.addSelectOption("","");
		FillOrder(form, DoField,-1);
		
		var sysdate=DateStamp();
		//var shipdate = form.addField('custpage_shipdate', 'text', 'Ship Date').setDefaultValue(sysdate);
		var shipdate = form.addField('custpage_shipdate', 'date', 'Ship Date').setDefaultValue(sysdate);

		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else //this is the POST block
	{
		var form = nlapiCreateForm('BOL Report');
		form.setScript('customscript_bolreport');
		var vebizbolno = request.getParameter('custpage_trailer');
		//Case Start 20125355� script excution exceed relates issue fixed
		var vshipdate = request.getParameter('custpage_shipdate');
		//end
		
		nlapiLogExecution('ERROR','vebizbolno',vebizbolno);

		var vfullfillno = request.getParameter('custpage_ord');
		nlapiLogExecution('ERROR','vfullfillno',vfullfillno);
		// var searchResultArray=new Array();
		//Appointment/Trip#
		var bolField = form.addField('custpage_trailer', 'select', 'Appointment/Trip#').setMandatory(true);
		bolField.setLayoutType('startrow', 'none');
		var shipLPFilers = new Array();

		var shipLPColumns = new Array();
		shipLPColumns[0] = new nlobjSearchColumn('name');
		shipLPColumns[1] = new nlobjSearchColumn('internalid');
		shipLPColumns[1].setSort();
		//var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, null, shipLPColumns);
		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, null, shipLPColumns);
		bolField.addSelectOption("", "");
		if(SearchResults !=null && SearchResults !=""){
			for (var i = 0; i < SearchResults.length; i++) {
				//shipLPId = shipLPSearchResults[0].getId();
				if(SearchResults[i].getValue('name')!=null && SearchResults[i].getValue('name')!="")
				{
					var res=  form.getField('custpage_trailer').getSelectOptions(SearchResults[i].getId(), 'is');
					//var res=  form.getField('custpage_trailer').getSelectOptions(SearchResults[i].getValue('name'), 'is');
					nlapiLogExecution('ERROR','res', +res);
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					bolField.addSelectOption(SearchResults[i].getId(), SearchResults[i].getValue('name'));
					//bolField.addSelectOption(SearchResults[i].getValue('name'), SearchResults[i].getValue('name'));
				}
			}
			if(SearchResults.length>=1000)
			{
				var maxno=SearchResults[SearchResults.length-1].getValue(shipLPColumns[1]);
				var searchresult=getAppointmentSearchResults(maxno);
//				if(searchResult!=null)
//				{
				for (var i = 0; i < searchresult.length; i++) {
					//shipLPId = shipLPSearchResults[0].getId();
					var search=searchresult[i];
					for (var k1 = 0; k1 < search.length; k1++) {
						if(search[k1].getValue('name')!=null && search[k1].getValue('name')!="")
						{
							var res=  form.getField('custpage_trailer').getSelectOptions(search[k1].getId(), 'is');
							//var res=  form.getField('custpage_trailer').getSelectOptions(SearchResults[i].getValue('name'), 'is');
							nlapiLogExecution('ERROR','res', +res);
							if (res != null) {				
								if (res.length > 0) {
									continue;
								}
							}

							bolField.addSelectOption(search[k1].getId(), search[k1].getValue('name'));
							//bolField.addSelectOption(SearchResults[i].getValue('name'), SearchResults[i].getValue('name'));
						}
					}
				}
				//}
			}
			//case 20126590 start set default value
			if(vebizbolno !=null && vebizbolno !='')
				bolField .setDefaultValue(vebizbolno);
			//case 20126590 end
		}


		//Fulfillment Order 
		/*var DoField = form.addField('custpage_ord', 'select', 'Fulfillment Ord#');
		DoField.setLayoutType('startrow', 'none');

		var filtersdo = new Array();
		filtersdo[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3');   //Pick Task
//		filtersdo[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9,26']); //statusFalg='G'
		filtersdo[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['14']);
		DoField.addSelectOption("", "");

		var searchresultsdo = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersdo, new nlobjSearchColumn('name'));		
		if (searchresultsdo != null && searchresultsdo != "") {
			for (var i = 0; i < searchresultsdo.length; i++) {

				var res=  form.getField('custpage_ord').getSelectOptions(searchresultsdo[i].getValue('name'), 'is');
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}

				DoField.addSelectOption(searchresultsdo[i].getValue('name'), searchresultsdo[i].getValue('name'));
			}
		}*/
		
		var DoField = form.addField('custpage_ord', 'select', 'Fulfillment Ord#');
		DoField.setLayoutType('startrow', 'none'); 
		DoField.addSelectOption("","");
		FillOrder(form, DoField,-1);
		//case 20126590 start set default value
		if(vfullfillno !=null && vfullfillno !='')
		DoField .setDefaultValue(vfullfillno);
		//case 20126590 end
		
		var sysdate=DateStamp();
		//var shipdate = form.addField('custpage_shipdate', 'text', 'Ship Date').setDefaultValue(sysdate);
		var shipdate = form.addField('custpage_shipdate', 'date', 'Ship Date');
		//case 20126590 start set default value
        if(vshipdate != null && vshipdate !='')
        	shipdate.setDefaultValue(vshipdate);
        else
        	shipdate.setDefaultValue(sysdate)
		//case 20126590 end


		var trailerName = getTrailerName(vebizbolno);


		var sublist = form.addSubList("custpage_items", "staticlist", "ItemList");
		sublist.addField("custpage_noofcases", "text", "No.of Cases");
		sublist.addField("custpage_nmfcitemno", "text", "NMFC Item #");
		sublist.addField("custpage_desc", "text", "Description");
		sublist.addField("custpage_cubes", "text", "Cubes");
		sublist.addField("custpage_totalwt", "text", "Total Weight in LBS");

		var ExpQty,Item,NoofCases="",NMFCitem,Desc,Cube,Weight,waveno,ordno,fulfillmentordno,shipLP;

		// to fetch wave no against trailerno from open task
		var filters = new Array();
		//case # 20127099 starts
		filters.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerName));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 4));//ship task
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','10','28']));
		nlapiLogExecution('ERROR', 'vshipdate',vshipdate);
		//Case Start 20125355? script excution exceed relates issue fixed
		//Case # 20127183�Start
		if(vshipdate!=null&&vshipdate!=''&&vshipdate!='null')//case # 20127099 end
		{
			filters.push(new nlobjSearchFilter('custrecord_current_date', null, 'onorbefore', vshipdate));
		}
		else
		{
			nlapiLogExecution('ERROR', 'DateStamp()',DateStamp());
			filters.push(new nlobjSearchFilter('custrecord_current_date', null, 'onorbefore', DateStamp()));	
		}
		//Case # 20127183� End
		//end
		if(vfullfillno !=null && vfullfillno !=""){
			filters.push(new nlobjSearchFilter('name', null, 'is', vfullfillno));
			}	
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[1] = new nlobjSearchColumn('custrecord_ship_lp_no');

		var searchresultsWaveNo = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		var PrevItems;
		var PrevItemsName;//added by surendra
		var PrevtotCubeQty=0,PrevtotWtQty=0;
		var totCubeQty=0,totWtQty=0;
		var NMFCitemGroup="",NMFCitemFamily="",NMFCitem="",ItemName="";
		var searchresultsopentask;
	//case 20124760
		if(searchresultsWaveNo != null && searchresultsWaveNo != "")
		{
			var ContLP;
			var vPrevContLP;
			var ContCount=0;
			var p=0;
			nlapiLogExecution('ERROR', 'before if',ContCount);
			for (var i = 0; searchresultsWaveNo != null && i < searchresultsWaveNo.length; i++) 
			{
				waveno = searchresultsWaveNo[i].getValue('custrecord_ebiz_wave_no');
				shipLP = searchresultsWaveNo[i].getValue('custrecord_ship_lp_no');
				nlapiLogExecution('ERROR', 'waveno',waveno);
				nlapiLogExecution('ERROR', 'shipLP',shipLP);
				var filters =new Array();
				//to fetch details of detailed block of PDF either from closed task or from open task
				if(trailerName !=null && trailerName !=""){
					filters.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerName));}
				if(vfullfillno !=null && vfullfillno !=""){
					filters.push(new nlobjSearchFilter('name', null, 'is', vfullfillno));	}	
				filters.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shipLP));
				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Pick Task
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','10','28']));//Build Ship Units and Trailer Load

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				columns[2] = new nlobjSearchColumn('custrecord_skudesc');
				columns[3] = new nlobjSearchColumn('custrecord_sku');
				columns[4] = new nlobjSearchColumn('custrecord_wms_status_flag');
				columns[5] = new nlobjSearchColumn('custrecord_totalcube');
				columns[6] = new nlobjSearchColumn('custrecord_total_weight');
				columns[7] = new nlobjSearchColumn('custrecord_expe_qty');
				columns[8] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				columns[9] = new nlobjSearchColumn('name');
				columns[1].setSort(true);
				columns[2].setSort(true);
				
				//sri case# 20127099 add
//				if(vfullfillno !=null && vfullfillno !=""){
					var searchresultsopentask = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	//				}
				//case# 20127100 starts (if condition removed)
				//searchresultsopentask = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
				//case# 20127100 end
				//sri case# 20127099 end



				for (var n = 0; searchresultsopentask != null && n < searchresultsopentask.length; n++) 
				{
					ContLP = searchresultsopentask[n].getValue('custrecord_container_lp_no');
					//var searchresultsclosedtasks = searchresultsclosedtask[i];
					ExpQty = searchresultsopentask[n].getValue('custrecord_expe_qty');
					Item = searchresultsopentask[n].getValue('custrecord_sku');
					//case# 20127634 starts 
					ItemName = searchresultsopentask[n].getText('custrecord_sku');
					//case# 20127634 end
					Desc = searchresultsopentask[n].getValue('custrecord_skudesc');
					
					//case # 20127754�getting Desc from item if null in open task.
					if(Desc=='' || Desc==null){
						var itemSubtype;
						if(PrevItems!=Item  && PrevItems!='' && PrevItems!=null)
						itemSubtype = nlapiLookupField('item', PrevItems, ['custitem_ebizdescriptionitems','salesdescription']);
						else
							itemSubtype = nlapiLookupField('item', Item, ['custitem_ebizdescriptionitems','salesdescription']);
						
						if(itemSubtype.salesdescription!=''){
							Desc=itemSubtype.salesdescription;
						}
						else if(itemSubtype.custitem_ebizdescriptionitems!=''){
							Desc=itemSubtype.custitem_ebizdescriptionitems;
						}
					}
					
					ordno = searchresultsopentask[n].getValue('custrecord_ebiz_order_no');
					fulfillmentordno = searchresultsopentask[n].getValue('name');
					var k = searchresultsopentask[n].getValue('custrecord_wms_status_flag');
					nlapiLogExecution('ERROR', 'into opentask CTExpQty',ExpQty);
					//nlapiLogExecution('ERROR', 'into closedtask CTItem',Item);
					//nlapiLogExecution('ERROR', 'into closedtask CTk',k);
					//nlapiLogExecution('ERROR', 'into closedtask CTDesc',Desc);
					//nlapiLogExecution('ERROR', 'into closedtask ordno',ordno);
					//nlapiLogExecution('ERROR', 'into closedtask fulfillmentordno',fulfillmentordno);
					nlapiLogExecution('ERROR', 'into opentask ContLP',ContLP);



						//to fetch NMFC code from item/tem group/item family for a item
						if(Item != null && Item != "")
						{
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('Internalid', null, 'is', Item);

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custitem_ebiz_skunmfccode');
							columns[1] = new nlobjSearchColumn('custitem_item_group');
							columns[2] = new nlobjSearchColumn('custitem_item_family');

							var ItemNMFCCoderesult = nlapiSearchRecord('inventoryitem', null, filters, columns);
						}

						if(ItemNMFCCoderesult != null && ItemNMFCCoderesult != "")
						{
							NMFCitemGroup = ItemNMFCCoderesult[0].getValue('custitem_item_group');
							NMFCitemFamily = ItemNMFCCoderesult[0].getValue('custitem_item_family');
							NMFCitem = ItemNMFCCoderesult[0].getValue('custitem_ebiz_skunmfccode');
						}
						//nlapiLogExecution('ERROR', 'NMFCitem item',NMFCitem);
						///nlapiLogExecution('ERROR', 'NMFCitemGroup item',NMFCitemGroup);
						//nlapiLogExecution('ERROR', 'NMFCitemFamily item',NMFCitemFamily);
						if(NMFCitem == null || NMFCitem == "" )
						{
							if(NMFCitemGroup != null && NMFCitemGroup != "" )
							{
								var filters = new Array();
								filters[0] = new nlobjSearchFilter('Internalid', null, 'is', NMFCitemGroup);

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_ebiz_skugrp_nmfccode');

								var ItemGroupNMFCCoderesult = nlapiSearchRecord('customrecord_ebiznet_sku_group', null, filters, columns);

								if(ItemGroupNMFCCoderesult != null && ItemGroupNMFCCoderesult != "")
								{

									NMFCitem = ItemGroupNMFCCoderesult[0].getValue('custrecord_ebiz_skugrp_nmfccode');
								}
								//nlapiLogExecution('ERROR', 'NMFCitem itemgroup',NMFCitem);
							}


							else if(NMFCitemFamily != null && NMFCitemFamily != "")
							{
								var filters = new Array();
								filters[0] = new nlobjSearchFilter('Internalid', null, 'is', NMFCitemFamily);

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_ebiz_skufam_nmfccode');

								var ItemFamNMFCCoderesult = nlapiSearchRecord('customrecord_ebiznet_sku_family', null, filters, columns);
								if(ItemFamNMFCCoderesult != null && ItemFamNMFCCoderesult != "")
								{
									NMFCitem = ItemFamNMFCCoderesult[0].getValue('custrecord_ebiz_skufam_nmfccode');
								}
								//nlapiLogExecution('ERROR', 'NMFCitem itemfamily',NMFCitem);
							}
						}

						//to fetch totcube,totwt from LP master 

					//var filters = new Array();// Case# 20127833
					var filters = new Array();
					if(shipLP != null && shipLP != "")	
						//Case# 20127833 starts
						//filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', shipLP);
						filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp', null, 'is', shipLP);
						// Case# 20127833 ends
					nlapiLogExecution('ERROR', 'custrecord_ebiz_lpmaster_masterlp',shipLP);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
						columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');

						var searchresultscubewt = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);

						if(searchresultscubewt != null && searchresultscubewt != "")
						{
							Cube = searchresultscubewt[0].getValue('custrecord_ebiz_lpmaster_totcube');
							if(Cube== null || Cube == "")
								Cube='0';
							Weight = searchresultscubewt[0].getValue('custrecord_ebiz_lpmaster_totwght');
							if(Weight== null || Weight == "")
								Weight='0';
						}

						nlapiLogExecution('ERROR', 'Cube',Cube);
						nlapiLogExecution('ERROR', 'Weight',Weight);

						//to fetch no of cases 

						if((Item!=null ||Item!='') && PrevItems!=Item)
						{
							nlapiLogExecution('ERROR', 'if Item is not null',Item);
							if(PrevItems!= null && PrevItems!="")
							{
								nlapiLogExecution('ERROR', 'PrevItems',PrevItems);
								nlapiLogExecution('ERROR', 'Item',Item);
								nlapiLogExecution('ERROR', 'totCubeQty',totCubeQty);
								nlapiLogExecution('ERROR', 'totWtQty',totWtQty);
								nlapiLogExecution('ERROR', 'ContCount',ContCount);



							form.getSubList('custpage_items').setLineItemValue('custpage_noofcases', p + 1, parseFloat(ContCount).toString());
							//case# 20127634 starts (Item name not displaying issue)
							if(NMFCitem!=null && NMFCitem!='')
								form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, NMFCitem);
							else
								//form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, ItemName);	
								form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, PrevItemsName);
							//form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  PrevItems);
							form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  Desc);
							//case# 20127634 end
							form.getSubList('custpage_items').setLineItemValue('custpage_cubes', p + 1, PrevtotCubeQty);
							form.getSubList('custpage_items').setLineItemValue('custpage_totalwt', p + 1, PrevtotWtQty);
							p=p+1;

								PrevItems=Item;
								PrevItemsName=ItemName;
								totCubeQty =parseFloat(Cube);
								totWtQty =parseFloat(Weight);
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);
								nlapiLogExecution('ERROR', 'PrevtotCubeQty',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty',PrevtotWtQty);
								ContCount=1;
								vPrevContLP=ContLP;	

							}
							else
							{
								PrevItems=Item;
								PrevItemsName=ItemName;
								nlapiLogExecution('ERROR', ' ContLP111',ContLP);

								vPrevContLP=ContLP;
								ContCount++;
								if(Cube!=null && Cube!='')
								{
									totCubeQty =parseFloat(Cube);	
								}
								if(Weight!=null && Weight!='')
								{
									totWtQty =parseFloat(Weight);	
									nlapiLogExecution('ERROR', 'totWtQty1',totWtQty);
								}
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);	
								nlapiLogExecution('ERROR', 'PrevtotCubeQty1',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty1',PrevtotWtQty);
							}
						}
						else  if(Item!=null && Item!='')
						{
							nlapiLogExecution('ERROR', 'else if Item is not null vPrevContLP',vPrevContLP);
							nlapiLogExecution('ERROR', 'else if Item is not null ContLP',ContLP);
							if(vPrevContLP!=ContLP)
							{
								nlapiLogExecution('ERROR', 'else if Item is not null',Item);
								if(Cube!=null && Cube!='')
								{
									totCubeQty =parseFloat(totCubeQty)+parseFloat(Cube);	
								}
								if(Weight!=null && Weight!='')
								{
									totWtQty =parseFloat(totWtQty)+parseFloat(Weight);	
									nlapiLogExecution('ERROR', 'totWtQty2',totWtQty);

								}
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);	
								nlapiLogExecution('ERROR', 'PrevtotCubeQty2',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty2',PrevtotWtQty);
								vPrevContLP=ContLP;	
								ContCount++;
							}
						}





					}
					if(PrevItems!= null && PrevItems != "")
					{
						nlapiLogExecution('ERROR', 'ContCount2',ContCount);

					form.getSubList('custpage_items').setLineItemValue('custpage_noofcases', p + 1, parseFloat(ContCount).toString());
					//case# 20127634 starts (Item name not displaying issue)
					if(NMFCitem!=null && NMFCitem!='')
						form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, NMFCitem);
					else
						//form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, ItemName);
						form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, PrevItemsName);
					//form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  PrevItems);
					form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  Desc);
					//case# 20127634 end
					//form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, NMFCitem);
					//form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  PrevItems);
					form.getSubList('custpage_items').setLineItemValue('custpage_cubes', p + 1, PrevtotCubeQty);
					form.getSubList('custpage_items').setLineItemValue('custpage_totalwt', p + 1, PrevtotWtQty);
				}



			}
		}
		
		if (searchresultsopentask=='' || searchresultsopentask==null){
			
	//case 20124760 end
		if(searchresultsWaveNo != null && searchresultsWaveNo != "")
		{
			var ContLP;
			var vPrevContLP;
			var ContCount=0;
			var p=0;
			nlapiLogExecution('ERROR', 'before if',ContCount);
			for (var i = 0; searchresultsWaveNo != null && i < searchresultsWaveNo.length; i++) 
			{
				waveno = searchresultsWaveNo[i].getValue('custrecord_ebiz_wave_no');
				shipLP = searchresultsWaveNo[i].getValue('custrecord_ship_lp_no');
				nlapiLogExecution('ERROR', 'waveno',waveno);
				nlapiLogExecution('ERROR', 'shipLP',shipLP);

					//to fetch details of detailed block of PDF either from closed task or from open task
					var filters = new Array();


					filters[0] = new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', shipLP);
					filters[1] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3]);//Pick Task
					filters[2] = new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', ['7','10']);//Build Ship Units and Trailer Load

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no');
				columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_sku_no');
				columns[2] = new nlobjSearchColumn('custrecord_ebiztask_skudesc');
				columns[3] = new nlobjSearchColumn('custrecord_ebiztask_sku');
				columns[4] = new nlobjSearchColumn('custrecord_ebiztask_wms_status_flag');
				columns[5] = new nlobjSearchColumn('custrecord_ebiztask_totalcube');
				columns[6] = new nlobjSearchColumn('custrecord_ebiztask_total_weight');
				columns[7] = new nlobjSearchColumn('custrecord_ebiztask_expe_qty');
				columns[8] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
				columns[9] = new nlobjSearchColumn('name');
				columns[2].setSort(true);
				columns[0].setSort(true);

				var searchresultsclosedtask = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);



					if (searchresultsclosedtask != null && searchresultsclosedtask != "") {
						nlapiLogExecution('ERROR', 'into closedtask searchresultsclosedtask.length',searchresultsclosedtask.length);
						for (var n = 0; searchresultsclosedtask != null && n < searchresultsclosedtask.length; n++) 
						{
							ContLP = searchresultsclosedtask[n].getValue('custrecord_ebiztask_ebiz_contlp_no');
							//var searchresultsclosedtasks = searchresultsclosedtask[i];
							ExpQty = searchresultsclosedtask[n].getValue('custrecord_ebiztask_expe_qty');
							Item = searchresultsclosedtask[n].getValue('custrecord_ebiztask_sku');
							//case# 20127634 starts ,//case 20148126�
							ItemName = searchresultsclosedtask[n].getText('custrecord_ebiztask_sku');
							//case# 20127634 end
							Desc = searchresultsclosedtask[n].getValue('custrecord_ebiztask_skudesc');
							ordno = searchresultsclosedtask[n].getValue('custrecord_ebiztask_ebiz_order_no');
							fulfillmentordno = searchresultsclosedtask[n].getValue('name');
							var k = searchresultsclosedtask[n].getValue('custrecord_wms_status_flag');
							nlapiLogExecution('ERROR', 'into closedtask CTExpQty',ExpQty);
							//nlapiLogExecution('ERROR', 'into closedtask CTItem',Item);
							//nlapiLogExecution('ERROR', 'into closedtask CTk',k);
							//nlapiLogExecution('ERROR', 'into closedtask CTDesc',Desc);
							//nlapiLogExecution('ERROR', 'into closedtask ordno',ordno);
							//nlapiLogExecution('ERROR', 'into closedtask fulfillmentordno',fulfillmentordno);
							nlapiLogExecution('ERROR', 'into closedtask ContLP',ContLP);



						//to fetch NMFC code from item/tem group/item family for a item
						if(Item != null && Item != "")
						{
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('Internalid', null, 'is', Item);

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custitem_ebiz_skunmfccode');
							columns[1] = new nlobjSearchColumn('custitem_item_group');
							columns[2] = new nlobjSearchColumn('custitem_item_family');

							var ItemNMFCCoderesult = nlapiSearchRecord('inventoryitem', null, filters, columns);
						}

						if(ItemNMFCCoderesult != null && ItemNMFCCoderesult != "")
						{
							NMFCitemGroup = ItemNMFCCoderesult[0].getValue('custitem_item_group');
							NMFCitemFamily = ItemNMFCCoderesult[0].getValue('custitem_item_family');
							NMFCitem = ItemNMFCCoderesult[0].getValue('custitem_ebiz_skunmfccode');
						}
						//nlapiLogExecution('ERROR', 'NMFCitem item',NMFCitem);
						///nlapiLogExecution('ERROR', 'NMFCitemGroup item',NMFCitemGroup);
						//nlapiLogExecution('ERROR', 'NMFCitemFamily item',NMFCitemFamily);
						if(NMFCitem == null || NMFCitem == "" )
						{
							if(NMFCitemGroup != null && NMFCitemGroup != "" )
							{
								var filters = new Array();
								filters[0] = new nlobjSearchFilter('Internalid', null, 'is', NMFCitemGroup);

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_ebiz_skugrp_nmfccode');

								var ItemGroupNMFCCoderesult = nlapiSearchRecord('customrecord_ebiznet_sku_group', null, filters, columns);

								if(ItemGroupNMFCCoderesult != null && ItemGroupNMFCCoderesult != "")
								{

									NMFCitem = ItemGroupNMFCCoderesult[0].getValue('custrecord_ebiz_skugrp_nmfccode');
								}
								//nlapiLogExecution('ERROR', 'NMFCitem itemgroup',NMFCitem);
							}


							else if(NMFCitemFamily != null && NMFCitemFamily != "")
							{
								var filters = new Array();
								filters[0] = new nlobjSearchFilter('Internalid', null, 'is', NMFCitemFamily);

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_ebiz_skufam_nmfccode');

								var ItemFamNMFCCoderesult = nlapiSearchRecord('customrecord_ebiznet_sku_family', null, filters, columns);
								if(ItemFamNMFCCoderesult != null && ItemFamNMFCCoderesult != "")
								{
									NMFCitem = ItemFamNMFCCoderesult[0].getValue('custrecord_ebiz_skufam_nmfccode');
								}
								//nlapiLogExecution('ERROR', 'NMFCitem itemfamily',NMFCitem);
							}
						}

						//to fetch totcube,totwt from LP master 

						var filters = new Array();
						nlapiLogExecution('ERROR', 'custrecord_ebiz_lpmaster_masterlptest',shipLP);
						
					
							if(shipLP != null && shipLP != "")
								// Case # 20127833 starts
								//filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', shipLP);
								filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp', null, 'is', shipLP);
								// Case # 20127833 ends

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
						columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');

						var searchresultscubewt = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);

						if(searchresultscubewt != null && searchresultscubewt != "")
						{
							Cube = searchresultscubewt[0].getValue('custrecord_ebiz_lpmaster_totcube');
							if(Cube== null || Cube == "")
								Cube='0';
							Weight = searchresultscubewt[0].getValue('custrecord_ebiz_lpmaster_totwght');
							if(Weight== null || Weight == "")
								Weight='0';
						}

						nlapiLogExecution('ERROR', 'Cube',Cube);
						nlapiLogExecution('ERROR', 'Weight',Weight);

						//to fetch no of cases 

						if((Item!=null ||Item!='') && PrevItems!=Item)
						{
							nlapiLogExecution('ERROR', 'if Item is not null',Item);
							if(PrevItems!= null && PrevItems!="")
							{
								nlapiLogExecution('ERROR', 'PrevItems',PrevItems);
								nlapiLogExecution('ERROR', 'Item',Item);
								nlapiLogExecution('ERROR', 'totCubeQty',totCubeQty);
								nlapiLogExecution('ERROR', 'totWtQty',totWtQty);
								nlapiLogExecution('ERROR', 'ContCount',ContCount);



									form.getSubList('custpage_items').setLineItemValue('custpage_noofcases', p + 1, parseFloat(ContCount).toString());
									//case# 20127634 starts (Item name not displaying issue)
									if(NMFCitem!=null && NMFCitem!='')
										form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, NMFCitem);
									else
										//form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, ItemName);	
										form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, PrevItemsName);	
									form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  Desc);
									//case# 20127634 end
									//form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, NMFCitem);
									//form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  PrevItems);
									form.getSubList('custpage_items').setLineItemValue('custpage_cubes', p + 1, PrevtotCubeQty);
									form.getSubList('custpage_items').setLineItemValue('custpage_totalwt', p + 1, PrevtotWtQty);
									p=p+1;

								PrevItems=Item;
								PrevItemsName=ItemName;
								totCubeQty =parseFloat(Cube);
								totWtQty =parseFloat(Weight);
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);
								nlapiLogExecution('ERROR', 'PrevtotCubeQty',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty',PrevtotWtQty);
								ContCount=1;
								vPrevContLP=ContLP;	

							}
							else
							{
								PrevItems=Item;
								PrevItemsName=ItemName;
								nlapiLogExecution('ERROR', ' ContLP111',ContLP);

								vPrevContLP=ContLP;
								ContCount++;
								if(Cube!=null && Cube!='')
								{
									totCubeQty =parseFloat(Cube);	
								}
								if(Weight!=null && Weight!='')
								{
									totWtQty =parseFloat(Weight);	
									nlapiLogExecution('ERROR', 'totWtQty1',totWtQty);
								}
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);	
								nlapiLogExecution('ERROR', 'PrevtotCubeQty1',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty1',PrevtotWtQty);
							}
						}
						else  if(Item!=null && Item!='')
						{
							nlapiLogExecution('ERROR', 'else if Item is not null vPrevContLP',vPrevContLP);
							nlapiLogExecution('ERROR', 'else if Item is not null ContLP',ContLP);
							if(vPrevContLP!=ContLP)
							{
								nlapiLogExecution('ERROR', 'else if Item is not null',Item);
								if(Cube!=null && Cube!='')
								{
									totCubeQty =parseFloat(totCubeQty)+parseFloat(Cube);	
								}
								if(Weight!=null && Weight!='')
								{
									totWtQty =parseFloat(totWtQty)+parseFloat(Weight);	
									nlapiLogExecution('ERROR', 'totWtQty2',totWtQty);

								}
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);	
								nlapiLogExecution('ERROR', 'PrevtotCubeQty2',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty2',PrevtotWtQty);
								vPrevContLP=ContLP;	
								ContCount++;
							}
						}





					}
					if(PrevItems!= null && PrevItems != "")
					{
						nlapiLogExecution('ERROR', 'ContCount2',ContCount);

							form.getSubList('custpage_items').setLineItemValue('custpage_noofcases', p + 1, parseFloat(ContCount).toString());
							//case# 20127634 starts (Item name not displaying issue)
							if(NMFCitem!=null && NMFCitem!='')
								form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, NMFCitem);
							else
								//form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, ItemName);
								form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, PrevItemsName);	
							form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  Desc);
							//case# 20127634 end
							//form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, NMFCitem);
							//form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  PrevItems);
							form.getSubList('custpage_items').setLineItemValue('custpage_cubes', p + 1, PrevtotCubeQty);
							form.getSubList('custpage_items').setLineItemValue('custpage_totalwt', p + 1, PrevtotWtQty);
						}
					}


			}
		}
		}
		
		nlapiLogExecution('ERROR', 'before Printreport vebizbolno',vebizbolno);
		nlapiLogExecution('ERROR', 'before Printreport fulfillmentordno',fulfillmentordno);
		nlapiLogExecution('ERROR', 'before Printreport waveno',waveno);
		nlapiLogExecution('ERROR', 'before Printreport ordno',ordno);

		var vfulfillmentordno = form.addField('custpage_fulfillmentordno', 'text', 'Fulfillmentordno').setDisplayType('hidden').setDefaultValue(fulfillmentordno);
		var vwaveno = form.addField('custpage_waveno', 'text', 'Container #').setDisplayType('hidden').setDefaultValue(waveno);
		var vordno = form.addField('custpage_ordno', 'select', 'Item','item').setDisplayType('hidden').setDefaultValue(ordno);
		//case 20124760
	var vfullfillno=form.addField('custpage_or', 'text', 'Fulfillmentordnos').setDisplayType('hidden').setDefaultValue(vfullfillno);
	//case 20124760 end
		nlapiLogExecution('ERROR', 'vebizbolno',vebizbolno);
		form.setScript('customscript_bolreport');
		form.addSubmitButton('Display');
		//form.addButton('custpage_print','Print','Printreport('+ vebizbolno +','+ fulfillmentordno +','+ waveno +','+ ordno +')');
		form.addButton('custpage_print','Print','PrintBolreport('+ vebizbolno +')');

		nlapiLogExecution('ERROR', 'before Printreport vebizbolno',vebizbolno);
		response.writePage(form);
	}
}

function PrintBolreport(vebizbolno){
	var fulfillmentordno = nlapiGetFieldValue('custpage_fulfillmentordno');
	var waveno = nlapiGetFieldValue('custpage_waveno');
	var ordno = nlapiGetFieldValue('custpage_ordno');
	//case 20124760 start
    var vfullfillno=nlapiGetFieldValue('custpage_or');
    //case 20124760 end
	//alert('into report');
	//alert('vebizbolno'+vebizbolno);
	//alert('fulfillmentordno'+fulfillmentordno);
	//alert('waveno'+waveno);
	//alert('ordno'+ordno);

	//nlapiLogExecution('ERROR', 'into Printreport vebizbolno',vebizbolno);
	//var BolPDFURL = nlapiResolveURL('SUITELET', 'customscript_bolreportpdf', 'customdeploy_bolreportpdf_di');
	var BolPDFURL = nlapiResolveURL('SUITELET', 'customscript_bolreportpdf', 'customdeploy_bolreportpdf_di');
	/*var ctx = nlapiGetContext();
	//nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		BolPDFURL = 'https://system.netsuite.com' + BolPDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			BolPDFURL = 'https://system.sandbox.netsuite.com' + BolPDFURL;				
		}*/
	//nlapiLogExecution('ERROR', 'Wave PDF URL',BolPDFURL);					
	BolPDFURL = BolPDFURL + '&custparam_ebiz_bolno='+ vebizbolno + '&custparam_fulfillmentordno='+ fulfillmentordno + '&custparam_waveno='+ waveno+ '&custparam_ordno='+ ordno+ '&custparam_or='+ vfullfillno;	
	//alert('BolPDFURL'+BolPDFURL);
	window.open(BolPDFURL);

}

function getTrailerName(trailerNo){
	var fields = ['name'];
	var columns = nlapiLookupField('customrecord_ebiznet_trailer', trailerNo, fields);
	var trailerName = columns.name;
	nlapiLogExecution('ERROR', "updateOpentask: trailerName", trailerName);

	return trailerName;
}
var searchResultArray=new Array();;
function getAppointmentSearchResults(maxno)
{


	var filters = new Array();
	filters[0] = new nlobjSearchFilter('id', null, 'greaterthan', maxno);
	var shipLPColumns = new Array();
	shipLPColumns[0] = new nlobjSearchColumn('name');
	shipLPColumns[1] = new nlobjSearchColumn('internalid');
	shipLPColumns[1].setSort();	


	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, filters, shipLPColumns);
	if(SearchResults!= null)
	{
		if(SearchResults.length>=1000)
		{  

			searchResultArray.push(SearchResults);        
			var maxno=SearchResults[SearchResults.length-1].getValue(shipLPColumns[1]);
			getAppointmentSearchResults(maxno); 

		}
		else
		{
			searchResultArray.push(SearchResults);     
		}
	}
	return searchResultArray ;
}

function FillOrder(form, DoField,maxno)
{
	nlapiLogExecution('ERROR','Into FillOrder', maxno);
	var filtersdo = new Array();		
	filtersdo[0] = new nlobjSearchFilter( 'custrecord_pickqty', null, 'greaterthan', 0 );
	//15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filtersdo[1] = new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [8,15,25,26]);
	if(maxno!=-1)
	{
		filtersdo.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_lineord').setSort(true);
//	columnsinvt[0].setSort(true);
	columnsinvt.push(new nlobjSearchColumn('id').setSort(true));	

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersdo,columnsinvt);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {
		//Searching for Duplicates		
		var res=  form.getField('custpage_ord').getSelectOptions(searchresults[i].getValue('custrecord_lineord'), 'is');
		if (res != null) {
			//nlapiLogExecution('ERROR', 'res.length', res.length +searchresults[i].getValue('custrecord_lineord') );
			if (res.length > 0) {
				continue;
			}
		}		
		DoField.addSelectOption(searchresults[i].getValue('custrecord_lineord'), searchresults[i].getValue('custrecord_lineord'));
	}

	if(searchresults!=null && searchresults.length>=1000)
	{
		var maxno=searchresults[searchresults.length-1].getValue('id');		
		FillOrder(form, DoField,maxno);	
	}
}
