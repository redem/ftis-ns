/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_LotFIFODate.js,v $
 *� $Revision: 1.1.2.1.4.4.2.2 $
 *� $Date: 2014/07/10 07:08:45 $
 *� $Author: skavuri $
 *� $Name: t_eBN_2014_1_StdBundle_3_157 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_WO_LotFIFODate.js,v $
 *� Revision 1.1.2.1.4.4.2.2  2014/07/10 07:08:45  skavuri
 *� Case# 20149080 Compatibility Issue Fixed
 *�
 *� Revision 1.1.2.1.4.4.2.1  2014/06/27 13:43:24  skavuri
 *� Case# 20149080 Compatability Issue Fixed
 *�
 *� Revision 1.1.2.1.4.4  2014/05/30 00:34:23  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.1.4.3  2013/06/17 14:05:31  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue rellated date format
 *�
 *� Revision 1.1.2.1.4.2  2013/04/17 16:02:37  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.1.2.1.4.1  2013/03/08 14:38:41  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Code merged from Endochoice as part of Standard bundle
 *�
 *� Revision 1.1.2.1  2012/12/03 16:08:05  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Added new screen to create Lot# in RF WO version
 *�
 *� Revision 1.2.2.8.4.4  2012/11/01 14:55:34  schepuri

 *
 ****************************************************************************/


/**
 * @param request
 * @param response
 */
function WOLotFIFODate(request, response){
	if (request.getMethod() == 'GET') {


		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var getWONo = request.getParameter('custparam_woname');		
		var getPOItem = request.getParameter('custparam_item');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_woid');
		var getPOQtyEntered = request.getParameter('custparam_expectedquantity');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_packcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
		/*var getItemQuantity = request.getParameter('hdnQuantity');
		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');*/

		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getMfgDate = request.getParameter('custparam_mfgdate');
		var getExpDate = request.getParameter('custparam_expdate');
		var getBestBeforeDate = request.getParameter('custparam_bestbeforedate');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getnoofrecords =request.getParameter('custparam_noofrecords');
		var getItemType = request.getParameter('custparam_itemtype');
		var getwoStatus =request.getParameter('custparam_wostatus');
		nlapiLogExecution('ERROR','getWHLocation',getWHLocation);
		nlapiLogExecution('ERROR','getBatchNo',getBatchNo);
		/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
		// to get Batch# expiry date
		var getLotExpirydate = request.getParameter('custparam_batchexpirydate');
		/*** up to here ***/
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "FECHA DELLEGADA FIFO";
			st1 = "&#218;LTIMA FECHA DISPONIBLE";
			st2 = "FECHA FIFO";
			st3 = "C&#211;DIGO FIFO";
			st4 = "FORMATO: MMDDAA";			
			st5 = "ENVIAR";
			st6 = "ANTERIOR";


		}
		else
		{
			st0 = "CHECKIN FIFO DATE";
			st1 = "LAST AVL DATE";
			st2 = "FIFO DATE";
			st3 = "FIFO CODE";
			st4 = "FORMAT : MMDDYY";
			st5 = "SEND";
			st6 = "PREV";
			st9 = "FIFO DATE SHOULD BE B/W MFGDATE AND EXPDATE ";
			st10 = "LAST AVL DATE format should be MMDDYY  ";
			st11 = "FIFO DATE format should be MMDDYY";

		}


		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>" + st0 + " </title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":</td>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getPOLineItemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
		html = html + "				<input type='hidden' name='hdnlotexpirydate' value=" + getLotExpirydate + ">";
		/*** up to here ***/
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnnoofrecords' value=" + getnoofrecords + ">";
		html = html + "				<input type='hidden' name='hdnWOName' value=" + getWONo + ">";
		html = html + "				<input type='hidden' name='hdnWOStatus' value=" + getwoStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				<input type='hidden' name='hdnItemId' value=" + getFetchedItemId + "></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteravldate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ":</td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfifodate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +": </td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfifocode' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		try {

			var optedEvent = request.getParameter('cmdPrevious');
			var getLastAvlDate = request.getParameter('enteravldate');
			var getFifoDate = request.getParameter('enterfifodate');
			var getFifoCode = request.getParameter('enterfifocode');
			var error='';
			var errorflag='F';

			var POarray = new Array();

			var getLanguage = request.getParameter('hdngetLanguage');
			POarray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);
			var getFetchedItemId = request.getParameter('hdnItemId');   	

			var st7;
			if( getLanguage == 'es_ES')
			{
				st7 = "ERROR EN LA FECHA FIFO";

			}
			else
			{
				st7 = "ERROR IN FIFO DATE";

			}
			POarray["custparam_fifodate"] =getFifoDate; //Case# 20149080
			POarray["custparam_woid"] = request.getParameter('custparam_woid');
			POarray["custparam_item"] = request.getParameter('custparam_item');
			POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
			POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
			POarray["custparam_poid"] = request.getParameter('custparam_poid');
			POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
			POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
			POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
			POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
			POarray["custparam_expectedquantity"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
			POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
			POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode'); //request.getParameter('custparam_polinepackcode');
			POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
			POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
			POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
			POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
			POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_error"] = st7;
			POarray["custparam_screenno"] = 'WOLotFIFODt';
			POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
			POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
			POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
			POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
			POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
			POarray["custparam_itemtype"] = request.getParameter('hdnItemType');
			/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
			POarray["custparam_batchexpirydate"] = request.getParameter('hdnlotexpirydate');	
			/*** up to here ***/
			nlapiLogExecution('ERROR', 'WH Location', POarray["custparam_whlocation"]);
			nlapiLogExecution('ERROR','hdnBatchNo',request.getParameter('hdnBatchNo'));
			POarray["custparam_noofrecords"] = request.getParameter('hdnnoofrecords');
			POarray["custparam_woname"] = request.getParameter('hdnWOName');
			if (optedEvent == 'F7') {
				nlapiLogExecution('DEBUG', 'CHECK IN FIFO DATE F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_lotexpdate', 'customdeploy_ebiz_rf_wo_lotexpdate_di', false, POarray);
			}
			else {
				// if (getFifoDate != '' && getFifoCode != '' && getLastAvlDate != '') {
				nlapiLogExecution('ERROR','Test2','Test2');
				nlapiLogExecution('ERROR','getLastAvlDate',getLastAvlDate);
				nlapiLogExecution('ERROR','getFifoCode',getFifoCode);
				
				if(getLastAvlDate != '' && getLastAvlDate.length > 6)
				{
					POarray["custparam_error"] = st10;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				else if(getFifoDate != '' && getFifoDate.length > 6)
				{
					POarray["custparam_error"] = st11;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				
							
				
				if (getLastAvlDate != '' && getLastAvlDate !=null)
				{
					var Lastdate= RFDateFormat(getLastAvlDate);
					nlapiLogExecution('ERROR','Lastdate[0]',Lastdate[0]);
					nlapiLogExecution('ERROR','Lastdate[1]',Lastdate[1]);
					if(Lastdate[0]=='true')
						POarray["custparam_lastdate"]=Lastdate[1];
					else {
						errorflag='T';
						error='LastDate:'+Lastdate[1];
					}
				}

//				POarray["custparam_lastdate"] = RFDateFormat(getLastAvlDate);
				if (POarray["custparam_fifodate"] == null || POarray["custparam_fifodate"] == "") {
					if (request.getParameter('hdnExpDate') != null || request.getParameter('hdnExpDate') != "") {
						POarray["custparam_fifodate"] = request.getParameter('hdnExpDate');
					}
					else 
						if (request.getParameter('hdnMfgDate') != null || request.getParameter('hdnMfgDate') != "") {
							POarray["custparam_fifodate"] = request.getParameter('hdnMfgDate');
						}
				}
				else {
					nlapiLogExecution('ERROR','Befor','Before');
					var Fifodate= RFDateFormat(getFifoDate);
					nlapiLogExecution('ERROR','Fifodate[0]',Fifodate[0]);
					nlapiLogExecution('ERROR','Fifodate[1]',Fifodate[1]);
					if(Fifodate[0]=='true')
					{
						nlapiLogExecution('ERROR','Fifodate',Fifodate[1]);
						POarray["custparam_fifodate"]=Fifodate[1];
					}
					else {
						errorflag='T';
						error='Fifodate:'+Fifodate[1];
					}
					//POarray["custparam_fifodate"] = RFDateFormat(getFifoDate); //Case #20149080 put comment here
				}
				nlapiLogExecution('ERROR','errorflag',errorflag);
				nlapiLogExecution('ERROR','error',error);
				if(errorflag=='F')
				{
					nlapiLogExecution('ERROR','errorflag',errorflag);

					var itemSubtype = nlapiLookupField('item', POarray["custparam_fetcheditemid"], ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
					nlapiLogExecution('ERROR','itemSubtype.recordType',itemSubtype.recordType);
					POarray["custparam_fifocode"] = getFifoCode;
					if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == "lotnumberedassemblyitem" ||itemSubtype.recordType == "assemblyitem" || itemSubtype.custitem_ebizbatchlot == 'T') {
						//checks for Batch/Lot exists in Batch Entry, Update if exists if not insert
						try {
							nlapiLogExecution('ERROR', 'in');
							var filtersbat = new Array();
							filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', POarray["custparam_batchno"]);
							if(getFetchedItemId!=null&&getFetchedItemId!="")
								filtersbat[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', getFetchedItemId);

							var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);
							nlapiLogExecution('ERROR', 'SrchRecord', SrchRecord);
							if (SrchRecord) {

								nlapiLogExecution('DEBUG', ' BATCH FOUND');
								var transaction = nlapiLoadRecord('customrecord_ebiznet_batch_entry', SrchRecord[0].getId());
								transaction.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
								transaction.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
								transaction.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
								transaction.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
								transaction.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
								transaction.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
								var rec = nlapiSubmitRecord(transaction, false, true);

							}
							else {
								nlapiLogExecution('ERROR', 'BATCH NOT FOUND');
								var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

								customrecord.setFieldValue('name', POarray["custparam_batchno"]);
								customrecord.setFieldValue('custrecord_ebizlotbatch', POarray["custparam_batchno"]);
								customrecord.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
								customrecord.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
								customrecord.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
								customrecord.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
								customrecord.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
								if(POarray["custparam_lastdate"]!=null && POarray["custparam_lastdate"] !='')
									customrecord.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
								nlapiLogExecution('ERROR', '1');
								nlapiLogExecution('ERROR', 'Item Id',getFetchedItemId);
								nlapiLogExecution('ERROR', 'getPOLinePackCode',getPOLinePackCode);
								//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
								customrecord.setFieldValue('custrecord_ebizsku', getFetchedItemId);
								if(getPOLinePackCode!=null && getPOLinePackCode !='')
									customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);
								customrecord.setFieldValue('custrecord_ebizsitebatch',POarray["custparam_whlocation"]);
								//custrecord_ebizsitebatch
								var rec = nlapiSubmitRecord(customrecord, false, true);
							}
						} 
						catch (e) {
							nlapiLogExecution('ERROR', 'Failed to Update/Insert into BATCH ENTRY Record',e);
						}
					}


					//CheckinLp(request.getParameter('custparam_fetcheditemid'), request.getParameter('custparam_poid'), request.getParameter('custparam_poitem'), request.getParameter('custparam_lineno'), request.getParameter('hdnPOInternalId'), request.getParameter('hdnPOQuantityEntered'), request.getParameter('hdnItemRemaininingQuantity'), request.getParameter('hdnItemPackCode'), request.getParameter('hdnItemStatus'), request.getParameter('hdnQuantity'), request.getParameter('hdnQuantityReceived'), request.getParameter('custparam_itemdescription'), request.getParameter('custparam_itemcube'), request.getParameter('hdnActualBeginDate'), getActualBeginTime, getActualBeginTimeAMPM);
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_loc', 'customdeploy_ebiz_rf_wo_putaway_loc_di', false, POarray);
				}
				else
				{
					POarray["custparam_error"] = error;
					nlapiLogExecution('ERROR','errorflag','error');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);

				}
				//                }
				//                else {
				//                    POarray["custparam_error"] = 'FIFO DATE IS NULL';
				//                    response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				//                }
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'Catch: Location not found');

		}
	}
}
