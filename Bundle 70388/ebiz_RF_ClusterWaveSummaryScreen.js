/***************************************************************************
  		   eBizNET Solutions                
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_ClusterWaveSummaryScreen.js,v $
 *     	   $Revision: 1.1.2.5.4.4.4.15.2.2 $
 *     	   $Date: 2015/04/13 16:12:49 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *

 *
 * 
 *****************************************************************************/
function WaveorCluster(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		nlapiLogExecution('DEBUG', 'Into Request', 'Into Request');
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8;
		
		if( getLanguage == 'es_ES')
		{
			st1 = "NO. DE PEDIDOS";
			st2 = "NO. De SELECCIONES:";
			st3 = "NO. LUGARES DE SELECCI&#211;N:";
			st4 = "NO. DE CAJAS:";
			st5 = "CUBE TOTAL:";
			st6 = "PESO TOTAL:";
			st7 = "ENVIAR";
			st8 = "ANTERIOR";
			

		}
		else
		{
			st1 = "NO. OF ORDERS ";
			st2 = "NO. OF PICKS:";
			st3 = "NO. OF PICK LOCATIONS:";
			st4 = "NO. OF CARTONS:";
			st5 = "TOTAL CUBE :";
			st6 = "TOTAL WEIGHT:";
			st7 = "SEND";
			st8 = "PREV";
			
		}
		
		var noofOrders=0;
		var noofPicks=0;
		var noofLocations=0;
		var noofCartons =0;
		var orderArray=new Array();
		var PickArray=new Array();
		var LocArray=new Array();
		var CartonArray=new Array();
		var CartonSizeArray=new Array();
		var totcube=0;
		var totwgt=0;
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchNo = request.getParameter('custparam_batchno');	
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');
		var OrdNo=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var vSOId=request.getParameter('custparam_ebizordno');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');

		var venterSO=request.getParameter('custparam_venterSO');
		var venterFo=request.getParameter('custparam_venterFO');
		var venterwave=request.getParameter('custparam_venterwave');
		var venterzone=request.getParameter('custparam_venterzone');
		var venterCarton=request.getParameter('custparam_ventercarton');

		nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		nlapiLogExecution('DEBUG', 'before getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('DEBUG', 'before getItemInternalId', getItemInternalId);

		if(getFetchedLocation==null || getFetchedLocation == "")
		{
			getFetchedLocation=NextLocation;
		}

		if(getItemInternalId==null || getItemInternalId == "")
		{
			getItemInternalId=NextItemInternalId;
		}

		var Type=request.getParameter('custparam_type');
		nlapiLogExecution('DEBUG', 'If Type', Type);
		nlapiLogExecution('DEBUG', 'If Cluster', vClusterNo);

		var vPickType=pickType;
		nlapiLogExecution('DEBUG', 'vPickType', vPickType);
		var SOFilters = new Array();
		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
		if(Type=="Wave")
		{

			if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && venterwave != null && venterwave != "")
			{
				nlapiLogExecution('DEBUG', 'venterwave inside If', venterwave);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(venterwave)));
			}	 
			if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && venterFo!=null && venterFo!="" && venterFo!= "null")
			{
				nlapiLogExecution('DEBUG', 'OrdNo inside If', venterFo);
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', venterFo));
			}
			if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && venterSO!=null && venterSO!="" && venterSO!= "null")
			{
				nlapiLogExecution('DEBUG', 'SO Id inside If', venterSO);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', venterSO));
			}
			if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && venterzone!=null && venterzone!="" && venterzone!= "null")
			{
				nlapiLogExecution('DEBUG', 'Zone # inside If', venterzone);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));			
			}
			if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && venterCarton!=null && venterCarton!="" && venterCarton!= "null")
			{
				nlapiLogExecution('DEBUG', 'Carton # inside If', venterCarton);
				SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', venterCarton));			
			}
		}
		else if(Type=="Cluster")
		{
			if(vClusterNo != null && vClusterNo !="")
			{nlapiLogExecution('DEBUG', 'If Cluster condition', vClusterNo);
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
			}

			if(venterFo!=null && venterFo!="")
			{
				nlapiLogExecution('DEBUG', 'getOrdNo inside if', venterFo);					
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', venterFo));
			} 
			if(venterSO!=null && venterSO!="")
			{
				nlapiLogExecution('DEBUG', 'getSONo inside if', venterSO);
				nlapiLogExecution('DEBUG', 'vSOId inside if', venterSO);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', venterSO));
			}
			if(venterzone!=null && venterzone!="" && venterzone!='null')
			{
				nlapiLogExecution('DEBUG', 'getZoneNo inside if', venterzone);
				nlapiLogExecution('DEBUG', 'vZoneId inside if', venterzone);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));
				//SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', venterzone));

			}
		}
		if(Type=="Wave" ||Type=="Cluster")
		{
			var SOColumns = new Array();

			var SOColumns = new Array();
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
			SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
			SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
			SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			SOColumns.push(new nlobjSearchColumn('name'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
			SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_totalcube'));
			SOColumns.push(new nlobjSearchColumn('custrecord_total_weight'));

			SOColumns[0].setSort();
			SOColumns[1].setSort();
			SOColumns[2].setSort();
			SOColumns[3].setSort();
			SOColumns[4].setSort();
			SOColumns[5].setSort(true);

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

			//added code by suman on 080812
			var Res=ContainerSizeCount(SOFilters);
			nlapiLogExecution('DEBUG','Res',Res);
			//end of code as of 080812

			var Orders,Picks,Locations,Cartons,Totalcube,TotalWeight,CartonsSize;
			if(SOSearchResults!=null&&SOSearchResults!=""){
				for (var i = 0; i < SOSearchResults.length; i++){
					nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length);
					Orders=SOSearchResults[i].getValue('custrecord_ebiz_order_no');
					Picks=SOSearchResults[i].getText('custrecord_expe_qty');
					Locations=SOSearchResults[i].getText('custrecord_actbeginloc');
					Cartons=SOSearchResults[i].getValue('custrecord_container_lp_no');
					//totcube=totcube+ parseFloat( SOSearchResults[i].getValue('custrecord_totalcube'));
					//totwgt= totwgt+ parseFloat(SOSearchResults[i].getValue('custrecord_total_weight'));
					noofPicks=SOSearchResults.length;
					CartonsSize=SOSearchResults[i].getValue('custrecord_container_lp_no');

					orderArray[i]=Orders;
					PickArray[i]=Picks;
					LocArray[i]=Locations;
					CartonArray[i]=Cartons;
					CartonSizeArray[i]=CartonsSize;
				}

				//added code by suman on 090812
				var TotWtCube=TotalWeightandCube(SOSearchResults,CartonArray);
				nlapiLogExecution('DEBUG','TotWtCube',TotWtCube);
				totwgt=TotWtCube[0];
				totcube=TotWtCube[1];
				//end of code as of 090812.

				if(orderArray.length>0)
				{
					nlapiLogExecution('DEBUG', 'orderArray', orderArray.length);
					noofOrders=removeDuplicateElement(orderArray);
					nlapiLogExecution('DEBUG', 'noofOrders', noofOrders);
				}

				if(LocArray.length>0)
				{
					nlapiLogExecution('DEBUG', 'LocArray', LocArray.length);
					noofLocations=removeDuplicateElement(LocArray);
					nlapiLogExecution('DEBUG', 'noofLocations', noofLocations);
				}
				if(CartonArray.length>0)
				{	
					nlapiLogExecution('DEBUG', 'CartonArray', CartonArray.length);
					noofCartons=removeDuplicateElement(CartonArray);
					nlapiLogExecution('DEBUG', 'noofCartons', noofCartons);
				}
			}
			else
			{
				var SOarray=new Array();
				SOarray["custparam_screenno"] = '12';
				SOarray["custparam_error"] = "Order mentioned is completely processed";
				nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return true;
			}
		}

		if(totcube==null || totcube=='' || isNaN(totcube))
			totcube=0;
		
		if(totwgt==null || totwgt=='' || isNaN(totwgt))
			totwgt=0;
		
		
		nlapiLogExecution('Error', 'totcube', totcube);
		nlapiLogExecution('Error', 'totwgt', totwgt);

		nlapiLogExecution('DEBUG', 'after getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('DEBUG', 'after getItemInternalId', getItemInternalId);
		nlapiLogExecution('DEBUG', 'getBeginLocation ID', getFetchedBeginLocation);
		nlapiLogExecution('DEBUG', 'getBeginLocation', getFetchedLocation);

		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId); 
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('enterwaveno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdntype' value=" + Type + ">";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getFetchedBeginLocation + ">";
		//		case no Issue: 20123645  
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value='" + getFetchedLocation + "'>";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo  + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + vBatchNo  + "'>";	
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";	
		html = html + "				<input type='hidden' name='hdnOrdNo' value=" + OrdNo + ">";
		html = html + "				<input type='hidden' name='hdnsoid' value=" + vSOId + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";	
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				<input type='hidden' name='hdnenterSO' value=" + venterSO + ">";
		html = html + "				<input type='hidden' name='hdnenterFo' value=" + venterFo + ">";
		html = html + "				<input type='hidden' name='hdnenterzone' value=" + venterzone + ">";
		html = html + "				<input type='hidden' name='hdnentercarton' value=" + venterCarton + ">";
		//html = html + "				<input type='hidden' name='hdnskipid' value=" + venterwave + ">";

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>"+noofOrders+"</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>"+noofPicks+"</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>"+noofLocations+"</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st4 +"<label>"+noofCartons+"</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		if(Res != null && Res !='' && Res.length>0)
		{
		for ( var count = 0; count < Res.length; count++) 
		{
			var cartonsize=Res[count][0];
			var cartoncount=Res[count][1];
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><b><label>"+cartonsize+"-"+cartoncount+";</label></b>";
			html = html + "				</td>";
			html = html + "			</tr>";
			}
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +"<label> "+parseFloat(totcube).toFixed(4)+"</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st6 +"<label> "+parseFloat(totwgt).toFixed(4)+"</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st7 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st8 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		
		var getLanguage = request.getParameter('hdngetLanguage');
		
		var st9,st10;
		
		if( getLanguage == 'es_ES')
		{
			st9 = "NO V&#193;LIDO WAVE #";
			st10 = "GRUPO DE VALIDEZ #";

		}
		else
		{
			st9 = "INVALID WAVE #";
			st10 = "INVALID CLUSTER #";
		}
		
		
//		var Type=request.getParameter('hdntype');
		var Type=request.getParameter('custparam_type');
		nlapiLogExecution('DEBUG', 'Type',Type );
		var getWaveNo=request.getParameter('hdnWaveNo');
		var vClusterNo=request.getParameter('hdnClusterNo');
		var vPickType=request.getParameter('hdnpicktype');
		
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') 
		{
			if(Type=="Wave")
			{   
				SOarray["custparam_error"] = st9;//'INVALID WAVE #';
				SOarray["custparam_screenno"] = '12';
				SOarray["custparam_type"] ="Wave";
				response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
			}
			else if(Type=="Cluster")
			{
				nlapiLogExecution('DEBUG', 'Type', Type);
				SOarray["custparam_error"] = st10;//'INVALID CLUSTER #';
				SOarray["custparam_screenno"] = 'CL1';
				SOarray["custparam_type"] ="Cluster";
				response.sendRedirect('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di', false, SOarray);
			}

		}
		else 
		{
			nlapiLogExecution('DEBUG', 'getWaveNo', getWaveNo);
			nlapiLogExecution('DEBUG', 'vClusterNo', vClusterNo);

			var vSOId;
			var vOrdFormat='W';
			var vZoneId;
			var SOFilters = new Array();
			var SOColumns = new Array();

			if(Type=="Wave")
			{
				if(getWaveNo!= null && getWaveNo!= "")
				{
					nlapiLogExecution('DEBUG', 'Type:WaveNo inside if', getWaveNo); 
					vOrdFormat = 'W';

					/*SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));

					SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
					SOColumns[1] = new nlobjSearchColumn('custrecord_bin_locgroup_seq');
					SOColumns[2] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
					SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
					SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty');
					SOColumns[5] = new nlobjSearchColumn('custrecord_actbeginloc');		
					SOColumns[6] = new nlobjSearchColumn('custrecord_skudesc');
					SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
					SOColumns[8] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
					SOColumns[9] = new nlobjSearchColumn('custrecord_invref_no');
					SOColumns[10] = new nlobjSearchColumn('custrecord_line_no');
					SOColumns[11] = new nlobjSearchColumn('custrecord_actbeginloc');
					SOColumns[12] = new nlobjSearchColumn('custrecord_batch_no');
					SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
					SOColumns[14] = new nlobjSearchColumn('custrecord_comp_id');
					SOColumns[15] = new nlobjSearchColumn('name');
					SOColumns[16] = new nlobjSearchColumn('custrecord_container');
					SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					SOColumns[18] = new nlobjSearchColumn('custrecord_ebizzone_no');
					SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');

					SOColumns[0].setSort();
		            SOColumns[1].setSort();
		            SOColumns[2].setSort();
		            SOColumns[3].setSort();
		            SOColumns[4].setSort(true);//Descending

					SOarray["custparam_picktype"] = vOrdFormat;
					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0) {
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}
						nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
						var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
						// SOarray["custparam_waveno"] = getWaveNo;

						nlapiLogExecution('DEBUG', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_lpno'));
						nlapiLogExecution('DEBUG', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
						nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
						SOarray["custparam_waveno"] = getWaveNo;
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
						SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
						SOarray["custparam_noofrecords"] = SOSearchResults.length;
						SOarray["name"] =  SOSearchResult.getValue('name');
						SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
						SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
//						if(getZoneNo!=null && getZoneNo!="")
//						{
//						SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
//						}
//						else
						SOarray["custparam_ebizzoneno"] = '';
						SOarray["custparam_type"] ="Wave";

						nlapiLogExecution('DEBUG', 'Location', SOSearchResult.getText('custrecord_actbeginloc'));*/

					SOarray["custparam_waveno"] = getWaveNo;
					SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
					SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
					SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
					SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocation');
					SOarray["custparam_item"] = request.getParameter('hdnItem');
					SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
					SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
					SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
					SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
					SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
					SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginLocationName');
					SOarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
					SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
					SOarray["custparam_whcompany"] = request.getParameter('hdnwhCompany');
					SOarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
					SOarray["name"] =  request.getParameter('hdnOrdNo');
					SOarray["custparam_containersize"] =  request.getParameter('hdnContainerSize');
					SOarray["custparam_ebizordno"] =  request.getParameter('hdnsoid');
					SOarray["custparam_venterSO"]=request.getParameter('hdnenterSO');
					SOarray["custparam_venterFO"]=request.getParameter('hdnenterFo');
					SOarray["custparam_venterwave"]=getWaveNo;
					SOarray["custparam_venterzone"]=request.getParameter('hdnenterzone');
					SOarray["custparam_ventercarton"]=request.getParameter('hdnentercarton');
					if(request.getParameter('hdnenterzone')!=null && request.getParameter('hdnenterzone')!="")
					{
						SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnenterzone');
					}
					else 
					{
						SOarray["custparam_ebizzoneno"] = '';
					}
					SOarray["custparam_type"] ="Wave";
					//SOarray["custparam_picktype"] = vOrdFormat;
					SOarray["custparam_picktype"] = vPickType;
					
					response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
					//}
					/*else 
					{
						nlapiLogExecution('DEBUG', 'Error: ', 'Wave # not found');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					}*/
				}
			}
			else if(Type=="Cluster")
			{
				if(vClusterNo != null && vClusterNo !="")
				{
					nlapiLogExecution('DEBUG', 'Type:Cluster inside if', vClusterNo);
					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
					vOrdFormat='CL';
					
					/*SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
					SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
					SOColumns[2] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
					SOColumns[3] = new nlobjSearchColumn('custrecord_sku',null,'group');
					SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
					SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
					SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no',null,'group');
					SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no',null,'group');
					SOColumns[8] = new nlobjSearchColumn('custrecord_line_no',null,'group');
					SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
					SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
					SOColumns[11] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
					SOColumns[12] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
					SOColumns[13] = new nlobjSearchColumn('name',null,'group');
					SOColumns[14] = new nlobjSearchColumn('custrecord_container',null,'group');
					//SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					SOColumns[15] = new nlobjSearchColumn('internalid',null,'group');
					SOColumns[16] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
					SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
					SOColumns[18] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
					SOColumns[19] = new nlobjSearchColumn('custrecord_lpno',null,'group');
					SOColumns[20] = new nlobjSearchColumn('internalid','custrecord_ebiz_order_no','group');
					

					SOColumns[0].setSort();		//	Location Pick Sequence
					SOColumns[1].setSort();		//	SKU
					SOColumns[3].setSort();		//	Location
*/					//SOColumns[16].setSort();	//	Container LP
					SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
					SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
					SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
					SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
					SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
					SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
					SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
					SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
					SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
					SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
					SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
					SOColumns[11] = new nlobjSearchColumn('custrecord_line_no',null,'group');
					SOColumns[12] = new nlobjSearchColumn('internalid',null,'group');
					SOColumns[13] = new nlobjSearchColumn('name',null,'group');
					SOColumns[14] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
					SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no',null,'group');
					SOColumns[16] = new nlobjSearchColumn('custrecord_invref_no',null,'group');
					/*SOColumns[12] = new nlobjSearchColumn('internalid','custrecord_ebiz_order_no','group');
					
					SOColumns[13] = new nlobjSearchColumn('internalid',null,'group');
					SOColumns[14] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no',null,'group');
					SOColumns[15] = new nlobjSearchColumn('custrecord_invref_no',null,'group');
					SOColumns[16] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
					SOColumns[17] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
					SOColumns[18] = new nlobjSearchColumn('custrecord_container',null,'group');*/
					//SOColumns[10] = new nlobjSearchColumn('custrecord_container',null,'group');
					//SOColumns[11] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');


					SOColumns[0].setSort(); 	//Location Pick Sequence
					SOColumns[1].setSort();		//SKU
					SOColumns[2].setSort();		//Location
					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

					if (SOSearchResults != null && SOSearchResults.length > 0) {
//						if(SOSearchResults.length <= parseInt(vSkipId))
//						{
//							vSkipId=0;
//						}

						var SOSearchResult = SOSearchResults[0];
						nlapiLogExecution('DEBUG', 'SOSearchResults[0].getId()',SOSearchResults[0].getId());
						SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no',null,'group');
						SOarray["custparam_recordinternalid"] = SOSearchResult.getValue('internalid',null,'group');
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku',null,'group');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc',null,'group');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no',null,'group');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no',null,'group');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no',null,'group');
						SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
						SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no',null,'group');
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no',null,'group');
						//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no',null,'group');
						SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
						SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id',null,'group');
						//SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container',null,'group');
						//SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('internalid','custrecord_ebiz_order_no','group');
						SOarray["name"] =  SOSearchResult.getValue('name',null,'group');
						SOarray["custparam_picktype"] =  'CL';
						SOarray["custparam_nextlocation"] = '';
						SOarray["custparam_nextiteminternalid"] = '';
						SOarray["custparam_nextitem"] = '';
						SOarray["custparam_type"] ="Cluster";
						
						if(SOarray["custparam_recordinternalid"] != null && SOarray["custparam_recordinternalid"] != "")
						{
							var Opentaskrec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', SOarray["custparam_recordinternalid"]);
							SOarray["custparam_ebizordno"] =  Opentaskrec.getFieldValue('custrecord_ebiz_order_no');
							nlapiLogExecution('DEBUG', 'custparam_ebizordno',SOarray["custparam_ebizordno"]);
						}
						
						nlapiLogExecution('DEBUG', 'Begin Location Name',SOarray["custparam_beginlocationname"]);
						nlapiLogExecution('DEBUG', 'Redirecting to Location Screen');
						response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);						

					}
					else 
					{
						nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

					}
				}
			}
		}
	}
}




function removeDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray.length;
}


/**
 * @param SOFilters
 * @returns {Array}
 */
function ContainerSizeCount(SOFilters)
{
	try
	{
		var SOColumns = new Array();
		SOColumns[0] = new nlobjSearchColumn('name',null,'group');
		SOColumns[1] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
		SOColumns[2] = new nlobjSearchColumn('custrecord_container',null,'group');
		SOColumns[1].setSort(true);
		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		var totalarray=new Array();
		var tempcontainersizeArray=new Array();
		var vcount=0;
		for(var x=0;x<SOSearchResults.length;x++)
		{
			var count=0;
			var vcontainerSize=SOSearchResults[x].getText('custrecord_container',null,'group');

			nlapiLogExecution('DEBUG','index',tempcontainersizeArray.indexOf(vcontainerSize));
			if(tempcontainersizeArray.indexOf(vcontainerSize)==-1)
			{
				for(var y=0;y<SOSearchResults.length;y++)
				{
					var tempcontainersize=SOSearchResults[y].getText('custrecord_container',null,'group');
					if(vcontainerSize==tempcontainersize)
					{
						tempcontainersizeArray.push(vcontainerSize);
						count++;
					}
				}
				totalarray[vcount]=new Array();
				totalarray[vcount][0]=vcontainerSize;
				totalarray[vcount][1]=count;
				vcount++;
			}
		}

		return totalarray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in ContianerSizeCount',exp);
	}
}

function TotalWeightandCube(SOSearchResults,CartonArray)
{
	try
	{
		var vtotalwt=0;
		var vtotalcube=0;
		var multiplier=0;
		//To get the LPcount for the distinct LPs.
		//Eg: lp1-2;lp2-5
		var LP_Count=ContainerLPCount(SOSearchResults);
		nlapiLogExecution('DEBUG','LP_Count',LP_Count);

		//To get the distinct LP Values.
		var duplicateContainerLp=vremoveDuplicateElement(CartonArray);
		nlapiLogExecution('DEBUG','duplicateContainerLp',duplicateContainerLp);

		for ( var count = 0; count < duplicateContainerLp.length; count++) 
		{
			for(var x=0;x<LP_Count.length;x++)
			{
				if(LP_Count[x][0]==duplicateContainerLp[count])
				{
					multiplier=LP_Count[x][1];
				}
			}
			var result=GetCubeWt_LpMaster(duplicateContainerLp[count]);
			vtotalwt=vtotalwt+(parseFloat(result[0])*parseFloat(multiplier));
			vtotalcube=vtotalcube+(parseFloat(result[1])*parseFloat(multiplier));
		}
		nlapiLogExecution('DEBUG','vtotalwt',vtotalwt);
		nlapiLogExecution('DEBUG','vtotalcube',vtotalcube);

		if(vtotalwt=="NaN"||vtotalwt==""||vtotalwt==null)
			vtotalwt=0;
		if(vtotalcube=="NaN"||vtotalcube==""||vtotalcube==null)
			vtotalcube=0;
		
		var FinalArray=new Array();
		FinalArray[0]=vtotalwt;
		FinalArray[1]=vtotalcube;

		return FinalArray;

	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in ContianerSizeCount',exp);
	}
}


function GetCubeWt_LpMaster(ContainerLP)
{
	try
	{
		var vtotalwt=0;
		var vtotalcube=0;
		var vTotalArray=new Array();
		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp',null,'is',ContainerLP));
		filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
		column[1]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');

		var searchResults=nlapiSearchRecord('customrecord_ebiznet_master_lp',null,filter,column);

		if(searchResults!=null&&searchResults!="")
		{
			vtotalwt=searchResults[0].getValue('custrecord_ebiz_lpmaster_totwght');
			vtotalcube=searchResults[0].getValue('custrecord_ebiz_lpmaster_totcube');
		}
		
		if(vtotalwt=="NaN"||vtotalwt==""||vtotalwt==null)
			vtotalwt=0;
		if(vtotalcube=="NaN"||vtotalcube==""||vtotalcube==null)
			vtotalcube=0;
		
		vTotalArray[0]=vtotalwt;
		vTotalArray[1]=vtotalcube;
		return vTotalArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetCubeWt_LpMaster',exp);
	}
}


function vremoveDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}


/**
 * @param SOFilters
 * @returns {Array}
 */
function ContainerLPCount(SOSearchResults)
{
	try
	{
		var totalarray=new Array();
		var tempcontainerLPArray=new Array();
		var vcount=0;
		for(var x=0;x<SOSearchResults.length;x++)
		{
			var count=0;
			var vcontainerLP=SOSearchResults[x].getValue('custrecord_container_lp_no');

			nlapiLogExecution('DEBUG','index',tempcontainerLPArray.indexOf(vcontainerLP));
			if(tempcontainerLPArray.indexOf(vcontainerLP)==-1)
			{
				for(var y=0;y<SOSearchResults.length;y++)
				{
					var tempcontainerLP=SOSearchResults[y].getValue('custrecord_container_lp_no');
					if(vcontainerLP==tempcontainerLP)
					{
						tempcontainerLPArray.push(vcontainerLP);
						count++;
					}
				}
				totalarray[vcount]=new Array();
				totalarray[vcount][0]=vcontainerLP;
				totalarray[vcount][1]=count;
				vcount++;
			}
		}

		return totalarray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in ContianerSizeCount',exp);
	}
}
