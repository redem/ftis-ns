/***************************************************************************
??????????????????????????????? ? ????????????????????????????? ??eBizNET
???????????????????????                           eBizNET Solutions Inc
****************************************************************************
*
*? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_ViewCurrentAllocation_SL.js,v $
*? $Revision: 1.1.2.2 $
*? $Date: 2012/07/20 15:34:58 $
*? $Author: spendyala $
*? $Name: t_NSWMS_2012_2_1_3 $
*
* DESCRIPTION
*? Functionality
*
* REVISION HISTORY
*? $Log: ebiz_ViewCurrentAllocation_SL.js,v $
*? Revision 1.1.2.2  2012/07/20 15:34:58  spendyala
*? CASE201112/CR201113/LOG201121
*? Fine Tunning.
*?
*? Revision 1.1.2.1  2012/07/10 06:35:55  spendyala
*? New Scripts for Order Management to view allocation against the item.
*?
*
****************************************************************************/

function CurrentAllocationReport(request, response)
{
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		var Item,fromdate,todate;
		var form = nlapiCreateForm('Current Allocations');
		nlapiLogExecution('ERROR', 'Start', 'CurrentAllocationReport');

		if(request.getParameter('custparam_Item')!=null)
		{
			Item=request.getParameter('custparam_Item');
			fromdate=request.getParameter('custparam_Fromdate');
			todate=request.getParameter('custparam_Todate');
			nlapiLogExecution('ERROR', 'result',Item );
		}

		var sublist = form.addSubList("custpage_results", "list", "Order List");
		sublist.addField("custpage_so", "text", "SO");
		sublist.addField("custpage_fo", "text", "FO");
		sublist.addField("custpage_wave", "text", "WaveNo");
		sublist.addField("custpage_itemname", "text", "Item");
		sublist.addField("custpage_expqty", "text", "Qty");

		var filtersso= new Array();
		if(Item!=null&&Item!="")
		filtersso.push(new nlobjSearchFilter('custrecord_sku', null,'anyof',Item));
		filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null,'anyof',3));//3=Pick
		filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['9']));//9-Pick Generated
		/*if(fromdate!=""&&todate!="")
		filtersso.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', fromdate, todate));*/
		filtersso.push(new nlobjSearchFilter('custrecord_act_end_date',null,'isempty'));


		var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('name',null,'group');
		columnso[1]=new nlobjSearchColumn('custrecord_sku',null,'group');
		columnso[2]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columnso[3]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		columnso[4]=new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnso);
		if(searchresults!=null && searchresults.length>0 )
		{
			nlapiLogExecution('ERROR', 'result', searchresults.length);
			var k=1;
			var So,Item,Waveno,Id,WMSflag;
			for ( var count = 0; count < searchresults.length; count++)
			{
				var Result=searchresults[count];
				var Sonumber=Result.getValue('custrecord_ebiz_order_no',null,'max');
				var Item=Result.getValue('custrecord_sku',null,'group');
				var ItemName=Result.getText('custrecord_sku',null,'group');
				var Waveno=Result.getValue('custrecord_ebiz_wave_no',null,'group');
				var Expqty=Result.getValue('custrecord_expe_qty',null,'sum');
				var Fo=Result.getValue('name',null,'group');

				nlapiLogExecution('ERROR', 'So', Sonumber);
				nlapiLogExecution('ERROR', 'Item', Item);
				nlapiLogExecution('ERROR', 'Itemname', ItemName);
				nlapiLogExecution('ERROR', 'Waveno', Waveno);
				nlapiLogExecution('ERROR', 'k value', k);

				form.getSubList('custpage_results').setLineItemValue('custpage_so', k, Sonumber);
				form.getSubList('custpage_results').setLineItemValue('custpage_fo', k, Fo);
				form.getSubList('custpage_results').setLineItemValue('custpage_itemname', k, ItemName);
				form.getSubList('custpage_results').setLineItemValue('custpage_wave', k, Waveno);
				form.getSubList('custpage_results').setLineItemValue('custpage_expqty', k,Expqty);

				k++;
			}

			response.writePage(form);
		}
	} 
	else 
	{  


	}
}










