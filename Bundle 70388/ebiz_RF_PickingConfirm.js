/***************************************************************************
 eBizNET Solutions                
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingConfirm.js,v $
 *     	   $Revision: 1.8.2.27.4.18.2.31.2.1 $
 *     	   $Date: 2014/12/09 06:31:17 $
 *     	   $Author: spendyala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingConfirm.js,v $
 * Revision 1.8.2.27.4.18.2.31.2.1  2014/12/09 06:31:17  spendyala
 * case # 201411232
 *
 * Revision 1.8.2.27.4.18.2.31  2014/06/18 15:47:07  skavuri
 * Case # 20147440 SB Issue Fixed
 *
 * Revision 1.8.2.27.4.18.2.30  2014/06/18 13:54:29  grao
 * Case#: 20148926  New GUI account issue fixes
 *
 * Revision 1.8.2.27.4.18.2.29  2014/06/13 12:37:09  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.8.2.27.4.18.2.28  2014/06/05 15:23:09  nneelam
 * case#  20148740
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.8.2.27.4.18.2.27  2014/05/30 00:41:04  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.8.2.27.4.18.2.26  2014/05/26 07:08:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.8.2.27.4.18.2.25  2014/04/22 16:35:52  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Item receipt
 *
 * Revision 1.8.2.27.4.18.2.24  2014/04/16 15:12:12  skreddy
 * case # 20148023
 * MHP sb  issue fix
 *
 * Revision 1.8.2.27.4.18.2.23  2014/03/07 15:52:57  skreddy
 * case 20127594
 * demo account isue fixs
 *
 * Revision 1.8.2.27.4.18.2.22  2013/12/04 16:19:11  skreddy
 * Case# 20126192
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.8.2.27.4.18.2.21  2013/11/22 14:50:10  grao
 * Case# 20125871  related issue fixes in SB 2014.1
 *
 * Revision 1.8.2.27.4.18.2.20  2013/11/22 14:49:21  grao
 * Case# 20125871  related issue fixes in SB 2014.1
 *
 * Revision 1.8.2.27.4.18.2.19  2013/11/22 08:40:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201216680
 *
 * Revision 1.8.2.27.4.18.2.18  2013/11/08 16:20:31  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Epicuren and surftech issue fixes
 *
 * Revision 1.8.2.27.4.18.2.17  2013/10/22 14:01:29  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20124795
 * Replen Priorities
 *
 * Revision 1.8.2.27.4.18.2.16  2013/10/09 16:22:02  skreddy
 * Case# 20124964
 * tpp SB issue fixs
 *
 * Revision 1.8.2.27.4.18.2.15  2013/09/17 16:04:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * standardbundlechanges
 * case#20124221
 *
 * Revision 1.8.2.27.4.18.2.14  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.8.2.27.4.18.2.13  2013/07/15 17:05:07  skreddy
 * Case# 20123446
 * Item info display CR
 *
 * Revision 1.8.2.27.4.18.2.12  2013/07/08 14:42:05  rrpulicherla
 * Case# 20123312
 * Picking Issues
 *
 * Revision 1.8.2.27.4.18.2.11  2013/06/20 14:13:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 201215543
 *
 * Revision 1.8.2.27.4.18.2.10  2013/06/19 22:56:06  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 *
 * Revision 1.8.2.27.4.18.2.9  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.8.2.27.4.18.2.8  2013/05/31 15:14:03  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.8.2.27.4.18.2.7  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.8.2.27.4.18.2.6  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.8.2.27.4.18.2.5  2013/04/10 15:49:24  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to display wrong name
 *
 * Revision 1.8.2.27.4.18.2.4  2013/04/08 08:47:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.8.2.27.4.18.2.3  2013/03/21 14:18:14  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.8.2.27.4.18.2.2  2013/03/08 14:40:33  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.8.2.27.4.18.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.8.2.27.4.18  2013/02/19 11:48:28  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.8.2.27.4.17  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.8.2.27.4.16  2013/01/25 06:57:36  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to wrong location
 *
 * Revision 1.8.2.27.4.15  2012/12/31 05:53:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Inventory Sync issue fixes
 * .
 *
 * Revision 1.8.2.27.4.14  2012/12/24 13:21:16  schepuri
 * CASE201112/CR201113/LOG201121
 * issue related to skip func
 *
 * Revision 1.8.2.27.4.13  2012/12/18 05:32:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.8.2.27.4.12  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.8.2.27.4.11  2012/11/11 03:40:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * GSUSA UAT issue fixes.
 *
 * Revision 1.8.2.27.4.10  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.8.2.27.4.9  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.8.2.27.4.8  2012/10/29 12:10:41  mbpragada
 * 20120490
 *
 * Revision 1.8.2.27.4.7  2012/10/23 17:08:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.8.2.27.4.6  2012/10/11 10:56:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick confirmation and other inventory issues
 *
 * Revision 1.8.2.27.4.5  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.8.2.27.4.4  2012/10/04 10:30:45  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.8.2.27.4.3  2012/10/01 05:18:30  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code from 2012.2.
 *
 * Revision 1.8.2.27.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.8.2.27.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.8.2.27  2012/09/03 13:52:07  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.8.2.26  2012/08/27 07:05:07  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * clusterpickingReplen
 *
 * Revision 1.8.2.25  2012/08/26 17:55:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Pick Confirm issues for FISK
 *
 * Revision 1.8.2.24  2012/08/16 16:17:18  gkalla
 * CASE201112/CR201113/LOG201121
 * Brace missed
 *
 * Revision 1.8.2.23  2012/08/07 21:18:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.8.2.22  2012/08/01 07:16:42  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Routing based on Loc group seq
 *
 * Revision 1.8.2.21  2012/07/27 12:58:53  schepuri
 * CASE201112/CR201113/LOG201121
 * Trimming item desc
 *
 * Revision 1.8.2.20  2012/07/19 05:22:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.8.2.19  2012/07/12 14:19:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.8.2.18  2012/07/09 06:50:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.8.2.17  2012/06/29 14:12:28  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.8.2.16  2012/06/02 09:20:50  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to picking resolved.
 *
 * Revision 1.8.2.15  2012/04/26 07:10:20  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issue unable to scan the item when we placed two items in a single
 *  location.
 *
 * Revision 1.8.2.14  2012/04/24 13:49:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.8.2.13  2012/04/24 11:25:13  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issues in skip location functionality in RF Picking
 *
 * Revision 1.8.2.12  2012/04/24 07:09:40  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issues in skip location functionality in RF Picking
 *
 * Revision 1.8.2.11  2012/04/23 13:37:38  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issues in skip location functionality in RF Picking
 *
 * Revision 1.8.2.10  2012/04/19 15:00:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.8.2.9  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.8.2.8  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.8.2.7  2012/03/12 08:05:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.17  2012/03/12 07:56:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.16  2012/03/09 07:16:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.15  2012/03/08 01:37:09  gkalla
 * CASE201112/CR201113/LOG201121
 * Fixed the issue of infinite loop of same item scanning
 *
 * Revision 1.14  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.13  2012/03/01 14:00:29  rmukkera
 * CASE201112/CR201113/LOG201121
 * Suspend Button commented
 *
 * Revision 1.12  2012/02/20 14:25:46  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Loc Exception
 *
 * Revision 1.11  2012/02/03 10:44:44  gkalla
 * CASE201112/CR201113/LOG201121
 * Added comment
 *
 * Revision 1.9  2012/01/11 14:33:07  rmukkera
 * CASE201112/CR201113/LOG201121
 * item description was added
 *
 * Revision 1.8  2011/12/12 12:58:54  schepuri
 * CASE201112/CR201113/LOG201121
 * adding new button for RF PICK Exception
 *
 * Revision 1.7  2011/09/29 16:17:23  gkalla
 * CASE201112/CR201113/LOG201121
 * To integrate Serial no entry in RF outbound
 *
 * Revision 1.6  2011/09/10 07:24:50  skota
 * CASE201112/CR201113/LOG201121
 * code changes to redirect to right suitelet for capturing serial nos
 *
 * Revision 1.5  2011/09/09 18:08:24  skota
 * CASE201112/CR201113/LOG201121
 * If condition change to consider serial out flag
 *
 * 
 *****************************************************************************/
function PickingConfirm(request, response){
	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11;

		if( getLanguage == 'es_ES')
		{
			st1 = "ELIJA LA CANTIDAD:";
			st2 = "UBICACI&#211;N:";
			st3 = "ART&#205;CULO:";
			st4 = "DESCRIPCI&#211;N DEL ART&#205;CULO:";
			st5 = "CANTIDAD RESTANTE:";
			st6 = "CANT EXCEPCI&#211;N";
			st7 = "UBICACI&#211;N / EXCEPCI&#211;N LOT";
			st8 = "CONF";
			st9 = "ANTERIOR";
			st10 = "SKIP";
			st11 = "LP EXCEPCI&#211;N";
		}
		else
		{
			st1 = "PICK QTY : ";
			st2 = "LOCATION : ";
			st3 = "ITEM : ";
			st4 = "ITEM DESCRIPTION: ";
			st5 = "REMAINING QTY : ";
			st6 = "QTY EXCEPTION ";
			st7 = "LOC/LOT EXCEPTION ";
			st8 = "CONF ";
			st9 = "PREV";
			st10 = "SKIP";	
			st11 = "LP EXCEPTION";
		}
		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getItemName = request.getParameter('custparam_itemname');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
		var vBatchno = request.getParameter('custparam_batchno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var pickType=request.getParameter('custparam_picktype');
		var whLocation = request.getParameter('custparam_whlocation');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');

		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');
		//var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_recid');
		//var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		
		var serialexpeqty = request.getParameter('custparam_serialexpqty');

//		nlapiLogExecution('DEBUG', 'EntLoc', EntLoc);
		nlapiLogExecution('DEBUG', 'EntLocRec', EntLocRec);
//		nlapiLogExecution('DEBUG', 'EntLocID', EntLocID);
		nlapiLogExecution('DEBUG', 'ExceptionFlag', ExceptionFlag);

		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		nlapiLogExecution('DEBUG', 'getnextExpectedQuantity', getnextExpectedQuantity);

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		if(getBeginBinLocation==null)
		{
			getBeginBinLocation=NextLocation;
		}
		if(getItemInternalId==null)
		{
			getItemInternalId=NextItemInternalId;
		}

		nlapiLogExecution('DEBUG', 'getItemName', getItemName);

		var getItem = '';
		var Itemdescription='';
		// case 20123446 start
		var ItemInstructions=request.getParameter('custparam_itemInstructions');
		// case 20123446 end
		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');

		if(getItemName==null || getItemName =='')
		{
//			var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
//			var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);

//			getItem = ItemRec.getFieldValue('itemid');

//			nlapiLogExecution('Debug', 'Location Name is', getItem);
//			if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
//			{	
//			Itemdescription = ItemRec.getFieldValue('description');
//			}
//			else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
//			{	
//			Itemdescription = ItemRec.getFieldValue('salesdescription');
//			}


			if(getItemInternalId!=null && getItemInternalId!='')
			{
				//var Itemtype = nlapiLookupField('item', getItemInternalId, 'recordType');

				var filtersitem = new Array();
				var columnsitem = new Array();

				filtersitem.push(new nlobjSearchFilter('internalid', null, 'is',getItemInternalId));
				filtersitem.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

				columnsitem[0] = new nlobjSearchColumn('description');    
				columnsitem[1] = new nlobjSearchColumn('salesdescription');
				columnsitem[2] = new nlobjSearchColumn('itemid');

				var itemRecord = nlapiSearchRecord("item", null, filtersitem, columnsitem);
				if(itemRecord!=null && itemRecord!='' && itemRecord.length>0)
				{
					if(itemRecord[0].getValue('description') != null && itemRecord[0].getValue('description') != '')
					{
						Itemdescription = itemRecord[0].getValue('description');
					}
					else if(itemRecord[0].getValue('salesdescription') != null && itemRecord[0].getValue('salesdescription') != "")
					{	
						Itemdescription = itemRecord[0].getValue('salesdescription');
					}

					getItem = itemRecord[0].getValue('itemid');
				}

				Itemdescription = Itemdescription.substring(0, 20);			
			}
		}
		else
		{
			getItem=getItemName;
		}

		nlapiLogExecution('Debug', 'Time Stamp after retreiving Item Details',TimeStampinSec());

		nlapiLogExecution('DEBUG', 'getItemdescription', Itemdescription);

		var str = 'getWaveno. = ' + getWaveno + '<br>';
		str = str + 'getOrderNo. = ' + getOrderNo + '<br>';	
		str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';
		str = str + 'getBeginLocationId. = ' + getBeginLocationId + '<br>';
		str = str + 'getEndLocInternalId. = ' + getEndLocInternalId + '<br>';
		str = str + 'NextLocation. = ' + NextLocation + '<br>';
		str = str + 'NextItemInternalId. = ' + NextItemInternalId + '<br>';
		str = str + 'getRecordInternalId. = ' + getRecordInternalId + '<br>';

		nlapiLogExecution('DEBUG', 'calculating Remaining Qty...', str);

		var totalallocqty=0;
		var remainpickqty=0;
		var getLP;
		var getItemStatus;

		var SOFilters = new Array();

		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

		if(getWaveno != null && getWaveno != "")
		{
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
		}

		if(getOrderNo!= null && getOrderNo!="" && getOrderNo!= "null")
		{
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', getOrderNo));
		}

		if(getItemInternalId != null && getItemInternalId != "")
		{
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
		}

		if(getEndLocInternalId != null && getEndLocInternalId != "")
		{
			SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', getEndLocInternalId));
		}

		if(getZoneNo!=null && getZoneNo!="" && getZoneNo!= "null")
		{
			nlapiLogExecution('DEBUG', 'Zone # inside If', getZoneNo);
			SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', getZoneNo));			
		}
		var SOColumns = new Array();				
		SOColumns[0] = new nlobjSearchColumn('custrecord_sku',null,'group');
		SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
		SOColumns[3] = new nlobjSearchColumn('custrecord_lpno',null,'group');
		SOColumns[4] = new nlobjSearchColumn('custrecord_sku_status',null,'group');
		SOColumns[5] = new nlobjSearchColumn('custrecord_from_lp_no',null,'group');

		SOColumns[0].setSort();	//SKU
		SOColumns[2].setSort();	//Location

		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		if (SOSearchResults != null && SOSearchResults.length > 0) {
			totalallocqty = SOSearchResults[0].getValue('custrecord_expe_qty',null,'sum');	
			getLP = SOSearchResults[0].getValue('custrecord_from_lp_no',null,'group');	
			getBeginLocationId = SOSearchResults[0].getValue('custrecord_actbeginloc',null,'group');	
			getItemStatus = SOSearchResults[0].getValue('custrecord_sku_status',null,'group');
		}
		nlapiLogExecution('DEBUG', 'totalallocqty', totalallocqty);
		nlapiLogExecution('DEBUG', 'getExpectedQuantity', getExpectedQuantity);

		remainpickqty = parseFloat(totalallocqty)-parseFloat(getExpectedQuantity);

		nlapiLogExecution('DEBUG', 'remainpickqty', remainpickqty);

		if(remainpickqty<0)
			remainpickqty=0;

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('cmdConfirm').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdConfirm').disabled);";
		//html = html + "	  alert(document.getElementById('hdnflag').disabled);";
		html = html + "	  if(document.getElementById('cmdConfirm').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "	document.onkeydown = function(){";
		html = html + "		if(window.event && window.event.keyCode == 116)";
		html = html + "		        {";
		html = html + "		    window.event.keyCode = 505;";
		html = html + "		      }";
		html = html + "		if(window.event && window.event.keyCode == 505)";
		html = html + "		        { ";
		html = html + "		    return false;";
		html = html + "		    }";
		html = html + "		}";


		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + name + "</label> ## WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getExpectedQuantity + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value='" + getItem + "'>";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value='" + getEnteredLocation + "'>";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value='" + getBeginBinLocation + "'>";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value='" + getFetchedLocation + "'>";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value='" + NextLocation + "'>";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnLocExcpflag'>";
		html = html + "				<input type='hidden' name='hdnQtyExcpflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";
		//html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		//html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnItemInstructions' value='" + ItemInstructions + "'>";
		// Case Start 20125871
		html = html + "				<input type='hidden' name='hdnserialexpqty' value=" + serialexpeqty + ">";
		//end
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getEnteredLocation + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>" + getItem + "</label><br>";
		//html = html + "				ITEM DESCRIPTION: <label>" + Itemdescription + "</label><br>";
		if (Itype == "lotnumberedinventoryitem" || Itype == "lotnumberedassemblyitem")
		{
			html = html + "				EXPECTED LOT#: <label>" +ActBatchno + "</label><br>";
			html = html + "				ACTUAL LOT#: <label>" + ExpBatchno + "</label><br>";
		}
		html = html + "				"+ st5 +"<label>" + parseFloat(remainpickqty).toFixed(4) + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>SUSPEND <input name='cmdSuspend' type='submit' value='F10'/>";
		html = html + "				<td align = 'left'>";
		html = html + "				"+ st6 +"<input name='cmdPickException' type='submit' value='F2' onclick='this.form.hdnQtyExcpflag.value=this.value;this.form.submit();this.disabled=true;return false'/></td></tr>";
		html = html + "				<tr><td align = 'left'>"+ st7 +"<input name='cmdlocexc' type='submit' value='F3' onclick='this.form.hdnLocExcpflag.value=this.value;this.form.submit();this.disabled=true;return false'/></td></tr>";
		html = html + "				<tr><td align = 'left'>"+ st11 +"<input name='cmdlpexc' type='submit' value='F6'/></td></tr>";
		//	html = html + "				<tr><td align = 'left'>"+ st8 +"<input name='cmdConfirm' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.focus();this.disabled=true; return false'/>";
		html = html + "				<tr><td align = 'left'>"+ st8 +"<input name='cmdConfirm' id='cmdConfirm' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdPickException.disabled=true;this.form.cmdlocexc.disabled=true;this.form.cmdSKIP.disabled=true; return false'/>";
		html = html + "				"+ st9 +" <input name='cmdPrevious' type='submit' value='F7'/>"+ st10 +"<input name='cmdSKIP' type='submit' value='F12'/></td></tr>";
		/*html = html + "				<tr><td align = 'left'>CONF AND STAGE <input name='cmdSuspend' type='submit' value='F11'/>";*/
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdConfirm').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		nlapiLogExecution('DEBUG', 'Time Stamp at the start of Response',TimeStampinSec());		
		nlapiLogExecution('DEBUG', 'Pick Type',request.getParameter('hdnpicktype'));
		nlapiLogExecution('DEBUG', 'cmdlocexc',request.getParameter('cmdlocexc'));
		nlapiLogExecution('DEBUG', 'hdnLocExcpflag',request.getParameter('hdnLocExcpflag'));

		var getLanguage = request.getParameter('hdngetLanguage');

		var st11;

		if( getLanguage == 'es_ES')
		{
			st11 = "CONFIRME LA REPLENS ABIERTAS";
		}
		else
		{
			st11 = "CONFIRM THE OPEN REPLENS";
		}

		var vZoneId=request.getParameter('hdnebizzoneno');
		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		var nextexpqty = request.getParameter('hdnextExpectedQuantity');
		var invrefno = request.getParameter('hdnInvoiceRefNo');
		var ItemType=request.getParameter('hdnitemtype');

		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_LP"] = request.getParameter('hdnLP');
	//	SOarray["custparam_ItemStatus"] = request.getParameter('hdnItemStatus');
		SOarray["custparam_itemStatus"] = request.getParameter('hdnItemStatus');// Case# 20147440
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginLocationName');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');;
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');		
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnextExpectedQuantity');
		SOarray["custparam_nextrecordid"]=request.getParameter('hdnRecordInternalId');
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		SOarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');
		SOarray["custparam_serialexpqty"] = request.getParameter('hdnserialexpqty');
		// case 20123446 start
		SOarray["custparam_itemInstructions"] = request.getParameter('hdnItemInstructions');
		// case 20123446 end
		//var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		//var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		//SOarray["custparam_Newendlocid"] = EntLocID;
		//SOarray["custparam_Newendloc"] = EntLoc;
		SOarray["custparam_EntLocRec"] = EntLocRec;
		SOarray["custparam_Exc"]=ExceptionFlag;

		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';

		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		if (request.getParameter('cmdPrevious') == 'F7') {
			// case 20123446 start
			if(request.getParameter('hdnItemInstructions') !=null && request.getParameter('hdnItemInstructions') !='' && request.getParameter('hdnItemInstructions') !='null')
			{
				response.sendRedirect('SUITELET', 'customscript_picking_item_instructions', 'customdeploy_ebiz_picking_item_instru_di', false, SOarray);
			}
			// case 20123446 end
			else
			{
				nlapiLogExecution('ERROR', 'Clicked on Previous', request.getParameter('cmdPrevious'));
				response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);
			}
		}
		else if(request.getParameter('cmdSKIP') == 'F12')
		{
			var RecordInternalId = request.getParameter('hdnRecordInternalId');
			var ContainerLPNo = request.getParameter('hdnContainerLpNo');
			var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
			var BeginLocation = request.getParameter('hdnBeginLocationId');
			var Item = request.getParameter('hdnItem');
			var ItemName = request.getParameter('hdnItemName');
			var ItemDescription = request.getParameter('hdnItemDescription');
			var ItemInternalId = request.getParameter('hdnItemInternalId');
			var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
			var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
			var BeginLocationName = request.getParameter('hdnBeginBinLocation');
			var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
			var EndLocation = request.getParameter('hdnEnteredLocation');
			var OrderLineNo = request.getParameter('hdnOrderLineNo');
			var NextShowLocation=request.getParameter('hdnNextLocation');
			var ContainerSize=request.getParameter('hdnContainerSize');
			var NextItemInternalId=request.getParameter('hdnNextItemId');
			var WaveNo = request.getParameter('hdnWaveNo');
			var ClusNo = request.getParameter('hdnClusterNo');
			var ebizOrdNo=request.getParameter('hdnebizOrdNo');
			var OrdName=request.getParameter('hdnName');
			var RecCount;
			var SOFilters = new Array();
			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
			if(WaveNo != null && WaveNo != "")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
			}
			if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
			{
				nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
			}
			if(OrdName!=null && OrdName!="" && OrdName!= "null")
			{
				nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
			}
			if(ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
			{
				nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
			}
			if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
			{
				nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
			}

			SOarray["custparam_ebizordno"] =ebizOrdNo;
			var SOColumns = new Array();
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//			SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
			//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
			SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
			//Code end as on 290414
			SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
			SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
			SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			SOColumns.push(new nlobjSearchColumn('name'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
			SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
			SOColumns[0].setSort();
			SOColumns[1].setSort();
			SOColumns[2].setSort();
			SOColumns[3].setSort();
			SOColumns[4].setSort();
			SOColumns[5].setSort(true);

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
			if (SOSearchResults != null && SOSearchResults.length > 0) {
				nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length);
				nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);

				vSkipId=0;

				if(SOSearchResults.length <= parseFloat(vSkipId))
				{
					vSkipId=0;
				}
				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', WaveNo);
				//var SOSearchResult = SOSearchResults[0];
				var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
				if(SOSearchResults.length > parseFloat(vSkipId) + 1)
				{
					//var SOSearchnextResult = SOSearchResults[1];
					var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
					nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
					nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
				}
				else
				{
					var SOSearchnextResult = SOSearchResults[0];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
					nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
					nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
				}

				SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
				SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
				//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
				SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
				SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
				SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
				SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
				SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
				SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
				SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
				SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
				SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
				SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
				SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
				SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
				SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
				SOarray["custparam_noofrecords"] = SOSearchResults.length;		
				SOarray["name"] =  SOSearchResult.getValue('name');
				SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
				SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
				getBeginLocation = SOSearchResult.getText('custrecord_actbeginloc');
				if(vZoneId!=null && vZoneId!="")
				{
					SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
				}
				else
					SOarray["custparam_ebizzoneno"] = '';

			}
			vSkipId= parseFloat(vSkipId) + 1;
			SOarray["custparam_skipid"] = vSkipId;	

			var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
			if(skiptask==null || skiptask=='')
			{
				skiptask=1;
			}
			else
			{
				skiptask=parseInt(skiptask)+1;
			}

			nlapiLogExecution('DEBUG', 'skiptask',skiptask);

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

//			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
//			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			if(BeginLocation != SOarray["custparam_nextlocation"])
			{  
				SOarray["custparam_beginLocationname"]=null;
				SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
			}
			else if(ItemInternalId != SOarray["custparam_nextiteminternalid"])
			{ 
				SOarray["custparam_beginLocationname"]=null;
				//SOarray["custparam_iteminternalid"]=null;
				SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
				SOarray["custparam_nextexpectedquantity"] = SOarray["custparam_expectedquantity"];
				response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
			}
			else if(ItemInternalId == SOarray["custparam_nextiteminternalid"])
			{ 
				SOarray["custparam_beginLocationname"]=null;
				SOarray["custparam_iteminternalid"]=ItemInternalId;
				SOarray["custparam_nextexpectedquantity"] = SOarray["custparam_expectedquantity"];
				response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
			}
		}
		else 
		{
			if (request.getParameter('hdnflag') == 'F8') {
				nlapiLogExecution('DEBUG', 'Confirm Pick');

				var RecordInternalId = request.getParameter('hdnRecordInternalId');
				var ContainerLPNo = request.getParameter('hdnContainerLpNo');
				var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
				var BeginLocation = request.getParameter('hdnBeginLocationId');
				var Item = request.getParameter('hdnItem');
				var ItemName = request.getParameter('hdnItemName');
				var ItemDescription = request.getParameter('hdnItemDescription');
				var ItemInternalId = request.getParameter('hdnItemInternalId');
				var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
				var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
				var BeginLocationName = request.getParameter('hdnBeginBinLocation');
				var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
				var EndLocation = request.getParameter('hdnEnteredLocation');
				var OrderLineNo = request.getParameter('hdnOrderLineNo');
				var NextShowLocation=request.getParameter('hdnNextLocation');
				var ContainerSize=request.getParameter('hdnContainerSize');
				var NextItemInternalId=request.getParameter('hdnNextItemId');
				var WaveNo = request.getParameter('hdnWaveNo');
				var ClusNo = request.getParameter('hdnClusterNo');
				var ebizOrdNo=request.getParameter('hdnebizOrdNo');
				var OrdName=request.getParameter('hdnName');
				var getZoneNo=request.getParameter('hdnebizzoneno');
				var whLocation=request.getParameter('hdnwhlocation');

				nlapiLogExecution('DEBUG', 'OrdName', OrdName);
				nlapiLogExecution('DEBUG', 'Next Location', NextShowLocation);
				nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);

				var invtqty=0;

				var IsPickFaceLoc = 'F';

				IsPickFaceLoc = isPickFaceLocation(ItemInternalId,EndLocInternalId);

				nlapiLogExecution('DEBUG', 'ExpectedQuantity', ExpectedQuantity);
				nlapiLogExecution('DEBUG', 'IsPickFaceLoc', IsPickFaceLoc);
				nlapiLogExecution('DEBUG', 'InvoiceRefNo', InvoiceRefNo);
				nlapiLogExecution('DEBUG', 'RecordInternalId', RecordInternalId);

				if(IsPickFaceLoc=='T')
				{
					var openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemInternalId,whLocation);

					if(openinventory!=null && openinventory!='')
					{
						nlapiLogExecution('DEBUG', 'Pick Face Inventory Length', openinventory.length);

						for(var x=0; x< openinventory.length;x++)
						{
							var vinvtqty=openinventory[x].getValue('custrecord_ebiz_qoh');
							//vinvtqty=openinventory[0].getValue('custrecord_ebiz_avl_qty',null,'sum');

							if(vinvtqty==null || vinvtqty=='' || isNaN(vinvtqty))
							{
								vinvtqty=0;
							}

							invtqty=parseInt(invtqty)+parseInt(vinvtqty);
						}						
					}
				}

				if(invtqty==null || invtqty=='' || isNaN(invtqty))
				{
					invtqty=0;
				}

				nlapiLogExecution('DEBUG', 'invtqty', invtqty);

				if(IsPickFaceLoc=='T' && (parseInt(ExpectedQuantity)>parseInt(invtqty)))
				{
					var openreplns = new Array();
					openreplns=getOpenReplns(ItemInternalId,EndLocInternalId);

					if(openreplns!=null && openreplns!='')
					{
						for(var i=0; i< openreplns.length;i++)
						{
							//var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
							var expqty=openreplns[i].getValue('custrecord_expe_qty');
							if(expqty == null || expqty == '')
							{
								expqty=0;
							}	
							nlapiLogExecution('DEBUG', 'open repln qty',expqty);
							nlapiLogExecution('DEBUG', 'getQuantity',ExpectedQuantity);

							nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');
						}

						var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
						if(skiptask==null || skiptask=='')
						{
							skiptask=1;
						}
						else
						{
							skiptask=parseInt(skiptask)+1;
						}

						nlapiLogExecution('DEBUG', 'skiptask',skiptask);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

						var vPickType = request.getParameter('hdnpicktype');

						//nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', 'Y');	
						var SOarray=new Array();
						SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);
						SOarray["custparam_skipid"] = vSkipId;
						if(NextShowLocation!=null && NextShowLocation!='')
							SOarray["custparam_screenno"] = '11EXP';
						else
							SOarray["custparam_screenno"] = '11LOCEXP';	
						SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;

					}
					else
					{
						var replengen = generateReplenishment(EndLocInternalId,ItemInternalId,whLocation,WaveNo,'0');
						nlapiLogExecution('DEBUG', 'replengen',replengen);
						//if(replengen==true)
						//{

						var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
						if(skiptask==null || skiptask=='')
						{
							skiptask=1;
						}
						else
						{
							skiptask=parseInt(skiptask)+1;
						}

						nlapiLogExecution('DEBUG', 'skiptask',skiptask);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

						//nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', 'Y');
						var vPickType = request.getParameter('hdnpicktype');
						var SOarray=new Array();

						SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);

						SOarray["custparam_skipid"] = vSkipId;
						if(NextShowLocation!=null && NextShowLocation!='')
							SOarray["custparam_screenno"] = '11EXP';
						else
							SOarray["custparam_screenno"] = '11LOCEXP';	

						if(replengen!=true)
							SOarray["custparam_error"]='Replen Generation Failed.Insufficient inventory in Bulk location';
						else 
							SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
						//}
					}

				}				
				else
				{
					if(IsPickFaceLoc=='T' && (InvoiceRefNo==null || InvoiceRefNo==''))
					{
						if(openinventory!=null && openinventory!='')
						{
							var openinvtrecid = openinventory[0].getId();
							nlapiLogExecution('DEBUG', 'openinvtrecid', openinvtrecid);

							try
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_invref_no', openinvtrecid);
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG', 'Exception in updating inventory reference number', exp);
							}
						}
					}

					nlapiLogExecution('DEBUG', 'Before Item Fulfillment', 'Before Item Fulfillment');
					//ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo)

					var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

					if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T'|| itemSubtype.recordType == 'serializedassemblyitem') {
						nlapiLogExecution('DEBUG', 'SOarray["custparam_serialexpqty"]', SOarray["custparam_serialexpqty"]);
						if(SOarray["custparam_serialexpqty"]!='null' && SOarray["custparam_serialexpqty"]!=null && SOarray["custparam_serialexpqty"]!=''){
							SOarray["custparam_serialexpqty"] = request.getParameter('hdnserialexpqty');
							if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
							SOarray["custparam_number"] = request.getParameter('custparam_number');
							else
								SOarray["custparam_number"] = 0;
								
						}
						else
						{
							SOarray["custparam_serialexpqty"] = SOarray['custparam_expectedquantity'];
							if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
								SOarray["custparam_number"] = request.getParameter('custparam_number');
								else
									SOarray["custparam_number"] = 0;
						}
						
						SOarray["custparam_RecType"] = itemSubtype.recordType;
						SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
						SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
						response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
					}
					else
					{		
						var vPickType = request.getParameter('hdnpicktype');
						var  vdono = DoLineId;
						//nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
						//Changed the code for auto containerization
						var vAutoContainerFlag='N'; // Set 'Y' to skip RF dialog for scanning container LP
						SOarray["custparam_autocont"] = vAutoContainerFlag;
						if(vAutoContainerFlag=='Y')
						{
							var RcId=RecordInternalId;
							var EndLocation = EndLocInternalId;
							var TotalWeight=0;
							var getReason=request.getParameter('custparam_enteredReason');
							var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
							var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
							getLPContainerSize= SORec.getFieldText('custrecord_container');

							SOarray["custparam_newcontainerlp"] = getContainerLPNo;

							var vBatchno = request.getParameter('hdnbatchno');
							var PickQty = ExpectedQuantity;
							var vActqty = ExpectedQuantity;
							var SalesOrderInternalId = ebizOrdNo;
							var getContainerSize = getLPContainerSize;

							var opentaskcount=0;

							opentaskcount=getOpenTasksCount(WaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,OrdName,vZoneId);
							nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
							//For fine tuning pick confirmation and doing autopacking in user event after last item
							var IsitLastPick='F';
							if(opentaskcount > parseFloat(vSkipId) + 1)
								IsitLastPick='F';
							else
								IsitLastPick='T';

							nlapiLogExecution('DEBUG', 'getItem', ItemInternalId);

							ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,
									vBatchno,IsitLastPick,invrefno,SalesOrderInternalId);
							//if(RecCount > 1)
							//if(opentaskcount > parseFloat(vSkipId) + 1)
							if(opentaskcount >  1)
							{
								var SOFilters = new Array();
								SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
								if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
								{
									SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
								}
								if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
								{
									nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
									SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
								}
								if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
								{
									nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
									SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
								}
								if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
								{
									nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
									SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
								}
								if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
								{
									nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
									SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
								}


								var SOColumns = new Array();
								SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//								SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
								//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
								SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
								//Code end as on 290414
								SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
								SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
								SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
								SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
								SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
								SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
								SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
								SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
								SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
								SOColumns.push(new nlobjSearchColumn('name'));
								SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
								SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
								SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
								SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
								SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
								SOColumns[0].setSort();
								SOColumns[1].setSort();
								SOColumns[2].setSort();
								SOColumns[3].setSort();
								SOColumns[4].setSort();
								SOColumns[5].setSort(true);

								var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
								nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
								var nxtitemname='';

								vSkipId=0;

								if (SOSearchResults != null && SOSearchResults.length > 0) 
								{
									if(SOSearchResults.length <= parseFloat(vSkipId))
									{
										vSkipId=0;
									}
									var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
									if(SOSearchResults.length > parseFloat(vSkipId) + 1)
									{
										var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
										SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
										SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
										SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
										nxtitemname = SOSearchnextResult.getText('custrecord_sku');
										nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
									}
									SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
									SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
									//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
									SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
									SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
									SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
									SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
									SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
									SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
									SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
									SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
									SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
									SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
									SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
									SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
									SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
									SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
									SOarray["custparam_noofrecords"] = SOSearchResults.length;		
									SOarray["name"] =  SOSearchResult.getValue('name');
									SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
									SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
									if(vZoneId!=null && vZoneId!="")
									{
										SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
									}
									else
										SOarray["custparam_ebizzoneno"] = '';

								}
								var str = 'BeginLocation. = ' + BeginLocation + '<br>';
								str = str + 'NextShowLocation. = ' + NextShowLocation + '<br>';	
								str = str + 'ItemInternalId. = ' + ItemInternalId + '<br>';
								str = str + 'NextItemInternalId. = ' + NextItemInternalId + '<br>';

								nlapiLogExecution('DEBUG', 'Next Loc and Next Item', str);
								if(BeginLocation != NextShowLocation)
								{ 
									if(nxtloc!='' && nxtloc!=null)
										SOarray["custparam_nextlocation"] = nxtloc;
									if(nxtitem!='' && nxtitem!=null)
										SOarray["custparam_nextiteminternalid"] = nxtitem;

									nlapiLogExecution('DEBUG', 'Next Location', nxtloc);
									nlapiLogExecution('DEBUG', 'Next Item', nxtitem);

									response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
									nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
								}
								else if(ItemInternalId != NextItemInternalId)
								{ 
									if(nxtloc!='' && nxtloc!=null)
										SOarray["custparam_nextlocation"] = nxtloc;
									if(nxtitem!='' && nxtitem!=null)
										SOarray["custparam_nextiteminternalid"] = nxtitem;

								//	SOarray["custparam_itemname"] = nxtitemname;
									nlapiLogExecution('DEBUG', 'Next Location', nxtloc);
									nlapiLogExecution('DEBUG', 'Next Item', nxtitem);	

									response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
									nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
								}
								else if(ItemInternalId == NextItemInternalId)
								{ 
									if(nxtloc!='' && nxtloc!=null)
										SOarray["custparam_nextlocation"] = nxtloc;
									if(nxtitem!='' && nxtitem!=null)
										SOarray["custparam_nextiteminternalid"] = nxtitem;
									SOarray["custparam_itemname"] = nxtitemname;

									nlapiLogExecution('DEBUG', 'Next Location', nxtloc);
									nlapiLogExecution('DEBUG', 'Next Item', nxtitem);	

									response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								
									nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
								}
							}
							else{

								nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
								//For fine tuning pick confirmation and doing autopacking in user event after last item
								//AutoPacking(SalesOrderInternalId);
								/*nlapiLogExecution('DEBUG', 'Navigating To1', 'Foot print');
							response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, SOarray);*/

								//nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
								//For fine tuning pick confirmation and doing autopacking in user event after last item
								//AutoPacking(SalesOrderInternalId);
								nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
								response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);					
							}

						}	
						else
						{
							var SOFilters = new Array();
							SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
							nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
							nlapiLogExecution('DEBUG', 'ClusNo', ClusNo);
							nlapiLogExecution('DEBUG', 'ClusNo', ClusNo);
							nlapiLogExecution('DEBUG', 'ebizOrdNo', ebizOrdNo);
							nlapiLogExecution('DEBUG', 'vZoneId', vZoneId);
							nlapiLogExecution('DEBUG', 'vdono', vdono);

							var SOFilters = new Array();
							SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
							if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
							{
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
							}
							if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
							{
								nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
							}
							if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
							{
								nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
								SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
							}
							if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
							{
								nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
							}
							if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
							{
								nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
								SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
							}

							var SOColumns=new Array();
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//							SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
							//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
							SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
							//Code end as on 290414
							SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
							SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
							SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
							SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
							SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
							SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
							SOColumns.push(new nlobjSearchColumn('name'));
							SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
							SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
							SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
							SOColumns[0].setSort();
							SOColumns[1].setSort();
							SOColumns[2].setSort();
							SOColumns[3].setSort();
							SOColumns[4].setSort();
							SOColumns[5].setSort(true);

							var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
							nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
							if (SOSearchResults != null && SOSearchResults.length > 0) 
							{
								vSkipId=0;

								nlapiLogExecution('DEBUG', 'SOSearchResults length', SOSearchResults.length);
								nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);
								if(SOSearchResults.length <= parseFloat(vSkipId))
								{
									vSkipId=0;
								}
								var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];							
								SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
								SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
								//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
								SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');

								SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
								SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
								SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
								SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
								SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
								SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
								SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
								SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
								SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
								SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
								SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
								SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
								SOarray["custparam_noofrecords"] = SOSearchResults.length;		
								SOarray["name"] =  SOSearchResult.getValue('name');
								SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
								SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
								SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
								if(vZoneId!=null && vZoneId!="")
								{
									SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
								}
								else
									SOarray["custparam_ebizzoneno"] = '';

								if(SOSearchResults.length > parseFloat(vSkipId) + 1)
								{
									var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
									SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
									SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
									SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
									SOarray["custparam_nextbatchno"] = SOSearchnextResult.getValue('custrecord_batch_no');
									nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
									nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
									nlapiLogExecution('DEBUG', 'Next Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));	
								}
								else
								{
									var SOSearchnextResult = SOSearchResults[0];
									SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
									SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
									SOarray["custparam_nextbatchno"] = SOSearchnextResult.getValue('custrecord_batch_no');
									nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
									nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
								}

							}
							response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
						}	
						// response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
					}
				}
			}
			else if (request.getParameter('cmdConfirm') == 'F11') {
				nlapiLogExecution('DEBUG', 'Confirm and stage location');
				var WaveNo = request.getParameter('hdnWaveNo');
				var RecordInternalId = request.getParameter('hdnRecordInternalId');
				var ContainerLPNo = request.getParameter('hdnContainerLpNo');
				var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
				var BeginLocation = request.getParameter('hdnBeginLocationId');
				var Item = request.getParameter('hdnItem');
				var ItemName = request.getParameter('hdnItemName');
				var ItemDescription = request.getParameter('hdnItemDescription');
				var ItemInternalId = request.getParameter('hdnItemInternalId');
				var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
				var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
				var BeginLocationName = request.getParameter('hdnBeginBinLocation');
				var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
				var EndLocation = request.getParameter('hdnEnteredLocation');
				var OrderLineNo = request.getParameter('hdnOrderLineNo');
				var NextShowLocation=request.getParameter('hdnNextLocation');
				var OrdName=request.getParameter('hdnName');
				var ContainerSize=request.getParameter('hdnContainerSize');
				var ebizOrdNo=request.getParameter('hdnebizOrdNo');
				var NextItemInternalId=request.getParameter('hdnNextItemId');
				nlapiLogExecution('DEBUG', 'OrdName', OrdName);
				nlapiLogExecution('DEBUG', 'Next Location', NextShowLocation);
				var vPickType = request.getParameter('hdnpicktype');
				nlapiLogExecution('DEBUG', 'Before Item Fulfillment', 'Before Item Fulfillment');
				//ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo)

				var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

				//if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T') {
				if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') {
					//SOarray["custparam_number"] = 0;
					if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
						SOarray["custparam_number"] = request.getParameter('custparam_number');
						else
							SOarray["custparam_number"] = 0;
					SOarray["custparam_RecType"] = itemSubtype.recordType;
					SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
					SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
					response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
				}
				else
				{		

					//nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
					//Changed the code for auto containerization 
					var RcId=RecordInternalId;
					var EndLocation = EndLocInternalId;
					var TotalWeight=0;
					var getReason=request.getParameter('custparam_enteredReason');
					var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
					var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
					getLPContainerSize= SORec.getFieldText('custrecord_container');

					SOarray["custparam_newcontainerlp"] = getContainerLPNo;

					var vBatchno = request.getParameter('hdnbatchno');
					var PickQty = ExpectedQuantity;
					var vActqty = ExpectedQuantity;
					var SalesOrderInternalId = ebizOrdNo;
					var getContainerSize = getLPContainerSize;
					var  vdono = DoLineId;

					var opentaskcount=0;
					opentaskcount=getOpenTasksCount(WaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,vdono);
					nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
					//For fine tuning pick confirmation and doing autopacking in user event after last item
					var IsitLastPick='F';
					if(opentaskcount > parseFloat(vSkipId) + 1)
						IsitLastPick='F';
					else
						IsitLastPick='T';

					nlapiLogExecution('DEBUG', 'getItem', ItemInternalId);

					ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,
							vBatchno,IsitLastPick,invrefno,SalesOrderInternalId);
					//if(RecCount > 1)

					nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
					//For fine tuning pick confirmation and doing autopacking in user event after last item
					//AutoPacking(SalesOrderInternalId);
					nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
					response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);						 


				}
			}
			else 
			{
				if (request.getParameter('cmdlpexc') == 'F6') {
					nlapiLogExecution('DEBUG', 'Clicked on LP Exception', request.getParameter('cmdlpexc'));
					response.sendRedirect('SUITELET', 'customscript_rf_pickinglpexc', 'customdeploy_rf_pickinglpexc', false, SOarray);
				}
				else
				{
					if (request.getParameter('cmdLocException') == 'F10') {
						nlapiLogExecution('DEBUG', 'Clicked on Location Exception', request.getParameter('cmdLocException'));
						response.sendRedirect('SUITELET', 'customscript_rf_putaway_loc_exception', 'customdeploy_rf_putaway_loc_exception_di', false, SOarray);
					}
					else
					{
						if (request.getParameter('cmdQtyException') == 'F11') {
							nlapiLogExecution('DEBUG', 'Clicked on Quantity Exception', request.getParameter('cmdQtyException'));
							response.sendRedirect('SUITELET', 'customscript_rf_putaway_qty_exception', 'customdeploy_rf_putaway_qty_exception_di', false, SOarray);
						}
						else
						{
							if (request.getParameter('hdnQtyExcpflag') == 'F2') {
								var WaveNo = request.getParameter('hdnWaveNo');
								var ebizOrdNo=request.getParameter('hdnebizOrdNo');
								/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
								var OrdName=request.getParameter('hdnName');
								var vPickType = request.getParameter('hdnpicktype');
								// added OrdName, vPickType parameter to getNextOpentaskInternalId function
								/*** up to here ***/

								SOarray["custparam_nextrecordid"]=getNextOpentaskInternalId(WaveNo,ClusNo,ebizOrdNo,vZoneId,vSkipId,SOarray,vPickType,OrdName,request);
								nlapiLogExecution('DEBUG', 'SOarray[custparam_nextrecordid]',SOarray["custparam_nextrecordid"]);
								nlapiLogExecution('DEBUG', 'Clicked on hdnQtyExcpflag', request.getParameter('hdnQtyExcpflag'));
								response.sendRedirect('SUITELET', 'customscript_rf_pickingqtyexception', 'customdeploy_rf_pickingqtyexception_di', false, SOarray);
								return;

							}
							if (request.getParameter('hdnLocExcpflag') == 'F3') {
								nlapiLogExecution('DEBUG', 'Clicked on cmdLocException', request.getParameter('cmdlocexc'));
								SOarray["custparam_serialexpqty"] = SOarray['custparam_expectedquantity'];
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_loc_exception', 'customdeploy_ebiz_rf_pick_loc_exc_di', false, SOarray);
							}
						}
					}
				}
			}
			nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

			nlapiLogExecution('DEBUG', 'Time Stamp at the end of Response',TimeStampinSec());
		}
	}
}

function ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,
		PickQty,vBatchno,IsitLastPick,invrefno,SalesOrderInternalId)
{
	var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
	var itemCube = 0;
	var itemWeight=0;	

	if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
		//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
		itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
		itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


	} 

	var ContainerCube;					
	var containerInternalId;
	var ContainerSize;
	var vRemaningqty=0;
	nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	

	if(getContainerSize=="" || getContainerSize==null)
	{
		getContainerSize=ContainerSize;
		nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	
	}
	nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	
	var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);					
	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

		ContainerCube =  parseFloat(arrContainerDetails[0]);						
		containerInternalId = arrContainerDetails[3];
		ContainerSize=arrContainerDetails[4];
		TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
	} 
	nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);	
	nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	

	//UpdateOpenTask,fulfillmentorder
	nlapiLogExecution('DEBUG', 'vdono', vdono);	

	UpdateRFOpenTask(RcId,vActqty, containerInternalId,getContainerLPNo,itemCube,itemWeight,EndLocation,getReason,PickQty,
			IsitLastPick,invrefno,SalesOrderInternalId);
	UpdateRFFulfillOrdLine(vdono,vActqty);

	//create new record in opentask,fullfillordline when we have qty exception
	//vRemaningqty = vDisplayedQty-vActqty;
	vRemaningqty = PickQty-vActqty;
	var recordcount=1;//it is iterator in GUI
	nlapiLogExecution('DEBUG', 'vDisplayedQty', PickQty);	
	nlapiLogExecution('DEBUG', 'vActqty', vActqty);	
	if(PickQty != vActqty)
	{
		nlapiLogExecution('DEBUG', 'inexception condition1', 'inexception condition');	
		CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getContainerLPNo,TotalWeight,vBatchno);//have to check for wt and cube calculation
	}
}

function getOpenTasksCount(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdName,vZoneId)
{
	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	 
	SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	return openreccount;

}

function ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, 
		ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo){
	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');

	nlapiLogExecution('DEBUG', "Line Count" + lineCnt);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	/*    
     for (var k = 1; k <= lineCnt; k++)
     {
     var chkVal = request.getLineItemValue('custpage_items', 'custpage_so', k);
     var SONo
     var ItemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
     var itemno = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
     var Lineno = request.getLineItemValue('custpage_items', 'custpage_lineno', k);
     var pickqty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
     var vinvrefno = request.getLineItemValue('custpage_items', 'custpage_invrefno', k);

     var WaveNo = request.getParameter('hdnWaveNo');
     var RecordInternalId = request.getParameter('hdnRecordInternalId');
     var ContainerLPNo = request.getParameter('hdnContainerLpNo');
     var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
     var BeginLocation = request.getParameter('hdnBeginLocationId');
     var Item = request.getParameter('hdnItem');
     var ItemDescription = request.getParameter('hdnItemDescription');
     var ItemInternalId = request.getParameter('hdnItemInternalId');
     var DoLineId = request.getParameter('hdnDOLineId');	//
     var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
     var BeginLocationName = request.getParameter('hdnBeginBinLocation');
     var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
     var EndLocation = getEnteredLocation;

     //        var Recid = request.getLineItemValue('custpage_items', 'custpage_recid', k);
     //        nlapiLogExecution('DEBUG', "Recid " + Recid)
	 */
	//updating allocqty in opentask
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
	OpenTaskTransaction.setFieldValue('custrecord_act_qty', parseFloat(ExpectedQuantity).toFixed(4));
	OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
	OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);

	nlapiLogExecution('DEBUG', 'Done PICK Record Insertion :', 'Success');

	//Updating DO 

	var DOLine = nlapiLoadRecord('customrecord_ebiznet_ordline', DoLineId);
	DOLine.setFieldValue('custrecord_linestatus_flag', '1'); //Statusflag='C'
	DOLine.setFieldValue('custrecord_pickqty', parseFloat(ExpectedQuantity).toFixed(4));
	var SalesOrderInternalId = DOLine.getFieldValue('name');

	try {
		nlapiLogExecution('DEBUG', 'Main Sales Order Internal Id', SalesOrderInternalId);
		var TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');

		var SOLength = TransformRec.getLineItemCount('item');
		nlapiLogExecution('DEBUG', "SO Length", SOLength);

		/*
         for (var j = 0; j < SOLength; j++)
         {
         nlapiLogExecution('DEBUG', "Get Line Value", TransformRec.getLineItemValue('item', 'line', j + 1));
         TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
         }
         TransformRec.setLineItemValue('item', 'itemfulfillment', OrderLineNo, 'T');
         TransformRec.setLineItemValue('item', 'quantity', OrderLineNo, ExpectedQuantity);
         var TransformRecId = nlapiSubmitRecord(TransformRec, true);
		 */
		for (var j = 1; j <= SOLength; j++) {
			nlapiLogExecution('DEBUG', "Get Line Value" + TransformRec.getLineItemValue('item', 'line', j + 1));

			var item_id = TransformRec.getLineItemValue('item', 'item', j);
			var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);

			if (itemLineNo == OrderLineNo) {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
				TransformRec.setCurrentLineItemValue('item', 'quantity', ExpectedQuantity);
				TransformRec.setCurrentLineItemValue('item', 'location', 1);
				TransformRec.commitLineItem('item');
			}
			else {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');
				TransformRec.commitLineItem('item');

			}
			//TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
		}
		//			invtRec.setFieldValue('custrecord_ebiz_inv_loc', '1');//WH Location.

		//			TransformRec.setFieldValue('custbody_ebiz_fulfillmentord',vdono) 
		var TransformRecId = nlapiSubmitRecord(TransformRec, true);
	} 

	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}

	///Updating qty,qoh in inventory
	nlapiLogExecution('DEBUG', 'Invoice Ref No', InvoiceRefNo);
	var InventoryTranaction = nlapiLoadRecord('customrecord_ebiznet_createinv', InvoiceRefNo);

	var InvAllocQty = InventoryTranaction.getFieldValue('custrecord_ebiz_alloc_qty');
	var InvQOH = InventoryTranaction.getFieldValue('custrecord_ebiz_qoh');

	if (isNaN(InvAllocQty)) {
		InvAllocQty = 0;
	}
	InventoryTranaction.setFieldValue('custrecord_ebiz_qoh', (parseFloat(InvQOH) - parseFloat(ExpectedQuantity)).toFixed(4));
	InventoryTranaction.setFieldValue('custrecord_ebiz_alloc_qty', (parseFloat(InvAllocQty) - parseFloat(ExpectedQuantity)).toFixed(4));
	var InventoryTranactionId = nlapiSubmitRecord(InventoryTranaction, false, true);
	nlapiSubmitRecord(OpenTaskTransaction, true);
	nlapiSubmitRecord(DOLine, true);

	//    }

	//    var form = nlapiCreateForm('Pick  Confirmation');
	//    form.addField('custpage_label', 'label', '<h2>Pick Confirmation Successful</h2>');

	//    response.writePage(form);
}
/**
 * 
 * @param RcId
 * @param PickQty
 * @param containerInternalId
 * @param getEnteredContainerNo
 * @param itemCube
 * @param itemWeight
 * @param EndLocation
 */
function UpdateRFOpenTask(RcId, PickQty, containerInternalId, getEnteredContainerNo, itemCube,
		itemWeight, EndLocation,vReason,ActPickQty,IsItLastPick,invrefno,SalesOrderInternalId)
{
	nlapiLogExecution('DEBUG', 'into UpdateRFOpenTask');
	var vinvrefno='';
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();

	nlapiLogExecution('DEBUG', 'invrefno',invrefno);

	if(invrefno==null || invrefno=='')
	{
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
		vinvrefno=transaction.getFieldValue('custrecord_invref_no');
		transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
		if(PickQty!= null && PickQty !="")
			transaction.setFieldValue('custrecord_act_qty', parseFloat(PickQty).toFixed(4));
		if(containerInternalId!= null && containerInternalId !="")
			transaction.setFieldValue('custrecord_container', containerInternalId);
		if(EndLocation!=null && EndLocation!="")
			transaction.setFieldValue('custrecord_actendloc', EndLocation);	
		if(itemWeight!=null && itemWeight!="")
			transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
		if(itemCube!=null && itemCube!="")
			transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
		if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
			transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
		transaction.setFieldValue('custrecord_act_end_date', DateStamp());
		transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
		if(vReason!=null && vReason!="")
			transaction.setFieldValue('custrecord_notes', vReason);	

		transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);

		transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
		var vemployee = request.getParameter('custpage_employee');
		nlapiLogExecution('DEBUG', 'vemployee :', vemployee);
		nlapiLogExecution('DEBUG', 'currentUserID :', currentUserID);
//		if (vemployee != null && vemployee != "") 
//		{
//		transaction.setFieldValue('custrecord_taskassignedto',vemployee);
//		} 
//		else if (currentUserID != null && currentUserID != "") 
//		{
//		transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
//		}


		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('DEBUG', 'Updated RF Open Task successfully');
	}
	else
	{
		vinvrefno = invrefno;
		var fieldNames = new Array(); 
		var newValues = new Array(); 

		fieldNames.push('custrecord_wms_status_flag');  
		fieldNames.push('custrecord_act_qty');
		fieldNames.push('custrecord_device_upload_flag');
		fieldNames.push('custrecord_actendloc');
		fieldNames.push('custrecord_act_end_date');
		fieldNames.push('custrecord_actualendtime');

		newValues.push(8);
		newValues.push(parseFloat(PickQty).toFixed(4));
		newValues.push(IsItLastPick);
		newValues.push(EndLocation);
		newValues.push(DateStamp());
		newValues.push(TimeStamp());

		if(containerInternalId!= null && containerInternalId !="")
		{
			fieldNames.push('custrecord_container');
			newValues.push(containerInternalId);
		}

		if(itemWeight!= null && itemWeight !="")
		{
			fieldNames.push('custrecord_total_weight');
			newValues.push(parseFloat(itemWeight).toFixed(4));
		}

		if(itemCube!= null && itemCube !="")
		{
			fieldNames.push('custrecord_totalcube');
			newValues.push(parseFloat(itemCube).toFixed(4));
		}

		if(getEnteredContainerNo!= null && getEnteredContainerNo !="")
		{
			fieldNames.push('custrecord_container_lp_no');	
			newValues.push(getEnteredContainerNo);
		}

		if(vReason!= null && vReason !="")
		{
			fieldNames.push('custrecord_notes');
			newValues.push(vReason);
		}

		if(currentUserID!= null && currentUserID !="")
		{
			fieldNames.push('custrecord_upd_ebiz_user_no');
			newValues.push(currentUserID);
		}

		nlapiSubmitField('customrecord_ebiznet_trn_opentask', RcId, fieldNames, newValues);

	}

	deleteAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty,SalesOrderInternalId);

	nlapiLogExecution('DEBUG', 'Out of UpdateRFOpenTask');
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,pickqty,contlpno,ActPickQty,SalesOrderInternalId)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	try
	{
		var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

		if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
			Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(ActPickQty)).toFixed(4));
		}

		Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH) - parseFloat(ActPickQty)).toFixed(4)); 
		Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
		Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		if(invtrecid!=null && invtrecid!='' && parseFloat(pickqty)>0)
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty,SalesOrderInternalId);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
}


function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	var pickqtyfinal =0;
	nlapiLogExecution('DEBUG', 'into UpdateRFFulfillOrdLine : ', vdono);

	var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);

	var oldpickQty = doline.getFieldValue('custrecord_pickqty');
	var ordqty = doline.getFieldValue('custrecord_ord_qty');

	if(isNaN(oldpickQty))
		oldpickQty=0;

	pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);

	var str = 'pickqtyfinal. = ' + pickqtyfinal + '<br>';
	str = str + 'ordqty. = ' + ordqty + '<br>';	

	nlapiLogExecution('DEBUG', 'Qty Details', str);

	if(parseFloat(ordqty)>=parseFloat(pickqtyfinal))
	{
		doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal).toFixed(4));

		if(parseFloat(pickqtyfinal)<parseFloat(ordqty))
			doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
		else
			doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
		doline.setFieldValue('custrecord_upddate', DateStamp());
		//doline.setFieldValue('custrecord_pickgen_qty', parseFloat(pickqtyfinal));

		nlapiSubmitRecord(doline, false, true);
	}

	nlapiLogExecution('DEBUG', 'Out of UpdateRFFulfillOrdLine : ', vdono);
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,SalesOrderInternalId) 
{
	nlapiLogExecution('DEBUG', 'Into CreateSTGInvtRecord (Container LP)',vContLp);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');	
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', SalesOrderInternalId);
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of CreateSTGInvtRecord');
}

function getNextOpentaskInternalId(WaveNo,ClusNo,ebizOrdNo,vZoneId,vSkipId,SOarray,vPickType,OrdName,request)
{
	nlapiLogExecution('DEBUG','getNextOpentaskInternalId',getNextOpentaskInternalId);
	nlapiLogExecution('DEBUG','WaveNo',WaveNo);
	nlapiLogExecution('DEBUG','ClusNo',ClusNo);
	nlapiLogExecution('DEBUG','ebizOrdNo',ebizOrdNo);
	nlapiLogExecution('DEBUG','vZoneId',vZoneId);
	nlapiLogExecution('DEBUG','vSkipId',vSkipId);
	nlapiLogExecution('DEBUG','vPickType',vPickType);
	nlapiLogExecution('DEBUG','OrdName',OrdName);

	var recid='';
	var SOFilters = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	var SOColumns=new Array();
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
	SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
	//Code end as on 290414
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));


	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
	nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);
	if (SOSearchResults != null && SOSearchResults.length > 0) 
	{nlapiLogExecution('DEBUG', 'Results.length', SOSearchResults.length);
	//if(SOSearchResults.length > parseFloat(vSkipId) + 1)//commented because last record values from open task are not passing to next screen
	//{

	vSkipId=0;

	if(SOSearchResults.length <= parseInt(vSkipId))
	{
		vSkipId=0;
	}
	var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];							
	SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
	SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
	//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
	SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
	SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
	SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
	SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
	SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
	SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
	SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
	SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
	SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
	SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
	SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
	SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
	SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
	SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
	SOarray["custparam_noofrecords"] = SOSearchResults.length;		
	SOarray["name"] =  SOSearchResult.getValue('name');
	SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
	SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
	if(vZoneId!=null && vZoneId!="")
	{
		SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
	}
	else
		SOarray["custparam_ebizzoneno"] = '';

	if(SOSearchResults.length > parseFloat(vSkipId) + 1)
	{
		var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
		SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
		SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
		nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
		nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));		
		nlapiLogExecution('DEBUG', 'Next Item Ex Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));		
	}
	else
	{
		var SOSearchnextResult = SOSearchResults[0];
		SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
		SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
		nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
		nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
	}

	var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
	if(SOSearchnextResult!=null && SOSearchnextResult!='')
		recid = SOSearchnextResult.getId();
	//}
	}
	return recid;

}

function getOpenReplns(item,location)
{
	nlapiLogExecution('DEBUG', 'Into getOpenReplns');
	var PickFaceSearchResults= new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Task Type - RPLN	 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'is', location));	
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_taskpriority');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, PickFaceFilters, PickFaceColumns);



	return PickFaceSearchResults;

}
//case 20126192 start : added WhLocation as parameter
function getQOHForAllSKUsinPFLocation(location,item,whLocation){

	nlapiLogExecution('DEBUG','Into getQOHForAllSKUsinPFLocation');

	nlapiLogExecution('DEBUG','location',location);
	nlapiLogExecution('DEBUG','item',item);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', whLocation));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('DEBUG','Out of getQOHForAllSKUsinPFLocation');

	return pfSKUInvList;
}

function buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,vZoneId,vPickType)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	var SOColumns = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
	SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
	//Code end as on 290414
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	SOarray["custparam_ebizordno"] = ebizOrdNo;

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_picktype"] = vPickType;
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}

function isPickFaceLocation(item,location)
{
	nlapiLogExecution('DEBUG', 'Into isPickFaceLocation');
	nlapiLogExecution('DEBUG', 'item',item);
	nlapiLogExecution('DEBUG', 'location',location);
	var isPickFace='F';
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_autoreplen');
	var PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);
	if(PickFaceSearchResults!=null && PickFaceSearchResults!=''&& PickFaceSearchResults.length>0)
	{
		isPickFace='T';
	}

	nlapiLogExecution('DEBUG', 'Return Value',isPickFace);
	nlapiLogExecution('DEBUG', 'Out of isPickFaceLocation');

	return isPickFace;
}
