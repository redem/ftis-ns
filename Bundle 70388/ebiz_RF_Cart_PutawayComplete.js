/***************************************************************************
������������������������������� � ��������������������������
  eBizNET Solutions Inc 
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_PutawayComplete.js,v $
*� $Revision: 1.2.2.3.4.3.4.7.2.1 $
*� $Date: 2014/09/16 14:41:57 $
*� $Author: skreddy $
*� $Name: t_eBN_2014_2_StdBundle_0_100 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_Cart_PutawayComplete.js,v $
*� Revision 1.2.2.3.4.3.4.7.2.1  2014/09/16 14:41:57  skreddy
*� case # 201410256
*�  TPP SB issue fix
*�
*� Revision 1.2.2.3.4.3.4.7  2014/06/13 07:48:00  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.2.2.3.4.3.4.6  2014/06/06 06:23:26  skavuri
*� Case# 20148749 (Refresh Functionality ) SB Issue Fixed
*�
*� Revision 1.2.2.3.4.3.4.5  2014/05/30 00:26:47  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.2.2.3.4.3.4.4  2013/06/11 14:30:41  schepuri
*� Error Code Change ERROR to DEBUG
*�
*� Revision 1.2.2.3.4.3.4.3  2013/05/10 00:33:08  kavitha
*� CASE201112/CR201113/LOG201121
*� Issue fixes
*�
*� Revision 1.2.2.3.4.3.4.2  2013/04/17 16:04:01  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.2.2.3.4.3.4.1  2013/03/08 14:35:02  skreddy
*� CASE201112/CR201113/LOG201121
*� Code merged from Endochoice as part of Standard bundle
*�
*� Revision 1.2.2.3.4.3  2012/09/27 10:54:36  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting multiple language with given Spanish terms
*�
*� Revision 1.2.2.3.4.2  2012/09/26 12:45:35  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.2.2.3.4.1  2012/09/21 14:57:16  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.2.2.3  2012/03/19 10:03:18  spendyala
*� CASE201112/CR201113/LOG201121
*� Disable button functionality is been added.
*�
*� Revision 1.2.2.2  2012/02/22 12:16:58  schepuri
*� CASE201112/CR201113/LOG201121
*� function Key Script code merged
*�
*� Revision 1.2  2012/02/16 10:33:33  schepuri
*� CASE201112/CR201113/LOG201121
*� Added FunctionkeyScript
*�
*� Revision 1.1  2012/02/02 08:58:33  rmukkera
*� CASE201112/CR201113/LOG201121
*�   cartlp new file
*�
*
****************************************************************************/


function PutawayComplete(request, response)
{
    if (request.getMethod() == 'GET') 
	{
		var getRecordCount = request.getParameter('custparam_recordcount');

		var getconfirmedLPCount = request.getParameter('custparam_confirmedLpCount');
		var getlpCount = request.getParameter('custparam_lpCount');
		var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
		nlapiLogExecution('DEBUG', 'confirmedLPCount ', getconfirmedLPCount);
		nlapiLogExecution('DEBUG', 'lpCount ', getlpCount);
		nlapiLogExecution('DEBUG', 'TempLPNoArray ', TempLPNoArray);
		//var optedEvent1 = request.getParameter('custparam_optedEvent');
		//nlapiLogExecution('DEBUG', 'optedEvent ', optedEvent1);
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
			
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "TAREA COMPLETADA"; 
			st2 = "PRESIONE F8 PARA NUEVA TAREA DE RECOLECCI&#211;N";
			st3 = "PR&#211;XIMO";
			
    	}
		else
		{
			st0 = "";
			st1 = "TASK COMPLETED";
			st2 = "PRESS F8 FOR NEW PUTAWAY TASK";
			st3 = "NEXT";
			

		}
		
		
		
		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
       	var html = "<html><head><title>" + st0 + "</title>";
       	html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
        html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
      //Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
        //html = html + " document.getElementById('cmdSend').focus();";        
        html = html + "</script>";
        html = html +functionkeyHtml;
		 html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
        html = html + "	<form name='_rf_putaway_lp' method='POST'>";
        html = html + "		<table>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st1 + " </td></tr>";
        html = html + "				<td align = 'left'>" + st2;
		html = html + "				<input type='hidden' name='hdnRecordCount' value=" + getRecordCount + "></td>";
		html = html + "				<input type='hidden' name='hdnconfirmedLPCount' value=" + getconfirmedLPCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpCount' value=" + getlpCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpNumbersArray' value=" + TempLPNoArray + "></td>";
        html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
        html = html + "				<input type='hidden' name='hdnflag'>";
        html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' id='cmdSend' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "		 </table>";
        html = html + "	</form>";
      //Case# 20148882 (added Focus Functionality for Textbox)
        html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
        html = html + "</body>";
        html = html + "</html>";
        
        response.write(html);
    }
    else 
	{
        nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
        
		getRecordCount = request.getParameter('hdnRecordCount');
		nlapiLogExecution('DEBUG', 'getRecordCount', getRecordCount);

		var getOptedField = request.getParameter('hdnOptedField');
		var getconfirmedLPCount = request.getParameter('hdnconfirmedLPCount');
		var getlpCount = request.getParameter('hdnlpCount');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
        // to the previous screen.
        var optedEvent = request.getParameter('hdnflag');
        //nlapiLogExecution('DEBUG', 'optedEvent1', optedEvent1);
        //	if the previous button 'F7' is clicked, it has to go to the previous screen 
        //  ie., it has to go to accept PO #.
        
        var POarray = new Array();
        
        var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
        
        POarray["custparam_lpNumbersArray"] = request.getParameter('hdnlpNumbersArray');
		POarray["custparam_confirmedLpCount"] = request.getParameter('hdnconfirmedLPCount');
		POarray["custparam_lpCount"] = request.getParameter('hdnlpCount');
		
        if (optedEvent == 'F8') 
		{
//            if (getOptedField == 4) 
//			{
//                response.sendRedirect('SUITELET', 'customscript_rf_putaway_cartlp', 'customdeploy_rf_putaway_cartlp_di', false, optedEvent);
//            }
//			else
//			{
				if(getconfirmedLPCount < getlpCount){
					nlapiLogExecution('DEBUG', 'if', 'if');
					nlapiLogExecution('DEBUG', 'custparam_confirmedLpCount if', POarray["custparam_confirmedLpCount"]);
					nlapiLogExecution('DEBUG', 'optedEvent if', optedEvent);
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
				}
				else{
					nlapiLogExecution('DEBUG', 'else', 'else');
					nlapiLogExecution('DEBUG', 'optedEvent else', optedEvent);
					var filtersmlp = new Array();
					filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_cart_closeflag', null, 'is', 'T');
					filtersmlp[1] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'anyof', 4);
					filtersmlp[2]= new nlobjSearchFilter('isinactive',null, 'is','F');
					var columns = new Array(); 
					columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
					columns[1] = new nlobjSearchColumn('custrecord_ebiz_cart_closeflag');

					var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp,columns);
					if(SrchRecord!=null && SrchRecord.length > 0)
						{
						 response.sendRedirect('SUITELET', 'customscript_rf_putaway_cartlp', 'customdeploy_rf_putaway_cartlp_di', false, POarray);
						}
					else
						{
						response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
						}
					
				}
//			}
        }
        nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
