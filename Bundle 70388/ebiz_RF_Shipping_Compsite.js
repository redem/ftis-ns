/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_Shipping_Compsite.js,v $
 *     	   $Revision: 1.1.2.1.4.9.2.1 $
 *     	   $Date: 2014/07/16 15:25:46 $
 *     	   $Author: skavuri $
 *     	   $Name: t_eBN_2014_1_StdBundle_3_175 $
 *This Suitelet is meant to scan or enter the licence plate # for which the item is created.
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * REVISION HISTORY
 * $Log: ebiz_RF_Shipping_Compsite.js,v $
 * Revision 1.1.2.1.4.9.2.1  2014/07/16 15:25:46  skavuri
 * Case# 20148691 SB Issue Fixed
 *
 * Revision 1.1.2.1.4.9  2014/06/13 11:06:44  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.1.4.8  2014/06/06 15:18:53  sponnaganti
 * case# 20148757
 * stnd Bundle Issue Fix
 *
 * Revision 1.1.2.1.4.7  2014/06/04 15:20:13  skavuri
 * Case# 20148691 SB Issue Fixed
 *
 * Revision 1.1.2.1.4.6  2014/05/30 00:41:06  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.4.5  2014/04/16 06:45:32  skreddy
 * case # 20127790
 * Dealmed sb  issue fix
 *
 * Revision 1.1.2.1.4.4  2013/09/06 07:34:24  rmukkera
 * Case# 20124261
 *
 * Revision 1.1.2.1.4.3  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.1.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.4.1  2013/03/05 14:47:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.1.2.1  2012/12/24 13:18:02  schepuri
 * CASE201112/CR201113/LOG201121
 * added new screen for multiple sites
 *
 * Revision 1.2.4.3  2012/05/08 23:24:41  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate valid company
 *
 * Revision 1.2.4.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.1  2012/02/21 13:24:50  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2  2011/09/16 07:32:24  schepuri
 * CASE201112/CR201113/LOG201121
 * duplicate lps
 *
 * Revision 1.2  2011/09/16 07:22:56  schepuri
 * CASE201112/CR201113/LOG201121
 * duplicate lps
 *
 * 
 */

function LocationCompany(request, response)
{		


	if (request.getMethod() == 'GET') 
	{				
		var functionkeyHtml=getFunctionkeyScript('_rf_sitecomp'); 
		var html = "<html><head><title>Location</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlocation').focus();";           
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_sitecomp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LOCATION:";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text' />";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>COMPANY:";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercompany' type='text'  />";
		html = html + "				</td>";		
		html = html + "			</tr>";       
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		var CIarray = new Array();

		var getEnterLocation = request.getParameter('enterlocation');
		var getEnterCompany = request.getParameter('entercompany');

		var Locintid ='';
		CIarray["custparam_screenno"] = 'SHIPLOC';	

		//if Both are not equal then fetch internal id of new entered location
		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_shipping_menu', 'customdeploy_rf_shipping_menu_di', false, CIarray);
		}
		else 
		{
			var fetchedcompany='';
			if(getEnterLocation!="" && getEnterLocation !=null)
			{
				var newLocationFilters = new Array();	
				newLocationFilters.push(new nlobjSearchFilter('name',null, 'is',getEnterLocation));	
				var newLocationColumn = new Array();	    	
				newLocationColumn.push(new nlobjSearchColumn('internalid'));
				//case# 20148757 starts
				newLocationColumn.push(new nlobjSearchColumn('custrecordcompany'));
				//case# 20148757 ends		
				var newLocationResults = nlapiSearchRecord('location', null, newLocationFilters, newLocationColumn);			    	

				if (newLocationResults != null) 
				{	       		
					nlapiLogExecution('DEBUG', 'newLocationResults', newLocationResults.length);
					Locintid = newLocationResults[0].getId();
					fetchedcompany=newLocationResults[0].getText('custrecordcompany');
					CIarray["custparam_locationId"] = Locintid;
					response.sendRedirect('SUITELET', 'customscript_rf_shipping_shiplp', 'customdeploy_rf_shipping_shiplp_di', false, CIarray);
				}        		
				else
				{	
					CIarray["custparam_error"] = 'INVALID LOCATION';
					nlapiLogExecution('DEBUG', 'Location Does not Exists');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				}
				if(getEnterCompany !=null && getEnterCompany !='')
				{
					
					nlapiLogExecution('DEBUG', 'getEnterCompany', getEnterCompany);
					var result=ValidateSplCharacter(getEnterCompany,'');
					nlapiLogExecution('DEBUG', 'result', result);
					
					if(result == false)
					{
						CIarray["custparam_error"] = 'SPECIAL CHARACTERS ARE NOT ALLOWED ';
						nlapiLogExecution('DEBUG', 'special characeters ');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					}
					//case# 20148757 starts
					//else if(fetchedcompany!=getEnterCompany.toUpperCase())
					else if(fetchedcompany.toUpperCase()!=getEnterCompany.toUpperCase())//Case# 20148691
					{
						CIarray["custparam_error"] = 'INVALID COMPANY';
						nlapiLogExecution('DEBUG', 'INVALID COMPANY ');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					}//case# 20148757 ends
					else
					{
						nlapiLogExecution('DEBUG', 'success ');
						response.sendRedirect('SUITELET', 'customscript_rf_shipping_shiplp', 'customdeploy_rf_shipping_shiplp_di', false, CIarray);
					}
				}
				nlapiLogExecution('DEBUG', 'newLocationinteralid', Locintid);	    		    	
			}
			else
			{
				CIarray["custparam_error"] = 'PLEASE ENTER LOCATION';
				nlapiLogExecution('DEBUG', 'Location Does not Exists');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
			}


		}

	}

}

function ValidateSplCharacter(string,name)
{
	try
	{
		//var iChars = "*|,\":<>[]{}`\';()@&$#% ";
		var iChars = "*|,\":<>[]{}`\';()@&$#%";// Case# 20148691(space removing from string)
		var length=string.length;
		var flag = 'N';
		for(var i=0;i<length;i++)
		{
			if(iChars.indexOf(string.charAt(i))!=-1)
			{
				flag='Y';
				break;
			}
		}
		if(flag == 'Y')
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	catch(e)
	{
		nlapiLogExecution("ERROR","ValidateSplCharacter",e);
		return false;
	}
}
