/***************************************************************************
 eBizNET Solutions Inc                
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingLocation.js,v $
 *     	   $Revision: 1.10.2.18.4.11.2.16.2.1 $
 *     	   $Date: 2014/12/09 06:31:17 $
 *     	   $Author: spendyala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingLocation.js,v $
 * Revision 1.10.2.18.4.11.2.16.2.1  2014/12/09 06:31:17  spendyala
 * case # 201411232
 *
 * Revision 1.10.2.18.4.11.2.16  2014/06/13 14:58:54  grao
 * Case#: 20148454�
 * Standard bundle issue fixes
 *
 * Revision 1.10.2.18.4.11.2.15  2014/06/13 12:29:37  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.10.2.18.4.11.2.14  2014/06/06 08:10:10  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.10.2.18.4.11.2.13  2014/05/30 00:41:04  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.10.2.18.4.11.2.12  2014/05/27 13:57:23  snimmakayala
 * Case#: 20148567
 * RF Packing issue fixes
 *
 * Revision 1.10.2.18.4.11.2.11  2014/05/26 07:08:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.10.2.18.4.11.2.10  2013/10/24 13:35:10  rmukkera
 * Case# 20125184
 *
 * Revision 1.10.2.18.4.11.2.9  2013/06/26 15:04:57  snimmakayala
 * Case# : 20123126
 * GSUSA :: Issue Fixes
 *
 * Revision 1.10.2.18.4.11.2.8  2013/06/20 14:13:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 201215543
 *
 * Revision 1.10.2.18.4.11.2.7  2013/06/19 22:56:06  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 *
 * Revision 1.10.2.18.4.11.2.6  2013/06/11 14:30:20  schepuri
 * Error Code Change DEBUG to DEBUG
 *
 * Revision 1.10.2.18.4.11.2.5  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.10.2.18.4.11.2.4  2013/04/08 08:47:04  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.10.2.18.4.11.2.3  2013/03/13 13:57:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.10.2.18.4.11.2.2  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.10.2.18.4.11.2.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.10.2.18.4.11  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.10.2.18.4.10  2013/01/25 06:56:49  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to wrong location
 *
 * Revision 1.10.2.18.4.9  2012/12/21 09:29:26  mbpragada
 * 20120490
 *
 * Revision 1.10.2.18.4.8  2012/12/21 09:05:15  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.10.2.18.4.7  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.10.2.18.4.6  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.10.2.18.4.5  2012/10/11 10:27:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick confirmation and other inventory issues
 *
 * Revision 1.10.2.18.4.4  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.10.2.18.4.3  2012/10/04 10:28:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.10.2.18.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.10.2.18.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.10.2.18  2012/08/28 16:00:15  spendyala
 * CASE201112/CR201113/LOG201121
 * Driving back to previous screen issue resolved.
 *
 * Revision 1.10.2.17  2012/08/26 17:55:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Pick Confirm issues for FISK
 *
 * Revision 1.10.2.16  2012/08/09 07:32:24  spendyala
 * CASE201112/CR201113/LOG201121
 * Driving user to new screen i.e Wave and cluster summary report.
 *
 * Revision 1.10.2.15  2012/08/07 21:18:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.10.2.14  2012/08/01 07:16:42  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Routing based on Loc group seq
 *
 * Revision 1.10.2.13  2012/07/19 05:22:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.10.2.12  2012/07/09 06:50:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.10.2.11  2012/04/26 07:10:21  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issue unable to scan the item when we placed two items in a single
 *  location.
 *
 * Revision 1.10.2.10  2012/04/24 13:49:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.10.2.9  2012/04/23 13:37:38  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issues in skip location functionality in RF Picking
 *
 * Revision 1.10.2.8  2012/04/19 15:00:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.10.2.7  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.10.2.6  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.10.2.5  2012/03/09 08:09:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Code Merged from trunk.
 *
 * Revision 1.10.2.4  2012/02/21 15:37:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.10.2.3  2012/02/21 07:05:07  spendyala
 * CASE201112/CR201113/LOG201121
 * Code syn b/w Trunck and Branch.
 *
 * Revision 1.10.2.2  2012/02/08 15:27:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * functional keys
 *
 * Revision 1.10.2.1  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.11  2012/01/23 23:21:17  gkalla
 * CASE201112/CR201113/LOG201121
 * RF Picking customized
 *
 * Revision 1.10  2011/12/16 12:30:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/12/12 12:58:54  schepuri
 * CASE201112/CR201113/LOG201121
 * adding new button for RF PICK Exception
 *
 * Revision 1.7  2011/09/29 16:17:23  gkalla
 * CASE201112/CR201113/LOG201121
 * To integrate Serial no entry in RF outbound
 *
 * Revision 1.6  2011/09/10 07:24:50  skota
 * CASE201112/CR201113/LOG201121
 * code changes to redirect to right suitelet for capturing serial nos
 *
 * Revision 1.5  2011/09/09 18:08:24  skota
 * CASE201112/CR201113/LOG201121
 * If condition change to consider serial out flag
 *
 * 
 *****************************************************************************/
function PickingLocation(request, response){
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();
	
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Debug', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR A LA UBICACI&#211;N:";
			st2 = "INGRESAR / ESCANEO DE LA UBICACI&#211;N";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "OVERRIDE";
			st6 = "SKIP";
		}
		else
		{
			st1 = "GO TO LOCATION: ";
			st2 = "ENTER/SCAN LOCATION ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "OVERRIDE";
			st6 = "SKIP";
		}


		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		//var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getContainerLpNo = request.getParameter('custparam_newcontainerlp');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var getBeginLocationName = request.getParameter('custparam_beginlocationname');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchNo = request.getParameter('custparam_batchno');	
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');
		var OrdNo=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var vSOId=request.getParameter('custparam_ebizordno');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('Debug', 'getItemInternalId', getItemInternalId);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		nlapiLogExecution('Debug', 'before getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Debug', 'before getItemInternalId', getItemInternalId);
		if(getFetchedLocation==null || getFetchedLocation == "")
		{
			getFetchedLocation=NextLocation;
		}

		if(getItemInternalId==null || getItemInternalId == "")
		{
			getItemInternalId=NextItemInternalId;
		}
		//code added by santosh on 7Aug2012
		var Type='';
		if(request.getParameter('custparam_type') !=null &&  request.getParameter('custparam_type')!="")
		{
			Type=request.getParameter('custparam_type');
		}
		nlapiLogExecution('Debug', 'Type', Type);
		//end of code on 7aug2012
		nlapiLogExecution('Debug', 'after getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Debug', 'after getItemInternalId', getItemInternalId);
		nlapiLogExecution('Debug', 'getBeginLocation ID', getFetchedBeginLocation);
		nlapiLogExecution('Debug', 'getBeginLocation', getFetchedLocation);
		nlapiLogExecution('Debug', 'before pickType in loc get', pickType);
		nlapiLogExecution('Debug', 'NextItemInternalId', NextItemInternalId); 

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends     
		//html = html + " document.getElementById('enterlocation').focus();";  

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + OrdNo + "</label> ## WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " <label>" + getFetchedLocation + "</label>";
		html = html + "				<input type='hidden' name='hdntype' value=" + Type + ">";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value='" + getFetchedLocation + "'>";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo  + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + vBatchNo  + ">";	
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";	
		html = html + "				<input type='hidden' name='hdnOrdNo' value=" + OrdNo + ">";
		html = html + "				<input type='hidden' name='hdnsoid' value=" + vSOId + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";	
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2;//ENTER/SCAN LOCATION 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true;this.form.cmdPrevious.disabled=true; this.form.cmdOverride.disabled=true;this.form.cmdSKIP.disabled=true;return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					" + st5 + " <input name='cmdOverride' type='submit' value='F11'/>";
		html = html + "					" + st6 + " <input name='cmdSKIP' type='submit' value='F12'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('Debug', 'Into Response', 'Into Response');
		var getLanguage = request.getParameter('hdngetLanguage');

		var st7;

		if( getLanguage == 'es_ES')
		{
			st7 = "UBICACI&#211;N NO V&#193;LIDA";

		}
		else
		{
			st7 = "INVALID LOCATION";
		}


		var getBeginLocation='';		
		var getBeginLocationName='';	
		var WaveNo=request.getParameter('hdnWaveNo');
		var OrdNo=request.getParameter('hdnOrdNo');
		var ClusNo = request.getParameter('hdnClusterNo');
		var NextShowLocation=request.getParameter('hdnBeginLocation');
		var NextShowItemId=request.getParameter('hdnItemInternalId');
		var vSOId=request.getParameter('hdnsoid');
		var vZoneId=request.getParameter('hdnebizzoneno');
		var vSkipId=request.getParameter('hdnskipid');
		var containerlp = request.getParameter('hdnContainerLpNo');
		var fointrid = request.getParameter('hdnDOLineId');
		var ordline = request.getParameter('hdnOrderLineNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getItem = request.getParameter('hdnItem');
		var getItemName = request.getParameter('hdnItemName');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var vBatchNo = request.getParameter('hdnBatchNo');	
		var whLocation = request.getParameter('hdnwhlocation');
		var whCompany = request.getParameter('hdnwhCompany');
		var RecordCount=request.getParameter('hdnRecCount');
		var ContainerSize=request.getParameter('hdnContainerSize');
		var Type=request.getParameter('hdntype');
		var vPickType=request.getParameter('hdnpicktype');

		var str = 'NextShowLocation. = ' + NextShowLocation + '<br>';
		str = str + 'NextShowItemId. = ' + NextShowItemId + '<br>';	
		str = str + 'WaveNo. = ' + WaveNo + '<br>';	
		str = str + 'OrdNo. = ' + OrdNo + '<br>';	
		str = str + 'ClusNo. = ' + ClusNo + '<br>';	
		str = str + 'vSkipId. = ' + vSkipId + '<br>';	
		str = str + 'vPickType. = ' + vPickType + '<br>';	
		str = str + 'vSOId. = ' + vSOId + '<br>';	
		str = str + 'vZoneId. = ' + vZoneId + '<br>';	

		nlapiLogExecution('Debug', 'Parameter Values', str);

		var SOarray = new Array();
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_ondemandstage"] = 'N';
		SOarray["custparam_language"] = getLanguage;

		
		if (sessionobj!=context.getUser()) 
		{
			try
			{
				if(request.getParameter('cmdOverride')!='F11')
				{
					var RecCount;
					var SOFilters = new Array();
					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

					if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
					{
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
					}

					if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
					{
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
					}

					if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdNo!=null && OrdNo!="" && OrdNo!= "null")
					{				
						SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
					}

					if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
					{
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
					}

					if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!="null")
					{
						SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
					}

					SOarray["custparam_ebizordno"] = vSOId;
					var SOColumns = new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
					SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
					//Code end as on 290414
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('Debug', 'Results', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0) {
						nlapiLogExecution('Debug', 'SOSearchResults.length', SOSearchResults.length);
						nlapiLogExecution('Debug', 'vSkipId', vSkipId);
						vSkipId=0;
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}

						nlapiLogExecution('Debug', 'vSkipId', vSkipId);
						var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
						if(SOSearchResults.length > parseFloat(vSkipId) + 1)
						{
							var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
							SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
							SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
							nlapiLogExecution('Debug', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
							nlapiLogExecution('Debug', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
						}
						else
						{
							var SOSearchnextResult = SOSearchResults[0];
							SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
							SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
						}

						SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						getRecordInternalId = SOSearchResult.getId();
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
						SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
						SOarray["custparam_noofrecords"] = SOSearchResults.length;		
						SOarray["name"] =  SOSearchResult.getValue('name');
						SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
						SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');

						if(vZoneId!=null && vZoneId!="")
						{
							SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
						}
						else
							SOarray["custparam_ebizzoneno"] = '';
						getBeginLocationName = SOSearchResult.getText('custrecord_actbeginloc');

					}

					var getEnteredLocation = request.getParameter('enterlocation');		
					nlapiLogExecution('Debug', 'Entered Location', getEnteredLocation);	
					nlapiLogExecution('Debug', 'Location', getBeginLocationName);	
					nlapiLogExecution('Debug', 'Name',  SOarray["name"]);	
					var vClusterNo = request.getParameter('hdnClusterNo');

					// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
					// to the previous screen.
					var optedEvent = request.getParameter('cmdPrevious');
					var optskipEvent = request.getParameter('cmdSKIP');


					SOarray["custparam_error"] = st7;//'INVALID LOCATION';
					SOarray["custparam_screenno"] = '14';

					//	if the previous button 'F7' is clicked, it has to go to the previous screen 
					//  ie., it has to go to accept SO #.
					if (optedEvent == 'F7') {	
						//code added by santosh on 7Aug2012
						SOarray["custparam_venterSO"]=vSOId;
						SOarray["custparam_venterFO"]=OrdNo;
						SOarray["custparam_venterwave"]=WaveNo;
						SOarray["custparam_venterzone"]=vZoneId;
						if(Type=="Wave")
						{ 
							SOarray["custparam_type"] ="Wave";
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusterwave_summary', 'customdeploy_ebiz_rf_clusterwave_summary', false, SOarray);
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
						}
						//end of the code on 7Aug2012
					}
					else if(optskipEvent == 'F12')
					{
						nlapiLogExecution('Debug', 'Entered into SKIP',getRecordInternalId);
						var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
						if(skiptask==null || skiptask=='')
						{
							skiptask=1;
						}
						else
						{
							skiptask=parseInt(skiptask)+1;
						}

						nlapiLogExecution('Debug', 'skiptask',skiptask);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);
						vSkipId= parseFloat(vSkipId) + 1;
						SOarray["custparam_skipid"] = vSkipId;		
						SOarray["custparam_beginLocationname"]=null;
						SOarray["custparam_beginlocationname"]=null;
						SOarray["custparam_beginLocation"]=null;
						SOarray["custparam_iteminternalid"]=null;
						response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
					}

					else {
						//To Remove case sensitive
						nlapiLogExecution('Debug', 'into else', 'done');	
						nlapiLogExecution('Debug', 'getBeginLocationName.toUpperCase()', getBeginLocationName.toUpperCase());	
						nlapiLogExecution('Debug', 'getEnteredLocation.toUpperCase()', getEnteredLocation.toUpperCase());
						if (getEnteredLocation != '' && getEnteredLocation.toUpperCase() == getBeginLocationName.toUpperCase()) {

							//Updating begin time of the task.
							if(getRecordInternalId!=null && getRecordInternalId!='')
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_actualbegintime', TimeStamp());
							}

							var binFilters = new Array();
							var binColumns = new Array();

							binFilters.push(new nlobjSearchFilter('name', null, 'is', getEnteredLocation));
							binFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

							var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, binFilters,binColumns);

							nlapiLogExecution('Debug', 'Length of Location Search', LocationSearch.length);

							if (LocationSearch.length != 0) {
								for (var s = 0; s < LocationSearch.length; s++) {
									var getEndLocationInternalId = LocationSearch[s].getId();
									nlapiLogExecution('Debug', 'End Location Id', getEndLocationInternalId);
								}

								SOarray["custparam_beginlocationname"] = getBeginLocationName;
								SOarray["custparam_beginLocationname"] = getBeginLocationName;
								SOarray["custparam_endlocinternalid"] = getEndLocationInternalId;
								SOarray["custparam_endlocation"] = getBeginLocationName;
								SOarray["custparam_clusterno"] = vClusterNo;	
								nlapiLogExecution('Debug', 'getBeginLocation',SOarray["custparam_beginLocation"]);
								var getBeginLocationid=	SOarray["custparam_beginLocation"];
								var wave=request.getParameter('hdnWaveNo');
								SOarray["custparam_remqty"]=0;

								//Added code specific to GSUSA.
								//Instead of checking weather the item has qty in pick face location and prompting user
								//to confirm replenishment at the end of the transaction .We are checking here only.
								var invtqty=0;

								var IsPickFaceLoc = 'F';
								var ItemInternalId=SOarray["custparam_iteminternalid"];
								var EndLocInternalId=getEndLocationInternalId;
								var ExpectedQuantity=SOarray["custparam_expectedquantity"];
								var InvoiceRefNo=SOarray["custparam_invoicerefno"];
								var RecordInternalId=getRecordInternalId;
								var WaveNo=wave;
								var ClusNo=SOarray["custparam_clusterno"];
								var OrdName=SOarray["name"];
								var ebizOrdNo=SOarray["custparam_ebizordno"];
								var getZoneNo=SOarray["custparam_ebizzoneno"];
								var whLocation=SOarray["custparam_whlocation"];
								IsPickFaceLoc = isPickFaceLocation(ItemInternalId,EndLocInternalId);

								nlapiLogExecution('Debug', 'ItemInternalId', ItemInternalId);
								nlapiLogExecution('Debug', 'EndLocInternalId', EndLocInternalId);
								nlapiLogExecution('Debug', 'ExpectedQuantity', ExpectedQuantity);
								nlapiLogExecution('Debug', 'IsPickFaceLoc', IsPickFaceLoc);
								nlapiLogExecution('Debug', 'InvoiceRefNo', InvoiceRefNo);
								nlapiLogExecution('Debug', 'RecordInternalId', RecordInternalId);
								nlapiLogExecution('Debug', 'WaveNo', WaveNo);
								nlapiLogExecution('Debug', 'ClusNo', ClusNo);
								nlapiLogExecution('Debug', 'OrdName', OrdName);
								nlapiLogExecution('Debug', 'ebizOrdNo', ebizOrdNo);
								nlapiLogExecution('Debug', 'whLocation', whLocation);

								if(IsPickFaceLoc=='T')
								{
									var openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemInternalId);

									if(openinventory!=null && openinventory!='')
									{
										nlapiLogExecution('Debug', 'Pick Face Inventory Length', openinventory.length);

										for(var x=0; x< openinventory.length;x++)
										{
											var vinvtqty=openinventory[0].getValue('custrecord_ebiz_qoh',null,'sum');

											if(vinvtqty==null || vinvtqty=='' || isNaN(vinvtqty))
											{
												vinvtqty=0;
											}
											nlapiLogExecution('Debug', 'vinvtqty',vinvtqty);
											invtqty=parseInt(invtqty)+parseInt(vinvtqty);
										}						
									}
								}
								nlapiLogExecution('Debug', 'invtqty', invtqty);
								if(invtqty==null || invtqty=='' || isNaN(invtqty))
								{
									invtqty=0;
								}

								nlapiLogExecution('Debug', 'invtqty', invtqty);

								if(IsPickFaceLoc=='T' && (parseInt(ExpectedQuantity)>parseInt(invtqty)))
								{
									var openreplns = new Array();
									openreplns=getOpenReplns(ItemInternalId,EndLocInternalId);

									if(openreplns!=null && openreplns!='')
									{
										for(var i=0; i< openreplns.length;i++)
										{
											var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
											var expqty=openreplns[i].getValue('custrecord_expe_qty');
											if(expqty == null || expqty == '')
											{
												expqty=0;
											}	
											nlapiLogExecution('Debug', 'open repln qty',expqty);
											nlapiLogExecution('Debug', 'getQuantity',ExpectedQuantity);

											var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
											if(skiptask==null || skiptask=='')
											{
												skiptask=1;
											}
											else
											{
												skiptask=parseInt(skiptask)+1;
											}

											nlapiLogExecution('Debug', 'skiptask',skiptask);

											nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

											var vPickType = request.getParameter('hdnpicktype');
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');

										}

										var SOarray=new Array();
										SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);
										SOarray["custparam_skipid"] = vSkipId;
										if(NextShowLocation!=null && NextShowLocation!='')
											SOarray["custparam_screenno"] = '11EXP';
										else
											SOarray["custparam_screenno"] = '11LOCEXP';	
										SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
										return;
									}
									else
									{
										var replengen = generateReplenishment(EndLocInternalId,ItemInternalId,whLocation,WaveNo);
										nlapiLogExecution('Debug', 'replengen',replengen);

										//If repeln generation failed, redirect to item so that user will perform short pick.
										if(replengen!=true)
										{
											nlapiLogExecution('ERROR', 'Replen Generation Failed.Insufficient inventory in Bulk location');
											nlapiLogExecution('ERROR', 'Redirecting to Item scan...');
											response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);					
											return;
										}

										var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
										if(skiptask==null || skiptask=='')
										{
											skiptask=1;
										}
										else
										{
											skiptask=parseInt(skiptask)+1;
										}

										nlapiLogExecution('Debug', 'skiptask',skiptask);

										nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

										var vPickType = request.getParameter('hdnpicktype');
										var SOarray=new Array();

										SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);

										SOarray["custparam_skipid"] = vSkipId;
										if(NextShowLocation!=null && NextShowLocation!='')
											SOarray["custparam_screenno"] = '11EXP';
										else
											SOarray["custparam_screenno"] = '11LOCEXP';	

										if(replengen!=true)
											SOarray["custparam_error"]='Replen Generation Failed.Insufficient inventory in Bulk location';
										else 
											SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';

										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
										return;
									}

								}				
								else
								{
									if(IsPickFaceLoc=='T' && (InvoiceRefNo==null || InvoiceRefNo==''))
									{
										if(openinventory!=null && openinventory!='')
										{
											var openinvtrecid = openinventory[0].getId();
											nlapiLogExecution('Debug', 'openinvtrecid', openinvtrecid);

											try
											{
												nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_invref_no', openinvtrecid);
											}
											catch(exp)
											{
												nlapiLogExecution('Debug', 'Exception in updating inventory reference number', exp);
											}
										}
									}
								}
								//End of code specific to GSUSA

								response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);					
								nlapiLogExecution('Debug', 'Done customrecord', 'Success');
							}
							else {
								SOarray["custparam_beginlocationname"]=NextShowLocation;
								SOarray["custparam_beginLocationname"] = NextShowLocation;
								SOarray["custparam_nextlocation"]=NextShowLocation;	
								SOarray["custparam_nextiteminternalid"]=NextShowItemId;	
								nlapiLogExecution('Debug', 'NextShowLocation : ', NextShowLocation);
								nlapiLogExecution('Debug', 'Error: ', 'Scanned a wrong location');
								nlapiLogExecution('Debug', 'NextShowItemId : ', NextShowItemId);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

							}
						}
						else if (getEnteredLocation != '' && getEnteredLocation.toUpperCase() != getBeginLocationName.toUpperCase()) {
							nlapiLogExecution('Debug', 'Different Location scanned');
							var vlocationfound='F';
							if (SOSearchResults != null && SOSearchResults.length > 0) {
								for (var l = 0; l < SOSearchResults.length; l++) {

									var actBeginLocation = SOSearchResults[l].getText('custrecord_actbeginloc');

									if((getEnteredLocation.toUpperCase()==actBeginLocation.toUpperCase()) && (vlocationfound=='F')){

										vlocationfound='T';

										nlapiLogExecution('Debug', 'vlocationfound', vlocationfound);

										var pikgenresults=SOSearchResults[l];

										SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
										SOarray["custparam_recordinternalid"] = pikgenresults.getId();
										//SOarray["custparam_containerlpno"] = pikgenresults.getValue('custrecord_lpno');
										SOarray["custparam_containerlpno"] = pikgenresults.getValue('custrecord_container_lp_no');
										SOarray["custparam_expectedquantity"] = pikgenresults.getValue('custrecord_expe_qty');
										SOarray["custparam_beginLocation"] = pikgenresults.getValue('custrecord_actbeginloc');
										SOarray["custparam_item"] = pikgenresults.getValue('custrecord_sku');
										SOarray["custparam_itemname"] = pikgenresults.getText('custrecord_sku');
										SOarray["custparam_itemdescription"] = pikgenresults.getValue('custrecord_skudesc');
										SOarray["custparam_iteminternalid"] = pikgenresults.getValue('custrecord_ebiz_sku_no');
										SOarray["custparam_dolineid"] = pikgenresults.getValue('custrecord_ebiz_cntrl_no');
										SOarray["custparam_invoicerefno"] = pikgenresults.getValue('custrecord_invref_no');
										SOarray["custparam_orderlineno"] = pikgenresults.getValue('custrecord_line_no');
										SOarray["custparam_beginLocationname"] = pikgenresults.getText('custrecord_actbeginloc');
										SOarray["custparam_beginlocationname"] = pikgenresults.getText('custrecord_actbeginloc');
										SOarray["custparam_batchno"] = pikgenresults.getValue('custrecord_batch_no');
										SOarray["custparam_whlocation"] = pikgenresults.getValue('custrecord_wms_location');
										SOarray["custparam_whcompany"] = pikgenresults.getValue('custrecord_comp_id');
										SOarray["custparam_noofrecords"] = SOSearchResults.length;		
										SOarray["name"] =  pikgenresults.getValue('name');
										SOarray["custparam_containersize"] =  pikgenresults.getValue('custrecord_container');
										SOarray["custparam_ebizordno"] =  pikgenresults.getValue('custrecord_ebiz_order_no');
										getBeginLocationName = pikgenresults.getText('custrecord_actbeginloc');

										var vNextRecs=0;

										nlapiLogExecution('Debug', 'Loop # : ', l+1);
										nlapiLogExecution('Debug', 'SOSearchResults Length : ', SOSearchResults.length);

										if(l+1 < SOSearchResults.length)
											vNextRecs=l+1;
										else 
											vNextRecs=0;

										nlapiLogExecution('Debug', 'vNextRecs : ', vNextRecs);

										if(SOSearchResults[vNextRecs].getText('custrecord_actbeginloc') != null && SOSearchResults[vNextRecs].getText('custrecord_actbeginloc') !='')
											SOarray["custparam_nextlocation"] = SOSearchResults[vNextRecs].getText('custrecord_actbeginloc');
										else
											SOarray["custparam_nextlocation"]='';

										if(SOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no') != null && SOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no') != '')	
											SOarray["custparam_nextiteminternalid"] = SOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no');
										else
											SOarray["custparam_nextiteminternalid"]='';

										if(SOSearchResults[vNextRecs].getValue('custrecord_expe_qty') != null && SOSearchResults[vNextRecs].getValue('custrecord_expe_qty') != '')
											SOarray["custparam_nextexpectedquantity"] = SOSearchResults[vNextRecs].getValue('custrecord_expe_qty');
										else
											SOarray["custparam_nextexpectedquantity"]=0;

										nlapiLogExecution('Debug', 'Next Location', SOarray["custparam_nextlocation"]);
										nlapiLogExecution('Debug', 'Next Item Intr Id', SOarray["custparam_nextiteminternalid"]);


										var binFilters = new Array();
										var binColumns = new Array();

										binFilters.push(new nlobjSearchFilter('name', null, 'is', getEnteredLocation));
										binFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

										var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, binFilters,binColumns);

										if (LocationSearch.length != 0) {
											for (var s = 0; s < LocationSearch.length; s++) {
												var getEndLocationInternalId = LocationSearch[s].getId();
												nlapiLogExecution('Debug', 'End Location Id', getEndLocationInternalId);
											}

											SOarray["custparam_beginlocationname"] = getBeginLocationName;
											SOarray["custparam_beginLocationname"] = getBeginLocationName;
											SOarray["custparam_endlocinternalid"] = getEndLocationInternalId;

											//To Remove case sensitive							 
											SOarray["custparam_endlocation"] = actBeginLocation;
											SOarray["custparam_clusterno"] = vClusterNo;

											//Added code specific to GSUSA.
											//Instead of checking weather the item has qty in pick face location and prompting user
											//to confirm replenishment at the end of the transaction .We are checking here only.
											var invtqty=0;

											var IsPickFaceLoc = 'F';
											var ItemInternalId=SOarray["custparam_item"];
											var EndLocInternalId=getEndLocationInternalId;
											var ExpectedQuantity=SOarray["custparam_expectedquantity"];
											var InvoiceRefNo=SOarray["custparam_invoicerefno"];
											var RecordInternalId=SOarray["custparam_recordinternalid"];
											var WaveNo=SOarray["custparam_waveno"];
											var ClusNo=SOarray["custparam_clusterno"];
											var OrdName=SOarray["name"];
											var ebizOrdNo=SOarray["custparam_ebizordno"];
											var getZoneNo=SOarray["custparam_ebizzoneno"];
											var whLocation=SOarray["custparam_whlocation"];
											IsPickFaceLoc = isPickFaceLocation(ItemInternalId,EndLocInternalId);

											nlapiLogExecution('Debug', 'ItemInternalId', ItemInternalId);
											nlapiLogExecution('Debug', 'EndLocInternalId', EndLocInternalId);
											nlapiLogExecution('Debug', 'ExpectedQuantity', ExpectedQuantity);
											nlapiLogExecution('Debug', 'IsPickFaceLoc', IsPickFaceLoc);
											nlapiLogExecution('Debug', 'InvoiceRefNo', InvoiceRefNo);
											nlapiLogExecution('Debug', 'RecordInternalId', RecordInternalId);
											nlapiLogExecution('Debug', 'WaveNo', WaveNo);
											nlapiLogExecution('Debug', 'ClusNo', ClusNo);
											nlapiLogExecution('Debug', 'OrdName', OrdName);
											nlapiLogExecution('Debug', 'ebizOrdNo', ebizOrdNo);
											nlapiLogExecution('Debug', 'whLocation', whLocation);


											if(IsPickFaceLoc=='T')
											{
												var openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemInternalId);

												if(openinventory!=null && openinventory!='')
												{
													nlapiLogExecution('Debug', 'Pick Face Inventory Length', openinventory.length);

													for(var x=0; x< openinventory.length;x++)
													{
														var vinvtqty=openinventory[x].getValue('custrecord_ebiz_qoh');

														if(vinvtqty==null || vinvtqty=='' || isNaN(vinvtqty))
														{
															vinvtqty=0;
														}

														invtqty=parseInt(invtqty)+parseInt(vinvtqty);
													}						
												}
											}

											if(invtqty==null || invtqty=='' || isNaN(invtqty))
											{
												invtqty=0;
											}

											nlapiLogExecution('Debug', 'invtqty', invtqty);

											if(IsPickFaceLoc=='T' && (parseInt(ExpectedQuantity)>parseInt(invtqty)))
											{
												var openreplns = new Array();
												openreplns=getOpenReplns(ItemInternalId,EndLocInternalId);

												if(openreplns!=null && openreplns!='')
												{
													for(var i=0; i< openreplns.length;i++)
													{
														var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
														var expqty=openreplns[i].getValue('custrecord_expe_qty');
														if(expqty == null || expqty == '')
														{
															expqty=0;
														}	
														nlapiLogExecution('Debug', 'open repln qty',expqty);
														nlapiLogExecution('Debug', 'getQuantity',ExpectedQuantity);

														var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
														if(skiptask==null || skiptask=='')
														{
															skiptask=1;
														}
														else
														{
															skiptask=parseInt(skiptask)+1;
														}

														nlapiLogExecution('Debug', 'skiptask',skiptask);

														nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

														var vPickType = request.getParameter('hdnpicktype');
														nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');

													}
													var SOarray=new Array();
													SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);
													SOarray["custparam_skipid"] = vSkipId;
													if(NextShowLocation!=null && NextShowLocation!='')
														SOarray["custparam_screenno"] = '11EXP';
													else
														SOarray["custparam_screenno"] = '11LOCEXP';	
													SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
													response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
													return;

												}
												else
												{
													var replengen = generateReplenishment(EndLocInternalId,ItemInternalId,whLocation,WaveNo);
													nlapiLogExecution('Debug', 'replengen',replengen);
													//If repeln generation failed, redirect to item so that user will perform short pick.
													if(replengen!=true)
													{
														nlapiLogExecution('ERROR', 'Replen Generation Failed.Insufficient inventory in Bulk location');
														nlapiLogExecution('ERROR', 'Redirecting to Item scan...');
														response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);					
														return;
													}

													var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
													if(skiptask==null || skiptask=='')
													{
														skiptask=1;
													}
													else
													{
														skiptask=parseInt(skiptask)+1;
													}

													nlapiLogExecution('Debug', 'skiptask',skiptask);

													nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

													var vPickType = request.getParameter('hdnpicktype');
													var SOarray=new Array();

													SOarray=buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,getZoneNo,vPickType);

													SOarray["custparam_skipid"] = vSkipId;
													if(NextShowLocation!=null && NextShowLocation!='')
														SOarray["custparam_screenno"] = '11EXP';
													else
														SOarray["custparam_screenno"] = '11LOCEXP';	

													if(replengen!=true)
														SOarray["custparam_error"]='Replen Generation Failed.Insufficient inventory in Bulk location';
													else 
														SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';

													response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
													return;
												}

											}				
											else
											{
												if(IsPickFaceLoc=='T' && (InvoiceRefNo==null || InvoiceRefNo==''))
												{
													if(openinventory!=null && openinventory!='')
													{
														var openinvtrecid = openinventory[0].getId();
														nlapiLogExecution('Debug', 'openinvtrecid', openinvtrecid);

														try
														{
															nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_invref_no', openinvtrecid);
														}
														catch(exp)
														{
															nlapiLogExecution('Debug', 'Exception in updating inventory reference number', exp);
														}
													}
												}
											}
											//End of code specific to GSUSA
											response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);					
										}						
									}
									//The below code is commented by Satish.N on 08/07/2012
									else if(vlocationfound=='F')
									{
										nlapiLogExecution('Debug', 'Into else vlocationfound', vlocationfound);
										//Upto here.

										var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',SOSearchResults[l].getId(),'custrecord_skiptask');
										if(skiptask==null || skiptask=='')
										{
											skiptask=1;
										}
										else
										{
											skiptask=parseInt(skiptask)+1;
										}

										nlapiLogExecution('Debug', 'skiptask',skiptask);

										nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[l].getId(), 'custrecord_skiptask', skiptask);

									}							
								}
								if(vlocationfound=='F'){
									SOarray["custparam_beginlocationname"]=NextShowLocation;
									SOarray["custparam_beginLocationname"]=NextShowLocation;
									SOarray["custparam_nextlocation"]=NextShowLocation;	
									SOarray["custparam_nextiteminternalid"]=NextShowItemId;

									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								}	
							}
						}
						else 
						{
							SOarray["custparam_beginlocationname"]=NextShowLocation;
							SOarray["custparam_beginLocationname"]=NextShowLocation;
							SOarray["custparam_nextlocation"]=NextShowLocation;	
							SOarray["custparam_nextiteminternalid"]=NextShowItemId;
							nlapiLogExecution('Debug', 'NextShowLocation : ', NextShowLocation);
							nlapiLogExecution('Debug', 'Error: ', 'Did not scan the location');
							nlapiLogExecution('Debug', 'NextShowItemId : ', NextShowItemId);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);				
						}
					}
				}
				else
				{

					nlapiLogExecution('Debug', 'Into ON-DEMAND Stage');
					nlapiLogExecution('Debug', 'OrdNo',OrdNo);
					nlapiLogExecution('Debug', 'vZoneId',vZoneId);
					nlapiLogExecution('Debug', 'vPickType',vPickType);

					//On Demand Stage
					SOarray["custparam_waveno"] = WaveNo;
					SOarray["custparam_recordinternalid"] = getRecordInternalId;
					SOarray["custparam_newcontainerlp"] = containerlp;
					SOarray["custparam_expectedquantity"] = getExpectedQuantity;
					SOarray["custparam_beginLocation"] = getBeginLocation;
					SOarray["custparam_item"] = getItem;
					SOarray["custparam_itemname"] = getItemName;
					SOarray["custparam_itemdescription"] = getItemDescription;
					SOarray["custparam_iteminternalid"] = getItemInternalId;
					SOarray["custparam_dolineid"] = fointrid;
					SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
					SOarray["custparam_orderlineno"] = ordline;
					SOarray["custparam_beginLocationname"] = getBeginLocation;
					SOarray["custparam_beginlocationname"] = getBeginLocation;
					SOarray["custparam_batchno"] = vBatchNo;
					SOarray["custparam_whlocation"] = whLocation;
					SOarray["custparam_whcompany"] = whCompany;
					SOarray["custparam_noofrecords"] = RecordCount;		
					SOarray["name"] =  OrdNo;
					SOarray["custparam_containersize"] =  ContainerSize;
					SOarray["custparam_ebizordno"] =  vSOId;
					SOarray["custparam_ebizzoneno"] =  vZoneId;
					SOarray["custparam_clusterno"] =  ClusNo;

					var SOFilters = new Array();

					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

					if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
					{
						nlapiLogExecution('Debug', 'WaveNo inside If', ClusNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(WaveNo)));
					}

					if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
					{
						nlapiLogExecution('Debug', 'ClusNo inside If', ClusNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
					}

					if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdNo!=null && OrdNo!="" && OrdNo!= "null")
					{
						nlapiLogExecution('Debug', 'OrdNo inside If', OrdNo);
						SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
					}

					if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
					{
						nlapiLogExecution('Debug', 'SO Id inside If', vSOId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
					}

					if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!="null")
					{
						nlapiLogExecution('Debug', 'Zone # inside If', vZoneId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
					}

					var SOColumns = new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
					SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
					//Code end as on 290414
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('Debug', 'Results', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0) {

						for (var s = 0; s < SOSearchResults.length; s++) {

							nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[s].getId(),'custrecord_taskassignedto', '');					
						}
					}

					SOarray["custparam_ondemandstage"] = 'Y';
					nlapiLogExecution('Debug', 'Navigating To', 'Stage Location');
					response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);

				}
			}

			catch (e) 
			{
				nlapiLogExecution('Debug', 'Exception: ', e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} 
			finally 
			{					
				context.setSessionObject('session', null);
			}
		}
		else
		{
			SOarray["custparam_screenno"] = '14';
			SOarray["custparam_error"] = 'INVALID LOCATION';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}



function buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,vZoneId,vPickType)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	var SOColumns = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(WaveNo)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('Debug', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('Debug', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('Debug', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('Debug', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
	SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
	//Code end as on 290414
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	SOarray["custparam_ebizordno"] = ebizOrdNo;

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('Debug', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('Debug', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('Debug', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('Debug', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_picktype"] = vPickType;
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}
