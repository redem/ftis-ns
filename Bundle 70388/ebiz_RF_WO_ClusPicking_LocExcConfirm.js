/***************************************************************************
  eBizNET Solutions Inc              
 ***************************************************************************
 **        $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_ClusPicking_LocExcConfirm.js,v $
 *     	   $Revision: 1.1.2.1.2.1 $
 *     	   $Date: 2015/11/10 14:08:15 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_ClusPicking_LocExcConfirm.js,v $
 * Revision 1.1.2.1.2.1  2015/11/10 14:08:15  skreddy
 * case 201415524
 * 2015.2  issue fix
 *
 * Revision 1.1.2.1  2015/02/13 14:34:37  sponnaganti
 * Jawbone Cluster Picking CR Changes
 * CR#  CR14US0323
 *
 * 
 *****************************************************************************/
function WO_PickingLocExpConfirm(request, response){
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	
	if (request.getMethod() == 'GET') {

		var fastpick = request.getParameter('custparam_fastpick');

		nlapiLogExecution('ERROR', 'fastpick', fastpick);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10;

		if( getLanguage == 'es_ES')
		{
			st1 = "RECOGIDA CANT : ";
			st2 = "UBICACI&#211;N : ";
			st3 = "TEMA : ";
			st4 = "SUSPEND";
			st5 = "CONF";
			st6 = "ANTERIOR";

		}
		else
		{
			st1 = "PICK QTY : ";
			st2 = "LOCATION : ";
			st3 = "ITEM : ";
			st4 = "SUSPEND";
			st5 = "CONF";
			st6 = "PREV";
		}

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
		/*var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);*/
		var vBatchno = request.getParameter('custparam_batchno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var pickType=request.getParameter('custparam_picktype');
		var whLocation = request.getParameter('custparam_whlocation');
		var vEntLocText = request.getParameter('custparam_entloc');
		var vEntInvRecId = request.getParameter('custparam_recid');
		var vEntLocId = request.getParameter('custparam_entlocid');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var NextExptdQty = request.getParameter('custparam_nextexpectedquantity');
		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');
		var vEntLotId = request.getParameter('custparam_entLotId');
		var detailTask=request.getParameter('custparam_detailtask');
		// Case# 20127440 starts
		var itemstatus = request.getParameter('custparam_itemstatus');
		nlapiLogExecution('DEBUG', 'itemstatus is ', itemstatus);
		var getFetchContainerLpNo = request.getParameter('custparam_fetchcontainerlpno');
		// Case# 20127440 end
		
		var woRecordCount=request.getParameter('custparam_nooflocrecords');
		var venterzone=request.getParameter('custparam_venterzone');
		nlapiLogExecution('DEBUG', 'getEbizSO', getOrderNo);
		nlapiLogExecution('ERROR', 'getBeginLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);
		nlapiLogExecution('DEBUG', 'vEntLocText', vEntLocText);
		nlapiLogExecution('DEBUG', 'vEntInvRecId', vEntInvRecId);
		nlapiLogExecution('DEBUG', 'vEntLocId', vEntLocId);
		nlapiLogExecution('DEBUG', 'NextExptdQty', NextExptdQty);
		nlapiLogExecution('DEBUG', 'vEntLotId', vEntLotId);
		nlapiLogExecution('DEBUG', 'getFetchContainerLpNo', getFetchContainerLpNo);
		nlapiLogExecution('DEBUG', 'venterzone', venterzone);
		nlapiLogExecution('DEBUG', 'woRecordCount', woRecordCount);

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		if(getBeginBinLocation==null)
		{
			getBeginBinLocation=NextLocation;
		}
		if(getItemInternalId==null)
		{
			getItemInternalId=NextItemInternalId;
		}

		//removed as on 051113 by suman to increase the performance.
		/*var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
	 	var getItem=ItemRec.getFieldValue('itemid');
		var Itemdescription='';
		nlapiLogExecution('ERROR', 'Location Name is', getItem);
		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('description');
		}	*/
		var filter=new Array();
		if(getItemInternalId!=null&&getItemInternalId!="")
		filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

		var column=new Array();
		column[0]=new nlobjSearchColumn("itemid");
		column[1]=new nlobjSearchColumn("description");
		var Itemdescription='';
		var getItem="";
		var searchres=nlapiSearchRecord("item",null,filter,column);
		if(searchres!=null&&searchres!="")
		{
			getItem=searchres[0].getValue("itemid");
			Itemdescription=searchres[0].getValue("description");
		}
		//end of code as on 051113

		if(Itemdescription !=null && Itemdescription !='')
			Itemdescription = Itemdescription.substring(0, 20);	

		nlapiLogExecution('DEBUG', 'getItemdescription', Itemdescription);
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		//html = html + " document.getElementById('cmdConfirm').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdConfirm').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";



		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getExpectedQuantity + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value='" + getItem + "'>";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnEntLocText' value=" + vEntLocText + ">";
		html = html + "				<input type='hidden' name='hdnEntInvRec' value=" + vEntInvRecId + ">";
		html = html + "				<input type='hidden' name='hdnEntLocId' value=" + vEntLocId + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnZoneNo' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnNextExptdQty' value=" + NextExptdQty + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";	
		html = html + "				<input type='hidden' name='hdnEntLotId' value=" + vEntLotId + ">";
		html = html + "				<input type='hidden' name='hdndetailtask' value=" + detailTask + ">";
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";
		// Case# 20127440 starts
		html = html + "				<input type='hidden' name='hdnitemstatus' value=" + itemstatus + ">";
		html = html + "				<input type='hidden' name='hdnFetchContainerLpNo' value=" + getFetchContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnenterzone' value=" + venterzone + ">";
		html = html + "				<input type='hidden' name='hdnlocreccount' value=" + woRecordCount + ">";
		// Case# 20127440 end
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + vEntLocText + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>" + getItem + "</label><br>ITEM DESCRIPTION: <label>" + Itemdescription + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		
		// case no 20125545
		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st4 +" <input name='cmdSuspend' type='submit' value='F10'/></td></tr>";		*/ 
		html = html + "				<tr><td align = 'left'>"+ st5 +"  <input name='cmdConfirm' id='cmdConfirm' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "				"+ st6 +" <input name='cmdPrevious' type='submit' value='F7'/></td></tr>";
		/*html = html + "				<tr><td align = 'left'>CONF AND STAGE <input name='cmdSuspend' type='submit' value='F11'/>";*/
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdConfirm').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		var getLanguage = request.getParameter('hdngetLanguage');
		nlapiLogExecution('DEBUG', 'Into Response request.getParameter(cmdPickException)', request.getParameter('cmdPickException'));
		var ItemType=request.getParameter('hdnitemtype');

		var fastpick = request.getParameter('hdnfastpick');
		var getFetchContainerLpNo = request.getParameter('hdnFetchContainerLpNo');
		var venterzone = request.getParameter('hdnenterzone');

		nlapiLogExecution('ERROR', 'getFetchContainerLpNo', getFetchContainerLpNo);
		nlapiLogExecution('ERROR', 'fastpick', fastpick);
		// This variable is to hold the SO# entered.
		var Picktype = request.getParameter('hdnpicktype');		
		nlapiLogExecution('DEBUG', 'Picktype', Picktype);
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_fastpick"] = fastpick;
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		//SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnFetchContainerLpNo');
		nlapiLogExecution('DEBUG', 'SOarray["custparam_waveno"]', SOarray["custparam_waveno"]);
		nlapiLogExecution('DEBUG', 'SOarray["custparam_recordinternalid"]', SOarray["custparam_recordinternalid"]);
		nlapiLogExecution('DEBUG', 'SOarray["custparam_containerlpno"]', SOarray["custparam_containerlpno"]);

		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');

		nlapiLogExecution('DEBUG', 'SOarray["custparam_item"]', SOarray["custparam_item"]);

		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');		
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_ebizzoneno"] = request.getParameter('hdnZoneNo');
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnNextExptdQty');
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		SOarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');
		SOarray["custparam_detailtask"] = request.getParameter('hdndetailtask');
		SOarray["custparam_venterzone"]=request.getParameter('hdnenterzone');
		SOarray["custparam_nooflocrecords"] = request.getParameter('hdnlocreccount');
		// Case# 20127440 starts
		SOarray["custparam_itemstatus"] = request.getParameter('hdnitemstatus');
		// Case# 20127440 end
		nlapiLogExecution('DEBUG', 'SOarray["custparam_nooflocrecords"]', SOarray["custparam_nooflocrecords"]);
		var vZoneId = request.getParameter('hdnZoneNo');;

		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		
		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_pick_loc_excep', 'customdeploy_ebiz_rf_wo_pick_loc_exp_di', false, SOarray);
		}
		else 
		{


			if (request.getParameter('hdnflag') == 'ENT') {
				nlapiLogExecution('DEBUG', 'Confirm Pick');

				var RecordInternalId = request.getParameter('hdnRecordInternalId');
				var ContainerLPNo = request.getParameter('hdnContainerLpNo');
				var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
				var BeginLocation = request.getParameter('hdnBeginLocationId');
				var Item = request.getParameter('hdnItem');
				var ItemDescription = request.getParameter('hdnItemDescription');
				var ItemInternalId = request.getParameter('hdnItemInternalId');
				var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
				var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
				var BeginLocationName = request.getParameter('hdnBeginBinLocation');
				var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
				var EndLocation = request.getParameter('hdnEnteredLocation');
				var OrderLineNo = request.getParameter('hdnOrderLineNo');
				var NextShowLocation=request.getParameter('hdnNextLocation');

				var ContainerSize=request.getParameter('hdnContainerSize');
				var NextItemInternalId=request.getParameter('hdnNextItemId');
				var WaveNo = request.getParameter('hdnWaveNo');
				var ClusNo = request.getParameter('hdnClusterNo');
				var ebizOrdNo=request.getParameter('hdnebizOrdNo');
				var OrdName=request.getParameter('hdnName');
				var NextExptdQty=request.getParameter('hdnNextExptdQty');

				var EntLocText = request.getParameter('hdnEntLocText');
				var EntInvRec=request.getParameter('hdnEntInvRec');
				var EntLocId=request.getParameter('hdnEntLocId');
				var EntLotId=request.getParameter('hdnEntLotId');
				SOarray["custparam_Newendlocid"] = EntLocId;
				SOarray["custparam_Newendloc"] = EntLocText;
				SOarray["custparam_NewInvRecId"] = EntInvRec;
				SOarray["locExc"] = 'Y';

				nlapiLogExecution('DEBUG', 'OrdName', OrdName);
				nlapiLogExecution('DEBUG', 'Next Location', NextShowLocation);
				nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);

				nlapiLogExecution('DEBUG', 'EntLocText', EntLocText);
				nlapiLogExecution('DEBUG', 'EntInvRec', EntInvRec);
				nlapiLogExecution('DEBUG', 'EntLocId', EntLocId);
				nlapiLogExecution('DEBUG', 'EntLotId', EntLotId);


				//ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo)
				var vPickType = request.getParameter('hdnpicktype');

				//nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
				//Changed the code for auto containerization
				var vAutoContainerFlag='N'; // Set 'Y' to skip RF dialog for scanning container LP
				SOarray["custparam_autocont"] = vAutoContainerFlag;
				if(vAutoContainerFlag=='Y')
				{
					var RcId=RecordInternalId;
					var EndLocation = EndLocInternalId;
					var TotalWeight=0;
					var getReason=request.getParameter('custparam_enteredReason');
					var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
					var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
					getLPContainerSize= SORec.getFieldText('custrecord_container');

					SOarray["custparam_newcontainerlp"] = getContainerLPNo;
					var vPickType = request.getParameter('hdnpicktype');
					var vBatchno = request.getParameter('hdnbatchno');
					var PickQty = ExpectedQuantity;
					var vActqty = ExpectedQuantity;
					var SalesOrderInternalId = ebizOrdNo;
					var getContainerSize = getLPContainerSize;
					var  vdono = DoLineId;
					nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
					nlapiLogExecution('DEBUG', 'getContainerLPNo', getContainerLPNo);
					var opentaskcount=0;
					nlapiLogExecution('DEBUG', 'open task RecordInternalId', RecordInternalId);
					nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
					nlapiLogExecution('DEBUG', 'getContainerLPNo', getContainerLPNo);
					nlapiLogExecution('DEBUG', 'vPickType', vPickType);
					nlapiLogExecution('DEBUG', 'SalesOrderInternalId', SalesOrderInternalId);
					nlapiLogExecution('DEBUG', 'vdono', vdono);
					opentaskcount=getOpenTasksCount(WaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,vdono);
					nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
					//For fine tuning pick confirmation and doing autopacking in user event after last item
					var IsitLastPick='F';
					if(opentaskcount > parseFloat(vSkipId) + 1)
						IsitLastPick='F';
					else
						IsitLastPick='T';

					nlapiLogExecution('DEBUG', 'getItem', ItemInternalId);

					//ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,vBatchno,IsitLastPick);
					ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EntLocId,getReason,PickQty,vBatchno,IsitLastPick,EntInvRec);
					//if(RecCount > 1)
					//if(opentaskcount > parseFloat(vSkipId) + 1)
					if(opentaskcount >  1)
					{
						var SOFilters = new Array();
						SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
						nlapiLogExecution('DEBUG', 'vPickType', vPickType);

						if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
						{
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
						}
						if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
						{
							nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
						}
						if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
						{
							nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
							SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
						}
						if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
						{
							nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
						}
						if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
						{
							nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
							SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
						}

						var SOColumns = new Array();

						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//						SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
						//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
						SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
						//Code end as on 290414
						SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
						SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
						SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
						SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
						SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
						SOColumns.push(new nlobjSearchColumn('name'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
						SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

						//SOColumns[0].setSort();
						SOColumns[3].setSort();
						SOColumns[4].setSort();
						/*SOColumns[3].setSort();
						SOColumns[4].setSort();
						SOColumns[5].setSort(true);*/

						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
						nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
						if (SOSearchResults != null && SOSearchResults.length > 0) 
						{
							if(SOSearchResults.length <= parseFloat(vSkipId))
							{
								vSkipId=0;
							}
							var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
							if(SOSearchResults.length > parseFloat(vSkipId) + 1)
							{
								var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
								SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
								SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
								nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
							}
							SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
							//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
							SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
							SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
							SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
							SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
							SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
							SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
							SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
							SOarray["custparam_noofrecords"] = SOSearchResults.length;		
							SOarray["name"] =  SOSearchResult.getValue('name');
							SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
							SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
							if(vZoneId!=null && vZoneId!="")
							{
								SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
							}
							else
								SOarray["custparam_ebizzoneno"] = '';

						}
						if(BeginLocation != NextShowLocation)
						{ 
							response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
							nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
						}
						else if(ItemInternalId != NextItemInternalId)
						{ 
							response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
							nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
						}
						else if(ItemInternalId == NextItemInternalId)
						{ 
							response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								
							nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
						}
					}
					else{

						nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
						//For fine tuning pick confirmation and doing autopacking in user event after last item
						//AutoPacking(SalesOrderInternalId);
						/*nlapiLogExecution('DEBUG', 'Navigating To1', 'Foot print');
							response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, SOarray);*/

						//nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
						//For fine tuning pick confirmation and doing autopacking in user event after last item
						//AutoPacking(SalesOrderInternalId);
						nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
						response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);					
					}

				}	
				else
				{
					nlapiLogExecution('DEBUG','Chkpt','ELSE');
					SOarray["custparam_EntLoc"] = EntLocText;
					SOarray["custparam_EntLocRec"] = EntInvRec;
					SOarray["custparam_EntLocId"] = EntLocId;
					SOarray["custparam_Exc"] = 'L';
					SOarray["custparam_entLotId"] = EntLotId; 


					var SOFilters = new Array();
					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
					nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
					nlapiLogExecution('DEBUG', 'ClusNo', ClusNo);
					nlapiLogExecution('DEBUG', 'ClusNo', ClusNo);
					nlapiLogExecution('DEBUG', 'ebizOrdNo', ebizOrdNo);
					nlapiLogExecution('DEBUG', 'vZoneId', vZoneId);
					nlapiLogExecution('DEBUG', 'vdono', vdono);
					nlapiLogExecution('DEBUG', 'getFetchContainerLpNo', getFetchContainerLpNo);

					var SOFilters = new Array();
					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
					if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
					{
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(WaveNo)));
					}
					if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
					{
						nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
					}
					if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
					{
						nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
						SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
					}
					if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
					{
						nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
					}
					if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
					{
						nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
					}

					if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getFetchContainerLpNo!=null && getFetchContainerLpNo!="" && getFetchContainerLpNo!= "null")
					{
						nlapiLogExecution('DEBUG', 'Carton # inside If', getFetchContainerLpNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchContainerLpNo));			
					}
					
					if(venterzone!=null && venterzone!="" && venterzone!='null')
					{
						nlapiLogExecution('DEBUG', 'getZoneNo inside if', venterzone);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', venterzone));
					
					}
					var SOColumns=new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
					//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
					SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
					//Code end as on 290414
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));//Case# 20149958

				//	SOColumns[0].setSort();
				//	SOColumns[1].setSort();
					//SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
				//	SOColumns[5].setSort(true);

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0) 
					{
						vSkipId=0;

								nlapiLogExecution('DEBUG', 'SOSearchResults length', SOSearchResults.length);
								nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);
								if(SOSearchResults.length <= parseInt(vSkipId))
								{
									vSkipId=0;
								}
								var SOSearchResult = SOSearchResults[parseInt(vSkipId)];							
								SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
								if(fastpick!='Y')
								{
									SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
									SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
								}
								//Case# 20149958 starts
								//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno'); 
								SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no'); 
								//Case# 20149958 ends
								
								nlapiLogExecution('DEBUG', 'parseFloat(ExpectedQuantity)', parseFloat(ExpectedQuantity));
								nlapiLogExecution('DEBUG', 'parseFloat(SOSearchResult.getValue(custrecord_expe_qty))', parseFloat(SOSearchResult.getValue('custrecord_expe_qty')));
								// case no 20126817� 
// system should display exceptioned qty in next screen
								if(parseFloat(ExpectedQuantity) == parseFloat(SOSearchResult.getValue('custrecord_expe_qty')))
									SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
									// case no 20126817� end

								SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
								SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
								SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
								SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
								SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
								SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
								SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
								SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
								SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
								//SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no'); //Case# 201410761
								SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
								SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
								SOarray["custparam_noofrecords"] = SOSearchResults.length;		
								SOarray["name"] =  SOSearchResult.getValue('name');
								SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
								SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
								SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
								SOarray["custparam_fastpick"] = fastpick;

						if(vZoneId!=null && vZoneId!="")
						{
							SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
						}
						else
							SOarray["custparam_ebizzoneno"] = '';

						if(SOSearchResults.length > parseInt(vSkipId) + 1)
						{
							var SOSearchnextResult = SOSearchResults[parseInt(vSkipId) + 1];
							SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
							SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
							nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
							nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
							nlapiLogExecution('DEBUG', 'Next Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));	
						}
						else
						{
							var SOSearchnextResult = SOSearchResults[0];
							SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
							SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
							nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
							nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
						}

					}


					if(ItemInternalId !=null && ItemInternalId !='')
					{
						var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

						if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') {
							nlapiLogExecution('DEBUG','Serialized Item','Done');
							SOarray["custparam_number"] = 0;
							SOarray["custparam_RecType"] = itemSubtype.recordType;
							SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
							SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
							SOarray["custparam_Actbatchno"]='';
							SOarray["custparam_Expbatchno"] = '';

							SOarray["custparam_fastpick"] = fastpick;
							response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
							return;
						}

					}

					if(Picktype =='CL')
					{
						SOarray["custparam_beginLocation"]=request.getParameter('hdnBeginLocationId');
						SOarray["custparam_expectedquantity"]=request.getParameter('custparam_expectedquantity');
						//SOarray["custparam_beginLocation"]="";
						response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_dettask', 'customdeploy_rf_wocluspicking_dettask', false, SOarray);
					}
					else
					{

						SOarray["custparam_EntLoc"] = EntLocText;
						SOarray["custparam_EntLocRec"] = EntInvRec;
						SOarray["custparam_EntLocId"] = EntLocId;
						SOarray["custparam_Exc"] = 'L';
						SOarray["custparam_fastpick"] = fastpick;

						if (fastpick!='Y')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
						}					

						return;
					}
					// response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
				}	

			}
			else if (request.getParameter('cmdConfirm') == 'F11') {
				nlapiLogExecution('DEBUG', 'Confirm and stage location');
				var WaveNo = request.getParameter('hdnWaveNo');
				var RecordInternalId = request.getParameter('hdnRecordInternalId');
				var ContainerLPNo = request.getParameter('hdnContainerLpNo');
				var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
				var BeginLocation = request.getParameter('hdnBeginLocationId');
				var Item = request.getParameter('hdnItem');
				var ItemDescription = request.getParameter('hdnItemDescription');
				var ItemInternalId = request.getParameter('hdnItemInternalId');
				var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
				var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
				var BeginLocationName = request.getParameter('hdnBeginBinLocation');
				var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
				var EndLocation = request.getParameter('hdnEnteredLocation');
				var OrderLineNo = request.getParameter('hdnOrderLineNo');
				var NextShowLocation=request.getParameter('hdnNextLocation');
				var OrdName=request.getParameter('hdnName');
				var ContainerSize=request.getParameter('hdnContainerSize');
				var ebizOrdNo=request.getParameter('hdnebizOrdNo');
				var NextItemInternalId=request.getParameter('hdnNextItemId');
				nlapiLogExecution('DEBUG', 'OrdName', OrdName);
				nlapiLogExecution('DEBUG', 'Next Location', NextShowLocation);
				var vPickType = request.getParameter('hdnpicktype');
				nlapiLogExecution('DEBUG', 'Before Item Fulfillment', 'Before Item Fulfillment');
				//ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo)

				var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

				//if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T') {
				if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') {
					SOarray["custparam_number"] = 0;
					SOarray["custparam_RecType"] = itemSubtype.recordType;
					SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
					SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
					response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
				}
				else
				{		

					//nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
					//Changed the code for auto containerization 
					var RcId=RecordInternalId;
					var EndLocation = EndLocInternalId;
					var TotalWeight=0;
					var getReason=request.getParameter('custparam_enteredReason');
					var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
					var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
					getLPContainerSize= SORec.getFieldText('custrecord_container');

					SOarray["custparam_newcontainerlp"] = getContainerLPNo;

					var vBatchno = request.getParameter('hdnbatchno');
					var PickQty = ExpectedQuantity;
					var vActqty = ExpectedQuantity;
					var SalesOrderInternalId = ebizOrdNo;
					var getContainerSize = getLPContainerSize;
					var  vdono = DoLineId;
					nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
					nlapiLogExecution('DEBUG', 'getContainerLPNo', getContainerLPNo);
					var opentaskcount=0;
					nlapiLogExecution('DEBUG', 'opentaskcount', WaveNo);
					nlapiLogExecution('DEBUG', 'opentaskcount', getContainerLPNo);
					nlapiLogExecution('DEBUG', 'opentaskcount', vPickType);
					nlapiLogExecution('DEBUG', 'opentaskcount', SalesOrderInternalId);
					nlapiLogExecution('DEBUG', 'opentaskcount', vdono);
					nlapiLogExecution('DEBUG', 'OrdName', OrdName);
					nlapiLogExecution('DEBUG', 'vdono', vdono);
					nlapiLogExecution('DEBUG', 'vZoneId', vZoneId); 
					opentaskcount=getOpenTasksCount(WaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,OrdName,vZoneId);

					nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
					//For fine tuning pick confirmation and doing autopacking in user event after last item
					var IsitLastPick='F';
					if(opentaskcount > parseFloat(vSkipId) + 1)
						IsitLastPick='F';
					else
						IsitLastPick='T';

					nlapiLogExecution('DEBUG', 'getItem', ItemInternalId);

					//ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,vBatchno,IsitLastPick,EntInvRec);
					ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EntLocId,getReason,PickQty,vBatchno,IsitLastPick,EntInvRec);
					//if(RecCount > 1)

					nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
					//For fine tuning pick confirmation and doing autopacking in user event after last item
					//AutoPacking(SalesOrderInternalId);
					nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
					response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);						 


				}
			}
			else 
			{
				nlapiLogExecution('DEBUG', 'Clicked on suspend', request.getParameter('cmdSuspend'));
				/*if (request.getParameter('cmdSuspend') == 'F10') {
					//for suspend button click
					nlapiLogExecution('DEBUG', 'Clicked on Location Exception', request.getParameter('cmdQtyException'));
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_loc_exception', 'customdeploy_ebiz_rf_pick_loc_exc_di', false, SOarray);
				}*/
				if (request.getParameter('cmdQtyException') == 'F11') {
					nlapiLogExecution('DEBUG', 'Clicked on Quantity Exception', request.getParameter('cmdQtyException'));
					response.sendRedirect('SUITELET', 'customscript_rf_putaway_qty_exception', 'customdeploy_rf_putaway_qty_exception_di', false, SOarray);
				}
				else
				{
					if (request.getParameter('cmdPickException') == 'F2') {
						nlapiLogExecution('DEBUG', 'Clicked on cmdPickException', request.getParameter('cmdPickException'));
						response.sendRedirect('SUITELET', 'customscript_rf_pickingqtyexception', 'customdeploy_rf_pickingqtyexception_di', false, SOarray);
					}
					if (request.getParameter('cmdPickException') == 'F3') {
						nlapiLogExecution('DEBUG', 'Clicked on cmdLocException', request.getParameter('cmdlocexc'));
						response.sendRedirect('SUITELET', 'customscript_rf_pickingqtyexception', 'customdeploy_rf_pickingqtyexception_di', false, SOarray);
					}
				}

			}
		}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			//
			SOarray["custparam_screenno"] = '12';
			SOarray["custparam_error"] = 'LOCATION ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}

function ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,vBatchno,IsitLastPick,NewRecId)
{
	var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
	var itemCube = 0;
	var itemWeight=0;	

	if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
		//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
		itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
		itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


	} 
	nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
	nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);
	nlapiLogExecution('DEBUG', 'ContainerSize:ContainerSize', getContainerSize);		


	var ContainerCube;					
	var containerInternalId;
	var ContainerSize;
	var vRemaningqty=0;
	nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	

	if(getContainerSize=="" || getContainerSize==null)
	{
		getContainerSize=ContainerSize;
		nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	
	}
	nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	
	var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);					
	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

		ContainerCube =  parseFloat(arrContainerDetails[0]);						
		containerInternalId = arrContainerDetails[3];
		ContainerSize=arrContainerDetails[4];
		TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
	} 
	nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);	
	nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	

	//UpdateOpenTask,fulfillmentorder
	nlapiLogExecution('DEBUG', 'vdono', vdono);	

	UpdateRFOpenTask(RcId,vActqty, containerInternalId,getContainerLPNo,itemCube,itemWeight,EndLocation,getReason,PickQty,IsitLastPick,NewRecId);
	UpdateRFFulfillOrdLine(vdono,vActqty);

	//create new record in opentask,fullfillordline when we have qty exception
	//vRemaningqty = vDisplayedQty-vActqty;
	vRemaningqty = PickQty-vActqty;
	var recordcount=1;//it is iterator in GUI
	nlapiLogExecution('DEBUG', 'vDisplayedQty', PickQty);	
	nlapiLogExecution('DEBUG', 'vActqty', vActqty);	
	if(PickQty != vActqty)
	{
		nlapiLogExecution('DEBUG', 'inexception condition1', 'inexception condition');	
		CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getContainerLPNo,TotalWeight,vBatchno);//have to check for wt and cube calculation
	}
}

function getOpenTasksCount(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdNo,vZoneId)
{
	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	 
	SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		nlapiLogExecution('DEBUG', 'WaveNo inside If', waveno);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}

	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);

		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}

	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdNo!=null && OrdNo!="" && OrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdNo);

		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
	}

	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}

	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!="null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
		//SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
	}

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	return openreccount;

}

function ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo){
	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');

	nlapiLogExecution('DEBUG', "Line Count" + lineCnt);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	/*    
     for (var k = 1; k <= lineCnt; k++)
     {
     var chkVal = request.getLineItemValue('custpage_items', 'custpage_so', k);
     var SONo
     var ItemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
     var itemno = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
     var Lineno = request.getLineItemValue('custpage_items', 'custpage_lineno', k);
     var pickqty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
     var vinvrefno = request.getLineItemValue('custpage_items', 'custpage_invrefno', k);

     var WaveNo = request.getParameter('hdnWaveNo');
     var RecordInternalId = request.getParameter('hdnRecordInternalId');
     var ContainerLPNo = request.getParameter('hdnContainerLpNo');
     var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
     var BeginLocation = request.getParameter('hdnBeginLocationId');
     var Item = request.getParameter('hdnItem');
     var ItemDescription = request.getParameter('hdnItemDescription');
     var ItemInternalId = request.getParameter('hdnItemInternalId');
     var DoLineId = request.getParameter('hdnDOLineId');	//
     var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
     var BeginLocationName = request.getParameter('hdnBeginBinLocation');
     var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
     var EndLocation = getEnteredLocation;

     //        var Recid = request.getLineItemValue('custpage_items', 'custpage_recid', k);
     //        nlapiLogExecution('DEBUG', "Recid " + Recid)
	 */
	//updating allocqty in opentask
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
	OpenTaskTransaction.setFieldValue('custrecord_act_qty', parseFloat(ExpectedQuantity).toFixed(4));
	OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
	OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);

	nlapiLogExecution('DEBUG', 'Done PICK Record Insertion :', 'Success');

	//Updating DO 

	var DOLine = nlapiLoadRecord('customrecord_ebiznet_ordline', DoLineId);
	DOLine.setFieldValue('custrecord_linestatus_flag', '1'); //Statusflag='C'
	DOLine.setFieldValue('custrecord_pickqty', parseFloat(ExpectedQuantity).toFixed(4));
	var SalesOrderInternalId = DOLine.getFieldValue('name');

	try {
		nlapiLogExecution('DEBUG', 'Main Sales Order Internal Id', SalesOrderInternalId);
		var TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');

		var SOLength = TransformRec.getLineItemCount('item');
		nlapiLogExecution('DEBUG', "SO Length", SOLength);

		/*
         for (var j = 0; j < SOLength; j++)
         {
         nlapiLogExecution('DEBUG', "Get Line Value", TransformRec.getLineItemValue('item', 'line', j + 1));
         TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
         }
         TransformRec.setLineItemValue('item', 'itemfulfillment', OrderLineNo, 'T');
         TransformRec.setLineItemValue('item', 'quantity', OrderLineNo, ExpectedQuantity);
         var TransformRecId = nlapiSubmitRecord(TransformRec, true);
		 */
		for (var j = 1; j <= SOLength; j++) {
			nlapiLogExecution('DEBUG', "Get Line Value" + TransformRec.getLineItemValue('item', 'line', j + 1));

			var item_id = TransformRec.getLineItemValue('item', 'item', j);
			var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);

			if (itemLineNo == OrderLineNo) {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
				TransformRec.setCurrentLineItemValue('item', 'quantity', ExpectedQuantity);
				TransformRec.setCurrentLineItemValue('item', 'location', 1);
				TransformRec.commitLineItem('item');
			}
			else {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');
				TransformRec.commitLineItem('item');

			}
			//TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
		}
		//			invtRec.setFieldValue('custrecord_ebiz_inv_loc', '1');//WH Location.

		//			TransformRec.setFieldValue('custbody_ebiz_fulfillmentord',vdono) 
		var TransformRecId = nlapiSubmitRecord(TransformRec, true);
	} 

	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}

	///Updating qty,qoh in inventory
	nlapiLogExecution('DEBUG', 'Invoice Ref No', InvoiceRefNo);
	var InventoryTranaction = nlapiLoadRecord('customrecord_ebiznet_createinv', InvoiceRefNo);

	var InvAllocQty = InventoryTranaction.getFieldValue('custrecord_ebiz_alloc_qty');
	var InvQOH = InventoryTranaction.getFieldValue('custrecord_ebiz_qoh');

	if (isNaN(InvAllocQty)) {
		InvAllocQty = 0;
	}
	InventoryTranaction.setFieldValue('custrecord_ebiz_qoh', (parseFloat(InvQOH) - parseFloat(ExpectedQuantity)).toFixed(4));
	InventoryTranaction.setFieldValue('custrecord_ebiz_alloc_qty', (parseFloat(InvAllocQty) - parseFloat(ExpectedQuantity)).toFixed(4));
	var InventoryTranactionId = nlapiSubmitRecord(InventoryTranaction, false, true);
	nlapiSubmitRecord(OpenTaskTransaction, true);
	nlapiSubmitRecord(DOLine, true);

	//    }

	//    var form = nlapiCreateForm('Pick  Confirmation');
	//    form.addField('custpage_label', 'label', '<h2>Pick Confirmation Successful</h2>');

	//    response.writePage(form);
}
/**
 * 
 * @param RcId
 * @param PickQty
 * @param containerInternalId
 * @param getEnteredContainerNo
 * @param itemCube
 * @param itemWeight
 * @param EndLocation
 */
function UpdateRFOpenTask(RcId, PickQty, containerInternalId, getEnteredContainerNo, itemCube,
		itemWeight, EndLocation,vReason,ActPickQty,IsItLastPick,NewRecId)
{
	nlapiLogExecution('DEBUG', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
	nlapiLogExecution('Error', 'vinvrefno :', vinvrefno);
	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	if(PickQty!= null && PickQty !="")
		transaction.setFieldValue('custrecord_act_qty', parseFloat(PickQty).toFixed(4));
	if(containerInternalId!= null && containerInternalId !="")
		transaction.setFieldValue('custrecord_container', containerInternalId);
	if(EndLocation!=null && EndLocation!="")
		transaction.setFieldValue('custrecord_actendloc', EndLocation);	
	if(itemWeight!=null && itemWeight!="")
		transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
	if(itemCube!=null && itemCube!="")
		transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
	if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
		transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
	if(vReason!=null && vReason!="")
		transaction.setFieldValue('custrecord_notes', vReason);	
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	transaction.setFieldValue('custrecord_taskassignedto',currentUserID);	
	nlapiLogExecution('DEBUG', 'IsItLastPick: ', IsItLastPick);
	transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
	var vemployee = request.getParameter('custpage_employee');
	nlapiLogExecution('DEBUG', 'vemployee :', vemployee);
	nlapiLogExecution('DEBUG', 'currentUserID :', currentUserID);
	//case# 20148648 starts
	if(NewRecId!=null&&NewRecId!='')
		transaction.setFieldValue('custrecord_invref_no',NewRecId);
	nlapiLogExecution('Error', 'NewRecId :', NewRecId);
	//case# 20148648 ends
//	if (vemployee != null && vemployee != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',vemployee);
//	} 
//	else if (currentUserID != null && currentUserID != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
//	}

	nlapiLogExecution('DEBUG', 'Updating RF Open Task Record Id: ', RcId);
	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('DEBUG', 'Updated RF Open Task successfully');

	deleteOldAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty);
	deleteNewAllocations(NewRecId,PickQty,getEnteredContainerNo,ActPickQty);
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteOldAllocations(invrefno,pickqty,contlpno,ActPickQty)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	try
	{
		var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

		if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
			Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(ActPickQty)).toFixed(4));
		}

		//Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH) - parseFloat(ActPickQty)); 
		Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
		Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		/*if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);
		 */
		/*if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}*/
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteNewAllocations(invrefno,pickqty,contlpno,ActPickQty)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	try
	{
		var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');



		Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH) - parseFloat(ActPickQty)).toFixed(4)); 
		Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
		Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
}

function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	var pickqtyfinal =0;
	nlapiLogExecution('DEBUG', 'into UpdateRFFulfillOrdLine : ', vdono);

	var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);
	doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	doline.setFieldValue('custrecord_upddate', DateStamp());

	var oldpickQty=doline.getFieldValue('custrecord_pickqty');

	if(isNaN(oldpickQty))
		oldpickQty=0;

	pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);
	nlapiLogExecution('DEBUG', 'pickqtyfinal in blocklevel', parseFloat(pickqtyfinal).toFixed(4));	
	doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal).toFixed(4));
	doline.setFieldValue('custrecord_pickgen_qty', parseFloat(pickqtyfinal).toFixed(4));

	nlapiSubmitRecord(doline, false, true);
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty) 
{
	nlapiLogExecution('DEBUG', 'Into CreateSTGInvtRecord (Container LP)',vContLp);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');			
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of CreateSTGInvtRecord');
}
