/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMoveContinue.js,v $
 *     	   $Revision: 1.13.2.9.4.7.2.15.2.2 $
 *     	   $Date: 2015/11/24 11:09:06 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_180 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * 
 * *
 *****************************************************************************/
function InventoryMoveContinue(request, response)
{
	
	nlapiLogExecution('ERROR', 'Time Stamp at the START',TimeStampinSec());
	
	var getBinLocationId = request.getParameter('custparam_imbinlocationid');
	var getBinLocation = request.getParameter('custparam_imbinlocationname');
	var getItemId = request.getParameter('custparam_imitemid');
	var getItem = request.getParameter('custparam_imitem');
	var getItemDescId = request.getParameter('custparam_itemDescid');
	var getItemDesc = request.getParameter('custparam_itemDesc');
	var getTotQuantity = '';//request.getParameter('custparam_totquantity');
	var getAvailQuantity = '';//request.getParameter('custparam_availquantity');
	var getLOTId = '';//request.getParameter('custparam_lotid');
	var getLOTNo = '';//request.getParameter('custparam_lot');
	var getStatusId = '';//request.getParameter('custparam_statusid');
	var getStatus = '';//request.getParameter('custparam_status');
	var getLPId = request.getParameter('custparam_lpid');
	var getAllocatdqty = '';

	var getUOM = '';//request.getParameter('custparam_uom');
	var getUOMId = '';//request.getParameter('custparam_uom');
	var getActualBeginDate = request.getParameter('custparam_actualbegindate');
	var getActualBeginTime = request.getParameter('custparam_actualbegintime');
	var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
	var getInvtRecID='';
	var locationId='';
	var GetNewLp='';
	var getNumber=0;
	var getLP ;
	var ItemDesc='';
	var compId='';
	var fifodate='';
	if(request.getParameter('custparam_number') != null && request.getParameter('custparam_number') != "")
	{
		getNumber=parseFloat(request.getParameter('custparam_number'));
	}
	else
	{
		getNumber=0; 
		getLP = request.getParameter('custparam_lp');
	}

	var	tempBinLocationId=request.getParameter('custparam_hdntmplocation');
	var tempItemId=request.getParameter('custparam_hdntempitem');
	var tempLP=request.getParameter('custparam_hdntemplp');
	var total;

	var str = 'BinLocation From Invt Move Main. = ' + getBinLocation + '<br>';				
	str = str + 'BinLocation ID From Invt Move Main. = ' + getBinLocationId + '<br>';
	str = str + 'LP From Invt Move Main. = ' + getLP + '<br>';
	str = str + 'getItem. = ' + getItem + '<br>';
	str = str + 'getItemDesc. = ' + getItemDesc + '<br>';
	str = str + 'getNumber. = ' + getNumber + '<br>';

	nlapiLogExecution('ERROR', 'getBinLocationId', getBinLocationId);
	nlapiLogExecution('ERROR', 'getLP', getLP);
	nlapiLogExecution('ERROR', 'getItemId', getItemId);


	// Fetch the Location for the item entered
	//if(getBinLocationId != null && getBinLocationId != "" && (getLP == null || getLP == "") && (getItemId == null || getItemId == ""))
	if(tempBinLocationId != null && tempBinLocationId != "" )
	{
		nlapiLogExecution('ERROR', 'Into Search Results by getBinLocationId', tempBinLocationId);	 
		var BinLocationFilters = new Array();    
		BinLocationFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', tempBinLocationId));
		if(tempLP!=null && tempLP!="")   
			BinLocationFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', tempLP));
		BinLocationFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19,3]));
		BinLocationFilters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
		BinLocationFilters.push(new nlobjSearchFilter("isinactive",'custrecord_ebiz_inv_binloc','is', "F"));
		BinLocationFilters.push(new nlobjSearchFilter("isinactive",'custrecord_ebiz_inv_sku','is', "F"));
		if(tempItemId!=null && tempItemId!="")   
			BinLocationFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', tempItemId));
		var vRoleLocation=getRoledBasedLocationNew();
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			vRoleLocation.push('@NONE@');
			BinLocationFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', vRoleLocation));
		}
		var BinLocationColumns = new Array();
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));//binlocation
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));//sitelocation
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty'));
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));//sitelocation
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));//sitelocation
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku'));
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_itemdesc'));
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_company'));
		BinLocationColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));

		var BinLocationResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, BinLocationFilters, BinLocationColumns);


		if (BinLocationResults != null) 
		{
			nlapiLogExecution('ERROR', 'BinLocationResults.length', BinLocationResults.length);  
			total = BinLocationResults.length;
			if(parseFloat(getNumber)<parseFloat(total))
			{
				var recNo=0;
				if(getNumber!=0)
					recNo=parseFloat(getNumber);

				//for (var t = 0; t < BinLocationResults.length; t++) {
				var BinLocationResult = BinLocationResults[recNo];	
				getItem = BinLocationResult.getText('custrecord_ebiz_inv_sku');
				getItemId = BinLocationResult.getValue('custrecord_ebiz_inv_sku');
				getBinLocation = BinLocationResult.getText('custrecord_ebiz_inv_binloc');
				getBinLocationId = BinLocationResult.getValue('custrecord_ebiz_inv_binloc');
				getInvtRecID = BinLocationResult.getId();
				locationId = BinLocationResult.getText('custrecord_ebiz_inv_loc');
				getStatus = BinLocationResult.getText('custrecord_ebiz_inv_sku_status');
				getStatusId = BinLocationResult.getValue('custrecord_ebiz_inv_sku_status');
				GetNewLp=BinLocationResult.getValue('custrecord_ebiz_inv_lp');
				getTotQuantity = BinLocationResult.getValue('custrecord_ebiz_qoh');
				getAllocatdqty=BinLocationResult.getValue('custrecord_ebiz_alloc_qty');	
				if(getAllocatdqty==null || getAllocatdqty=='')
					getAllocatdqty=0;
				var Availqty=parseFloat(getTotQuantity)-parseFloat(getAllocatdqty);
				
//				if(Availqty==null || Availqty=='')
//					Availqty=0;
				
				/*if((Availqty.toString()=="NaN")||(Availqty==''))
				{
					Availqty='';
				}*/
				
				if(isNaN(getTotQuantity)||getTotQuantity==null||getTotQuantity=="")
				{
				     nlapiLogExecution('ERROR', 'getTotQuantity to tst first: ', getTotQuantity);
				     getTotQuantity=0;
				}
				
				if(isNaN(getAllocatdqty)||getAllocatdqty==null||getAllocatdqty=="")
				{
				     nlapiLogExecution('ERROR', 'getAllocatdqty to tst first: ', getAllocatdqty);
				     getAllocatdqty=0;
				}
				
				if(isNaN(Availqty)||Availqty==null||Availqty=="")
				{
				     nlapiLogExecution('ERROR', 'Availqty to tst first: ', Availqty);
				     Availqty=0;
				}
				
				getAvailQuantity = Availqty;
				getLOTNo = BinLocationResult.getText('custrecord_ebiz_inv_lot');
				getLOTId = BinLocationResult.getValue('custrecord_ebiz_inv_lot');
				locationId = BinLocationResult.getValue('custrecord_ebiz_inv_loc');//siteLocation
				getInvtRecID = BinLocationResult.getId();
				ItemDesc=BinLocationResult.getValue('custrecord_ebiz_itemdesc');
				compId=BinLocationResult.getValue('custrecord_ebiz_inv_company');
				fifodate=BinLocationResult.getValue('custrecord_ebiz_inv_fifo');				

				var str = 'Total QOH in Location. = ' + getTotQuantity + '<br>';				
				str = str + 'Allocated Qty. = ' + getAllocatdqty + '<br>';
				str = str + 'Available Qty. = ' + getAvailQuantity + '<br>';
				str = str + 'Invt Rec Id. = ' + getInvtRecID + '<br>';
				str = str + 'FIFO Date. = ' + fifodate + '<br>';
				str = str + 'Item. = ' + getItem + '<br>';
				str = str + 'Record #. = ' + recNo + '<br>';

				nlapiLogExecution('ERROR', 'Log in Search Results by Location', str);

				//}
			}
			else
			{
				var IMovearray=new Array();
				IMovearray["custparam_error"] = 'No More Records';
				IMovearray["custparam_screenno"] = '19';
				IMovearray["custparam_hdntmplocation"]=tempBinLocationId;
				IMovearray["custparam_hdntempitem"]=tempItemId;
				IMovearray["custparam_hdntemplp"]=tempLP;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
				return;
			}
		}
		else
		{
			nlapiLogExecution('ERROR', 'BinLocationResults null', BinLocationResults);	 
			nlapiLogExecution('ERROR', 'INVALID Location', '');	    		
			var IMovearray=new Array();
			IMovearray["custparam_error"] = 'INVALID LOCATION/LP/ITEM';
			IMovearray["custparam_screenno"] = '18';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
			return;
		}
		//nlapiLogExecution('ERROR', 'Fetched Location locationId if', locationId);
		//nlapiLogExecution('ERROR', 'Fetched Location if', getBinLocation);
	}

	else if(tempLP != null && tempLP != "")
	{

		nlapiLogExecution('ERROR', 'Into Search Results by LP', tempLP);	  

		var searchFilters = new Array();    
		searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', tempLP));
		searchFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19,3]));
		searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));


		if(tempBinLocationId!=null && tempBinLocationId!="")    	
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', tempBinLocationId));
		
		if(tempItemId!=null && tempItemId!="")   
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', tempItemId));
		searchFilters.push(new nlobjSearchFilter("isinactive",'custrecord_ebiz_inv_binloc','is', "F"));
		searchFilters.push(new nlobjSearchFilter("isinactive",'custrecord_ebiz_inv_sku','is', "F"));
		var vRoleLocation=getRoledBasedLocation();
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			//case 20126102 start:
			vRoleLocation.push('@NONE@');
			searchFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', vRoleLocation));
			//case 20126102 end
		}
		var searchColumns = new Array();
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));//sitelocation
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_itemdesc'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_company'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku'));
		searchColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));


		var searchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, searchFilters, searchColumns);

		if (searchResults != null) 
		{
			total=searchResults.length;
			if(parseFloat(getNumber)<parseFloat(total))
			{
				var recNo=0;
				if(getNumber!=0)
					recNo=parseFloat(getNumber);

				var ItemResult = searchResults[recNo];		
				getItem = ItemResult.getText('custrecord_ebiz_inv_sku');
				getItemId = ItemResult.getValue('custrecord_ebiz_inv_sku');
				getInvtRecID = ItemResult.getId();
				locationId = ItemResult.getValue('custrecord_ebiz_inv_loc');//siteLocation
				getBinLocation = ItemResult.getText('custrecord_ebiz_inv_binloc');
				getBinLocationId = ItemResult.getValue('custrecord_ebiz_inv_binloc');
				getStatus = ItemResult.getText('custrecord_ebiz_inv_sku_status');
				GetNewLp = ItemResult.getValue('custrecord_ebiz_inv_lp');
				nlapiLogExecution('ERROR', 'GetNewLp', GetNewLp);

				getStatusId = ItemResult.getValue('custrecord_ebiz_inv_sku_status');
				getTotQuantity = ItemResult.getValue('custrecord_ebiz_qoh');
				//getAllocatdqty = ItemResult.getValue('custrecord_ebiz_alloc_qty');
				if(ItemResult.getValue('custrecord_ebiz_alloc_qty') != null && ItemResult.getValue('custrecord_ebiz_alloc_qty') != '')
				{
					getAllocatdqty = ItemResult.getValue('custrecord_ebiz_alloc_qty');
				}
				else
				{
					getAllocatdqty = 0;
				}
				var availqty=parseFloat(getTotQuantity)-parseFloat(getAllocatdqty);
				
//				if(Availqty==null || Availqty=='')
//				Availqty=0;
				
				/*if((availqty.toString()=="NaN")||(availqty==''))
				{
					availqty='';
				}*/
				if(isNaN(getTotQuantity)||getTotQuantity==null||getTotQuantity=="")
				{
				     nlapiLogExecution('ERROR', 'getTotQuantity to tst first: ', getTotQuantity);
				     getTotQuantity=0;
				}
				
				if(isNaN(getAllocatdqty)||getAllocatdqty==null||getAllocatdqty=="")
				{
				     nlapiLogExecution('ERROR', 'getAllocatdqty to tst first: ', getAllocatdqty);
				     getAllocatdqty=0;
				}
				
				if(isNaN(availqty)||availqty==null||availqty=="")
				{
				     nlapiLogExecution('ERROR', 'availqty to tst first: ', availqty);
				     availqty=0;
				}
				
				getAvailQuantity = availqty;
				getLOTNo = ItemResult.getText('custrecord_ebiz_inv_lot');
				getLOTId = ItemResult.getValue('custrecord_ebiz_inv_lot');
				locationId = ItemResult.getValue('custrecord_ebiz_inv_loc');//siteLocation
				ItemDesc=ItemResult.getValue('custrecord_ebiz_itemdesc');
				compId=ItemResult.getValue('custrecord_ebiz_inv_company');
				fifodate=ItemResult.getValue('custrecord_ebiz_inv_fifo');
				getInvtRecID = ItemResult.getId();

				var str = 'Total QOH in Location. = ' + getTotQuantity + '<br>';				
				str = str + 'Allocated Qty. = ' + getAllocatdqty + '<br>';
				str = str + 'Available Qty. = ' + getAvailQuantity + '<br>';
				str = str + 'Invt Rec Id. = ' + getInvtRecID + '<br>';
				str = str + 'FIFO Date. = ' + fifodate + '<br>';
				str = str + 'Item. = ' + getItem + '<br>';
				str = str + 'Record #. = ' + recNo + '<br>';

				nlapiLogExecution('ERROR', 'Log in Search Results by LP', str);
			}
			else
			{
				var IMovearray=new Array();
				IMovearray["custparam_error"] = 'No More Records';
				IMovearray["custparam_screenno"] = '19';
				IMovearray["custparam_hdntmplocation"]=tempBinLocationId;
				IMovearray["custparam_hdntempitem"]=tempItemId;
				IMovearray["custparam_hdntemplp"]=tempLP;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
				return;
			}

//			total = searchResults.length;
//			var Results = searchResults[0];		
//			getBinLocation = Results.getText('custrecord_ebiz_inv_binloc');
//			getBinLocationId = Results.getValue('custrecord_ebiz_inv_binloc');

//			getStatus = Results.getText('custrecord_ebiz_inv_sku_status');
//			getStatusId = Results.getValue('custrecord_ebiz_inv_sku_status');
//			nlapiLogExecution('ERROR', 'getStatusId', getStatusId);

//			getTotQuantity = Results.getValue('custrecord_ebiz_qoh');
//			getAllocatdqty = Results.getValue('custrecord_ebiz_alloc_qty');
//			nlapiLogExecution('ERROR', 'getTotQuantity loc', getTotQuantity);  
//			nlapiLogExecution('ERROR', 'getAllocatdqty', getAllocatdqty); 
//			var Availqty=parseFloat(getTotQuantity)-parseFloat(getAllocatdqty);
//			getAvailQuantity = Availqty;
//			nlapiLogExecution('ERROR', 'getAvailQuantity loc', getAvailQuantity);
//			getLOTNo = Results.getText('custrecord_ebiz_inv_lot');
//			getLOTId = Results.getValue('custrecord_ebiz_inv_lot');
//			locationId = Results.getValue('custrecord_ebiz_inv_loc');//siteLocation
//			ItemDesc=Results.getValue('custrecord_ebiz_itemdesc');
//			compId=Results.getValue('custrecord_ebiz_inv_company');
//			fifodate=Results.getValue('custrecord_ebiz_inv_fifo');
//			getInvtRecID = Results.getId();
//			nlapiLogExecution('ERROR', 'getInvtRecID1', getInvtRecID);
//			nlapiLogExecution('ERROR', 'fifodate', fifodate);
		}
		else
		{
			nlapiLogExecution('ERROR', 'INVALID LP', '');	    		
			var IMovearray=new Array();
			IMovearray["custparam_error"] = 'INVALID LOCATION/LP/ITEM';
			IMovearray["custparam_screenno"] = '18';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
			return false;
		}
		nlapiLogExecution('ERROR', 'Fetched Location locationId else', locationId);
		nlapiLogExecution('ERROR', 'Fetched Location else', getBinLocation);


		// Fetch the Item for the LP entered
//		if(getItem == null || getItem == "")
//		{
//		nlapiLogExecution('ERROR', 'getLP2', getLP);	  
//		nlapiLogExecution('ERROR', 'getBinLocationId2', getBinLocationId);	

//		var ItemFilters = new Array();    
//		// ItemFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', getLP));
//		if(getBinLocationId!=null && getBinLocationId!="")  
//		ItemFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', getBinLocationId));

//		if(getLP!=null && getLP!="")   
//		ItemFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', getLP));

//		ItemFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19,3]));
//		ItemFilters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

//		var ItemColumns = new Array();
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku'));
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));//sitelocation	    
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty'));
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));			
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_itemdesc'));
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_company'));
//		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));
//		var ItemResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, ItemFilters, ItemColumns);

//		if (ItemResults != null) 
//		{
//		total=ItemResults.length;
//		if(parseFloat(getNumber)<parseFloat(total))
//		{
//		var recNo=0;
//		if(getNumber!=0)
//		recNo=parseFloat(getNumber);

//		var ItemResult = ItemResults[recNo];		
//		getItem = ItemResult.getText('custrecord_ebiz_inv_sku');
//		nlapiLogExecution('ERROR','GETITEM',getItem+'-'+recNo);
//		getItemId = ItemResult.getValue('custrecord_ebiz_inv_sku');
//		getInvtRecID = ItemResult.getId();
//		locationId = ItemResult.getValue('custrecord_ebiz_inv_loc');//siteLocation


//		getBinLocation = ItemResult.getText('custrecord_ebiz_inv_binloc');
//		getBinLocationId = ItemResult.getValue('custrecord_ebiz_inv_binloc');

//		getStatus = ItemResult.getText('custrecord_ebiz_inv_sku_status');
//		getStatusId = ItemResult.getValue('custrecord_ebiz_inv_sku_status');

//		getTotQuantity = ItemResult.getValue('custrecord_ebiz_qoh');
//		getAllocatdqty = ItemResult.getValue('custrecord_ebiz_alloc_qty');
//		nlapiLogExecution('ERROR', 'getTotQuantity item', getTotQuantity); 
//		nlapiLogExecution('ERROR', 'getAllocatdqty', getAllocatdqty); 

//		var availqty=parseFloat(getTotQuantity)-parseFloat(getAllocatdqty);

//		getAvailQuantity = availqty;
//		nlapiLogExecution('ERROR', 'getAvailQuantity item', getAvailQuantity);
//		getLOTNo = ItemResult.getText('custrecord_ebiz_inv_lot');
//		getLOTId = ItemResult.getValue('custrecord_ebiz_inv_lot');
//		locationId = ItemResult.getValue('custrecord_ebiz_inv_loc');//siteLocation
//		ItemDesc=ItemResult.getValue('custrecord_ebiz_itemdesc');
//		compId=ItemResult.getValue('custrecord_ebiz_inv_company');
//		fifodate=ItemResult.getValue('custrecord_ebiz_inv_fifo');
//		getInvtRecID = ItemResult.getId();
//		nlapiLogExecution('ERROR', 'getInvtRecID2', getInvtRecID);
//		nlapiLogExecution('ERROR', 'fifodate', fifodate);
//		}
//		else
//		{
//		var IMovearray=new Array();
//		IMovearray["custparam_error"] = 'No Records';
//		IMovearray["custparam_screenno"] = '19';
//		IMovearray["custparam_imbinlocationid"]=getBinLocationId;
//		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
//		}
//		nlapiLogExecution('ERROR', 'Fetched Item locationId if', locationId);
//		nlapiLogExecution('ERROR', 'Fetched Item if', getItem);
//		}

//		} 
	}
	else if(tempItemId != null && tempItemId != "")
	{
		nlapiLogExecution('ERROR', 'Into Search Results by getItemId', tempItemId);	  
		var ItemidFilters = new Array();  

		ItemidFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', tempItemId));

		ItemidFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19,3]));
		ItemidFilters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
		ItemidFilters.push(new nlobjSearchFilter("isinactive",'custrecord_ebiz_inv_binloc','is', "F"));
		ItemidFilters.push(new nlobjSearchFilter("isinactive",'custrecord_ebiz_inv_sku','is', "F"));
		var vRoleLocation=getRoledBasedLocation();
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			//case 20126239 start
			vRoleLocation.push('@NONE@');
			ItemidFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', vRoleLocation));
			//case 20126239 end
		}		
		var ItemColumns = new Array();
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));//binlocation
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));//sitelocation
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty'));
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));//sitelocation
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));//sitelocation
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku'));
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_itemdesc'));
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_company'));
		ItemColumns.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));

		var ItemidResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, ItemidFilters, ItemColumns);


		if (ItemidResults != null) 
		{
			nlapiLogExecution('ERROR', 'ItemidResults.length', ItemidResults.length);			 
			total = ItemidResults.length;
			nlapiLogExecution('ERROR', 'ItemidResults getNumber', getNumber);  		
			if(parseInt(getNumber)<parseInt(total))
			{
				var recNo=0;
				if(getNumber!=0)
					recNo=parseInt(getNumber);

				//for (var t = 0; t < ItemidResults.length; t++) {
				var ItemidResult = ItemidResults[recNo];	
				getItem = ItemidResult.getText('custrecord_ebiz_inv_sku');
				getItemId = ItemidResult.getValue('custrecord_ebiz_inv_sku');
				getBinLocation = ItemidResult.getText('custrecord_ebiz_inv_binloc');
				getBinLocationId = ItemidResult.getValue('custrecord_ebiz_inv_binloc');
				getInvtRecID = ItemidResult.getId();
				locationId = ItemidResult.getText('custrecord_ebiz_inv_loc');
				getStatus = ItemidResult.getText('custrecord_ebiz_inv_sku_status');
				getStatusId = ItemidResult.getValue('custrecord_ebiz_inv_sku_status');
				GetNewLp=ItemidResult.getValue('custrecord_ebiz_inv_lp');
				getTotQuantity = ItemidResult.getValue('custrecord_ebiz_qoh');
				getAllocatdqty=ItemidResult.getValue('custrecord_ebiz_alloc_qty');	
				if(getAllocatdqty==null || getAllocatdqty=='' || getAllocatdqty=='null')
					getAllocatdqty=0;
				var Availqty=parseFloat(getTotQuantity)-parseFloat(getAllocatdqty);
				getAvailQuantity = Availqty;
				getLOTNo = ItemidResult.getText('custrecord_ebiz_inv_lot');
				getLOTId = ItemidResult.getValue('custrecord_ebiz_inv_lot');
				locationId = ItemidResult.getValue('custrecord_ebiz_inv_loc');//siteLocation
				getInvtRecID = ItemidResult.getId();
				ItemDesc=ItemidResult.getValue('custrecord_ebiz_itemdesc');
				compId=ItemidResult.getValue('custrecord_ebiz_inv_company');
				fifodate=ItemidResult.getValue('custrecord_ebiz_inv_fifo');				

				var str = 'Total QOH in Location. = ' + getTotQuantity + '<br>';				
				str = str + 'Allocated Qty. = ' + getAllocatdqty + '<br>';
				str = str + 'Available Qty. = ' + getAvailQuantity + '<br>';
				str = str + 'Invt Rec Id. = ' + getInvtRecID + '<br>';
				str = str + 'FIFO Date. = ' + fifodate + '<br>';
				str = str + 'Item. = ' + getItem + '<br>';
				str = str + 'Record #. = ' + recNo + '<br>';

				nlapiLogExecution('ERROR', 'Log in Search Results by ItemId', str);
			}
			else
			{
				var IMovearray=new Array();
				IMovearray["custparam_error"] = 'No More Records';
				IMovearray["custparam_screenno"] = '19';
				IMovearray["custparam_hdntempitem"]=tempItemId;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
				return;
			}
		}
		else
		{
			nlapiLogExecution('ERROR', 'ItemidResults null', ItemidResults);	 
			nlapiLogExecution('ERROR', 'INVALID ITEM', '');	    		
			var IMovearray=new Array();
			IMovearray["custparam_error"] = 'INVALID LOCATION/LP/ITEM';
			IMovearray["custparam_screenno"] = '18';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
			return;
		}

	}
	if(getLP==null || getLP =='')
	{
		getLP=GetNewLp;
	}
	nlapiLogExecution('ERROR', 'getLP', getLP);
	nlapiLogExecution('ERROR', 'GetNewLp', GetNewLp);
	nlapiLogExecution('ERROR', 'ItemDesc', ItemDesc);
	nlapiLogExecution('ERROR', 'getItem Internal Id', getItemId);
	var Itemdescription='';
	
	if(getItemId!=null && getItemId!='')
	{
		var Itype = nlapiLookupField('item', getItemId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemId);

		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('description');
		}
		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('salesdescription');
		}
	}
	else
	{
		var IMovearray=new Array();
		IMovearray["custparam_error"] = 'No Records';
		IMovearray["custparam_screenno"] = '18';
		IMovearray["custparam_imbinlocationid"]=getBinLocationId;
		IMovearray["custparam_lp"]=getLP;
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMovearray);
	}
	nlapiLogExecution('ERROR', 'Itemdescription', Itemdescription);
	
	nlapiLogExecution('ERROR', 'Time Stamp at the START of GET method',TimeStampinSec());
	
	if (request.getMethod() == 'GET') 
	{
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st12;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "INVENTARIO DE MUDANZA";
			st1 = "SKU / LUGAR SELECCIONADO:";
			st2 = "UBICACI&#211;N :";
			st3 = "TEMA";
			st4 = "ESTADO";
			st5 = "LP ";
			st6 = "CANTIDAD TOTAL DE ";
			st7 = "NEXT";
			st8 = "CONTINUAR";
			st9 = "ANTERIOR";
			st10 = "CANTIDAD DISPONIBLE";
			
		}
		else
		{
			st0 = "INVENTORY MOVE";
			st1 = "SKU/LOCATION SELECTED:";
			st2 = "LOCATION :";
			st3 = "ITEM";
			st12 = "ITEM DESC : "
			st4 = "STATUS";
			st5 = "LP ";
			st6 = "TOTAL QTY";
			st7 = "NEXT";
			st8 = "CONTINUE";
			st9 = "PREV";
			st10 = "AVAIL QTY";
			
	
		}
			
		var functionkeyHtml=getFunctionkeyScript('_rf_inventorymove_continue');		
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('cmdSearch').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventorymove_continue' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " OF <label>" + parseFloat(total) + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + "<label>" + getBinLocation + "</label>";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value='" + getBinLocation + "'>";//Case# 20127589
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value='" + getItem + "'>";// Case#20127589 Item must be pass as string through url 
		html = html + "					<input type='hidden' name='hdnLP' value='" + getLP + "'>";//Case# 20127589
		html = html + "					<input type='hidden' name='hdnLPId' value=" + getLPId + ">";        
		html = html + "					<input type='hidden' name='hdnLOTNo' value=" + getLOTNo + ">";
		html = html + "					<input type='hidden' name='hdnLOTNoId' value=" + getLOTId + ">";
		html = html + "					<input type='hidden' name='hdnStatus' value='" + getStatus + "'>";//Case# 20127589
		html = html + "					<input type='hidden' name='hdnStatusId' value=" + getStatusId + ">";
		html = html + "					<input type='hidden' name='hdnUOM' value=" + getUOM + ">";
		html = html + "					<input type='hidden' name='hdnUOMId' value=" + getUOMId + ">";
		html = html + "					<input type='hidden' name='hdnTotQty' value=" + getTotQuantity + ">";
		html = html + "					<input type='hidden' name='hdnAvailQty' value=" + getAvailQuantity + ">";
		html = html + "					<input type='hidden' name='hdnInvtRecID' value=" + getInvtRecID + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnlocationId' value=" + locationId + ">";
		html = html + "				    <input type='hidden' name='hdncompId' value=" + compId + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + ItemDesc + "'>";
		html = html + "				    <input type='hidden' name='hdnfifodate' value='" + fifodate + "'>";
		
		html = html + "				    <input type='hidden' name='hdntmplocation' value=" + tempBinLocationId + ">";
		html = html + "				    <input type='hidden' name='hdntemplp' value='" + tempLP + "'>";
		html = html + "				    <input type='hidden' name='hdntempitem' value='" + tempItemId + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 +" :<label>" + getItem + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + Itemdescription + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st12 +" <label>" + Itemdescription + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " :<label>" + getStatus + "</label>";//LOT :<label>" + getLOTNo + "</label> ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " :<label>" + GetNewLp + "</label>"; //UOM :<label>" + getUOM + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6+ " :<label>" + getTotQuantity + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st10 + ":<label>" + getAvailQuantity + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr><td align = 'left'>";
		if(total>1)
		{
			html = html + "				" + st7 +" <input name='cmdnext' type='submit' value='F7'/>";
		}
		html = html + "				" + st8 + " <input name='cmdSearch' id='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				" + st9 + " <input name='cmdPreviouscmdSearch' type='submit' value='F9'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSearch').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";
		
		nlapiLogExecution('ERROR', 'Time Stamp at the END of GET method',TimeStampinSec());

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('ERROR', 'Time Stamp at the START of POST method',TimeStampinSec());
		
		nlapiLogExecution('ERROR', 'ItemDesc1', request.getParameter('hdnItemdesc'));
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var IMarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		IMarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', IMarray["custparam_language"]);
    	
				
		IMarray["custparam_imbinlocationid"] = request.getParameter('hdnBinLocationId');
		IMarray["custparam_imbinlocationname"] = request.getParameter('hdnBinLocation');
		IMarray["custparam_imitemid"] = request.getParameter('hdnItemId');
		
		IMarray["custparam_hdntmplocation"] = request.getParameter('hdntmplocation');
		IMarray["custparam_hdntemplp"] = request.getParameter('hdntemplp');
		IMarray["custparam_hdntempitem"] = request.getParameter('hdntempitem');
		
		IMarray["custparam_imitem"] = request.getParameter('hdnItem');
		IMarray["custparam_totquantity"] = request.getParameter('hdnTotQty');
		IMarray["custparam_availquantity"] = request.getParameter('hdnAvailQty');  		
		IMarray["custparam_status"] = request.getParameter('hdnStatus');
		IMarray["custparam_statusid"] = request.getParameter('hdnStatusId');
		IMarray["custparam_uom"] = request.getParameter('hdnUOM');
		IMarray["custparam_uomid"] = request.getParameter('hdnUOMId');		
		IMarray["custparam_lot"] = request.getParameter('hdnLOTNo');
		IMarray["custparam_lotid"] = request.getParameter('hdnLOTNoId');
		IMarray["custparam_lp"] = request.getParameter('hdnLP');
		IMarray["custparam_lpid"] = request.getParameter('hdnLPId');
		IMarray["custparam_invtrecid"] = request.getParameter('hdnInvtRecID');
		IMarray["custparam_locationId"] = request.getParameter('hdnlocationId');
		nlapiLogExecution('ERROR', 'custparam_locationId', IMarray["custparam_locationId"]);
		IMarray["custparam_actualbegindate"] = getActualBeginDate;
		IMarray["custparam_itemdesc"] = request.getParameter('hdnItemdesc');
		IMarray["custparam_compid"] = request.getParameter('hdncompId');
		IMarray["custparam_fifodate"] = request.getParameter('hdnfifodate');

		nlapiLogExecution('ERROR', 'Compid', request.getParameter('hdncompId'));


		IMarray["custparam_screenno"] = '19';	


		var itemSubtype = nlapiLookupField('item', request.getParameter('hdnItemId'), ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
		IMarray["custparam_itemtype"] = itemSubtype.recordType;
		nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype.recordType);
		nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
		nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);

		var TimeArray = new Array();
		IMarray["custparam_actualbegintime"] = getActualBeginTime;
		IMarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;

		var optedEvent = request.getParameter('cmdPreviouscmdSearch');
		var optnext=request.getParameter('cmdnext');
		if(optnext=='F7')
		{
			getNumber=getNumber+1;
			IMarray["custparam_number"]= getNumber;
			response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_cont', 'customdeploy_rf_inventory_move_cont_di', false, IMarray);
			//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_main', 'customdeploy_rf_inventory_move_main_di', false, IMarray);
		}
		else if (optedEvent == 'F9') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_main', 'customdeploy_rf_inventory_move_main_di', false, IMarray);
		}
		else 
		{
			try 
			{    
				nlapiLogExecution('ERROR', 'Hi1');   
				nlapiLogExecution('ERROR', 'itemSubtype.recordType',itemSubtype.recordType);
				nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizbatchlot',itemSubtype.custitem_ebizbatchlot);
				//if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == 'lotnumberedassemblyitem' || itemSubtype.recordType == 'assemblyitem' || itemSubtype.custitem_ebizbatchlot == 'T') 
				if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == 'lotnumberedassemblyitem'  || itemSubtype.custitem_ebizbatchlot == 'T')
				{
					nlapiLogExecution('ERROR', 'Hi2');					
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_batchconf', 'customdeploy_rf_inventory_move_batch_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Navigating to Batch Confirm', 'Success');                      
				}
				else
				{    
					nlapiLogExecution('ERROR', 'Hi3');
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_qty', 'customdeploy_rf_inventory_move_qty_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Navigating to Move Qty', 'Success');
				}
			} 
			catch (e) 
			{
				nlapiLogExecution('ERROR', 'Hi4');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Hi5');
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}
		
		nlapiLogExecution('ERROR', 'Time Stamp at the END of POST method',TimeStampinSec());
	}
	
	nlapiLogExecution('ERROR', 'Time Stamp at the END',TimeStampinSec());
}
