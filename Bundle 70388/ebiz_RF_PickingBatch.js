/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingBatch.js,v $
 *     	   $Revision: 1.6.2.6.4.5.2.17.2.2 $
 *     	   $Date: 2015/11/16 10:31:38 $
 *     	   $Author: skreddy $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_150 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingBatch.js,v $
 * Revision 1.6.2.6.4.5.2.17.2.2  2015/11/16 10:31:38  skreddy
 * case 201414545
 * 2015.2 issue fix
 *
 * Revision 1.6.2.6.4.5.2.17.2.1  2015/09/28 15:12:23  sponnaganti
 * 201414641
 * Briggs Issue fix
 *
 * Revision 1.6.2.6.4.5.2.17  2015/07/24 15:25:54  skreddy
 * Case# 201413611
 * 2015.2  issue fix
 *
 * Revision 1.6.2.6.4.5.2.16  2015/07/16 15:23:32  grao
 * 2015.2   issue fixes  201413082
 *
 * Revision 1.6.2.6.4.5.2.15  2014/06/23 08:13:53  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.6.2.6.4.5.2.14  2014/05/30 00:41:04  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.6.2.6.4.5.2.13  2014/04/17 15:51:03  skavuri
 * Case # 20148058 SB issue fixed
 *
 * Revision 1.6.2.6.4.5.2.12  2014/03/11 14:58:51  sponnaganti
 * case# 20127638,20127661
 * (Standard Bundle Issue Fix)
 *
 * Revision 1.6.2.6.4.5.2.11  2014/03/10 16:19:43  sponnaganti
 * case# 20127531
 * Standard Bundle Issue fix
 *
 * Revision 1.6.2.6.4.5.2.10  2014/03/10 16:15:56  sponnaganti
 * case# 20127531
 * Standard Bundle Issue fix
 *
 * Revision 1.6.2.6.4.5.2.9  2013/11/22 08:40:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201216680
 *
 * Revision 1.6.2.6.4.5.2.8  2013/10/24 14:29:34  schepuri
 * 20125164
 *
 * Revision 1.6.2.6.4.5.2.7  2013/07/08 14:42:05  rrpulicherla
 * Case# 20123341
 * Picking Issues
 *
 * Revision 1.6.2.6.4.5.2.6  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.6.2.6.4.5.2.5  2013/05/13 15:08:06  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.6.2.6.4.5.2.4  2013/05/07 15:37:53  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.6.2.6.4.5.2.3  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.6.2.6.4.5.2.2  2013/04/03 17:11:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Standed bundle
 *
 * Revision 1.6.2.6.4.5.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.6.2.6.4.5  2013/02/18 06:31:52  skreddy
 * CASE201112/CR201113/LOG201121
 *  To process for new Lot#
 *
 * Revision 1.6.2.6.4.4  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.6.2.6.4.3  2013/01/25 06:57:47  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to wrong location
 *
 * Revision 1.6.2.6.4.2  2012/10/23 17:08:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.6.2.6.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.6.2.6  2012/07/20 15:33:31  spendyala
 * CASE201112/CR201113/LOG201121
 * Depending upon system rule driving user to Scan lot# or not?
 *
 * Revision 1.6.2.5  2012/04/20 11:21:27  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.6.2.4  2012/04/19 15:00:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.6.2.3  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.6.2.2  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.6.2.1  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.9  2012/02/03 10:44:31  gkalla
 * CASE201112/CR201113/LOG201121
 * Commented soarray line
 *
 * Revision 1.7  2012/01/12 00:07:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.6  2012/01/03 08:33:05  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/12/27 15:43:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.4  2011/07/20 14:25:38  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.10  2011/07/20 12:57:48  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.4  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function PickingBatch(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10;

		if( getLanguage == 'es_ES')
		{
			st1 = "LOTE # : ";
			st2 = "OVERRIDE";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";


		}
		else
		{
			st1 = "LOT# : ";
			st2 = "SEND ";
			st3 = "PREV";
			st4 = "OVERRIDE";
		}
		var getZoneNo = request.getParameter('custparam_zoneno');
		var getZoneId=request.getParameter('custparam_ebizzoneno');
		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		// case no 20125164? 
		var Bulkpickflag=request.getParameter('custparam_bulkpickflag');
		var getFetchedLocation = request.getParameter('custparam_beginlocationname');
		var vClusterno = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');		
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/	
		
		var clusterRecordCount=request.getParameter('custparam_nooflocrecords');
		/* Up to here */ 
		var getOrderNo=request.getParameter('custparam_ebizordno');
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		//removed as on 051113 by suman to increase the performance.
		/*var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);

		nlapiLogExecution('ERROR', 'getItem', getItem);
		var getItemName = ItemRec.getFieldValue('itemid');*/
		var filter=new Array();
		if(getItemInternalId!=null&&getItemInternalId!="")
		filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

		var column=new Array();
		column[0]=new nlobjSearchColumn("itemid");
		column[1]=new nlobjSearchColumn("description");
		var Itemdescription='';
		var getItemName="";
		var searchres=nlapiSearchRecord("item",null,filter,column);
		if(searchres!=null&&searchres!="")
		{
			getItemName=searchres[0].getValue("itemid");
			Itemdescription=searchres[0].getValue("description");
		}
		//end of code as on 051113
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		nlapiLogExecution('DEBUG', 'getnextExpectedQuantity', getnextExpectedQuantity);
		var pickType=request.getParameter('custparam_picktype');
		nlapiLogExecution('DEBUG', 'getItem', getItemName);
		nlapiLogExecution('DEBUG', 'Next Location', NextLocation);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');

		//Code added on 20July 2012
		var LotRequired=GetSystemRuleForLotRequired();
		//End of code as of 20July
		var itemstatus=request.getParameter('custparam_itemstatus');
		var itemType=request.getParameter('custparam_itemType');
		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getFetchedBeginLocation);

		//  var getFetchedLocation = BinLocationRec.getFieldValue('custrecord_ebizlocname');
		//   nlapiLogExecution('DEBUG', 'Location Name is', getFetchedLocation);
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enterbatch').focus();"; 

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + vBatchno + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationname' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterno + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value='" + vBatchno + "'>";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnbulkpickflag' value=" + Bulkpickflag + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneId + ">";
		html = html + "				<input type='hidden' name='hdnZoneNo' value='" + getZoneNo + "'>";// Case# 20148058
		/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/
		html = html + "				<input type='hidden' name='hdnclusterRecCount' value=" + clusterRecordCount + ">";
		/* Up to here */ 
		html = html + "				<input type='hidden' name='hdnnext' value='" + NextLocation + "'>";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value='" + getEnteredLocation + "'>";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnitemstatus' value=" + itemstatus + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";	
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnitemStatus' value=" + itemstatus + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>ENTER/SCAN BATCH/LOT# ";
//		html = html + "				</td>";
//		html = html + "			</tr>";
		if(LotRequired=='Y')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='venterbatch' id='venterbatch' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
//		html = html + "				<td align = 'left'><input name='enterbatch' type='text'/>";
		html = html + "				<td align = 'left'><input type='hidden' name='enterbatch' value='" + vBatchno+"'>";
		html = html + "				<td align = 'left'><input type='hidden' name='lotrequired' value=" + LotRequired+">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st2 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>"+ st2 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st3 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		if(LotRequired=='Y')
		{
		html = html + "					"+ st4 +" <input name='cmdOverride' type='submit' value='F11'/>";
		}
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('venterbatch').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		var lotrequired=request.getParameter('lotrequired');
		if(lotrequired=='Y')
		{
			var vgetEnteredbatch = request.getParameter('venterbatch');
			nlapiLogExecution('DEBUG', 'Entered Item', getEnteredbatch);
		}

		else
			var vgetEnteredbatch = request.getParameter('enterbatch');

		nlapiLogExecution('DEBUG', 'vgetEnteredbatch', vgetEnteredbatch);

		var getEnteredbatch = request.getParameter('enterbatch');
		nlapiLogExecution('DEBUG', 'Entered Item', getEnteredbatch);
		var getWaveNo = request.getParameter('hdnWaveNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getBeginLocationname = request.getParameter('hdnBeginLocationname');
		var getItem = request.getParameter('hdnItem');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		var getItem = request.getParameter('hdnItemName');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');	
		var whLocation = request.getParameter('hdnwhlocation');
		//SOarray["custparam_whlocation"] = whLocation;
		var whCompany = request.getParameter('hdnwhCompany');
		var RecCount=request.getParameter('hdnRecCount');
		var getNext=request.getParameter('hdnnext');
		var OrdName=request.getParameter('hdnName');
		var ContainerSize=request.getParameter('hdnContainerSize');
		var ebizOrdNo=request.getParameter('hdnebizOrdNo');
		var ItemStatus=request.getParameter('hdnitemstatus');
		var ItemType=request.getParameter('hdnitemtype');
		var SOarray = new Array();
		var getLanguage =  request.getParameter('hdngetLanguage');
		var Bulkpickflag=request.getParameter('hdnbulkpickflag'); 
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);


		var st5;

		if( getLanguage == 'es_ES')
		{

			st5 = "LOT # NO V�LIDO";

		}
		else
		{	
			st5 = "INVALID LOT#";
		}

		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_whlocation"] = whLocation; 
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optoverride = request.getParameter('cmdOverride');

		// This variable is to hold the SO# entered.


		SOarray["custparam_error"] = st5;//'INVALID LOT#';
		SOarray["custparam_screenno"] = '14PickingBatch';
		var vZoneNo=request.getParameter('hdnZoneNo');
		var vZoneId=request.getParameter('hdnebizzoneno');
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  vZoneId;
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
		SOarray["custparam_zoneno"] =  vZoneNo;
		
		SOarray["custparam_waveno"] = getWaveNo;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_beginLocation"] = getBeginLocation;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_dolineid"] = getDOLineId;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_batchno"] = getEnteredbatch;	//vBatchNo;
		SOarray["custparam_Actbatchno"] = vBatchNo;
		SOarray["custparam_Expbatchno"] = vgetEnteredbatch;
		SOarray["custparam_noofrecords"] = RecCount;
		SOarray["custparam_nextlocation"] = getNext;
		SOarray["name"] = OrdName;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = ebizOrdNo;
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_itemstatus"] = ItemStatus;
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_beginlocationname"] = getBeginLocationname;
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnextExpectedQuantity');
		/* The below code is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/
		SOarray["custparam_nooflocrecords"]=request.getParameter('hdnclusterRecCount');
		SOarray["custparam_bulkpickflag"] = Bulkpickflag;
		/* Up to here */ 
		var pickType=request.getParameter('hdnpicktype');
		nlapiLogExecution('DEBUG', 'Order LineNo is', getOrderLineNo);
		nlapiLogExecution('DEBUG', 'pickType', pickType);
		var ItemStatus =request.getParameter('hdnitemStatus');
		nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);

		var getBeginLocIntrId=request.getParameter('hdnEndLocInternalId');
		// Fetch the actual location based on the begin location value that is fetched and passed as a parameter
		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getBeginLocation);

		// getBeginBinLocation = BinLocationRec.getFieldTx('custrecord_ebizlocname');
		//  nlapiLogExecution('DEBUG', 'Location Name is', getBeginBinLocation);
		/*       
        getBeginLocationInternalId = BinLocationRec.getId();
        nlapiLogExecution('DEBUG', 'Begin Location Internal Id', getBeginLocationInternalId);
		 */        
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
			if(Bulkpickflag=="Y")
			{
				SOarray["custparam_bulkpickflag"] = Bulkpickflag;
				response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_item', 'customdeploy_rf_bulkpick_item', false, SOarray);

			}
			else if(pickType !='CL'){//for order picking
				//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
				//response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
				response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);
			}
			else
			{ // for cluster picking
				response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
			}
		}
		else {
			
			nlapiLogExecution('DEBUG', 'vgetEnteredbatch123', vgetEnteredbatch);
			nlapiLogExecution('DEBUG', 'optoverride', optoverride);
			
			if((vgetEnteredbatch==null || vgetEnteredbatch == '') &&  optoverride!='F11' )
			{
				nlapiLogExecution('DEBUG', 'vgetEnteredbatch1234', vgetEnteredbatch);
				SOarray["custparam_error"] = 'PLEASE ENTER LOT#';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;

			}
			
			if(vgetEnteredbatch!=null&&vgetEnteredbatch!="")
			{
				if(vgetEnteredbatch!=getEnteredbatch)
				{
					var result=GetBatchId(getItemInternalId,vgetEnteredbatch);
					var BatchId=result[0];
					var IsValidflag=result[1];
					if(IsValidflag=='T')
					{
						var result=IsBatchNoValid(SOarray["custparam_endlocinternalid"],getItemInternalId,getExpectedQuantity,ItemStatus,BatchId);
						if(result.length>0){
							var IsValidBatchForBinLoc=result[0];
							var NewInvtRecId=result[1];
							var NewLp=result[2];
							var NewPackcode=result[3];
							var NewItemStatus=result[4];

							nlapiLogExecution('DEBUG', 'IsValidBatchForBinLoc', IsValidBatchForBinLoc);
							nlapiLogExecution('DEBUG', 'NewInvtRecId', NewInvtRecId);
							nlapiLogExecution('DEBUG', 'NewItemStatus', NewItemStatus);

							if(IsValidBatchForBinLoc=='T')
							{


								if(pickType =='CL')
								{
									//for cluster picking
									nlapiLogExecution('DEBUG', 'Cluster picking', 'Cluster Picking');
									nlapiLogExecution('DEBUG', 'vClusterNo', vClusterNo);
									nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
									nlapiLogExecution('DEBUG', 'getBeginLocIntrId', getBeginLocIntrId);

									var SOFilters = new Array();

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK


									if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
									}

									if(getItemInternalId != null && getItemInternalId != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
									}

									if(getBeginLocIntrId != null && getBeginLocIntrId != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', getBeginLocIntrId));
									}

									var SOColumns = new Array();				
									SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
									SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
									SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
									SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc');
									SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');					
									SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location');
									SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id');
									SOColumns[7] = new nlobjSearchColumn('custrecord_ebizmethod_no');
									SOColumns[8] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
									SOColumns[0].setSort();//SKU
									SOColumns[2].setSort();//Location

									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									if (SOSearchResults != null && SOSearchResults !='')
									{
										if(SOSearchResults.length>0)
										{
											nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length);
											for (var i=0; i< SOSearchResults.length;i++)
											{
												var getRecordId =SOSearchResults[i].getId();
												nlapiLogExecution('DEBUG', 'getRecordId', getRecordId);
												var UpdateOpentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordId);
												UpdateOpentask.setFieldValue('custrecord_batch_no', vgetEnteredbatch);
												UpdateOpentask.setFieldValue('custrecord_lpno', NewLp);
												//UpdateOpentask.setFieldValue('custrecord_packcode', NewPackcode);
												//UpdateOpentask.setFieldValue('custrecord_sku_status', NewItemStatus);
												UpdateOpentask.setFieldValue('custrecord_invref_no',NewInvtRecId); 
												nlapiSubmitRecord(UpdateOpentask, false, true);

											}

										}

										//deallocate old lot# allocated Qty in create inventory					
										var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',getInvoiceRefNo);
										var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
										var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
										var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
										var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
										var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

										nlapiLogExecution('DEBUG', 'qty', qty);
										nlapiLogExecution('DEBUG', 'allocqty', allocqty);
										nlapiLogExecution('DEBUG', 'getExpectedQuantity', getExpectedQuantity);
										transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(getExpectedQuantity)));
										nlapiSubmitRecord(transaction, false, true);


										//update allocated qty to new Lot# in create inventory
										var InvtDetails = nlapiLoadRecord('customrecord_ebiznet_createinv', NewInvtRecId);
										var Invtqty = InvtDetails.getFieldValue('custrecord_ebiz_qoh');
										var Invtallocqty = InvtDetails.getFieldValue('custrecord_ebiz_alloc_qty');
										var InvtvLP=InvtDetails.getFieldValue('custrecord_ebiz_inv_lp');
										var InvtvPackcode=InvtDetails.getFieldValue('custrecord_ebiz_inv_packcode');
										if(Invtallocqty == null || Invtallocqty =='')
											Invtallocqty=0;

										InvtDetails.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invtallocqty)+ parseFloat(getExpectedQuantity)));
										nlapiSubmitRecord(InvtDetails, false, true);
									}


									SOarray["custparam_prevscreen"] = request.getParameter('custparam_prevscreen');
									response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
									nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');		
								}
								else
								{


									//for order picking

									nlapiLogExecution('DEBUG', 'Wave picking', 'Wave Picking');
									//update open task with new lot# and Invtrefno
									var UpdateOpentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
									UpdateOpentask.setFieldValue('custrecord_batch_no', vgetEnteredbatch);
									UpdateOpentask.setFieldValue('custrecord_lpno', NewLp);
									//UpdateOpentask.setFieldValue('custrecord_packcode', NewPackcode);
									//UpdateOpentask.setFieldValue('custrecord_sku_status', NewItemStatus);
									UpdateOpentask.setFieldValue('custrecord_invref_no',NewInvtRecId); 
									nlapiSubmitRecord(UpdateOpentask, false, true);

									//deallocate old lot# allocated Qty in create inventory					
									var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',getInvoiceRefNo);
									var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
									var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
									var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
									var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
									var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

									nlapiLogExecution('DEBUG', 'qty', qty);
									nlapiLogExecution('DEBUG', 'allocqty', allocqty);
									nlapiLogExecution('DEBUG', 'getExpectedQuantity', getExpectedQuantity);
									transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(getExpectedQuantity)));
									nlapiSubmitRecord(transaction, false, true);


									//update allocated qty to new Lot# in create inventory
									var InvtDetails = nlapiLoadRecord('customrecord_ebiznet_createinv', NewInvtRecId);
									var Invtqty = InvtDetails.getFieldValue('custrecord_ebiz_qoh');
									var Invtallocqty = InvtDetails.getFieldValue('custrecord_ebiz_alloc_qty');
									var InvtvLP=InvtDetails.getFieldValue('custrecord_ebiz_inv_lp');
									var InvtvPackcode=InvtDetails.getFieldValue('custrecord_ebiz_inv_packcode');

									InvtDetails.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invtallocqty)+ parseFloat(getExpectedQuantity)));
									nlapiSubmitRecord(InvtDetails, false, true);


									SOarray["custparam_batchno"] = vgetEnteredbatch;
									//SOarray["custparam_entloc"] = getBeginLocation;
									SOarray["custparam_recid"] = NewInvtRecId;
									//SOarray["custparam_entlocid"] = SOarray["custparam_endlocinternalid"]; 
									SOarray["custparam_Exc"] = 'L';
									//response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
									//nlapiLogExecution('DEBUG', 'Done customrecord1', 'Success');
									
									if(Bulkpickflag=="Y")
									{
										SOarray["custparam_bulkpickflag"] = Bulkpickflag;
										response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_confirm', 'customdeploy_rf_bulkpick_confirm_di', false, SOarray);
										nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
										return;
									}



									response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
									nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');	
								}

							}
							else
							{
								SOarray["custparam_error"] = 'INVENTORY IS NOT AVAILABLE FOR THE LOT#';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('DEBUG', 'Error: ', 'Does not belong to the bin location');
							}

						}
						else
						{
							SOarray["custparam_error"] = 'INVENTORY IS NOT AVAILABLE FOR THE LOT#';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						}
					}
					else 
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Scanned batch is invalid');
					}
				}
				else if(Bulkpickflag=="Y")
				{
					SOarray["custparam_bulkpickflag"] = Bulkpickflag;
					response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_confirm', 'customdeploy_rf_bulkpick_confirm_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
				else if(pickType =='CL')
				{
					//for cluster picking
					SOarray["custparam_prevscreen"] = request.getParameter('custparam_prevscreen');
					response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');		
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');		
				}
			}
			//if (getEnteredLocation != '' && getEnteredLocation == getBeginBinLocation) {
			else{			// && getEnteredbatch == vBatchNo) {
				if(Bulkpickflag=="Y")
				{
					SOarray["custparam_bulkpickflag"] = Bulkpickflag;
					response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_confirm', 'customdeploy_rf_bulkpick_confirm_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
			       else if(pickType =='CL')
				{
					//for cluster picking
					SOarray["custparam_prevscreen"] = request.getParameter('custparam_prevscreen');
					response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');		
				}
				else
				{
				response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');				
				}
			}
		}
	}
}



function GetSystemRuleForLotRequired()
{
	try
	{
		var rulevalue='N';
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is Lot# Scan required in RF Picking?'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null&&searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('DEBUG','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetSystemRuleForLPRequired',exp);
	}
}

function GetBatchId(itemid,getEnteredbatch)
{
	try
	{
		var globalArray=new Array();
		var BatchID;
		var flag='F';
		var filter=new Array();
		if(itemid!=null&&itemid!="")
			filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid));
		filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',getEnteredbatch));
		var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filter,null);
		if(rec!=null&&rec!="")
		{
			BatchID=rec[0].getId();
			flag='T';
		}
		globalArray.push(BatchID);
		globalArray.push(flag);
		nlapiLogExecution('DEBUG','globalArray',globalArray);
		return globalArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetBatchId',exp);
	}
}


function IsBatchNoValid(BeginLocationId,ItemId,getExpectedQuantity,getItemStatusId,BatchId)
{
	try
	{
		nlapiLogExecution('DEBUG','BeginLocationId',BeginLocationId);
		nlapiLogExecution('DEBUG','ItemId',ItemId);
		nlapiLogExecution('DEBUG','getExpectedQuantity',getExpectedQuantity);
		nlapiLogExecution('DEBUG','getItemStatusId',getItemStatusId);
		nlapiLogExecution('DEBUG','BatchId',BatchId);

		var summaryArray=new Array();
		var tempflag='F';
		var recid="";
		var Lp="";
		var ItemStatus="";
		var Packcode="";
		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null,'anyof',BeginLocationId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null,'anyof',ItemId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot',null,'anyof',BatchId));

//		if(getItemStatusId !=null && getItemStatusId !=''){
//		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof',getItemStatusId));
//		}

		filter.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty',null, 'greaterthan', getExpectedQuantity));
		filter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));

		var Columns = new Array();
		Columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		Columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
		Columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');

		var searchrecord=nlapiSearchRecord('customrecord_ebiznet_createinv',null,filter,Columns);

		if(searchrecord!=null&&searchrecord!="")
		{
			tempflag='T';
			recid=searchrecord[0].getId();
			Lp=searchrecord[0].getValue('custrecord_ebiz_inv_lp');
			ItemStatus=searchrecord[0].getValue('custrecord_ebiz_inv_sku_status');
			Packcode=searchrecord[0].getValue('custrecord_ebiz_inv_packcode');
		}
		summaryArray.push(tempflag);
		summaryArray.push(recid);
		summaryArray.push(Lp);
		summaryArray.push(Packcode);
		summaryArray.push(ItemStatus);
		nlapiLogExecution('DEBUG','summaryArray',summaryArray);
		return summaryArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in IsBatchNoValid',exp);
	}
}
