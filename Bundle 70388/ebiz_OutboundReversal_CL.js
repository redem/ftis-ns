/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/Attic/ebiz_OutboundReversal_CL.js,v $
 *     	   $Revision: 1.1.4.2.4.2 $
 *     	   $Date: 2015/07/30 13:27:18 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_OutboundReversal_CL.js,v $
 * Revision 1.1.4.2.4.2  2015/07/30 13:27:18  schepuri
 * case# 201413685
 *
 * Revision 1.1.4.2.4.1  2014/09/16 16:21:46  sponnaganti
 * Case# 201410402
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.4.2  2012/10/07 23:06:22  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 *
 *****************************************************************************/
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function ValidateLine(type, name)
{	
	//alert('hi');
	//alert(name);

	if(trim(name)==trim('custpage_selectpage'))
	{
		nlapiSetFieldValue('custpage_flag','paging');
		var tempflag= nlapiGetFieldValue('custpage_flag');
		NLDoMainFormButtonAction("submitter",true);	
		//nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
		//NLDoMainFormButtonAction("submitter",true);	
	}
	else
	{
		return true;
	}
}
function OnSave(type, name)
{
	var lineCnt = nlapiGetLineItemCount('custpage_items');
	var tempflag= nlapiGetFieldValue('custpage_flag');
	//nlapiSetFieldValue('custpage_flag','');
	var isselected=false;
	if(tempflag != 'paging')
	{
		for(var j=1;j<=lineCnt;j++)
		{
			var chkselect=nlapiGetLineItemValue('custpage_items','custpage_so', j);
			if(chkselect=='T')
			{
				isselected=true;

//				case# 201413685
				var vkititem=nlapiGetLineItemValue('custpage_items', 'custpage_kititem',j);
				var vkititemtext=nlapiGetLineItemValue('custpage_items', 'custpage_kititemtext',j);
				var memeritem=nlapiGetLineItemValue('custpage_items', 'custpage_item',j);
				var fono = nlapiGetLineItemValue('custpage_items', 'custpage_fono',j);
				//var Orderno = nlapiGetLineItemValue('custpage_items', 'custpage_waveordernovalue',j);
				var memerItemvalue = nlapiGetLineItemValue('custpage_items', 'custpage_itemintrid',j);

				//alert(vkititemtext);
				if(vkititem!=null && vkititem!='')
				{

//					var kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');
					var kititemTypesku=nlapiGetLineItemValue('custpage_items','custpage_kititemtype',j);

					if(kititemTypesku=='Kit')
					{

						var memberItemsCount=0;
						var filters = new Array(); 			 
						filters[0] = new nlobjSearchFilter('internalid', null, 'is', vkititem);	

						var columns1 = new Array(); 
						columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
						columns1[1] = new nlobjSearchColumn( 'memberquantity' );

						var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 
						if(searchresults!=null && searchresults!='' && searchresults.length>0)
						{
							memberItemsCount=searchresults.length;
						}



						var vcount=0;
						for (var t = 1; t <= lineCnt; t++) 
						{
							var vinnkititem=nlapiGetLineItemValue('custpage_items', 'custpage_kititem',t);
							var vinnfono = nlapiGetLineItemValue('custpage_items', 'custpage_fono',t);
							var vinnlinecheck= nlapiGetLineItemValue('custpage_items','custpage_so',t);
							//alert('vinnkititem'+vinnkititem);
							//alert('vkititem ::'+vkititem);

							//alert('fono'+fono);
							//alert('vinnfono ::'+vinnfono);

							if(vinnkititem==vkititem && fono==vinnfono)
							{
								if(vinnlinecheck=='F')
								{
									alert("Select All Component Items in Kit Item:"+vkititemtext);
									return false
								}
								else
								{
									vcount=vcount+1;
								}

							}
						}
						var componentitempicks=0;
						var vcomponentpicks = getcomponentpicks(vkititem,fono);
						if(vcomponentpicks!=null && vcomponentpicks!='')
						{
							componentitempicks=vcomponentpicks.length;
						}

						//alert("memberItemsCount: "+memberItemsCount);
						//alert("componentitempicks: "+componentitempicks);

						if(parseInt(memberItemsCount) != parseInt(componentitempicks))
						{
							//alert('vcount1 ::'+vcount);
							alert("Cann't Cancel Partial Component Items in Kit Item:"+vkititemtext);
							return false;
						}


					}
				}

			}
		}


		if(isselected==false)
		{
			alert('Please select atleast one line');
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}

}

function getcomponentpicks(parentitem,fono)
{
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', [29]))
	SOFilters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null, 'is', parentitem));
	SOFilters.push(new nlobjSearchFilter('name', null, 'is', fono));

	//201413306
	SOColumns.push(new nlobjSearchColumn('custrecord_sku',null,'group'));

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	return SOSearchResults;

}
