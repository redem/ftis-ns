/***************************************************************************
	  		   eBizNET Solutions Inc
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_TOPutawayForm_SL.js,v $
 *     	   $Revision: 1.1.4.1.4.1.10.2 $
 *     	   $Date: 2015/07/30 15:46:46 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * DESCRIPTION
 * REVISION HISTORY
 * $Log: ebiz_TOPutawayForm_SL.js,v $
 * Revision 1.1.4.1.4.1.10.2  2015/07/30 15:46:46  nneelam
 * case# 201413649
 *
 * Revision 1.1.4.1.4.1.10.1  2015/04/27 13:26:16  schepuri
 * case# 201412499
 *
 * Revision 1.1.4.1.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.4.1  2012/04/20 13:17:37  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.1  2011/09/28 11:40:05  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.1  2011/09/22 08:36:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 *
 *****************************************************************************/
function TOAssignPutAway(request, response){
	if (request.getMethod() == 'GET') {
		try {
			var getPOid = request.getParameter('custparam_po');
			nlapiLogExecution('ERROR', 'getPOid', getPOid);
			var getPoValue = request.getParameter('custparam_povalue');
			var getttype = request.getParameter('custparam_ttype');

			nlapiLogExecution('ERROR', 'getPoValue', getPoValue);
			nlapiLogExecution('ERROR', 'getttype', getttype);

			var form = nlapiCreateForm('TransferOrder Putaway Report');

			form.setScript('customscript_assignputaway');
			// case# 201412499
			//form.addButton('custpage_print', 'Print', 'Printreport(' + getPOid + ',' + getPoValue + ')');
			form.addButton('custpage_print', 'Print',"Printreport('"+getPOid+"','"+getPoValue+"')");

			var selectReceive = form.addField('custpage_selectreceive', 'select', 'Receive Mode').setLayoutType('startrow');

			selectReceive.addSelectOption('T', 'TO');


			var selectRType = form.addField('custpage_receive_type', 'select', 'Receipt Type').setLayoutType('midrow');

			selectRType.addSelectOption('TO', 'Transfer Order');


			/* 
            //Get distinct po's and its IDs into dropdown  
            var selectPO = form.addField('custpage_po', 'select', 'PO #').setLayoutType('endrow');
            var filterspo = new Array();
            filterspo[0] = new nlobjSearchFilter('mainline', null, 'is', 'T');
            var searchresults = nlapiSearchRecord('purchaseorder', null, filterspo, new nlobjSearchColumn('tranid'));
            for (var i = 0; i < Math.min(500, searchresults.length); i++) {

                selectPO.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('tranid'));
            }            
			 */

			var selectPO = form.addField('custpage_po', 'text', 'TO #').setDisplayType('disabled');            
			selectPO.setDefaultValue(getPoValue);

			/*
             Defining SubList lines
			 */
			var sublist = form.addSubList("custpage_items", "list", "ItemList");
			sublist.addField("custpage_po_asn", "text", "TO#");
			sublist.addField("custpage_line", "text", "Line#");
			sublist.addField("custpage_itemname", "text", "Item");
			sublist.addField("custpage_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDisplayType('inline');
			/*
  Commented on 03/31/2011 by Phani as Task Type is not required to be displayed on the report

            sublist.addField("custpage_taskstatus", "text", "Task Status");
			 */
			sublist.addField("custpage_uom", "text", "UOM");
			sublist.addField("custpage_quantity", "text", "Quantity");
			sublist.addField("custpage_lp", "text", "LP#");
			sublist.addField("custpage_transport_lp", "text", "Cart LP #");
			sublist.addField("custpage_serialno", "textarea", "Serial #");
			sublist.addField("custpage_seriallot", "text", "LOT#");
			sublist.addField("custpage_location", "select", "Bin Location", "customrecord_ebiznet_location").setDisplayType('inline');
			sublist.addField("custpage_poid", "text", "PO ID").setDisplayType('hidden');
			sublist.addField("custpage_ttype", "text", "Task Type").setDisplayType('hidden');


			var autobutton = form.addSubmitButton('Confirm Putaway');

			//LN,Serial # were comma separated here...!
			
			//for (var i = 0; i < arrayLP.length; i++) 
				//nlapiLogExecution('ERROR', 'arrayLP - [LP#|Serial#]', '[' + arrayLP[i][0] + '|' + arrayLP[i][1] + ']');

			// define search filters
			var filters = new Array();
			//var status = new Array("N","L");
			filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', getPOid);
			filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
			// filters[2] = new nlobjSearchFilter('custrecord_status_flag', null, 'is', 'L');
			filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);


			//filters[2] = new nlobjSearchFilter('custrecord_actendloc', null, 'is', isNaN);
			//filters[2] = new nlobjSearchFilter('custrecord_actendloc', null, 'noneof', '@NONE@');
			//filters[3] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

			// return opportunity sales rep, customer custom field, and customer ID
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			columns[1] = new nlobjSearchColumn('custrecord_line_no');
			columns[2] = new nlobjSearchColumn('custrecord_sku');
			columns[3] = new nlobjSearchColumn('custrecord_sku_status');
			columns[4] = new nlobjSearchColumn('custrecord_tasktype');
			columns[5] = new nlobjSearchColumn('custrecord_uom_id');
			columns[6] = new nlobjSearchColumn('custrecord_expe_qty');
			//columns[7] = new nlobjSearchColumn('custrecord_ebiz_lpno');custrecord_lpno
			columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[9] = new nlobjSearchColumn('custrecord_batch_no');
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[11] = new nlobjSearchColumn('custrecord_transport_lp');
			columns[1].setSort();


			// execute the  search, passing null filter and return columns
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			var po_asn, line, itemname, itemstatus, taskstatus, uom, qty, lp, loc, seriallot, retserialcsv, itemintid,cartlp;

			/*
             Searching records from custom record and dynamically adding to the sublist lines
			 */
			for (var i = 0; searchresults != null && i < searchresults.length; i++) {
				var searchresult = searchresults[i];

				po_asn = searchresult.getValue('custrecord_ebiz_cntrl_no');
				line = searchresult.getValue('custrecord_line_no');
				itemname = searchresult.getText('custrecord_sku');
				itemstatus = searchresult.getValue('custrecord_sku_status');
				taskstatus = searchresult.getText('custrecord_tasktype');
				uom = searchresult.getValue('custrecord_uom_id');
				qty = searchresult.getValue('custrecord_expe_qty');
				//lp = searchresult.getValue('custrecord_ebiz_lpno');
				lp = searchresult.getValue('custrecord_lpno');       
				cartlp = searchresult.getValue('custrecord_transport_lp');    
				loc = searchresult.getValue('custrecord_actbeginloc');
				seriallot = searchresult.getValue('custrecord_batch_no');
				itemintid = searchresult.getValue('custrecord_ebiz_sku_no');
				nlapiLogExecution('ERROR', 'seriallot', seriallot);
				nlapiLogExecution('ERROR', 'lp', lp);
				var serialInflg = "F";
				var batchflg = "F";
				var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
				var columns = nlapiLookupField('item', itemintid, fields);
				var ItemType = columns.recordType;
				serialInflg = columns.custitem_ebizserialin;
				batchflg = columns.custitem_ebizbatchlot;


				form.getSubList('custpage_items').setLineItemValue('custpage_po_asn', i + 1, getPoValue);
				form.getSubList('custpage_items').setLineItemValue('custpage_line', i + 1, line);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, itemname);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, itemstatus);
				/*
Commented on 03/31/2011 by Phani as Task Type is not required to be displayed on the report

                form.getSubList('custpage_items').setLineItemValue('custpage_taskstatus', i + 1, taskstatus);
				 */

				form.getSubList('custpage_items').setLineItemValue('custpage_uom', i + 1, uom);
				form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, qty);
				
			

				nlapiLogExecution('DEBUG', 'ItemType', ItemType);
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
					var arrayLP = new Array();
					var lpfilter = new Array();
					lpfilter[0] = new nlobjSearchFilter('custrecord_serialpono', null, 'is', getPoValue);
					lpfilter[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
					var lpcolumns = new Array();
					lpcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
					lpcolumns[1] = new nlobjSearchColumn('custrecord_serialnumber');
					var lpsearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, lpfilter, lpcolumns);
					for (var v1= 0; lpsearchresults != null && v1 < lpsearchresults.length; v1++) {
						var SerialLP = new Array();
						SerialLP[0] = lpsearchresults[v1].getValue('custrecord_serialparentid');
						nlapiLogExecution('DEBUG', 'SerialLP[0]', SerialLP[0]);
						SerialLP[1] = lpsearchresults[v1].getValue('custrecord_serialnumber');
						nlapiLogExecution('DEBUG', 'SerialLP[1]', SerialLP[1]);
						arrayLP.push(SerialLP);

					}
				
					nlapiLogExecution('DEBUG', 'into getSerialNoCSV');
					retserialcsv = getSerialNoCSV(arrayLP, lp);
					nlapiLogExecution('DEBUG', 'retserialcsv', retserialcsv);
					form.getSubList('custpage_items').setLineItemValue('custpage_serialno', i + 1, retserialcsv);
				}
				form.getSubList('custpage_items').setLineItemValue('custpage_lp', i + 1, lp);
				form.getSubList('custpage_items').setLineItemValue('custpage_transport_lp', i + 1, cartlp);
				form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, loc);
				form.getSubList('custpage_items').setLineItemValue('custpage_poid', i + 1, getPOid);
				form.getSubList('custpage_items').setLineItemValue('custpage_ttype', i + 1, getttype);

				//  form.getSubList('custpage_items').setLineItemValue('custpage_seriallot', i + 1, seriallot);
				if (ItemType == "lotnumberedinventoryitem" || batchflg == "T") {
					form.getSubList('custpage_items').setLineItemValue('custpage_seriallot', i + 1, seriallot);
				}
				nlapiLogExecution('ERROR', 'POInternalId in Request Object', getPOid);


			}
			//form.addPageLink('breadcrumb', 'Back to PO Check-In', nlapiResolveURL('RECORD', 'purchaseorder',getPOid))
			form.addPageLink('crosslink', 'Back to TransferOrder Check-In', nlapiResolveURL('RECORD', 'transferorder', getPOid));
			response.writePage(form);
		} 
		catch (expss) {
			nlapiLogExecution('ERROR', 'Request Error', expss);
		}


	}
	else {
		nlapiLogExecution('ERROR', 'Into Response of assing Putaway', 'Assign Putaway');
		var POInternalId = request.getLineItemValue('custpage_items', 'custpage_poid', 1);
		var ttype = request.getLineItemValue('custpage_items', 'custpage_ttype', 1);
		var POarrayAP = new Array();
		POarrayAP["custparam_poid"] = POInternalId;
		// POarrayAP["custparam_ttype"] = ttype;
		nlapiLogExecution('ERROR', 'POInternalId', POInternalId);
		var i = -1;
		var arrQty = new Array();
		var arrLine = new Array();
		// To Consolidate Quantity and Send to TRN table.
		try {
			var lineCnt = request.getLineItemCount('custpage_items');
			nlapiLogExecution('ERROR', 'lineCnt', lineCnt);
			var Lineno;
			var TempLineno;
			var boolFlag = false;
			var Qty = 0;
			var stempLine = 0;

			for (var s = 1; s <= lineCnt + 1; s++) {
				if (s == lineCnt + 1) {
					i++;
					arrQty[i] = Qty;
					arrLine[i] = Lineno;
					nlapiLogExecution('ERROR', 'Value of i and Qty if s is cnt', arrQty[i] + 'vale of i ' + i + 'In last case' + Lineno);
					break;
				}
				Lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
				nlapiLogExecution('ERROR', 'Lineno', Lineno);
				if (s == 1) {
					TempLineno = Lineno;
				}
				if (TempLineno == Lineno) {
					if (boolFlag == false) {
						boolFlag = true;
						Qty = parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
						nlapiLogExecution('ERROR', 'Qty1', Qty);
					}
					else {
						Qty += parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
						nlapiLogExecution('ERROR', 'Qty2', Qty);
					}
				}
				else {
					i = i + 1;

					nlapiLogExecution('ERROR', 'Value of i and Qty in else', Qty + 'vale of i ' + i);
					arrQty[i] = Qty;
					arrLine[i] = TempLineno;
					Qty = 0; //make it null for further loop.
					boolFlag = true;
					Qty = parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
					Lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
					TempLineno = Lineno;
				}

			}
		} 
		catch (exp) {
			nlapiLogExecution('ERROR', 'Err in summing Qty', exp);
		}
		//To get tran type for a record.
		nlapiLogExecution('ERROR', 'POInternalId', POInternalId);
		var tranType = nlapiLookupField('transaction', POInternalId, 'recordType');
		nlapiLogExecution('ERROR', 'tranType', tranType);
		//Transaction Line Updation.
		var type = tranType;
		var putgenqty = 0;
		var putconfqty = 0;
		// var ttype = "ASPW";
		if(ttype == "ASPW")
		{
			for (var p = 0; p < arrLine.length; p++) {
				// var type = "purchaseorder";

				var confqty = arrQty[p]; //checkinLineQty;
				var lineno = arrLine[p];
				///nlapiLogExecution('ERROR', '-->quan', quan);
				nlapiLogExecution('ERROR', 'confqty', confqty);
				//TrnLineUpdation(orderType, ttype, poValue, poid, lineno, ItemId, quan, confqty,chkAssignputW,ItemStatus){
				var tranValue = TrnLineUpdation(type, ttype, POInternalId, POInternalId, lineno, null, null, confqty, null, 1);
			}
		}
		nlapiLogExecution('ERROR', 'test', 'test');


		if(tranType =='transferorder')
		{
			response.sendRedirect('SUITELET', 'customscript_to_confirmputaway_sl', 'customdeploy_to_confirmputaway_dl', false, POarrayAP);
		}


	}
}

function getSerialNoCSV(arrayLP, lp){
	var csv = "";
	for (var i = 0; i < arrayLP.length; i++) {
		if (arrayLP[i][0] == lp) {
			csv += arrayLP[i][1] + " , ";
		}
	}
	return csv;
}

function Printreport(poid, povalue){
	nlapiLogExecution('ERROR', 'poid', poid);
	nlapiLogExecution('ERROR', 'povalue', povalue);
	//var linkURL = nlapiResolveURL('SUITELET', 'customscript_putw_report_pdf', 'customdeploy_putw_report_pdf');
	var linkURL = nlapiResolveURL('SUITELET', 'customscript_putw_report_pdf', 'customdeploy_putw_report_pdf_di');
	linkURL = linkURL + '&custparam_po=' + poid;
	linkURL = linkURL + '&custparam_povalue=' + povalue;
	window.open(linkURL);

}
