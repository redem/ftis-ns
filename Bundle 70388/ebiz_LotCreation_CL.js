
	
	
	function OnSave()
	{
		window.opener.focus();
		var linecount = nlapiGetLineItemCount('custpage_items');
		var lineno = nlapiGetLineItemValue('custpage_items','custpage_lineno',1);
		var actqty = nlapiGetLineItemValue('custpage_items','custpage_recqty',1);
		var finalbatch="";
		var totalqty=0;
		for(var count=1;count<=linecount;count++)
		{
			var lot = nlapiGetLineItemValue('custpage_items','custpage_lot',count);
			var qty = nlapiGetLineItemValue('custpage_items','custpage_qty',count);
			if(count==1)
				finalbatch=lot+"("+qty+")";
			else
				finalbatch=finalbatch+","+lot+"("+qty+")";
			totalqty=parseInt(totalqty)+parseInt(qty);
		}

		if(parseInt(totalqty) > parseInt(actqty))
		{
			alert("Total Qty Entered exceeds actual Qty");
			return false;
		}
		else
		{
			for(var count=1;count<=linecount;count++)
			{
				var lot = nlapiGetLineItemValue('custpage_items','custpage_lot',count);
				var isAlreadyLotExist = nlapiGetLineItemValue('custpage_items','custpage_isalreadyexist',count);
				var expdate = nlapiGetLineItemValue('custpage_items','custpage_expdate',count);
				var fifodate = nlapiGetLineItemValue('custpage_items','custpage_fifodate',count);
				var location = nlapiGetLineItemValue('custpage_items','custpage_loc',count);
				var skuid = nlapiGetLineItemValue('custpage_items','custpage_itemid',count);
//				alert("isAlreadyLotExist"+isAlreadyLotExist);
				if(isAlreadyLotExist=="F")
				{
					var invtRec = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
					invtRec.setFieldValue('name', lot);
					invtRec.setFieldValue('custrecord_ebizlotbatch', lot);
					invtRec.setFieldValue('custrecord_ebizsitebatch', location);
					invtRec.setFieldValue('custrecord_ebizsku', skuid);
					invtRec.setFieldValue('custrecord_ebizexpirydate', expdate);
					invtRec.setFieldValue('custrecord_ebizfifodate', fifodate);
					var invtrecid = nlapiSubmitRecord(invtRec, false, true);
				}
			}
			window.opener.document.getElementById("custpage_lotbatch"+lineno).value=finalbatch;
			window.close();
		}
	}
	
	function onchange(type, name,linenum)
	{
		if(name=='custpage_lot')
		{
			var item = nlapiGetCurrentLineItemValue('custpage_items','custpage_itemid');
			var lotentered = nlapiGetCurrentLineItemValue('custpage_items','custpage_lot');
			var location = nlapiGetCurrentLineItemValue('custpage_items','custpage_loc');

			var filter=new Array();
			filter[0]=new nlobjSearchFilter("custrecord_ebizlotbatch",null,"is",lotentered);
			filter[1]=new nlobjSearchFilter("custrecord_ebizsitebatch",null,"anyof",location);
			filter[1]=new nlobjSearchFilter("custrecord_ebizsku",null,"anyof",item);


			var column=new Array();
			column[0]=new nlobjSearchColumn("custrecord_ebizexpirydate");
			column[1]=new nlobjSearchColumn("custrecord_ebizfifodate");
			var res=nlapiSearchRecord("customrecord_ebiznet_batch_entry",null,filter,column);
			if(res!=null&&res!="")
			{
				
				var expdate=res[0].getValue("custrecord_ebizexpirydate");
				var fifodate=res[0].getValue("custrecord_ebizfifodate");
				nlapiSetCurrentLineItemValue('custpage_items','custpage_expdate',expdate,false);
				nlapiSetCurrentLineItemValue('custpage_items','custpage_fifodate',fifodate,false);
				nlapiSetCurrentLineItemValue('custpage_items','custpage_isalreadyexist',"T",false);
			}
			else
				nlapiSetCurrentLineItemValue('custpage_items','custpage_isalreadyexist',"F",false);
		}
	}