/***************************************************************************
			 eBizNET
    eBizNET Solutions Inc
 ****************************************************************************
 *
 * $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/UserEvents/ebiz_ASNCConfirmation_UE.js,v $
 * $Revision: 1.12.2.22.4.1 $
 * $Date: 2012/11/01 14:54:56 $
 * $Author: schepuri $
 * $Name: t_NSWMS_2013_1_2_7 $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_ASNCConfirmation_UE.js,v $
 * Revision 1.12.2.22.4.1  2012/11/01 14:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.12.2.22  2012/09/10 14:26:58  mbpragada
 * 20120490
 * fetching duplicate records from closed task
 *
 * Revision 1.12.2.21  2012/08/09 07:30:18  mbpragada
 * 20120490
 *
 * Revision 1.12.2.13  2012/06/12 13:18:46  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.12.2.12  2012/05/23 14:16:17  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12.2.11  2012/05/16 06:41:45  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12.2.10  2012/05/15 06:04:42  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12.2.9  2012/05/09 14:35:09  mbpragada
 * CASE201112/CR201113/LOG201121
 * container data repetition,bol no missing
 *
 * Revision 1.12.2.8  2012/05/09 13:24:02  mbpragada
 * CASE201112/CR201113/LOG201121
 * container data repetition
 *
 * Revision 1.12.2.7  2012/05/08 13:05:59  mbpragada
 * CASE201112/CR201113/LOG201121
 * PRO number logic changed
 *
 * Revision 1.12.2.6  2012/05/03 06:19:03  mbpragada
 * CASE201112/CR201113/LOG201121
 * UCC numbers should be mapped to C_UCC tag in the XML
 *
 * Revision 1.12.2.5  2012/05/01 14:25:41  mbpragada
 * CASE201112/CR201113/LOG201121
 * SO weight ,cube and pickupauth number logic changed
 *
 * Revision 1.12.2.3  2012/03/30 13:35:27  mbpragada
 * CASE201112/CR201113/LOG201121
 * added seal no
 *
 * Revision 1.12.2.2  2012/03/27 14:02:26  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12.2.1  2012/03/05 13:27:40  mbpragada
 * CASE201112/CR201113/LOG201121
 * inserting bol number and so internal id
 *
 * Revision 1.17  2012/01/18 06:56:12  sallampati
 * CASE201112/CR201113/LOG201121
 * Added code to populate all the dims in stage table.
 * SCAC code related code is fine tuned
 *
 * Revision 1.15  2012/01/12 16:06:15  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.14  2012/01/12 14:35:48  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2012/01/11 14:44:38  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/12/28 14:13:10  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/12/28 12:40:17  mbpragada
 * CASE201112/CR201113/LOG201121
 * added upc code
 *
 * Revision 1.10  2011/12/20 08:27:53  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/12/19 14:40:04  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/12/12 13:16:11  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/12/12 06:30:52  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/12/07 13:32:33  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/12/06 13:41:08  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/12/05 16:05:21  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/11/30 09:40:00  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/11/10 07:11:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Added with BatchId,Container Level Qty and Ship LevelQty
 *
 * Revision 1.1  2011/11/08 06:56:38  spendyala
 * CASE201112/CR201113/LOG201121
 * created a user event which create records to make inputs for SPS configuration
 *
 *
 ****************************************************************************/
/**
 * Function will Get all the customer details. This list will be used to get the ASN Required? information.
 * @param skulist
 * @returns {Array}
 */
function GetAllCustomers()
{	
	nlapiLogExecution('ERROR', 'getAllCustomers', 'Start');	

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custentity_ebiz_asn_required', null, 'is', 'T'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('entityid');
	columns[1] = new nlobjSearchColumn('custentity_ebiz_asn_required');	
	columns[2] = new nlobjSearchColumn('altname');	
	columns[3] = new nlobjSearchColumn('internalid');	
	var results = nlapiSearchRecord('customer' , null,	filters, columns);	
	nlapiLogExecution('ERROR', 'getAllCustomers results', results.length);	
	return results
}

 function GetAllresultsfromASNTiming()
 {	
 	nlapiLogExecution('ERROR', 'GetAllresultsfromASNTiming', 'Start'); 	
 	var columns = new Array();
 	columns[0] = new nlobjSearchColumn('custrecord_ebiz_asntiming_entity');
 	columns[1] = new nlobjSearchColumn('custrecord_ebiz_asntiming_dcstore');	
 		
 	var results = nlapiSearchRecord('customrecord_ebiznet_asn_timing' , null,	null, columns);	
 	
 	nlapiLogExecution('ERROR', 'GetAllresultsfromASNTiming', results.length);	
 	return results
 }
/**
 * Function Gets all the sales order inernal id  , customer internal id 
 * @param OrderList - All distinct orders departed for the trailer.
 * @returns {Array}
 */

function GetSalesorderDetailsforASNTrigger(orderList)
{
	nlapiLogExecution('ERROR', 'GetcustmerASNreq', 'Start');	
	var salesorderDetailList = new Array();
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', orderList));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('entity');
	columns[2] = new nlobjSearchColumn('custbody_dc_store');
	columns[3] = new nlobjSearchColumn('entity');
		
	var Res = nlapiSearchRecord('salesorder', null, filters,columns);
	nlapiLogExecution('ERROR', 'GetcustmerASNreq Res', Res);	
	return Res;
}
function GetIndividualabuyerpart( SOLIst,line,so)
{
	var buyerpart = "";
	try
	{
		if (SOLIst != null && SOLIst.length>0) {
			//nlapiLogExecution('ERROR', 'SOLIst.length', SOLIst.length);
			for ( var i=0; i < SOLIst.length ; i++){
				//if (SOLIst[i]['line'] == line && SOLIst[i]['internalid'] == so){
				//nlapiLogExecution('ERROR', 'SOLIst[i].getValue(line)', SOLIst[i].getValue('line'));
				//nlapiLogExecution('ERROR', 'line', line);
				//nlapiLogExecution('ERROR', 'SOLIst[i].getId()', SOLIst[i].getId());
				//nlapiLogExecution('ERROR', 'so', so);
				if (SOLIst[i].getValue('line') == line && SOLIst[i].getId() == so){
					buyerpart = SOLIst[i].getValue('custcol13');
					i = SOLIst.length;
				}

			}
		}
	}
	catch(exception) {
		nlapiLogExecution('ERROR', 'GetIndividiualBatchID', exception);
	}
	return buyerpart;	
}
function getallbuyerpartno(orderList)
{
	nlapiLogExecution('ERROR', 'GetcustmerASNreq', 'Start');	
	var salesorderDetailList = new Array();
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', orderList));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));

	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('entity');
	columns[2] = new nlobjSearchColumn('custbody_dc_store');
	columns[3] = new nlobjSearchColumn('line');
	columns[4] = new nlobjSearchColumn('custcol13');
	
		
	var Res = nlapiSearchRecord('salesorder', null, filters,columns);
	nlapiLogExecution('ERROR', 'GetcustmerASNreq Res', Res);	
	return Res;
}
function getRealtimeOrders(OrdersEligibleForASNGeneration)
{
	nlapiLogExecution('ERROR', 'getRealtimeOrders', 'Start');	
	var salesorderDetailList = new Array();
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', OrdersEligibleForASNGeneration));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('entity');
	columns[2] = new nlobjSearchColumn('custbody_dc_store');
	columns[3] = new nlobjSearchColumn('entity');
		
	var Res = nlapiSearchRecord('salesorder', null, filters,columns);
	nlapiLogExecution('ERROR', 'getRealtimeOrders Res', Res);	
	return Res;
}
function getDistinctshiplps(shiplpList){
	nlapiLogExecution('ERROR', 'getDistinctshiplps', 'Start');
	var shiplpIDList = new Array();
	try{

		if(shiplpList != null && shiplpList.length > 0)
		{
			for(var i = 0; i < shiplpList.length; i++)
			{
				var shiplpID = shiplpList[i].getValue('custrecord_ebiz_order_no');
				if(!isDistinctShipLP(shiplpIDList, shiplpID))
					shiplpIDList.push(shiplpID);
			}
		}
		nlapiLogExecution('ERROR', 'getDistinctshiplps', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'getDistinctshiplps', exception);
	}
	nlapiLogExecution('ERROR', 'getDistinctshiplps', 'End');
	return shiplpIDList;

}
function isDistinctShipLP(shiplpIDList, shiplpID){
	var matchFound = false;
	if(shiplpIDList != null && shiplpIDList.length > 0){
		for(var i = 0; i < shiplpIDList.length; i++){
			if(shiplpIDList[i] == shiplpID)
				matchFound = true;
		}
	}

	return matchFound;
}
function getShippingDetailsForEligibleOrders(orderlist)
{
	nlapiLogExecution('ERROR', 'getShippingDetailsForEligibleOrders', 'Start');
	var noofshiplpfilter = new Array();
	var noofshiplpcol = new Array();		

	noofshiplpfilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', orderlist));
	noofshiplpfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4])); 	//SHIP Task
	noofshiplpfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [14])); // SHIPPED
	noofshiplpfilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); // LOADED

	noofshiplpcol[0] = new nlobjSearchColumn('custrecord_ship_lp_no').setSort(); 
	noofshiplpcol[1] = new nlobjSearchColumn('custrecord_ebiz_trailer_no'); 
	noofshiplpcol[2] = new nlobjSearchColumn('custrecord_wms_status_flag'); 
	noofshiplpcol[3] = new nlobjSearchColumn('custrecord_tasktype'); 
	noofshiplpcol[4] = new nlobjSearchColumn('custrecord_container'); 
	noofshiplpcol[5] = new nlobjSearchColumn('custrecord_comp_id'); 
	noofshiplpcol[6] = new nlobjSearchColumn('custrecord_site_id');
	noofshiplpcol[7] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	shiplpsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, noofshiplpfilter, noofshiplpcol);
	nlapiLogExecution('ERROR', 'getShippingDetailsForEligibleOrders shiplpsearchresults', shiplpsearchresults);
	return shiplpsearchresults;
}

function GetIndividiualClosedTaskDetails(searchrecordClosedTask,ebizorderno){

	var individiualClosedTaskDetailsListT = new Array();
	var individiualClosedTaskDetailsList = new Array();
	nlapiLogExecution('ERROR', 'GetIndividiualClosedTaskDetails', 'Start');
	nlapiLogExecution('ERROR', 'searchrecordClosedTask', searchrecordClosedTask);
	nlapiLogExecution('ERROR', 'ebizorderno', ebizorderno);
	try{

		if (searchrecordClosedTask != null && searchrecordClosedTask.length >0 && ebizorderno != null && ebizorderno.length>0){
			for ( var i = 0 ; i < searchrecordClosedTask.length ; i++){
				//nlapiLogExecution('ERROR', 'searchrecordClosedTask custrecord_ebiztask_ebiz_order_no',searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_order_no'));
				if (searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_order_no')  == ebizorderno )
				{
					//nlapiLogExecution('ERROR', 'OrderNo matches and order internalid', searchrecordClosedTask[i]['custrecord_ebiztask_ebiz_order_no']);

					individiualClosedTaskDetailsList = new Array();
					individiualClosedTaskDetailsList[0] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_line_no');
					individiualClosedTaskDetailsList[1] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_sku_no');
					individiualClosedTaskDetailsList[2] = searchrecordClosedTask[i].getText('custrecord_ebiztask_ebiz_sku_no');					
					individiualClosedTaskDetailsList[3] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_act_qty');
					individiualClosedTaskDetailsList[4] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_order_no');
					individiualClosedTaskDetailsList[5] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_uom_level');					
					individiualClosedTaskDetailsList[6] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_contlp_no');
					individiualClosedTaskDetailsList[7] = searchrecordClosedTask[i].getText('custrecord_ebiztask_ebiz_contlp_no');
					individiualClosedTaskDetailsList[8] = searchrecordClosedTask[i].getText('custrecord_ebiztask_serial_no');
					individiualClosedTaskDetailsList[9] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_act_end_date');
					individiualClosedTaskDetailsList[10] = searchrecordClosedTask[i].getId();
					individiualClosedTaskDetailsList[11] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_batch_no');
					individiualClosedTaskDetailsList[12] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_packcode');
					individiualClosedTaskDetailsList[13] = searchrecordClosedTask[i].getValue('tranid','custrecord_ebiztask_ebiz_order_no');	

					individiualClosedTaskDetailsListT.push(individiualClosedTaskDetailsList);
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'individiualClosedTaskDetailsListT', exception);
	}

	nlapiLogExecution('ERROR', 'GetIndividualOrderInfo', 'end');
	nlapiLogExecution('ERROR', 'Individual order info-individiualClosedTaskDetailsListT', individiualClosedTaskDetailsListT);
	return individiualClosedTaskDetailsListT;
}
/**
 * Function to retrieve the SKU details for a list of SKUs
 * @param skulist
 * @returns {Array}
 */
function GetAllcustomfieldDetails(closedTaskList,skulist){
	nlapiLogExecution('ERROR', 'GetAllcustomfieldDetails', 'Start');

	var UPCDetailsListT = new Array();
	
		var UPCDetailsList = new Array();
		var filters = new Array();
		var columns = new Array();
		try
		{			
			filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'E'));
			filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof', skulist));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord14');
			columns[1] = new nlobjSearchColumn('custrecord17');		

			var ResupcCodes = nlapiSearchRecord('customrecord265', null, filters, columns);


			if(ResupcCodes != null && ResupcCodes.length > 0){
				for(var i = 0; i < ResupcCodes.length; i++ ){
					UPCDetailsList = new Array();
					UPCDetailsList[0] = ResupcCodes[i].getValue('custrecord17');
					UPCDetailsList[1] = ResupcCodes[i].getValue('custrecord14');				
					UPCDetailsListT.push (UPCDetailsList);
				}
			}
		}
		catch(exception){
			nlapiLogExecution('ERROR', 'GetAllcustomfieldDetails', exception);
		}		
		return UPCDetailsListT;
	}
	/**
	 * Function to retrieve the SKU details for a list of SKUs
	 * @param skulist
	 * @returns {Array}
	 */
	function GetAllUPCDetails(closedTaskList,skulist){
		nlapiLogExecution('ERROR', 'GetAllUPCDetails', 'Start');

		var UPCDetailsListT = new Array();

		var UPCDetailsList = new Array();
		var filters = new Array();
		var columns = new Array();
		try
		{			
			filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'U'));
			filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof', skulist));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord14');
			columns[1] = new nlobjSearchColumn('custrecord17');				

			var ResupcCodes = nlapiSearchRecord('customrecord265', null, filters, columns);

			if(ResupcCodes != null && ResupcCodes.length > 0){
				for(var i = 0; i < ResupcCodes.length; i++ ){
					UPCDetailsList = new Array();
					UPCDetailsList[0] = ResupcCodes[i].getValue('custrecord17');
					UPCDetailsList[1] = ResupcCodes[i].getValue('custrecord14');
					UPCDetailsListT.push (UPCDetailsList);
				}
			}
		}
		catch(exception){
			nlapiLogExecution('ERROR', 'GetAllUPCDetails', exception);
		}			
		return UPCDetailsListT;
	}
		
/**
 * Landing function for ASN Confirmation - 856 EDI for SPS Commerce
 * 
 * @param type: Edit Mode Flag
 */
	function AsncConfirmationUE(type) {
		nlapiLogExecution('ERROR', 'AsncConfirmationUE', 'Start');
		nlapiLogExecution('ERROR', 'Event Type=', type);

		var trailername="",departDate="";	var ASNCRuleVal = "",Dummylps="";	var SKUList = new Array();	var SKUDetailsList = new Array();	var newRecIdsArray = new Array();
		var containerLengthTotal = 0;	var containerWidthTotal = 0;	var containerHeightTotal = 0;	var containerWeightTotal = 0;var ShipUnitCharges = 0;
		var ShipunitlevelQty = 0;var shipLP="", trailer="",wmsStatusflag="",tasktype="",ContainerSizeId="",compid="",siteid="";
		var SalesorderDetails="", MasterBOL="", salesOrderList="", batchDetails="", uomDetails="", skuDetails="", searchrecordClosedTask="", orderList="",EntityDCStoreResults='';
		var OrderwithCustomerInfo="",CustomerMasterInfo="",ShippingDetailsforOrder="";var OrdersEligibleForASNGeneration=new Array();  // Added by Mahesh on 072712 to trigger the ASN based on customer master ASN Required?
		var skulist="",prevcompid="",prevsiteid="",UPCDetails="",CustomFieldDetails="";var RealtimeOrders=new Array(); 

		var ebizpro="",ebizseal="",ebizmastershipper="",ebizappointmenttrailer="",ebizexparrivaldate="",scaccodetrlr="";
		try {
			if (type == 'edit' || type == 'xedit') { // Process ASN Confirmation only if in EDIT
				// mode

				if(type == 'edit'){
					var newRecord = nlapiGetNewRecord();			
					trailername = newRecord.getFieldValue('name');
					departDate = newRecord.getFieldValue('custrecord_ebizdepartdate');	

					ebizpro = newRecord.getFieldValue('custrecord_ebizpro');
					ebizseal = newRecord.getFieldValue('custrecord_ebizseal');
					ebizmastershipper= newRecord.getFieldValue('custrecord_ebizmastershipper');
					ebizappointmenttrailer= newRecord.getFieldValue('custrecord_ebizappointmenttrailer');
					ebizexparrivaldate= newRecord.getFieldValue('custrecord_ebizexparrivaldate');
					scaccodetrlr= newRecord.getFieldText('custrecord_ebizcarrierid');

					nlapiLogExecution('ERROR', 'edit trailer name', trailername);
					nlapiLogExecution('ERROR', 'edit departDate', departDate);
				}
				else if(type == 'xedit'){
					var oldRecord = nlapiGetOldRecord();
					var newRecord = nlapiGetNewRecord();
					departDate = newRecord.getFieldValue('custrecord_ebizdepartdate');
					trailername = oldRecord.getFieldValue('name');
					ebizpro = oldRecord.getFieldValue('custrecord_ebizpro');
					ebizseal = oldRecord.getFieldValue('custrecord_ebizseal');
					ebizmastershipper= oldRecord.getFieldValue('custrecord_ebizmastershipper');
					ebizappointmenttrailer= oldRecord.getFieldValue('custrecord_ebizappointmenttrailer');
					ebizexparrivaldate= oldRecord.getFieldValue('custrecord_ebizexparrivaldate');
					scaccodetrlr= oldRecord.getFieldText('custrecord_ebizcarrierid');

					nlapiLogExecution('ERROR', 'xedit trailer name', trailername);
					nlapiLogExecution('ERROR', 'xedit departDate', departDate);
				}

				var trlrfilter = new Array();
				var trlrcol = new Array();		

				trlrfilter.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailername));
				trlrfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); 	//PICK Task			
				trlrfilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); // LOADED
				trlrcol[0] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
				trlrsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, trlrfilter, trlrcol);
				if(trlrsearchresults == null || trlrsearchresults == ""){

					if(departDate != null && departDate != ""){
						//get number of shiplps' from opentask

						var noofshiplpfilter = new Array();
						var noofshiplpcol = new Array();		

						noofshiplpfilter.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailername));
						noofshiplpfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4])); 	//SHIP Task
						noofshiplpfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [14])); // SHIPPED
						noofshiplpfilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); // LOADED

						noofshiplpcol[0] = new nlobjSearchColumn('custrecord_ship_lp_no'); 
						noofshiplpcol[1] = new nlobjSearchColumn('custrecord_ebiz_trailer_no'); 
						noofshiplpcol[2] = new nlobjSearchColumn('custrecord_wms_status_flag'); 
						noofshiplpcol[3] = new nlobjSearchColumn('custrecord_tasktype'); 
						noofshiplpcol[4] = new nlobjSearchColumn('custrecord_container'); 
						noofshiplpcol[5] = new nlobjSearchColumn('custrecord_comp_id'); 
						noofshiplpcol[6] = new nlobjSearchColumn('custrecord_site_id');
						noofshiplpcol[7] = new nlobjSearchColumn('custrecord_ebiz_order_no');

						shiplpsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, noofshiplpfilter, noofshiplpcol);
						nlapiLogExecution('ERROR', 'shiplpsearchresults', shiplpsearchresults);
						if(shiplpsearchresults !=null && shiplpsearchresults !="")
						{
							orderList = getDistinctshiplps(shiplpsearchresults);	
							nlapiLogExecution('ERROR', 'distinct orderlist count', orderList);
							//to find real time or scheduled script							
							ASNTimingResults=GetAllresultsfromASNTiming();
							CustomerMasterInfo=GetAllCustomers();
							OrderwithCustomerInfo=GetSalesorderDetailsforASNTrigger(orderList);					
							//to get req so's with flay req=Y					
							var matchFound = false;

							/* The below for loop is used to filter the order numbers which are eligible for ASN Generation */  
							for ( var m = 0; OrderwithCustomerInfo != null && m < OrderwithCustomerInfo.length; m++) 
							{
								for ( var n = 0; CustomerMasterInfo != null && n < CustomerMasterInfo.length; n++) 
								{

									if (CustomerMasterInfo[n].getValue('internalid') == OrderwithCustomerInfo[m].getValue('entity') && CustomerMasterInfo[n].getValue('custentity_ebiz_asn_required') == 'T')
									{
										OrdersEligibleForASNGeneration.push(OrderwithCustomerInfo[m].getValue('internalid'));
										matchFound = true;
										//nlapiLogExecution('ERROR', 'matchFound', matchFound);
									}
								}
							}	
							nlapiLogExecution('ERROR', 'OrdersEligibleForASNGeneration', OrdersEligibleForASNGeneration);
							var getRealtimeOrderRes=getRealtimeOrders(OrdersEligibleForASNGeneration)
							for ( var p = 0; p < getRealtimeOrderRes.length; p++) 
							{
								//nlapiLogExecution('ERROR', 'entity',getRealtimeOrderRes[p].getValue('entity'));
								//nlapiLogExecution('ERROR', 'dc',getRealtimeOrderRes[p].getValue('custbody_dc_store'));
								for ( var q = 0; q < ASNTimingResults.length; q++) 
								{
									//nlapiLogExecution('ERROR', 'ASN entity',ASNTimingResults[q].getValue('custrecord_ebiz_asntiming_entity'));
									//nlapiLogExecution('ERROR', 'ASN dc',ASNTimingResults[q].getValue('custrecord_ebiz_asntiming_dcstore'));
									if (ASNTimingResults[q].getValue('custrecord_ebiz_asntiming_entity') == getRealtimeOrderRes[p].getValue('entity') && ASNTimingResults[q].getValue('custrecord_ebiz_asntiming_dcstore') == getRealtimeOrderRes[p].getValue('custbody_dc_store'))
									{
										RealtimeOrders.push(getRealtimeOrderRes[p].getValue('internalid'));										
									}
								}
							}							
							nlapiLogExecution('ERROR', 'RealtimeOrders', RealtimeOrders);
							if(RealtimeOrders !=null && RealtimeOrders !="")
							{
								searchrecordClosedTask = GeteBizTaskRecs(siteid, compid,RealtimeOrders);

								nlapiLogExecution('ERROR', 'After closed taskrecs - aval Usage', nlapiGetContext().getRemainingUsage());

								if (searchrecordClosedTask != null	&& searchrecordClosedTask != "") 
								{
									logMsg = logMsg + 'Closed Task Count='	+ searchrecordClosedTask.length + '<br>';
								} else {
									logMsg = logMsg + 'Closed Task Count=' + 'Zero' + '<br>';
								}

								// Get the details for the distinct list of SKUs
								skulist = getDistinctSKUsFromTaskList(searchrecordClosedTask);
								skuDetails = GetSKUDetails(searchrecordClosedTask,skulist);
								UPCDetails = GetAllUPCDetails(searchrecordClosedTask,skulist);
								CustomFieldDetails = GetAllcustomfieldDetails(searchrecordClosedTask,skulist);

								nlapiLogExecution('ERROR', 'After SKU details - aval Usage', nlapiGetContext().getRemainingUsage());

								if (skuDetails != null && skuDetails != "") {
									logMsg = logMsg + 'SKU Details Count=' + skuDetails.length	+ '<br>';
								} else {
									logMsg = logMsg + 'SKU Details Count=' + 'Zero' + '<br>';
								}
								// Get the UOM Details for the distinct SKUS

								uomDetails = GetUOMDetails(searchrecordClosedTask,skulist);
								nlapiLogExecution('ERROR', 'After UOM details - aval Usage', nlapiGetContext().getRemainingUsage());
								nlapiLogExecution('ERROR', 'Before uom details length', uomDetails);
								if (uomDetails != null && uomDetails != "") {
									logMsg = logMsg + 'UOM Details Count=' + uomDetails.length+ '<br>';
								} else {
									logMsg = logMsg + 'UOM Details Count=' + 'Zero' + '<br>';
								}
								// Get the Batch Details for the distinct skus

								var batchDetails = GetBatchDetails(searchrecordClosedTask,skulist);
								nlapiLogExecution('ERROR', 'After Batch details - aval Usage', nlapiGetContext().getRemainingUsage());
								if (batchDetails != null && batchDetails != "") {
									logMsg = logMsg + 'Batch Details Count='+ batchDetails.length + '<br>';
								} else {
									logMsg = logMsg + 'Batch Details Count=' + 'Zero' + '<br>';
								}			

								//  to get distinct SO internal Id's
								salesOrderList = getDistinctOrderIDsFromTaskList(searchrecordClosedTask);				
								//to get BOL#

								MasterBOL=GetMasterBOL(salesOrderList);
								nlapiLogExecution('ERROR', 'MasterBOL 0',MasterBOL[0]);
								nlapiLogExecution('ERROR', 'MasterBOL 1',MasterBOL[1]);

								// Get the details for the distinct list of orders.
								SalesorderDetails = GetSalesOrderDetails(searchrecordClosedTask,salesOrderList);
								nlapiLogExecution('ERROR', 'After SO details - aval Usage', nlapiGetContext().getRemainingUsage());
								if (SalesorderDetails != null && SalesorderDetails != "") {
									logMsg = logMsg + 'Salesorder Details Count='+ SalesorderDetails.length + '<br>';				} 
								else {
									logMsg = logMsg + ' Salesorder Details Count=' + 'Zero'+ '<br>';
								}
								//}


								ShippingDetailsforOrder=getShippingDetailsForEligibleOrders(RealtimeOrders);
								nlapiLogExecution('ERROR', 'RealtimeOrders', RealtimeOrders);
								var allalternateids=getallbuyerpartno(RealtimeOrders);
								nlapiLogExecution('ERROR', 'allalternateids', allalternateids);
								var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record
								for (var q = 0; ShippingDetailsforOrder!= null && q < ShippingDetailsforOrder.length; q++) 
								{
									//nlapiLogExecution('ERROR', 'ShippingDetailsforOrder', ShippingDetailsforOrder.length);
									//nlapiSubmitField('customrecord_ebiznet_trn_opentask',shiplpsearchresults[q].getId(), 'custrecord_hostid', 'SPS');

									shipLP = ShippingDetailsforOrder[q].getValue('custrecord_ship_lp_no');
									trailer = ShippingDetailsforOrder[q].getValue('custrecord_ebiz_trailer_no');
									wmsStatusflag = ShippingDetailsforOrder[q].getValue('custrecord_wms_status_flag');
									tasktype = ShippingDetailsforOrder[q].getValue('custrecord_tasktype');
									ContainerSizeId = ShippingDetailsforOrder[q].getValue('custrecord_container');
									compid = ShippingDetailsforOrder[q].getValue('custrecord_comp_id');
									siteid = ShippingDetailsforOrder[q].getValue('custrecord_site_id');		
									ebizorderno = ShippingDetailsforOrder[q].getValue('custrecord_ebiz_order_no');


									//nlapiLogExecution('ERROR', 'Current Available Usage', nlapiGetContext().getRemainingUsage());
									// Get system rule for SPS ASN Confirmation
									if(prevcompid == compid && prevsiteid == siteid)
									{//nothing doing

									}
									else
									{
										ASNCRuleVal = SystemRuleforASNC(siteid, compid);
									}
									prevcompid = compid;
									prevsiteid=siteid;
									// Get all dummy lp's againast to Master Lp					
									//nlapiLogExecution('ERROR', 'After system rule - aval Usage', nlapiGetContext().getRemainingUsage());
									//nlapiLogExecution('ERROR', 'ASNCRuleValue', ASNCRuleVal);
									// Hardcoding for Dyna. It should be removed after testing.

									ASNCRuleVal = 'Y';

									var logMsg = 'shipLP=' + shipLP + '<br>';
									logMsg = logMsg + 'Trailer=' + trailer + '<br>';
									logMsg = logMsg + 'WMS Status Flag=' + wmsStatusflag + '<br>';
									logMsg = logMsg + 'Task Type=' + tasktype + '<br>';
									logMsg = logMsg + 'Container SizeID=' + ContainerSizeId + '<br>';
									logMsg = logMsg + 'Comp id=' + compid + '<br>';
									logMsg = logMsg + 'Site ID=' + siteid + '<br>';
									logMsg = logMsg + '856SystemRule=' + ASNCRuleVal;

									nlapiLogExecution('ERROR', 'Current Available Values', logMsg);

									// WMS STATUS FLAG = STATUS.OUTBOUND.TRAILER_LOADED & TASK TYPE =
									// SHIP
									if (wmsStatusflag == 14 && tasktype == 4 && ASNCRuleVal == 'Y') {

										// get details from closed task
										//var searchrecordClosedTask = GeteBizTaskRecs(siteid, compid,shipLP);
//										if (searchrecordClosedTask != null	&& searchrecordClosedTask != "") 
//										{

										//var TrailerDetails = GetTrailerDetails(trailer);

										//nlapiLogExecution('ERROR', 'TrailerDetails', TrailerDetails);

										nlapiLogExecution('ERROR', 'After Trailer details - aval Usage', nlapiGetContext().getRemainingUsage());
//										if (TrailerDetails != null && TrailerDetails != "") {
//										logMsg = logMsg + 'Trailer Details Count='+ TrailerDetails.length + '<br>';
//										} else {
//										logMsg = logMsg + 'TrailerDetails Details Count=' + 'Zero' + '<br>';
//										}

										nlapiLogExecution('ERROR', 'All Record sets Lengths', logMsg);

										if (searchrecordClosedTask != null 	&& searchrecordClosedTask.length > 0) {

											// Varaible Declaration
											var lineno ="", skuid ="", skuidText ="", Actqty = "",SOid = "",uomlevel ="", containerLP ="", containerLPtext ="", SerilalId = "",IssueDate = "",IntId = "";
											var BatchIdText = "",packCode="",BatchId = "",uomIdText = "",upcCode="",customField="",IndividualSKUDetailsList ="",SKUDesc ="",SKUFamily ="",SKUGroup = "",SKUtype = "";
											var HazmatCode ="", TempCntrl ="", vendor ="",IndividualOrderInfoList ="",SalesOrdNo = "",ebizOrderNo="",EDIREF = "",ShipmentVendor ="", Shipmethod = "",ShipmethodID ="", SCACCode = "";
											var PrevShipmethod ="",FOB ="",ShipAddressee ="",ShipAddress1 = "",ShipAddress2 = "",Shipcity = "",ShipState = "",ShipCountry = "",Shipzip = "",ShiptoPhone ="";
											var ExpectedShipdate = "",ActualArrivalDate = "",PlannedArrivalDate = "",Destination = "",Route = "",Class = "",ConsigneeId = "",Billaddressee = "",Billaddr1 = "";
											var Billaddr2 = "",Billaddr3 = "",Billcity = "",BillState = "",Billzip = "",Billcountry = "",Billphone = "",location = "",Company = "",Department = "";
											var varTerms = "",CustomerPO = "",ShiptoPhone = "",Billphone = "",ShipAddrID = "",individualitemdimsList ="",UOM1 = "",UOMQty1 = "",UOMLength1 = "",UOMWidth1 = "";
											var UOMHeight1 = "",UOMWeight1 = "",UOMCube1 = "",Packcode1 = "",UOMlevel1 = "",UOM2 = "",UOMQty2 = "",UOMLength2 = "",UOMWidth2 = "",UOMHeight2 = "",UOMWeight2 = "";
											var UOMCube2 = "",Packcode2 = "",UOMlevel2 = "",UOM3 = "",UOMQty3 = "",UOMLength3 = "",UOMWidth3 = "",UOMHeight3 = "",UOMWeight3 = "",UOMCube3 = "",Packcode3 = "";
											var UOMlevel3 = "",UOM4 = "",UOMQty4 = "",UOMLength4 = "",UOMWidth4 = "",UOMHeight4 = "",UOMWeight4 = "",UOMCube4 = "",Packcode4 = "",UOMlevel4 = "",UOM5 = "",UOMQty5 = "";
											var UOMLength5 = "",UOMWidth5 = "",UOMHeight5 = "",UOMWeight5 = "",UOMCube5 = "",Packcode5 = "",UOMlevel5 = "",CUCC = "",Conainercube = "",ContainerWeight = "",IndividualCUCCDetailsList = "";
											var EUCC = "",IndividualEUCCDetailsList = "",SUCC = "",ShipunitCube = "",ShipunitWeight = "",IndividualSUCCDetailsList = "",ContainerLength = "",ContainerWidth = "",ContainerHeight = "";
											var ContainerMaxWeight = "",ContainerTareWeight = "",ContainerCube = "",IndividualContainerDimslist ="",TrackingNumbers = "",ShippingCharges = "",IndividualShipmanfDetList ="",Pronum = "";
											var SealNo = "",MasterShipper = "",TrailerAppointNo = "",IndividualTrailerDetails = "",prevskuid="",prevcontainer  ="",prevshipLP ="",prevcontainerLP="", previtemId="", newcontlp="",prevtrailer="";
											var PrevOrdNo = "",prevlineno = "",TaskInternalID="",CarrierOptions="",ShiptoEmail ="",ShiptoFax="", ShiptoEmail="",tranid="";
											var ConsigneeId="",salesordWeight="",salesordCube="",Billcity="",BillState="",Billzip="",Billcountry="",Billphone="",location="",Company="";
											var Department="",Terms="",OrderType="",OrderPriority="",CustomerPO="",ShipAddrID="",CUCC="",CUCC = "",Conainercube = "",ContainerWeight = "",EUCC="";
											var TrackingNumbers="",ShippingCharges="",SUCC = "",ShipunitCube = "",ShipunitWeight = "",ContainerLength = "",ContainerWidth = "",ContainerHeight = "",ContainerMaxWeight = "",ContainerTareWeight = "",ContainerCube = "";
											var Pronum="",MasterShipper="",TrailerAppointNo="",SOid="",SOidText='',prevSOid="",soalternateidlist="",alternateid="", ItemID="";					
											var IndividualupcCodeDetailsList="",IndividualcustomFieldDetailsList="";
											var sysdate=DateStamp();
											nlapiLogExecution('ERROR', 'Before for loop - Available Usage', nlapiGetContext().getRemainingUsage());
											var stsupdatecnt =0;

											var IndividiualOrdersearchrecordClosedTask=GetIndividiualClosedTaskDetails(searchrecordClosedTask,ebizorderno);
											nlapiLogExecution('ERROR', 'IndividiualOrdersearchrecordClosedTask', IndividiualOrdersearchrecordClosedTask);
											for ( var count = 0; count < IndividiualOrdersearchrecordClosedTask.length; count++) {


												lineno=IndividiualOrdersearchrecordClosedTask[count][0];
												skuid=IndividiualOrdersearchrecordClosedTask[count][1];
												skuidText=IndividiualOrdersearchrecordClosedTask[count][2];					
												Actqty=IndividiualOrdersearchrecordClosedTask[count][3] ;
												SOid=IndividiualOrdersearchrecordClosedTask[count][4] ;
												uomlevel=IndividiualOrdersearchrecordClosedTask[count][5] ;					
												containerLP=IndividiualOrdersearchrecordClosedTask[count][6];
												containerLPtext=IndividiualOrdersearchrecordClosedTask[count][7] ;
												SerilalId=IndividiualOrdersearchrecordClosedTask[count][8] ;
												IssueDate=IndividiualOrdersearchrecordClosedTask[count][9] ;
												TaskInternalID=IndividiualOrdersearchrecordClosedTask[count][10];
												BatchIdText=IndividiualOrdersearchrecordClosedTask[count][11] ;
												packCode=IndividiualOrdersearchrecordClosedTask[count][12] ;
												SOidText=IndividiualOrdersearchrecordClosedTask[count][13] ;


												logMsg = 'LineNo=' + lineno + '<br>';
												logMsg = logMsg + 'SKU ID=' + skuid + '<br>';
												logMsg = logMsg + 'skuidText=' + skuidText + '<br>';
												logMsg = logMsg + 'Act Qty=' + Actqty + '<br>';
												logMsg = logMsg + 'SO ID=' + SOid + '<br>';
												logMsg = logMsg + 'SOid Text=' + SOidText + '<br>';
												logMsg = logMsg + 'UOM Level=' + uomlevel + '<br>';
												logMsg = logMsg + 'Container LP=' + containerLP	+ '<br>';
												logMsg = logMsg + 'Batch ID Text=' + BatchIdText;
												nlapiLogExecution('ERROR', 'Current Record Values',	logMsg);
												logMsg = "";

												// Get Batch ID
												if (BatchIdText != null && BatchIdText != "") {
													BatchId = GetIndividualBatchID(batchDetails, skuid);
												}
												// Get UPC Code
												if ( skuid == prevskuid )
												{	//No need to call upc API. You can use the existing one.
													nlapiLogExecution('ERROR', 'skuid == prevskuid cond',	skuid);
												}
												else
												{ //Get the upc code1
													//nlapiLogExecution('ERROR', 'skuid != prevskuid cond',	skuid);
													//upcCode = GetupcCode(skuid);
													IndividualupcCodeDetailsList = GetIndividualUPCInfo(UPCDetails, skuid);
													if(IndividualupcCodeDetailsList !="")
													{
														upcCode=IndividualupcCodeDetailsList[0][0];
													}
													// get the details for itemmoreinfo custom field value 1
													//customField = Getcustomfield(skuid);
													IndividualcustomFieldDetailsList = GetIndividualcustomFieldInfo(CustomFieldDetails, skuid);
													if(IndividualcustomFieldDetailsList !=""){
														customField=IndividualcustomFieldDetailsList[0][0];
													}
													//nlapiLogExecution('ERROR', 'upc code',	upcCode);
													//nlapiLogExecution('ERROR', 'customField',customField);

													// The below API will take skudetails , skuid as
													// parameter and
													// returns all the details of the skuid
													//nlapiLogExecution('ERROR', 'skudetails list sending to ind-skufun', skuDetails + '  ' + skuid 	);

													IndividualSKUDetailsList = GetIndividualSKUInfo(skuDetails, skuid);
													//nlapiLogExecution('ERROR', 'IndividualSKUDetailsList',	IndividualSKUDetailsList.length);
													ItemID= IndividualSKUDetailsList[0][0]; //SKU  	
													SKUDesc = IndividualSKUDetailsList[0][2];  		//SalesDesc
													SKUFamily = IndividualSKUDetailsList[0][3]; 	// itemFamily
													SKUGroup = IndividualSKUDetailsList[0][4];      //itemGroup
													SKUtype = IndividualSKUDetailsList[0][1];       //skutype
													HazmatCode = IndividualSKUDetailsList[0][5];    //HazmatCls];
													TempCntrl = IndividualSKUDetailsList[0][6];     //TempCntrl;
													vendor = IndividualSKUDetailsList[0][7];		// VendorName


													// Get SKU dimensions for the sku.
													individualitemdimsList = AssignUOMDetails(uomDetails, skuid);

													UOM1 = "";UOMQty1 = "";UOMLength1 = "";UOMWidth1 = "";
													UOMHeight1 = "";UOMWeight1 = "";UOMCube1 = "";Packcode1 = "";UOMlevel1 = "";UOM2 = "";UOMQty2 = "";UOMLength2 = "";	UOMWidth2 = "";	UOMHeight2 = "";UOMWeight2 = "";
													UOMCube2 = "";	Packcode2 = "";	UOMlevel2 = "";	UOM3 = "";
													UOMQty3 = "";UOMLength3 = "";UOMWidth3 = "";UOMHeight3 = "";UOMWeight3 = "";UOMCube3 = "";
													Packcode3 = "";	UOMlevel3 = "";	UOM4 = "";UOMQty4 = "";	UOMLength4 = "";
													UOMWidth4 = "";UOMHeight4 = "";UOMWeight4 = "";UOMCube4 = "";Packcode4 = "";UOMlevel4 = "";UOM5 = "";UOMQty5 = "";UOMLength5 = "";UOMWidth5 = "";UOMHeight5 = "";UOMWeight5 = "";UOMCube5 = "";Packcode5 = "";UOMlevel5 = "";
													nlapiLogExecution('ERROR', 'SKU Dim details',individualitemdimsList);

													if (individualitemdimsList != null&& individualitemdimsList.length > 0) {
														nlapiLogExecution('ERROR', 'individualitemdimsList.length',individualitemdimsList.length);
														if (individualitemdimsList.length == 1) {
															UOM1 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];
														} else if (individualitemdimsList.length == 2) {

															nlapiLogExecution('ERROR', 'individualitemdimsList[0][0]',individualitemdimsList[0][0]);
															UOM1 = individualitemdimsList[0][0];//['UOMID'];
															nlapiLogExecution('ERROR', 'UOM1',UOM1);
															UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

															UOM2 = individualitemdimsList[1][0];//['UOMID'];
															UOMQty2 = individualitemdimsList[1][1];//['UOMQty'];
															UOMLength2 = individualitemdimsList[1][2];//['UOMLength'];
															UOMWidth2 = individualitemdimsList[1][3];//['UOMWidth'];
															UOMHeight2 = individualitemdimsList[1][4];//['UOMHeight'];
															UOMWeight2 = individualitemdimsList[1][5];//['UOMWeight'];
															UOMCube2 = individualitemdimsList[1][6];//['UOMCube'];
															Packcode2 = individualitemdimsList[1][9];//['UOMlevel'];
															UOMlevel2 = individualitemdimsList[1][8];//['Packcode'];

														} else if (individualitemdimsList.length == 3) {

															UOM1 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

															UOM2 = individualitemdimsList[1][0];//['UOMID'];
															UOMQty2 = individualitemdimsList[1][1];//['UOMQty'];
															UOMLength2 = individualitemdimsList[1][2];//['UOMLength'];
															UOMWidth2 = individualitemdimsList[1][3];//['UOMWidth'];
															UOMHeight2 = individualitemdimsList[1][4];//['UOMHeight'];
															UOMWeight2 = individualitemdimsList[1][5];//['UOMWeight'];
															UOMCube2 = individualitemdimsList[1][6];//['UOMCube'];
															Packcode2 = individualitemdimsList[1][9];//['UOMlevel'];
															UOMlevel2 = individualitemdimsList[1][8];//['Packcode'];


															UOM3 = individualitemdimsList[2][0];//['UOMID'];
															UOMQty3 = individualitemdimsList[2][1];//['UOMQty'];
															UOMLength3 = individualitemdimsList[2][2];//['UOMLength'];
															UOMWidth3 = individualitemdimsList[2][3];//['UOMWidth'];
															UOMHeight3 = individualitemdimsList[2][4];//['UOMHeight'];
															UOMWeight3 = individualitemdimsList[2][5];//['UOMWeight'];
															UOMCube3 = individualitemdimsList[2][6];//['UOMCube'];
															Packcode3 = individualitemdimsList[2][9];//['UOMlevel'];
															UOMlevel3 = individualitemdimsList[2][8];//['Packcode'];

														} else if (individualitemdimsList.length == 4) {

															UOM1 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

															UOM2 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty2 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength2 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth2 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight2 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight2 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube2 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode2 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel2 = individualitemdimsList[0][8];//['Packcode'];


															UOM3 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty3 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength3 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth3 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight3 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight3 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube3 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode3 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel3 = individualitemdimsList[0][8];//['Packcode'];

															UOM4 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty4 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength4 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth4 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight4 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight4 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube4 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode4 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel4 = individualitemdimsList[0][8];//['Packcode'];
														} else if (individualitemdimsList.length == 5) {

															UOM1 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

															UOM2 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty2 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength2 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth2 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight2 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight2 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube2 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode2 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel2 = individualitemdimsList[0][8];//['Packcode'];


															UOM3 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty3 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength3 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth3 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight3 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight3 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube3 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode3 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel3 = individualitemdimsList[0][8];//['Packcode'];

															UOM4 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty4 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength4 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth4 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight4 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight4 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube4 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode4 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel4 = individualitemdimsList[0][8];//['Packcode'];

															UOM5 = individualitemdimsList[0][0];//['UOMID'];
															UOMQty5 = individualitemdimsList[0][1];//['UOMQty'];
															UOMLength5 = individualitemdimsList[0][2];//['UOMLength'];
															UOMWidth5 = individualitemdimsList[0][3];//['UOMWidth'];
															UOMHeight5 = individualitemdimsList[0][4];//['UOMHeight'];
															UOMWeight5 = individualitemdimsList[0][5];//['UOMWeight'];
															UOMCube5 = individualitemdimsList[0][6];//['UOMCube'];
															Packcode5 = individualitemdimsList[0][9];//['UOMlevel'];
															UOMlevel5 = individualitemdimsList[0][8];//['Packcode'];
														}
													}
												}
												prevskuid = skuid;					

												logMsg = 'SKU=' + skuid + '<br>';
												logMsg = logMsg + 'SKU Desc=' + SKUDesc + '<br>';
												logMsg = logMsg + 'SKU Family=' + SKUFamily + '<br>';
												logMsg = logMsg + 'SKU Group=' + SKUGroup + '<br>';
												logMsg = logMsg + 'SKU Type=' + SKUtype + '<br>';
												logMsg = logMsg + 'HazmatCode=' + HazmatCode + '<br>';
												logMsg = logMsg + 'Temp Cntrl=' + TempCntrl + '<br>';
												logMsg = logMsg + 'Vendor=' + vendor + '<br>';
												logMsg = logMsg + 'UPC Code=' + upcCode + '<br>';
												logMsg = logMsg + 'Batch ID=' + BatchId + '<br>';

												nlapiLogExecution('ERROR', 'Current SKU Record Values',logMsg);
												if(SOid == prevSOid){
													//No need to call upc API. You can use the existing one.
													nlapiLogExecution('ERROR', 'SOid == prevSOid cond',	SOid);
												}
												else
												{
													IndividualOrderInfoList = GetIndividualOrderInfo(SalesorderDetails, SOid);
													//nlapiLogExecution('ERROR', 'Order info list-IndividualOrderInfoList',IndividualOrderInfoList);

													ebizOrderNo=SOid;
													SalesOrdNo = IndividualOrderInfoList[0][0]; //tranID
													EDIREF = IndividualOrderInfoList[0][1];   //EDIRefFld;
													ShipmentVendor = IndividualOrderInfoList[0][2]; // ShipmentVendor;
													Shipmethod = IndividualOrderInfoList[0][3];   // Shipmethod
													ShipmethodID = IndividualOrderInfoList[0][4]; // ShipmethodID'
													SCACCode=Shipmethod;
													operatingUnit= IndividualOrderInfoList[0][45];//['operating unit'];
													nlapiLogExecution('ERROR', 'Shipment vendor',ShipmentVendor);
													nlapiLogExecution('ERROR', 'operatingUnit',operatingUnit);

													salesordWeight= IndividualOrderInfoList[0][46];//['operating unit'];
													salesordCube= IndividualOrderInfoList[0][47];//['operating unit'];						
													nlapiLogExecution('ERROR', 'salesordWeight',salesordWeight);
													nlapiLogExecution('ERROR', 'salesordCube',salesordCube);

													tranid = IndividualOrderInfoList[0][0];
													FOB = IndividualOrderInfoList[0][39];  // FOB
													ShipAddressee = IndividualOrderInfoList[0][5]; //ShipAddressee
													ShipAddress1 = IndividualOrderInfoList[0][6];// ShipAddr1
													ShipAddress2 = IndividualOrderInfoList[0][7];// [ShipAddr2]
													Shipcity = IndividualOrderInfoList[0][8];// ['ShipCity'];
													ShipState = IndividualOrderInfoList[0][9];//['ShipState'];
													ShipCountry = IndividualOrderInfoList[0][10];//['Shipcountry'];
													Shipzip = IndividualOrderInfoList[0][11]; //['Shipzip'];
													ShiptoPhone = IndividualOrderInfoList[0][42]; //['ShiptoPhone'];
													ExpectedShipdate = IndividualOrderInfoList[0][12];//['ExpectedShipDate'];
													ActualArrivalDate = IndividualOrderInfoList[0][13];//['ActualArrivalDate'];

													PlannedArrivalDate = IndividualOrderInfoList[0][43];//'PlannedArrivaDateTime'];
													nlapiLogExecution('ERROR', 'Planeed Arrival date and time',PlannedArrivalDate);

													Destination = IndividualOrderInfoList[0][14];//['Destination'];
													Route = IndividualOrderInfoList[0][15];//['Route'];
													nlapiLogExecution('ERROR', '1',Route);
													Class = IndividualOrderInfoList[0][17];//['class'];
													ConsigneeId = IndividualOrderInfoList[0][18]; //['ConsigneeId'];
													Billaddressee = IndividualOrderInfoList[0][19];//['BillAddressee'];
													Billaddr1 = IndividualOrderInfoList[0][20]; //['Billaddr1'];
													Billaddr2 = IndividualOrderInfoList[0][21];//['Billaddr2'];
													Billaddr3 = IndividualOrderInfoList[0][22];//['Billaddr3'];
													Billcity = IndividualOrderInfoList[0][23];//['BillCity'];
													BillState = IndividualOrderInfoList[0][24];//['BillState'];
													Billzip = IndividualOrderInfoList[0][25];//['Billzip'];
													Billcountry = IndividualOrderInfoList[0][26];//['BillCountry'];
													Billphone = IndividualOrderInfoList[0][27];//['BillPhone'];
													location = IndividualOrderInfoList[0][28];//['Location'];
													Company = IndividualOrderInfoList[0][29]; //['Company'];
													Department = IndividualOrderInfoList[0][30];// ['Department'];
													Terms = IndividualOrderInfoList[0][31]; //['Terms'];
													OrderType = IndividualOrderInfoList[0][32]; //['OrderType'];
													OrderPriority = IndividualOrderInfoList[0][33]; //['OrderPriority'];
													CustomerPO = IndividualOrderInfoList[0][34];//['CustomerPO'];
													Shipphone = IndividualOrderInfoList[0][35];//['ShipPhone'];
													//Billphone = IndividualOrderInfoList[0][37];//['BillPhone'];
													ShipAddrID = IndividualOrderInfoList[0][40];//['ShipAddrID'];
												}									
												prevSOid=SOid;

												// Getting CUCC label data.
												if ( containerLP == prevcontainerLP){
													//No need to get the details, use the existing details
												}
												else{
													// Getting CUCC 
													IndividualCUCCDetailsList = GetContainerCUCCDetails(searchrecordClosedTask, containerLP);
													nlapiLogExecution('ERROR', 'Getting IndividualCUCCDetailsList',IndividualCUCCDetailsList);
													CUCC = IndividualCUCCDetailsList[0][1]; //['CUCC'];
													Conainercube = IndividualCUCCDetailsList[0][2];//['ContainerCube'];
													ContainerWeight = IndividualCUCCDetailsList[0][3]; //['ContainerWeight'];
													//Getting Shipmanifest Details
													//GetIndividualShipmanifestDetails
													IndividualShipmanfDetList = GetShipmanifestDetails("", containerLP);
													if (IndividualShipmanfDetList != null && IndividualShipmanfDetList.length > 0) {
														TrackingNumbers = IndividualShipmanfDetList[0][1];//['TrackingNumbers'];
														ShippingCharges = IndividualShipmanfDetList[0][2];//['ShippingCharges'];

														if (ShippingCharges == null	 || ShippingCharges == "" || ShippingCharges == "0.0")
															ShippingCharges = 0;
													}

												} // end if -contlp
												prevcontainerLP = containerLP;
												// This code is moved by sudheer. To filter item and contlp combination
//												Getting EUCC
												//nlapiLogExecution('ERROR', 'Getting EUCC details with contlp',containerLP);
												//IndividualEUCCDetailsList = GetContainerEUCCDetails(searchrecordClosedTask, containerLP,ItemID);
												//nlapiLogExecution('ERROR', 'IndividualEUCCDetailsList',IndividualEUCCDetailsList);
//												if (IndividualEUCCDetailsList != null && IndividualEUCCDetailsList.length > 0) {
//												EUCC = IndividualEUCCDetailsList; //['EUCC'];
//												nlapiLogExecution('ERROR', 'EUCC',EUCC);
//												}
												// Getting SUCC Details for shipLP
												if ( shipLP == prevshipLP)
												{	//No need of calling API and use existing values.
												}
												else
												{
													IndividualSUCCDetailsList = GetContainerSUCCDetails(shipLP);

													if (IndividualSUCCDetailsList != null
															&& IndividualSUCCDetailsList.length > 0) {

														SUCC = IndividualSUCCDetailsList[0][1]; //['SUCC'];
														ShipunitCube = IndividualSUCCDetailsList[0][2]; //['Shipunitcube'];
														ShipunitWeight = IndividualSUCCDetailsList[0][3]; //['ShipunitWeight'];
													}
												}
												prevshipLP=shipLP;
												// Getting Container Dims
												//nlapiLogExecution('ERROR', 'ContainerSizeId' , ContainerSizeId);
												//nlapiLogExecution('ERROR', 'prevcontainer' , prevcontainer);
												if ( ContainerSizeId == prevcontainer)
												{// no need to call API.
												}
												else
												{
													IndividualContainerDimslist=  GetContainerDims(ContainerSizeId);
													nlapiLogExecution('ERROR', 'Getindividualcondims-IndividualContainerDimslist',IndividualContainerDimslist);
													if (IndividualContainerDimslist != null && IndividualContainerDimslist.length > 0) {
														ContainerLength = IndividualContainerDimslist[0][0];//['ContainerLength'];
														ContainerWidth = IndividualContainerDimslist[0][1];//['ContainerWidth'];
														ContainerHeight = IndividualContainerDimslist[0][2];//['ContainerHeight'];
														ContainerMaxWeight = IndividualContainerDimslist[0][3];//['ContainerMaxWeight'];
														ContainerTareWeight = IndividualContainerDimslist[0][4];//['ContainerTareWeight'];
														ContainerCube = IndividualContainerDimslist[0][5];//['ContainerCube'];
													}
												}
												prevcontainer = ContainerSizeId;
												// Getting Trailer Details
												//commented by mahesh bcoz no where it's using 
												//IndividualTrailerDetails = GetTrailerDetails(trailer);
//												if (TrailerDetails != null
//												&& TrailerDetails.length > 0) {

												Pronum =ebizpro;// TrailerDetails[0][0];//['ProNo'];
												SealNo = ebizseal;//TrailerDetails[0][1];//['Seal'];
												MasterShipper = ebizmastershipper;//TrailerDetails[0][2];//['MasterShipper'];
												TrailerAppointNo =ebizappointmenttrailer;// TrailerDetails[0][3];//['TrailerAppointNo'];
												//}											

												if ( PlannedArrivalDate == null || PlannedArrivalDate == "")
												{
													PlannedArrivalDate = ebizexparrivaldate;//TrailerDetails[0][4];//['TrailerAppointNo']; // If sales order planned arrival date is null get the value from trailer exp arrival date
												}

												if(newcontlp == containerLP && ItemID == previtemId)
												{
													//nlapiLogExecution('ERROR', 'into if', 'done');
													nlapiSubmitField('customrecord_ebiznet_trn_ebiztask',TaskInternalID, 'custrecord_ebiztask_hostid', 'SPS');
												}
												else {
	
													
													var Resdummylpdetails = getdummylpdetails(containerLP,ItemID,'-1');

													if(  Resdummylpdetails !=null){
														nlapiLogExecution('ERROR', 'Resdummylpdetails.length', Resdummylpdetails.length);}
													if(OrderType == 'Standard' && operatingUnit == 'Finished Goods' ){

														for ( var i = 0 ; Resdummylpdetails !=null && i < Resdummylpdetails.length; i++ )
														{
															UCC = Resdummylpdetails[i].getValue('custrecord_ebiz_lpmaster_sscc');							
															LP = Resdummylpdetails[i].getValue('custrecord_ebiz_lpmaster_lp');

															//var createRec = nlapiCreateRecord('customrecord_ebiznetinterfaceasnc');

															parent.selectNewLineItem('recmachcustrecord_ebiz_stagetable_parent');											


															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ebizordno', ebizOrderNo);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_delivery_item_custfield0', customField);
															//parent.setCurrentLineItemValue('custrecord_ebiz_asnc_packcode', packCode);

															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_delivery_custfield_000',EDIREF);

															//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',SCACCode);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',scaccodetrlr);															
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunitdimsuom', 'Inches');
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carriermode','LTL');
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierservicelevel',Shipmethod);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_fobpaycode',FOB);
															//nlapiLogExecution('ERROR', ' before consignee Route', Route);
															//nlapiLogExecution('ERROR', ' ConsigneeId', ConsigneeId);

															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_pickauth_no',Route);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerdimsuom', 'Inches');
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_skudimsuom','Inches');
															if (vendor == null || vendor == "") {
																vendor = ItemID;
															}
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_skuvendor',vendor);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_vendor',ShipmentVendor);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_delivery_uom','EACH');
															// Added by Sudheer to populate all sku dims cubes
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube',UOMCube1);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube2',UOMCube2);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube3',UOMCube3);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube4',UOMCube4);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube5',UOMCube5);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_cont_cube',ContainerCube);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sealno', SealNo);
															//parent.setCurrentLineItemValue('custrecord_ebiz_bol', MasterBOL[0]);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_bol', MasterShipper);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_waybill_ref',MasterShipper);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_temp_cntrl',TempCntrl);
															if (ShipAddressee == "" || ShipAddressee == null)
																ShipAddressee = ConsigneeId;
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipto_contact',ShipAddressee);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_markfor', 'N');
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','name', shipLP);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_contwght_lbs',
																	ContainerWeight);
															//parent.setCurrentLineItemValue('custrecord_ebiz_shipunitwght_lbs',	ShipunitWeight);
															//commented by mahesh as per sravan sir request
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunitwght_lbs',	salesordWeight);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiztrailerno',trailer);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizdepartment',Department);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_class', Class);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizshipunitid',shipLP);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierid',Shipmethod);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrier_name',Shipmethod);
															if (Route == "" || Route == null)
																Route = Destination;
															// custbody_locationaddressid
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_route', Route);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_dest',Destination);
															if(Pronum == null || Pronum == "")
																Pronum=MasterBOL[1];
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_pronum',Pronum);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiploc_id',ShipAddrID);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptoadd1',ShipAddress1);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptoadd2',ShipAddress2);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptocity',Shipcity);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptostate',ShipState);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptozip',Shipzip);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptocountry', ShipCountry);
															//parent.setCurrentLineItemValue('custrecord_ebiz_shiptophone',ShiptoPhone);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptophone',Shipphone);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptoemail',ShiptoEmail); // *********
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptofax',ShiptoFax); 							
															if(ActualArrivalDate =="" || ActualArrivalDate == null) // Added by Mahesh
																ActualArrivalDate=sysdate;
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_expected_arrivaldate',ActualArrivalDate);

															if(PlannedArrivalDate =="" || PlannedArrivalDate == null) // Added by Sudheer
																PlannedArrivalDate=sysdate;
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_planned_arrival_datetime',PlannedArrivalDate);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerlp',LP);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerlp_charges',ShippingCharges);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_trackingno',TrackingNumbers);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_issue_period',IssueDate);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_payment_terms', Terms);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_length',ContainerLength);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_width',ContainerWidth);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_height',ContainerHeight);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_weight',ContainerMaxWeight);
															//nlapiLogExecution('ERROR', 'tranid before insert',tranid);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_deliveryid',tranid);
															//nlapiLogExecution('ERROR', 'Order type is  ', OrderType);		
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_order_type',OrderType);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_order_priority',OrderPriority);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_consigneeid',ConsigneeId);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billinginfo_locationid',Billaddr3);
															if (Billaddressee == "" || Billaddressee == null)
																Billaddressee = ConsigneeId;
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billto_contactname',Billaddressee);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billto_addr1',Billaddr1);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billto_addr2',Billaddr2);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtocity',Billcity);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtostate',BillState);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtozip',Billzip);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtocountry', Billcountry);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtophone',Billphone);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtoemail',ShiptoEmail); // ******************
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtofax',ShiptoFax);// *******************
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_customer_pono', CustomerPO);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_host_ordno',SOid);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_deliveryline_no', lineno);
															//nlapiLogExecution('ERROR', 'skuid internal ', skuid);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_item_internalid', skuid);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku', ItemID);
															//nlapiLogExecution('ERROR', 'skuid internal ', ItemID);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_desc', SKUDesc);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_family', SKUFamily);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_group', SKUGroup);
															if(SKUtype == 'undefined' || SKUtype== "" || SKUtype == null)
																SKUtype="";
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_type', SKUtype);

															// ATTN - Ratnakar 3 Aug 2012; Commenting the if condition to ensure that the buyer part number is always populated 
															// in the ASNC stage custom record.
//															if (prevlineno == lineno) {
//																// No need of calling salsorder alternateid
//															} else {

																// call get aternateid for line
															//alternateid= GetAlternateIDforOrderLine(SOid,lineno);
															//nlapiLogExecution('ERROR', 'allalternateids', allalternateids);
															//nlapiLogExecution('ERROR', 'lineno', lineno);
															//nlapiLogExecution('ERROR', 'SOid', SOid);
															alternateid=GetIndividualabuyerpart( allalternateids,lineno,SOid);
															//nlapiLogExecution('ERROR', 'alternateid', alternateid);
//															}
															prevlineno = lineno;
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_buyer_partnumber',	alternateid); 
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_upc_code',upcCode);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_hazmat_code',HazmatCode);
															if (BatchId != null && BatchId != "")
															{
																parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_batch_id',BatchId);
																parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_batch_delivery_qty', Actqty);
															}
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_serial_id',SerilalId);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_quantity',Actqty);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id', UOM1);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty',UOMQty1);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length',UOMLength1);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width',UOMWidth1);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight',UOMWeight1);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height',UOMHeight1);

															if ( packCode == null || packCode =="")  // If packcode is null , defaulting to 1.
																packCode="1";
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_packcode', packCode);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id2', UOM2);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty2',UOMQty2);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length2',UOMLength2);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width2',UOMWidth2);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_wght2',UOMWeight2);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_hght2',UOMHeight2);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id3', UOM3);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty3',UOMQty3);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length3',UOMLength3);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width3',UOMWidth3);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight3',UOMWeight3);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height3',UOMHeight3);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id4', UOM4);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty4',UOMQty4);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length4',UOMLength4);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width4',UOMWidth4);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight4',UOMWeight4);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height4',UOMHeight4);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id5', UOM5);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty5',UOMQty5);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length5',UOMLength5);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width5',UOMWidth5);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight5',UOMWeight5);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height5',UOMHeight5);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_wh_location',location);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_company',Company);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_container_levelqty', Actqty);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_s_ucc128',SUCC);

															if (EUCC != null && EUCC != "")
																EUCC = EUCC.substring(0, EUCC.length - 1);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_c_ucc128',UCC);								
															//parent.setCurrentLineItemValue('custrecord_ebiz_eucc128', EUCC);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_eucc128', UCC);

															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiznetshipunitlgth',ContainerLength);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunit_width',ContainerWidth);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunit_height',ContainerHeight);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunit_weight',ShipunitWeight);
															//parent.setCurrentLineItemValue('custrecord_ebiz_ship_cube',ShipunitCube);
															//commented by mahesh as per sravan sir request
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_cube',salesordCube);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipunitcharges',ShipUnitCharges);
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipunit_levelqty',Actqty);

															// The below line is added by sudheer to create records with INPROGRESS STATUS
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_status_flag',"E"); 
															//var TaskID = searchrecordClosedTask[count].getId();

															parent.commitLineItem('recmachcustrecord_ebiz_stagetable_parent');
														} 
														nlapiSubmitField('customrecord_ebiznet_trn_ebiztask',TaskInternalID, 'custrecord_ebiztask_hostid', 'SPS');


													}
												}
												newcontlp = containerLP;
												previtemId=ItemID;
											} // End of closed task for loop
											nlapiSubmitField('customrecord_ebiznet_trn_opentask',shiplpsearchresults[q].getId(), 'custrecord_hostid', 'SPS');
										}
									}									
									parent.selectNewLineItem('recmachcustrecord_ebiz_asn_parent');
									parent.setCurrentLineItemValue('recmachcustrecord_ebiz_asn_parent','custrecord_parent_orderno', SOid);
									parent.setCurrentLineItemValue('recmachcustrecord_ebiz_asn_parent','custrecord_parent_deliveryid', SOidText);
									parent.commitLineItem('recmachcustrecord_ebiz_asn_parent');									
								} //write
								nlapiLogExecution('ERROR', 'Before inserting into stage table','done');
								nlapiSubmitRecord(parent); 

								nlapiLogExecution('ERROR', 'After updating ebiz status flag = E  and Out of Forloop - Current Available Usage', nlapiGetContext().getRemainingUsage());
							}

						}
					}
				}
				else
				{
					nlapiLogExecution('ERROR', 'Records','Records exist with pick task in open task');
				}
			}// end if wmsStatusflag == 10
			else {
				nlapiLogExecution('ERROR', 'Records','New Rec doesnot exist to Create SPS');
			}
		} // End if if (type == 'edit')

		//End for Try 
		catch (exp) {
			nlapiLogExecution('ERROR', 'Exeception', exp);
		}

		nlapiLogExecution('ERROR', 'AsncConfirmationUE', 'End');
}
function GetIndividualcustomFieldInfo(skulist, sku ){

	var IndividualSKUInfoList = new Array();

	var IndividualSKUInfoListT = new Array();
	nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', 'Start');
	nlapiLogExecution('ERROR', 'skulist', skulist);
	nlapiLogExecution('ERROR', 'sku', sku);
	try{

		if (skulist != null && skulist.length >0 && sku != null && sku.length>0){
			for ( var i = 0 ; i < skulist.length ; i++){

				if (skulist[i][0]  == sku )
				{
					IndividualSKUInfoList = new Array();
					nlapiLogExecution('ERROR', 'sku matches..internal id is', skulist[i][0]);
					IndividualSKUInfoList[0] = skulist[i][1]; 					
					IndividualSKUInfoListT.push(IndividualSKUInfoList);
					i = skulist.length; 	
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', exception);

	}

	nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', 'end');
	nlapiLogExecution('ERROR', 'IndividualSKUInfoListT', IndividualSKUInfoListT);
	return IndividualSKUInfoListT;
}
function GetIndividualUPCInfo(skulist, sku ){

	var IndividualSKUInfoList = new Array();

	var IndividualSKUInfoListT = new Array();
	nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', 'Start');
	nlapiLogExecution('ERROR', 'skulist', skulist);
	nlapiLogExecution('ERROR', 'sku', sku);
	try{

		if (skulist != null && skulist.length >0 && sku != null && sku.length>0){
			for ( var i = 0 ; i < skulist.length ; i++){

				if (skulist[i][0]  == sku )
				{
					IndividualSKUInfoList = new Array();
					nlapiLogExecution('ERROR', 'sku matches..internal id is', skulist[i][0]);
					IndividualSKUInfoList[0] = skulist[i][1]; 					
					IndividualSKUInfoListT.push(IndividualSKUInfoList);
					i = skulist.length; 	
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', exception);

	}

	nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', 'end');
	nlapiLogExecution('ERROR', 'IndividualSKUInfoListT', IndividualSKUInfoListT);
	return IndividualSKUInfoListT;
}
function GetAllRecentRecords(OrdersEligibleForASNGeneration,trailername){
	nlapiLogExecution('ERROR', 'into GetAllRecentRecords', OrdersEligibleForASNGeneration);
	nlapiLogExecution('ERROR', 'into GetAllRecentRecords trailername', trailername);
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_ebizordno', null, 'anyof', OrdersEligibleForASNGeneration));
	filters.push(new nlobjSearchFilter('custrecord_ebiztrailerno', null, 'is', trailername));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_status_flag', null, 'is', 'I'));

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_ebizordno');
	columns[1] = new nlobjSearchColumn('custrecord_ebiztrailerno');
	columns[2] = new nlobjSearchColumn('internalid');
	columns[3] = new nlobjSearchColumn('name');

	var ResDetails = nlapiSearchRecord('customrecord_ebiznetinterfaceasnc', null, filters,columns);
	nlapiLogExecution('ERROR', 'ResDetails', ResDetails);
	return ResDetails;
}

/* This function returns sales order details */
function GetSalesOrderDetails(closedTaskList,salesOrderList){
	nlapiLogExecution('ERROR', 'GetSalesOrderDetails', 'Start');



	var salesorderDetailListT = new Array();
	try
	{
		//12344444
		var salesorderDetailList = new Array();
		//commented by mahesh
		//var salesOrderList = getDistinctOrderIDsFromTaskList(closedTaskList);

		nlapiLogExecution('ERROR', 'SOList', salesOrderList);

		//salesOrderList = ProvideInputs(salesOrderList,'List');

		//nlapiLogExecution('ERROR', 'SOList with Provideinputs', salesOrderList);

		var filters = new Array();
		var columns = new Array();


		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', salesOrderList));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		// Getting sku type is pending


		columns[0] = new nlobjSearchColumn('tranid');
		columns[1] = new nlobjSearchColumn('custbody_edi_ref_mr');
		////var alternateid=salesorderRec.getLineItemValue('item','custcol13',lineno);
		/// carrieroptions
		///Shipping method
		columns[2] = new nlobjSearchColumn('custbody_dynacraftvendornumber');
		columns[3] = new nlobjSearchColumn('shipmethod');
		columns[4] = new nlobjSearchColumn('custbody_edi_fob');
		columns[5] = new nlobjSearchColumn('shipaddressee');
		columns[6] = new nlobjSearchColumn('shipaddress1');
		columns[7] = new nlobjSearchColumn('shipaddress2');
		columns[8] = new nlobjSearchColumn('shipcity');
		columns[9] = new nlobjSearchColumn('shipstate');
		columns[10] = new nlobjSearchColumn('shipcountry');
		columns[11] = new nlobjSearchColumn('shipzip');
		columns[12] = new nlobjSearchColumn('custbody_nswmspoexpshipdate');
		columns[13] = new nlobjSearchColumn('custbody_nswmsactualarrivaldate');
		columns[14] = new nlobjSearchColumn('custbody_nswmssodestination');
		columns[15] = new nlobjSearchColumn('custbody_nswmssoroute');
		columns[16] = new nlobjSearchColumn('class');
		columns[17] = new nlobjSearchColumn('entity');
		columns[18] = new nlobjSearchColumn('billaddressee');
		columns[19] = new nlobjSearchColumn('billaddress1');
		columns[20] = new nlobjSearchColumn('billaddress2');
		columns[21] = new nlobjSearchColumn('billaddress3');
		columns[22] = new nlobjSearchColumn('billcity');
		columns[23] = new nlobjSearchColumn('billstate');
		columns[24] = new nlobjSearchColumn('billzip');
		columns[25] = new nlobjSearchColumn('billcountry');
		columns[26] = new nlobjSearchColumn('billphone');
		columns[27] = new nlobjSearchColumn('location');
		columns[28] = new nlobjSearchColumn('custbody_nswms_company');
		columns[29] = new nlobjSearchColumn('department');
		columns[30] = new nlobjSearchColumn('terms');
		columns[31] = new nlobjSearchColumn('custbody_nswmssoordertype');		                                   
		columns[32] = new nlobjSearchColumn('custbody_nswmspriority');
		columns[33] = new nlobjSearchColumn('otherrefnum');
		columns[34] = new nlobjSearchColumn('shipphone');
		columns[35] = new nlobjSearchColumn('custbody_locationaddressid');
		columns[36] = new nlobjSearchColumn('shipphone');
		columns[37] = new nlobjSearchColumn('custbody_nswmsarrivaldate');
		columns[38] = new nlobjSearchColumn('internalid');		
		columns[39] = new nlobjSearchColumn('class');
		columns[40] = new nlobjSearchColumn('custbody_shipment_no');

		columns[41] = new nlobjSearchColumn('custbody_total_weight');
		columns[42] = new nlobjSearchColumn('custbody_total_vol');	


		//235235,235233,235234

		var ResSSODetails = nlapiSearchRecord('salesorder', null, filters,columns);
		nlapiLogExecution('ERROR', 'ResSSODetails Length', ResSSODetails.length);

		if ( ResSSODetails !=null && ResSSODetails.length > 0)
		{
			for ( var i = 0 ; i < ResSSODetails.length; i++ ){
				salesorderDetailList = new Array();

				salesorderDetailList[0] = ResSSODetails[i].getValue('tranid');
				salesorderDetailList[1] = ResSSODetails[i].getValue('custbody_edi_ref_mr');
				salesorderDetailList[2] = ResSSODetails[i].getValue('custbody_dynacraftvendornumber');
				salesorderDetailList[3] = ResSSODetails[i].getText('Shipmethod');
				salesorderDetailList[4] = ResSSODetails[i].getValue('Shipmethod');
				salesorderDetailList[5]=  ResSSODetails[i].getValue('custbody_edi_fob');
				salesorderDetailList[6] = ResSSODetails[i].getValue('shipaddressee');
				salesorderDetailList[7] = ResSSODetails[i].getValue('shipaddress1');
				nlapiLogExecution('ERROR', 'shipaddr1', ResSSODetails[i].getValue('shipaddr1'));
				nlapiLogExecution('ERROR', 'shipaddr2', ResSSODetails[i].getValue('shipaddr2'));
				salesorderDetailList[8] = ResSSODetails[i].getValue('shipaddress2');
				salesorderDetailList[9] = ResSSODetails[i].getValue('shipcity');
				salesorderDetailList[10] = ResSSODetails[i].getValue('shipstate');
				salesorderDetailList[11] = ResSSODetails[i].getValue('shipcountry');
				salesorderDetailList[12] = ResSSODetails[i].getValue('shipzip');
				salesorderDetailList[13] = ResSSODetails[i].getValue('custbody_nswmspoexpshipdate');
				salesorderDetailList[14] = ResSSODetails[i].getValue('custbody_nswmsactualarrivaldate');
				salesorderDetailList[15] = ResSSODetails[i].getText('custbody_nswmssodestination');
				//salesorderDetailList[16] = ResSSODetails[i].getText('custbody_nswmssoroute');
				salesorderDetailList[16] = ResSSODetails[i].getValue('custbody_shipment_no');
				nlapiLogExecution('ERROR', 'salesorderDetailList[16]',salesorderDetailList[16]);
				nlapiLogExecution('ERROR', 'ResSSODetails[i].getText',ResSSODetails[i].getText('custbody_shipment_no'));
				salesorderDetailList[17] = ResSSODetails[i].getValue('shipzip');
				salesorderDetailList[18] = ResSSODetails[i].getText('class');
				salesorderDetailList[19] = ResSSODetails[i].getText('entity');
				salesorderDetailList[20] = ResSSODetails[i].getValue('billaddressee');
				salesorderDetailList[21] = ResSSODetails[i].getValue('billaddress1');
				salesorderDetailList[22] = ResSSODetails[i].getValue('billaddress2');
				salesorderDetailList[23] = ResSSODetails[i].getValue('billaddress3');
				salesorderDetailList[24] = ResSSODetails[i].getValue('billcity');
				salesorderDetailList[25] = ResSSODetails[i].getValue('billstate');
				salesorderDetailList[26] = ResSSODetails[i].getValue('billzip');
				salesorderDetailList[27] = ResSSODetails[i].getValue('billcountry');
				salesorderDetailList[28] = ResSSODetails[i].getValue('billphone');
				salesorderDetailList[29] = ResSSODetails[i].getText('Location');
				salesorderDetailList[30] = ResSSODetails[i].getText('custbody_nswms_company');
				salesorderDetailList[31] = ResSSODetails[i].getText('department');
				salesorderDetailList[32]=  ResSSODetails[i].getText('terms');
				//salesorderDetailList[33]=  ResSSODetails[i].getText('custbody_nswmsordertype');
				salesorderDetailList[33]=  ResSSODetails[i].getText('custbody_nswmssoordertype');
				salesorderDetailList[34] = ResSSODetails[i].getText('custbody_nswmspriority');
				salesorderDetailList[35] = ResSSODetails[i].getValue('otherrefnum');
				salesorderDetailList[36] = ResSSODetails[i].getValue('shipphone');
				salesorderDetailList[37] = ResSSODetails[i].getValue('billphone');
				salesorderDetailList[38] = ResSSODetails[i].getText('custbody_nswmspriority');
				salesorderDetailList[39] = ResSSODetails[i].getValue('otherrefnum');
				//salesorderDetailList[40]=  ResSSODetails[i].getValue('shipmethod').getSelectOptions(null, null);
				salesorderDetailList[41] = ResSSODetails[i].getValue('custbody_locationaddressid');
				//salesorderDetailList[42] = ResSSODetails[i].getValue('ShiptoPhone');
				salesorderDetailList[43] = ResSSODetails[i].getValue('custbody_nswmsarrivaldate');
				salesorderDetailList[44] = ResSSODetails[i].getValue('internalid');
				salesorderDetailList[45] = ResSSODetails[i].getText('class');

				salesorderDetailList[46] = ResSSODetails[i].getValue('custbody_total_weight');
				salesorderDetailList[47] = ResSSODetails[i].getValue('custbody_total_vol');	

				salesorderDetailListT.push(salesorderDetailList);
			}
		}
		// Get details from item master against the given item

	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'GetSalesOrderDetails', exception);
	}

	nlapiLogExecution('ERROR', 'GetSalesOrderDetails', 'end');
	nlapiLogExecution('ERROR', 'salesorderDetailList', salesorderDetailList);

	return salesorderDetailListT;

}

function GetMasterBOL(salesOrderList)
{
	var masterbol="",opentaskpronumber="";
	var arrbolpronum = new Array();
	var bolfilters = new Array();		
	bolfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));
	if(salesOrderList !=null && salesOrderList !=""){
		bolfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', salesOrderList));	}	

	var bolcolumns = new Array();
	bolcolumns[0] = new nlobjSearchColumn('custrecord_bol');  
	bolcolumns[1] = new nlobjSearchColumn('custrecord_pro');  
	searchresultsbol = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, bolfilters, bolcolumns);
	if( searchresultsbol != null &&  searchresultsbol != "")
	{
		masterbol=searchresultsbol[0].getValue('custrecord_bol');
		opentaskpronumber=searchresultsbol[0].getValue('custrecord_pro');
		arrbolpronum.push(masterbol);
		arrbolpronum.push(opentaskpronumber);
	}

	return arrbolpronum;
}

/* This function returns shipmanifest details for the contlp */
function GetShipmanifestDetails(closedTaskList, contlp){
	nlapiLogExecution('ERROR', 'GetShipmanifestDetails', 'Start');


	var shipmanifestDetailsListT = new Array();
	try
	{
		//var salesOrderList = getDistinctOrderIDsFromTaskList(closedTaskList);

		var filters = new Array();
		var columns = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ship_contlp', null, 'is', contlp));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ship_charges');
		columns[1] =  new nlobjSearchColumn('custrecord_ship_trackno');
		columns[2] = new nlobjSearchColumn('custrecord_ship_contlp');
		columns[3] = new nlobjSearchColumn('custrecord_ship_orderno');


		var ResShipmanfDetails = nlapiSearchRecord('customrecord_ship_manifest', null, filters,columns);

		if ( ResShipmanfDetails !=null && ResShipmanfDetails.length > 0){
			for ( var i = 0 ; i < ResShipmanfDetails.length; i++ ){
				var shipmanifestDetailsList = new Array();
				shipmanifestDetailsList[0] = ResShipmanfDetails[i].getValue('custrecord_ship_contlp');
				shipmanifestDetailsList[1] = ResShipmanfDetails[i].getValue('custrecord_ship_trackno');
				shipmanifestDetailsList[2] = ResShipmanfDetails[i].getValue('custrecord_ship_charges');
				shipmanifestDetailsList[3] = ResShipmanfDetails[i].getValue('custrecord_ship_orderno');
				shipmanifestDetailsListT.push(shipmanifestDetailsList);
			}
		}
	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'Get GetShipmanifestDetails', exception);
	}

	nlapiLogExecution('ERROR', 'GetShipmanifestDetails', 'end');
	nlapiLogExecution('ERROR', 'shipmanifestDetailsListT', shipmanifestDetailsListT);
	return shipmanifestDetailsListT;

}

/* This function returns shipmanifest details for all the distinct orders */
function GetIndividualShipmanifestDetails(shipmanifestDetails, contlp) {
	nlapiLogExecution('ERROR', 'GetIndividualShipmanifestDetails', 'Start');
	var IndividualShipmanifestDetailsT = new Array();
	try {

		if (shipmanifestDetails != null && shipmanifestDetails.length > 0) {
			for ( var i = 0; i < shipmanifestDetails.length; i++) {
				if (IndividualShipmanifestDetails[i]['ContLP'] == contlp) {
					var IndividualShipmanifestDetails = new Array();
					IndividualShipmanifestDetails[0] = shipmanifestDetails[i]
					.getValue('TrackingNumbers');
					IndividualShipmanifestDetails[1] = shipmanifestDetails[i]
					.getValue('ShippingCharges');
					IndividualShipmanifestDetailsT
					.push(IndividualShipmanifestDetails);
				}
			}
		}
	}

	catch (exception) {
		nlapiLogExecution('ERROR', ' GetIndividualShipmanifestDetails',
				exception);
	}

	nlapiLogExecution('ERROR', 'GetIndividualShipmanifestDetails', 'end');
	return IndividualShipmanifestDetails;

}


/* This function returns SCAC code of the carrier */
function ReturnSCACCode(ShipCarrierOptions,  ShipMethodActValue)
{
	nlapiLogExecution('ERROR', 'ReturnSCACCode', 'Start');
	nlapiLogExecution('ERROR', 'ShipmethodActvalue', ShipMethodActValue);
	nlapiLogExecution('ERROR', 'ShipCarrierOptions', ShipCarrierOptions);
	var shipItemCode="";
	try {

		var ShipItemID ="";
		var shipItemText="";
		for(var i=0; i<ShipCarrierOptions.length; i++){
			ShipItemID = ShipCarrierOptions[i].getId();
			shipItemText = ShipCarrierOptions[i].getText();

			if ( ShipItemID == ShipMethodActValue)
			{
				var res = nlapiSearchGlobal(shipItemText);
				nlapiLogExecution('ERROR', 'shipItemText', shipItemText);

				if(res != null && res.length >0 &&  res[0].getValue('type') == 'Shipping Cost Item')
				{
					shipItemCode = res[0].getValue('info1');
					i=ShipCarrierOptions.length;
				}
			}
		}
	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'Getting SCAC code ', exception);
	}
	nlapiLogExecution('ERROR', 'Getting SCAC code -shipitemcode ', shipItemCode);
	return shipItemCode;

	nlapiLogExecution('ERROR', 'ReturnSCACCode', 'End');
}

function isDistinctSKU(skuList, sku) {
	var matchFound = false;
	nlapiLogExecution('ERROR', 'isDistinctSKU', 'start');
	try {

		if (skuList != null && skuList.length > 0) {
			for ( var i = 0; i < skuList.length; i++) {
				if (skuList[i] == sku)
					matchFound = true;
			}
		}
	} catch (exception) {
		nlapiLogExecution('ERROR', 'isDistinctSKU', exception);
	}
	nlapiLogExecution('ERROR', 'isDistinctSKU', 'end');
	return matchFound;
}

function isDistinctContainerSizeID(ContainerSizIDLIst, Containersizeid) {
	nlapiLogExecution('ERROR', 'isDistinctContainerSizeID', 'start');
	var matchFound = false;
	try {
		if (ContainerSizIDLIst != null && ContainerSizIDLIst.length > 0) {
			for ( var i = 0; i < ContainerSizIDLIst.length; i++) {
				if (ContainerSizIDLIst[i] == Containersizeid)
					matchFound = true;
			}
		}
	} catch (exception) {
		nlapiLogExecution('ERROR', 'isDistinctContainerSizeID', exception);
	}
	nlapiLogExecution('ERROR', 'isDistinctContainerSizeID', 'end');
	return matchFound;
}



function isDistinctOrderID(OrderIDList, OrderID){
	var matchFound = false;
	if(OrderIDList != null && OrderIDList.length > 0){
		for(var i = 0; i < OrderIDList.length; i++){
			if(OrderIDList[i] == OrderID)
				matchFound = true;
		}
	}

	return matchFound;
}

function isDistinctContainer(ContainerIDList, ContLP){
	var matchFound = false;
	if(ContainerIDList != null && ContainerIDList.length > 0){
		for(var i = 0; i < ContainerIDList.length; i++){
			if(ContainerIDList[i] == ContLP)
				matchFound = true;
		}
	}

	return matchFound;
}

function getDistinctContLPsFromTaskList(closedTaskList){
	nlapiLogExecution('ERROR', 'getDistinctContLPsFromTaskList', 'Start');
	var ContLPList = new Array();

	if(closedTaskList != null && closedTaskList.length > 0){
		for(var i = 0; i < closedTaskList.length; i++){
			var ContLP = closedTaskList[i].getValue('custrecord_ebiztask_ebiz_contlp_no');
			if(!isDistinctContainer(ContLPList, ContLP))
				ContLPList.push(ContLP);
		}
	}

	nlapiLogExecution('ERROR', 'getDistinctContLPsFromTaskList', 'End');
	nlapiLogExecution('ERROR', 'ContLPList', ContLPList);
	return ContLPList;
}
/* This function returns disict container sizeids for the shipLP */
function getDistinctContainerSizeIDsFromTaskList(closedTaskList){
	nlapiLogExecution('ERROR', 'getDistinctContainerSizeIDsFromTaskList', 'Start');
	var ContainerSizeIDList = new Array();
	try{

		if(closedTaskList != null && closedTaskList.length > 0)
		{
			for(var i = 0; i < closedTaskList.length; i++)
			{
				var ContainerSizeID = closedTaskList[i].getValue('custrecord_container');
				if(!isDistinctContainerSizeID(ContainerSizeIDList, ContainerSizeID))
					ContainerSizeIDList.push(ContainerSizeID);
			}
		}

		nlapiLogExecution('ERROR', 'getDistinctContainerSizeIDsFromTaskList', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'getDistinctContainerSizeIDsFromTaskList', exception);
	}
	nlapiLogExecution('ERROR', 'getDistinctContainerSizeIDsFromTaskList', 'End');
	return ContainerSizeIDList;

}

/**
 * Function will take container size id as input and return the dims.
 * @param skulist
 * @returns {Array}
 */
function GetContainerDims(ContainerSizeIDs){
	nlapiLogExecution('ERROR', 'GetContainerDims', 'Start');
	var ContainerDims = new Array();
	var ContainerDimsT = new Array();
	try{
		// Get the list of distinct SKUs
		//var ContainerSizeIDs = getDistinctContainerSizeIDsFromTaskList(closedTaskList);
		//nlapiLogExecution('ERROR', 'GetContainerDims-Containersizeids', ContainerSizeIDs);
		var filters = new Array();
		var columns = new Array();
		filters.push(new nlobjSearchFilter('internalid' , null, 'is',ContainerSizeIDs));
		// Getting sku type is pending

		columns[0] = new nlobjSearchColumn('custrecord_length');
		columns[1] = new nlobjSearchColumn('custrecord_widthcontainer');
		columns[2] = new nlobjSearchColumn('custrecord_heightcontainer');
		columns[3] = new nlobjSearchColumn('custrecord_maxweight');
		columns[4] = new nlobjSearchColumn('custrecord_tareweight');
		columns[5] = new nlobjSearchColumn('custrecord_cubecontainer');
		columns[6] = new nlobjSearchColumn('internalid');

		var ResContainerDims = nlapiSearchRecord('customrecord_ebiznet_container', null,filters, columns);

		if(ResContainerDims != null && ResContainerDims.length > 0){
			for(var i = 0; i < ResContainerDims.length; i++ ){
				ContainerDims = new Array();
				ContainerDims[0] = ResContainerDims[i].getValue('custrecord_length');
				ContainerDims[1] = ResContainerDims[i].getValue('custrecord_widthcontainer');
				ContainerDims[2] = ResContainerDims[i].getValue('custrecord_heightcontainer');
				ContainerDims[3] = ResContainerDims[i].getValue('custrecord_maxweight');
				ContainerDims[4] = ResContainerDims[i].getValue('custrecord_tareweight');
				ContainerDims[5] = ResContainerDims[i].getValue('custrecord_cubecontainer');
				ContainerDims[6] = ResContainerDims[i].getValue('internalid');
				ContainerDimsT.push(ContainerDims);
			}

		}
	}catch(exception){
		nlapiLogExecution('ERROR', 'GetContainerDims', exception);
	}

	nlapiLogExecution('ERROR', 'GetContainerDims', 'end');
	nlapiLogExecution('ERROR', 'ContainerDimsT', ContainerDimsT);
	return ContainerDimsT;
}
/**
 * Function to retrieve the Container details for a list of ContainerSizeIDS
 * @param skulist
 * @returns {Array}
 */
function GetIndividualContainerDims(containerdims , containersizeid ){
	nlapiLogExecution('ERROR', 'GetIndividualContainerDims', 'Start');
	nlapiLogExecution('ERROR', 'containerdims', containerdims);
	nlapiLogExecution('ERROR', 'containersizeid', containersizeid);
	var IndividualContainerDimslist = new Array();
	var IndividualContainerDimslistT = new Array();
	try{
		if(containerdims != null && containerdims.length > 0){
			for(var i = 0; i < containerdims.length; i++ ){

				if (ContainerDims[i][6] == containersizeid){

					nlapiLogExecution('ERROR', 'containersizeid is matches ', ContainerDims[i][6]);

					IndividualContainerDimslist = new Array();
					IndividualContainerDimslist[0] = containerdims[i].getValue('ContainerLength');
					IndividualContainerDimslist[1] = containerdims[i].getValue('ContainerWidth');
					IndividualContainerDimslist[2] = containerdims[i].getValue('ContainerHeight');
					IndividualContainerDimslist[3] = containerdims[i].getValue('ContainerMaxWeight');	
					IndividualContainerDimslist[4] = containerdims[i].getValue('ContainerHeight');
					IndividualContainerDimslist[5] = containerdims[i].getValue('ContainerCube');
					IndividualContainerDimslistT.push(IndividualContainerDimslist);
				}
			}
		}
	}catch(exception){
		nlapiLogExecution('ERROR', 'GetIndividualContainerDims', exception);
	}

	nlapiLogExecution('ERROR', 'GetIndividualContainerDims', 'end');
	return IndividualContainerDimslist;
}


/*
 * This function returns SUCC, shipunit total cube , shipunit total weight
 * details
 */
function GetContainerSUCCDetails(shipLP){
	nlapiLogExecution('ERROR', 'GetContainerSUCCDetails', 'Start');


	var ShipunitDetailsListT = new Array();
	try
	{
		var filters = new Array();
		var columns = new Array();


		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp',  null, 'is',shipLP));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');

		var ResShipunitDetails = nlapiSearchRecord('customrecord_ebiznet_master_lp', null,	filters, columns);
		if ( ResShipunitDetails !=null && ResShipunitDetails.length > 0)
		{
			for ( var i = 0 ; i < ResShipunitDetails.length; i++ ){
				var ShipunitDetailsList = new Array();
				ShipunitDetailsList[0] = ResShipunitDetails[i].getValue('SHILP');
				ShipunitDetailsList[1] = ResShipunitDetails[i].getValue('custrecord_ebiz_lpmaster_sscc');
				ShipunitDetailsList[2] = ResShipunitDetails[i].getValue('custrecord_ebiz_lpmaster_totcube');
				ShipunitDetailsList[3] = ResShipunitDetails[i].getValue('custrecord_ebiz_lpmaster_totwght');
				ShipunitDetailsListT.push(ShipunitDetailsList);
			}
		}
	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', ' GetContainerSUCCDetails', exception);
	}

	nlapiLogExecution('ERROR', 'GetContainerSUCCDetails', 'end');
	nlapiLogExecution('ERROR', 'ShipunitDetailsListT',ShipunitDetailsListT);
	return ShipunitDetailsListT;

}
/*
 * This function returns SUCC, shipunit total cube , shipunit total weight
 * details
 */
function GetTrailerDetails(trailer){
	nlapiLogExecution('ERROR', 'GetTrailerDetails', 'Start');


	var TrailerDetailsListT = new Array();
	try
	{

		var filters = new Array();
		var columns = new Array();
		var filters = new Array();
		filters.push( new nlobjSearchFilter('name', null,'is', trailer));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizpro');
		columns[1] = new nlobjSearchColumn('custrecord_ebizseal');
		columns[2] = new nlobjSearchColumn('custrecord_ebizmastershipper');
		columns[3] = new nlobjSearchColumn('custrecord_ebizappointmenttrailer');
		columns[4] = new nlobjSearchColumn('custrecord_ebizexparrivaldate');
		var ResTrailerDet = nlapiSearchRecord('customrecord_ebiznet_trailer', null,filters, columns);
		if ( ResTrailerDet !=null && ResTrailerDet.length > 0){
			for ( var i = 0 ; i < ResTrailerDet.length; i++ ){
				var TrailerDetailsList = new Array();
				TrailerDetailsList[0] = ResTrailerDet[i].getValue('custrecord_ebizpro');
				TrailerDetailsList[1] = ResTrailerDet[i].getValue('custrecord_ebizseal');
				TrailerDetailsList[2] = ResTrailerDet[i].getValue('custrecord_ebizmastershipper');
				TrailerDetailsList[3] = ResTrailerDet[i].getValue('custrecord_ebizappointmenttrailer');
				TrailerDetailsList[4] = ResTrailerDet[i].getValue('custrecord_ebizexparrivaldate');

				TrailerDetailsListT.push(TrailerDetailsList);
			}
		}
	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', ' GetTrailerDetails', exception);
	}

	nlapiLogExecution('ERROR', 'GetTrailerDetails', 'end');
	nlapiLogExecution('ERROR', 'TrailerDetailsListT', TrailerDetailsListT);
	return TrailerDetailsListT;

}


/* This function returns Container CUCC , container total cube , container total weight details  for the contlp */
function GetContainerCUCCDetails(closedTaskList, contlp){
	nlapiLogExecution('ERROR', 'GetContainerCUCCDetails', 'Start');

	var ContainerDetailsList = new Array();
	var ContainerDetailsListT = new Array();

	try
	{
		//var ContLPDetails = getDistinctContLPsFromTaskList(closedTaskList);
		//nlapiLogExecution('ERROR', 'ContLPDetails', ContLPDetails);
		var filters = new Array();
		var columns = new Array();

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is',contlp));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');

		var ResContainerDetails = nlapiSearchRecord('customrecord_ebiznet_master_lp', null,	filters, columns);		
		if ( ResContainerDetails !=null && ResContainerDetails.length > 0)
		{
			for ( var i = 0 ; i < ResContainerDetails.length; i++ ){
				ContainerDetailsList = new Array();
				ContainerDetailsList[0] = ResContainerDetails[i].getValue('custrecord_ebiz_lpmaster_lp');
				ContainerDetailsList[1] = ResContainerDetails[i].getValue('custrecord_ebiz_lpmaster_sscc');
				ContainerDetailsList[2] = ResContainerDetails[i].getValue('custrecord_ebiz_lpmaster_totcube');
				ContainerDetailsList[3] = ResContainerDetails[i].getValue('custrecord_ebiz_lpmaster_totwght');
				ContainerDetailsListT.push(ContainerDetailsList);
			}
		}
		// Get details from item master against the given item

	}


	catch(exception) 
	{
		nlapiLogExecution('ERROR', ' GetContainerCUCCDetails', exception);
	}

	nlapiLogExecution('ERROR', 'GetContainerCUCCDetails', 'end');
	nlapiLogExecution('ERROR', ' ContainerDetailsListT', ContainerDetailsListT);
	return ContainerDetailsListT;

}

/*This fuction returns EUCC numbers for the container */

function GetContainerEUCCDetails(closedTaskList, contlp,ItemID){
	nlapiLogExecution('ERROR', 'GetContainerEUCCDetails', 'Start');

	var ContainerEUCCDetailsT = new Array();

	try
	{

		var EUCC="";

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp',  null, 'is',contlp));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_item',  null, 'is',ItemID));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');

		var ResContainerEUCCDetails = nlapiSearchRecord('customrecord_ebiznet_master_lp' , null,	filters, columns);		
		if ( ResContainerEUCCDetails !=null && ResContainerEUCCDetails.length > 0)
		{
			for ( var i = 0 ; i < ResContainerEUCCDetails.length; i++ ){

				if ( EUCC != null  && EUCC !="" )
				{
					EUCC = EUCC +  "," + ResContainerEUCCDetails[i].getValue('custrecord_ebiz_lpmaster_sscc');
				}
				else
				{
					EUCC = ResContainerEUCCDetails[i].getValue('custrecord_ebiz_lpmaster_sscc');
				}

			}

		}
		// Get details from item master against the given item

	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', ' GetContainerEUCCDetails', exception);
	}

	nlapiLogExecution('ERROR', 'GetContainerEUCCDetails', 'end');
	nlapiLogExecution('ERROR', 'EUCC', EUCC);
	return EUCC;

}




/*This fuction returns EUCC numbers for the container */

function GetIndividiualEUCCDetails(EUCCdetailslist, contlp){
	nlapiLogExecution('ERROR', 'GetIndividiualEUCCDetails', 'Start');
	nlapiLogExecution('ERROR', 'EUCCdetailslist', EUCCdetailslist);
	var IndividualEUCCDetailsList = new Array();
	var IndividualEUCCDetailsListT = new Array();
	try
	{
		if ( EUCCdetailslist !=null && EUCCdetailslist.length > 0)
		{
			for ( var i = 0 ; i < EUCCdetailslist.length; i++ ){

				if (EUCCdetailslist[i]['ContLP'] = contlp) {

					IndividualEUCCDetailsList[0]  = IndividualEUCCDetailsList[0]['EUCC'] + ",";


				} 


			}
			IndividualEUCCDetailsListT.push(IndividualEUCCDetailsList);
		}
		// Get details from item master against the given item

	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', ' GetIndividiualEUCCDetails', exception);
	}

	nlapiLogExecution('ERROR', 'GetIndividiualEUCCDetails', 'end');
	return IndividualEUCCDetailsListT;

}

/*  This function returns distinct internal ids of skus */
function getDistinctSKUsFromTaskList(closedTaskList){
	nlapiLogExecution('ERROR', 'getDistinctSKUsFromTaskList', 'Start');
	var skusList = new Array();
	try{	
		if(closedTaskList != null && closedTaskList.length > 0){
			for(var i = 0; i < closedTaskList.length; i++){
				var skuID = closedTaskList[i].getValue('custrecord_ebiztask_ebiz_sku_no');
				nlapiLogExecution('ERROR', 'skuID', skuID);
				if(!isDistinctSKU(skusList, skuID))
					skusList.push(skuID);

			}
		}
	}
	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'getDistinctSKUsFromTaskList', exception);
	}

	nlapiLogExecution('ERROR', 'getDistinctSKUsFromTaskList', 'End');
	nlapiLogExecution('ERROR', 'skulist', skusList);
	return skusList;
}

/*  This function returns distinct internal ids of skus with square brackets which can be used for search filters */
function getDistinctSKUsFromTaskListLR(closedTaskList){
	nlapiLogExecution('ERROR', 'getDistinctSKUsFromTaskListLR', 'Start');
	var skuListLR = new Array();
	try{	
		if(closedTaskList != null && closedTaskList.length > 0){
			for(var i = 0; i < closedTaskList.length; i++){
				var skuID = closedTaskList[i].getValue('custrecord_ebiztask_ebiz_sku_no');
				skuID= '[' + skuID + ']';
				nlapiLogExecution('ERROR', 'skuID', skuID);
				if(!isDistinctSKU(skuListLR, skuID))
					skuListLR.push(skuID);

			}
		}
	}
	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'getDistinctSKUsFromTaskListLR', exception);
	}

	nlapiLogExecution('ERROR', 'getDistinctSKUsFromTaskListLR', 'End');
	nlapiLogExecution('ERROR', 'skulistLR', skuListLR);
	return skuListLR;
}

/**
 * 
 * @param closedTaskList
 * @returns {Array}
 */
function getDistinctOrderIDsFromTaskList(closedTaskList){
	nlapiLogExecution('ERROR', 'getDistinctOrderIDsFromTaskList', 'Start');
	var OrderIDList = new Array();
	try{

		if(closedTaskList != null && closedTaskList.length > 0)
		{
			for(var i = 0; i < closedTaskList.length; i++)
			{
				var OrderID = closedTaskList[i].getValue('custrecord_ebiztask_ebiz_order_no');
				if(!isDistinctOrderID(OrderIDList, OrderID))
					OrderIDList.push(OrderID);
			}
		}

		nlapiLogExecution('ERROR', 'getDistinctOrderIDsFromTaskList', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'getDistinctOrderIDsFromTaskList', exception);
	}
	nlapiLogExecution('ERROR', 'getDistinctOrderIDsFromTaskList', 'End');
	return OrderIDList;

}
/**
 * Function to retrieve the UPC details for the list of skus
 * 
 * @param skulist
 * @returns {Array}
 */
function GetUPCDetails(closedTaskList){
	nlapiLogExecution('ERROR', 'GetUPCDetails', 'Start');

	var upcDetailsListT = new Array();

	try{
		// Get the list of distinct SKUs
		var skulist = getDistinctSKUsFromTaskList(closedTaskList);

		var filters = new Array();
		var columns = new Array();


		filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof',skulist)); // item
		filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'U'));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord17'); // item
		columns[1] = new nlobjSearchColumn('custrecord14'); // UPC

		nlapiLogExecution('ERROR', 'Beforesearch', 'Start');

		var resUPCDetails = nlapiSearchRecord('customrecord265', null,filters, columns);

		if(resUPCDetails != null && resUPCDetails.length > 0){
			for(var i = 0; i < resUPCDetails.length; i++ ){
				var upcDetailsList = new Array();
				upcDetailsList[0] = resUPCDetails[i].getValue('custrecord17');
				upcDetailsList[1] = resUPCDetails[i].getValue('custrecord14');
				upcDetailsListT.push(upcDetailsList);
			}
		}
	}catch(exception){
		nlapiLogExecution('ERROR', 'GetUPCDetails', exception);
	}

	nlapiLogExecution('ERROR', 'GetUPCDetails', 'end');
	return upcDetailsListT;
}



/**
 * Function to retrieve the batch details for the list of SKUS
 * 
 * @param skulist
 * @returns {Array}
 */
function GetBatchDetails(closedTaskList,skulist){
	nlapiLogExecution('ERROR', 'GetBatchDetails', 'Start');

	var BatchDetailsListT = new Array();

	try{
		// Get the list of distinct SKUs
		//var skulist = getDistinctSKUsFromTaskList(closedTaskList);

		var filters = new Array();
		var columns = new Array();
		nlapiLogExecution('ERROR', 'GetBatchDetails skulist', skulist);

		filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',skulist));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsku');

		var ResBatchDetails = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null,filters, columns);

		if(ResBatchDetails != null && ResBatchDetails.length > 0){
			for(var i = 0; i < ResBatchDetails.length; i++ ){
				var BatchDetailsList = new Array();
				BatchDetailsList[0] = ResBatchDetails[i].getValue('custrecord_ebizsku');
				BatchDetailsList[1] = ResBatchDetails[i].getValue('custrecord_ebizlotbatch');
				BatchDetailsListT.push(BatchDetailsList);
			}
		}
	}catch(exception){
		nlapiLogExecution('ERROR', 'GetBatchDetails', exception);
	}

	nlapiLogExecution('ERROR', 'GetBatchDetails', 'end');
	return BatchDetailsList;
}


/**
 * Function to retrieve the SKU details for a list of SKUs
 * @param skulist
 * @returns {Array}
 */
function GetUOMDetails(closedTaskList,skulist){
	nlapiLogExecution('ERROR', 'GetUOMDetails', 'Start');

	var uomDetailsListT = new Array();
	//var skulist = new Array();
	var skustring ="";


	try{
		// Get the list of distinct SKUs
		//skulist = getDistinctSKUsFromTaskList(closedTaskList);
		nlapiLogExecution('ERROR', 'Before provide inputs SKUList', skulist);
		// This API returns skulist as [1,2,3];
		// skustring = ProvideInputs(skulist,'List' );
		var filters = new Array();
		var columns = new Array();

		nlapiLogExecution('ERROR', 'After provide inputs -SKUList', skulist);
		filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof',skulist));


		// Getting sku type is pending
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizuomskudim');
		columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
		columns[2] = new nlobjSearchColumn('custrecord_ebizlength');
		columns[3] = new nlobjSearchColumn('custrecord_ebizwidth');
		columns[4] = new nlobjSearchColumn('custrecord_ebizheight');
		columns[5] = new nlobjSearchColumn('custrecord_ebizweight');
		columns[6] = new nlobjSearchColumn('custrecord_ebizcube');
		columns[7] = new nlobjSearchColumn('custrecord_ebizitemdims');
		columns[8] = new nlobjSearchColumn('custrecord_ebizpackcodeskudim');
		columns[9] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');

		var ResItemDims = nlapiSearchRecord('customrecord_ebiznet_skudims', null,filters, columns);

		if(ResItemDims != null && ResItemDims.length > 0){
			for(var i = 0; i < ResItemDims.length; i++ ){

				var uomDetailsList = new Array();
				uomDetailsList[0] = ResItemDims[i].getText('custrecord_ebizuomskudim');
				uomDetailsList[1] = ResItemDims[i].getValue('custrecord_ebizqty');
				uomDetailsList[2] = ResItemDims[i].getValue('custrecord_ebizlength');
				uomDetailsList[3] = ResItemDims[i].getValue('custrecord_ebizwidth');
				uomDetailsList[4] = ResItemDims[i].getValue('custrecord_ebizheight');
				uomDetailsList[5] = ResItemDims[i].getValue('custrecord_ebizweight');
				uomDetailsList[6] = ResItemDims[i].getValue('custrecord_ebizcube');
				uomDetailsList[7] = ResItemDims[i].getValue('custrecord_ebizitemdims');
				uomDetailsList[8] = ResItemDims[i].getValue('custrecord_ebizpackcodeskudim');
				uomDetailsList[9] = ResItemDims[i].getValue('custrecord_ebizuomlevelskudim');
				nlapiLogExecution('ERROR', 'Get UOM Details - Array pushing to 2D Array',uomDetailsList );
				uomDetailsListT.push(uomDetailsList);
			}
		}
	}catch(exception){
		nlapiLogExecution('ERROR', 'Get UOM Details', exception);
	}

	nlapiLogExecution('ERROR', 'GetUOMDetails', 'end');
	nlapiLogExecution('ERROR', 'GetUOMDetails',uomDetailsListT.length );
	return uomDetailsListT;

}

/* This function accepts a list and based on the object it will return the data */
function ProvideInputs(recordset, mode) {
	nlapiLogExecution('ERROR', 'ProvideInputs', 'Start');
	nlapiLogExecution('ERROR', 'recordset', recordset);
	nlapiLogExecution('ERROR', 'mode', mode);

	var listOutput = "";
	try {

		if (recordset != null && recordset.length > 0) {
			if (mode == "List") {
				listOutput = "[";
				for ( var i = 0; i < recordset.length; i++) {
					listOutput = listOutput + "'" + recordset[i] + "'" + ",";
				}
				listOutput = listOutput.substring(0,
						listOutput.length - 1);

				listOutput = listOutput + "]";
			}

		}

	} catch (exception) {
		nlapiLogExecution('ERROR', 'ProvideInputs', exception);

	}
	nlapiLogExecution('ERROR', 'listOutput', listOutput);
	nlapiLogExecution('ERROR', 'ProvideInputs', 'End');
	return listOutput;

}

/**
 * Function to retrieve the SKU details for a list of SKUs
 * @param skulist
 * @returns {Array}
 */
function GetSKUDetails(closedTaskList,skulist){
	nlapiLogExecution('ERROR', 'GetSKUDetails', 'Start');

	var skuDetailsListT = new Array();
	try{
		// Get the list of distinct SKUs
		var skuDetailsList = new Array();
		//var skulist = getDistinctSKUsFromTaskList(closedTaskList);
		nlapiLogExecution('ERROR', 'skulist', skulist);
		var filters = new Array();
		var columns = new Array();


		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', skulist));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12

		// Getting sku type is pending
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('salesdescription');
		columns[1] = new nlobjSearchColumn('custitem_item_family');
		columns[2] = new nlobjSearchColumn('custitem_item_group');
		columns[3] = new nlobjSearchColumn('custitem_ebizhazmatclass');
		columns[4] = new nlobjSearchColumn('custitem_ebiztempcntrl');
		columns[5] = new nlobjSearchColumn('itemid');
		columns[6] = new nlobjSearchColumn('vendorname');
		columns[7] = new nlobjSearchColumn('internalid');
		//columns[7] = new nlobjSearchColumn('recordtype')

		var ResSKUDetails = nlapiSearchRecord('item', null, filters, columns);

		if(ResSKUDetails != null && ResSKUDetails.length > 0){
			for(var i = 0; i < ResSKUDetails.length; i++ ){
				skuDetailsList = new Array();
				skuDetailsList[0] = ResSKUDetails[i].getValue('itemid');
				//skuDetailsList[i]['recordtype'] = ResSKUDetails[i].getValue('recordtype');
				skuDetailsList[1] = ResSKUDetails[i].getValue('salesdescription');
				skuDetailsList[2] = ResSKUDetails[i].getText('custitem_item_family');
				skuDetailsList[3]= ResSKUDetails[i].getText('custitem_item_group');
				skuDetailsList[4] = ResSKUDetails[i].getText('custitem_ebizhazmatclass');
				skuDetailsList[5] = ResSKUDetails[i].getValue('custitem_ebiztempcntrl');
				skuDetailsList[6] = ResSKUDetails[i].getText('vendorname');
				skuDetailsList[7] = ResSKUDetails[i].getValue('internalid');
				skuDetailsListT.push (skuDetailsList);
			}
		}
	}catch(exception){
		nlapiLogExecution('ERROR', 'Get SKU Details', exception);
	}

	nlapiLogExecution('ERROR', 'GetSKUDetails', 'end');
	nlapiLogExecution('ERROR', 'skuDetailsListT', skuDetailsListT);
	return skuDetailsListT;
}
/*
 * This functon accepts skulist and sku as two parameters and returns all the
 * details of the sku
 */
function GetIndividualSKUInfo(skulist, sku ){

	var IndividualSKUInfoList = new Array();

	var IndividualSKUInfoListT = new Array();
	nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', 'Start');
	nlapiLogExecution('ERROR', 'skulist', skulist);
	nlapiLogExecution('ERROR', 'sku', sku);
	try{

		if (skulist != null && skulist.length >0 && sku != null && sku.length>0){
			for ( var i = 0 ; i < skulist.length ; i++){

				if (skulist[i][7]  == sku )
				{
					IndividualSKUInfoList = new Array();
					nlapiLogExecution('ERROR', 'sku matches..internal id is', skulist[i][7]);
					IndividualSKUInfoList[0] = skulist[i][0]; //['itemid'];
					//IndividualSKUInfoList[1] = skulist[i]['recordtype'];
					IndividualSKUInfoList[2] = skulist[i][1];//['salesdescription'];
					IndividualSKUInfoList[3] = skulist[i][2];//['custitem_item_family'];
					IndividualSKUInfoList[4] = skulist[i][3];//['custitem_item_group'];
					IndividualSKUInfoList[5] = skulist[i][4];//['custitem_ebizhazmatclass'];
					IndividualSKUInfoList[6] = skulist[i][5];//['custitem_ebiztempcntrl'];
					IndividualSKUInfoList[7] = skulist[i][6];//['vendorname'];
					IndividualSKUInfoListT.push(IndividualSKUInfoList);
					i = skulist.length; 	
				}

			}
		}


	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', exception);

	}

	nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', 'end');
	nlapiLogExecution('ERROR', 'IndividualSKUInfoListT', IndividualSKUInfoListT);
	return IndividualSKUInfoListT;
}

/*
 * This function accepts containerlist , contlp as parameters and returns CUCC
 * no, weight and cube
 */
function GetIndividualCUCCData(containerlist , ContLP ){


	var IndividualCUCCDetailsListT = new Array();
	nlapiLogExecution('ERROR', 'GetIndividualCUCCData', 'Start');
	try{

		if (containerlist != null && containerlist.length >0 && ContLP != null && ContLP.length>0){
			for ( var i = 0 ; i < containerlist.length ; i++){

				if (containerlist[i]['ContLP']  == ContLP )
				{
					var IndividualCUCCDetailsList = new Array();
					IndividualCUCCDetailsList[0] = containerlist[i]['ContLP'];
					IndividualCUCCDetailsList[1]= containerlist[i]['CUCC'];
					IndividualCUCCDetailsList[2] = containerlist[i]['ContainerCube'];
					IndividualCUCCDetailsList[3] = containerlist[i]['ContainerWeight'];
					IndividualCUCCDetailsListT.push(IndividualCUCCDetailsList);
					i = containerlist.length; 	
				}

			}
		}


	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetIndividualCUCCData', exception);
	}

	nlapiLogExecution('ERROR', 'GetIndividualCUCCData', 'end');
	return IndividualCUCCDetailsList;
}


/* This function accepts salesorderdetaillist ,salesorder as parameter and returns salesorder details.*/

function GetIndividualOrderInfo(OrderDetailList, OrderNo ){


	var IndividualOrderInfoListT = new Array();
	var IndividualOrderInfoList = new Array();
	nlapiLogExecution('ERROR', 'GetIndividualOrderInfo', 'Start');
	nlapiLogExecution('ERROR', 'OrderDetailList', OrderDetailList);
	nlapiLogExecution('ERROR', 'OrderNo', OrderNo);
	try{

		if (OrderDetailList != null && OrderDetailList.length >0 && OrderNo != null && OrderNo.length>0){
			for ( var i = 0 ; i < OrderDetailList.length ; i++){

				if (OrderDetailList[i][44]  == OrderNo )
				{
					nlapiLogExecution('ERROR', 'OrderNo matches and order internalid', OrderDetailList[i][44]);				
					IndividualOrderInfoList = new Array();
					IndividualOrderInfoList[0] = OrderDetailList[i][0];//.getValue('tranid');
					IndividualOrderInfoList[1] = OrderDetailList[i][1];//.getValue('custbody_edi_ref_mr');
					IndividualOrderInfoList[2] = OrderDetailList[i][2];//.getValue('custbody_dynacraftvendornumber');
					IndividualOrderInfoList[3] = OrderDetailList[i][3];//.getValue('Shipmethod');
					IndividualOrderInfoList[4] = OrderDetailList[i][4];//.shipmethodi //getValue('custbody_edi_fob');
					IndividualOrderInfoList[5] = OrderDetailList[i][6];//.getValue('shipaddressee');
					IndividualOrderInfoList[6] = OrderDetailList[i][7];//.getValue('shipaddr1');
					IndividualOrderInfoList[7] = OrderDetailList[i][8];//.getValue('shipaddr2');
					IndividualOrderInfoList[8] = OrderDetailList[i][9];//.getValue('shipcity');
					IndividualOrderInfoList[9] = OrderDetailList[i][10];//.getValue('shipstate');
					IndividualOrderInfoList[10] = OrderDetailList[i][11];//.getValue('shipcountry');
					IndividualOrderInfoList[11] = OrderDetailList[i][12];//.getValue('shipzip');
					IndividualOrderInfoList[12] = OrderDetailList[i][13];//.getValue('custbody_nswmspoexpshipdate');
					IndividualOrderInfoList[13] = OrderDetailList[i][14];//.getValue('custbody_nswmsactualarrivaldate');
					IndividualOrderInfoList[14] = OrderDetailList[i][15];//.getValue('custbody_nswmssodestination');
					IndividualOrderInfoList[15] = OrderDetailList[i][16];//.getValue('custbody_nswmssoroute');
					nlapiLogExecution('ERROR', 'OrderDetailList[i][16]',OrderDetailList[i][16]);
					//IndividualOrderInfoList[16] = OrderDetailList[i].getValue('shipzip');
					IndividualOrderInfoList[17] = OrderDetailList[i][18];//.getValue('class');
					IndividualOrderInfoList[18] = OrderDetailList[i][19];//getValue('entity');
					IndividualOrderInfoList[19] = OrderDetailList[i][20];//.getValue('billaddressee');
					IndividualOrderInfoList[20] = OrderDetailList[i][21];//.getValue('billaddr1');
					IndividualOrderInfoList[21] = OrderDetailList[i][22];//.getValue('billaddr2');
					IndividualOrderInfoList[22] = OrderDetailList[i][23];//.getValue('billaddr3');
					IndividualOrderInfoList[23] = OrderDetailList[i][24];//.getValue('billcity');
					IndividualOrderInfoList[24] = OrderDetailList[i][25];//.getValue('billstate');
					IndividualOrderInfoList[25] = OrderDetailList[i][26];//.getValue('billzip');
					IndividualOrderInfoList[26] = OrderDetailList[i][27];//.getValue('billcountry');
					IndividualOrderInfoList[27] = OrderDetailList[i][28];//.getValue('billphone');
					IndividualOrderInfoList[28] = OrderDetailList[i][29];//.getValue('Location');
					IndividualOrderInfoList[29] = OrderDetailList[i][30];//.getValue('custbody_nswms_company');
					IndividualOrderInfoList[30] = OrderDetailList[i][31];//.getValue('department');
					IndividualOrderInfoList[31] = OrderDetailList[i][32];//.getValue('terms');
					IndividualOrderInfoList[32] = OrderDetailList[i][33];//.getValue('custbody_nswmsordertype');
					IndividualOrderInfoList[33] = OrderDetailList[i][34];//getValue('custbody_nswmspriority');
					IndividualOrderInfoList[34] = OrderDetailList[i][35];//.getValue('otherrefnum');
					IndividualOrderInfoList[35] = OrderDetailList[i][36];//.getValue('shipphone');
					IndividualOrderInfoList[36] = OrderDetailList[i][37];//.getValue('billphone');
					IndividualOrderInfoList[37] = OrderDetailList[i][38];//.getValue('custbody_nswmspriority');
					IndividualOrderInfoList[38] = OrderDetailList[i][39];//.getValue('otherrefnum');
					//IndividualOrderInfoList[39] = OrderDetailList[i][40];//['CarrierOptions'];
					IndividualOrderInfoList[39] = OrderDetailList[i][5];// //getValue('custbody_edi_fob');
					IndividualOrderInfoList[40] = OrderDetailList[i][41];//custbody_locationaddressid
					//IndividualOrderInfoList[41] = OrderDetailList[i][3];//['ShipMethodID'];
					IndividualOrderInfoList[42] = OrderDetailList[i][42];//['ShipPhone'];
					IndividualOrderInfoList[43]= OrderDetailList[i][43];//['PlannedArrivaDateTime'];
					//IndividualOrderInfoList[44]= OrderDetailList[i][44];//['PlannedArrivaDateTime'];
					IndividualOrderInfoList[45]= OrderDetailList[i][45];//['operating unit'];

					IndividualOrderInfoList[46]= OrderDetailList[i][46];
					IndividualOrderInfoList[47]= OrderDetailList[i][47];

					IndividualOrderInfoListT.push(IndividualOrderInfoList);
					//salesorderDetailList[44] = ResSSODetails[i].getValue('internalid');

				}

			}
		}


	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetIndividualOrderInfo', exception);
	}

	nlapiLogExecution('ERROR', 'GetIndividualOrderInfo', 'end');
	nlapiLogExecution('ERROR', 'Individual order info-IndividualOrderInfoListT', IndividualOrderInfoListT);
	return IndividualOrderInfoListT;
}

/*
 * This function accepts salesorderdetaillist ,salesorder as parameter and
 * returns salesorder details.
 */

/* This function will accept sales order internal id as parameter and returns all the shipmethod master */

function GetCarrierOptions( SOid ){
	var ShipCarrierOptions="";
	var salesorderRec ="";
	try
	{
		nlapiLogExecution('ERROR', 'GetCarrierOptions', 'Start');
		nlapiLogExecution('ERROR', 'GetCarrierOptions-SOid',SOid);
		salesorderRec = nlapiLoadRecord('salesorder', SOid);					
		ShipCarrierOptions = salesorderRec.getField('shipmethod').getSelectOptions(null, null);
		nlapiLogExecution('ERROR', 'shipoptionlen', ShipCarrierOptions.length);

	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetCarrierOptions', exception);

	}

	nlapiLogExecution('ERROR', 'GetCarrierOptions', 'End');
	//nlapiLogExecution('ERROR', 'GetCarrierOptions-ShipCarrierOptions', ShipCarrierOptions);
	return ShipCarrierOptions;
}

/*
 * This function accepts salesorderdetaillist ,salesorder as parameter and
 * returns salesorder details.
 */

function GetAlternateIDSforOrder(OrdNo){

	var OrderAlternateIDsList = new Array();
	var OrderAlternateIDsListT = new Array();
	var filters= new Array();
	var columns = new Array();
	nlapiLogExecution('ERROR', 'GetAlternateIDSforOrder', 'Start');
	try{

		if ( OrdNo != null &&  OrdNo !=""){

			filters.push(new nlobjSearchFilter('internalid', null, 'is', OrdNo));


			columns[0] = new nlobjSearchColumn('line');
			columns[1] = new nlobjSearchColumn('alternateid');

			var ResOrderAlternateDetails = nlapiSearchRecord('salesorder', null, filters,columns);

			if (ResOrderAlternateDetails != null &&  ResOrderAlternateDetails.length > 0 )
			{
				for(var i = 0; i < ResOrderAlternateDetails.length; i++ ){
					OrderAlternateIDsList = new Array();
					OrderAlternateIDsList[0] = ResOrderAlternateDetails[i].getValue('line');
					OrderAlternateIDsList[1] = ResOrderAlternateDetails[i].getValue('alternateid');
					OrderAlternateIDsListT.push(OrderAlternateIDsList);
				}



			}




		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetAlternateIDSforOrder', exception);
	}
	nlapiLogExecution('ERROR', '', 'OrderAlternateIDsListT',OrderAlternateIDsListT);
	nlapiLogExecution('ERROR', 'GetAlternateIDSforOrder', 'end');

	return OrderAlternateIDsListT;
}



/* This function is to return AlternateId of the salesorder items. Parameters are orderno , line no , item.
 * 
 * 
 * 
 */

/*
 * This function accepts salesorder alternateid item list and lineno returns
 * salesorder details.
 */

function GetAlternateIDforOrderLine(orderinternalid, lineno ){

	var alternateid="";
	nlapiLogExecution('ERROR', 'GetAlternateIDforOrderLine', 'Start');
	nlapiLogExecution('ERROR', 'Internal id and lineno are', orderinternalid + ' ' + lineno );

	try{

		var salesorderRec = nlapiLoadRecord('salesorder', orderinternalid);
		alternateid=salesorderRec.getLineItemValue('item','custcol13',lineno);
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetAlternateIDforOrderLine', exception);
	}

	nlapiLogExecution('ERROR', 'Alternateid', alternateid);
	nlapiLogExecution('ERROR', 'GetAlternateIDforOrderLine', 'end');

	return alternateid;
}


/*This API will assign all the UOM details for the item 
 * Type : Skulist , sku*/

function AssignUOMDetails ( uomDetailsList , item)
{
	nlapiLogExecution('ERROR', 'uomDetailsList', uomDetailsList);
	nlapiLogExecution('ERROR', 'item', item);
	var uomIndividualdetailsT  = new Array();
	var uomCnt = 0;
	try {

		if ( uomDetailsList != null && uomDetailsList.length > 0 ){

			for ( var i = 0 ; i < uomDetailsList.length ; i++ ) {


				var uomDetails = uomDetailsList[i];
				//if  (uomDetailsList[i]['Item'] == item){
				//nlapiLogExecution('ERROR', 'uomDetails[7]', uomDetails[7]);
				if  (uomDetails[7] == item){
					var uomIndividualdetails  = new Array();

					nlapiLogExecution('ERROR', 'uomDetails', uomDetails);					

					uomIndividualdetails[0]= uomDetails[0];
					uomIndividualdetails[1]= uomDetails[1];
					uomIndividualdetails[2]= uomDetails[2];
					uomIndividualdetails[3]= uomDetails[3];
					uomIndividualdetails[4]= uomDetails[4];
					uomIndividualdetails[5]= uomDetails[5];
					uomIndividualdetails[6]= uomDetails[6];
					uomIndividualdetails[7]= uomDetails[1];
					uomIndividualdetails[8]= uomDetails[8];

					/*uomDetailsList[0] = ResItemDims[i].getValue('custrecord_ebizuomskudim');
					uomDetailsList[1] = ResItemDims[i].getValue('custrecord_ebizqty');
					uomDetailsList[2] = ResItemDims[i].getValue('custrecord_ebizlength');
					uomDetailsList[3] = ResItemDims[i].getValue('custrecord_ebizwidth');
					uomDetailsList[4] = ResItemDims[i].getValue('custrecord_ebizheight');
					uomDetailsList[5] = ResItemDims[i].getValue('custrecord_ebizweight');
					uomDetailsList[6] = ResItemDims[i].getValue('custrecord_ebizcube');
					uomDetailsList[7] = ResItemDims[i].getValue('custrecord_ebizitemdims');
					uomDetailsList[8] = ResItemDims[i].getValue('custrecord_ebizpackcodeskudim');
					uomDetailsList[9] = ResItemDims[i].getValue('custrecord_ebizuomlevelskudim');*/

					uomIndividualdetailsT.push(uomIndividualdetails);
					nlapiLogExecution('ERROR', 'uomIndividualdetailsT', uomIndividualdetailsT);
				} 



			} // End for 

		} // If resultset is not null					

	} // end for try		

	catch (exception){
		nalapiLogExecution('ERROR','AssignUOMDetails', exception)
	}

	nlapiLogExecution ('ERROR','AssignUOMDetails','End');
	nlapiLogExecution('ERROR', 'uomIndividualdetailsT Final', uomIndividualdetailsT);
	return uomIndividualdetailsT;
}
/**
 * 
 */
/* This function will return Y , in case system rule defined for 856 Generation*/
function SystemRuleforASNC(siteid, compid)
{
	var ASNCRule = 'N';
	try {
		var filters = new Array();
		if (compid != null && compid != ""){
			filters.push(new nlobjSearchFilter('custrecord_ebizcomp', null, 'anyof', compid));
		}

		if (siteid != null && siteid != ""){
			filters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', siteid));
		}

		filters.push(new nlobjSearchFilter('name', null, 'is','ASNC_CNF'));
		filters.push(new nlobjSearchFilter('custrecord_ebizdescription', null, 'is','ASN Confirmation Required?'));
		filters.push(new nlobjSearchFilter('custrecord_ebizruleval_refid', null, 'anyof', '1004'));
		filters.push(new nlobjSearchFilter('custrecord_ebizrulevalue', null, 'is', 'Yes'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizruleval_refid');
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');

		var ResASNCRule = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters,columns);

		if (ResASNCRule!= null  && ResASNCRule.length > 0){
			ASNCRule = 'Y'; 
		}
	}	
	catch(exception) {
		nlapiLogExecution('ERROR', 'Systemrule Fetching-856', exception);
	}

	return ASNCRule;
}
/*
 * This function will take shipLP as parameter and return the result set of all shipped task records.
 *  */
function GeteBizTaskRecs(siteid, compid,shipLP)
{
	var ResultTaskRecords = "";
	try {
		nlapiLogExecution ('ERROR','ShipLP',shipLP);
		var filters = new Array();
		//filters.push(new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', shipLP));
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', shipLP));
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', '3'));// 3=pick
		//filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof','7'));// 7=STATUS.OUTBOUND.SHIP_UNIT_BUILT
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof','14'));// ship
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_hostid', null, 'isempty'));
		filters.push(new nlobjSearchFilter('mainline','custrecord_ebiztask_ebiz_order_no','is','T'));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiztask_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiztask_sku');
		columns[3] = new nlobjSearchColumn('custrecord_ebiztask_act_qty');
		columns[4] = new nlobjSearchColumn('tranid','custrecord_ebiztask_ebiz_order_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiztask_uom_level');
		columns[6] = new nlobjSearchColumn('custrecord_ebiztask_uom_id');
		columns[7] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no');
		columns[8] = new nlobjSearchColumn('custrecord_ebiztask_serial_no');
		columns[9] = new nlobjSearchColumn('custrecord_ebiztask_act_end_date');
		columns[10] = new nlobjSearchColumn('custrecord_ebiztask_batch_no');
		columns[11] = new nlobjSearchColumn('custrecord_ebiztask_packcode');
		columns[12] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
		columns[2].setSort();
		ResultTaskRecords = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null,filters, columns);

		nlapiLogExecution('ERROR', 'ResultTaskRecords', ResultTaskRecords);

	}

	catch(exception) {
		nlapiLogExecution('ERROR', 'GeteBizTaskRecs', exception);
	}

	return ResultTaskRecords;
}
/*
 * This function will take batchdetailsarry and item as inputs and return batch
 * id as output
 */
function GetIndividualBatchID( BatchDetailsList,item)
{
	var BatchID = "";
	try
	{
		if (BatchDetailsList != null && BatchDetailst.length>0) {
			for ( var i=0; i < BatchDetailsList.length ; i++){
				if (BatchDetailsList[i]['Item'] == item){

					BatchID = BatchDetailsList[i].getText('BatchID');
					i = BatchDetailsList.length;
				}

			}
		}
	}
	catch(exception) {
		nlapiLogExecution('ERROR', 'GetIndividiualBatchID', exception);
	}
	return BatchId;	
}

/*
 * This function will take upcdetailslist and item as inputs and return upcCode
 * id as output
 */
function GetIndividiualupcCode ( upcDetailsList,item)
{
	var upcCode = "";
	try
	{
		if (upcDetailsList != null && upcDetailsList.length>0) {
			for ( var i=0; i < upcDetailsList.length ; i++){
				if (upcDetailsList[i]['Item'] == item){

					upcCode = upcDetailsList[i].getValue('BatchID');
					i = upcDetailsList.length;
				}

			}
		}
	}
	catch(exception) {
		nlapiLogExecution('ERROR', 'GetIndividiualupcCode', exception);
	}
	return upcCode;	
}
/**
 * Method to retrieve the UPC Code for a given SKU
 * 
 * @param siteid
 * @param compid
 * @param batchtxt
 * @returns {String}
 */
function GetupcCode ( skuid){
	var upcCode = "";
	try
	{
		nlapiLogExecution('ERROR', 'GetupcCode', 'start');
		var filters = new Array();
		//skuid = '[' + skuid + ']';
		nlapiLogExecution('ERROR', 'skuid', skuid);
		filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'U'));
		filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof', skuid));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord14');

		var ResupcCodes = nlapiSearchRecord('customrecord265', null, filters, columns);
		if (ResupcCodes != null && ResupcCodes.length > 0) {
			upcCode = ResupcCodes[0].getValue('custrecord14');
		}
	}
	catch(exception) {
		nlapiLogExecution('ERROR', 'GetupcCode', exception);
	}
	nlapiLogExecution('ERROR', 'GetupcCode', 'stop');
	return upcCode;	
}
function Getcustomfield (skuid){
	var upcCode = "";
	try
	{
		nlapiLogExecution('ERROR', 'GetupcCode', 'start');
		var filters = new Array();
		//skuid = '[' + skuid + ']';
		nlapiLogExecution('ERROR', 'skuid', skuid);
		filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'E'));
		filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof', skuid));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord14');

		var ResupcCodes = nlapiSearchRecord('customrecord265', null, filters, columns);
		if (ResupcCodes != null && ResupcCodes.length > 0) {
			upcCode = ResupcCodes[0].getValue('custrecord14');
		}
	}
	catch(exception) {
		nlapiLogExecution('ERROR', 'GetupcCode', exception);
	}
	nlapiLogExecution('ERROR', 'GetupcCode', 'stop');
	return upcCode;	
}

//Ending
function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}



function getdummylpdetails(containerLP,ItemID,maxno)
{
	var Resdummylpdetails = new Array();
	nlapiLogExecution('ERROR', 'into getdummylpdetails', 'done');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp', null, 'is',containerLP));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_item', null, 'is',ItemID));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	if(maxno!=null && maxno!='' && maxno!='-1')
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_masterlp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_item');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
	columns[4] = new nlobjSearchColumn('id').setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp' , null,	filters, columns);	
	if( searchresults!=null && searchresults.length>=1000)
	{ 
		for(var s=0;s<searchresults.length;s++)
		{	
			Resdummylpdetails.push(searchresults[s]);
		}
		var maxno=searchresults[searchresults.length-1].getValue('id');
		getdummylpdetails(containerLP,ItemID,maxno);	
	}
	else
	{
		for(var s=0;s<searchresults.length;s++)
		{	
			Resdummylpdetails.push(searchresults[s]);
		} 
	}
	nlapiLogExecution('ERROR', 'out getdummylpdetails', Resdummylpdetails);
	return Resdummylpdetails;


}