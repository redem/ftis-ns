/***************************************************************************
                                                  
   eBizNET Solutions Inc
****************************************************************************
*
*  $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_ClusItemAttribute.js,v $
*  $Revision: 1.1.2.6.2.1 $
*  $Date: 2015/02/17 22:35:10 $
*  $Author: sponnaganti $
*  $Name: t_eBN_2014_2_StdBundle_0_224 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_RF_ClusItemAttribute.js,v $
*  Revision 1.1.2.6.2.1  2015/02/17 22:35:10  sponnaganti
*  Case# 201411523
*  KRM SB issue fix
*
*  Revision 1.1.2.6  2014/06/13 13:26:11  skavuri
*  Case# 20148882 (added Focus Functionality for Textbox)
*
*  Revision 1.1.2.5  2014/05/30 00:41:00  nneelam
*  case#  20148622
*  Stanadard Bundle Issue Fix.
*
*  Revision 1.1.2.4  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/

function ClusterItemAttribute(request, response){
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('DEBUG', 'Time Stamp at the start of GET',TimeStampinSec());

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{
//			st1 = "IR AL MONTAJE";
//			st2 = "N&#218;MERO DE EMPAQUE:";
//			st3 = "UBICACI&#211;N:";
//			st4 = "INGRESAR / ESCANEAR ETAPA:";
		}
		else
		{
			st1 = " ITEM ATTRIBUTE";
			st2 = "Scan the Item Case Barcode :";
			st3 = " CONF ";
			st4 = " PREV";
		}

		var getWaveno = request.getParameter('custparam_waveno');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var pickType = request.getParameter('custparam_picktype');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getItem = request.getParameter('custparam_item');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var NextItem = request.getParameter('custparam_nextitem');
		var name = request.getParameter('name');
		var vSOId = request.getParameter('custparam_ebizordno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getNextLoc = request.getParameter('custparam_nextlocation');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');
		var vBatchno = request.getParameter('custparam_batchno');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var ContainerSize = request.getParameter('custparam_containersize');
		var LocRecordCount = request.getParameter('custparam_nooflocrecords');
		var getSerialNo = request.getParameter('custparam_serialno');
		var getRemainqty = request.getParameter('custparam_remainqty');
		var getScreenNo = request.getParameter('custparam_screenno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getDetailtask = request.getParameter('custparam_detailtask');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var whLocation = request.getParameter('custparam_whlocation');
		var getNewContainerLpNo = request.getParameter('custparam_newcontainerlp');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var serialscanned = request.getParameter('custparam_serialscanned');
		var error = request.getParameter('custparam_error');
		var redirectscreen = request.getParameter('custparam_redirectscreen');

		nlapiLogExecution('DEBUG', 'in get redirectscreen',redirectscreen);

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterstagelocation').focus();";        
		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"</td></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdnLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnNextItem' value=" + NextItem + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnvSOId' value=" + vSOId + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnNextLoc' value=" + getNextLoc + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEndLocationId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value='" + vBatchno + "'>";
		html = html + "				<input type='hidden' name='hdnBeginBinLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnLocRecordCount' value=" + LocRecordCount + ">";
		html = html + "				<input type='hidden' name='hdnSerialNo' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnRemainqty' value=" + getRemainqty + ">";
		html = html + "				<input type='hidden' name='hdnScreenNo' value=" + getScreenNo + ">";		
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnDetailtask' value=" + getDetailtask + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhLocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnNewContainerLpNo' value=" + getNewContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnScannedSerialno' value=" + serialscanned + ">";
		html = html + "				<input type='hidden' name='hdnerror' value=" + error + ">";
		html = html + "				<input type='hidden' name='hdnredirectscreen' value=" + redirectscreen + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritemattribute' id='enteritemattribute' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritemattribute').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

		nlapiLogExecution('DEBUG', 'Time Stamp at the end of GET',TimeStampinSec());
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		nlapiLogExecution('DEBUG', 'Time Stamp at the start of response',TimeStampinSec());

		var getLanguage = request.getParameter('hdnLanguage');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var getWaveno = request.getParameter('hdnWaveNo');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var pickType = request.getParameter('hdnpicktype');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		var getItem = request.getParameter('hdnItem');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var NextItem = request.getParameter('hdnNextItem');
		var name = request.getParameter('hdnName');
		var vSOId = request.getParameter('hdnvSOId');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getEnteredLocation = request.getParameter('hdnEnteredLocation');
		var getNextLoc = request.getParameter('hdnNextLoc');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var NextItemInternalId=request.getParameter('hdnNextItemId');
		var getEndLocInternalId = request.getParameter('hdnEndLocationId');
		var vBatchno = request.getParameter('hdnbatchno');
		var getBeginLocationId = request.getParameter('hdnBeginBinLocationId');
		var ContainerSize = request.getParameter('hdnContainerSize');
		var LocRecordCount = request.getParameter('hdnLocRecordCount');
		var getSerialNo = request.getParameter('hdnSerialNo');
		var getRemainqty = request.getParameter('hdnRemainqty');
		var getScreenNo = request.getParameter('hdnScreenNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getDetailtask = request.getParameter('hdnDetailtask');
		var getBeginBinLocation = request.getParameter('hdnBeginBinLocation');
		var whLocation = request.getParameter('hdnwhLocation');
		var getNewContainerLpNo = request.getParameter('hdnNewContainerLpNo');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var serialscanned = request.getParameter('hdnScannedSerialno');
		var error = request.getParameter('hdnerror');
		var redirectscreen = request.getParameter('hdnredirectscreen');

		var optedEvent = request.getParameter('cmdPrevious');

		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_waveno"] = getWaveno;
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_picktype"] = pickType;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_nextitem"] = NextItem;
		SOarray["name"] = name;
		SOarray["custparam_ebizordno"] = vSOId;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_endlocation"] = getEnteredLocation;
		SOarray["custparam_nextlocation"] = getNextLoc;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_nextiteminternalid"] = NextItemInternalId;
		SOarray["custparam_endlocinternalid"] = getEndLocInternalId;
		SOarray["custparam_batchno"] = vBatchno;
		SOarray["custparam_beginLocation"] = getBeginLocationId;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_nooflocrecords"] = LocRecordCount;
		SOarray["custparam_remainqty"] = getRemainqty;
		SOarray["custparam_screenno"] = getScreenNo;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		SOarray["custparam_dolineid"] = getDOLineId;
		SOarray["custparam_detailtask"] = getDetailtask;
		SOarray["custparam_beginlocationname"] = getBeginBinLocation;
		SOarray["custparam_whlocation"] = whLocation;
		SOarray["custparam_newcontainerlp"] = getNewContainerLpNo;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_serialscanned"] = serialscanned;
		SOarray["custparam_error"] = error;
		SOarray["custparam_serialno"] = getSerialNo;
		SOarray["custparam_redirectscreen"] = redirectscreen;
		
		nlapiLogExecution('ERROR', 'Lading getContainerLpNo...', getContainerLpNo);
		nlapiLogExecution('ERROR', 'Lading getNewContainerLpNo...', getNewContainerLpNo);
		nlapiLogExecution('ERROR', 'Lading whLocation...', whLocation);
		nlapiLogExecution('ERROR', 'Lading getEnteredLocation...', getEnteredLocation);
		nlapiLogExecution('ERROR', 'Lading getNextLoc...', getNextLoc);
		nlapiLogExecution('ERROR', 'Lading getBeginLocationId...', getBeginLocationId);
		nlapiLogExecution('ERROR', 'Lading getBeginBinLocation...', getBeginBinLocation);
		nlapiLogExecution('ERROR', 'Lading getItem...', getItem);
		nlapiLogExecution('ERROR', 'Lading getItemInternalId...', getItemInternalId);
		nlapiLogExecution('ERROR', 'Lading NextItem...', NextItem);

		nlapiLogExecution('DEBUG', 'redirectscreen',redirectscreen);
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);
		}
		else 
		{
			var itemattribute = request.getParameter('enteritemattribute');
			if(itemattribute !='' && itemattribute != null)
			{
//				try
//				{
//					nlapiLogExecution('ERROR', 'Lading Open Task...', getRecordInternalId);
//					var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
//					SORec.setFieldValue('custrecord_ebiz_item_attribute',itemattribute);
//					nlapiSubmitRecord(SORec);
//					nlapiLogExecution('ERROR', 'Lading Open Task...Submit complete', getRecordInternalId);
//				}
//				catch(exp)
//				{
//					nlapiLogExecution('ERROR', 'Exception in Loading Open Task', exp);
//				}
				
				var filters = new Array();				
				filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getNewContainerLpNo));				
				filters.push( new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', getBeginLocationId));
				filters.push( new nlobjSearchFilter('custrecord_sku', null, 'anyof', getItemInternalId));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiz_item_attribute');
				
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
				for(var i = 0; searchresults != null && i < searchresults.length; i++ ){
					nlapiLogExecution('ERROR', 'searchresults.length', searchresults.length);
					var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresults[i].getId());
					SORec.setFieldValue('custrecord_ebiz_item_attribute',itemattribute);
					nlapiSubmitRecord(SORec);
				}
				
				if(redirectscreen == 'LOC')
					response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
				else if(redirectscreen == 'ITEM')
					response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
				else if(redirectscreen == 'DETTASK')
					response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);
				else
					response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);		

			}
			else
			{
				SOarray["custparam_error"]='INVALID ITEM ATTRIBUTE';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			}
		}

	}
}