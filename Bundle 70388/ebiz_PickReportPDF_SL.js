/***************************************************************************
	 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_PickReportPDF_SL.js,v $
 *     	   $Revision: 1.32.2.9.4.5.4.21.2.5 $
 *     	   $Date: 2015/12/02 15:36:25 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_216 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PickReportPDF_SL.js,v $
 * Revision 1.32.2.9.4.5.4.21.2.5  2015/12/02 15:36:25  deepshikha
 * 2015.2 issues
 * 201415915
 *
 * Revision 1.32.2.9.4.5.4.21.2.4  2015/11/19 14:18:11  schepuri
 * case# 201415743
 *
 * Revision 1.32.2.9.4.5.4.21.2.3  2015/11/11 23:33:47  nneelam
 * case# 201415542
 *
 * Revision 1.32.2.9.4.5.4.21.2.2  2015/10/15 07:33:11  rrpulicherla
 * pick report changes
 *
 * Revision 1.32.2.9.4.5.4.21.2.1  2015/09/22 13:57:01  aanchal
 * 201414434
 * 2015.2 Issue FIx
 *
 * Revision 1.32.2.9.4.5.4.21  2015/07/30 15:51:28  nneelam
 * case# 201413679
 *
 * Revision 1.32.2.9.4.5.4.20  2015/07/02 15:19:23  grao
 * SW issue fixes  201413084
 *
 * Revision 1.32.2.9.4.5.4.19  2015/06/19 13:54:27  schepuri
 * case# 201413165
 *
 * Revision 1.32.2.9.4.5.4.18  2015/05/27 15:45:45  grao
 * SB issue fixes  201412915
 *
 * Revision 1.32.2.9.4.5.4.17  2014/09/15 15:53:29  skavuri
 * Case# 201410335 CT issue fixed
 *
 * Revision 1.32.2.9.4.5.4.16  2014/04/15 15:45:19  skavuri
 * Case # 20147997 SB issue fixed
 *
 * Revision 1.32.2.9.4.5.4.15  2014/04/14 15:53:26  nneelam
 * case#  20147955�
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.32.2.9.4.5.4.14  2014/04/01 06:34:33  skreddy
 * case 20127817
 * MHP prod  issue fix
 *
 * Revision 1.32.2.9.4.5.4.13  2014/03/18 07:26:25  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to sku description
 *
 * Revision 1.32.2.9.4.5.4.12  2014/03/13 20:46:33  grao
 * Case# 20127718 related issue fixes in Leisure Living issue fixes
 *
 * Revision 1.32.2.9.4.5.4.11  2014/03/13 15:23:19  snimmakayala
 * Case #: 201218745
 *
 * Revision 1.32.2.9.4.5.4.10  2014/03/05 12:06:33  snimmakayala
 * Case #: 20127464
 *
 * Revision 1.32.2.9.4.5.4.9  2014/01/17 23:29:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20126870
 *
 * Revision 1.32.2.9.4.5.4.8  2013/09/13 15:46:34  nneelam
 * Case#. 20124394
 * Wave Report  Issue Fix..
 *
 * Revision 1.32.2.9.4.5.4.7  2013/09/06 23:32:43  nneelam
 * Case#.20124184
 * Pick Report PDF Issue Fix..
 *
 * Revision 1.32.2.9.4.5.4.6  2013/09/03 00:04:37  nneelam
 * Case#.20124229�
 * Pic Report Issue Fixed...
 *
 * Revision 1.32.2.9.4.5.4.5  2013/08/23 15:09:00  rmukkera
 * Case# 20123995�
 *
 * Revision 1.32.2.9.4.5.4.4  2013/04/16 13:23:55  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixes.
 *
 * Revision 1.32.2.9.4.5.4.3  2013/04/02 16:02:08  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * TSg Issue fixes
 *
 * Revision 1.32.2.9.4.5.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.32.2.9.4.5.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.32.2.9.4.5  2013/01/25 06:52:31  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related displaying wrong weight
 *
 * Revision 1.32.2.9.4.4  2012/12/06 07:18:16  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Performance Fine tuning.
 *
 * Revision 1.32.2.9.4.3  2012/11/27 19:08:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new column i.e. no of cases to the report
 *
 * Revision 1.32.2.9.4.2  2012/11/08 14:01:30  schepuri
 * CASE201112/CR201113/LOG201121
 * added special instruction field
 *
 * Revision 1.32.2.9.4.1  2012/10/26 08:05:40  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick report by order sorting
 *
 * Revision 1.32.2.9  2012/08/10 09:32:24  spendyala
 * CASE201112/CR201113/LOG201121
 * Replacing Special character with ASCII code.
 *
 * Revision 1.32.2.8  2012/08/01 07:29:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Routing
 *
 * Revision 1.32.2.7  2012/07/04 09:40:09  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Addr2 in ship address
 *
 * Revision 1.32.2.6  2012/07/03 21:51:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Remove Special Characters.
 *
 * Revision 1.32.2.5  2012/05/11 14:52:19  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to SKU bar code printing.
 *
 * Revision 1.32.2.4  2012/04/20 14:23:39  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.32.2.3  2012/04/20 08:36:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.32.2.2  2012/02/21 06:54:07  spendyala
 * CASE201112/CR201113/LOG201121
 * Code syn b/w Trunck and Branch.
 *
 * Revision 1.32.2.1  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.33  2012/01/13 00:34:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Pickreport
 *
 * Revision 1.32  2011/12/30 16:13:02  spendyala
 * CASE201112/CR201113/LOG201121
 * added fulfilment order to sublist and barcode to fulfillment order.
 *
 * Revision 1.31  2011/12/23 14:06:10  gkalla
 * CASE201112/CR201113/LOG201121
 * Added barcode and dynamic logo
 *
 * Revision 1.30  2011/12/23 12:45:10  gkalla
 * CASE201112/CR201113/LOG201121
 * Added barcode and dynamic logo
 *
 *****************************************************************************/
/**
 * This is the main function to print the PICK LIST against the given wave number  
 */
function PickReportPDFNEWSuitelet(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Pick Report');
		var vQbWave,vQbfullfillmentNo ="";
		var getwaveNo=request.getParameter('custparam_ebiz_wave_no');
		var getFullfillmentNo=request.getParameter('custparam_ebiz_fullfilmentno');
		//var getButtonId=request.getParameter('id');
		SetPrintFlag(getwaveNo,getFullfillmentNo);
		var filters = new Array();

//		9-STATUS.OUTBOUND.PICK_GENERATED ,26-STATUS.OUTBOUND.FAILED
		//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));

		if(request.getParameter('custparam_ebiz_wave_no')!=null && request.getParameter('custparam_ebiz_wave_no')!="")
		{
			vQbWave = request.getParameter('custparam_ebiz_wave_no');
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(vQbWave)));
		} 
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		if(request.getParameter('custparam_ebiz_fullfilmentno')!= null && request.getParameter('custparam_ebiz_fullfilmentno')!= "")
		{
			vQbfullfillmentNo = request.getParameter('custparam_ebiz_fullfilmentno');
			filters.push(new nlobjSearchFilter('name', null, 'is', vQbfullfillmentNo));
		}


		var columns = new Array();

		columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[1] = new nlobjSearchColumn('custrecord_skiptask');
		columns[2] = new nlobjSearchColumn('custrecord_line_no');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[4] = new nlobjSearchColumn('custrecord_bin_locgroup_seq');
		columns[5] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[6] = new nlobjSearchColumn('custrecord_sku');
		columns[7] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[8] = new nlobjSearchColumn('custrecord_tasktype');
		columns[9] = new nlobjSearchColumn('custrecord_lpno');
		columns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[11] = new nlobjSearchColumn('custrecord_sku_status');
		columns[12] = new nlobjSearchColumn('custrecord_packcode');
		columns[13] = new nlobjSearchColumn('name');
		columns[14] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
		columns[15] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[16] = new nlobjSearchColumn('custrecord_container');
		columns[17] = new nlobjSearchColumn('description','custrecord_sku');
		columns[18] = new nlobjSearchColumn('upccode','custrecord_sku');
		columns[19] = new nlobjSearchColumn('custrecord_batch_no');
		columns[20] = new nlobjSearchColumn('custrecord_total_weight');
		columns[21] = new nlobjSearchColumn('custrecord_ebizwmscarrier');
		columns[22] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
		columns[23] = new nlobjSearchColumn('custrecord_expirydate');
		columns[24] = new nlobjSearchColumn('salesdescription','custrecord_sku');

		/*columns[13].setSort(false);
		columns[14].setSort(false);*/
		columns[0].setSort();
		columns[1].setSort();
		columns[4].setSort();
		columns[5].setSort();
		columns[6].setSort();
		columns[7].setSort(true);



		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);


		var itemsarr = new Array();
		var itemdimsarr = new Array();

		if(searchresults!=null && searchresults.length > 0)
		{
			for (var i1 = 0; i1 < searchresults.length; i1++){
				var searchresult = searchresults[i1];
				itemsarr.push(searchresult.getValue('custrecord_ebiz_sku_no'));
			}
		}

			if(itemsarr !=null && itemsarr !='')
			{
				itemsarr = removeDuplicateElement(itemsarr);
				nlapiLogExecution('ERROR', 'itemsarr',itemsarr);

				nlapiLogExecution('ERROR', 'Time Stamp Before calling  for all items',TimeStampinSec());
				itemdimsarr = getItemDimensions(itemsarr,-1);
				nlapiLogExecution('ERROR', 'Time Stamp After calling  for all items',TimeStampinSec());
			}
		if(searchresults!=null&&searchresults!=''&&searchresults.length>0)
		{
			
			if(searchresults.length>100)
			{
				var param = new Array();
				param['custscript_ebiz_wave_no'] = vQbWave;
				param['custscript_ebiz_fullfilmentno'] = vQbfullfillmentNo;
				//nlapiScheduleScript('customscript_wms_pickreportscheduler', null,param);
				nlapiScheduleScript('customscript_ebiz_pickreport_scheduler', null,param);// case# 201415743


				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_email_alert_type', null, 'anyof', ['2']));
				filters.push(new nlobjSearchFilter('custrecord_email_trantype', null, 'anyof', ['2']));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_user_email');
				columns[1] = new nlobjSearchColumn('custrecord_email_option');
				var searchresults1 = nlapiSearchRecord('customrecord_email_config', null, filters, columns);		 
				var email= "";
				var emailbcc="";
				var emailcc="";
				var emailappend="";
				var emailbccappend= new Array();
				var emailccappend=new Array();
				var count=0;
				var bcccount=0;
				var substirngemail= '';
				//nlapiLogExecution('ERROR', 'searchresults.length',searchresults.length);
				for(var g=0;searchresults1!=null && g<searchresults1.length;g++)
				{
					var emailtext=searchresults1[g].getText('custrecord_email_option');
					nlapiLogExecution('ERROR','emailtext',emailtext);
					if(emailtext=="BCC")
					{
						emailbccappend[bcccount]=searchresults1[g].getValue('custrecord_user_email');
						bcccount++;		 
					}
					else if(emailtext=="CC")
					{
						emailccappend[count]=searchresults1[g].getValue('custrecord_user_email');
						count++;		 
					}
					else
					{
						email =searchresults1[g].getValue('custrecord_user_email');
						emailappend +=email+";";
					}

					substirngemail= emailappend.substring(0, emailappend .length-1);
				} 

				showInlineMessage(form, 'Confirmation', 'Pick Report has been generated and sent mail to  '+ substirngemail);
				response.writePage(form);
				return;
			}
			var totalwt=0;

			var SoIds=new Array();
			var soname = new Array();
			for ( var intg = 0; intg < searchresults.length; intg++)
			{
				SoIds[intg]=searchresults[intg].getValue('custrecord_ebiz_order_no');
				soname[intg]=searchresults[intg].getValue('name');
			}


			var distinctSoIds = removeDuplicateElement(SoIds);

			var distname = removeDuplicateElement(soname);
			



			var vline, vitem, vqty, vTaskType,vlotbatch, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono,vcontlp,vcontsize,vclusno,vweight,vcaseqty,vnoofcases;
			var upccode="";
			var itemdesc="";
			var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;

			//date
			var sysdate=DateStamp();
			var systime=TimeStamp();
			var Timez=calcTime('-5.00');
			var datetime= new Date();
			datetime=datetime.toLocaleTimeString() ;
			var finalimageurl = '';

			var url;
			/*var ctx = nlapiGetContext();
			if (ctx.getEnvironment() == 'PRODUCTION') 
			{
				url = 'https://system.netsuite.com';			
			}
			else if (ctx.getEnvironment() == 'SANDBOX') 
			{
				url = 'https://system.sandbox.netsuite.com';				
			}*/

			/*var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
			if (filefound) 
			{ 
				nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
				var imageurl = filefound.getURL();
				//finalimageurl = url + imageurl;//+';';
				finalimageurl = imageurl;//+';';
				finalimageurl=finalimageurl.replace(/&/g,"&amp;");

			} 
			else 
			{
				nlapiLogExecution('ERROR', 'Event', 'No file;');
			}
*/
			for ( var count = 0; count < distinctSoIds.length; count++)
			{
				var CartonArray=new Array();
				var totalwt=0;var pageno=0;
				var vtotalcube=0;
				var trantype = nlapiLookupField('transaction', SoIds, 'recordType');
				var salesorder = nlapiLoadRecord(trantype, distinctSoIds[count]);
				var	address = salesorder.getFieldValue('shipaddressee');
				nlapiLogExecution('ERROR', 'salesorder',salesorder);
				var ismultilineship=salesorder.getFieldValue('ismultishipto');
				if(searchresults!=null && searchresults!='')
				{
					var vlineno=searchresults[0].getValue('custrecord_line_no');

				}
				var shiptovalue=salesorder.getLineItemValue('item','shipaddress',vlineno);
				var shiptotext=salesorder.getLineItemText('item','shipaddress',vlineno);
				nlapiLogExecution('ERROR', 'ismultilineship',ismultilineship);
				nlapiLogExecution('ERROR', 'shiptovalue',shiptovalue);
				nlapiLogExecution('ERROR', 'shiptotext',shiptotext);
				var customerrecord=salesorder.getFieldValue('entity');

				
				var locationinternalid=salesorder.getFieldValue('location');
				
				var LogoValue;
				var LogoText;
				if(locationinternalid != null && locationinternalid != ""){
					var companylist = nlapiLoadRecord('location', locationinternalid);
					 LogoValue=companylist.getFieldValue('logo');
					 LogoText=companylist.getFieldText('logo');
				}
				nlapiLogExecution('ERROR','logo value',LogoValue);
				nlapiLogExecution('ERROR','logo text ',LogoText);
				var filefound='';
				//var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
				if(LogoText !=null && LogoText !='')
				  filefound = nlapiLoadFile('Images/'+LogoText+''); 
				else
					 filefound = nlapiLoadFile('Images/LOGOCOMP.jpg');
				
				if (filefound) 
				{ 
					nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
					var imageurl = filefound.getURL();
					nlapiLogExecution('ERROR','imageurl',imageurl);
					//var finalimageurl = url + imageurl;//+';';
					var finalimageurl = imageurl;//+';';
					//finalimageurl=finalimageurl+ '&expurl=T;';
					nlapiLogExecution('ERROR','imageurl',finalimageurl);
					finalimageurl=finalimageurl.replace(/&/g,"&amp;");

				} 
				else 
				{
					nlapiLogExecution('ERROR', 'Event', 'No file;');
				}


				nlapiLogExecution('ERROR', 'customerrecord',customerrecord);
				var entityrecord ;
				if(customerrecord != "" && customerrecord != null)
				{
					if(trantype=='vendorreturnauthorization')
						{
						entityrecord = nlapiLoadRecord('vendor', customerrecord);
						}
					else{
					entityrecord = nlapiLoadRecord('customer', customerrecord);
					    }
				}


				if(address != null && address !="")
					address=address.replace(replaceChar,'');
				else
					address="";

				var	HNo = salesorder.getFieldValue('shipaddr1');
				if(HNo != null && HNo !="")
					HNo=HNo.replace(replaceChar,'');
				else
					HNo="";
				var	addr2 = salesorder.getFieldValue('shipaddr2');
				if(addr2 != null && addr2 !="")
					addr2=addr2.replace(replaceChar,'');
				else
					addr2="";
				nlapiLogExecution('ERROR','addr2',addr2);
				var	city = salesorder.getFieldValue('shipcity');
				if(city != null && city !="")
					city=city.replace(replaceChar,'');
				else
					city="";
				var	state = salesorder.getFieldValue('shipstate');
				if(state != null && state !="")
					state=state.replace(replaceChar,'');
				else
					state="";
				var	country = salesorder.getFieldValue('shipcountry');
				if(country != null && country !="")
					country=country.replace(replaceChar,'');
				else
					country="";
				var	zipcode = salesorder.getFieldValue('shipzip');
				var	carrier = salesorder.getFieldText('shipmethod');
				if(carrier != null && carrier !="")					
				carrier = carrier.replace(/&/g,"&amp;");
				
				//carrier=carrier;
				var SalesorderNo= salesorder.getFieldValue('tranid');

				var tempInstructions=salesorder.getFieldValue('custbody_nswmspoinstructions');
				var Instructions="";
				if(tempInstructions!=null)
				{
					Instructions=tempInstructions;
				}
				if(Instructions != null && Instructions !="")
					Instructions=Instructions.replace(replaceChar,'');
				else
					Instructions="";

				//calculate total wt for particular so#
				/*for (var x = 0; x < searchresults.length; x++)
				{
					var searchresult = searchresults[x];
					var ContainerLP=searchresult.getValue('custrecord_container_lp_no');
					vdono = searchresult.getValue('name');
					if(vdono.split('.')[0] == SalesorderNo)
					{

						if( searchresults[x].getValue('custrecord_total_weight')!=null && searchresults[x].getValue('custrecord_total_weight')!="")
							totalwt=parseFloat(totalwt)+parseFloat(searchresults[x].getValue('custrecord_total_weight'));
					}
					var result=GetCubeWt_LpMaster(ContainerLP,salesorder.getFieldValue('location'));
					totalwt=parseFloat(totalwt)+parseFloat(result[0]);
				}*/

				if(ismultilineship=='T')
				{
					if(entityrecord!=null && entityrecord!='')
					{
						var custlineitemcount=entityrecord.getLineItemCount('addressbook');
						for(var customerline=1;customerline<=custlineitemcount;customerline++)
						{	var custline=parseInt(customerline).toString();
//						var customerlabel = entityrecord.getLineItemValue('addressbook','label',custline);
						//phonenumber=entityrecord.getLineItemValue('addressbook','phone',custline);
//						if(customerlabel==shiptotext)
						var customerlabelid = entityrecord.getLineItemValue('addressbook','internalid',custline);
						if(customerlabelid==shiptovalue)
						{
							nlapiLogExecution('ERROR', 'test','test1');
							address = entityrecord.getLineItemValue('addressbook','addressee',custline);
							if(address != null && address !="")
								address=address.replace(replaceChar,'');
							else
								address="";
							HNo= entityrecord.getLineItemValue('addressbook','addr1',custline);
							if(HNo != null && HNo !="")
								HNo=HNo.replace(replaceChar,'');
							else
								HNo="";
							addr2 = entityrecord.getLineItemValue('addressbook','addr2',custline);
							if(addr2 != null && addr2 !="")
								addr2=addr2.replace(replaceChar,'');
							else
								addr2="";

							city = entityrecord.getLineItemValue('addressbook','city',custline);
							if(city != null && city !="")
								city=city.replace(replaceChar,'');
							else
								city="";
							state = entityrecord.getLineItemValue('addressbook','dropdownstate',custline);
							if(state != null && state !="")
								state=state.replace(replaceChar,'');
							else
								state="";
							zipcode = entityrecord.getLineItemValue('addressbook','zip',custline);
							if(zipcode != null && zipcode !="")
								zipcode=zipcode.replace(replaceChar,'');
							country=entityrecord.getLineItemValue('addressbook','country',custline);
							if(country != null && country !="")
								country=country.replace(replaceChar,'');
							else
								country="";
						}
						}
					}
				}

				for (var x = 0; x < searchresults.length; x++)
				{
					var searchresult = searchresults[x];
					var ContainerLP=searchresult.getValue('custrecord_container_lp_no');
					CartonArray[x]=ContainerLP;
					//nlapiLogExecution('ERROR','ContainerLP',ContainerLP);
				}

				var duplicateContainerLp= vremoveDuplicateElement(CartonArray);
				nlapiLogExecution('ERROR','duplicateContainerLp',duplicateContainerLp);

				/*for ( var count = 0; count < duplicateContainerLp.length; count++) 
				{
					nlapiLogExecution('ERROR','duplicateContainerLp',duplicateContainerLp.length);
					var result=GetCubeWt_LpMaster(duplicateContainerLp[count],whLocation);
					nlapiLogExecution('ERROR','result',result[0]);

					totalwt=totalwt+(parseFloat(result[0])*parseFloat(1));
					vtotalcube=vtotalcube+(parseFloat(result[1])*parseFloat(1));
				}
				 */
				for (var x = 0; x < searchresults.length; x++)
				{
					var searchresult = searchresults[x];
					vdono = searchresult.getValue('name');
					if(vdono.split('.')[0] == SalesorderNo)
					{

						if( searchresults[x].getValue('custrecord_total_weight')!=null && searchresults[x].getValue('custrecord_total_weight')!="")
							totalwt=parseFloat(totalwt)+parseFloat(searchresults[x].getValue('custrecord_total_weight'));
					}
				}
				var newvdono = distname[count];
				nlapiLogExecution('ERROR','totalwt',totalwt);
				var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

				if(count==0)
					var strxml = "<table width='100%' >";
				else
				{
					var strxml=strxml+"";
					strxml += "<table width='100%' >";
				}
				if(pageno==0)
				{
					pageno=parseFloat(pageno+1);
				}


				strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
				strxml += "FT Pick Report ";
				strxml += "</td><td align='right'>&nbsp;</td></tr></table>";
				strxml += "<p align='right'>Date/Time:"+Timez+"</p>";
				strxml +="<table style='width:100%;'>";
				strxml +="<tr><td valign='top'>";
				strxml +="<table align='left' style='width:70%;' border='1'>";
				strxml +="<tr><td align='left' style='width:51px'>Wave# :</td>";

				strxml +="<td>";
				if(vQbWave != null && vQbWave!= "")
				{
					strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
					strxml += vQbWave;
					strxml += "\"/>";
				}
				strxml += "</td></tr>";

				strxml +="<tr><td align='left' style='width:51px'>Order# :</td>";

				strxml +="<td>";
				if(SalesorderNo != null && SalesorderNo != "")
				{
					strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
					strxml += SalesorderNo;
					strxml += "\"/>";
				}
				strxml += "</td></tr>";

				strxml +="<tr><td align='left' style='width:51px'>Total Weight(lbs):</td>";
				strxml +="<td>"+totalwt+"</td></tr>";
				strxml +="</table><table><tr><td>&nbsp;</td></tr></table>	<table align='left' style='width:70%;' border='1'>";
				strxml +="<tr><td align='left' style='width:51px'>Carrier:</td>";
				strxml +="<td align='left'>"+carrier+"</td></tr>"; 

				/*//splinstructions = splinstructions.replace(replaceChar,'');
				strxml +="<tr><td align='left' style='width:51px'>Special Instructions:</td>";
				//strxml +="<td align='left'>"+splinstructions+"</td></tr>"; 
				strxml +="<td align='left'>"+Instructions+"</td></tr>"; */

				strxml +="</table></td>";
				strxml +="<td>";
				strxml +="<table align='right' style='width:60%;' border='1'>";
				strxml +="<tr><td align='center' colspan='2'><b>Ship To</b></td></tr>";
				strxml +="<tr><td align='right' style='width:51px'>Address:</td>";
				strxml +="<td>"+address.replace(replaceChar,'')+"</td></tr>";
				strxml +="<tr><td style='width:51px'>&nbsp;</td>";
				strxml +="<td>"+HNo.replace(replaceChar,'')+"</td></tr>";
				if(addr2 != null && addr2 != '')
				{	
					strxml +="<tr><td style='width:51px'>&nbsp;</td>";
					strxml +="<td>"+addr2.replace(replaceChar,'')+"</td></tr>";
				}
				strxml +="<tr><td align='right' style='width:51px'>City:</td>";
				strxml +="<td>"+city.replace(replaceChar,'')+"</td></tr>";
				strxml +="<tr><td align='right' style='width:51px'>State:</td>";
				strxml +="<td>"+state.replace(replaceChar,'')+"</td></tr>";
				strxml +="<tr><td align='right' style='width:51px'>Zip:</td>";
				if(zipcode != null && zipcode !="" && zipcode!='null' )// Case# 20147997
				strxml +="<td>"+zipcode.replace(replaceChar,'')+"</td></tr>";
				else
					{
					strxml +="<td>"+zipcode+"</td></tr>";
					}
				strxml +="<tr><td align='right' style='width:51px'>Country:</td>";
				if(country != null && country !="" && country!='null' )// Case# 20147997
				strxml +="<td>"+country.replace(replaceChar,'')+"</td></tr>";			
				else
					{
					strxml +="<td>"+country+"</td></tr>";
					}
				strxml +="</table>";
				strxml +=" <p>&nbsp;</p>";
				strxml +="</td></tr></table>";
				if(Instructions!="")
				{
					strxml +=" <p>&nbsp;</p>";
					strxml +="<table  width='100%' style='width:100%;' >";
					strxml +="<tr style=\"font-weight:bold\"><td width='100%' style='border-width: 0.5px; border-color: #000000'>";
					strxml +="<span style='font-size:9'>Order Instructions: <p> "+Instructions.replace(replaceChar,'')+"</p></span>";
					strxml +="</td></tr></table>";
					strxml +=" <p>&nbsp;</p>";
				}
				strxml +="<table  width='100%'>";
				strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
				strxml += " Fulfillment#";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += "Line #";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
				strxml += " Part#/Item";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
				strxml += "UPC Code";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='15%' style='border-width: 1px; border-color: #000000'>";
				strxml += "Item Description";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
				strxml += "Status";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";//uncommented
				strxml += "Pack Code";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";	

				strxml += "<td width='3%' style='border-width: 1px; border-color: #000000'>";			
				strxml += "Qty";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='3%' style='border-width: 1px; border-color: #000000'>";			
				strxml += "No.Of Cases";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
				strxml += "Bin Location";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";//uncommented
				strxml += "LP #";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
				strxml += "Lot #";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
				strxml += "Expiry Date";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
				strxml += "Container LP";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
				strxml += "Container Size";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
				strxml += "Cluster #";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += "Weight(lbs)";
				strxml += "</td>";


				strxml += "<td style='border-width: 1px; border-color: #000000'>";
				strxml += "Zone";
				strxml += "</td>";

				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += "WMS Carrier";
				strxml =strxml+  "</td></tr>";
				var tempLineNo;var pagetotalno=0;
				if (searchresults != null) {
					for (var i = 0; i < searchresults.length; i++) {
						var searchresult = searchresults[i];

						vline = searchresult.getValue('custrecord_line_no');
						vlotbatch = searchresult.getValue('custrecord_batch_no');
						vitem = searchresult.getValue('custrecord_ebiz_sku_no');
						vqty = searchresult.getValue('custrecord_expe_qty');
						vTaskType = searchresult.getText('custrecord_tasktype');
						vLpno = searchresult.getValue('custrecord_lpno');
						vlocationid = searchresult.getValue('custrecord_actbeginloc');
						vlocation = searchresult.getText('custrecord_actbeginloc');
						vSKUID=searchresult.getValue('custrecord_sku');

						//The below code is commented by Satish.N on 12/05/2012 as we can deirectly get Item name from open task.
						//var Itype = nlapiLookupField('item', vSKUID, 'recordType');
						//var ItemRec = nlapiLoadRecord(Itype, vSKUID);
						//nlapiLogExecution('ERROR', 'Time Stamp after loading Item record',TimeStampinSec());
						//vSKU = ItemRec.getFieldValue('itemid');

						vSKU = searchresult.getText('custrecord_sku');
						vskustatus = searchresult.getText('custrecord_sku_status');
						vpackcode = searchresult.getValue('custrecord_packcode');
						vdono = searchresult.getValue('name');
						vcontlp=searchresult.getValue('custrecord_container_lp_no');
						vcontsize=searchresult.getText('custrecord_container');
						vclusno=searchresult.getValue('custrecord_ebiz_clus_no');

						upccode=searchresult.getValue('upccode','custrecord_sku');
						/*itemdesc=searchresult.getValue('description','custrecord_sku');
						if(itemdesc==null||itemdesc=="")
							itemdesc=searchresult.getValue('salesdescription','custrecord_sku');*/
						
						for(var actSOcount=1;actSOcount<=salesorder.getLineItemCount('item');actSOcount++)
						{
							var actLineno=salesorder.getLineItemValue('item','line',actSOcount);
							if(vline==actLineno)
							{
								var itemdesc=salesorder.getLineItemValue('item','description',actSOcount);
								break;
							}
						}
						vweight=searchresult.getValue('custrecord_total_weight');
						var vWMSCarrier=searchresult.getText('custrecord_ebizwmscarrier');
						var vzone=searchresult.getText('custrecord_ebiz_zoneid'); 
						var vExpiryDate=searchresult.getValue('custrecord_expirydate');

						if(upccode != null && upccode !="")
							upccode=upccode.replace(replaceChar,'');

						if(vskustatus != null && vskustatus !="")
							vskustatus=vskustatus;//.replace(replaceChar,'');

						if(vlotbatch != null && vlotbatch !="")
							vlotbatch=vlotbatch.replace(replaceChar,'');

						if(vcontsize != null && vcontsize !="")
							vcontsize=vcontsize.replace(replaceChar,'');

						if(vzone != null && vzone !="")
							vzone=vzone;//.replace(replaceChar,'');

						if(vWMSCarrier != null && vWMSCarrier !="")
							
						vWMSCarrier = vWMSCarrier.replace(/&/g,"&amp;");
						//vWMSCarrier=vWMSCarrier ;
						

						if (itemdimsarr != null && itemdimsarr.length > 0) 
						{	
							//nlapiLogExecution('ERROR', 'AllItemDims.length', itemdimsarr.length);
							for(var p = 0; p < itemdimsarr.length; p++)
							{
								var itemval = itemdimsarr[p][0];

								if(itemdimsarr[p][0] == vitem)
								{
									//nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', vitem);

									var skuDim = itemdimsarr[p][2];
									var skuQty = itemdimsarr[p][3];	
									var packflag = itemdimsarr[p][5];	
									var weight = itemdimsarr[p][6];
									var cube = itemdimsarr[p][7];
									var uom = itemdimsarr[p][1];

									if(skuDim == '2'){
										vcaseqty = skuQty;
										break;
									}								
								}	
							}
						}
						//nlapiLogExecution('ERROR', 'Case Qty: ', vcaseqty);
//						case# 20124184� Start
						if(vcaseqty != 0 && vcaseqty!='' && vcaseqty!=null && !isNaN(vcaseqty)){
							vnoofcases = parseFloat(vqty / vcaseqty);
						}
						//end
						else{
							//case# 20124229 Start
							if(vqty==null || vqty=='')
								vqty=0;
							//case# 20124229 End
							vnoofcases = parseFloat(vqty);
						}
						//nlapiLogExecution('ERROR', '# of Cases: ', vnoofcases);


						if(vdono.split('.')[0] == SalesorderNo)
						{
							if(tempLineNo==null)
							{
								tempLineNo=vline;
							}

							if(vline!=tempLineNo)
							{
								var lineInstructions=salesorder.getLineItemValue('item','custcol2',tempLineNo);
								if(lineInstructions!=null && lineInstructions!="")
								{
									nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
									nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
									strxml +=  "<tr><td width='100%' colspan='15' style='border-width: 1px; border-color: #000000'>";
									strxml +="<span style='font-size:9'> Line Instructions :</span>"+ "<p>"+lineInstructions.replace(replaceChar,'')+"</p>";
									strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
								}
							}

							strxml =strxml+  "<tr>";

							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
							if(vdono != null && vdono != "")
							{
								strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
								//strxml += vdono.replace(replaceChar,'');
								strxml += vdono; // case# 201413165
								strxml += "\"/>";
							}
							strxml += "</td>";

							strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
							strxml += vline.replace(replaceChar,'');
							strxml += "</td>";

							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
							if(vSKU != null && vSKU != "")
							{
								strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
								var sku = vSKU.replace(/"/g,"&#34;");
								strxml += sku.replace(/&/g,"&amp;");
								strxml += "\"/>";
							}
							strxml += "</td>";

							strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";
							strxml += upccode.replace(replaceChar,'');
							strxml += "</td>";

							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
							if(itemdesc != null && itemdesc != "")//case # 20147955  Replace || with &&
							//	strxml += itemdesc;//.replace(replaceChar,'');
							strxml += itemdesc.replace(/&/g,'');
							strxml += "</td>";

							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
							if(vskustatus != null && vskustatus != "")
							strxml += vskustatus;//.replace(replaceChar,'');
							strxml += "</td>";

							strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
							strxml += vpackcode;//.replace(replaceChar,'');
							strxml += "</td>";	

							strxml += "<td width='3%' align='right' style='border-width: 1px; border-color: #000000'>";			
							strxml += vqty;
							strxml += "</td>";

							strxml += "<td width='3%' align='right' style='border-width: 1px; border-color: #000000'>";			
							strxml += vnoofcases.toFixed(2);
							strxml += "</td>";

							strxml += "<td width='5%' style='border-width: 1px; border-color: #000000'>";
							//strxml += vlocation.replace(replaceChar,'');
							strxml += vlocation.replace(/-/g,"&#45;"); //Case# 201410335
							strxml += "</td>";

							strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
							strxml += vLpno.replace(replaceChar,'');
							strxml += "</td>";

							strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
							strxml += vlotbatch.replace(replaceChar,'');
							strxml += "</td>";

							strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
							strxml += vExpiryDate;
							strxml += "</td>";

							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
							strxml += vcontlp.replace(replaceChar,'');
							strxml += "</td>";

							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
							strxml += vcontsize.replace(replaceChar,'');
							strxml += "</td>";

							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
							if(vclusno != null && vclusno != "")
							{
								strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
								strxml += vclusno;
								strxml += "\"/>";
							}
							strxml += "</td>";

							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
							strxml += vweight;
							strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";


							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
							strxml += vzone;//.replace(replaceChar,'');
							strxml +="</td>";

							strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
							strxml += vWMSCarrier;
							strxml =strxml+  "</td></tr>";						  
							tempLineNo=vline;
							pagetotalno=parseFloat(pagetotalno)+1;
						}
					}
				}

				var lineInstructions=salesorder.getLineItemValue('item','custcol2',tempLineNo);

				if(lineInstructions!=null && lineInstructions!="")
				{
					lineInstructions=lineInstructions.replace(replaceChar,'');

					strxml +=  "<tr><td width='100%' colspan='15' style='border-width: 1px; border-color: #000000'>";
					strxml +="<span style='font-size:9'> Line Instructions :</span>"+ "<p>"+lineInstructions.replace(replaceChar,'')+"</p>";
					strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
					tempLineNo=null;
				}
				strxml =strxml+"</table>";
				if((distinctSoIds.length-count)>1)
				{
					pageno=parseFloat(pageno)+1;
					strxml=strxml+ "<p style='page-break-after:always'></p>";
				}
				else
				{
					//pageno=parseFloat(pageno)+1;
					//strxml=strxml+ "<p style='vertical-align:bottom;align:right;font-size:9'>Page No: "+pageno+" </p>";
				}
			}
			strxml =strxml+ "\n</body>\n</pdf>";
			xml=xml +strxml;

			var file = nlapiXMLToPDF(xml);	
			response.setContentType('PDF','PickReport.pdf');
			response.write( file.getValue() );
		}
		else
		{
			showInlineMessage(form, 'Error', 'No Data Found');
			response.writePage(form);
		}
	}
	else //this is the POST block
	{

	}
}

function SetPrintFlag(waveno,fullfillment)
{
	nlapiLogExecution('ERROR','Into SetPrintFlag',waveno);
	var filter=new Array();

	if(waveno!=null && waveno!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave','null','equalto',parseFloat(waveno)));

	if(fullfillment!=null&&fullfillment!="")
		filter.push(new nlobjSearchFilter('custrecord_lineord','null','is',fullfillment.toString()));

	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_printflag');
	column[1]=new nlobjSearchColumn('custrecord_print_count');

	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,column);
	if(searchrecord!=null)
	{
		for ( var count = 0; count < searchrecord.length; count++) {
			var intId=searchrecord[count].getId();
			var flagcount=searchrecord[count].getValue('custrecord_print_count');
			var Newflagcount;
			if(flagcount==null || flagcount=="")
				Newflagcount=1;
			else
				Newflagcount=parseFloat(flagcount)+1;

			var fieldNames = new Array(); 
			fieldNames.push('custrecord_printflag');  
			fieldNames.push('custrecord_print_count');
			fieldNames.push('custrecord_ebiz_pr_dateprinted');

			var newValues = new Array(); 
			newValues.push('T');
			newValues.push(Newflagcount);
			newValues.push(DateStamp());			

			nlapiSubmitField('customrecord_ebiznet_ordline', intId, fieldNames, newValues);
		}
	}
	nlapiLogExecution('ERROR','Out of SetPrintFlag',waveno);
}

function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}

var itemDimensionList = new Array();
function getItemDimensions(skuList,maxno){
	nlapiLogExecution('ERROR','into  getItemDimensions', skuList.length);

	var itemDimCount = 0;

	var filters = new Array();
	//case # 20124394�Start
	if(skuList!=null && skuList!='')
		//case # 20124394� End
		filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', skuList));		// ALL SKUs IN SKU LIST
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));							// ACTIVE RECORDS ONLY

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');								// ITEM/SKU
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');								// UOM
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');						// UOM LEVEL
	columns[3] = new nlobjSearchColumn('custrecord_ebizqty');									// QUANTITY
	columns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');								// BASE UOM FLAG
	columns[5] = new nlobjSearchColumn('custrecord_ebizdims_packflag');							// PACK FLAG	
	columns[6] = new nlobjSearchColumn('custrecord_ebizweight');								// WEIGHT	
	columns[7] = new nlobjSearchColumn('custrecord_ebizcube');									// CUBE	
	columns[8] = new nlobjSearchColumn('internalid');
	columns[8].setSort(true);

	// SEARCH FROM ITEM DIMENSIONS
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(searchResults != null && searchResults != '' && searchResults.length >= 1000){
		var maxno1=searchResults[searchResults.length-1].getValue(columns[8]);
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];
			itemDimensionList[itemDimCount++] = skuDimRecord;
		}

		getItemDimensions(skuList,maxno1);
	}
	else
	{
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];
			itemDimensionList[itemDimCount++] = skuDimRecord;
		}

	}
	nlapiLogExecution('ERROR','out of  getItemDimensions', skuList.length);
	return itemDimensionList;
}








function vremoveDuplicateElement(arr)
{
	var dups = {}; 
	return arr.filter(
			function(el) { 
				var hash = el.valueOf(); 
				var isDup = dups[hash]; 
				dups[hash] = true; 
				return !isDup; 
			}
	); 
}

