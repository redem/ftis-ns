function eBiz_Hook_fn(request, response) {
	if(request.getMethod()=="GET")
	{

		var varItem=null;

		var location=null; 

		var itemfamily=null ;

		var itemgroup=null; 
		genReplenGETSuitelet(request, response);
	}//Else
	else		
	{
		var form = nlapiCreateForm('Generate Replenishment');

		var ItemFamilyGroup = new Array();
		var Reportno = "";
		var inventoryArray = new Array();
		var Pickarray = new Array();

		var lineCount = request.getLineItemCount('custpage_replen_items');
		var availableQty = 0;
		var itemfamily;
		var itemgroup;
		nlapiLogExecution('ERROR','pickfaceDetails',lineCount);
		nlapiLogExecution('ERROR','itemfam',request.getParameter('custpage_itemfamily'));
		nlapiLogExecution('ERROR','itemgrp',request.getParameter('custpage_itemgroup'));

		var replenItemArray =  new Array();
		var currentRow = new Array();
		var j = 0;
		var idx = 0;
		var userselection = false;
		if (request.getParameter('custpage_itemfamily') != null && request.getParameter('custpage_itemfamily') != "") {
			itemfamily = request.getParameter('custpage_itemfamily');
		}
		if (request.getParameter('custpage_itemgroup') != null && request.getParameter('custpage_itemgroup') != "") {
			itemgroup = request.getParameter('custpage_itemgroup');
		}
		// Go through all the items in replenishment
		// Depending on user selection form a 2-dimensional array of data
		/*	To calculate number of replen tasks to be created depending on 
		 * 	available quantity and replen qty.
		 * 	0 = item, 
		 * 	1 = replenQty, 
		 * 	2 = replenRule, 
		 * 	3 = maxQty, 
		 * 	4 = minQty, 
		 * 	5 = pickfaceQty, 
		 * 	6 = whLocation, 
		 * 	7 = actEndLocation
		 *  8 = tempQty
		 *  9 = replenTaskCount
		 */
		for(var i = 1; i <= lineCount; i++){
			var selectValue = request.getLineItemValue('custpage_replen_items', 'custpage_replen_sel', i);
			if (selectValue == 'T') 
			{
				userselection = true;	        	
			}

			// Checking for the Check-Box; Selected = 'T'; Unselected = 'F'
			if (selectValue == 'T') {
				var item = request.getLineItemValue('custpage_replen_items', 'custpage_replen_sku', i);
				var replenQty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_rplnqty', i);
				//var replenRule = request.getLineItemValue('custpage_replen_items', 'custpage_skureplenruleid', i);
				var maxQty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_maxqty', i);
				var minQty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_minqty', i);
				var pickfaceQty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_qty', i);
				var whLocation = request.getLineItemValue('custpage_replen_items', 'custpage_whlocation', i);
				var actEndLocation = request.getLineItemValue('custpage_replen_items', 'custpage_replen_location', i);
				var pickfacefixedlp = request.getLineItemValue('custpage_replen_items', 'custpage_pickfacefixedlp', i);
				var status = request.getLineItemValue('custpage_replen_items', 'custpage_replen_status', i);
				var statusId = request.getLineItemValue('custpage_replen_items', 'custpage_replen_status_id', i);
				var roundQty = request.getLineItemValue('custpage_replen_items', 'custpage_round_qty', i);// case# 201414131


				var putawatqty=0;
				var pickqty=0;
				var openreplensqty=0;
				var Qohinpickface=0;
				nlapiLogExecution('ERROR','before  ifw',openreplensqty);
				nlapiLogExecution('ERROR','before  if',parseInt(openreplensqty));
				var serchreplensqty=getopenreplens(item,actEndLocation,whLocation);
				if(serchreplensqty!=null && serchreplensqty !='')
				{
					for(k=0;k<serchreplensqty.length;k++)
					{
						openreplensqty += serchreplensqty[k].getValue('custrecord_expe_qty');
					}
				}
				nlapiLogExecution('ERROR','after  ifw',openreplensqty);
				nlapiLogExecution('ERROR','after  if',parseInt(openreplensqty));
				var searchQohinpickface=getQOHinPFLocations(item,actEndLocation);
				if(searchQohinpickface!=null && searchQohinpickface!='')
				{
					Qohinpickface = searchQohinpickface[0].getValue('custrecord_ebiz_qoh',null,'sum');
				}

//				if(searchQohinpickface!=null && searchQohinpickface !='')
//				{
//				for(v=0;v<searchQohinpickface.length;v++)
//				{
//				Qohinpickface += searchQohinpickface[v].getValue('custrecord_expe_qty',null,'sum');
//				}
//				}


				var searchputawyqty=getOpenputawayqty(item,actEndLocation);
				if(searchputawyqty!=null && searchputawyqty!='')
				{
					putawatqty = searchputawyqty[0].getValue('custrecord_expe_qty',null,'sum');
				}
				var searchpickyqty=getOpenPickqty(item,actEndLocation);
				if(searchpickyqty!=null && searchpickyqty!='')
				{
					pickqty = searchpickyqty[0].getValue('custrecord_expe_qty',null,'sum');
				}
				
				nlapiLogExecution('ERROR','putawatqty',putawatqty);
				nlapiLogExecution('ERROR','pickqty',pickqty);
				nlapiLogExecution('ERROR','pickfaceQty',pickfaceQty);
				nlapiLogExecution('ERROR','openreplensqty',openreplensqty);
				
				if(openreplensqty==null || openreplensqty=='' || isNaN(openreplensqty))
					openreplensqty=0;

				if(putawatqty==null || putawatqty=='' || isNaN(putawatqty))
					putawatqty=0;

				if(pickqty==null || pickqty=='' || isNaN(pickqty))
					pickqty=0;
							
				if(roundQty == null || roundQty == '')
					roundQty=1;	
				
				nlapiLogExecution('ERROR','putawatqty',putawatqty);
				nlapiLogExecution('ERROR','pickqty',pickqty);
				nlapiLogExecution('ERROR','pickfaceQty',pickfaceQty);
				nlapiLogExecution('ERROR','replenQty',replenQty);
				nlapiLogExecution('ERROR','openreplensqty',openreplensqty);
				nlapiLogExecution('ERROR','Qohinpickface',Qohinpickface);
				nlapiLogExecution('ERROR','maxQty',maxQty);
				nlapiLogExecution('ERROR','roundQty',roundQty);
				if(Qohinpickface==null || Qohinpickface=='' || isNaN(Qohinpickface))
					Qohinpickface=0;

				var remQtyToCreate = parseInt(maxQty) - parseInt(Qohinpickface);
				nlapiLogExecution('ERROR','remQtyToCreate',remQtyToCreate);

				var replenTaskCount = Math.ceil(parseInt(remQtyToCreate) / parseInt(replenQty));
				nlapiLogExecution('ERROR','replenTaskCount',replenTaskCount);
				var qtyToFind = parseInt(replenTaskCount) * parseInt(replenQty);
				nlapiLogExecution('ERROR','qtyToFind',qtyToFind);
				//var tempQty = (parseInt(maxQty) - (parseInt(openreplensqty)  + parseInt(pickfaceQty) - parseInt(pickqty) + parseInt(putawatqty)));
				//var tempQty = (parseInt(maxQty) - (parseInt(openreplensqty) + parseInt(Qohinpickface) + parseInt(pickfaceQty) - parseInt(pickqty) + parseInt(putawatqty)));
				var tempQty = (parseInt(maxQty) - (parseInt(openreplensqty) + parseInt(Qohinpickface)  - parseInt(pickqty) + parseInt(putawatqty)));
				nlapiLogExecution('ERROR','tempQty22',(parseInt(maxQty) - (parseInt(openreplensqty) + parseInt(Qohinpickface)  - parseInt(pickqty) + parseInt(putawatqty))));
				nlapiLogExecution('ERROR','tempQty',tempQty);



				nlapiLogExecution('DEBUG','tempQty1',tempQty);

				var tempwithRoundQty = Math.floor(parseFloat(tempQty)/parseFloat(roundQty));

				if(tempwithRoundQty == null || tempwithRoundQty == '' || isNaN(tempwithRoundQty))
					tempwithRoundQty=0;

				nlapiLogExecution('DEBUG','tempwithRoundQty',tempwithRoundQty);

				tempQty = parseFloat(roundQty)*parseFloat(tempwithRoundQty);

				nlapiLogExecution('DEBUG','tempQty2',tempQty);
				
				
				


				currentRow = [item, replenQty, "", maxQty, minQty, pickfaceQty, whLocation, 
				              actEndLocation, tempQty, replenTaskCount, pickfacefixedlp,status,statusId,roundQty];

				// temporary calculation to determine the size of currentRow;
				// just in case we decide to add some more items in the form
				if(idx == 0)
					idx = currentRow.length;

				replenItemArray[j++] = currentRow;
			}
		}
		if(userselection==true)
		{
			var taskCount;
			var zonesArray = new Array();

			var replenRulesList = getListForAllReplenZones();

			var replenZoneList = "";
			var replenZoneTextList = "";
			var replenzonestatuslist="";
			var replenzonestatustextlist="";

			if (replenRulesList != null){
				for (var i = 0; i < replenRulesList.length; i++){
					nlapiLogExecution('ERROR','replenRulesList.length',replenRulesList.length);
					if (i == 0){
						replenZoneList += replenRulesList[i].getValue('custrecord_replen_strategy_zone');
						replenZoneTextList += replenRulesList[i].getText('custrecord_replen_strategy_zone');
						replenzonestatuslist +=replenRulesList[i].getValue('custrecord_replen_strategy_itemstatus');
						replenzonestatustextlist +=replenRulesList[i].getText('custrecord_replen_strategy_itemstatus');
					} else {
						replenZoneList += ',';
						replenZoneList += replenRulesList[i].getValue('custrecord_replen_strategy_zone');

						replenZoneTextList += ',';
						replenZoneTextList += replenRulesList[i].getText('custrecord_replen_strategy_zone');

						replenzonestatuslist += ',';
						replenzonestatuslist += replenRulesList[i].getValue('custrecord_replen_strategy_itemstatus');

						replenzonestatustextlist += ',';
						replenzonestatustextlist += replenRulesList[i].getText('custrecord_replen_strategy_itemstatus');
					}
				}
			} 

			nlapiLogExecution('ERROR','replenZoneList',replenZoneList);

			var itemList = buildItemListForReplenProcessing(replenItemArray, j);
			nlapiLogExecution('ERROR','replen Item List',itemList);



			var locnGroupList = getZoneLocnGroupsList(replenZoneList);	
			nlapiLogExecution('ERROR','Location Group List',locnGroupList);
			var ZoneAndLocationGrp =getZoneLocnGroupsListforRepln(replenZoneList,whLocation);
			nlapiLogExecution('DEBUG','ZoneAndLocationGrp',ZoneAndLocationGrp);

			//var locnGroupList = getZoneLocnGroupsList(replenZoneList);	
			//nlapiLogExecution('ERROR','Location Group List',locnGroupList);

			var inventorySearchResults = getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslist, whLocation);
			nlapiLogExecution('ERROR','inventorySearchResults',inventorySearchResults);
//			if(userselection==true)
//			{
			if (inventorySearchResults != null && inventorySearchResults != ""){
				nlapiLogExecution('ERROR','test2','test2');
				// Get the report number
				if (Reportno == null || Reportno == "") {
					Reportno = GetMaxTransactionNo('REPORT');
					Reportno = Reportno.toString();
				}

				/*
				 * Loop through the total task count and get the location from where the item has to be 
				 * replenished.
				 */

				//Get the Stage Location in case of two step replenishment process
				//Commented By Rami Reddy
				var stageLocation='';
				//var stageLocation=GetStageLocation(item, "", whLocation, "", "BOTH");
				nlapiLogExecution('ERROR','Reportno',Reportno);

				var resultArray=replenRuleZoneLocation(inventorySearchResults, replenItemArray, Reportno, stageLocation, whLocation,'','','',ZoneAndLocationGrp);

				var result=resultArray[0];
				nlapiLogExecution('ERROR','result',result);
//				// For generating Clusters in case cluster is configured.
//				var clusterArray = new Array();
//				clusterArray[0] = replenMethodId;
//				clusterArray[1] = replenZoneId;
//				var boolfound = false;

////				for (m = 0; m < Pickarray.length; m++) {
////				var pickrecarray = Pickarray[m];
////				if (pickrecarray[0] == vPickMethod && pickrecarray[1] == vPickzone) {
////				boolfound = true;
////				break;
////				}
////				}
////				if (!boolfound) {
////				Pickarray.push(clusterArray);
////				}

//				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', recordId);
//				var invtAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');

//				if (invtAllocQty == null || isNaN(invtAllocQty) || invtAllocQty == "") {
//				invtAllocQty = 0;
//				}

//				invtAllocQty = parseInt(invtAllocQty) + parseInt(replenQty);
//				transaction.setFieldValue('custrecord_ebiz_alloc_qty', invtAllocQty);
//				transaction.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);

//				nlapiSubmitRecord(transaction, true);
////				}
////				}

////				if (Pickarray.length > 0) {
////				for (var r = 0; r < Pickarray.length; r++) {
////				nlapiLogExecution('ERROR', 'In to Cluster Loop');
////				var clusterPickarray = new Array();
//				//
////				clusterPickarray = Pickarray[r];
////				var vpickmethod = clusterPickarray[0];
////				var vpickzone = clusterPickarray[1];
////				clusterpicking(Reportno, vpickmethod, vpickzone,'9');
////				clusterpicking(Reportno, vpickmethod, vpickzone,'24');
////				}
////				}
//				}
				if(result==true)
				{
					showInlineMessage(form, 'Confirmation', 'Replenishment Report No', Reportno);
					response.writePage(form);
				}
				else
				{
					showInlineMessage(form, 'Error', 'Insufficient Inventory to generate replen', null);
					//showInlineMessage(form, 'Error', 'Confirm open replens / Putaway', null);
					response.writePage(form);
				}

			}
			else{
				nlapiLogExecution('ERROR','test1','test1');
				showInlineMessage(form, 'Error', 'Inventory not available in the replenishment zone(s)', null);
				response.writePage(form);
			}
		}
		else
		{
			nlapiLogExecution('ERROR','itemfamily',itemfamily);
			nlapiLogExecution('ERROR','itemgroup',itemgroup);
			genReplenGETSuitelet(request, response, location, item,itemfamily,itemgroup);
		} 
	}
} 





function genReplenGETSuitelet(request, response)
{
	try
	{
		var form = nlapiCreateForm('Generate Replenishment');

		form.setScript('customscript_inventoryclientvalidations');

		item = form.addField('custpage_item', 'multiselect', 'Item','Item');    

		whlocation = form.addField('custpage_location', 'select', 'Location');

		itemfamily = form.addField('custpage_itemfamily', 'select', 'Item Family','customrecord_ebiznet_sku_family');

		itemgroup = form.addField('custpage_itemgroup', 'select', 'Item Group','customrecord_ebiznet_sku_group');
		
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		
		msg.setLayoutType('outside','startcol');
		
//		whlocation.addSelectOption('', '');

		var filtersloc = new Array();
		filtersloc.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));

		var columnsloc = new Array();
		columnsloc.push(new nlobjSearchColumn('name'));

		var searchlocresults = nlapiSearchRecord('Location', null, filtersloc, columnsloc);

		if (searchlocresults != null && searchlocresults!='')
		{

			nlapiLogExecution('ERROR','test1',searchlocresults.length);
			for (var i = 0; i < searchlocresults.length; i++) 
			{
				var res = form.getField('custpage_location').getSelectOptions(searchlocresults[i].getValue('name'), 'is');
				if (res != null) 
				{
					if (res.length > 0)	                
						continue;	                
				}
				whlocation.addSelectOption(searchlocresults[i].getId(), searchlocresults[i].getValue('name'));
			}
		} 
		if(request.getParameter('custpage_location')!='' && request.getParameter('custpage_location')!=null)
		{
			whlocation.setDefaultValue(request.getParameter('custpage_location'));	
		}
		//itemfamily.addSelectOption('', '');
		if(request.getParameter('custpage_itemfamily')!='' && request.getParameter('custpage_itemfamily')!=null)
		{
			itemfamily.setDefaultValue(request.getParameter('custpage_itemfamily'));	
		}
//		itemgroup.addSelectOption('', '');
		if(request.getParameter('custpage_itemgroup')!='' && request.getParameter('custpage_itemgroup')!=null)
		{
			itemgroup.setDefaultValue(request.getParameter('custpage_itemgroup'));	
		}

		itemfamily = request.getParameter('custpage_itemfamily');
		itemgroup = request.getParameter('custpage_itemgroup');
		whlocation=request.getParameter('custpage_location');

		/*		var itemSubList = form.addSubList("custpage_replen_items", "list", "ItemList");
		itemSubList.addMarkAllButtons();
		itemSubList.addField("custpage_replen_sel", "checkbox", "Select");
		itemSubList.addField("custpage_replen_sku", "select", "Item", "item");
		itemSubList.addField("custpage_replen_location", "select", "Pickface Location", "customrecord_ebiznet_location");
		nlapiLogExecution('ERROR', 'itemgroup', itemgroup);
		nlapiLogExecution('ERROR', 'itemfamily', itemfamily);
		nlapiLogExecution('ERROR', 'whlocation', whlocation);

		itemSubList.addField("custpage_replen_packcode", "text", "PackCode");
		itemSubList.addField("custpage_replen_status", "text", "Status");
		itemSubList.addField("custpage_replen_minqty", "text", "Min Qty");
		itemSubList.addField("custpage_replen_maxqty", "text", "Max Qty");
		itemSubList.addField("custpage_replen_rplnqty", "text", "Replen Qty");
		itemSubList.addField("custpage_replen_qty", "text", "PickFace Loc Qty");


		itemSubList.addField("custpage_replen_allocqty", "text", "PickFace Loc Alloc Qty");
		itemSubList.addField("custpage_replen_availqty", "text", "PickFace Loc Avail Qty");


		itemSubList.addField("custpage_replen_isqtyavailableinwh", "text", "Is Qty Available in WH?");
		itemSubList.addField("custpage_skurecid", "text", "Recid").setDisplayType('hidden');
		itemSubList.addField("custpage_skureplenruleid", "text", "Replen Ruleid").setDisplayType('hidden');
		itemSubList.addField("custpage_whlocation", "text", "Site").setDisplayType('hidden');
		itemSubList.addField("custpage_itemfamily", "text", "itemfamily").setDisplayType('hidden');
		itemSubList.addField("custpage_itemgroup", "text", "itemgroup").setDisplayType('hidden');
		itemSubList.addField("custpage_pickfacefixedlp", "text", "FixedLP").setDisplayType('hidden');*/
		var item = request.getParameter('custpage_item');
		//var binLocation = request.getParameter('custpage_binlocation');

		var vInactiveBins=getInactiveBins();
		var vInactiveBinsArr=new Array();
		if(vInactiveBins != null && vInactiveBins != '')
		{
			for(var s=0;s<vInactiveBins.length;s++)
				vInactiveBinsArr.push(vInactiveBins[s].getValue('custrecord_pickbinloc'));
		}	
		var vItemArr=new Array();
		if(item != null && item != "")
		{
			vItemArr = item.split('');
		}
		nlapiLogExecution('ERROR', 'item Internal ID', item);


		/*var sublist = form.addSubList("custpage_items", "list", "Replenishment Item Details");

		//Item, Bin location, Min Qty, Max Qty, Replen Qty, QOH, Allocated Qty, Avail Qty, Location Committed Qty 
		//Pend Putw Qty, Pend Repln Qty, 
		sublist.addField("custpage_option", "text", "S.No");
		sublist.addField("custpage_item", "text", "Item");
		sublist.addField("custpage_itemfam", "text", "Item Family");
		sublist.addField("custpage_itemgroup", "text", "Item Group");
		sublist.addField("custpage_pflocation", "text", "Pickface Location");
		sublist.addField("custpage_minqty", "float", "Min. Qty");
		sublist.addField("custpage_maxqty", "float", "Max. Qty");
		sublist.addField("custpage_replqty", "float", "Replen. Qty");
		sublist.addField("custpage_qoh", "float", "Qty on Hand");
		sublist.addField("custpage_allocqty", "float", "Alloc. Qty");
		sublist.addField("custpage_availqty", "text", "Avail. Qty");
		sublist.addField("custpage_commqty", "float", "Committed Qty");
		sublist.addField("custpage_pputqty", "float", "Pend. PUTW Qty");
		sublist.addField("custpage_prplqty", "float", "Pend. REPL Qty");
		sublist.addField("custpage_reqqty", "float", "Required Qty");
		sublist.addField("custpage_bulkqty", "float", "Bulk Avail. Qty");*/

		// Pickface Location Details
		//var vPFResultSet = nlapiSearchRecord('Item','customsearch_ebiz_reqrepl_items', null, null);
		//var vPFDetailsArray = new Array();

		//var vPFResultSet = eBnfngetvPFResults(-1); 
		var serchreplensitems=getopenreplenitems(whlocation);
		
		nlapiLogExecution('ERROR', 'Time record starts1',TimeStampinSec());
		var vPFResultSetArray = eBnfngetvPFResults(vItemArr,itemgroup,itemfamily,whlocation,-1,serchreplensitems);
		nlapiLogExecution('ERROR', 'Time record starts2',TimeStampinSec());
		//var vPTResultSet = eBnfngetvPTResults(-1);
		var vPTResultSetArray = eBnfngetvPTResults(vItemArr,itemgroup,itemfamily,whlocation,-1,serchreplensitems);
		nlapiLogExecution('ERROR', 'Time record starts3',TimeStampinSec());
		// Pending Task Details
		//var vPTResultSet = nlapiSearchRecord('Item','customsearch_ebiz_pendrepl_tasks', null, null);
		//var vPTDetailsArray = new Array();

		var vPFItem,vPFItemId, vPTItem, vCmmQty, vPFLocation,vPFLocationId, vPTLocation, vMinQty, vMaxQty, vReplQty, vQOH, vAllocQty, vAvailQty, vPReplQty, vPPutQty,vPFItemStatus,pickfacefixedlp,site,vPFItemStatusId,vpackcode;// 201413569
		var vItemFam, vItemGroup;
		var vBulkQty = 0;
		var vGridIndex = 1;
		var vReqQty = 0;
		vPPutQty =  0;
		vPReplQty =  0;
		vCmmQty = 0;
		var vTotalLocQty = 0;
		var roundqty=0;
		var vReplenRecs=new Array();
		//PTSSDetails: Item | CmmQty | PFL | MinQty | MaxQty | ReplQty | PPUTQty | PPREPLQty 
		//PFSSDetails: Item | CmmQty | PFL | MinQty | MaxQty | ReplQty | QOH | AvailableQty | AllocQty

		//** Checking Pickaface Location Details

		//nlapiLogExecution('ERROR','Total Reocrds',vPFResultSet.length);
		if(vPFResultSetArray != null && vPFResultSetArray != "")
		{
			for(var r=0; r < vPFResultSetArray.length; r++)
			{
				var vPFResultSet = vPFResultSetArray[r];

				if(vPFResultSet != null && vPFResultSet != "")
				{
					for(var i=0; i < vPFResultSet.length; i++)
					{
						var result = vPFResultSet[i];
						var pfcolumns = result.getAllColumns();

						nlapiLogExecution('ERROR','Reocrd #',i);

						if( (result.getValue(pfcolumns[0]) != null) && (result.getValue(pfcolumns[0]) != '') )
						{
							vPFItem =  result.getValue(pfcolumns[0]);

						}


						nlapiLogExecution('ERROR','vPFItem',vPFItem);

						if( (result.getValue(pfcolumns[1]) != null) && (result.getValue(pfcolumns[1]) != '') )
						{
							vCmmQty =  parseFloat(result.getValue(pfcolumns[1]));
						}
						else
						{
							vCmmQty =  0;
						}

						if( (result.getValue(pfcolumns[2]) != null) && (result.getValue(pfcolumns[2]) != '') )
						{
							vPFLocation =  result.getText(pfcolumns[2]);
							vPFLocationId =  result.getValue(pfcolumns[2]);
						}

						if( (result.getValue(pfcolumns[3]) != null) && (result.getValue(pfcolumns[3]) != '') )
						{
							vMinQty =  parseFloat(result.getValue(pfcolumns[3]));
						}
						else
						{
							vMinQty =  0;
						}

						if( (result.getValue(pfcolumns[4]) != null) && (result.getValue(pfcolumns[4]) != '') )
						{
							vMaxQty =  parseFloat(result.getValue(pfcolumns[4]));//result.getValue(pfcolumns[4]);
						}
						else
						{
							vMaxQty =  0;
						}

						if( (result.getValue(pfcolumns[5]) != null) && (result.getValue(pfcolumns[5]) != '') )
						{
							vReplQty =  parseFloat(result.getValue(pfcolumns[5]));//result.getValue(pfcolumns[5]);
						}
						else
						{
							vReplQty =  0;
						}

						if( (result.getValue(pfcolumns[6]) != null) && (result.getValue(pfcolumns[6]) != '') )
						{
							vQOH =  parseFloat(result.getValue(pfcolumns[6]));//result.getValue(pfcolumns[6]);
						}
						else
						{
							vQOH =  0;
						}

						if( (result.getValue(pfcolumns[7]) != null) && (result.getValue(pfcolumns[7]) != '') )
						{
							vAvailQty =  parseFloat(result.getValue(pfcolumns[7]));//result.getValue(pfcolumns[7]);
						}
						else
						{
							vAvailQty =  0;
						}

						if( (result.getValue(pfcolumns[8]) != null) && (result.getValue(pfcolumns[8]) != '') )
						{
							vAllocQty =  parseFloat(result.getValue(pfcolumns[8]));//result.getValue(pfcolumns[8]);
						}
						else
						{
							vAllocQty =  0;
						}

						if( (result.getValue(pfcolumns[9]) != null) && (result.getValue(pfcolumns[9]) != '') )
						{
							vItemFam =  result.getText(pfcolumns[9]);
						}

						if( (result.getValue(pfcolumns[10]) != null) && (result.getValue(pfcolumns[10]) != '') )
						{
							vItemGroup =  result.getText(pfcolumns[10]);
						}

						if( (result.getValue(pfcolumns[11]) != null) && (result.getValue(pfcolumns[11]) != '') )
						{
							vBulkQty =  parseFloat(result.getValue(pfcolumns[11]));//result.getValue(pfcolumns[8]);
						}
						else
						{
							vBulkQty =  0;
						}
						if( (result.getValue(pfcolumns[12]) != null) && (result.getValue(pfcolumns[12]) != '') )
						{ 	 
							vPFItemId =  result.getValue(pfcolumns[12]);
						}
						if( (result.getValue(pfcolumns[13]) != null) && (result.getValue(pfcolumns[13]) != '') )
						{
							vPFItemStatus =  result.getText(pfcolumns[13]);
							vPFItemStatusId =  result.getValue(pfcolumns[13]);

						}
						if( (result.getValue(pfcolumns[14]) != null) && (result.getValue(pfcolumns[14]) != '') )
						{
							//pickfacefixedlp =  result.getValue(pfcolumns[14]);//
							pickfacefixedlp =  result.getText(pfcolumns[14]);

						}
						if( (result.getValue(pfcolumns[15]) != null) && (result.getValue(pfcolumns[15]) != '') )
						{
							site =  result.getValue(pfcolumns[15]);

						}
						if( (result.getValue(pfcolumns[16]) != null) && (result.getValue(pfcolumns[16]) != '') )
						{
							roundqty =  result.getValue(pfcolumns[16]);

						}
						if( (result.getValue(pfcolumns[17]) != null) && (result.getValue(pfcolumns[17]) != '') )
						{
							vpackcode =  result.getValue(pfcolumns[17]);

						}

						var	openreplensqty=0;
						var	pickqty=0;
						var	putawatqty=0;
						var pendingReplens='';
						//  nlapiLogExecution('ERROR','vPTResultSet.length',vPTResultSet.length);


						if(vPTResultSetArray != null && vPTResultSetArray != "")
						{
							for(var s=0; s < vPTResultSetArray.length; s++)
							{
								var vPTResultSet = vPTResultSetArray[s];

								//** Pending Putaway and Replen Task Details Array
								if(vPTResultSet != null && vPTResultSet != "")
								{
									for(var j=0; j < vPTResultSet.length; j++)
									{
										var result = vPTResultSet[j];

										var ptcolumns = result.getAllColumns();

										//				        nlapiLogExecution('ERROR','j',j);

										if( (result.getValue(ptcolumns[0]) != null) && (result.getValue(ptcolumns[0]) != '') )
										{
											vPTItem =  result.getValue(ptcolumns[0]);
										}

										if( (result.getValue(ptcolumns[2]) != null) && (result.getValue(ptcolumns[2]) != '') )
										{
											vPTLocation =  result.getText(ptcolumns[2]);
										}

										//** Checing condition for the same item and location
										if( (vPFItem == vPTItem) && (vPFLocation == vPTLocation))
										{
											nlapiLogExecution('ERROR','vPTItem',vPTItem);
											nlapiLogExecution('ERROR','vPTLocation',vPTLocation);

											//** Get the Pending Putaway Qty Details
											if( (result.getValue(ptcolumns[6]) != null) && (result.getValue(ptcolumns[6]) != '') )
											{
												vPPutQty =  parseFloat(result.getValue(ptcolumns[6]));//result.getValue(ptcolumns[6]);
												if (isNaN(vPPutQty)) {
													vPPutQty = 0;
												}
											}
											else
											{
												vPPutQty =  0;
											}

											if( (result.getValue(ptcolumns[7]) != null) && (result.getValue(ptcolumns[7]) != '') )
											{

												vPReplQty =  parseFloat(result.getValue(ptcolumns[7]));//result.getValue(ptcolumns[7]);

												if (isNaN(vPReplQty)) {
													vPReplQty = 0;
												}
												nlapiLogExecution('ERROR','(vPReplQty1) ',vPReplQty);
											}
											else
											{
												vPReplQty =  0;
											}

											if(parseFloat(vQOH) < parseFloat(vMaxQty))
											{
												pendingReplens	= getOpenQty(vPFItem,vPFLocation);
											}
											
											//to exit from the pending task details if the current item is found
											break;
										}
										//** //** End condition for Checing condition for the same item and location        

									}//** Pend Task Details End For


								}//** Pend Task Details End If

							}//** Pend Task Details Main Array End For

						}//** Pend Task Details Main Array End If
						
						nlapiLogExecution('DEBUG', 'pendingReplens', pendingReplens);
						if(pendingReplens !=null && pendingReplens!='')
						{
							if(pendingReplens[0][2]!=null && pendingReplens[0][2]!='' && pendingReplens[0][2]!='null' && pendingReplens[0][2]!='undefined')
							{
								openreplensqty = pendingReplens[0][2];
								nlapiLogExecution('DEBUG', 'openreplensqty', openreplensqty);
								
							}
							if(pendingReplens[0][0]!=null && pendingReplens[0][0]!='' && pendingReplens[0][0]!='null' && pendingReplens[0][0]!='undefined')
							{
								putawatqty = pendingReplens[0][0];
								nlapiLogExecution('DEBUG', 'putawatqty', putawatqty);
							}
							if(pendingReplens[0][2]!=null && pendingReplens[0][1]!='' && pendingReplens[0][1]!='null' && pendingReplens[0][1]!='undefined')
							{
								pickqty = pendingReplens[0][1];
								nlapiLogExecution('DEBUG', 'pickqty', pickqty);
							}
						}

						var reqdQty = (parseFloat(vMaxQty) - (parseFloat(openreplensqty) + parseFloat(vQOH)  - parseFloat(pickqty) + parseFloat(putawatqty)));
						
						nlapiLogExecution('ERROR','(roundqty) ',roundqty );
						var roundreqdqty = Math.floor(parseFloat(reqdQty)/parseFloat(roundqty));
						nlapiLogExecution('DEBUG', 'roundreqdqty', roundreqdqty);
						//** Main Condition to determine the required replenishment records
						//(QOH  + Pend Putw Qty + Pend Repln Qty) ? (Location Committed Qty)  < 0
						//(QOH  + Pend Putw Qty + Pend Repln Qty) < Max Qty
						nlapiLogExecution('ERROR','(vQOH) ',(vQOH) );
						nlapiLogExecution('ERROR','(vPPutQty) ',(vPPutQty) );
						nlapiLogExecution('ERROR','(vPReplQty) ',(vPReplQty) );
						nlapiLogExecution('ERROR','(vCmmQty) ',(vCmmQty) );


						nlapiLogExecution('ERROR','(vMaxQty) ',(vMaxQty) );

						vTotalLocQty = 0;

						vTotalLocQty = parseFloat(vQOH) + parseFloat(vPPutQty) + parseFloat(vPReplQty); 

						//if((vTotalLocQty - vCmmQty < 0) && (vTotalLocQty < vMaxQty) &&(parseFloat(roundreqdqty) > 0) )
						if((vBulkQty > 0) && (vTotalLocQty - vCmmQty < 0) && (vTotalLocQty < vMaxQty) &&(parseFloat(roundreqdqty) > 0) )// case# 201413433
						{
							// Here need to show the record
							vReqQty = vMaxQty - vTotalLocQty; //(vQOH+vPPutQty+vPReplQty);
							nlapiLogExecution('ERROR','(vReqQty) ',(vReqQty) );
							nlapiLogExecution('ERROR','(vPFItem) ',(vPFItemId) );
							nlapiLogExecution('ERROR','(vPFLocationId) ',(vPFLocationId) );
							/*							itemSubList.setLineItemValue('custpage_replen_sku', vGridIndex, vPFItemId);
							itemSubList.setLineItemValue('custpage_replen_rplnqty', vGridIndex, vReplQty);
							itemSubList.setLineItemValue('custpage_replen_location', vGridIndex, vPFLocationId);
							itemSubList.setLineItemValue('custpage_replen_maxqty', vGridIndex, vMaxQty);
							itemSubList.setLineItemValue('custpage_replen_minqty', vGridIndex, vMinQty);
							itemSubList.setLineItemValue('custpage_replen_qty', vGridIndex, vQOH);

							itemSubList.setLineItemValue('custpage_replen_allocqty', vGridIndex, vAllocQty);*/
							if (isNaN(vAvailQty)) {
								vAvailQty = 0;
							}
							/*							itemSubList.setLineItemValue('custpage_replen_availqty', vGridIndex, vAvailQty);

							itemSubList.setLineItemValue('custpage_replen_isqtyavailableinwh', vGridIndex, vBulkQty);
							//itemSubList.setLineItemValue('custpage_skureplenruleid', vGridIndex, replenRuleId);
							itemSubList.setLineItemValue('custpage_whlocation', vGridIndex, site);
							itemSubList.setLineItemValue('custpage_pickfacefixedlp', vGridIndex, pickfacefixedlp);
							itemSubList.setLineItemValue('custpage_replen_status', vGridIndex, vPFItemStatus);*/

							if(vInactiveBinsArr.indexOf(vPFLocationId) == -1)
							{
								var currentRow = [vPFItemId, vReplQty, vPFLocationId, vMaxQty, vMinQty, vQOH, vAllocQty, 
								                  vAvailQty, vBulkQty, site, pickfacefixedlp,vPFItemStatus,vPFItemStatusId,vpackcode,roundqty];
								vReplenRecs.push(currentRow);
							}
							/*form.getSubList('custpage_items').setLineItemValue('custpage_option', vGridIndex, vGridIndex.toString());
							form.getSubList('custpage_items').setLineItemValue('custpage_item', vGridIndex, vPFItem);
							//nlapiLogExecution('ERROR','(vPFItem) 1 ',(vPFItem) );
							form.getSubList('custpage_items').setLineItemValue('custpage_itemfam', vGridIndex, vItemFam);
							form.getSubList('custpage_items').setLineItemValue('custpage_itemgroup', vGridIndex, vItemGroup);
							form.getSubList('custpage_items').setLineItemValue('custpage_pflocation', vGridIndex, vPFLocation);
							//nlapiLogExecution('ERROR','(vPFLocation) 1 ',(vPFLocation) );
							form.getSubList('custpage_items').setLineItemValue('custpage_minqty', vGridIndex, vMinQty);
							//nlapiLogExecution('ERROR','(vMinQty) 1 ',(vMinQty) );
							form.getSubList('custpage_items').setLineItemValue('custpage_maxqty', vGridIndex, vMaxQty);
							//nlapiLogExecution('ERROR','(vMaxQty) 1 ',(vMaxQty) );
							form.getSubList('custpage_items').setLineItemValue('custpage_replqty', vGridIndex, vReplQty);	
							//nlapiLogExecution('ERROR','(vReplQty) 1 ',(vReplQty) );
							form.getSubList('custpage_items').setLineItemValue('custpage_qoh', vGridIndex, vQOH);
							//nlapiLogExecution('ERROR','(vQOH) 1 ',(vQOH) );
							form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', vGridIndex, vAllocQty);
							//nlapiLogExecution('ERROR','(vAllocQty) 1 ',(vAllocQty) );	
							if (vAvailQty <= 0)
							{
								form.getSubList('custpage_items').setLineItemValue('custpage_availqty', vGridIndex,'<font color=#ff0000><b>' +  vAvailQty + '</b></font>');
							}
							else
							{	

								form.getSubList('custpage_items').setLineItemValue('custpage_availqty', vGridIndex,  vAvailQty);
							}


							//nlapiLogExecution('ERROR','(vAvailQty) 1 ',(vAvailQty) );
							form.getSubList('custpage_items').setLineItemValue('custpage_commqty', vGridIndex, vCmmQty);
							//nlapiLogExecution('ERROR','(vCmmQty) 1 ',(vCmmQty) );
							form.getSubList('custpage_items').setLineItemValue('custpage_pputqty', vGridIndex, vPPutQty);
							//nlapiLogExecution('ERROR','(vPPutQty) 1 ',(vPPutQty) );
							form.getSubList('custpage_items').setLineItemValue('custpage_prplqty', vGridIndex, vPReplQty);
							//nlapiLogExecution('ERROR','(vPReplQty) 1 ',(vPReplQty) );
							form.getSubList('custpage_items').setLineItemValue('custpage_reqqty', vGridIndex, vReqQty);
							form.getSubList('custpage_items').setLineItemValue('custpage_bulkqty', vGridIndex, vBulkQty);*/
							//nlapiLogExecution('ERROR','(vReqQty) 1 ',(vReqQty) );

							vGridIndex++;
						}
						//Main Condition If

					}//** PickfaceLoc Det For Ending

				}//** PickfaceLoc Det If Ending

			}//** PickfaceLoc Det Main Array For Ending

			if(vReplenRecs!=null && vReplenRecs!='')
			{
				fnBindPaging(request,response,form,vReplenRecs);
			}
			else
			{
				//showInlineMessage(form, 'Error','Insufficent inventory');
				
				nlapiLogExecution('ERROR', 'Insufficent inventory',TimeStampinSec());
				msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'Insufficient inventory', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
				response.writePage(form);
			//	return;
			}


		}//** PickfaceLoc Det Main Array If Ending
		//case# 201410391 adding else block to display message when inventory not available for item
		else
		{
			

			nlapiLogExecution('ERROR', 'Insufficent inventory',TimeStampinSec());
			msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'Inventory not available in bulk location', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			response.writePage(form);
			//showInlineMessage1(form, 'Error','Inventory not available in bulk location', '');
		}
		//form.addSubmitButton('Display');

		//form.addButton('custpage_print','Export To Excel','Printexcel()');

		//form.addButton('custpage_print','Print','Printreport('+ vSOId +')');
		form.addSubmitButton('Generate Replenishment');
		var end = new Date();
		nlapiLogExecution('ERROR', 'Time record starts4',TimeStampinSec());
		response.writePage(form);

	}// Try
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in POST',exp);
	}//Catch	
}

function fnBindPaging(request,response,form,enoughAvailArray)
{
	if(enoughAvailArray!=null  && enoughAvailArray.length>0)
	{



		var idx = 0;

		var f1 = 0;
		var f2 = 0;
		var f3 = 0;

		t1 = new Date();

		nlapiLogExecution('ERROR', 'LOOP COUNT', enoughAvailArray.length);
		var itemSubList = form.addSubList("custpage_replen_items", "list", "ItemList");
		itemSubList.addMarkAllButtons();
		itemSubList.addField("custpage_replen_sel", "checkbox", "Select");
		itemSubList.addField("custpage_replen_sku", "select", "Item", "item");
		itemSubList.addField("custpage_replen_location", "select", "Pickface Location", "customrecord_ebiznet_location");
		nlapiLogExecution('ERROR', 'itemgroup', itemgroup);
		nlapiLogExecution('ERROR', 'itemfamily', itemfamily);
		nlapiLogExecution('ERROR', 'whlocation', whlocation);

		itemSubList.addField("custpage_replen_packcode", "text", "PackCode");
		itemSubList.addField("custpage_replen_status", "text", "Status");
		itemSubList.addField("custpage_replen_minqty", "text", "Min Qty");
		itemSubList.addField("custpage_replen_maxqty", "text", "Max Qty");
		itemSubList.addField("custpage_replen_rplnqty", "text", "Replen Qty");
		itemSubList.addField("custpage_replen_qty", "text", "PickFace Loc Qty");
		itemSubList.addField("custpage_round_qty", "text", "Round Qty");//.setDisplayType('hidden');


		itemSubList.addField("custpage_replen_allocqty", "text", "PickFace Loc Alloc Qty");
		itemSubList.addField("custpage_replen_availqty", "text", "PickFace Loc Avail Qty");


		itemSubList.addField("custpage_replen_isqtyavailableinwh", "text", "Is Qty Available in WH?");
		itemSubList.addField("custpage_skurecid", "text", "Recid").setDisplayType('hidden');
		itemSubList.addField("custpage_skureplenruleid", "text", "Replen Ruleid").setDisplayType('hidden');
		itemSubList.addField("custpage_whlocation", "text", "Site").setDisplayType('hidden');
		itemSubList.addField("custpage_itemfamily", "text", "itemfamily").setDisplayType('hidden');
		itemSubList.addField("custpage_itemgroup", "text", "itemgroup").setDisplayType('hidden');
		itemSubList.addField("custpage_pickfacefixedlp", "text", "FixedLP").setDisplayType('hidden');
		itemSubList.addField("custpage_replen_status_id", "text", "Status Id").setDisplayType('hidden');// case# 201413434
		
		
		var hiddenField_selectpage = form.addField('custpage_hiddenfield', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');// case# 201416383
		
		
		if(enoughAvailArray.length>25)
		{
			//var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('entry');
			var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');// case# 201416384
			pagesize.setDisplaySize(10,10);
			pagesize.setLayoutType('outsidebelow', 'startrow');
			var select= form.addField('custpage_selectpage','select', 'Select Records');	
			select.setLayoutType('outsidebelow', 'startrow');			
			select.setDisplaySize(200,30);
			if (request.getMethod() == 'GET'){
				pagesize.setDefaultValue("25");
				pagesizevalue=25;
			}
			else
			{
				if(request.getParameter('custpage_pagesize')!=null)
				{pagesizevalue= request.getParameter('custpage_pagesize');}
				else
				{pagesizevalue= 25;pagesize.setDefaultValue("25");}
			}
			form.setScript('customscript_inventoryclientvalidations');

			var len=enoughAvailArray.length/parseInt(pagesizevalue);
			for(var z=1;z<=Math.ceil(len);z++)
			{

				var from;var to;

				to=parseInt(z)*parseInt(pagesizevalue);
				from=(parseInt(to)-parseInt(pagesizevalue))+1;

				if(parseInt(to)>enoughAvailArray.length)
				{
					to=enoughAvailArray.length;
					test=from.toString()+","+to.toString(); 
				}

				var temp=from.toString()+" to "+to.toString();
				var tempto=from.toString()+","+to.toString();
				select.addSelectOption(tempto,temp);

			} 
			if (request.getMethod() == 'POST'){

				if(request.getParameter('custpage_selectpage')!=null ){

					select.setDefaultValue(request.getParameter('custpage_selectpage'));	

				}
				if(request.getParameter('custpage_pagesize')!=null ){

					pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

				}
			}
		}
		else
		{
			pagesizevalue=25;
		}
		var minval=0;var maxval=parseInt(pagesizevalue);
		if(parseInt(pagesizevalue)>enoughAvailArray.length)
		{
			maxval=enoughAvailArray.length;
		}
		var selectno=request.getParameter('custpage_selectpage');
		if(selectno!=null )
		{
			var temp= request.getParameter('custpage_selectpage');
			var temparray=temp.split(',');
			nlapiLogExecution('ERROR', 'temparray',temparray.length);
			nlapiLogExecution('ERROR', 'temparray[1]',temparray[1]);
			nlapiLogExecution('ERROR', 'temparray[0]',temparray[0]);
			var diff=parseInt(temparray[1])-(parseInt(temparray[0])-1);
			nlapiLogExecution('ERROR', 'diff',diff);

			var pagevalue=request.getParameter('custpage_pagesize');
			nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
			if(pagevalue!=null)
			{
				if(parseInt(diff)==parseInt(pagevalue)|| test==selectno)
				{ 
					var temparray=selectno.split(',');	
					nlapiLogExecution('ERROR', 'temparray.length ', temparray.length);  
					minval=parseInt(temparray[0])-1;
					nlapiLogExecution('ERROR', 'temparray[0] ', temparray[0]);  
					maxval=parseInt(temparray[1]);
					nlapiLogExecution('ERROR', 'temparray[1] ', temparray[1]);  
				}
			}
		}


		// For all SKUs in pickface locations
		for(var j = minval; j < maxval; j++){


//			// Determine if there is enough inventory in the replen zone
			var skuRecordFromPF = enoughAvailArray[j];

			itemSubList.setLineItemValue('custpage_replen_sku', idx + 1, skuRecordFromPF[0]);
			itemSubList.setLineItemValue('custpage_replen_rplnqty', idx + 1, skuRecordFromPF[1]);
			itemSubList.setLineItemValue('custpage_replen_location', idx + 1, skuRecordFromPF[2]);
			itemSubList.setLineItemValue('custpage_replen_maxqty', idx + 1, skuRecordFromPF[3]);
			itemSubList.setLineItemValue('custpage_replen_minqty', idx + 1, skuRecordFromPF[4]);
			itemSubList.setLineItemValue('custpage_replen_qty', idx + 1, skuRecordFromPF[5]);

			itemSubList.setLineItemValue('custpage_replen_allocqty', idx + 1, skuRecordFromPF[6]);

			itemSubList.setLineItemValue('custpage_replen_availqty', idx + 1, skuRecordFromPF[7]);

			itemSubList.setLineItemValue('custpage_replen_isqtyavailableinwh', idx + 1, skuRecordFromPF[8]);
			//itemSubList.setLineItemValue('custpage_skureplenruleid', vGridIndex, replenRuleId);
			itemSubList.setLineItemValue('custpage_whlocation', idx + 1, skuRecordFromPF[9]);
			itemSubList.setLineItemValue('custpage_pickfacefixedlp', idx + 1, skuRecordFromPF[10]);
			itemSubList.setLineItemValue('custpage_replen_status', idx + 1, skuRecordFromPF[11]);
			itemSubList.setLineItemValue('custpage_replen_status_id', idx + 1, skuRecordFromPF[12]);
			itemSubList.setLineItemValue('custpage_replen_packcode', idx + 1, skuRecordFromPF[13]);
			itemSubList.setLineItemValue('custpage_round_qty', idx + 1, skuRecordFromPF[14]);


			idx++;


		}
	}	
	else
	{
		showInlineMessage(form, 'Error','Insufficent inventory', '');

	}
}


var eBnfngetvPTResult = new Array();
var eBnfngetvPFResult =new Array();

function eBnfngetvPFResults(vItemArr,itemgroup,itemfamily,whlocation,maxno,serchreplensitems)
{
	var filters = new Array();

	nlapiLogExecution('ERROR', 'vItemArr in eBnfngetvPFResults ',vItemArr );
	nlapiLogExecution('ERROR', 'itemgroup ',itemgroup );
	nlapiLogExecution('ERROR', 'itemfamily ',itemfamily );
	nlapiLogExecution('ERROR', 'whlocation ',whlocation );
	nlapiLogExecution('ERROR', 'maxno ',maxno );
	if(maxno != -1)
	{
		filters.push(new nlobjSearchFilter('internalid', null, 'greaterthan', maxno).setSummaryType('max'));
	}
	if(itemgroup != null && itemgroup != '')
		filters.push(new nlobjSearchFilter('custitem_item_group', null, 'anyof', itemgroup));
	if(itemfamily != null && itemfamily != '')
		filters.push(new nlobjSearchFilter('custitem_item_family', null, 'anyof', itemfamily));

	if(whlocation != null && whlocation != '')// case# 201413435 displaying 2 records with different locations
		filters.push(new nlobjSearchFilter('inventorylocation', null, 'anyof', whlocation));
	//	filters.push(new nlobjSearchFilter('custrecord_pickface_location','custrecord_pickfacesku','anyof', whlocation));
	//filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', whlocation));
	if(vItemArr != null && vItemArr != '')
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', vItemArr));
	if(serchreplensitems != null && serchreplensitems != '')
		filters.push(new nlobjSearchFilter('internalid', null, 'noneof', serchreplensitems));
	//var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, column);
	var vPFResultSet = nlapiSearchRecord('Item','customsearch_ebiz_reqrepl_items', filters, null);
	//var vPFResultSetNF = nlapiSearchRecord('Item','customsearch_ebiz_reqrepl_items', null, null);


	if(vPFResultSet != null)
	{
		nlapiLogExecution('ERROR', 'vPFResultSet.length ',vPFResultSet.length );
		if(vPFResultSet.length >= 1000)
		{
			var result = vPFResultSet[vPFResultSet.length-1];
			var pfcolumns = result.getAllColumns();

			var maxno1 = vPFResultSet[vPFResultSet.length-1].getValue(pfcolumns[12]); //Need to give the id
			nlapiLogExecution('ERROR', 'vPFResultSet MaxNo',maxno1);			

			eBnfngetvPFResult.push(vPFResultSet);

			eBnfngetvPFResults(vItemArr,itemgroup,itemfamily,whlocation,maxno1);
		}
		else
		{
			eBnfngetvPFResult.push(vPFResultSet);
		}
	}

	return eBnfngetvPFResult;
}




function eBnfngetvPTResults(vItemArr,itemgroup,itemfamily,whlocation,maxno,serchreplensitems)
{
	nlapiLogExecution('ERROR', 'vItemArr in eBnfngetvPTResults ',vItemArr );
	nlapiLogExecution('ERROR', 'itemgroup ',itemgroup );
	nlapiLogExecution('ERROR', 'itemfamily ',itemfamily );
	nlapiLogExecution('ERROR', 'whlocation ',whlocation );
	nlapiLogExecution('ERROR', 'maxno ',maxno );
	var filters = new Array();

	if(maxno != -1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}
	if(itemgroup != null && itemgroup != '')
		filters.push(new nlobjSearchFilter('custitem_item_group', null, 'anyof', itemgroup));
	if(itemfamily != null && itemfamily != '')
		filters.push(new nlobjSearchFilter('custitem_item_family', null, 'anyof', itemfamily));
	if(vItemArr != null && vItemArr != '')
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', vItemArr));
	if(serchreplensitems != null && serchreplensitems != '')
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', serchreplensitems));
	
	if(whlocation != null && whlocation != '')
		filters.push(new nlobjSearchFilter('inventorylocation', null, 'anyof', whlocation));
		//filters.push(new nlobjSearchFilter('custrecord_pickface_location','custrecord_pickfacesku','anyof', whlocation));

	var vPTResultSet = nlapiSearchRecord('Item','customsearch_ebiz_pendrepl_tasks', filters, null);
	//var vPTResultSet = nlapiSearchRecord('Item','customsearch_ebiz_pendrepl_tasks', null, null);

	if(vPTResultSet != null)
	{
		nlapiLogExecution('ERROR', 'vPTResultSet.length ',vPTResultSet.length );
		if(vPTResultSet.length >= 1000)
		{
			var result = vPTResultSet[vPTResultSet.length-1];
			var pfcolumns = result.getAllColumns();

			var maxno1 = vPTResultSet[vPTResultSet.length-1].getValue(pfcolumns[1]); //Need to give the id
			nlapiLogExecution('ERROR', 'vPTResultSet MaxNo',maxno1);

			eBnfngetvPTResult.push(vPTResultSet);
			//eBnfngetvPTResults(vItemArr,itemgroup,itemfamily,whlocation,maxno);
		}
		else
		{
			eBnfngetvPTResult.push(vPTResultSet);
		}
	}

	return eBnfngetvPTResult;
}
function getopenreplens(fulfilmentItem,pfLocationId,pfWHLoc)
{

	nlapiLogExecution('ERROR','fulfilmentItem',fulfilmentItem);
	nlapiLogExecution('ERROR','pfLocationId',pfLocationId);
	nlapiLogExecution('ERROR','pfWHLoc',pfWHLoc);
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', controlno); // Remove this from where clause
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_sku', null,'anyof' ,fulfilmentItem));
	//filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null,'is' ,pfLocationId));
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null,'is' ,pfLocationId));
	if(pfWHLoc != null && pfWHLoc != '')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null,'anyof' ,pfWHLoc));
	// Add pickface location filter
	// Add location(site) for filter

	var columns = new Array();
	//columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}
function getopenreplenitems(pfWHLoc)
{

	
	nlapiLogExecution('ERROR','pfWHLoc',pfWHLoc);
	var filters = new Array();
	var itemarray=new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', controlno); // Remove this from where clause
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); //Cyclecount released
	if(pfWHLoc != null && pfWHLoc != '')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null,'anyof' ,pfWHLoc));
	

	var columns = new Array();
	//columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');
	columns[0] = new nlobjSearchColumn('custrecord_sku',null ,'group');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!='')
		{
		for (var t = 0; t < searchresults.length; t++){
			itemarray.push(searchresults[t].getValue('custrecord_sku',null ,'group'));
		}
		}
	return itemarray;
}

function getQOHinPFLocations(itemid,binlocation){

	nlapiLogExecution('ERROR', 'Into getQOHinPFLocation');
	nlapiLogExecution('ERROR', 'itemid',itemid);
	nlapiLogExecution('ERROR', 'binlocation',binlocation);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', binlocation));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemid));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));	

	var columns = new Array();	
	//columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('ERROR', 'Out of pfSKUInvList',pfSKUInvList);
	return pfSKUInvList;
}
function getOpenputawayqty(item,actEndLocation)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	filters[3] = new nlobjSearchFilter('custrecord_sku', null, 'anyof',item);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}
function getOpenPickqty(item,actEndLocation)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	filters[3] = new nlobjSearchFilter('custrecord_sku', null, 'anyof',item);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}
/**
 * Build the string for all the items and zones selected in the list. 
 * This item list has to be passed as a string to search for all bin locations in the inventory
 * NOTE: COULD RETURN NULL
 *  
 * @param replenItemArray
 * @param itemArrayLength
 * @returns List of items as comma separated string
 */
function buildItemListForReplenProcessing(replenItemArray, itemArrayLength){
	var replenItemList = new Array();

	for (var p = 0; p < itemArrayLength; p++){
		replenItemList.push(replenItemArray[p][0]);
	}

	return replenItemList;
}

function getInactiveBins()
{
	var filters = new Array();

	filters.push(new nlobjSearchFilter('isinactive', 'custrecord_pickbinloc',
			'is', 'T')); 
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_pickbinloc');

	var searchresultsInactive = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);
	return searchresultsInactive;
}





function getOpenQty(item,actEndLocation)
{
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', actEndLocation);
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['2','3','8']);				
	filters[1] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	filters[2] = new nlobjSearchFilter('custrecord_sku', null, 'anyof',item);
	filters[3]= new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 9, 20, 21]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_tasktype',null,'group');
	columns[2] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);



	var pendingTaskArray = new Array();
	var pendingopenPutawayTasks =0;
	var pendingopenPickTasks =0;
	var pendingopenReplenTasks=0;

	if(searchresults !=null && searchresults !='')
	{
		for(var m=0;m<searchresults.length;m++)
		{
			var tasktype=searchresults[m].getValue('custrecord_tasktype',null,'group');

			if(tasktype==2 && (actEndLocation == searchresults[m].getValue('custrecord_actbeginloc',null,'group')))
			{
				pendingopenPutawayTasks +=parseFloat(searchresults[m].getValue('custrecord_expe_qty',null,'sum'));
			}
			else if(tasktype==3 && (actEndLocation == searchresults[m].getValue('custrecord_actbeginloc',null,'group')))
			{
				pendingopenPickTasks +=parseFloat(searchresults[m].getValue('custrecord_expe_qty',null,'sum'));
			}
			else if(tasktype==8 && (actEndLocation == searchresults[m].getValue('custrecord_actendloc',null,'group')))
			{
				pendingopenReplenTasks +=parseFloat(searchresults[m].getValue('custrecord_expe_qty',null,'sum'));
			}
			else
			{

			}

		}
	}
	pendingTaskArray[0]=[pendingopenPutawayTasks,pendingopenPickTasks,pendingopenReplenTasks];
	return pendingTaskArray;
}