/**
 * @author Phani Attili
 * This Suitelet is meant to display scan picking location Screen
 */
/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_PickingLPException.js,v $
 *     	   $Revision: 1.1.2.1.4.11 $
 *     	   $Date: 2014/06/13 12:27:36 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingLPException.js,v $
 * Revision 1.1.2.1.4.11  2014/06/13 12:27:36  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.1.4.10  2014/05/30 15:32:24  sponnaganti
 * case# 20148624
 * Stnd Bundle Issue Fix
 *
 * Revision 1.1.2.1.4.9  2014/05/30 00:41:04  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.4.8  2014/05/21 15:36:56  skavuri
 * Case # 20148467 SB Issue Fixed
 *
 * Revision 1.1.2.1.4.7  2013/09/06 07:33:55  rmukkera
 * Case# 20124246
 *
 * Revision 1.1.2.1.4.6  2013/09/03 00:06:04  nneelam
 * Case#.20124233�
 * RF LP Exception Issue Fix..
 *
 * Revision 1.1.2.1.4.5  2013/05/07 15:37:53  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.1.2.1.4.4  2013/04/19 15:40:01  skreddy
 * CASE201112/CR201113/LOG201121
 * issue fixes
 *
 * Revision 1.1.2.1.4.3  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.4.2  2013/04/15 14:59:05  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.1.2.1.4.1  2013/03/19 11:47:21  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.1  2012/12/24 13:18:22  schepuri
 * CASE201112/CR201113/LOG201121
 * LP Exception func added
 *
 * Revision 1.13.2.21.2.5  2012/11/07 23:07:10  snimmakayala
 *
 *****************************************************************************/
function PickingLPexp(request, response){
	if (request.getMethod() == 'GET') {

		var getWaveNo = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		//var getItemName = request.getParameter('custparam_itemname');
		var getItemDesc = request.getParameter('custparam_itemdescription');
		var getItemNo = request.getParameter('custparam_iteminternalid');
		var getdoLoineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getBeginLocationName = request.getParameter('custparam_beginlocationname');
		var getEndLocationInternalId = request.getParameter('custparam_endlocinternalid');
		var getEndLocation = request.getParameter('custparam_endlocation');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getClusterNo = request.getParameter('custparam_clusterno');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getNoofRecords = request.getParameter('custparam_noofrecords');
		var getNextLocation = request.getParameter('custparam_nextlocation');
		var name = request.getParameter('name');
		var getRecCount = request.getParameter('custparam_RecCount');
		var getContainerSize = request.getParameter('custparam_containersize');
		var getEbizOrdNo = request.getParameter('custparam_ebizordno');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var whLocation = request.getParameter('custparam_whlocation');
		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');
		/*	//parameters erquired if serializedinventoryitem
		var getnumber = request.getParameter('custparam_number');
		var getRecType = request.getParameter('custparam_RecType');
		var getSerOut = request.getParameter('custparam_SerOut');
		var getSerIn = request.getParameter('custparam_SerIn');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var getNextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		var NextRecordId=request.getParameter('custparam_nextrecordid');
		var Exceptionflag=request.getParameter('custparam_Exceptionflag');
		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');*/

		var pickType=request.getParameter('custparam_picktype');
		var getLP=request.getParameter('custparam_LP');
		var getItemStatus=request.getParameter('custparam_ItemStatus');

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');

		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('ERROR', 'getWaveNo', getWaveNo);
		nlapiLogExecution('ERROR', 'getRecordInternalId', getRecordInternalId);
		nlapiLogExecution('ERROR', 'getContainerLpNo', getContainerLpNo);
		nlapiLogExecution('ERROR', 'getQuantity', getQuantity);
		nlapiLogExecution('ERROR', 'getBeginLocation', getBeginLocation);
		nlapiLogExecution('ERROR', 'getItem', getItem);
//		nlapiLogExecution('ERROR', 'getItemName', getItemName);
		nlapiLogExecution('ERROR', 'getItemDesc', getItemDesc);
		nlapiLogExecution('ERROR', 'getItemNo', getItemNo);
		nlapiLogExecution('ERROR', 'getdoLoineId', getdoLoineId);
		nlapiLogExecution('ERROR', 'getInvoiceRefNo', getInvoiceRefNo);
		nlapiLogExecution('ERROR', 'getBeginLocationName', getBeginLocationName);
		nlapiLogExecution('ERROR', 'getEndLocationInternalId', getEndLocationInternalId);
		nlapiLogExecution('ERROR', 'getEndLocation', getEndLocation);
		nlapiLogExecution('ERROR', 'getOrderLineNo', getOrderLineNo);
		nlapiLogExecution('ERROR', 'getClusterNo', getClusterNo);
		nlapiLogExecution('ERROR', 'getBatchNo', getBatchNo);
		nlapiLogExecution('ERROR', 'getNoofRecords', getNoofRecords);
		nlapiLogExecution('ERROR', 'getNextLocation', getNextLocation);
		nlapiLogExecution('ERROR', 'name', name);
		nlapiLogExecution('ERROR', 'getRecCount', getRecCount);
		nlapiLogExecution('ERROR', 'getContainerSize', getContainerSize);
		nlapiLogExecution('ERROR', 'getEbizOrdNo', getEbizOrdNo);
		/*	nlapiLogExecution('ERROR', 'getnumber', getnumber);
		nlapiLogExecution('ERROR', 'getRecType', getRecType);
		nlapiLogExecution('ERROR', 'getSerOut', getSerOut);
		nlapiLogExecution('ERROR', 'getSerIn', getSerIn);
		nlapiLogExecution('ERROR', 'getNextExpectedQuantity', getNextExpectedQuantity);
		nlapiLogExecution('ERROR', 'getBeginLocation', getBeginLocation);
		nlapiLogExecution('ERROR', 'NextItemInternalId', NextItemInternalId);*/
		nlapiLogExecution('ERROR', 'getLP', getLP);
		nlapiLogExecution('ERROR', 'getItemStatus', getItemStatus);


		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enteritem').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";		
		html = html + "				<td align = 'left'>LP: <label>" + getLP + "</label>";

		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		//html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnItemDesc' value=" + getItemDesc + ">";
		html = html + "				<input type='hidden' name='hdnItemNo' value=" + getItemNo + ">";
		html = html + "				<input type='hidden' name='hdndoLoineId' value=" + getdoLoineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnEndLocationInternalId' value=" + getEndLocationInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEndLocation' value=" + getEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + getClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "				<input type='hidden' name='hdnNoofRecords' value=" + getNoofRecords + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + getNextLocation + ">";
		html = html + "				<input type='hidden' name='hdnname' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + getRecCount + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnEbizOrdNo' value=" + getEbizOrdNo + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";	
		/*html = html + "				<input type='hidden' name='hdnnumber' value=" + getnumber + ">";
		html = html + "				<input type='hidden' name='hdnRecType' value=" + getRecType + ">";
		html = html + "				<input type='hidden' name='hdnSerOut' value=" + getSerOut + ">";
		html = html + "				<input type='hidden' name='hdnSerIn' value=" + getSerIn + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";*/
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		/*html = html + "				<input type='hidden' name='hdnNextexpectedqty' value=" + getNextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnNextrecordid' value=" + NextRecordId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";*/	
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";
		html = html + "				<input type='hidden' name='hdnfetchedLP' value=" + getLP + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getItemStatus + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN LP ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');
		var getEnteredLPNew = request.getParameter('enterlp');
		nlapiLogExecution('ERROR', 'getEnteredLPNew', getEnteredLPNew);

		var vZoneId=request.getParameter('hdnebizzoneno');


		var SOarray = new Array();

		var fetchedLP = request.getParameter('hdnfetchedLP');
		var WaveNo = request.getParameter('hdnWaveNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');//is open task internalid
		var ContainerLpNo = request.getParameter('hdnContainerLpNo');
		var FetchedQuantity = request.getParameter('hdnQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocation');
		var Item = request.getParameter('hdnItem');
		//var ItemName = request.getParameter('hdnItemName');
		var ItemDesc = request.getParameter('hdnItemDesc');
		var ItemNo = request.getParameter('hdnItemNo');
		var doLoineId = request.getParameter('hdndoLoineId');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginLocationName');
		var EndLocationInternalId = request.getParameter('hdnEndLocationInternalId');
		var EndLocation = request.getParameter('hdnEndLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var ClusterNo = request.getParameter('hdnClusterNo');
		var BatchNo = request.getParameter('hdnBatchNo');
		var NoofRecords = request.getParameter('hdnNoofRecords');
		var NextLocation = request.getParameter('hdnNextLocation');
		var name = request.getParameter('hdnname');
		var RecCount = request.getParameter('hdnRecCount');
		var ContainerSize = request.getParameter('hdnContainerSize');
		var EbizOrdNo = request.getParameter('hdnEbizOrdNo');
		var NextItemInternalId=request.getParameter('hdnNextItemId');
		var poLoc = request.getParameter('hdnwhlocation');
		var getItemStatus = request.getParameter('hdnItemStatus');
		//var NextrecordID=request.getParameter('hdnNextrecordid');
		//var ItemType=request.getParameter('hdnitemtype');

		//var number = request.getParameter('hdnnumber');
		//var RecType = request.getParameter('hdnRecType');
		//var SerOut = request.getParameter('hdnSerOut');
		//var SerIn = request.getParameter('hdnSerIn');


		SOarray["custparam_screenno"] = 'PickLPExp';
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_itemstatus"] = request.getParameter('hdnItemStatus');

		//nlapiLogExecution('ERROR', 'getEnteredQuantity', getEnteredQuantity);
		nlapiLogExecution('ERROR', 'getFetchedQuantity', FetchedQuantity);
		nlapiLogExecution('ERROR', 'WaveNo', WaveNo);
		nlapiLogExecution('ERROR', 'RecordInternalId', RecordInternalId);
		nlapiLogExecution('ERROR', 'ContainerLpNo', ContainerLpNo);

		nlapiLogExecution('ERROR', 'BeginLocation', BeginLocation);
		nlapiLogExecution('ERROR', 'Item', Item);
		//nlapiLogExecution('ERROR', 'ItemName', ItemName);
		nlapiLogExecution('ERROR', 'ItemDesc', ItemDesc);
		nlapiLogExecution('ERROR', 'ItemNo', ItemNo);
		nlapiLogExecution('ERROR', 'doLoineId', doLoineId);
		nlapiLogExecution('ERROR', 'InvoiceRefNo', InvoiceRefNo);
		nlapiLogExecution('ERROR', 'BeginLocationName', BeginLocationName);
		nlapiLogExecution('ERROR', 'EndLocationInternalId', EndLocationInternalId);
		nlapiLogExecution('ERROR', 'EndLocation', EndLocation);
		nlapiLogExecution('ERROR', 'OrderLineNo', OrderLineNo);
		nlapiLogExecution('ERROR', 'ClusterNo', ClusterNo);
		nlapiLogExecution('ERROR', 'BatchNo', BatchNo);
		nlapiLogExecution('ERROR', 'NoofRecords', NoofRecords);
		nlapiLogExecution('ERROR', 'NextLocation', NextLocation);
		nlapiLogExecution('ERROR', 'name', name);
		nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);
		nlapiLogExecution('ERROR', 'EbizOrdNo', EbizOrdNo);
		nlapiLogExecution('ERROR', 'poLoc', poLoc);
		//	nlapiLogExecution('ERROR', 'number', number);
		//	nlapiLogExecution('ERROR', 'RecType', RecType);
		//	nlapiLogExecution('ERROR', 'SerOut', SerOut);
		//	nlapiLogExecution('ERROR', 'SerIn', SerIn);



		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = RecordInternalId;
		SOarray["custparam_containerlpno"] = ContainerLpNo;
		SOarray["custparam_expectedquantity"] = FetchedQuantity;
		SOarray["custparam_beginLocation"] = BeginLocation;
		SOarray["custparam_item"] = Item;
		//	SOarray["custparam_itemname"] = ItemName;
		SOarray["custparam_itemdescription"] = ItemDesc;
		SOarray["custparam_iteminternalid"] = ItemNo;
		SOarray["custparam_dolineid"] = doLoineId;
		SOarray["custparam_invoicerefno"] = InvoiceRefNo;
		SOarray["custparam_beginlocationname"] = BeginLocationName;
		SOarray["custparam_endlocinternalid"] = EndLocationInternalId;
		SOarray["custparam_endlocation"] = EndLocation;
		SOarray["custparam_orderlineno"] = OrderLineNo;
		SOarray["custparam_clusterno"] = ClusterNo;
		SOarray["custparam_batchno"] = BatchNo;
		SOarray["custparam_noofrecords"] = NoofRecords;
		SOarray["custparam_nextlocation"] = NextLocation;
		SOarray["name"] = name;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = EbizOrdNo;
		SOarray["custparam_LP"] = fetchedLP;
		SOarray["custparam_ItemStatus"] = getItemStatus;
		//SOarray["custparam_number"] = number;
		//SOarray["custparam_RecType"] = RecType;
		//SOarray["custparam_SerOut"] = SerOut;
		//SOarray["custparam_SerIn"] = SerIn;
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_nextexpectedquantity"]=request.getParameter('hdnNextexpectedqty');
		//SOarray["custparam_nextrecordid"]=request.getParameter('hdnNextrecordid');
		SOarray["custparam_itemType"] = request.getParameter('hdnitemtype');
		SOarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		SOarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');
		//var nextexpqty = request.getParameter('hdnNextexpectedqty');

		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';

		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');		
		//SOarray["custparam_remqty"] = remainqty;

		if (request.getParameter('cmdPrevious') == 'F7') 
		{
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
		}
		else 
		{
			nlapiLogExecution('ERROR', 'getEnteredLPNew ', getEnteredLPNew);
			var NewLPAllocQty = 0;
			var NewLPAvailQty=0;
			var Newinvrecid,NewItemStatus,NewBeginLoc,NewItem;

			var LPAllocQty = 0;
			var LPAvailQty=0;
			var invrecid;

			var fieldNames = new Array(); 
			var newValues = new Array(); 

			if(getEnteredLPNew!="")
			{
				if(fetchedLP != getEnteredLPNew)
				{


					var searchresultsinvt = getInvDetails(getEnteredLPNew,poLoc,ItemNo,BeginLocation,getItemStatus);
					var searchresultsinvtOldLP = getInvDetails(fetchedLP,poLoc,ItemNo,BeginLocation,getItemStatus);

					if(searchresultsinvt == null || searchresultsinvt == '')
					{
						SOarray["custparam_LP"] = fetchedLP;
						SOarray["custparam_error"] = "NO Inventory for this LP";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('ERROR', 'Error: ', 'NO Inventory for this LP');
						return;
					}
					else
					{

						NewLPAllocQty = searchresultsinvt[0].getValue('custrecord_ebiz_alloc_qty');
						NewLPAvailQty =  searchresultsinvt[0].getValue('custrecord_ebiz_avl_qty');
						Newinvrecid = searchresultsinvt[0].getId();

						NewItem =  searchresultsinvt[0].getValue('custrecord_ebiz_inv_sku');
						NewBeginLoc =  searchresultsinvt[0].getValue('custrecord_ebiz_inv_binloc');
						NewItemStatus =  searchresultsinvt[0].getValue('custrecord_ebiz_inv_sku_status');


						/*	if(BeginLocation != NewBeginLoc)
						{
							SOarray["custparam_LP"] = fetchedLP;
							SOarray["custparam_error"] = "LP Location is Mismatching"
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('ERROR', 'Error: ', 'NO Inventory for this LP');
							return;
						}*/

						if(parseInt(FetchedQuantity) > parseInt(NewLPAvailQty))
						{
							SOarray["custparam_LP"] = fetchedLP;
							SOarray["custparam_error"] = "Insufficent Inventory for this LP Available :"+ ' '+ NewLPAvailQty +", Req :"+FetchedQuantity;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('ERROR', 'Error: ', 'Insufficent Inventory for this LP');
							return;
						}
						else
						{
							if(searchresultsinvtOldLP != null && searchresultsinvtOldLP != '')
							{
								nlapiLogExecution('ERROR', 'searchresultsinvtOldLP.length',searchresultsinvtOldLP.length);
								invrecid = searchresultsinvtOldLP[0].getId();
								LPAllocQty = searchresultsinvtOldLP[0].getValue('custrecord_ebiz_alloc_qty');
								LPAvailQty =  searchresultsinvtOldLP[0].getValue('custrecord_ebiz_avl_qty');

								var NewinvTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', Newinvrecid);
								if(NewLPAllocQty == null || NewLPAllocQty == '')
									NewLPAllocQty=0;
								NewLPAllocQty = parseInt(NewLPAllocQty) + parseInt(FetchedQuantity);
								NewinvTransaction.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(NewLPAllocQty)); 
								nlapiLogExecution('ERROR', 'NewLPAllocQty',NewLPAllocQty);

								var invTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invrecid);
								if(LPAllocQty == null || LPAllocQty == '')
									LPAllocQty=0;
								if(LPAllocQty > 0)
									LPAllocQty = parseInt(LPAllocQty) - parseInt(FetchedQuantity);
								invTransaction.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(LPAllocQty)); 
								nlapiLogExecution('ERROR', 'LPAllocQty',LPAllocQty);

								var Newinvtrecid = nlapiSubmitRecord(NewinvTransaction, false, true);
								nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',Newinvtrecid);

								var Oldinvtrecid = nlapiSubmitRecord(invTransaction, false, true);
								nlapiLogExecution('ERROR', 'Allocations added successfully (Inventory Record ID)',Oldinvtrecid);

								fieldNames.push('custrecord_lpno');
								fieldNames.push('custrecord_from_lp_no');
								fieldNames.push('custrecord_notes');
								fieldNames.push('custrecord_invref_no');

								var oldLP = "Old LP is "+fetchedLP;
								newValues.push(getEnteredLPNew);
								newValues.push(getEnteredLPNew);
								newValues.push(oldLP);
								newValues.push(Newinvrecid);


								nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, fieldNames, newValues);

								SOarray["custparam_LP"] = getEnteredLPNew;
								response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
								return;

							}


						}
					}
				}
				else
				{
					SOarray["custparam_LP"] = fetchedLP;
					//case# 20148624 starts
					//SOarray["custparam_error"] = "Fetched LP:"+fetchedLP+" = Entered LP :"+ ' '+ getEnteredLPNew ;//+", Req :"+FetchedQuantity;
					SOarray["custparam_error"] = "Fetched LP:"+fetchedLP+" = Entered LP :"+ ' '+ getEnteredLPNew+" <br> PLEASE ENTER ANOTHER LP " ;//+", Req :"+FetchedQuantity;
					//case# 20148624 ends
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

				}
			}
			// Case# 20148467 starts
			else
				{
				nlapiLogExecution('ERROR', 'Please Enter LP','Please Enter LP');
				SOarray["custparam_error"] = "Please Enter LP";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				}


		}
	}
}




function getInvDetails(getLP,poLoc,getItemNo,getBeginLocation,getItemStatus)
{
	nlapiLogExecution('ERROR', 'getLP',getLP);
	nlapiLogExecution('ERROR', 'poLoc',poLoc);
	nlapiLogExecution('ERROR', 'getItemNo',getItemNo);
	nlapiLogExecution('ERROR', 'getBeginLocation',getBeginLocation);
	nlapiLogExecution('ERROR', 'getItemStatus',getItemStatus);
	var filtersinvt = new Array();

	filtersinvt[0] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', getLP);
	filtersinvt[1] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', poLoc);
	filtersinvt[2] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', getItemNo);
	filtersinvt[3] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', getBeginLocation);
	//case# 20124233,if condition added.
	if(getItemStatus!=null && getItemStatus!='' && getItemStatus!='null')
	filtersinvt[4] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', getItemStatus);



	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno');
	//columnsinvt[5].setSort();

	var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

	return searchresultsinvt;
}


