/***************************************************************************
 eBizNET Solutions Inc  
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplenPutawayCartLP.js,v $
 *     	   $Revision: 1.1.4.2 $
 *     	   $Date: 2015/04/13 16:14:07 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_44 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplenPutawayCartLP.js,v $
 * Revision 1.1.4.2  2015/04/13 16:14:07  snimmakayala
 * Case#:201411349
 * Two Step Replenishment
 *
 * Revision 1.1.2.1  2015/01/02 15:05:29  skreddy
 * Case# 201411349
 * Two step replen process
 *
 * Revision 1.4.4.5.4.7.2.12  2014/07/01 15:17:11  skavuri
 * Case# 20148710 Compatibility Issue fixed
 *
 * Revision 1.4.4.5.4.7.2.11  2014/06/17 05:46:48  grao
 * Case#: 20148907   New GUI account issue fixes
 *
 * Revision 1.4.4.5.4.7.2.10  2014/06/13 06:45:18  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.4.5.4.7.2.9  2014/06/06 06:52:07  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.4.4.5.4.7.2.8  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.5.4.7.2.7  2014/02/25 15:13:52  rmukkera
 * Case # 20127339
 *
 * Revision 1.4.4.5.4.7.2.6  2013/12/02 08:59:59  schepuri
 * 20126048
 *
 * Revision 1.4.4.5.4.7.2.5  2013/09/27 15:08:00  schepuri
 * for Batch item while scanning the Cart# system is saying "Error: null"
 *
 * Revision 1.4.4.5.4.7.2.4  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.4.5.4.7.2.3  2013/05/22 11:39:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating PO Status before user perform transaction.
 *
 * Revision 1.4.4.5.4.7.2.2  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.4.5.4.7.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.4.4.5.4.7  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.4.4.5.4.6  2012/12/21 15:47:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.4.4.5.4.5  2012/10/03 05:09:30  grao
 * no message
 *
 * Revision 1.4.4.5.4.4  2012/09/28 06:38:47  grao
 * no message
 *
 * Revision 1.4.4.5.4.3  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.4.4.5.4.2  2012/09/26 12:48:01  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.4.4.5.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.4.4.5  2012/05/14 14:48:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating the CARTLP issue is fixed.
 *
 * Revision 1.4.4.4  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.4.3  2012/03/15 12:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * cart closing functionality modified
 *
 * Revision 1.4.4.2  2012/02/23 13:48:08  schepuri
 * CASE201112/CR201113/LOG201121
 * fixed TPP issues
 *
 * Revision 1.7  2012/02/23 11:51:46  rmukkera
 * CASE201112/CR201113/LOG201121
 * focus given to the input box
 *
 * Revision 1.6  2012/02/16 10:41:51  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.5  2012/02/02 09:01:52  rmukkera
 * CASE201112/CR201113/LOG201121
 * modifications in code
 *
 * Revision 1.4  2011/09/22 08:29:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * The spelling in License Plate is  " LICENSE"  not "LICENCE"
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function TwoStepReplenPutawayCartLP(request, response){
	if (request.getMethod() == 'GET') {
		var getCartLP;

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		

		nlapiLogExecution('DEBUG', 'getCartLP', getCartLP);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		// 20126048

		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "";
			st1 = "LP Carro";
			st2 = "ENTRAR / ESCANEAR N&#218;MERO DE CARRO";	
			st3 = "N&#250;mero de placa";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";

		}
		else
		{
			st0 = "";
			st1 = "CART LP";
			st2 = "ENTER/SCAN CART LP";
			st3 = "LICENSE PLATE NUMBER";
			st4 = "SEND";
			st5 = "PREV";

		}



		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entercartlp').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>"; 
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> ";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label id='CartLPNo'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + "</td></tr>";
		html = html + "				<td align = 'left'>" + st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercartlp' id='entercartlp' type='text'/>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4  +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercartlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		// This variable is to hold the LP entered.
		var getCartLPNo = request.getParameter('entercartlp');
		nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);
	
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;
		POarray["custparam_screenno"] = '2Steppw';
		POarray["custparam_language"] = getLanguage;

		var st4,st5,st6;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st4 = "ESTE LP CART NO EXISTE";
			st5 = "NO V&#193;LIDO LP CART";
			st6 = "Introduzca CARRO LP";

		}
		else
		{

			st4 = "THIS CART LP DOESN'T EXISTS";
			st5 = "INVALID CART LP";
			st6 = "Please Enter CART LP";//Case# 20148710

		}

		POarray["custparam_error"] = st5;
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenmenu', 'customdeploy_ebiz_twostepreplenmenu', false, POarray);
			return;
		}
		else {

			try {
				if (getCartLPNo != null && getCartLPNo !='') {
					POarray["custparam_cartlpno"] = getCartLPNo;
					var lpmasterfilters =new Array();           
					lpmasterfilters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', getCartLPNo));
					var lpmastercolumns = new Array();
					lpmastercolumns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));        	
					lpmastercolumns.push(new nlobjSearchColumn('custrecord_ebiz_cart_closeflag')); 
					// To get the data from Master LP based on selection criteria
					var	lpMasterSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, lpmasterfilters, lpmastercolumns);

					if(lpMasterSearchResults!=null)
					{
						var cartCloseFalg=lpMasterSearchResults[0].getValue('custrecord_ebiz_cart_closeflag');
						  if(cartCloseFalg == 'F')
							  {
							  	
							  	response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cnf_opencartlp', 'customdeploy_ebiz_rf_cnf_opencartlp', false, POarray);
								
								return;
							  }
						  else
							  {
							 
							  	response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_scanloc', 'customdeploy_ebiz_rf_twostp_scanloc', false, POarray);
								
								return;
							  }
					}
					else {
						
						POarray["custparam_screenno"] = '2Steppw'; 
						POarray["custparam_error"] =st6 ;
						
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'getCartLPNo is null ', getCartLPNo);
					}
				
				}
				else {
				
					POarray["custparam_screenno"] = '2Steppw'; 
					POarray["custparam_error"] =st6 ;
					
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'getCartLPNo is null ', getCartLPNo);
				}
			} 
			catch (e) {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'In error catch block ', e);
			}

		}
	}
}
