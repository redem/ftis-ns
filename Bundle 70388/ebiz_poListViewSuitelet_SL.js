/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_poListViewSuitelet_SL.js,v $
*  $Revision: 1.2.14.1 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_poListViewSuitelet_SL.js,v $
*  Revision 1.2.14.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function poListViewSS(request, response)
{
	var list = nlapiCreateList('PO List');
     //list.setStyle(request.getParameter('style'));
     //list.setScript(239);
 
   /*
  var column = list.addColumn('number','text', 'Number', 'LEFT');
     column.setURL(nlapiResolveURL('RECORD','purchaseorder'));
     column.addParamToURL('id','id', true);
*/
	 
     list.addColumn('trandate','date', 'Date', 'LEFT');
     list.addColumn('tranid','text', 'PO', 'LEFT');
     list.addColumn('status','text', 'Order Status', 'LEFT');
     
 
     var returncols = new Array();
     returncols[0] = new nlobjSearchColumn('trandate');
     returncols[1] = new nlobjSearchColumn('tranid');
	 returncols[2] = new nlobjSearchColumn('status');
     
 
     var results = nlapiSearchRecord('purchaseorder', null, new
		nlobjSearchFilter('mainline',null,'is','T'), returncols);
     list.addRows( results );
 
    /*
 list.addPageLink('crosslink', 'Create Phone Call',
		nlapiResolveURL('TASKLINK','EDIT_CALL'));
     list.addPageLink('crosslink', 'Create SalesOrder',
		nlapiResolveURL('TASKLINK','EDIT_TRAN_SALESORD'));
 
*/
     //list.addButton('custombutton','Simple Button',"alert('hello world')");
     response.writePage( list );
}