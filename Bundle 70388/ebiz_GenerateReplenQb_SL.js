/***************************************************************************
						eBizNET Solutions Inc
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_GenerateReplenQb_SL.js,v $
 *     	   $Revision: 1.1.2.3.4.1.4.2.4.1 $
 *     	   $Date: 2014/07/24 15:20:14 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *$Log: ebiz_GenerateReplenQb_SL.js,v $
 *Revision 1.1.2.3.4.1.4.2.4.1  2014/07/24 15:20:14  nneelam
 *case#  20149675
 *New UI Issue Fix.
 *
 *Revision 1.1.2.3.4.1.4.2  2013/11/04 15:21:47  rmukkera
 *Case#  20125541
 *
 *Revision 1.1.2.3.4.1.4.1  2013/02/26 12:56:04  skreddy
 *CASE201112/CR201113/LOG201121
 *merged boombah changes
 *
 *Revision 1.1.2.3.4.1  2012/10/11 14:50:59  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *REPLEN CHANGES
 *
 *Revision 1.1.2.3  2012/09/14 10:31:16  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *Replen changes
 *
 *Revision 1.1.2.2  2012/09/04 22:35:20  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *Replen issues
 *
 *Revision 1.1.2.1  2012/03/22 08:11:10  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 *Revision 1.5  2011/08/20 07:22:41  vrgurujala
 *CASE201112/CR201113/LOG201121
 *
 *Revision 1.4  2011/06/15 08:01:04  skota
 *CASE201112/CR201113/LOG201121
 *code changes to check 'Active' status while populating company drop down and removed the condition 'Math.min(500)' while populating item and location drop downs
 *
 *****************************************************************************/
function GenerateReplenQB(request, response){
	if (request.getMethod() == 'GET') 
	{
		var form = nlapiCreateForm('Generate Replenishment');

		var varItem = form.addField('custpage_item', 'multiselect', 'Item','Item');    
		var varLoc = form.addField('custpage_location', 'select', 'Location').setMandatory(true);

		var itemfamily = form.addField('custpage_itemfamily', 'select', 'Item Family','customrecord_ebiznet_sku_family');

		var itemgroup = form.addField('custpage_itemgroup', 'select', 'Item Group','customrecord_ebiznet_sku_group');
		varLoc.addSelectOption('', '');

		var inboundlocgroup = form.addField('custpage_inblocgroup', 'select', 'Inbound Location Group','customrecord_ebiznet_loc_group');
		
		var filtersloc = new Array();
		filtersloc.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));

		var columnsloc = new Array();
		columnsloc.push(new nlobjSearchColumn('name'));

		var searchlocresults = nlapiSearchRecord('Location', null, filtersloc, columnsloc);

		if (searchlocresults != null && searchlocresults!='')
		{

			nlapiLogExecution('ERROR','test1',searchlocresults.length);
			for (var i = 0; i < searchlocresults.length; i++) 
			{
				var res = form.getField('custpage_location').getSelectOptions(searchlocresults[i].getValue('name'), 'is');
				if (res != null) 
				{
					if (res.length > 0)	                
						continue;	                
				}
				varLoc.addSelectOption(searchlocresults[i].getId(), searchlocresults[i].getValue('name'));
			}
		}      
		if(request.getParameter('custpage_location')!='' && request.getParameter('custpage_location')!=null)
		{
			varLoc.setDefaultValue(request.getParameter('custpage_location'));	
		}


		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else 
	{    
		var vargetitem = request.getParameter('custpage_item');
		var vargetlocation = request.getParameter('custpage_location');
		var vargetlocfamily = request.getParameter('custpage_itemfamily');
		var vargetlocgroup = request.getParameter('custpage_itemgroup');
		nlapiLogExecution('ERROR','vargetlocfamily',vargetlocfamily);
		nlapiLogExecution('ERROR','vargetlocfamily',vargetlocfamily);
		var replenarray = new Array();
		replenarray["custpage_item"] = vargetitem;    
		replenarray["custpage_location"] = vargetlocation;
		replenarray["custpage_itemfamily"] = vargetlocfamily;
		replenarray["custpage_itemgroup"] = vargetlocgroup;
		response.sendRedirect('SUITELET', 'customscript_generate_replen_suitlet', 'customdeploy_generate_replen_suitlet_di', false, replenarray);
	}

}
