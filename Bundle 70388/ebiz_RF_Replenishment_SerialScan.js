/***************************************************************************
                            eBizNET
                       eBizNET Solutions Inc
****************************************************************************
*
*  $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_Replenishment_SerialScan.js,v $
*  $Revision: 1.1.2.2.2.9.2.3 $
*  $Date: 2015/11/17 15:04:15 $
*  $Author: gkalla $
*  $Name: t_WMS_2015_2_StdBundle_1_162 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_RF_Replenishment_SerialScan.js,v $
*  Revision 1.1.2.2.2.9.2.3  2015/11/17 15:04:15  gkalla
*  case# 201415608
*  Serial numbers are not updating in RF replen confirmation.
*
*  Revision 1.1.2.2.2.9.2.2  2015/11/13 15:42:46  grao
*  2015.2 Issue Fixes 201415579
*
*  Revision 1.1.2.2.2.9.2.1  2015/11/10 15:25:13  schepuri
*  case# 201413693
*
*  Revision 1.1.2.2.2.9  2015/07/08 13:49:31  nneelam
*  case# 201413399
*
*  Revision 1.1.2.2.2.8  2015/07/03 15:40:58  skreddy
*  Case# 201412905
*  Signwarehouse SB issue fix
*
*  Revision 1.1.2.2.2.7  2014/06/13 09:34:37  skavuri
*  Case# 20148882 (added Focus Functionality for Textbox)
*
*  Revision 1.1.2.2.2.6  2014/05/30 00:34:23  nneelam
*  case#  20148622
*  Stanadard Bundle Issue Fix.
*
*  Revision 1.1.2.2.2.5  2014/01/09 14:08:58  grao
*  Case# 20126579 related issue fixes in Sb issue fixes
*
*  Revision 1.1.2.2.2.4  2014/01/06 13:16:03  grao
*  Case# 20126579 related issue fixes in Sb issue fixes
*
*  Revision 1.1.2.2.2.3  2013/11/22 14:16:53  schepuri
*  20125772
*
*  Revision 1.1.2.2.2.2  2013/04/17 16:02:37  skreddy
*  CASE201112/CR201113/LOG201121
*  added meta tag
*
*  Revision 1.1.2.2.2.1  2013/04/02 14:16:19  rmukkera
*  issue Fix related to vaidating the serailno
*
*  Revision 1.1.2.2  2013/02/06 01:27:12  kavitha
*  CASE201112/CR201113/LOG201121
*  Serial # functionality - Inventory process
*
*  Revision 1.1.2.1  2012/11/27 17:50:23  spendyala
*  CASE201112/CR201113/LOG201121
*  Serial Capturing file.
*
*
****************************************************************************/

function ReplenishmentSerialScan(request,response)
{
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	
	if(request.getMethod() == 'GET')
	{
		try
		{
		// case no 20125772
			var getNumber = request.getParameter('custparam_serialnumber');
			
			nlapiLogExecution('ERROR','getNumber before',getNumber);
			if(getNumber==null||getNumber==""||getNumber=='NaN')
				getNumber=0;
			
			nlapiLogExecution('ERROR','getNumber ',getNumber);
			var getItemQuantity = request.getParameter('custparam_totalqtyscan');
			nlapiLogExecution('ERROR','getItemQuantity',getItemQuantity);
			var getNo = request.getParameter('custparam_repno');
			var getBegLoc = request.getParameter('custparam_repbegloc');
			var getPickLoc = request.getParameter('custparam_reppickloc');
			var getPickLocNo = request.getParameter('custparam_reppicklocno');
			var getSku = request.getParameter('custparam_repsku');
			var getSkuNo = request.getParameter('custparam_repskuno');
			var getExpQty = request.getParameter('custparam_torepexpqty');
			var getid = request.getParameter('custparam_repid');
			var vClustno = request.getParameter('custparam_clustno');
			var whlocation = request.getParameter('custparam_whlocation');
			var beginLocId = request.getParameter('custparam_repbeglocId');
			var nextlocation=request.getParameter('custparam_nextlocation');
			var nextqty=request.getParameter('custparam_nextqty');
			var batchno=request.getParameter('custparam_batchno');
			var fromlpno=request.getParameter('custparam_fromlpno');
			var tolpno=request.getParameter('custparam_tolpno');
			var nextitem=request.getParameter('custparam_nextitem');
			var nextitemno=request.getParameter('custparam_nextitemno');
			var beginLocId = request.getParameter('custparam_repbeglocId');
			var newLp = request.getParameter('custparam_NewLpScaned');
			var scannedLP = request.getParameter('custparam_scannedLP');
			var getLanguage = request.getParameter('custparam_language');
			nlapiLogExecution('Debug','newLp',newLp);
			nlapiLogExecution('Debug','scannedLP',scannedLP);
			nlapiLogExecution('Debug','beginLocId',beginLocId);
			nlapiLogExecution('Debug','nextitemno',nextitemno);
			nlapiLogExecution('Debug','nextitem',nextitem);
			nlapiLogExecution('Debug','tolpno',tolpno);
			nlapiLogExecution('Debug','fromlpno',fromlpno);
			nlapiLogExecution('Debug','nextqty',nextqty);
			nlapiLogExecution('Debug','nextlocation',nextlocation);
			nlapiLogExecution('Debug','getSkuNo',getSkuNo);
			nlapiLogExecution('Debug','getSku',getSku);
			nlapiLogExecution('Debug','getBegLoc',getBegLoc);
			nlapiLogExecution('Debug','getPickLocNo',getPickLocNo);
			
			var getreportNo=request.getParameter('custparam_enterrepno');
			var getitem = request.getParameter('custparam_entersku');
			var getitemid = request.getParameter('custparam_enterskuid');
			var getloc = request.getParameter('custparam_enterpickloc');
			var getlocid = request.getParameter('custparam_enterpicklocid');
			var replenType = request.getParameter('custparam_replentype');
			var remainingreplenqty = request.getParameter('custparam_remainingreplenqty');


			var beginLocId = request.getParameter('custparam_repbeglocId');
			var systemrule = request.getParameter('custparam_systemrule');
			if(scannedLP =='' || scannedLP ==null || scannedLP =='null' )
				scannedLP=tolpno;
			nlapiLogExecution('Debug', 'expQty', expQty);
			nlapiLogExecution('Debug', 'tolpno', tolpno);
			nlapiLogExecution('Debug', 'getSkuNo', getSkuNo);
			nlapiLogExecution('Debug', 'taskpriority in GET', taskpriority);
			nlapiLogExecution('Debug', 'RecordCount in GET', RecordCount);

			nlapiLogExecution('Debug', 'entereditem', getitem);
			nlapiLogExecution('Debug', 'entereditemid', getitemid);
			nlapiLogExecution('Debug', 'enteredloc', getloc);
			nlapiLogExecution('Debug', 'enteredlocid', getlocid);		
			nlapiLogExecution('Debug', 'taskpriority', taskpriority);
			nlapiLogExecution('Debug', 'getreportNo', getreportNo);
			nlapiLogExecution('Debug', 'remainingreplenqty', remainingreplenqty);
			nlapiLogExecution('Debug', 'systemrule', systemrule);

			
			
			nlapiLogExecution('Debug', 'getItemQuantity', getItemQuantity);
			var TotalqtytoScan=0;
			if(getItemQuantity==null||getItemQuantity==""||getItemQuantity=='NaN')
				TotalqtytoScan=GetTotalQty(getSkuNo,beginLocId,getNo);
			else
				TotalqtytoScan=getItemQuantity;
			
			nlapiLogExecution('Debug', 'TotalqtytoScan', TotalqtytoScan);
			
			var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_serialscan');
			var html = "<html><head><title>REPLENISHMENT Serial Scan</title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
			//Case# 20148749 Refresh Functionality starts
			html = html + "var version = navigator.appVersion;";
			html = html + "document.onkeydown = function (e) {";
			html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
			html = html + "if ((version.indexOf('MSIE') != -1)) { ";
			html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
			html = html + "else {if (keycode == 116)return false;}";
			html = html + "};";
			//Case# 20148749 Refresh Functionality ends
			//html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";
			
			html = html + "nextPage = new String(history.forward());";          
			html = html + "if (nextPage == 'undefined')";     
			html = html + "{}";     
			html = html + "else";     
			html = html + "{  location.href = window.history.forward();"; 
			html = html + "} ";
			
			//html = html + " document.getElementById('enterlp').focus();";

		//	html = html + "</script>";  
			
			//html = html + " document.getElementById('enterserialno').focus();";        
			html = html + "</script>";
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "<body>";
			html = html + "	<form name='_rf_replenishment_serialscan' method='POST'>";
			html = html + "		<table>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>LP:  <label>" + tolpno + "</label></td>";
			html = html + "				<input type='hidden' name='hdnNo' value=" + getNo + ">";
			html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBegLoc + ">";
			html = html + "				<input type='hidden' name='hdnSku' value=" + getSku + ">";
			html = html + "				<input type='hidden' name='hdnExpQty' value=" + getExpQty + ">";
			html = html + "				<input type='hidden' name='hdnPickLoc' value=" + getPickLoc + ">";
			html = html + "				<input type='hidden' name='hdnPickLocNo' value=" + getPickLocNo + ">";
			html = html + "				<input type='hidden' name='hdnid' value=" + getid + ">";
			html = html + "				<input type='hidden' name='hdnclustno' value=" + vClustno + ">";
			html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
			html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
			html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
			html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
			html = html + "				<input type='hidden' name='hdnbatchno' value=" + batchno + ">";
			html = html + "				<input type='hidden' name='hdnnitem' value=" + nextitem + ">";
			html = html + "				<input type='hidden' name='hdnnextitemno' value=" + nextitemno + ">";
			html = html + "				<input type='hidden' name='hdnskuno' value=" + getSkuNo + ">";
			html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
			html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
			html = html + "				<input type='hidden' name='hdnnewlp' value=" + newLp + ">";
			html = html + "				<input type='hidden' name='hdnscannedLP' value=" + scannedLP + ">";
			html = html + "				<input type='hidden' name='hdnNumber' value=" + getNumber + ">";
			html = html + "				<input type='hidden' name='hdnTotalqtytoScan' value=" + TotalqtytoScan + ">";
			html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
			html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";

			html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";

			html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
			html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
			html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
			html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
			html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
			html = html + "				<input type='hidden' name='hdnremainingreplenqty' value=" + remainingreplenqty + ">";
			html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
			html = html + "				<input type='hidden' name='hdnsystemrule' value=" + systemrule + ">";			
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + (parseInt(getNumber) + 1) + " OF <label>" + TotalqtytoScan + "</label>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>ENTER SERIAL NO: ";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterserialno' id='enterserialno' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			//html = html + "				<td align = 'left'>PARENT ID : <label>" + scannedLP + "</label>";
			html = html + "				<td align = 'left'>PARENT ID : <label>" + fromlpno + "</label>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
			html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "		 </table>";
			html = html + "	</form>";
			//Case# 20148882 (added Focus Functionality for Textbox)
			html = html + "<script type='text/javascript'>document.getElementById('enterserialno').focus();</script>";
			html = html + "</body>";
			html = html + "</html>";
			response.write(html);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception in Replenishment Get Block',exp);
		}
	}
	else
	{
		try
		{
			var Reparray = new Array();
			var getSerialNo = request.getParameter('enterserialno');
			var getNumber = request.getParameter('hdnNumber');
			var getItemQuantity = request.getParameter('hdnTotalqtytoScan');

			var optedEvent = request.getParameter('cmdPrevious');
			var lp = request.getParameter('hdnscannedLP');		
			var hdnBeginLocation = request.getParameter('hdnBeginLocation');
			var hdnrepLpno=request.getParameter('hdnfromLpno');	
			var hdnreptoLpno=request.getParameter('hdntoLpno');	
			var clusterNo = request.getParameter('hdnclustno');		
			var RecordCount = request.getParameter('hdnRecCount');			
			var ExpQty=request.getParameter('hdnExpQty');
			var ReportNo=request.getParameter('hdnNo');
			var recid=request.getParameter('hdnid');
			var vClustNo = request.getParameter('hdnclustno');
			var batchno = request.getParameter('hdnbatchno');
			var whlocation = request.getParameter('hdnwhlocation');
			var sku = request.getParameter('hdnSku');
			var skuno = request.getParameter('hdnskuno');
			var binlocid= request.getParameter('hdnPickLocNo');
			var NewLpScaned= request.getParameter('hdnnewlp');
			var updatedSerialLP=lp;
			var SystemRule= request.getParameter('hdnsystemrule');
			nlapiLogExecution('ERROR', 'SystemRule', SystemRule);
			nlapiLogExecution('ERROR', 'recid', recid);
			nlapiLogExecution('ERROR', 'gethdnBeginLocation', hdnBeginLocation);
			nlapiLogExecution('ERROR', 'recid', recid);
			nlapiLogExecution('ERROR', 'ReportNo', ReportNo);
			nlapiLogExecution('ERROR', 'nextlocation', request.getParameter('hdnnextloc'));
			nlapiLogExecution('ERROR', 'nextqty', request.getParameter('hdnnextqty'));
			nlapiLogExecution('ERROR', 'binlocid', request.getParameter('hdnPickLocNo'));
			
			Reparray["custparam_totalqtyscan"] = getItemQuantity;
			Reparray["custparam_scannedLP"] = request.getParameter('hdnscannedLP');
			Reparray["custparam_repno"] = request.getParameter('hdnNo');
			Reparray["custparam_repbegloc"] = request.getParameter('hdnnextloc');
			Reparray["custparam_repsku"] = request.getParameter('hdnSku');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_repid"] = request.getParameter('hdnid');
			Reparray["custparam_repskuno"] = request.getParameter('hdnskuno');
			Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
			Reparray["custparam_batchno"] = request.getParameter('hdnbatchno');
			Reparray["custparam_error"] = 'INVALID SERIAL NO';
//			Reparray["custparam_screenno"] = 'R8';
			Reparray["custparam_clustno"] = clusterNo;			
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnPickLocNo');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnPickLoc');
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
			Reparray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
			Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
			Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
			Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_screenno"] = 'Rserial';
			Reparray["custparam_clustno"] = vClustNo;
			Reparray["custparam_nextitem"] = request.getParameter('hdnnitem');
			Reparray["custparam_nextitemno"] = request.getParameter('hdnnextitemno');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnPickLoc');
			Reparray["custparam_NewLpScaned"] = NewLpScaned;
			Reparray["custparam_serialno"] = request.getParameter('custparam_serialno');
			Reparray["custparam_serialnumber"] = parseInt(request.getParameter('custparam_serialnumber'));
			
			var expQty= request.getParameter('hdnExpQty');
			Reparray["custparam_torepexpqty"] = expQty;

			var beginLocationId = request.getParameter('hdnbeginLocId');
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');

			nlapiLogExecution('ERROR','beginLocationId', Reparray["custparam_repbeglocId"]);
			nlapiLogExecution('ERROR','nextitem', Reparray["custparam_nextitem"]);
			Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');

			Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
			Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
			Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
			Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
			Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
			Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
			Reparray["custparam_remainingreplenqty"] = request.getParameter('hdnremainingreplenqty');
			
			if (sessionobj!=context.getUser()) {
				try
				{

					if(sessionobj==null || sessionobj=='')
					{
						sessionobj=context.getUser();
						context.setSessionObject('session', sessionobj); 
					}


					if (optedEvent == 'F7') {
						nlapiLogExecution('ERROR', 'REPLENISHMENT LOCATION F7 Pressed');
						//response.sendRedirect('SUITELET', 'customscript_replen_tolp', 'customdeploy_rf_replen_tolp_di', false, Reparray);
						response.sendRedirect('SUITELET', 'customscript_replen_lp', 'customdeploy_rf_replen_lp_di', false, Reparray);
					}
					else if (getSerialNo == "") {
						//	if the 'Send' button is clicked without any option value entered,
						//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						nlapiLogExecution('ERROR', 'Enter Serial No', getSerialNo);
						Reparray["custparam_error"] = 'ENTER SERIAL NO';
					}
					else 
					{
						nlapiLogExecution('ERROR', 'Serial No', getSerialNo);
						//	getSerialNo=SerialNoIdentification(Reparray["custparam_repskuno"],getSerialNo);
						//	nlapiLogExecution('ERROR', 'After Serial No Parsing', getSerialNo);
						var getActualEndDate = DateStamp();
						var getActualEndTime = TimeStamp();
						var TempSerialNoArray = new Array();
						var TempSerialNoIDArray =new Array();
						if (request.getParameter('custparam_serialno') != null) 
							TempSerialNoArray = request.getParameter('custparam_serialno').split(',');

//						if (getBeginLocation != '') 
//						{
						nlapiLogExecution('ERROR', 'INTO SERIAL ENTRY');
						//checking serial no's in already scanned one's
						for (var t = 0; t < TempSerialNoArray.length; t++) {
							if (getSerialNo == TempSerialNoArray[t]) {
								
								Reparray["custparam_serialnumber"] = parseInt(getNumber);
								Reparray["custparam_error"] = "Serial No. Already Scanned";
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
								return;
							}
						}
						var pickloc=request.getParameter('hdnPickLoc');
						if (beginLocationId == null || beginLocationId =='')
						{
							var fields=new Array();
							nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
							fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
							var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
							if(sresult!=null && sresult.length>0)
							{
								beginLocationId = sresult[0].getId();
								Reparray["custparam_repbeglocId"]=beginLocationId;
							}
						}
						//checking serial no's in records
						nlapiLogExecution('ERROR', 'getSerialNo',getSerialNo);
						nlapiLogExecution('ERROR', 'skuno',skuno);
						nlapiLogExecution('ERROR', 'beginLocationId',beginLocationId);
						nlapiLogExecution('ERROR', 'hdnrepLpno',hdnrepLpno);
						var filtersser = new Array();
						filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);
						filtersser[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', skuno);
						filtersser[2] = new nlobjSearchFilter('custrecord_serialbinlocation', null, 'anyof', beginLocationId);
						filtersser[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['19','3']);
						filtersser[4] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', hdnrepLpno);
						var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, null);
						nlapiLogExecution('ERROR', 'SrchRecord', SrchRecord);
						for ( var i = 0; SrchRecord!= null && i < SrchRecord.length; i++) 
						{
							var searchresultserial = SrchRecord[i];

							var serrialid=searchresultserial.getId();
						}
						nlapiLogExecution('ERROR', 'serrialid', serrialid);
						if (SrchRecord =='null'|| SrchRecord==null || SrchRecord=="" ) {
							Reparray["custparam_error"] = "Serial No. doesn't Exists";
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
							return;
						}
						else {
							// nlapiLogExecution('ERROR', 'SERIAL NO NOT FOUND');
							if (request.getParameter('custparam_serialno') == null || request.getParameter('custparam_serialno') == "") {
								Reparray["custparam_serialno"] = getSerialNo;
								Reparray["custparam_serialnoID"] = serrialid;
							}
							else {
								Reparray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
								Reparray["custparam_serialnoID"] = request.getParameter('custparam_serialnoID') + ',' + serrialid;
							}

							Reparray["custparam_serialnumber"] = parseInt(getNumber) + 1;
							TempSerialNoArray.push(getSerialNo);
						}
						/*TempSerialNoArray.push(getSerialNo);*/
						nlapiLogExecution('ERROR', '(getNumber + 1)', (parseInt(getNumber) + 1));
						nlapiLogExecution('ERROR', 'getItemQuantity', getItemQuantity);
						if ((parseInt(getNumber) + 1) < parseInt(getItemQuantity)) {
							nlapiLogExecution('ERROR', 'Scanning Serial No.');
							response.sendRedirect('SUITELET', 'customscript_rf_replen_serialscan', 'customdeploy_rf_replen_serialscan_di', false, Reparray);
							return;
						}
						else {
							nlapiLogExecution('ERROR', 'Inserting Into Records',Reparray["custparam_serialnoID"]);
							TempSerialNoIDArray=Reparray["custparam_serialnoID"].split(',');
							if(TempSerialNoIDArray !=null && TempSerialNoIDArray !='')
							{
								for (var j = 0; j < TempSerialNoIDArray.length; j++) {
									nlapiLogExecution('ERROR', 'Inserting Serial NO.:', TempSerialNoIDArray[j]);
//									if (SrchRecord!=null && SrchRecord!="") 
//									{
									var vserialid =  TempSerialNoIDArray[j];
									nlapiLogExecution('ERROR', 'Serial ID Internal Id :',vserialid);

									nlapiLogExecution('ERROR', "Item" , Reparray["custparam_repskuno"]);
									if(vserialid!=null && vserialid!=' ' && vserialid!='null')
									{
										var fields = new Array();
										var values = new Array();
										fields[0] = 'custrecord_serialstatus';						
										values[0] = 'R';

										var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',vserialid, fields, values);
										nlapiLogExecution('ERROR', 'Record Updated :');
									}
//									}
								}
							}
							//response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
							//var searchresults =getReplenTasks(ReportNo,skuno,request.getParameter('hdnPickLocNo'));

							

							var getitemid = request.getParameter('hdngetitemid');
							var getlocid = request.getParameter('hdngetlocid');
							var taskpriority = request.getParameter('hdntaskpriority');
							var taskpriority = request.getParameter('hdntaskpriority');
							var getreportNo = request.getParameter('hdngetreportno');
							var replenType = request.getParameter('hdnReplenType');
var opentaskcount=0;
							var	opentasksearchresults=getConsolidatedopentaskcount(ReportNo,getitemid,request.getParameter('hdnPickLocNo'),taskpriority,getreportNo,replenType,request);
							if(opentasksearchresults!=null && opentasksearchresults.length>0)
							{
								opentaskcount=opentasksearchresults.length;
							}
							nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);

							if(opentaskcount >= 1)
							{
								if(SystemRule=='Y')
								{
									if(hdnBeginLocation==Reparray["custparam_nextlocation"])
									{

										response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);

									}
									else
										response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);

								}
								else
								{
									var item=opentasksearchresults[0].getValue('custrecord_sku',null,'group');
									var binloc=opentasksearchresults[0].getText('custrecord_actbeginloc',null,'group');
									var ItemType = nlapiLookupField('item', skuno, ['recordType','custitem_ebizbatchlot']);
									if(item==skuno && hdnBeginLocation==binloc && (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ))
									{
										Reparray['custparam_repbatchno']=opentasksearchresults[0].getValue('custrecord_batch_no',null,'group');
										Reparray['custparam_repexpqty']=opentasksearchresults[0].getValue('custrecord_expe_qty',null,'sum');
//										if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
//										{
										response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);
										return;
//										}
									}
									else if(item!=skuno && hdnBeginLocation==binloc )
									{
										response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);
										return;
									}
									else
									{
										response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
										return;
									}
								}
							}
							else
							{
								nlapiLogExecution('Debug', 'Test2', 'Test2');
								response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
							}
							/*var searchresults =getReplenTasks(ReportNo,skuno,request.getParameter('hdnPickLocNo'),getitemid,getlocid,taskpriority,getreportNo,replenType);
					nlapiLogExecution('ERROR', 'searchresults', searchresults);
					for (var s = 0; searchresults!=null && s < searchresults.length; s++) 
					{
						nlapiLogExecution('ERROR', 'test', 'test');
						nlapiLogExecution('ERROR', 'searchresults', searchresults.length);
						var Rid =searchresults[s].getId();
						var qty =searchresults[s].getValue('custrecord_expe_qty');
						var batch=searchresults[s].getValue('custrecord_batch_no');
						var skuno=searchresults[s].getValue('custrecord_ebiz_sku_no');
						var beginlocation =searchresults[s].getValue('custrecord_actbeginloc');
						var itemStatus =searchresults[s].getValue('custrecord_sku_status');

						try {
							// Load the record from opentask and update the corresponding values
							var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask',Rid);
							transaction.setFieldValue('custrecord_actendloc', request.getParameter('hdnPickLocNo'));
							transaction.setFieldValue('custrecord_act_end_date', DateStamp());
							transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
							transaction.setFieldValue('custrecord_tasktype', 8);
							transaction.setFieldValue('custrecord_wms_status_flag', 19);
							transaction.setFieldValue('custrecord_act_qty', qty);
							if(NewLpScaned=='T')
								transaction.setFieldValue('custrecord_ebiz_new_lp', lp);

							var varsku=transaction.getFieldValue('custrecord_ebiz_sku_no');
							var ebizTaskno=transaction.getFieldValue('custrecord_ebiz_task_no');
							var invtLpno=transaction.getFieldValue('custrecord_lpno');
							var whLocation=transaction.getFieldValue('custrecord_wms_location');
							var binLocation=transaction.getFieldValue('custrecord_actendloc');
							var beginLocation = transaction.getFieldValue('custrecord_actbeginloc');
							var fromLPno=transaction.getFieldValue('custrecord_from_lp_no');
							//	var itemStatus = getDefaultItemStatus();
							var accountNumber = getAccountNumber(request.getParameter('hdnwhlocation'));
							nlapiLogExecution('ERROR', 'accountNumber', accountNumber);
							nlapiLogExecution('ERROR', 'ebizTaskno', ebizTaskno);
							var stageLocation=GetStageLocation(varsku, "", whLocation, "", "BOTH");
							if(stageLocation == -1){

								var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno);
								//var bulkLocationInventoryResults=getRecord(varsku, beginLocation, invtLpno);
								if(bulkLocationInventoryResults != null)
									updateBulkLocation(bulkLocationInventoryResults, varsku, beginlocation, qty, itemStatus);

								var invtresults=getRecord(varsku, binLocation,lp);

								var vGetFifoDate="";
								if(bulkLocationInventoryResults!=null)
									vGetFifoDate=GetFiFoDate(bulkLocationInventoryResults);
								else
									vGetFifoDate=DateStamp();

								nlapiLogExecution('ERROR','vGetFifoDate', vGetFifoDate);
								updateOrCreatePickfaceInventory(invtresults, varsku, qty, lp, binLocation, 
										itemStatus, accountNumber,batch,whlocation,skuno,vGetFifoDate);
								nlapiSubmitRecord(transaction, true);
							}
							else
							{
								//if(ebizTaskno != null && ebizTaskno != "")
								//{         
								//var invrecord =createstagInvtRecord(varsku, varsku,  qty, lp, whLocation,binLocation, itemStatus, accountNumber);
								//updateStagTask(ebizTaskno,invrecord);

								// Update inventory for the begin location
//								var bulkLocationInventoryResults=getRecord(varsku, request.getParameter('hdnBeginLocation'), invtLpno);
								var bulkLocationInventoryResults=getRecord(varsku, beginlocation, fromLPno);

								if(bulkLocationInventoryResults != null)
									updateBulkLocation(bulkLocationInventoryResults, varsku, beginlocation, qty, itemStatus);

								var vGetFifoDate="";
								if(bulkLocationInventoryResults!=null)
								{
									vGetFifoDate=GetFiFoDate(bulkLocationInventoryResults);
									nlapiLogExecution('ERROR','vGetFifoDate', vGetFifoDate);
								}
								else
									vGetFifoDate=DateStamp();
								//}

								nlapiLogExecution('ERROR','vGetFifoDate', vGetFifoDate);
								///nlapiLogExecution('ERROR','invrecord', invrecord);
								nlapiLogExecution('ERROR','qty', qty);
								nlapiLogExecution('ERROR','varsku', varsku);
								nlapiLogExecution('ERROR','Location',beginlocation);
								nlapiLogExecution('ERROR','invtLpno', invtLpno);
								nlapiLogExecution('ERROR','beginLocationId', beginlocation);

								var invRefNo=transaction.getFieldValue('custrecord_invref_no');                           
								var result = nlapiSubmitRecord(transaction);

								nlapiLogExecution('ERROR','varsku', varsku);
								nlapiLogExecution('ERROR','Bin Location Id', binLocation);
								nlapiLogExecution('ERROR','Begin Location Id', beginLocation);
								nlapiLogExecution('ERROR','Inventory Ref No', invRefNo);

								var invtresults=getRecord(varsku, request.getParameter('hdnPickLocNo'),lp);

								if(invRefNo != null && invRefNo != "")
								{
									//Update Inventory in pickface bin location
									updateOrCreatePickfaceInventory(invtresults, varsku, qty, lp, binLocation, 
											itemStatus, accountNumber,batch,whlocation,skuno,vGetFifoDate);
									nlapiLogExecution('ERROR', 'beginLocation', beginLocation);
									var LocationType = nlapiLookupField('customrecord_ebiznet_location', beginLocation, ['custrecord_ebizlocationtype']);
									nlapiLogExecution('ERROR', 'LocationType', LocationType);
									if (LocationType.custrecord_ebizlocationtype == '8' )
									{
										DeleteInventoryRecord(invRefNo);
									}
								}
							}
							if(invtresults != null && invtresults != "")
							{
								var columns = nlapiLookupField('customrecord_ebiznet_createinv', invtresults[0].getId(),['custrecord_ebiz_inv_lp']);
		    					if(columns.custrecord_ebiz_inv_lp != null && columns.custrecord_ebiz_inv_lp != "")
		    					{        						
		    						updatedSerialLP = columns.custrecord_ebiz_inv_lp;	
		    						nlapiLogExecution('ERROR','columns.custrecord_ebiz_inv_lp', columns.custrecord_ebiz_inv_lp);
		    					}
								TempSerialNoIDArray=Reparray["custparam_serialnoID"].split(',');
								for (var j = 0; j < TempSerialNoIDArray.length; j++) {
									nlapiLogExecution('ERROR', 'Inserting Serial NO.:', TempSerialNoIDArray[j]);
										var vserialid =  TempSerialNoIDArray[j];

										var fields = new Array();
										var values = new Array();
										fields[0] = 'custrecord_serialparentid';
										fields[1] = 'custrecord_serialbinlocation';

										values[0] = updatedSerialLP;
										values[1] = binLocation;

										var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',vserialid, fields, values);
										nlapiLogExecution('ERROR', 'Record Updated with Merged LP:');
								}
							}
						}
						catch (e) {
							nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed', e);
						}
					}
					var Endlocationsearchresults =getendReplenTasks(ReportNo,skuno,beginlocation);
					if(Endlocationsearchresults!=null && Endlocationsearchresults!='')
					{
						Reparray["custparam_repno"]= Endlocationsearchresults[0].getValue('name');
						Reparray["custparam_repbegloc"] = Endlocationsearchresults[0].getText('custrecord_actbeginloc');
						Reparray["custparam_repbeglocId"] = Endlocationsearchresults[0].getValue('custrecord_actbeginloc');
						Reparray["custparam_repsku"] = Endlocationsearchresults[0].getText('custrecord_sku');
						Reparray["custparam_repexpqty"] = Endlocationsearchresults[0].getValue('custrecord_expe_qty');
						Reparray["custparam_repskuno"] = Endlocationsearchresults[0].getValue('custrecord_ebiz_sku_no');
						Reparray["custparam_repid"] = Endlocationsearchresults[0].getId();
						Reparray["custparam_whlocation"] = Endlocationsearchresults[0].getValue('custrecord_wms_location');
						Reparray["custparam_reppicklocno"] = Endlocationsearchresults[0].getValue('custrecord_actendloc');
						Reparray["custparam_reppickloc"] = Endlocationsearchresults[0].getText('custrecord_actendloc');
						Reparray["custparam_fromlpno"] = Endlocationsearchresults[0].getValue('custrecord_from_lp_no');
						Reparray["custparam_tolpno"] = Endlocationsearchresults[0].getValue('custrecord_lpno');
						response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
					}
					else
					{
						var opentaskcount=0;									
						opentaskcount=getOpenTasksCount(ReportNo);
						nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);

						if(opentaskcount >= 1)
						{
							nlapiLogExecution('ERROR', 'Test1', 'Test1');
							//response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);
							response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
						}
						else
						{
							nlapiLogExecution('ERROR', 'Test2', 'Test2');
							response.sendRedirect('SUITELET', 'customscript_rf_replen_cont', 'customdeploy_rf_replen_cont_di', false, Reparray);
						}
							}*/
						}
					}
				}
				catch (e)  {
					nlapiLogExecution('ERROR', 'exception', e);
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				} finally {					
					context.setSessionObject('session', null);
					nlapiLogExecution('DEBUG', 'finally','block');

				}
			}
			else
			{
				Reparray["custparam_screenno"] = 'R1';
				Reparray["custparam_error"] = 'SERIAL# ALREADY IN PROCESS';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			}
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception in Replenishment Post Block',exp);
		}
	}
}



function createMasterLPRecord(lp,whlocation)
{
	nlapiLogExecution('ERROR', 'Into createMasterLPRecord');

	var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
	MastLP.setFieldValue('name', lp);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lptype', 1);
	if(whlocation!=null && whlocation!='')
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_site', whlocation);

	var retktoval = nlapiSubmitRecord(MastLP);

	nlapiLogExecution('ERROR', 'Out of createMasterLPRecord');
}

function validateLP(lp,skuno,binlocid,whlocation)
{
	nlapiLogExecution('ERROR', 'Into validateLP');

	var str = 'lp. = ' + lp + '<br>';
	str = str + 'skuno. = ' + skuno + '<br>';	
	str = str + 'binlocid. = ' + binlocid + '<br>';
	str = str + 'whlocation. = ' + whlocation + '<br>';

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	var isValidLp='F';
	var ebizlpno='-1';

	ebizlpno=geteBizLPNo(lp,whlocation);
	nlapiLogExecution('ERROR', 'ebizlpno',ebizlpno);
	if(ebizlpno!=null && ebizlpno!='' && ebizlpno!='-1')
	{
		var isLPExist='F';

		isLPExist = isLPExistinStorage(lp,binlocid,whlocation);
		nlapiLogExecution('ERROR', 'isLPExist',isLPExist);
		if(isLPExist=='T')
		{
			isValidLp='F';
		}
		else
		{
			isValidLp='T';
		}

	}
	else
	{
		var getEnteredLPPrefix = lp.substring(0, 3).toUpperCase();
		nlapiLogExecution('ERROR', 'getEnteredLPPrefix',getEnteredLPPrefix);
		var LPReturnValue = ebiznet_LPRange_CL_withLPType(lp.replace(/\s+$/,""), '2','1',whlocation);//'2'UserDefiend,'1'PALT
		nlapiLogExecution('ERROR', 'LPReturnValue',LPReturnValue);
		if(LPReturnValue == true)
		{
			isValidLp='T';
		}
		else
		{
			isValidLp='F';
		}
	}

	nlapiLogExecution('ERROR', 'Out of validateLP');

	return isValidLp;
}

function isLPExistinStorage(lp,binlocid,whlocation)
{
	nlapiLogExecution('ERROR', 'Into isLPExistinStorage');

	var isLPExist='F';
	var filtersso = new Array();

	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', [binlocid]));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp));

	if(whlocation!=null && whlocation!="")
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [whlocation]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var searchresultsInvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsInvt != null && searchresultsInvt != '' && searchresultsInvt.length>0)
		isLPExist='T';

	nlapiLogExecution('ERROR', 'Out of isLPExistinStorage');

	return isLPExist;
}

function geteBizLPNo(lp,whlocation)
{
	nlapiLogExecution('ERROR', 'Into geteBizLPNo');

	var ebizlpno='-1';
	var filters = new Array();
	var columns = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));

	if(whlocation!=null && whlocation!="")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', ['@NONE@',whlocation]));

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');

	var lpmasterrecordsearch= nlapiSearchRecord('customrecord_ebiznet_master_lp',null,filters,columns);
	if(lpmasterrecordsearch!=null && lpmasterrecordsearch!='' && lpmasterrecordsearch.length>0)
		ebizlpno=lpmasterrecordsearch[0].getId();

	nlapiLogExecution('ERROR', 'Out of geteBizLPNo');

	return ebizlpno;		
}

function getReplenTasks(reportNo,sku,endlocation,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('Debug', 'reportNo', reportNo);
	nlapiLogExecution('Debug', 'sku', sku);
	nlapiLogExecution('Debug', 'endlocation', endlocation);

	nlapiLogExecution('Debug', 'getitemid in replen task',getitemid);
	nlapiLogExecution('Debug', 'getlocid in replen task',getlocid);
	nlapiLogExecution('Debug', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('Debug', 'getreportNo in replen task',getreportNo);

	var filters = new Array();

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}

	//if(getBinLocationId!=null && getBinLocationId!='')
	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

	}

	//if(getPriority!=null && getPriority!='')
	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('Debug','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isnotempty'));

	/*filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);
	filters[3] = new nlobjSearchFilter('custrecord_sku', null,'is', sku);
	filters[4] = new nlobjSearchFilter('custrecord_act_qty', null, 'isnotempty');
	if(endlocation!=null && endlocation!='')
		filters[5] = new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', endlocation);
	 */

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_batch_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_sku_status');
	columns[5] = new nlobjSearchColumn('custrecord_act_qty');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_reversalqty');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	nlapiLogExecution('Debug', 'searchresults', searchresults.length);
	return searchresults;
}


function getendReplenTasks(ReportNo,skuno,beginlocation)
{
	nlapiLogExecution('ERROR', 'ReportNo', ReportNo);
	nlapiLogExecution('ERROR', 'skuno', skuno);
	nlapiLogExecution('ERROR', 'beginlocation', beginlocation);
	var openreccount=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', ReportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);
	filters[3] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	//filters[4] = new nlobjSearchFilter('custrecord_sku', null,'is', skuno);
	filters[4] = new nlobjSearchFilter('custrecord_actbeginloc', null,'is', beginlocation);
	
	filters[5] = new nlobjSearchFilter('custrecord_act_qty', null, 'isnotempty');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_batch_no');

	columns[1].setSort(false);
	columns[2].setSort(false);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!=''&& searchresults.length>0)
		openreccount=searchresults.length;		


	return searchresults;

}

function getOpenTasksCount(ReportNo)
{
	nlapiLogExecution('ERROR', 'ReportNo', ReportNo);
	var openreccount=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', ReportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);
	filters[3] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_batch_no');

	columns[1].setSort(false);
	columns[2].setSort(false);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!=''&& searchresults.length>0)
		openreccount=searchresults.length;		


	return openreccount;

}
/**
 * 
 * @param bulkLocationInventoryResults
 * @param expQty
 * @param itemStatus
 */
function updateBulkLocation(bulkLocationInventoryResults, itemID, beginLocationId, expQty, itemStatus){
	if(bulkLocationInventoryResults != null)
	{	
		var bulkLocationInventoryRecId=bulkLocationInventoryResults[0].getId();
		nlapiLogExecution("ERROR", "Bulk Location Inventory Rec Id", bulkLocationInventoryRecId);								
		var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', bulkLocationInventoryRecId);
		var inventoryQty = transaction.getFieldValue('custrecord_ebiz_inv_qty');
		var inventoryAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
		var inventoryQOHQty= transaction.getFieldValue('custrecord_ebiz_qoh');

		nlapiLogExecution("ERROR", "inventoryQty", inventoryQty);
		nlapiLogExecution("ERROR", "inventoryAllocQty", inventoryAllocQty);
		nlapiLogExecution("ERROR", "inventoryQOHQty", inventoryQOHQty);

		if (inventoryQty == null || isNaN(inventoryQty) || inventoryQty == "") 
		{
			inventoryQty = 0;
		}

		inventoryAllocQty = parseInt(inventoryAllocQty) - parseInt(expQty);				
		inventoryQty =	parseInt(inventoryQty) - parseInt(expQty);	
		var QOHQty= parseInt(inventoryQOHQty)- parseInt(expQty);	

		nlapiLogExecution("ERROR", "After inventoryQty", inventoryQty);
		nlapiLogExecution("ERROR", "After inventoryAllocQty", inventoryAllocQty);
		nlapiLogExecution("ERROR", "After inventoryQOHQty", inventoryQOHQty);

		transaction.setFieldValue('custrecord_ebiz_alloc_qty',inventoryAllocQty);
		transaction.setFieldValue('custrecord_ebiz_inv_sku_status',itemStatus);
		transaction.setFieldValue('custrecord_ebiz_qoh',QOHQty);	
//		transaction.setFieldValue('custrecord_ebiz_inv_qty',inventoryQty);	

		nlapiSubmitRecord(transaction);

		nlapiLogExecution("ERROR", "Updating Alloc Qty", inventoryAllocQty);

		var retValue =  LocationCubeUpdation(itemID, beginLocationId, expQty, 'P');
		nlapiLogExecution('ERROR', 'updateBulkLocation LocCubeUpdation', 'Success');

	}
}

function LocationCubeUpdation(itemID, beginLocationId, expQty, optype)
{
	var arrDims = getSKUCubeAndWeight(itemID, 1);
	var itemCube = 0;
	var vTotalCubeValue = 0;
	if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
	{
		var uomqty = ((parseFloat(expQty))/(parseFloat(arrDims["BaseUOMQty"])));			
		itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
	} 

	var vOldRemainingCube = GeteLocCube(beginLocationId);
	nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

	if(optype == "P")
		vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
	else
		vTotalCubeValue = parseFloat(itemCube) - parseFloat(vOldRemainingCube);

	nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

	var retValue =  UpdateLocCube(beginLocationId,vTotalCubeValue);
	nlapiLogExecution('ERROR', 'LocCubeUpdation', retValue);

	return retValue; 
}


/**
 * 
 * @param invtresults
 */
function updateOrCreatePickfaceInventory(invtresults, varsku, expQty, invtLpno, binLocation, 
		itemStatus, accountNumber,batchno,whlocation,skuno,GetFifoDate){
	
	nlapiLogExecution("ERROR", "expQty Assigned in update/create pickface inv", expQty);	
	nlapiLogExecution("ERROR", "invtLpno Assigned in update/create pickface inv", invtLpno);	
	nlapiLogExecution("ERROR", "binLocation Assigned in update/create pickface inv", binLocation);	
	nlapiLogExecution("ERROR", "itemStatus Assigned in update/create pickface inv", itemStatus);	
	nlapiLogExecution("ERROR", "accountNumber Assigned in update/create pickface inv", accountNumber);	
	nlapiLogExecution("ERROR", "batchno Assigned in update/create pickface inv", batchno);	
	nlapiLogExecution("ERROR", "whlocation Assigned in update/create pickface inv", whlocation);	
	nlapiLogExecution("ERROR", "invtresults Assigned in update/create pickface inv", invtresults);	
	
	
	if(invtresults != null)
	{
		nlapiLogExecution("ERROR", "Inventory Exists", invtresults[0].getId());	
		var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtresults[0].getId());								
		var invtqty = transaction.getFieldValue('custrecord_ebiz_qoh');									

		if (invtqty == null || isNaN(invtqty) || invtqty == "") {
			invtqty = 0;
		}

		invtqty = parseInt(invtqty) + parseInt(expQty);						
		transaction.setFieldValue('custrecord_ebiz_inv_sku_status',itemStatus);	
		transaction.setFieldValue('custrecord_ebiz_qoh',invtqty);	
		transaction.setFieldValue('custrecord_ebiz_callinv','N');	
		transaction.setFieldValue('custrecord_ebiz_displayfield','N');	

		nlapiLogExecution("ERROR", "Quantity Assigned ", invtqty);						
		nlapiSubmitRecord(transaction);	

		updateInvRefforOpenPickTasks(varsku,binLocation,invtresults[0].getId());
	}
	else
	{
		nlapiLogExecution("ERROR", "Inventory Not Exists", varsku);					
		var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
		invrecord.setFieldValue('name', varsku+1);					
		invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
		invrecord.setFieldValue('custrecord_ebiz_inv_note1','Replenished');
		invrecord.setFieldValue('custrecord_ebiz_qoh', expQty);
		invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		
		invrecord.setFieldValue('custrecord_ebiz_inv_lp', invtLpno);		
		invrecord.setFieldValue('custrecord_ebiz_inv_binloc', binLocation);	
		invrecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
		invrecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
		invrecord.setFieldValue('custrecord_ebiz_inv_qty', expQty);
		if(accountNumber!=null && accountNumber!='')
			invrecord.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);

		invrecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
		invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
		invrecord.setFieldValue('custrecord_invttasktype', 8);
		invrecord.setFieldValue('custrecord_inv_ebizsku_no', skuno);
		nlapiLogExecution("ERROR", "GetFifoDate", GetFifoDate);	
		if(GetFifoDate==null || GetFifoDate=='')
			GetFifoDate=DateStamp();
		invrecord.setFieldValue('custrecord_ebiz_inv_fifo', GetFifoDate);
		invrecord.setFieldValue('custrecord_ebiz_expdate', GetFifoDate);

		if(batchno!=null && batchno!='')
			invrecord.setFieldText('custrecord_ebiz_inv_lot', batchno);
		invrecord.setFieldValue('custrecord_ebiz_inv_loc', whlocation);

		var invtrefno = nlapiSubmitRecord(invrecord);
		nlapiLogExecution("ERROR", "invtrefno", invtrefno);	
		updateInvRefforOpenPickTasks(varsku,binLocation,invtrefno);
	}

	var retValue =  LocationCubeUpdation(varsku, binLocation, expQty, 'M');
	nlapiLogExecution('ERROR', 'updateOrCreatePickfaceInventory LocCubeUpdation', 'Success');
}

function updateInvRefforOpenPickTasks(itemno,binlocation,invrefno)
{
	nlapiLogExecution('ERROR', 'Into  updateInvRefforOpenPickTasks');

	var str = 'itemno. = ' + itemno + '<br>';
	str = str + 'binlocation. = ' + binlocation + '<br>';	
	str = str + 'invrefno. = ' + invrefno + '<br>';

	nlapiLogExecution('ERROR', 'Parameter Details', str);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', binlocation));
	filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', itemno));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));	 // TASK TYPE - PICK	
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	 // STATUS - PICKS GENERATED
	filters.push(new nlobjSearchFilter('custrecord_invref_no', null, 'isempty'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, null);

	if(searchresults!=null && searchresults!='')
	{
		for (var s = 0; s < searchresults.length; s++) {
			var taskrecid = searchresults[s].getId();

			if(taskrecid!=null && taskrecid!='')
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', taskrecid, 'custrecord_invref_no', invrefno);
		}
	}

	nlapiLogExecution('ERROR', 'Out of  updateInvRefforOpenPickTasks');
}

/**
 * This is to update the status of the scanned location from 'R' to 'T'
 * @param recId
 */
function updateScannedLoc(recId)
{
	try {
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
		transaction.setFieldValue('custrecord_wms_status_flag', 24);
		var result = nlapiSubmitRecord(transaction);
	}
	catch (e) {
		nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed 2', e);
	}
}

/**
 * 
 * @param itemid
 * @param location
 * @param invtLPNo
 * @returns
 */
function getRecord(itemid,location, invtLPNo)
{
	nlapiLogExecution('ERROR', 'getRecord', location);
	var filtersso = new Array();
	var socount=0;		

	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', [itemid]));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));
	filtersso.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
	//filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', invtLPNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsLine != null)
	{
		nlapiLogExecution('ERROR', 'Inventory count', searchresultsLine.length);
		socount=searchresultsLine.length; 
	} 
	else
	{ 
		nlapiLogExecution('ERROR', 'inside else if results a', 'null');
	}       
	return searchresultsLine;
}

/**
 * 
 * @param recId
 * @param invrecord
 */
function updateStagTask(recId,invrecord)
{
	try {
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
		transaction.setFieldValue('custrecord_wms_status_flag', 24);		//20);
		transaction.setFieldValue('custrecord_invref_no', invrecord);
		var result = nlapiSubmitRecord(transaction);
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed 1', e);
	}
}

/**
 * 
 * @param vitem
 * @param varsku
 * @param varqty
 * @param lp
 * @param whLocation
 * @param binLocation
 * @param itemStatus
 * @param accountNumber
 * @returns
 */
function createstagInvtRecord(vitem, varsku, varqty, lp, whLocation, binLocation, itemStatus, accountNumber)
{
	nlapiLogExecution('ERROR','Inside InvtRecCreatedfor ','Function');
	var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	invrecord.setFieldValue('name', vitem+1);
	invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku);
	invrecord.setFieldValue('custrecord_ebiz_inv_note1','Replenished');
	invrecord.setFieldValue('custrecord_ebiz_qoh', varqty);
	invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');
	invrecord.setFieldValue('custrecord_ebiz_inv_lp', lp);
	invrecord.setFieldValue('custrecord_ebiz_inv_binloc', binLocation);
	invrecord.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
	invrecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
	invrecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
	invrecord.setFieldValue('custrecord_ebiz_inv_qty', varqty);
	//invrecord.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);

	invrecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
	invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
	invrecord.setFieldValue('custrecord_invttasktype', 8);

	var retval=nlapiSubmitRecord(invrecord);
	return retval;
}

/**
 * 
 */
function DeleteInventoryRecord(invRefNo)
{
	nlapiLogExecution('ERROR','Inside DeleteInvtRecCreatedforCHKNTask ','Function');
	var Ifilters = new Array();
	Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
	Ifilters.push(new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [8]));
	Ifilters.push(new nlobjSearchFilter('internalid', null, 'is', invRefNo));

	var invtId = "";
	var invtType = "";
	var searchInventory= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (searchInventory){
		for (var s = 0; s < searchInventory.length; s++) {
			invtId = searchInventory[s].getId();
			invtType= searchInventory[s].getRecordType();
			nlapiDeleteRecord(searchInventory[s].getRecordType(),searchInventory[s].getId());
			nlapiLogExecution('ERROR','Inventory record deleted ',invtId);
		}
	}
}

/**
 * 
 */
function UpdateInventory()
{
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
	transaction.setFieldValue('custrecord_wms_status_flag', 23);

	var result = nlapiSubmitRecord(transaction);
}

/**
 * 
 * Below is the search criteria to fetch the default item status from the item status master
 */

function getDefaultItemStatus(){

	var filtersitemstatus = new Array();
	filtersitemstatus[0] = new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T');

	var searchresultsitemstatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersitemstatus, null);
	if(searchresultsitemstatus != null && searchresultsitemstatus.length > 0)
	{
		var itemStatus = searchresultsitemstatus[0].getId();
		nlapiLogExecution("ERROR", "searchresultsitemstatus[0].getId()", itemStatus);
	}
	return itemStatus;
}

/**
 * 
 * @param whLocation
 * @returns
 */
function getAccountNumber(whLocation){
	var filtersAccount = new Array();
	filtersAccount[0] = new nlobjSearchFilter('custrecord_location', null, 'is', whLocation);

	var columnsAccount = new Array();
	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);              

	if (accountSearchResults != null){
		var accountNumber = accountSearchResults[0].getValue('custrecord_accountno');                                
		nlapiLogExecution('ERROR', 'Account #',accountNumber);
	}

	return accountNumber;
}

function GetFiFoDate(SearchRec)
{
	try
	{
		var Fifodate=nlapiLookupField('customrecord_ebiznet_createinv',SearchRec[0].getId(),'custrecord_ebiz_inv_fifo');
		return Fifodate;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetFiFoDate',exp);	
	}
}

function GetTotalQty(getSkuNo,beginLocId,ReportNo)
{
	try
	{
		var qty;
		nlapiLogExecution('ERROR','Into GetTotalQty',getSkuNo+'/'+beginLocId);
		var filters=new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is', ReportNo);
		filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
		filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);
		
		if(getSkuNo!=null&&getSkuNo!="")
		filters.push(new nlobjSearchFilter('custrecord_sku',null,'is',getSkuNo));
		
		if(beginLocId!=null&&beginLocId!="")
		filters.push(new nlobjSearchFilter('custrecord_actbeginloc',null,'anyof',beginLocId));
		
		var column=new Array();
		column[0]=new nlobjSearchColumn('formulanumeric',null,'SUM');
		column[0].setFormula("TO_NUMBER({custrecord_act_qty})");
		
		var searchrec=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);
		
		if(searchrec!=null&&searchrec!="")
			qty=searchrec[0].getValue('formulanumeric',null,'SUM');
		nlapiLogExecution('ERROR','qty',qty);
		return qty;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetTotalQty',exp);	
	}
	

}







function getConsolidatedopentaskcount(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType,request)
{

	nlapiLogExecution('ERROR', 'reportNo',reportNo);

	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);
	nlapiLogExecution('ERROR', 'replenType in replen task',replenType);

	var filters = new Array();

	if(reportNo != null && reportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
	}

	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='')
	{
		nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));
	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
	}

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
	columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
//	var fields=new Array();
//	nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
//	fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
//	var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
//	if(sresult!=null && sresult.length>0)
//	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', sresult[0].getId()));
	if(getitemid!=null && getitemid!=''&&getitemid!='null')
	{
		var ItemType = nlapiLookupField('item', getitemid, ['recordType','custitem_ebizbatchlot']);
		if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
		{
			columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
		}
	}


	columns[1].setSort(false);
	columns[2].setSort(false);
	columns[3].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR', 'searchresults.length ingetreplen',searchresults.length);
	return searchresults;

}
