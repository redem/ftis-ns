/***************************************************************************
eBizNET Solutions                
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingContainerProcess.js,v $
 *     	   $Revision: 1.8.2.17.4.8.2.10 $
 *     	   $Date: 2013/06/24 15:45:32 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * 
 *****************************************************************************/

function PickingContainerProcess(request, response){
	nlapiLogExecution('DEBUG', 'Into Request', 'Into Request');
	if (request.getMethod() == 'GET') 
	{
		var SOarray = new Array();
		var fastpick = request.getParameter('custparam_fastpick');
		nlapiLogExecution('DEBUG', 'fastpick', 'fastpick');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		SOarray["custparam_language"] = getLanguage;

		var getWaveNo=request.getParameter('custparam_waveno');
		var getOrdNo=request.getParameter('custparam_orderno');
		var getClusterNo=request.getParameter('custparam_clusno');

		nlapiLogExecution('DEBUG', 'getOrdNo', getOrdNo);
		nlapiLogExecution('DEBUG', 'getWaveNo', getWaveNo);
		nlapiLogExecution('DEBUG', 'getClusterNo', getClusterNo);

		if(getClusterNo!=null && getClusterNo!='' && getClusterNo!='null')
		{

			var taskassignedto='';
			var SOFilters = new Array();

			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
			SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', getClusterNo));
			var vRoleLocation=getRoledBasedLocation();
			nlapiLogExecution('DEBUG', 'vRoleLocation', vRoleLocation);
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

			}

			var SOColumns = new Array();
			//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
			SOColumns[0]=new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
			//Code end as on 290414
			SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			SOColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
			SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
			SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
			SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
			SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
			SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
			SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');
			SOColumns[11] = new nlobjSearchColumn('custrecord_wms_location');
			SOColumns[12] = new nlobjSearchColumn('custrecord_comp_id');
			SOColumns[13] = new nlobjSearchColumn('name');
			SOColumns[14] = new nlobjSearchColumn('custrecord_container');
			SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			SOColumns[16] = new nlobjSearchColumn('custrecord_container_lp_no');
			SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
			SOColumns[18] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
			SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');
			SOColumns[20] = new nlobjSearchColumn('custrecord_taskassignedto');
			SOColumns[21] = new nlobjSearchColumn('custrecord_transport_lp');
			SOColumns[22] = new nlobjSearchColumn('custrecord_ebiz_pickingcartsize');


			SOColumns[0].setSort();	
			SOColumns[1].setSort();		//	Location Pick Sequence
			SOColumns[3].setSort();		//	SKU
			//SOColumns[16].setSort();	//	Container LP	

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

			if (SOSearchResults != null && SOSearchResults.length > 0) {

				var vorderno;
				var HoldStatus;
				var vordername;
				//var HoldStatusFlag = 'T';
				var OrdeNoArray = new Array();
				for(var f=0; f < SOSearchResults.length; f++)
				{
					var currrow=[SOSearchResults[f].getValue('custrecord_ebiz_order_no'),SOSearchResults[f].getText('custrecord_ebiz_order_no')];
					OrdeNoArray.push(currrow);

				}
				if(OrdeNoArray != null && OrdeNoArray != '')
				{
					OrdeNoArray = removeDuplicateElement(OrdeNoArray); 

					for(var u=0;u<OrdeNoArray.length;u++)
					{
						//nlapiLogExecution('DEBUG', 'OrdeNoArray[u][0] before tst',OrdeNoArray[u][0] );
						//nlapiLogExecution('DEBUG', 'OrdeNoArray[u][1] before tst',OrdeNoArray[u][1] );
						HoldStatus = fnCreateFo(OrdeNoArray[u][0]);
						//nlapiLogExecution('DEBUG', 'HoldStatus before tst',HoldStatus );

						//HoldStatus = 'F';
						//nlapiLogExecution('DEBUG', 'OrdeNoArray tst',OrdeNoArray );
						//nlapiLogExecution('DEBUG', 'HoldStatus after tst',HoldStatus );

						if(HoldStatus == 'F')
						{
							nlapiLogExecution('DEBUG', 'OrdeNoArray[u][0] after tst',OrdeNoArray[u][0] );
							nlapiLogExecution('DEBUG', 'OrdeNoArray[u][1] after tst',OrdeNoArray[u][1] );
							vordername = OrdeNoArray[u][1];
							break;
						}

					}
				}

				taskassignedto=SOSearchResults[0].getValue('custrecord_taskassignedto');
				nlapiLogExecution('DEBUG', 'taskassignedto', taskassignedto);
				var currentContext = nlapiGetContext();  
				var currentUserID = currentContext.getUser();
				nlapiLogExecution('DEBUG', 'currentUserID', currentUserID);
				if(taskassignedto==null || taskassignedto=='' )
				{
					for ( var b = 0; b < SOSearchResults.length; b++)
					{
						var RecID=SOSearchResults[b].getId();

						var fields=new Array();
						fields[0]='custrecord_taskassignedto';
						fields[1]='custrecord_actualbegintime';
						var Values=new Array();
						Values[0]=currentUserID;
						Values[1]=TimeStamp();
						var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',RecID,fields,Values);
					}
				}
				else if(currentUserID!=taskassignedto)
				{
					nlapiLogExecution('DEBUG', 'taskassignedto1', taskassignedto);

					var msg1='Picking Is In-Progress for the Cluster#';
					var msg2='. Contact Supervisor.';
					SOarray["custparam_error"] = msg1+getClusterNo+msg2;
					nlapiLogExecution('DEBUG', 'errorMessage', msg1+getClusterNo+msg2);
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}

				var vSkipId=0;

				var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];

				SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no');
				SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
				SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
				SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
				SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
				SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
				SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
				SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
				SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
				SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
				SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no');
				SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
				SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
				SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
				SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
				SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
				SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
				SOarray["name"] =  SOSearchResult.getValue('name');
				SOarray["custparam_picktype"] =  'CL';
				SOarray["custparam_nextlocation"] = '';
				SOarray["custparam_nextiteminternalid"] = '';
				SOarray["custparam_nextitem"] = '';
				SOarray["custparam_pickingcarton"]=SOSearchResult.getValue('custrecord_transport_lp');
				SOarray["custparam_pickingcarton_sizeid"]=SOSearchResult.getText('custrecord_ebiz_pickingcartsize');
				SOarray["custparam_pickingcarton_sizeidvalue"]=SOSearchResult.getValue('custrecord_ebiz_pickingcartsize');
				//code added by santosh on 7Aug2012
				SOarray["custparam_type"] ="Cluster";
				//end of the code on 7Aug2012
				nlapiLogExecution('DEBUG', 'Begin Location Name',SOarray["custparam_beginlocationname"]);
				nlapiLogExecution('DEBUG', 'Redirecting to Location Screen');


				if(HoldStatus == "F")
				{
					SOarray["custparam_error"] = "Either SO is Closed/Cancelled or Payment is HOLD/Days Over Due/Credit Limit Exceed for  " + vordername;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				else
				{
					var orderArray=ISOrderPartiallyPickedForShipCompleteOrders(SOarray["custparam_waveno"]);

					if(orderArray!=null&&orderArray!="")
					{
						var msg="";
						for(var j=0;j<orderArray.length;j++)
						{
							if(j==0)
								msg=orderArray[j];
							else
								msg=msg+","+orderArray[j];
						}
						nlapiLogExecution('DEBUG', 'msg', msg);
						SOarray["custparam_error"] = 'Picks generated partially for an orders# "' + msg + '" with ship complete';
						nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					else
					{
						//Case # 201410947 Start

						var varSystemRuleValue='N' ;
						varSystemRuleValue = getSystemRuleValue(SOarray["custparam_whlocation"]);
						if(varSystemRuleValue == 'N')
						{
							nlapiLogExecution('DEBUG', 'inside varSystemRuleValue');


							try
							{
								var SOFilters = new Array();


								SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
								SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', getClusterNo));

								var SOColumns = new Array();
								SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
								SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
								SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
								SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
								SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
								SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
								SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
								SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
								SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
								SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
								SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
								SOColumns[11] = new nlobjSearchColumn('custrecord_line_no',null,'group');
								SOColumns[12] = new nlobjSearchColumn('internalid',null,'group');
								SOColumns[13] = new nlobjSearchColumn('name',null,'group');
								SOColumns[14] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
								SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no',null,'group');
								SOColumns[16] = new nlobjSearchColumn('custrecord_invref_no',null,'group');

								SOColumns[0].setSort(); 	//Location Pick Sequence
								SOColumns[1].setSort();		//SKU
								SOColumns[2].setSort();		//Location
								var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
								nlapiLogExecution('DEBUG', 'SOSearchResults' ,SOSearchResults);
								if (SOSearchResults != null && SOSearchResults.length > 0) {


									var SOSearchResult = SOSearchResults[0];
									nlapiLogExecution('DEBUG', 'SOSearchResults[0].getId()',SOSearchResults[0].getId());
									SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no',null,'group');
									SOarray["custparam_recordinternalid"] = SOSearchResult.getValue('internalid',null,'group');
									SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
									SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
									SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku',null,'group');
									SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc',null,'group');
									SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
									SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no',null,'group');
									SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no',null,'group');
									SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no',null,'group');
									SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
									SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no',null,'group');
									SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no',null,'group');
									//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no',null,'group');
									SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
									SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id',null,'group');
									//SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container',null,'group');
									//SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('internalid','custrecord_ebiz_order_no','group');
									SOarray["name"] =  SOSearchResult.getValue('name',null,'group');
									SOarray["custparam_picktype"] =  'CL';
									SOarray["custparam_nextlocation"] = '';
									SOarray["custparam_nextiteminternalid"] = '';
									SOarray["custparam_nextitem"] = '';
									SOarray["custparam_type"] ="Cluster";

									if(SOarray["custparam_recordinternalid"] != null && SOarray["custparam_recordinternalid"] != "")
									{
										var Opentaskrec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', SOarray["custparam_recordinternalid"]);
										SOarray["custparam_ebizordno"] =  Opentaskrec.getFieldValue('custrecord_ebiz_order_no');
										nlapiLogExecution('DEBUG', 'custparam_ebizordno',SOarray["custparam_ebizordno"]);
									}

									nlapiLogExecution('DEBUG', 'Begin Location Name',SOarray["custparam_beginlocationname"]);
									nlapiLogExecution('DEBUG', 'Redirecting to Location Screen');
									response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);						
									return;
								}
								else 
								{
									nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
									return;
								}
							}
							catch(e)
							{
								nlapiLogExecution('DEBUG', 'Error: ', e);
							}
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusterwave_summary', 'customdeploy_ebiz_rf_clusterwave_summary', false, SOarray);
							return;
						}
					}
				}
			}
			else {
				nlapiLogExecution('DEBUG', 'Error: ', 'Cluster # not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

			}

		}
		else
		{

			var SOFilters = new Array();
			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
			SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
			if(getWaveNo != null && getWaveNo != "")
			{
				nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
				SOarray["custparam_picktype"] = 'W';

			}
			if(getOrdNo!=null && getOrdNo!="")
			{
				nlapiLogExecution('DEBUG', 'getOrdNo inside if', getOrdNo);					
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', getOrdNo));
				SOarray["custparam_picktype"] = 'O';

			}


			var SOColumns = new Array();

			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));				
			SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
			SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));						
			SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			SOColumns.push(new nlobjSearchColumn('name'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
			SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
			SOColumns.push(new nlobjSearchColumn('custrecord_taskassignedto'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

//			SOColumns[0].setSort();
//			SOColumns[1].setSort();
//			SOColumns[2].setSort();
//			SOColumns[3].setSort();
//			SOColumns[4].setSort();
//			SOColumns[5].setSort(true);

			SOColumns[2].setSort();
			SOColumns[3].setSort();
			SOColumns[4].setSort();


			var taskassignedto;
			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResults);
			if (SOSearchResults != null && SOSearchResults.length > 0) {

				var vorderno;
				var HoldStatus;
				var vordername;
				//var HoldStatusFlag = 'T';
				var OrdeNoArray = new Array();
				for(var f=0; f < SOSearchResults.length; f++)
				{
					var currrow=[SOSearchResults[f].getValue('custrecord_ebiz_order_no'),SOSearchResults[f].getText('custrecord_ebiz_order_no')];
					OrdeNoArray.push(currrow);

				}
				if(OrdeNoArray != null && OrdeNoArray != '')
				{
					for(var u=0;u<OrdeNoArray.length;u++)
					{
						nlapiLogExecution('DEBUG', 'OrdeNoArray[u][0] before tst',OrdeNoArray[u][0] );
						nlapiLogExecution('DEBUG', 'OrdeNoArray[u][1] before tst',OrdeNoArray[u][1] );
						HoldStatus = fnCreateFo(OrdeNoArray[u][0]);
						nlapiLogExecution('DEBUG', 'HoldStatus before tst',HoldStatus );

						//HoldStatus = 'F';
						nlapiLogExecution('DEBUG', 'OrdeNoArray tst',OrdeNoArray );
						nlapiLogExecution('DEBUG', 'HoldStatus after tst',HoldStatus );

						if(HoldStatus == 'F')
						{
							nlapiLogExecution('DEBUG', 'OrdeNoArray[u][0] after tst',OrdeNoArray[u][0] );
							nlapiLogExecution('DEBUG', 'OrdeNoArray[u][1] after tst',OrdeNoArray[u][1] );
							vordername = OrdeNoArray[u][1];
							break;
						}

					}
				}
				taskassignedto=SOSearchResults[0].getValue('custrecord_taskassignedto');
				nlapiLogExecution('DEBUG', 'taskassignedto', taskassignedto);
				var currentContext = nlapiGetContext();  
				var currentUserID = currentContext.getUser();
				nlapiLogExecution('DEBUG', 'currentUserID', currentUserID);
				if(taskassignedto==null || taskassignedto=='' )
				{
					nlapiLogExecution('DEBUG', 'test1', 'test1');
					for ( var i = 0; i < SOSearchResults.length; i++)
					{
						var RecID=SOSearchResults[i].getId();
						var fields=new Array();
						fields[0]='custrecord_taskassignedto';					
						var Values=new Array();
						Values[0]=currentUserID;							
						var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',RecID,fields,Values);
					}
				}
				vSkipId=0;
				if(SOSearchResults.length <= parseFloat(vSkipId))
				{
					vSkipId=0;
				}
				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
				var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];

				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_lpno'));
				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
//				SOarray["custparam_waveno"] = getWaveNo;
				SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no');
				SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
				//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
				SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
				SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
				SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
				SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
				SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
				SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
				SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
				SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
				SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
				SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
				SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
				SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
				SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
				SOarray["custparam_noofrecords"] = SOSearchResults.length;
				SOarray["name"] =  SOSearchResult.getValue('name');
				SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
				SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');

				SOarray["custparam_venterFO"]=getOrdNo;
				SOarray["custparam_venterwave"]=getWaveNo;

				/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/					
				SOarray["custparam_nextlocation"]='';	
				SOarray["custparam_nextiteminternalid"]='';
				/* Up to here */ 

				var st13="Either SO is Closed/Cancelled or Payment is HOLD/Days Over Due/Credit Limit Exceed for  ";

				SOarray["custparam_type"] ="Wave";
				SOarray["custparam_fastpick"] = fastpick;
				nlapiLogExecution('DEBUG', 'WaveClusterScreen','WaveClusterScreen' );
				nlapiLogExecution('DEBUG', 'Location newly added', SOSearchResult.getText('custrecord_actbeginloc'));
				if(HoldStatus == "F")
				{
					SOarray["custparam_error"] = st13 + vordername;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
				}
			}
		}
	}
}

function ISOrderPartiallyPickedForShipCompleteOrders(getWaveNo)
{
	try
	{

		var SOFilters = new Array();

		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated/pickfailed
		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));

		var SOColumns = new Array();
		SOColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

		var SOSearchResult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

		var vSOArray=new Array();
		var vOrderArray=new Array();
		if(SOSearchResult!=null&&SOSearchResult!="")
		{
			nlapiLogExecution('DEBUG', 'SOSearchResult', SOSearchResult.length);

			for(var i=0;i<SOSearchResult.length;i++)
			{
				vSOArray.push(SOSearchResult[i].getValue('custrecord_ebiz_order_no'));
			}

			nlapiLogExecution('DEBUG', 'SOArray', vSOArray);

			var filter = new Array();
			filter[0]= new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', vSOArray);
			filter[1]= new nlobjSearchFilter('custrecord_shipcomplete', null, 'is', "T"); 
			filter[2]= new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickgen_qty}),0)");

			var column = new Array();
			column[0] = new nlobjSearchColumn('custrecord_lineord');
			column[1] = new nlobjSearchColumn('custrecord_shipcomplete');
			column[2] = new nlobjSearchColumn('custrecord_pickgen_qty');
			column[3] = new nlobjSearchColumn('custrecord_ebiz_linesku');

			var searchRec = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);

			if(searchRec!=null&&searchRec!="")
			{
				for(var j=0;j<searchRec.length;j++)
					vOrderArray.push(searchRec[j].getValue("custrecord_lineord"));			
			}
			nlapiLogExecution('DEBUG', 'vOrderArray', vOrderArray);
		}
		return vOrderArray;
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception in ISOrderPartiallyPickedForShipCompleteOrders",exp);
	}
}

function getSystemRuleValue(site)
{
	var getSystemRuleValue='N' ;

	var SystemRuleFilters = new Array();

	SystemRuleFilters.push(new nlobjSearchFilter('name', null, 'is', 'IS Summary Screen Required?'));
	if(site != null && site !='')
	{
		SystemRuleFilters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', site));
	}
	var cols= new Array();
	cols[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');

	var SystemRuleSearchResults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, SystemRuleFilters, cols);
	nlapiLogExecution('DEBUG', 'SystemRuleSearchResults', 'SystemRuleSearchResults');
	if (SystemRuleSearchResults != null && SystemRuleSearchResults.length > 0) {
		nlapiLogExecution('DEBUG', 'SystemRuleSearchResults', SystemRuleSearchResults.length);
		nlapiLogExecution('DEBUG', 'SystemRuleSearchResults', SystemRuleSearchResults[0].getId());
		getSystemRuleValue = SystemRuleSearchResults[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('DEBUG', 'getSystemRuleValue', getSystemRuleValue);

	}

	return getSystemRuleValue;

}


