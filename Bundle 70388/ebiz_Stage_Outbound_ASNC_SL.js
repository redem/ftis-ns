/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_PackingList_SL.js,v $
 *     	   $Revision: 1.5 $
 *     	   $Date: 2011/10/03 06:10:31 $
 *     	   $Author: mbpragada $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log:  $
 *****************************************************************************/
function ebiznet_StageOutboundASNC(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Stage Outbound ASNC');		 

		var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');
		trailer.setLayoutType('startrow', 'none');		 

		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else //this is the POST block
	{
		nlapiLogExecution('ERROR', 'into else part', 'done');
		var form = nlapiCreateForm('Stage Outbound ASNC');
		var vQbtrailer = request.getParameter('custpage_trailer');
		nlapiLogExecution('ERROR', 'vQbtrailer', vQbtrailer);
		var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');
		trailer.setLayoutType('startrow', 'none');
		trailer.setDefaultValue(vQbtrailer);

		var sublist = form.addSubList("custpage_items", "list", "ItemList");
		sublist.addMarkAllButtons();
		sublist.addField("custpage_select", "checkbox", "Confirm");
		sublist.addField("custpage_reference", "text", "ReferenceID").setDisplayType('entry');
		sublist.addField("custpage_messagetype", "text", "MessageType").setDisplayType('entry');
		sublist.addField("custpage_creationdatetime", "text", "CreationDateTime").setDisplayType('entry');
		sublist.addField("custpage_senderbusiness", "text", "SenderBusinessSystemID").setDisplayType('entry');
		sublist.addField("custpage_recipientbusiness", "text", "RecipientBusinessSystemID").setDisplayType('entry');
		sublist.addField("custpage_trailer", "text", "Trailer No#").setDisplayType('entry');
		sublist.addField("custpage_department", "select", "Department","Department").setDisplayType('entry');
		sublist.addField("custpage_class", "select", "Class","Class").setDisplayType('entry');
		sublist.addField("custpage_vendorid", "text", "VendorID").setDisplayType('entry');
		sublist.addField("custpage_shipunitid", "text", "ShipUnit ID").setDisplayType('entry');
		sublist.addField("custpage_shipunitcharges", "text", "ShipUnit Charges").setDisplayType('entry');
		sublist.addField("custpage_shipunitlengths", "text", "Shipunit Length").setDisplayType('entry');
		sublist.addField("custpage_shipunitwidth", "text", "Shipunit Width").setDisplayType('entry');
		sublist.addField("custpage_shipunitweight", "text", "Shipunit Weight").setDisplayType('entry');
		sublist.addField("custpage_shipunitheight", "text", "Shipunit Height").setDisplayType('entry');
		sublist.addField("custpage_shipunitlevelqty", "text", "ShipUnitLevelQty").setDisplayType('entry');
		sublist.addField("custpage_waybillref", "text", "Waybillref#").setDisplayType('entry');
		sublist.addField("custpage_carrierid", "text", "CarrierID").setDisplayType('entry');
		sublist.addField("custpage_scaccode", "text", "SCACCode").setDisplayType('entry');
		sublist.addField("custpage_carriername", "text", "CarrierName").setDisplayType('entry');
		sublist.addField("custpage_succ", "text", "S_UCC128 Label#").setDisplayType('entry');
		sublist.addField("custpage_route", "text", "Route").setDisplayType('entry');
		sublist.addField("custpage_dest", "text", "Dest").setDisplayType('entry');
		sublist.addField("custpage_vessel", "text", "Vessel").setDisplayType('entry');
		sublist.addField("custpage_voyage", "text", "Voyage").setDisplayType('entry');
		sublist.addField("custpage_bol", "text", "BOL").setDisplayType('entry');
		sublist.addField("custpage_pronum", "text", "Pronum").setDisplayType('entry');
		sublist.addField("custpage_shippinglocation", "text", "Shipping Location ID").setDisplayType('entry');
		sublist.addField("custpage_shiptoaddrone", "text", "Ship to Addr1").setDisplayType('entry');
		sublist.addField("custpage_shiptoaddrtwo", "text", "Ship to Addr2").setDisplayType('entry');
		sublist.addField("custpage_shiptocity", "text", "Ship to City").setDisplayType('entry');
		sublist.addField("custpage_shiptostate", "text", "Ship to State").setDisplayType('entry');
		sublist.addField("custpage_shiptozip", "text", "Ship to Zip").setDisplayType('entry');
		sublist.addField("custpage_shiptocountry", "text", "Ship to Country").setDisplayType('entry');
		sublist.addField("custpage_shiptophone", "text", "Ship to Phone").setDisplayType('entry');
		sublist.addField("custpage_shiptoemail", "text", "Ship to Email").setDisplayType('entry');
		sublist.addField("custpage_shiptofax", "text", "Ship to Fax").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcustzero", "text", "Shipunitaddr Custfiled_000").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcustone", "text", "Shipunitaddr Custfiled_001").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcusttwo", "text", "Shipunitaddr Custfiled_002").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcustthree", "text", "Shipunitaddr Custfiled_003").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcustfour", "text", "Shipunitaddr Custfiled_004").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcustfive", "text", "Shipunitaddr Custfiled_005").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcustsix", "text", "Shipunitaddr Custfiled_006").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcustseven", "text", "Shipunitaddr Custfiled_007").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcusteight", "text", "Shipunitaddr Custfiled_008").setDisplayType('entry');
		sublist.addField("custpage_shipunitaddrcustnine", "text", "Shipunitaddr Custfiled_009").setDisplayType('entry');
		sublist.addField("custpage_expectedarrival", "text", "Expected Arrival Date").setDisplayType('entry');
		sublist.addField("custpage_plannedarrival", "text", "Planned Arrival Date and Time").setDisplayType('entry');
		sublist.addField("custpage_issueperiod", "text", "Issue Period").setDisplayType('entry');
		sublist.addField("custpage_shippingcustzero", "text", "Shipping Custfield_000").setDisplayType('entry');
		sublist.addField("custpage_shippingcustone", "text", "Shipping Custfield_001").setDisplayType('entry');
		sublist.addField("custpage_shippingcusttwo", "text", "Shipping Custfield_002").setDisplayType('entry');
		sublist.addField("custpage_shippingcustthree", "text", "Shipping Custfield_003").setDisplayType('entry');
		sublist.addField("custpage_shippingcustfour", "text", "Shipping Custfield_004").setDisplayType('entry');
		sublist.addField("custpage_shippingcustfive", "text", "Shipping Custfield_005").setDisplayType('entry');
		sublist.addField("custpage_shippingcustsix", "text", "Shipping Custfield_006").setDisplayType('entry');
		sublist.addField("custpage_shippingcustseven", "text", "Shipping Custfield_007").setDisplayType('entry');
		sublist.addField("custpage_shippingcusteight", "text", "Shipping Custfield_008").setDisplayType('entry');
		sublist.addField("custpage_shippingcustnine", "text", "Shipping Custfield_009").setDisplayType('entry');
		sublist.addField("custpage_containerlp", "text", "ContainerLP").setDisplayType('entry');
		sublist.addField("custpage_containerlpcharge", "text", "ContainerLP Charges").setDisplayType('entry');
		sublist.addField("custpage_containertracking", "text", "Container TrackingNo").setDisplayType('entry');
		sublist.addField("custpage_cucc", "text", "C_UCC128 Lable#").setDisplayType('entry');
		sublist.addField("custpage_paymentterms", "text", "Payment Terms").setDisplayType('entry');
		sublist.addField("custpage_containerlength", "text", "Container Length").setDisplayType('entry');
		sublist.addField("custpage_containerwidth", "text", "Container Width").setDisplayType('entry');
		sublist.addField("custpage_containerheight", "text", "Container Height").setDisplayType('entry');
		sublist.addField("custpage_containerweight", "text", "Container Weight").setDisplayType('entry');
		sublist.addField("custpage_containerlevelqty", "text", "ContainerLevelQty").setDisplayType('entry');
		sublist.addField("custpage_deliveryid", "text", "DeliveryID").setDisplayType('entry');
		sublist.addField("custpage_ordertype", "text", "OrderType").setDisplayType('entry');
		sublist.addField("custpage_markfor", "text", "MARKFOR").setDisplayType('entry');
		sublist.addField("custpage_orderpriority", "text", "Order Priority").setDisplayType('entry');
		sublist.addField("custpage_consigneeid", "text", "ConsigneeID").setDisplayType('entry');
		sublist.addField("custpage_billinginfolocation", "text", "Billinginfo LocationID").setDisplayType('entry');
		sublist.addField("custpage_billtocontactname", "text", "Billto ContactName").setDisplayType('entry');
		sublist.addField("custpage_billtoaddrone", "text", "Billto Addr1").setDisplayType('entry');
		sublist.addField("custpage_billtoaddrtwo", "text", "Billto Addr2").setDisplayType('entry');
		sublist.addField("custpage_billtocity", "text", "Bill to City").setDisplayType('entry');
		sublist.addField("custpage_billtostate", "text", "Bill to State").setDisplayType('entry');
		sublist.addField("custpage_billtozip", "text", "Bill to Zip").setDisplayType('entry');
		sublist.addField("custpage_billtocountry", "text", "Bill to Country").setDisplayType('entry');
		sublist.addField("custpage_billtophone", "text", "Bill to Phone").setDisplayType('entry');
		sublist.addField("custpage_billtoemail", "text", "Bill to email").setDisplayType('entry');
		sublist.addField("custpage_billtofax", "text", "Bill to Fax").setDisplayType('entry');
		sublist.addField("custpage_billinginfocustzero", "text", "Billinginfo Custfield_000").setDisplayType('entry');
		sublist.addField("custpage_billinginfocustone", "text", "Billinginfo Custfield_001").setDisplayType('entry');
		sublist.addField("custpage_billinginfocusttwo", "text", "Billinginfo Custfield_002").setDisplayType('entry');
		sublist.addField("custpage_billinginfocustthree", "text", "Billinginfo Custfield_003").setDisplayType('entry');
		sublist.addField("custpage_billinginfocustfour", "text", "Billinginfo Custfield_004").setDisplayType('entry');
		sublist.addField("custpage_billinginfocustfive", "text", "Billinginfo Custfield_005").setDisplayType('entry');
		sublist.addField("custpage_billinginfocustsix", "text", "Billinginfo Custfield_006").setDisplayType('entry');
		sublist.addField("custpage_billinginfocustseven", "text", "Billinginfo Custfield_007").setDisplayType('entry');
		sublist.addField("custpage_billinginfocusteight", "text", "Billinginfo Custfield_008").setDisplayType('entry');
		sublist.addField("custpage_billinginfocustnine", "text", "Billinginfo Custfield_009").setDisplayType('entry');
		sublist.addField("custpage_markforlocation", "text", "Markfor LocationId").setDisplayType('entry');
		sublist.addField("custpage_markforshiptocontactname", "text", "Markfor shiptocontactname").setDisplayType('entry');
		sublist.addField("custpage_markforshiptoaddrone", "text", "Markfor ShiptoAddr1").setDisplayType('entry');
		sublist.addField("custpage_markforshiptoaddrtwo", "text", "Markfor ShiptoAddr2").setDisplayType('entry');
		sublist.addField("custpage_markforshiptocity", "text", "Markfor ShiptoCity").setDisplayType('entry');
		sublist.addField("custpage_markforshiptostate", "text", "Markfor ShiptoState").setDisplayType('entry');
		sublist.addField("custpage_markforshiptozip", "text", "Markfor ShiptoZip").setDisplayType('entry');
		sublist.addField("custpage_markforshiptocountry", "text", "Markfor Shiptocountry").setDisplayType('entry');
		sublist.addField("custpage_markforshiptoemail", "text", "Markfor Shiptoemail").setDisplayType('entry');
		sublist.addField("custpage_markforshiptophone", "text", "Makrfor ShiptoPhone").setDisplayType('entry');
		sublist.addField("custpage_markforshiptofax", "text", "Markfor ShiptoFax").setDisplayType('entry');
		sublist.addField("custpage_customerpo", "text", "CustomerPO#").setDisplayType('entry');
		sublist.addField("custpage_hostord", "text", "HostOrd#").setDisplayType('entry');
		sublist.addField("custpage_deliveryline", "text", "DeliveryLine#").setDisplayType('entry');
		sublist.addField("custpage_iteminternaid", "text", "Item InternalID").setDisplayType('entry');
		sublist.addField("custpage_sku", "text", "sku").setDisplayType('entry');
		sublist.addField("custpage_skudesc", "text", "SKU Desc").setDisplayType('entry');
		sublist.addField("custpage_skufamily", "text", "SKU Family").setDisplayType('entry');
		sublist.addField("custpage_skugroup", "text", "SKU Group").setDisplayType('entry');
		sublist.addField("custpage_skutype", "text", "SKU Type").setDisplayType('entry');
		sublist.addField("custpage_buyerpartnumber", "text", "BuyerPartNumber").setDisplayType('entry');
		sublist.addField("custpage_upc", "text", "UPC Code").setDisplayType('entry');
		sublist.addField("custpage_hazmat", "text", "HAZMAT Code").setDisplayType('entry');
		sublist.addField("custpage_natl", "text", "NATL_FRT_CODE").setDisplayType('entry');
		sublist.addField("custpage_tempcntrl", "text", "TempCntrl").setDisplayType('entry');
		sublist.addField("custpage_cntrlsubstance", "text", "CntrlSubstance").setDisplayType('entry');
		sublist.addField("custpage_batch", "select", "Batch ID","customrecord_ebiznet_batch_entry").setDisplayType('entry');
		sublist.addField("custpage_batchdelqty", "text", "Batch Delivery Qty").setDisplayType('entry');
		sublist.addField("custpage_serial", "select", "Serial Id","customrecord_ebiznetserialentry").setDisplayType('entry');
		sublist.addField("custpage_quantity", "text", "Quantity").setDisplayType('entry');
		sublist.addField("custpage_uom", "text", "UOM ID").setDisplayType('entry');
		sublist.addField("custpage_uomqty", "text", "UOM Qty").setDisplayType('entry');
		sublist.addField("custpage_skulength", "text", "SKU Length").setDisplayType('entry');
		sublist.addField("custpage_skuwidth", "text", "SKU Width").setDisplayType('entry');
		sublist.addField("custpage_skuweight", "text", "SKU Weight").setDisplayType('entry');
		sublist.addField("custpage_skuheight", "text", "SKU Height").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcustzero", "text", "Delivery item custfield_000").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcustone", "text", "Delivery item custfield_001").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcusttwo", "text", "Delivery item custfield_002").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcustthree", "text", "Delivery item custfield_003").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcustfour", "text", "Delivery item custfield_004").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcustfive", "text", "Delivery item custfield_005").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcustsix", "text", "Delivery item custfield_006").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcustseven", "text", "Delivery item custfield_007").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcusteight", "text", "Delivery item custfield_008").setDisplayType('entry');
		sublist.addField("custpage_deliveryitemcustnine", "text", "Delivery item custfield_009").setDisplayType('entry');
		sublist.addField("custpage_deliverycustzero", "text", "Delivery Custfield_000").setDisplayType('entry');
		sublist.addField("custpage_deliverycustone", "text", "Delivery Custfield_001").setDisplayType('entry');
		sublist.addField("custpage_deliverycusttwo", "text", "Delivery Custfield_002").setDisplayType('entry');
		sublist.addField("custpage_deliverycustthree", "text", "Delivery Custfield_003").setDisplayType('entry');
		sublist.addField("custpage_deliverycustfour", "text", "Delivery Custfield_004").setDisplayType('entry');
		sublist.addField("custpage_deliverycustfive", "text", "Delivery Custfield_005").setDisplayType('entry');
		sublist.addField("custpage_deliverycustsix", "text", "Delivery Custfield_006").setDisplayType('entry');
		sublist.addField("custpage_deliverycustseven", "text", "Delivery Custfield_007").setDisplayType('entry');
		sublist.addField("custpage_deliverycusteight", "text", "Delivery Custfield_008").setDisplayType('entry');
		sublist.addField("custpage_deliverycustnine", "text", "Delivery Custfield_009").setDisplayType('entry');
		sublist.addField("custpage_containercustzero", "text", "Container Custfield_000").setDisplayType('entry');
		sublist.addField("custpage_containercustone", "text", "Container Custfield_001").setDisplayType('entry');
		sublist.addField("custpage_containercusttwo", "text", "Container Custfield_002").setDisplayType('entry');
		sublist.addField("custpage_containercustthree", "text", "Container Custfield_003").setDisplayType('entry');
		sublist.addField("custpage_containercustfour", "text", "Container Custfield_004").setDisplayType('entry');
		sublist.addField("custpage_containercustfive", "text", "Container Custfield_005").setDisplayType('entry');
		sublist.addField("custpage_containercustsix", "text", "Container Custfield_006").setDisplayType('entry');
		sublist.addField("custpage_containercustseven", "text", "Container Custfield_007").setDisplayType('entry');
		sublist.addField("custpage_containercusteight", "text", "Container Custfield_008").setDisplayType('entry');
		sublist.addField("custpage_containercustnine", "text", "Container Custfield_009").setDisplayType('entry');
		sublist.addField("custpage_shipunitcustzero", "text", "Shipunit Custfield_000").setDisplayType('entry');
		sublist.addField("custpage_shipunitcustone", "text", "Shipunit Custfield_001").setDisplayType('entry');
		sublist.addField("custpage_shipunitcusttwo", "text", "Shipunit Custfield_002").setDisplayType('entry');
		sublist.addField("custpage_shipunitcustthree", "text", "Shipunit Custfield_003").setDisplayType('entry');
		sublist.addField("custpage_shipunitcustfour", "text", "Shipunit Custfield_004").setDisplayType('entry');
		sublist.addField("custpage_shipunitcustfive", "text", "Shipunit Custfield_005").setDisplayType('entry');
		sublist.addField("custpage_shipunitcustsix", "text", "Shipunit Custfield_006").setDisplayType('entry');
		sublist.addField("custpage_shipunitcustseven", "text", "Shipunit Custfield_007").setDisplayType('entry');
		sublist.addField("custpage_shipunitcusteight", "text", "Shipunit Custfield_008").setDisplayType('entry');
		sublist.addField("custpage_shipunitcustnine", "text", "Shipunit Custfield_009").setDisplayType('entry');
		sublist.addField("custpage_whlocation", "select", "WH Location","customrecord_ebiznet_location").setDisplayType('entry');
		sublist.addField("custpage_company", "select", "Company","customrecord_ebiznet_company").setDisplayType('entry');
		sublist.addField("custpage_account", "text", "Account#").setDisplayType('entry');
		sublist.addField("custpage_ebizstatus", "text", "eBiz Status Flag").setDisplayType('entry');
		sublist.addField("custpage_interfacestatus", "text", "Interface Status Flag").setDisplayType('entry');
		sublist.addField("custpage_host", "text", "Host ID").setDisplayType('entry');
		sublist.addField("custpage_currentdate", "text", "Current Date").setDisplayType('entry');
		sublist.addField("custpage_currenttime", "text", "Current Time").setDisplayType('entry');
		sublist.addField("custpage_confirmhostdate", "text", "Confirmed to Host Date").setDisplayType('entry');
		sublist.addField("custpage_confirmhosttime", "text", "Confirmed to Host Time").setDisplayType('entry');
		sublist.addField("custpage_errordesc", "text", "Error Description").setDisplayType('entry');
		sublist.addField("custpage_filename", "text", "FileName").setDisplayType('entry');
		sublist.addField("custpage_filecreateddate", "text", "File Created Date").setDisplayType('entry');
		sublist.addField("custpage_filecreatedtime", "text", "File Created Time").setDisplayType('entry');	
		sublist.addField("custpage_internalid", "text", "Internal Id").setDisplayType('hidden');
		 
		var trlrfields = ['name'];
		var trlrcolumns = nlapiLookupField('customrecord_ebiznet_trailer', vQbtrailer, trlrfields);
		var trailerName = trlrcolumns.name;
		nlapiLogExecution('ERROR', "trailerName", trailerName);

	

		var searchfilters = new Array();
		if (vQbtrailer != "" && vQbtrailer != null) {
			nlapiLogExecution('ERROR', 'vQbtrailer', vQbtrailer);
			searchfilters.push(new nlobjSearchFilter('custrecord_ebiztrailerno', null, 'is', trailerName));			 
		}

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_asnc_reference_id'));
		columns.push(new nlobjSearchColumn('custrecord_asnc_message_type'));
		columns.push(new nlobjSearchColumn('custrecord_asnc_creation_datetime'));
		columns.push(new nlobjSearchColumn('custrecord_sender_business_id'));
		columns.push(new nlobjSearchColumn('custrecord_recipient_business_id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiztrailerno'));
		columns.push(new nlobjSearchColumn('custrecord_ebizdepartment'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_class'));
		columns.push(new nlobjSearchColumn('custrecord_ebizvendor'));
		columns.push(new nlobjSearchColumn('custrecord_ebizshipunitid'));
		columns.push(new nlobjSearchColumn('custrecord_shipunitcharges'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznetshipunitlgth'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_width'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_weight'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_height'));
		columns.push(new nlobjSearchColumn('custrecord_shipunit_levelqty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_waybill_ref'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_carrierid'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_scac_code'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_carrier_name'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_s_ucc128'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_route'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_dest'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_vessel'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_voyage'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_bol'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_pronum'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiploc_id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiptoadd1'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiptoadd2'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiptocity'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiptostate'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiptozip'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiptocountry'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiptophone'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiptoemail'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shiptofax'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_000'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_001'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_002'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_003'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_004'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_005'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_006'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_007'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_008'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunitadd_custfld_009'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_expected_arrivaldate'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_planned_arrival_datetime'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_issue_period'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_000'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_001'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_002'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_003'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_004'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_005'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_006'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_007'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_008'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipping_custfield_009'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_containerlp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_containerlp_charges'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_trackingno'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_c_ucc128'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_payment_terms'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_length'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_width'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_height'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_weight'));
		columns.push(new nlobjSearchColumn('custrecord_container_levelqty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_deliveryid'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_order_type'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_order_priority'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_consigneeid'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_locationid'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billto_contactname'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billto_addr1'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billto_addr2'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billtocity'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billtostate'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billtozip'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billtocountry'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billtophone'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billtoemail'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billtofax'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_00')); 
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_01')); 
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_02')); 
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_03')); 
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_04')); 
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_05'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_06'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_07'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_08'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_09'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_billinginfo_custfield_00')); 
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shptocontactname')); 
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shipaddr1'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shipaddr2'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shiptocity'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shiptostate'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shiptozip'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shiptocountry'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shiptoemail'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shiptophone'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_markfor_shiptofax'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_customer_pono'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_host_ordno'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_deliveryline_no'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_item_internalid'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_asnc_sku'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_asnc_sku_desc'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_asnc_sku_family'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_asnc_sku_group'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_asnc_sku_type'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_buyer_partnumber'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_upc_code'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_hazmat_code'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_natl_frt_code'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_temp_cntrl'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_substance'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_batch_id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_batch_delivery_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_serial_id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_quantity'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_uom_id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_uom_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_length'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_width'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_weight'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_height'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield0')); 
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield1'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield2'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield3'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield4'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield5'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield6'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield7'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield8')); 
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_item_custfield9'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_000'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_001'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_002'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_003'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_004'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_005'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_006'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_007'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_008'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_delivery_custfield_009'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_000'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_001'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_002'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_003'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_004'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_005'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_006'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_007'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_008'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_container_custfield_009'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_000'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_001'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_002'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_003'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_004'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_005'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_006'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_007'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_008'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipunit_custfield_009'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_wh_location'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_asnc_company'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_account'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_status_flag'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_interface_statusflag'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_host_id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_current_date'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_current_time'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_confirmed_to_hostdate'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_confirmed_to_hosttime'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_error_description'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_filename'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_file_created_date'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_file_created_time'));

		var linesearchresults = nlapiSearchRecord('customrecord_ebiznetinterfaceasnc', null, searchfilters, columns);

		for (var i = 0; linesearchresults != null && i < linesearchresults.length; i++) {			 

			var ReferenceID	 = linesearchresults[i].getValue('custrecord_asnc_reference_id');
			var MessageType	 = linesearchresults[i].getValue('custrecord_asnc_message_type');
			var CreationDateTime = linesearchresults[i].getValue('custrecord_asnc_creation_datetime');
			var SenderBusinessSystemID	 = linesearchresults[i].getValue('custrecord_sender_business_id');
			var RecipientBusinessSystemID = linesearchresults[i].getValue('custrecord_recipient_business_id');
			var TrailerNo = linesearchresults[i].getValue('custrecord_ebiztrailerno');
			var Department = linesearchresults[i].getValue('custrecord_ebizdepartment');
			var asncclass	 = linesearchresults[i].getValue('custrecord_ebiz_class');
			var VendorID = linesearchresults[i].getValue('custrecord_ebizvendor');
			var ShipUnitID= linesearchresults[i].getValue('custrecord_ebizshipunitid');
			var ShipUnitCharges= linesearchresults[i].getValue('custrecord_shipunitcharges');
			var ShipunitLength	= linesearchresults[i].getValue('custrecord_ebiznetshipunitlgth');
			var ShipunitWidth	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_width');
			var ShipunitWeight	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_weight');
			var ShipunitHeight	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_height');
			var ShipUnitLevelQty= linesearchresults[i].getValue('custrecord_shipunit_levelqty');
			var Waybillref	= linesearchresults[i].getValue('custrecord_ebiz_waybill_ref');
			var CarrierID	= linesearchresults[i].getValue('custrecord_ebiz_carrierid');
			var SCACCode	= linesearchresults[i].getValue('custrecord_ebiz_scac_code');
			var CarrierName	= linesearchresults[i].getValue('custrecord_ebiz_carrier_name');
			var S_UCC128	= linesearchresults[i].getValue('custrecord_ebiz_s_ucc128');
			var Route		= linesearchresults[i].getValue('custrecord_ebiz_route');
			var Dest		= linesearchresults[i].getValue('custrecord_ebiz_dest');
			var Vessel		= linesearchresults[i].getValue('custrecord_ebiz_vessel');
			var Voyage		= linesearchresults[i].getValue('custrecord_ebiz_voyage');
			var BOL		= linesearchresults[i].getValue('custrecord_ebiz_bol');
			var Pronum		= linesearchresults[i].getValue('custrecord_ebiz_pronum');
			var ShippingLocationID= linesearchresults[i].getValue('custrecord_ebiz_shiploc_id');
			var ShiptoAddr1		= linesearchresults[i].getValue('custrecord_ebiz_shiptoadd1');
			var ShiptoAddr2	= linesearchresults[i].getValue('custrecord_ebiz_shiptoadd2');
			var ShiptoCity	= linesearchresults[i].getValue('custrecord_ebiz_shiptocity');
			var ShiptoState	= linesearchresults[i].getValue('custrecord_ebiz_shiptostate');
			var ShiptoZip	= linesearchresults[i].getValue('custrecord_ebiz_shiptozip');
			var ShiptoCountry	= linesearchresults[i].getValue('custrecord_ebiz_shiptocountry');
			var ShiptoPhone	= linesearchresults[i].getValue('custrecord_ebiz_shiptophone');
			var ShiptoEmail	= linesearchresults[i].getValue('custrecord_ebiz_shiptoemail');
			var ShiptoFax	= linesearchresults[i].getValue('custrecord_ebiz_shiptofax');
			var ShipunitaddrCustfiled_000= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_000');
			var ShipunitaddrCustfiled_001= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_001');
			var ShipunitaddrCustfiled_002= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_002');
			var ShipunitaddrCustfiled_003= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_003');
			var ShipunitaddrCustfiled_004= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_004');
			var ShipunitaddrCustfiled_005= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_005');
			var ShipunitaddrCustfiled_006= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_006');
			var ShipunitaddrCustfiled_007= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_007');
			var ShipunitaddrCustfiled_008= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_008');
			var ShipunitaddrCustfiled_009= linesearchresults[i].getValue('custrecord_ebiz_shipunitadd_custfld_009');
			var ExpectedArrivalDate	= linesearchresults[i].getValue('custrecord_ebiz_expected_arrivaldate');
			var PlannedArrivalDateTime	 = linesearchresults[i].getValue('custrecord_ebiz_planned_arrival_datetime');
			var IssuePeriod			 = linesearchresults[i].getValue('custrecord_ebiz_issue_period');
			var ShippingCustfield_000 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_000');
			var ShippingCustfield_001 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_001');
			var ShippingCustfield_002 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_002');
			var ShippingCustfield_003 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_003');
			var ShippingCustfield_004 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_004');
			var ShippingCustfield_005 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_005');
			var ShippingCustfield_006 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_006');
			var ShippingCustfield_007 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_007');
			var ShippingCustfield_008 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_008');
			var ShippingCustfield_009 = linesearchresults[i].getValue('custrecord_ebiz_shipping_custfield_009');
			var ContainerLP		 = linesearchresults[i].getValue('custrecord_ebiz_containerlp');
			var ContainerLPCharges	 = linesearchresults[i].getValue('custrecord_ebiz_containerlp_charges');
			var ContainerTrackingNo	 = linesearchresults[i].getValue('custrecord_ebiz_container_trackingno');
			var C_UCC128= linesearchresults[i].getValue('custrecord_ebiz_c_ucc128');
			var PaymentTerms	 = linesearchresults[i].getValue('custrecord_ebiz_payment_terms');
			var ContainerLength = linesearchresults[i].getValue('custrecord_ebiz_container_length');
			var ContainerWidth	 = linesearchresults[i].getValue('custrecord_ebiz_container_width');
			var ContainerHeight = linesearchresults[i].getValue('custrecord_ebiz_container_height');
			var ContainerWeight = linesearchresults[i].getValue('custrecord_ebiz_container_weight');
			var ContainerLevelQty = linesearchresults[i].getValue('custrecord_container_levelqty');
			var DeliveryID= linesearchresults[i].getValue('custrecord_ebiz_deliveryid');
			var OrderType= linesearchresults[i].getValue('custrecord_ebiz_order_type');
			var MARKFOR= linesearchresults[i].getValue('custrecord_ebiz_markfor');
			var OrderPriority	 = linesearchresults[i].getValue('custrecord_ebiz_order_priority');
			var ConsigneeID	 = linesearchresults[i].getValue('custrecord_ebiz_consigneeid');
			var BillinginfoLocationID = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_locationid');
			var BilltoContactName	 = linesearchresults[i].getValue('custrecord_ebiz_billto_contactname');
			var BilltoAddr1		 = linesearchresults[i].getValue('custrecord_ebiz_billto_addr1');
			var BilltoAddr2	 = linesearchresults[i].getValue('custrecord_ebiz_billto_addr2');
			var BilltoCity	 = linesearchresults[i].getValue('custrecord_ebiz_billtocity');
			var BilltoState	 = linesearchresults[i].getValue('custrecord_ebiz_billtostate');
			var BilltoZip	 = linesearchresults[i].getValue('custrecord_ebiz_billtozip');
			var BilltoCountry	 = linesearchresults[i].getValue('custrecord_ebiz_billtocountry');
			var BilltoPhone	 = linesearchresults[i].getValue('custrecord_ebiz_billtophone');
			var Billtoemail	 = linesearchresults[i].getValue('custrecord_ebiz_billtoemail');
			var BilltoFax	 = linesearchresults[i].getValue('custrecord_ebiz_billtofax');
			var BillinginfoCustfield_000 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_00'); 
			var BillinginfoCustfield_001 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_01'); 
			var BillinginfoCustfield_002 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_02'); 
			var BillinginfoCustfield_003 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_03'); 
			var BillinginfoCustfield_004 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_04'); 
			var BillinginfoCustfield_005 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_05');
			var BillinginfoCustfield_006 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_06');
			var BillinginfoCustfield_007 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_07');
			var BillinginfoCustfield_008 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_08');
			var BillinginfoCustfield_009 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_09');
			var MarkforLocationId	 = linesearchresults[i].getValue('custrecord_ebiz_billinginfo_custfield_00'); 
			var Markforshiptocontactname = linesearchresults[i].getValue('custrecord_ebiz_markfor_shptocontactname'); 
			var MarkforShiptoAddr1	 = linesearchresults[i].getValue('custrecord_ebiz_markfor_shipaddr1');
			var MarkforShiptoAddr2	 = linesearchresults[i].getValue('custrecord_ebiz_markfor_shipaddr2');
			var MarkforShiptoCity	 = linesearchresults[i].getValue('custrecord_ebiz_markfor_shiptocity');
			var MarkforShiptoState	 = linesearchresults[i].getValue('custrecord_ebiz_markfor_shiptostate');
			var MarkforShiptoZip	 = linesearchresults[i].getValue('custrecord_ebiz_markfor_shiptozip');
			var MarkforShiptocountry	 = linesearchresults[i].getValue('custrecord_ebiz_markfor_shiptocountry');
			var MarkforShiptoemail	 = linesearchresults[i].getValue('custrecord_ebiz_markfor_shiptoemail');
			var MakrforShiptoPhone	 = linesearchresults[i].getValue('custrecord_ebiz_markfor_shiptophone');
			var MarkforShiptoFax	 = linesearchresults[i].getValue('custrecord_ebiz_markfor_shiptofax');
			var CustomerPO	 = linesearchresults[i].getValue('custrecord_ebiz_customer_pono');
			var HostOrd	 = linesearchresults[i].getValue('custrecord_ebiz_host_ordno');
			var DeliveryLine	 = linesearchresults[i].getValue('custrecord_ebiz_deliveryline_no');
			var ItemInternalID	 = linesearchresults[i].getValue('custrecord_ebiz_item_internalid');
			var SKU		 = linesearchresults[i].getValue('custrecord_ebiz_asnc_sku');
			var SKUDesc	 = linesearchresults[i].getValue('custrecord_ebiz_asnc_sku_desc');
			var SKUFamily	 = linesearchresults[i].getValue('custrecord_ebiz_asnc_sku_family');
			var SKUGroup	 = linesearchresults[i].getValue('custrecord_ebiz_asnc_sku_group');
			var SKUType	 = linesearchresults[i].getValue('custrecord_ebiz_asnc_sku_type');
			var BuyerPartNumber	 = linesearchresults[i].getValue('custrecord_ebiz_buyer_partnumber');
			var UPCCode	 = linesearchresults[i].getValue('custrecord_ebiz_upc_code');
			var HAZMATCode	 = linesearchresults[i].getValue('custrecord_ebiz_hazmat_code');
			var NATL_FRT_CODE	 = linesearchresults[i].getValue('custrecord_ebiz_natl_frt_code');
			var TempCntrl	 = linesearchresults[i].getValue('custrecord_ebiz_temp_cntrl');
			var CntrlSubstance	 = linesearchresults[i].getValue('custrecord_ebiz_cntrl_substance');
			var BatchID		 = linesearchresults[i].getValue('custrecord_ebiz_batch_id');
			var BatchDeliveryQty	 = linesearchresults[i].getValue('custrecord_ebiz_batch_delivery_qty');
			var SerialId	= linesearchresults[i].getValue('custrecord_ebiz_serial_id');
			var Quantity	= linesearchresults[i].getValue('custrecord_ebiz_quantity');
			var UOMID		= linesearchresults[i].getValue('custrecord_ebiz_uom_id');
			var UOMQty		= linesearchresults[i].getValue('custrecord_ebiz_uom_qty');
			var SKULength	= linesearchresults[i].getValue('custrecord_ebiz_sku_length');
			var SKUWidth	= linesearchresults[i].getValue('custrecord_ebiz_sku_width');
			var SKUWeight	= linesearchresults[i].getValue('custrecord_ebiz_sku_weight');
			var SKUHeight	 = linesearchresults[i].getValue('custrecord_ebiz_sku_height');
			var Deliveryitemcustfield_000= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield0'); 
			var Deliveryitemcustfield_001= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield1');
			var Deliveryitemcustfield_002= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield2');
			var Deliveryitemcustfield_003= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield3');
			var Deliveryitemcustfield_004= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield4');
			var Deliveryitemcustfield_005= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield5');
			var Deliveryitemcustfield_006= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield6');
			var Deliveryitemcustfield_007= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield7');
			var Deliveryitemcustfield_008= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield8'); 
			var Deliveryitemcustfield_009= linesearchresults[i].getValue('custrecord_ebiz_delivery_item_custfield9');
			var DeliveryCustfield_000	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_000');
			var DeliveryCustfield_001	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_001');
			var DeliveryCustfield_002	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_002');
			var DeliveryCustfield_003	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_003');
			var DeliveryCustfield_004	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_004');
			var DeliveryCustfield_005	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_005');
			var DeliveryCustfield_006	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_006');
			var DeliveryCustfield_007	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_007');
			var DeliveryCustfield_008	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_008');
			var DeliveryCustfield_009	= linesearchresults[i].getValue('custrecord_ebiz_delivery_custfield_009');
			var ContainerCustfield_000	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_000');
			var ContainerCustfield_001	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_001');
			var ContainerCustfield_002	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_002');
			var ContainerCustfield_003	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_003');
			var ContainerCustfield_004	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_004');
			var ContainerCustfield_005	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_005');
			var ContainerCustfield_006	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_006');
			var ContainerCustfield_007	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_007');
			var ContainerCustfield_008	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_008');
			var ContainerCustfield_009	= linesearchresults[i].getValue('custrecord_ebiz_container_custfield_009');
			var ShipunitCustfield_000	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_000');
			var ShipunitCustfield_001	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_001');
			var ShipunitCustfield_002	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_002');
			var ShipunitCustfield_003	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_003');
			var ShipunitCustfield_004	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_004');
			var ShipunitCustfield_005	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_005');
			var ShipunitCustfield_006	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_006');
			var ShipunitCustfield_007	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_007');
			var ShipunitCustfield_008	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_008');
			var ShipunitCustfield_009	= linesearchresults[i].getValue('custrecord_ebiz_shipunit_custfield_009');
			var WHLocation	= linesearchresults[i].getValue('custrecord_ebiz_wh_location');
			var Company		= linesearchresults[i].getValue('custrecord_ebiz_asnc_company');
			var Account	= linesearchresults[i].getValue('custrecord_ebiz_account');
			var eBizStatusFlag= linesearchresults[i].getValue('custrecord_ebiz_status_flag');
			var InterfaceStatusFlag	= linesearchresults[i].getValue('custrecord_ebiz_interface_statusflag');
			var HostID		= linesearchresults[i].getValue('custrecord_ebiz_host_id');
			var CurrentDate	= linesearchresults[i].getValue('custrecord_ebiz_current_date');
			var CurrentTime	= linesearchresults[i].getValue('custrecord_ebiz_current_time');
			var ConfirmedtoHostDate = linesearchresults[i].getValue('custrecord_ebiz_confirmed_to_hostdate');
			var ConfirmedtoHostTime = linesearchresults[i].getValue('custrecord_ebiz_confirmed_to_hosttime');
			var ErrorDescription	 = linesearchresults[i].getValue('custrecord_ebiz_error_description');
			var FileName		 = linesearchresults[i].getValue('custrecord_ebiz_filename');
			var FileCreatedDate	 = linesearchresults[i].getValue('custrecord_ebiz_file_created_date');
			var FileCreatedTime	 = linesearchresults[i].getValue('custrecord_ebiz_file_created_time');



			form.getSubList('custpage_items').setLineItemValue('custpage_reference', i + 1, TrailerNo);
			form.getSubList('custpage_items').setLineItemValue('custpage_messagetype', i + 1, MessageType);
			form.getSubList('custpage_items').setLineItemValue('custpage_creationdatetime', i + 1, CreationDateTime);
			form.getSubList('custpage_items').setLineItemValue('custpage_senderbusiness', i + 1, SenderBusinessSystemID);
			form.getSubList('custpage_items').setLineItemValue('custpage_recipientbusiness	 ', i + 1, RecipientBusinessSystemID);
			form.getSubList('custpage_items').setLineItemValue('custpage_trailer', i + 1, TrailerNo);
			form.getSubList('custpage_items').setLineItemValue('custpage_department', i + 1, Department);
			form.getSubList('custpage_items').setLineItemValue('custpage_class', i + 1, asncclass);
			form.getSubList('custpage_items').setLineItemValue('custpage_vendorid', i + 1, VendorID);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitid', i + 1, ShipUnitID);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcharges', i + 1, ShipUnitCharges);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitlengths', i + 1, ShipunitLength);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitwidth', i + 1, ShipunitWidth);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitweight', i + 1, ShipunitWeight);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitheight', i + 1, ShipunitHeight);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitlevelqty', i + 1, ShipUnitLevelQty);
			form.getSubList('custpage_items').setLineItemValue('custpage_waybillref', i + 1, Waybillref);
			form.getSubList('custpage_items').setLineItemValue('custpage_carrierid', i + 1, CarrierID);
			form.getSubList('custpage_items').setLineItemValue('custpage_scaccode', i + 1, SCACCode);
			form.getSubList('custpage_items').setLineItemValue('custpage_carriername', i + 1, CarrierName);
			form.getSubList('custpage_items').setLineItemValue('custpage_succ', i + 1, S_UCC128);
			form.getSubList('custpage_items').setLineItemValue('custpage_route', i + 1, Route);
			form.getSubList('custpage_items').setLineItemValue('custpage_dest', i + 1, Dest);
			form.getSubList('custpage_items').setLineItemValue('custpage_vessel', i + 1, Vessel);
			form.getSubList('custpage_items').setLineItemValue('custpage_voyage', i + 1, Voyage);
			form.getSubList('custpage_items').setLineItemValue('custpage_bol', i + 1, BOL);
			form.getSubList('custpage_items').setLineItemValue('custpage_pronum', i + 1, Pronum);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippinglocation', i + 1, ShippingLocationID);
			form.getSubList('custpage_items').setLineItemValue('custpage_shiptoaddrone', i + 1, ShiptoAddr1);
			form.getSubList('custpage_items').setLineItemValue('custpage_shiptoaddrtwo', i + 1, ShiptoAddr2);
			form.getSubList('custpage_items').setLineItemValue('custpage_shiptocity', i + 1, ShiptoCity);
			form.getSubList('custpage_items').setLineItemValue('custpage_shiptostate', i + 1, ShiptoState);
			form.getSubList('custpage_items').setLineItemValue('custpage_shiptozip', i + 1, ShiptoZip);
			form.getSubList('custpage_items').setLineItemValue('custpage_shiptocountry', i + 1, ShiptoCountry);
			form.getSubList('custpage_items').setLineItemValue('custpage_shiptophone', i + 1, ShiptoPhone);
			form.getSubList('custpage_items').setLineItemValue('custpage_shiptoemail', i + 1, ShiptoEmail);
			form.getSubList('custpage_items').setLineItemValue('custpage_shiptofax', i + 1, ShiptoFax);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcustzero', i + 1, ShipunitaddrCustfiled_000);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcustone', i + 1, ShipunitaddrCustfiled_001);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcusttwo', i + 1, ShipunitaddrCustfiled_002);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcustthree', i + 1, ShipunitaddrCustfiled_003);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcustfour', i + 1, ShipunitaddrCustfiled_004);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcustfive', i + 1, ShipunitaddrCustfiled_005);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcustsix', i + 1, ShipunitaddrCustfiled_006);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcustseven', i + 1, ShipunitaddrCustfiled_007);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcusteight', i + 1, ShipunitaddrCustfiled_008);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitaddrcustnine', i + 1, ShipunitaddrCustfiled_009);
			form.getSubList('custpage_items').setLineItemValue('custpage_pectedarrival', i + 1, ExpectedArrivalDate);
			form.getSubList('custpage_items').setLineItemValue('custpage_plannedarrival', i + 1, PlannedArrivalDateTime);
			form.getSubList('custpage_items').setLineItemValue('custpage_issueperiod  ', i + 1, IssuePeriod);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcustzero', i + 1, ShippingCustfield_000);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcustone', i + 1, ShippingCustfield_001);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcusttwo', i + 1, ShippingCustfield_002);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcustthree', i + 1, ShippingCustfield_003);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcustfour', i + 1, ShippingCustfield_004);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcustfive', i + 1, ShippingCustfield_005);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcustsix', i + 1, ShippingCustfield_006);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcustseven', i + 1, ShippingCustfield_007);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcusteight', i + 1, ShippingCustfield_008);
			form.getSubList('custpage_items').setLineItemValue('custpage_shippingcustnine', i + 1, ShippingCustfield_009);
			form.getSubList('custpage_items').setLineItemValue('custpage_containerlp 	 ', i + 1, ContainerLP);
			form.getSubList('custpage_items').setLineItemValue('custpage_containerlpcharge', i + 1, ContainerLPCharges);
			form.getSubList('custpage_items').setLineItemValue('custpage_containertracking', i + 1, ContainerTrackingNo);
			form.getSubList('custpage_items').setLineItemValue('custpage_cucc', i + 1, C_UCC128);
			form.getSubList('custpage_items').setLineItemValue('custpage_paymentterms', i + 1, PaymentTerms);
			form.getSubList('custpage_items').setLineItemValue('custpage_containerlength', i + 1, ContainerLength);
			form.getSubList('custpage_items').setLineItemValue('custpage_containerwidth', i + 1, ContainerWidth);
			form.getSubList('custpage_items').setLineItemValue('custpage_containerheight', i + 1, ContainerHeight);
			form.getSubList('custpage_items').setLineItemValue('custpage_containerweight', i + 1, ContainerWeight);
			form.getSubList('custpage_items').setLineItemValue('custpage_containerlevelqty', i + 1, ContainerLevelQty);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryid', i + 1, DeliveryID);
			form.getSubList('custpage_items').setLineItemValue('custpage_ordertype', i + 1, OrderType);
			form.getSubList('custpage_items').setLineItemValue('custpage_markfor', i + 1, MARKFOR);
			form.getSubList('custpage_items').setLineItemValue('custpage_orderpriority', i + 1, OrderPriority);
			form.getSubList('custpage_items').setLineItemValue('custpage_consigneeid', i + 1, ConsigneeID);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfolocatio', i + 1, BillinginfoLocationID);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtocontactname', i + 1, BilltoContactName);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtoaddrone', i + 1, BilltoAddr1);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtoaddrtwo', i + 1, BilltoAddr2);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtocity', i + 1, BilltoCity);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtostate', i + 1, BilltoState);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtozip', i + 1, BilltoZip);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtocountr', i + 1, BilltoCountry);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtophone', i + 1, BilltoPhone);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtoemail', i + 1, Billtoemail);
			form.getSubList('custpage_items').setLineItemValue('custpage_billtofax', i + 1, BilltoFax);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocustzero', i + 1, BillinginfoCustfield_000);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocustone', i + 1, BillinginfoCustfield_001);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocusttwo', i + 1, BillinginfoCustfield_002);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocustthree', i + 1, BillinginfoCustfield_003);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocustfour', i + 1, BillinginfoCustfield_004);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocustfive', i + 1, BillinginfoCustfield_005);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocustsix', i + 1, BillinginfoCustfield_006);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocustseven', i + 1, BillinginfoCustfield_007);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocusteight', i + 1, BillinginfoCustfield_008);
			form.getSubList('custpage_items').setLineItemValue('custpage_billinginfocustnine', i + 1, BillinginfoCustfield_009);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforlocation', i + 1, MarkforLocationId);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptoconta', i + 1, Markforshiptocontactname);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptoaddrone', i + 1, MarkforShiptoAddr1);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptoaddrtwo', i + 1, MarkforShiptoAddr2);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptocity', i + 1, MarkforShiptoCity);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptostate', i + 1, MarkforShiptoState);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptozip', i + 1, MarkforShiptoZip);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptocount', i + 1, MarkforShiptocountry);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptoemail', i + 1, MarkforShiptoemail);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptophone', i + 1, MakrforShiptoPhone);
			form.getSubList('custpage_items').setLineItemValue('custpage_markforshiptofax	 ', i + 1, MarkforShiptoFax);
			form.getSubList('custpage_items').setLineItemValue('custpage_customerpo', i + 1, CustomerPO);
			form.getSubList('custpage_items').setLineItemValue('custpage_hostord', i + 1, HostOrd);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryline', i + 1, DeliveryLine);
			form.getSubList('custpage_items').setLineItemValue('custpage_iteminternaid ', i + 1, ItemInternalID);
			form.getSubList('custpage_items').setLineItemValue('custpage_sku', i + 1, SKU);
			form.getSubList('custpage_items').setLineItemValue('custpage_skudesc', i + 1, SKUDesc);
			form.getSubList('custpage_items').setLineItemValue('custpage_skufamily', i + 1, SKUFamily);
			form.getSubList('custpage_items').setLineItemValue('custpage_skugroup', i + 1, SKUGroup);
			form.getSubList('custpage_items').setLineItemValue('custpage_skutype 	 ', i + 1, SKUType);
			form.getSubList('custpage_items').setLineItemValue('custpage_buyerpartnumber', i + 1, BuyerPartNumber);
			form.getSubList('custpage_items').setLineItemValue('custpage_upc', i + 1, UPCCode);
			form.getSubList('custpage_items').setLineItemValue('custpage_hazmat', i + 1, HAZMATCode);
			form.getSubList('custpage_items').setLineItemValue('custpage_natl', i + 1, NATL_FRT_CODE);
			form.getSubList('custpage_items').setLineItemValue('custpage_tempcntrl', i + 1, TempCntrl);
			form.getSubList('custpage_items').setLineItemValue('custpage_cntrlsubstance', i + 1, CntrlSubstance);
			form.getSubList('custpage_items').setLineItemValue('custpage_batch', i + 1, BatchID);
			form.getSubList('custpage_items').setLineItemValue('custpage_batchdelqty ', i + 1, BatchDeliveryQty);
			form.getSubList('custpage_items').setLineItemValue('custpage_serial', i + 1, SerialId);
			form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, Quantity);
			form.getSubList('custpage_items').setLineItemValue('custpage_uom', i + 1, UOMID);
			form.getSubList('custpage_items').setLineItemValue('custpage_uomqty', i + 1, UOMQty);
			form.getSubList('custpage_items').setLineItemValue('custpage_skulength', i + 1, SKULength);
			form.getSubList('custpage_items').setLineItemValue('custpage_skuwidth', i + 1, SKUWidth);
			form.getSubList('custpage_items').setLineItemValue('custpage_skuweight', i + 1, SKUWeight);
			form.getSubList('custpage_items').setLineItemValue('custpage_skuheight', i + 1, SKUHeight);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcustzero', i + 1, Deliveryitemcustfield_000);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcustone', i + 1, Deliveryitemcustfield_001);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcusttwo', i + 1, Deliveryitemcustfield_002);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcustthree', i + 1, Deliveryitemcustfield_003);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcustfour', i + 1, Deliveryitemcustfield_004);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcustfive', i + 1, Deliveryitemcustfield_005);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcustsix', i + 1, Deliveryitemcustfield_006);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcustseven', i + 1, Deliveryitemcustfield_007);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcust8', i + 1, Deliveryitemcustfield_008);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliveryitemcust9', i + 1, Deliveryitemcustfield_009);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycustzero', i + 1, DeliveryCustfield_000);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycustone', i + 1, DeliveryCustfield_001);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycusttwo', i + 1, DeliveryCustfield_002);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycustthree', i + 1, DeliveryCustfield_003);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycustfour', i + 1, DeliveryCustfield_004);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycustfive', i + 1, DeliveryCustfield_005);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycustsix', i + 1, DeliveryCustfield_006);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycustseven', i + 1, DeliveryCustfield_007);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycust8', i + 1, DeliveryCustfield_008);
			form.getSubList('custpage_items').setLineItemValue('custpage_deliverycust9', i + 1, DeliveryCustfield_009);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercustzero', i + 1, ContainerCustfield_000);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercustone', i + 1, ContainerCustfield_001);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercusttwo', i + 1, ContainerCustfield_002);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercustthree', i + 1, ContainerCustfield_003);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercustfour', i + 1, ContainerCustfield_004);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercustfive', i + 1, ContainerCustfield_005);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercustsix', i + 1, ContainerCustfield_006);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercustseven', i + 1, ContainerCustfield_007);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercust8', i + 1, ContainerCustfield_008);
			form.getSubList('custpage_items').setLineItemValue('custpage_containercust9', i + 1, ContainerCustfield_009);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcustzero', i + 1, ShipunitCustfield_000);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcustone', i + 1, ShipunitCustfield_001);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcusttwo', i + 1, ShipunitCustfield_002);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcustthree', i + 1, ShipunitCustfield_003);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcustfour', i + 1, ShipunitCustfield_004);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcustfive', i + 1, ShipunitCustfield_005);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcustsix', i + 1, ShipunitCustfield_006);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcustseven', i + 1, ShipunitCustfield_007);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcust8', i + 1, ShipunitCustfield_008);
			form.getSubList('custpage_items').setLineItemValue('custpage_shipunitcust9', i + 1, ShipunitCustfield_009);
			form.getSubList('custpage_items').setLineItemValue('custpage_whlocation', i + 1, WHLocation);
			form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1, Company);
			form.getSubList('custpage_items').setLineItemValue('custpage_account', i + 1, Account);
			form.getSubList('custpage_items').setLineItemValue('custpage_ebizstatus', i + 1, eBizStatusFlag);
			form.getSubList('custpage_items').setLineItemValue('custpage_interfacestatus', i + 1, InterfaceStatusFlag);
			form.getSubList('custpage_items').setLineItemValue('custpage_host', i + 1, HostID);
			form.getSubList('custpage_items').setLineItemValue('custpage_currentdate', i + 1, CurrentDate);
			form.getSubList('custpage_items').setLineItemValue('custpage_currenttime', i + 1, CurrentTime);
			form.getSubList('custpage_items').setLineItemValue('custpage_confirmhostdate', i + 1, ConfirmedtoHostDate);
			form.getSubList('custpage_items').setLineItemValue('custpage_confirmhosttime', i + 1, ConfirmedtoHostTime);
			form.getSubList('custpage_items').setLineItemValue('custpage_errordesc', i + 1, ErrorDescription);
			form.getSubList('custpage_items').setLineItemValue('custpage_filename', i + 1, FileName);
			form.getSubList('custpage_items').setLineItemValue('custpage_filecreateddate', i + 1, FileCreatedDate);
			form.getSubList('custpage_items').setLineItemValue('custpage_filecreatedtime', i + 1, FileCreatedTime);
			form.getSubList('custpage_items').setLineItemValue('custpage_internalid', i + 1, linesearchresults[i].getId());

		}
		nlapiLogExecution('ERROR','before performUpdateOperations','done');
		performUpdateOperations(request,form);
		var button = form.addSubmitButton('Submit');
		response.writePage(form);
		nlapiLogExecution('ERROR','last','done');
	}
}


function performUpdateOperations(request,form)
{
	nlapiLogExecution('ERROR','into performUpdateOperations','done');
	try
	{
		var LineCount = request.getLineItemCount('custpage_items');	
		nlapiLogExecution('ERROR','LineCount',LineCount);
		for(var s=1; s <= LineCount; s++){
			nlapiLogExecution('ERROR','into for loop','done');
			flag = request.getLineItemValue('custpage_items', 'custpage_select', s);
			nlapiLogExecution('ERROR','flag',flag);
			if(flag == "T"){
				nlapiLogExecution('ERROR','into select','done');

				internalId = request.getLineItemValue('custpage_items', 'custpage_internalid', s);
				var reference       = request.getLineItemValue('custpage_items', 'custpage_reference', s);
				var messagetype      = request.getLineItemValue('custpage_items', 'custpage_messagetype', s);
				var creationdatetime = request.getLineItemValue('custpage_items', 'custpage_creationdatetime', s);
				var senderbusiness      = request.getLineItemValue('custpage_items', 'custpage_senderbusiness', s);
				var recipientbusiness      = request.getLineItemValue('custpage_items', 'custpage_recipientbusiness', s);
				var trailer      = request.getLineItemValue('custpage_items', 'custpage_trailer', s);
				var department      = request.getLineItemValue('custpage_items', 'custpage_department', s);
				var asncclass      = request.getLineItemValue('custpage_items', 'custpage_class', s);
				var vendorid      = request.getLineItemValue('custpage_items', 'custpage_vendorid', s);
				var shipunitid      = request.getLineItemValue('custpage_items', 'custpage_shipunitid', s);
				var shipunitcharges           = request.getLineItemValue('custpage_items', 'custpage_shipunitcharges', s);
				var shipunitlengths           = request.getLineItemValue('custpage_items', 'custpage_shipunitlengths', s);
				var shipunitwidth       = request.getLineItemValue('custpage_items', 'custpage_shipunitwidth', s);
				var shipunitweight      = request.getLineItemValue('custpage_items', 'custpage_shipunitweight', s);
				var shipunitheight      = request.getLineItemValue('custpage_items', 'custpage_shipunitheight', s);
				var shipunitlevelqty      = request.getLineItemValue('custpage_items', 'custpage_shipunitlevelqty', s);
				var waybillref      = request.getLineItemValue('custpage_items', 'custpage_waybillref', s);
				var carrierid      = request.getLineItemValue('custpage_items', 'custpage_carrierid', s);
				var scaccode      = request.getLineItemValue('custpage_items', 'custpage_scaccode', s);
				var carriername      = request.getLineItemValue('custpage_items', 'custpage_carriername', s);
				var succ      = request.getLineItemValue('custpage_items', 'custpage_succ', s);
				var route      = request.getLineItemValue('custpage_items', 'custpage_route', s);
				var dest     = request.getLineItemValue('custpage_items', 'custpage_dest', s);
				var vessel   = request.getLineItemValue('custpage_items', 'custpage_vessel', s);
				var voyage   = request.getLineItemValue('custpage_items', 'custpage_voyage', s);
				var bol      = request.getLineItemValue('custpage_items', 'custpage_bol', s);
				var pronum   = request.getLineItemValue('custpage_items', 'custpage_pronum', s);
				var shippinglocation  = request.getLineItemValue('custpage_items', 'custpage_shippinglocation', s);
				var shiptoaddrone     = request.getLineItemValue('custpage_items', 'custpage_shiptoaddrone', s);
				var shiptoaddrtwo     = request.getLineItemValue('custpage_items', 'custpage_shiptoaddrtwo', s);
				var shiptocity     = request.getLineItemValue('custpage_items', 'custpage_shiptocity', s);
				var shiptostate    = request.getLineItemValue('custpage_items', 'custpage_shiptostate', s);
				var shiptozip      = request.getLineItemValue('custpage_items', 'custpage_shiptozip', s);
				var shiptocountry  = request.getLineItemValue('custpage_items', 'custpage_shiptocountry', s);
				var shiptophone    = request.getLineItemValue('custpage_items', 'custpage_shiptophone', s);
				var shiptoemail    = request.getLineItemValue('custpage_items', 'custpage_shiptoemail', s);
				var shiptofax      = request.getLineItemValue('custpage_items', 'custpage_shiptofax', s);
				var shipunitaddrcustzero = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcustzero', s);
				var shipunitaddrcustone = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcustone', s);
				var shipunitaddrcusttwo = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcusttwo', s);
				var shipunitaddrcustthree = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcustthree', s);
				var shipunitaddrcustfour = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcustfour', s);
				var shipunitaddrcustfive = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcustfive', s);
				var shipunitaddrcustsix = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcustsix', s);
				var shipunitaddrcustseven  = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcustseven', s);
				var shipunitaddrcusteigh      = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcusteigh', s);
				var shipunitaddrcustnine = request.getLineItemValue('custpage_items', 'custpage_shipunitaddrcustnine', s);
				var expectedarrival           = request.getLineItemValue('custpage_items', 'custpage_expectedarrival', s);
				var plannedarrival      = request.getLineItemValue('custpage_items', 'custpage_plannedarrival', s);
				var issueperiod      = request.getLineItemValue('custpage_items', 'custpage_issueperiod', s);
				var shippingcustzero     = request.getLineItemValue('custpage_items', 'custpage_shippingcustzero', s);
				var shippingcustone      = request.getLineItemValue('custpage_items', 'custpage_shippingcustone', s);
				var shippingcusttwo      = request.getLineItemValue('custpage_items', 'custpage_shippingcusttwo', s);
				var shippingcustthree    = request.getLineItemValue('custpage_items', 'custpage_shippingcustthree', s);
				var shippingcustfour     = request.getLineItemValue('custpage_items', 'custpage_shippingcustfour', s);
				var shippingcustfive     = request.getLineItemValue('custpage_items', 'custpage_shippingcustfive', s);
				var shippingcustsix      = request.getLineItemValue('custpage_items', 'custpage_shippingcustsix', s);
				var shippingcustseven    = request.getLineItemValue('custpage_items', 'custpage_shippingcustseven', s);
				var shippingcusteight    = request.getLineItemValue('custpage_items', 'custpage_shippingcusteight', s);
				var shippingcustnine     = request.getLineItemValue('custpage_items', 'custpage_shippingcustnine', s);
				var containerlp     = request.getLineItemValue('custpage_items', 'custpage_containerlp', s);
				var containerlpcharge    = request.getLineItemValue('custpage_items', 'custpage_containerlpcharge', s);
				var containertracking    = request.getLineItemValue('custpage_items', 'custpage_containertracking', s);
				var cucc      = request.getLineItemValue('custpage_items', 'custpage_cucc', s);
				var paymentterms      = request.getLineItemValue('custpage_items', 'custpage_paymentterms', s);
				var containerlength      = request.getLineItemValue('custpage_items', 'custpage_containerlength', s);
				var containerwidth      = request.getLineItemValue('custpage_items', 'custpage_containerwidth', s);
				var containerheight      = request.getLineItemValue('custpage_items', 'custpage_containerheight', s);
				var containerweight      = request.getLineItemValue('custpage_items', 'custpage_containerweight', s);
				var containerlevelqty    = request.getLineItemValue('custpage_items', 'custpage_containerlevelqty', s);
				var deliveryid  = request.getLineItemValue('custpage_items', 'custpage_deliveryid', s);
				var ordertype   = request.getLineItemValue('custpage_items', 'custpage_ordertype', s);
				var markfor     = request.getLineItemValue('custpage_items', 'custpage_markfor', s);
				var orderpriority       = request.getLineItemValue('custpage_items', 'custpage_orderpriority', s);
				var consigneeid      = request.getLineItemValue('custpage_items', 'custpage_consigneeid', s);
				var billinginfolocation = request.getLineItemValue('custpage_items', 'custpage_billinginfolocation', s);
				var billtocontactname      = request.getLineItemValue('custpage_items', 'custpage_billtocontactname', s);
				var billtoaddrone       = request.getLineItemValue('custpage_items', 'custpage_billtoaddrone', s);
				var billtoaddrtwo       = request.getLineItemValue('custpage_items', 'custpage_billtoaddrtwo', s);
				var billtocity    = request.getLineItemValue('custpage_items', 'custpage_billtocity', s);
				var billtostate   = request.getLineItemValue('custpage_items', 'custpage_billtostate', s);
				var billtozip     = request.getLineItemValue('custpage_items', 'custpage_billtozip', s);
				var billtocountry  = request.getLineItemValue('custpage_items', 'custpage_billtocountry', s);
				var billtophone   = request.getLineItemValue('custpage_items', 'custpage_billtophone', s);
				var billtoemail   = request.getLineItemValue('custpage_items', 'custpage_billtoemail', s);
				var billtofax     = request.getLineItemValue('custpage_items', 'custpage_billtofax', s);
				var billinginfocustzero  = request.getLineItemValue('custpage_items', 'custpage_billinginfocustzero', s);
				var billinginfocustone   = request.getLineItemValue('custpage_items', 'custpage_billinginfocustone', s);
				var billinginfocusttwo   = request.getLineItemValue('custpage_items', 'custpage_billinginfocusttwo', s);
				var billinginfocustthree = request.getLineItemValue('custpage_items', 'custpage_billinginfocustthree', s);
				var billinginfocustfour  = request.getLineItemValue('custpage_items', 'custpage_billinginfocustfour', s);
				var billinginfocustfive  = request.getLineItemValue('custpage_items', 'custpage_billinginfocustfive', s);
				var billinginfocustsix   = request.getLineItemValue('custpage_items', 'custpage_billinginfocustsix', s);
				var billinginfocustseven = request.getLineItemValue('custpage_items', 'custpage_billinginfocustseven', s);
				var billinginfocusteigh  = request.getLineItemValue('custpage_items', 'custpage_billinginfocusteigh', s);
				var billinginfocustnine  = request.getLineItemValue('custpage_items', 'custpage_billinginfocustnine', s);
				var markforlocation      = request.getLineItemValue('custpage_items', 'custpage_markforlocation', s);
				var markforshiptocontactname = request.getLineItemValue('custpage_items', 'custpage_markforshiptocontactname', s);
				var markforshiptoaddrone = request.getLineItemValue('custpage_items', 'custpage_markforshiptoaddrone', s);
				var markforshiptoaddrtwo = request.getLineItemValue('custpage_items', 'custpage_markforshiptoaddrtwo', s);
				var markforshiptocity    = request.getLineItemValue('custpage_items', 'custpage_markforshiptocity', s);
				var markforshiptostate   = request.getLineItemValue('custpage_items', 'custpage_markforshiptostate', s);
				var markforshiptozip     = request.getLineItemValue('custpage_items', 'custpage_markforshiptozip', s);
				var markforshiptocountry = request.getLineItemValue('custpage_items', 'custpage_markforshiptocountry', s);
				var markforshiptoemail   = request.getLineItemValue('custpage_items', 'custpage_markforshiptoemail', s);
				var markforshiptophone   = request.getLineItemValue('custpage_items', 'custpage_markforshiptophone', s);
				var markforshiptofax     = request.getLineItemValue('custpage_items', 'custpage_markforshiptofax', s);
				var customerpo      = request.getLineItemValue('custpage_items', 'custpage_customerpo', s);
				var hostord      = request.getLineItemValue('custpage_items', 'custpage_hostord', s);
				var deliveryline      = request.getLineItemValue('custpage_items', 'custpage_deliveryline', s);
				var iteminternaid       = request.getLineItemValue('custpage_items', 'custpage_iteminternaid', s);
				var sku      = request.getLineItemValue('custpage_items', 'custpage_sku', s);
				var skudesc     = request.getLineItemValue('custpage_items', 'custpage_skudesc', s);
				var skufamily   = request.getLineItemValue('custpage_items', 'custpage_skufamily', s);
				var skugroup    = request.getLineItemValue('custpage_items', 'custpage_skugroup', s);
				var skutype     = request.getLineItemValue('custpage_items', 'custpage_skutype', s);
				var buyerpartnumber      = request.getLineItemValue('custpage_items', 'custpage_buyerpartnumber', s);
				var upc     = request.getLineItemValue('custpage_items', 'custpage_upc', s);
				var hazmat  = request.getLineItemValue('custpage_items', 'custpage_hazmat', s);
				var natl    = request.getLineItemValue('custpage_items', 'custpage_natl', s);
				var tempcntrl      = request.getLineItemValue('custpage_items', 'custpage_tempcntrl', s);
				var cntrlsubstance      = request.getLineItemValue('custpage_items', 'custpage_cntrlsubstance', s);
				var batch      = request.getLineItemValue('custpage_items', 'custpage_batch', s);
				var batchdelqty      = request.getLineItemValue('custpage_items', 'custpage_batchdelqty', s);
				var serial      = request.getLineItemValue('custpage_items', 'custpage_serial', s);
				var quantity    = request.getLineItemValue('custpage_items', 'custpage_quantity', s);
				var uom      = request.getLineItemValue('custpage_items', 'custpage_uom', s);
				var uomqty      = request.getLineItemValue('custpage_items', 'custpage_uomqty', s);
				var skulength     = request.getLineItemValue('custpage_items', 'custpage_skulength', s);
				var skuwidth      = request.getLineItemValue('custpage_items', 'custpage_skuwidth', s);
				var skuweight     = request.getLineItemValue('custpage_items', 'custpage_skuweight', s);
				var skuheight     = request.getLineItemValue('custpage_items', 'custpage_skuheight', s);
				var deliveryitemcustzero = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcustzero', s);
				var deliveryitemcustone  = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcustone', s);
				var deliveryitemcusttwo  = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcusttwo', s);
				var deliveryitemcustthree = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcustthree', s);
				var deliveryitemcustfour = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcustfour', s);
				var deliveryitemcustfive = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcustfive', s);
				var deliveryitemcustsix   = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcustsix', s);
				var deliveryitemcustseven = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcustseven', s);
				var deliveryitemcusteigh  = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcusteigh', s);
				var deliveryitemcustnine = request.getLineItemValue('custpage_items', 'custpage_deliveryitemcustnine', s);
				var deliverycustzero      = request.getLineItemValue('custpage_items', 'custpage_deliverycustzero', s);
				var deliverycustone       = request.getLineItemValue('custpage_items', 'custpage_deliverycustone', s);
				var deliverycusttwo       = request.getLineItemValue('custpage_items', 'custpage_deliverycusttwo', s);
				var deliverycustthree     = request.getLineItemValue('custpage_items', 'custpage_deliverycustthree', s);
				var deliverycustfour      = request.getLineItemValue('custpage_items', 'custpage_deliverycustfour', s);
				var deliverycustfive      = request.getLineItemValue('custpage_items', 'custpage_deliverycustfive', s);
				var deliverycustsix       = request.getLineItemValue('custpage_items', 'custpage_deliverycustsix', s);
				var deliverycustseven     = request.getLineItemValue('custpage_items', 'custpage_deliverycustseven', s);
				var deliverycusteight     = request.getLineItemValue('custpage_items', 'custpage_deliverycusteight', s);
				var deliverycustnine      = request.getLineItemValue('custpage_items', 'custpage_deliverycustnine', s);
				var containercustzero     = request.getLineItemValue('custpage_items', 'custpage_containercustzero', s);
				var containercustone      = request.getLineItemValue('custpage_items', 'custpage_containercustone', s);
				var containercusttwo      = request.getLineItemValue('custpage_items', 'custpage_containercusttwo', s);
				var containercustthree    = request.getLineItemValue('custpage_items', 'custpage_containercustthree', s);
				var containercustfour     = request.getLineItemValue('custpage_items', 'custpage_containercustfour', s);
				var containercustfive     = request.getLineItemValue('custpage_items', 'custpage_containercustfive', s);
				var containercustsix      = request.getLineItemValue('custpage_items', 'custpage_containercustsix', s);
				var containercustseven    = request.getLineItemValue('custpage_items', 'custpage_containercustseven', s);
				var containercusteigh     = request.getLineItemValue('custpage_items', 'custpage_containercusteigh', s);
				var containercustnine     = request.getLineItemValue('custpage_items', 'custpage_containercustnine', s);
				var shipunitcustzero      = request.getLineItemValue('custpage_items', 'custpage_shipunitcustzero', s);
				var shipunitcustone           = request.getLineItemValue('custpage_items', 'custpage_shipunitcustone', s);
				var shipunitcusttwo           = request.getLineItemValue('custpage_items', 'custpage_shipunitcusttwo', s);
				var shipunitcustthree      = request.getLineItemValue('custpage_items', 'custpage_shipunitcustthree', s);
				var shipunitcustfour     = request.getLineItemValue('custpage_items', 'custpage_shipunitcustfour', s);
				var shipunitcustfive     = request.getLineItemValue('custpage_items', 'custpage_shipunitcustfive', s);
				var shipunitcustsix      = request.getLineItemValue('custpage_items', 'custpage_shipunitcustsix', s);
				var shipunitcustseven    = request.getLineItemValue('custpage_items', 'custpage_shipunitcustseven', s);
				var shipunitcusteight    = request.getLineItemValue('custpage_items', 'custpage_shipunitcusteight', s);
				var shipunitcustnine     = request.getLineItemValue('custpage_items', 'custpage_shipunitcustnine', s);
				var whlocation  = request.getLineItemValue('custpage_items', 'custpage_whlocation', s);
				var company     = request.getLineItemValue('custpage_items', 'custpage_company', s);
				var account     = request.getLineItemValue('custpage_items', 'custpage_account', s);
				var ebizstatus  = request.getLineItemValue('custpage_items', 'custpage_ebizstatus', s);
				var interfacestatus           = request.getLineItemValue('custpage_items', 'custpage_interfacestatus', s);
				var host     = request.getLineItemValue('custpage_items', 'custpage_host', s);
				var currentdate     = request.getLineItemValue('custpage_items', 'custpage_currentdate', s);
				var currenttime     = request.getLineItemValue('custpage_items', 'custpage_currenttime', s);
				var confirmhostdate           = request.getLineItemValue('custpage_items', 'custpage_confirmhostdate', s);
				var confirmhosttime           = request.getLineItemValue('custpage_items', 'custpage_confirmhosttime', s);
				var errordesc     = request.getLineItemValue('custpage_items', 'custpage_errordesc', s);
				var filename      = request.getLineItemValue('custpage_items', 'custpage_filename', s);
				var filecreateddate           = request.getLineItemValue('custpage_items', 'custpage_filecreateddate', s);
				var filecreatedtime           = request.getLineItemValue('custpage_items', 'custpage_filecreatedtime', s);

				nlapiLogExecution('ERROR','before update','done');
				updateInStageASNC(internalId,  reference,
                        messagetype ,                        creationdatetime,                        senderbusiness ,                        recipientbusiness,
                        trailer,                        department ,                        asncclass,                        vendorid,
                        shipunitid ,                        shipunitcharges,                        shipunitlengths,                        shipunitwidth,
                        shipunitweight ,                        shipunitheight ,                        shipunitlevelqty ,                        waybillref ,
                        carrierid ,                        scaccode,                        carriername ,                        succ,                        route,
                        dest,                        vessel ,                        voyage ,                        bol,                        pronum ,                        shippinglocation ,
                        shiptoaddrone,                        shiptoaddrtwo,                        shiptocity,                        shiptostate,
                        shiptozip ,                        shiptocountry ,                        shiptophone,                        shiptoemail,                        shiptofax ,
                        shipunitaddrcustzero,                        shipunitaddrcustone,                        shipunitaddrcusttwo,                        shipunitaddrcustthree,                        shipunitaddrcustfour,
                        shipunitaddrcustfive,                        shipunitaddrcustsix,                        shipunitaddrcustseven ,                        shipunitaddrcusteigh  ,
                        shipunitaddrcustnine,                        expectedarrival,                        plannedarrival ,                        issueperiod ,                        shippingcustzero,
                        shippingcustone ,                        shippingcusttwo ,                        shippingcustthree,                        shippingcustfour,                        shippingcustfive,
                        shippingcustsix ,                        shippingcustseven,                        shippingcusteight,                        shippingcustnine,                        containerlp,
                        containerlpcharge,                        containertracking,                        cucc,                        paymentterms ,                        containerlength ,
                        containerwidth ,                        containerheight ,                        containerweight ,                        containerlevelqty,                        deliveryid,                        ordertype ,
                        markfor,                        orderpriority,                        consigneeid ,                        billinginfolocation,                        billtocontactname,                        billtoaddrone,
                        billtoaddrtwo,                        billtocity,                        billtostate ,                        billtozip,                        billtocountry ,
                        billtophone ,                        billtoemail ,                        billtofax,                        billinginfocustzero ,                        billinginfocustone  ,                        billinginfocusttwo  ,
                        billinginfocustthree,                        billinginfocustfour ,                        billinginfocustfive ,                        billinginfocustsix  ,                        billinginfocustseven,
                        billinginfocusteigh ,                        billinginfocustnine ,                        markforlocation ,                        markforshiptocontactname ,                        markforshiptoaddrone,
                        markforshiptoaddrtwo,                        markforshiptocity,                        markforshiptostate  ,                        markforshiptozip,                        markforshiptocountry,
                        markforshiptoemail  ,                        markforshiptophone  ,                        markforshiptofax,                        customerpo ,                        hostord,                        deliveryline ,
                        iteminternaid,                        sku,                        skudesc,                        skufamily ,                        skugroup,                        skutype,
                        buyerpartnumber ,                        upc,                        hazmat,                        natl,                        tempcntrl ,                        cntrlsubstance ,
                        batch,                        batchdelqty ,                        serial,                        quantity,                        uom,                        uomqty,
                        skulength,                        skuwidth,                        skuweight,                        skuheight,                        deliveryitemcustzero,                        deliveryitemcustone ,
                        deliveryitemcusttwo ,                        deliveryitemcustthree,                        deliveryitemcustfour,                        deliveryitemcustfive,                        deliveryitemcustsix  ,                        deliveryitemcustseven,
                        deliveryitemcusteigh ,                        deliveryitemcustnine,                        deliverycustzero ,                        deliverycustone,                        deliverycusttwo,                        deliverycustthree,
                        deliverycustfour ,                        deliverycustfive ,                        deliverycustsix,                        deliverycustseven,                        deliverycusteight,                        deliverycustnine ,
                        containercustzero,                        containercustone ,                        containercusttwo ,                        containercustthree,                        containercustfour,                        containercustfive,
                        containercustsix ,                        containercustseven,                        containercusteigh,                        containercustnine,                        shipunitcustzero ,
                        shipunitcustone,                        shipunitcusttwo,                        shipunitcustthree,                        shipunitcustfour,                        shipunitcustfive,
                        shipunitcustsix ,                        shipunitcustseven,                        shipunitcusteight,                        shipunitcustnine,                        whlocation,
                        company,                        account,                        ebizstatus,                        interfacestatus,
                        host,                        currentdate,                        currenttime,                        confirmhostdate,                        confirmhosttime,
                        errordesc,                        filename,
                        filecreateddate,                        filecreatedtime              );
				nlapiLogExecution('ERROR','after update','done');

			}
		}
	}	
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception',exp);
	}
 
}

function updateInStageASNC(recordId,  reference,
        messagetype ,
        creationdatetime,
        senderbusiness ,
        recipientbusiness,
        trailer,
        department ,
        asncclass,
        vendorid,
        shipunitid ,
        shipunitcharges,
        shipunitlengths,
        shipunitwidth,
        shipunitweight ,
        shipunitheight ,
        shipunitlevelqty ,
        waybillref ,
        carrierid ,
        scaccode,
        carriername ,
        succ,
        route,
        dest,
        vessel ,
        voyage ,
        bol,
        pronum ,
        shippinglocation ,
        shiptoaddrone,
        shiptoaddrtwo,
        shiptocity,
        shiptostate,
        shiptozip ,
        shiptocountry ,
        shiptophone,
        shiptoemail,
        shiptofax ,
        shipunitaddrcustzero,
        shipunitaddrcustone,
        shipunitaddrcusttwo,
        shipunitaddrcustthree,
        shipunitaddrcustfour,
        shipunitaddrcustfive,
        shipunitaddrcustsix,
        shipunitaddrcustseven ,
        shipunitaddrcusteigh  ,
        shipunitaddrcustnine,
        expectedarrival,
        plannedarrival ,
        issueperiod ,
        shippingcustzero,
        shippingcustone ,
        shippingcusttwo ,
        shippingcustthree,
        shippingcustfour,
        shippingcustfive,
        shippingcustsix ,
        shippingcustseven,
        shippingcusteight,
        shippingcustnine,
        containerlp,
        containerlpcharge,
        containertracking,
        cucc,
        paymentterms ,
        containerlength ,
        containerwidth ,
        containerheight ,
        containerweight ,
        containerlevelqty,
        deliveryid,
        ordertype ,
        markfor,
        orderpriority,
        consigneeid ,
        billinginfolocation,
        billtocontactname,
        billtoaddrone,
        billtoaddrtwo,
        billtocity,
        billtostate ,
        billtozip,
        billtocountry ,
        billtophone ,
        billtoemail ,
        billtofax,
        billinginfocustzero ,
        billinginfocustone  ,
        billinginfocusttwo  ,
        billinginfocustthree,
        billinginfocustfour ,
        billinginfocustfive ,
        billinginfocustsix  ,
        billinginfocustseven,
        billinginfocusteigh ,
        billinginfocustnine ,
        markforlocation ,
        markforshiptocontactname ,
        markforshiptoaddrone,
        markforshiptoaddrtwo,
        markforshiptocity,
        markforshiptostate  ,
        markforshiptozip,
        markforshiptocountry,
        markforshiptoemail  ,
        markforshiptophone  ,
        markforshiptofax,
        customerpo ,
        hostord,
        deliveryline ,
        iteminternaid,
        sku,
        skudesc,
        skufamily ,
        skugroup,
        skutype,
        buyerpartnumber ,
        upc,
        hazmat,
        natl,
        tempcntrl ,
        cntrlsubstance ,
        batch,
        batchdelqty ,
        serial,
        quantity,
        uom,
        uomqty,
        skulength,
        skuwidth,
        skuweight,
        skuheight,
        deliveryitemcustzero,
        deliveryitemcustone ,
        deliveryitemcusttwo ,
        deliveryitemcustthree,
        deliveryitemcustfour,
        deliveryitemcustfive,
        deliveryitemcustsix  ,
        deliveryitemcustseven,
        deliveryitemcusteigh ,
        deliveryitemcustnine,
        deliverycustzero ,
        deliverycustone,
        deliverycusttwo,
        deliverycustthree,
        deliverycustfour ,
        deliverycustfive ,
        deliverycustsix,
        deliverycustseven,
        deliverycusteight,
        deliverycustnine ,
        containercustzero,
        containercustone ,
        containercusttwo ,
        containercustthree,
        containercustfour,
        containercustfive,
        containercustsix ,
        containercustseven,
        containercusteigh,
        containercustnine,
        shipunitcustzero ,
        shipunitcustone,
        shipunitcusttwo,
        shipunitcustthree,
        shipunitcustfour,
        shipunitcustfive,
        shipunitcustsix ,
        shipunitcustseven,
        shipunitcusteight,
        shipunitcustnine,
        whlocation,
        company,
        account,
        ebizstatus,
        interfacestatus,
        host,
        currentdate,
        currenttime,
        confirmhostdate,
        confirmhosttime,
        errordesc,
        filename,
        filecreateddate,
        filecreatedtime){
	nlapiLogExecution('ERROR','into update record','done');
	nlapiLogExecution('ERROR','recordId',recordId);
	var transaction = nlapiLoadRecord('customrecord_ebiznetinterfaceasnc', recordId);
	
	transaction.setFieldValue('custrecord_asnc_reference_id', reference);
	transaction.setFieldValue('custrecord_asnc_message_type', messagetype);
	transaction.setFieldValue('custrecord_asnc_creation_datetime', creationdatetime);
	transaction.setFieldValue('custrecord_sender_business_id', senderbusiness);
	transaction.setFieldValue('custrecord_recipient_business_id', recipientbusiness);
	transaction.setFieldValue('custrecord_ebiztrailerno', trailer);
	transaction.setFieldValue('custrecord_ebizdepartment', department);
	transaction.setFieldValue('custrecord_ebiz_class', asncclass);
	transaction.setFieldValue('custrecord_ebizvendor', vendorid);
	transaction.setFieldValue('custrecord_ebizshipunitid', shipunitid);
	transaction.setFieldValue('custrecord_shipunitcharges', shipunitcharges);
	transaction.setFieldValue('custrecord_ebiznetshipunitlgth', shipunitlengths);
	transaction.setFieldValue('custrecord_ebiz_shipunit_width', shipunitwidth);
	transaction.setFieldValue('custrecord_ebiz_shipunit_weight', shipunitweight);
	transaction.setFieldValue('custrecord_ebiz_shipunit_height', shipunitheight);
	transaction.setFieldValue('custrecord_shipunit_levelqty', shipunitlevelqty);
	transaction.setFieldValue('custrecord_ebiz_waybill_ref', waybillref);
	transaction.setFieldValue('custrecord_ebiz_carrierid', carrierid);
	transaction.setFieldValue('custrecord_ebiz_scac_code', scaccode);
	transaction.setFieldValue('custrecord_ebiz_carrier_name', carriername);
	transaction.setFieldValue('custrecord_ebiz_s_ucc128', succ);
	transaction.setFieldValue('custrecord_ebiz_route', route);
	transaction.setFieldValue('custrecord_ebiz_dest', dest);
	transaction.setFieldValue('custrecord_ebiz_vessel', vessel);
	transaction.setFieldValue('custrecord_ebiz_voyage', voyage);
	transaction.setFieldValue('custrecord_ebiz_bol', bol);
	transaction.setFieldValue('custrecord_ebiz_pronum', pronum);
	transaction.setFieldValue('custrecord_ebiz_shiploc_id', shippinglocation);
	transaction.setFieldValue('custrecord_ebiz_shiptoadd1', shiptoaddrone);
	transaction.setFieldValue('custrecord_ebiz_shiptoadd2', shiptoaddrtwo);
	transaction.setFieldValue('custrecord_ebiz_shiptocity', shiptocity);
	transaction.setFieldValue('custrecord_ebiz_shiptostate', shiptostate);
	transaction.setFieldValue('custrecord_ebiz_shiptozip', shiptozip);
	transaction.setFieldValue('custrecord_ebiz_shiptocountry', shiptocountry);
	transaction.setFieldValue('custrecord_ebiz_shiptophone', shiptophone);
	transaction.setFieldValue('custrecord_ebiz_shiptoemail', shiptoemail);
	transaction.setFieldValue('custrecord_ebiz_shiptofax', shiptofax);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_000', shipunitaddrcustzero);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_001', shipunitaddrcustone);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_002', shipunitaddrcusttwo);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_003', shipunitaddrcustthree);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_004', shipunitaddrcustfour);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_005', shipunitaddrcustfive);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_006', shipunitaddrcustsix);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_007', shipunitaddrcustseven);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_008', shipunitaddrcusteigh);
	transaction.setFieldValue('custrecord_ebiz_shipunitadd_custfld_009', shipunitaddrcustnine);
	transaction.setFieldValue('custrecord_ebiz_expected_arrivaldate', expectedarrival);
	transaction.setFieldValue('custrecord_ebiz_planned_arrival_datetime', plannedarrival);
	transaction.setFieldValue('custrecord_ebiz_issue_period', issueperiod);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_000', shippingcustzero);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_001', shippingcustone);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_002', shippingcusttwo);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_003', shippingcustthree);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_004', shippingcustfour);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_005', shippingcustfive);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_006', shippingcustsix);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_007', shippingcustseven);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_008', shippingcusteight);
	transaction.setFieldValue('custrecord_ebiz_shipping_custfield_009', shippingcustnine);
	transaction.setFieldValue('custrecord_ebiz_containerlp', containerlp);
	transaction.setFieldValue('custrecord_ebiz_containerlp_charges', containerlpcharge);
	transaction.setFieldValue('custrecord_ebiz_container_trackingno', containertracking);
	transaction.setFieldValue('custrecord_ebiz_c_ucc128', cucc);
	transaction.setFieldValue('custrecord_ebiz_payment_terms', paymentterms);
	transaction.setFieldValue('custrecord_ebiz_container_length', containerlength);
	transaction.setFieldValue('custrecord_ebiz_container_width', containerwidth);
	transaction.setFieldValue('custrecord_ebiz_container_height', containerheight);
	transaction.setFieldValue('custrecord_ebiz_container_weight', containerweight);
	transaction.setFieldValue('custrecord_container_levelqty', containerlevelqty);
	transaction.setFieldValue('custrecord_ebiz_deliveryid', deliveryid);
	transaction.setFieldValue('custrecord_ebiz_order_type', ordertype);
	transaction.setFieldValue('custrecord_ebiz_markfor', markfor);
	transaction.setFieldValue('custrecord_ebiz_order_priority', orderpriority);
	transaction.setFieldValue('custrecord_ebiz_consigneeid', consigneeid);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_locationid', billinginfolocation);
	transaction.setFieldValue('custrecord_ebiz_billto_contactname', billtocontactname);
	transaction.setFieldValue('custrecord_ebiz_billto_addr1', billtoaddrone);
	transaction.setFieldValue('custrecord_ebiz_billto_addr2', billtoaddrtwo);
	transaction.setFieldValue('custrecord_ebiz_billtocity', billtocity);
	transaction.setFieldValue('custrecord_ebiz_billtostate', billtostate);
	transaction.setFieldValue('custrecord_ebiz_billtozip', billtozip);
	transaction.setFieldValue('custrecord_ebiz_billtocountry', billtocountry);
	transaction.setFieldValue('custrecord_ebiz_billtophone', billtophone);
	transaction.setFieldValue('custrecord_ebiz_billtoemail', billtoemail);
	transaction.setFieldValue('custrecord_ebiz_billtofax', billtofax);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_00', billinginfocustzero);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_01', billinginfocustone);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_02', billinginfocusttwo);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_03', billinginfocustthree);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_04', billinginfocustfour);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_05', billinginfocustfive);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_06', billinginfocustsix);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_07', billinginfocustseven);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_08', billinginfocusteigh);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_09', billinginfocustnine);
	transaction.setFieldValue('custrecord_ebiz_billinginfo_custfield_00', markforlocation);
	transaction.setFieldValue('custrecord_ebiz_markfor_shptocontactname', markforshiptocontactname);
	transaction.setFieldValue('custrecord_ebiz_markfor_shipaddr1', markforshiptoaddrone);
	transaction.setFieldValue('custrecord_ebiz_markfor_shipaddr2', markforshiptoaddrtwo);
	transaction.setFieldValue('custrecord_ebiz_markfor_shiptocity', markforshiptocity);
	transaction.setFieldValue('custrecord_ebiz_markfor_shiptostate', markforshiptostate);
	transaction.setFieldValue('custrecord_ebiz_markfor_shiptozip', markforshiptozip);
	transaction.setFieldValue('custrecord_ebiz_markfor_shiptocountry', markforshiptocountry);
	transaction.setFieldValue('custrecord_ebiz_markfor_shiptoemail', markforshiptoemail);
	transaction.setFieldValue('custrecord_ebiz_markfor_shiptophone', markforshiptophone);
	transaction.setFieldValue('custrecord_ebiz_markfor_shiptofax', markforshiptofax);
	transaction.setFieldValue('custrecord_ebiz_customer_pono', customerpo);
	transaction.setFieldValue('custrecord_ebiz_host_ordno', hostord);
	transaction.setFieldValue('custrecord_ebiz_deliveryline_no', deliveryline);
	transaction.setFieldValue('custrecord_ebiz_item_internalid', iteminternaid);
	transaction.setFieldValue('custrecord_ebiz_asnc_sku', sku);
	transaction.setFieldValue('custrecord_ebiz_asnc_sku_desc', skudesc);
	transaction.setFieldValue('custrecord_ebiz_asnc_sku_family', skufamily);
	transaction.setFieldValue('custrecord_ebiz_asnc_sku_group', skugroup);
	transaction.setFieldValue('custrecord_ebiz_asnc_sku_type', skutype);
	transaction.setFieldValue('custrecord_ebiz_buyer_partnumber', buyerpartnumber);
	transaction.setFieldValue('custrecord_ebiz_upc_code', upc);
	transaction.setFieldValue('custrecord_ebiz_hazmat_code', hazmat);
	transaction.setFieldValue('custrecord_ebiz_natl_frt_code', natl);
	transaction.setFieldValue('custrecord_ebiz_temp_cntrl', tempcntrl);
	transaction.setFieldValue('custrecord_ebiz_cntrl_substance', cntrlsubstance);
	transaction.setFieldValue('custrecord_ebiz_batch_id', batch);
	transaction.setFieldValue('custrecord_ebiz_batch_delivery_qty', batchdelqty);
	transaction.setFieldValue('custrecord_ebiz_serial_id', serial);
	transaction.setFieldValue('custrecord_ebiz_quantity', quantity);
	transaction.setFieldValue('custrecord_ebiz_uom_id', uom);
	transaction.setFieldValue('custrecord_ebiz_uom_qty', uomqty);
	transaction.setFieldValue('custrecord_ebiz_sku_length', skulength);
	transaction.setFieldValue('custrecord_ebiz_sku_width', skuwidth);
	transaction.setFieldValue('custrecord_ebiz_sku_weight', skuweight);
	transaction.setFieldValue('custrecord_ebiz_sku_height', skuheight);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield0', deliveryitemcustzero);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield1', deliveryitemcustone);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield2', deliveryitemcusttwo);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield3', deliveryitemcustthree);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield4', deliveryitemcustfour);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield5', deliveryitemcustfive);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield6', deliveryitemcustsix);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield7', deliveryitemcustseven);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield8', deliveryitemcusteigh);
	transaction.setFieldValue('custrecord_ebiz_delivery_item_custfield9', deliveryitemcustnine);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_000', deliverycustzero);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_001', deliverycustone);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_002', deliverycusttwo);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_003', deliverycustthree);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_004', deliverycustfour);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_005', deliverycustfive);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_006', deliverycustsix);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_007', deliverycustseven);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_008', deliverycusteight);
	transaction.setFieldValue('custrecord_ebiz_delivery_custfield_009', deliverycustnine);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_000', containercustzero);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_001', containercustone);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_002', containercusttwo);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_003', containercustthree);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_004', containercustfour);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_005', containercustfive);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_006', containercustsix);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_007', containercustseven);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_008', containercusteigh);
	transaction.setFieldValue('custrecord_ebiz_container_custfield_009', containercustnine);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_000', shipunitcustzero);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_001', shipunitcustone);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_002', shipunitcusttwo);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_003', shipunitcustthree);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_004', shipunitcustfour);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_005', shipunitcustfive);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_006', shipunitcustsix);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_007', shipunitcustseven);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_008', shipunitcusteight);
	transaction.setFieldValue('custrecord_ebiz_shipunit_custfield_009', shipunitcustnine);
	transaction.setFieldValue('custrecord_ebiz_wh_location', whlocation);
	transaction.setFieldValue('custrecord_ebiz_asnc_company', company);
	transaction.setFieldValue('custrecord_ebiz_account', account);
	transaction.setFieldValue('custrecord_ebiz_status_flag', ebizstatus);
	transaction.setFieldValue('custrecord_ebiz_interface_statusflag', interfacestatus);
	transaction.setFieldValue('custrecord_ebiz_host_id', host);
	transaction.setFieldValue('custrecord_ebiz_current_date', currentdate);
	transaction.setFieldValue('custrecord_ebiz_current_time', currenttime);
	transaction.setFieldValue('custrecord_ebiz_confirmed_to_hostdate', confirmhostdate);
	transaction.setFieldValue('custrecord_ebiz_confirmed_to_hosttime', confirmhosttime);
	transaction.setFieldValue('custrecord_ebiz_error_description', errordesc);
	transaction.setFieldValue('custrecord_ebiz_filename', filename);
	transaction.setFieldValue('custrecord_ebiz_file_created_date', filecreateddate);
	transaction.setFieldValue('custrecord_ebiz_file_created_time', filecreatedtime);	  
	 
	nlapiSubmitRecord(transaction, false, true);
}