/***************************************************************************
 							eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_ItemLotNumbers_SL.js,v $
 *     	   $Revision: 1.1.2.3.4.1 $
 *     	   $Date: 2013/02/27 13:06:28 $
 *     	   $Author: rmukkera $
 *     	   $Name: t_NSWMS_2013_1_3_1 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * 
 * 
 *
 *****************************************************************************/

function InventoryForItem(request,response)
{
	if(request.getMethod()=='GET')
	{
		var vItemId="";
		var vItemStatus="";
		var vLocationId="";
		var ReqQty="";	
		var sFlag="";
		//var lotno="";
		var totalQty = 0;
		//var form = nlapiCreateForm('Inventory Details');
		try{

			if(request.getParameter('custparam_item')!=null&& request.getParameter('custparam_item')!="")
			{
				vItemId=request.getParameter('custparam_item');
				vItemStatus=request.getParameter('custparam_itemstatus');
				vLocationId=request.getParameter('custparam_location');
				ReqQty=request.getParameter('custparam_qty');
				//lotno=request.getParameter('custparam_lotno');
				var vCntrl=request.getParameter('cus_sl');
				sFlag=request.getParameter('cus_flag');

				nlapiLogExecution('ERROR', 'ItemId',vItemId );
				nlapiLogExecution('ERROR', 'ItemStatus',vItemStatus);
				nlapiLogExecution('ERROR', 'vLocationId',vLocationId);
				nlapiLogExecution('ERROR', 'ReqQty',ReqQty);
				nlapiLogExecution('ERROR', 'sFlag',sFlag);


				var ItemType = nlapiLookupField('item', vItemId, 'recordType');
				nlapiLogExecution('ERROR','ItemType1',ItemType);

				/*				var sublist = form.addSubList("custpage_items", "list", "Inventory");
				sublist.addMarkAllButtons();
				sublist.addField("custpage_chkbox", "checkbox", "Confirm").setDefaultValue('F');
				sublist.addField("custpage_item", "text", "Item#");
				sublist.addField("custpage_lp", "text", "LP#");				
				sublist.addField("custpage_itemstatus", "text", "Item Status");
				sublist.addField("custpage_lotno", "text", "LOT#");
				sublist.addField("custpage_qty", "text", "Inventory Quantity");
				sublist.addField("custpage_enterqty", "text", "").setDisplayType('entry');
				sublist.addField("custpage_hiddenqty", "text", "").setDisplayType('hidden');

				form.addSubmitButton('Sumbit');*/

				var ItemRecord = nlapiLookupField('item',vItemId, ['recordType', 'itemid' ]);
				var vItemName=ItemRecord.itemid;


				var filter= new Array();
				filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof', vItemId));
				if(vItemStatus !=null && vItemStatus!=''){
					filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof', vItemStatus));
				}
				filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'anyof', vLocationId));
				filter.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));
				filter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']));

				if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
				{
					filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'noneof','@NONE@'));
				}



				var columns = new Array();	
				//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));
				columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku',null,'group'));			 
				columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot',null,'group'));
				columns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty',null,'sum'));
				//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
				//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));

				var SearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filter, columns);
				if(SearchResults!=null && SearchResults!="" && SearchResults.length>0)
				{
					var functionkeyHtml=getsubmitvalue('_rf_cluster_no',vCntrl,ReqQty,sFlag);  
					var html = "<html><head>";
					html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
					html = html + " document.getElementById('enterquantity').focus();";        
					html = html + "</script>";   	

					html = html +functionkeyHtml;
					//html = html + "</head><body onkeydown='return OnSubmit_CL();'>";
					html = html + "</head><body>";
					html = html + "	<form name='_rf_cluster_no' method='POST'>";
					html = html + "		<table cellspacing=0 cellpadding=0>";
					html = html + "			<tr>";
					html = html + "				<td align = 'Center'><h2> INVENTORY DETAILS </h2> ";
					html = html + "				</td>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					html = html + "				<td align = 'Center' style='font-face=Tahoma;font-weight:bold;font-size: 14px;'>ITEM :"+vItemName+" ";
					html = html + "				</td>";
					html = html + "			</tr>";

					html = html + "			<tr>";
					html = html + "		<table id='tbl' border=1 cellspacing=0 cellpadding=0  >";
					html = html + "			<tr style=\"font-face=Tahoma;font-weight:bold;background-color:gray;color:white;\">";
					html = html + "				<td align = 'Center'>Confirm";
					html = html + "				</td>";
					html = html + "				<td style = 'display:none'>";
					html = html + "				</td>";
					/*html = html + "				<td align = 'left'>BinLocation ";
					html = html + "				</td>";
					html = html + "				<td align = 'left'>LP# ";
					html = html + "				</td>";
					html = html + "				<td align = 'left'>Item Status";
					html = html + "				</td>";*/
					html = html + "				<td align = 'Center' style='font-size: 14px;'>Lot#";
					html = html + "				</td>";
					html = html + "				<td align = 'Center' style='font-size: 14px;'>Available Quantity";
					html = html + "				</td>";
					html = html + "				<td align = 'Center' style='font-size: 14px;'>Quantity";
					html = html + "				</td>";
					html = html + "				<td>";
					html = html + "				</td>";
					html = html + "			</tr>";


					for(i=0;i<SearchResults.length; i++)
					{
						html = html + "			<tr>";
						//var LP=SearchResults[i].getValue('custrecord_ebiz_inv_lp');
						//var binlocation=SearchResults[i].getText('custrecord_ebiz_inv_binloc');
						var Item=SearchResults[i].getText('custrecord_ebiz_inv_sku',null,'group');
						var Qty=SearchResults[i].getValue('custrecord_ebiz_avl_qty',null,'sum');
						var Lotno=SearchResults[i].getText('custrecord_ebiz_inv_lot',null,'group');
						//var ItemStatus=SearchResults[i].getText('custrecord_ebiz_inv_sku_status');
						var LotnoInteralId=SearchResults[i].getValue('custrecord_ebiz_inv_lot',null,'group');

						html = html + "				<td align = 'Center'><input id='entercheckbox' type='checkbox'/>";
						html = html + "				</td>";
						html = html + "				<td align = 'left' style = 'display:none'>";
						html = html+ "<label id='lblitemid'>" + Item + "</label>";
						html = html + "				</td>";

						/*html = html + "				<td align = 'left'>";
						html = html+ "<label id='lblbinloc'>" + binlocation + "</label>";
						html = html + "				</td>";

						html = html + "				<td align = 'left'>";
						html = html+ "<label id='lblLP'>" + LP + "</label>";
						html = html + "				</td>";

						html = html + "				<td align = 'left'>";
						html = html+ "<label id='lblitemstatus'>" + ItemStatus + "</label>";
						html = html + "				</td>";*/

						html = html + "				<td align = 'left' style='font-size: 12px;'>";
						html = html+ "<label id='lbllotno'>" + Lotno + "</label>";
						html = html + "				</td>";

						html = html + "				<td align = 'right' style='font-size: 12px;'>";
						html = html+ "<label id='lblqty'>" + Qty + "</label>";
						html = html + "				</td>";

						html = html + "				<td align = 'right' style='font-size: 12px;'><input id='enterquantity' type='text' style='text-align: right'/>";						
						html = html + "				</td>";

						//html = html + "				<td style = 'display:none'>";						
						html = html + "				<td style='font-size: 12px;'>";
						html = html+ "<input type='hidden' id='hdnPONo' value=" + LotnoInteralId + ">";
						//html = html+ "<label type='hidden' id='lbllotid'>" + LotnoInteralId + "</label>";
						//html = html+ LotnoInteralId;
						html = html + "				</td>";						

						html = html + "			</tr>";
					}

					html = html + "		 </table>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					html = html + "			<td><b>SUBMIT</b><input name='btnsubmit' type='submit' value='F2' onclick='return OnSubmit_CL();'/>";
					html = html + "			</td>";
					html = html + "			</tr>";

					html = html + "		 </table>";

					html = html + "	</form>";
					html = html + "</body>";
					html = html + "</html>";

					response.write(html);

				}
				else
				{nlapiLogExecution('ERROR','else part ','else');
				var functionkeyHtml=getsubmitvalue('_rf_cluster_no',vCntrl,ReqQty,sFlag);  
				var html = "<html><head>";
				html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
				html = html + " document.getElementById('enterquantity').focus();";        
				html = html + "</script>";   	
				html = html +functionkeyHtml;
				//html = html + "</head><body onkeydown='return OnSubmit_CL();'>";
				html = html + "</head><body>";
				html = html + "	<form name='_rf_cluster_no' method='POST'>";
				html = html + "		<table cellspacing=0 cellpadding=0>";
				html = html + "			<tr>";
				html = html + "				<td align = 'Center'><h2> INVENTORY DETAILS </h2> ";
				html = html + "				</td>";
				html = html + "			</tr>";
				html = html + "			<tr>";
				html = html + "				<td align = 'Center' style='font-face=Tahoma;font-weight:bold;font-size: 14px;'>ITEM :"+vItemName+" ";
				html = html + "				</td>";
				html = html + "			</tr>";
				html = html + "			<tr>";
				html = html + "				<td align = 'Center' style='font-face=Tahoma;font-weight:bold;font-size: 14px;'>No Inventory Available for the ITEM";
				html = html + "				</td>";
				html = html + "			</tr>";
				html = html + "		 </table>";
				html = html + "	</form>";
				html = html + "</body>";
				html = html + "</html>";
				response.write(html);
				}

			}


		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception ',exp);
		}

		//response.writePage(form);

	}


}

function getsubmitvalue(formid,vCntrl,ReqQty,sFlag){
	var html=""; 
	html = html + "<SCRIPT LANGUAGE='javascript'>";
	html = html + "function OnSubmit_CL() ";
	html = html + " { ";     
	// html = html + "alert('HI');";

	html = html +" var rows = document.getElementById('tbl').getElementsByTagName('tr').length;";
	//html = html + "alert(rows);";
	html = html + "var vtable = document.getElementById('tbl');";
	html = html + "var varqty=0;";
	//html = html + "alert(vtable);";
	html = html + "var rowLength = rows;";
	html = html + "var vText;";
	html = html + "var vLotid;";
	html = html + "var vcount=0;";
	html = html + "if (navigator.userAgent.indexOf('Firefox') !=-1)";//detecting browser
	html = html + "{ ";  
	// html = html +"alert('Firefox');";
	//html = html +"alert('HI');";
	html = html + "for (var i = 1; i < rows; i++){";
	//html = html + "alert(rows);";
	//html = html + "alert(i);";
	html = html + "var row = vtable.rows[i];";

	html = html +"var vTableCells = row.getElementsByTagName('td');";

	html = html +" if ((vTableCells[4].firstChild.value != null) && (!isNaN(vTableCells[4].firstChild.value))){";
	html = html +"}";
	html = html +"else{";
	html = html + "alert('Please Enter Numbers Only');";
	html = html + " return false; ";
	html = html + "}";


	/* code merged from monobid on feb 27th 2013 by radhika */

	// html = html +"alert(vTableCells[4].firstChild.value);";
	html = html + "if(parseFloat(vTableCells[4].firstChild.value)<1){"
	html = html + "alert('Entered Quantity Should not be Less than 1');";
	/* upto here */
	html = html + " return false; ";
	html = html + "}";



	//html = html +"alert(vTableCells.length);";
	//html= html+"varqty=varqty+vTableCells[2].innerText";
	//html = html +"alert(vTableCells[2].innerText);";
	html = html + "if(vTableCells[0].firstChild.checked){";
	//html = html +"alert(vTableCells[6].firstChild.value);";
	// html = html +"alert(vTableCells[5].innerText);";
	// html = html +" var Lotno=vTableCells[4].innerText";
	//html = html +"var enterQty=vTableCells[6].firstChild.value;";
	// html = html +"alert(vTableCells[6].firstChild.value);";
	// html = html +"var InvtQty=vTableCells[5].innerText;";
	//html = html +"alert(vTableCells[7].innerText);";
	//html = html +"alert(vTableCells[7].firstChild.value);";
	html = html +"if(parseFloat(vTableCells[4].firstChild.value)>parseFloat(vTableCells[3].textContent)){";
	html = html +"alert('Entered Quantity should not be greater than Available Qty for the Lot# :'+vTableCells[2].textContent);";
	html = html + " return false; ";
	html = html +"}";
	html = html +"if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!=''){";
	html = html +"vcount= parseFloat(vcount)+1;";
	html = html +"varqty=varqty+parseFloat(vTableCells[4].firstChild.value);";        
	html = html +"if(vText != null && vText != ''){";
	html = html +"vText=vText+','+vTableCells[2].textContent.trim()+'('+vTableCells[4].firstChild.value+')';";
	html = html +"vLotid=vLotid+','+vTableCells[5].firstChild.value;}";
	html = html +"else { vText=vTableCells[2].textContent.trim()+'('+vTableCells[4].firstChild.value+')';"
	html = html +" vLotid=vTableCells[5].firstChild.value;}";
	html = html +"}";
	html = html +"}";
	html = html +"else{";
	html = html +"if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!=''){";
	html = html +"alert('Entered Quantity for unselected Lot# '+vTableCells[2].textContent);";
	html = html + " return false; }";
	html = html + " }";

//	html = html + " }";
//	html = html + "if(varqty>Quantity){";
//	html = html + "alert('Selected Quantity is not greater than Actual quantity');";
	//html = html + " }";
	html = html + "}"; 
	html = html + "}";
	html = html + " else";
	html = html + "{";
	// html = html +"alert('IE');";
	html = html + "for (var i = 1; i < rows; i++){";
	//html = html + "alert(rows);";
	//html = html + "alert(i);";
	html = html + "var row = vtable.rows[i];";
	html = html +"var vTableCells = row.getElementsByTagName('td');";
	html = html +" if ((vTableCells[4].firstChild.value != null) && (!isNaN(vTableCells[4].firstChild.value))){";
	html = html +"}";
	html = html +"else{";
	html = html + "alert('Please Enter Numeric values Only');";
	html = html + " return false; ";
	html = html + "}";
	// html = html +"alert(vTableCells[4].firstChild.value);";
	html = html + "if(parseFloat(vTableCells[4].firstChild.value)<=0){"
	html = html + "alert('Entered Quantity Should not be less than or equal to 0');";
	html = html + " return false; ";
	html = html + "}";
	html = html + "if(vTableCells[0].firstChild.checked){";
	html = html +"if(parseFloat(vTableCells[4].firstChild.value)>parseFloat(vTableCells[3].innerText)){";
	html = html +"alert('Entered Quantity should not be greater than Available Qty for the Lot# :'+vTableCells[2].innerText);";
	html = html + " return false; ";
	html = html +"}";

	html = html +"if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!=''){";
	html = html +"vcount= parseFloat(vcount)+1;";
	html = html +"varqty=varqty+parseFloat(vTableCells[4].firstChild.value);";        
	html = html +"if(vText != null && vText != ''){";
	html = html +"vText=vText+','+vTableCells[2].innerText+'('+vTableCells[4].firstChild.value+')';";
	html = html +"vLotid=vLotid+','+vTableCells[5].firstChild.value;}";
	html = html +"else { vText=vTableCells[2].innerText+'('+vTableCells[4].firstChild.value+')';"
	html = html +" vLotid=vTableCells[5].firstChild.value;}";
	html = html +"}";
	html = html +"}";


	html = html +"else{";
	html = html +"if(vTableCells[4].firstChild.value!=null && vTableCells[4].firstChild.value!=''){";
	html = html +"alert('Entered Quantity for unselected Lot# '+vTableCells[2].innerText);";
	html = html + " return false; }";
	html = html + " }";

	html = html + "}";

	html = html + "}";

	html = html + "if(vText == null || vText == ''){";
	html = html + "vText='';";
	html = html + "vLotid='';";
	html = html + " }";

	//html = html + "alert(varqty);";
	//html = html + "alert('"+ReqQty+"');";
	html = html + "if((parseFloat(varqty)>parseFloat('"+ReqQty+"')|| parseFloat(varqty)<parseFloat('"+ReqQty+"'))&& varqty>0){";
	html = html + "alert('Entered Quantity must be equal to Required quantity:'+'"+ReqQty+"');";
	html = html + " return false; ";
	html = html + " }";

	//html = html + "alert(vcount);";
	html = html +" if(('"+sFlag+"')=='T')";
	html = html +"{";
	html = html +"if(vcount!=1 && vcount!=0)";
	html = html +"{"
	html = html +"alert('NotToSplit Lot# Flag is checked for the Customer, So Select Quantity from One Lot# ');";
	html = html + " return false; ";
	html = html +"}";
	html = html +"}";

	//html = html + "alert(vText);";
	//html = html + "alert(vLotid);";
	// html = html + "alert(" + vCntrl + ");";
	var vCntrlId= "custpage_lotbatch" + vCntrl;
	var vLotId= "custpage_lotnoid" + vCntrl;
	//html = html + "alert('" + vCntrlId + "');";
	html = html + "if (navigator.userAgent.indexOf('Firefox') !=-1)";
	html = html +"{";
	html = html + "window.opener.document.getElementById('"+ vCntrlId +"').value = vText;window.opener.focus();target='_self';";
	html = html + "window.opener.document.getElementById('"+ vLotId +"').value = vLotid;window.opener.focus();target='_self';self.close();";
	html = html + "return false;"
	html = html +"}";
	html = html +"else";
	html = html +"{";
	html = html + "window.opener.document.getElementById('"+ vCntrlId +"').value = vText;window.opener.focus();target='_self';self.close();";
	html = html + "window.opener.document.getElementById('"+ vLotId +"').value = vLotid;window.opener.focus();target='_self';self.close();";
	html = html + "return false;"
	html = html +"}";
	//html = html + "window.opener.document.getElementById('custpage_lotbatch1').value = '"+varqty+"';self.close();";
	//html = html + "    return false; ";
	html = html + "}";


	html = html + " </SCRIPT>";
	return html;


}



function getclose(formid){
	var html=""; 
	html = html + "<SCRIPT LANGUAGE='javascript'>";
	html = html + " var Qty='REC6544'; "; 
	html = html + "function Onclose_CL() ";
	html = html + " { ";   
	html = html + " alert('hi'); "; 
	//html = html + "window.opener.document._rf_cluster_no.custpage_items_custpage_lotbatch1_fs = 0;";
	//html = html + "window.opener.document.'main_form'.custpage_items_custpage_lotbatch1_fs.value = 0;";

	//html = html + "window.opener.document.getElementById('custpage_items_custpage_lotbatch1_fs').value = '0';self.close();";
	html = html + "window.opener.document.getElementById('custpage_lotbatch1').value = 'RVC1235';self.close();";
	//html = html + "window.close();";
	html = html + "return false;"
	html = html + "}";		
	html = html + " </SCRIPT>";
	return html;

}









