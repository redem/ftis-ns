/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_itemhistory_sch.js,v $
 *     	   $Revision: 1.1.4.2 $
 *     	   $Date: 2014/07/08 13:49:16 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_eBN_2014_1_StdBundle_3_151 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_itemhistory_sch.js,v $
 * Revision 1.1.4.2  2014/07/08 13:49:16  snimmakayala
 * 20149246
 *
 * Revision 1.1.2.1  2014/07/02 13:39:54  snimmakayala
 * Case # : 20149246
 * Inventory Snap Shot Scheduler
 *
 *
 **************************************************************************************************/

function ItemHistory(type)
{
	nlapiLogExecution('DEBUG', 'Into ItemHistory', type);
	var context = nlapiGetContext();

	nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());

	try
	{	

		var vToDay = DateStamp();

		//var vToDay = '7/1/2014';

		var vinvtsnapdet = getInvtSnapshotDetails(0,vToDay);
		if(vinvtsnapdet!=null && vinvtsnapdet!='')
		{
			nlapiLogExecution('DEBUG','vinvtsnapdet length',vinvtsnapdet.length);
		}
		var vputawayDet = getPutawayDetails(vToDay);
		if(vputawayDet!=null && vputawayDet!='')
		{
			nlapiLogExecution('DEBUG','vputawayDet length',vputawayDet.length);
		}
		var vshipDet = getShipDetails(vToDay);
		if(vshipDet!=null && vshipDet!='')
		{
			nlapiLogExecution('DEBUG','vshipDet length',vshipDet.length);
		}
		var vinvadjDet = getAdjustments(vToDay);
		if(vinvadjDet!=null && vinvadjDet!='')
		{
			nlapiLogExecution('DEBUG','vinvadjDet length',vinvadjDet.length);
		}
		var vinvtdet = getInvtDetails(0,vToDay) ;
		if(vinvtdet!=null && vinvtdet!='')
		{
			nlapiLogExecution('DEBUG','vinvtdet length',vinvtdet.length);
		}

		if(vinvtsnapdet!=null && vinvtsnapdet!='')
		{
			for(i=0;i<vinvtsnapdet.length;i++)
			{
				var vitem = vinvtsnapdet[i].getValue('custrecord_ebiz_invsnap_item',null,'group');
				var vlocation = vinvtsnapdet[i].getValue('custrecord_ebiz_invsnap_location',null,'group');
				var vopbal = vinvtsnapdet[i].getValue('custrecord_ebiz_invsnap_qtyonhand',null,'sum');

				var vqtyputaway = 0;
				var vqtyshipped = 0;
				var vqtyadjusted = 0;
				var vqtybuilt = 0;
				var vclosebal = 0;

				for(j=0;vputawayDet!=null&&j<vputawayDet.length;j++)
				{
					if(vitem == vputawayDet[j][1] && vlocation ==  vputawayDet[j][0])
					{
						vqtyputaway+=parseFloat(vputawayDet[j][2]);
					}
				}

				for(k=0;vshipDet!=null&&k<vshipDet.length;k++)
				{
					if(vitem == vshipDet[k][1] && vlocation ==  vshipDet[k][0])
					{
						vqtyshipped+=parseFloat(vshipDet[k][2]);
					}
				}

				for(l=0;vinvadjDet!=null&&l<vinvadjDet.length;l++)
				{
					if(vitem == vinvadjDet[l][1] && vlocation ==  vinvadjDet[l][0])
					{
						vqtyadjusted+=parseFloat(vinvadjDet[l][2]);
					}
				}

				for(m=0;vinvtdet!=null&&m<vinvtdet.length;m++)
				{
					var vinvtitem = vinvtdet[m].getValue('custrecord_ebiz_inv_sku',null,'group');
					var vinvtlocation = vinvtdet[m].getValue('custrecord_ebiz_inv_loc',null,'group');
					var vinvtqoh = vinvtdet[m].getValue('custrecord_ebiz_qoh',null,'sum');

					if(vitem == vinvtitem && vlocation ==  vinvtlocation)
					{
						vclosebal+=parseFloat(vinvtqoh);
					}
				}

				var invtRec = nlapiCreateRecord('customrecord_ebiz_itemsummary');	

				invtRec.setFieldValue('custrecord_ebiz_ihdate', vToDay);
				invtRec.setFieldValue('custrecord_ebiz_ihitem', vitem);
				invtRec.setFieldValue('custrecord_ebiz_ihlocation', vlocation);
				invtRec.setFieldValue('custrecord_ebiz_ihopbalance', parseFloat(vopbal));
				invtRec.setFieldValue('custrecord_ebiz_ihqtyputaway', parseFloat(vqtyputaway));
				invtRec.setFieldValue('custrecord_ebiz_ihqtyshipped', parseFloat(vqtyshipped));
				invtRec.setFieldValue('custrecord_ebiz_ihqtyadjusted', parseFloat(vqtyadjusted));
				invtRec.setFieldValue('custrecord_ebiz_ihqtybuilt', parseFloat(vqtybuilt));
				invtRec.setFieldValue('custrecord_ebiz_ihclosebalance', parseFloat(vclosebal));

				var invtrecid = nlapiSubmitRecord(invtRec, false, true);	

				if(context.getRemainingUsage()<100 && (i<vinvtsnapdet.length-1))
				{				
					setRecoveryPoint()

					var state = nlapiYieldScript();
					if( state.status == 'FAILURE')
					{
						nlapiLogExecution("DEBUG","Failed to yield script, exiting: Reason = "+state.reason + " / Size = "+ state.size);
					} 
					else if ( state.status == 'RESUME' )
					{
						nlapiLogExecution("DEBUG", "Resuming script because of " + state.reason+".  Size = "+ state.size);
					}
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in ItemHistory', exp);
	}
	nlapiLogExecution('DEBUG', 'Out of ItemHistory', type);

} 

function setRecoveryPoint()
{
	var state = nlapiSetRecoveryPoint(); //100 point governance
	nlapiLogExecution("DEBUG",'state status',state.status);
	if( state.status == 'SUCCESS' ) return;  //we successfully create a new recovery point
	if( state.status == 'RESUME' ) //a recovery point was previously set, we are resuming due to some unforeseen error
	{
		nlapiLogExecution("DEBUG", "Resuming script because of " + state.reason+".  Size = "+ state.size);
		//handleScriptRecovery();
	}
	else if ( state.status == 'FAILURE' )  //we failed to create a new recovery point
	{
		nlapiLogExecution("DEBUG","Failed to create recovery point. Reason = "+state.reason + " / Size = "+ state.size);
		//handleRecoveryFailure(state);
	}
}

var allinvdet = new Array();

function getInvtSnapshotDetails(maxid,vToDay) 
{
	nlapiLogExecution('DEBUG','Into getInvtSnapshotDetails',maxid);
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_invsnap_date', null, 'on', vToDay));

	if(parseFloat(maxid)>1)
	{
		//nlapiLogExecution('DEBUG','In side internalid in search filter',maxid);
		filters.push(new nlobjSearchFilter('internalidnumber','custrecord_ebiz_invsnap_item','lessthan', maxid));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_invsnap_item',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_invsnap_location',null,'group');	
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_invsnap_qtyonhand',null,'sum');	
	columns[3] = new nlobjSearchColumn('internalid','custrecord_ebiz_invsnap_item','group');	
	columns[3].setSort(true);

	var invtSearchResults = nlapiSearchRecord('customrecord_ebiz_inventory_sanpshot', null, filters, columns);


	if(invtSearchResults!=null)
	{
		nlapiLogExecution('ERROR','invtSearchResults',invtSearchResults.length);

		if(invtSearchResults.length>=1000)
		{
			var maxno1=invtSearchResults[invtSearchResults.length-1].getValue('internalid','custrecord_ebiz_invsnap_item','group');
			for(k=0;k<invtSearchResults.length;k++)
			{
				allinvdet.push(invtSearchResults[k]);
			}

			getInvtSnapshotDetails(maxno1,vToDay);

		}
		else
		{			
			for(k=0;k<invtSearchResults.length;k++)
			{
				allinvdet.push(invtSearchResults[k]);
			}
		}
	}
	
	nlapiLogExecution('DEBUG','Out of getInvtSnapshotDetails');

	return allinvdet;
}

function getPutawayDetails(vToDay)
{
	nlapiLogExecution('DEBUG','Into getPutawayDetails',vToDay);
	
	var putawayDetArr = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'on', vToDay));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[2]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',[3]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[2] = new nlobjSearchColumn('custrecord_act_qty',null,'sum');

	var openTaskDet = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(openTaskDet!=null)
	{
		for(k=0;k<openTaskDet.length;k++)
		{
			var vloc = openTaskDet[k].getValue('custrecord_wms_location',null,'group');
			var vItem = openTaskDet[k].getValue('custrecord_sku',null,'group');
			var vQty = openTaskDet[k].getValue('custrecord_act_qty',null,'sum');

			var curRow = [vloc,vItem,vQty]; 
			putawayDetArr.push(curRow);
		} 

	}

	var ctfilters = new Array();

	ctfilters.push(new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'on',  vToDay));
	ctfilters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof',[2]));
	ctfilters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof',[3]));

	var ctcolumns = new Array();
	ctcolumns[0] = new nlobjSearchColumn('custrecord_ebiztask_wms_location',null,'group');
	ctcolumns[1] = new nlobjSearchColumn('custrecord_ebiztask_sku',null,'group');
	ctcolumns[2] = new nlobjSearchColumn('custrecord_ebiztask_act_qty',null,'sum');

	var closeTaskDet = new nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, ctfilters, ctcolumns);
	if(closeTaskDet!=null)
	{
		for(k=0;k<closeTaskDet.length;k++)
		{
			var vloc = closeTaskDet[k].getValue('custrecord_ebiztask_wms_location',null,'group');
			var vItem = closeTaskDet[k].getValue('custrecord_ebiztask_sku',null,'group');
			var vQty = closeTaskDet[k].getValue('custrecord_ebiztask_act_qty',null,'sum');

			var curRow = [vloc,vItem,vQty]; 
			putawayDetArr.push(curRow);
		} 

	}
	
	nlapiLogExecution('DEBUG','Out of getPutawayDetails');

	return putawayDetArr;
}

function getShipDetails(vToDay)
{
	nlapiLogExecution('DEBUG','Into getShipDetails',vToDay);
	
	var shipDetArr = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'on',  vToDay));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',[7,8,10,14,28]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[2] = new nlobjSearchColumn('custrecord_act_qty',null,'sum');

	var openTaskDet = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(openTaskDet!=null)
	{
		for(k=0;k<openTaskDet.length;k++)
		{
			var vloc = openTaskDet[k].getValue('custrecord_wms_location',null,'group');
			var vItem = openTaskDet[k].getValue('custrecord_sku',null,'group');
			var vQty = openTaskDet[k].getValue('custrecord_act_qty',null,'sum');

			var curRow = [vloc,vItem,vQty]; 
			shipDetArr.push(curRow);
		} 

	}

	var ctfilters = new Array();

	ctfilters.push(new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'on',  vToDay));
	ctfilters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof',[3]));
	ctfilters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof',[7,8,10,14,28]));

	var ctcolumns = new Array();
	ctcolumns[0] = new nlobjSearchColumn('custrecord_ebiztask_wms_location',null,'group');
	ctcolumns[1] = new nlobjSearchColumn('custrecord_ebiztask_sku',null,'group');
	ctcolumns[2] = new nlobjSearchColumn('custrecord_ebiztask_act_qty',null,'sum');

	var closeTaskDet = new nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, ctfilters, ctcolumns);
	if(closeTaskDet!=null)
	{
		for(k=0;k<closeTaskDet.length;k++)
		{
			var vloc = closeTaskDet[k].getValue('custrecord_ebiztask_wms_location',null,'group');
			var vItem = closeTaskDet[k].getValue('custrecord_ebiztask_sku',null,'group');
			var vQty = closeTaskDet[k].getValue('custrecord_ebiztask_act_qty',null,'sum');

			var curRow = [vloc,vItem,vQty]; 
			shipDetArr.push(curRow);
		} 

	}
	
	nlapiLogExecution('DEBUG','Out of getShipDetails');

	return shipDetArr;
}

function getAdjustments(vToDay)
{
	nlapiLogExecution('DEBUG','Into getAdjustments',vToDay);
	
	var invtadjuarr = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_recorddate', null, 'on', vToDay));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_siteid',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_ebizskuno',null,'group');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_adjustqty',null,'sum');

	var invadjDetails = new nlapiSearchRecord('customrecord_ebiznet_invadj', null, filters, columns);
	if(invadjDetails!=null && invadjDetails!='')
	{
		for (var s = 0; s < invadjDetails.length; s++)
		{
			var vloc = invadjDetails[s].getValue('custrecord_ebiz_siteid',null,'group');
			var vItem = invadjDetails[s].getValue('custrecord_ebizskuno',null,'group');
			var vQty = invadjDetails[s].getValue('custrecord_ebiz_adjustqty',null,'sum');

			var curRow = [vloc,vItem,vQty]; 
			invtadjuarr.push(curRow);					
		}
	}
	
	nlapiLogExecution('DEBUG','Out of getAdjustments',vToDay);

	return invtadjuarr;
}


var allinvtqohdet = new Array();

function getInvtDetails(maxid,vToDay) 
{
	nlapiLogExecution('DEBUG','Into getInvtDetails',maxid);
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));

	if(parseFloat(maxid)>1)
	{
		//nlapiLogExecution('DEBUG','In side internalid in search filter',maxid);
		filters.push(new nlobjSearchFilter('internalidnumber','custrecord_ebiz_inv_sku','lessthan', maxid));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_loc',null,'group');	
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');	
	columns[3] = new nlobjSearchColumn('internalid','custrecord_ebiz_inv_sku','group');	
	columns[3].setSort(true);

	var invtSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);


	if(invtSearchResults!=null)
	{
		nlapiLogExecution('ERROR','invtSearchResults',invtSearchResults.length);

		if(invtSearchResults.length>=1000)
		{
			var maxno1=invtSearchResults[invtSearchResults.length-1].getValue('internalid','custrecord_ebiz_inv_sku','group');
			for(k=0;k<invtSearchResults.length;k++)
			{
				allinvtqohdet.push(invtSearchResults[k]);
			}

			getInvtDetails(maxno1,vToDay);

		}
		else
		{			
			for(k=0;k<invtSearchResults.length;k++)
			{
				allinvtqohdet.push(invtSearchResults[k]);
			}
		}
	}

	nlapiLogExecution('DEBUG','Out of getInvtDetails');
	
	return allinvtqohdet;
}
