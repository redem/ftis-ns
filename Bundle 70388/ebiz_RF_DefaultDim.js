/***************************************************************************
���������������������������eBizNET
���������������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_DefaultDim.js,v $
*� $Revision: 1.1.2.2.4.3.4.4.2.1 $
*� $Date: 2014/08/06 07:45:33 $
*� $Author: rmukkera $
*� $Name: t_eBN_2014_2_StdBundle_0_30 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_DefaultDim.js,v $
*� Revision 1.1.2.2.4.3.4.4.2.1  2014/08/06 07:45:33  rmukkera
*� Case # 20149847
*�
*� Revision 1.1.2.2.4.3.4.4  2014/06/13 06:53:27  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.1.2.2.4.3.4.3  2014/05/30 00:26:49  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.1.2.2.4.3.4.2  2013/06/11 14:30:41  schepuri
*� Error Code Change ERROR to DEBUG
*�
*� Revision 1.1.2.2.4.3.4.1  2013/04/17 16:04:01  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.1.2.2.4.3  2012/11/01 14:55:35  schepuri
*� CASE201112/CR201113/LOG201121
*� Decimal Qty Conversions
*�
*� Revision 1.1.2.2.4.2  2012/09/27 10:56:31  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting multiple language with given Spanish terms
*�
*� Revision 1.1.2.2.4.1  2012/09/21 14:57:16  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.1.2.2  2012/06/11 13:32:01  spendyala
*� CASE201112/CR201113/LOG201121
*� Reducing size of the text box.
*�
*� Revision 1.1.2.1  2012/06/02 09:08:07  spendyala
*� CASE201112/CR201113/LOG201121
*� New script file.
*�
*
****************************************************************************/

/**
 * @param request
 * @param response
 */
function DefaultDim(request, response)
{
	if (request.getMethod() == 'GET') 
	{    
		//	Get the PO#, PO Line Item, Line#, Entered Item and PO Internal Id 
		//  from the previous screen, which is passed as a parameter
		
		
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		
		
		var st0,st1,st2,st3,st4,st5,st6,st7,st8;
		
		if( getLanguage == 'es_ES')
		{
			st0 = "RECEPCI&#211;N DE MEN&#218;";
			st1 = "ART&#205;CULO";
			st2 = "DESCRIPCI&#211;N DEL ART&#205;CULO";
			st3 = "LONGITUD";
			st4 = "ANCHO";
			st5 = "ALTURA";
			st6 = "PESO";
			st7 = "ENVIAR";
			st8 = "ANTERIOR";

			
			
		}
		else
		{
			st0 = "RECEIVING MENU";
			st1 = "ITEM";
			st2 = "ITEM DESCRIPTION";
			st3 = "LENGTH";
			st4 = "WIDTH";
			st5 = "HEIGHT";
			st6 = "WEIGHT";
			st7 = "SEND";
			st8 = "PREV";
		}

		
		
		
		
		

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var trantype= request.getParameter('custparam_trantype');
		var whCompany= request.getParameter('custparam_company');
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		var whLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG','WH Location', whLocation);
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		nlapiLogExecution('DEBUG','getPOItem', getPOItem);
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getItemBaseUomQty=request.getParameter('custparam_baseuomqty');
		var getitemdesc= request.getParameter('custparam_itemdescription');
		var enteredOption = request.getParameter('custparam_enteredOption');
		nlapiLogExecution('DEBUG','getFetchedItemId', getFetchedItemId);
		nlapiLogExecution('DEBUG','getPOLineNo', getPOLineNo);

		try
		{
			var poItemFields = ['custitem_ebizdimprompt','custitem_ebizweightprompt'];
			var rec = nlapiLookupField('item', getFetchedItemId, poItemFields);

			var promtDim=rec.custitem_ebizdimprompt;
			var promtWt=rec.custitem_ebizweightprompt;
			nlapiLogExecution('DEBUG','promtDim',promtDim);
			nlapiLogExecution('DEBUG','promtWt',promtWt);

			var itemdimFilter=new Array();
			itemdimFilter.push(new nlobjSearchFilter('custrecord_ebizitemdims',null,'anyof',getFetchedItemId));
			itemdimFilter.push(new nlobjSearchFilter('custrecord_ebizbaseuom',null,'is','T'));

			var itemdimColumn=new Array();
			itemdimColumn[0]=new nlobjSearchColumn('custrecord_ebizlength');
			itemdimColumn[1]=new nlobjSearchColumn('custrecord_ebizwidth');
			itemdimColumn[2]=new nlobjSearchColumn('custrecord_ebizheight');
			itemdimColumn[3]=new nlobjSearchColumn('custrecord_ebizweight');

			var searchrecItemDim=nlapiSearchRecord('customrecord_ebiznet_skudims',null,itemdimFilter,itemdimColumn);

			if(searchrecItemDim!=null&&searchrecItemDim!="")
			{
				var length=searchrecItemDim[0].getValue('custrecord_ebizlength');
				var width=searchrecItemDim[0].getValue('custrecord_ebizwidth');
				var height=searchrecItemDim[0].getValue('custrecord_ebizheight');
				var weight=searchrecItemDim[0].getValue('custrecord_ebizweight');
				var recid=searchrecItemDim[0].getId();
			}

		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG','Exception in fetching default dims',exp);
		}
		var functionkeyHtml=getFunctionkeyScript('_rf_defaultdim'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('cmdSend').focus();";        
		html = html + "</script>";        
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_defaultdim' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" +  st1 + " :<label>" + getPOItem + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" +  st2 + " :<label>" + getitemdesc + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		if(promtDim=='T'&&promtWt=='F')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> " + st3;
//			html = html + "				</td>";
			html = html + "				<input name='enterlength' type='text' size='4' value='"+length+"'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> " + st4;
//			html = html + "				</td>";
			html = html + "				&nbsp&nbsp<input name='enterwidth' type='text' size='4' value='"+width+"'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> " + st5;
//			html = html + "				</td>";
			html = html + "				&nbsp<input name='enterheight' type='text' size='4' value='"+height+"'/>";
			html = html + "				</td>";
			html = html + "				<td align = 'left'><input name='enterweight' type='hidden' value='"+weight+"'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		else if(promtDim=='F'&&promtWt=='T')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterlength' type='hidden' value='"+length+"'/>";
			html = html + "				</td>";
			html = html + "				<td align = 'left'><input name='enterwidth' type='hidden' value='"+width+"'/>";
			html = html + "				</td>";
			html = html + "				<td align = 'left'><input name='enterheight' type='hidden' value='"+height+"'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> " + st6;
			html = html + "				<input name='enterweight' type='text' size='4' value='"+weight+"'/>";
			html = html + "			</tr>";
		}
		else if(promtDim=='T'&&promtWt=='T')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> " + st6;
			html = html + "				<input name='enterweight' type='text' size='4' value='"+weight+"'/>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> " + st3;
			//html = html + "				</td>";
			html = html + "				<input name='enterlength' type='text' size='4' value='"+length+"'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> " + st4;
			//html = html + "				</td>";
			html = html + "				&nbsp&nbsp<input name='enterwidth' type='text' size='4' value='"+width+"'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> " + st5;
			//html = html + "				</td>";
			html = html + "				&nbsp<input name='enterheight' type='text' size='4' value='"+height+"'/>";
			html = html + "				</td>";
			html = html + "			</tr>";

		}
		html = html + "			<tr>";
		html = html + "				<td>";
		html = html + "				<input type='hidden' name='hdnrecid' value=" + recid + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnWhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		html = html + "				<input type='hidden' name='hdngetPONo' value=" + getPONo + ">";
		html = html + "				<input type='hidden' name='hdngetPOItem' value=" + getPOItem + ">";
		html = html + "				<input type='hidden' name='hdngetPOLineNo' value=" + getPOLineNo + ">";
		html = html + "				<input type='hidden' name='hdngetFetchedItemId' value=" + getFetchedItemId + ">";
		html = html + "				<input type='hidden' name='hdngetPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdngetItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdngetItemBaseUomQty' value=" + getItemBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdngetitemdesc' value=" + getitemdesc + ">";
		html = html + "				<input type='hidden' name='hdnEnteredOption' value=" + enteredOption + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st7 + " <input name='cmdSend' id='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st8 +  " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		var POarray = new Array();
		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
		POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
		POarray["custparam_option"] = request.getParameter('hdnOptedField');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_company"] = request.getParameter('hdnWhCompany');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		POarray["custparam_poid"] = request.getParameter('hdngetPONo');
		POarray["custparam_poitem"] = request.getParameter('hdngetPOItem');
		POarray["custparam_lineno"] = request.getParameter('hdngetPOLineNo');
		POarray["custparam_fetcheditemid"] = request.getParameter('hdngetFetchedItemId');
		POarray["custparam_pointernalid"] = request.getParameter('hdngetPOInternalId');
		POarray["custparam_itemcube"] = request.getParameter('hdngetItemCube');
		POarray["custparam_baseuomqty"] = request.getParameter('hdngetItemBaseUomQty');
		POarray["custparam_itemdescription"] = request.getParameter('hdngetitemdesc');
		POarray["custparam_enteredOption"] = request.getParameter('hdnEnteredOption');
		
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
		
//		POarray["custparam_error"] = 'INVALID ITEM';
//		POarray["custparam_screenno"] = '2';
		var optedEvent = request.getParameter('cmdPrevious');
		if (optedEvent == 'F7') {

			response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false, POarray);
		}
		else{
			//updating the Itemdim record
			try{
				var BaseUOMlength=request.getParameter('enterlength');
				var BaseUOMwidth=request.getParameter('enterwidth');
				var BaseUOMheight=request.getParameter('enterheight');
				var BaseUOMweight=request.getParameter('enterweight');
				var BaseUOMCube=parseFloat(BaseUOMlength)*parseFloat(BaseUOMwidth)*parseFloat(BaseUOMheight);
				var BaseUOMid=request.getParameter('hdnrecid');
				nlapiLogExecution('DEBUG','BaseUOMlength',BaseUOMlength);
				nlapiLogExecution('DEBUG','BaseUOMwidth',BaseUOMwidth);
				nlapiLogExecution('DEBUG','BaseUOMheight',BaseUOMheight);
				nlapiLogExecution('DEBUG','BaseUOMCube',BaseUOMCube);
				nlapiLogExecution('DEBUG','BaseUOMid',BaseUOMid);
				var valueArray=new Array();
				valueArray[0]=parseFloat(BaseUOMlength).toFixed(4);
				valueArray[1]=parseFloat(BaseUOMwidth).toFixed(4);
				valueArray[2]=parseFloat(BaseUOMheight).toFixed(4);
				valueArray[3]=parseFloat(BaseUOMweight).toFixed(4);
				valueArray[4]=parseFloat(BaseUOMCube).toFixed(4);

				var FieldsArray=new Array();
				FieldsArray[0]='custrecord_ebizlength';
				FieldsArray[1]='custrecord_ebizwidth';
				FieldsArray[2]='custrecord_ebizheight';
				FieldsArray[3]='custrecord_ebizweight';
				FieldsArray[4]='custrecord_ebizcube';
				if(BaseUOMid!=null&&BaseUOMid!="")
					var recid=nlapiSubmitField('customrecord_ebiznet_skudims',BaseUOMid,FieldsArray,valueArray);
				nlapiLogExecution('DEBUG','ItemDim successfull',recid);
				response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false, POarray);
			}
			catch(exp)
			{
				nlapiLogExecution('DEBUG','Exception in itemdim updation',exp);
			}
		}
	}
}