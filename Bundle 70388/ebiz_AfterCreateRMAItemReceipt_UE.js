/***************************************************************************
 eBizNET Solutions
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/UserEvents/Attic/ebiz_AfterCreateRMAItemReceipt_UE.js,v $
 *  $Revision: 1.1.4.3.2.8.2.5 $
 *  $Date: 2015/06/22 13:32:40 $
 *  $Author: schepuri $
 *  $Name: t_eBN_2015_1_StdBundle_1_154 $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: ebiz_AfterCreateRMAItemReceipt_UE.js,v $
 *  Revision 1.1.4.3.2.8.2.5  2015/06/22 13:32:40  schepuri
 *  case# 201413071
 *
 *  Revision 1.1.4.3.2.8.2.4  2015/06/17 12:34:23  schepuri
 *  case# 201413071
 *
 *  Revision 1.1.4.3.2.8.2.3  2015/03/20 15:33:15  grao
 *  LP SB issue fixes201412116   ,201412128
 *
 *  Revision 1.1.4.3.2.8.2.2  2014/11/14 08:17:06  rrpulicherla
 *  Std fixes
 *
 *  Revision 1.1.4.3.2.8.2.1  2014/09/18 15:22:13  skreddy
 *  case # 201410414
 *  TPP SB issue fix
 *
 *  Revision 1.1.4.3.2.8  2014/06/09 15:21:50  sponnaganti
 *  case# 20148488
 *  Stnd Bundle Issue Fix
 *
 *  Revision 1.1.4.3.2.7  2014/05/21 14:34:33  snimmakayala
 *  Case #: 20148458
 *
 *  Revision 1.1.4.3.2.6  2014/04/29 15:14:56  rmukkera
 *  Case # 20148071
 *
 *  Revision 1.1.4.3.2.5  2014/04/22 16:35:52  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 *
 *  Item receipt
 *
 *  Revision 1.1.4.3.2.3  2013/05/21 15:06:51  skreddy
 *  CASE201112/CR201113/LOG201121
 *  Standard bundle Issue Fixes
 *
 *  Revision 1.1.4.3.2.2  2013/05/20 10:33:48  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 *  RMA issue in production Accounts
 *
 *  Revision 1.1.4.3.2.1  2013/03/19 11:46:47  snimmakayala
 *  CASE201112/CR201113/LOG2012392
 *  Production and UAT issue fixes.
 *
 *  Revision 1.1.4.3  2013/02/19 10:50:36  snimmakayala
 *  CASE201112/CR201113/LOG2012392
 *  Production Issue fixes.
 *
 *  Revision 1.1.4.2  2013/01/16 08:53:36  spendyala
 *  CASE201112/CR201113/LOG201121
 *  New script for RMA item receipt.
 *
 *
 ****************************************************************************/

function AfterRMAItemreceiptCreate(type) 
{
	var vLocation=nlapiGetFieldText('location');
	nlapiLogExecution('ERROR', 'vLocation', vLocation);

	for(var z = 0; z < nlapiGetLineItemCount('item'); z++)
	{
		var vLineLoc = nlapiGetLineItemValue('item','location',  z + 1);
		nlapiLogExecution('ERROR','vLineLoc',vLineLoc);
	}
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','getExecutionContext',context.getExecutionContext());
	//&& context.getExecutionContext() == 'webservices'
	if(type=='create' )
	{
		var EndLocationId;
		var invtlp;
		var itemid;
		var itemstatus;
		var itempackcode;
		var quantity;
		var batchno; 
		var itemdesc;
		var whLocation;
		var getlotnoid="";
		var expdate='';
		var fifodate="";
		var id=nlapiGetFieldValue('id');
		var form=nlapiGetFieldValue('customform');
		var pointid=nlapiGetFieldValue('createdfrom');
		var vLocation=nlapiGetFieldValue('location');		
		var trantype = nlapiLookupField('transaction', pointid, 'recordType');

		var str = 'id. = ' + id + '<br>';
		str = str + 'trantype. = ' + trantype + '<br>';	
		str = str + 'form. = ' + form + '<br>';	
		str = str + 'pointid. = ' + pointid + '<br>';
		str = str + 'vLocation. = ' + vLocation + '<br>';

		nlapiLogExecution('DEBUG', 'Parameter Details1', str);

		//case # 20148071 start
		var searchresultsopentask = null;

		if(pointid !=null && pointid!='')
		{
			var filters = new Array();		
			filters[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid);

			searchresultsopentask = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			nlapiLogExecution('DEBUG','searchresultsopentask',searchresultsopentask);
		}

		var mwhsiteflag;
		if(context.getExecutionContext()=='userinterface')
		{
			try
			{
				if(vLocation!=null && vLocation!='')
				{
					var fields = ['custrecord_ebizwhsite'];

					var locationcolumns = nlapiLookupField('Location', vLocation, fields);
					if(locationcolumns!=null)
						mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
				}

				var reStockFlag = 'T'; // case# 201413071
				if(trantype=='returnauthorization' && mwhsiteflag=='T' && searchresultsopentask==null )
				{
					//case # 20148071 end
					for(var t = 0; t < nlapiGetLineItemCount('item'); t++)
					{
						var Itemreceiptcheck=nlapiGetLineItemValue('item','itemreceive', t + 1);
						reStockFlag = nlapiGetLineItemValue('item','restock', t + 1);

						nlapiLogExecution('ERROR','reStockFlag first',reStockFlag);

						if(Itemreceiptcheck=='T' )
						{
							if(reStockFlag != 'F')
							{
								var validinvtlp=nlapiGetLineItemValue('item','custcol_lp', t + 1);	
								nlapiLogExecution('ERROR','validinvtlp',validinvtlp);
								whLocation=nlapiGetLineItemValue('item','location',  t + 1);
								nlapiLogExecution('ERROR','whLocation',whLocation);
								nlapiLogExecution('ERROR','vLocation',vLocation);
								if(vLocation!=whLocation)
								{
									var cannotDelError = nlapiCreateError('CannotDelete',
											'RMA Location and Line Location diffrent for Line#'+ t+1, true);
									throw cannotDelError; 
								}
								var LPValidate=ebiznet_LPRangePutw_CL(validinvtlp,whLocation);
								nlapiLogExecution('ERROR','LPValidate',LPValidate);
								if(LPValidate!='Y')
								{
									var cannotDelError = nlapiCreateError('CannotDelete',
											'Enter Valid LP# In Line#'+ t+1, true);
									throw cannotDelError;  
								}
								else
								{
									var filtersmlp = new Array();
									filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', validinvtlp);
									filtersmlp[1] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', whLocation);
									var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

									if (SrchRecord != null && SrchRecord.length > 0) {
										var cannotDelError = nlapiCreateError('CannotDelete',
												'LP# Already Exists In Line#'+ t+1, true);
										throw cannotDelError;  
									}
								}
							}
						}

					}
					for(var i =0; i < nlapiGetLineItemCount('item'); i++)
					{
						var BoolInvMerged=false;

						var Itemreceiptcheckval = nlapiGetLineItemValue('item','itemreceive', i + 1);
						EndLocationId = nlapiGetLineItemValue('item','custcol_locationsitemreceipt',  i + 1);
						invtlp = nlapiGetLineItemValue('item','custcol_lp', i + 1);
						reStockFlag = nlapiGetLineItemValue('item','restock', i + 1);
						nlapiLogExecution('ERROR','reStockFlag',reStockFlag);
						if(Itemreceiptcheckval=='T')
						{
							if(reStockFlag != 'F')
							{
								if((EndLocationId!=null && EndLocationId!='') && (invtlp!=null && invtlp!='')  )
								{
									if(invtlp==null || invtlp=='')
										invtlp=nlapiGetLineItemValue( 'item','custcol_ebiz_lp', i + 1);
									itemid=nlapiGetLineItemValue('item', 'item', i + 1);
									itemdesc=nlapiGetLineItemText('item', 'item', i + 1);
									itemstatus=nlapiGetLineItemValue('item','custcol_ebiznet_item_status',  i + 1);
									itempackcode=nlapiGetLineItemValue('item','custcol_nswmspackcode',  i + 1);
									if(itempackcode==null || itempackcode=='')
										itempackcode='1';
									quantity=nlapiGetLineItemValue('item','quantity',  i + 1);
									batchno=nlapiGetLineItemValue('item','serialnumbers', i + 1);
									whLocation=nlapiGetLineItemValue('item','location',  i + 1);

									var str = 'EndLocationId. = ' + EndLocationId + '<br>';
									str = str + 'invtlp. = ' + invtlp + '<br>';	
									str = str + 'itemid. = ' + itemid + '<br>';	
									str = str + 'itemdesc. = ' + itemdesc + '<br>';
									str = str + 'itemstatus. = ' + itemstatus + '<br>';
									str = str + 'itempackcode. = ' + itempackcode + '<br>';
									str = str + 'quantity. = ' + quantity + '<br>';
									str = str + 'whLocation. = ' + whLocation + '<br>';
									str = str + 'reStockFlag. = ' + reStockFlag + '<br>';

									nlapiLogExecution('DEBUG', 'Parameter Details2', str);


									var fields = ['recordType', 'custitem_ebizbatchlot'];
									var itemRec = nlapiLookupField('item', itemid, fields);
									var itemType = itemRec.recordType;
									var batchflag = itemRec.custitem_ebizbatchlot;
									if(invtlp==null || invtlp=='')
										invtlp=GetMaxLPNo(1, 1,whLocation);
									nlapiLogExecution('ERROR','systemgeneratedlp',invtlp);
									if((EndLocationId!=null && EndLocationId!='') && (invtlp!=null && invtlp!=''))
									{
										var LPValidate=ebiznet_LPRangePutw_CL(invtlp,whLocation);
										nlapiLogExecution('ERROR','LPValidate',LPValidate);

										if(LPValidate=='Y')
										{
											nlapiLogExecution('ERROR','itemType',itemType);
											try {
												nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');

												nlapiLogExecution('DEBUG', 'LP NOT FOUND');
												var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
												customrecord.setFieldValue('name', invtlp);
												customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', invtlp);
												customrecord.setFieldValue('custrecord_ebiz_lpmaster_site', whLocation);
												var rec = nlapiSubmitRecord(customrecord, false, true);

											} 
											catch (e) {
												nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
											}

											if(itemType=='kititem')
											{
												//**********************************************************************************//

												var searchresultsitem = nlapiLoadRecord(itemType, itemid);
												var SkuNo=searchresultsitem.getFieldValue('itemid');
												nlapiLogExecution('DEBUG', 'SkuNo', SkuNo);
												var recCount=0; 
												var filters = new Array(); 			 
												filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);	
												filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');	
												filters[2] = new nlobjSearchFilter('type', null, 'is', 'Kit');

												var columns1 = new Array(); 
												columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
												columns1[1] = new nlobjSearchColumn( 'memberquantity' );

												var itemsearchresults = nlapiSearchRecord( 'item', null, filters, columns1 );

												for(var m=0; m<itemsearchresults.length;m++) 
												{ 
													var subitemid = itemsearchresults[m].getValue('memberitem');
													var subitemqty = itemsearchresults[m].getValue('memberquantity');
													var vsubitemputwqty = parseFloat(quantity)*parseFloat(subitemqty);

													var str = 'subitemid. = ' + subitemid + '<br>';
													str = str + 'subitemqty. = ' + subitemqty + '<br>';	
													str = str + 'receiveqty. = ' + quantity + '<br>';	
													str = str + 'vsubitemputwqty. = ' + vsubitemputwqty + '<br>';

													nlapiLogExecution('DEBUG', 'Component Details', str);

													try
													{
														var varPaltQty = getMaxUOMQty(subitemid,itempackcode);
														nlapiLogExecution('ERROR', 'varPaltQty', varPaltQty);

														var fields = ['recordType', 'custitem_ebizbatchlot'];
														var subitemRec = nlapiLookupField('item', subitemid, fields);
														var subitemType = subitemRec.recordType;
														var subbatchflag = subitemRec.custitem_ebizbatchlot;

														if(subitemType == "lotnumberedinventoryitem" || subitemType=="lotnumberedassemblyitem" || subbatchflag == "T")
														{
															nlapiLogExecution('ERROR', 'batchno', batchno);
															if(batchno!="" && batchno!=null)
															{
																var filterspor = new Array();
																filterspor[0] = new nlobjSearchFilter('name', null, 'is', batchno);
																if(whLocation!=null && whLocation!="")
																	filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);
																if(subitemid!=null && subitemid!="")
																	filterspor[2] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', subitemid);

																var column=new Array();
																column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');

																var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
																if(receiptsearchresults!=null)
																{
																	getlotnoid= receiptsearchresults[0].getId();
																	nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
																	expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
																}
																nlapiLogExecution('ERROR', 'expdate', expdate);
															}
														}

														var filtersinvt = new Array();

														if(subitemid!=null&&subitemid!="")
															filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', subitemid));

														if(itemstatus!=null&&itemstatus!="")
															filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemstatus));

														if(EndLocationId!=null&&EndLocationId!="")
															filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', EndLocationId));

														filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', '19'));

														if(fifodate!=null && fifodate!='')
															filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', fifodate));

														if(itempackcode!=null && itempackcode!='')
															filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', itempackcode));							

														if(getlotnoid!=null && getlotnoid!='')
															filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', getlotnoid));

														if(varPaltQty!=null && varPaltQty!='')
															filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'lessthan', varPaltQty));							

														var columnsinvt = new Array();
														columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
														columnsinvt[0].setSort();

														var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

														if(invtsearchresults != null && invtsearchresults != ''  && invtsearchresults.length > 0)
														{
															nlapiLogExecution('ERROR', 'invtsearchresults.length', invtsearchresults.length);
															var newputqty=vsubitemputwqty;
															var putQty=vsubitemputwqty;

															for (var k = 0; k < invtsearchresults.length; k++) 
															{
																var qoh=invtsearchresults[k].getValue('custrecord_ebiz_qoh');

																nlapiLogExecution('ERROR', 'exiting qoh', qoh);
																nlapiLogExecution('ERROR', 'putQty', putQty);
																nlapiLogExecution('ERROR', 'varPaltQty', varPaltQty);

																if(newputqty>0 && (parseInt(putQty)+parseInt(qoh))<=parseInt(varPaltQty))
																{
																	nlapiLogExecution('ERROR', 'Inventory Record ID', invtsearchresults[k].getId());
																	BoolInvMerged=true;
																	nlapiLogExecution('ERROR', 'BoolInvMerged', BoolInvMerged);
																	var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[k].getId());											

																	var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
																	var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
																	var varExistLP=invttransaction.getFieldValue('custrecord_ebiz_inv_lp');

																	nlapiLogExecution('ERROR', 'varExistQOHQty', varExistQOHQty);
																	nlapiLogExecution('ERROR', 'varExistInvQty', varExistInvQty);

																	var varupdatedQOHqty = parseInt(varExistQOHQty) + parseInt(putQty);
																	var varupdatedInvqty = parseInt(varExistInvQty) + parseInt(putQty);

																	invttransaction.setFieldValue('custrecord_ebiz_qoh', parseInt(varupdatedQOHqty));
																	invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseInt(varupdatedInvqty));
																	invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

																	nlapiSubmitRecord(invttransaction,false, true);

																	nlapiLogExecution('ERROR', 'nlapiSubmitRecord', 'Record Submitted');
																	nlapiLogExecution('ERROR', 'Inventory is merged to the LP '+varExistLP);
																	newputqty=0;
																}
															}
														}
													}
													catch(exp)
													{
														nlapiLogExecution('ERROR','Exception in Inventory merge',exp);
													}
													nlapiLogExecution('ERROR','BoolInvMerged',BoolInvMerged);
													if(BoolInvMerged==false)
													{
														var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

														var filters=new Array();
														filters.push(new nlobjSearchFilter('internalid', null, 'is', EndLocationId));
														filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
														filters.push(new nlobjSearchFilter('isinactive', 'custrecord_inboundlocgroupid', 'is', 'F'));
														filters.push(new nlobjSearchFilter('isinactive', 'custrecord_outboundlocgroupid', 'is', 'F'));
														var columns=new Array();
														columns.push(new nlobjSearchColumn('custrecord_inboundlocgroupid'));
														columns.push(new nlobjSearchColumn('custrecord_outboundlocgroupid'));
														var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
														var inblocgroupid='';var outlocgroupid='';
														if(searchresults != null && searchresults != '' && searchresults.length>0)
														{	
															nlapiLogExecution('ERROR', 'Into Update location groups');
															inblocgroupid = searchresults[0].getValue('custrecord_inboundlocgroupid');
															outlocgroupid = searchresults[0].getValue('custrecord_outboundlocgroupid');
														}
														invtRec.setFieldValue('name', invtlp);
														invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
														invtRec.setFieldValue('custrecord_inboundinvlocgroupid', inblocgroupid);
														invtRec.setFieldValue('custrecord_outboundinvlocgroupid', outlocgroupid);
														invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
														invtRec.setFieldValue('custrecord_ebiz_inv_sku', subitemid);
														invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
														if(itempackcode!=null&&itempackcode!="")
															invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
														invtRec.setFieldValue('custrecord_ebiz_inv_qty', vsubitemputwqty);
														invtRec.setFieldValue('custrecord_inv_ebizsku_no', subitemid);
														invtRec.setFieldValue('custrecord_ebiz_qoh', vsubitemputwqty);
														invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
														invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
														invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
														invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
														invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');
														invtRec.setFieldValue('custrecord_invttasktype', '2');
														invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);

														var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
														var columns = nlapiLookupField('item', subitemid, fields);
														var ItemType = columns.recordType;					
														var batchflg = columns.custitem_ebizbatchlot;
														var itemfamId= columns.custitem_item_family;
														var itemgrpId= columns.custitem_item_group;
														nlapiLogExecution('ERROR', 'ItemType', ItemType);
														nlapiLogExecution('ERROR', 'batchflg', batchflg);
														nlapiLogExecution('ERROR', 'batchno', batchno);
														if(getlotnoid!=null && getlotnoid!='')
														{
															var fifovalue = FifovalueCheck(ItemType,subitemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
															invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
														}
														if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
														{				
															if (getlotnoid != "") {
																fifovalue = FifovalueCheck(ItemType,subitemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
																nlapiLogExecution('ERROR','FIFO Value Check.',fifovalue);
																invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
																invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
																invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
															}

														}
														else
															invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());


														nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORDS');

														var invtrecid = nlapiSubmitRecord(invtRec, false, true);

													}

													var vNewWHlocatin;
													if(itemstatus!=null && itemstatus!="")
													{
														var Itemstatusvalue = nlapiLoadRecord('customrecord_ebiznet_sku_status', itemstatus);
														if(Itemstatusvalue!= null && Itemstatusvalue != "")
														{
															vNewWHlocatin=Itemstatusvalue.getFieldValue('custrecord_item_status_location_map');

														} 
													}
													nlapiLogExecution('ERROR', 'vNewWHlocatin', vNewWHlocatin);
													nlapiLogExecution('ERROR', 'whLocation', whLocation);
													if(whLocation!=vNewWHlocatin)
													{
														InvokeNSInventoryTransfer(subitemid,itemstatus,whLocation,vNewWHlocatin,vsubitemputwqty,null);
													}
												}

												//**********************************************************************************//

											}
											else
											{

												//code to merge inventory if exist
												try
												{
													var varPaltQty = getMaxUOMQty(itemid,itempackcode);
													nlapiLogExecution('ERROR', 'varPaltQty', varPaltQty);

													if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
													{
														nlapiLogExecution('ERROR', 'batchno', batchno);
														if(batchno!="" && batchno!=null)
														{
															var filterspor = new Array();
															filterspor[0] = new nlobjSearchFilter('name', null, 'is', batchno);
															if(whLocation!=null && whLocation!="")
																filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);
															if(itemid!=null && itemid!="")
																filterspor[2] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid);

															var column=new Array();
															column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');

															var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
															if(receiptsearchresults!=null)
															{
																getlotnoid= receiptsearchresults[0].getId();
																nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
																expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
															}
															nlapiLogExecution('ERROR', 'expdate', expdate);
														}
													}

													var filtersinvt = new Array();

													if(itemid!=null&&itemid!="")
														filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemid));

													if(itemstatus!=null&&itemstatus!="")
														filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemstatus));

													if(EndLocationId!=null&&EndLocationId!="")
														filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', EndLocationId));

													filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', '19'));

													if(fifodate!=null && fifodate!='')
														filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', fifodate));

													if(itempackcode!=null && itempackcode!='')
														filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', itempackcode));							

													if(getlotnoid!=null && getlotnoid!='')
														filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', getlotnoid));

													if(varPaltQty!=null && varPaltQty!='')
														filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'lessthan', varPaltQty));							

													var columnsinvt = new Array();
													columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
													columnsinvt[0].setSort();

													var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

													if(invtsearchresults != null && invtsearchresults != ''  && invtsearchresults.length > 0)
													{
														nlapiLogExecution('ERROR', 'invtsearchresults.length', invtsearchresults.length);
														var newputqty=quantity;
														var putQty=quantity;

														for (var k = 0; k < invtsearchresults.length; k++) 
														{
															var qoh=invtsearchresults[k].getValue('custrecord_ebiz_qoh');

															nlapiLogExecution('ERROR', 'exiting qoh', qoh);
															nlapiLogExecution('ERROR', 'putQty', putQty);
															nlapiLogExecution('ERROR', 'varPaltQty', varPaltQty);

															if(newputqty>0 && (parseInt(putQty)+parseInt(qoh))<=parseInt(varPaltQty))
															{
																nlapiLogExecution('ERROR', 'Inventory Record ID', invtsearchresults[k].getId());
																BoolInvMerged=true;
																nlapiLogExecution('ERROR', 'BoolInvMerged', BoolInvMerged);
																var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[k].getId());											

																var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
																var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
																var varExistLP=invttransaction.getFieldValue('custrecord_ebiz_inv_lp');

																nlapiLogExecution('ERROR', 'varExistQOHQty', varExistQOHQty);
																nlapiLogExecution('ERROR', 'varExistInvQty', varExistInvQty);

																var varupdatedQOHqty = parseInt(varExistQOHQty) + parseInt(putQty);
																var varupdatedInvqty = parseInt(varExistInvQty) + parseInt(putQty);

																invttransaction.setFieldValue('custrecord_ebiz_qoh', parseInt(varupdatedQOHqty));
																invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseInt(varupdatedInvqty));
																invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

																nlapiSubmitRecord(invttransaction,false, true);

																nlapiLogExecution('ERROR', 'nlapiSubmitRecord', 'Record Submitted');
																nlapiLogExecution('ERROR', 'Inventory is merged to the LP '+varExistLP);
																newputqty=0;
															}
														}
													}
												}
												catch(exp)
												{
													nlapiLogExecution('ERROR','Exception in Inventory merge',exp);
												}
												nlapiLogExecution('ERROR','BoolInvMerged',BoolInvMerged);
												if(BoolInvMerged==false)
												{
													var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

													var filters=new Array();
													filters.push(new nlobjSearchFilter('internalid', null, 'is', EndLocationId));
													filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
													filters.push(new nlobjSearchFilter('isinactive', 'custrecord_inboundlocgroupid', 'is', 'F'));
													filters.push(new nlobjSearchFilter('isinactive', 'custrecord_outboundlocgroupid', 'is', 'F'));
													var columns=new Array();
													columns.push(new nlobjSearchColumn('custrecord_inboundlocgroupid'));
													columns.push(new nlobjSearchColumn('custrecord_outboundlocgroupid'));
													var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
													var inblocgroupid='';var outlocgroupid='';
													if(searchresults != null && searchresults != '' && searchresults.length>0)
													{	
														nlapiLogExecution('ERROR', 'Into Update location groups');
														inblocgroupid = searchresults[0].getValue('custrecord_inboundlocgroupid');
														outlocgroupid = searchresults[0].getValue('custrecord_outboundlocgroupid');
													}
													invtRec.setFieldValue('name', invtlp);
													invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
													invtRec.setFieldValue('custrecord_inboundinvlocgroupid', inblocgroupid);
													invtRec.setFieldValue('custrecord_outboundinvlocgroupid', outlocgroupid);

													invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
													invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
													invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
													if(itempackcode!=null&&itempackcode!="")
														invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
													invtRec.setFieldValue('custrecord_ebiz_inv_qty', quantity);
													invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
													invtRec.setFieldValue('custrecord_ebiz_qoh', quantity);
													invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
													invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
													invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
													invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
													invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
													invtRec.setFieldValue('custrecord_invttasktype', '2');
													invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);

													var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
													var columns = nlapiLookupField('item', itemid, fields);
													var ItemType = columns.recordType;					
													var batchflg = columns.custitem_ebizbatchlot;
													var itemfamId= columns.custitem_item_family;
													var itemgrpId= columns.custitem_item_group;
													nlapiLogExecution('ERROR', 'ItemType', ItemType);
													nlapiLogExecution('ERROR', 'batchflg', batchflg);
													nlapiLogExecution('ERROR', 'batchno', batchno);

													if(getlotnoid!=null && getlotnoid!='')
													{
														var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
														invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
													}
													if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
													{ 
														//Checking FIFO Policy.					
														if (getlotnoid != "") {
															fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
															nlapiLogExecution('ERROR','FIFO Value Check.',fifovalue);
															invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
															invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
															invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
														}

													}
													else
														invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());

													nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORDS');

													var invtrecid = nlapiSubmitRecord(invtRec, false, true);
												}

												var vNewWHlocatin;
												if(itemstatus!=null && itemstatus!="")
												{
													var Itemstatusvalue = nlapiLoadRecord('customrecord_ebiznet_sku_status', itemstatus);
													if(Itemstatusvalue!= null && Itemstatusvalue != "")
													{
														vNewWHlocatin=Itemstatusvalue.getFieldValue('custrecord_item_status_location_map');

													} 
												}
												nlapiLogExecution('ERROR', 'vNewWHlocatin', vNewWHlocatin);
												nlapiLogExecution('ERROR', 'whLocation', whLocation);
												if(vNewWHlocatin!=null && vNewWHlocatin!='' && whLocation!=vNewWHlocatin)
												{
													InvokeNSInventoryTransfer(itemid,itemstatus,whLocation,vNewWHlocatin,quantity,null);
												}
											}
										}
										else
										{
											var cannotDelError = nlapiCreateError('CannotDelete',
													'Invalid LP#', true);
											throw cannotDelError;  
										}
									}
									else
									{
										var cannotDelError = nlapiCreateError('Cannot Receive',
												'Please Enter Values for Binlocation and LP#', true);
										throw cannotDelError; 
									}
								}
								else
								{
									if((EndLocationId==null || EndLocationId=='') || (invtlp==null || invtlp==''))
									{
										var cannotDelError = nlapiCreateError('Cannot Receive',
												'Please Enter Values for Binlocation and LP#', true);
										throw cannotDelError; 
									}
								}
							}
						}
					}
				}
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR','Exception in RMA',exp);

				var cannotReceiveError = nlapiCreateError('Cannot Receive', exp , true);
				throw cannotReceiveError; //throw this error object, do not catch it

			}
		}
	}
} 

function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot)
{

	nlapiLogExecution('ERROR', 'Into InvokeNSInventoryTransfer');		
	nlapiLogExecution('ERROR', 'item', item);		
	nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
	nlapiLogExecution('ERROR', 'loc', loc);
	nlapiLogExecution('ERROR', 'toloc', toloc);
	nlapiLogExecution('ERROR', 'qty', qty);
	nlapiLogExecution('ERROR', 'lot', lot);

	var ItemType='';

	if(item!= null && item != "")
	{
		var columns = nlapiLookupField('item', item, [ 'recordType','custitem_ebizserialout' ]);
		ItemType = columns.recordType;
	}

	nlapiLogExecution('ERROR', 'ItemType', ItemType);
	var confirmLotToNS='Y';
	confirmLotToNS=GetConfirmLotToNS(loc);
	var vItemname;	

	var filters = new Array();          
	filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
	// Changed On 30/4/12 by Suman
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	var columns = new Array();		
	columns[0] = new nlobjSearchColumn('itemid');
	var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
	if (itemdetails !=null) 
	{
		vItemname=itemdetails[0].getValue('itemid');
	}

	var invttransfer = nlapiCreateRecord('inventorytransfer');			

	invttransfer.setFieldValue('location', loc);//from Location
	invttransfer.setFieldValue('transferlocation', toloc);
	invttransfer.selectNewLineItem('inventory');
	invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
	invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('ERROR', 'subs', subs);
	if(subs==true)
	{
		var vSubsidiaryVal=getSubsidiaryNew(loc);
		nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
		if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);			
	}

	if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem") 
		//if(lot!=null && lot!='')
	{
		//vItemname=vItemname.replace(" ","-");
		vItemname=vItemname.replace(/ /g,"-");
		if(parseFloat(qty)<0)
			qty=parseFloat(qty)*(-1);
		if(confirmLotToNS=='N')
			lot=vItemname;		

		nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
		invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  lot + "(" + parseFloat(qty) + ")");
	}

	invttransfer.commitLineItem('inventory'); 
	nlapiSubmitRecord(invttransfer, true, true);
	nlapiLogExecution('ERROR', 'Out of InvokeNSInventoryTransfer');

}


function GetConfirmLotToNS(Site)
{
	var filters = new Array();

	filters[0] = new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'WMSLOT');
	filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
	columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

	columns[1].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
	if(searchresults != null && searchresults != '')
	{
		if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
		{
			return searchresults[0].getValue('custrecord_ebizrulevalue');
		}
		else
			return 'Y';
	}
	else
		return 'Y';

}

function ebiznet_LPRangePutw_CL(lpno,whLocation){

	var type = "PALT";
	var vargetlpno = lpno;
	var vResult = "";
	nlapiLogExecution('ERROR', 'whLocation',whLocation);
	if ((vargetlpno!=null && vargetlpno!='') && (vargetlpno.length > 0)) {

		var filters=new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', type);
		filters[1] = new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', whLocation);
		filters[2] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', '2');
		filters[3]=new nlobjSearchFilter('isinactive', null, 'is', 'F');
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, column);

		if (searchresults) {
			for (var i = 0; i < Math.min(50, searchresults.length); i++) {
				try {
					nlapiLogExecution('ERROR', 'searchresults',searchresults.length);
					var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');

					var recid = searchresults[i].getId();

					var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recid);

					var varBeginLPRange = transaction.getFieldValue('custrecord_ebiznet_lprange_begin');

					var varEndRange = transaction.getFieldValue('custrecord_ebiznet_lprange_end');

					var getLPGenerationTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lpgentype');

					var getLPTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lptype');

					if (getLPTypeValue == "1") {
						var getLPrefix = transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix');

						var LPprefixlen = getLPrefix.length;
						var vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();


						if (vLPLen == getLPrefix) {

							var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);

							if (varnum.length > varEndRange.length) {

								vResult = "N";
								break;
							}


							if ((parseFloat(varnum,10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum,10) > parseFloat(varEndRange))) {

								vResult = "N";
								break;
							}
							else {
								vResult = "Y";
								break;
							}
						}
						else {
							vResult = "N";
						}

					} //end of if statement
					else {
						vResult = "N";
					}

				} 
				catch (err) {
					// alert("exception" + err + "value is ");

				}
			} //end of for loop
		}


	}
	return vResult;
}


function getMaxUOMQty(putItemId,putItemPC)
{
	var varPaltQty=0;
	var searchresults = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', putItemId));
	if(putItemPC!=null && putItemPC!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', putItemPC));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	columns[1].setSort(true);

	searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	if(searchresults != null && searchresults != '' && searchresults.length > 0)
	{
		varPaltQty = searchresults[0].getValue('custrecord_ebizqty');
	}

	return varPaltQty;
}
