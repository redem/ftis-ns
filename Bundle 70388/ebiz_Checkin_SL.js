/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************

 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_Checkin_SL.js,v $

 *     	   $Revision: 1.45.2.25.4.6.2.56.2.2 $
 *     	   $Date: 2015/11/23 15:37:37 $
 *     	   $Author: grao $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_177 $

 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Checkin_SL.js,v $
 * Revision 1.45.2.25.4.6.2.56.2.2  2015/11/23 15:37:37  grao
 * 2015.2 Issue Fixes 201415811
 *
 * Revision 1.45.2.25.4.6.2.56.2.1  2015/09/21 10:40:13  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.45.2.25.4.6.2.56  2015/06/11 13:33:53  schepuri
 * case# 201412982
 *
 * Revision 1.45.2.25.4.6.2.55  2015/04/13 09:26:10  skreddy
 * Case# 201412323
 * changed the url path which was hard coded
 *
 * Revision 1.45.2.25.4.6.2.54  2014/10/27 13:34:13  vmandala
 * Case# 201410766 Stdbundle issue fixed
 *
 * Revision 1.45.2.25.4.6.2.53  2014/09/24 15:59:30  sponnaganti
 * Case# 201410440
 * Stnd Bundle Issue fix
 *
 * Revision 1.45.2.25.4.6.2.52  2014/09/16 16:02:32  skavuri
 * Case# 201410373 Std Bundle Issue Fixed
 *
 * Revision 1.45.2.25.4.6.2.51  2014/08/22 15:24:58  skavuri
 * Case# 201410123 	Clever Training Issue Fixed
 *
 * Revision 1.45.2.25.4.6.2.50  2014/07/28 16:16:49  sponnaganti
 * Case# 20149733
 * Stnd Bundle Issue fix
 *
 * Revision 1.45.2.25.4.6.2.49  2014/07/18 15:12:52  sponnaganti
 * Case# 20148739
 * Stnd Bundle Issue fix
 *
 * Revision 1.45.2.25.4.6.2.48  2014/06/27 16:02:02  sponnaganti
 * case# 20148871
 * Stnd Bundle Issue Fix
 *
 * Revision 1.45.2.25.4.6.2.47  2014/06/16 15:21:01  sponnaganti
 * case# 20148871
 * Stnd Bundle Issue fix
 *
 * Revision 1.45.2.25.4.6.2.46  2014/06/12 14:23:46  grao
 * Case#: 20148778  New GUI account issue fixes
 *
 * Revision 1.45.2.25.4.6.2.45  2014/04/10 17:17:55  skavuri
 * cASE # 20127929  ISSUE FIXED
 *
 * Revision 1.45.2.25.4.6.2.44  2014/04/03 16:07:42  skavuri
 * Case # 20127929 issue fixed
 *
 * Revision 1.45.2.25.4.6.2.43  2014/02/21 15:55:10  nneelam
 * case#  20127189
 * Standard Bundle Issue Fix.
 *
 * Revision 1.45.2.25.4.6.2.42  2014/02/19 06:29:20  nneelam
 * case#  20127189
 * Standard Bundle Issue Fix.
 *
 * Revision 1.45.2.25.4.6.2.41  2014/02/17 15:34:18  nneelam
 * case#  20127189
 * Standard Bundle Issue Fix.
 *
 * Revision 1.45.2.25.4.6.2.40  2014/02/13 15:34:46  grao
 * Case# 20127111 related issue fixes in Standard bundle issue fixes
 *
 * Revision 1.45.2.25.4.6.2.39  2014/02/10 16:35:39  skavuri
 * Case#20127061
 *
 * Revision 1.45.2.25.4.6.2.38  2014/01/24 14:50:27  rmukkera
 * Case # 20126882
 *
 * Revision 1.45.2.25.4.6.2.37  2013/12/20 16:09:18  gkalla
 * case#20126475
 * Standard bundle issue
 *
 * Revision 1.45.2.25.4.6.2.36  2013/11/29 15:06:40  grao
 * Case# 20125984  related issue fixes in SB 2014.1
 *
 * Revision 1.45.2.25.4.6.2.35  2013/11/01 13:54:54  schepuri
 * 20125090
 *
 * Revision 1.45.2.25.4.6.2.34  2013/11/01 12:49:59  rmukkera
 * Case# 20124936
 *
 * Revision 1.45.2.25.4.6.2.33  2013/10/30 07:36:45  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20124515
 * Empty location check during putaway location generation.
 *
 * Revision 1.45.2.25.4.6.2.32  2013/10/15 15:28:10  grao
 * issue fixes related to the 20124971
 *
 * Revision 1.45.2.25.4.6.2.31  2013/10/08 15:49:20  rmukkera
 * Case# 20124850
 *
 * Revision 1.45.2.25.4.6.2.30  2013/10/07 13:48:15  schepuri
 * Allowing more than PO overage expected quantity
 *
 * Revision 1.45.2.25.4.6.2.29  2013/10/04 15:44:35  rmukkera
 * Case# 20124672
 *
 * Revision 1.45.2.25.4.6.2.28  2013/09/19 15:20:21  rmukkera
 * Case# 20124445
 *
 * Revision 1.45.2.25.4.6.2.27  2013/09/18 15:27:26  nneelam
 * Case#. 20124475
 * UI Check In allowed for quantity more than PO Overage.
 *
 * Revision 1.45.2.25.4.6.2.26  2013/09/11 15:23:51  rmukkera
 * Case# 20124376
 *
 * Revision 1.45.2.25.4.6.2.25  2013/09/06 07:32:45  rmukkera
 * Case# 20123969
 *
 * Revision 1.45.2.25.4.6.2.24  2013/09/02 15:38:37  grao
 * Issue fixes related to the 20124216
 *
 * Revision 1.45.2.25.4.6.2.23  2013/08/30 16:40:33  skreddy
 * Case# 20124143
 * standard bundle issue fix
 *
 * Revision 1.45.2.25.4.6.2.22  2013/08/21 16:50:30  grao
 * Issue Fix related to
 * 20123932
 *
 * Revision 1.45.2.25.4.6.2.21  2013/08/13 15:58:05  nneelam
 * Case#.  20123885
 * Standard Bundle Issue Fixed..
 *
 * Revision 1.45.2.25.4.6.2.20  2013/08/13 15:22:06  rmukkera
 * Issue Fix related to 20123771,20123770,20122320
 *
 * Revision 1.45.2.25.4.6.2.19  2013/08/01 23:48:00  skreddy
 * Case# 20123693
 * issue rellated to expiry date
 *
 * Revision 1.45.2.25.4.6.2.18  2013/07/24 22:01:49  grao
 * Ryonet Issue fixes  Case#:20123596
 *
 * Revision 1.45.2.25.4.6.2.17  2013/07/06 01:01:08  skreddy
 * Case# 20123235
 * UOM conversions, display UOM text
 *
 * Revision 1.45.2.25.4.6.2.16  2013/06/27 16:37:06  skreddy
 * Case#:20123183
 * POline level locations
 *
 * Revision 1.45.2.25.4.6.2.15  2013/06/24 15:34:12  skreddy
 * CASE201112/CR201113/LOG201121
 * standard bundle issue fixes
 *
 * Revision 1.45.2.25.4.6.2.14  2013/06/04 07:26:55  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Parsing CR for PCT
 *
 * Revision 1.45.2.25.4.6.2.13  2013/05/16 13:39:58  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.45.2.25.4.6.2.12  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.45.2.25.4.6.2.11  2013/05/01 13:05:01  rmukkera
 * Fix related filetering the binlocations in the sublist
 *
 * Revision 1.45.2.25.4.6.2.10  2013/05/01 12:44:10  rmukkera
 * Autobreakdownrelated changes were done
 *
 * Revision 1.45.2.25.4.6.2.9  2013/04/24 15:20:31  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.45.2.25.4.6.2.8  2013/04/23 15:29:43  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.45.2.25.4.6.2.7  2013/04/16 13:21:32  spendyala
 * CASE201112/CR201113/LOG2012392
 * Updating item pallet dimension when ever user changes the dimensions of EACH
 *
 * Revision 1.45.2.25.4.6.2.6  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.45.2.25.4.6.2.5  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.45.2.25.4.6.2.4  2013/03/19 11:46:47  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.45.2.25.4.6.2.3  2013/03/05 13:35:46  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.45.2.25.4.6.2.2  2013/03/01 14:34:59  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.45.2.25.4.6.2.1  2013/02/26 12:52:09  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.45.2.25.4.6  2013/02/07 08:51:27  skreddy
 * CASE201112/CR201113/LOG201121
 * GUI Lot auto generating FIFO enhancement
 *
 * Revision 1.45.2.25.4.5  2013/01/08 14:17:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.45.2.25.4.4  2012/12/03 15:38:53  rmukkera
 * CASE201112/CR201113/LOG2012392
 * Issue fix Unexpected suitescript error
 *
 * Revision 1.45.2.25.4.3  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.45.2.25.4.2  2012/10/03 11:31:30  schepuri
 * CASE201112/CR201113/LOG201121
 * Auto Gen Lot No
 *
 * Revision 1.45.2.25.4.1  2012/09/26 21:35:05  svanama
 * CASE201112/CR201113/LOG201121
 * item label generation code added
 *
 * Revision 1.45.2.25  2012/09/11 15:38:09  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Check in issue
 *
 * Revision 1.45.2.24  2012/09/03 12:59:08  schepuri
 * CASE201112/CR201113/LOG201121
 * issue related to fetch putstrategy
 *
 * Revision 1.45.2.23  2012/08/24 18:41:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changesbackup
 *
 * Revision 1.45.2.21  2012/08/22 14:50:07  schepuri
 * CASE201112/CR201113/LOG201121
 * QC customscript name is corrected
 *
 * Revision 1.45.2.20  2012/08/21 22:43:24  spendyala
 * CASE201112/CR201113/LOG201121
 * Updating UPC code depending upon confirmation by user.
 *
 * Revision 1.45.2.19  2012/08/03 13:20:48  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue related to query string
 *
 * Revision 1.45.2.18  2012/07/30 23:20:38  gkalla
 * CASE201112/CR201113/LOG201121
 * NSUOM issue
 *
 * Revision 1.45.2.17  2012/07/27 13:00:57  schepuri
 * CASE201112/CR201113/LOG201121
 * issue There are no records of this type
 *
 * Revision 1.45.2.16  2012/06/02 09:32:45  spendyala
 * CASE201112/CR201113/LOG201121
 * Driving user to update the ItemDim when Prompt for Dimensions(Check box)
 * or  Prompt for Weight (Check box) are checked in item master.
 *
 * Revision 1.45.2.15  2012/05/07 13:46:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Fetching Pallet qty from item dimensions issue is resolved.
 *
 * Revision 1.45.2.14  2012/05/03 14:45:02  mbpragada
 * CASE201112/CR201113/LOG201121
 * While we are performing Check In Exception for Qty ,
 *  Exception Qty is not updated in Closed task.
 *
 * Revision 1.45.2.13  2012/04/25 15:46:55  spendyala
 * CASE201112/CR201113/LOG201121
 * While Showing Recommended qty POoverage is not considered.
 *
 * Revision 1.45.2.12  2012/04/20 12:45:38  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.45.2.11  2012/04/20 09:35:32  vrgurujala
 * t_NSWMS_LOG201121_120
 *
 * Revision 1.45.2.10  2012/04/16 15:00:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Calculating POoverage is moved to general function.
 *
 * Revision 1.45.2.9  2012/04/13 22:21:51  spendyala
 * CASE201112/CR201113/LOG201121
 * Depending upon POoverage Value mentioned in the item master
 * value of poOverage will be calculated accordingly.
 *
 * Revision 1.45.2.8  2012/03/29 06:40:37  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * checkin pickfaceloc
 *
 * Revision 1.45.2.6  2012/03/27 13:11:48  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.45.2.5  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.54  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.53  2012/02/21 07:26:45  spendyala
 * CASE201112/CR201113/LOG201121
 * code merged upto 1.45.2.4
 *
 * Revision 1.52  2012/02/08 13:59:39  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.51  2012/01/30 14:36:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.50  2012/01/30 07:05:05  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.49  2012/01/27 06:47:02  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM Processing
 *
 * Revision 1.48  2012/01/25 14:14:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM Processing
 *
 * Revision 1.47  2012/01/24 06:51:18  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM Processing
 *
 * Revision 1.46  2012/01/23 11:04:36  spendyala
 * CASE201112/CR201113/LOG201121
 *  added search column in fetchPalletQuantity fn to return the record whose UOM Level is highest for a particular item.
 *
 * Revision 1.45  2012/01/06 13:23:17  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing, for serialized inventory item name fetching in sublist
 *
 * Revision 1.44  2012/01/04 15:13:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.43  2011/12/27 23:24:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.42  2011/12/27 17:16:00  gkalla
 * CASE201112/CR201113/LOG201121
 * For PO Overage
 *
 * Revision 1.41  2011/12/21 22:25:24  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.40  2011/12/15 14:09:02  schepuri
 * CASE201112/CR201113/LOG201121
 * modified field names in customrecord_ebiznet_trn_poreceipt
 *
 * Revision 1.39  2011/12/02 09:41:04  schepuri
 * CASE201112/CR201113/LOG201121
 * To generate location acc to the location defined in PO
 *
 * Revision 1.38  2011/11/18 22:57:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Delete record from inventory if QOH becomes 0
 *
 * Revision 1.37  2011/11/17 22:40:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Receiving for lotassembledbuilditems.
 *
 * Revision 1.36  2011/11/17 08:35:53  spendyala
 * CASE2011263/CR2011260/LOG2011188
 * wms status flag for create inventory has been changed to 17 i.e.,FLAG-INVENTORY_INBOUND
 *
 * Revision 1.35  2011/10/20 09:56:30  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Get Stage locations code is merged with TNT prod code
 *
 * Revision 1.34  2011/10/20 09:45:02  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Changed are done to support the check in functionality for Decimal values.System should not
 * give any issue if the purchase order qty is in negetives.
 *
 * Revision 1.33  2011/10/14 06:58:47  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.31  2011/10/13 06:56:54  schepuri
 * CASE201112/CR201113/LOG201121
 * itemrecommended qty calculation removing unneccessary else condition
 *
 * Revision 1.30  2011/09/28 16:45:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.29  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.28  2011/09/10 11:26:57  schepuri
 * CASE201112/CR201113/LOG201121
 *
 * po overage value passing to client from sublist through item sublist
 *
 * Revision 1.27  2011/08/23 08:51:46  skota
 * CASE201112/CR201113/LOG201121
 * Variable 'poOverage' initialization changed from 'N' to 0 (zero) as the field type changed from checkbox to freeform text
 *
 * Revision 1.26  2011/08/19 14:15:02  schepuri
 * CASE201112/CR201113/LOG201121
 * PO Overage Acc to %
 *
 * Revision 1.25  2011/07/29 11:44:44  rmukkera
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.24  2011/07/21 06:58:37  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

/*
 * Get Transaction ID for the PO
 */
function getTranIDForPO(tranType, poID) {
	var tranID = null;
	var poRecord = nlapiLoadRecord(tranType, poID);
	if (poRecord != null)
		tranID = poRecord.getFieldValue('tranid');
	return tranID;
}

/*
 * Add PO as Hyperlink
 */
function addPOFieldAsHRef(form,request) {
	var poField = form.addField('custpage_po', 'select', 'Created From',
	'transaction');
	poField.setDefaultValue(request.getParameter('poID'));
	poField.setDisplayType('inline');
	poField.setLayoutType('startrow');
}

/*
 * Create a HREF for New PO Receipt. This will be next to the receipt drop down.
 */
function addPOReceiptURL(environment, poId, tranId, poLineNum) {
	var poReceiptURL = nlapiResolveURL('SUITELET','customscript_ebiznet_poreceipt', 'customdeploy_poreceipt');

	// Get the FQDN depending on the context environment
	/*var fqdn = getFQDNForHost(environment);

	var ctx = nlapiGetContext();

	if (fqdn == '')
		nlapiLogExecution('ERROR', 'Unable to retrieve FQDN based on context');
	else
		poReceiptURL = getFQDNForHost(ctx.getEnvironment()) + poReceiptURL;*/

	poReceiptURL = poReceiptURL + '&custparam_poid=' + poId;
	poReceiptURL = poReceiptURL + '&custparam_povalue=' + tranId;
	poReceiptURL = poReceiptURL + '&custparam_polinenum=' + poLineNum;

	return poReceiptURL;
}

/*
 * Add receipt field and fill data in drop down NOTE: This does not add the New
 * PO Receipt HREF
 */
function addReceiptFieldToForm(form,request) {
	var poRcptQtyArr = new Array;
	var selectionArr = new Array(); // temporary array to return rcptQty,
	// trailer, bol

	var receipt = form.addField('custpage_poreceipt', 'select', 'Receipt');

	var rcptFilters = new Array();
	rcptFilters[0] = new nlobjSearchFilter('custrecord_ebiz_poreceipt_pono', null, 'anyof', request.getParameter('poID'));
	rcptFilters[1] = new nlobjSearchFilter('custrecord_ebiz_poreceipt_lineno', null, 'is', request.getParameter('LineNum'));

	var rcptColumns = new Array();
	rcptColumns[0] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptno');
	rcptColumns[1] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptqty');
	rcptColumns[2] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_trailerno');
	rcptColumns[3] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_bol');

	var rcptSearchResults = nlapiSearchRecord(
			'customrecord_ebiznet_trn_poreceipt', null, rcptFilters,
			rcptColumns);

	if (rcptSearchResults != null) {
		var resultCount = rcptSearchResults.length;
		nlapiLogExecution('ERROR', 'rcptSearchResults.length', resultCount);
		for ( var i = 0; i < Math.min(500, resultCount); i++) {
			receipt.addSelectOption(rcptSearchResults[i].getValue('custrecord_ebiz_poreceipt_receiptno'),
					rcptSearchResults[i].getValue('custrecord_ebiz_poreceipt_receiptno'));
			poRcptQtyArr[i] = rcptSearchResults[i].getValue('custrecord_ebiz_poreceipt_receiptqty');
			nlapiLogExecution('ERROR', 'PO Rcpt Qty', poRcptQtyArr[i]);
		}

		receipt.setDefaultValue(rcptSearchResults[resultCount - 1].getValue('custrecord_ebiz_poreceipt_receiptno'));
		selectionArr[0] = poRcptQtyArr[resultCount - 1];
		selectionArr[1] = rcptSearchResults[resultCount - 1].getValue('custrecord_ebiz_poreceipt_trailerno');
		selectionArr[2] = rcptSearchResults[resultCount - 1].getValue('custrecord_ebiz_poreceipt_bol');
	}

	return selectionArr;
}

/*
 * Get total check in quantity
 */
function getTotalCheckinQuantity(poID, poLineNum) {
	// Define search filter to get checkin qty for particular CHKN & PO
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is',
			poID);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', 1);// CHKN
	// task
	// type
	filters[2] = new nlobjSearchFilter('custrecord_line_no', null, 'is',
			poLineNum);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty');

	var searchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',
			null, filters, columns);
	var checkInQty = 0;

	if (searchResults != null) {
		for ( var i = 0; i < searchResults.length; i++) {
			var searchResult = searchResults[i];
			checkInQty = checkInQty
			+ parseFloat(searchResult.getValue('custrecord_expe_qty'));
		}
	}

	nlapiLogExecution('ERROR', 'CheckIn Qty', checkInQty);

	return checkInQty;
}

/*
 * Adding item sublist for checkin
 */
function addItemSublistToForm(form, itemID, poLineNum, poSerialNos, poRcptQty, poItemStatus,poQtyChkn,
		poOverage,poItemUOM,poItemPackcode,vbaseuom,uomlevel,vbaseuomqty,vuomqty,location,vAutoLotgenFlag,CaptureExpiryDate,ShelfLife,poLineQty,poLineLocation,poItemUOMtext) {
	nlapiLogExecution('ERROR', 'CaptureExpiryDate', CaptureExpiryDate);
	nlapiLogExecution('ERROR', 'ShelfLife', ShelfLife);
	nlapiLogExecution('ERROR', 'poLineLocation', poLineLocation);

	if (poRcptQty == "" || poRcptQty == null || isNaN(poRcptQty))
		poRcptQty='';

	var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
	var columns = nlapiLookupField('item', itemID, fields);
	var ItemType;
	var serialInflg="F";		
	var batchflag="F";
	var vExpDate,vBatchNo;

	if(columns != null && columns != '')
	{
		ItemType = columns.recordType;	
		serialInflg = columns.custitem_ebizserialin;
		batchflag= columns.custitem_ebizbatchlot;
	}


	var itemRec="",itemName,itemDesc;

	if(vbaseuom==null || vbaseuom=='')
	{
		var eBizItemDims=geteBizItemDimensions(itemID);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					vbaseuom = eBizItemDims[z].getText('custrecord_ebizuomskudim');
					vbaseuomqty = eBizItemDims[z].getText('custrecord_ebizqty');
					uomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
					vuomqty = eBizItemDims[z].getText('custrecord_ebizqty');
				}
			}
		}
	}

	nlapiLogExecution('ERROR', 'vbaseuom', vbaseuom);
	nlapiLogExecution('ERROR', 'uomlevel', uomlevel);

	nlapiLogExecution('ERROR', 'CheckIn itemType', ItemType);
	/*if(ItemType == 'inventoryitem')
	{*/
	nlapiLogExecution('ERROR', 'CheckIn itemType1', ItemType);
	itemRec = nlapiLoadRecord(ItemType, itemID);
	itemName = itemRec.getFieldValue('itemid');
	itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
	if(itemDesc==null)
	{
		itemDesc = itemRec.getFieldValue('purchasedescription');
	}
	if(itemDesc==null)
	{
		itemDesc = itemRec.getFieldValue('displayname');
	}		
	nlapiLogExecution('ERROR', 'itemDesc new', itemDesc);

	/*}
	else if(ItemType == 'lotnumberedassemblyitem')
	{
		nlapiLogExecution('ERROR', 'CheckIn itemType lotnumberedassemblyitem', ItemType);
		itemRec = nlapiLoadRecord('lotnumberedassemblyitem', itemID);
		itemName = itemRec.getFieldValue('itemid');
		itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
	}
	else if(ItemType == 'lotnumberedinventoryitem')
	{
		nlapiLogExecution('ERROR', 'CheckIn itemType lotnumberedinventoryitem', ItemType);
		itemRec = nlapiLoadRecord('lotnumberedinventoryitem', itemID);
		itemName = itemRec.getFieldValue('itemid');
		itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
	}
	else if(ItemType == 'serializedassemblyitem')
	{
		nlapiLogExecution('ERROR', 'CheckIn itemType serializedassemblyitem', ItemType);
		itemRec = nlapiLoadRecord('serializedassemblyitem', itemID);
		itemName = itemRec.getFieldValue('itemid');
		itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
	}
	else if(ItemType == 'serializedinventoryitem')
	{
		nlapiLogExecution('ERROR', 'CheckIn itemType serializedinventoryitem', ItemType);
		itemRec = nlapiLoadRecord('serializedinventoryitem', itemID);
		itemName = itemRec.getFieldValue('itemid');
		itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
	}*/
	nlapiLogExecution('ERROR', 'CheckIn itemName', itemName);
	nlapiLogExecution('ERROR', 'request.getParameter(poSerialNos)',
			poSerialNos);

	var itemSubList = form.addSubList("custpage_items", "inlineeditor",
	"ItemList");
	itemSubList.addField("custpage_chkn_pfvalidateflag", "text", "validateflag")
	.setDisplayType('hidden');

	itemSubList.addField("custpage_chkn_line", "text", "Line #")
	.setDefaultValue(poLineNum);
	itemSubList.addField("custpage_chkn_sku", "text", "Item").setDisabled(true)
	.setDefaultValue(itemName);
	itemSubList.addField("custpage_chkn_itemdesc", "text", "Item Desc")
	.setDisabled(true).setDefaultValue(itemDesc);

	/*
	itemSubList.addField("custpage_chkn_skustatus", "select", "Item Status",
	"customrecord_ebiznet_sku_status").setDefaultValue(poItemStatus);
	 */

	//This code is added on 19th for populating sku status


	var vItemStatus = itemSubList.addField("custpage_chkn_skustatus", "select", "Item Status");


	var itemStatusFilters = new Array();
	itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
	itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
	if(poLineLocation != null && poLineLocation != '')
		itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',poLineLocation);
	else if(location != null && location != '')
		itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',location);
	var itemStatusColumns = new Array();
	itemStatusColumns[0] = new nlobjSearchColumn('internalid');
	itemStatusColumns[1] = new nlobjSearchColumn('name');
	itemStatusColumns[0].setSort();
	itemStatusColumns[1].setSort();

	var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);


	nlapiLogExecution('ERROR', 'Location infomartion for check in', location);


	if (itemStatusSearchResult != null){
		for (var i = 0; i < itemStatusSearchResult.length; i++) 
		{					
			vItemStatus.addSelectOption(itemStatusSearchResult[i].getValue('internalid'), itemStatusSearchResult[i].getValue('name'));		
		}
	}

	//upt to here
	nlapiLogExecution('ERROR', 'poItemStatus', poItemStatus);
	if(poItemStatus!=null && poItemStatus!='')
	{
		vItemStatus.setDefaultValue(poItemStatus);
	}

	nlapiLogExecution('ERROR', 'poItemPackcode', poItemPackcode);
	if (poItemPackcode != "" && poItemPackcode != null && poItemPackcode !='null') //Case# 201410123
	//if (poItemPackcode != "" && poItemPackcode != null)
		itemSubList.addField("custpage_chkn_pc", "text", "Pack Code")
		.setDefaultValue(poItemPackcode);
	else
		itemSubList.addField("custpage_chkn_pc", "text", "Pack Code")
		.setDefaultValue('1');
	itemSubList.addField("custpage_chkn_buom", "text", "Base UOM")
	.setDefaultValue(vbaseuom);
	//Case '20123235 Start ..to display UOM text
	if(poItemUOM==null || poItemUOM=='' )
		poItemUOMtext = vbaseuom;

	itemSubList.addField("custpage_chkn_units", "text", "Units")
	.setDefaultValue(poItemUOMtext);
	//Case '20123235 end
	if (poRcptQty != "" && poRcptQty != null)
		itemSubList.addField("custpage_pochkn_qty", "text", "Receive Qty")
		.setDefaultValue(poRcptQty);
	else
		itemSubList.addField("custpage_pochkn_qty", "text", "Receive Qty")
		.setMandatory(true);

	if (poRcptQty != "" && poRcptQty != null)
		itemSubList.addField("custpage_chkn_rcvngqty", "text", "Base Receive Qty")
		.setDefaultValue(poRcptQty);
	else
		itemSubList.addField("custpage_chkn_rcvngqty", "text", "Base Receive Qty").setDisabled(true);
	itemSubList.addField("custpage_chkn_lp", "text", "LP(License Plate)")
	.setMandatory(true);
	/*if(ItemType == 'inventoryitem')
	{
		itemSubList.addField("custpage_lotbatch", "text", "Batch/Lot Numbers").setDisabled(true);
	}
	else
	{
		itemSubList.addField("custpage_lotbatch", "text", "Batch/Lot Numbers").setDefaultValue(poSerialNos);;
	}*/


	if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
	{
		itemSubList.addField("custpage_lotbatch", "text", "LOT#").setDefaultValue(poSerialNos);;
	}
	else
	{
		itemSubList.addField("custpage_lotbatch", "text", "LOT#").setDisabled(true);
	}

	if(ItemType=="serializedinventoryitem")
	{
		itemSubList.addField("custpage_displayserialno", "text", "SerialNo#").setDefaultValue(poSerialNos);

	}
	else
	{
		itemSubList.addField("custpage_displayserialno", "text", "SerialNo#").setDisplayType('hidden');
	}
	if(CaptureExpiryDate == 'T')
	{itemSubList.addField("custpage_hdncatchexpdate", "text", "Expiry Date(MMDDYYYY)");
	//itemSubList.addField("custpage_expdateformat", "text", "Date Format").setDisabled(true).setDefaultValue('(MMDDYY)');
	}
	else
	{
		itemSubList.addField("custpage_hdncatchexpdate", "text", "Expiry Date(MMDDYYYY)").setDisplayType('hidden');
		//itemSubList.addField("custpage_expdateformat", "text", "Date Format").setDisplayType('hidden').setDefaultValue('(MMDDYY)');
	}

	itemSubList.addField("custpage_capexpirydateflag", "text", "CapExpiryFalg ").setDisplayType('hidden').setDefaultValue(CaptureExpiryDate);
	itemSubList.addField("custpage_shelflife", "text", "ShelfLife").setDisplayType('hidden').setDefaultValue(ShelfLife);

	itemSubList.addField("custpage_chkn_binlocation", "select", "Bin Location",
	"customrecord_ebiznet_location");
	if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
	{
		if (vAutoLotgenFlag != "" && vAutoLotgenFlag != null && vAutoLotgenFlag =='Y')
			itemSubList.addField("custpage_chkn_manufacturelot", "text", "MANUFACTURE LOT#");
	}


	itemSubList.addField("custpage_iteminternalid", "text", "Item Id")
	.setDisplayType('hidden').setDefaultValue(itemID);
	itemSubList.addField("custpage_serialno", "text", "Item SerialNo")
	.setDisplayType('hidden').setDefaultValue(poSerialNos);
	nlapiLogExecution('ERROR', 'pooverage',	poOverage);
	itemSubList.addField("custpage_pooverage", "text", "PoOverage")
	.setDisplayType('hidden').setDefaultValue(poOverage);

	nlapiLogExecution('ERROR', 'poRcptQty',	parseFloat(poRcptQty));
	nlapiLogExecution('ERROR', 'poOverage',	poOverage);

	itemSubList.addField("custpage_chkn_qty", "text", "CheckIn Qty").setDisplayType('hidden');
	itemSubList.addField("custpage_uomlevel", "text", "UOM Level").setDisplayType('hidden').setDefaultValue(uomlevel);
	itemSubList.addField("custpage_uomqty", "text", "UOM Qty").setDisplayType('hidden').setDefaultValue(vuomqty);
	itemSubList.addField("custpage_baseuomqty", "text", "Base UOM Qty").setDisplayType('hidden').setDefaultValue(vbaseuomqty);
	itemSubList.addField("custpage_polinelocation", "text", "Line Location").setDisplayType('hidden').setDefaultValue(poLineLocation);
}

function fetchPalletQuantity(itemId, location, company,poItemPackcode)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	nlapiLogExecution('ERROR','location',location);
	nlapiLogExecution('ERROR','company',company);
	nlapiLogExecution('ERROR','poItemPackcode',poItemPackcode);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', ['3','2','1']));//3=Pallet
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));
	if(company!=null&&company!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizcompanyskudimension',null,'anyof',['@NONE@',company]));
	/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/	
	if(poItemPackcode!=null && poItemPackcode!='' && poItemPackcode!='null')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',poItemPackcode));
	/* Up to here */ 

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	itemColumns[4].setSort(true);

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function fetchBaseUOM(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	var vbaseuom = '';
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	itemFilters[2] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');	

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomskudim'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null && itemSearchResults != '')
	{
		vbaseuom = itemSearchResults[0].getText('custrecord_ebizuomskudim');
		nlapiLogExecution('ERROR','vbaseuom',vbaseuom);
	}
	return vbaseuom;
}

function checkPOOverage(POId,location,company){
	nlapiLogExecution('ERROR','PO Internal Id', POId);
	var poOverage = 0;

	var poFields = ['custbody_nswmsporeceipttype'];
	var poColumns = nlapiLookupField('transaction', POId, poFields);

	var receiptType = poColumns.custbody_nswmsporeceipttype;

	nlapiLogExecution('ERROR','here', receiptType);

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		nlapiLogExecution('ERROR','here', receiptType);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('ERROR','Out of check PO Overage', poOverage);
	return poOverage;
}

/*
 * Process HTTP Get Request
 */
function checkInGETRequest(request, response) {
	// create the Suitelet form
	var form = nlapiCreateForm('Check-In');

	var ctx = nlapiGetContext();
	var ShelfLife='',CaptureExpiryDate=''; 
	/*
	 * Registering client script This script is registered against a client
	 * script. This is invoked whenever the 'ADD' button is selected against any
	 * order line
	 */
	form.setScript('customscript_chknlpvalidation');

	var poID = request.getParameter('poID');
	/*var poLineNum = request.getParameter('poLineNum');
	var poLineQty = request.getParameter('poLineQty');
	var poQtyChkn = request.getParameter('QtyChkn');
	var poQtyRem = request.getParameter('QtyRem');
	var itemID = request.getParameter('poLineItem');
	var poSerialNos = request.getParameter('poSerialNos');
	var poItemStatus = request.getParameter('poItemStatus');
	var poItemUOM = request.getParameter('poItemUOM');	
	var poItemPackcode = request.getParameter('poItemPackcode');	*/

	var poLineNum = request.getParameter('LineNum');
	var poLineQty = request.getParameter('Qty');
	var poQtyChkn = request.getParameter('QtyChk');
	var poQtyRem = request.getParameter('QtyRem');
	var itemID = request.getParameter('Item');
	var poSerialNos = request.getParameter('SerialNo');
	var poItemStatus = request.getParameter('skuStatus');
	var poItemUOM = request.getParameter('UOM');	
	var poItemPackcode = request.getParameter('Packcode');
	var poLineLocation = request.getParameter('POLineLocation');
	//Case '20123235 Start ..to display UOM text 
	var poItemUOMtext = request.getParameter('UOMtext');
	//Case '20123235 end

	// To get PO location and recordType
	var fields = [ 'recordType','tranid', 'location', 'custbody_nswms_company' ];
	var columns = nlapiLookupField('transaction', poID, fields);
	var tranType = columns.recordType;
	var location = columns.location;
	var company = columns.custbody_nswms_company;
	var poValue = columns.tranid;

	nlapiLogExecution('ERROR', 'poname', poValue);
	var vbaseuom='';
	var vbaseuomqty='';
	var vuomqty='';
	var vuomlevel='';

	if(poItemUOM!=null && poItemUOM!='null' && poItemUOM!='')
	{
		var eBizItemDims=geteBizItemDimensions(itemID);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					vbaseuom = eBizItemDims[z].getText('custrecord_ebizuomskudim');
					vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
				}
				nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
				nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
				nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
				if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
				{
					vuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
					vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
				}
			}

			if(vuomqty==null || vuomqty=='' || isNaN(vuomqty))
				vuomqty=vbaseuomqty;

			if(poLineQty==null || poLineQty=='' || isNaN(poLineQty))
				poLineQty=0;
			else
				poLineQty = (parseFloat(poLineQty)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

			if(poQtyChkn==null || poQtyChkn=='' || isNaN(poQtyChkn))
				poQtyChkn=0;
			else
				poQtyChkn = (parseFloat(poQtyChkn)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

			if(poQtyRem==null || poQtyRem=='' || isNaN(poQtyRem))
				poQtyChkn=0;
			else
				poQtyRem = (parseFloat(poQtyRem)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

		}
	}

	nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
	nlapiLogExecution('ERROR', 'baseuom', vbaseuom);
	nlapiLogExecution('ERROR', 'baseuomqty', vbaseuomqty);
	nlapiLogExecution('ERROR', 'uomqty', vuomqty);
	nlapiLogExecution('ERROR', 'uomlevel', vuomlevel);
	nlapiLogExecution('ERROR', 'poLineQty', poLineQty);
	nlapiLogExecution('ERROR', 'quantityRcvd', poQtyChkn);
	nlapiLogExecution('ERROR', 'RemQty', poQtyRem);
	nlapiLogExecution('ERROR', 'poItemUOMtext', poItemUOMtext);

	var transactionFilters = new Array();
	transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poID);
	transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', itemID);
	transactionFilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', poLineNum);


	var transactionColumns = new Array();
	transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
	transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	transactionColumns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	transactionColumns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');
	transactionColumns[4] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');

	var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);

	if(transactionSearchresults != null && transactionSearchresults != "")
	{
		for(var h=0;h<transactionSearchresults.length;h++)
		{
			var vordQty = transactionSearchresults[h].getValue('custrecord_orderlinedetails_order_qty');
			var vCheckQty = transactionSearchresults[h].getValue('custrecord_orderlinedetails_checkin_qty');

			nlapiLogExecution('ERROR', 'vordQty', vordQty);
			nlapiLogExecution('ERROR', 'vCheckQty', vCheckQty);

			poQtyRem=parseFloat(vordQty) - parseFloat(vCheckQty);
		}	
	}	


	var genLocnCheckBox = form.addField('custpage_genloccheck', 'checkbox',
	'Generate Locations').setDefaultValue('T');

	nlapiLogExecution('ERROR', 'PO Id from Client', poID);

	// Getting Tran Id for this particular id and sending to PO receipt screen.
	var tranID = getTranIDForPO(tranType, poID);
	nlapiLogExecution('ERROR', 'tranID', tranID);

	// add the PO field and make it a hyperlink
	addPOFieldAsHRef(form,request);

	//nlapiLogExecution('ERROR', 'PO Location', poLocation);

	// Added for Location select field
	var poLocation = form.addField('custpage_loc_id', 'select', 'Location',
	'location');
	if(poLineLocation != null && poLineLocation != '')
		poLocation.setDefaultValue(poLineLocation);
	else
		poLocation.setDefaultValue(location);

	// Adding the receipt dropdown and the new PO receipt URL text
	// poOtherAttributesArr consists of 3 records i.e. poRcptQty, trailerNo, bol
	var poOtherAttributesArr = addReceiptFieldToForm(form,request);
	nlapiLogExecution('ERROR', 'after tranID poOtherAttributesArr[0]', poOtherAttributesArr[0]);
	var poRcptURL = addPOReceiptURL(ctx.getEnvironment(), poID, tranID,
			poLineNum);

	// add the quantity field, make it editable

//	var itemQuanField = form.addField('custpage_item_quan', 'integer',
//	'Quantity', null);

//	code modifed by suman 	
	var itemQuanField = form.addField('custpage_item_quan', 'float',
			'Quantity', null);
	//end of code

	itemQuanField.setDefaultValue(poLineQty);
	itemQuanField.setDisplayType('inline');

	// Add the PO Value field and make it hidden
	//var poValue = request.getParameter('poValue');
	var poValueField = form.addField('custpage_povalue', 'text', 'Tran Value');
	poValueField.setDefaultValue(poValue);
	poValueField.setDisplayType('hidden');

	//case# 20127061 starts
	// PO internalId
	var poIDpost=form.addField('custpage_poidpost','text','poIDpost').setVisible(false);
	poIDpost.setDefaultValue(poID);
	//case#20127061 ends
	// Item
	var item = form.addField('custpage_line_item', 'select', 'Item', 'item');
	item.setDefaultValue(itemID);
	item.setDisplayType('inline');

	// Line No.
	var lineNo = form.addField('custpage_line_itemlineno', 'integer', 'Line No');
	lineNo.setDefaultValue(poLineNum);
	lineNo.setDisplayType('inline');

	// Trailer
	var trailer = poOtherAttributesArr[1];

	var trailerNo = form.addField('custpage_trailerno', 'select', 'Trailer',
	'customrecord_ebiznet_trailer');
	if (trailer != null && trailer != "") {
		//trailerNo.setDefaultValue(trailer);
		//trailerNo.setDisplayType('inline');
	}

	// New PO Receipt URL
	var createNewReqLink = form.addField('custpage_porecipt_link',
			'inlinehtml', null, null, null);
	createNewReqLink.setDefaultValue('<B><A HREF="#" onclick="javascript:window.open('
			+ "'" + poRcptURL + "'" + ');">New PO Receipt</A></B>');

	// add the remaining quantity field, make it editable
	var remainingItemQtyField = form.addField('custpage_remainitem_quan',
			'float', 'Remaining Quantity', null).setDisabled(true);
	//remainingItemQtyField.setDefaultValue(request.getParameter('custparam_remainingqty'));
	remainingItemQtyField.setDefaultValue(poQtyRem);

	// add the Check-In Quantity field, make it editable
	var checkinQtyField = form.addField('custpage_checkin_quan', 'float',
			'Check-In Quantity', null);
	checkinQtyField.setDisabled(true);
	//checkinQtyField.setDefaultValue(getTotalCheckinQuantity(poID, poLineNum));
	checkinQtyField.setDefaultValue(poQtyChkn);

	var transactionTypeField = form.addField('custpage_trantype', 'text',
	'Trantype');
	transactionTypeField.setDefaultValue(tranType);
	transactionTypeField.setDisplayType('hidden');

	// BOL
	var bol = poOtherAttributesArr[2];
	var bolNoField = form.addField('custpage_bolno', 'text', 'BOL');
	if (bol != null && bol != "") {
		bolNoField.setDefaultValue(bol);
		bolNoField.setDisplayType('inline');
	}

	// Company
	var chknCompany = form.addField('custpage_company', 'select', 'Company',
	'customrecord_ebiznet_company');
	chknCompany.setDefaultValue(company);
	chknCompany.setDisplayType('inline');

	// add the Priority Putaway Quantity field
	var priorityPutawayLocnArr = priorityPutaway(itemID, poLineQty,location,poItemStatus);
	var priorityQty = priorityPutawayLocnArr[1];

	var priorityQtyField = form.addField('custpage_priority_quan', 'float',
			'Pick Face Recommended Quantity', null);
	priorityQtyField.setDisabled(true);
	priorityQtyField.setDefaultValue(priorityQty);

	// add the Recommended Quantity field//shylaja


	//var poOverage = checkPOOverage(getPOInternalId,getWHLocation, null);
	var ItemRecommendedQuantity =0;
	var hdnItemRecmdQtyWithPOoverage =0;
//	var poOverage = checkPOOverage(poID,location, null);
	/*var vPOoverageQty=0;
	var poOverage=0;
	var vPOoverageChecked='F';
	var vConfig=nlapiLoadConfiguration('accountingpreferences');
	if(vConfig != null && vConfig != '')
	{
		vPOoverageChecked=vConfig.getFieldValue('OVERRECEIPTS');
	}
	nlapiLogExecution('ERROR','vPOoverageChecked', vPOoverageChecked);

	var poItemFields = ['custitem_ebizpo_overage'];
	var poItemColumns = nlapiLookupField('item', itemID, poItemFields);

	vPOoverageQty   = poItemColumns.custitem_ebizpo_overage;
	nlapiLogExecution('ERROR','vPOoverageQty', vPOoverageQty);

	if(vPOoverageChecked =='T')
	{
		if(vPOoverageQty=="")
			poOverage = checkPOOverage(poID,location, null);
		else
			poOverage =vPOoverageQty;
	}*/


	var poOverage=GetPoOverage(itemID,poID,location);
	//var poOverage = checkPOOverage(getPOInternalId,getWHLocation, null);
	nlapiLogExecution('ERROR','poOverage', poOverage);
	if(poOverage == null || poOverage == '')
	{
		poOverage = checkPOOverage(poID,location, null);
	}
	nlapiLogExecution('ERROR','poOverage1', poOverage);
	nlapiLogExecution('ERROR','poQtyRem', poQtyRem);
	nlapiLogExecution('ERROR','poLineQty', poLineQty);

	var palletQuantity = fetchPalletQuantity(itemID,location,null,poItemPackcode);
	if (poOverage == null || poOverage == ""){
		poOverage = 0;	
	}
	//else{
//	ItemRecommendedQuantity = Math.min (parseFloat(poQtyRem) + parseFloat((poOverage * poLineQty)/100), palletQuantity);
//	case 20124475,Math.max to Math.min
	hdnItemRecmdQtyWithPOoverage = Math.min(parseFloat(poQtyRem) + parseFloat((poOverage * poLineQty)/100), palletQuantity);
	ItemRecommendedQuantity = Math.min (parseFloat(hdnItemRecmdQtyWithPOoverage), palletQuantity);
	//}
	nlapiLogExecution('ERROR', 'palletQuantity', palletQuantity);
	nlapiLogExecution('ERROR', 'RecommendedQtyField poLineQty', poLineQty);
	nlapiLogExecution('ERROR', 'RecommendedQtyField poOverage', poOverage);
	nlapiLogExecution('ERROR', 'RecommendedQtyField ItemRecommendedQuantity', parseFloat(poLineQty) + parseFloat((poOverage * poLineQty)/100));
	nlapiLogExecution('ERROR', 'RecommendedQtyField least ItemRecommendedQuantity', ItemRecommendedQuantity.toString());
	var RecommendedQtyField = form.addField('custpage_recommended_quan', 'float',
			'Recommended Quantity', null);
	RecommendedQtyField.setDisabled(true);
	if(ItemRecommendedQuantity==null||ItemRecommendedQuantity==""||ItemRecommendedQuantity.toString()=="NaN")
	{
		ItemRecommendedQuantity = 0;
	}
	nlapiLogExecution('ERROR', 'RecommendedQtyField least ItemRecommendedQuantity', ItemRecommendedQuantity.toString());
	RecommendedQtyField.setDefaultValue(ItemRecommendedQuantity.toString());

	var ItemRecmdQtyWithPOoverageField = form.addField('custpage_itemrecmd_qty_withpooverage', 'float',
			'Recommended Quantity with poOverage', null);
	ItemRecmdQtyWithPOoverageField.setDisplayType('hidden');
	if(hdnItemRecmdQtyWithPOoverage==null||hdnItemRecmdQtyWithPOoverage==""||hdnItemRecmdQtyWithPOoverage.toString()=="NaN")
	{
		hdnItemRecmdQtyWithPOoverage = 0;
	}
	nlapiLogExecution('ERROR', 'RecommendedQtyField least ItemRecommendedQuantity', hdnItemRecmdQtyWithPOoverage.toString());
	ItemRecmdQtyWithPOoverageField.setDefaultValue(hdnItemRecmdQtyWithPOoverage.toString());


	//Code to display Default Dim against the Item
	//code as of 01Jun 2012
	try{

		var dimLength=form.addField('custpage_defaultlength','text','Length').setLayoutType('outsidebelow', 'startrow').setVisible(false);
		var dimWidth=form.addField('custpage_defaultwidth','text','Width').setLayoutType('outsidebelow', 'startcol').setVisible(false);
		var dimHeight=form.addField('custpage_defaultheight','text','Height').setLayoutType('outsidebelow', 'startcol').setVisible(false);
		var dimWeight=form.addField('custpage_defaultweight','text','Weight').setLayoutType('outsidebelow', 'startcol').setVisible(false);
		var vUPC=form.addField('custpage_defaultupc','text','UPC');
		var vhiddenUPC=form.addField('custpage_hiddendefaultupc','text','UPC').setVisible(false);
		form.addField('custpage_updateupccode','checkbox','Want to update UPC code').setVisible(false);
		//Case 20123693 start :muliple(custpage_itemlabelcheckbox) Id declared
		//form.addField('custpage_itemlabelcheckbox','checkbox','Request For ItemLabel').setVisible(true);
		//end
		var labelrequired=form.addField('custpage_itemlabelcount','text','#No of Labels');
		labelrequired.setDisabled(true);
		var dimRecid=form.addField('custpage_itemdimrecid','text','RecordId').setVisible(false);
		form.addField('custpage_itemlabelcheckbox','checkbox','Print Item Labels').setVisible(true);
		//The below code is DJN Specific to populate the printer details
		/*var labelPrinter=form.addField('custpage_printer','select','Printer #');
		labelPrinter.setDisabled(true);
		labelPrinter.addSelectOption("","");

		var filter=new Array();
		filter.push(new nlobjSearchFilter("isinactive",null,'is','F'));

		var column=new Array();
		column[0]=new nlobjSearchColumn("name");

		var searchPrinter=nlapiSearchRecord("customrecord_printer_preferences",null,filter,column);

		if(searchPrinter!=null&&searchPrinter!="")
		{
			for ( var count = 0; count < searchPrinter.length; count++) 
			{
				var vprinter=searchPrinter[count].getValue("name");
				var vrecid=searchPrinter[count].getId();
				labelPrinter.addSelectOption(vrecid,vprinter);
			}
		}*/
//		end of code as of 150413
		nlapiLogExecution('ERROR','itemID',itemID);

		var poItemFields = ['custitem_ebizdimprompt','custitem_ebizweightprompt','upccode','custitem_ebiz_item_shelf_life','custitem_ebiz_item_cap_expiry_date'];
		var rec = nlapiLookupField('item', itemID, poItemFields);

		//rec=nlapiLoadRecord('item',itemID);
		var promtDim=rec.custitem_ebizdimprompt;
		var promtWt=rec.custitem_ebizweightprompt;
		var UPC=rec.upccode;
		ShelfLife=rec.custitem_ebiz_item_shelf_life;
		CaptureExpiryDate=rec.custitem_ebiz_item_cap_expiry_date;
		vUPC.setDefaultValue(UPC);
		vhiddenUPC.setDefaultValue(UPC);
		nlapiLogExecution('ERROR','promtDim',promtDim);
		nlapiLogExecution('ERROR','promtWt',promtWt);
		nlapiLogExecution('ERROR','UPC',UPC);
		if(promtDim=='T'&&promtWt=='F')
		{
			dimLength.setVisible(true);
			dimWidth.setVisible(true);
			dimHeight.setVisible(true);
		}
		else if(promtDim=='F'&&promtWt=='T')
		{
			dimWeight.setVisible(true);
		}
		else if(promtDim=='T'&&promtWt=='T')
		{
			dimLength.setVisible(true);
			dimWidth.setVisible(true);
			dimHeight.setVisible(true);
			dimWeight.setVisible(true);
		}

		var itemdimFilter=new Array();
		itemdimFilter.push(new nlobjSearchFilter('custrecord_ebizitemdims',null,'anyof',itemID));
		itemdimFilter.push(new nlobjSearchFilter('custrecord_ebizbaseuom',null,'is','T'));

		var itemdimColumn=new Array();
		itemdimColumn[0]=new nlobjSearchColumn('custrecord_ebizlength');
		itemdimColumn[1]=new nlobjSearchColumn('custrecord_ebizwidth');
		itemdimColumn[2]=new nlobjSearchColumn('custrecord_ebizheight');
		itemdimColumn[3]=new nlobjSearchColumn('custrecord_ebizweight');

		var searchrecItemDim=nlapiSearchRecord('customrecord_ebiznet_skudims',null,itemdimFilter,itemdimColumn);

		if(searchrecItemDim!=null&&searchrecItemDim!="")
		{
			var length=searchrecItemDim[0].getValue('custrecord_ebizlength');
			var width=searchrecItemDim[0].getValue('custrecord_ebizwidth');
			var height=searchrecItemDim[0].getValue('custrecord_ebizheight');
			var weight=searchrecItemDim[0].getValue('custrecord_ebizweight');
			var recid=searchrecItemDim[0].getId();
			dimLength.setDefaultValue(length);
			dimWidth.setDefaultValue(width);
			dimHeight.setDefaultValue(height);
			dimWeight.setDefaultValue(weight);
			dimRecid.setDefaultValue(recid);
		}

	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in fetching itemDim',exp);
	}

	var vAutoLotgenFlag=getSystemRule('Auto Generate Lot Flag');
	nlapiLogExecution('ERROR', 'vAutoLotgenFlag',vAutoLotgenFlag);

	var AutoGenFlagField = form.addField('custpage_autogenflaglotno', 'text', 'Auto Gen LotNo');
	AutoGenFlagField.setDefaultValue(vAutoLotgenFlag);
	AutoGenFlagField.setDisplayType('hidden');

	//End of the code as of 01Jun 2012
	/*if(parseInt(poQtyRem) > 0)
	{
		// adding item sublist to the form
		checkin_palletisation(form, itemID, poLineNum, poSerialNos,poOtherAttributesArr[0], 
				poItemStatus,poQtyChkn,poOverage,poItemUOM,poItemPackcode,vbaseuom,vuomlevel,vbaseuomqty,vuomqty,location,CaptureExpiryDate,ShelfLife,poLineQty,poQtyRem);
	}*/
	addItemSublistToForm(form, itemID, poLineNum, poSerialNos,poOtherAttributesArr[0], 
			poItemStatus,poQtyChkn,poOverage,poItemUOM,poItemPackcode,vbaseuom,vuomlevel,vbaseuomqty,vuomqty,location,vAutoLotgenFlag,CaptureExpiryDate,ShelfLife,poLineQty,poLineLocation,poItemUOMtext);

	nlapiLogExecution('ERROR', 'After addItemSublistToForm');

	form.addSubmitButton('Submit');
	form.addPageLink('crosslink', 'Back to PO Check-In', nlapiResolveURL(
			'RECORD', tranType, poID));
	nlapiLogExecution('ERROR', 'After addPageLink');
	response.writePage(form);
}

/*
 * Get Stage Locn
 */
/*function getStageLocn() {
	var stageLocn = '';
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', null,
			'anyof', [ 29 ]);
	//var locnSearch = nlapiSearchRecord('customrecord_ebiznet_location', null,filters);
	var locnSearch = getStagingLocation();
	if (locnSearch != null)
		stageLocn = locnSearch[0].getId();

	return stageLocn;
}*/


/*
 * Get dock location
 */
/*function getDockLocn() {
	var dockLocn = '';
	var dockLocnFilter = new Array();
	dockLocnFilter[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid',
			null, 'anyof', [ 30 ]);
	//var dockLocnSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockLocnFilter);
	var dockLocnSearchResults = getDockLocation();

	if (dockLocnSearchResults != null)
		dockLocn = dockLocnSearchResults[0].getId();

	return dockLocn;
}*/

/*
 * Getting location attribute
 */
function getLocationAttribute(locnDimArray, attrName){
	var retVal;

	if(attrName == "LOCN_NAME"){
		if(locnDimArray[0] != "")
			retVal = locnDimArray[0];
		else
			retVal = "";
	}else if(attrName == "REMAINING_CUBE"){
		if (!isNaN(locnDimArray[1])) {
			retVal = locnDimArray[1];
		} else {
			retVal = 0;
		}
	}else if(attrName == "LOCN_ID"){
		if (!isNaN(locnDimArray[2])) {
			retVal = locnDimArray[2];
		} else {
			retVal = "";
		}
	}else if(attrName == "OB_LOCN_ID"){
		if (!isNaN(locnDimArray[3])) {
			retVal = locnDimArray[3];
		} else {
			retVal = "";
		}
	} else if (attrName == "OB_LOCN_ID") {
		if (!isNaN(locnDimArray[4])) {
			retVal = locnDimArray[4];
		} else {
			retVal = "";
		}
	} else if (attrName == "IB_LOCN_ID") {
		if (!isNaN(locnDimArray[5])) {
			retVal = locnDimArray[5];
		} else {
			retVal = "";
		}
	} else if (attrName == "PUT_SEQ") {
		if (!isNaN(locnDimArray[6])) {
			retVal = locnDimArray[6];
		} else {
			retVal = "";
		}
	} else if (attrName == "LOCN_REMAINING_CUBE") {
		if (!isNaN(locnDimArray[7])) {
			retVal = locnDimArray[7];
		} else {
			retVal = "";
		}
	}

	return retVal;
}
/*
 * Processing HTTP POST method
 */
function checkInPOSTRequest(request, response) {
	// this is the POST block
	/* To fetch Stage(Out) location */
	var stageLocn = '';
	var dockLocn = '';

	var t1 = new Date();
	//stageLocn = getStagingLocation();
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'StageLocn Search', getElapsedTimeDuration(t1, t2));

	t1 = new Date();
	dockLocn = getDockLocation();
	t2 = new Date();
	nlapiLogExecution('ERROR', 'DockLocn Search', getElapsedTimeDuration(t1, t2));

	//To update UPC code
	var ChangeUPC = request.getParameter('custpage_updateupccode');
	nlapiLogExecution('ERROR','ChangeUPC',ChangeUPC);
	if(ChangeUPC=='T')
	{
		var EnteredUPC=request.getParameter('custpage_defaultupc');
		var ItemId=request.getParameter('custpage_line_item');
		nlapiLogExecution('ERROR','ItemId',ItemId);
		var fields = ['recordType'];
		var columns = nlapiLookupField('item', ItemId, fields);
		var ItemType = columns.recordType; 
		var recupdate=nlapiSubmitField(ItemType,ItemId,'upccode',EnteredUPC);
	}
	/*
	 * Qc Validation. We check QC flag in Item Master and if it exists bring the
	 * INB-Loc from QC validations record and create CHKN and PUTW records with
	 * status flag as 'N' and redirect to QC validation suitelet.
	 */

	// extract the values from the previous page
	var qcID = request.getParameter('custpage_line_item');
	var qcLineNo = request.getParameter('custpage_line_itemlineno');
	var fields = [ 'recordType', 'custitem_ebizqualitycheck','custitem_ebizserialin','custitem_ebizbatchlot' ];
	var columns = nlapiLookupField('item', qcID, fields);
	var lgItemType = columns.recordType;
	var qcValidate = columns.custitem_ebizqualitycheck;
	var batchflag=columns.custitem_ebizbatchlot;
	var company = request.getParameter('custpage_company');
	var po = request.getParameter('custpage_po');
	var poLocn = request.getParameter('custpage_loc_id');
	var poValue = request.getParameter('custpage_povalue');
	var quantity = request.getParameter('custpage_item_quan');
	var lineNo = request.getParameter('custpage_line_itemlineno');
	var remainingQty = request.getParameter('custpage_remainitem_quan');
	var lineCount = request.getLineItemCount('custpage_items');
	var poRcptNo = request.getParameter('custpage_poreceipt');
	var transactionType = request.getParameter('custpage_trantype');
	var genLocnCheckVal = request.getParameter('custpage_genloccheck');
	nlapiLogExecution('ERROR', 'Generate Location Selection', genLocnCheckVal);
	nlapiLogExecution('ERROR', 'poLocn', poLocn);
	stageLocn = getStagingLocation(poLocn);

	var autogenflag = request.getParameter('custpage_autogenflaglotno');
	nlapiLogExecution('ERROR', 'custpage_autogenflaglotno', autogenflag);

	var checkinLineQty = 0;
	var taskType = "";
	var wmsStatusFlag = "";
	var beginLocn = "";
	var endLocn = "";
	var chkAssignputW="";
	var chkQty=0;
	var vReturnProcess="";

	var vitemId = "";
	var vitemStatusinfo="";

	//updating the Itemdim record
	try{
		var BaseUOMlength=request.getParameter('custpage_defaultlength');
		var BaseUOMwidth=request.getParameter('custpage_defaultwidth');
		var BaseUOMheight=request.getParameter('custpage_defaultheight');
		var BaseUOMweight=request.getParameter('custpage_defaultweight');
		var BaseUOMCube=parseFloat(BaseUOMlength)*parseFloat(BaseUOMwidth)*parseFloat(BaseUOMheight);
		var BaseUOMid=request.getParameter('custpage_itemdimrecid');
		
		//case 201410766 start
		for ( var d = 1; d <= lineCount; d++) {

			var uomlevel = request.getLineItemValue('custpage_items','custpage_chkn_buom', d);

		}

		var uomid="";
		if(uomlevel = "EACH")
			uomid=1;
		else if(uomlevel = "CASE")
			uomid=2;
		else
			uomid = 3;

		//case 201410766 end

		nlapiLogExecution('ERROR','BaseUOMid',BaseUOMid);
		nlapiLogExecution('ERROR','BaseUOMlength',BaseUOMlength);
		nlapiLogExecution('ERROR','BaseUOMwidth',BaseUOMwidth);
		nlapiLogExecution('ERROR','BaseUOMheight',BaseUOMheight);
		nlapiLogExecution('ERROR','BaseUOMweight',BaseUOMweight);
		nlapiLogExecution('ERROR','BaseUOMCube',BaseUOMCube);
		var valueArray=new Array();
		valueArray[0]=parseFloat(BaseUOMlength).toFixed(5);
		valueArray[1]=parseFloat(BaseUOMwidth).toFixed(5);
		valueArray[2]=parseFloat(BaseUOMheight).toFixed(5);
		valueArray[3]=parseFloat(BaseUOMweight).toFixed(5);
		valueArray[4]=parseFloat(BaseUOMCube).toFixed(5);

		var FieldsArray=new Array();
		FieldsArray[0]='custrecord_ebizlength';
		FieldsArray[1]='custrecord_ebizwidth';
		FieldsArray[2]='custrecord_ebizheight';
		FieldsArray[3]='custrecord_ebizweight';
		FieldsArray[4]='custrecord_ebizcube';
		if(BaseUOMid!=null&&BaseUOMid!="")
			var recid=nlapiSubmitField('customrecord_ebiznet_skudims',BaseUOMid,FieldsArray,valueArray);
		nlapiLogExecution('ERROR','ItemDim successfull',recid);

		  //case 201410766 start added uomid
		updatePalletQtyWithNEWDims(qcID,BaseUOMlength,BaseUOMwidth,BaseUOMheight,BaseUOMweight,uomid);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in itemdim updation',exp);
	}
	//End of updating itemdim record
//	Creating item label in label custom record
	var labelcount=request.getParameter('custpage_itemlabelcount');
	var checkboxchecked=request.getParameter('custpage_itemlabelcheckbox');
	var upcnumber=request.getParameter('custpage_defaultupc');
	nlapiLogExecution('ERROR','checkboxchecked',checkboxchecked);
	if(checkboxchecked=="T")
	{
		nlapiLogExecution('ERROR','createitemlabel','createitemlabel');
		var ItemId=request.getParameter('custpage_line_item');
		nlapiLogExecution('ERROR','createItemId',ItemId);
		var fields = ['itemid', 'upccode'];
		var columns = nlapiLookupField('item',ItemId,fields);
		var Itemvalue = columns.itemid;
		var upcnumber=columns.upccode;
		nlapiLogExecution('ERROR','createItemvalue',Itemvalue);
		nlapiLogExecution('ERROR','createupcnumber',upcnumber);
		if((labelcount==null)||(labelcount==""))
		{
			labelcount=0;
		}


		createItemlabel(upcnumber,labelcount,poValue,Itemvalue);
	}
	//end of item label creation
	//Checking the Qty by opening mutiple browsers
	for ( var k = 1; k <= lineCount; k++) {
		chkQty = parseFloat(chkQty) + parseFloat(request.getLineItemValue('custpage_items','custpage_chkn_rcvngqty', k));
		vitemId = request.getLineItemValue('custpage_items','custpage_iteminternalid', k);
	}

	//Added for checking qty purpose Added by ramana
	var posearchresults = nlapiLoadRecord('purchaseorder', po); //1020
	var vponum= posearchresults.getFieldValue('tranid');			      
	//alert(vponum);
	var Qtyfilters = new Array();   
	if(vponum!=null && vponum!='')
	{
		Qtyfilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'is',vponum);
		Qtyfilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto',lineNo);
		//case 20123885 start
		if(vitemId!=null && vitemId!='')
			//case 20123885 end
			Qtyfilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_item', null, 'is',vitemId);

		// Qtyfilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'equalTo',vponum);
		// Qtyfilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalTo',lineNo);
	}
	var Qtycolumns = new Array();
	Qtycolumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');

	var searchQtyresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details',null, Qtyfilters, Qtycolumns);
	var PutwQty = 0;


	if(searchQtyresults!=null)
	{    						  			    	
		PutwQty = searchQtyresults[0].getValue('custrecord_orderlinedetails_checkin_qty');  
	}    				    
	nlapiLogExecution('ERROR', 'vponum', vponum);
	nlapiLogExecution('ERROR', 'lineNo', lineNo);	
	nlapiLogExecution('ERROR', 'PutwQty', PutwQty);				    
	nlapiLogExecution('ERROR', 'chkQty', chkQty);

	var totalchkQty = parseFloat(PutwQty) + parseFloat(chkQty);

	nlapiLogExecution('ERROR', 'totalchkQty', totalchkQty);		
	nlapiLogExecution('ERROR', 'quantity', quantity);		

	//if(parseFloat(PutwQty)>=parseFloat(chkQty))
	/*
	if(parseFloat(totalchkQty)>parseFloat(quantity))
	{			 
		nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
		var form = nlapiCreateForm('Check in');
		nlapiLogExecution('ERROR', 'Form Called', 'form');					  
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'PO Overage is not allowed. Change the quantity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
		nlapiLogExecution('ERROR', 'message', msg);					  
		response.writePage(form);
		nlapiLogExecution('ERROR', 'Write Page to form', 'Written');		
		return;					
	}*/  
	//}

	/*if(vReturnProcess=="CHECK")
		{
			nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
			var form = nlapiCreateForm('Check in');
			nlapiLogExecution('ERROR', 'Form Called', 'form');					  
			var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'PO Overage is not allowed. Change the quantity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
			nlapiLogExecution('ERROR', 'message', msg);					  
			response.writePage(form);
			nlapiLogExecution('ERROR', 'Write Page to form', 'Written');		
			return;	
		}*/
	//upto to here


	//upto to here




	/*
	 * Logic
	 * If QC Validation is not set for the item,
	 *		For all lines
	 * 			Create a CHKIN task in TRN_OPENTASK with begin locn as Stage Locn and End locn as Dock Locn
	 * 			Move record to EBIZTASK
	 * 			If LP has been specified, Create an LP record
	 * 			If Bin Location has been specified,
	 * 				Create a PUTW task in TRN_OPENTASK
	 * 			Else if Bin Location is not specified
	 * 				If user has selected 'Generate Location', then
	 * 					If priority putaway needs to be done,
	 * 						Get priority putaway locations
	 * 						Create PUTW task at priority putaway location
	 * 					End If
	 * 				End If
	 * 			End If
	 * 		End For
	 * End If
	 */
	var vBatchNoMonthYear;
	var vBatchNoSequence;
	var vExpDate;
	var counter=0;
	var vSeq=01;
	if (lgItemType == "lotnumberedinventoryitem" || lgItemType=="lotnumberedassemblyitem" || batchflag=="T" )
	{
		if(autogenflag=='Y')
		{
			if(vitemId != null && vitemId != '')
			{											
				var vLotArr=AutolotNumber(vitemId);
				nlapiLogExecution('ERROR', 'vLotArr',vLotArr);
				if(vLotArr != null && vLotArr != '' && vLotArr.length>0)
				{
					if(vLotArr[2] != null && vLotArr[2].length==1)
						vSeq='0'+vLotArr[2];
					else
						vSeq=vLotArr[2];

					vExpDate=vLotArr[1];//expiry date
					vBatchNoMonthYear=vLotArr[0];//month&year eg:sep and 2012 it is I12
					vBatchNoSequence=vSeq;//sequence eg:02
					//total batno is 02I12
				}
			}
		}
	}
	nlapiLogExecution('ERROR', 'vExpDate',vExpDate);
	nlapiLogExecution('ERROR', 'vBatchNoMonthYear',vBatchNoMonthYear);
	nlapiLogExecution('ERROR', 'vBatchNoSequence',vBatchNoSequence);
	nlapiLogExecution('ERROR', 'Poid',po);
	var vendor;
	if(po !='' && po!=null)
	{		
		vendor = nlapiLookupField('purchaseorder',po,'entity');
		nlapiLogExecution('ERROR', 'vendor',vendor);
	}


	if (qcValidate == 'F') {
		for ( var s = 1; s <= lineCount; s++) {




			vBatchNoSequence = parseFloat(vSeq)+parseFloat(counter);
			// Retreiving line item details
			var itemName = request.getLineItemValue('custpage_items','custpage_chkn_sku', s);
			var pfqtyvalidateflag = request.getLineItemValue('custpage_items','custpage_chkn_pfvalidateflag', s);
			var itemStatus = request.getLineItemValue('custpage_items','custpage_chkn_skustatus', s);
			var packCode = request.getLineItemValue('custpage_items','custpage_chkn_pc', s);
			var baseUOM = request.getLineItemValue('custpage_items','custpage_chkn_buom', s);
			var lp = request.getLineItemValue('custpage_items','custpage_chkn_lp', s);
			var binLocn = request.getLineItemValue('custpage_items','custpage_chkn_binlocation', s);
			var receiveQty = request.getLineItemValue('custpage_items','custpage_chkn_rcvngqty', s);
			var itemDesc = request.getLineItemValue('custpage_items','custpage_chkn_itemdesc', s);
			var itemId = request.getLineItemValue('custpage_items','custpage_iteminternalid', s);
			if(autogenflag=='Y')
				var lotBatchNo = vBatchNoSequence + vBatchNoMonthYear;
			else
				var lotBatchNo = request.getLineItemValue('custpage_items','custpage_lotbatch', s);
			var lotWithQty = lotBatchNo + "(" + receiveQty + ")";
			var uomlevel= request.getLineItemValue('custpage_items', 'custpage_uomlevel', s);
			//var ExpDate= request.getLineItemValue('custpage_items', 'custpage_chkn_expdate', s);
			var manufaturedate= request.getLineItemValue('custpage_items', 'custpage_chkn_manufacturelot', s);
			//nlapiLogExecution('ERROR', 'uomlevel', uomlevel);
			var ExpDate = request.getLineItemValue('custpage_items','custpage_hdncatchexpdate', s);
			var ShelfLife = request.getLineItemValue('custpage_items','custpage_shelflife', s);
			var CaptureExpiryDate = request.getLineItemValue('custpage_items','custpage_capexpirydateflag', s);

			nlapiLogExecution('ERROR', 'lotBatchNo', lotBatchNo);
			nlapiLogExecution('ERROR', 'vExpDate', vExpDate);
			nlapiLogExecution('ERROR', 'manufaturedate', manufaturedate);

			chkQty = receiveQty;

			vitemStatusinfo = itemStatus;



			// Suming the QTY to Send to Trn Tables.
			checkinLineQty = parseFloat(checkinLineQty) + parseFloat(receiveQty);

			// Creating CHKN record even if BIN Location is null or not.
			// Create CHKN task in trn_opentask
			// tasktype = 1(CHKN) and wmsStatusFlag = 1(INBOUND/CHECK-IN)
			// beginLocn = dockLocn and endLocn = stageLocn
			taskType = "1";
			wmsStatusFlag = "1";
			beginLocn = dockLocn;
			endLocn = stageLocn;

			var ActBatchno=lotBatchNo;
			var lotBatchNo = GetLotParsingResults(itemId,vendor,lotBatchNo);
			nlapiLogExecution('ERROR', 'After Parsing ',lotBatchNo);
			//create new record in batch entry
			CreateBatchentryRecord( itemId,lotBatchNo,company,packCode,ItemStatus,poLocn,ExpDate,CaptureExpiryDate,ShelfLife,ActBatchno);

			var chknRecID = createRecordLocationNULL(poValue, itemId, quantity, receiveQty, lp, lineNo, 
					packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag,
					qcID, itemDesc, lotBatchNo, poLocn, lgItemType, lotWithQty, company,batchflag,uomlevel);

			// Move task from TRN_OPENTASK to EBIZTASK
			MoveTaskRecord(chknRecID);

			// Create LP Record
			if (lp != "")
				createLPRecord(lp,poLocn);
			nlapiLogExecution('ERROR', 'binLocn', binLocn);
			if (binLocn != null){
				taskType = "2"; // PUTW
				wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
				beginLocn = binLocn;
				endLocn = "";

				// Create PUTW task
				var retPutRecId = createRecordLocationNULL(poValue, itemId, quantity, receiveQty, lp, lineNo, packCode, itemStatus,
						baseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn,
						lgItemType, lotWithQty, company,batchflag,uomlevel);

				//Case# 20126882� Start
				if (retPutRecId != "") {


					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

					
					

					var filters = new Array();						
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

					if(binLocn != null && binLocn != "")
						filters.push(new nlobjSearchFilter('internalid', null, 'is', binLocn));
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					nlapiLogExecution('ERROR', 'locResults', locResults);
					if(locResults!=null && locResults!='')
					{
						nlapiLogExecution('ERROR', 'receiveQty',receiveQty);
						nlapiLogExecution('ERROR', 'quantity', quantity);
						
						// Item dimensions
						var arrDims = new Array();
						arrDims = getSKUCubeAndWeight(itemId, 1);
						nlapiLogExecution('ERROR', 'arrDims.length', arrDims.length);
						var itemCube = 0;
						var RemCube=0;
						if (arrDims.length > 0 && (!isNaN(arrDims[0]))) //BaseUOMItemCube
						{
							nlapiLogExecution('ERROR', 'arrDims[0]', arrDims[0]);
							nlapiLogExecution('ERROR', 'arrDims[1]', arrDims[1]);
							var uomqty = ((parseFloat(receiveQty))/(parseFloat(arrDims[1])));	//BaseUOMQty		
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));//BaseUOMItemCube
							//itemCube = (parseFloat(arrDims[0]) * parseFloat(priorityRemainingQty));
							nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
						} else {

							itemCube = 0;
							nlapiLogExecution('ERROR', 'elsepart:itemCube', itemCube);
						}
						
						
							LocRemCube = locResults[0].getValue('custrecord_remainingcube');
							LocId = locResults[0].getId();
							nlapiLogExecution('ERROR', 'LocRemCube', LocRemCube);
							nlapiLogExecution('ERROR', 'itemCube', itemCube);
							if ((parseFloat(LocRemCube) >= parseFloat(itemCube))) {
								RemCube = parseFloat(LocRemCube) - parseFloat(itemCube);
							}
							nlapiLogExecution('ERROR', 'RemCube', RemCube);
							nlapiLogExecution('ERROR', 'LocId', LocId);
							if (RemCube >= 0 && LocId!=null && LocId!='') {
								var retValue = UpdateLocCube(LocId, RemCube);

							}
						
					}
				}////Case# 20126882� End

			}else{
				if (genLocnCheckVal == 'T') {					
					// Calling Priority putaway function to get priority putaway location
					//case# 20149733 starts (when remaining cube is lessthan itemcube then system not to generate pick face location)
					var arrDims = new Array();
					arrDims = getSKUCubeAndWeight(itemId, 1);
					nlapiLogExecution('ERROR', 'arrDims.length', arrDims.length);
					var itemCube = 0;
					if (arrDims.length > 0 && (!isNaN(arrDims[0]))) //BaseUOMItemCube
					{
						var uomqty = ((parseFloat(receiveQty))/(parseFloat(arrDims[1])));	//BaseUOMQty		
						itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));//BaseUOMItemCube
						//itemCube = (parseFloat(arrDims[0]) * parseFloat(priorityRemainingQty));
						nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
					} else {

						itemCube = 0;
						nlapiLogExecution('ERROR', 'elsepart:itemCube', itemCube);
					}
					//var priorityPutawayLocnArr = priorityPutaway(itemId, receiveQty,poLocn,itemStatus);
					var priorityPutawayLocnArr = priorityPutaway(itemId, receiveQty,poLocn,itemStatus,itemCube);
					//case# 20149733 ends
					var priorityRemainingQty = priorityPutawayLocnArr[0];
					var priorityQty = priorityPutawayLocnArr[1];
					var priorityLocnID = priorityPutawayLocnArr[2];
					var zoneid = priorityPutawayLocnArr[3];

					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityRemainingQty', priorityRemainingQty);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityQty', priorityQty);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityLocnID', priorityLocnID);
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:priorityZoneID', zoneid);

					// If quantity can be putaway'ed on priority, create PUTW task in TRN_OPENTASK
					// set taskType = 2(PUTW) and wmsStatusFlag = 2(INBOUND/LOCATIONS ASSIGNED)
					// beginLocation = priorityLocnID and endLocn = ""
					if(pfqtyvalidateflag != "Y")
					{
						if (priorityQty != 0){
							taskType = "2"; // PUTW
							wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
							beginLocn = priorityLocnID;
							endLocn = "";
							//var systemlp = GetMaxLPNo(1, 1);
							//case# 20148739 starts (Pick face remaining cube issue)
							/*var arrDims = new Array();
							arrDims = getSKUCubeAndWeight(itemId, 1);
							nlapiLogExecution('ERROR', 'arrDims.length', arrDims.length);
							var itemCube = 0;
							if (arrDims.length > 0 && (!isNaN(arrDims[0]))) //BaseUOMItemCube
							{
								var uomqty = ((parseFloat(receiveQty))/(parseFloat(arrDims[1])));	//BaseUOMQty		
								itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));//BaseUOMItemCube
								//itemCube = (parseFloat(arrDims[0]) * parseFloat(priorityRemainingQty));
								nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
							} else {

								itemCube = 0;
								nlapiLogExecution('ERROR', 'elsepart:itemCube', itemCube);
							}*/
							
							var retPutRecId = CreatePUTWwithLocation(poValue, itemId, quantity, priorityQty, lp, lineNo,
									packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo,
									wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn, lgItemType, lotWithQty, company,itemCube,zoneid,batchflag,uomlevel);//passing itemcube
							
							var TotalRemainingCube=GeteLocCube(beginLocn);
							if(TotalRemainingCube==null || TotalRemainingCube=='' || isNaN(TotalRemainingCube))
								TotalRemainingCube=0;
							
							nlapiLogExecution('ERROR', 'TotalRemainingCube', TotalRemainingCube);
							if(itemCube==null || itemCube=='' || isNaN(itemCube))
								itemCube=0;
							
							var RemCube=parseFloat(TotalRemainingCube)-parseFloat(itemCube);
							if (retPutRecId != "") {

								if (binLocn == null && RemCube >= 0 && beginLocn!=null && beginLocn!='') {
									var retValue = UpdateLocCube(beginLocn, RemCube);

								}
							}
							//case# 20148739 ends
						}
					}
					else
					{
						priorityRemainingQty = receiveQty;
					}
					nlapiLogExecution('ERROR', 'priorityRemainingQty', priorityRemainingQty);
					// Check if there is any remaining quantity to be putaway
					if (parseFloat(priorityRemainingQty) != 0) {
						// Item dimensions
						var arrDims = new Array();
						arrDims = getSKUCubeAndWeight(itemId, 1);
						nlapiLogExecution('ERROR', 'arrDims.length', arrDims.length);
						var itemCube = 0;
						if (arrDims.length > 0 && (!isNaN(arrDims[0]))) //BaseUOMItemCube
						{
							var uomqty = ((parseFloat(priorityRemainingQty))/(parseFloat(arrDims[1])));	//BaseUOMQty		
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));//BaseUOMItemCube
							//itemCube = (parseFloat(arrDims[0]) * parseFloat(priorityRemainingQty));
							nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
						} else {

							itemCube = 0;
							nlapiLogExecution('ERROR', 'elsepart:itemCube', itemCube);
						}
						nlapiLogExecution('ERROR', 'checkin poLocn', poLocn);
						//var LoctDims = GetPutawayLocation(itemId, null, itemStatus, null, itemCube, lgItemType,poLocn);









						var filters2 = new Array();

						var columns2 = new Array();
						columns2[0] = new nlobjSearchColumn('custitem_item_family');
						columns2[1] = new nlobjSearchColumn('custitem_item_group');
						columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
						columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
						columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
						columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
						columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');
						filters2.push(new nlobjSearchFilter('internalid', null, 'is', itemId));
						var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

						var filters3=new Array();
						filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						var columns3=new Array();
						columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
						columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
						columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
						columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
						columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
						columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
						columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
						columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
						columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
						columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
						columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
						columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
						columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
						columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
						columns3[14] = new nlobjSearchColumn('formulanumeric');
						columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
						columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
						//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
						columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
						// Upto here on 29OCT2013 - Case # 20124515
						columns3[14].setSort();

						var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);

						var LoctDims =   GetPutawayLocationNew(itemId, packCode, itemStatus, baseUOM, parseFloat(itemCube),lgItemType,poLocn,ItemInfoResults,putrulesearchresults,priorityLocnID); // case# 201412982

						var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube,putmethod,putrule;
						if (LoctDims.length > 0) {
							LocName = getLocationAttribute(LoctDims, "LOCN_NAME");
							RemCube = getLocationAttribute(LoctDims, "REMAINING_CUBE");
							LocId = getLocationAttribute(LoctDims, "LOCN_ID");
							OubLocId = getLocationAttribute(LoctDims, "OB_LOCN_ID");
							PickSeq = getLocationAttribute(LoctDims, "PICK_SEQ");
							InbLocId = getLocationAttribute(LoctDims, "IB_LOCN_ID");
							PutSeq = getLocationAttribute(LoctDims, "PUT_SEQ");
							LocRemCube = getLocationAttribute(LoctDims, "LOCN_REMAINING_CUBE");
							zoneid=LoctDims[8];
							putmethod=LoctDims[9];
							putrule=LoctDims[10];
						} else {
							LocName = "";
							LocId = "";
							RemCube = "";
							InbLocId = "";
							OubLocId = "";
							PickSeq = "";
							PutSeq = "";
							zoneid="";
							putmethod='';
							putrule='';
						}

						chkAssignputW  = "Y";
						// Creating Putaway record in TRN_OPENTASK
						taskType = "2"; 		// PUTW
						wmsStatusFlag = "2"; 	// INBOUND/LOCATIONS ASSIGNED
						beginLocn = LocId; 		// Identified location
						endLocn = "";						
						var putwrecordid = CreatePUTWwithLocation(poValue, itemId, quantity, priorityRemainingQty, lp, lineNo,
								packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo,wmsStatusFlag, qcID, itemDesc, 
								lotBatchNo, poLocn, lgItemType, lotWithQty, company,itemCube, zoneid,batchflag,uomlevel,putmethod,putrule);

						if (putwrecordid != "") {

							if (binLocn == null && RemCube >= 0 && LocId!=null && LocId!='') {
								var retValue = UpdateLocCube(LocId, RemCube);

							}
						}
					}// /If priroty remaining QTY is Zero

					if(autogenflag=='Y')
					{
						CreateAutoLotNoinBatchEntry(lgItemType,lotBatchNo,itemId,vExpDate,packCode,poLocn,manufaturedate,s,batchflag);
						counter++;
					}

				} else {
					// create PUTW task in TRN_OPENTASK
					taskType = "2"; 		// PUTW
					wmsStatusFlag = "6"; 	// INBOUND/NO LOCATIONS ASSIGNED
					beginLocn = "";
					endLocn = "";
					var retPutRecId = createRecordLocationNULL(poValue, itemId, quantity, receiveQty, lp, lineNo, packCode,
							itemStatus, baseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag, qcID, itemDesc,
							lotBatchNo, poLocn, lgItemType, lotWithQty, company,batchflag,uomlevel);
				}
			}
		}// end of For loop lines count.
	} else if (qcValidate == 'T') {
		nlapiLogExecution('ERROR', 'IF QC validate flag is True', qcValidate);
		nlapiLogExecution('ERROR', 'IF QC validate flag is lineCnt ', lineCount);

		for ( var s = 1; s <= lineCount; s++) {
			vBatchNoSequence = parseFloat(vSeq)+parseFloat(counter);
			// Retreiving line item details
			var ItemName = request.getLineItemValue('custpage_items', 'custpage_chkn_sku', s);
			var ItemStatus = request.getLineItemValue('custpage_items', 'custpage_chkn_skustatus', s);
			var PackCode = request.getLineItemValue('custpage_items', 'custpage_chkn_pc', s);
			var BaseUOM = request.getLineItemValue('custpage_items', 'custpage_chkn_buom', s);
			var LP = request.getLineItemValue('custpage_items', 'custpage_chkn_lp', s);
			var BinLoc = request.getLineItemValue('custpage_items', 'custpage_chkn_binlocation', s);
			var RcvQty = request.getLineItemValue('custpage_items', 'custpage_chkn_rcvngqty', s);
			var ItemDesc = request.getLineItemValue('custpage_items', 'custpage_chkn_itemdesc', s);
			var ItemId = request.getLineItemValue('custpage_items', 'custpage_iteminternalid', s);
			if(autogenflag=='Y')
				var lotBatchNo = vBatchNoSequence + vBatchNoMonthYear;
			else
				var lotBatchNo = request.getLineItemValue('custpage_items','custpage_lotbatch', s);
			var lotwithqty = lotBatchNo + "(" + RcvQty + ")";
			var uomlevel= request.getLineItemValue('custpage_items', 'custpage_uomlevel', s);
			//var ExpDate= request.getLineItemValue('custpage_items', 'custpage_chkn_expdate', s);
			var manufaturedate= request.getLineItemValue('custpage_items', 'custpage_chkn_manufacturelot', s);
			nlapiLogExecution('ERROR', 'manufaturedate', manufaturedate);
			nlapiLogExecution('ERROR', 'vExpDate', vExpDate);
			nlapiLogExecution('ERROR', 'lotBatchNo', lotBatchNo);
			var ExpDate = request.getLineItemValue('custpage_items','custpage_hdncatchexpdate', s);
			var ShelfLife = request.getLineItemValue('custpage_items','custpage_shelflife', s);
			var CaptureExpiryDate = request.getLineItemValue('custpage_items','custpage_capexpirydateflag', s);
			nlapiLogExecution('ERROR', 'CaptureExpiryDate', CaptureExpiryDate);
			nlapiLogExecution('ERROR', 'ShelLife', ShelfLife);
			nlapiLogExecution('ERROR', 'ExpDate', ExpDate);


			vitemStatusinfo = ItemStatus;

			// Suming the QTY to Send to Trn Tables.
			checkinLineQty = parseFloat(checkinLineQty) + parseFloat(RcvQty);

			// Create CHKN task in TRN_OPENTASK
			// taskType = 1(CHKN) and wmsStatusFlag = 1(INBOUND/CHECK-IN)
			// beginLocn = dockLocn and endLocn = stageLocn
			//create new record in batch entry.
			var ActBatchno=lotBatchNo;
			var lotBatchNo = '';
			if(itemId!=null && itemId!='')
			{
				lotBatchNo = GetLotParsingResults(itemId,vendor,lotBatchNo);
			}
			
			nlapiLogExecution('ERROR', 'After Parsing ',lotBatchNo);

			CreateBatchentryRecord( ItemId,lotBatchNo,company,PackCode,ItemStatus,poLocn,ExpDate,CaptureExpiryDate,ShelfLife,ActBatchno);
			
			var LotBatch = lotBatchNo;
			
			var QcChknRecId = createRecordLocationNULL(poValue, ItemId,
					quantity, RcvQty, LP, lineNo, PackCode, ItemStatus,
					BaseUOM, "1", dockLocn, stageLocn, po, poRcptNo,
					"1", qcID, ItemDesc, LotBatch, poLocn,
					lgItemType, lotwithqty, company,batchflag,uomlevel);

			// move task to EBIZTASK
			MoveTaskRecord(QcChknRecId);

			// Create LP Record
			if (LP != "") {
				createLPRecord(LP,poLocn);
			}

			// Create PUTW task in TRN_OPENTASK
			// taskType = 2(PUTW) and wmsStatusFlag = 23(INBOUND/QUALITY CHECK)
			// beginLocn = "" and endLocn = ""
			taskType = "2"; // PUTW
			wmsStatusFlag = "23"; // INBOUND/QUALITY CHECK
			beginLocn = "";
			endLocn = "";
			var QcChknRecId = createRecordLocationNULL(poValue, ItemId, quantity, RcvQty, LP, lineNo, PackCode, ItemStatus,
					BaseUOM, taskType, beginLocn, endLocn, po, poRcptNo, wmsStatusFlag, qcID, ItemDesc, LotBatch, poLocn,
					lgItemType, lotwithqty, company,batchflag,uomlevel);
			if(autogenflag=='Y')
			{
				CreateAutoLotNoinBatchEntry(lgItemType,lotBatchNo,itemId,vExpDate,packCode,poLocn,manufaturedate,s,batchflag);
				counter++;
			}
		}

	}

	if(autogenflag=='Y')
	{
		fnUpdateBuildSequence(counter);
	}

	var type = transactionType;// "purchaseorder";
	var putGenQty = 0;
	var putConfQty = 0;
	var ttype = "CHKN";
	var confQty = checkinLineQty;

	nlapiLogExecution('ERROR', 'checkInPOSTRequest:quantity', quantity);
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:confQty', confQty);
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:poValue', poValue);

	nlapiLogExecution('ERROR', 'chkAssignputW Value::', chkAssignputW);

	var tranValue = TrnLineUpdation(type, ttype, poValue, po, qcLineNo, qcID,
			parseFloat(request.getParameter('custpage_item_quan')), confQty,chkAssignputW,vitemStatusinfo);

	var poArray = new Array();
	poArray["custparam_po"] = po;
	poArray["custparam_povalue"] = poValue;






	// Redirecting to QC Validation screen if QC Validate is TRUE
	if (qcValidate == 'T') {
		nlapiLogExecution('ERROR', 'Navigating TO QC Screen');
		poArray["custparam_poqctemid"] = qcID;
		poArray["custparam_poqctemlineno"] = qcLineNo;
		poArray["custparam_company"] = company;

		response.sendRedirect('SUITELET', 'customscript_qc_list',
				'customdeploy_qc_list', false, poArray);
	} else {
		// redirect to the record in view mode(customscript_assignputaway)

		nlapiLogExecution('ERROR', 'Navigating', 'To Assign putaway');
		response.sendRedirect('SUITELET', 'customscript_assignputaway',
				'customdeploy_ebizassignputaway', false, poArray);
		var context = nlapiGetContext();
		var usageRemaining = context.getRemainingUsage();
		nlapiLogExecution('ERROR', 'usageRemaining', usageRemaining);
	}
}

function createItemlabel(UPCCODE,labelcount,poValue,item)
{
	try
	{
		var Label="";
		Label += "^XA";
		Label += "^COY,0^MMT^MTT^MD+0";
		Label += "^XZ";
		Label += "^XA";
		Label += "^PR12^FS";
		Label += "^FO50,50^GB700,1130,7,B^FS";
		Label += "^FO405,200^A0R,150,150^FD"+  UPCCODE+" ^FS";
		Label += "^BY5,1,^FS";
		Label += "^FO110,300^BCR,170,N,N,N^FD>"+ UPCCODE+"^FS";
		Label += "^XZ";

		var print="F";
		var tasktype="1";
		var reprint="F";
		var labeltype="ItemLabel";
		if((item==null)||(item==""))
		{
			item=poValue;
		}
		var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
		labelrecord.setFieldValue('name',item); 
		labelrecord.setFieldValue('custrecord_labeldata',Label);  
		labelrecord.setFieldValue('custrecord_label_refno',poValue);     
		labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);//chkn task   
		labelrecord.setFieldValue('custrecord_labeltype',labeltype);                                                                                                                                                                     
		labelrecord.setFieldValue('custrecord_label_print', print);
		labelrecord.setFieldValue('custrecord_label_reprint', reprint);
		labelrecord.setFieldValue('custrecord_label_adresslabel_generation',labelcount);
		labelrecord.setFiedlValue('custrecord_label_lp',UPCCODE);
		var tranid = nlapiSubmitRecord(labelrecord);


	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'ExceptioninItemLabel', exp);	
	}

}
/*
 * Main function to process PO Check-in
 */
function checkInConceptSuitelet(request, response) {
	// obtain the context object
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') {
		// Creating the form for check-in
		checkInGETRequest(request, response);
	} else {
		checkInPOSTRequest(request, response);
	}// End of else block.
}

/*
 * Create CHKN and PUTW Records .
 */
function createRecordLocationNULL(poValue, itemID, quantity, receiveQty, lp,
		lineNo, packCode, itemStatus, baseUOM, taskType, beginLocn, endLocn,
		po, poReceiptNo, wmsStatusFlag, qcID, itemDesc, lotBatchNo, poLocn,
		lgItemType, lotWithQty, vCompany,batchflag,uomlevel) {
	var timeStamp1 = new Date();
	nlapiLogExecution('ERROR','poValue',poValue);
	nlapiLogExecution('ERROR','po',po);
	nlapiLogExecution('ERROR','quantity is',quantity);
	nlapiLogExecution('ERROR','receiveQty is',receiveQty);
	var t3 = new Date();
	var customRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	var t4 = new Date();
	nlapiLogExecution('ERROR', 'createRecordLocationNULL:nlapiCreateRecord|trn_opentask',
			getElapsedTimeDuration(t4, t3));

	// populating the fields
	customRecord.setFieldValue('name', poValue);
	customRecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
	customRecord.setFieldValue('custrecord_ebiz_sku_no', itemID);
	nlapiLogExecution('ERROR', 'custrecord_act_qty',parseFloat(quantity).toFixed(5));
	nlapiLogExecution('ERROR', 'custrecord_expe_qty',parseFloat(receiveQty).toFixed(5));
	//commented
	//case# 20148871 starts
	//customRecord.setFieldValue('custrecord_act_qty', parseFloat(quantity));
	//case# 20148871 ends
	customRecord.setFieldValue('custrecord_expe_qty', parseFloat(receiveQty));

	//customRecord.setFieldValue('custrecord_act_qty', parseFloat(receiveQty).toFixed(5));
	//customRecord.setFieldValue('custrecord_expe_qty', parseFloat(quantity).toFixed(5));

	customRecord.setFieldValue('custrecord_lpno', lp);
	customRecord.setFieldValue('custrecord_ebiz_lpno', lp);
	customRecord.setFieldValue('custrecord_line_no', lineNo);
	customRecord.setFieldValue('custrecord_packcode', packCode);
	customRecord.setFieldValue('custrecord_sku_status', itemStatus);
	customRecord.setFieldValue('custrecord_uom_id', baseUOM);
	customRecord.setFieldValue('custrecord_tasktype', taskType);
	/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/	
	customRecord.setFieldValue('custrecord_actbeginloc', beginLocn);
	/* Up to here */ 

	// taskType = 1 (CHKN)
	if (taskType == "1") {
		customRecord.setFieldValue('custrecord_actbeginloc', beginLocn);
		customRecord.setFieldValue('custrecord_actendloc', endLocn);
		customRecord.setFieldValue('custrecordact_begin_date', DateStamp());
		customRecord.setFieldValue('custrecord_act_end_date', DateStamp());
		//Case# 201410373 starts (After checkin, we have update both actual and expected quantities are same)
		//case# 20148871 starts
		//customRecord.setFieldValue('custrecord_act_qty', parseFloat(quantity));
		//case# 20148871 ends
		customRecord.setFieldValue('custrecord_act_qty', parseFloat(receiveQty));
		//Case# 201410373 ends
		// Adding fields to update time zones.
		customRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
		customRecord.setFieldValue('custrecord_actualendtime', TimeStamp());
		customRecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customRecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
	}

	customRecord.setFieldValue('custrecord_current_date', DateStamp());
	customRecord.setFieldValue('custrecord_upd_date', DateStamp());
	customRecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
	customRecord.setFieldValue('custrecord_ebiz_receipt_no', poReceiptNo);

	// Status flag .
	customRecord.setFieldValue('custrecord_wms_status_flag', wmsStatusFlag);

	// Added for Item name and desc
	customRecord.setFieldValue('custrecord_sku', qcID);
	customRecord.setFieldValue('custrecord_skudesc', itemDesc);

	// LotBatch and batch with Quantity.
	if (lgItemType == "lotnumberedinventoryitem" || lgItemType == "lotnumberedassemblyitem" || batchflag=='T') {
		customRecord.setFieldValue('custrecord_batch_no', lotBatchNo);
		customRecord.setFieldValue('custrecord_lotnowithquantity', lotWithQty);
		var filtersbat = new Array();
		filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', lotBatchNo);
		if(itemID!=null&&itemID!="")
			filtersbat[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemID);
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
		column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
		column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
		column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
		column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
		column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');
		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat,column);
		if(SrchRecord)
		{
			var ExpDate=SrchRecord[0].getValue('custrecord_ebizexpirydate');
			var FifoDate=SrchRecord[0].getValue('custrecord_ebizfifodate');
			nlapiLogExecution('ERROR', 'ExpDate', ExpDate);
			nlapiLogExecution('ERROR', 'FifoDate', FifoDate);
			customRecord.setFieldValue('custrecord_fifodate', FifoDate);
			customRecord.setFieldValue('custrecord_expirydate', ExpDate);
		}
	}

	customRecord.setFieldValue('custrecord_wms_location', poLocn);
	//customRecord.setFieldValue('custrecord_fifodate', DateStamp());
	customRecord.setFieldValue('custrecord_comp_id', vCompany);

	var currentUserID = getCurrentUser();

	customRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
	customRecord.setFieldValue('custrecord_ebizuser', currentUserID);
	customRecord.setFieldValue('custrecord_taskassignedto', currentUserID);
	nlapiLogExecution('ERROR', 'uomlevel', uomlevel);
	customRecord.setFieldValue('custrecord_uom_level', uomlevel);

	//added code on 15Feb 2012
	if(po!=null&&po!="")
		customRecord.setFieldValue('custrecord_ebiz_order_no',po);

	// commit the record to NetSuite
	t3 = new Date();
	var recordId = nlapiSubmitRecord(customRecord);
	t4 = new Date();
	nlapiLogExecution('ERROR', 'createRecordLocationNULL:nlapiSubmitRecord|trn_opentask',
			getElapsedTimeDuration(t4, t3));

	nlapiLogExecution('ERROR', 'Check-In Record Insertion', 'Success');

	var invtRecordId = createInvtRecord(taskType, po, endLocn, lp, itemID,
			itemStatus, itemDesc, receiveQty, poLocn, lgItemType, packCode,
			vCompany,lotBatchNo,uomlevel);

	var timeStamp2 = new Date();
	nlapiLogExecution('ERROR', 'Elapsed time for createRecordLocationNULL',
			getElapsedTimeDuration(timeStamp1, timeStamp2));
	return recordId;
}

function createInvtRecord(taskType, po, endLocn, lp, itemId, itemStatus, itemDesc,
		receiveQty, poLocn, lgItemType, packCode, vCompany,lotBatchNo,uomlevel) {
	// Creating inventory for taskType = 1 (CHKN)
	if (taskType == "1") {
		try {
			var varAccountNo='';
			// Creating Inventory Record.
			var t1 = new Date();
			var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
			var t2 = new Date();
			nlapiLogExecution('ERROR', 'createInvtRecord:nlapiCreateRecord|createinv',
					getElapsedTimeDuration(t2, t1)); 


			nlapiLogExecution('ERROR', 'Location',poLocn);                
			var filtersAccNo = new Array();
			// filtersAccNo[0] = new nlobjSearchFilter('custrecord_siteid', null, 'is', 'New Jersey');
			filtersAccNo[0] = new nlobjSearchFilter('custrecord_location', null, 'is', poLocn);
			var colsAcc = new Array();
			colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
			var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);              
			if(Accsearchresults!=null&&Accsearchresults!='')
			{
				varAccountNo = Accsearchresults[0].getValue('custrecord_accountno');                                
				nlapiLogExecution('ERROR', 'Account ',varAccountNo );
			}

			invtRec.setFieldValue('name', po);
			invtRec.setFieldValue('custrecord_ebiz_inv_binloc', endLocn);
			invtRec.setFieldValue('custrecord_ebiz_inv_lp', lp);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemId);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
			invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packCode);
			invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(receiveQty).toFixed(5));

			invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemId);

			invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(receiveQty).toFixed(5));
			invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemDesc);
			invtRec.setFieldValue('custrecord_ebiz_inv_loc', poLocn);

			invtRec.setFieldValue('custrecord_invttasktype', 1); // CHKN..
//			invtRec.setFieldValue('custrecord_wms_inv_status_flag', 1); // CHKN
			invtRec.setFieldValue('custrecord_wms_inv_status_flag', 17); //17=FLAG.INVENTORY.INBOUND
			//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', 1);
			invtRec.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);
			//

			invtRec.setFieldValue('customrecord_ebiznet_location', endLocn); //Added by ramana on 30th may

			invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
			//

			nlapiLogExecution('ERROR', 'Location Information', endLocn);

			invtRec.setFieldValue('custrecord_ebiz_inv_company', vCompany);
			if(lotBatchNo!=null && lotBatchNo!='')
				invtRec.setFieldText('custrecord_ebiz_inv_lot', lotBatchNo);
			nlapiLogExecution('ERROR', 'uomlevel', uomlevel);

			if(uomlevel!=null && uomlevel!='')
				invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);

			//code added on 15Feb by suman.
			if(po!=null && po!="")
				invtRec.setFieldValue('custrecord_ebiz_transaction_no', po);

			t1 = new Date();
			var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
			t2 = new Date();
			nlapiLogExecution('ERROR', 'createInvtRecord:nlapiSubmitRecord:createinv',
					getElapsedTimeDuration(t2, t1));

			if (invtRecordId != null) {
				nlapiLogExecution('ERROR', 'Inventory Record Creation', invtRecordId);
			} else {
				nlapiLogExecution('ERROR', 'Inventory Record Creation', 'Failed');
			}
		} catch (error) {
			nlapiLogExecution('ERROR', 'Check for error msg in create invt', error);
		}
	}
}

function createLPRecord(lp,poLocn) {
	nlapiLogExecution('ERROR', 'LP Creation ',lp);
	var timestamp1 = new Date();
	var t1 = new Date();
	var lpRecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord:nlapiCreateRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	lpRecord.setFieldValue('name', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_site', poLocn);
	t1 = new Date();
	var recid = nlapiSubmitRecord(lpRecord);
	t2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord:nlapiSubmitRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	var timestamp2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord Duration',
			getElapsedTimeDuration(timestamp1, timestamp2));
}

function getStagingLocation(RoleLocation){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	
	//var RoleLocation=getRoledBasedLocation();
	
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	// ACTIVE RECORDS ONLY
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));
	
	//case # 20127189..........Added the location filter.
	stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', 'null', 'anyof', RoleLocation));
	
	
	
	
	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));
	var Columns = new Array();
	Columns[0]=new nlobjSearchColumn('custrecord_locgrouptype', 'custrecord_inboundlocgroupid');
/*	Columns[1]=new nlobjSearchColumn('custrecord_sequenceno','custrecord_inboundlocgroupid');
	Columns[1].setSort();*/
	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters,Columns);
//	nlapiLogExecution('ERROR', 'locgrptype', locgrptype);
	// This search could return more than one inbound staging location.  We are using the first one.
	var id="";
	for(var s = 0; stageLocnResults!=null && s < stageLocnResults.length ; s++)
	{
		var locgrptype = stageLocnResults[s].getText('custrecord_locgrouptype','custrecord_inboundlocgroupid');
		nlapiLogExecution('ERROR', 'locgrptype', locgrptype);
		if(locgrptype =='INBOUND')
		{
			nlapiLogExecution('ERROR', 'test1', locgrptype);
			retInboundStagingLocn = stageLocnResults[0].getId();
		}

		if(locgrptype=='BOTH')
		{
			nlapiLogExecution('ERROR', 'test2', locgrptype);
			id = stageLocnResults[0].getId();
		}
	}

	if(retInboundStagingLocn==null || retInboundStagingLocn=="")
		retInboundStagingLocn=id;

	return retInboundStagingLocn;
}

function getDockLocation(){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}


function CreateBatchentryRecord( ItemId,LotBatch,VCompany,PackCode,ItemStatus,poLocn,expdate,CaptureExpiryDate,ShelfLife,ActBatchno)
{
	nlapiLogExecution('ERROR', 'CreateBatchentryRecord','done');
	nlapiLogExecution('ERROR', 'ItemId',ItemId);
	nlapiLogExecution('ERROR', 'LotBatch',LotBatch);
	nlapiLogExecution('ERROR', 'VCompany',VCompany);
	nlapiLogExecution('ERROR', 'PackCode',PackCode);
	nlapiLogExecution('ERROR', 'ItemStatus',ItemStatus);
	nlapiLogExecution('ERROR', 'poLocn',poLocn);
	nlapiLogExecution('ERROR', 'expdate',expdate);
	nlapiLogExecution('ERROR', 'CaptureExpiryDate',CaptureExpiryDate);	
	nlapiLogExecution('ERROR', 'ShelfLife',ShelfLife);
	nlapiLogExecution('ERROR', 'ActBatchno',ActBatchno);
	var dtsettingFlag = DateSetting();

	nlapiLogExecution('Debug', 'dtsettingFlag', dtsettingFlag);
	if(LotBatch!=null && LotBatch!='')
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[ItemId]));
		filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',LotBatch));

		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
		column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
		column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
		column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
		column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
		column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');

		var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);

		if(searchRecord==null || searchRecord=='')
		{

			var FifoDate='';
			var ExpiryDate='';
			FifoDate=DateStamp();
			if(CaptureExpiryDate == 'F'){
				if(ShelfLife !=null && ShelfLife!=''){
					var sysdate=DateStamp();
					var d = new Date(sysdate);
					d.setDate( d.getDate()+ parseInt(ShelfLife));
					if(dtsettingFlag == 'DD/MM/YYYY')
					{
						ExpiryDate=(d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear());
					}
					else
					{
						ExpiryDate=((d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear());
					}

					nlapiLogExecution('ERROR', 'ExpiryDate', ExpiryDate);
					nlapiLogExecution('ERROR', 'sysdate', sysdate);
				}
				else
				{

					ExpiryDate='01/01/2099';
				}
			}
			else
			{
				if(expdate != null && expdate != '')
				{	
					var vdate= RFDateFormat(expdate,dtsettingFlag);
					if(vdate!=null && vdate !=''&& vdate[0]=='true')
					{
						nlapiLogExecution('ERROR','Mfgdate',vdate[1]);
						ExpiryDate=vdate[1];

					}
				}
				else
				{
					if(ShelfLife !=null && ShelfLife!=''){
						var sysdate=DateStamp();
						var d = new Date(sysdate);
						d.setDate( d.getDate()+ parseInt(ShelfLife));
						//ExpiryDate=((d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear());
						if(dtsettingFlag == 'DD/MM/YYYY')
						{
							ExpiryDate=(d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear());
						}
						else
						{
							ExpiryDate=((d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear());
						}
						nlapiLogExecution('ERROR', 'ExpiryDate', ExpiryDate);
						nlapiLogExecution('ERROR', 'sysdate', sysdate);
					}
					else
					{

						ExpiryDate='01/01/2099';
					}
				}	

			}


			var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
			customrecord.setFieldValue('name', LotBatch);
			customrecord.setFieldValue('custrecord_ebizlotbatch', LotBatch);
			customrecord.setFieldValue('custrecord_ebizsku', ItemId);
			customrecord.setFieldValue('custrecord_ebizpackcode',PackCode);
			//customrecord.setFieldValue('custrecord_ebizsitebatch',poLocn);
			customrecord.setFieldValue('custrecord_ebizcompanybatch',VCompany);
			customrecord.setFieldValue('custrecord_ebizexpirydate',ExpiryDate);
			customrecord.setFieldValue('custrecord_ebizfifodate',FifoDate);
			customrecord.setFieldValue('custrecord_ebiz_manufacturelot',ActBatchno);					
			var rec = nlapiSubmitRecord(customrecord, false, true);
		}
	}
}

function CreatePUTWwithLocation(poValue, ItemId, quan, PriortyQty, LP, lineno,
		PackCode, ItemStatus, BaseUOM, tasktype, beginloc, endloc, po,
		poreceiptno, wmsStsflag, QcID, ItemDesc, LotBatch, poloc, lgItemType,
		lotwithqty, VCompany,itemCube, zoneid,batchflag,uomlevel,putmethod,putrule) {
	var t1 = new Date();
	var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'CreatePUTWwithLocation:nlapiCreateRecord|trn_opentask',
			getElapsedTimeDuration(t2, t1));

	nlapiLogExecution('ERROR', 'Creating PUTW record with Location',
	'TRN_OPENTASK');
	// populating the fields
	putwrecord.setFieldValue('name', poValue);
	putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
	putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);

	nlapiLogExecution('ERROR', 'quan',quan);
	nlapiLogExecution('ERROR', 'PriortyQty',PriortyQty);

	//case# 20148871 starts
	//putwrecord.setFieldValue('custrecord_act_qty', parseFloat(quan));
	//case# 20148871 ends
	putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(PriortyQty));

//	putwrecord.setFieldValue('custrecord_act_qty', parseFloat(PriortyQty));
//	putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan));

	putwrecord.setFieldValue('custrecord_lpno', LP);
	putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
	putwrecord.setFieldValue('custrecord_line_no', lineno);
	putwrecord.setFieldValue('custrecord_packcode', PackCode);
	putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
	putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
	putwrecord.setFieldValue('custrecord_tasktype', tasktype);
	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();
	putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
	putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
	putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
	putwrecord.setFieldValue('custrecord_actbeginloc', beginloc);
	putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
	putwrecord.setFieldValue('custrecord_ebizzone_no', zoneid);

	//Addfor total cube	
	putwrecord.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(5));
	//
	nlapiLogExecution('ERROR', 'Total Cube in Checkin', parseFloat(itemCube).toFixed(5));

	// For eBiznet Receipt Number .
	putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

	// month+1 / date / year
	putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

	// Adding fields to update time zones.
	// hour:min:am/pm
	putwrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
	putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());

	// month+1 / date / year
	putwrecord.setFieldValue('custrecord_current_date', DateStamp());
	putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

	// Status flag .
	putwrecord.setFieldValue('custrecord_wms_status_flag', wmsStsflag);

	// Added for Item name and desc
	putwrecord.setFieldValue('custrecord_sku', ItemId);
	putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

	// LotBatch and batch with Quantity.
	if (lgItemType == "lotnumberedinventoryitem" || lgItemType == "lotnumberedassemblyitem" ||batchflag=='T') {
		nlapiLogExecution('ERROR', 'lotnumberedinventoryitem PUTW record',
		'LOT-->TRN_OPENTASK');
		putwrecord.setFieldValue('custrecord_batch_no', LotBatch);
		putwrecord.setFieldValue('custrecord_lotnowithquantity', lotwithqty);
		var filtersbat = new Array();
		filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', LotBatch);
		if(ItemId!=null&&ItemId!="")
			filtersbat[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', ItemId);
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
		column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
		column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
		column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
		column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
		column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');
		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat,column);
		if(SrchRecord)
		{
			var ExpDate=SrchRecord[0].getValue('custrecord_ebizexpirydate');
			var FifoDate=SrchRecord[0].getValue('custrecord_ebizfifodate');
			nlapiLogExecution('ERROR', 'ExpDate', ExpDate);
			nlapiLogExecution('ERROR', 'FifoDate', FifoDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
		}
	}
	putwrecord.setFieldValue('custrecord_wms_location', poloc);
	//putwrecord.setFieldValue('custrecord_fifodate', DateStamp());
	putwrecord.setFieldValue('custrecord_comp_id', VCompany);
	nlapiLogExecution('ERROR', 'uomlevel', uomlevel);
	if(uomlevel!=null && uomlevel!='')
		putwrecord.setFieldValue('custrecord_uom_level', uomlevel);
	putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
	putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);	

	//code added on 15Feb 2012 by suman
	if(po!=null&&po!="")
		putwrecord.setFieldValue('custrecord_ebiz_order_no',po);

	// commit the record to NetSuite
	t2 = new Date();
	var putwrecordid = nlapiSubmitRecord(putwrecord);
	t1 = new Date();
	nlapiLogExecution('ERROR', 'CreatePUTWwithLocation:nlapiSubmitRecord|trn_opentask',
			getElapsedTimeDuration(t2, t1));
}

function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}
function checkin_palletisation(form, itemID, poLineNum, poSerialNos, poRcptQty, poItemStatus,poQtyChkn,
		poOverage,poItemUOM,poItemPackcode,vbaseuom,uomlevel,vbaseuomqty,vuomqty,location,CaptureExpiryDate,ShelfLife,poLineQty,poQtyRem) 
{
	nlapiLogExecution('ERROR', 'checkin_palletisation : Start', "here");	

	try {


		var AllItemDims = getItemDimensions(itemID);

		var poValue = "";


		// PO line break down
		// Get item dimentions for the item id					

		var eachQty = 0;
		var caseQty = 0;
		var palletQty = 0;
		var noofPallets = 0;
		var noofCases = 0;
		var remQty = 0;
		var tOrdQty=0;
		if(poQtyRem==0)
		{
			tOrdQty = poLineQty;
		}
		else
		{
			tOrdQty = parseInt(poQtyRem);
		}
		var noofEaches=0;
		var ExpiryDate='';
		var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
		var columns = nlapiLookupField('item', itemID, fields);
		var ItemType = columns.recordType;	
		var serialInflg="F";		
		serialInflg = columns.custitem_ebizserialin;
		var batchflag="F";
		batchflag= columns.custitem_ebizbatchlot;
		var itemRec="",itemName,itemDesc;
		nlapiLogExecution('ERROR', 'CheckIn itemType', ItemType);
		if(ItemType == 'inventoryitem')
		{
			nlapiLogExecution('ERROR', 'CheckIn itemType1', ItemType);
			itemRec = nlapiLoadRecord('inventoryitem', itemID);
			itemName = itemRec.getFieldValue('itemid');
			itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('purchasedescription');
			}
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('displayname');
			}		
		}
		else if(ItemType == 'lotnumberedassemblyitem')
		{
			nlapiLogExecution('ERROR', 'CheckIn itemType lotnumberedassemblyitem', ItemType);
			itemRec = nlapiLoadRecord('lotnumberedassemblyitem', itemID);
			itemName = itemRec.getFieldValue('itemid');
			itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('purchasedescription');
			}
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('displayname');
			}
		}
		else if(ItemType == 'lotnumberedinventoryitem')
		{
			nlapiLogExecution('ERROR', 'CheckIn itemType lotnumberedinventoryitem', ItemType);
			itemRec = nlapiLoadRecord('lotnumberedinventoryitem', itemID);
			itemName = itemRec.getFieldValue('itemid');
			itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('purchasedescription');
			}
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('displayname');
			}

		}
		else if(ItemType == 'serializedassemblyitem')
		{
			nlapiLogExecution('ERROR', 'CheckIn itemType serializedassemblyitem', ItemType);
			itemRec = nlapiLoadRecord('serializedassemblyitem', itemID);
			itemName = itemRec.getFieldValue('itemid');
			itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('purchasedescription');
			}
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('displayname');
			}

		}
		else if(ItemType == 'serializedinventoryitem')
		{
			nlapiLogExecution('ERROR', 'CheckIn itemType serializedinventoryitem', ItemType);
			itemRec = nlapiLoadRecord('serializedinventoryitem', itemID);
			itemName = itemRec.getFieldValue('itemid');
			itemDesc = itemRec.getFieldValue('custitem_ebizdescriptionitems');
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('purchasedescription');
			}
			if(itemDesc==null)
			{
				itemDesc = itemRec.getFieldValue('displayname');
			}



		}



		var itemSubList = form.addSubList("custpage_items", "inlineeditor",
		"ItemList");
		itemSubList.addField("custpage_chkn_select", "checkbox", "Select").setDefaultValue('T');
		itemSubList.addField("custpage_chkn_pfvalidateflag", "text", "validateflag")
		.setDisplayType('hidden');

		var ItemLineno=	itemSubList.addField("custpage_chkn_line", "select", "Line #").setDisabled(true);
		//
		if (poLineNum != null){

			ItemLineno.addSelectOption(poLineNum,poLineNum);		
			ItemLineno.setDefaultValue(poLineNum);
			ItemLineno.setDisabled(true);
		}

		var itemname=itemSubList.addField("custpage_chkn_sku", "select", "Item").setDisabled(true);
		//.setDefaultValue(itemName);
		if (itemName != null){

			itemname.addSelectOption(itemName,itemName);		
			itemname.setDefaultValue(itemName);
			itemname.setDisabled(true);
		}

		var itemdesc=itemSubList.addField("custpage_chkn_itemdesc", "select", "Item Desc").setDisabled(true);
		//.setDisabled(true);//.setDefaultValue(itemDesc);
		if (itemDesc != null){

			itemdesc.addSelectOption(itemDesc,itemDesc);		
			itemdesc.setDefaultValue(itemDesc);
			itemdesc.setDisabled(true);
		}

		var vItemStatus = itemSubList.addField("custpage_chkn_skustatus", "select", "Item Status");


		var itemStatusFilters = new Array();
		itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
		itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
		if(location!=null && location!='' && location!='null')
			itemStatusFilters[1] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',location);

		var itemStatusColumns = new Array();
		itemStatusColumns[0] = new nlobjSearchColumn('internalid');
		itemStatusColumns[1] = new nlobjSearchColumn('name');
		itemStatusColumns[0].setSort();
		itemStatusColumns[1].setSort();

		var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);


		nlapiLogExecution('ERROR', 'Location infomartion for check in', location);


		if (itemStatusSearchResult != null){
			for (var i = 0; i < itemStatusSearchResult.length; i++) 
			{					
				vItemStatus.addSelectOption(itemStatusSearchResult[i].getValue('internalid'), itemStatusSearchResult[i].getValue('name'));		
			}
		}

		//upt to here

		if(poItemStatus!=null && poItemStatus!='')
		{
			vItemStatus.setDefaultValue(poItemStatus);
		}


//		if (poItemPackcode != "" && poItemPackcode != null)
//		itemSubList.addField("custpage_chkn_pc", "text", "Pack Code")
//		//.setDefaultValue(poItemPackcode);
//		else
		var itempc=	itemSubList.addField("custpage_chkn_pc", "select", "Pack Code").setDisabled(true);
		//.setDefaultValue('1');
		if (poItemPackcode == null || poItemPackcode=='' || poItemPackcode=='null' ){ //Case# 201410123
		//if (poItemPackcode == null || poItemPackcode=='' ){
			poItemPackcode=1;
		}
		if (poItemPackcode != null){

			itempc.addSelectOption(poItemPackcode,poItemPackcode);		
			itempc.setDefaultValue(poItemPackcode);
			itempc.setDisabled(true);
		}


		var itembaseuom=itemSubList.addField("custpage_chkn_buom", "select", "Base UOM").setDisabled(true);
		//.setDefaultValue(vbaseuom);
		if (vbaseuom != null){

			itembaseuom.addSelectOption(vbaseuom,vbaseuom);		
			itembaseuom.setDefaultValue(vbaseuom);
			itembaseuom.setDisabled(true);
		}

//		if(poItemUOM==null || poItemUOM=='' )
//		poItemUOM=vbaseuom;

		var itemunits=itemSubList.addField("custpage_chkn_units", "select", "Units").setDisabled(true);
		//.setDefaultValue(poItemUOM);
		if(poItemUOM==null || poItemUOM==''  )
		{
			if(vbaseuom!=null && vbaseuom!='')
			{
				poItemUOM=vbaseuom;
			}
		}

		if (poItemUOM != null && poItemUOM!='null'){

			itemunits.addSelectOption(poItemUOM,poItemUOM);		
			itemunits.setDefaultValue(poItemUOM);
			itemunits.setDisabled(true);
		}
		if (poRcptQty != "" && poRcptQty != null)
			itemSubList.addField("custpage_pochkn_qty", "text", "Receive Qty");
		else
			itemSubList.addField("custpage_pochkn_qty", "text", "Receive Qty")
			.setMandatory(true);

		if (poRcptQty != "" && poRcptQty != null)
			itemSubList.addField("custpage_chkn_rcvngqty", "text", "Base Receive Qty");
		else
			itemSubList.addField("custpage_chkn_rcvngqty", "text", "Base Receive Qty").setDisabled(true);
		itemSubList.addField("custpage_chkn_lp", "text", "LP(License Plate)").setMandatory(true);
		/*if(ItemType == 'inventoryitem')
		{
			itemSubList.addField("custpage_lotbatch", "text", "Batch/Lot Numbers").setDisabled(true);
		}
		else
		{
			itemSubList.addField("custpage_lotbatch", "text", "Batch/Lot Numbers").setDefaultValue(poSerialNos);;
		}*/


		if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
		{
			itemSubList.addField("custpage_lotbatch", "text", "LOT#");//.setDefaultValue(poSerialNos);
		}
		else
		{
			itemSubList.addField("custpage_lotbatch", "text", "LOT#").setDisabled(true);
		}
		if(CaptureExpiryDate == 'T')
		{itemSubList.addField("custpage_hdncatchexpdate", "text", "Expiry Date(MMDDYY)");
		//itemSubList.addField("custpage_expdateformat", "text", "Date Format").setDisabled(true).setDefaultValue('(MMDDYY)');
		}
		else
		{
			itemSubList.addField("custpage_hdncatchexpdate", "text", "Expiry Date(MMDDYY)").setDisplayType('hidden');//.setDefaultValue(ExpiryDate);
			//itemSubList.addField("custpage_expdateformat", "text", "Date Format").setDisplayType('hidden').setDefaultValue('(MMDDYY)');
		}

		itemSubList.addField("custpage_capexpirydateflag", "text", "CapExpiryFalg ").setDisplayType('hidden');//.setDefaultValue(CaptureExpiryDate);
		itemSubList.addField("custpage_shelflife", "text", "ShelfLife").setDisplayType('hidden');//.setDefaultValue(ShelfLife);

		var binlocations=itemSubList.addField("custpage_chkn_binlocation", "select", "Bin Location");


		var siteSearchResults = getbinlocations(location,0);

		if (siteSearchResults != null)
		{
			nlapiLogExecution('ERROR', 'siteSearchResults ', siteSearchResults.length);
			binlocations.addSelectOption('','');
			for (var i = 0; siteSearchResults != null && i < siteSearchResults.length; i++) {

				/*	//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
				if(siteSearchResults[i].getValue('name') != null && siteSearchResults[i].getValue('name') != "" && siteSearchResults[i].getValue('name') != " ")
				{
					var resdo = form.getField('custpage_chkn_binlocation').getSelectOptions(siteSearchResults[i].getValue('name'), 'is');
					if (resdo != null) {
						if (resdo.length > 0) {
							continue;
						}
					}
				}*/
				binlocations.addSelectOption(siteSearchResults[i].getId(), siteSearchResults[i].getValue('name'));
			}
		}

		itemSubList.addField("custpage_iteminternalid", "text", "Item Id")
		.setDisplayType('hidden');//.setDefaultValue(itemID);
		itemSubList.addField("custpage_serialno", "text", "Item SerialNo")
		.setDisplayType('hidden');//.setDefaultValue(poSerialNos);
		nlapiLogExecution('ERROR', 'pooverage',	poOverage);
		itemSubList.addField("custpage_pooverage", "text", "PoOverage")
		.setDisplayType('hidden');//.setDefaultValue(poOverage);

		nlapiLogExecution('ERROR', 'poRcptQty',	parseInt(poRcptQty));
		nlapiLogExecution('ERROR', 'poOverage',	poOverage);

		itemSubList.addField("custpage_chkn_qty", "text", "CheckIn Qty").setDisplayType('hidden');
		itemSubList.addField("custpage_uomlevel", "text", "UOM Level").setDisplayType('hidden');//.setDefaultValue(uomlevel);
		itemSubList.addField("custpage_uomqty", "text", "UOM Qty").setDisplayType('hidden');//.setDefaultValue(vuomqty);
		itemSubList.addField("custpage_baseuomqty", "text", "Base UOM Qty").setDisplayType('hidden');//.setDefaultValue(vbaseuomqty);



		if (AllItemDims != null && AllItemDims.length > 0) 
		{							
			for(var p = 0; p < AllItemDims.length; p++)
			{
				var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
				var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');


				nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', itemID);

				var skuDim = AllItemDims[p].getValue('custrecord_ebizuomlevelskudim');
				var skuQty = AllItemDims[p].getValue('custrecord_ebizqty');						
				nlapiLogExecution('ERROR','skuDim',skuDim);
				nlapiLogExecution('ERROR','SKUQTY',skuQty);
				if(skuDim == '1')
				{
					eachQty = skuQty;
				}
				else if(skuDim == '2')
				{
					caseQty = skuQty;
				}
				else if(skuDim == '3')
				{
					palletQty = skuQty;
				}


			}

			if(parseInt(eachQty) == 0)
			{
				nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim EACH is not configured for SKU: '+ itemID);
			}
			if(parseInt(caseQty) == 0)
			{
				nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim CASE is Not Configured for SKU: '+ itemID);
			}
			if(parseInt(palletQty) == 0)
			{
				nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim PALLET is not Configured for SKU: '+ itemID);
			}
			var count=1;
			if(eachQty>0 ||caseQty>0 || palletQty>0)  // break down selected PO line quantity provided Item dims are configured
			{					
				//nlapiLogExecution('ERROR', 'Item', itemId);
				nlapiLogExecution('ERROR', 'Each Qty', eachQty);
				nlapiLogExecution('ERROR', 'Case Qty', caseQty);
				nlapiLogExecution('ERROR', 'Pallet Qty', palletQty);
				nlapiLogExecution('ERROR', 'Ord Qty', tOrdQty);

				// Get the number of pallets required for this item (there will be one LP per pallet)
				if(parseInt(palletQty)>0)
				{
					noofPallets = getSKUDimCount(tOrdQty, palletQty);
					remQty = parseInt(tOrdQty) - (parseInt(noofPallets) * parseInt(palletQty));	

					nlapiLogExecution('ERROR', 'autobreakdown_palletisation:No. of Pallets', noofPallets);
					nlapiLogExecution('ERROR', 'autobreakdown_palletisation:Remaining Quantity', remQty);

					for(var p=0;p<noofPallets;p++)
					{
						// itemSubList.addField("custpage_pochkn_qty", "text", "Receive Qty");
						bindSublist(count,poLineNum,itemName,itemDesc,poItemStatus,poItemPackcode,vbaseuom,poItemUOM,poRcptQty,poSerialNos,ExpiryDate,CaptureExpiryDate,ShelfLife,itemID,poSerialNos,poOverage,uomlevel,vuomqty,vbaseuomqty,palletQty,form);
						count=count+1;
					}

				}


				// check whether we need to break down into cases or not
				if (parseInt(remQty) > 0 && parseInt(caseQty) != 0) 
				{
					// Get the number of cases required for this item (only one LP for all cases)
					noofCases = getSKUDimCount(remQty, caseQty);
					remQty = parseInt(remQty) - (noofCases * caseQty);

					nlapiLogExecution('ERROR', 'autobreakdown_palletisation:No. of Cases', noofCases);
					nlapiLogExecution('ERROR', 'autobreakdown_palletisation:Remaining Quantity', remQty);
					for(var p=0;p<noofCases;p++)
					{
						bindSublist(count,poLineNum,itemName,itemDesc,poItemStatus,poItemPackcode,vbaseuom,poItemUOM,poRcptQty,poSerialNos,ExpiryDate,CaptureExpiryDate,ShelfLife,itemID,poSerialNos,poOverage,uomlevel,vuomqty,vbaseuomqty,caseQty,form);
						count=count+1;
					}
				}

				// check whether we need to further break down into eaches or not (there will be one LP for all eaches)
				if (parseInt(remQty) > 0) 
				{
					bindSublist(count,poLineNum,itemName,itemDesc,poItemStatus,poItemPackcode,vbaseuom,poItemUOM,poRcptQty,poSerialNos,ExpiryDate,CaptureExpiryDate,ShelfLife,itemID,poSerialNos,poOverage,uomlevel,vuomqty,vbaseuomqty,remQty.toString(),form);
				}
				//noofEaches =1;


				//nlapiLogExecution('ERROR', 'No. Of LPs required for line#:' + lineno +'=', noofLpsRequired);






			}// end of auto break down calculation

		} 
		else 
		{ 
			nlapiLogExecution('ERROR', 'UOM Dims', 'For all items, Dimensions are not configured ');
		}			



	} 
	catch(exp) 
	{
		//alert('Expception'+exp);
		nlapiLogExecution('ERROR', 'Exception in checkin_palletisation:', exp);		
		result = "F";
	}


}
function bindSublist(count,poLineNum,itemName,itemDesc,poItemStatus,poItemPackcode,vbaseuom,poItemUOM,poRcptQty,poSerialNos,ExpiryDate,CaptureExpiryDate,ShelfLife,itemID,poSerialNos,poOverage,uomlevel,vuomqty,vbaseuomqty,QTY,form)
{
	//form.getSubList('custpage_items').setLineItemValue('custpage_chkn_line', count, poLineNum);
	// form.getSubList('custpage_items').setLineItemValue('custpage_chkn_sku', count, itemName);
	//form.getSubList('custpage_items').setLineItemValue('custpage_chkn_itemdesc', count, itemDesc);
//	if(poItemStatus!=null && poItemStatus!='')
//	{
//	form.getSubList('custpage_items').setLineItemValue('custpage_chkn_skustatus', count, poItemStatus);
//	}
//	if (poItemPackcode != "" && poItemPackcode != null)
//	{
//	form.getSubList('custpage_items').setLineItemValue('custpage_chkn_pc', count, poItemPackcode);
//	}
//	else
//	{
//	form.getSubList('custpage_items').setLineItemValue('custpage_chkn_pc', count, '1');
//	}
	// form.getSubList('custpage_items').setLineItemValue('custpage_chkn_buom', count, vbaseuom);
//	if(poItemUOM==null || poItemUOM=='' )
//	poItemUOM=vbaseuom;
	// form.getSubList('custpage_items').setLineItemValue('custpage_chkn_units', count, poItemUOM);
	if (poRcptQty != "" && poRcptQty != null)
	{
		form.getSubList('custpage_items').setLineItemValue('custpage_pochkn_qty', count, poRcptQty);
		form.getSubList('custpage_items').setLineItemValue('custpage_chkn_rcvngqty', count, poRcptQty);
	}
	else
	{
		form.getSubList('custpage_items').setLineItemValue('custpage_pochkn_qty', count, QTY);
		form.getSubList('custpage_items').setLineItemValue('custpage_chkn_rcvngqty', count, QTY);
	}

	form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', count, poSerialNos);
	form.getSubList('custpage_items').setLineItemValue('custpage_hdncatchexpdate', count, ExpiryDate);
	form.getSubList('custpage_items').setLineItemValue('custpage_capexpirydateflag', count, CaptureExpiryDate);
	form.getSubList('custpage_items').setLineItemValue('custpage_shelflife', count, ShelfLife);
	form.getSubList('custpage_items').setLineItemValue('custpage_iteminternalid', count, itemID);
	form.getSubList('custpage_items').setLineItemValue('custpage_serialno', count, poSerialNos);
	form.getSubList('custpage_items').setLineItemValue('custpage_pooverage', count, poOverage);
	form.getSubList('custpage_items').setLineItemValue('custpage_uomlevel', count, uomlevel);
	form.getSubList('custpage_items').setLineItemValue('custpage_uomqty', count, vuomqty);
	form.getSubList('custpage_items').setLineItemValue('custpage_baseuomqty', count, vbaseuomqty);
}


function CreateAutoLotNoinBatchEntry(recordType,lotno,itemid,expdate,packcode,whlocation,maufacturelotno,k,batchflag)
{
	nlapiLogExecution('ERROR', 'recordType', recordType);
	nlapiLogExecution('ERROR', 'lotno', lotno);
	nlapiLogExecution('ERROR', 'itemid', itemid);
	nlapiLogExecution('ERROR', 'expdate', expdate);
	nlapiLogExecution('ERROR', 'packcode', packcode);
	nlapiLogExecution('ERROR', 'whlocation', whlocation);
	nlapiLogExecution('ERROR', 'maufacturelotno', maufacturelotno);
	nlapiLogExecution('ERROR', 'k', k);
	nlapiLogExecution('ERROR', 'batchflag', batchflag);
	//if (recordType == 'lotnumberedinventoryitem' || recordType == "lotnumberedassemblyitem" || recordType == "assemblyitem" || custitem_ebizbatchlot == 'T') {
	if (recordType == 'lotnumberedinventoryitem' || recordType == "lotnumberedassemblyitem" || recordType == "assemblyitem" || batchflag == 'T') {

		try {
			nlapiLogExecution('ERROR', 'Auto Gen BATCH FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

			customrecord.setFieldValue('name', lotno);
			customrecord.setFieldValue('custrecord_ebizlotbatch', lotno);
			customrecord.setFieldValue('custrecord_ebizexpirydate', expdate);
			customrecord.setFieldValue('custrecord_ebizfifodate', DateStamp());
			customrecord.setFieldValue('custrecord_ebizsku', itemid);
			customrecord.setFieldValue('custrecord_ebizpackcode',packcode);
			customrecord.setFieldValue('custrecord_ebizsitebatch',whlocation);
			customrecord.setFieldValue('custrecord_ebiz_manufacture_lot',maufacturelotno);

			//customrecord.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
			//customrecord.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
			//customrecord.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
			//customrecord.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);

			nlapiLogExecution('ERROR', 'before rec',' rec');
			var rec = nlapiSubmitRecord(customrecord, false, true);
			nlapiLogExecution('ERROR', 'rec', rec);
			//if(getAutoLotFlag=='Y')
			//{

			//}	

		} 
		catch (e) {
			nlapiLogExecution('ERROR', 'Failed to Update/Insert into BATCH ENTRY Record');
		}
	}
}
/*******************************************************************************
 * This is a User Event Script that generates a link for each line item that
 * points to the Suitelet page above. It is meant to be deployed on the Purchase
 * Order record.
 */
/*
function poAddCheckInLinkUE(type, form, request) {
	// obtain the context object
	var ctx = nlapiGetContext();

	// only execute when the exec context is web browser and 
	// in view mode of the PO
	if (ctx.getExecutionContext() == 'userinterface' && type == 'view') {
		form.getSubList('item').addField('custpage_checkin_link', 'text', 'Check-In', null);

		// obtain the PO internal ID
		var poID = nlapiGetRecordId();
		var poName = nlapiGetFieldValue('tranid');
		// resolve the generic relative URL for the Suitelet
		var checkInURL = nlapiResolveURL('SUITELET', 'customscript_ebiznetcheckin',
				'customdeploy_checkinsuitelet');

		checkInURL = getFQDNForHost(ctx.getEnvironment()) + checkInURL;

		// loop through the item sublist
		for ( var i = 0; i < form.getSubList('item').getLineItemCount(); i++) {

			var quantity = parseFloat(form.getSubList('item').getLineItemValue('quantity', i));
			var quantityRcv = parseFloat(form.getSubList('item').getLineItemValue('quantityreceived', i));

			nlapiLogExecution('ERROR', 'Quantity', quantity);
			nlapiLogExecution('ERROR', 'Rcv Quantity', quantityRcv);

			// generate the hyperlink only when the line has not been fully
			// received
			if (quantityRcv < quantity) {
				var linkURL;

				// add the PO ID, item, and quantity to the URL
				linkURL = checkInURL + '&poID=' + poID;
				linkURL = linkURL + '&poValue=' + poName;
				linkURL = linkURL + '&poLineItem='+ nlapiGetLineItemValue('item', 'item', i + 1);
				linkURL = linkURL + '&poLineQty='+ nlapiGetLineItemValue('item', 'quantity', i + 1);
				linkURL = linkURL + '&poLineNum='+ nlapiGetLineItemValue('item', 'line', i + 1);
				linkURL = linkURL + '&poSerialNos='+ nlapiGetLineItemValue('item', 'serialnumbers', i + 1);
				// log the URL
				nlapiLogExecution('DEBUG', 'check in url', linkURL);

				// populate the URL into the check in field as a hyperlink to
				// the Suitelet
				form.getSubList('item').setLineItemValue('custpage_checkin_link', i + 1,
						'<a href="' + linkURL + '">Check In</a>');
			}
		}
	}
}
 */


function updatePalletQtyWithNEWDims(vitemid,newlength,newwidth,newheight,newweight,uomid)
{
	try
	{
		nlapiLogExecution("ERROR",'Into updatePalletQtyWithNEWDims',vitemid);
		nlapiLogExecution("ERROR",'newlength',newlength);
		nlapiLogExecution("ERROR",'newwidth',newwidth);
		nlapiLogExecution("ERROR",'newheight',newheight);
		nlapiLogExecution("ERROR",'newweight',newweight);
		if(vitemid!=null&&vitemid!="")
		{
			var filter=new Array();
			filter.push(new nlobjSearchFilter("custrecord_ebizitemdims",null,"anyof",vitemid));  //case 201410766 start
			//filter.push(new nlobjSearchFilter("custrecord_ebizuomskudim",null,"anyof",3));//3=Pallet
			filter.push(new nlobjSearchFilter("custrecord_ebizuomskudim",null,"anyof",uomid));//3=Pallet
			  //case 201410766 end
			filter.push(new nlobjSearchFilter("custrecord_ebizuomlevelskudim",null,"anyof",3));

			var column=new Array();
			column[0]=new nlobjSearchColumn("custrecord_ebizqty");
			
			var search=nlapiSearchRecord('customrecord_ebiznet_skudims',null,filter,column);

			if(search!=null&&search!="")
			{
				var palletqty=search[0].getValue("custrecord_ebizqty");
				nlapiLogExecution("ERROR","palletqty",palletqty);

				var recid=search[0].getId();
				nlapiLogExecution("ERROR","recid",recid);
				// Case # 20127929 starts
				//var palletlength=palletqty*newlength;
				//var palletwidth=palletqty*newwidth;
				//var palletheight=palletqty*newheight;
				//var palletweight=palletqty*newweight;
				var palletlength=newlength;
				var palletwidth=newwidth;
				var palletheight=newheight;
				var palletweight=newweight;
				// Case # 20127929 ends
				var palletcube=palletlength*palletwidth*palletheight;
				nlapiLogExecution("ERROR",'palletlength',palletlength);
				nlapiLogExecution("ERROR",'palletwidth',palletwidth);
				nlapiLogExecution("ERROR",'palletheight',palletheight);
				nlapiLogExecution("ERROR",'palletweight',palletweight);
				nlapiLogExecution("ERROR",'palletcube',palletcube);

				var fields=new Array();
				fields.push("custrecord_ebizlength");
				fields.push("custrecord_ebizwidth");
				fields.push("custrecord_ebizheight");
				fields.push("custrecord_ebizweight");
				fields.push("custrecord_ebizcube");

				var values=new Array();
				values.push(palletlength);
				values.push(palletwidth);
				values.push(palletheight);
				values.push(palletweight);
				values.push(palletcube);

				var recupdated=nlapiSubmitField("customrecord_ebiznet_skudims",recid,fields,values);
				nlapiLogExecution("ERROR","recupdated",recupdated);
			}
		}
		else
			nlapiLogExecution("ERROR","Cant update pallet qty as item id is null");

	}
	catch(exp)
	{

	}
}

function getItemDimensions(itemID){
	//nlapiLogExecution('ERROR', 'In Item Dimensions', "here1");
	var retItemDimArray = null;

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemID));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomskudim');
	columns[3] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');
	columns[0].setSort(true);

	retItemDimArray = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, columns);
	nlapiLogExecution('ERROR', 'retItemDimArray', retItemDimArray.length);
	return retItemDimArray;
}


var binLocationArray=new Array();
function getbinlocations(loc,maxno)
{

	var siteFilters = new Array();
	siteFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', loc));
	siteFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(maxno!=0)
	{
		siteFilters.push(new nlobjSearchFilter('id', null, 'lessthan',maxno));

	}

	var siteColumns = new Array();
	siteColumns[0] = new nlobjSearchColumn('name');
	siteColumns[1] = new nlobjSearchColumn('id');	
	siteColumns[1].setSort(true);

	var siteSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, siteFilters, siteColumns);

	if (siteSearchResults != null && siteSearchResults.length>=1000)
	{
		for(var k=0;k<siteSearchResults.length;k++)
		{
			binLocationArray[binLocationArray.length]=siteSearchResults[k];
		}
		var maxno=siteSearchResults[siteSearchResults.length-1].getId();
		getbinlocations(loc,maxno);

	}
	else
	{
		for(var k=0;k<siteSearchResults.length;k++)
		{
			binLocationArray[binLocationArray.length]=siteSearchResults[k];
		}
	}
	return binLocationArray;
}
