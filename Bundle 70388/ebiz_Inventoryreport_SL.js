/***************************************************************************
 	eBizNET Solutions					
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/ebiz_Inventoryreport_SL.js,v $
<<<<<<< ebiz_Inventoryreport_SL.js
 *     	   $Revision: 1.17.4.5.4.7.4.16.2.7 $
 *     	   $Date: 2015/05/07 21:47:18 $
 *     	   $Author: skreddy $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_94 $
=======
 *     	   $Revision: 1.17.4.5.4.7.4.16.2.7 $
 *     	   $Date: 2015/05/07 21:47:18 $
 *     	   $Author: skreddy $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_94 $
>>>>>>> 1.17.4.5.4.7.4.16.2.5
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Inventoryreport_SL.js,v $
 * Revision 1.17.4.5.4.7.4.16.2.7  2015/05/07 21:47:18  skreddy
 * Case# 201412686
 * LP SB1 issue fix
 *
 * Revision 1.17.4.5.4.7.4.16.2.6  2014/09/25 15:43:39  grao
 * Case#: 201410533  Adding Bin Location group field in SportsHq Production
 *
 * Revision 1.17.4.5.4.7.4.16.2.5  2014/08/08 15:18:21  sponnaganti
 * Case# 20149507 20149904
 * Stnd Bundle Issue fix
 *
 * Revision 1.17.4.5.4.7.4.16.2.4  2014/08/01 14:22:10  rmukkera
 * Case # 20149284
 *
 * Revision 1.17.4.5.4.7.4.16.2.3  2014/07/30 15:35:14  sponnaganti
 * Case# 20149507
 * Stnd Bundle Issue fix
 *
 * Revision 1.17.4.5.4.7.4.16.2.2  2014/07/28 16:13:41  sponnaganti
 * Case# 20149727
 * Stnd Bundle Issue fix
 *
 * Revision 1.17.4.5.4.7.4.16.2.1  2014/07/25 13:49:27  skreddy
 * case # 20149697
 * Nautlus prod issue fix
 *
 * Revision 1.17.4.5.4.7.4.16  2014/06/20 14:39:57  rmukkera
 * no message
 *
 * Revision 1.17.4.5.4.7.4.15  2014/06/09 15:27:24  sponnaganti
 * case# 20148798
 * Stnd Bundle Issue Fix
 *
 * Revision 1.17.4.5.4.7.4.14  2014/04/15 13:10:00  snimmakayala
 * *** empty log message ***
 *
 * Revision 1.17.4.5.4.7.4.13  2014/03/27 16:03:33  skavuri
 * Case # 20127849 issue fixed
 *
 * Revision 1.17.4.5.4.7.4.12  2014/03/12 06:23:03  nneelam
 * case#  20127637
 * Standard Bundle Issue Fix.
 *
 * Revision 1.17.4.5.4.7.4.11  2013/11/23 02:50:59  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20125688
 *
 * Revision 1.17.4.5.4.7.4.10  2013/11/22 07:15:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20125860
 *
 * Revision 1.17.4.5.4.7.4.9  2013/09/06 00:24:53  nneelam
 * Case#.20124288�
 * Inventory Report  Issue Fix..
 *
 * Revision 1.17.4.5.4.7.4.8  2013/09/02 15:48:12  skreddy
 * Case# 20124201
 * standard bundle issue fix
 *
 * Revision 1.17.4.5.4.7.4.7  2013/06/20 20:06:46  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 *
 * Revision 1.17.4.5.4.7.4.6  2013/06/03 14:20:19  schepuri
 * PCT SB Export to Excel issue
 *
 * Revision 1.17.4.5.4.7.4.5  2013/05/08 15:12:00  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.17.4.5.4.7.4.4  2013/04/16 15:04:23  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.17.4.5.4.7.4.3  2013/03/22 11:44:32  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.17.4.5.4.7.4.2  2013/03/05 14:49:48  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.17.4.5.4.7.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.17.4.5.4.7  2013/01/08 14:17:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.17.4.5.4.6  2012/12/27 15:56:35  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17.4.5.4.5  2012/12/11 16:26:26  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17.4.5.4.4  2012/11/23 06:35:53  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue related Displaying item info same for all items
 *
 * Revision 1.17.4.5.4.3  2012/11/09 14:24:29  schepuri
 * CASE201112/CR201113/LOG201121
 * added special instruction field
 *
 * Revision 1.17.4.5.4.2  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.17.4.5.4.1  2012/10/26 13:59:05  skreddy
 * CASE201112/CR201113/LOG201121
 * Added new a link to allocated Qty Field
 *
 * Revision 1.17.4.5  2012/05/23 14:16:17  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17.4.4  2012/05/16 08:54:03  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17.4.3  2012/05/16 06:41:45  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17.4.2  2012/05/14 13:52:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Inventory Report
 *
 * Revision 1.17.4.1  2012/04/20 12:10:20  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.17  2011/11/18 22:57:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Delete record from inventory if QOH becomes 0
 *
 * Revision 1.16  2011/10/24 15:26:23  rmukkera
 * CASE201112/CR201113/LOG201121
 * serail# field was removed from sublist
 *
 * Revision 1.15  2011/10/21 17:48:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Added Warehouse Location
 *
 * Revision 1.14  2011/10/21 13:46:06  rmukkera
 * CASE201112/CR201113/LOG201121
 * bugs fixed
 *
 * Revision 1.13  2011/10/20 15:33:11  rmukkera
 * CASE201112/CR201113/LOG201121
 * paging functionality added
 *
 * Revision 1.12  2011/10/13 17:24:35  mbpragada
 * CASE201112/CR201113/LOG201121
 * Report view based on location and SKU
 *
 * Revision 1.11  2011/10/11 13:45:19  mbpragada
 * CASE201112/CR201113/LOG201121
 * Added  ALL LOV to Inventory Status
 *
 * Revision 1.10  2011/10/11 13:11:56  mbpragada
 * CASE201112/CR201113/LOG201121
 * Added  ALL LOV to Inventory Status
 *
 * Revision 1.9  2011/10/11 12:38:29  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * ALL  LOV is added in query block in 1.8 version. CVS header is added for that and its 1.9 ver
 *
 * Revision 1.5  2011/09/30 08:43:07  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * The version code 1.1.2.3 is merged with 1.4.
 * This will be the latest with kitting
 *
 * Revision 1.4  2011/09/28 11:19:39  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Kit to order functionality is divided into two proceses. all the logic is incorporated
 *
 * Revision 1.3  2011/07/21 05:30:54  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
var pagesize;
function addItemsToList(fieldName, list, variable, distinctOnlyFlag, displayTextFlag){
	logTime('addLPsToList', 'Start');

	fieldName.addSelectOption('', '');			// Blank option

	if(list != null && list.length > 0){
		for(var i = 0; i < list.length; i++){
			var value = list[i].getValue(variable);
			var text = list[i].getText(variable);
			if(distinctOnlyFlag){
				var index;

				if(displayTextFlag)
					index = fieldName.getSelectOptions(text, 'is');
				else
					index = fieldName.getSelectOptions(value, 'is');

				if(index != null && index.length > 0)
					continue;
			}
			if(displayTextFlag)
				fieldName.addSelectOption(value, text);
			else
				fieldName.addSelectOption(value, value);
		}
	}

	logTime('addLPsToList', 'End');
}

function getLPList(){
	logTime('getLPList', 'Start');
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'isnot', '');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[0].setSort();

	var lpList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	logCountMessage('ERROR', lpList);
	logTime('getLPList', 'End');
	return lpList;
}

function getLotNoList(){
	logTime('getLotNoList', 'Start');
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'noneof', '@NONE@');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns[0].setSort();

	var lotNoList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	logCountMessage('ERROR', lotNoList);
	logTime('getLotNoList', 'End');
	return lotNoList;
}

function getItemZoneList(){
	logTime('getItemZoneList', 'Start');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_putzoneid');
	columns[0].setSort();

	var itemZoneList = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, null, columns);
	logCountMessage('ERROR', itemZoneList);
	logTime('getItemZoneList', 'Start');
	return itemZoneList;
}

function getBinLocnList(){
	logTime('getBinLocnList', 'Start');
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', '@NONE@');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[0].setSort();

	var binLocnList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	logCountMessage('ERROR', binLocnList);
	logTime('getBinLocnList', 'End');
	return binLocnList;
}

function addSublistToForm(form){
	logTime('addSublistToForm', 'Start');

	var sublist = form.addSubList("custpage_items", "list", "Inventory Report");
	sublist.addField("custpage_no", "text", "SL NO").setDisplayType('disabled');
	sublist.addField("custpage_sku", "select", "Item", "item").setDisplayType('inline');
	sublist.addField("custpage_skuintid", "text", "ItemIntId").setDisplayType('hidden');
	
	sublist.addField("custpage_skudesc", "textarea", "Item Desc");
	sublist.addField("custpage_whloc", "text", "Location");
	sublist.addField("custpage_fifodate", "text", "FIFO Date");
	sublist.addField("custpage_expirydate", "text", "Expiry Date");
	sublist.addField("custpage_packcode", "text", "Pkg #");
	sublist.addField("custpage_skustatus", "select", "Item Status", 'customrecord_ebiznet_sku_status').setDisplayType('inline');

	if(request.getParameter('custpage_reportviewby') == "a")
	{	
		sublist.addField("custpage_modelno", "text", "Model");
		sublist.addField("custpage_invtlp", "text", "LP #");

		//sublist.addField("custpage_serialno", "textarea", "Serial #");
	}
	sublist.addField("custpage_lotbat", "text", "LOT#");
	if(request.getParameter('custpage_reportviewby') == "a")
	{	
		var displayBinLocation = sublist.addField("custpage_actendloc", "text", "Bin Location", 'customrecord_ebiznet_location').setDisplayType('inline');
		displayBinLocation.setDisplaySize(50);
	}
	//sublist.addField("custpage_totalqty", "text", "Total Qty");
	sublist.addField("custpage_allocqty", "text", "Alloc Qty");
	/*	sublist.addField("custpage_availqty", "text", "Avail Qty");
	sublist.addField("custpage_resvqty", "text", "Rsv Qty");*/
	/*sublist.addField("custpage_availqty", "text", "Avail Qty");
	sublist.addField("custpage_resvqty", "text", "Rsv Qty");*/
	sublist.addField("custpage_qtyonhand", "text", "Quantity On Hand");
	sublist.addField("custpage_availqty", "text", "Avail Qty");
	sublist.addField("custpage_invstatus", "text", "Inventory Status").setDisplayType('hidden');
	if(request.getParameter('custpage_reportviewby') == "a")
	{	
		sublist.addField("custpage_iteminfo1", "text", "Item Info1");
		sublist.addField("custpage_iteminfo2", "text", "Item Info2");
		sublist.addField("custpage_binlocgrp", "text", "BinLocation Group");
		sublist.addField("custpage_zone", "text", "Zone");
	}
	//Adding status flag to the sublist, code added by suman
	sublist.addField("custpage_statusflag", "select", "StatusFlag", "customrecord_wms_status_flag").setDisplayType('inline');
	//end of code
	logTime('addSublistToForm', 'End');
}

function createInventoryReportForm(request, form, subListIndicator){
	//logTime('createInventoryReportForm', 'Start');

	// Item Family
	var itemFamily = form.addField('custpage_itemfamily', 'select', 'Item Family', 'customrecord_ebiznet_sku_family');
	itemFamily.setMandatory(false);

	// Item Group
	var itemGroup = form.addField('custpage_itemgroup', 'select', 'Item Group', 'customrecord_ebiznet_sku_group');
	itemGroup.setMandatory(false);

	// Item
	var item = form.addField('custpage_item', 'select', 'Item', 'Item');

	// Item Status
	var itemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status', 'customrecord_ebiznet_sku_status');

	// Inventory Status
	var inventoryStatus = form.addField('custpage_inventorysts', 'select', 'Inventory Status');
	inventoryStatus.addSelectOption('1','ALL');
	inventoryStatus.addSelectOption('2','INBOUND  Staging');
	inventoryStatus.addSelectOption('3','OUTBOUND Staging');
	inventoryStatus.addSelectOption('4', 'STORAGE ');
	inventoryStatus.setDefaultValue(request.getParameter('custpage_inventorysts'));

	// Report View By Field
	var reportViewBy = form.addField('custpage_reportviewby', 'select', 'Report View By');
	reportViewBy.addSelectOption('a', 'Location');
	reportViewBy.addSelectOption('b', 'SKU');

	// Licence Plate
	var lpNo = form.addField('custpage_lp', 'select', 'LP #','customrecord_ebiznet_master_lp');
//	var lpList = getLPList();
//	addItemsToList(lpNo, lpList, 'custrecord_ebiz_inv_lp', false, false);

	// Lot / Batch Number
	//case# 20148798 starts
	/*var lotBatchNo = form.addField('custpage_lotbatch', 'select', 'LOT#');
	var lotNoList = getLotNoList();
	addItemsToList(lotBatchNo, lotNoList, 'custrecord_ebiz_inv_lot', true, true);
*/
	var lotBatchNo = form.addField('custpage_lotbatch', 'select', 'LOT#','customrecord_ebiznet_batch_entry');
	//case# 20148798 ends
	// Bin Location
	//var binLocnField = form.addField('custpage_binlocation', 'select', 'Bin Location','customrecord_ebiznet_createinv');
	var binLocnField = form.addField('custpage_binlocation', 'select', 'Bin Location','customrecord_ebiznet_location');
//	var binLocnList = getBinLocnList();
//	addItemsToList(binLocnField, binLocnList, 'custrecord_ebiz_inv_binloc', true, true);

	// Item Zone
	var itemZone = form.addField('custpage_itemzone', 'select', 'Zone');
	var itemZoneList = getItemZoneList();
	addItemsToList(itemZone, itemZoneList, 'custrecord_putzoneid', false, false);
	/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle; ***/
	// Added for Location select field
	var Location = form.addField('custpage_loc_id', 'select', 'Location',
	'location');
	/*** up to here ***/
	var binlocGroupField = form.addField('custpage_binlocgroup', 'select', 'Bin Location Group','customrecord_ebiznet_loc_group');
	
	
	pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('entry');
	pagesize.setDisplaySize(10,10);
	pagesize.setLayoutType('outsidebelow', 'startrow');

	if (request.getMethod() == 'GET'){

		pagesize.setDefaultValue("50");
		pagesize.setDisplayType('hidden');

	}
	if (request.getMethod() == 'POST'){

		if(request.getParameter('custpage_pagesize')!=null ){

			pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

		}
		/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle; ***/
		// added Location feild
		if(request.getParameter('custpage_loc_id')!=null){
			Location.setDefaultValue(request.getParameter('custpage_loc_id'));}
		/*** up to here ***/
		if(request.getParameter('custpage_itemfamily')!=null){
			itemFamily.setDefaultValue(request.getParameter('custpage_itemfamily'));}

		if(request.getParameter('custpage_itemgroup')!=null){
			itemGroup.setDefaultValue(request.getParameter('custpage_itemgroup'));}

		if(request.getParameter('custpage_itemstatus')!=null){
			itemStatus.setDefaultValue(request.getParameter('custpage_itemstatus'));}

		if(request.getParameter('custpage_item')!=null){
			item.setDefaultValue(request.getParameter('custpage_item'));}

		if(request.getParameter('custpage_lp')!=null){
			lpNo.setDefaultValue(request.getParameter('custpage_lp'));}

		if(request.getParameter('custpage_lotbatch')!=null){
			lotBatchNo.setDefaultValue(request.getParameter('custpage_lotbatch'));}

		if(request.getParameter('custpage_binlocation')!=null){
			binLocnField.setDefaultValue(request.getParameter('custpage_binlocation'));}
		//itemZone.setDefaultValue(request.getParameter('custpage_itemgroup'));
		if(request.getParameter('custpage_binlocgroup')!=null){
			binlocGroupField.setDefaultValue(request.getParameter('custpage_binlocgroup'));}

		if(request.getParameter('custpage_itemzone')!=null){
			itemZone.setDefaultValue(request.getParameter('custpage_itemzone'));	
		}
	}

	// Submit Button; Label: Display
	var button = form.addSubmitButton('Display');

	// Adding sublist to the form
	if(subListIndicator)
		addSublistToForm(form);

	logTime('createInventoryReportForm', 'End');
}

function ebiznet_InventoryReport_UI(request, response){
	if (request.getMethod() == 'GET'){
		var form = nlapiCreateForm('Inventory Report');
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start of Inventory' + i,context.getRemainingUsage());
		createInventoryReportForm(request, form, false);


		response.writePage(form);

	} else {
		var form = nlapiCreateForm("Inventory Report");
		var ctx = nlapiGetContext();
		createInventoryReportForm(request, form, true);
		var arrayLP = new Array();
		var lpcolumns = new Array();
		lpcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
		lpcolumns[1] = new nlobjSearchColumn('custrecord_serialnumber');
		var lpsearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, null, lpcolumns);
		for (var i = 0; lpsearchresults != null && i < lpsearchresults.length; i++) {
			var SerialLP = new Array();
			SerialLP[0] = lpsearchresults[i].getValue('custrecord_serialparentid');
			SerialLP[1] = lpsearchresults[i].getValue('custrecord_serialnumber');
			arrayLP.push(SerialLP);
		}

		// Filling the details from opentask table.     
		var reportview  = request.getParameter('custpage_reportviewby');

		var filters = new Array();
		var i = 0; var lpname="";   

		var pagesizevalue= request.getParameter('custpage_pagesize');
		/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle; ***/ 
		var site=request.getParameter('custpage_loc_id');
		/*** up to here ***/
		var invStatus  = request.getParameter('custpage_inventorysts');
		if(request.getParameter('custpage_lp') !=null && request.getParameter('custpage_lp') !=""){
			var fields = ['custrecord_ebiz_lpmaster_lp'];
			var columns = nlapiLookupField('customrecord_ebiznet_master_lp', request.getParameter('custpage_lp'), fields);
			lpname = columns.custrecord_ebiz_lpmaster_lp;
		}

		nlapiLogExecution('ERROR','lpname',lpname);

		nlapiLogExecution('ERROR','Value of Inventory Flag.',request.getParameter('custpage_inventorysts'));
		if (request.getParameter('custpage_inventorysts') != "") {
			nlapiLogExecution('ERROR','invStatus',invStatus);
			if(invStatus == '4')
			{
				filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']);
			}
			else if(invStatus == '3')
			{
				filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['18']);
			}  
			else if(invStatus == '2')      			
			{
				filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['1','17']);
			}
			else if(invStatus == '1')      			
			{
				//case# 20149507 and 20149904 starts(added 36=FLAG.INVENTORY.WIP to filter)
				//filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['1','3','17','18','19']);
				filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['1','3','17','18','19','36']);
				//case# 20149507 and 20149904 ends
			}
			i++;

		}

		if (request.getParameter('custpage_itemfamily') != "") {
			//nlapiLogExecution('ERROR','itemFamily',request.getParameter('custpage_itemfamily'));
			filters[i] = new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_inv_sku', 'is', request.getParameter('custpage_itemfamily'));
			i++;
		}

		if (request.getParameter('custpage_itemgroup') != "") {
			filters[i] = new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_inv_sku', 'is', request.getParameter('custpage_itemgroup'));
			i++;
		}

		if (request.getParameter('custpage_item') != "") {
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', request.getParameter('custpage_item'));
			i++;
		}

		if (request.getParameter('custpage_itemstatus') != "") {
			//nlapiLogExecution('ERROR','SKUSTATUS',request.getParameter('custpage_itemstatus'));
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', request.getParameter('custpage_itemstatus'));
			i++;
		}
		if (lpname != "" && lpname !=null) {

			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lpname);
			i++;
		}
		if (request.getParameter('custpage_lotbatch') != "") {
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', request.getParameter('custpage_lotbatch'));
			i++;
		}
		nlapiLogExecution('ERROR','Location Group',request.getParameter('custpage_binlocgroup'));
		if (request.getParameter('custpage_binlocgroup') != "") {
			filters[i] = new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', request.getParameter('custpage_binlocgroup'));
			//filters[i] = new nlobjSearchFilter('custrecord_inboundinvlocgroupid', 'anyof', request.getParameter('custpage_binlocgroup'));
			i++;
		}
		if (request.getParameter('custpage_binlocation') != "") {
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', request.getParameter('custpage_binlocation'));
			i++;
		}
		/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle; ***/ 
		//case# 20149727 starts(commented because of we are filtering location under vRoleLocation)
		/*if (request.getParameter('custpage_loc_id') != "") {
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', request.getParameter('custpage_loc_id'));
			i++;
		}*//*** upto here ***/
		//case# 20149727 ends

		var zonecols = new Array();
		zonecols[0] = new nlobjSearchColumn('custrecord_locgroup_no'); 
		zonecols[1]=new nlobjSearchColumn('custrecord_putzoneid','custrecordcustrecord_putzoneid');
		var zonefilters = new Array();

		if (request.getParameter('custpage_itemzone') != "") { 


			var warehouseZoneFilter =new Array();
			warehouseZoneFilter[0] =new nlobjSearchFilter('custrecord_putzoneid', null, 'is',request.getParameter('custpage_itemzone'));

			var warehouseZoneresult =nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, warehouseZoneFilter, null);
			nlapiLogExecution('DEBUG','zoneID',warehouseZoneresult[0].getId());
			zonefilters[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof',warehouseZoneresult[0].getId());


		} 

		nlapiLogExecution('DEBUG','zone',request.getParameter('custpage_itemzone'));
		var zonessearchresult=nlapiSearchRecord('customrecord_zone_locgroup', null, zonefilters, zonecols);
		nlapiLogExecution('DEBUG','zonessearchresult',zonessearchresult);
		if (request.getParameter('custpage_itemzone') != "") { 
			var zonelocationgroupArray= new Array();
			if(zonessearchresult !=null && zonessearchresult!='')
			{
				for(var k1=0;k1<zonessearchresult.length;k1++){

					zonelocationgroupArray.push(zonessearchresult[k1].getValue('custrecord_locgroup_no'));	
				}
			}
			nlapiLogExecution('DEBUG','zonelocationgroupArray',zonelocationgroupArray);
			if(zonelocationgroupArray!=null && zonelocationgroupArray!='' && zonelocationgroupArray.length > 0)
			{
				filters[i] = new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', zonelocationgroupArray);
				i++;
				filters[i] = new nlobjSearchFilter('custrecord_outboundinvlocgroupid', null, 'anyof', zonelocationgroupArray);
				i++;
			}
			else
				{
				filters[i] = new nlobjSearchFilter('custrecord_inboundinvlocgroupid', null, 'anyof', 0);
				i++;
				filters[i] = new nlobjSearchFilter('custrecord_outboundinvlocgroupid', null, 'anyof', 0);
				i++;
				}

		}

		filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
		/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle ***/  
		var vRoleLocation=getRoledBasedLocation();
		nlapiLogExecution('DEBUG','vRoleLocation',vRoleLocation);
		nlapiLogExecution('DEBUG','request.getParameter(custpage_loc_id)',request.getParameter('custpage_loc_id'));
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			if(request.getParameter('custpage_loc_id') ==null || request.getParameter('custpage_loc_id')=='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',vRoleLocation));
			else
			{
				nlapiLogExecution('ERROR','index',vRoleLocation.indexOf(request.getParameter('custpage_loc_id')));
				nlapiLogExecution('ERROR','index',vRoleLocation.indexOf(parseInt(request.getParameter('custpage_loc_id'))));
				//if(vRoleLocation.indexOf(parseInt(request.getParameter('custpage_loc_id')))!=-1)
				//if(vRoleLocation.indexOf(request.getParameter('custpage_loc_id'))!=-1)//case# 20149727
				if(vRoleLocation.indexOf(parseInt(request.getParameter('custpage_loc_id')))!=-1)
				{
					
					filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
				}
				else
					filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',["@NONE@"]));
			}
		}
		else
		{
			if(request.getParameter('custpage_loc_id') !=null && request.getParameter('custpage_loc_id') !='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
		}
		
	/*	
		
		
		
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			if(request.getParameter('custpage_loc_id') ==null || request.getParameter('custpage_loc_id')=='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',vRoleLocation));
			else
			{
				nlapiLogExecution('DEBUG','index',vRoleLocation.indexOf(request.getParameter('custpage_loc_id')));
				if(vRoleLocation.indexOf(request.getParameter('custpage_loc_id'))!=-1)
					filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
				else
					filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',["@NONE@"]));
			}
		}
		else
		{
			if(request.getParameter('custpage_loc_id') !=null && request.getParameter('custpage_loc_id') !='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
		}	*/
		//upto here
		var columns = new Array();
		if( reportview== "a"){

			columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku'); //Item
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_itemdesc'); // Item Desc
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'); //Item Status
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lot'); //Batch no
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo'); //FIFO Date
			columns[5] = new nlobjSearchColumn('custrecord_ebiz_expdate');//Expiry Date
			columns[6] = new nlobjSearchColumn('custrecord_ebiz_model'); //Model no
			columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');//Packcode
			columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_qty'); //Total Qty
			columns[9] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty'); //Allocated Qty
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_qoh'); //Avail Qty
			columns[11] = new nlobjSearchColumn('custrecord_ebiz_resinventory'); //Resv Qty
			columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');//invt Lp
			columns[13] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columns[14] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');       
			columns[15] = new nlobjSearchColumn('custitem_item_info_1','custrecord_ebiz_inv_sku');
			columns[16] = new nlobjSearchColumn('custitem_item_info_2','custrecord_ebiz_inv_sku'); 
			columns[17] = new nlobjSearchColumn('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc');
			columns[18] = new nlobjSearchColumn('internalid');
			columns[18].setSort();
			columns[19] = new nlobjSearchColumn('custrecord_wms_inv_status_flag');
			columns[20] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
			columns[21] = new nlobjSearchColumn('custrecord_ebiz_avl_qty'); //Avail Qty
//case 20124201 start
			columns[22] = new nlobjSearchColumn('custrecord_inv_ebizsku_no'); //end
		}
		else if(reportview == "b"){

			columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku', null, 'group');  //Item
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_itemdesc', null, 'group');  // Item Desc
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status', null, 'group');  //Item Status
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lot', null, 'group');  //Batch no
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo', null, 'group');  //FIFO Date
			columns[5] = new nlobjSearchColumn('custrecord_ebiz_expdate', null, 'group'); //Expiry Date
			columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode', null, 'group'); //Packcode
			columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_qty', null, 'group');  //Total Qty
			columns[8] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty', null, 'sum'); ; //Allocated Qty
			columns[9] = new nlobjSearchColumn('custrecord_ebiz_qoh', null, 'sum');  //Avail Qty
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_resinventory', null, 'sum');  //Resv Qty
			columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_lot', null, 'group');        
			columns[12] = new nlobjSearchColumn('internalid', null, 'max');
			columns[12].setSort();
			columns[13] = new nlobjSearchColumn('custrecord_wms_inv_status_flag', null, 'group');
			columns[14] = new nlobjSearchColumn('custrecord_ebiz_inv_loc', null, 'group');
			columns[15] = new nlobjSearchColumn('custrecord_ebiz_avl_qty', null, 'sum');  //Avail Qty
				columns[16] = new nlobjSearchColumn('custrecord_inv_ebizsku_no', null, 'group');

		}
		// execute the  search by passing the parameters as filters and getting the output columns by providing coluns as paramerts


		var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);	
		var test='';
		var statusflag='';
		nlapiLogExecution('ERROR','searchresults',searchresults);

		if(searchresults!=null)
		{
			if(parseFloat(pagesizevalue)>searchresults.length)
			{
				pagesizevalue=50;
				pagesize.setDefaultValue('50');
			}
			// paging dropdown
			if(searchresults.length>50)
			{
				var select= form.addField('custpage_selectpage','select', 'Select Page');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(150,30);

				var selectno=request.getParameter('custpage_selectpage');
				var len=searchresults.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>searchresults.length)
					{
						to=searchresults.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);





				} 
			}
		}



		if(searchresults!=null)
		{

			if(searchresults.length>=1000)
			{
				var maxno;
				if( reportview== "a"){
					maxno=searchresults[searchresults.length-1].getValue(columns[18]);
				}
				else if(reportview == "b"){
					maxno=searchresults[searchresults.length-1].getValue(columns[12]);	
				}


				var maxsearchresult=getSearchResults(maxno,columns,reportview);

				var tempcount=0;var maxlen=0;
				if(maxsearchresult!=null){
//					for (var k = 0; k < maxsearchresult.length; k++) {

//					var search=maxsearchresult[k];
//					maxlen=maxlen+search.length;
//					}

					for (var j = 0; j < maxsearchresult.length; j++) {


						var search=maxsearchresult[j];

						if(search!=null)
						{
							tempcount=tempcount+1000;
							var len=search.length/parseFloat(pagesizevalue);
							nlapiLogExecution('ERROR', 'len',len);
							for(var k=1;k<=Math.ceil(len);k++)
							{
								var to;var from;var to1;var from1;


								to=parseFloat(tempcount)+(parseFloat(pagesizevalue)*parseFloat(k));
								from=(parseFloat(to)-parseFloat(pagesizevalue))+1;



								to1=(parseFloat(pagesizevalue)*parseFloat(k));

								if(to1>search.length)
								{
									to1=search.length;
									to=parseFloat(tempcount)+(search.length);

								}

								from1=(parseFloat(to1)-parseFloat(pagesizevalue))+1;



								var temp=from.toString()+" to "+to.toString(); var tempto;
								if(to1==search.length)
								{
									var c=parseFloat(from)-parseFloat(tempcount);
									tempto=c.toString()+","+to1.toString()+","+tempcount.toString();
									test=tempto;  
								}
								else
								{
									tempto=from1.toString()+","+to1.toString()+","+tempcount.toString();

								}
								select.addSelectOption(tempto,temp);	

							} 



						}

					}
				}

			}
		}
		if(searchresults!=null)
		{
			if(searchresults.length>50)
			{
				if (request.getParameter('custpage_selectpage')!="" && request.getParameter('custpage_selectpage')!=null)
				{

					var temp= request.getParameter('custpage_selectpage');
					var temparray=temp.split(',');
					nlapiLogExecution('ERROR', 'temparray',temparray.length);

					var diff=parseFloat(temparray[1])-(parseFloat(temparray[0])-1);
					nlapiLogExecution('ERROR', 'diff',diff);

					var pagevalue=request.getParameter('custpage_pagesize');
					nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
					nlapiLogExecution('ERROR', 'test',test);
					nlapiLogExecution('ERROR', 'selectno',selectno);
					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{
						if(select!=null)
						{
							select.setDefaultValue(selectno);}
					}

				}
			}
		}

		var sku='', skudesc='', skustatus='', batchno='', fifodate='', expirydate='', modelno='', skuIntId='',
		actendloc='', packcode='', expeqty='', inventorysts='', totalqty='', allocqty='', resvqty='', 
		availqty='', lotbat='', invtLp='', item='', iteminfo1='', iteminfo2='', binloc='', binlocgroup='', binlocgroupvalue='',
		whlocation='',qtyonHand='';

		/*var zonecols = new Array();
		zonecols[0] = new nlobjSearchColumn('custrecord_locgroup_no'); 
		zonecols[1]=new nlobjSearchColumn('custrecordcustrecord_putzoneid');
		var zonefilters = new Array();

		if (request.getParameter('custpage_itemzone') != "") { 


			zonefilters[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is',request.getParameter('custpage_itemzone'));


		} 


		var zonessearchresult=nlapiSearchRecord('customrecord_zone_locgroup', null, zonefilters, zonecols);*/

		//nlapiLogExecution('ERROR', 'zonessearchresult length', zonessearchresult.length);  

		var index=1;
		var sumtotalqty = 0;
		var sumavailqty = 0;
		var sumallocqty = 0;
		var sumresvqty = 0;
		var sumqtyonHand =0;
		var minval=0;var maxval=parseFloat(pagesizevalue);var mainmaxvalue=0;
		if(searchresults!=null)
		{
			if(searchresults.length>50)
			{
				if(selectno!=null)
				{
					var tarray=selectno.split(',');	
					if (request.getParameter('custpage_selectpage')!="" && request.getParameter('custpage_selectpage')!=null && parseFloat(tarray[1])<= searchresults.length )
					{
						var temp= request.getParameter('custpage_selectpage');
						var temparray=temp.split(',');
						nlapiLogExecution('ERROR', 'temparray',temparray.length);

						var diff=parseFloat(temparray[1])-(parseFloat(temparray[0])-1);
						nlapiLogExecution('ERROR', 'diff',diff);

						var pagevalue=request.getParameter('custpage_pagesize');
						pagesize.setDisplayType('entry');
						nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
						if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
						{			

							var temparray=selectno.split(',');	
							nlapiLogExecution('ERROR', 'temparray.length ', temparray.length);  
							minval=parseFloat(temparray[0])-1;
							nlapiLogExecution('ERROR', 'temparray[0] ', temparray[0]);  
							maxval=parseFloat(temparray[1]);
							nlapiLogExecution('ERROR', 'temparray[1] ', temparray[1]);  


							if(temparray.length>2)
							{

								mainmaxvalue=parseFloat(temparray[2])+1;
							}
							else
							{
								mainmaxvalue=1000;
							}


							nlapiLogExecution('ERROR', 'minval ', minval);  
							nlapiLogExecution('ERROR', 'maxval ', maxval);
							nlapiLogExecution('ERROR', 'str', str);
							nlapiLogExecution('ERROR', 'mainmaxvalue', mainmaxvalue);
						}
					}
					else
					{
						if(maxsearchresult==null)
						{
							mainmaxvalue=1000;
						}
						else
						{

							var temparray=selectno.split(',');	
							nlapiLogExecution('ERROR', 'temparray.length ', temparray.length);  
							minval=parseFloat(temparray[0])-1;
							nlapiLogExecution('ERROR', 'temparray[0] ', temparray[0]);  
							maxval=parseFloat(temparray[1]);
							nlapiLogExecution('ERROR', 'temparray[1] ', temparray[1]);  


							if(temparray.length>2)
							{

								mainmaxvalue=parseFloat(temparray[2])+1;
							}
							else
							{
								mainmaxvalue=1000;
							}
						}
					}
				}
			}
			else
			{
				minval=0; maxval=parseFloat(searchresults.length);mainmaxvalue=1000;
				pagesize.setDisplayType('hidden');

			}
		}
		else
		{
			pagesize.setDisplayType('hidden');
		}

		if(searchresults != null && searchresults.length > 0){
			for (var l = minval; l < maxval; l++) 
			{
				var  zone='';var searchresult; var tempindex;
				if(mainmaxvalue<=1000)
				{
					tempindex=l+1;
					searchresult = searchresults[l];
				}
				else
				{

					var tcount=1000;

					var tempstr=mainmaxvalue.toString();
					var str=tempstr.substring(0,1);
					var arrayindex=parseFloat(str)-parseFloat(1);
					tempindex=l+1+(parseFloat(str)*1000);

					nlapiLogExecution('ERROR', 'str', str);
					nlapiLogExecution('ERROR', 'arrayindex', arrayindex);				
					var tempsearchresult = maxsearchresult[arrayindex];

					searchresult = tempsearchresult[l];
				}



				nlapiLogExecution('ERROR', 'Search Results', searchresults.length);

				if(reportview == "a"){
					line_no = l + 1; //serial no , Autogenerated one.
					sku = searchresult.getValue('custrecord_ebiz_inv_sku'); //Itemgetvalue 
					skuIntId=searchresult.getValue('custrecord_inv_ebizsku_no'); //Itemgetvalue
					nlapiLogExecution('ERROR', 'SKU', searchresult.getValue('custrecord_ebiz_sku_no'));

					skudesc = searchresult.getValue('custrecord_ebiz_itemdesc'); // Item Desc
					skustatus = searchresult.getValue('custrecord_ebiz_inv_sku_status'); //Item Status
					batchno = searchresult.getValue('custrecord_ebiz_inv_lot'); //Batch no
					fifodate = searchresult.getValue('custrecord_ebiz_inv_fifo'); //FIFO Date
					expirydate = searchresult.getValue('custrecord_ebiz_expdate');//Expiry Date
					modelno = searchresult.getValue('custrecord_ebiz_model'); //Model no
					actendloc = searchresult.getText('custrecord_ebiz_inv_binloc'); //Bin location
					packcode = searchresult.getText('custrecord_ebiz_inv_packcode');//Packcode
					totalqty = searchresult.getValue('custrecord_ebiz_inv_qty'); //Total Qty
					availqty = searchresult.getValue('custrecord_ebiz_avl_qty'); //Avail Qty
					qtyonHand = searchresult.getValue('custrecord_ebiz_qoh'); //Qty on Hand
					allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty'); //Total Qty
					resvqty = searchresult.getValue('custrecord_ebiz_resinventory'); //Resv Qty
					invtLp = searchresult.getValue('custrecord_ebiz_inv_lp');
					lotbat = searchresult.getText('custrecord_ebiz_inv_lot');
					iteminfo1 = searchresult.getText('custitem_item_info_1','custrecord_ebiz_inv_sku');
					iteminfo2 = searchresult.getText('custitem_item_info_2','custrecord_ebiz_inv_sku');
					binlocgroup = searchresult.getText('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc');
					binlocgroupvalue = searchresult.getValue('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc');
					statusflag = searchresult.getValue('custrecord_wms_inv_status_flag');
					whlocation = searchresult.getText('custrecord_ebiz_inv_loc');

				}


				else if(reportview == "b"){

					line_no = l + 1; //serial no , Autogenerated one.
					sku = searchresult.getValue('custrecord_ebiz_inv_sku', null, 'group'); //Itemgetvalue 
					skuIntId=searchresult.getValue('custrecord_inv_ebizsku_no', null, 'group'); //Itemgetvalue
						
					//nlapiLogExecution('ERROR', 'SKU', searchresult.getValue('custrecord_ebiz_sku_no'));

					skudesc = searchresult.getValue('custrecord_ebiz_itemdesc', null, 'group'); // Item Desc
					skustatus = searchresult.getValue('custrecord_ebiz_inv_sku_status', null, 'group'); //Item Status
					batchno = searchresult.getValue('custrecord_ebiz_inv_lot', null, 'group'); //Batch no
					fifodate = searchresult.getValue('custrecord_ebiz_inv_fifo', null, 'group'); //FIFO Date
					expirydate = searchresult.getValue('custrecord_ebiz_expdate', null, 'group');//Expiry Date
					modelno = '';//searchresult.getValue('custrecord_ebiz_model', null, 'group'); //Model no
					actendloc ='';// searchresult.getText('custrecord_ebiz_inv_binloc', null, 'group'); //Bin location
					packcode = searchresult.getText('custrecord_ebiz_inv_packcode', null, 'group');//Packcode
					totalqty = searchresult.getValue('custrecord_ebiz_inv_qty', null, 'group'); //Total Qty
					qtyonHand = searchresult.getValue('custrecord_ebiz_qoh', null, 'sum'); //Qty on Hand
					availqty = searchresult.getValue('custrecord_ebiz_avl_qty', null, 'sum'); //Avail Qty
					allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty', null, 'sum'); //Total Qty
					resvqty = searchresult.getValue('custrecord_ebiz_resinventory', null, 'sum'); //Resv Qty
					invtLp = '';//searchresult.getValue('custrecord_ebiz_inv_lp', null, 'group');
					lotbat = searchresult.getText('custrecord_ebiz_inv_lot', null, 'group');
					iteminfo1 = '';//searchresult.getValue('custitem_item_info_1','custrecord_ebiz_inv_sku', null, 'group');
					iteminfo2 = '';//searchresult.getValue('custitem_item_info_2','custrecord_ebiz_inv_sku', null, 'group');
					binlocgroup = '';//searchresult.getText('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc', null, 'group');
					binlocgroupvalue = '';//searchresult.getValue('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc', null, 'group');
					statusflag = searchresult.getValue('custrecord_wms_inv_status_flag', null, 'group');
					whlocation = searchresult.getText('custrecord_ebiz_inv_loc', null, 'group');


				}

//				new code


				var zonesarray=new Array(); 
				var elementfound =false;
				for (var m = 0; zonessearchresult!= null && m < zonessearchresult.length; m++) 
				{
					if (zonessearchresult[m].getText('custrecord_locgroup_no')== binlocgroup)
					{                
						elementfound=true;
						if (zone=='')
						{

							zone = zone+zonessearchresult[m].getValue('custrecord_putzoneid','custrecordcustrecord_putzoneid')+',';
							zonesarray[m]=zone.slice(0, zone.lastIndexOf(','));

						}
						else
						{
							var tempZone=zonessearchresult[m].getValue('custrecord_putzoneid','custrecordcustrecord_putzoneid');

							if(findIndex(tempZone,zonesarray)==-1)
							{
								zone = zone+zonessearchresult[m].getValue('custrecord_putzoneid','custrecordcustrecord_putzoneid')+',';
								zonesarray[m]=tempZone;
							}

						}
					}                       
				}             	

				zone = zone.slice(0, zone.lastIndexOf(','));            
				//nlapiLogExecution('ERROR', 'zone', zone);  
				var retserialcsv='';
				if(reportview == "a"){
					retserialcsv = getSerialNoCSV(arrayLP, invtLp);
				}

				var serialnourl = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial_no', 'customdeploy_lprelated_serial_no') + "&custparam_lp=" + invtLp+ "&custparam_itemid=" + skuIntId;
				inventorysts = 'STORAGE';           

				//code added by santosh on 12Sep12
				//var AllocQtyurl=addURL(ctx.getEnvironment(),sku,invtLp,skustatus,lotbat,packcode);
				//case # 20127637 
				var AllocQtyurl = nlapiResolveURL('SUITELET', 'customscript_ebiz_viewallocqtylocation', 'customdeploy_ebiz_viewallocqtylocation_d') + "&custparam_Item=" + sku + "&custparam_lp=" + invtLp + "&custparam_itemstatus=" + skustatus + "&custparam_lotno=" + lotbat + "&custparam_packcode=" + packcode;
				//end of the code
				
				
				// Case# 20127849 starts
				nlapiLogExecution('DEBUG', 'sku is', sku);
				var itemTypesku='';
				if(sku!=null && sku!='')
				itemTypesku = nlapiLookupField('item', sku, 'salesdescription');
				// Case# 20127849 end


				/*	if(request.getParameter('custpage_itemzone') != "")
				{			

					if(elementfound==true)        		
					{

						form.getSubList('custpage_items').setLineItemValue('custpage_no',  index, tempindex.toString());     	
						form.getSubList('custpage_items').setLineItemValue('custpage_sku',  index, sku); 
						form.getSubList('custpage_items').setLineItemValue('custpage_skuintid', index, skuIntId);
						// Case# 20127849 starts
						//form.getSubList('custpage_items').setLineItemValue('custpage_skudesc', index, skudesc); 
						if(itemTypesku!=null && itemTypesku!='')
						form.getSubList('custpage_items').setLineItemValue('custpage_skudesc', index, itemTypesku);
						else
							form.getSubList('custpage_items').setLineItemValue('custpage_skudesc', index, skudesc);
						// Case# 20127849 ends
						form.getSubList('custpage_items').setLineItemValue('custpage_whloc', index, whlocation);
						form.getSubList('custpage_items').setLineItemValue('custpage_skustatus', index, skustatus);                
						// form.getSubList('custpage_items').setLineItemValue('custpage_batchno', i + 1, batchno);
						form.getSubList('custpage_items').setLineItemValue('custpage_fifodate', index, fifodate);               
						form.getSubList('custpage_items').setLineItemValue('custpage_expirydate', index, expirydate);               
						form.getSubList('custpage_items').setLineItemValue('custpage_modelno', index, modelno);                
						form.getSubList('custpage_items').setLineItemValue('custpage_actendloc', index, actendloc);               
						form.getSubList('custpage_items').setLineItemValue('custpage_packcode', index, packcode);            
						//form.getSubList('custpage_items').setLineItemValue('custpage_totalqty', index, totalqty);           
						//code added by santosh on 12Sep12
						if(allocqty>0)
						{
//							form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', index, "<a href=\"#\" onclick=\"javascript:window.open('" + AllocQtyurl + "');\" >" + allocqty + "</a>");
							form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', index, "<a href='"+AllocQtyurl+"' target='_blank'>"+allocqty+"</a>");
						}
						else
						{
							form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', index, allocqty);         
						}
						//end of the code
						form.getSubList('custpage_items').setLineItemValue('custpage_qtyonhand', index, qtyonHand);         
						//form.getSubList('custpage_items').setLineItemValue('custpage_resvqty', index, resvqty);        
						form.getSubList('custpage_items').setLineItemValue('custpage_availqty', index, availqty);         
						form.getSubList('custpage_items').setLineItemValue('custpage_invstatus', index, inventorysts);          
						form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo1', index, iteminfo1);           
						form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo2', index, iteminfo2);          
						form.getSubList('custpage_items').setLineItemValue('custpage_binlocgrp',index, binlocgroup);        
						form.getSubList('custpage_items').setLineItemValue('custpage_zone', index, zone);
						//form.getSubList('custpage_items').setLineItemValue('custpage_invtlp', index, "<a href=\"#\" onclick=\"javascript:window.open('" + serialnourl + "');\" >" + invtLp + "</a>");
						form.getSubList('custpage_items').setLineItemValue('custpage_invtlp', index, "<a href='"+serialnourl+"' target='_blank'>"+invtLp+"</a>");// Added By Ganapathi On 27th Dec 2012
						form.getSubList('custpage_items').setLineItemValue('custpage_lotbat', index, lotbat);             
						//	form.getSubList('custpage_items').setLineItemValue('custpage_serialno', index, retserialcsv); 
						form.getSubList('custpage_items').setLineItemValue('custpage_statusflag', index, statusflag);
						if(totalqty == "")
							totalqty=0;
						sumtotalqty = parseFloat(sumtotalqty) + parseFloat(totalqty);
						if(allocqty=="")
							allocqty=0;
						sumallocqty = parseFloat(sumallocqty) + parseFloat(allocqty);
						if(availqty=="")
							availqty=0;
						sumavailqty = parseFloat(sumavailqty) + parseFloat(availqty);
						if(resvqty=="")
						resvqty=0;
					sumresvqty = parseFloat(sumresvqty) + parseFloat(resvqty);
						if(qtyonHand=="")
							qtyonHand=0;
						sumqtyonHand = parseFloat(sumqtyonHand) + parseFloat(qtyonHand);
						index=parseFloat(index+1);
					}
					//}
				}
				else
				{*/
					form.getSubList('custpage_items').setLineItemValue('custpage_no',  index, tempindex.toString()); 
					form.getSubList('custpage_items').setLineItemValue('custpage_sku', index, sku);
					form.getSubList('custpage_items').setLineItemValue('custpage_skuintid', index, skuIntId);
					// Case# 20127849 starts
					//form.getSubList('custpage_items').setLineItemValue('custpage_skudesc', index, skudesc); 
					if(itemTypesku!=null && itemTypesku!='')
						form.getSubList('custpage_items').setLineItemValue('custpage_skudesc', index, itemTypesku);
						else
							form.getSubList('custpage_items').setLineItemValue('custpage_skudesc', index, skudesc);
					// Case# 20127849 ends
					form.getSubList('custpage_items').setLineItemValue('custpage_whloc', index, whlocation);
					form.getSubList('custpage_items').setLineItemValue('custpage_skustatus', index, skustatus); 				
					form.getSubList('custpage_items').setLineItemValue('custpage_fifodate', index, fifodate);         
					form.getSubList('custpage_items').setLineItemValue('custpage_expirydate', index, expirydate);          
					form.getSubList('custpage_items').setLineItemValue('custpage_modelno', index, modelno);         
					form.getSubList('custpage_items').setLineItemValue('custpage_actendloc', index, actendloc);          
					form.getSubList('custpage_items').setLineItemValue('custpage_packcode', index, packcode);  		        
					//code added by santosh on 12Sep12
					if(allocqty>0)
					{
						//	form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', index, "<a href=\onclick=\"javascript:window.open('" + AllocQtyurl + "');\" >" + allocqty + "</a>");
						form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', index, "<a href='"+AllocQtyurl+"' target='_blank'>"+allocqty+"</a>");
					}
					else
					{
						form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', index, allocqty);         
					}
					//end of the code
					/*form.getSubList('custpage_items').setLineItemValue('custpage_availqty', index, availqty);          
					form.getSubList('custpage_items').setLineItemValue('custpage_resvqty', index, resvqty);     */

					form.getSubList('custpage_items').setLineItemValue('custpage_qtyonhand', index, qtyonHand);         
					form.getSubList('custpage_items').setLineItemValue('custpage_availqty', index, availqty);      
					form.getSubList('custpage_items').setLineItemValue('custpage_invstatus', index, inventorysts);          
					form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo1', index, iteminfo1);          
					form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo2', index, iteminfo2);         
					form.getSubList('custpage_items').setLineItemValue('custpage_binlocgrp',index, binlocgroup);         
					form.getSubList('custpage_items').setLineItemValue('custpage_zone', index, zone);        
					//form.getSubList('custpage_items').setLineItemValue('custpage_invtlp', index, "<a href=\"#\" onclick=\"javascript:window.open('" + serialnourl + "');\" >" + invtLp + "</a>");

					form.getSubList('custpage_items').setLineItemValue('custpage_invtlp', index, "<a href='"+serialnourl+"' target='_blank'>"+invtLp+"</a>");// Added By Ganapathi On 27th Dec 2012

					form.getSubList('custpage_items').setLineItemValue('custpage_lotbat', index, lotbat);         
					//form.getSubList('custpage_items').setLineItemValue('custpage_serialno', index, retserialcsv);    
					form.getSubList('custpage_items').setLineItemValue('custpage_statusflag', index, statusflag);
					if(totalqty == "")
						totalqty=0;
					sumtotalqty = parseFloat(sumtotalqty) + parseFloat(totalqty);
					if(allocqty=="")
						allocqty=0;
					sumallocqty = parseFloat(sumallocqty) + parseFloat(allocqty);
					if(availqty=="")
						availqty=0;
					sumavailqty = parseFloat(sumavailqty) + parseFloat(availqty);
					/*if(resvqty=="")
					resvqty=0;
				sumresvqty = parseFloat(sumresvqty) + parseFloat(resvqty);*/
					if(qtyonHand=="")
						qtyonHand=0;
					sumqtyonHand = parseFloat(sumqtyonHand) + parseFloat(qtyonHand);
					index=parseFloat(index+1);
				//}
			}
		}// End of for loop 
		sumallocqty = parseFloat(sumallocqty).toFixed(4);
		sumavailqty = parseFloat(sumavailqty).toFixed(4);		
		sumqtyonHand = parseFloat(sumqtyonHand).toFixed(4);	
		
		form.getSubList('custpage_items').setLineItemValue('custpage_actendloc', index, 'Total');    	
		form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', index, sumallocqty.toString());         
		form.getSubList('custpage_items').setLineItemValue('custpage_availqty', index, sumavailqty.toString());
		//form.getSubList('custpage_items').setLineItemValue('custpage_resvqty', index, sumresvqty.toString());   
		form.getSubList('custpage_items').setLineItemValue('custpage_qtyonhand', index, sumqtyonHand.toString());

		var myUrl = 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=35&deploy=1';

		var sItem = "window.location=" + myUrl;


		var printid="1";
		var excelid="2";
		form.setScript('customscript_inventoryreport');		
		//form.addButton('custpage_print','Print','Printreport('+printid+')');
		form.addButton('custpage_excel','ExportToExcel','Printreport('+excelid+')');

		response.writePage(form);
	}
}

function Printreport(ebizwaveno){
	var fullfillment=null;
	var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_inventoryreport_excel', 'customdeploy_inventoryreport_excel_di');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	//var linkURL = "https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=141&deploy=1";	
	
	
	var vitemfam=nlapiGetFieldValue('custpage_itemfamily');
	var vitemgroup=nlapiGetFieldValue('custpage_itemgroup');
	var vitem=nlapiGetFieldValue('custpage_item');
	var vitemstatus=nlapiGetFieldValue('custpage_itemstatus');
	var vlp=nlapiGetFieldValue('custpage_lp');
	var vlot=nlapiGetFieldValue('custpage_lotbatch');
	var vbinloc=nlapiGetFieldValue('custpage_binlocation');
	var vinvstatus=nlapiGetFieldValue('custpage_inventorysts');
	var vitemzone=nlapiGetFieldValue('custpage_itemzone');
	var vreportviewby=nlapiGetFieldValue('custpage_reportviewby');
	var vlocid=nlapiGetFieldValue('custpage_loc_id');
	
	WavePDFURL = WavePDFURL + '&custparam_ebiz_wave_no='+ ebizwaveno+'&custitem_item_family='+ vitemfam+'&custitem_item_group='+ vitemgroup+'&custpage_item='+ vitem+'&custpage_itemstatus='+ vitemstatus+'&custpage_lp='+ vlp+'&custpage_lotbatch='+ vlot+'&custpage_binlocation='+ vbinloc+'&custpage_inventorysts='+ vinvstatus+'&custpage_itemzone='+ vitemzone+'&custpage_reportviewby='+ vreportviewby+'&custpage_loc_id='+ vlocid;
	window.open(WavePDFURL);
}

function getSerialNoCSV(arrayLP, lp){
	var csv = "";
	for (var i = 0; i < arrayLP.length; i++) {
		if (arrayLP[i][0] == lp) {
			csv += arrayLP[i][1] + " , ";
		}
	}
	return csv;
}
function findIndex(value,testarray){
	var ctr = -1;
	for (var i=0; i < testarray.length; i++) {
		// use === to check for Matches. ie., identical (===), ;
		if (testarray[i] == value) {
			return i;
		}
	}
	return ctr;
};

var searchResultArray=new Array();
function getSearchResults(maxno,columns,reportview)
{


	var filters = new Array();
	var i = 0;    


	var invStatus  = request.getParameter('custpage_inventorysts');

	nlapiLogExecution('ERROR','Value of Inventory Flag.',request.getParameter('custpage_inventorysts'));
	if (request.getParameter('custpage_inventorysts') != "") {

		if(invStatus == '4')
		{
			filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']);
		}
		else if(invStatus == '3')
		{
			filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['18']);
		}  
		else if(invStatus == '2')      			
		{
			filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['1','17']);
		}
		else if(invStatus == '1')      			
		{
			//case# 20149507 starts(added 36=FLAG.INVENTORY.WIP to filter)
			//filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['1','3','17','18','19']);
			filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['1','3','17','18','19','36']);
			//case# 20149507 ends
		}
		i++;

	}

	if (request.getParameter('custpage_itemfamily') != "") {
		//nlapiLogExecution('ERROR','itemFamily',request.getParameter('custpage_itemfamily'));
		filters[i] = new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_inv_sku', 'is', request.getParameter('custpage_itemfamily'));
		i++;
	}

	if (request.getParameter('custpage_itemgroup') != "") {
		filters[i] = new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_inv_sku', 'is', request.getParameter('custpage_itemgroup'));
		i++;
	}

	if (request.getParameter('custpage_item') != "") {
		filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', request.getParameter('custpage_item'));
		i++;
	}

	if (request.getParameter('custpage_itemstatus') != "") {
		//nlapiLogExecution('ERROR','SKUSTATUS',request.getParameter('custpage_itemstatus'));
		filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', request.getParameter('custpage_itemstatus'));
		i++;
	}
	if (request.getParameter('custpage_lp') != "") {
		filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', request.getParameter('custpage_lp'));
		i++;
	}
	if (request.getParameter('custpage_lotbatch') != "") {
		filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', request.getParameter('custpage_lotbatch'));
		i++;
	}
	nlapiLogExecution('ERROR','Location Group11',request.getParameter('custpage_binlocgroup'));
	if (request.getParameter('custpage_binlocgroup') != "") {
		filters[i] = new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', request.getParameter('custpage_binlocgroup'));
		//filters[i] = new nlobjSearchFilter('custrecord_inboundinvlocgroupid', 'anyof', request.getParameter('custpage_binlocgroup'));
		i++;
	}
	if (request.getParameter('custpage_binlocation') != "") {
		filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', request.getParameter('custpage_binlocation'));
		i++;
	}
	/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle ***//*  
	if (request.getParameter('custpage_loc_id') != "") {
		filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', request.getParameter('custpage_loc_id'));
		i++;
	}
	*//***  up to here ***/
	
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('DEBUG','vRoleLocation',vRoleLocation);
	nlapiLogExecution('DEBUG','request.getParameter(custpage_loc_id)',request.getParameter('custpage_loc_id'));
	
	/*if(vRoleLocation!=""&&vRoleLocation!=null)
	{
		if(request.getParameter('custpage_loc_id') ==null || request.getParameter('custpage_loc_id')=='')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',vRoleLocation));
		else
		{
			nlapiLogExecution('DEBUG','index',vRoleLocation.indexOf(request.getParameter('custpage_loc_id')));
			if(vRoleLocation.indexOf(request.getParameter('custpage_loc_id'))!=-1)
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
			else
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',["@NONE@"]));
		}
	}
	else
	{
		if(request.getParameter('custpage_loc_id') !=null && request.getParameter('custpage_loc_id') !='')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
	}*/
	
	
	
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		if(request.getParameter('custpage_loc_id') ==null || request.getParameter('custpage_loc_id')=='')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',vRoleLocation));
		else
		{
			nlapiLogExecution('ERROR','index',vRoleLocation.indexOf(request.getParameter('custpage_loc_id')));
			//if(vRoleLocation.indexOf(parseInt(request.getParameter('custpage_loc_id')))!=-1)
			if(vRoleLocation.indexOf(request.getParameter('custpage_loc_id'))!=-1)//case# 20149727
			{
				nlapiLogExecution('ERROR','loc id',request.getParameter('custpage_loc_id'));
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
			}
			else
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',["@NONE@"]));
		}
	}
	else
	{
		if(request.getParameter('custpage_loc_id') !=null && request.getParameter('custpage_loc_id') !='')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
	}
	//upto here
	filters[i] = new nlobjSearchFilter('id', null, 'greaterthan', maxno);
	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(SearchResults !=null && SearchResults !='' && SearchResults.length>0)
	{
	if(SearchResults.length>=1000)
	{  

		searchResultArray.push(SearchResults); 
		var maxno;
		if( reportview== "a"){
			maxno=SearchResults[SearchResults.length-1].getValue(columns[18]);
		}
		else if(reportview == "b"){
			maxno=SearchResults[SearchResults.length-1].getValue(columns[12]);	
		}
		//var maxno=SearchResults[SearchResults.length-1].getValue(shipLPColumns[1]);
		getSearchResults(maxno,columns,reportview); 

	}
	else
	{
		searchResultArray.push(SearchResults);     
		}
	}
	return searchResultArray ;
}
//code added by santosh on 12Sep12
function addURL(environment,sku,invtLp,skustatus,lotbat,packcode) {
	var SoURL;
	SoURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_viewallocqtylocation', 'customdeploy_ebiz_viewallocqtylocation_d');
	nlapiLogExecution('ERROR', 'sku', sku);
	nlapiLogExecution('ERROR', 'invtLp', invtLp);
	// Get the FQDN depending on the context environment
	var fqdn = getFQDNForHost(environment);
//	var ctx = nlapiGetContext();
//	if (fqdn == '')
//		nlapiLogExecution('ERROR', 'Unable to retrieve FQDN based on context');
//	else
//		SoURL = getFQDNForHost(ctx.getEnvironment()) + SoURL;
	SoURL = SoURL + '&custparam_Item=' + sku;
	SoURL = SoURL + '&custparam_lp=' + invtLp;
	SoURL = SoURL + '&custparam_itemstatus=' +skustatus;
	SoURL = SoURL + '&custparam_lotno=' +lotbat;
	SoURL = SoURL + '&custparam_packcode=' + packcode;
	nlapiLogExecution('ERROR', 'UrL', SoURL);
	return SoURL;
}
//end of the code
