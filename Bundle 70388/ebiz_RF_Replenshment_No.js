/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_Replenshment_No.js,v $
<<<<<<< ebiz_RF_Replenshment_No.js
 *     	   $Revision: 1.7.2.7.4.2.4.17.2.3 $
 *     	   $Date: 2015/11/25 15:39:10 $
 *     	   $Author: aanchal $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_209 $
=======
 *     	   $Revision: 1.7.2.7.4.2.4.17.2.3 $
 *     	   $Date: 2015/11/25 15:39:10 $
 *     	   $Author: aanchal $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_209 $
>>>>>>> 1.7.2.7.4.2.4.11
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Replenshment_No.js,v $
 * Revision 1.7.2.7.4.2.4.17.2.3  2015/11/25 15:39:10  aanchal
 * 2015.2 Issue fix
 * 201415752,201415718
 *
 * Revision 1.7.2.7.4.2.4.17.2.2  2015/11/25 15:38:23  aanchal
 * 2015.2 Issue fix
 * 201415752,201415718
 *
 * Revision 1.7.2.7.4.2.4.13.2.4  2015/07/22 15:41:20  grao
 * 2015.2   issue fixes  201413585
 *
 * Revision 1.7.2.7.4.2.4.13.2.3  2014/08/28 06:27:18  skreddy
 * case # 201410021
 * DCD SB issue fix
 *
 * Revision 1.7.2.7.4.2.4.13.2.2  2014/07/11 15:59:29  sponnaganti
 * Case# 20149400
 * Compatibility Issue fix
 *
 * Revision 1.7.2.7.4.2.4.13.2.1  2014/07/01 06:33:52  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic issue fixes
 *
 * Revision 1.7.2.7.4.2.4.13  2014/06/23 07:21:48  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.7.2.7.4.2.4.12  2014/06/18 13:52:45  grao
 * Case#: 20148956  New GUI account issue fixes
 *
 * Revision 1.7.2.7.4.2.4.11  2014/06/13 09:06:06  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.7.2.7.4.2.4.10  2014/05/30 00:34:23  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.7.2.7.4.2.4.9  2013/11/20 20:45:48  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20125767
 * Replen Exceptions
 *
 * Revision 1.7.2.7.4.2.4.8  2013/09/12 15:35:07  rmukkera
 * Case# 20124196
 *
 * Revision 1.7.2.7.4.2.4.7  2013/09/04 15:38:45  skreddy
 * Case# 20124179
 * standard bundle issue fix
 *
 * Revision 1.7.2.7.4.2.4.6  2013/08/27 16:34:09  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdchanges
 * case#20123755
 *
 * Revision 1.7.2.7.4.2.4.5  2013/05/14 14:16:37  schepuri
 * RF Replen confirm based on filters
 *
 * Revision 1.7.2.7.4.2.4.4  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.7.2.7.4.2.4.3  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.7.2.7.4.2.4.2  2013/03/13 13:57:18  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.7.2.7.4.2.4.1  2013/03/05 14:46:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.7.2.7.4.2  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.7.2.7.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.7.2.7  2012/08/28 15:53:04  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to showing improper message.
 *
 * Revision 1.7.2.6  2012/08/24 16:09:57  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Replen confirm
 *
 * Revision 1.7.2.5  2012/07/30 17:08:12  gkalla
 * CASE201112/CR201113/LOG201121
 * replenishment merging
 *
 * Revision 1.7.2.4  2012/07/12 14:47:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.7.2.3  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.7.2.2  2012/02/21 13:26:42  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.7.2.1  2012/02/07 12:40:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Replen Issue Fixes
 *
 * Revision 1.8  2012/01/17 13:53:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.7  2012/01/05 17:24:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.5  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *****************************************************************************/

function Replenishment_No(request, response){
	if (request.getMethod() == 'GET') {

		var html=getScanReportNo(request);        
		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		// This variable is to hold the REPLEN NO# entered.
		var Reparray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		Reparray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);


		var st8,st9,st10;
		if( getLanguage == 'es_ES')
		{

			st8 = "NO V&#193;LIDA REPOSICI&#211;N";
			st9 = "NO REPLENS DE ESTA ONDA NO";
			st10 = "NO REPLENS para esta selecci?n";
		}
		else
		{

			st8 = "PLEASE ENTER VALID REPLEN#";
			st9 = "NO REPLENS FOR THIS WAVE NO";
			st10= "NO OPEN REPLENS FOR THIS SELECTION";


		}

		Reparray["custparam_repno"] = request.getParameter('enterReplenNo');
		Reparray["custparam_waveno"] = request.getParameter('enterwaveNo');
		Reparray["custparam_error"] = st8;
		Reparray["custparam_waveno"]='';
		Reparray["custparam_entersku"] = request.getParameter('enterItem');
		Reparray["custparam_enterpickloc"] = request.getParameter('enterPrimaryloc');
		Reparray["custparam_entertaskpriority"] = request.getParameter('enterPriority');
		Reparray["custparam_screenno"] = 'R1';

		nlapiLogExecution('Error', 'Reparray["custparam_repno"]', Reparray["custparam_repno"]);
		nlapiLogExecution('Error', 'Reparray["custparam_waveno"]', Reparray["custparam_waveno"]);
		nlapiLogExecution('Error', 'Reparray["custparam_sku"]', Reparray["custparam_entersku"]);
		nlapiLogExecution('Error', 'Reparray["custparam_reppickloc"]', Reparray["custparam_enterpickloc"]);
		nlapiLogExecution('Error', 'Reparray["custparam_taskpriority"]',Reparray["custparam_entertaskpriority"]);

		var reportNo=Reparray["custparam_repno"];
		var waveno=Reparray["custparam_waveno"];

		var getItem = Reparray["custparam_entersku"];
		var getPrimaryloc = Reparray["custparam_enterpickloc"];
		var getPriority = Reparray["custparam_entertaskpriority"];
		var getItemId='';

		var getNumber = request.getParameter('hdngetnumber');
		nlapiLogExecution('Error', 'getNumber', getNumber);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optedNext = request.getParameter('cmdNEXT');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, Reparray);
		}
		else if(optedNext=='F6')
		{
			if((reportNo==null||reportNo=="")&&(getItem==null||getItem=="")&&(getPrimaryloc==null||getPrimaryloc=="")&& (getPriority==null||getPriority==""))
				{
				Reparray["custparam_error"] = 'PLEASE ENTER VALUE';
				nlapiLogExecution('ERROR',  'No value entered');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				return;
				}
			getNumber=parseFloat(getNumber) + 1;
			Reparray["custparam_number"] = getNumber;
			response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false, Reparray);
			return;
		}
		else {
			var waveno;
			var reportNo= request.getParameter('enterReplenNo');
			//	var waveno= request.getParameter('enterwaveNo');		


			if (getItem != null && getItem != '') 
			{
				var ItemFilters=new Array();
				ItemFilters.push(new nlobjSearchFilter('nameinternal', null, 'is', getItem));
				ItemFilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
				var ItemSearchResults = nlapiSearchRecord('item', null, ItemFilters, null);   	

				if (ItemSearchResults != null && ItemSearchResults.length > 0) 
				{
					nlapiLogExecution('ERROR', 'ItemSearchResults', ItemSearchResults.length);
					var getItemId = ItemSearchResults[0].getId();
					nlapiLogExecution('ERROR', 'Item Id', getItemId);
					Reparray["custparam_enterskuid"] = getItemId;
					Reparray["custparam_entersku"] = getItem;
				}
				else 
				{
					nlapiLogExecution('ERROR', 'into upc code', getItem);
					var vItemfilters = new Array();
					vItemfilters.push(new nlobjSearchFilter('upccode', null, 'is', getItem));
					vItemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
					var vItemCol = new Array();
					vItemCol[0] = new nlobjSearchColumn('itemid');

					var invtRes = nlapiSearchRecord('item', null, vItemfilters, vItemCol);
					if(invtRes!=null && invtRes !='' )
					{
						var getItemId =invtRes[0].getId();;
						var getItem = invtRes[0].getValue('itemid');

						nlapiLogExecution('ERROR', 'getItemId', getItemId);
						nlapiLogExecution('ERROR', 'getItemtext', getItem);
						Reparray["custparam_enterskuid"] = getItemId;
						Reparray["custparam_entersku"] = getItem;

					}
					else
					{

						Reparray["custparam_error"] = 'INVALID ITEM';
						nlapiLogExecution('ERROR', 'SearchResults of given Item', 'Item is found');

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						nlapiLogExecution('ERROR', 'Item is not entered', getItem);
						return;
					}
				}
			}

			if (getPrimaryloc != null && getPrimaryloc != '') 
			{
				var BinLocationFilters = new Array();
				BinLocationFilters[0] = new nlobjSearchFilter('name', null, 'is', getPrimaryloc);

				var LocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, BinLocationFilters, null);    				

				if (LocationSearchResults != null && LocationSearchResults.length > 0) 
				{
					nlapiLogExecution('ERROR', 'LocationSearchResults', LocationSearchResults.length);
					var getBinLocationId = LocationSearchResults[0].getId();
					nlapiLogExecution('ERROR', 'Location Id', getBinLocationId );
					Reparray["custparam_enterpicklocid"] = getBinLocationId;
					Reparray["custparam_enterpickloc"] = getPrimaryloc;
				}
				else 
				{
					Reparray["custparam_error"] = 'INVALID LOCATION';
					nlapiLogExecution('ERROR', 'SearchResults of given Location', 'Location is not found');

					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					//nlapiLogExecution('ERROR', 'Bin Location is not entered', getBinLocation);
					return;
				}	
			}


			if (reportNo != "" || waveno!="" || getItem !="" || getPrimaryloc!="" || getPriority!="" ){
				Reparray["custparam_enterrepno"] = reportNo;
				Reparray["custparam_waveno"] = waveno;
				Reparray["custparam_entersku"] = getItem;
				Reparray["custparam_enterskuid"] = getItemId;
				Reparray["custparam_enterpicklocid"] = getBinLocationId;
				Reparray["custparam_enterpickloc"] = getPrimaryloc;
				Reparray["custparam_entertaskpriority"] = getPriority;

				var vOrdFormat='';
				if(reportNo!=null && reportNo!='')
				{
					vOrdFormat='R';
				}


				if(getItemId!=null && getItemId!='')
				{
					vOrdFormat=vOrdFormat + 'I';
				}

				if(getBinLocationId!=null && getBinLocationId!='')
				{
					vOrdFormat=vOrdFormat + 'L';
				}

				if(getPriority!=null && getPriority!='')
				{
					vOrdFormat=vOrdFormat + 'P';
				}

				if(reportNo != null && reportNo != "" && getItemId!=null && getItemId!="" && getBinLocationId!=null && getBinLocationId!="" && getPriority!=null && getPriority!="")
				{
					vOrdFormat='ALL';
				}

				//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
				var searchresults =getReplenTasks(reportNo,waveno,getItemId,getBinLocationId,getPriority);
				var systemRule=GetSystemRuleForReplen();
				if (searchresults != null && searchresults.length > 0) {
					if(systemRule=='Y')
					{
						if (Reparray["custparam_repno"] != null || Reparray["custparam_waveno"]!=null) {
							if(Reparray["custparam_repno"]==null || Reparray["custparam_repno"]=='')
								Reparray["custparam_repno"]= searchresults[0].getValue('name');

							Reparray["custparam_replentype"] = vOrdFormat;
							Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
							Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc');
							Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku');
							Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
							Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
							Reparray["custparam_repid"] = searchresults[0].getId();
							Reparray["custparam_whlocation"] = searchresults[0].getValue('custrecord_wms_location');
							Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc');
							Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc');
							Reparray["custparam_fromlpno"] = searchresults[0].getValue('custrecord_from_lp_no');
							Reparray["custparam_tolpno"] = searchresults[0].getValue('custrecord_lpno');

//							Reparray["custparam_sku"] = searchresults[0].getValue('custrecord_sku');
//							Reparray["custparam_primaryloc"] = searchresults[0].getValue('custrecord_actbeginloc');
							//Reparray["custparam_taskpriority"] = searchresults[0].getValue('custrecord_taskpriority');

							Reparray["custparam_noofrecords"] = searchresults.length;	
							nlapiLogExecution('ERROR','custparam_noofrecords', Reparray["custparam_noofrecords"]);

							var currentContext = nlapiGetContext();  
							var currentUserID = currentContext.getUser();
							nlapiLogExecution('ERROR', 'currentUserID', currentUserID);
							nlapiLogExecution('ERROR','beginLocationId', Reparray["custparam_repbeglocId"]);

							var RecID=searchresults[0].getId();

							var fields=new Array();
							fields[0]='custrecord_taskassignedto';		
							fields[1]='custrecord_actualbegintime';	

							var Values=new Array();
							Values[0]=currentUserID;	
							Values[1]=TimeStamp();	

							var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',RecID,fields,Values);

							//*** Upto here ***//

							response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);

						}
						else {    
							nlapiLogExecution('ERROR','test1', 'test1');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);                		 
						}
					}
					else
					{

						Reparray["custparam_replentype"] = vOrdFormat;
						Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku',null,'group');
						Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty',null,'sum');
						Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no',null,'group');					
						Reparray["custparam_whlocation"] = searchresults[0].getValue('custrecord_wms_location',null,'group');
						Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc',null,'group');
						Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc',null,'group');

						Reparray["custparam_noofrecords"] = searchresults.length;	
						nlapiLogExecution('ERROR','custparam_noofrecords', Reparray["custparam_noofrecords"]);

						nlapiLogExecution('ERROR','beginLocationId', Reparray["custparam_repbeglocId"]);

						nlapiLogExecution('ERROR','getReplenTasks: whLocation', searchresults[0].getValue('custrecord_wms_location'));

						response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
					}
				}
				else {
					nlapiLogExecution('ERROR', 'getIntransitReplenTasks');
					var searchresults = getIntransitReplenTasks(reportNo,waveno,getItemId,getBinLocationId,getPriority);
					nlapiLogExecution('ERROR', 'After getIntransitReplenTasks');
					if(systemRule=='Y')
					{
						if (searchresults != null && searchresults.length > 0) {
							nlapiLogExecution('ERROR', 'In getIntransitReplenTasks');	
							Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
							Reparray["custparam_repsku"] = searchresults[0].getValue('custrecord_sku');
							Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
							Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
							Reparray["custparam_repid"] = searchresults[0].getId();
							Reparray["custparam_whlocation"] = searchresults[0].getValue('custrecord_wms_location');

							Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc');
							Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc');
							Reparray["custparam_fromlpno"] = searchresults[0].getValue('custrecord_from_lp_no');
							Reparray["custparam_tolpno"] = searchresults[0].getValue('custrecord_lpno');

//							Reparray["custparam_sku"] = searchresults[0].getValue('custrecord_sku');
//							Reparray["custparam_primaryloc"] = searchresults[0].getValue('custrecord_actbeginloc');
							//Reparray["custparam_taskpriority"] = searchresults[0].getValue('custrecord_taskpriority');

							Reparray["custparam_noofrecords"] = searchresults.length;

//							var PickFaceSearchResult = getPickFaceLoc(searchresults[0].getValue('custrecord_sku'));

//							if (PickFaceSearchResult != null && PickFaceSearchResult.length > 0) {
//							Reparray["custparam_reppicklocno"] = PickFaceSearchResult[0].getValue('custrecord_pickbinloc');
//							Reparray["custparam_reppickloc"] = PickFaceSearchResult[0].getText('custrecord_pickbinloc');                     
//							}

							nlapiLogExecution('ERROR','getIntransitReplenTasks:whLocation', searchresults[0].getValue('custrecord_wms_location'));

							response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
						}
						else
						{
							nlapiLogExecution('ERROR','test3', 'test3');
							if(waveno!=null && waveno!='')
							{
								Reparray["custparam_error"] = st9;
							}
							else if(vOrdFormat.indexOf('R') != -1)
							{
								Reparray["custparam_error"] = st8;
							}
							else
							{
								Reparray["custparam_error"] = st10;
							}
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);                		 
						}
					}
					else
					{
						if (searchresults != null && searchresults.length > 0) {
							Reparray["custparam_replentype"] = vOrdFormat;
							Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc',null,'group');
							Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc',null,'group');
							Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku',null,'group');
							Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty',null,'sum');
							Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no',null,'group');					
							Reparray["custparam_whlocation"] = searchresults[0].getValue('custrecord_wms_location',null,'group');
							Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc',null,'group');
							Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc',null,'group');

							Reparray["custparam_noofrecords"] = searchresults.length;	
							nlapiLogExecution('ERROR','custparam_noofrecords', Reparray["custparam_noofrecords"]);

							nlapiLogExecution('ERROR','beginLocationId', Reparray["custparam_repbeglocId"]);

							nlapiLogExecution('ERROR','getReplenTasks: whLocation', searchresults[0].getValue('custrecord_wms_location'));
							response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
						}
						else
						{
							nlapiLogExecution('ERROR','test3', 'test3');
							if(waveno!=null && waveno!='')
							{
								Reparray["custparam_error"] = st9;
							}
							else if(vOrdFormat.indexOf('R') != -1)
							{
								Reparray["custparam_error"] = st8;
							}
							else
							{
								Reparray["custparam_error"] = st10;
							}
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);                		 
						}
					}
				}
			}
			else{
				nlapiLogExecution('ERROR','test2', 'test2');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);				
			}
		}
	} //end of else loop.
}

//Get HTML code for scaning Report no
function getScanReportNo(request)
{
	var getNumber;
	if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
		getNumber = request.getParameter('custparam_number');
	else
		getNumber=0;

	nlapiLogExecution('ERROR','getNumber', getNumber);
	var reportno;
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
	//filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[1].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if (searchresults != null  && searchresults!='' ) {
		nlapiLogExecution('ERROR','getNumber', getNumber);
		if(searchresults.length==getNumber)
		{
			getNumber=0;
		}
		var searchresult = searchresults[getNumber];

		reportno = searchresult.getValue(columns[0]);
	}


	var getLanguage = request.getParameter('custparam_language');
	nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

	var st0,st1,st2,st3,st4,st5,st6,st7;
	if( getLanguage == 'es_ES')
	{

		st0 = "REPOSICI&#211;N DE NO";
		st1 = "INFORME NO";	
		st2 = "REPOSICI&#211;N";
		st3 = "ENTRAR/SCAN INFORME NO";
		st4 = "Y / O ENTRAR / SCAN ONDA NO";
		st5 = "ENVIAR";
		st6 = "PR&#211;XIMO";
		st7 = "ANTERIOR";		

	}
	else
	{
		st0 = "REPLENISHMENT NO";
		st1 = "REPORT NO ";	
		st2 = "REPLENISHMENT";
		st3 = "ENTER/SCAN REPORT NO";
		st4 = "AND/OR ENTER/SCAN WAVE NO";
		st5 = "SEND";
		st6 = "NEXT";
		st7 = "PREV";


	}		


	var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_po'); 
	var html = "<html><head><title>" + st0 + "</title>";
	html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
	html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
	//Case# 20148749 Refresh Functionality starts
	html = html + "var version = navigator.appVersion;";
	html = html + "document.onkeydown = function (e) {";
	html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
	html = html + "if ((version.indexOf('MSIE') != -1)) { ";
	html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
	html = html + "else {if (keycode == 116)return false;}";
	html = html + "};";
	//Case# 20148749 Refresh Functionality ends
	//html = html + " document.getElementById('enterReplenNo').focus();";        
	html = html + "function stopRKey(evt) { ";
	//html = html + "	  alert('evt');";
	html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
	html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
	html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
	//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

	html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
	html = html + "	  alert('System Processing, Please wait...');";
	html = html + "	  return false;}} ";
	html = html + "	} ";

	html = html + "	document.onkeypress = stopRKey; ";
	html = html + "</script>";
	html = html +functionkeyHtml;
	html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
	html = html + "	<form name='_rf_replenishment_po' method='POST'>";
	html = html + "		<table>";
	/*html = html + "			<tr>";
	html = html + "				<td align = 'left'>REPORT NO : <label>" + reportno + "</label>";
	html = html + "				</td>";
	html = html + "			</tr>";*/
	html = html + "			<tr>";	
	html = html + "				<td align = 'left'>" + st2;
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + st3;
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input name='enterReplenNo' id='enterReplenNo' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	/*html = html + "			<tr>";
	html = html + "				<td align = 'left'>AND/OR ENTER/SCAN WAVE NO";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input id='txtwaveNo' name='enterwaveNo' type='text'/>";
	html = html + "				</td>";
	html = html + "				<input type='hidden' name='hdngetnumber' value=" + getNumber + ">";
	html = html + "			</tr>";*/

	html = html + "			<tr>";
	html = html + "				<td align = 'left'>ITEM:";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input id='txtItem' name='enterItem' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";

	html = html + "			<tr>";
	html = html + "				<td align = 'left'>PRIMARY LOCATION:";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input id='txtPrimaryloc' name='enterPrimaryloc' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";

	html = html + "			<tr>";
	html = html + "				<td align = 'left'>PRIORITY:";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input id='txtPriority' name='enterPriority' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
	html = html + "					" + st6 + " <input name='cmdNEXT' type='submit' value='F6'/>" + st7 + " <input name='cmdPrevious' type='submit' value='F7'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "			<td>";
	html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
	html = html + "			</td>";
	html = html + "			</tr>";
	html = html + "		 </table>";
	html = html + "	</form>";
	//Case# 20148882 (added Focus Functionality for Textbox)
	html = html + "<script type='text/javascript'>document.getElementById('enterReplenNo').focus();</script>";
	html = html + "</body>";
	html = html + "</html>";
	return html;
}

function getReplenTasks(reportNo,getWaveNo,getItemId,getBinLocationId,getPriority)
{
	/*var filters = new Array();
	if(reportNo!=null && reportNo!='')
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	if(getWaveNo!=null && getWaveNo!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));
	 */
	var filters = new Array();
	var vOrdFormat='R';
	var vOrdFormat;
	if(reportNo!=null && reportNo!='')
	{
		nlapiLogExecution('ERROR','replenreportNo',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
		vOrdFormat='R';
	}
	if(getItemId!=null && getItemId!='')
	{
		nlapiLogExecution('ERROR','Replenitem',getItemId);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getItemId));
		vOrdFormat=vOrdFormat + 'I';
	}

	if(getBinLocationId!=null && getBinLocationId!='')
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getBinLocationId);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getBinLocationId));
		vOrdFormat=vOrdFormat + 'L';
	}

	if(getPriority!=null && getPriority!='')
	{
		nlapiLogExecution('ERROR','Replenpriority',getPriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', getPriority));
		vOrdFormat=vOrdFormat + 'P';
	}

	if(reportNo != null && reportNo != "" && getItemId!=null && getItemId!="" && getBinLocationId!=null && getBinLocationId!="" && getPriority!=null && getPriority!="")
	{
		vOrdFormat='ALL';
	}
	//case# 20149400 starts
	var RoleLocation=getRoledBasedLocationNew();
	if((RoleLocation !=null && RoleLocation !='' && RoleLocation !=0))
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', RoleLocation));
	}
	//case# 20149400 ends
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
	//filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));
	try
	{
		var systemRule=GetSystemRuleForReplen();
		nlapiLogExecution('DEBUG','systemRule',systemRule);
		var columns = new Array();
		if(systemRule=='N')
		{

			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
			columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
			/*columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	*/
		}
		else
		{

			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			columns[2] = new nlobjSearchColumn('custrecord_sku');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc');
			columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
			columns[9] = new nlobjSearchColumn('name');	
			columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	
		}

		columns[1].setSort(false);
		columns[2].setSort(false);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		if(searchresults != null && searchresults != '')
			nlapiLogExecution('ERROR','searchresults tssssst',searchresults.length);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','searchresults tssssst',exp);
	}
	nlapiLogExecution('DEBUG','searchresults',searchresults);
	return searchresults;
}

function getIntransitReplenTasks(reportNo,waveno,getItemId,getBinLocationId,getPriority)
{

	nlapiLogExecution('ERROR','IntransitReplenitem',getItemId);
	nlapiLogExecution('ERROR','IntransitReplenprimaryloc',getBinLocationId);
	nlapiLogExecution('ERROR','IntransitReplenpriority',getPriority);
	var filters = new Array();
//	filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
//	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
//	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [24]);

	/*if(reportNo!=null && reportNo!='')
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
		if(waveno!=null && waveno!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(waveno)));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [24])); */

	var filters = new Array();
	var vOrdFormat='R';
	var vOrdFormat;
	if(reportNo!=null && reportNo!='')
	{
		nlapiLogExecution('ERROR','replenreportNo',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
		vOrdFormat='R';
	}
	if(getItemId!=null && getItemId!='')
	{
		nlapiLogExecution('ERROR','Replenitem',getItemId);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getItemId));
		vOrdFormat=vOrdFormat + 'I';
	}

	if(getBinLocationId!=null && getBinLocationId!='')
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getBinLocationId);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getBinLocationId));
		vOrdFormat=vOrdFormat + 'L';
	}

	if(getPriority!=null && getPriority!='')
	{
		nlapiLogExecution('ERROR','Replenpriority',getPriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', getPriority));
		vOrdFormat=vOrdFormat + 'P';
	}

	if(reportNo != null && reportNo != "" && getItemId!=null && getItemId!="" && getBinLocationId!=null && getBinLocationId!="" && getPriority!=null && getPriority!="")
	{
		vOrdFormat='ALL';
	}
	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('DEBUG','vRoleLocation',vRoleLocation);
	
	if(vRoleLocation!=null&&vRoleLocation!=""&&vRoleLocation!=0)
	filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [24])); 
	var systemRule=GetSystemRuleForReplen();
	var columns = new Array();
	if(systemRule=='N')
	{
		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
		columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
	}
	else{

		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc');
		columns[7] = new nlobjSearchColumn('custrecord_lpno');
		columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[9] = new nlobjSearchColumn('name');	
		columns[10] = new nlobjSearchColumn('custrecord_taskpriority');
	}
	columns[1].setSort(false);
	columns[2].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

function getPickFaceLoc(skuNo)
{
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skuNo);

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_pickbinloc');

	var searchresult = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filter, column);
	return searchresult;
}
function GetSystemRuleForReplen()
{
	try
	{
		var rulevalue='Y';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for Replen?'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null && searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		
		if(rulevalue == null || rulevalue == '')
		{
			rulevalue ='Y';
		}
		
		nlapiLogExecution('ERROR','rulevalue',rulevalue);


		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}
