/***************************************************************************
 eBizNET Solutions Inc                
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_BulkPicking_Confirm_SL.js,v $
 *     	   $Revision: 1.1.2.11.2.1 $
 *     	   $Date: 2014/08/01 15:25:23 $
 *     	   $Author: sponnaganti $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_BulkPicking_Confirm_SL.js,v $
 * Revision 1.1.2.11.2.1  2014/08/01 15:25:23  sponnaganti
 * Case# 20148688
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.11  2014/06/13 13:33:13  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.10  2014/06/06 07:15:21  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.9  2014/06/03 15:42:04  skavuri
 * Case# 20148688 SB Issue Fixed
 *
 * Revision 1.1.2.8  2014/05/30 15:16:57  skavuri
 * Case # 20148653 SB Issue Fixed
 *
 * Revision 1.1.2.7  2014/05/30 00:41:00  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.6  2014/03/13 09:52:52  nneelam
 * case#  20127529�
 * Standard Bundle Issue Fix.
 *
 * Revision 1.1.2.5  2014/03/11 15:03:16  sponnaganti
 * case# 20127638,20127661
 * (Standard Bundle Issue Fix)
 *
 * Revision 1.1.2.4  2014/03/10 16:25:20  skavuri
 * Case# 20127528 issue fixed
 *
 * Revision 1.1.2.3  2014/03/10 16:21:47  sponnaganti
 * case# 20127531
 * Standard Bundle Issue fix
 *
 * Revision 1.1.2.2  2013/08/23 06:13:10  snimmakayala
 * Case# 20124032
 * NLS - UAT ISSUES
 * Quantity is displaying wrongly on RF bulk picking screen for SHIPASIS items.
 *
 * Revision 1.1.2.1  2013/07/19 08:18:22  gkalla
 * Case# 20123527
 * Bulk picking CR for Nautilus
 *
 * Revision 1.8.2.27.4.18.2.11  2013/06/20 14:13:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 201215543
 *  
 *****************************************************************************/
function PickingConfirm(request, response){
	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11;

		if( getLanguage == 'es_ES')
		{
			st1 = "ELIJA LA CANTIDAD:";
			st2 = "UBICACI&#211;N:";
			st3 = "ART&#205;CULO:";
			st4 = "DESCRIPCI&#211;N DEL ART&#205;CULO:";
			st5 = "CANTIDAD RESTANTE:";
			st6 = "CANT EXCEPCI&#211;N";
			st7 = "EXCEPCI&#211;N LOC";
			st8 = "CONF";
			st9 = "ANTERIOR";
			st10 = "SKIP";
			st11 = "LP EXCEPCI&#211;N";
		}
		else
		{
			st1 = "PICK QTY : ";
			st2 = "LOCATION : ";
			st3 = "ITEM : ";
			st4 = "ITEM DESCRIPTION: ";
			st5 = "REMAINING QTY : ";
			st6 = "QTY EXCEPTION ";
			st7 = "LOC EXCEPTION ";
			st8 = "CONF ";
			st9 = "PREV";
			st10 = "SKIP";	
			st11 = "LP EXCEPTION";
		}
		var getWaveno = request.getParameter('custparam_waveno');
		var getZoneNo = request.getParameter('custparam_zoneno');
		var getZoneId = request.getParameter('custparam_zoneid');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var itemType = request.getParameter('custparam_itemType');
		var Bulkpickflag=request.getParameter('custparam_bulkpickflag');
		var getBeginLocationName = request.getParameter('custparam_beginlocationname');
		var RecordInternalId = request.getParameter('custparam_recordinternalid');// Case# 20148688
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		
		var ActBatchno=request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');
		
		var getZoneId=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('Debug', 'getItemInternalId', getItemInternalId);
		/*var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');*/
		var getFetchedLocation=getBeginLocation;
		nlapiLogExecution('Debug', 'before getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Debug', 'before getItemInternalId', getItemInternalId);
		var getEnteredLocation=request.getParameter('custparam_endlocation');
		var getEndLocInternalId=request.getParameter('custparam_endlocinternalid');
		//code added by santosh on 7Aug2012
		var Type='';
		if(request.getParameter('custparam_type') !=null &&  request.getParameter('custparam_type')!="")
		{
			Type=request.getParameter('custparam_type');
		}
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');


		var getItem = '';
		var Itemdescription='';
		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');

		if(getItemName==null || getItemName =='')
		{

			var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);

			getItem = ItemRec.getFieldValue('itemid');

			nlapiLogExecution('DEBUG', 'Location Name is', getItem);
			if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
			{	
				Itemdescription = ItemRec.getFieldValue('description');
			}
			else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
			{	
				Itemdescription = ItemRec.getFieldValue('salesdescription');
			}
		}
		else
		{
			getItem=getItemName;
		}

		nlapiLogExecution('DEBUG', 'getItemdescription', Itemdescription);

		var str = 'getWaveno. = ' + getWaveno + '<br>';
var getBeginLocationId=getBeginLocation;
 
		str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';
		str = str + 'getBeginLocationId. = ' + getBeginLocationId + '<br>';
		str = str + 'getEndLocInternalId. = ' + getEndLocInternalId + '<br>';

		nlapiLogExecution('DEBUG', 'calculating Remaining Qty...', str);

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";      
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		/*html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";*/
		//html = html + " document.getElementById('cmdConfirm').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdConfirm').disabled);";
		//html = html + "	  alert(document.getElementById('hdnflag').disabled);";
		html = html + "	  if(document.getElementById('cmdConfirm').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "	document.onkeydown = function(){";
		html = html + "		if(window.event && window.event.keyCode == 116)";
		html = html + "		        {";
		html = html + "		    window.event.keyCode = 505;";
		html = html + "		      }";
		html = html + "		if(window.event && window.event.keyCode == 505)";
		html = html + "		        { ";
		html = html + "		    return false;";
		html = html + "		    }";
		html = html + "		}";


		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> Zone# :<label>" + getZoneNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getExpectedQuantity + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";

		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value='" + getItem + "'>";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";

		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value='" + getEnteredLocation + "'>";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value='" + getBeginLocationName + "'>";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value='" + getFetchedLocation + "'>";

		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneId + ">";
		html = html + "				<input type='hidden' name='hdnZoneNo' value='" + getZoneNo + "'>";//Case# 20127528
		html = html + "				<input type='hidden' name='hdnbulkpickflag' value=" + Bulkpickflag + ">";

		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnLocExcpflag'>";
		html = html + "				<input type='hidden' name='hdnQtyExcpflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";

		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";

		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + RecordInternalId + ">";// Case# 20148688

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getEnteredLocation + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>" + getItem + "</label><br>";
		//html = html + "				ITEM DESCRIPTION: <label>" + Itemdescription + "</label><br>";
		if (Itype == "lotnumberedinventoryitem" || Itype == "lotnumberedassemblyitem")
		{
			html = html + "				EXPECTED LOT#: <label>" +ActBatchno + "</label><br>";
			html = html + "				ACTUAL LOT#: <label>" + ExpBatchno + "</label><br>";
			html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
			html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";		

		}

		html = html + "				</td>";
		html = html + "			</tr>";

		//html = html + "				<tr><td align = 'left'>"+ st11 +"<input name='cmdlpexc' type='submit' value='F6'/></td></tr>";
		//	html = html + "				<tr><td align = 'left'>"+ st8 +"<input name='cmdConfirm' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.focus();this.disabled=true; return false'/>";
		html = html + "				<tr><td align = 'left'>"+ st8 +"<input name='cmdConfirm' id='cmdConfirm' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		//html = html + "				"+ st9 +" <input name='cmdPrevious' type='submit' value='F7'/>"+ st10 +"<input name='cmdSKIP' type='submit' value='F12'/></td></tr>";

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdConfirm').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		nlapiLogExecution('DEBUG', 'Time Stamp at the start of Response',TimeStampinSec());		
		nlapiLogExecution('DEBUG', 'Pick Type',request.getParameter('hdnpicktype'));
		nlapiLogExecution('DEBUG', 'cmdlocexc',request.getParameter('cmdlocexc'));
		nlapiLogExecution('DEBUG', 'hdnLocExcpflag',request.getParameter('hdnLocExcpflag'));

		var getLanguage = request.getParameter('hdngetLanguage');

		var st11;

		if( getLanguage == 'es_ES')
		{
			st11 = "CONFIRME LA REPLENS ABIERTAS";
		}
		else
		{
			st11 = "CONFIRM THE OPEN REPLENS";
		}

		var vZoneId=request.getParameter('hdnebizzoneno');
		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		var nextexpqty = request.getParameter('hdnextExpectedQuantity');
		var invrefno = request.getParameter('hdnInvoiceRefNo');
		var ItemType=request.getParameter('hdnitemtype');
		var Bulkpickflag=request.getParameter('hdnbulkpickflag');  
		
		SOarray["custparam_bulkpickflag"] = Bulkpickflag;
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_LP"] = request.getParameter('hdnLP');
		SOarray["custparam_ItemStatus"] = request.getParameter('hdnItemStatus');

		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginLocationName');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');;
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');		
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnextExpectedQuantity');
		SOarray["custparam_nextrecordid"]=request.getParameter('hdnRecordInternalId');
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		SOarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');
		SOarray["custparam_recordinternalid"]= request.getParameter('custparam_recordinternalid');// Case# 20148688
		//var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		//var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		//SOarray["custparam_Newendlocid"] = EntLocID;
		//SOarray["custparam_Newendloc"] = EntLoc;
		SOarray["custparam_EntLocRec"] = EntLocRec;
		SOarray["custparam_Exc"]=ExceptionFlag;

		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';

		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');// Case# 20148688
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_item', 'customdeploy_rf_bulkpick_item', false, SOarray);
		}
		/*else if(request.getParameter('cmdSKIP') == 'F12')
		{
			var RecordInternalId = request.getParameter('hdnRecordInternalId');
			var ContainerLPNo = request.getParameter('hdnContainerLpNo');
			var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
			var BeginLocation = request.getParameter('hdnBeginLocationId');
			var Item = request.getParameter('hdnItem');
			var ItemName = request.getParameter('hdnItemName');
			var ItemDescription = request.getParameter('hdnItemDescription');
			var ItemInternalId = request.getParameter('hdnItemInternalId');
			var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
			var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
			var BeginLocationName = request.getParameter('hdnBeginBinLocation');
			var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
			var EndLocation = request.getParameter('hdnEnteredLocation');
			var OrderLineNo = request.getParameter('hdnOrderLineNo');
			var NextShowLocation=request.getParameter('hdnNextLocation');
			var ContainerSize=request.getParameter('hdnContainerSize');
			var NextItemInternalId=request.getParameter('hdnNextItemId');
			var WaveNo = request.getParameter('hdnWaveNo');
			var ClusNo = request.getParameter('hdnClusterNo');
			var ebizOrdNo=request.getParameter('hdnebizOrdNo');
			var OrdName=request.getParameter('hdnName');
			var RecCount;
			var SOFilters = new Array();
			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
			if(WaveNo != null && WaveNo != "")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
			}
			if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
			{
				nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
			}
			if(OrdName!=null && OrdName!="" && OrdName!= "null")
			{
				nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
			}
			if(ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
			{
				nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
			}
			if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
			{
				nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
			}

			SOarray["custparam_ebizordno"] =ebizOrdNo;
			var SOColumns = new Array();
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
			SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
			SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
			SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			SOColumns.push(new nlobjSearchColumn('name'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
			SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
			SOColumns[0].setSort();
			SOColumns[1].setSort();
			SOColumns[2].setSort();
			SOColumns[3].setSort();
			SOColumns[4].setSort();
			SOColumns[5].setSort(true);

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
			if (SOSearchResults != null && SOSearchResults.length > 0) {
				nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length);
				nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);

				vSkipId=0;

				if(SOSearchResults.length <= parseFloat(vSkipId))
				{
					vSkipId=0;
				}
				nlapiLogExecution('DEBUG', 'SearchResults of given Wave', WaveNo);
				//var SOSearchResult = SOSearchResults[0];
				var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
				if(SOSearchResults.length > parseFloat(vSkipId) + 1)
				{
					//var SOSearchnextResult = SOSearchResults[1];
					var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
					nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
					nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
				}
				else
				{
					var SOSearchnextResult = SOSearchResults[0];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
					nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
					nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
				}

				SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
				SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
				//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
				SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
				SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
				SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
				SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
				SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
				SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
				SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
				SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
				SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
				SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
				SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
				SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
				SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
				SOarray["custparam_noofrecords"] = SOSearchResults.length;		
				SOarray["name"] =  SOSearchResult.getValue('name');
				SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
				SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
				getBeginLocation = SOSearchResult.getText('custrecord_actbeginloc');
				if(vZoneId!=null && vZoneId!="")
				{
					SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
				}
				else
					SOarray["custparam_ebizzoneno"] = '';

			}
			vSkipId= parseFloat(vSkipId) + 1;
			SOarray["custparam_skipid"] = vSkipId;	

			var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
			if(skiptask==null || skiptask=='')
			{
				skiptask=1;
			}
			else
			{
				skiptask=parseInt(skiptask)+1;
			}

			nlapiLogExecution('DEBUG', 'skiptask',skiptask);

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);

//			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
//			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			if(BeginLocation != SOarray["custparam_nextlocation"])
			{  
				SOarray["custparam_beginLocationname"]=null;
				SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
			}
			else if(ItemInternalId != SOarray["custparam_nextiteminternalid"])
			{ 
				SOarray["custparam_beginLocationname"]=null;
				//SOarray["custparam_iteminternalid"]=null;
				SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
				SOarray["custparam_nextexpectedquantity"] = SOarray["custparam_expectedquantity"];
				response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
			}
			else if(ItemInternalId == SOarray["custparam_nextiteminternalid"])
			{ 
				SOarray["custparam_beginLocationname"]=null;
				SOarray["custparam_iteminternalid"]=ItemInternalId;
				SOarray["custparam_nextexpectedquantity"] = SOarray["custparam_expectedquantity"];
				response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
			}
		}*/
		else 
		{
			if (request.getParameter('hdnflag') == 'F8') {
				nlapiLogExecution('DEBUG', 'Confirm Pick');


				var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
				var BeginLocation = request.getParameter('hdnBeginLocationId');
				var Item = request.getParameter('hdnItem');
				var ItemName = request.getParameter('hdnItemName');
				var ItemDescription = request.getParameter('hdnItemDescription');
				var ItemInternalId = request.getParameter('hdnItemInternalId');

				var BeginLocationName = request.getParameter('hdnBeginBinLocation');
				var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
				var EndLocation = request.getParameter('hdnEnteredLocation');

				var getWaveNo = request.getParameter('hdnWaveNo');
				var RecordInternalId = request.getParameter('hdnRecordInternalId');// Case# 20148653 

				var vZoneNo=request.getParameter('hdnZoneNo');
				var vZoneId=request.getParameter('hdnebizzoneno');
				if(vZoneId!=null && vZoneId!="")
				{
					SOarray["custparam_ebizzoneno"] =  vZoneId;
				}
				else
					SOarray["custparam_ebizzoneno"] = '';
				SOarray["custparam_zoneno"] =  vZoneNo;
				var whLocation=request.getParameter('hdnwhlocation');
				nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);

				//var SOarray = new Array();
				SOarray["custparam_language"] = getLanguage;
				 
				 
				/*SOarray["custparam_screenno"] = 'BULK04';
				SOarray["custparam_whlocation"] = whLocation;
				SOarray["custparam_waveno"] = getWaveNo;
				 
				SOarray["custparam_expectedquantity"] = getExpectedQuantity;
				SOarray["custparam_beginLocation"] = getBeginLocation;
				SOarray["custparam_beginLocationname"] = getBeginLocation;
				SOarray["custparam_item"] = getItem;
				SOarray["custparam_itemname"] = getItemName;
				SOarray["custparam_itemdescription"] = getItemDescription;
				SOarray["custparam_iteminternalid"] = getItemInternalId;*/
				   
				SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
				SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
				  
				var vZoneNo=request.getParameter('hdnZoneNo');
				var vZoneId=request.getParameter('hdnebizzoneno');
				if(vZoneId!=null && vZoneId!="")
				{
					SOarray["custparam_ebizzoneno"] =  vZoneId;
				}
				else
					SOarray["custparam_ebizzoneno"] = '';
				SOarray["custparam_zoneno"] =  vZoneNo;
				
				var vSkipId=request.getParameter('hdnskipid');
				SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
				

				var invtqty=0;

				var IsPickFaceLoc = 'F';

				IsPickFaceLoc = isPickFaceLocation(ItemInternalId,EndLocInternalId);

				nlapiLogExecution('DEBUG', 'ExpectedQuantity', ExpectedQuantity);
				nlapiLogExecution('DEBUG', 'IsPickFaceLoc', IsPickFaceLoc);
 

				if(IsPickFaceLoc=='T')
				{
					var openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemInternalId);

					if(openinventory!=null && openinventory!='')
					{
						nlapiLogExecution('DEBUG', 'Pick Face Inventory Length', openinventory.length);

						for(var x=0; x< openinventory.length;x++)
						{
							var vinvtqty=openinventory[x].getValue('custrecord_ebiz_qoh');
							//vinvtqty=openinventory[0].getValue('custrecord_ebiz_avl_qty',null,'sum');

							if(vinvtqty==null || vinvtqty=='' || isNaN(vinvtqty))
							{
								vinvtqty=0;
							}

							invtqty=parseInt(invtqty)+parseInt(vinvtqty);
						}						
					}
				}

				if(invtqty==null || invtqty=='' || isNaN(invtqty))
				{
					invtqty=0;
				}

				nlapiLogExecution('DEBUG', 'invtqty', invtqty);

				if(IsPickFaceLoc=='T' && (parseInt(ExpectedQuantity)>parseInt(invtqty)))
				{
					var openreplns = new Array();
					openreplns=getOpenReplns(ItemInternalId,EndLocInternalId);

					if(openreplns!=null && openreplns!='')
					{
						for(var i=0; i< openreplns.length;i++)
						{
							var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
							var expqty=openreplns[i].getValue('custrecord_expe_qty');
							if(expqty == null || expqty == '')
							{
								expqty=0;
							}	
							nlapiLogExecution('DEBUG', 'open repln qty',expqty);
							nlapiLogExecution('DEBUG', 'getQuantity',ExpectedQuantity);
							//if(parseInt(expqty)>=parseInt(ExpectedQuantity))
							//{
							//case# 20148688 starts(commented because skiptask functionality is not there in Bulk picking)
							/*var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
							if(skiptask==null || skiptask=='')
							{
								skiptask=1;
							}
							else
							{
								skiptask=parseInt(skiptask)+1;
							}

							nlapiLogExecution('DEBUG', 'skiptask',skiptask);*/
							//case# 20148688 ends
							//nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);


							//nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');
							//nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', 'Y');	
							 
							SOarray["custparam_screenno"] = 'BULK04';	
							SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
							//}
						}
					}
					else
					{
						var replengen = generateReplenishment(EndLocInternalId,ItemInternalId,whLocation,getWaveNo);
						nlapiLogExecution('DEBUG', 'replengen',replengen);
						//if(replengen==true)
						//{
						//case# 20148688 starts(commented because skiptask functionality is not there in Bulk picking)
						/*var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',RecordInternalId,'custrecord_skiptask');
						if(skiptask==null || skiptask=='')
						{
							skiptask=1;
						}
						else
						{
							skiptask=parseInt(skiptask)+1;
						}

						nlapiLogExecution('DEBUG', 'skiptask',skiptask);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', skiptask);*/
						//case# 20148688 ends
						//nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', 'Y');
						 	

						if(replengen!=true)
							SOarray["custparam_error"]='Replen Generation Failed.Insufficient inventory in Bulk location';
						else 
							SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
						//}
					}

				}				
				else
				{
					/*if(IsPickFaceLoc=='T' && (InvoiceRefNo==null || InvoiceRefNo==''))
					{
						if(openinventory!=null && openinventory!='')
						{
							var openinvtrecid = openinventory[0].getId();
							nlapiLogExecution('DEBUG', 'openinvtrecid', openinvtrecid);

							try
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_invref_no', openinvtrecid);
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG', 'Exception in updating inventory reference number', exp);
							}
						}*/
				}

				
				//ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo)

				var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);
				nlapiLogExecution('DEBUG', 'itemSubtype', itemSubtype);
				nlapiLogExecution('DEBUG', 'itemSubtype.recordType', itemSubtype.recordType);
				nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizserialout', itemSubtype.custitem_ebizserialout);
				//case # 20127529�
				if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') {
				SOarray["custparam_number"] = 0;
				SOarray["custparam_RecType"] = itemSubtype.recordType;
				SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
				SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
				response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_serialscan', 'customdeploy_rf_bulkpick_serialscan_di', false, SOarray);
				}
				else
				{	
				var vPickType = request.getParameter('hdnpicktype');
				//var  vdono = DoLineId;
				//nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
				//Changed the code for auto containerization
				/*var vAutoContainerFlag='N'; // Set 'Y' to skip RF dialog for scanning container LP
					SOarray["custparam_autocont"] = vAutoContainerFlag;*/

				SOarray["custparam_bulkpickflag"] = Bulkpickflag;
				response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_container_no', 'customdeploy_rf_bulkpick_container_no_di', false, SOarray);

				}
			}
		}


		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

		nlapiLogExecution('DEBUG', 'Time Stamp at the end of Response',TimeStampinSec());
	}
}
 

function getOpenReplns(item,location)
{
	nlapiLogExecution('DEBUG', 'Into getOpenReplns');
	var PickFaceSearchResults= new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Task Type - RPLN	 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'is', location));	
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_taskpriority');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, PickFaceFilters, PickFaceColumns);



	return PickFaceSearchResults;

}
 
function isPickFaceLocation(item,location)
{
	nlapiLogExecution('DEBUG', 'Into isPickFaceLocation');
	nlapiLogExecution('DEBUG', 'item',item);
	nlapiLogExecution('DEBUG', 'location',location);
	var isPickFace='F';
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_autoreplen');
	var PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);
	if(PickFaceSearchResults!=null && PickFaceSearchResults!=''&& PickFaceSearchResults.length>0)
	{
		isPickFace='T';
	}

	nlapiLogExecution('DEBUG', 'Return Value',isPickFace);
	nlapiLogExecution('DEBUG', 'Out of isPickFaceLocation');

	return isPickFace;
}

function getQOHForAllSKUsinPFLocation(location,item){

	nlapiLogExecution('DEBUG','Into getQOHForAllSKUsinPFLocation');

	nlapiLogExecution('DEBUG','location',location);
	nlapiLogExecution('DEBUG','item',item);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('DEBUG','Out of getQOHForAllSKUsinPFLocation');

	return pfSKUInvList;
}