/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_PickCnfm_CL.js,v $
 *     	   $Revision: 1.10.2.7.4.1.4.55.2.7 $
 *     	   $Date: 2015/11/14 09:51:31 $
 *     	   $Author: sponnaganti $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PickCnfm_CL.js,v $
 * Revision 1.10.2.7.4.1.4.55.2.7  2015/11/14 09:51:31  sponnaganti
 * case# 201415433
 * 2015.2 issue fix
 *
 * Revision 1.10.2.7.4.1.4.55.2.6  2015/11/09 16:02:55  aanchal
 * 2015.2 Issue fix
 * 201415445
 *
 * Revision 1.10.2.7.4.1.4.55.2.5  2015/11/06 22:22:06  sponnaganti
 * case# 201415443
 * 2015.2 issue fix
 *
 * Revision 1.10.2.7.4.1.4.55.2.4  2015/11/06 15:46:34  grao
 * 2015.2 Issue Fixes 201414344
 *
 * Revision 1.10.2.7.4.1.4.55.2.3  2015/10/16 12:58:44  schepuri
 * case# 201415080
 *
 * Revision 1.10.2.7.4.1.4.55.2.2  2015/10/15 07:33:10  rrpulicherla
 * pick report changes
 *
 * Revision 1.10.2.7.4.1.4.55.2.1  2015/10/08 14:39:49  schepuri
 * case# 201414344
 *
 * Revision 1.10.2.7.4.1.4.55  2015/09/03 07:44:57  schepuri
 * case# 201414200
 *
 * Revision 1.10.2.7.4.1.4.54  2015/09/01 15:13:38  aanchal
 * 2015.2 issue fixes
 * 201414141
 *
 * Revision 1.10.2.7.4.1.4.53  2015/08/24 14:38:17  schepuri
 * case# 201414010
 *
 * Revision 1.10.2.7.4.1.4.52  2015/07/24 15:33:01  grao
 * 2015.2   issue fixes  201413613
 *
 * Revision 1.10.2.7.4.1.4.51  2015/07/23 15:33:28  grao
 * 2015.2   issue fixes  201412890
 *
 * Revision 1.10.2.7.4.1.4.50  2015/07/17 15:14:04  grao
 * 2015.2   issue fixes  201412890
 *
 * Revision 1.10.2.7.4.1.4.49  2015/07/14 15:30:28  grao
 * 2015.2 ssue fixes  201413387
 *
 * Revision 1.10.2.7.4.1.4.48  2015/06/16 07:31:12  nneelam
 * case# 201413021
 *
 * Revision 1.10.2.7.4.1.4.47  2014/10/17 13:12:22  skavuri
 * Case# 201410588 Std bundle Issue fixed
 *
 * Revision 1.10.2.7.4.1.4.46  2014/09/15 16:00:33  skavuri
 * Case# 201410340 Std Bundle issue fixed
 *
 * Revision 1.10.2.7.4.1.4.45  2014/09/03 15:48:16  sponnaganti
 * case# 20147957
 * stnd bundle issue fix
 *
 * Revision 1.10.2.7.4.1.4.44  2014/09/02 11:00:31  gkalla
 * case#20149227
 * ACE and JB cycle count issues
 *
 * Revision 1.10.2.7.4.1.4.43  2014/08/13 09:23:52  gkalla
 * case#20149227
 * JB-Cycle Count - Even though the Cycle Count is in process user should be able to pick the product
 *
 * Revision 1.10.2.7.4.1.4.42  2014/08/08 15:05:46  skavuri
 * Case# 20149906 SB Issue Fixed
 *
 * Revision 1.10.2.7.4.1.4.41  2014/08/01 15:17:36  skavuri
 * cASE # 20149747 sb iSSUE fIXED
 *
 * Revision 1.10.2.7.4.1.4.40  2014/07/24 15:15:13  skavuri
 * Case # 20149599 Compatibility Issue Fixed
 *
 * Revision 1.10.2.7.4.1.4.39  2014/06/05 15:28:27  nneelam
 * case#  20148629
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.10.2.7.4.1.4.38  2014/05/29 15:40:27  sponnaganti
 * case# 20148590
 * Stnd Bundle Issue fix
 *
 * Revision 1.10.2.7.4.1.4.37  2014/04/16 07:55:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to validating LP
 *
 * Revision 1.10.2.7.4.1.4.36  2014/03/14 14:54:30  rmukkera
 * Case # 20127191
 *
 * Revision 1.10.2.7.4.1.4.35  2014/02/14 06:22:47  grao
 * Case# 20127089 related issue fixes in Dealmed  issue fixes
 *
 * Revision 1.10.2.7.4.1.4.34  2014/01/16 13:36:23  schepuri
 * 20126734
 * standard bundle issue fix
 *
 * Revision 1.10.2.7.4.1.4.33  2014/01/15 13:41:53  schepuri
 * 20126734
 * standard bundle issue fix
 *
 * Revision 1.10.2.7.4.1.4.32  2014/01/09 14:54:20  rmukkera
 * Case # 20126633�
 *
 * Revision 1.10.2.7.4.1.4.31  2014/01/07 15:30:37  rmukkera
 * Case # 20126486
 *
 * Revision 1.10.2.7.4.1.4.30  2013/12/27 15:38:42  rmukkera
 * Case # 20126486
 *
 * Revision 1.10.2.7.4.1.4.29  2013/12/26 15:21:05  rmukkera
 * Case # 20126486
 *
 * Revision 1.10.2.7.4.1.4.28  2013/12/17 15:21:27  rmukkera
 * Case # 20126347�
 *
 * Revision 1.10.2.7.4.1.4.27  2013/11/28 14:52:28  rmukkera
 * Case# 20126037�,20126038�
 *
 * Revision 1.10.2.7.4.1.4.26  2013/10/22 15:04:51  skreddy
 * Case# 20125190
 * standard bundle issue fix
 *
 * Revision 1.10.2.7.4.1.4.25  2013/10/15 15:48:37  rmukkera
 * Case# 20124589
 *
 * Revision 1.10.2.7.4.1.4.24  2013/10/04 15:45:04  rmukkera
 * Case# 20124590
 *
 * Revision 1.10.2.7.4.1.4.23  2013/09/25 06:51:22  schepuri
 *  picking qty is deducted from Previous Bin Location because of alert msg
 *
 * Revision 1.10.2.7.4.1.4.22  2013/09/16 14:26:14  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to serial out while pick confirmation.
 *
 * Revision 1.10.2.7.4.1.4.21  2013/09/12 15:34:58  rmukkera
 * Case# 20124196
 *
 * Revision 1.10.2.7.4.1.4.20  2013/09/11 15:23:51  rmukkera
 * Case# 20124376
 *
 * Revision 1.10.2.7.4.1.4.19  2013/08/30 16:39:41  skreddy
 * Case# 20124068
 * standard bundle issue fix
 *
 * Revision 1.10.2.7.4.1.4.18  2013/08/21 15:38:10  rmukkera
 * Issue Fix related to 20123844
 *
 * Revision 1.10.2.7.4.1.4.17  2013/08/19 16:42:39  grao
 * Standard bundle issues fixes :20123844�
 * System should not allowed to enter lot# at confirm picks screen
 *
 * Revision 1.10.2.7.4.1.4.16  2013/08/14 15:34:35  rmukkera
 * standard bundle.  Issue case #20123806
 *
 * Revision 1.10.2.7.4.1.4.15  2013/08/12 15:15:18  rmukkera
 * Issue FIx for caseNo 20123845�,20123846�,20123849�
 *
 * Revision 1.10.2.7.4.1.4.14  2013/07/15 07:55:25  spendyala
 * case# 20123441
 * Validating user to select atleast one value while confirming picks
 *
 * Revision 1.10.2.7.4.1.4.13  2013/06/25 15:50:45  gkalla
 * Case# 20123182
 * Standard bundle Issue fix
 *
 * Revision 1.10.2.7.4.1.4.12  2013/06/25 15:40:21  nneelam
 * CASE201112/CR201113/LOG201121
 * Pick Location exception through UI:Issue No. 20123139
 *
 * Revision 1.10.2.7.4.1.4.11  2013/06/21 23:33:44  skreddy
 * CASE201112/CR201113/LOG201121
 * to restrict when payment credit limit exceed
 *
 * Revision 1.10.2.7.4.1.4.10  2013/06/11 16:31:23  skreddy
 * CASE201112/CR201113/LOG201121
 * standard bundle issue fixes
 *
 * Revision 1.10.2.7.4.1.4.9  2013/06/11 16:01:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Stdbundle issue fixes
 *
 * Revision 1.10.2.7.4.1.4.8  2013/05/14 14:53:00  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.10.2.7.4.1.4.7  2013/05/07 15:13:44  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.10.2.7.4.1.4.6  2013/04/09 08:47:44  grao
 * CASE201112/CR201113/LOG201121
 * Issue fixed for url changed
 *
 * Revision 1.10.2.7.4.1.4.5  2013/04/03 01:20:11  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.10.2.7.4.1.4.4  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.10.2.7.4.1.4.3  2013/03/19 12:12:36  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.10.2.7.4.1.4.2  2013/03/19 11:46:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.10.2.7.4.1.4.1  2013/03/05 13:35:46  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.10.2.7.4.1  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.10.2.7  2012/09/14 07:21:57  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for null
 *
 * Revision 1.10.2.6  2012/08/08 15:19:10  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Kit to order
 *
 * Revision 1.10.2.5  2012/05/04 13:38:00  mbpragada
 * CASE201112/CR201113/LOG201121
 * Here System allowed  to short pick quantity  while doing Pick confirmation.
 *
 * Revision 1.10.2.4  2012/04/30 06:55:51  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10.2.2  2012/04/18 13:41:54  schepuri
 * CASE201112/CR201113/LOG201121
 * validating so qty with pick qty
 *
 * Revision 1.10.2.1  2012/04/05 13:15:23  schepuri
 * CASE201112/CR201113/LOG201121
 * paging functionality is added
 *
 * Revision 1.10  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/11/28 07:32:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Exception changes
 *
 * Revision 1.8  2011/10/21 13:42:47  schepuri
 * CASE201112/CR201113/LOG201121
 * Qty discrepancy with short PICK
 *
 * Revision 1.7  2011/10/03 08:25:05  snimmakayala
 * CASE201112/CR201113/LOG20112
 *
 * Revision 1.6  2011/09/29 16:16:40  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/09/29 14:00:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/08/25 12:25:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Pick Confirmation Changes
 *
 * Revision 1.3  2011/07/21 04:57:26  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.10  2011/07/20 12:57:48  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.4  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function SerialnoSelection(type, fld){
	try {   
		if (fld == "custpage_itemserialno") {
			var serialout;
			var serialin;
			var ItemType;
			var itemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemno');
			var columns = nlapiLookupField('item', itemId, ['recordType','custitem_ebizserialout','custitem_ebizserialin']);
			if(columns != null && columns!='')
			{
				ItemType= columns.recordType;
				serialout= columns.custitem_ebizserialout;
				serialin= columns.custitem_ebizserialin;
			}
			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialout=='T') {
				var SerialURL;
				var ctx = nlapiGetContext();
				nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
				/*if (ctx.getEnvironment() == 'PRODUCTION') {
					SerialURL = 'https://system.netsuite.com';			
				}
				else 
					if (ctx.getEnvironment() == 'SANDBOX') {
						SerialURL = 'https://system.sandbox.netsuite.com';				
					}

				var linkURL = SerialURL + "/app/site/hosting/scriptlet.nl?script=customscript_pickcnfmserialscan&deploy=1";*/	

				var linkURL = nlapiResolveURL('SUITELET', 'customscript_pickcnfmserialscan', 'customdeploy_pickcnfmserialscan_di');
				linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemno');
				//linkURL = linkURL + '&custparam_serialskuname='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemname');//commented because of item containing special characters and this breaks url sequence				
				linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_fromlp');
				linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_ordqty');
				linkURL = linkURL + '&custparam_opentaskid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_recid');
				linkURL = linkURL + '&custparam_contLpno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lpno');
				linkURL = linkURL + '&custparam_dointernno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_dono');
				linkURL = linkURL + '&custparam_dolineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lineno');
				linkURL = linkURL + '&custparam_actbeginloc=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_actbeginloc');
				linkURL = linkURL + '&custparam_saleorderid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_salesordinternid');

				if(serialin=="F")
					linkURL = linkURL + '&custparam_serialout=' +serialout;
				else
					linkURL = linkURL + '&custparam_serialout=F';
				window.open(linkURL);
			}
			else
			{
				var serial=nlapiGetCurrentLineItemValue('custpage_items','custpage_itemserialno');
				if(serial!=null && serial!='')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_itemserialno','',false,false);
					alert("This is not a serial Item");

					return false;
				}
			}
		}

		if (fld == "custpage_actbeginloc") {

			var vbinLoc = nlapiGetCurrentLineItemValue('custpage_items','custpage_actbeginloc');
			var vorderqty = nlapiGetCurrentLineItemValue('custpage_items','custpage_ordqty');
			var skustatus=nlapiGetCurrentLineItemValue('custpage_items','custpage_skustatusvalue');
			var wmslocation=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_wmslocation');

			//Case# 20126037�start

			if(vbinLoc!=null && vbinLoc!='')
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('internalid', null, 'is', vbinLoc));
				var columns = new Array();
				columns.push(new nlobjSearchColumn('custrecord_ebizsitelocf'));

				var binsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

				if(binsearchresults !=null && binsearchresults!='')
				{
					var temploc=binsearchresults[0].getValue('custrecord_ebizsitelocf');

					if(temploc!=null && temploc!='' )
					{
						if(parseInt(temploc)!=parseInt(wmslocation))
						{
							alert('This Location Belongs to other site ('+binsearchresults[0].getText('custrecord_ebizsitelocf')+')');
							return false;
						}
						else
						{
							nlapiSetCurrentLineItemValue('custpage_items', 'custpage_itembatchno','',false,false);						
						}
					}
				}
			}
			//Case# 20126037� End.
			//alert("vbinLoc::" + vbinLoc);	
			var serialout;
			var serialin;
			var ItemType;
			var itemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemno');			

			var columns = nlapiLookupField('item', itemId, ['recordType','custitem_ebizserialout','custitem_ebizserialin']);
			if(columns != null && columns!='')
			{
				ItemType= columns.recordType;
				serialout= columns.custitem_ebizserialout;
				serialin= columns.custitem_ebizserialin;
			}
			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialout=='T') {
				var SerialURL;

				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', vbinLoc));
				//Case # 20124196 start
				filters.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthanorequalto', vorderqty));
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemId));
				filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));
				if(wmslocation!=null && wmslocation!='')
					filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',wmslocation));
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', skustatus));
				// Case # 20124196 end
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
				columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');			
				columns[1].setSort();
				var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
				var invtlp;
				if(invtsearchresults != null && invtsearchresults !=''){
					//alert("searchresults::" + invtsearchresults.length);
					/*for (var k=0;k<invtsearchresults.length;k++)
					{*/ 
					//alert("hi::");
					//var searchresult = invtsearchresults[k];

					invtlp = invtsearchresults[0].getValue('custrecord_ebiz_inv_lp');
					//alert("invtlp::" + invtlp);

					//}				


					var linkURL = nlapiResolveURL('SUITELET', 'customscript_pickcnfmserialscan', 'customdeploy_pickcnfmserialscan_di');
					linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemno');
					//linkURL = linkURL + '&custparam_serialskuname='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemname');//commented because of item containing special characters and this breaks url sequence
					linkURL = linkURL + '&custparam_serialskulp=' + invtlp;
					linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_ordqty');
					linkURL = linkURL + '&custparam_opentaskid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_recid');
					linkURL = linkURL + '&custparam_contLpno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lpno');
					linkURL = linkURL + '&custparam_dointernno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_dono');
					linkURL = linkURL + '&custparam_dolineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lineno');
					linkURL = linkURL + '&custparam_actbeginloc=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_actbeginloc');
					linkURL = linkURL + '&custparam_saleorderid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_salesordinternid');

					linkURL = linkURL + '&custparam_oldLP=' +invtlp;
					if(serialin=="F")
						linkURL = linkURL + '&custparam_serialout=' +serialout;
					else
						linkURL = linkURL + '&custparam_serialout=F';


					window.open(linkURL);

				}
				else					
				{
					alert("No Inventory For This Bin Location");
					return false;
				}


			}  

		}

		var linecheck= nlapiGetCurrentLineItemValue('custpage_items','custpage_so');
		if(fld == 'custpage_itembatchno')
		{
			//alert("fld: " + fld);
			if (linecheck == "T") {	
				var ItemType;var batchflg;
				var fields = ['recordType', 'custitem_ebizbatchlot'];
				var itemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemno');			
				var vbatchno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itembatchno');
				var columns = nlapiLookupField('item', itemId, fields);
				//alert("vbatchno:" + vbatchno);
				if(columns != null && columns!='')
				{
					ItemType = columns.recordType;					
					batchflg = columns.custitem_ebizbatchlot;		

				}

//				alert("ItemType: " + ItemType);

				if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
				{
					return true;
				}
				else
				{			
					/*

			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialout=='T'){*/

					if(vbatchno!=null || vbatchno!='')
					{
						var p = 1;
						nlapiSetCurrentLineItemValue('custpage_items','custpage_itembatchno','',false,false);
						alert("This is not a lot Item");

						return false;
					}
					else
					{
						return true;
					}

				}

			}
		}



		if(fld == 'custpage_itembatchno')
		{
			//alert("fld: " + fld);

			var ItemType;
			var itemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemno');			
			var vbatchno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itembatchno');
			var columns = nlapiLookupField('item', itemId, ['recordType']);
			//alert("vbatchno:" + vbatchno);
			if(columns != null && columns!='')
			{
				ItemType= columns.recordType;		

			}
//			alert("ItemType: " + ItemType);

			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialout=='T'){

				if(vbatchno!=null || vbatchno!='')
				{
					var p = 1;
					alert("Serial item not allowed the Lot numbers");
					nlapiSetLineItemValue('custpage_items','custpage_itembatchno',p,'');					
					return false;
				}

			}
			else 
			{

			}

		}



		var fulfilmentItem;
		var kitqty;
		var memberitemqty;

		if (linecheck == "T") {	

			var itemno = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemno');
			var itemnname = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemname');
			var vserialno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemserialno');
			var vOrdQty=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_ordqty');
			var vDisplayedQty=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_displyedordqty');
			var vkititem=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_kititem');

			if(vkititem!=null && vkititem!='')
			{

				var kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');

				if(kititemTypesku=='kititem')
				{
					var searchresultsitem = nlapiLoadRecord(kititemTypesku, vkititem);
					var SkuNo=searchresultsitem.getFieldValue('itemid');

					var filters = new Array(); 			 
					filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);	


					var columns1 = new Array(); 
					columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
					columns1[1] = new nlobjSearchColumn( 'memberquantity' );

					var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 	

					if(searchresults!=null && searchresults!='')
					{

						for(var w=0; w<searchresults.length;w++) 
						{
							fulfilmentItem = searchresults[w].getValue('memberitem');
							memberitemqty = searchresults[w].getValue('memberquantity');

							if(fulfilmentItem==itemno)
							{
								kitqty=Math.floor(vOrdQty/memberitemqty)*memberitemqty;

								if((kitqty!=vOrdQty) && (parseFloat(vOrdQty) < parseFloat(vDisplayedQty)))
								{				
									alert("Pick in multiples of 2 as this pick is part of Kit Package item");

									return false;
								}
								else
								{
									return true;
								}
							}
						}
					}				
				}
			}
		}
		if (fld == "custpage_lpno") // case# 201415080
		{
			var NewContLP = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lpno');// case# 201414200
			var siteid=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_wmslocation');

			var lpExists='N';
			var filtersmlp = new Array();
			filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', NewContLP);

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

			if (SrchRecord != null && SrchRecord.length > 0) {
				nlapiLogExecution('ERROR', 'LP FOUND');

				lpExists = 'Y';
			}

			if(lpExists =='N')
			{	
				var LPReturnValue = ebiznet_LPRange_CL_withLPType(NewContLP.replace(/\s+$/,""), '2','2',siteid);//'2'UserDefiend,'2'PICK
				//if(LPReturnValue==false && IsAuto != 'AUTO')
				//{

				if(LPReturnValue==false)
				{	
					alert('Invalid LP Range');
					return false;
				}
				//}
			}
			else
			{
				alert('LP Already Exist');
				return false;
			}
		}
	} 
	catch (err) {
	}
	if(fld=='custpage_selectpage'||fld=='custpage_pagesize')
	{
		//nlapiSetFieldText('custpage_flag','T');
		nlapiSetFieldValue('custpage_flag','paging');
		var tempflag= nlapiGetFieldValue('custpage_flag');
		NLDoMainFormButtonAction("submitter",true);	
	}
	else
	{
		return true;
	}

}

function BatchCustRecordChk(ChknBatchNo){

	var filtersbat = new Array();
	filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', ChknBatchNo);

	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);

	if (SrchRecord) {
		return true;
	}
	else {
		return false;
	}
}

function Ondelete(type)
{
	//alert('Ondelete');
	var vfono=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_dorefno');
	var vfolineno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lineno');
	
	
	if(vfono == null || vfono == '')
	{
		nlapiSetCurrentLineItemValue('custpage_items','custpage_so','F');
		return false;
		
	}
	
	var shipcomplete='N';
	var filtersfo = new Array();

	if(vfono!=null && vfono!='' && vfono!='null')
	filtersfo.push(new nlobjSearchFilter('internalid', null, 'is', vfono));				
	filtersfo.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', vfolineno));

	var columnsfo = new Array();
	columnsfo[0] = new nlobjSearchColumn('custrecord_shipcomplete');

	var searchresultsfo = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersfo, columnsfo);
	if(searchresultsfo!=null)
		shipcomplete=searchresultsfo[0].getValue('custrecord_shipcomplete');
	
	if(shipcomplete=='T')
	{

		alert('This SO has created with Ship Complete set ON, you can not delete the line');
		nlapiSetCurrentLineItemValue('custpage_items', 'custpage_so','T');
		//return true;

	}

	

}
/*
This Script is called when a PUTaway Record is Saved with out a Location.
 */
function onSave(type)
{

	var lineCnt = nlapiGetLineItemCount('custpage_items');
	var i = 0;	
	var pickexception='F';
	var AtleastonelinetoSelected="F";
	var flag = 'T';
	var binlocidarr=new Array();
	for (var s = 1; s <= lineCnt; s++) {

		var linecheck= nlapiGetLineItemValue('custpage_items','custpage_so',s);

		if (linecheck == "T") {	
			AtleastonelinetoSelected="T";

			var serialin;


			var itemno = nlapiGetLineItemValue('custpage_items', 'custpage_itemno', s);
			var itemnname = nlapiGetLineItemValue('custpage_items', 'custpage_itemname', s);
			var vserialno=nlapiGetLineItemValue('custpage_items', 'custpage_itemserialno', s);
			var vOrdQty=nlapiGetLineItemValue('custpage_items', 'custpage_ordqty', s);
			var vDisplayedQty=nlapiGetLineItemValue('custpage_items', 'custpage_displyedordqty', s);
			var vkititem=nlapiGetLineItemValue('custpage_items', 'custpage_kititem', s);
			var AcctCreditPrefer=nlapiGetLineItemValue('custpage_items', 'custpage_acctcreditpreference', s);
			var vbatchNo=nlapiGetLineItemValue('custpage_items', 'custpage_itembatchno', s);
			var vbinLoc=nlapiGetLineItemValue('custpage_items', 'custpage_actbeginloc', s);
			var vfolineno=nlapiGetLineItemValue('custpage_items', 'custpage_lineno', s);
			var vsoname=nlapiGetLineItemValue('custpage_items', 'custpage_sono', s);
			
			
			var ItemType = nlapiGetLineItemValue('custpage_items', 'custpage_itemtype', s);
			var serialout = nlapiGetLineItemValue('custpage_items', 'custpage_serialout', s);
			var vSerialIn = nlapiGetLineItemValue('custpage_items', 'custpage_serialin', s);
			
			//Case# 201410588 starts
					
			/*var ItemType = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemtype');
			var vSerialOut = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialout');
			var vSerialIn = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialin');*/
			
			//Case# 201410588 ends
			binlocidarr.push(vbinLoc);
			
			/*if(itemno!=null&&itemno!="")
				var columns = nlapiLookupField('item', itemno, ['recordType', 'custitem_ebizserialout','custitem_ebizserialin']);

			if(columns != null && columns!='')
			{
				ItemType = columns.recordType;
				serialout = columns.custitem_ebizserialout;
				serialin= columns.custitem_ebizserialin;
			}*/
//alert(ItemType);
			if (vserialno==null || vserialno == "" ) { 
				if (ItemType == "serializedinventoryitem") {
					alert("Please select Serial No(s)");
					return false;
				}
				else if (serialout == 'T') {
					alert("Please Enter Serial No(s)");
					return false;
				}
			}

		/*	var ItemType = '';
			var serialout = '';
			var columns="";*/
			if(vserialno!=null && vserialno!='')
			{
				/*if(itemno!=null&&itemno!="")
					var columns = nlapiLookupField('item', itemno, ['recordType', 'custitem_ebizserialout','custitem_ebizserialin']);

			if(columns != null && columns!='')
			{
				ItemType = columns.recordType;
				serialout = columns.custitem_ebizserialout;
				serialin= columns.custitem_ebizserialin;
			}
*/

			var containerLPNo='';
			if (vserialno==null || vserialno == "" ) { 
				if (ItemType == "serializedinventoryitem" || serialout == 'T') {
					// start case no 20126734
//					when we enter qty as 0 system should not ask for serial no's
					
						if(parseFloat(vOrdQty) > 0)
						{

							//Cadse# 201410588 starts 
							//alert("Please enter serial(s)for: " + itemnname );					
							//return false;
							
							//alert("Please select Serial No(s)");
							alert("Please enter Serial No(s)");// case# 201416122
							var SerialURL;
							var ctx = nlapiGetContext();
							nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
							
							var linkURL = nlapiResolveURL('SUITELET', 'customscript_pickcnfmserialscan', 'customdeploy_pickcnfmserialscan_di');
							linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemno');					
							//linkURL = linkURL + '&custparam_serialskuname='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemname');//commented because of item containing special characters and this breaks url sequence
							linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_fromlp');
							linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_ordqty');
							linkURL = linkURL + '&custparam_opentaskid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_recid');
							linkURL = linkURL + '&custparam_contLpno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lpno');
							linkURL = linkURL + '&custparam_dointernno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_dono');
							linkURL = linkURL + '&custparam_dolineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lineno');
							linkURL = linkURL + '&custparam_actbeginloc=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_actbeginloc');
							linkURL = linkURL + '&custparam_saleorderid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_salesordinternid');
							if(vSerialIn=="F")
								linkURL = linkURL + '&custparam_serialout=' +vSerialOut;
							else
								linkURL = linkURL + '&custparam_serialout=F';
							window.open(linkURL);
							return false;
						
							
							//Case# 201410588 ends

						}
					

					// end of 20126734
					//alert("Please enter serial(s)for: " + itemnname );					
					//return false;
				}
			}
			else
			{
				if (ItemType == "serializedinventoryitem" || serialout == 'T') {
					var varr=vserialno.split(",");
					if(vOrdQty!= null && vOrdQty!="" && parseFloat(vOrdQty)!= varr.length)
					{
						alert("Please select " + vOrdQty + " Serial No(s) for: " + itemnname);
						return false;
					}

					var ItemId = nlapiGetLineItemValue('custpage_items', 'custpage_itemno',s);
					var fromlp=nlapiGetLineItemValue('custpage_items', 'custpage_fromlp',s);
					var serialno=nlapiGetLineItemValue('custpage_items', 'custpage_itemserialno',s);
//					case # 20124068 start
					//	var isSerialValid=SerialCustRecordChk(ItemId,fromlp,serialno);
					//	return isSerialValid;
//					case # 20124068 end

					}
				}
			}
			if(vbatchNo!=null && vbatchNo!='')
			{
			if(ItemType== "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem"){

				if(vbatchNo==null || vbatchNo=='')
				{
					alert('Please Enter BatchNo');
					return false;
				}
			}
			else
			{

			}
			}
			var DisplayQty = parseFloat(vDisplayedQty);
			var OrdrQty = parseFloat(vOrdQty);
			//alert("DisplayQty:" + DisplayQty);
			//alert("OrdrQty:" + OrdrQty);
			if(DisplayQty != OrdrQty)
			{
				pickexception='T';
			}
			//alert('vOrdQty'+vOrdQty);
			//alert('vDisplayedQty'+vDisplayedQty);
			if(parseFloat(vOrdQty) > parseFloat(vDisplayedQty))
			{				
				alert("Pick Qty should not be greater than Ordered Qty");
				return false;
			}

			var FO=nlapiGetLineItemValue('custpage_items', 'custpage_sono', s);
			var HoldStatus=nlapiGetLineItemValue('custpage_items', 'custpage_paymenthold', s); 

			if(HoldStatus!=null && HoldStatus!='')
			{

				if(HoldStatus == '<font color=#ff0000><b>CLOSED</b></font>')
					HoldStatus = "CLOSED";
				if(HoldStatus == '<font color=#ff0000><b>CANCELLED</b></font>')
					HoldStatus = "CANCELLED";
				if(HoldStatus == '<font color=#ff0000><b>HOLD</b></font>')
					HoldStatus = "HOLD";
				if(HoldStatus == '<font color=#ff0000><b>Days Over Due</b></font>')
					HoldStatus = "Days Over Due";
				if(HoldStatus == '<font color=#ff0000><b>Credit Limit Exceed</b></font>')
					HoldStatus = "Credit Limit Exceed";
			}
			//if (HoldStatus!=null && HoldStatus != "" ) { 
			//alert(HoldStatus)
			//if(HoldStatus == '<font color=#ff0000><b>HOLD</b></font>')
			//HoldStatus = "HOLD"
			if(HoldStatus == "CLOSED" || HoldStatus == "CANCELLED" || HoldStatus == "HOLD" || HoldStatus == "Days Over Due" )
			{

				//nlapiSetCurrentLineItemValue('custpage_items','custpage_so','F');
				//nlapiSetLineItemValue(type, fldnam, linenum, value)
				//alert('Payment is HOLD for few Orders,you cannot process those Orders');
				flag = 'F';
				//nlapiSetLineItemValue('custpage_items', 'custpage_so', m ,'F');
				//unCheckArray.push(s);
			}
			if(HoldStatus == "CLOSED")
			{
				alert('Either Sales Order is Closed');
				return false;
			}
			else if(HoldStatus == "CANCELLED")
			{
				alert('Either Sales Order is Cancelled');
				return false;
			}
			else if(HoldStatus == "HOLD")
			{
				alert('Either Sales Order Payment is HOLD');
				return false;
			}
			else if(HoldStatus == "Days Over Due")
			{
				alert('Either Sales Order Payment is Days Over Due');
				return false;
			}

			if(AcctCreditPrefer == 'WARNING')
			{

				if(HoldStatus == "Credit Limit Exceed")
				{
					alert('Customer Exceeded the Credit Limit ');
					return true;
				}

			}
			if(AcctCreditPrefer == 'HOLD')
			{
				if(HoldStatus == "Credit Limit Exceed")
				{
					alert('Customer Exceeded the Credit Limit');
					return false;
				}
			}
			var vPresetInvQOH=nlapiGetLineItemValue('custpage_items', 'custpage_presentqoh', s); 
			if(vPresetInvQOH == null || vPresetInvQOH == '')
				vPresetInvQOH=0;

			if(parseFloat(vPresetInvQOH) < parseFloat(OrdrQty))
			{
				//alert('Insufficient Inventory.  Please do quantity exception for line# ' + vfolineno + '  on  ' + vsoname + '  and item ' + itemnname + '   Please Uncheck the Order# :');
				alert('Either inventory record has been deleted or Insufficient Inventory  for line# ' + vfolineno + '  on  ' + vsoname + '  and item ' + itemnname + '   Please Uncheck the Order# :');
				//nlapiSetLineItemValue('custpage_items', 'custpage_so', s ,'F');
				return false;
			}

			//}
		}
	}
	//alert('middle' + context.getRemainingUsage());
	//nlapiLogExecution('ERROR','Remaining usage before binloc search binloc of save',context.getRemainingUsage());
	if(binlocidarr!=null && binlocidarr!='')
		{
			var filters = new Array();
			filters.push(new nlobjSearchFilter('internalid', null, 'anyof', binlocidarr));
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'T'));

			var binsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, null);
			if(binsearchresults!=null && binsearchresults!=''){
				alert("The location is Inactive. So you cannot pick the product from this location." );					
				return false;
				
			}
		}
	if(flag == 'F')
	{
		alert('Either Sales Order is Closed/Cancelled or Payment is HOLD/Days Over Due/Credit Limit Exceed for the Orders,Please Uncheck');
		return false;
		for(var k=0;k<unCheckArray.length;k++)
		{

			nlapiSetLineItemValue('custpage_items', 'custpage_so', unCheckArray[k] ,'F');
		}



	}
	if(pickexception=='T')
	{
		alert("Creating Inventory Adjustment(s) for the differential quantity");
		//return false;
	}
	if(AtleastonelinetoSelected=="F")
	{
		alert("Please Select atleast one value(s)");
		return false;
	}
	//nlapiLogExecution('ERROR','Remaining usage at the end of save',context.getRemainingUsage());
	return true;
}


function ValidatePickLine()
{


	var locNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actloc');
	var selectedlocNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actbeginloc');
	var vOrdQty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_ordqty');
	var vItemType = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemtype');
	var vSerialItems = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemserialno');
	var vSerialOut = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialout');
	var vSerialIn = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialin');	
	var vDisplayedQty=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_displyedordqty');
	var vfono=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_dorefno');
	var vfolineno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lineno');
	var vbatchno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itembatchno');
	var wmslocation=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_wmslocation');
	var ItemIdno = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemno');//Case# 20149906
	var hiddenbatchno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itembatchnohidden'); // Case# 201410340
	var vBatchFlag=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_wmsbatchflag');
	//alert(vbatchno);
	//alert(vItemType);
	var date=DateStamp();
	//alert(date);
	
	
	var shipcomplete='N';
	var filtersfo = new Array();
	
	
	if(vfono == null || vfono == '')
	{
		nlapiSetCurrentLineItemValue('custpage_items','custpage_so','F');
		return false;
		
	}
	if(vfono!=null && vfono!='' && vfono!='null')
	filtersfo.push(new nlobjSearchFilter('internalid', null, 'is', vfono));				
	filtersfo.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', vfolineno));

	var columnsfo = new Array();
	columnsfo[0] = new nlobjSearchColumn('custrecord_shipcomplete');

	var searchresultsfo = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersfo, columnsfo);
	if(searchresultsfo!=null)
		shipcomplete=searchresultsfo[0].getValue('custrecord_shipcomplete');
	
	
	
	var linecheck= nlapiGetCurrentLineItemValue('custpage_items','custpage_so');
	
	//alert('shipcomplete:' + shipcomplete);
	//alert('linecheck:' + linecheck);
	
	if(shipcomplete=='T')
	{
		if (linecheck == "F")
		{
			alert('This SO has created with Ship Complete set ON, you can not delete the line');
			nlapiSetCurrentLineItemValue('custpage_items', 'custpage_so','T');
			//return true;
		}
	}


	

	if (linecheck == "T") {	

		//case 20125190 start : restrict Qty exception when Ship complete flag Enable
		if(vDisplayedQty != vOrdQty)
		{		
			

			if(shipcomplete=='T')
			{
				//var result=confirm('This SO has created with Ship Complete set ON, Do you want to perform exception.');
				alert('This SO has created with Ship Complete set ON, you can not change the quantity');
				//if(result==false)
				//{
				nlapiSetCurrentLineItemValue('custpage_items', 'custpage_ordqty',vDisplayedQty);
				return true;
				//}	
			}

		}
		//case 20125190 end






		if(vItemType == "lotnumberedinventoryitem" || vItemType == "lotnumberedassemblyitem" || vBatchFlag == 'T')
		{
			var filtersbatch = new Array();
			var batchexpiry;
			if(vbatchno==null || vbatchno=='')
			{
				alert('Please enter batch#');
				return false;
			}//case# 20126038� start
			/*else
			{
				var res= BatchCustRecordChk(vbatchno);

				if(res==false)
				{
					var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
					window.open(linkURL);
				}
			}*/
			//case# 20126038� end
			//Case# 20149599 starts
			else
			{
				var BatchEntryResults = new Array();

				var filters = new Array();
				if(vbatchno!=null && vbatchno!='' && vbatchno!='null')
					filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', vbatchno));
				//Case# 20149747 starts
				//if(selectedlocNo!=null && selectedlocNo!='' && selectedlocNo!='null')
					//filters.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', selectedlocNo));
				if(wmslocation!=null && wmslocation!='' && wmslocation!='null')
					filters.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', wmslocation));
				//Case# 20149747 ends
				//Case# 20149906 starts
				if(ItemIdno!=null && ItemIdno!='' && ItemIdno!='null')
					filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', ItemIdno));
				//Case# 20149906 ends
				filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebizfifodate');
				columns[1] = new nlobjSearchColumn('custrecord_ebizexpirydate');
				columns[2] = new nlobjSearchColumn('custrecord_ebizlotbatch');
				columns[3] = new nlobjSearchColumn('Internalid');
				BatchEntryResults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
				if(BatchEntryResults =='' || BatchEntryResults ==null || BatchEntryResults =='null')
				{
					alert('Please enter valid batch no');
					return false;
				}
			}
			//Case# 20149599 ends
		}
		if(vItemType=='serializedinventoryitem' || vSerialOut=='T' )
		{
			if(vSerialItems!=null && vSerialItems !="" && locNo==selectedlocNo )
			{
				if(vOrdQty!= null && vOrdQty!="")
				{
					var varr=vSerialItems.split(",");
					if(parseFloat(vOrdQty)!= varr.length)
					{

						alert("Please enter " + vOrdQty + " Serial No(s)");
						var SerialURL;
						var ctx = nlapiGetContext();
						nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
						var linkURL = nlapiResolveURL('SUITELET', 'customscript_pickcnfmserialscan', 'customdeploy_pickcnfmserialscan_di');
						linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemno');					
						//linkURL = linkURL + '&custparam_serialskuname='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemname');//commented because of item containing special characters and this breaks url sequence
						linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_fromlp');
						linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_ordqty');
						linkURL = linkURL + '&custparam_opentaskid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_recid');
						linkURL = linkURL + '&custparam_contLpno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lpno');
						linkURL = linkURL + '&custparam_dointernno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_dono');
						linkURL = linkURL + '&custparam_dolineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lineno');
						linkURL = linkURL + '&custparam_actbeginloc=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_actbeginloc');

						if(vSerialIn=="F")
							linkURL = linkURL + '&custparam_serialout=' +vSerialOut;
						else
							linkURL = linkURL + '&custparam_serialout=F';
						window.open(linkURL);
						return false;
					}
				}
			}
			else if(vDisplayedQty != vOrdQty)
			{
				// start of case no 20126734
//				when we enter qty as 0 system should not ask for serial no's
				if(parseFloat(vOrdQty) > 0)
				{



					alert("Please enter Serial No(s)");
					var SerialURL;
					var ctx = nlapiGetContext();
					nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
					/*if (ctx.getEnvironment() == 'PRODUCTION') {
					SerialURL = 'https://system.netsuite.com';			
				}
				else 
					if (ctx.getEnvironment() == 'SANDBOX') {
						SerialURL = 'https://system.sandbox.netsuite.com';				
					}

				var linkURL = SerialURL + "/app/site/hosting/scriptlet.nl?script=customscript_pickcnfmserialscan&deploy=1";*/
					var linkURL = nlapiResolveURL('SUITELET', 'customscript_pickcnfmserialscan', 'customdeploy_pickcnfmserialscan_di');
					linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemno');					
					//linkURL = linkURL + '&custparam_serialskuname='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemname');//commented because of item containing special characters and this breaks url sequence
					linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_fromlp');
					linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_ordqty');
					linkURL = linkURL + '&custparam_opentaskid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_recid');
					linkURL = linkURL + '&custparam_contLpno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lpno');
					linkURL = linkURL + '&custparam_dointernno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_dono');
					linkURL = linkURL + '&custparam_dolineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lineno');
					linkURL = linkURL + '&custparam_actbeginloc=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_actbeginloc');
					linkURL = linkURL + '&custparam_saleorderid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_salesordinternid');
					if(vSerialIn=="F")
						linkURL = linkURL + '&custparam_serialout=' +vSerialOut;
					else
						linkURL = linkURL + '&custparam_serialout=F';
					window.open(linkURL);
					return false;
				}
				// end of case no 20126734
			}
			else
			{
				if(locNo==selectedlocNo)
				{


					alert("Please enter Serial No(s)");
					var SerialURL;
					var ctx = nlapiGetContext();
					nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
					/*if (ctx.getEnvironment() == 'PRODUCTION') {
				SerialURL = 'https://system.netsuite.com';			
			}
			else 
				if (ctx.getEnvironment() == 'SANDBOX') {
					SerialURL = 'https://system.sandbox.netsuite.com';				
				}

			var linkURL = SerialURL + "/app/site/hosting/scriptlet.nl?script=customscript_pickcnfmserialscan&deploy=1";*/
					var linkURL = nlapiResolveURL('SUITELET', 'customscript_pickcnfmserialscan', 'customdeploy_pickcnfmserialscan_di');
					linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemno');					
					//linkURL = linkURL + '&custparam_serialskuname='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemname');//commented because of item containing special characters and this breaks url sequence
					linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_fromlp');
					linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_ordqty');
					linkURL = linkURL + '&custparam_opentaskid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_recid');
					linkURL = linkURL + '&custparam_contLpno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lpno');
					linkURL = linkURL + '&custparam_dointernno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_dono');
					linkURL = linkURL + '&custparam_dolineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lineno');
					linkURL = linkURL + '&custparam_actbeginloc=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_actbeginloc');
					linkURL = linkURL + '&custparam_saleorderid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_salesordinternid');
					if(vSerialIn=="F")
						linkURL = linkURL + '&custparam_serialout=' +vSerialOut;
					else
						linkURL = linkURL + '&custparam_serialout=F';
					window.open(linkURL);
					return false;

					if (vItemType == "serializedinventoryitem") {
						var ItemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemno');
						var fromlp=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_fromlp');
						var serialno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemserialno');
						var IsvalidSerial=SerialCustRecordChk(ItemId,fromlp,serialno);
						return IsvalidSerial;

					}
				}

			}

		}
		/*	if(searchresultsbatch ==null || searchresultsbatch =='')
			{

				alert('Enter Batch Expiry Date must greater than Current Date');
				return true;
			}*/




		/*	if(vDisplayedQty != vOrdQty)
		{		
			var shipcomplete='N';
			var filtersfo = new Array();

			filtersfo.push(new nlobjSearchFilter('internalid', null, 'is', vfono));				
			filtersfo.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', vfolineno));

			var columnsfo = new Array();
			columnsfo[0] = new nlobjSearchColumn('custrecord_shipcomplete');

			var searchresultsfo = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersfo, columnsfo);
			if(searchresultsfo!=null)
				shipcomplete=searchresultsfo[0].getValue('custrecord_shipcomplete');

			if(shipcomplete=='T')
			{
				//var result=confirm('This SO has created with Ship Complete set ON, Do you want to perform exception.');
				alert('This SO has created with Ship Complete set ON, you can not change the quantity');
				//if(result==false)
				//{
				nlapiSetCurrentLineItemValue('custpage_items', 'custpage_ordqty',vDisplayedQty);
				return true;
				//}	
			}

		}*/

		if((locNo!=selectedlocNo))
		{
			//Case# 20126037�start

			if(selectedlocNo!=null && selectedlocNo!='')
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('internalid', null, 'is', selectedlocNo));
				var columns = new Array();
				columns.push(new nlobjSearchColumn('custrecord_ebizsitelocf'));

				var binsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

				if(binsearchresults !=null && binsearchresults!='')
				{
					var temploc=binsearchresults[0].getValue('custrecord_ebizsitelocf');

					if(temploc!=null && temploc!='' )
					{
						if(parseInt(temploc)!=parseInt(wmslocation))
						{
							alert('This Location Belongs to other site ('+binsearchresults[0].getText('custrecord_ebizsitelocf')+')');
							return false;
						}

						else
						{
							//nlapiSetCurrentLineItemValue('custpage_items', 'custpage_itembatchno','',false,false);						
						}
					}
				}
			}
			//Case# 20126037� End.
		}

		 //Case# 201410340 starts
		//if((locNo!=selectedlocNo))
		if((locNo!=selectedlocNo) || (vbatchno !=hiddenbatchno))//Case# 201410340 ends
		{




			var itemNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itemno');
			var batchNo= nlapiGetCurrentLineItemValue('custpage_items', 'custpage_itembatchno');
			var filtersbatch=new Array();
			var filtersinvt = new Array();
			//Case # 20126486 Start
			var lot=null;
			if(batchNo !=null && batchNo!='')
			{
				filtersbatch.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', vbatchno));

				filtersbatch.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemNo));

				var columnsbatch = new Array();
				columnsbatch[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');

				var searchresultsbatch = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbatch, columnsbatch);
				if(searchresultsbatch!=null && searchresultsbatch!='')
				{
					lot=searchresultsbatch[0].getId();
				}
			}
			if(lot!=null && lot!='')
			{
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', lot));
			}
			//Case # 20126486 End
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemNo));				
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', selectedlocNo));
			filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));
			if(wmslocation!=null && wmslocation!='')
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', wmslocation));

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno');
			columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
			columnsinvt[5].setSort();

			var boolfound=false;
			var vrefno="";var LP ='';
			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');

				var Recid = searchresult.getId();
				vrefno=Recid ;
				if (allocqty==null || isNaN(allocqty)||allocqty == "") {
					allocqty = 0;
				}

				if (parseFloat(actqty) < 0) {
					actqty = 0;
				}
				var remainqty = actqty - allocqty;

				if(remainqty >=vOrdQty)
				{
					if(vItemType=='serializedinventoryitem' || vSerialOut=='T' )
					{
						var filters = new Array();	
						if(itemNo !=null && itemNo!='')
							filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'is', itemNo)); 	

						filters.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP));
						filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [3,19]));


						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');

						var searchresults1 = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						if(searchresults1!=null && searchresults1!='' && searchresults1.length>=parseInt(vOrdQty))
						{
							boolfound=true;
							break;
						}
					}
					else
					{
						boolfound=true;
						break;
					}
				}
			}

			if(!boolfound)
			{
				if(searchresultsinvt!=null && searchresultsinvt!='' && !boolfound)
				{
					//alert("Serial/Lot numbers are not available for this location");
					//return false;
					//case# 20148590 starts
					if(vItemType == "lotnumberedinventoryitem" || vItemType == "lotnumberedassemblyitem" ||vItemType=='serializedinventoryitem' || vSerialOut=='T')
					{
						alert("Serial/Lot numbers are not available for this location");
						return false;
					}
					else
					{
						alert("Insufficient Inventory");
						return false;
					}
					//case# 20148590 ends
				}
				else
				{
					alert("Inventory not found");
					return false;
				}

			}
			else
			{

				var oldLp=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lpno');
				nlapiSetCurrentLineItemValue('custpage_items', 'custpage_invrefno',vrefno);
				nlapiSetCurrentLineItemValue('custpage_items', 'custpage_fromlp',LP);



				if(vItemType=='serializedinventoryitem' || vSerialOut=='T' )
				{
					if(vSerialItems!=null && vSerialItems !="")
					{
						if(vOrdQty!= null && vOrdQty!="")
						{
							var varr=vSerialItems.split(",");
							if((parseFloat(vOrdQty)!= varr.length) || (locNo!=selectedlocNo))
							{

								alert("Please enter " + vOrdQty + " Serial No(s)");
								var SerialURL;
								var ctx = nlapiGetContext();
								nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
								var linkURL = nlapiResolveURL('SUITELET', 'customscript_pickcnfmserialscan', 'customdeploy_pickcnfmserialscan_di');
								linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemno');					
								//linkURL = linkURL + '&custparam_serialskuname='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemname');//commented because of item containing special characters and this breaks url sequence
								linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_fromlp');
								linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_ordqty');
								linkURL = linkURL + '&custparam_opentaskid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_recid');
								linkURL = linkURL + '&custparam_contLpno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lpno');
								linkURL = linkURL + '&custparam_dointernno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_dono');
								linkURL = linkURL + '&custparam_dolineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lineno');
								linkURL = linkURL + '&custparam_actbeginloc=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_actbeginloc');

								if(locNo!=selectedlocNo)
								{
									linkURL = linkURL + '&custparam_invref=' +vrefno;
									linkURL = linkURL + '&custparam_oldLP=' +LP;
									linkURL = linkURL + '&custparam_serials=' +vSerialItems;
								}
								if(vSerialIn=="F")
									linkURL = linkURL + '&custparam_serialout=' +vSerialOut;
								else
									linkURL = linkURL + '&custparam_serialout=F';
								window.open(linkURL);
								return false;
							}
						}
					}
					else
					{
						alert("Please enter Serial No(s)");
						var SerialURL;
						var ctx = nlapiGetContext();
						nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
						/*if (ctx.getEnvironment() == 'PRODUCTION') {
						SerialURL = 'https://system.netsuite.com';			
					}
					else 
						if (ctx.getEnvironment() == 'SANDBOX') {
							SerialURL = 'https://system.sandbox.netsuite.com';				
						}

					var linkURL = SerialURL + "/app/site/hosting/scriptlet.nl?script=customscript_pickcnfmserialscan&deploy=1";*/
						var linkURL = nlapiResolveURL('SUITELET', 'customscript_pickcnfmserialscan', 'customdeploy_pickcnfmserialscan_di');
						linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemno');					
						//linkURL = linkURL + '&custparam_serialskuname='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_itemname');//commented because of item containing special characters and this breaks url sequence
						linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_fromlp');
						linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_ordqty');
						linkURL = linkURL + '&custparam_opentaskid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_recid');
						linkURL = linkURL + '&custparam_contLpno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lpno');
						linkURL = linkURL + '&custparam_dointernno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_dono');
						linkURL = linkURL + '&custparam_dolineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_lineno');
						linkURL = linkURL + '&custparam_actbeginloc=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_actbeginloc');
						linkURL = linkURL + '&custparam_saleorderid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_salesordinternid');

						if(locNo!=selectedlocNo)
						{
							linkURL = linkURL + '&custparam_invref=' +vrefno;
							linkURL = linkURL + '&custparam_oldLP=' +LP;

						}
						if(vSerialIn=="F")
							linkURL = linkURL + '&custparam_serialout=' +vSerialOut;
						else
							linkURL = linkURL + '&custparam_serialout=F';
						window.open(linkURL);
						return false;
					}

				}
				return true;
			}
		}

		//}
		return true; 
	}
	
	else 
		return true;
}


function SerialCustRecordChk(ItemId,fromlp,serialno)
{

	var boolisValid=true;
	if(serialno!=null && serialno!='')
	{
		var serialArray=serialno.split(',');

		for(var k=0;k<serialArray.length;k++)
		{
			try
			{
				var serial=serialArray[k].toString();
				var filterSerialEntry=new Array();
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',ItemId));
				//filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizsono',null,'is',SOid));
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialnumber',null,'is',serial));
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialwmsstatus',null,'anyof',['3','19']));
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',fromlp));

				var SearchRec=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filterSerialEntry,null);
				if(SearchRec!=null&&SearchRec!="")
				{

					boolisValid=true;
				}
				else 
				{
					alert('Please Enter Valid SerialNo');
					boolisValid=false;
					break;
				}
			}
			catch(e)
			{
				var x=e.getDetails();
				var c=0;
			}
		}
	}
	return boolisValid;
}
function PickStrategy(item,avlqty,OldLocationInternalId,whLocation){
	//alert("5");
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('DEBUG','item',item); 

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('item',item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;

	nlapiLogExecution('DEBUG', 'Item', item);
	nlapiLogExecution('DEBUG', 'ItemFamily', ItemFamily);
	nlapiLogExecution('DEBUG', 'ItemGroup', ItemGroup);
	nlapiLogExecution('DEBUG', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('DEBUG', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('DEBUG', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('DEBUG', 'whLocation', whLocation);

	var filters = new Array();	
	filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', ['@NONE@',whLocation]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', ItemFamily]));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', ItemGroup]));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof',['@NONE@', ItemInfo3]));
	}

	var k=1; 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	//columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul').setSort();
	columns[3]=new nlobjSearchColumn('formulanumeric');
	columns[3].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();

	//Fetching pick rule
	nlapiLogExecution('DEBUG','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);
	var vTotRecCount=0;

	if(searchresults != null && searchresults != '')
	{	

		var searchresultszone=getpikzone(-1,whLocation);

		nlapiLogExecution('DEBUG','searchresultszone',searchresultszone.length);	

		var searchresultsinvt=getLocGroup(-1,item,OldLocationInternalId);
		nlapiLogExecution('DEBUG','searchresultsinvt',searchresultsinvt.length);


		for (var i = 0;  i < searchresults.length; i++) {

			var searchresultpick = searchresults[i];				
			var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');			

			//nlapiLogExecution('DEBUG', "LP: " + LP);
			//nlapiLogExecution('DEBUG','PickZone',vpickzone);
			if(vpickzone != null && vpickzone != '')
			{	
				/*var filterszone = new Array();	
				filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', [vpickzone]);
				filterszone[1] = new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',whLocation]);
				filterszone[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

				var columnzone = new Array();
				columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');
				columnzone[1] = new nlobjSearchColumn('custrecord_zone_seq').setSort();				

				var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
				nlapiLogExecution('DEBUG','Loc Group Fetching');
				nlapiLogExecution('DEBUG','searchresultszone1',searchresultszone.length);*/

				for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {
					var pikzone = searchresultszone[j].getValue('custrecordcustrecord_putzoneid');
					//if(vpickzone == searchresultszone[j][1])
					if(vpickzone == pikzone	)
					{
						var searchresultzone = searchresultszone[j];
						var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');
						//var vlocgroupno = searchresultzone[2];
						//nlapiLogExecution('DEBUG','Loc Group Fetching',vlocgroupno);

						/*var filtersinvt = new Array();

						filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
						filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['19']));
						if(OldLocationInternalId != null && OldLocationInternalId != '')
							filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof',OldLocationInternalId));
						filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));				 
						//filtersinvt.push(new nlobjSearchFilter('greaterthanorequalto', 'greaterthanorequalto', avlqty));

						//nlapiLogExecution('DEBUG', "custrecord_ebiz_inv_sku: " + item);

						var columnsinvt = new Array();
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));					
						columnsinvt.push(new nlobjSearchColumn('custrecord_pickseqno'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_qty'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));				 
						columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
						columnsinvt[0].setSort();
						columnsinvt[1].setSort();				
						columnsinvt[2].setSort();
						columnsinvt[3].setSort(false);

						var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);*/
						var vOldLocation='';
						var vOldRec='';
						var vOldLocId='';
						var vTotRemQty=0;
						var vIntCount=0;
						var vOldlot='';
						var vOldlotId='';
						var vlp='';
						for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

							var LocGrp = searchresultsinvt[l].getValue('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc');
							//var LocGrp = searchresultsinvt[l].getValue(columns[9]);

							//nlapiLogExecution('DEBUG','Loc Group Fetching new',LocGrp);
							if(vlocgroupno == LocGrp)
							{
								nlapiLogExecution('DEBUG','entered1',LocGrp);
								var searchresult = searchresultsinvt[l];
								var actqty = searchresult.getValue('custrecord_ebiz_qoh');
								var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
								var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
								var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
								var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
								var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
								var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
								var Recid = searchresult.getId();  

								if (isNaN(allocqty)) {
									allocqty = 0;
								}
								if (allocqty == "") {
									allocqty = 0;
								}
								if( parseFloat(actqty)<0)
								{
									actqty=0;
								}

								var remainqty=0;

								//Added by Ganesh not to allow negative or zero Qtys 
								if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0)
									remainqty = parseFloat(actqty) - parseFloat(allocqty);
								//nlapiLogExecution('DEBUG', 'remainqty:',remainqty); 
								//nlapiLogExecution('DEBUG', 'avlqty:',avlqty); 
								var cnfmqty;


								if (parseFloat(remainqty) >= parseFloat(avlqty)) { 
									//nlapiLogExecution('DEBUG', 'vOldLocation:',vOldLocation);
									//nlapiLogExecution('DEBUG', 'vactLocationtext:',vactLocationtext);
									if(vOldLocation!=vactLocationtext)
									{
										if(vIntCount==0)
										{
											vTotRemQty=remainqty;
											vOldRec=Recid;
											vOldLocation=vactLocationtext;
											vOldLocId=vactLocation;
											vOldlot=vlotText;
											vOldlotId=vlotno;
											//vlp=LP;
											vIntCount=1;
										}
										else
										{
											//nlapiLogExecution('DEBUG', 'vTotRemQty :',vTotRemQty); 
											//nlapiLogExecution('DEBUG', 'vOldLocation:',vOldLocation); 
											//nlapiLogExecution('DEBUG', 'vTotRecCount :',vTotRecCount); 
											var invtarray = new Array();
											////alert('cnfmqty3 ' +cnfmqty);
											invtarray[0]= vTotRemQty; 
											invtarray[1]= vOldLocId; 
											invtarray[2] = vOldRec; 
											invtarray[3] =vOldLocation; 
											invtarray[4] =vOldlot;
											invtarray[5] =vOldlotId;
											invtarray[6] =vlp;
											//nlapiLogExecution('DEBUG', 'RecId:',vOldRec); 
											Resultarray.push(invtarray);
											vTotRecCount=parseFloat(vTotRecCount) +1;
											//if(vTotRecCount ==5)
											//return Resultarray;
											vOldLocation=vactLocationtext;
											vTotRemQty=remainqty;
											vOldLocId=vactLocation;
											vOldRec=Recid;
											vOldlot=vlotText;
											vOldlotId=vlotno;
											//vlp=LP;
										}								

									}
									else if(vOldlot==vlotText)
									{
										vTotRemQty= parseFloat(vTotRemQty) + parseFloat(remainqty);

									}
									else
									{
										var invtarray = new Array();
										////alert('cnfmqty3 ' +cnfmqty);
										invtarray[0]= vTotRemQty; 
										invtarray[1]= vOldLocId; 
										invtarray[2] = vOldRec; 
										invtarray[3] =vOldLocation; 
										invtarray[4] =vOldlot;
										invtarray[5] =vOldlotId;
										//invtarray[6] =vlp;
										//nlapiLogExecution('DEBUG', 'RecId:',vOldRec); 
										Resultarray.push(invtarray);
										vTotRecCount=parseFloat(vTotRecCount) +1;
										//if(vTotRecCount ==5)
										//return Resultarray;
										vOldLocation=vactLocationtext;
										vTotRemQty=remainqty;
										vOldLocId=vactLocation;
										vOldRec=Recid;
										vOldlot=vlotText;
										vOldlotId=vlotno;
										//vlp=LP;
									}
								} 
							}
						} 
						if(vTotRemQty != null && vTotRemQty != '' && vTotRemQty != '0' && vTotRemQty != 0)
						{	nlapiLogExecution('DEBUG','entered2',LocGrp);
						//nlapiLogExecution('DEBUG', 'vTotRemQty in out :',vTotRemQty); 
						//nlapiLogExecution('DEBUG', 'vOldLocation:',vOldLocation); 
						//nlapiLogExecution('DEBUG', 'vTotRecCount in out :',vTotRecCount); 

						var invtarray = new Array();
						////alert('cnfmqty3 ' +cnfmqty);
						invtarray[0]= vTotRemQty; 
						invtarray[1]= vOldLocId; 
						invtarray[2] = vOldRec; 
						invtarray[3] =vOldLocation; 
						invtarray[4] =vOldlot;
						invtarray[5] =vOldlotId;
						//invtarray[6] =vlp;
						//nlapiLogExecution('DEBUG', 'RecId:',vOldRec); 
						Resultarray.push(invtarray);
						vTotRecCount=parseFloat(vTotRecCount) +1;				 
						vOldLocation='';
						vTotRemQty='';
						vOldLocId='';
						vOldRec='';
						vOldlot='';
						vOldlotId='';
						//vlp='';
						}
					}
				}
			}
		} 
	}
	return Resultarray;  
}
var tempPikzoneResultsArray=new Array();
function getpikzone(maxno,whLocation)
{


	var filterszone = new Array();	
	//filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', [vpickzone]);
	filterszone[0] = new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',whLocation]);
	filterszone[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	if(maxno!=-1)
	{
		filterszone[2] = new nlobjSearchFilter('id', null,'lessthan', maxno);
	}


	var columnzone = new Array();

	columnzone[0] = new nlobjSearchColumn('id');
	columnzone[0].setSort(true);
	columnzone[1] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
	columnzone[2] = new nlobjSearchColumn('custrecord_locgroup_no');
	columnzone[3] = new nlobjSearchColumn('custrecord_zone_seq');//.setSort();				

	var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);

	if(searchresults!=null)
	{
		if(searchresults.length>=1000)
		{	
			var maxno1=searchresults[searchresults.length-1].getId();	
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempPikzoneResultsArray.push(searchresults[y]);
			}
			getpikzone(maxno1,whLocation);
		}
		else
		{
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempPikzoneResultsArray.push(searchresults[y]);
			}
		}
	}

	nlapiLogExecution('DEBUG', 'tempPikzoneResultsArray', tempPikzoneResultsArray.length);
	return tempPikzoneResultsArray;


}


var tempLocGrouResultsArray=new Array();
function getLocGroup(maxno,item,OldLocationInternalId)
{

	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['19']));
	if(OldLocationInternalId != null && OldLocationInternalId != '')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof',OldLocationInternalId));
	//filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));				 

	if(maxno!=-1)
	{
		filtersinvt.push(new nlobjSearchFilter('id', null,'lessthan', maxno));
	}
	//nlapiLogExecution('DEBUG', "custrecord_ebiz_inv_sku: " + item);

	var columnsinvt = new Array();

	columnsinvt.push(new nlobjSearchColumn('id'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));					
	columnsinvt.push(new nlobjSearchColumn('custrecord_pickseqno'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_qty'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));				 
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc'));

	columnsinvt[0].setSort(true);
	columnsinvt[1].setSort();
	columnsinvt[2].setSort();				
	columnsinvt[3].setSort();
	columnsinvt[4].setSort(false);
	columnsinvt[5].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);


	if(searchresults!=null)
	{
		if(searchresults.length>=1000)
		{	
			var maxno1=searchresults[searchresults.length-1].getId();	
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempLocGrouResultsArray.push(searchresults[y]);
			}
			getLocGroup(maxno,item,OldLocationInternalId);
		}
		else
		{
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempLocGrouResultsArray.push(searchresults[y]);
			}
		}
	}

	nlapiLogExecution('DEBUG', 'tempLocGrouResultsArray', tempLocGrouResultsArray.length);
	return tempLocGrouResultsArray;


}
function removeDuplicateLocations(arrayName){
	var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < newArray.length; j++) {
			if (newArray[j][3] == arrayName[i][3]) 
				continue label;
		}

		var invtarray = new Array();
		invtarray[0] = arrayName[i][0]; 
		invtarray[1] = arrayName[i][1]; 
		invtarray[2] = arrayName[i][2]; 
		invtarray[3] = arrayName[i][3]; 
		invtarray[4] = arrayName[i][4]; 
		invtarray[5] = arrayName[i][5]; 
		newArray.push(invtarray);
	}
	return newArray;
}
