/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_Replenishment_Item.js,v $
 *     	   $Revision: 1.3.2.12.4.4.4.25.2.4 $
 *     	   $Date: 2015/11/12 15:03:00 $
 *     	   $Author: skreddy $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_118 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Replenishment_Item.js,v $
 * Revision 1.3.2.12.4.4.4.25.2.4  2015/11/12 15:03:00  skreddy
 * case 201415500
 * Boombah SB  issue fix
 *
 * Revision 1.3.2.12.4.4.4.25.2.3  2015/11/10 16:41:27  snimmakayala
 * 201415474
 * 2015,2 Issues
 *
 * Revision 1.3.2.12.4.4.4.25.2.2  2015/11/06 22:25:53  sponnaganti
 * case# 201415474
 * 2015.2 issue fix
 *
 * Revision 1.3.2.12.4.4.4.25.2.1  2015/11/03 15:33:11  grao
 * 2015.2 Issue Fixes 201413874
 *
 * Revision 1.3.2.12.4.4.4.25  2015/08/21 15:34:42  grao
 * 2015.2   issue fixes 201414080
 *
 * Revision 1.3.2.12.4.4.4.24  2015/08/04 14:12:03  grao
 * 2015.2   issue fixes  201413693
 *
 * Revision 1.3.2.12.4.4.4.23  2015/07/03 15:40:45  skreddy
 * Case# 201412907 & 201412906
 * Signwarehouse SB issue fix
 *
 * Revision 1.3.2.12.4.4.4.22  2015/03/04 13:44:24  schepuri
 * issue fix # 201411817
 *
 * Revision 1.3.2.12.4.4.4.21  2015/01/21 14:15:39  nneelam
 * case# 201411343
 *
 * Revision 1.3.2.12.4.4.4.20  2014/11/19 14:53:03  schepuri
 * case#  201411070
 *
 * Revision 1.3.2.12.4.4.4.19  2014/11/12 13:31:20  sponnaganti
 * Case# 201410855
 * TPP SB Issue fix
 *
 * Revision 1.3.2.12.4.4.4.18  2014/09/19 11:53:49  skavuri
 * case # 201410182
 *
 * Revision 1.3.2.12.4.4.4.17  2014/08/13 05:39:16  gkalla
 * case#20149698
 * DCDental unexpected error
 *
 * Revision 1.3.2.12.4.4.4.16  2014/06/13 09:38:43  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.3.2.12.4.4.4.15  2014/06/06 12:18:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.3.2.12.4.4.4.14  2014/05/30 00:34:22  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.2.12.4.4.4.13  2013/12/13 13:59:49  schepuri
 * 20126105
 *
 * Revision 1.3.2.12.4.4.4.12  2013/11/20 20:45:48  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20125767
 * Replen Exceptions
 *
 * Revision 1.3.2.12.4.4.4.11  2013/11/14 15:41:55  skreddy
 * Case# 20125719 & 20125658
 * Afosa SB issue fix
 *
 * Revision 1.3.2.12.4.4.4.10  2013/11/14 14:34:17  rmukkera
 * Case # 20125742
 *
 * Revision 1.3.2.12.4.4.4.9  2013/11/12 06:40:59  skreddy
 * Case# 20125658 & 20125602
 * Afosa SB issue fix
 *
 * Revision 1.3.2.12.4.4.4.8  2013/11/01 13:39:10  rmukkera
 * Case# 20125479
 *
 * Revision 1.3.2.12.4.4.4.7  2013/09/02 15:36:35  rmukkera
 * Case# 20124178, 20124180
 *
 * Revision 1.3.2.12.4.4.4.6  2013/07/18 15:16:43  schepuri
 * case# 20123505
 * Addeed item descrption
 *
 * Revision 1.3.2.12.4.4.4.5  2013/07/08 15:28:28  nneelam
 * Case# 20123303
 * Confirm replanishment through RF  Issue Fix.
 *
 * Revision 1.3.2.12.4.4.4.4  2013/05/14 14:16:37  schepuri
 * RF Replen confirm based on filters
 *
 * Revision 1.3.2.12.4.4.4.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.2.12.4.4.4.2  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.3.2.12.4.4.4.1  2013/03/05 14:46:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.3.2.12.4.4  2012/12/20 07:48:48  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.3.2.12.4.3  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.2.12.4.2  2012/09/26 12:37:08  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.3.2.12.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.3.2.12  2012/08/24 16:09:57  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Replen confirm
 *
 * Revision 1.3.2.11  2012/08/17 22:55:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *  RF Replen
 *
 * Revision 1.3.2.8  2012/07/12 14:47:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.3.2.7  2012/06/04 11:47:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * HUBPEN - REPLEN ISSUE FIXES
 *
 * Revision 1.3.2.3  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.2.2  2012/02/21 13:25:51  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.5  2012/02/21 11:50:37  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */
function ReplenishmentItem(request, response){
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('ERROR', 'GET method started');

		var reportNo = request.getParameter('custparam_repno');
		var beginLoc = request.getParameter('custparam_repbegloc');
		var sku = request.getParameter('custparam_repsku');
		var skuNo = request.getParameter('custparam_repskuno');
		var expQty = request.getParameter('custparam_repexpqty');
		var repInternalId = request.getParameter('custparam_repid');
		var clustNo = request.getParameter('custparam_clustno');
		var whlocation = request.getParameter('custparam_whlocation');
		var actEndLocationId = request.getParameter('custparam_reppicklocno');
		var actEndLocation = request.getParameter('custparam_reppickloc');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var fromlpno=request.getParameter('custparam_fromlpno');
		var tolpno=request.getParameter('custparam_tolpno');
		var nextlocation=request.getParameter('custparam_nextlocation');
		var nextqty=request.getParameter('custparam_nextqty');
		var nextitem=request.getParameter('custparam_nextitem');
		var nextitemno=request.getParameter('custparam_nextitemno');
		var taskpriority = request.getParameter('custparam_entertaskpriority');
		var getNumber = request.getParameter('custparam_number');
		var getItemdescription = request.getParameter('custparam_Itemdescription');


		var getreportNo=request.getParameter('custparam_enterrepno');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');
		var enterfromloc = request.getParameter('custparam_enterfromloc');

		nlapiLogExecution('ERROR', 'actEndLocationId', actEndLocationId);
		nlapiLogExecution('ERROR', 'actEndLocation', actEndLocation);
		nlapiLogExecution('ERROR', 'fromlpno', fromlpno);
		nlapiLogExecution('ERROR', 'nextitem in GET', nextitem);
		nlapiLogExecution('ERROR', 'nextitemno in GET', nextitemno);
		nlapiLogExecution('ERROR', 'taskpriority in GET', taskpriority);
		nlapiLogExecution('ERROR', 'RecordCount in GET', RecordCount);
		nlapiLogExecution('ERROR', 'getNumber in GET', getNumber);
		nlapiLogExecution('ERROR', 'reportNo in GET', reportNo);
		nlapiLogExecution('ERROR', 'repInternalId in GET', repInternalId);
		nlapiLogExecution('ERROR', 'getItemdescription in GET', getItemdescription);

		nlapiLogExecution('ERROR', 'entereditem', getitem);
		nlapiLogExecution('ERROR', 'entereditemid', getitemid);
		nlapiLogExecution('ERROR', 'enteredloc', getloc);
		nlapiLogExecution('ERROR', 'enteredlocid', getlocid);		
		nlapiLogExecution('ERROR', 'taskpriority', taskpriority);

		var html=getItemScan(beginLoc, reportNo, sku, expQty, skuNo, repInternalId, 
				clustNo, whlocation, actEndLocationId, actEndLocation, beginLocId,RecordCount,fromlpno,tolpno,nextlocation,nextqty,nextitem,nextitemno,taskpriority,getNumber
				,replenType,getitem,getitemid,getloc,getlocid,getreportNo,enterfromloc,getItemdescription);        
		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var getEnteredItem = request.getParameter('enteritem');
		nlapiLogExecution('ERROR', 'Entered Item', getEnteredItem);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		var Reparray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		Reparray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);


		var st5,st6,st7;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{

			st5 = "ART&#205;CULO INV&#193;LIDO";
			st6 = "ART&#205;CULO NO SE HAN ENCONTRADO";
			st7 =  "POR FAVOR ENTRAR art�culo v�lido";
		}
		else
		{
			st5 = "INVALID ITEM";
			st6 = "ITEM DID NOT MATCH";
			st7 = "PLEASE ENTER VALID ITEM";

		}



		var reportNo = request.getParameter('hdnNo');
		var hdnBeginLocation = request.getParameter('hdnBeginLocation');
		var clusterNo = request.getParameter('hdnclustno');	
		var skuno=request.getParameter('hdnSkuNo');
		var getItem=request.getParameter('hdngetitemname');
		Reparray["custparam_repno"] = request.getParameter('hdnNo');
		Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');

		Reparray["custparam_repexpqty"] = request.getParameter('hdnExpQty');
		Reparray["custparam_repid"] = request.getParameter('hdnid');
		Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
		Reparray["custparam_clustno"] = clusterNo;	
		Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		Reparray["custparam_reppicklocno"] = request.getParameter('hdnendlocationid');
		Reparray["custparam_reppickloc"] = request.getParameter('hdnendlocation');
		Reparray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
		Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
		Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
		Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
		Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');	
		Reparray["custparam_repsku"] = request.getParameter('hdnSku');	
		Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');
		Reparray["custparam_number"] = request.getParameter('hdngetNumber');
		Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
		Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
		Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
		Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
		Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
		Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
		Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

		//var getitemid = request.getParameter('hdngetitemid');
		var getitemid = request.getParameter('hdnSkuNo')
		var getlocid = request.getParameter('hdngetlocid');
		var taskpriority = request.getParameter('hdntaskpriority');
		var taskpriority = request.getParameter('hdntaskpriority');
		var getreportNo = request.getParameter('hdngetreportno');
		var replenType = request.getParameter('hdnReplenType');
		if(getitemid==null || getitemid=='')
			getitemid=skuno;
		//nlapiLogExecution('ERROR', 'request.getParameter(hdnRecCount) in POST', request.getParameter('hdnRecCount'));
		//nlapiLogExecution('ERROR', 'request.getParameter(hdngetNumber) in POST', request.getParameter('hdngetNumber'));



		nlapiLogExecution('ERROR', 'WH Location', Reparray["custparam_whlocation"]);

		Reparray["custparam_error"] = st5;
		Reparray["custparam_screenno"] = 'R3';

		nlapiLogExecution('ERROR', 'End Location Id', Reparray["custparam_reppicklocno"]);
		nlapiLogExecution('ERROR', 'End Location', Reparray["custparam_reppickloc"]);

		Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
		nlapiLogExecution('ERROR','reportNo', reportNo);

		if(request.getParameter('hdngetNumber')!= null && request.getParameter('hdngetNumber') != "")
			getNumber = request.getParameter('hdngetNumber');
		else
			getNumber=0;	

		nlapiLogExecution('ERROR', 'End Location', Reparray["custparam_reppickloc"]);

		var searchresults =getReplenTasks(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType);
		var searchresultsforconfirm =getReplenTasksconfirm(reportNo,getEnteredItem,getlocid,taskpriority,getreportNo,replenType);

		nlapiLogExecution('ERROR', 'getNumber in post', getNumber);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
		}
		else {

			Reparray["custparam_repno"] = request.getParameter('custparam_repno');
			Reparray["custparam_repbegloc"] = request.getParameter('custparam_repbegloc');
			Reparray["custparam_repexpqty"] = request.getParameter('custparam_repexpqty');
			Reparray["custparam_repid"] = request.getParameter('custparam_repid');
			Reparray["custparam_repskuno"] = request.getParameter('custparam_repskuno');
			Reparray["custparam_clustno"] = request.getParameter('custparam_clustno');;	
			Reparray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
			Reparray["custparam_reppicklocno"] = request.getParameter('custparam_reppicklocno');
			Reparray["custparam_reppickloc"] = request.getParameter('custparam_reppickloc');
			Reparray["custparam_noofrecords"] = request.getParameter('custparam_noofrecords');
			Reparray["custparam_fromlpno"] = request.getParameter('custparam_fromlpno');
			Reparray["custparam_tolpno"] = request.getParameter('custparam_tolpno');
			Reparray["custparam_nextlocation"] = request.getParameter('custparam_nextlocation');
			Reparray["custparam_nextqty"] = request.getParameter('custparam_nextqty');
			Reparray["custparam_nextitem"] = request.getParameter('custparam_nextitem');
			Reparray["custparam_nextitemno"] = request.getParameter('custparam_nextitemno');
			Reparray["custparam_repno"] = request.getParameter('custparam_repno');
			Reparray["custparam_repbegloc"] = request.getParameter('custparam_repbegloc');
			Reparray["custparam_repexpqty"] = request.getParameter('custparam_repexpqty');
			Reparray["custparam_repid"] = request.getParameter('custparam_repid');
			Reparray["custparam_repskuno"] = request.getParameter('custparam_repskuno');
			Reparray["custparam_entertaskpriority"] = request.getParameter('custparam_entertaskpriority');
			Reparray["custparam_enterskuid"] = request.getParameter('custparam_enterskuid');
			Reparray["custparam_entersku"] = request.getParameter('custparam_entersku');
			Reparray["custparam_enterpicklocid"] = request.getParameter('custparam_enterpicklocid');
			Reparray["custparam_enterpickloc"] = request.getParameter('custparam_enterpickloc');
			Reparray["custparam_replentype"] = request.getParameter('custparam_replentype');
			Reparray["custparam_enterrepno"] = request.getParameter('custparam_enterrepno');
			Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');


			nlapiLogExecution('ERROR', 'Reparray["custparam_noofrecords"] in POST', Reparray["custparam_noofrecords"]);

			var systemRule=GetSystemRuleForReplen();
			if (getEnteredItem != '') {
				var actItemid = validateSKUId(getEnteredItem);
				if (actItemid == null) 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					nlapiLogExecution('ERROR', 'SearchResults ', 'Length is null');
				}
				else
				{
					ItemSearchedId=actItemid[1];
					nlapiLogExecution('ERROR', 'Fetched Item ID', ItemSearchedId);
					if(ItemSearchedId!=null && ItemSearchedId!='')
					{
						var ItemType = nlapiLookupField('item', ItemSearchedId, ['recordType','custitem_ebizbatchlot']);
						var ItemRec = nlapiLoadRecord(ItemType.recordType, ItemSearchedId);
						var ItemDescription = ItemRec.getFieldValue('itemid');
						/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
						var Itemdesc=ItemRec.getFieldValue('nameinternal');
						/*** up to here ***/
						nlapiLogExecution('ERROR', 'ItemDescription', ItemDescription);
						nlapiLogExecution('ERROR', 'getItem', getItem);

						Reparray["custparam_repskuno"] = ItemSearchedId;
						nlapiLogExecution('ERROR', 'Reparray["custparam_repskunostart"]1 ', Reparray["custparam_repskuno"]);

						Reparray["custparam_repsku"] = ItemDescription;
					}
					var recNo=0;
					if (searchresults != null && searchresults.length > 0) 
					{
						if(searchresults.length==getNumber)
						{
							getNumber=0;
						}
					}
					if(getNumber!=0)
						recNo=parseFloat(getNumber);
					nlapiLogExecution('ERROR', 'recNo in taskpriority ',recNo);	

					if(getItem==ItemDescription)
					{

						nlapiLogExecution('ERROR', 'ItemType.custitem_ebizbatchlot ',ItemType.custitem_ebizbatchlot);	
						nlapiLogExecution('ERROR', 'ItemType.recordType ',ItemType.recordType);	
						if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
						{
							if (searchresults != null && searchresults.length > 0) 
							{
								nlapiLogExecution('ERROR', 'searchresults.length ', searchresults.length );
								if(systemRule=='Y')
								{
									if(searchresults.length > 1)
									{
										nlapiLogExecution('ERROR', 'recNo ', recNo);
										
										//var SOSearchnextResult = searchresults[parseFloat(recNo) + 1];
										var SOSearchnextResult = searchresults[parseInt(recNo) + 1];//case# 201410855
										if(SOSearchnextResult !=null && SOSearchnextResult!='')
										{
											Reparray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku');
											Reparray["custparam_nextitemno"] = SOSearchnextResult.getValue('custrecord_sku');
											Reparray["custparam_nextlocation"] =SOSearchnextResult.getValue('custrecord_actbeginloc');
											Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty');
											Reparray["custparam_nextlocation"] =SOSearchnextResult.getText('custrecord_actbeginloc');
											Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty');

											nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
											nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
											nlapiLogExecution('ERROR', 'nextlocation', Reparray["custparam_nextlocation"]);
										}
									}
									else
									{
										Reparray["custparam_nextitem"] = '';
										Reparray["custparam_nextitemno"] = '';
										Reparray["custparam_nextqty"] = '';
										Reparray["custparam_nextlocation"] ='';

										nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
										nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
										nlapiLogExecution('ERROR', 'nextlocation', Reparray["custparam_nextlocation"]);
									}

									Reparray["custparam_repbegloc"] = searchresults[recNo].getText('custrecord_actbeginloc');
									Reparray["custparam_repbeglocId"] = searchresults[recNo].getValue('custrecord_actbeginloc');
									Reparray["custparam_repsku"] = searchresults[recNo].getText('custrecord_sku');
									Reparray["custparam_repexpqty"] = searchresults[recNo].getValue('custrecord_expe_qty');
									Reparray["custparam_repskuno"] = searchresults[recNo].getValue('custrecord_ebiz_sku_no');
									Reparray["custparam_repid"] = searchresults[recNo].getId();
									Reparray["custparam_whlocation"] = searchresults[recNo].getValue('custrecord_wms_location');
									Reparray["custparam_reppicklocno"] = searchresults[recNo].getValue('custrecord_actendloc');
									Reparray["custparam_reppickloc"] = searchresults[recNo].getText('custrecord_actendloc');
									Reparray["custparam_fromlpno"] = searchresults[recNo].getValue('custrecord_from_lp_no');
									Reparray["custparam_tolpno"] = searchresults[recNo].getValue('custrecord_lpno');
									Reparray["custparam_repbatchno"] = searchresults[recNo].getValue('custrecord_batch_no');
								}
								else
								{
									nlapiLogExecution('ERROR', 'searchresults.length ', searchresults.length );
									if(searchresults.length > 1)
									{
										var SOSearchnextResult = searchresults[1];
										Reparray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');
										Reparray["custparam_nextitemno"] = SOSearchnextResult.getValue('custrecord_sku',null,'group');
										Reparray["custparam_nextlocation"] =SOSearchnextResult.getValue('custrecord_actbeginloc',null,'group');
										Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty',null,'sum');
										Reparray["custparam_nextlocation"] =SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
										Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty',null,'sum');

										nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
										nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
										nlapiLogExecution('ERROR', 'nextlocation', Reparray["custparam_nextlocation"]);
									}
									else
									{
										Reparray["custparam_nextitem"] = '';
										Reparray["custparam_nextitemno"] = '';
										Reparray["custparam_nextlocation"] = '';
										Reparray["custparam_nextqty"] 

										nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
										nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
										nlapiLogExecution('ERROR', 'nextlocation', Reparray["custparam_nextlocation"]);
									}

									Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc',null,'group');
									Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc',null,'group');
									Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku',null,'group');
									Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty',null,'sum');
									Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no',null,'group');
									Reparray["custparam_whlocation"] = searchresults[0].getValue('custrecord_wms_location',null,'group');
									Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc',null,'group');
									Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc',null,'group');
									Reparray["custparam_repbatchno"] = searchresults[0].getValue('custrecord_batch_no',null,'group');
									nlapiLogExecution('ERROR', 'batch', searchresults[0].getValue('custrecord_batch_no',null,'group') );

								}
							}
							response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);
							return;
						}
						if (searchresults != null && searchresults.length > 0) 
						{
							nlapiLogExecution('ERROR', 'searchresults.length ', searchresults.length );
							nlapiLogExecution('ERROR', 'chkpt ', 'chkpt' );
							if(systemRule=='Y')
							{
								//if(searchresults.length >parseFloat(recNo) + 1)
								if(searchresults.length >parseInt(recNo) + 1)
								{
									//var SOSearchnextResult = searchresults[parseFloat(recNo) + 1];
									var SOSearchnextResult = searchresults[parseInt(recNo) + 1];
									Reparray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku');
									Reparray["custparam_nextitemno"] = SOSearchnextResult.getValue('custrecord_sku');
									Reparray["custparam_nextlocation"] =SOSearchnextResult.getValue('custrecord_actbeginloc');
									Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty');
									Reparray["custparam_nextlocation"] =SOSearchnextResult.getText('custrecord_actbeginloc');
									Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty');

									nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
									nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
									nlapiLogExecution('ERROR', 'nextlocation', Reparray["custparam_nextlocation"]);
								}
								else
								{
									Reparray["custparam_nextitem"] = '';
									Reparray["custparam_nextitemno"] = '';
									Reparray["custparam_nextlocation"] = '';
									Reparray["custparam_nextqty"] = '';

									nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
									nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
									nlapiLogExecution('ERROR', 'nextlocation', Reparray["custparam_nextlocation"]);
								}

								Reparray["custparam_repbegloc"] = searchresults[recNo].getText('custrecord_actbeginloc');
								Reparray["custparam_repbeglocId"] = searchresults[recNo].getValue('custrecord_actbeginloc');
								Reparray["custparam_repsku"] = searchresults[recNo].getText('custrecord_sku');
								Reparray["custparam_repexpqty"] = searchresults[recNo].getValue('custrecord_expe_qty');
								Reparray["custparam_repskuno"] = searchresults[recNo].getValue('custrecord_ebiz_sku_no');
								Reparray["custparam_repid"] = searchresults[recNo].getId();
								Reparray["custparam_whlocation"] = searchresults[recNo].getValue('custrecord_wms_location');
								Reparray["custparam_reppicklocno"] = searchresults[recNo].getValue('custrecord_actendloc');
								Reparray["custparam_reppickloc"] = searchresults[recNo].getText('custrecord_actendloc');
								Reparray["custparam_fromlpno"] = searchresults[recNo].getValue('custrecord_from_lp_no');
								Reparray["custparam_tolpno"] = searchresults[recNo].getValue('custrecord_lpno');

								nlapiLogExecution('ERROR', 'Reparray["custparam_repsku"] before LP screen', Reparray["custparam_repsku"]);
								nlapiLogExecution('ERROR', 'Reparray["custparam_repexpqty"] before LP screen', Reparray["custparam_repexpqty"]);
								nlapiLogExecution('ERROR', 'Reparray["custparam_fromlpno"] before LP screen', Reparray["custparam_fromlpno"]);
								nlapiLogExecution('ERROR', 'Reparray["custparam_tolpno"] before LP screen', Reparray["custparam_tolpno"]);
								nlapiLogExecution('ERROR', 'Reparray["custparam_repid"] before LP screen', Reparray["custparam_repid"]);
								nlapiLogExecution('ERROR', 'Reparray["custparam_reppickloc"] before LP screen', Reparray["custparam_reppickloc"]);
								nlapiLogExecution('ERROR', 'Reparray["custparam_reppicklocno"] before LP screen', Reparray["custparam_reppicklocno"]);
							}
							else
							{
								if(searchresults.length >parseInt(recNo) + 1)
								{
									var SOSearchnextResult = searchresults[parseInt(recNo) + 1];
									Reparray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');
									Reparray["custparam_nextitemno"] = SOSearchnextResult.getValue('custrecord_sku',null,'group');
									Reparray["custparam_nextlocation"] =SOSearchnextResult.getValue('custrecord_actbeginloc',null,'group');
									Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty',null,'sum');
									Reparray["custparam_nextlocation"] =SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
									Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty',null,'sum');
								}

								if(searchresults!=null && searchresults!='' && searchresults.length>0)
								{
									// Temporary fix by Satish.N
									//recNo=0;

									Reparray["custparam_repbegloc"] = searchresults[recNo].getText('custrecord_actbeginloc',null,'group');
									Reparray["custparam_repbeglocId"] = searchresults[recNo].getValue('custrecord_actbeginloc',null,'group');
									Reparray["custparam_repsku"] = searchresults[recNo].getText('custrecord_sku',null,'group');
									Reparray["custparam_repexpqty"] = searchresults[recNo].getValue('custrecord_expe_qty',null,'sum');
									Reparray["custparam_repskuno"] = searchresults[recNo].getValue('custrecord_ebiz_sku_no',null,'group');							
									Reparray["custparam_whlocation"] = searchresults[recNo].getValue('custrecord_wms_location',null,'group');
									Reparray["custparam_reppicklocno"] = searchresults[recNo].getValue('custrecord_actendloc',null,'group');
									Reparray["custparam_reppickloc"] = searchresults[recNo].getText('custrecord_actendloc',null,'group');
									Reparray["custparam_fromlpno"] = searchresults[recNo].getValue('custrecord_lpno',null,'group');
									Reparray["custparam_tolpno"] = searchresults[recNo].getValue('custrecord_lpno',null,'group');
								}
							}
							Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

							response.sendRedirect('SUITELET', 'customscript_replen_lp', 'customdeploy_rf_replen_lp_di', false, Reparray);
						}
						else
						{
							if(searchresultsforconfirm != null && searchresultsforconfirm.length > 0) 
							{
								nlapiLogExecution('ERROR', 'searchresultsforconfirm.length', searchresultsforconfirm.length);
								if(systemRule=='Y')
								{
									if(searchresultsforconfirm.length >parseInt(1))
									{
										var SOSearchnextResult = searchresultsforconfirm[parseInt(recNo) + 1];
										Reparray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku');
										Reparray["custparam_nextitemno"] = SOSearchnextResult.getValue('custrecord_sku');
										Reparray["custparam_nextlocation"] =SOSearchnextResult.getValue('custrecord_actbeginloc');
										Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty');
										Reparray["custparam_nextlocation"] =SOSearchnextResult.getText('custrecord_actbeginloc');
										Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty');

										nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
										nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
										nlapiLogExecution('ERROR', 'nextlocation', Reparray["custparam_nextlocation"]);
									}
									else
									{
										Reparray["custparam_nextitem"] = '';
										Reparray["custparam_nextitemno"] = '';
										Reparray["custparam_nextlocation"] = '';
										Reparray["custparam_nextqty"] = '';

										nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
										nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
										nlapiLogExecution('ERROR', 'nextlocation', Reparray["custparam_nextlocation"]);
									}
									if(searchresults!=null && searchresults!='' && searchresults.length>0)
									{
										Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
										Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc');
										Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku');
										Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
										Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
										Reparray["custparam_repid"] = searchresults[0].getId();
										Reparray["custparam_whlocation"] = searchresults[0].getValue('custrecord_wms_location');
										Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc');
										Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc');
										Reparray["custparam_fromlpno"] = searchresults[0].getValue('custrecord_from_lp_no');
										Reparray["custparam_tolpno"] = searchresults[0].getValue('custrecord_lpno');
									}
								}
								else
								{
									// case # 201411817
									if(searchresults!=null && searchresults!='' && (searchresults.length >parseInt(recNo) + 1))
									{
										var SOSearchnextResult = searchresults[parseInt(recNo) + 1];
										Reparray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');
										Reparray["custparam_nextitemno"] = SOSearchnextResult.getValue('custrecord_sku',null,'group');
										Reparray["custparam_nextlocation"] =SOSearchnextResult.getValue('custrecord_actbeginloc',null,'group');
										Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty',null,'sum');
										Reparray["custparam_nextlocation"] =SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
										Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty',null,'sum');
									}

									if(searchresults!=null && searchresults!='' && searchresults.length>0)
									{
										// Temporary fix by Satish.N
										//recNo=0;

										Reparray["custparam_repbegloc"] = searchresults[recNo].getText('custrecord_actbeginloc',null,'group');
										Reparray["custparam_repbeglocId"] = searchresults[recNo].getValue('custrecord_actbeginloc',null,'group');
										Reparray["custparam_repsku"] = searchresults[recNo].getText('custrecord_sku',null,'group');
										Reparray["custparam_repexpqty"] = searchresults[recNo].getValue('custrecord_expe_qty',null,'sum');
										Reparray["custparam_repskuno"] = searchresults[recNo].getValue('custrecord_ebiz_sku_no',null,'group');							
										Reparray["custparam_whlocation"] = searchresults[recNo].getValue('custrecord_wms_location',null,'group');
										Reparray["custparam_reppicklocno"] = searchresults[recNo].getValue('custrecord_actendloc',null,'group');
										Reparray["custparam_reppickloc"] = searchresults[recNo].getText('custrecord_actendloc',null,'group');
										Reparray["custparam_fromlpno"] = searchresults[recNo].getValue('custrecord_lpno',null,'group');
									}
								}
								response.sendRedirect('SUITELET', 'customscript_replen_lp', 'customdeploy_rf_replen_lp_di', false, Reparray);
							}
							else
							{
								if(searchresultsforconfirm != null && searchresultsforconfirm.length > 0) 
								{
									if(systemRule=='Y')
									{
										//if(searchresultsforconfirm.length >parseFloat(1))
										if(searchresultsforconfirm.length >parseInt(1))
										{
											var SOSearchnextResult = searchresultsforconfirm[1];
											Reparray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku');
											Reparray["custparam_nextitemno"] = SOSearchnextResult.getValue('custrecord_sku');
											Reparray["custparam_nextlocation"] =SOSearchnextResult.getValue('custrecord_actbeginloc');
											Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty');
											Reparray["custparam_nextlocation"] =SOSearchnextResult.getText('custrecord_actbeginloc');
											Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty');
											nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
											nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
										}
										else
										{
											Reparray["custparam_nextitem"] = '';
											Reparray["custparam_nextitemno"] = '';
											Reparray["custparam_nextlocation"] = '';
											Reparray["custparam_nextqty"] = '';

											nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
											nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
											nlapiLogExecution('ERROR', 'nextlocation', Reparray["custparam_nextlocation"]);
										}
										if(searchresultsforconfirm!=null && searchresultsforconfirm!='' && searchresultsforconfirm.length>0)
										{
											Reparray["custparam_repbegloc"] = searchresultsforconfirm[0].getText('custrecord_actbeginloc');
											Reparray["custparam_repbeglocId"] = searchresultsforconfirm[0].getValue('custrecord_actbeginloc');
											Reparray["custparam_repsku"] = searchresultsforconfirm[0].getText('custrecord_sku');
											Reparray["custparam_repexpqty"] = searchresultsforconfirm[0].getValue('custrecord_expe_qty');
											Reparray["custparam_repskuno"] = searchresultsforconfirm[0].getValue('custrecord_ebiz_sku_no');
											Reparray["custparam_repid"] = searchresultsforconfirm[0].getId();
											Reparray["custparam_whlocation"] = searchresultsforconfirm[0].getValue('custrecord_wms_location');
											Reparray["custparam_reppicklocno"] = searchresultsforconfirm[0].getValue('custrecord_actendloc');
											Reparray["custparam_reppickloc"] = searchresultsforconfirm[0].getText('custrecord_actendloc');
											Reparray["custparam_fromlpno"] = searchresultsforconfirm[0].getValue('custrecord_from_lp_no');
											Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');
											Reparray["custparam_tolpno"] = searchresultsforconfirm[0].getValue('custrecord_lpno');
										}
									}
									else
									{
										if(searchresultsforconfirm.length >parseInt(1))
										{
											var SOSearchnextResult = searchresultsforconfirm[1];
											Reparray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');
											Reparray["custparam_nextitemno"] = SOSearchnextResult.getValue('custrecord_sku',null,'group');
											Reparray["custparam_nextlocation"] =SOSearchnextResult.getValue('custrecord_actbeginloc',null,'group');
											Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty',null,'sum');
											Reparray["custparam_nextlocation"] =SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
											Reparray["custparam_nextqty"] = SOSearchnextResult.getValue('custrecord_expe_qty',null,'sum');
											nlapiLogExecution('ERROR', 'nextitem', Reparray["custparam_nextitem"]);
											nlapiLogExecution('ERROR', 'nextitemno', Reparray["custparam_nextitemno"]);
										}

										Reparray["custparam_repbegloc"] = searchresultsforconfirm[0].getText('custrecord_actbeginloc',null,'group');
										Reparray["custparam_repbeglocId"] = searchresultsforconfirm[0].getValue('custrecord_actbeginloc',null,'group');
										Reparray["custparam_repsku"] = searchresultsforconfirm[0].getText('custrecord_sku',null,'group');
										Reparray["custparam_repexpqty"] = searchresultsforconfirm[0].getValue('custrecord_expe_qty',null,'sum');
										Reparray["custparam_repskuno"] = searchresultsforconfirm[0].getValue('custrecord_ebiz_sku_no',null,'group');
										Reparray["custparam_whlocation"] = searchresultsforconfirm[0].getValue('custrecord_wms_location',null,'group');
										Reparray["custparam_reppicklocno"] = searchresultsforconfirm[0].getValue('custrecord_actendloc',null,'group');
										Reparray["custparam_reppickloc"] = searchresultsforconfirm[0].getText('custrecord_actendloc',null,'group');
										Reparray["custparam_repexpqty"] = searchresultsforconfirm[0].getValue('custrecord_expe_qty',null,'sum');
										Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');
										//response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
									}
									response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
								}


							}
						}
						//Case 20123303 start
					}
					//Case 20123303 end
					else
					{
						Reparray["custparam_repskuno"] = skuno;
						Reparray["custparam_repsku"] = getItem;// case no 201410182 
						Reparray["custparam_error"] = st6;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						nlapiLogExecution('ERROR', 'Item is null');
					}
				}

//				}
//				else {
//				Reparray["custparam_error"] = 'ITEM DID NOT MATCH';
//				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
//				nlapiLogExecution('ERROR', 'Item did not Match');
//				}
			}
			else 
			{
				Reparray["custparam_error"] = st7;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				nlapiLogExecution('ERROR', 'Item is null');
			}
		}
	}
}

function getReplenTasks(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('ERROR', 'reportNo',reportNo);

	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);
	nlapiLogExecution('ERROR', 'replenType in replen task',replenType);

	var filters = new Array();

	if(reportNo != null && reportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
	}

	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));
	}

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	//filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));//uncommented by surendra on 12/03/15 (Because of same bin having different items we are updating actual qty field for first item,so at lp screen we are redirecting to item screen and not to get already processed item)

	var systemRule=GetSystemRuleForReplen();
	if(systemRule=='N')
	{
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
		columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
		var fields=new Array();
		nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
		fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
		var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
		if(sresult!=null && sresult.length>0)
			filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', sresult[0].getId()));
		if(getitemid != null && getitemid != '')
		{
			var ItemType = nlapiLookupField('item', getitemid, ['recordType','custitem_ebizbatchlot']);

			nlapiLogExecution('ERROR','ItemType.recordType',ItemType.recordType);
			if(ItemType!=null && ItemType!='')
			{
				if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
				{
					columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
				}
				else if (ItemType.recordType == 'serializedinventoryitem' ) 
				{
					columns[7] = new nlobjSearchColumn('custrecord_lpno',null,'group');
				}
			}
		}
	}
	else
	{
		// case no 20126105
		//filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc');
		columns[7] = new nlobjSearchColumn('custrecord_lpno');
		columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[9] = new nlobjSearchColumn('name');	
		columns[10] = new nlobjSearchColumn('custrecord_taskpriority');
		columns[11] = new nlobjSearchColumn('custrecord_batch_no');
	}
	columns[1].setSort(false);
	columns[2].setSort(false);
	//columns[3].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR', 'searchresults.length ingetreplen',searchresults.length);
	return searchresults;
}
function getReplenTasksconfirm(reportNo,getEnteredItem,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('ERROR', 'Into getReplenTasksconfirm');
	nlapiLogExecution('ERROR', 'reportNo',reportNo);
	nlapiLogExecution('ERROR', 'getEnteredItem',getEnteredItem);
	nlapiLogExecution('ERROR', 'getlocid',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo',getreportNo);
	nlapiLogExecution('ERROR', 'replenType',replenType);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);
	filters[3] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	if(getEnteredItem!=null && getEnteredItem!='')
		filters[4] = new nlobjSearchFilter('custrecord_sku',null, 'is', getEnteredItem);

	var systemRule=GetSystemRuleForReplen();
	var columns = new Array();
	if(systemRule=='N')
	{
		var filters = new Array();

		if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
		{
			nlapiLogExecution('ERROR','name',getreportNo);
			filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

		}

		if(getEnteredItem != null && getEnteredItem != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
		{
			nlapiLogExecution('ERROR','Replenitem',getEnteredItem);
			filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getEnteredItem));

		}

		if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
		{
			nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
			filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

		}

		if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
		{
			nlapiLogExecution('ERROR','Replenpriority',taskpriority);
			filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

		}
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));

		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
		columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
		var fields=new Array();
		nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
		fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
		var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
		if(sresult!=null && sresult.length>0)
		{
			nlapiLogExecution('ERROR','actbeginloc',sresult[0].getId());
			filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', sresult[0].getId()));
		}
		if(getEnteredItem!=null && getEnteredItem!='')
		{

			var itemFilters=new Array();
			itemFilters.push(new nlobjSearchFilter('name', null, 'is',getEnteredItem));
			var itemsresult=nlapiSearchRecord('customrecord_ebiznet_location', null, itemFilters, null);
			nlapiLogExecution('ERROR','itemsresult',itemsresult);
			if(itemsresult!=null && itemsresult.length>0)
			{
				var ItemType = nlapiLookupField('item', itemsresult[0].getId(), ['recordType','custitem_ebizbatchlot']);
				if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
				{
					columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
				}
				else if (ItemType.recordType == 'serializedinventoryitem' ) 
				{
					columns[7] = new nlobjSearchColumn('custrecord_lpno',null,'group');
				}
			}
		}
	}
	else
	{
		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc');
		columns[7] = new nlobjSearchColumn('custrecord_lpno');
		columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[9] = new nlobjSearchColumn('name');	
		columns[10] = new nlobjSearchColumn('custrecord_batch_no');
	}
	columns[1].setSort(false);
	columns[2].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	nlapiLogExecution('ERROR', 'searchresults',searchresults);
	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR', 'searchresults.length ingetreplenconfirm',searchresults.length);
	return searchresults;
}

function getItemScan(beginLoc,reportNo,sku,expQty,skuNo,repInternalId,clustNo,whlocation,actEndLocationId, actEndLocation, beginLocId,
		RecordCount,fromlpno,tolpno,nextlocation,nextqty,nextitem,nextitemno,taskpriority,getNumber,replenType,getitem,getitemid,
		getloc,getlocid,getreportNo,enterfromloc,getItemdescription)
{
	nlapiLogExecution('ERROR', 'nextitem in getItemScan',nextitem);
	nlapiLogExecution('ERROR', 'nextitemno in getItemScan',nextitemno);
//	if(nextitemno!=null && nextitemno!='')
//	{
//	//sku=nextitem;
//	var Itype = nlapiLookupField('item', nextitemno, 'recordType');
//	var ItemRec = nlapiLoadRecord(Itype, nextitemno);		

//	//sku = ItemRec.getFieldValue('itemid');
//	//sku=nextitem;
//	sku=nlapiLookupField('item', nextitemno, 'name')
//	nlapiLogExecution('ERROR', 'sku',sku);
//	}
//	if(nextitemno!=null && nextitemno!='')
//	{
//	skuNo=nextitemno;
//	}

	if(skuNo!=null&&skuNo!="")
	{
		var Itype = nlapiLookupField('item', skuNo, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, skuNo);
		var getItemName = ItemRec.getFieldValue('itemid');
		sku=nlapiLookupField('item', skuNo, 'name');
		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			getItemdescription = ItemRec.getFieldValue('description');
		}
		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
		{	
			getItemdescription = ItemRec.getFieldValue('salesdescription');
		}	
		if(getItemdescription!=null && getItemdescription!='' && getItemdescription!='null' && getItemdescription!='undefined')
		{
			getItemdescription = getItemdescription.substring(0, 100);
		}
		else
		{
			getItemdescription='';
		}
	}
	else
		var getItemName=sku;

	var getLanguage = request.getParameter('custparam_language');
	nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

	var st0,st1,st2,st3,st4,st5;
	//case20125658 and 20125602 start:spanish conversion
	if( getLanguage == 'es_ES' || getLanguage =='es_AR')
	{

		st0 = "REPOSICI&#211;N DE PARTIDA";
		st1 = "ART&#205;CULO";
		st2 = "ENTRAR/ESCANEADO DE &#205;TEMS";			
		st3 = "ENVIAR";
		st4 = "ANTERIOR";
		st5 = "DESCRIPCI&#211;N DE ART&#205;CULO";


	}
	else
	{
		st0 = "REPLENISHMENT ITEM";
		st1 = "ITEM";
		st2 = "ENTER/SCAN ITEM";			
		st3 = "SEND";
		st4 = "PREV";
		st5 = "ITEM DESCRIPTION";


	}		
	//case20125658 and 20125602 end

	var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_item'); 
	var html = "<html><head><title>" + st0 + "</title>";
	html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
	html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";     
	//Case# 20148749 Refresh Functionality starts
	html = html + "var version = navigator.appVersion;";
	html = html + "document.onkeydown = function (e) {";
	html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
	html = html + "if ((version.indexOf('MSIE') != -1)) { ";
	html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
	html = html + "else {if (keycode == 116)return false;}";
	html = html + "};";
	//Case# 20148749 Refresh Functionality ends
	//html = html + " document.getElementById('enteritem').focus();";     

	html = html + "function stopRKey(evt) { ";
	html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
	html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
	html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";

	html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
	html = html + "	  alert('System Processing, Please wait...');";
	html = html + "	  return false;}} ";
	html = html + "	} ";

	html = html + "	document.onkeypress = stopRKey; ";

	html = html + "</script>";
	html = html +functionkeyHtml;
	html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
	html = html + "	<form name='_rf_replenishment_item' method='POST'>";
	html = html + "		<table>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + st1 + ": <label>" + sku + "</label>";
	html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
	html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + beginLoc + ">";
	html = html + "				<input type='hidden' name='hdnSku' value='" + sku + "'>";
	html = html + "				<input type='hidden' name='hdnExpQty' value=" + expQty + ">";
	html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
	html = html + "				<input type='hidden' name='hdnid' value=" + repInternalId + ">";
	html = html + "				<input type='hidden' name='hdnclustno' value=" + clustNo + ">";
	html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
	html = html + "				<input type='hidden' name='hdnendlocationid' value=" + actEndLocationId + ">";
	html = html + "				<input type='hidden' name='hdnendlocation' value=" + actEndLocation + ">";
	html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
	html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
	html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
	html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
	html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
	html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
	html = html + "				<input type='hidden' name='hdngetitemname' value='" + getItemName + "'>";
	html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
	html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
	html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
	html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
	html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
	html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
	html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
	html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
	html = html + "				<input type='hidden' name='hdnenterfromloc' value=" + enterfromloc + ">";
	html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st5+" :  <label>" + getItemdescription + "</label></td>";
	html = html + "				</td>";
	html = html + "			</tr>";

	html = html + "			<tr>";
	html = html + "				<td align = 'left'> " + st2;
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
	html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "		 </table>";
	html = html + "	</form>";
	//Case# 20148882 (added Focus Functionality for Textbox)
	html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
	html = html + "</body>";
	html = html + "</html>";
	return html;
}

//Get Pick Face Location
function getPickFaceLoc(skuNo)
{
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skuNo);

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_pickbinloc');

	var searchresult = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filter, column);
	return searchresult;
}

function updateScannedLoc(recId)
{
	try {
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
		transaction.setFieldValue('custrecord_wms_status_flag', 24);
		var result = nlapiSubmitRecord(transaction);
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed', e);
	}
}

function getIntransitReplenTasks(reportNo)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [24]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_actendloc');    
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;    
}

function GetSystemRuleForReplen()
{
	try
	{
		var rulevalue='Y';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for Replen?'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null && searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);


		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}
