/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_cyclecountresolve_SL.js,v $
 *     	   $Revision: 1.14.2.17.4.16.2.72.2.4 $
 *     	   $Date: 2015/11/16 15:37:32 $
 *     	   $Author: rmukkera $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_cyclecountresolve_SL.js,v $
 * Revision 1.14.2.17.4.16.2.72.2.4  2015/11/16 15:37:32  rmukkera
 * case # 201415113
 *
 * Revision 1.14.2.17.4.16.2.72.2.3  2015/11/09 15:20:07  schepuri
 * case# 201413003
 *
 * Revision 1.14.2.17.4.16.2.72.2.2  2015/11/06 15:24:52  schepuri
 * case# 201415404
 *
 * Revision 1.14.2.17.4.16.2.72.2.1  2015/09/21 14:02:13  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.14.2.17.4.16.2.72  2015/08/06 15:33:14  grao
 * 2015.2   issue fixes  201413655
 *
 * Revision 1.14.2.17.4.16.2.71  2015/07/17 15:27:24  skreddy
 * Case# 201413373
 *  SW SB issue fix
 *
 * Revision 1.14.2.17.4.16.2.70  2015/07/15 15:13:39  grao
 * 2015.2 ssue fixes  201413086
 *
 * Revision 1.14.2.17.4.16.2.69  2015/05/27 15:56:38  nneelam
 * case# 201412920
 *
 * Revision 1.14.2.17.4.16.2.68  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.14.2.17.4.16.2.67  2015/04/10 21:28:10  skreddy
 * Case# 201412323�
 * changed the url path which was hard coded
 *
 * Revision 1.14.2.17.4.16.2.66  2015/01/22 07:34:12  skreddy
 * Case# 201411376
 * True Fab
 *  Prod issue fix
 *
 * Revision 1.14.2.17.4.16.2.65  2014/09/02 10:59:27  gkalla
 * case#20149227
 * ACE and JB cycle count issues
 *
 * Revision 1.14.2.17.4.16.2.64  2014/09/01 16:20:59  gkalla
 * case#201410170
 * ACE Not showing all the records
 *
 * Revision 1.14.2.17.4.16.2.63  2014/08/25 13:46:34  sponnaganti
 * Case# 201410134
 * Stnd Bundle Issue fixed
 *
 * Revision 1.14.2.17.4.16.2.62  2014/08/12 15:33:35  skavuri
 * Case# 20148565 SB Issue Fixed
 *
 * Revision 1.14.2.17.4.16.2.61  2014/08/08 15:11:12  sponnaganti
 * Case# 20149641 20149644
 * Stnd Bundle Issue fix
 *
 * Revision 1.14.2.17.4.16.2.60  2014/07/29 15:03:08  grao
 * Case#: 20149641  , 20149644   and 20149643   New 2014.2 Compatibilty issue fixes
 *
 * Revision 1.14.2.17.4.16.2.59  2014/07/21 15:55:39  sponnaganti
 * Case# 20148647,20148167,20148170
 * Stnd Bundle Issue fix
 *
 * Revision 1.14.2.17.4.16.2.58  2014/07/10 08:01:41  skavuri
 * Case# 20149271, 20126510  and 20148167 Compatibility Issue Fixed
 *
 * Revision 1.14.2.17.4.16.2.57  2014/07/07 16:00:59  sponnaganti
 * case# 20149267
 * Comaptibility Issue fix
 *
 * Revision 1.14.2.17.4.16.2.56  2014/07/07 07:05:57  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149311
 *
 * Revision 1.14.2.17.4.16.2.55  2014/06/20 14:47:25  rmukkera
 * Case # 20127185�,20127488
 *
 * Revision 1.14.2.17.4.16.2.54  2014/03/26 15:43:54  nneelam
 * case#  20127488
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.14.2.17.4.16.2.53  2014/03/26 15:37:03  skavuri
 * Case # 20127831 issue fixed
 * 
 *
 * Revision 1.14.2.17.4.16.2.52  2014/03/21 16:04:51  skavuri
 * Case # 20127753 issue fixed
 *
 * Revision 1.14.2.17.4.16.2.51  2014/03/19 15:58:26  skavuri
 * Case # 20127753 issue fixed
 *
 * Revision 1.14.2.17.4.16.2.50  2014/02/19 06:30:43  nneelam
 * case#  20127185�
 * Standard Bundle Issue Fix.
 *
 * Revision 1.14.2.17.4.16.2.49  2014/02/12 15:16:34  sponnaganti
 * case# 20127131
 * (totalonhandQty is replaced with totalQty)
 *
 * Revision 1.14.2.17.4.16.2.48  2014/02/05 06:57:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20126821
 *
 * Revision 1.14.2.17.4.16.2.47  2014/02/04 16:43:49  skavuri
 * case # 20127037(resolve qty(record actual qty) is taken and set into allocated qty of create inventory table)
 *
 * Revision 1.14.2.17.4.16.2.46  2014/01/06 08:00:26  schepuri
 * 20126236
 *
 * Revision 1.14.2.17.4.16.2.45  2013/12/24 15:24:05  skreddy
 * case#20126530
 * TSG prod issue fix
 *
 * Revision 1.14.2.17.4.16.2.44  2013/12/23 15:08:32  rmukkera
 * Case # 20126539,20126406
 *
 * Revision 1.14.2.17.4.16.2.43  2013/12/20 15:37:01  rmukkera
 * Case # 20126437
 *
 * Revision 1.14.2.17.4.16.2.42  2013/12/19 15:22:55  nneelam
 * case# 20126283
 * Serial no Issue in Resolve Fix.
 *
 * Revision 1.14.2.17.4.16.2.41  2013/12/18 15:24:08  nneelam
 * case# 20126283
 * Std Bundle Issue Fix.
 *
 * Revision 1.14.2.17.4.16.2.40  2013/12/16 14:48:49  rmukkera
 * Case # 20126291,20126107,20126190
 *
 * Revision 1.14.2.17.4.16.2.39  2013/12/12 14:02:52  schepuri
 * 20126110
 *
 * Revision 1.14.2.17.4.16.2.38  2013/12/02 09:00:16  schepuri
 * 20126053
 *
 * Revision 1.14.2.17.4.16.2.37  2013/11/08 16:20:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Epicuren and surftech issue fixes
 *
 * Revision 1.14.2.17.4.16.2.36  2013/10/28 15:43:41  grao
 * Issue fix related to the 20125356
 *
 * Revision 1.14.2.17.4.16.2.35  2013/10/22 15:40:09  rmukkera
 * Case # 20125187
 *
 * Revision 1.14.2.17.4.16.2.34  2013/10/21 15:42:55  nneelam
 * Case# 20124779
 * Qty variance Deciaml fix.
 *
 * Revision 1.14.2.17.4.16.2.33  2013/10/19 13:18:41  rmukkera
 * Case # 20125160�
 *
 * Revision 1.14.2.17.4.16.2.32  2013/10/08 15:48:01  rmukkera
 * Case# 20124813
 *
 * Revision 1.14.2.17.4.16.2.31  2013/09/25 06:53:46  schepuri
 *  eBizNET qty is updated in round values and Netsuite Qty is updated in Decimal Values.
 *
 * Revision 1.14.2.17.4.16.2.30  2013/09/17 15:11:03  rmukkera
 * Case# 20124320�
 *
 * Revision 1.14.2.17.4.16.2.29  2013/09/10 16:02:33  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Afosa changes
 * case#20123755
 *
 * Revision 1.14.2.17.4.16.2.28  2013/09/10 15:49:13  rmukkera
 * Case# 20124320
 *
 * Revision 1.14.2.17.4.16.2.27  2013/09/10 15:32:27  nneelam
 * Case#.20124324��
 * Cycle Count Alert msg  Issue Fix..
 *
 * Revision 1.14.2.17.4.16.2.26  2013/09/05 15:40:23  skreddy
 * Case# 20124256
 * standard bundle issue fix
 *
 * Revision 1.14.2.17.4.16.2.25  2013/08/26 15:51:02  skreddy
 * Case# 20124074
 * standard bundle- issue fix
 *
 * Revision 1.14.2.17.4.16.2.24  2013/08/23 07:26:10  schepuri
 * case no 20123938
 *
 * Revision 1.14.2.17.4.16.2.23  2013/08/21 16:51:42  grao
 * Issue Fix related to
 * 20123967
 *
 * Revision 1.14.2.17.4.16.2.22  2013/08/14 06:29:30  grao
 * TSG SB: Subsidiaries related issue fixes case#:20123891
 *
 * Revision 1.14.2.17.4.16.2.21  2013/08/03 21:16:08  snimmakayala
 * Case# 201214994
 * Cycle Count Issue Fixes
 *
 * Revision 1.14.2.17.4.16.2.20  2013/07/11 00:18:27  grao
 * no message
 *
 * Revision 1.14.2.17.4.16.2.19  2013/07/10 15:03:46  grao
 * GSUSA SB Cycle Count issues fixed Case 20123357,20123358,20123359, 20123360,20123361,20123362
 *
 * Revision 1.14.2.17.4.16.2.18  2013/07/02 15:29:10  nneelam
 * Case# 20123248
 * Cycle count Resolve Issue Fix.
 *
 * Revision 1.14.2.17.4.16.2.17  2013/06/28 15:50:57  spendyala
 * CASE201112/CR201113/LOG2012392
 * Plan no is not able to view in drop down.
 *
 * Revision 1.14.2.17.4.16.2.16  2013/06/19 22:57:21  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of optimistic locking
 *
 * Revision 1.14.2.17.4.16.2.15  2013/06/05 22:11:20  spendyala
 * CASE201112/CR201113/LOG201121
 * FIFO field updating with data stamp is removed.
 *
 * Revision 1.14.2.17.4.16.2.14  2013/06/04 07:48:06  rmukkera
 * Issue Fix For
 * Cycle Count Resolve Screen does not show records when Show All is selected.
 *
 * Revision 1.14.2.17.4.16.2.13  2013/06/03 06:54:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inventory Changes
 *
 * Revision 1.14.2.17.4.16.2.12  2013/05/10 00:33:08  kavitha
 * CASE201112/CR201113/LOG201121
 * Issue fixes
 *
 * Revision 1.14.2.17.4.16.2.11  2013/05/03 01:00:41  kavitha
 * CASE201112/CR201113/LOG201121
 * Issue fixes
 *
 * Revision 1.14.2.17.4.16.2.10  2013/05/01 01:41:15  kavitha
 * CASE201112/CR201113/LOG201121
 * TSG Issue fixes
 *
 * Revision 1.14.2.17.4.16.2.9  2013/04/30 15:57:08  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.14.2.17.4.16.2.8  2013/04/17 15:55:08  skreddy
 * CASE201112/CR201113/LOG201121
 * issue fixes in cycle count process
 *
 * Revision 1.14.2.17.4.16.2.7  2013/04/04 16:03:26  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to update invt to eBn and NS
 *
 * Revision 1.14.2.17.4.16.2.6  2013/04/03 01:54:37  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.14.2.17.4.16.2.5  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.14.2.17.4.16.2.4  2013/03/19 11:46:26  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.14.2.17.4.16.2.3  2013/03/15 09:28:45  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.14.2.17.4.16.2.2  2013/03/06 09:48:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet Files merging for Standard bundle
 *
 * Revision 1.14.2.17.4.16.2.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.14.2.17.4.16  2013/02/21 12:08:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.14.2.17.4.15  2013/02/21 10:38:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.14.2.17.4.14  2013/02/20 22:23:51  kavitha
 * CASE201112/CR201113/LOG201121
 * Cycle count process - Issue fixes
 *
 * Revision 1.14.2.17.4.13  2013/02/19 10:26:56  snimmakayala
 * no message
 *
 * Revision 1.14.2.17.4.12  2013/02/14 01:32:54  kavitha
 * CASE201112/CR201113/LOG201121
 * Cycle count process - Issue fixes
 *
 * Revision 1.14.2.17.4.11  2013/02/11 17:01:22  kavitha
 * CASE201112/CR201113/LOG201121
 * Serial # functionality - Inventory process - Gaps fixes
 *
 * Revision 1.14.2.17.4.10  2013/02/06 03:18:57  kavitha
 * CASE201112/CR201113/LOG201121
 * Serial # functionality - Inventory process
 *
 * Revision 1.14.2.17.4.9  2013/02/01 07:57:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Cyclecountlatestfixed
 *
 * Revision 1.14.2.15  2012/07/31 10:37:13  schepuri
 * CASE201112/CR201113/LOG201121
 * confirming to NS issue
 *
 * Revision 1.14.2.14  2012/07/04 14:07:08  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issues
 *
 * Revision 1.14.2.13  2012/06/28 16:03:06  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Cycle count Resolve files
 *
 * Revision 1.14.2.12  2012/06/28 08:40:19  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.14.2.11  2012/06/13 22:23:28  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.14.2.10  2012/06/11 22:25:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.14.2.9  2012/04/30 11:38:54  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.14.2.8  2012/04/27 15:15:01  spendyala
 * CASE201112/CR201113/LOG201121
 * Paging is added.
 *
 * Revision 1.14.2.7  2012/04/20 13:28:29  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.14.2.6  2012/04/19 15:07:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Blind Cycle Count
 *
 * Revision 1.14.2.5  2012/04/06 14:03:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.14.2.4  2012/03/20 15:22:51  spendyala
 * CASE201112/CR201113/LOG201121
 * Set the cyclecount hold flag to "F" when ever the cycle count plan is resolved or closed.
 *
 * Revision 1.14.2.3  2012/02/11 06:42:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * kitting
 *
 * Revision 1.14.2.2  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.17  2012/02/08 12:03:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.16  2012/02/06 22:19:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * cycle count issue fixes
 *
 * Revision 1.15  2012/02/04 00:05:06  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * putaway form
 *
 * Revision 1.14  2011/11/29 07:01:05  spendyala
 * CASE201112/CR201113/LOG201121
 * added filter criteria to the cycle count drop down.
 *
 * Revision 1.13  2011/11/28 09:19:29  rmukkera
 * CASE201112/CR201113/LOG201121
 * clientscript code moved to suitelet
 *
 * Revision 1.12  2011/11/28 08:50:30  spendyala
 * CASE201112/CR201113/LOG201121
 * created the filter criteria to the dropdown list.
 *
 * Revision 1.11  2011/11/23 14:47:31  spendyala
 * CASE201112/CR201113/LOG201121
 * changed the wms status flag
 *
 *
 *****************************************************************************/
function ebiznet_CycleCountResolve_SL(request, response){

	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Resolve');
//		var selcyclecount = form.addField('custpage_cyclecountplan', 'select', 'Cycle Count Plan#', 'customrecord_ebiznet_cylc_createplan');

		var varCycleCountPlanNo =  form.addField('custpage_cyclecountplan','select', 'Cycle Count Plan#');

		var CycWhLocation =  form.addField('custpage_cyclecountwhlocation','select', 'Location','location');
		var Cycitem =  form.addField('custpage_cyclecountitem','select', 'Item','item');
		var CycbinLoc = form.addField('custpage_cyclecountbinloc', 'select', 'Bin Location','customrecord_ebiznet_location');

		var filtersitem = new Array();

		varCycleCountPlanNo.addSelectOption("","");
		var filtersitem = new Array();
		//filtersitem.push(new nlobjSearchFilter('custrecord_cyclerec_date',null, 'isnotempty', null));
		filtersitem.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [31]));
		var vRoleLocation=getRoledBasedLocation();
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			filtersitem.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
		}

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_cycle_count_plan',null,'group'));
		columns[0].setSort(true);
		var result = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filtersitem,columns);

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];
			var vplan=soresult.getValue('custrecord_cycle_count_plan',null,'group');			
			var res=  form.getField('custpage_cyclecountplan').getSelectOptions(vplan, 'is');
			if (res != null) {				
				if (res.length > 0) {
					continue;
				}
			}		
			varCycleCountPlanNo.addSelectOption(vplan, vplan);
		}
		form.addField('restypeflag', 'radio', 'Show All', 'cyclecountsa').setLayoutType('outsidebelow', 'startcol');
		form.addField('restypeflag', 'radio', 'Show Discrepancy', 'cyclecountsd').setLayoutType('outsidebelow', 'startcol');
		form.addField('restypeflag', 'radio', 'Show Non Discrepancy', 'cyclecountsnd').setLayoutType('outsidebelow', 'startcol');

		form.getField('restypeflag', 'cyclecountsd' ).setDefaultValue( 'cyclecountsd' );

		var flagselected = form.getField('restypeflag').getSelectOptions();

		var buttondisplay = form.addSubmitButton('Display');
		response.writePage(form);
	}
	else {
		var form = nlapiCreateForm('Resolve');

		var vsa = form.addField('restypeflag', 'radio', 'Show All', 'cyclecountsa').setLayoutType('outsidebelow', 'startcol');
		var vsd = form.addField('restypeflag', 'radio', 'Show Discrepancy', 'cyclecountsd').setLayoutType('outsidebelow', 'startcol');
		var vsnd = form.addField('restypeflag', 'radio', 'Show Non Discrepancy', 'cyclecountsnd').setLayoutType('outsidebelow', 'startcol');

		nlapiLogExecution('ERROR', 'request.getParameter(restypeflag) before', request.getParameter('restypeflag'));


		if(request.getParameter('restypeflag') != null && request.getParameter('restypeflag') != '')
		{
			if(request.getParameter('custpage_hiddenfieldshow')!=null && request.getParameter('custpage_hiddenfieldshow')!='')
			{
				if((request.getParameter('restypeflag')=='cyclecountsa') || (request.getParameter('custpage_hiddenfieldshow')=='ALL'))
				{
					form.getField('restypeflag', 'cyclecountsa' ).setDefaultValue( 'cyclecountsa' );
				}
				else if((request.getParameter('restypeflag')=='cyclecountsd') || (request.getParameter('custpage_hiddenfieldshow')=='D'))
				{
					form.getField('restypeflag', 'cyclecountsd' ).setDefaultValue( 'cyclecountsd' );
				}
				else if((request.getParameter('restypeflag')=='cyclecountsnd') || (request.getParameter('custpage_hiddenfieldshow')=='ND'))
				{
					form.getField('restypeflag', 'cyclecountsnd' ).setDefaultValue('cyclecountsnd' );
				}
				else
				{

				}
			}
			else
			{
				if(request.getParameter('restypeflag')=='cyclecountsa')
					form.getField('restypeflag', 'cyclecountsa' ).setDefaultValue( 'cyclecountsa' );

				if(request.getParameter('restypeflag')=='cyclecountsd')
					form.getField('restypeflag', 'cyclecountsd' ).setDefaultValue( 'cyclecountsd' );

				if(request.getParameter('restypeflag')=='cyclecountsnd')
					form.getField('restypeflag', 'cyclecountsnd' ).setDefaultValue('cyclecountsnd' );	
			}

		}

		vsa.setDisplayType('hidden');
		vsd.setDisplayType('hidden');
		vsnd.setDisplayType('hidden');

		nlapiLogExecution('ERROR', 'request.getParameter(restypeflag) after', request.getParameter('restypeflag'));

		var varCCplanNo = request.getParameter("custpage_cyclecountplan");
		var isplanclosed='F';

		form.setScript('customscript_cyclecountresolvesubmit');
		//form.addPageLink('crosslink', 'Go Back', 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=142&deploy=1');
		var linkURL = nlapiResolveURL('SUITELET', 'customscript_cyclecountresolve', 'customdeploy_cyclecountresolve_di');
		form.addPageLink('crosslink', 'Go Back',linkURL);
		try {
			nlapiLogExecution('ERROR', 'Resolve Plan No1', request.getParameter('custpage_cyclecountplan'));
			nlapiLogExecution('ERROR','hdn1',request.getParameter('custpage_hiddenfieldselectpage'));
			if(varCCplanNo!=null && varCCplanNo!='')
			{
				var fields = ['custrecord_cyccplan_close', 'custrecord_ebiz_include_emploc'];
				var columns = nlapiLookupField('customrecord_ebiznet_cylc_createplan', varCCplanNo, fields);
				isplanclosed = columns.custrecord_cyccplan_close;
			}
			nlapiLogExecution('ERROR', 'isplanclosed', isplanclosed);
			if(isplanclosed=='F'){
				if (request.getParameter('custpage_hiddenfieldselectpage') != 'F') {

					var buttonsave = form.addSubmitButton('Save');
					var planno = form.addField('custpage_planno', 'text', 'Cycle Count Plan No:').setDisplayType('disabled');
					planno.setDefaultValue(varCCplanNo);
					var buttonsa = form.addButton('custpage_cyclecountsabtn','Show All','showall()');
					var buttonsd = form.addButton('custpage_cyclecountsdbtn','Show Discrepancy','showdiscrepancy()');
					var buttonsnd = form.addButton('custpage_cyclecountsndbtn','Show Non Discrepancy','shownondiscrepancy()');

					nlapiLogExecution('ERROR', 'Resolve Plan No2', request.getParameter('custpage_cyclecountplan'));

					//form.addField('restype', 'radio', 'Resolve', 'resolve').setLayoutType('endrow').setDefaultValue('resolve');
					form.addField('restype', 'radio', 'Resolve', 'resolve').setLayoutType('outsidebelow', 'startrow');
					form.addField('restype', 'radio', 'Resolve & Close', 'close').setLayoutType('outsidebelow', 'startcol');
					form.addField('restype', 'radio', 'Ignore', 'ignore').setLayoutType('outsidebelow', 'startcol');
					form.addField('restype', 'radio', 'Recount', 'recount').setLayoutType('outsidebelow', 'startcol');

					form.getField('restype', 'resolve' ).setDefaultValue( 'resolve' );
					var hiddenfieldshowvalue;
					var hiddenfieldshow=form.addField('custpage_hiddenfieldshow', 'text', 'hiddenflag1').setDisplayType('hidden');
					//hiddenfieldshow.setDefaultValue('F');
					if(request.getParameter('custpage_hiddenfieldshow') != null && request.getParameter('custpage_hiddenfieldshow') != '')
					{
						hiddenfieldshowvalue = request.getParameter('custpage_hiddenfieldshow');
					}

					hiddenfieldshow.setDefaultValue(hiddenfieldshowvalue);

					nlapiLogExecution('ERROR', 'custpage_hiddenfieldshow', request.getParameter('custpage_hiddenfieldshow'));
					nlapiLogExecution('ERROR', 'hiddenfieldshowvalue', hiddenfieldshowvalue);
					nlapiLogExecution('ERROR', 'hiddenfieldshow', hiddenfieldshow);

					var hiddenCycleCountPlan=form.addField('custpage_cyclecountplan', 'text', 'CycleCountPlan').setDisplayType('hidden');
					hiddenCycleCountPlan.setDefaultValue(request.getParameter('custpage_cyclecountplan'));
					var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
					var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
					hiddenfieldselectpage.setDefaultValue('F');

					var k = form.getField('restype').getSelectOptions();

					var chkconfirmtohost=form.addField('custpage_confirmtohost', 'checkbox','Update GL#').setDefaultValue('T');

					addFieldsToForm(form);

					var orderList = getOrdersForCycleCount(request,0,form);

					if(((request.getParameter('restypeflag')=='cyclecountsa' && hiddenfieldshowvalue != 'D') && (request.getParameter('restypeflag')=='cyclecountsa' && hiddenfieldshowvalue != 'ND'))|| hiddenfieldshowvalue == 'ALL')
					{
						nlapiLogExecution('ERROR', 'orderList.lenght tst in all', orderList.length);
						if(orderList != null && orderList.length > 0){
							setPagingForSublist(orderList,form);
						}
					}
					if((request.getParameter('restypeflag')=='cyclecountsd' || hiddenfieldshowvalue == 'D') || (request.getParameter('restypeflag')=='cyclecountsnd' || hiddenfieldshowvalue == 'ND'))
					{
						nlapiLogExecution('ERROR', 'orderList.lenght tst in D or ND', orderList.length);
						if(orderList != null && orderList.length > 0){
							displaysublist(form,orderList,hiddenfieldshowvalue);
						}
					}
				}
				else {
					nlapiLogExecution('ERROR','hdn2',request.getParameter('custpage_hiddenfieldselectpage'));
					var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
					hiddenfieldselectpage.setDefaultValue('F');

					var hiddenfieldshow=form.addField('custpage_hiddenfieldshow', 'text', 'hiddenflag1').setDisplayType('hidden');

					nlapiLogExecution('ERROR','count1',request.getLineItemCount('custpage_items'));
					var Values=null;
					if(request.getLineItemCount('custpage_items')!=null&&request.getLineItemCount('custpage_items')!="" && request.getLineItemCount('custpage_items')!=-1)
					{
						Values=ebiznet_CycleCoundResolveSave_CS(request);
					}

					nlapiLogExecution('ERROR', 'ELSE', 'ELSEXE--->');

					var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
					if(Values !=null && Values !='') {
						if (Values[0]== false)
						{
							nlapiLogExecution('ERROR', 'nto values[0]', 'done');
							//msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Allocated Qty is greater than Actual qty for LP's:', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
							msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'Allocated Qty is greater than Actual qty "+Values[1]+".', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
							response.writePage(form);
							//return;
						}
					}
					//nlapiLogExecution('ERROR', 'request.getParameter(restype)', request.getParameter('restype'));
					//case # 20124324 Start
					else if (request.getParameter('restype')!=null && request.getParameter('restype') == 'resolve')
					{
						//msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Resolved Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
						nlapiLogExecution('ERROR', 'request.getLineItemCount(custpage_items)', request.getLineItemCount('custpage_items'));
						if(request.getLineItemCount('custpage_items')==-1)
						{
							msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'No records to resolve', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						}
						else
						{			

							msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Resolved Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
						}
					}
					else if(request.getParameter('restype')!=null && request.getParameter('restype') == 'ignore')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Ignored Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else if(request.getParameter('restype')!=null && request.getParameter('restype') == 'recount')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Recounted Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else
					{							

						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Resolved Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");

					}

					//case # 20124324 End.

					/*else if (form.getField == 'resolve')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Resolved Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else if(form.getField == 'ignore')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Ignored Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else if(form.getField == 'recount')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Ignored Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Resolved Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}*/
				}
			}
			else
			{
				showInlineMessage(form, 'Confirmation', 'Plan# :  ' + varCCplanNo + ' is Closed ' , '');
			}
		} 
		catch (e) {
			nlapiLogExecution('ERROR', 'Catch', e);
		}

		response.writePage(form);
		//nlapiLogExecution('ERROR', 'after page submit', ' --->');

	}
}
function ebiznet_CycleCoundResolveSave_CS(request){

	nlapiLogExecution('ERROR','count',request.getLineItemCount('custpage_items'));
	nlapiLogExecution('ERROR','resttype1',nlapiGetFieldValue('restype'));
	nlapiLogExecution('ERROR','resttype2',request.getParameter('restype'));

	if (request.getParameter('restype') == 'ignore' || request.getParameter('restype') == 'resolve'|| request.getParameter('restype') == 'close')
	{
		var TempArrVal = new Array(); 
		nlapiLogExecution('ERROR', 'into checking', 'done');	
		var tempmsg='';
		for (var i = 0; i < request.getLineItemCount('custpage_items'); i++) {
			nlapiLogExecution('ERROR', 'into checking1', 'done');	
			if (request.getLineItemValue('custpage_items', 'custpage_rec', i + 1) == 'T') {	
				nlapiLogExecution('ERROR', 'into checking2', 'done');	
				if (request.getParameter('restype') == 'resolve'|| request.getParameter('restype') == 'close') {
					nlapiLogExecution('ERROR', 'into checking3', tempmsg);	
					nlapiLogExecution('ERROR', 'into checking4', request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1));	
					try
					{
						//if(request.getLineItemValue('custpage_items', 'custpage_invtid') !=null && request.getLineItemValue('custpage_items', 'custpage_invtid') !='' && request.getLineItemValue('custpage_items', 'custpage_invtid')!='null' && request.getLineItemValue('custpage_items', 'custpage_invtid') !='undefined')
						if(request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1) !=null && request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1) !='' && request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1)!='null' && request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1) !='undefined')
						{
							//Case # 20148565 starts
							//var transaction1 = nlapiLoadRecord('customrecord_ebiznet_createinv', request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1));
							var invref1 = request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1);
							nlapiLogExecution('ERROR', 'invref1 is 1', invref1);	
							var transaction1 = nlapiLoadRecord('customrecord_ebiznet_createinv',invref1);
							//Case # 20148565 ends
							// Case#20127831 starts
							transaction1.setFieldValue('custrecord_ebiz_invholdflg', 'F');
							transaction1.setFieldValue('custrecord_ebiz_cycl_count_hldflag', 'F');
							nlapiSubmitRecord(transaction1, false, true);
							// Case#20127831 ends
							var allocQty1 = transaction1.getFieldValue('custrecord_ebiz_alloc_qty');
							var quanty1=request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);

							if(parseFloat(quanty1) < parseFloat(allocQty1))
							{
								nlapiLogExecution('ERROR', 'into tempmsg', tempmsg);	
								tempmsg=request.getLineItemValue('custpage_items', 'custpage_actlp', i + 1)+ ","+ tempmsg;						
							}
						}
					}
					catch (e) {
						nlapiLogExecution('ERROR', 'This record doesnt exists so cannot load the record', e);

					}
				}
			}
		}
		if( tempmsg !=null && tempmsg !='')
		{
			var flag=false;
			TempArrVal.push(flag);
			TempArrVal.push(tempmsg);
			return TempArrVal;
		}
	}	

	for (var i = 0; i < request.getLineItemCount('custpage_items'); i++) {

		if (request.getLineItemValue('custpage_items', 'custpage_rec', i + 1) == 'T') {

			var fieldNames = new Array(); 
			var newValues = new Array(); 

			var vcycrecid=request.getLineItemValue('custpage_items', 'custpage_id', i + 1);

			fieldNames.push('custrecord_cyclerec_upd_date'); 
			newValues.push(DateStamp());
			fieldNames.push('custrecord_cyclerec_upd_time'); 
			newValues.push(TimeStamp());
			fieldNames.push('custrecord_cycleupd_user_no'); 
			newValues.push(nlapiGetContext().getUser());

			if (request.getParameter('restype') == 'recount') {


				fieldNames.push('custrecord_cycleact_sku'); 
				newValues.push('');
				fieldNames.push('custrecord_cyclecount_act_ebiz_sku_no'); 
				newValues.push('');
				fieldNames.push('custrecord_cycle_act_qty'); 
				newValues.push('');
				fieldNames.push('custrecord_cycact_lpno'); 
				newValues.push('');
				fieldNames.push('custrecord_cycleabatch_no'); 
				newValues.push('');
				fieldNames.push('custrecord_cyclesku_status'); 
				newValues.push('');
			}
			if (request.getParameter('restype') == 'recount') {

				fieldNames.push('custrecord_cyclestatus_flag'); 
				newValues.push('20');
			}
			else 
				if (request.getParameter('restype') == 'ignore') {
					fieldNames.push('custrecord_cyclestatus_flag'); 
					newValues.push('21');
				}
				else 
					if (request.getParameter('restype') == 'resolve') {
						fieldNames.push('custrecord_cyclestatus_flag'); 
						newValues.push('22');
					}
					else {
						fieldNames.push('custrecord_cyclestatus_flag'); 
						newValues.push('22');
					}

			fieldNames.push('custrecord_cyccplan_resolvedate'); 
			newValues.push(DateStamp());
			fieldNames.push('custrecord_cyccplan_resolvetime'); 
			newValues.push(TimeStamp());
			fieldNames.push('custrecord_cyccplan_resolvedby'); 
			newValues.push(nlapiGetContext().getUser());

			nlapiSubmitField('customrecord_ebiznet_cyclecountexe', vcycrecid, fieldNames, newValues);	

			var invtrecid=request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1);
			nlapiLogExecution("ERROR","invtrecid",invtrecid);
			if(invtrecid!=null&&invtrecid!=""&&invtrecid!="null")
			{
				var scount=1;
				LABL1: for(var j=0;j<scount;j++)
				{	
					nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', j);
					try
					{
						var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtrecid);
						transaction.setFieldValue('custrecord_ebiz_invholdflg', "F");
						transaction.setFieldValue('custrecord_ebiz_cycl_count_hldflag', "F");						

						nlapiSubmitRecord(transaction, false, true);
						nlapiLogExecution('ERROR', 'Cyclecount generated for inv ref ',invtrecid);

					}
					catch(ex)
					{
						var exCode='CUSTOM_RECORD_COLLISION'; 

						if (ex instanceof nlobjError) 
						{	
							wmsE=ex.getCode() + '\n' + ex.getDetails();
							exCode=ex.getCode();
						}
						else
						{
							wmsE=ex.toString();
							exCode=ex.toString();
						}				 
						if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
						{  
							scount=scount+1;
							continue LABL1;
						}
						else break LABL1;
					}
				}
			}
		}
	}
	nlapiLogExecution('ERROR', 'before loop', 'done');
	if (request.getParameter('restype') == 'ignore' || request.getParameter('restype') == 'resolve'|| request.getParameter('restype') == 'close' || request.getParameter('restype') == 'recount') {
		nlapiLogExecution('ERROR', 'entered into process', 'done');
		for (var i = 0; i < request.getLineItemCount('custpage_items'); i++) {
			if (request.getLineItemValue('custpage_items', 'custpage_rec', i + 1) == 'T') {
				var accountno="";
				var sitelocn="";
				var resultQty=0;
				var resultQty1 = 0;
				var memo="";
				var NSAdjustid='';
				var NSAdjustid1='';
				var CyccRecID = request.getLineItemValue('custpage_items', 'custpage_id', i + 1);
				var CyccRecord = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe',CyccRecID); // 2 UNITS
				var expserialno = CyccRecord.getFieldValue('custrecord_cycle_expserialno');
				var actserialno = CyccRecord.getFieldValue('custrecord_cycle_serialno');
				var vItemId=request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1);
				var vExpItemId=request.getLineItemValue('custpage_items', 'custpage_expsku', i + 1);

				var isfromaddNewItem=CyccRecord.getFieldValue('custrecord_cycle_notes');
				nlapiLogExecution('ERROR', 'isfromaddNewItem', isfromaddNewItem);

				if (request.getParameter('restype') == 'resolve'|| request.getParameter('restype') == 'close') {
					try {
						var vWHLoc;
						var vTaskType='7';
						var vAdjusttype;
						vWHLoc=request.getLineItemValue('custpage_items', 'custpage_site', i + 1);
						var qtyonhand;
						var opentaskid = request.getLineItemValue('custpage_items', 'custpage_opentaskid', i + 1);

						var AccountNo = getStockAdjustmentAccountNoNew(vWHLoc,vTaskType,vAdjusttype);

						if(AccountNo !=null && AccountNo != "" && AccountNo.length>0)
						{
							var CreatePlanRecord = nlapiLoadRecord('customrecord_ebiznet_cylc_createplan',request.getLineItemValue('custpage_items', 'custpage_planno', i + 1)); // 2 UNITS
							var Cycexeplanid= (request.getLineItemValue('custpage_items', 'custpage_planno', i + 1));
							var vEmptyBinLoc = CreatePlanRecord.getFieldValue('custrecord_ebiz_include_emploc');
							nlapiLogExecution('ERROR', 'vEmptyBinLoc', vEmptyBinLoc);
							nlapiLogExecution('ERROR', 'Cycexeplanid', Cycexeplanid);
							if(isfromaddNewItem!=null && isfromaddNewItem=='fromaddnewitem')
							{
								vEmptyBinLoc='T';
							}
							var filterscycexec = new Array();
							filterscycexec.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [22]));
							filterscycexec.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',Cycexeplanid));
							filterscycexec.push(new nlobjSearchFilter('custrecord_opentaskid', null, 'equalto',opentaskid));
							var vRoleLocation=getRoledBasedLocation();
							if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
							{
								filterscycexec.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
							}

							var columns = new Array();
							columns.push(new nlobjSearchColumn('custrecord_cycle_count_plan'));
							columns.push(new nlobjSearchColumn('custrecord_cycleact_sku'));
							columns.push(new nlobjSearchColumn('custrecord_cycle_act_qty'));
							columns.push(new nlobjSearchColumn('custrecord_cycact_lpno'));
							columns.push(new nlobjSearchColumn('custrecord_cyclecount_act_ebiz_sku_no'));
							columns.push(new nlobjSearchColumn('custrecord_cyclestatus_flag'));
							columns.push(new nlobjSearchColumn('custrecord_cycact_beg_loc'));
							columns.push(new nlobjSearchColumn('custrecord_expcyclesku_status'));
							columns.push(new nlobjSearchColumn('custrecord_cyclepc'));
							columns.push(new nlobjSearchColumn('custrecord_cyclesite_id'));
							columns.push(new nlobjSearchColumn('custrecord_cyclesku_status'));
							columns.push(new nlobjSearchColumn('custrecord_invtid'));
							columns.push(new nlobjSearchColumn('custrecord_cycle_skudesc'));
							var CycleCountSearchResult = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filterscycexec,columns);
							nlapiLogExecution('ERROR', 'cyclecountexecutionresult', CycleCountSearchResult);

							if(vEmptyBinLoc=='T' && CycleCountSearchResult !=null && CycleCountSearchResult.length>0 && (CycleCountSearchResult[0].getValue('custrecord_invtid') == null || CycleCountSearchResult[0].getValue('custrecord_invtid') == ""))
							{
								var expvarianceqty='';
								var actualvarianceqty='';
								var varianceinActserialNoNew='';

								var varianceinExpserialNo = '';
								var varianceinActserialNo = '';


								var actualvarianceqtyNew=0;
								var expvarianceqtynotforNS=0;
								var expserialnoToNS ='';
								var expserialnoNotforNS ='';
								var actnewserialnoToNS ='';

								nlapiLogExecution('ERROR', 'cyclecountexecutionresult.length', CycleCountSearchResult.length);
								nlapiLogExecution('ERROR', 'actserialno in empty bin loc', actserialno);
								if(actserialno != null && actserialno != ''&& actserialno != 'undefined')// case# 201415404
								{
									actserialno = actserialno.trim();
									var getactserialArr = actserialno.split(',');
									if(expserialno != null && expserialno != '')
									{
										expserialno = expserialno.trim();
										var getexpserialArr = expserialno.split(',');
									}

									for (var q = 0; q < getactserialArr.length; q++) 
									{
										if(getexpserialArr != null && getexpserialArr != '')
										{
											if(getexpserialArr.indexOf(getactserialArr[q])==-1)//not found temSeriIdArr
											{
												if (varianceinActserialNo == null || varianceinActserialNo == '') {
													varianceinActserialNo= getactserialArr[q];

												}
												else {
													varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

												}
											}
										}
										else
										{
											if (varianceinActserialNo == null || varianceinActserialNo == '') {
												varianceinActserialNo= getactserialArr[q];

											}
											else {
												varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

											}
										}
										varianceinActserialNo = getactserialArr[q];
										nlapiLogExecution('ERROR', 'getactserialArr[q]', getactserialArr[q]);
										var filters = new Array();
										filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getactserialArr[q]);

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_serialparentid');
										var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

										if(searchResults != null && searchResults != "" && searchResults.length == 1)
										{

											if (varianceinActserialNoNew == "") {
												varianceinActserialNoNew= getactserialArr[q];

											}
											else {
												varianceinActserialNoNew = varianceinActserialNoNew + "," + getactserialArr[q];

											}
										}
									}

									nlapiLogExecution('ERROR', 'vItemId ', vItemId);
									nlapiLogExecution('ERROR', 'vWHLoc ', vWHLoc);

									var totalNSserialno = '';

									var filters = new Array();
									filters.push(new nlobjSearchFilter('item',null,'anyof', vItemId));
									filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', vWHLoc);

									var columns = new Array();
									columns[0] = new nlobjSearchColumn('inventorynumber');
									columns[1] = new nlobjSearchColumn('internalid').setSort();

									var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);

									for (var i2 = 0; searchresultsitem != null && i2 < searchresultsitem.length; i2++) 
									{
										if(totalNSserialno == null || totalNSserialno == '')
											totalNSserialno = searchresultsitem[i2].getValue('inventorynumber');
										else
											totalNSserialno = totalNSserialno + ',' + searchresultsitem[i2].getValue('inventorynumber');

									}

									var vtotNSSerialARR = new Array();
									if(totalNSserialno != null && totalNSserialno  != '')
										vtotNSSerialARR = totalNSserialno.split(',');

									nlapiLogExecution('ERROR', 'vtotNSSerialARR ', vtotNSSerialARR);


									if(varianceinActserialNoNew != null && varianceinActserialNoNew != '')
									{
										nlapiLogExecution('ERROR', 'varianceinActserialNoNew inside if', varianceinActserialNoNew);	
										var vMissedActSerialARR = varianceinActserialNoNew.split(',');
										nlapiLogExecution('ERROR', 'vMissedActSerialARR ', vMissedActSerialARR);

										for(m=0;m<vMissedActSerialARR.length;m++)
										{
											if(vtotNSSerialARR.indexOf(vMissedActSerialARR[m])==-1)// found temSeriIdArr

											{
												if(actnewserialnoToNS ==null || actnewserialnoToNS =='')
													actnewserialnoToNS = vMissedActSerialARR[m];
												else
													actnewserialnoToNS = actnewserialnoToNS + ',' +vMissedActSerialARR[m];
											}
										}
									}

									if(varianceinActserialNo != null && varianceinActserialNo != '')
										actualvarianceqty =varianceinActserialNo.split(',').length;

									if(actnewserialnoToNS != null && actnewserialnoToNS != '')
										actualvarianceqtyNew =actnewserialnoToNS.split(',').length;

									nlapiLogExecution('ERROR', 'actualvarianceqty ', actualvarianceqty);	
									nlapiLogExecution('ERROR', 'actualvarianceqtyNew ', actualvarianceqtyNew);										
									nlapiLogExecution('ERROR', 'varianceinActserialNo ', varianceinActserialNo);	
									nlapiLogExecution('ERROR', 'actualvarianceqtyNew ', actualvarianceqtyNew);									
									nlapiLogExecution('ERROR', 'actnewserialnoToNS ', actnewserialnoToNS);	

								}

								nlapiLogExecution('ERROR', 'cyclecountexecutionresultlength', CycleCountSearchResult.length);
								var vStatusFlag=CycleCountSearchResult[0].getValue('custrecord_cyclestatus_flag');
								nlapiLogExecution('ERROR', 'vStatusFlag',vStatusFlag );
								if(vStatusFlag!=null && vStatusFlag==22)
								{
									var LotBatchId='';
									var LotBatchText=request.getLineItemValue('custpage_items', 'custpage_actbatch', i + 1);
									var vtempactualItem=request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1);
									nlapiLogExecution('ERROR', 'LotBatchText is 1',LotBatchText ); // 
									if(LotBatchText!=null && LotBatchText!='')
										LotBatchId=getLotBatchId(LotBatchText,vtempactualItem);

									/*if(LotBatchText!=null && LotBatchText!='')
										LotBatchId=getLotBatchId(LotBatchText);*/
									var ItemStatus = "";

									if(CycleCountSearchResult[0].getValue('custrecord_cyclesku_status')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclesku_status') != "")
									{
										ItemStatus = CycleCountSearchResult[0].getValue('custrecord_cyclesku_status').toString();
									}
									else if(CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status')!= null && CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status') != "")
									{
										ItemStatus = CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status').toString();
									}
									else
									{
										var vRoleLocation=getRoledBasedLocation();
										nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
										var itemStatusFilters = new Array();
										itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
										itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
										itemStatusFilters[2] = new nlobjSearchFilter('custrecord_defaultskustaus',null, 'is','T');
										if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
										{
											//Case # 20126438 Start
											itemStatusFilters[3] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',vRoleLocation);
											//Case # 20126438 End
										}

										var itemStatusColumns = new Array();			
										itemStatusColumns[0] = new nlobjSearchColumn('name');

										var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);			
										if(itemStatusSearchResult != null && itemStatusSearchResult != '')
										{
											ItemStatus = itemStatusSearchResult[0].getId();
										}
										nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
									}

									var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
									var columns = nlapiLookupField('item', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no'), fields);
									//nlapiLogExecution('ERROR', 'Time Stamp at the end of Item Lookup',TimeStampinSec());
									var ItemType = columns.recordType;	
									var serialInflg="F";		
									serialInflg = columns.custitem_ebizserialin;
									var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
									nlapiLogExecution('ERROR', 'Creating INVT Record ', 'INVT');
									var loc=CycleCountSearchResult[0].getValue('custrecord_cyclesite_id');
									var getsysItemLP = GetMaxLPNo(1, 1,loc);
									if(Cycexeplanid!= null && Cycexeplanid != "")
										invtRec.setFieldValue('name', Cycexeplanid);
									nlapiLogExecution('ERROR', 'Cycexeplanid',Cycexeplanid );
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_binloc', CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc'));
									nlapiLogExecution('ERROR', 'actbeginloc',CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc') );
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_lpno')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_lpno') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_lp', CycleCountSearchResult[0].getValue('custrecord_cycact_lpno'));
									else
										invtRec.setFieldValue('custrecord_ebiz_inv_lp', getsysItemLP);
									nlapiLogExecution('ERROR', 'invlp', CycleCountSearchResult[0].getValue('custrecord_cycact_lpno'));
									if(CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_sku',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									nlapiLogExecution('ERROR', 'sku_no',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatus);
									nlapiLogExecution('ERROR', 'sku_status',CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status'));
									if(CycleCountSearchResult[0].getValue('custrecord_cyclepc')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclepc') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_packcode', CycleCountSearchResult[0].getValue('custrecord_cyclepc'));
									nlapiLogExecution('ERROR', 'packcode',CycleCountSearchResult[0].getValue('custrecord_cyclepc') );
									if(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty')!= null && CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty') != "")
									{
										var qty=parseFloat(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty'));
										invtRec.setFieldValue('custrecord_ebiz_qoh', qty);
										invtRec.setFieldValue('custrecord_ebiz_inv_qty', qty.toString());
										nlapiLogExecution('ERROR', 'actqty',qty);
									}
									if(CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') != "")
										invtRec.setFieldValue('custrecord_inv_ebizsku_no', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no'));
									//
									nlapiLogExecution('ERROR', 'sku',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									// Case# 20127753 starts
									if(CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc')!= null && CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc') != "")
										//Case# 20127753 ends
										invtRec.setFieldValue('custrecord_ebiz_itemdesc', CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc'));
									nlapiLogExecution('ERROR', 'itemdesc',CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc') );
									invtRec.setFieldValue('custrecord_invttasktype', 2);
									invtRec.setFieldValue('custrecord_wms_inv_status_flag', ['19']); //Inventory Storage
									if(CycleCountSearchResult[0].getValue('custrecord_cyclesite_id')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclesite_id') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_loc',CycleCountSearchResult[0].getValue('custrecord_cyclesite_id').toString());
									invtRec.setFieldValue('custrecord_ebiz_callinv','N');
									invtRec.setFieldValue('custrecord_ebiz_displayfield','N');
									if(LotBatchId!=null&&LotBatchId!="")
									{
										invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatchId);	
										var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',LotBatchId,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
										var fifodate=rec.custrecord_ebizfifodate;
										var expdate=rec.custrecord_ebizexpirydate;
										if(fifodate!=null && fifodate!='')
											invtRec.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);

										invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
									}
									else
									{
										if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
										{ 										
											var itemid=CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');
											nlapiLogExecution('ERROR', 'itemid', itemid);
											var Batchfilters = new Array();
											Batchfilters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',itemid ));
											var Batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, Batchfilters);
											if(Batchsearchresults!=null)
											{
												var	getlotnoid= Batchsearchresults[0].getId();
												nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
												invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
											}	
										}
									}

									var LP = "";
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_lpno')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_lpno') != "")
									{
										LP = CycleCountSearchResult[0].getValue('custrecord_cycact_lpno');
									}									

									var serialnumbers = "";
									var localSerialNoArray = new Array();
									//Case # 20127488 start
									var templot="";
									//Case # 20127488 end
									if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
									{
										var sku = CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');
										var qty=parseFloat(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty'));
										//localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,parseFloat(qty),sku,LP,request.getParameter('restype'),CyccRecID);
										AdjustSerialNumbers(localSerialNoArray,parseFloat(qty),sku,LP,request.getParameter('restype'),CyccRecID,'');
										//Case # 20127488 start
										templot=actnewserialnoToNS;
										//Case # 20127488 end
									}
									if(AccountNo!= null && AccountNo != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_account_no', AccountNo[0]);	

									try{
										nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORD');
										var invtrecid = nlapiSubmitRecord(invtRec, false, true);
										nlapiLogExecution('ERROR', 'After Submitting invtrecid',invtrecid );
									}
									catch (e)
									{
										if ( e instanceof nlobjError )
											nlapiLogExecution( 'ERROR', 'system error', e.getCode() + '\n' + e.getDetails() );
										else
											nlapiLogExecution( 'ERROR', 'unexpected error', e.getCode() + '\n' + e.getDetails() );
									}

									if(request.getParameter('custpage_confirmtohost')=='T')
									{
										var LotBatchText='';
										LotBatchText=request.getLineItemValue('custpage_items', 'custpage_actbatch', i + 1);
										nlapiLogExecution('ERROR', 'LotBatchText is- 1',LotBatchText );
										if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
										{ 	
											//Case# 20148565 starts
											templot=LotBatchText;
											//if(LotBatchText == "" || LotBatchText == null)
											//Case# 20148565 ends
											if(templot == "" || templot == null)
											{
												nlapiLogExecution( 'ERROR', 'skuno', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
												var Batchfilters = new Array();
												Batchfilters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') ));
												var Batchcolumns=new Array();
												Batchcolumns[0]=new nlobjSearchColumn('name');
												var Batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry',null, Batchfilters, Batchcolumns);

												nlapiLogExecution( 'ERROR', 'Batchsearchresults', Batchsearchresults );
												if(Batchsearchresults!=null)
												{

													LotBatchText= Batchsearchresults[0].getValue('name');
													nlapiLogExecution( 'ERROR', 'LotBatchText', LotBatchText );
													//Case # 20127488 start
													templot=LotBatchText;
													//Case # 20127488 end
												}
											}
										}
										var itmstatus=null;
										var actqty1 = request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);
										var FromAccNo=AccountNo[0];
										var ItemId=CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');										
										var notes="This is from Cycle count";

										nlapiLogExecution('ERROR','ItemStatus',ItemStatus);
										try
										{
											//NSAdjustid =InvokeNSInventoryAdjustmentNew(ItemId,ItemStatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,varianceinActserialNoNew,FromAccNo);
											//case # 20127488 Passing the var LotBatchText into the function.
											//Case # 20127488 start
											NSAdjustid =InvokeNSInventoryAdjustmentNew(ItemId,ItemStatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,templot,FromAccNo);
											//Case # 20127488 end
										}
										catch(exp)
										{
											nlapiLogExecution('ERROR','Exception in InvokeNSInventoryAdjustmentNew',exp);
										}
									}
								}

							}
							else
							{	
								var LP = "";
								var TotQty = request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);
								var palletQuantity = 0;

								var expvarianceqty='';
								var actualvarianceqty='';
								var varianceinActserialNoNew='';

								var varianceinExpserialNo = '';
								var varianceinActserialNo = '';

								var varianceinActserialNoNew=0;
								var varianceinActserialNoNew=0;
								var actualvarianceqtyNew=0;
								var expvarianceqtytoNS=0;
								var expvarianceqtynotforNS=0;
								var expserialnoToNS ='';
								var expserialnoNotforNS ='';
								var actnewserialnoToNS ='';

								if((actserialno != null && actserialno != '') || (expserialno != null && expserialno != ''))
								{
									var getexpserialArr =  new Array();
									var getactserialArr =  new Array();

									if(expserialno != null && expserialno != '')
									{
										expserialno = expserialno.trim();
										getexpserialArr = expserialno.split(',');
									}

									if(actserialno != null && actserialno != '')
									{
										actserialno = actserialno.trim();
										getactserialArr = actserialno.split(',');
									}

									if(getexpserialArr != null && getexpserialArr != '')
									{
										for (var p = 0; p < getexpserialArr.length; p++) 
										{

											if(actserialno != null && actserialno != '')
											{
												if(actserialno.indexOf(getexpserialArr[p])==-1)//not found temSeriIdArr
												{
													if (varianceinExpserialNo == "") {
														varianceinExpserialNo= getexpserialArr[p];

													}
													else {
														varianceinExpserialNo = varianceinExpserialNo + "," + getexpserialArr[p];

													}
												}
											}
										}
									}

									if(getactserialArr != null && getactserialArr != '')
									{
										for (var q = 0; q < getactserialArr.length; q++) 
										{

											if(getexpserialArr != null && getexpserialArr != '')
											{
												if(getexpserialArr.indexOf(getactserialArr[q])==-1)//not found temSeriIdArr
												{
													nlapiLogExecution('ERROR', 'getactserialArr[q] in side', getactserialArr[q]);
													if (varianceinActserialNo == "") {
														varianceinActserialNo= getactserialArr[q];

													}
													else {
														varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

													}

													var filters = new Array();
													filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getactserialArr[q]);

													var columns = new Array();
													columns[0] = new nlobjSearchColumn('custrecord_serialparentid');
													var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

													if(searchResults != null && searchResults != "" && searchResults.length == 1)
													{

														if (varianceinActserialNoNew == "") {
															varianceinActserialNoNew= getactserialArr[q];

														}
														else {
															varianceinActserialNoNew = varianceinActserialNoNew + "," + getactserialArr[q];

														}
													}
												}
											}
											else
											{
												// case no 20126236

												nlapiLogExecution('ERROR', 'getactserialArr[q] in side', getactserialArr[q]);
												if (varianceinActserialNo == "") {
													varianceinActserialNo= getactserialArr[q];

												}
												else {
													varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

												}

												var filters = new Array();
												filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getactserialArr[q]);

												var columns = new Array();
												columns[0] = new nlobjSearchColumn('custrecord_serialparentid');
												var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

												if(searchResults != null && searchResults != "" && searchResults.length == 1)
												{

													if (varianceinActserialNoNew == "") {
														varianceinActserialNoNew= getactserialArr[q];

													}
													else {
														varianceinActserialNoNew = varianceinActserialNoNew + "," + getactserialArr[q];

													}
												}

											}
										}
									}
									nlapiLogExecution('ERROR', 'varianceinExpserialNo ', varianceinExpserialNo);
									nlapiLogExecution('ERROR', 'varianceinActserialNoNew ', varianceinActserialNoNew);	

									nlapiLogExecution('ERROR', 'vItemId ', vItemId);
									nlapiLogExecution('ERROR', 'vWHLoc ', vWHLoc);

									var totalNSserialno = '';
									var totalNSserialnoforexpitem = '';
									var vtotNSSerialARR = new Array();
									var vtotNSSerialARRforexpitem = new Array();

									if(vExpItemId == vItemId)
									{
										var filters = new Array();
										filters.push(new nlobjSearchFilter('item',null,'anyof', vItemId));
										filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', vWHLoc);

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('inventorynumber');
										columns[1] = new nlobjSearchColumn('internalid').setSort();

										var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);

										for (var i1 = 0; searchresultsitem != null && i1 < searchresultsitem.length; i1++) 
										{
											if(totalNSserialno == null || totalNSserialno == '')
												totalNSserialno = searchresultsitem[i1].getValue('inventorynumber');
											else
												totalNSserialno = totalNSserialno + ',' + searchresultsitem[i1].getValue('inventorynumber');

										}

										if(totalNSserialno != null && totalNSserialno  != '')
											vtotNSSerialARR = totalNSserialno.split(',');

										nlapiLogExecution('ERROR', 'vtotNSSerialARR if same item', vtotNSSerialARR);


										if(totalNSserialnoforexpitem == null || totalNSserialnoforexpitem  == '')
											vtotNSSerialARRforexpitem = totalNSserialno.split(',');

										nlapiLogExecution('ERROR', 'vtotNSSerialARRforexpitem  if same item', vtotNSSerialARRforexpitem);
									}
									else
									{
										//Search for expitem total serial numbers from NS
										var filters = new Array();
										filters.push(new nlobjSearchFilter('item',null,'anyof', vExpItemId));
										filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', vWHLoc);

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('inventorynumber');
										columns[1] = new nlobjSearchColumn('internalid').setSort();

										var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);

										for (var i1 = 0; searchresultsitem != null && i1 < searchresultsitem.length; i1++) 
										{
											if(totalNSserialnoforexpitem == null || totalNSserialnoforexpitem == '')
												totalNSserialnoforexpitem = searchresultsitem[i1].getValue('inventorynumber');
											else
												totalNSserialnoforexpitem = totalNSserialnoforexpitem + ',' + searchresultsitem[i1].getValue('inventorynumber');

										}

										//search for actitem total serial numbers from NS			


										var filters = new Array();
										filters.push(new nlobjSearchFilter('item',null,'anyof', vItemId));
										filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', vWHLoc);

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('inventorynumber');
										columns[1] = new nlobjSearchColumn('internalid').setSort();

										var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);

										for (var i1 = 0; searchresultsitem != null && i1 < searchresultsitem.length; i1++) 
										{
											if(totalNSserialno == null || totalNSserialno == '')
												totalNSserialno = searchresultsitem[i1].getValue('inventorynumber');
											else
												totalNSserialno = totalNSserialno + ',' + searchresultsitem[i1].getValue('inventorynumber');

										}


										if(totalNSserialnoforexpitem != null && totalNSserialnoforexpitem  != '')
											vtotNSSerialARRforexpitem = totalNSserialnoforexpitem.split(',');

										nlapiLogExecution('ERROR', 'vtotNSSerialARRforexpitem ', vtotNSSerialARRforexpitem);



										if(totalNSserialno != null && totalNSserialno  != '')
											vtotNSSerialARR = totalNSserialno.split(',');

										nlapiLogExecution('ERROR', 'vtotNSSerialARR ', vtotNSSerialARR);

									}



									//	var vMissedSerial = 'S42000000000899,S42000000000900,XTATM265-A06-1211460243';
//									the below is to find whether varianceexpected serails number are there in NS or not if it there then it goes to if cond if not it goes to else
									if(varianceinExpserialNo != null && varianceinExpserialNo != '')
									{
										nlapiLogExecution('ERROR', 'varianceinExpserialNo in side if', varianceinExpserialNo);
										nlapiLogExecution('ERROR', 'vtotNSSerialARRforexpitem in side if', vtotNSSerialARRforexpitem);
										var vMissedExpSerialARR = varianceinExpserialNo.split(',');
										nlapiLogExecution('ERROR', 'vMissedExpSerialARR if', vMissedExpSerialARR);
										for(k=0;k<vMissedExpSerialARR.length;k++)
										{
											nlapiLogExecution('ERROR', 'vMissedExpSerialARR[k] ', vMissedExpSerialARR[k]);
											if(vtotNSSerialARRforexpitem.indexOf(vMissedExpSerialARR[k])!=-1)// found temSeriIdArr
											{
												if(expserialnoToNS ==null || expserialnoToNS =='')
													expserialnoToNS = vMissedExpSerialARR[k];
												else
												{
													if(expserialnoToNS.length>3500)
													{
														expserialnoToNS = expserialnoToNS + ',' +vMissedExpSerialARR[k]+"@";
														expserialnoToNS='';
													}
													else
													{
														expserialnoToNS = expserialnoToNS + ',' +vMissedExpSerialARR[k];
													}
												}

											}
											else
											{
												if(expserialnoNotforNS ==null || expserialnoNotforNS =='')
													expserialnoNotforNS = vMissedExpSerialARR[k];
												else
													expserialnoNotforNS = expserialnoNotforNS + ',' +vMissedExpSerialARR[k];
											}
										}
									}
									if(varianceinActserialNoNew != null && varianceinActserialNoNew != '')
									{
										nlapiLogExecution('ERROR', 'varianceinActserialNoNew inside if', varianceinActserialNoNew);	
										var vMissedActSerialARR = varianceinActserialNoNew.split(',');
										nlapiLogExecution('ERROR', 'vMissedActSerialARR ', vMissedActSerialARR);

										for(m=0;m<vMissedActSerialARR.length;m++)
										{
											//if(varianceinActserialNoNew.indexOf(vtotNSSerialARR[k])==-1)// found temSeriIdArr
											if(vtotNSSerialARR.indexOf(vMissedActSerialARR[m])==-1)// found temSeriIdArr
											{
												if(actnewserialnoToNS ==null || actnewserialnoToNS =='')
													actnewserialnoToNS = vMissedActSerialARR[m];
												else
												{
													if(actnewserialnoToNS.length>3500)
													{
														actnewserialnoToNS = actnewserialnoToNS + ',' +vMissedActSerialARR[m]+"@";
														actnewserialnoToNS='';
													}
													else
													{
														actnewserialnoToNS = actnewserialnoToNS + ',' +vMissedActSerialARR[m];
													}
												}
											}
										}
									}
									if(varianceinExpserialNo != null && varianceinExpserialNo != '')
										expvarianceqty =varianceinExpserialNo.split(',').length;

									if(expserialnoToNS != null && expserialnoToNS != '')
										expvarianceqtytoNS =expserialnoToNS.split(',').length;

									if(expserialnoNotforNS != null && expserialnoNotforNS != '')
										expvarianceqtynotforNS =expserialnoNotforNS.split(',').length;


									if(varianceinActserialNo != null && varianceinActserialNo != '')
										actualvarianceqty =varianceinActserialNo.split(',').length;

									if(actnewserialnoToNS != null && actnewserialnoToNS != '')
										actualvarianceqtyNew =actnewserialnoToNS.split(',').length;

									nlapiLogExecution('ERROR', 'expvarianceqty ', expvarianceqty);		
									nlapiLogExecution('ERROR', 'actualvarianceqty ', actualvarianceqty);	
									nlapiLogExecution('ERROR', 'actualvarianceqtyNew ', actualvarianceqtyNew);	
									nlapiLogExecution('ERROR', 'varianceinExpserialNo ', varianceinExpserialNo);	
									nlapiLogExecution('ERROR', 'expvarianceqtynotforNS ', expvarianceqtynotforNS);	
									nlapiLogExecution('ERROR', 'varianceinActserialNo ', varianceinActserialNo);	
									nlapiLogExecution('ERROR', 'actualvarianceqtyNew ', actualvarianceqtyNew);	
									nlapiLogExecution('ERROR', 'expserialnoToNS ', expserialnoToNS);	
									nlapiLogExecution('ERROR', 'expserialnoNotforNS ', expserialnoNotforNS);	
									nlapiLogExecution('ERROR', 'actnewserialnoToNS ', actnewserialnoToNS);	


								}
								palletQuantity = fetchPalletQuantity(request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1),sitelocn,null);
								nlapiLogExecution('ERROR','palletQuantity',palletQuantity);
								nlapiLogExecution('ERROR','TotQty',TotQty);

								var LotBatchId='';
								var oldLotBatchId='';
								var LotBatchText=request.getLineItemValue('custpage_items', 'custpage_actbatch', i + 1);
								var oldLotBatchText=request.getLineItemValue('custpage_items', 'custpage_expbatch', i + 1);
								var vtempactualItem=request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1);
								var vtempexpectedItem=request.getLineItemValue('custpage_items', 'custpage_expsku', i + 1);

								if(LotBatchText!=null && LotBatchText!='')
									LotBatchId=getLotBatchId(LotBatchText,vtempactualItem);

								if(oldLotBatchText!=null && oldLotBatchText!='')
									oldLotBatchId=getLotBatchId(oldLotBatchText,vtempexpectedItem);

								nlapiLogExecution('ERROR', 'LotBatchText tst', LotBatchText);	
								nlapiLogExecution('ERROR', 'LotBatchId new tst', LotBatchId);


								nlapiLogExecution('ERROR', 'oldLotBatchText tst', oldLotBatchText);	
								nlapiLogExecution('ERROR', 'oldLotBatchId new tst', oldLotBatchId);
								var itmstatus = request.getLineItemValue('custpage_items', 'custpage_expskustatusvalue', i + 1);
								var ItemNewStatus = request.getLineItemValue('custpage_items', 'custpage_actskustatusvalue', i + 1);

								nlapiLogExecution('ERROR','itmstatus',itmstatus);
								nlapiLogExecution('ERROR','ItemNewStatus',ItemNewStatus);

								var CallFlag='N';
								var OldMakeWHFlag='Y';
								var NewMakeWHFlag='Y';
								var vNewWHlocatin = "";
								var vOldWHlocatin = "";
								var NewWhLocation = "";
								var OldWhLocation = "";
								var vNSFlag;
								var UpdateFlag;
								var vNSCallFlag='N';
								var qtyonhand;
								var accountno;
								var sitelocn;
								var memo;
								if(itmstatus==ItemNewStatus)
									CallFlag='N';
								else
								{

									vNSFlag=GetNSCallFlag(itmstatus,ItemNewStatus);
									nlapiLogExecution('ERROR', 'vNSFlag',vNSFlag);

									if(vNSFlag!= null && vNSFlag!= "" && vNSFlag.length>0)
									{
										CallFlag=vNSFlag[0];
										nlapiLogExecution('ERROR', 'vNSFlag[0]',vNSFlag[0]);
									}
									if(vNSFlag[0]=='N')
										CallFlag='N';
									else if(vNSFlag[0]=='Y') 
									{ 
										OldMakeWHFlag= GetMWFlag(vNSFlag[1]);
										NewMakeWHFlag= GetMWFlag(vNSFlag[2]);
										vNewWHlocatin=vNSFlag[2]; 
										vOldWHlocatin=vNSFlag[1]; 
										OldWhLocation=vNSFlag[3]; 
										NewWhLocation=vNSFlag[4]; 
									}
								}

								nlapiLogExecution('ERROR','CallFlag',CallFlag);
								nlapiLogExecution('ERROR','OldMakeWHFlag',OldMakeWHFlag);
								nlapiLogExecution('ERROR','NewMakeWHFlag',NewMakeWHFlag);
								nlapiLogExecution('ERROR','vNewWHlocatin',vNewWHlocatin);
								nlapiLogExecution('ERROR','vOldWHlocatin',vOldWHlocatin);

								var scount=1;

								LABL1: for(var p=0;p<scount;p++)
								{	

									nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', p);
									try
									{
										var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1));

										transaction.setFieldValue('custrecord_ebiz_inv_packcode', '1');
										var totalQty = request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);

										//var qtyonhand = transaction.getFieldValue('custrecord_ebiz_qoh');
										/***Below code is merged from Lexjet Production on 4th March 2013 by Ganesh K***/
										if((parseFloat(palletQuantity) != 0) && (parseFloat(TotQty) > parseFloat(palletQuantity)))
										{
											nlapiLogExecution('ERROR', 'TotQty old',TotQty);
											nlapiLogExecution('ERROR', 'palletQuantity old',palletQuantity);
											totalQty = palletQuantity;
										}
										transaction.setFieldValue('custrecord_ebiz_inv_qty',parseFloat(totalQty).toFixed(5));
										nlapiLogExecution('ERROR', 'totalQty ', totalQty);
										/***Upto here***/
										qtyonhand = transaction.getFieldValue('custrecord_ebiz_qoh');
										accountno = transaction.getFieldValue('custrecord_ebiz_inv_account_no');
										sitelocn = transaction.getFieldValue('custrecord_ebiz_inv_loc');
										memo = transaction.getFieldValue('custrecord_ebiz_inv_note1');
										var allocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
										nlapiLogExecution('ERROR', 'totalQty', totalQty);	
										nlapiLogExecution('ERROR', 'allocQty', allocQty);
										// Case # 20124813 Start
										if(allocQty==null || allocQty=='' || allocQty=='null')
										{ 
											allocQty=0;
										}
										// Case # 20124813 END
										/* code added from FactoryMation on 28Feb13 by santosh, DecimalConversion */
										var availqty = parseFloat(totalQty) - parseFloat(allocQty);

										nlapiLogExecution('ERROR', 'availqty', availqty);
										nlapiLogExecution('ERROR', 'qtyonhand', qtyonhand);
										nlapiLogExecution('ERROR', 'qtytobe adjust', request.getLineItemValue('custpage_items', 'custpage_qtytobeadjust', i + 1));
										// case # 20126953 starts
										//var totalonhandQty = parseFloat(qtyonhand) + parseFloat(request.getLineItemValue('custpage_items', 'custpage_qtytobeadjust', i + 1));
										var totalonhandQty =  parseFloat(request.getLineItemValue('custpage_items', 'custpage_qtytobeadjust', i + 1));
										// case 20126953 ends
										nlapiLogExecution('ERROR', 'totalonhandQty', totalonhandQty);

										if(parseFloat(totalonhandQty) <= 0)
											totalonhandQty=0;
										//else
										//	totalonhandQty=totalQty;
										nlapiLogExecution('ERROR', 'Math.round(totalonhandQty)', Math.round(totalonhandQty));
										//transaction.setFieldValue('custrecord_ebiz_qoh', Math.round(totalQty));
										//transaction.setFieldValue('custrecord_ebiz_qoh', Math.round(totalonhandQty));

										//case # 20127037 (,resolve qty(record actual qty) is taken and set into allocated qty of create inventory table)
										//transaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totalonhandQty).toFixed(5));
										transaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totalQty).toFixed(5));
										transaction.setFieldValue('custrecord_ebiz_avl_qty', parseFloat(availqty).toFixed(5));
										//case # 20127037 ends
										if((request.getLineItemValue('custpage_items', 'custpage_actlp', i + 1) != null && request.getLineItemValue('custpage_items', 'custpage_actlp', i + 1) != ""))
										{
											LP = request.getLineItemValue('custpage_items', 'custpage_actlp', i + 1);
											transaction.setFieldValue('custrecord_ebiz_inv_lp', request.getLineItemValue('custpage_items', 'custpage_actlp', i + 1));
										}									
										if(request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1) != null && request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1) != "")
											transaction.setFieldValue('custrecord_ebiz_inv_sku', request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1));
										var vactitem = request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1);
										var vexpitem = request.getLineItemValue('custpage_items', 'custpage_expsku', i + 1);
										nlapiLogExecution('ERROR','vactitem',vactitem);
										nlapiLogExecution('ERROR','vexpitem',vexpitem);
										var fields = ['recordType', 'custitem_ebizserialin'];
										//var columns = nlapiLookupField('item', ItemId, fields);
										var columns = nlapiLookupField('item', vactitem, fields);//case# 20149641
										var vItemType = columns.recordType;
										nlapiLogExecution('ERROR','vItemType',vItemType);//here


										if(vactitem!= vexpitem)
										{
											if(vItemType != "lotnumberedinventoryitem" || vItemType!="lotnumberedassemblyitem")
											{
												transaction.setFieldValue('custrecord_ebiz_expdate', '');
												transaction.setFieldValue('custrecord_ebiz_inv_fifo', '');


											}
										}
										//custpage_actskustatus
										if(request.getLineItemValue('custpage_items', 'custpage_actskustatus', i + 1) != null && request.getLineItemValue('custpage_items', 'custpage_actskustatus', i + 1) != "")
											transaction.setFieldValue('custrecord_ebiz_inv_sku_status', request.getLineItemValue('custpage_items', 'custpage_actskustatusvalue', i + 1));
										transaction.setFieldValue('custrecord_ebiz_invholdflg', 'F');
										transaction.setFieldValue('custrecord_ebiz_cycl_count_hldflag', 'F');
										transaction.setFieldValue('custrecord_ebiz_inv_lot', LotBatchId);
										transaction.setFieldValue('custrecord_ebiz_inv_updateddate', DateStamp());
										transaction.setFieldValue('custrecord_ebiz_inv_updatedtime', TimeStamp());
										transaction.setFieldValue('custrecord_wms_inv_status_flag', '19');//Storage
										transaction.setFieldValue('custrecord_invttasktype', '7');//Storage
										transaction.setFieldValue('custrecord_ebiz_callinv', 'N');

										// Case# 20127753 starts
										transaction.setFieldValue('custrecord_ebiz_itemdesc', CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc'));
										// Case# 20127753 end

										if(LotBatchId!=null&&LotBatchId!="")
										{
											transaction.setFieldValue('custrecord_ebiz_inv_lot', LotBatchId);	
											var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',LotBatchId,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
											var fifodate=rec.custrecord_ebizfifodate;
											var expdate=rec.custrecord_ebizexpirydate;
											if(fifodate!=null && fifodate!='')
												transaction.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
											/*else
												transaction.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
											transaction.setFieldValue('custrecord_ebiz_expdate', expdate);
										}
										//vNewWHlocatin
										if(NewWhLocation != null && NewWhLocation != "")
										{
											if(NewWhLocation != sitelocn)
											{
												transaction.setFieldValue('custrecord_ebiz_inv_loc', NewWhLocation);
											}
										}
										var id = nlapiSubmitRecord(transaction, true);

										try
										{
											//case# 20127131 starts (totalonhandQty is replaced with totalQty)
											nlapiLogExecution('ERROR', "allocQty/totalQty",allocQty+'/'+totalQty);
											if(parseInt(allocQty)== 0&&parseInt(totalQty)==0&&id!=null&&id!="")
											{
												//case# 20127131 end
												nlapiLogExecution('ERROR', ' in Delete Inventory Record ', id);	
												var delid = nlapiDeleteRecord('customrecord_ebiznet_createinv',id);				
											}
										}
										catch (exp) 
										{
											nlapiLogExecution('ERROR', 'Exception in Delete Inventory Record ', exp);	
										}


									}
									catch(ex)
									{
										var exCode='CUSTOM_RECORD_COLLISION'; 
										var wmsE='Inventory record being updated by another user. Please try again...';
										if (ex instanceof nlobjError) 
										{	
											wmsE=ex.getCode() + '\n' + ex.getDetails();
											exCode=ex.getCode();
										}
										else
										{
											wmsE=ex.toString();
											exCode=ex.toString();
										} 

										nlapiLogExecution('ERROR', 'Exception in Cycle count Generate release : ', wmsE); 
										if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
										{  
											scount=scount+1;
											continue LABL1;
										}
										else break LABL1;
									}
								}

								var actqty1 = request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);
								if(parseFloat(palletQuantity) != 0 && (parseFloat(TotQty) > parseFloat(palletQuantity)))
								{
									actqty1 = palletQuantity;
								}
								var expqty1 = request.getLineItemValue('custpage_items', 'custpage_expqty', i + 1);
								nlapiLogExecution('ERROR', 'expqty1', expqty1);	
								nlapiLogExecution('ERROR', 'actqty1', actqty1);	
								var Currentexpqty1 = request.getLineItemValue('custpage_items', 'custpage_currentinvt', i + 1);
								nlapiLogExecution('ERROR', 'Currentexpqty1', Currentexpqty1);	
								if(Currentexpqty1=='undefined'||Currentexpqty1==null||Currentexpqty1==''||isNaN(Currentexpqty1)){
									Currentexpqty1=0;
								}

								if(Currentexpqty1 !=null && Currentexpqty1 !='' && Currentexpqty1 !=0)
								{
									nlapiLogExecution('ERROR', 'into', Currentexpqty1);
									var resultNew =parseFloat(allocQty)+parseFloat(Currentexpqty1);
									nlapiLogExecution('ERROR', 'resultNew', resultNew);
									resultQty1 = parseFloat(actqty1) - parseFloat(resultNew);
								}
								else
								{
									resultQty1 = parseFloat(actqty1) - parseFloat(expqty1);
								}
								nlapiLogExecution('ERROR', 'resultQty1', resultQty1);	
								var ItemId=request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1);
								var expItemId=request.getLineItemValue('custpage_items', 'custpage_expsku', i + 1);
								//var itmstatus=null;
								var ItemNewStatus=null;
								var itmstatus=request.getLineItemValue('custpage_items', 'custpage_expskustatusvalue', i + 1);

								var FromAccNo=AccountNo[0];
								var ToAccNo=null;

								nlapiLogExecution('ERROR', 'Exp. Item ', expItemId);	
								nlapiLogExecution('ERROR', 'Act. Item ', ItemId);	

								var notes="This is from Cycle count";

								var fields = ['recordType', 'custitem_ebizserialin'];
								var columns = nlapiLookupField('item', ItemId, fields);
								var ItemType = columns.recordType;
								nlapiLogExecution('ERROR','ItemType',ItemType);

								var serialInflg="F";		
								serialInflg = columns.custitem_ebizserialin;

								var serialnumbers = "";
								var localSerialNoArray = new Array();
								if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg =="T") 
								{
									var adjqty = 0;
									adjqty = parseFloat(resultQty1);
									if(parseFloat(actqty1) == 0)
									{
										adjqty = 0;
									}

									AdjustSerialNumbers(localSerialNoArray,parseFloat(adjqty),ItemId,LP,request.getParameter('restype'),CyccRecID,varianceinExpserialNo);

								}							
								var expfields = ['recordType', 'custitem_ebizserialin'];
								var expcolumns = nlapiLookupField('item', expItemId, expfields);
								var expItemType = expcolumns.recordType;
								nlapiLogExecution('ERROR','expItemType',expItemType);

								var expserialInflg="F";		
								expserialInflg = expcolumns.custitem_ebizserialin;



								if (expItemType == "serializedinventoryitem" || expItemType == "serializedassemblyitem" || expserialInflg == "T") 
								{
									//if(localSerialNoArray != null && localSerialNoArray != "")
									//{
									//nlapiLogExecution('ERROR','localSerialNoArray',localSerialNoArray.ToString());
									//if(localSerialNoArray.length != 0)
									//{
									//nlapiLogExecution('ERROR','localSerialNoArray.length',localSerialNoArray.length);
									//serialnumbers = getSerialNoCSV(localSerialNoArray,LP);
									serialnumbers = getSerialNoCSVvalues(ItemId,LP,expqty1,actqty1);
									nlapiLogExecution('ERROR','serialnumbers',serialnumbers);
									LotBatchText = serialnumbers;
									//}
									//}
								}
								nlapiLogExecution('ERROR','host',request.getParameter('custpage_confirmtohost'));

								if(request.getParameter('custpage_confirmtohost')=='T'  )
								{
									try
									{
										if(expItemId==ItemId && parseFloat(resultQty1)!=0 && oldLotBatchId==LotBatchId && ((varianceinExpserialNo == null || varianceinExpserialNo == "" || varianceinExpserialNo == 0) && (varianceinActserialNoNew == null || varianceinActserialNoNew == '' || varianceinActserialNoNew ==0)) && ((expserialnoToNS == null || expserialnoToNS == "") && (actnewserialnoToNS == null || actnewserialnoToNS == '')))
										{
											nlapiLogExecution('ERROR','test1','test1');
											NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(resultQty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
										}
										else if(expItemId==ItemId && ((expserialnoToNS != null && expserialnoToNS != "") || (actnewserialnoToNS != null && actnewserialnoToNS != '')))
										{
											nlapiLogExecution('ERROR','expserialnoToNS before NS inv',expserialnoToNS);
											//commented the below line becuase of doubt related to qty sent and no.of serialno sent
											//NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,-parseFloat(expvarianceqty),notes,vTaskType,vAdjusttype,expserialnoToNS,FromAccNo);
											//case# 20148167 20148647 20148170 starts
											//NSAdjustid1 = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,-parseFloat(expvarianceqtytoNS),notes,vTaskType,vAdjusttype,expserialnoToNS,FromAccNo);
											if(parseFloat(actualvarianceqtyNew)>0)
											{
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actualvarianceqtyNew),notes,vTaskType,vAdjusttype,actnewserialnoToNS,FromAccNo);
											}
											else
											{
												NSAdjustid1 = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,-parseFloat(expvarianceqtytoNS),notes,vTaskType,vAdjusttype,expserialnoToNS,FromAccNo);
											}
											//case# 20148167 20148647 20148170 ends
										}
										else if(expItemId!=ItemId && ((expserialnoToNS != null && expserialnoToNS != "") || (actnewserialnoToNS != null && actnewserialnoToNS != '')))
										{
											nlapiLogExecution('ERROR','expserialnoToNS before NS inv if itemid is not equal',expserialnoToNS);

											if(actnewserialnoToNS != null && actnewserialnoToNS != '')
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actualvarianceqtyNew),notes,vTaskType,vAdjusttype,actnewserialnoToNS,FromAccNo);//positive
											if ((expItemType == "serializedinventoryitem" || expItemType == "serializedassemblyitem" || expserialInflg =="T")  && expserialnoToNS != null && expserialnoToNS != "") 
												NSAdjustid1 = InvokeNSInventoryAdjustmentNew(expItemId,itmstatus,vWHLoc,- parseFloat(expvarianceqtytoNS),notes,vTaskType,vAdjusttype,expserialnoToNS,FromAccNo);//negative
											else
												NSAdjustid1 = InvokeNSInventoryAdjustmentNew(expItemId,itmstatus,vWHLoc,- parseFloat(expqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);//negative
										}
										else if(expItemId!=ItemId && ((varianceinExpserialNo == null || varianceinExpserialNo == "") && (varianceinActserialNoNew == null || varianceinActserialNoNew == '')))
										{
											if(parseFloat(actqty1)>0)
											{
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);//positive
											}
											NSAdjustid1 = InvokeNSInventoryAdjustmentNew(expItemId,itmstatus,vWHLoc,- parseFloat(expqty1),notes,vTaskType,vAdjusttype,oldLotBatchText,FromAccNo);//negative
										}
										else if(oldLotBatchId!=LotBatchId && parseFloat(resultQty1)==0)
										{
											nlapiLogExecution('ERROR', 'parseFloat(resultQty1) 2 ', parseFloat(resultQty1));
											if(parseFloat(actqty1)>0)
											{
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
											}
											NSAdjustid1 = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,-parseFloat(expqty1),notes,vTaskType,vAdjusttype,oldLotBatchText,FromAccNo);
										}
										else if(oldLotBatchId!=LotBatchId && parseFloat(resultQty1)!=0)
										{
											nlapiLogExecution('ERROR', 'parseFloat(actqty1) 1 ', parseFloat(actqty1));
											if(parseFloat(actqty1)>0)
											{
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
											}
											NSAdjustid1 = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,-parseFloat(expqty1),notes,vTaskType,vAdjusttype,oldLotBatchText,FromAccNo);
										}
									}
									catch(exp)
									{
										nlapiLogExecution('ERROR','Exception in InvokeNSInventoryAdjustmentNew',exp);
									}
								}
								if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
								{
									var tempserial = actserialno.split(',');
									nlapiLogExecution('ERROR', 'tempserial 1 ', tempserial);
									nlapiLogExecution('ERROR', 'tempserial.length 1 ', tempserial.length);
									var serial='';
									/*for(var i=0;i<tempserial.length;i++)
									{
										if(serial == null || serial =='' )
											serial = tempserial[i] + "(" + actqty1 + ")";
											else
												serial = serial + " " + tempserial[i] + "(" + actqty1 + ")";

										nlapiLogExecution('ERROR', 'serial 1 ', serial);
									}*/

									LotBatchText = actserialno;
								}
								nlapiLogExecution('ERROR', 'LotBatchText 1 ', LotBatchText);
								if(CallFlag=='Y')//To trigger NS automatically
								{
									try
									{
										InvokeNSInventoryTransfer(ItemId,ItemNewStatus,vOldWHlocatin,vNewWHlocatin,actqty1,LotBatchText);

										//case 20126530 start
										var QtyVariance = request.getLineItemValue('custpage_items', 'custpage_qtyvariance', i + 1);
										nlapiLogExecution('ERROR', 'QtyVariance',QtyVariance);
										if(expItemId==ItemId && parseInt(QtyVariance)!=0 &&QtyVariance!=null && QtyVariance!='')//case# 20148170
										{
											nlapiLogExecution('ERROR', 'into QtyVariance',QtyVariance);
											InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseInt(QtyVariance),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
										}
										//case 20126530 end

									}
									catch(exp)
									{
										nlapiLogExecution('ERROR','Exception in InvokeNSInventoryTransfer',exp);
									}


									/* This part of code is commented as per inputs from NS team
									 * For all vitual location inventory move NS team suggested us to us Inventory Transfer object instead of inventory adjustment object
									 * Averate cost/price of the item will not change if we use Inventory Transfer object

									//Code for fetching account no from stockadjustment custom record	
									var AccountNo = getStockAdjustmentAccountNoNew(InvAdjType);

									if(AccountNo !=null && AccountNo != "" && AccountNo.length>0)
									{
										var FromAccNo=AccountNo[0];
										var ToAccNo=AccountNo[1];
										nlapiLogExecution('ERROR', 'vOldWHlocatin',vOldWHlocatin);
										nlapiLogExecution('ERROR', 'vNewWHlocatin',vNewWHlocatin);

										nlapiLogExecution('ERROR', 'FromAccNo',FromAccNo);
										nlapiLogExecution('ERROR', 'ToAccNo',ToAccNo);
										nlapiLogExecution('ERROR', 'LotBatchText',LotBatchText);
										InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vOldWHlocatin,-(parseFloat(ItemNewQty)),"",vTaskType,InvAdjType,LotBatchText,FromAccNo);
										InvokeNSInventoryAdjustmentNew(ItemId,ItemNewStatus,vNewWHlocatin,ItemNewQty,"",vTaskType,InvAdjType,LotBatchText,ToAccNo);

									}
									 */ 
								}


								if((parseFloat(palletQuantity) != 0) && ( (parseFloat(TotQty) > parseFloat(palletQuantity)) || (parseFloat(expqty1)==0) ))
								{
									nlapiLogExecution('ERROR','Creating new record as qty is more than pallet qty',TotQty);
									var LotBatchId='';
									var LotBatchText=request.getLineItemValue('custpage_items', 'custpage_actbatch', i + 1);
									var vtempactualItem=request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1);
									var vtempexpectedItem=request.getLineItemValue('custpage_items', 'custpage_expsku', i + 1);

									if(LotBatchText!=null && LotBatchText!='')
										LotBatchId=getLotBatchId(LotBatchText,vtempactualItem);

									/*if(LotBatchText!=null && LotBatchText!='')
										LotBatchId=getLotBatchId(LotBatchText);*/

									var ItemStatus = "";
									if(CycleCountSearchResult[0].getValue('custrecord_cyclesku_status')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclesku_status') != "")
									{
										ItemStatus = CycleCountSearchResult[0].getValue('custrecord_cyclesku_status').toString();
									}
									else if(CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status')!= null && CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status') != "")
									{
										ItemStatus = CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status').toString();
									}
									else
									{
										var vRoleLocation=getRoledBasedLocation();
										nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
										var itemStatusFilters = new Array();
										itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
										itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');										
										itemStatusFilters[2] = new nlobjSearchFilter('custrecord_defaultskustaus',null, 'is','T');
										if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
										{
											//Case # 20126438 Start
											itemStatusFilters[3] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',vRoleLocation);
											//Case # 20126438 End
										}

										var itemStatusColumns = new Array();			
										itemStatusColumns[0] = new nlobjSearchColumn('name');

										var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);			
										if(itemStatusSearchResult != null && itemStatusSearchResult != '')
										{
											ItemStatus = itemStatusSearchResult[0].getId();
										}
										nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
									}

									var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
									var columns = nlapiLookupField('item', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no'), fields);
									//nlapiLogExecution('ERROR', 'Time Stamp at the end of Item Lookup',TimeStampinSec());
									var ItemType = columns.recordType;	
									var serialInflg="F";		
									serialInflg = columns.custitem_ebizserialin;
									var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
									nlapiLogExecution('ERROR', 'Creating INVT Record ', 'INVT');
									if(Cycexeplanid!= null && Cycexeplanid != "")
										invtRec.setFieldValue('name', Cycexeplanid);
									nlapiLogExecution('ERROR', 'Cycexeplanid',Cycexeplanid );
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_binloc', CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc'));
									nlapiLogExecution('ERROR', 'actbeginloc',CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc') );
									var getItemLP = GetMaxLPNo(1, 1,CycleCountSearchResult[0].getValue('custrecord_cyclesite_id').toString());
									invtRec.setFieldValue('custrecord_ebiz_inv_lp', getItemLP);
									nlapiLogExecution('ERROR', 'invlp', CycleCountSearchResult[0].getValue('custrecord_cycact_lpno'));
									if(CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_sku',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									nlapiLogExecution('ERROR', 'sku_no',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatus);
									nlapiLogExecution('ERROR', 'sku_status',CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status'));
									if(CycleCountSearchResult[0].getValue('custrecord_cyclepc')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclepc') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_packcode', CycleCountSearchResult[0].getValue('custrecord_cyclepc'));
									nlapiLogExecution('ERROR', 'packcode',CycleCountSearchResult[0].getValue('custrecord_cyclepc') );
									if(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty')!= null && CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty') != "")
									{
										nlapiLogExecution('ERROR', 'TotQty new',TotQty);
										nlapiLogExecution('ERROR', 'palletQuantity new',palletQuantity);
										//var qty=parseFloat(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty'));
										var qty = parseFloat(TotQty) - parseFloat(palletQuantity);
										invtRec.setFieldValue('custrecord_ebiz_qoh', qty);
										invtRec.setFieldValue('custrecord_ebiz_inv_qty', qty.toString());
										nlapiLogExecution('ERROR', 'actqty',qty);
									}
									if(CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') != "")
										invtRec.setFieldValue('custrecord_inv_ebizsku_no', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no'));
									//
									nlapiLogExecution('ERROR', 'sku',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									// Case# 20127753 starts
									if(CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc')!= null && CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc') != "")
										// Case# 20127753 ends	
										invtRec.setFieldValue('custrecord_ebiz_itemdesc', CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc'));
									nlapiLogExecution('ERROR', 'itemdesc',CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc') );
									invtRec.setFieldValue('custrecord_invttasktype', 2);
									invtRec.setFieldValue('custrecord_wms_inv_status_flag', ['19']); //Inventory Storage
									if(CycleCountSearchResult[0].getValue('custrecord_cyclesite_id')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclesite_id') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_loc',CycleCountSearchResult[0].getValue('custrecord_cyclesite_id').toString());
									invtRec.setFieldValue('custrecord_ebiz_callinv','N');
									invtRec.setFieldValue('custrecord_ebiz_displayfield','N');
									if(LotBatchId!=null&&LotBatchId!="")
									{
										invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatchId);	
										var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',LotBatchId,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
										var fifodate=rec.custrecord_ebizfifodate;
										var expdate=rec.custrecord_ebizexpirydate;
										if(fifodate!=null && fifodate!='')
											invtRec.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
										/*else
											invtRec.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
										invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
									}
									else
									{
										if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
										{ 										
											var itemid=CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');
											nlapiLogExecution('ERROR', 'itemid', itemid);
											var Batchfilters = new Array();
											Batchfilters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',itemid ));
											var Batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, Batchfilters);
											if(Batchsearchresults!=null)
											{
												var	getlotnoid= Batchsearchresults[0].getId();
												nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
												invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
											}	
										}
									}
									//}
									var LP = "";
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_lpno')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_lpno') != "")
									{
										LP = CycleCountSearchResult[0].getValue('custrecord_cycact_lpno');
									}									

									var serialnumbers = "";
									var localSerialNoArray = new Array();
									if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
									{
										//localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,parseFloat(actqty1),sku,LP,request.getParameter('restype'),CyccRecID,'');
										AdjustSerialNumbers(localSerialNoArray,parseFloat(actqty1),sku,LP,request.getParameter('restype'),CyccRecID,'');
									}

									if(AccountNo!= null && AccountNo != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_account_no', AccountNo[0]);	

									try{
										nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORD');
										var invtrecid = nlapiSubmitRecord(invtRec, false, true);
										nlapiLogExecution('ERROR', 'After Submitting invtrecid',invtrecid );
									}
									catch (e)
									{
										if ( e instanceof nlobjError )
											nlapiLogExecution( 'ERROR', 'system error', e.getCode() + '\n' + e.getDetails() );
										else
											nlapiLogExecution( 'ERROR', 'unexpected error', e.getCode() + '\n' + e.getDetails() );
									}

									if(request.getParameter('custpage_confirmtohost')=='T')
									{
										var LotBatchText='';
										LotBatchText=request.getLineItemValue('custpage_items', 'custpage_actbatch', i + 1);
										if(LotBatchText == "" || LotBatchText == null)
										{
											nlapiLogExecution( 'ERROR', 'skuno', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
											var Batchfilters = new Array();
											Batchfilters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') ));
											var Batchcolumns=new Array();
											Batchcolumns[0]=new nlobjSearchColumn('name');
											var Batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry',null, Batchfilters, Batchcolumns);

											nlapiLogExecution( 'ERROR', 'Batchsearchresults', Batchsearchresults );
											if(Batchsearchresults!=null)
											{
												LotBatchText= Batchsearchresults[0].getValue('name');
												nlapiLogExecution( 'ERROR', 'LotBatchText', LotBatchText );
											}
										}
										var itmstatus=null;
										var actqty1 = request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);
										var FromAccNo=AccountNo[0];
										var ItemId=CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');										

										if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
										{
											if(localSerialNoArray != null && localSerialNoArray != "")
											{
												//nlapiLogExecution('ERROR','localSerialNoArray',localSerialNoArray.ToString());
												if(localSerialNoArray.length != 0)
												{
													nlapiLogExecution('ERROR','localSerialNoArray.length',localSerialNoArray.length);
													serialnumbers = getSerialNoCSV(localSerialNoArray);
													nlapiLogExecution('ERROR','serialnumbers',serialnumbers);
													LotBatchText = serialnumbers;
												}
											}
										}
										//InvokeNSInventoryAdjustment(ItemId,itmstatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText);
										try
										{
											NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,ItemStatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
										}
										catch(exp)
										{
											nlapiLogExecution('ERROR','Exception in InvokeNSInventoryAdjustmentNew',exp);
										}
									}
								}
							}
						}
						else
						{
							//To throw error message to configure stock adjustment type
							nlapiLogExecution('ERROR',"Stock adjustment type is not configured for Task type Cycle Count");
						}

						var binloc = request.getLineItemValue('custpage_items', 'custpage_location', i + 1);
						nlapiLogExecution('ERROR',"binloc",binloc);
						var skuno = request.getLineItemValue('custpage_items', 'custpage_expsku', i + 1);
						var expqty1 = request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);
						var explp = request.getLineItemValue('custpage_items', 'custpage_explp', i + 1);
						var accoutno='';
						if(AccountNo!=null && AccountNo!='')
						{
							accoutno = AccountNo[1];
						}

						var actqty = request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);
						var expqty = request.getLineItemValue('custpage_items', 'custpage_expqty', i + 1);

						if(actqty=='' || actqty==null || isNaN(actqty))
							actqty=0;

						if(expqty=='' || expqty==null || isNaN(expqty))
							expqty=0;

						var actItem=request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1);
						var expItem=request.getLineItemValue('custpage_items', 'custpage_expsku', i + 1);

						var adjustqty = parseFloat(actqty) - parseFloat(expqty);

						if(resultQty1=='' || resultQty1==null || isNaN(resultQty1))
							resultQty1=0;

						var arrdims = new Array();
						if(vEmptyBinLoc=='T')
						{
							//createInvtAdjtRecord(binloc,actItem,0,explp,accoutno,sitelocn,actqty,actnewserialnoToNS,NSAdjustid);
							arrdims = getSKUCubeAndWeight(request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1), 1);
						}
						else
						{/*
							if(expItemId==ItemId && ((varianceinExpserialNo == null || varianceinExpserialNo == "") && (varianceinActserialNoNew == null || varianceinActserialNoNew == '')))
							{
								createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,adjustqty,serialnumbers,NSAdjustid);
							}
							else if(expItemId==ItemId && ((varianceinExpserialNo != null && varianceinExpserialNo != "") || (actnewserialnoToNS != null && actnewserialnoToNS != '')))
							{
								if(expserialnoToNS != null && expserialnoToNS != '')
									createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,-expvarianceqtytoNS,expserialnoToNS,NSAdjustid1);
								//createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,-expvarianceqty,expserialnoToNS);
								if(expserialnoNotforNS != null && expserialnoNotforNS != '')
									createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,-expvarianceqtynotforNS,expserialnoNotforNS,'');
								if(actnewserialnoToNS != null && actnewserialnoToNS != '')
									createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,actualvarianceqtyNew,actnewserialnoToNS,NSAdjustid);
							}
							else//have to check this cond when items are diff for a serial item						
							{
								createInvtAdjtRecord(binloc,expItem,expqty,explp,accoutno,sitelocn,-expqty,serialnumbers,NSAdjustid1);
								createInvtAdjtRecord(binloc,actItem,0,explp,accoutno,sitelocn,actqty,serialnumbers,NSAdjustid);
							}
							arrdims = getSKUCubeAndWeight(request.getLineItemValue('custpage_items', 'custpage_expsku', i + 1), 1);
						 */
							//  Case 20125356�Start Loc Remaining cube update
							arrdims = getSKUCubeAndWeight(expItemId, 1);
							//end	
						}

						if(resultQty1=='' || resultQty1==null || isNaN(resultQty1))
							resultQty1=0;

						nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:length11', arrdims.length);

						var itemCube = 0;
						var uomqty = 0;
						//if (arrdims["BaseUOMItemCube"] != "" && (!isNaN(arrdims["BaseUOMItemCube"]))) 
						/*	if (arrdims[0] != "" && (!isNaN(arrdims[0])))
						{
							nlapiLogExecution('ERROR','resultQty1', parseFloat(resultQty1));
							nlapiLogExecution('ERROR','arrdims[1]', parseFloat(arrdims[1]));
							if(parseFloat(resultQty1)>0)
							{
								uomqty = ((parseFloat(resultQty1))/(parseFloat(arrdims[1])));
							}
							else
							{
								uomqty = ((parseFloat(actqty1))/(parseFloat(arrdims[1])));
							}
							nlapiLogExecution('ERROR','uomqty',uomqty);
							itemCube = (parseFloat(uomqty) * parseFloat(arrdims[0]));
							nlapiLogExecution('ERROR','itemCubeinif', itemCube);

						} 
						else 
						{	
							nlapiLogExecution('ERROR','test1else', 'test1else');						
							itemCube = 0;
						}*/

						/*LocRemCube = GeteLocCube(binloc);
						var oldLocRemCube =0;	
						//  Case 20125356�Start Loc Remaining cube update 
						if(parseInt(resultQty1)>0)
						{
							oldremcube = (parseFloat(LocRemCube) - parseFloat(itemCube));
						}
						else
						{
							oldremcube = (parseFloat(LocRemCube) + parseFloat(itemCube));
						}*/
						//end
						//var oldremcube = (parseFloat(LocRemCube) + parseFloat(itemCube));			
						//	oldLocRemCube = parseFloat(oldremcube);
						//nlapiLogExecution('ERROR','itemCube', itemCube);
						//	nlapiLogExecution('ERROR','LocRemCube', LocRemCube);
						//	nlapiLogExecution('ERROR','LocRemCubeafteradding', oldLocRemCube);
						//var retValue = UpdateLocCubeNew(request.getLineItemValue('custpage_items', 'custpage_location', i + 1), oldLocRemCube);

						if (request.getParameter('restype') == 'close') {

							nlapiLogExecution('ERROR', 'request.getLineItemValue(custpage_items, custpage_planno, i + 1) new log', request.getLineItemValue('custpage_items', 'custpage_planno', i + 1));

							var vPlanNo = request.getLineItemValue('custpage_items', 'custpage_planno', i + 1);

							var filters = new Array();

							filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',vPlanNo));
							filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]));

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_invtid');     
							var List = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);
							if( List!=null && List!='')
							{ 

								for(var n =0; n<List.length; n++){

									try{
										var	invrecid = List[n].getValue('custrecord_invtid');

										var fieldNames = new Array(); 
										fieldNames.push('custrecord_ebiz_invholdflg');  
										fieldNames.push('custrecord_ebiz_cycl_count_hldflag');

										var newValues = new Array(); 
										newValues.push('F');
										newValues.push('F');

										nlapiSubmitField('customrecord_ebiznet_createinv', invrecid, fieldNames, newValues);

									}
									catch(e){
										if (e instanceof nlobjError) 
										{
											nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
										}

										else 
										{
											nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
										}
										nlapiLogExecution('ERROR','Unsuccessful to uncheck the inventory cycle count hold flag' + '\n' + 'Error: ' + e.toString());


									}


								}

							
							}

							nlapiSubmitField('customrecord_ebiznet_cylc_createplan', request.getLineItemValue('custpage_items', 'custpage_planno', i + 1), 
									'custrecord_cyccplan_close', 'T');							
						}
					}
					catch (e) {

						if (e instanceof nlobjError) 
						{
							nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
						}

						else 
						{
							nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
						}
						nlapiLogExecution('ERROR','Save unsuccessful into Create Inventory' + '\n' + 'Error: ' + e.toString());
					}

				} 
				else if(request.getParameter('restype') == 'ignore')
				{
					try
					{						
						var ItemId = request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1);

						var fields = ['recordType', 'custitem_ebizserialin'];
						var columns = nlapiLookupField('item', ItemId, fields);
						var ItemType = columns.recordType;
						nlapiLogExecution('ERROR','ItemType',ItemType);

						var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;	

						var serialnumbers = "";
						var localSerialNoArray = new Array();
						if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
						{
							var LP = request.getLineItemValue('custpage_items', 'custpage_actlp', i + 1);
							var actqty1 = request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);
							var expqty1 = request.getLineItemValue('custpage_items', 'custpage_expqty', i + 1);
							nlapiLogExecution('ERROR', 'expqty1', expqty1);	
							nlapiLogExecution('ERROR', 'actqty1', actqty1);	
							var resultQty1 = parseFloat(actqty1) - parseFloat(expqty1);
							nlapiLogExecution('ERROR', 'resultQty1', resultQty1);	

							//localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,parseFloat(resultQty1),ItemId,LP,request.getParameter('restype'),CyccRecID,'');
							AdjustSerialNumbers(localSerialNoArray,parseFloat(resultQty1),ItemId,LP,request.getParameter('restype'),CyccRecID,'');
						}

						var scount=1;
						LABL1: for(var j=0;j<scount;j++)
						{	
							nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', j);
							try
							{
								var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', request.getLineItemValue('custpage_items', 'custpage_invtid', i + 1));
								transaction.setFieldValue('custrecord_ebiz_invholdflg', 'F');
								transaction.setFieldValue('custrecord_ebiz_cycl_count_hldflag', 'F');
								transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
								var id = nlapiSubmitRecord(transaction, true);	

							}
							catch(ex)
							{
								var exCode='CUSTOM_RECORD_COLLISION'; 

								if (ex instanceof nlobjError) 
								{	
									wmsE=ex.getCode() + '\n' + ex.getDetails();
									exCode=ex.getCode();
								}
								else
								{
									wmsE=ex.toString();
									exCode=ex.toString();
								}				 
								if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
								{  
									scount=scount+1;
									continue LABL1;
								}
								else break LABL1;
							}
						}

					}
					catch (e) {

						if (e instanceof nlobjError) 
						{
							nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
						}

						else 
						{
							nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
						}
						nlapiLogExecution('ERROR','Save unsuccessful into Create Inventory when ignore status is choosen' + '\n' + 'Error: ' + e.toString());
					}
				}
				else if(request.getParameter('restype') == 'recount')
				{
					nlapiLogExecution('ERROR', '*************** Into recount', request.getParameter('restype'));
					try
					{						
						var ItemId = request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1);

						var fields = ['recordType','custitem_ebizserialin'];
						var columns = nlapiLookupField('item', ItemId, fields);
						var ItemType = columns.recordType;
						nlapiLogExecution('ERROR','ItemType',ItemType);

						var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;	

						var serialnumbers = "";
						var localSerialNoArray = new Array();
						if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
						{
							var LP = request.getLineItemValue('custpage_items', 'custpage_actlp', i + 1);
							var actqty1 = request.getLineItemValue('custpage_items', 'custpage_actqty', i + 1);
							var expqty1 = request.getLineItemValue('custpage_items', 'custpage_expqty', i + 1);
							nlapiLogExecution('ERROR', 'expqty1', expqty1);	
							nlapiLogExecution('ERROR', 'actqty1', actqty1);	
							//var resultQty1 = parseFloat(expqty1) - parseFloat(actqty1);
							var resultQty1 = parseFloat(actqty1) - parseFloat(expqty1);
							nlapiLogExecution('ERROR', 'resultQty1', resultQty1);	

							//localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,parseFloat(resultQty1),ItemId,LP,request.getParameter('restype'),CyccRecID,'');
							AdjustSerialNumbers(localSerialNoArray,parseFloat(resultQty1),ItemId,LP,request.getParameter('restype'),CyccRecID,'');
						}

					}
					catch (e) {

						if (e instanceof nlobjError) 
						{
							nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
						}

						else 
						{
							nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
						}
					}
				}

				try {
					nlapiLogExecution('ERROR','test1', 'test1');
					var vopenrecid=request.getLineItemValue('custpage_items', 'custpage_opentaskid', i + 1);
					nlapiLogExecution('ERROR','id', vopenrecid);
					nlapiLogExecution('ERROR','sku', request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1));
					nlapiLogExecution('ERROR','DateStamp', DateStamp());
					nlapiLogExecution('ERROR','TimeStamp', TimeStamp());
					nlapiLogExecution('ERROR','LPno', request.getLineItemValue('custpage_items', 'custpage_actlp', i + 1));

					var fieldNames = new Array(); 
					fieldNames.push('custrecord_act_end_date');  
					fieldNames.push('custrecord_actualendtime');
					fieldNames.push('custrecord_sku');
					fieldNames.push('custrecord_act_qty');
					fieldNames.push('custrecord_lpno');

					var newValues = new Array(); 
					newValues.push(DateStamp());
					newValues.push(TimeStamp());
					newValues.push(request.getLineItemValue('custpage_items', 'custpage_actsku', i + 1));
					newValues.push(parseFloat(actqty).toFixed(5));
					newValues.push(request.getLineItemValue('custpage_items', 'custpage_actlp', i + 1));
					if (request.getParameter('restype') == 'resolve') {
						fieldNames.push('custrecord_wms_status_flag'); 
						newValues.push('22');
					}
					if (request.getParameter('restype') == 'ignore') {
						fieldNames.push('custrecord_wms_status_flag'); 
						newValues.push('21');
					}
					fieldNames.push('custrecord_upd_date');
					newValues.push(DateStamp());
					fieldNames.push('custrecord_sku_status');

					var OldItemstatus = request.getLineItemValue('custpage_items', 'custpage_expskustatusvalue', i + 1);
					var NewItemStatus = request.getLineItemValue('custpage_items', 'custpage_actskustatusvalue', i + 1);
					var vitemstatus = OldItemstatus;
					nlapiLogExecution('ERROR','vitemstatus before if', vitemstatus);
					if(NewItemStatus !=null && NewItemStatus !='')
					{
						if(parseInt(NewItemStatus)!= parseInt(OldItemstatus))
						{
							vitemstatus = NewItemStatus;
						}

					}
					nlapiLogExecution('ERROR','vitemstatus after if', vitemstatus);
					newValues.push(vitemstatus);
					if(vopenrecid!=null && vopenrecid!='')//case# 20148167 20148647 20148170
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', vopenrecid, fieldNames, newValues);

				} 
				catch (e) {
					nlapiLogExecution('ERROR','Save unsuccessful into OPEN TASK' + '\n' + 'Error: ' + e.toString());
				}
			}
		}
	}
}


function GetNSCallFlag(Status,NewStatus)
{
	nlapiLogExecution('ERROR', 'Into GetNSCallFlag', '');
	nlapiLogExecution('ERROR', 'Status', Status);
	nlapiLogExecution('ERROR', 'New Status', NewStatus);
	var result=new Array();
	var retFlag='N';
	var OldMapLocation;
	var NewMapLocation;
	var OldLocation;
	var NewLocation;
	if(Status!=null && Status!="")
	{
		var OldIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', Status);
		if(OldIStatus!= null && OldIStatus != "")
		{
			OldMapLocation=OldIStatus.getFieldValue('custrecord_item_status_location_map');
			OldLocation=OldIStatus.getFieldValue('custrecord_ebizsiteskus');
			OldIStatus=null;
		} 
	}
	if(NewStatus!=null && NewStatus!="")
	{
		var NewIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', NewStatus);
		if(NewIStatus!= null && NewIStatus != "")
		{
			NewMapLocation=NewIStatus.getFieldValue('custrecord_item_status_location_map');
			NewLocation=NewIStatus.getFieldValue('custrecord_ebizsiteskus');
		} 
	}
	nlapiLogExecution('ERROR', 'OldLocation', OldLocation);
	nlapiLogExecution('ERROR', 'NewLocation', NewLocation);
	if(OldMapLocation != null && OldMapLocation !="" && NewMapLocation != null && NewMapLocation != "")
	{
		if(OldMapLocation==NewMapLocation)
			//if(OldLocation==NewLocation)
		{
			retFlag='N';
			result.push('N');
		}
		else
		{
			result.push('Y');
		}
		result.push(OldMapLocation);
		result.push(NewMapLocation);
		result.push(OldLocation);
		result.push(NewLocation);
	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'OldMapLocation', OldMapLocation);
	nlapiLogExecution('ERROR', 'NewMapLocation', NewMapLocation);
	nlapiLogExecution('ERROR', 'Out of GetNSCallFlag', '');
	return result;
}


function GetMWFlag(WHLocation)
{
	nlapiLogExecution('ERROR', 'Into GetMWFlag : WHLocation', WHLocation);
	var retFlag='Y';
	var MWHSiteFlag;

	if(WHLocation!=null && WHLocation!="")
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', WHLocation));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebizwhsite')); 

		var WHOld = nlapiSearchRecord('location', null, filters, columns);

		if(WHOld!= null && WHOld != "" && WHOld.length>0)
		{
			MWHSiteFlag=WHOld[0].getValue('custrecord_ebizwhsite');
			nlapiLogExecution('ERROR', 'MWHSiteFlag', MWHSiteFlag);
			WHOld=null;
		} 
	} 
	if(MWHSiteFlag != null && MWHSiteFlag !="" )
	{
		if(MWHSiteFlag=='F')
			retFlag='N';
		else
			retFlag='Y';

	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'Out of GetMWFlag ', '');
	return retFlag;
}

function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot)
{
	try{
		nlapiLogExecution('ERROR', 'Into InvokeNSInventoryTransfer');		
		nlapiLogExecution('ERROR', 'item', item);		
		nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
		nlapiLogExecution('ERROR', 'loc', loc);
		nlapiLogExecution('ERROR', 'toloc', toloc);
		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'lot', lot);

		var ItemType='';

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType','custitem_ebizserialout' ]);
			ItemType = columns.recordType;
		}

		nlapiLogExecution('ERROR', 'ItemType', ItemType);
		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;	

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails =  nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}

		var invttransfer = nlapiCreateRecord('inventorytransfer');			

		var subs = nlapiGetContext().getFeature('subsidiaries');		
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);
		}		

		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));

//		case no 20126110
		//if(lot!=null && lot!='')
		//case# 20148170 starts
		//if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
		if(lot!=null && lot!='')
		{

			vItemname=vItemname.replace(/ /g,"-");

			if(confirmLotToNS=='N')
				lot=vItemname;	

			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

			/*	if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				nlapiLogExecution('ERROR', 'test1', 'test1');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
				invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
			}
		}
		else if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
		{
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
			invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot);
		}*/

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				// case no 20126516
				var templot = lot.split(',');

				nlapiLogExecution('ERROR', 'templot', templot.length);

				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					for(var t=0; t< templot.length; t++)
					{
						nlapiLogExecution('ERROR', 'test11', templot[t]);
						compSubRecord.selectNewLineItem('inventoryassignment');
						compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
						//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
						compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', templot[t]);

						compSubRecord.commitLineItem('inventoryassignment');
					}
				}
				else
				{
					nlapiLogExecution('ERROR', 'test1', 'test1');
					compSubRecord.selectNewLineItem('inventoryassignment');
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
					//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

					compSubRecord.commitLineItem('inventoryassignment');
				}

				compSubRecord.commit();
			}
			else
			{
				if(parseInt(qty)<0)
					qty=parseInt(qty)*(-1);

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					nlapiLogExecution('DEBUG', 'LotNowithQty', lot);
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  lot);
				}	
				else
				{
					nlapiLogExecution('DEBUG', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseInt(qty) + ")");
				}
			}
		}

		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);
		nlapiLogExecution('ERROR', 'Out of InvokeNSInventoryTransfer');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}
}

function createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,whsite,adjustqty,serialnumbers,NSAdjustid)
{
	var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj'); 
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_binloc', binloc);
	invAdjustCustRecord.setFieldValue('custrecord_ebizskuno', skuno);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_expeqty', parseFloat(expqty).toFixed(5));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_invjlpno', explp);

	if(accoutno!=null && accoutno!='')
	{
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjtype', accoutno);
	}

	invAdjustCustRecord.setFieldValue('custrecord_ebiz_siteid', whsite);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustqty', parseFloat(adjustqty).toFixed(5));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_tasktype', 7);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_recorddate', DateStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_rectime', TimeStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_updateduserno', nlapiGetContext().getUser());
	if(NSAdjustid != null && NSAdjustid != '')
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno', NSAdjustid);//NS asdjustment id
	if(serialnumbers != null && serialnumbers != '')
	{
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjserialnumbers', serialnumbers);
	}

	var id = nlapiSubmitRecord(invAdjustCustRecord, false, true);
}

function getSKUCubeAndWeight(skuNo, uom){
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight', 'Start');
	var timestamp1 = new Date();
	var cube = 0;
	var BaseUOMQty = 0;    
	var dimArray = new Array();
	if (uom == "")
		uom = 1;

	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:SKU info', skuNo);
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:UOM', uom);

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', skuNo);
	//case # 20123248 start
	filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', uom);
	//case # 20123248 end
	filters[2] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var t1 = new Date();
	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSearchRecord:getSKUCubeAndWeight:skudims',
			getElapsedTimeDuration(t1, t2));

	if(skuDimsSearchResults != null){
		for (var i = 0; i < skuDimsSearchResults.length; i++) {
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	dimArray[0] = cube;
	dimArray[1] = BaseUOMQty;

	//dimArray["BaseUOMItemCube"] = cube;
	//dimArray["BaseUOMQty"] = BaseUOMQty;

	var timestamp2 = new Date();
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight Duration', getElapsedTimeDuration(timestamp1, timestamp2));

	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:Cube', cube);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:qty', BaseUOMQty);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:length', dimArray.length);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:Cube1', dimArray[0]);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:qty1', dimArray[1]);
	return dimArray;
}

function GeteLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('DEBUG', 'GeteLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('DEBUG', 'GeteLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}

function getLotBatchId(lotbatchtext,skuid)
{
	var batchid='';
	var filters = new Array();          
	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is',lotbatchtext));
	if(skuid!=null&&skuid!="")
		filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',skuid));

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	var batchdetails =  nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
	if (batchdetails !=null && batchdetails !='' && batchdetails.length>0) 
	{
		batchid=batchdetails[0].getValue('internalid');
	}
	return batchid;
}


/**
 * Passing item,item status, qty and location to NS
 * 
 * @param item
 * @param itemstatus
 * @param loc
 * @param qty
 * @param notes
 */
function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,AccNo)
{
	try{
		var vCost;
		var vAvgCost;
		var vItemname;
		var NSid='';
		var confirmLotToNS='N';
		nlapiLogExecution('ERROR', 'item::', item);
		nlapiLogExecution('ERROR', 'itemstatus::', itemstatus);
		nlapiLogExecution('ERROR', 'Location info::', loc);
		nlapiLogExecution('ERROR', 'Task Type::', tasktype);
		nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
		nlapiLogExecution('ERROR', 'qty::', qty);
		nlapiLogExecution('ERROR', 'AccNo::', AccNo);
		nlapiLogExecution('ERROR', 'lot::', lot);
		//alert("AccNo :: "+ AccNo);
		confirmLotToNS=GetConfirmLotToNS(loc);

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));

		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails =  nlapiSearchRecord('item', null, filters, columns);
		//alert("itemdetails " + itemdetails);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
		}		

		var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
		nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
		if(AccNo==null || AccNo=="")
		{		
			AccNo = getAccountNo(loc,vItemMapLocation);
			nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
		}
		//alert("NS Qty "+ parseFloat(qty));
		//	if(lot==null || lot=='' || lot=='null')
		//	{
		var outAdj = nlapiCreateRecord('inventoryadjustment');			

		outAdj.setFieldValue('account', AccNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.setFieldValue('adjlocation', vItemMapLocation);
		outAdj.selectNewLineItem('inventory');
		outAdj.setCurrentLineItemValue('inventory', 'item', item);
		outAdj.setCurrentLineItemValue('inventory', 'location', vItemMapLocation);	
		outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
		var subs = nlapiGetContext().getFeature('subsidiaries');
		nlapiLogExecution('Debug', 'subs', subs);
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
		}
		//	}

		outAdj.setCurrentLineItemValue('inventory', 'unitcost','');
		//alert('Hi1');
		//alert("vAvgCost "+ vAvgCost);
		/*if(vAvgCost != null &&  vAvgCost != "" && outAdj!=null && outAdj!='' && outAdj!='null')	
		{
			nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
			outAdj.setCurrentLineItemValue('inventory', 'unitcost', vAvgCost);
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
			if(outAdj!=null && outAdj!='' && outAdj!='null')	
				outAdj.setCurrentLineItemValue('inventory', 'unitcost',  vCost);
		}
*/
		var vAdvBinManagement=false;

		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

		var vItemType="";


		if(lot!=null && lot!='')
		{
			var fields = ['recordType','custitem_ebizserialin'];
			var columns = nlapiLookupField('item', item, fields);
			vItemType = columns.recordType;
			nlapiLogExecution('ERROR','vItemType',vItemType);

			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;	

			if(vAdvBinManagement)
			{
				if (vItemType == "lotnumberedinventoryitem" || vItemType == "lotnumberedassemblyitem")
				{
					var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');

					if(confirmLotToNS=='N')
						lot=vItemname;			

					nlapiLogExecution('ERROR', 'lot', lot);

					compSubRecord.selectNewLineItem('inventoryassignment');
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

					compSubRecord.commitLineItem('inventoryassignment');

					compSubRecord.commit();

				}
			}
			if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
			{
				vItemname=vItemname.replace(/ /g,"-");

				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);

				if(confirmLotToNS=='N')
					lot=vItemname;			

				nlapiLogExecution('ERROR', 'lot', lot);

				nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
			}
			else if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T")
			{
				//case # 20127185ï¿½,split based on ,.
				/*var cnt=lot.split(',');
				nlapiLogExecution('ERROR', 'cnt', cnt);


				for(var n=0;n<cnt.length;n++)
				{

					//var lotnumbers=cnt[n];
				   var lotnumbers = lot.split(',');*/
				var cnt;
				var lotnumbers;

				if(lot!=null && lot!='' && lot!='null' && serialInflg != "T")
				{
					cnt=lot.split(',');
					lotnumbers = lot.split(',');
				}


				nlapiLogExecution('ERROR', 'lotnumbers', lotnumbers);
				if(lotnumbers!=null && lotnumbers!='' && lotnumbers!='null')
				{

					var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
					nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
					nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
					if(AccNo==null || AccNo=="")
					{		
						AccNo = getAccountNo(loc,vItemMapLocation);
						nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
					}
					var outAdj = nlapiCreateRecord('inventoryadjustment');	
					outAdj.setFieldValue('account', AccNo);
					outAdj.setFieldValue('memo', notes);
					outAdj.setFieldValue('adjlocation', vItemMapLocation);
					outAdj.selectNewLineItem('inventory');
					outAdj.setCurrentLineItemValue('inventory', 'item', item);
					outAdj.setCurrentLineItemValue('inventory', 'location', vItemMapLocation);	
					outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
					//Case # 20126291,20126107,20126190  start
					if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
					{

						var	compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');

						//var tempQtyArray=lotnumbers.split(',');
						var tempQtyArray=lotnumbers;

						for (var x2 = 0; x2 < tempQtyArray.length; x2++)
						{
							var tempQty;

							if(parseFloat(qty)<0)
							{
								tempQty=-1;
							}
							else
							{
								tempQty=1;
							}

							/*if(x==0)
								var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');*/
							compSubRecord.selectNewLineItem('inventoryassignment');
							//case # 20126232� start
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQty);
							//case # 20126232� end
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempQtyArray[x2]);

							compSubRecord.commitLineItem('inventoryassignment');

						}
						compSubRecord.commit();
					}
					else
					{
						nlapiLogExecution('ERROR', 'LotNowithQty', lot);
						outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lot);
					}
					//Case # 20126291,20126107,20126190  End

					var subs = nlapiGetContext().getFeature('subsidiaries');
					nlapiLogExecution('Debug', 'subs', subs);
					if(subs==true)
					{
						var vSubsidiaryVal=getSubsidiaryNew(loc);
						nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
						if(vSubsidiaryVal != null && vSubsidiaryVal != '')
							outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
					}
					outAdj.setCurrentLineItemValue('inventory', 'unitcost','');					


					/*if(vAvgCost != null &&  vAvgCost != "" && outAdj!=null && outAdj!='' && outAdj!='null')	
					{
						nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
						outAdj.setCurrentLineItemValue('inventory', 'unitcost', vAvgCost);
					}
					else
					{
						nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
						if(outAdj!=null && outAdj!='' && outAdj!='null')	
							outAdj.setCurrentLineItemValue('inventory', 'unitcost',  vCost);
					}*/

					nlapiLogExecution('ERROR', 'lot with serial no', lotnumbers);

					nlapiLogExecution('ERROR', 'LotNowithQty', lotnumbers);
					//outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lotnumbers);

					//Case # 20127185 end
					var subs = nlapiGetContext().getFeature('subsidiaries');
					nlapiLogExecution('ERROR', 'subs', subs);
					if(subs==true)
					{
						var vSubsidiaryVal=getSubsidiaryNew(loc);
						if(vSubsidiaryVal != null && vSubsidiaryVal != '')
							outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
						nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
					}
					/*outAdj.commitLineItem('inventory');
						NSid=nlapiSubmitRecord(outAdj, true, true);
						//alert("Success NS");
						nlapiLogExecution('ERROR', 'NSid', NSid);*/
				}
				outAdj.commitLineItem('inventory');
				NSid=nlapiSubmitRecord(outAdj, true, true);
				//alert("Success NS");
				nlapiLogExecution('ERROR', 'NSid', NSid);
				//}	
			}
			//}
		}
		//alert('Hi3');
		if (vItemType != "serializedinventoryitem" && vItemType != "serializedassemblyitem" && serialInflg != "T")
		{
			var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
			nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
			if(AccNo==null || AccNo=="")
			{		
				AccNo = getAccountNo(loc,vItemMapLocation);
				nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
			}
			var subs = nlapiGetContext().getFeature('subsidiaries');
			nlapiLogExecution('ERROR', 'subs', subs);
			if(subs==true)
			{
				var vSubsidiaryVal=getSubsidiaryNew(loc);
				if(vSubsidiaryVal != null && vSubsidiaryVal != '')
					outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
				nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
			}
			outAdj.commitLineItem('inventory');
			NSid=nlapiSubmitRecord(outAdj, true, true);
			//alert("Success NS");
			nlapiLogExecution('ERROR', 'NSid', NSid);

			nlapiLogExecution('ERROR', 'type argument', 'type is create');
		}
	}


	catch(e) {
		if (e instanceof nlobjError) 
		{
			// nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
			nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
		}

		else 
		{
			//nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
		}

		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryAdjustmentNew ', e);
		//alert("Error" + exp.toString());

	}
	return NSid;
}

function getAccountNo(location,itemstatusmaploc)
{
	nlapiLogExecution('ERROR', 'location:', location);
	nlapiLogExecution('ERROR', 'itemstatusmaploc:', itemstatusmaploc);
	var accountno;	
	var filtersAccNo = new Array(); 
	filtersAccNo.push(new nlobjSearchFilter('custrecord_location', null, 'is', location));
	filtersAccNo.push(new nlobjSearchFilter('custrecord_inventorynslocation', null, 'is', itemstatusmaploc));        						
	var colsAcc = new Array();
	colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
	var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);
	nlapiLogExecution('ERROR', 'Accsearchresults', Accsearchresults);
	if(Accsearchresults!=null)
		accountno = Accsearchresults[0].getValue('custrecord_accountno');	
	return accountno;
}

function getStockAdjustmentAccountNoNew(loc,tasktype,adjusttype)
{	
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();
	//alert("loc : "+loc);
	if(loc!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location',null,'anyof',[loc]));	

	if(tasktype!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_adjusttasktype',null,'anyof',[tasktype]));


	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');
	colsStAccNo[2] = new nlobjSearchColumn('internalid');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	//alert("StockAdjustAccResults " + StockAdjustAccResults);
	//alert("StockAdjustAccResults " + StockAdjustAccResults.length);
	if(StockAdjustAccResults !=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		//alert("1 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('internalid'));

		//alert("2 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
		if(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != null && StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != "")
			StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));


	}	

	return StAdjustmentAccountNo;	
}

/**
 * Updates the bin location volume for the specified location
 * @param locnId
 * @param remainingCube
 */
function UpdateLocCubeNew(locnId, remainingCube){
	//alert("Val1 "+ locnId);
	//alert("Val2 "+ remainingCube);
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_remainingcube';
	values[0] = parseFloat(remainingCube).toFixed(5);
	//alert("parse "+ parseFloat(remainingCube));
	//nlapiLogExecution('ERROR','Location Internal Id', locnId);

	//nlapiLogExecution('ERROR','Location Internal IdremainingCube', remainingCube);

	//alert("into");
	//var t1 = new Date();
	nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
	//alert("Success");
	//var t2 = new Date();
	//nlapiLogExecution('ERROR', 'nlapiSubmitField:UpdateLocCube:location', getElapsedTimeDuration(t1, t2));
}

var searchResultArray=new Array();
function getOrdersForCycleCount(request,maxno,form){

	var localVarArray;
	//this is to maintain the querystring parameters while refreshing the paging drop down
	if (request.getMethod() != 'POST') {
		var queryparams=request.getParameter('custpage_qeryparams');
		nlapiLogExecution('Error', 'queryparams', queryparams);
		// queryparams=queryparams.
		var tempArray=new Array();
		tempArray.push(queryparams.split(','));
		localVarArray=tempArray;
		nlapiLogExecution('Error', 'tempArray', tempArray.length);
	}
	else
	{
		localVarArray = validateRequestParams(request,form);
		nlapiLogExecution('Error', 'localVarArrayinelse', localVarArray);
	}

	// Get all the search filters for wave generation
	var filters = new Array();   
	filters = specifyInvtFilters(localVarArray,maxno);
	nlapiLogExecution('Error', 'afterspecifyfilters', localVarArray);
	// Adding search columns
	var columns = new Array();
	columns=addColumnsForSearch();
	nlapiLogExecution('Error', 'afterspecifycolumns', localVarArray);
	var orderList = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);
	if( orderList!=null && orderList.length>=1000)
	{ 
		nlapiLogExecution('Error', 'orderList.length', orderList.length);
		searchResultArray.push(orderList); 
		var maxno=orderList[orderList.length-1].getValue(columns[21]);
		getOrdersForCycleCount(request,maxno,form);	
	}
	else
	{
		if(orderList!=null && orderList!='' && orderList.length>0)
		{
			nlapiLogExecution('Error', 'orderList.length', orderList.length);
			searchResultArray.push(orderList);
		}
	}
	return searchResultArray;

}


function setPagingForSublist(orderList,form)
{
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';

		if(orderListArray.length>0)
		{

			if(orderListArray.length>20)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("20");
					pagesizevalue=20;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{pagesizevalue= request.getParameter('custpage_pagesize');}
					else
					{pagesizevalue= 20;pagesize.setDefaultValue("20");}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}
			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{

					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}


			var c=0;
			var minvalue;
			minvalue=minval;

			for(var j = minvalue; j < maxval; j++){		

				var currentOrder = orderListArray[j];
				setLineItemValuesFromSearch(form, currentOrder, c);
//				addFulfilmentOrderToSublist(form, currentOrder, c);
				c=c+1;
			}
		}
	}
}


function addFieldsToForm(form)
{
	/*
	 * Defining SubList lines
	 */
	var sublist = form.addSubList("custpage_items", "list", "Resolve");
	sublist.addMarkAllButtons();
	sublist.addField("custpage_rec", "checkbox", "Record").setDefaultValue('T');

	sublist.addField("custpage_locationtext", "text", "Location", "customrecord_ebiznet_location").setDisplayType('inline');
	sublist.addField("custpage_location", "text", "Location", "customrecord_ebiznet_location").setDisplayType('hidden');
	sublist.addField("custpage_expsku", "text", "Expected Item", "item").setDisplayType('hidden');
	sublist.addField("custpage_expskutext", "text", "Expected Item", "item").setDisplayType('inline');
	//var actsku = sublist.addField("custpage_actsku", "select", "Actual SKU", "item").setDisplayType('inline');
	sublist.addField("custpage_actsku", "text", "Actual Item", "item").setDisplayType('hidden');
	//sublist.addField("custpage_actskutext", "text", "Actual Item", "item").setDisplayType('inline');

	sublist.addField("custpage_actskutextlink", "textarea", "Actual Item", "item");

	sublist.addField("custpage_expqty", "text", "Expected Qty");
	var actqty = sublist.addField("custpage_actqty", "text", "Actual Qty");
	sublist.addField("custpage_qtyvariance", "text", "Qty Variance");
	sublist.addField("custpage_dollarvariance", "text", "$ Variance");
	sublist.addField("custpage_explp", "text", "Expected LP");
	var actlp = sublist.addField("custpage_actlp", "text", "Actual LP"); 
	sublist.addField("custpage_expbatch", "text", "Expected LOT#").setDisplayType('inline');
	sublist.addField("custpage_actbatch", "text", "Actual LOT#").setDisplayType('inline');
	sublist.addField("custpage_expskustatus", "text", "Expected Item Status");
	sublist.addField("custpage_expskustatusvalue", "text", "Expected SKU Status value").setDisplayType('hidden');
	sublist.addField("custpage_actskustatus", "text", "Actual Item Status").setDisplayType('inline');
	sublist.addField("custpage_actskustatusvalue", "text", "Actual SKU Status value").setDisplayType('hidden');
	sublist.addField("custpage_currentinvt", "text", "Current Inventory");
	sublist.addField("custpage_qtytobeadjust", "text", "Qty to be Adjusted").setDisplayType('entry');	

	sublist.addField("custpage_id", "text", "ID").setDisplayType('hidden');
	sublist.addField("custpage_planno", "text", "Plan#").setDisplayType('hidden');
	sublist.addField("custpage_opentaskid", "text", "Open Task ID").setDisplayType('hidden');
	sublist.addField("custpage_invtid", "text", "Invt ID").setDisplayType('hidden');
	sublist.addField("custpage_name", "text", "Name").setDisplayType('hidden');
	sublist.addField("custpage_date", "text", "date").setDisplayType('hidden').setDefaultValue(DateStamp());
	sublist.addField("custpage_time", "text", "time").setDisplayType('hidden').setDefaultValue(TimeStamp());
	sublist.addField("custpage_site", "text", "Site").setDisplayType('hidden');	
	sublist.addField("custpage_expserial", "textarea", "Expected Serial#").setDisplayType('hidden');	
	sublist.addField("custpage_actserial", "textarea", "Actual Serial#").setDisplayType('hidden');	
	sublist.addField("custpage_currentqoh", "text", "Current QOH Inventory").setDisplayType('hidden');
}


function addColumnsForSearch()
{	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_cycact_beg_loc');
	columns[1] = new nlobjSearchColumn('custrecord_cyclecount_exp_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_cycleact_sku');
	columns[3] = new nlobjSearchColumn('custrecord_cycleexp_qty');
	columns[4] = new nlobjSearchColumn('custrecord_cycle_act_qty');
	columns[5] = new nlobjSearchColumn('custrecord_cyclelpno');
	columns[6] = new nlobjSearchColumn('custrecord_cycact_lpno');
	columns[7] = new nlobjSearchColumn('custrecord_cycle_count_plan');
	columns[8] = new nlobjSearchColumn('custrecord_opentaskid');
	columns[9] = new nlobjSearchColumn('custrecord_invtid');                
	columns[10] = new nlobjSearchColumn('custrecord_expcyclesku_status');
	columns[11] = new nlobjSearchColumn('custrecord_cyclesku_status');
	columns[12] = new nlobjSearchColumn('custrecord_cyclesite_id');
	columns[13] = new nlobjSearchColumn('custrecord_cyclesku'); 
	columns[14] = new nlobjSearchColumn('custrecord_cyclecount_act_ebiz_sku_no');
	columns[15] = new nlobjSearchColumn('custrecord_expcycleabatch_no');
	columns[16] = new nlobjSearchColumn('custrecord_cycleabatch_no');

	columns[17] = new nlobjSearchColumn('custrecord_cycle_serialno');
	columns[18] = new nlobjSearchColumn('custrecord_cycle_expserialno');

	return columns;

	
}

function specifyInvtFilters(localVarArray,maxno)
{
	nlapiLogExecution('ERROR','localVarArray[0]',localVarArray[0]);
	// define search filters
	var filters = new Array();

	if(localVarArray[0][0] != "" && localVarArray[0][0] != null)
		filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',localVarArray[0][0]));

	if(localVarArray[0][1] != "" && localVarArray[0][1] != null)
		filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'anyof',localVarArray[0][1]));
	if(localVarArray[0][2] != "" && localVarArray[0][2] != null)
		filters.push(new nlobjSearchFilter('custrecord_cycleact_sku', null, 'anyof',localVarArray[0][2]));// case# 201416369
		//filters.push(new nlobjSearchFilter('custrecord_cyclesku', null, 'anyof',localVarArray[0][2]));
	if(localVarArray[0][3] != "" && localVarArray[0][3] != null)
		filters.push(new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof',localVarArray[0][3]));

	filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [31]));//Cycle Count Record
	var vRoleLocation=getRoledBasedLocation();
	if((localVarArray[0][1]==null || localVarArray[0][1]=='') && (vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0))
	{
		filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
	}
	return filters;


	// define search filters
//	var filters = new Array();

//	var filters = new Array();

//	if(vfilterarr[0] != "" && vfilterarr[0] != null)
	//	filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',vfilterarr[0]));

//	if(vfilterarr[1] != "" && vfilterarr[1] != null)
	//	filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'anyof',vfilterarr[1]));
//	if(vfilterarr[2] != "" && vfilterarr[2] != null)
	//	filters.push(new nlobjSearchFilter('custrecord_cyclesku', null, 'anyof',vfilterarr[2]));
//	if(vfilterarr[3] != "" && vfilterarr[3] != null)
	//	filters.push(new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof',vfilterarr[3]));

//	filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [31]));//Cycle Count Record
//	var vRoleLocation=getRoledBasedLocation();
//	if((vfilterarr[1]==null || vfilterarr[1]=='') && (vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0))
//	{
//	filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
//	}
//	return filters;







}

function validateRequestParams(request,form)
{
	var CyclecountPlan = "";
	var CycwhLoc = "";
	var CycItem = "";
	var CycBinloc = "";
	var localVarArray = new Array();
	if (request.getParameter('custpage_cyclecountplan') != null && request.getParameter('custpage_cyclecountplan') != "") {
		CyclecountPlan = request.getParameter('custpage_cyclecountplan');
	}

	if (request.getParameter('custpage_cyclecountwhlocation') != null && request.getParameter('custpage_cyclecountwhlocation') != "") {
		CycwhLoc = request.getParameter('custpage_cyclecountwhlocation');
	}

	if (request.getParameter('custpage_cyclecountitem') != null && request.getParameter('custpage_cyclecountitem') != "") {

		CycItem = request.getParameter('custpage_cyclecountitem');
	}

	if (request.getParameter('custpage_cyclecountbinloc') != null && request.getParameter('custpage_cyclecountbinloc') != "") {
		CycBinloc = request.getParameter('custpage_cyclecountbinloc');
	}

	if (request.getParameter('custpage_qeryparams')==null )
	{
		var hiddenQueryParams=form.getField('custpage_qeryparams');
		nlapiLogExecution('ERROR','localVarArray',localVarArray.toString());
		hiddenQueryParams.setDefaultValue(localVarArray.toString());
	}

	nlapiLogExecution('ERROR', 'CycwhLoc', CycwhLoc);
	nlapiLogExecution('ERROR', 'CycItem', CycItem);
	nlapiLogExecution('ERROR', 'CycBinloc', CycBinloc);

	var currentRow = [CyclecountPlan,CycwhLoc,CycItem,CycBinloc];

	localVarArray.push(currentRow);
	return localVarArray;

}


/*
 * Set line item level values based on search results
 */
function setLineItemValuesFromSearch(form, searchResults,i){
	var loc,locText,expsku,expskutext,actskutext,actsku,expqty,actqty,explp,actlp,expskustatus,actskustatus;
	var actskustatusvalue,planno,name,opentaskid,intid,siteis,actlot,explot,expskustatusvalue,expserialno,actserialno;
	var dollarvariance;

	/*
	 *  Searching records from custom record and dynamically adding to
	 * the sublist lines
	 */
	var searchresult = searchResults;
	nlapiLogExecution('ERROR', 'searchresults.length', searchresult.length);

	loc = searchresult.getValue('custrecord_cycact_beg_loc');
	locText = searchresult.getText('custrecord_cycact_beg_loc');
	expsku = searchresult.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
	expskutext = searchresult.getText('custrecord_cyclesku');
	//actsku = searchresult.getValue('custrecord_cycleact_sku');
	actskutext = searchresult.getText('custrecord_cycleact_sku');
	actsku = searchresult.getValue('custrecord_cyclecount_act_ebiz_sku_no');
	expqty = searchresult.getValue('custrecord_cycleexp_qty');
	actqty = searchresult.getValue('custrecord_cycle_act_qty');
	explp = searchresult.getValue('custrecord_cyclelpno');
	actlp = searchresult.getValue('custrecord_cycact_lpno');

	expserialno = searchresult.getValue('custrecord_cycle_expserialno');
	actserialno = searchresult.getValue('custrecord_cycle_serialno');

	nlapiLogExecution('ERROR', 'expserialno in show all', expserialno);		
	nlapiLogExecution('ERROR', 'actserialno in show all', actserialno);		

	expskustatus = searchresult.getText('custrecord_expcyclesku_status');
	expskustatusvalue = searchresult.getValue('custrecord_expcyclesku_status');
	actskustatus = searchresult.getText('custrecord_cyclesku_status');
	actskustatusvalue = searchresult.getValue('custrecord_cyclesku_status');

	if(actskustatus == null || actskustatus == "")
		actskustatus = expskustatus;
	if(actskustatusvalue == null || actskustatusvalue == "")
		actskustatusvalue = expskustatusvalue;

	planno = searchresult.getValue('custrecord_cycle_count_plan');
	name = searchresult.getValue('custrecord_cycle_count_plan');
	opentaskid = searchresult.getValue('custrecord_opentaskid');
	invtid = searchresult.getValue('custrecord_invtid');
	siteid = searchresult.getValue('custrecord_cyclesite_id');
	actlot = searchresult.getValue('custrecord_cycleabatch_no');
	explot = searchresult.getValue('custrecord_expcycleabatch_no');

	var vItemname='';
	var vCost=0;
	var vAvgCost=0;

	//Code Added By Ganapathi on 26th Nov 2012
	if(actsku!=null && actsku!='')
	{
		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',actsku));
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);          
		}
	}

	if(actqty==null||actqty=="")
	{
		nlapiLogExecution('ERROR', 'actqty:', actqty);
		actqty=0;
	}

	if(expqty==null||expqty=="")
	{
		nlapiLogExecution('ERROR', 'expqty: ', expqty);
		expqty=0;
	}

	dollarvariance = (parseFloat(actqty) - parseFloat(expqty)) * vAvgCost;

	nlapiLogExecution('ERROR','dollarvariance',dollarvariance);

	if(isNaN(dollarvariance)||dollarvariance==null||dollarvariance=="")
	{
		nlapiLogExecution('ERROR', 'dollarvariance: ', dollarvariance);
		dollarvariance=0;
	}
	else
	{
		//dollarvariance=Math.round(dollarvariance,2);	
		dollarvariance = Math.round(dollarvariance * 100) / 100;
		//parseFloat(dollarvariance).round(2);

	}

	//code to get custpage_qtytobeadjust value based on RF picking done between record and resolve

	nlapiLogExecution('ERROR','actsku',actsku);
	nlapiLogExecution('ERROR','loc',loc);
	nlapiLogExecution('ERROR','actskustatusvalue',actskustatusvalue);
	nlapiLogExecution('ERROR','expskustatusvalue',expskustatusvalue);
	nlapiLogExecution('ERROR','actlp',actlp);

	var QoH=0;
	var NewExpQty=0,NewActQty=0,ExpQuantity=0,QtytobeAdjusted=0,SkuStatusValue;

	if(actskustatusvalue == null || actskustatusvalue == '' || actskustatusvalue=='null')
	{
		actskustatusvalue = expskustatusvalue;
	}
	var filterslp = new Array();
	//filterslp[0] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [2, 10]);
	filterslp.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',[19]));

	if(actsku!=null && actsku!='')	
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof',actsku));

	if(loc!=null && loc!='')	
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof',loc));

	if(actskustatusvalue!=null && actskustatusvalue!='')
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof',actskustatusvalue));

	if(actlp!=null && actlp!='')
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is',actlp));


	var columnlp=new Array();
	columnlp[0]= new nlobjSearchColumn('custrecord_ebiz_qoh');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filterslp,columnlp);

	if(searchresults != null && searchresults != '')
	{
		QoH = searchresults[0].getValue('custrecord_ebiz_qoh');
		if(parseFloat(QoH) != parseFloat(expqty))
		{
			nlapiLogExecution('ERROR', 'in not equal if QoH', QoH);  
			nlapiLogExecution('ERROR', 'in not equal if expqty', expqty);  
			nlapiLogExecution('ERROR', 'in not equal if actqty', actqty);  

			ExpQuantity = parseFloat(expqty) - parseFloat(QoH);
			nlapiLogExecution('ERROR', 'in not equal if ExpQuantity', ExpQuantity);  
			NewActQty = parseFloat(actqty) - parseFloat(ExpQuantity);
			nlapiLogExecution('ERROR', 'in not equal if NewActQty', NewActQty);  
			/*if(parseFloat(NewActQty) == 0)
			{
				QtytobeAdjusted = 0;
			}
			else
			{*/
			QtytobeAdjusted = parseFloat(NewActQty) - parseFloat(QoH);
			//	}

			nlapiLogExecution('ERROR', 'after ExpQuantity', ExpQuantity);    
			nlapiLogExecution('ERROR', 'after NewActQty', NewActQty);    
			nlapiLogExecution('ERROR', 'after QtytobeAdjusted', QtytobeAdjusted);    

		}
		else
		{
			nlapiLogExecution('ERROR', 'in not equal else', 'else');    
			QtytobeAdjusted = parseFloat(actqty) - parseFloat(expqty);
		}
	}
	else
	{
		nlapiLogExecution('ERROR', 'in search result null else', 'else');  
		var tempqty = 0;
		if(parseFloat(expqty) < parseFloat(actqty))
		{
			NewActQty = parseFloat(actqty) - parseFloat(expqty);
			QtytobeAdjusted = parseFloat(NewActQty) - parseFloat(tempqty);
		}	
		else
		{
			QtytobeAdjusted = parseFloat(tempqty) - parseFloat(actqty);
		}
//		if(dollarvariance=='0')
//		{
//		QtytobeAdjusted = '0';
//		}
	}

	//var serialnourl = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial', 'customdeploy_lprelated_serial') + "&custparam_item=" + actsku + "&custparam_planno=" + planno;
	var serialnourl = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial', 'customdeploy_lprelated_serial') + "&custparam_expserial=" + null + "&custparam_actserial=" + null + "&custparam_expskustatus=" + expskustatusvalue + "&custparam_actskustatus=" + actskustatusvalue +"&custparam_recid=" + searchresult.getId();
	var Invturl; //= nlapiResolveURL('SUITELET', 'customscript_ebiz_resolve_expitemurl', 'customdeploy_ebiz_resolve_expitemurl_di') + "&custparam_expsku=" + actsku + "&custparam_planno=" + planno;
	var BinlocInvturl; //= nlapiResolveURL('SUITELET', 'customscript_ebiz_cycbinloc_invt', 'customdeploy_ebiz_cycbinloc_invt_di') + "&custparam_binloc=" + loc + "&custparam_planno=" + planno;


	nlapiLogExecution('ERROR', 'actsku', actsku);
	nlapiLogExecution('ERROR', 'loc', loc);
	nlapiLogExecution('ERROR', 'actlp', actlp);

	var invtfilters = new Array();		
	invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', loc));
	if(actsku != null && actsku != '')
		invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', actsku));
	if(actlp != null && actlp != '')
		invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', actlp));
	invtfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var invtcolumns = new Array();
	invtcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	invtcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	invtcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	invtcolumns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	invtcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	invtcolumns[0].setSort();
	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns);
	if(invtsearchresults!=null && invtsearchresults!='')
		var currentinvt = invtsearchresults[0].getValue('custrecord_ebiz_avl_qty');

	if(currentinvt=='undefined'||currentinvt==null||currentinvt==''||isNaN(currentinvt)){
		currentinvt=0;
	}
	//case# 201410134 (href commented because we are not passing any url to BinlocInvturl because of this in client we are unable to get the binlocation)
	form.getSubList('custpage_items').setLineItemValue('custpage_locationtext', i + 1, locText);
	//form.getSubList('custpage_items').setLineItemValue('custpage_locationtext', i + 1, "<a href='"+ BinlocInvturl +"' target='_blank'>"+ locText + "</a>");
	form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, loc);
	form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i + 1, expsku);
	form.getSubList('custpage_items').setLineItemValue('custpage_expskutext', i + 1, expskutext);

	form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i + 1, actsku);                    
	//form.getSubList('custpage_items').setLineItemValue('custpage_actskutext', i + 1, actskutext);
	//form.getSubList('custpage_items').setLineItemValue('custpage_actskutext', i + 1, "<a href='"+ Invturl +"' target='_blank'>"+ actskutext + "</a>");
	//form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink', i + 1, "<a href=\"#\" onclick=\"javascript:window.open('" + Invturl + "');\" >" + actskutext + "</a>");
	form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink', i + 1, actskutext);
	form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i + 1, expqty);
	form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i + 1, actqty);

	form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance', i + 1, dollarvariance);
	//case 20124779, Deciaml quantity fixed to 2 points.
	form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance', i + 1, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
//	End
	//form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust', i + 1, parseFloat(actqty) - parseFloat(expqty));
	form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust', i + 1, parseFloat(QtytobeAdjusted).toFixed(2));
	form.getSubList('custpage_items').setLineItemValue('custpage_explp', i + 1, explp);
	form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i + 1, actlp);

	//form.getSubList('custpage_items').setLineItemValue('custpage_expserial', i + 1, expserialno);
	//form.getSubList('custpage_items').setLineItemValue('custpage_actserial', i + 1, actserialno);


	form.getSubList('custpage_items').setLineItemValue('custpage_expbatch', i + 1, explot);
	form.getSubList('custpage_items').setLineItemValue('custpage_actbatch', i + 1, actlot);

	form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus', i + 1, expskustatus);
	form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue', i + 1, expskustatusvalue);
	form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus', i + 1, actskustatus);
	form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue', i + 1, actskustatusvalue);

	form.getSubList('custpage_items').setLineItemValue('custpage_id', i + 1, searchresult.getId());
	form.getSubList('custpage_items').setLineItemValue('custpage_planno', i + 1, planno);
	form.getSubList('custpage_items').setLineItemValue('custpage_name', i + 1, name);
	form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i + 1, opentaskid);
	form.getSubList('custpage_items').setLineItemValue('custpage_invtid', i + 1, invtid);
	form.getSubList('custpage_items').setLineItemValue('custpage_site', i + 1, siteid);
	form.getSubList('custpage_items').setLineItemValue('custpage_currentinvt', i + 1, currentinvt);
	nlapiLogExecution('ERROR', 'siteid ', siteid);
	nlapiLogExecution('ERROR', 'after loc', loc);
	nlapiLogExecution('ERROR', 'after searchresultinvtid', invtid);
	nlapiLogExecution('ERROR', 'after expsku', expsku);
	nlapiLogExecution('ERROR', 'after actsku', actsku);     

}

function AdjustSerialNumbers(localSerialNoArray,qty,item,lp,restype,CyccRecID,varianceinExpserialNo)
{	
	try
	{
		nlapiLogExecution('ERROR','AdjustSerialNumbers qty',parseFloat(qty));
		nlapiLogExecution('ERROR','item',item);
		nlapiLogExecution('ERROR','lp',lp);
		nlapiLogExecution('ERROR','restype',restype);
		nlapiLogExecution('ERROR','CyccRecID',CyccRecID);
		if(restype == 'resolve'|| restype == 'close')
		{
			var fields = ['custrecord_cycle_serialno','custrecord_cycle_expserialno'];
			var columns = nlapiLookupField('customrecord_ebiznet_cyclecountexe', CyccRecID, fields);
			var ActSerialNos = columns.custrecord_cycle_serialno;
			var ActSerialNosArrayTemp=new Array();

			if(ActSerialNos!=null && ActSerialNos!='')
			{
				ActSerialNosArrayTemp=ActSerialNos.split(',');
			}

			nlapiLogExecution('ERROR', 'ActSerialNos in AdjustSerialNumbers: ', ActSerialNos);
			nlapiLogExecution('ERROR', 'ActSerialNosArrayTemp in AdjustSerialNumbers: ', ActSerialNosArrayTemp);
			var ExpSerialNos = columns.custrecord_cycle_expserialno;
			nlapiLogExecution('ERROR', 'ExpSerialNos in AdjustSerialNumbers: ', ExpSerialNos);


			if(parseFloat(qty) < 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'D');
				filters[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof',[3,19]);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "" && ActSerialNos == "")
				{ nlapiLogExecution('ERROR', 'searchResults.length in qty<0 in if: ', searchResults.length);
				for (var i = 0; i < searchResults.length; i++) 
				{   
					if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
					{
						var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
						var InternalID = searchResults[i].getId();

						//var currentRow = [SerialNo];					
						//localSerialNoArray.push(currentRow);
						//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

						//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
						//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
					}
				}
				}
				else
				{


					var filters = new Array();
					filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
					filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
					var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

					if(searchResults != null && searchResults != "")
					{ nlapiLogExecution('ERROR', 'searchResults.length in qty<0 in else: ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');

							nlapiLogExecution('ERROR', 'searchResults.length : SerialNo ', SerialNo);
							nlapiLogExecution('ERROR', 'searchResults.length : ActSerialNos', ActSerialNos);


							//case # 20126283
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								//if(varianceinExpserialNo.indexOf(SerialNo) != -1)
								//{
								nlapiLogExecution('ERROR', 'SerialNo in ActSerialNos : ', SerialNo);
								var InternalID = searchResults[i].getId();

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
									LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

									var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
									nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/									

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);
								//}

								//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
								//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
							}
						}
					}
					}
				}
			}
			else if(parseFloat(qty) > 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				//Case #  20126109  Start,
				//	filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
				//Case # 20126109 End.

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				columns[1] = new nlobjSearchColumn('custrecord_serialstatus');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					nlapiLogExecution('ERROR', 'searchResults.length : ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var SerialStatus=searchResults[i].getValue('custrecord_serialstatus');
							/*var InternalID = searchResults[i].getId();

							var currentRow = [SerialNo];					
							localSerialNoArray.push(currentRow);
							nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/
							//case# 20148167 and 20148170 20148647 starts (commented because old serialnums are updating with wmsclose status)
							/*if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								nlapiLogExecution('ERROR', 'Index of SerialNo in ExpSerialNosArrayTemp  ', ExpSerialNosArrayTemp.indexOf(SerialNo));
								if(ExpSerialNosArrayTemp.indexOf(SerialNo) != -1)
								{
								nlapiLogExecution('ERROR', 'SerialNo in ActSerialNos if qty>0: ', SerialNo);
								var InternalID = searchResults[i].getId();

									//var currentRow = [SerialNo];					
									//localSerialNoArray.push(currentRow);
									//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

									var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
									LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

									var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
									nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);
								//case #  20126283 Added If condition.
								if(SerialStatus!='I'){

									var fieldNames = new Array(); 
									var newValues = new Array(); 

									fieldNames.push('custrecord_serialwmsstatus'); 
									newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);
								//}
								//}

								//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
								//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
							}*/
							//case# 20148167 and 20148170 20148647 ends
						}
					}
				}
			}
			else if(parseFloat(qty) == 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');							
							//var InternalID = searchResults[i].getId();



							//added now
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								var InternalID = searchResults[i].getId();
								//nlapiLogExecution('ERROR', 'Serial Entry rec id to tst',InternalID);

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray in resolve and qty ==0: ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
								LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

								var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);*/
								//nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);

							}
							//added now


							//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);	


						}
					}
				}
			}
		}
		if(restype == 'ignore' || restype == 'recount')
		{
			if(parseFloat(qty) < 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							//var currentRow = [SerialNo];					
							//localSerialNoArray.push(currentRow);
							//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);
						}
					}
				}
			}
			else if(parseFloat(qty) > 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					nlapiLogExecution('ERROR', 'searchResults.length : ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							//var currentRow = [SerialNo];					
							//localSerialNoArray.push(currentRow);
							//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());							

							var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
						}
					}
				}
			}		
		}		

	}
	catch (e) {
		nlapiLogExecution('ERROR', 'Catch', e);
	}
	return localSerialNoArray;
}

function getSerialNoCSV(arrayLP){
	var csv = "";
	nlapiLogExecution('ERROR', 'arrayLP.length',arrayLP.length);
	if(arrayLP.length == 1)
	{
		csv = arrayLP[0][0];
	}
	else
	{
		for (var i = 0; i < arrayLP.length; i++) {
			if(i == arrayLP.length -1)
			{
				csv += arrayLP[i][0];
			}
			else
			{
				csv += arrayLP[i][0] + ",";
			}
		}
	}
	return csv;
}

function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	nlapiLogExecution('ERROR','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', 3);

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function displaysublist(form, searchResults,hiddenfieldshowvalue){
	var loc,locText,expsku,expskutext,actskutext,actsku,expqty,actqty,explp,actlp,expskustatus,actskustatus;
	var actskustatusvalue,planno,name,opentaskid,intid,siteis,actlot,explot,expskustatusvalue,expserialno ,actserialno ;
	var dollarvariance;

	/*
	 *  Searching records from custom record and dynamically adding to
	 * the sublist lines
	 */
	var i=0;

	if(searchResults != null && searchResults != "")
	{
		var orderListArray=new Array();		
		for(k=0;k<searchResults.length;k++)
		{
			//nlapiLogExecution('ERROR', 'searchResults[k]', searchResults[k]); 
			var ordsearchresult = searchResults[k];

			if(ordsearchresult!=null)
			{
				//nlapiLogExecution('ERROR', 'ordsearchresult.length new tst', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}

		nlapiLogExecution('ERROR', 'searchresults.length in new sublist bind', searchResults.length);
		var ShowAll,ShowDis,ShowNonDis;
		if(request.getParameter('restypeflag')=='cyclecountsa' && (request.getParameter('custpage_hiddenfieldshow') == '' || request.getParameter('custpage_hiddenfieldshow') == null))
		{
			nlapiLogExecution('ERROR', 'ShowAll if', ShowAll);
			//vsa = request.getParameter('restypeflag');
			ShowAll = 'T';
		}
		else if(request.getParameter('custpage_hiddenfieldshow') == 'ALL')
		{
			nlapiLogExecution('ERROR', 'ShowAll else', ShowAll);
			ShowAll = 'T';
		}


		if(request.getParameter('restypeflag')=='cyclecountsd' && (request.getParameter('custpage_hiddenfieldshow') == '' || request.getParameter('custpage_hiddenfieldshow') == null))
		{
			nlapiLogExecution('ERROR', 'ShowDis if', ShowDis);
			//vsa = request.getParameter('restypeflag');
			ShowDis = 'T';
		}
		else if(request.getParameter('custpage_hiddenfieldshow') == 'D')
		{
			nlapiLogExecution('ERROR', 'ShowDis else', ShowDis);
			ShowDis = 'T';
		}

		if(request.getParameter('restypeflag')=='cyclecountsnd' && (request.getParameter('custpage_hiddenfieldshow') == '' || request.getParameter('custpage_hiddenfieldshow') == null))
		{
			nlapiLogExecution('ERROR', 'ShowNonDis if', ShowNonDis);
			//vsa = request.getParameter('restypeflag');
			ShowNonDis = 'T';
		}
		else if(request.getParameter('custpage_hiddenfieldshow') == 'ND')
		{
			nlapiLogExecution('ERROR', 'ShowNonDis else', ShowNonDis);
			ShowNonDis = 'T';
		}

		nlapiLogExecution('ERROR', 'ShowAll in bind', ShowAll);
		nlapiLogExecution('ERROR', 'ShowDis in bind', ShowDis);
		nlapiLogExecution('ERROR', 'ShowNonDis in bind', ShowNonDis);


		for(var m=0;m<orderListArray.length;m++)
		{
			nlapiLogExecution('ERROR', 'm', m);			

			loc = orderListArray[m].getValue('custrecord_cycact_beg_loc');
			locText = orderListArray[m].getText('custrecord_cycact_beg_loc');
			expsku = orderListArray[m].getValue('custrecord_cyclecount_exp_ebiz_sku_no');
			expskutext = orderListArray[m].getText('custrecord_cyclesku');
			//actsku = orderListArray[m].getValue('custrecord_cycleact_sku');
			actskutext = orderListArray[m].getText('custrecord_cycleact_sku');
			actsku = orderListArray[m].getValue('custrecord_cyclecount_act_ebiz_sku_no');
			expqty = orderListArray[m].getValue('custrecord_cycleexp_qty');
			actqty = orderListArray[m].getValue('custrecord_cycle_act_qty');
			explp = orderListArray[m].getValue('custrecord_cyclelpno');
			actlp = orderListArray[m].getValue('custrecord_cycact_lpno');

			expserialno = orderListArray[m].getValue('custrecord_cycle_expserialno');
			actserialno = orderListArray[m].getValue('custrecord_cycle_serialno');

			nlapiLogExecution('ERROR', 'expserialno in show discrepancy', expserialno);		
			nlapiLogExecution('ERROR', 'actserialno in show discrepancy', actserialno);		

			expskustatus = orderListArray[m].getText('custrecord_expcyclesku_status');
			expskustatusvalue = orderListArray[m].getValue('custrecord_expcyclesku_status');
			actskustatus = orderListArray[m].getText('custrecord_cyclesku_status');
			actskustatusvalue = orderListArray[m].getValue('custrecord_cyclesku_status');

			if(actskustatus == null || actskustatus == "")
				actskustatus = expskustatus;
			if(actskustatusvalue == null || actskustatusvalue == "")
				actskustatusvalue = expskustatusvalue;

			nlapiLogExecution('ERROR', 'actskustatus in show discrepancy', actskustatus);		
			nlapiLogExecution('ERROR', 'expskustatus in show discrepancy', expskustatus);	


			planno = orderListArray[m].getValue('custrecord_cycle_count_plan');
			name = orderListArray[m].getValue('custrecord_cycle_count_plan');
			opentaskid = orderListArray[m].getValue('custrecord_opentaskid');
			invtid = orderListArray[m].getValue('custrecord_invtid');
			siteid = orderListArray[m].getValue('custrecord_cyclesite_id');
			actlot = orderListArray[m].getValue('custrecord_cycleabatch_no');
			explot = orderListArray[m].getValue('custrecord_expcycleabatch_no');

			//Code Added By Ganapathi on 26th Nov 2012
			var filters = new Array();   
			if(actsku!=null && actsku!='')
				filters.push(new nlobjSearchFilter('internalid', null, 'is',actsku));
			filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('cost');
			columns[1] = new nlobjSearchColumn('averagecost');
			columns[2] = new nlobjSearchColumn('itemid');

			var itemdetails = nlapiSearchRecord('item', null, filters, columns);
			var vCost=0;
			var vAvgCost=0;
			if (itemdetails !=null) 
			{
				vItemname=itemdetails[0].getValue('itemid');
				vCost = itemdetails[0].getValue('cost');
				nlapiLogExecution('ERROR', 'vCost', vCost);
				vAvgCost = itemdetails[0].getValue('averagecost');
				nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);          
			}

			if(actqty==null||actqty=="")
			{
				nlapiLogExecution('ERROR', 'actqty:', actqty);
				actqty=0;
			}

			if(expqty==null||expqty=="")
			{
				nlapiLogExecution('ERROR', 'expqty: ', expqty);
				expqty=0;
			}

			dollarvariance = (parseFloat(actqty) - parseFloat(expqty)) * vAvgCost;

			nlapiLogExecution('ERROR','dollarvariance',dollarvariance);

			if(isNaN(dollarvariance)||dollarvariance==null||dollarvariance=="")
			{
				nlapiLogExecution('ERROR', 'dollarvariance: ', dollarvariance);
				dollarvariance=0;
			}
			else
			{
				dollarvariance=Math.round(dollarvariance,2);
			}

			nlapiLogExecution('ERROR','planno',planno);
			var varianceinExpserialNo='';
			var varianceinActserialNo='';		
			if(actserialno != null && actserialno != '' && expserialno != null && expserialno != '')
			{
				actserialno = actserialno.trim();
				expserialno = expserialno.trim();

				if(expserialno != null && expserialno != '')
					var getexpserialArr = expserialno.split(',');

				if(actserialno != null && actserialno != '')
					var getactserialArr = actserialno.split(',');


				if(getexpserialArr != null && getexpserialArr != '')
				{
					for (var p = 0; p < getexpserialArr.length; p++) 
					{				
						if(actserialno != null && actserialno != '')
						{
							if(actserialno.indexOf(getexpserialArr[p])==-1)//not found temSeriIdArr
							{
								if (varianceinExpserialNo == "") {
									varianceinExpserialNo= getexpserialArr[p];
								}
								else {
									varianceinExpserialNo = varianceinExpserialNo + "," + getexpserialArr[p];
								}
							}
						}
					}
				}
				nlapiLogExecution('ERROR', 'varianceinExpserialNo', varianceinExpserialNo);

				if(getactserialArr != null && getactserialArr != '')
				{
					for (var q = 0; q < getactserialArr.length; q++) 
					{
						if(getexpserialArr != null && getexpserialArr != '')
						{

							if(getexpserialArr.indexOf(getactserialArr[q])==-1)//not found temSeriIdArr
							{
								if (varianceinActserialNo == "") {
									varianceinActserialNo= getactserialArr[q];
								}
								else {
									varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];
								}
							}
						}
					}
				}
				nlapiLogExecution('ERROR', 'varianceinActserialNo', varianceinActserialNo);
			}

			/*nlapiLogExecution('ERROR', 'expqty', expqty);
			nlapiLogExecution('ERROR', 'actqty', actqty);
			nlapiLogExecution('ERROR', 'expsku', expsku);
			nlapiLogExecution('ERROR', 'actsku', actsku);
			nlapiLogExecution('ERROR', 'explot', explot);
			nlapiLogExecution('ERROR', 'actlot', actlot);
			nlapiLogExecution('ERROR', 'explp', explp);
			nlapiLogExecution('ERROR', 'actlp', actlp);
			nlapiLogExecution('ERROR', 'expskustatusvalue', expskustatusvalue);
			nlapiLogExecution('ERROR', 'actskustatusvalue', actskustatusvalue);*/

			nlapiLogExecution('ERROR', 'orderListArray[m].getId()', orderListArray[m].getId());
			//var serialnourl = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial', 'customdeploy_lprelated_serial') + "&custparam_item=" + actsku+ "&custparam_planno=" + planno;
			var serialnourl = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial', 'customdeploy_lprelated_serial') + "&custparam_expserial=" + null + "&custparam_actserial=" + null + "&custparam_expskustatus=" + expskustatusvalue + "&custparam_actskustatus=" + actskustatusvalue +"&custparam_recid=" + orderListArray[m].getId();
//			case 20124074 start
			var Invturl; //= nlapiResolveURL('SUITELET', 'customscript_ebiz_resolve_expitemurl', 'customdeploy_ebiz_resolve_expitemurl_di') + "&custparam_expsku=" + actsku + "&custparam_planno=" + planno;
//			end

			//if((request.getParameter('restypeflag')=='cyclecountsd' || hiddenfieldshowvalue == 'D') && (expqty != actqty ||  expsku != actsku || expserialno != actserialno || explot != actlot || explp != actlp ))
			if((ShowDis == 'T') && (expqty != actqty ||  expsku != actsku || explot != actlot || explp != actlp || expskustatusvalue != actskustatusvalue || varianceinExpserialNo != "" ||  varianceinActserialNo != ""))
			{
				i = i+1;
				nlapiLogExecution('ERROR', 'request.getParameter(restypeflag) in if binding', request.getParameter('restypeflag'));
				nlapiLogExecution('ERROR', 'hiddenfieldshowvalue in if binding', hiddenfieldshowvalue);
				nlapiLogExecution('ERROR', 'orderListArray[m].getId() inside', orderListArray[m].getId());


				form.getSubList('custpage_items').setLineItemValue('custpage_locationtext', i, locText);
				form.getSubList('custpage_items').setLineItemValue('custpage_location', i, loc);
				form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i, expsku);
				form.getSubList('custpage_items').setLineItemValue('custpage_expskutext', i, expskutext);
				form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i, actsku);                    
				//form.getSubList('custpage_items').setLineItemValue('custpage_actskutext', i, actskutext);
//				case 20124074 start
				//form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink', i, "<a href=\"#\" onclick=\"javascript:window.open('" + Invturl + "');\" >" + actskutext + "</a>");
				form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink', i, actskutext);
//				end
				form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i, expqty);
				form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i, actqty);

				form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance', i, dollarvariance);
				form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance', i, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
				form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust', i, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
				form.getSubList('custpage_items').setLineItemValue('custpage_explp', i, explp);
				form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i, actlp);

				//form.getSubList('custpage_items').setLineItemValue('custpage_expserial', i, expserialno);
				//form.getSubList('custpage_items').setLineItemValue('custpage_actserial', i, actserialno);



				form.getSubList('custpage_items').setLineItemValue('custpage_expbatch', i, explot);
				form.getSubList('custpage_items').setLineItemValue('custpage_actbatch', i, actlot);

				form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus', i, expskustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue', i, expskustatusvalue);
				form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus', i, actskustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue', i, actskustatusvalue);

				form.getSubList('custpage_items').setLineItemValue('custpage_id', i, orderListArray[m].getId());
				form.getSubList('custpage_items').setLineItemValue('custpage_planno', i, planno);
				form.getSubList('custpage_items').setLineItemValue('custpage_name', i, name);
				form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i, opentaskid);
				form.getSubList('custpage_items').setLineItemValue('custpage_invtid', i, invtid);
				form.getSubList('custpage_items').setLineItemValue('custpage_site', i, siteid);

			}

			//else if((request.getParameter('restypeflag')=='cyclecountsnd' || hiddenfieldshowvalue == 'ND') && (expqty == actqty &&  expsku == actsku && expserialno == actserialno && explot == actlot && explp == actlp ))
			else if((ShowNonDis == 'T') && (expqty == actqty &&  expsku == actsku && explot == actlot && explp == actlp && expskustatusvalue == actskustatusvalue && varianceinExpserialNo == "" && varianceinActserialNo == ""))
			{

				i =i+1;
				nlapiLogExecution('ERROR', 'request.getParameter(restypeflag) in else binding', request.getParameter('restypeflag'));
				nlapiLogExecution('ERROR', 'hiddenfieldshowvalue in else binding', hiddenfieldshowvalue);
				nlapiLogExecution('ERROR', 'orderListArray[m].getId() else inside', orderListArray[m].getId());


				form.getSubList('custpage_items').setLineItemValue('custpage_locationtext', i, locText);
				form.getSubList('custpage_items').setLineItemValue('custpage_location', i, loc);
				form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i, expsku);
				form.getSubList('custpage_items').setLineItemValue('custpage_expskutext', i, expskutext);
				form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i, actsku);                    
				//form.getSubList('custpage_items').setLineItemValue('custpage_actskutext', i, actskutext);
				//form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink', i, "<a href=\"#\" onclick=\"javascript:window.open('" + serialnourl + "');\" >" + actskutext + "</a>");
				form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink', i, actskutext);
				form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i, expqty);
				form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i, actqty);

				form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance', i, dollarvariance);
				form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance', i, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
				form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust', i, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
				form.getSubList('custpage_items').setLineItemValue('custpage_explp', i, explp);
				form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i, actlp);

				//form.getSubList('custpage_items').setLineItemValue('custpage_expserial', i, expserialno);
				//form.getSubList('custpage_items').setLineItemValue('custpage_actserial', i, actserialno);


				form.getSubList('custpage_items').setLineItemValue('custpage_expbatch', i, explot);
				form.getSubList('custpage_items').setLineItemValue('custpage_actbatch', i, actlot);

				form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus', i, expskustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue', i, expskustatusvalue);
				form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus', i, actskustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue', i, actskustatusvalue);

				form.getSubList('custpage_items').setLineItemValue('custpage_id', i, orderListArray[m].getId());
				form.getSubList('custpage_items').setLineItemValue('custpage_planno', i, planno);
				form.getSubList('custpage_items').setLineItemValue('custpage_name', i, name);
				form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i, opentaskid);
				form.getSubList('custpage_items').setLineItemValue('custpage_invtid', i, invtid);
				form.getSubList('custpage_items').setLineItemValue('custpage_site', i, siteid);
			}
		}
	}
}


function getSerialNoCSVvalues(ItemId,LP,expqty1,actqty1)
{
	nlapiLogExecution('ERROR', ' into getserialno ItemId', ItemId);
	nlapiLogExecution('ERROR', 'LP', LP);
	nlapiLogExecution('ERROR', 'expqty1', expqty1);
	nlapiLogExecution('ERROR', 'actqty1', actqty1);
	var filters = new Array();

	filters[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', ItemId);	
	filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);
	if(parseFloat(expqty1)<parseFloat(actqty1))
		filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', '32');
	var tempSerial = "";

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
	var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
	if(serchRec!=null && serchRec!='')
	{
		for (var z = 0; z < serchRec.length; z++) {

			if (tempSerial == "") {
				tempSerial = serchRec[z].getValue('custrecord_serialnumber');

			}
			else {

				tempSerial = tempSerial + "," + serchRec[z].getValue('custrecord_serialnumber');
			}

		}


	}
	return tempSerial;
}
