/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WO_wave_release_SL.js,v $
 *     	   $Revision: 1.1.2.3.2.5 $
 *     	   $Date: 2015/12/02 14:54:43 $
 *     	   $Author: rmalraj $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_212 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WO_wave_release_SL.js,v $
 * Revision 1.1.2.3.2.5  2015/12/02 14:54:43  rmalraj
 * 201415201--work order wave creation and release screen changes
 *
 * Revision 1.1.2.3.2.4  2015/11/14 15:23:24  snimmakayala
 * 201415652
 *
 * Revision 1.1.2.3.2.3  2015/11/13 15:55:12  rmukkera
 * case # 201414431
 *
 * Revision 1.1.2.3.2.2  2015/11/12 17:59:35  sponnaganti
 * case# 201414314
 * 2015.2 issue fix
 *
 * Revision 1.1.2.3.2.1  2015/10/06 06:06:19  aanchal
 * 2015.2 issue fixes
 * 201414318
 *
 * Revision 1.1.2.3  2015/05/25 15:41:50  grao
 * SB issue fixes  201412569
 *
 * Revision 1.1.2.2  2015/05/06 10:57:15  grao
 * SB issue fixes  201412632
 *
 * Revision 1.1.2.1  2015/02/13 08:00:36  skreddy
 * Case# 201410541
 * Work order location generation sch & wave generation for work order
 *
 * Revision 1.1.4.8.2.20.2.6  2014/09/17 16:04:57  sponnaganti
 * Case# 201410403
 * Stnd Bundle Issue fix
 *
 *
 **********************************************************************************************************************/
/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Oct 2014     GA300732
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function WOWaveRelease(request, response){
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('WO Wave Creation & Release');
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');	
		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
		
		
		
		 var vWONo = request.getParameter('custpage_qbwo');
		nlapiLogExecution('DEBUG', 'vWONo', vWONo);
		
		hiddenQueryParams.setDefaultValue(vWONo);
		
		/*var OrdersCount=form.addField('custpage_orderscount','text', 'Orders Count');//.setDisabled(true).setDefaultValue(parseInt(0));
		OrdersCount.setDefaultValue(parseInt(0));
		OrdersCount.setLayoutType('outsidebelow', 'startrow');
		OrdersCount.setDisabled(true);
		OrdersCount.setDisplaySize(50,30);
		var OrdersLineCount=form.addField('custpage_orderslinecount','text', 'Order Lines Count');//.setDisabled(true).setDefaultValue(parseInt(0));
		OrdersLineCount.setDefaultValue(parseInt(0));
		OrdersLineCount.setLayoutType('outsidebelow', 'startcol');
		OrdersLineCount.setDisabled(true);
		OrdersLineCount.setDisplaySize(50,30);*/
		form.setScript('customscript_wavegeneration_cl');//client script
		// Create the sublist
		//createWaveGenSublist(form);
		var orderList = getOrdersForWaveGen(request,null,form,null);

		if(orderList==null || orderList=='')
		{
			showInlineMessage(form, 'Error', 'No Data Found for the Given Work Order '+ vWONo);
		}
		else
		{
			setPagingForSublist(orderList,form,request);

			form.addSubmitButton('Generate Wave');
		}
		response.writePage(form);
	}
	else{
		nlapiLogExecution("ERROR","sessionobj",sessionobj);
		nlapiLogExecution("ERROR","context.getUser()",context.getUser());
		if (sessionobj!=context.getUser())
		{
			try
			{
				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				generateWave(request, response);
			}
			catch(e)
			{
				nlapiLogExecution("ERROR","exception",e);
			}
			finally
			{
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally else block','block');
			}
		}
		else
		{
			var form = nlapiCreateForm('WO Wave Creation & Release');
			form.setScript('customscript_wavegeneration_cl');
			form.addButton('custpage_backtosearch','Back to Search','backtoreleaseWOorders()');
			form.addButton('custpage_wavestatus','Wave Status Report','WOgotowavestatus()');
			response.writePage(form);
		}
	}
}

function getOrdersForWaveGen(request){
	// Validating all the request parameters and pushing to a local array
	var orderList;
	var location;
	var vWONo;
	var vOrderTye;
	var vProjectRoute;
	var vStartDate;
	var vEndDate;
	var vManuRoute;
	var vCustomer;
	//var vJob;
	/*if (request.getParameter('custpage_qbwojob')!=null && request.getParameter('custpage_qbwojob')!="" )
	{
		vJob=request.getParameter('custpage_qbwojob'); 
	}*/
	if (request.getParameter('custpage_qbwocustomer')!=null && request.getParameter('custpage_qbwocustomer')!="" )
	{
		vCustomer=request.getParameter('custpage_qbwocustomer'); 
	}
	
	if (request.getParameter('custpage_site')!=null && request.getParameter('custpage_site')!="" )
	{
		location=request.getParameter('custpage_site'); 
	}

	if (request.getParameter('custpage_qbwo') != null && request.getParameter('custpage_qbwo') != "") 
	{
		vWONo = request.getParameter('custpage_qbwo');
	}

	
	if (request.getParameter('custpage_qbwoordertype') != null && request.getParameter('custpage_qbwoordertype') != "") 
	{
		vOrderTye = request.getParameter('custpage_qbwoordertype');
	}
	
	nlapiLogExecution('DEBUG', 'vOrderTye', vOrderTye);

	if (request.getParameter('custpage_qbwojobrouting') != null && request.getParameter('custpage_qbwojobrouting') != "") 
	{
		vProjectRoute = request.getParameter('custpage_qbwojobrouting');
	}
	
	if (request.getParameter('custpage_qbwostartdate') != null && request.getParameter('custpage_qbwostartdate') != "") 
	{
		vStartDate = request.getParameter('custpage_qbwostartdate');
	}
	if (request.getParameter('custpage_qbwoenddatedate') != null && request.getParameter('custpage_qbwoenddatedate') != "") 
	{
		vEndDate = request.getParameter('custpage_qbwoenddatedate');
	}
	
	if (request.getParameter('custpage_qbwomanufacturerouting') != null && request.getParameter('custpage_qbwomanufacturerouting') != "") 
	{
		vManuRoute = request.getParameter('custpage_qbwomanufacturerouting');
	}
	
	// Get all the search filters for wave generation
	var filters = new Array(); 

	if(vWONo != null && vWONo != '')
		filters.push(new nlobjSearchFilter('tranid', null, 'is', vWONo));

	if(vOrderTye != null && vOrderTye != '')
		filters.push(new nlobjSearchFilter('custbody_nswmssoordertype', null, 'anyof',vOrderTye));
	
	if(vProjectRoute != null && vProjectRoute != '')
		filters.push(new nlobjSearchFilter('custbody_project_routing', null, 'is',vProjectRoute));
	if(vStartDate != null && vStartDate != '')
		filters.push(new nlobjSearchFilter('startdate', null, 'on',vStartDate));
	
	if(vEndDate != null && vEndDate != '')
		filters.push(new nlobjSearchFilter('enddate', null, 'on',vEndDate));
	
	if(vManuRoute != null && vManuRoute != '')
		filters.push(new nlobjSearchFilter('manufacturingrouting', null, 'anyof',vManuRoute));
	
	if(vCustomer != null && vCustomer != '')
		filters.push(new nlobjSearchFilter('entity', null, 'anyof',vCustomer));
	
	/*if(vJob != null && vJob != '')
		filters.push(new nlobjSearchFilter('job', null, 'anyof',vJob));
	*/
	var orderList = nlapiSearchRecord('transaction', 'customsearch_ebiz_open_wo_to_wave', filters, null);


	return orderList;
}
function setPagingForSublist(orderListArray,form,request)
{
	if(orderListArray != null && orderListArray != "" && orderListArray.length > 0){
		nlapiLogExecution('DEBUG', 'orderListArray length ', orderListArray.length);

		var test='';

		if(orderListArray.length>0)
		{


			nlapiLogExecution('DEBUG', 'orderListArray length ', orderListArray.length);

			// case # 20124327ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½ start
			createWaveGenSublist(form);
			// case # 20124327ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½ end

			if(orderListArray.length>25)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("25");
					pagesizevalue=25;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue=25;
						pagesize.setDefaultValue("25");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{
					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}

			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);

				var pagevalue=request.getParameter('custpage_pagesize');

				if(pagevalue!=null)
				{
					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{
						var selectedPageArray=selectno.split(',');
						minval=parseFloat(selectedPageArray[0])-1;
						maxval=parseFloat(selectedPageArray[1]);
					}
				}
			}

			var c=0;
			var minvalue;
			minvalue=minval;
			var prevVal=null;var currVal=null;

			if(parseInt(orderListArray.length)<parseInt(maxval))
			{
				minval=0;
				maxval=orderListArray.length;
			}

			nlapiLogExecution('DEBUG', 'maxval ', minval+"/"+maxval); 

			for(var j = minvalue; j < maxval; j++){		
				nlapiLogExecution('DEBUG', 'j ', j); 
				var currentOrder = orderListArray[j];

				currVal=currentOrder.getValue('internalid',null,'group');
				if(prevVal!=currVal)
				{
					//case# 20149120 starts
					//addFulfilmentOrderToSublist(form, currentOrder, c,orderListArray,vDueDaysList,vCreditExceedList,vDueDaysRestrict);
					addFulfilmentOrderToSublist(form, currentOrder, c,orderListArray,request);
					//case#20149120 ends
					prevVal=currVal;
					c=c+1;
				}
				else
				{
					//var fullfilmentordcount=form.getSubList('custpage_items').getLineItemValue('custpage_lineno',c);
					//form.getSubList('custpage_items').setLineItemValue('custpage_lineno', c,(parseFloat(fullfilmentordcount)+1).toString());
				}
			}
		}
	}
}

/**
 * Create the sublist that will display the items for wave generation
 * 
 * @param form
 */
function createWaveGenSublist(form){
	var sublist = form.addSubList("custpage_items", "list", "OrderList");	
	sublist.addField("custpage_so", "checkbox", "Confirm").setDefaultValue('F');
	sublist.addField("custpage_wono", "text", "Work Order #");
	//sublist.addField("custpage_lineno", "text", "# of Lines Selected");
	sublist.addField("custpage_totallines", "text", "Total # of Lines");//.setDisplayType('hidden');//santosh
	//sublist.addField("custpage_customer", "text", "Customer");

	//sublist.addField("custpage_ordtype", "text", "Order Type");
	//sublist.addField("custpage_ordpriority", "text", "Order Priority");
	sublist.addField("custpage_ordcrtddate", "text", "Order Date");
	sublist.addField("custpage_location", "text", "Location");
	sublist.addField("custpage_loc_intid", "text", "Location Int id").setDisplayType('hidden');
	sublist.addField("custpage_wointid", "text", "OrdIntId").setDisplayType('hidden');
	//sublist.addField("custpage_shipdate", "date", "Ship Date");//santosh
	//sublist.addField("custpage_linestatus", "text", "Order Line Status").setDisplayType('hidden');
	//sublist.addField("custpage_paymenthold", "text", "Notes");

	sublist.addMarkAllButtons();
}

/**
 * Function to add one fulfillment order to the sublist on the form
 * 
 * @param form
 * @param currentOrder
 * @param i
 */
function addFulfilmentOrderToSublist(form, currentOrder, i,orderList,request){

	var vCarrier='';
	var wmsstatusflag='';
	var vnotes = ''; 




	/*form.getSubList('custpage_items').setLineItemValue('custpage_ordtype', i + 1,
			currentOrder.getText('custrecord_do_order_type',null,'group'));

	form.getSubList('custpage_items').setLineItemValue('custpage_ordpriority', i + 1, 
			currentOrder.getText('custrecord_do_order_priority',null,'group'));*/

	form.getSubList('custpage_items').setLineItemValue('custpage_wono', i + 1,currentOrder.getValue('tranid',null,'group'));

	form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1,currentOrder.getText('location',null,'group'));
	form.getSubList('custpage_items').setLineItemValue('custpage_loc_intid', i + 1,currentOrder.getValue('location',null,'group'));

	if(currentOrder.getValue('datecreated',null,'group') !=null 
			&& currentOrder.getValue('datecreated',null,'group')!='')
	{
		form.getSubList('custpage_items').setLineItemValue('custpage_ordcrtddate', i + 1, 
				currentOrder.getValue('datecreated',null,'group'));
	}


	form.getSubList('custpage_items').setLineItemValue('custpage_totallines', i + 1,
			currentOrder.getValue('linesequencenumber',null,'count'));

	form.getSubList('custpage_items').setLineItemValue('custpage_wointid', i + 1,
			currentOrder.getValue('internalid',null,'group'));


}
/**
 * Return the fulfillment order information to be used for wave generation
 * NOTE: WILL NEVER RETURN NULL; WILL ALWAYS RETURN ATLEAST A BLANK ARRAY
 * 
 * @param request
 * @returns {Array}
 */
function getSelectedFulfillmentOrderInfo(request){
	nlapiLogExecution('DEBUG','into getSelectedFulfillmentOrderInfo', '');
	var WOInfoArray = new Array();
	var orderInfoCount = 0;

	// Retrieve the number of items for which wave needs to be created
	var lineCount = request.getLineItemCount('custpage_items');
	var vWOIdName="";
	var vWOId="";
	for(var k = 1; k <= lineCount; k++){
		var selectValue = request.getLineItemValue('custpage_items', 'custpage_so', k);
		nlapiLogExecution('DEBUG','selectValue', selectValue + ',' + k);
		if(selectValue == 'T'){	

			var doName = request.getLineItemValue('custpage_items', 'custpage_wono', k);
			var doIntId = request.getLineItemValue('custpage_items', 'custpage_wointid', k);
			var locationId = request.getLineItemValue('custpage_items', 'custpage_loc_intid', k);
			nlapiLogExecution('DEBUG','doName', doName);
			if(doIntId != null && doIntId != '')
			{
				if(vWOId ==  null || vWOId == '')
				{
					vWOId=doIntId;
					vWOIdName=doName;
				}
				else
				{
					vWOId= vWOId + ',' + doIntId;
					vWOIdName= vWOIdName + ',' + doName;
				}	
			}  		 
		}
	}
	if(vWOId != null && vWOId != '')
	{	
		WOInfoArray.push(vWOId);
		WOInfoArray.push(vWOIdName);
	}
	nlapiLogExecution('DEBUG','out of getSelectedFulfillmentOrderInfo', WOInfoArray);

	return WOInfoArray;
}
/**
 * 
 */
function isItemSelected(request, subList, element, index){
	var retVal = false;
	if(request.getLineItemValue(subList, element, index) == 'T')
		retVal = true;

	return retVal;
}
/**
 * 
 * @param request
 * @param response
 */
function generateWave(request, response){

	var form = nlapiCreateForm('WO Wave Creation & Release');
	try{
		
		
		
		 var vWONo = request.getParameter('custpage_qeryparams');
		nlapiLogExecution('DEBUG', 'vWONo', vWONo);
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		//hiddenField_selectpage.setDefaultValue('F');	
		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','request.getParameter',request.getParameter('custpage_hiddenfieldselectpage'));
		nlapiLogExecution('DEBUG','Remaining usage at the start',context.getRemainingUsage());
		form.setScript('customscript_wavegeneration_cl');//Client script


		if (request.getParameter('custpage_site')!=null && request.getParameter('custpage_site')!="" ){
			location=request.getParameter('custpage_site'); 
		}

		if (request.getParameter('custpage_qbwo') != null && request.getParameter('custpage_qbwo') != "") {
			vWONo = request.getParameter('custpage_qbwo');
		}
		// Get the list of fulfillment orders selected for wave generation
		// Every element of the returned list contains index, soInternalID, lineNo, doNo, doName, 
		// itemName, itemNo, itemStatus, availQty, orderQty, packCode, uom, lotBatchNo, company, 
		// itemInfo1, itemInfo2, itemInfo3, orderType and orderPriority
		var fulfillOrderList = getSelectedFulfillmentOrderInfo(request);

		if(fulfillOrderList !=null)
			nlapiLogExecution('DEBUG', 'fulfillOrderList.length', fulfillOrderList.length);

		// Proceed only if there is atleast 1 fulfillment order selected
		if(fulfillOrderList!=null && fulfillOrderList.length > 0){

			var eBizWaveNo=createwaveordrecord(fulfillOrderList,form,vWONo);
			var waveDetails = new Array();
			nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);
			waveDetails["ebiz_wave_no"] = eBizWaveNo;
			waveDetails["short_pick"] = 'N';	

			if(eBizWaveNo!='')
			{
				showInlineMessage(form, 'Confirmation', 'Wave created and Pick generation has been initiated for Wave '+ eBizWaveNo);
				hiddenField_selectpage.setDefaultValue('T');
				nlapiLogExecution('DEBUG','request.getParameter',request.getParameter('custpage_hiddenfieldselectpage'));
				// case # 20124327Ãƒâ€šÃ‚Â  start
//				createWaveGenSublist(form);
				// case # 20124327Ãƒâ€šÃ‚Â  end
				//Add two functions
				/*var vDueDaysList=getDueDaysList();
				var vCreditExceedList=getCreditExceedList();

				var vConfig=nlapiLoadConfiguration('accountingpreferences');
				var vDueDaysRestrict;
				if(vConfig != null && vConfig != '')
				{
					vDueDaysRestrict=vConfig.getFieldValue('CREDLIMDAYS');
				}

				var orderList = getOrdersForWaveGen(request,0,form);
				if(orderList != null && orderList.length > 0){
					setPagingForSublist(orderList,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,request);

					var OrdersCount=form.addField('custpage_orderscount','text', 'Orders Count');//.setDisabled(true).setDefaultValue(parseInt(0));
					OrdersCount.setDefaultValue(parseInt(0));
					OrdersCount.setLayoutType('outsidebelow', 'startrow');
					OrdersCount.setDisabled(true);
					OrdersCount.setDisplaySize(50,30);
					var OrdersLineCount=form.addField('custpage_orderslinecount','text', 'Order Lines Count');//.setDisabled(true).setDefaultValue(parseInt(0));
					OrdersLineCount.setDefaultValue(parseInt(0));
					OrdersLineCount.setLayoutType('outsidebelow', 'startcol');
					OrdersLineCount.setDisabled(true);
					OrdersLineCount.setDisplaySize(50,30);
					form.addSubmitButton('Generate Wave');
				}*/
			}
			else
			{
				//showInlineMessage(form, 'Confirmation', 'No lines are selected');
			}
			form.addButton('custpage_backtosearch','Back to Search','backtoreleaseWOorders()');
			form.addButton('custpage_wavestatus','Wave Status Report','WOgotowavestatus('+eBizWaveNo+')');
			//form.addButton('custpage_clusterstatus','Cluster Status Report','gotoclusterstatus()');
			response.writePage(form);
		}
		else
		{ 
			var orderList = getOrdersForWaveGen(request,null,form,null);
			setPagingForSublist(orderList,form,request);


			/*var OrdersCount=form.addField('custpage_orderscount','text', 'Orders Count');//.setDisabled(true).setDefaultValue(parseInt(0));
			OrdersCount.setDefaultValue(parseInt(0));
			OrdersCount.setLayoutType('outsidebelow', 'startrow');
			OrdersCount.setDisabled(true);
			OrdersCount.setDisplaySize(50,30);
			var OrdersLineCount=form.addField('custpage_orderslinecount','text', 'Order Lines Count');//.setDisabled(true).setDefaultValue(parseInt(0));
			OrdersLineCount.setDefaultValue(parseInt(0));
			OrdersLineCount.setLayoutType('outsidebelow', 'startcol');
			OrdersLineCount.setDisabled(true);
			OrdersLineCount.setDisplaySize(50,30);*/
			form.addSubmitButton('Generate Wave');
		}

		nlapiLogExecution('DEBUG','Remaining usage at the end',context.getRemainingUsage());

		response.writePage(form);
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in generatewave ', exp);	
		showInlineMessage(form, 'DEBUG', 'Wave Generation Failed', "");
		response.writePage(form);
	}
	finally
	{
		//context.setSessionObject('session', null);
	}
}

//function createwaveordrecord(fulfillOrderList,eBizWaveNo,vlineStatus,fono,shipmentno,shipmethod,parametersval)
//{
//nlapiLogExecution('DEBUG', 'Into  createwaveordrecord ', eBizWaveNo);	
function createwaveordrecord(fulfillOrderList,form,Orderno)
{

	try
	{

		var eBizWaveNo='';
		nlapiLogExecution('DEBUG', 'fulfillOrderList Length ', fulfillOrderList.length);	
		nlapiLogExecution('DEBUG', 'fulfillOrderList ', fulfillOrderList);
		nlapiLogExecution('DEBUG', 'Orderno', Orderno);

		if(fulfillOrderList != null && fulfillOrderList.length > 0)
		{
			var context = nlapiGetContext();
			var currentUserID = context.getUser();
			
			/*var Filters = new Array();
			Filters[0] = new nlobjSearchFilter('custrecord_ebiz_processname', null, 'is', 'WAVE RELEASE');
			Filters[1] = new nlobjSearchFilter('custrecord_ebiz_processstatus', null, 'is', 'Submitted');				
			Filters[2] = new nlobjSearchFilter('custrecord_ebiz_processtranrefno', null, 'is', Orderno);		

			var Columns = new Array();
			var Searchresults = nlapiSearchRecord('customrecord_ebiz_schedulescripts_status', null, Filters, Columns);
			
			nlapiLogExecution('DEBUG', 'With Submitted Status ',Searchresults);
			if(Searchresults == null || Searchresults =='' || Searchresults == 'null')
			{
				Filters[0] = new nlobjSearchFilter('custrecord_ebiz_processname', null, 'is', 'WAVE RELEASE');
				Filters[1] = new nlobjSearchFilter('custrecord_ebiz_processstatus', null, 'is', 'In Progress');	
			}
			 Searchresults = nlapiSearchRecord('customrecord_ebiz_schedulescripts_status', null, Filters, Columns);
			
			 nlapiLogExecution('DEBUG', 'With In Progress / Submitted Status ', Searchresults);
			 
			if(Searchresults==null || Searchresults=='' || Searchresults=='null')
			{
				if(eBizWaveNo == null || eBizWaveNo=='')
				{
					eBizWaveNo = GetMaxTransactionNo('WAVE');
					nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);
				}

			}
			else
			{
				showInlineMessage1(form, 'Confirmation', 'Pick generation already intiated for this order ');
			}*/
			
			
			
			if(eBizWaveNo == null || eBizWaveNo=='')
			{
				eBizWaveNo = GetMaxTransactionNo('WAVE');
				nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);
			}
			
//			for(var j=0;searchResults!=null && j<searchResults.length;j++)
//			{
//			updateFulfilmentOrder(searchResults[j][0], eBizWaveNo);
//			}	
			if(eBizWaveNo!='')
			{
				nlapiLogExecution('DEBUG', 'Invoking Schedule Script Starts', TimeStampinSec());	

				var param = new Array();
				param['custscript_ebizwowaveno'] = eBizWaveNo;
				param['custscript_ebizwoid_ordlist'] = fulfillOrderList[0];
				param['custscript_ebizwo_ordlist'] = fulfillOrderList[1];
				//param['custscript_ebizwolocation'] = fulfillOrderList;
				nlapiScheduleScript('customscript_ebiz_wo_pickgen_sch', null,param);
				nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);

				//nlapiLogExecution('DEBUG', 'tranType', tranType);
				updateScheduleScriptStatus('WAVE RELEASE',currentUserID,'Submitted',Orderno,null);

				nlapiLogExecution('DEBUG', 'Invoking Schedule Script Ends', TimeStampinSec());
			}

		}
	}
	catch(exp) 
	{
		nlapiLogExecution('DEBUG', 'Exception in createwaveordrecord ', exp);	
	}
	nlapiLogExecution('DEBUG', 'Out of  createwaveordrecord ', eBizWaveNo);
	return eBizWaveNo;
}

function showInlineMessage1(form, messageType, messageText, messageVariable){
	var msg;
	var priority;

	// Create the message field in the form
	msg = form.addField('custpage_message1', 'inlinehtml', null, null, null);

	if(messageType == 'Confirmation')
		priority = 'NLAlertDialog.TYPE_LOWEST_PRIORITY';
	else if(messageType == 'ERROR')
		priority = 'NLAlertDialog.TYPE_HIGH_PRIORITY';
	else
		priority = 'NLAlertDialog.TYPE_HIGH_PRIORITY';

	// Set the message value
	if(messageVariable != null)
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', '" +
				messageType + "', '" + messageText + ":" + messageVariable + "', " +
				priority + ",  '100%', null, null, null);</script></div>");
	else
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', '" +
				messageType + "', '" + messageText + "', " +
				priority + ",  '100%', null, null, null);</script></div>");
}