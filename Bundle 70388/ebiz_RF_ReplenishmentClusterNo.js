/***************************************************************************
����� eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/ebiz_RF_ReplenishmentClusterNo.js,v $
*� $Revision: 1.2.4.2.4.2.4.5 $
*� $Date: 2014/06/13 09:33:20 $
*� $Author: skavuri $
*� $Name: t_NSWMS_2014_1_3_125 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_ReplenishmentClusterNo.js,v $
*� Revision 1.2.4.2.4.2.4.5  2014/06/13 09:33:20  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.2.4.2.4.2.4.4  2014/06/06 07:35:43  skavuri
*� Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
*�
*� Revision 1.2.4.2.4.2.4.3  2014/05/30 00:34:23  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.2.4.2.4.2.4.2  2013/04/17 16:02:35  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.2.4.2.4.2.4.1  2013/03/05 14:46:44  skreddy
*� CASE201112/CR201113/LOG201121
*� Code merged from Lexjet production as part of Standard bundle
*�
*� Revision 1.2.4.2.4.2  2012/09/26 12:37:08  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.2.4.2.4.1  2012/09/21 15:01:30  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.2.4.2  2012/03/16 14:08:54  spendyala
*� CASE201112/CR201113/LOG201121
*� Disable-button functionality is been added.
*�
*� Revision 1.2.4.1  2012/02/21 13:26:11  schepuri
*� CASE201112/CR201113/LOG201121
*� function Key Script code merged
*�
*� Revision 1.3  2012/02/21 11:49:55  schepuri
*� CASE201112/CR201113/LOG201121
*� Added FunctionkeyScript
*�
 */
function ReplenClusterNo(request, response){
	if (request.getMethod() == 'GET') {
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "REPOSICI&#211;N CLUSTER NO";
			st1 = "ENTRAR/SCAN GRUPO NO";			
			st2 = "ENVIAR";
			st3 = "ANTERIOR";
			
		}
		else
		{
			st0 = "REPLENISHMENT CLUSTER NO";
			st1 = "ENTER/SCAN CLUSTER NO ";			
			st2 = "SEND";
			st3 = "PREV";
			
			
		}		
		
		
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterclusterno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterclusterno' id='enterclusterno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "			<td>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "			</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterclusterno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		var getClusterNo = request.getParameter('enterclusterno');

		var SOarray = new Array();
		
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', SOarray["custparam_language"]);
    	
		
		var st5;
		if( getLanguage == 'es_ES')
		{
			
			st5 = "GRUPO DE VALIDEZ #";
		}
		else
		{
			
			st5 = "INVALID CLUSTER #";
			
		}
		

		SOarray["custparam_error"] = st5;
		SOarray["custparam_screenno"] = '18';

		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di', false, SOarray);
		}
		else {
			nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

			// This variable is to hold the REPLEN NO# entered.
			var Reparray = new Array();
			var getLanguage = request.getParameter('hdngetLanguage');
			Reparray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);
	    	
			
			var st6;
			if( getLanguage == 'es_ES')
			{
				
				st6 = "NO V&#193;LIDA REPOSICI&#211;N";
			}
			else
			{
				
				st6 = "INVALID REPLENISHMENT NO";
				
			}
			
			
			Reparray["custparam_repno"] = getClusterNo;
			Reparray["custparam_error"] = st6;
			Reparray["custparam_screenno"] = '1';

			var Repfilters = new Array();
			Repfilters[0] = new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', getClusterNo);
			Repfilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);

			var RepColumns = new Array();
			RepColumns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			RepColumns[1] = new nlobjSearchColumn('custrecord_sku');
			RepColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
			RepColumns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');

			// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
			// to the previous screen.
			var optedEvent = request.getParameter('cmdPrevious');

			//	if the previous button 'F7' is clicked, it has to go to the previous screen 
			//  ie., it has to go to RF Main Menu.
			if (optedEvent == 'F7') {
				response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, optedEvent);
			}
			else {
				//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Repfilters, RepColumns);

				if (searchresults != null && searchresults.length > 0) {
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

					if (Reparray["custparam_repno"] != null) {

						Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
						Reparray["custparam_repsku"] = searchresults[0].getValue('custrecord_sku');
						Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
						Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
						Reparray["custparam_repid"] = searchresults[0].getId();
						Reparray["custparam_clustno"] = getClusterNo;


						nlapiLogExecution('DEBUG', 'Reparray["custparam_repbegloc"]', Reparray["custparam_repbegloc"]);
						nlapiLogExecution('DEBUG', 'Reparray["custparam_repsku"]', Reparray["custparam_repsku"]);
						nlapiLogExecution('DEBUG', 'Reparray["custparam_repexpqty"]', Reparray["custparam_repexpqty"]);
						nlapiLogExecution('DEBUG', 'Reparray["custparam_repskuno"]', Reparray["custparam_repskuno"]);
						nlapiLogExecution('DEBUG', 'Reparray["custparam_repid"]', Reparray["custparam_repid"]);

						response.sendRedirect('SUITELET', 'customscript_rf_replen_loc', 'customdeploy_rf_replen_loc', false, Reparray);
					}
					else {
						//	if the 'Send' button is clicked without any option value entered,
						//  it has to remain in the same screen ie., Receiving Menu.
						nlapiLogExecution('DEBUG', 'Replen No', Reparray["custparam_repno"]);
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					}
				}
				else {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
				}
			}
		}
	}
}
