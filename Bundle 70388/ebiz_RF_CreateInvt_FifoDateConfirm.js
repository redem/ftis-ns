/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_CreateInvt_FifoDateConfirm.js,v $
 *     	   $Revision: 1.1.2.1.2.5.2.1 $
 *     	   $Date: 2015/02/23 17:09:37 $
 *     	   $Author: rrpulicherla $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_1 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CreateInvt_FifoDateConfirm.js,v $
 * Revision 1.1.2.1.2.5.2.1  2015/02/23 17:09:37  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.1.2.5  2014/05/30 00:34:19  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.2.4  2014/02/13 14:55:40  sponnaganti
 * case# 20127149
 * (pass locationid,itemid etc to hidden fields)
 *
 * Revision 1.1.2.1.2.3  2013/08/13 15:53:15  nneelam
 * Case#.  20123863
 * Standard Bundle Issue Fixed..
 *
 * Revision 1.1.2.1.2.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.1.2.1  2013/02/07 08:47:26  skreddy
 * CASE201112/CR201113/LOG201121
 *  RF Lot auto generating FIFO enhancement
 *
 * Revision 1.18.2.55  2012/08/28 15:49:03  spendyala
 * CASE201112/CR201113/LOG201121
 *
 *
 *
 *****************************************************************************/

function CreateInvtFIFODateConfirm(request, response){
	if (request.getMethod() == 'GET') {
		//	Get the Error Message from the previous screen, which is passed as a parameter		
		var getError = request.getParameter('custparam_error');
		nlapiLogExecution('DEBUG', 'Into Request', getError);
		var screenNo=request.getParameter('custparam_screenno');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getLocationId = request.getParameter('custparam_locationId');
		var getBinLocationId = request.getParameter('custparam_binlocationid');
		var getBinLocationName = request.getParameter('custparam_binlocationname');    
		var getItemId = request.getParameter('custparam_itemid');
		var getItem = request.getParameter('custparam_item');
		var getItemType = request.getParameter('custparam_ItemType');
		var getQuantity = request.getParameter('custparam_quantity');
		var getBatchNo = request.getParameter('custparam_BatchNo');
		var getMfgDate = request.getParameter('custparam_mfgdate');
		var getExpDate = request.getParameter('custparam_expdate');
		var getBestBeforeDate = request.getParameter('custparam_bestbeforedate');
		var getWHLocation = request.getParameter('custparam_whlocation');		
		var ItemShelfLife = request.getParameter('custparam_shelflife');
		var CaptureExpiryDate = request.getParameter('custparam_captureexpirydate');
		var CaptureFifoDate = request.getParameter('custparam_capturefifodate');
		var fifodate=request.getParameter('custparam_fifodate');

		var functionkeyHtml=getFunctionkeyScript('_rf_error'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_error' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONFIRMATION: <br><label>" + getError + "</label>";
		//html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getPOLineItemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		//html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		//html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		//html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		//html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		//html = html + "				<input type='hidden' name='hdnBaseUomQty' value=" + getItemBaseUomQty + ">";
	//	html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseInt(getPOItemRemainingQty) + ">";
		//html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseInt(getPOQtyEntered) + ">";
		//html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		
		//case#20127149 starts (pass locationid,itemid etc to hidden fields)
		html = html + "				<input type='hidden' name='hdnItemId' value='" + getItemId + "'>";
		html = html + "				<input type='hidden' name='hdnLocationId' value=" + getLocationId + ">";
		html = html + "				<input type='hidden' name='hdnitemType' value=" + getItemType + ">";
		html = html + "				<input type='hidden' name='hdnBinLocationId' value='" + getBinLocationId + "'>";
		html = html + "				<input type='hidden' name='hdnBinLocation' value=" + getBinLocationName + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnQty' value=" + getQuantity + ">";
		
		//case#20127149 end
		
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemShelfLife' value=" + ItemShelfLife + ">";
		html = html + "				<input type='hidden' name='hdnCaptureExpiryDate' value=" + CaptureExpiryDate + ">";
		html = html + "				<input type='hidden' name='hdnCaptureFifoDate' value=" + CaptureFifoDate + ">";	
		html = html + "				<input type='hidden' name='hdnfifoDate' value=" + fifodate + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONTINUE <input name='cmdContinue' type='submit' value='F8'/>";
		html = html + "					BACK <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var optedEvent = request.getParameter('cmdPrevious');
		var getError = request.getParameter('custparam_error');
		var getScreenNo = request.getParameter('custparam_screenno');
		nlapiLogExecution('ERROR', 'Screen No', getScreenNo);
		nlapiLogExecution('ERROR', 'optedEvent', optedEvent);

		var getBatchNo=request.getParameter('hdnBatchNo');
		nlapiLogExecution('ERROR','getBatchNo',getBatchNo);
		var ItemId= request.getParameter('hdnItemId');
		var locationId = request.getParameter('hdnLocationId');
		nlapiLogExecution('ERROR', 'locationId tst ', locationId);
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var Itemtype= request.getParameter('hdnitemType');



		var CIarray = new Array();

		// From Check-In process
		CIarray["custparam_binlocationid"] = request.getParameter('hdnBinLocationId');
		CIarray["custparam_binlocationname"] = request.getParameter('hdnBinLocation');
		CIarray["custparam_itemid"] = request.getParameter('hdnItemId');
		CIarray["custparam_item"] = request.getParameter('hdnItem');
//		CIarray["custparam_item"] = Item;
		CIarray["custparam_quantity"] = request.getParameter('hdnQty');
		CIarray["custparam_itemtype"]=Itemtype;
		CIarray["custparam_actualbegindate"] = getActualBeginDate;
		CIarray["custparam_locationId"] = locationId;
		CIarray["custparam_actualbegintime"] = getActualBeginTime;
		CIarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
		CIarray["custparam_screenno"] = '15C';
		CIarray["custparam_BatchNo"] = request.getParameter('hdnBatchNo');
		CIarray["custparam_error"] = 'ERROR IN FIFO DATE';
		CIarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
		CIarray["custparam_expdate"] = request.getParameter('hdnExpDate');
		CIarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
		CIarray["custparam_lastdate"]='';
		CIarray["custparam_shelflife"] = request.getParameter('hdnItemShelfLife');
		CIarray["custparam_captureexpirydate"] = request.getParameter('hdnCaptureExpiryDate');
		CIarray["custparam_capturefifodate"] = request.getParameter('hdnCaptureFifoDate');
		//case # 20123863  start
		CIarray["custparam_fifodate"] = request.getParameter('hdnfifoDate');
		//case # 20123863 end

		if (optedEvent == 'F7') {
			nlapiLogExecution('ERROR', 'CHECK IN FIFO DATE F7 Pressed');
			response.sendRedirect('SUITELET', 'customscript_rf_create_invt_fifodate', 'customdeploy_rf_create_invt_fifodate_di', false, CIarray);

		}

		else
		{
			nlapiLogExecution('ERROR', 'BATCH NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
			customrecord.setFieldValue('name', CIarray["custparam_BatchNo"]);
			customrecord.setFieldValue('custrecord_ebizlotbatch', CIarray["custparam_BatchNo"]);
			customrecord.setFieldValue('custrecord_ebizmfgdate', CIarray["custparam_mfgdate"]);
			customrecord.setFieldValue('custrecord_ebizbestbeforedate', CIarray["custparam_bestbeforedate"]);
			customrecord.setFieldValue('custrecord_ebizexpirydate', CIarray["custparam_expdate"]);
			customrecord.setFieldValue('custrecord_ebizfifodate', CIarray["custparam_fifodate"]);
			customrecord.setFieldValue('custrecord_ebizfifocode', CIarray["custparam_fifocode"]);
			customrecord.setFieldValue('custrecord_ebizlastavldate', CIarray["custparam_lastdate"]);
			nlapiLogExecution('ERROR', '1');
			nlapiLogExecution('ERROR', 'Item Id',ItemId);
			customrecord.setFieldValue('custrecord_ebizsku', ItemId);
//			customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);
			if(CIarray["custparam_locationId"] != null  && CIarray["custparam_locationId"] != '')
							customrecord.setFieldValue('custrecord_ebizsitebatch',CIarray["custparam_locationId"]);
			var rec = nlapiSubmitRecord(customrecord, false, true);


			nlapiLogExecution('ERROR', 'rec',rec);
			nlapiLogExecution('ERROR', 'getBatchNo',getBatchNo);
			CIarray["custparam_BatchId"] = rec;
			CIarray["custparam_BatchNo"] = getBatchNo;
			response.sendRedirect('SUITELET', 'customscript_rf_create_invt_itemstat', 'customdeploy_rf_create_invt_itemstat_di', false, CIarray);

		}
		nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
	}
}
