/***************************************************************************
�������������������������eBizNET
�������������������� eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_PackingMenu.js,v $
*� $Revision: 1.1.2.7.4.2.4.9 $
*� $Date: 2014/06/23 08:18:12 $
*� $Author: skavuri $
*� $Name: t_eBN_2014_1_StdBundle_3_126 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_PackingMenu.js,v $
*� Revision 1.1.2.7.4.2.4.9  2014/06/23 08:18:12  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
*�
*� Revision 1.1.2.7.4.2.4.8  2014/06/13 12:48:16  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.1.2.7.4.2.4.7  2014/06/12 15:28:36  skavuri
*� Case # 20148880 (LinkButton functionality added to options in RF Screens)
*�
*� Revision 1.1.2.7.4.2.4.6  2014/05/30 00:41:03  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.1.2.7.4.2.4.5  2013/11/12 06:40:15  skreddy
*� Case# 20125642
*� Afosa SB issue fix
*�
*� Revision 1.1.2.7.4.2.4.4  2013/06/11 14:30:19  schepuri
*� Error Code Change ERROR to DEBUG
*�
*� Revision 1.1.2.7.4.2.4.3  2013/05/09 15:23:00  skreddy
*� CASE201112/CR201113/LOG201121
*� changed the Packing Menu
*�
*� Revision 1.1.2.7.4.2.4.2  2013/05/07 15:15:30  grao
*� CASE201112/CR201113/LOG201121
*� Standard bundle issues fixes
*�
*� Revision 1.1.2.7.4.2.4.1  2013/04/18 07:30:11  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.1.2.7.4.2  2012/09/26 12:28:40  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.1.2.7.4.1  2012/09/25 07:13:53  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi Language
*�
*� Revision 1.1.2.7  2012/09/14 09:22:20  grao
*� no message
*�
*� Revision 1.1.2.3  2012/06/15 10:08:49  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� RF Packing changes
*�
*� Revision 1.1.2.2  2012/06/06 07:39:03  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� RF Packing
*�
*
****************************************************************************/


/**
 * @param request
 * @param response
 */
function PackingMenu(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    //case 20125642 start: spanish conversion
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{//case 20125642 end
			st0 = "MEN&#218;DEL EMBALAJE";
			st1 = "MERGE CART&#211;N";
			st2 = "MOVER CONTENIDO DE LA CAJA";
			st3 = "EMBALAJE";
			st4 = "PAQUETE DE VERIFICACI&#211;N";			
			st5 = "ENVIAR";
			st6 = "ANTERIOR";

			
		}
		else
		{
			st0 = "PACKING MENU";
			st1 = "MERGE CARTON";
			st2 = "MOVE CARTON CONTENTS";
			st3 = "PACKING";
			st4 = "PACK VERIFICATION";
			st5 = "SEND";
			st6 = "PREV";

		}
		//Case# 20148880 (LinkButton Functionality)
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_rf_mergepacking_orderno', 'customdeploy_rf_mergepacking_orderno_di');
		var linkURL_1 = checkInURL_1; 
		
		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di');
		var linkURL_2 = checkInURL_2; 
		var checkInURL_3 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_pack_scanorder', 'customdeploy_ebiz_rf_pack_scanorder_di');
		var linkURL_3 = checkInURL_3; 
		
		var functionkeyHtml=getFunctionkeyScript('_rfpackingmenu'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('selectoption').focus();";     
		html = html + "function checkrange(){var numRange=document.getElementById('selectoption').value; if(numRange!=null && numRange!=''){if(numRange =='1' || numRange =='2' || numRange =='3' || numRange =='4'){return true;}else{alert('Please enter valid Option');return false;}}}";      
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfpackingmenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1. " + st1;
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 2. " + st2;
		html = html + "				<td align = 'left'> 2. <a href='" + linkURL_2 + "' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 3. " + st3;
		html = html + "				<td align = 'left'> 3. <a href='" + linkURL_3 + "' style='text-decoration: none'>" + st3 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
	//	html = html + "			<tr>";
	//	html = html + "				<td align = 'left'> 4. " + st4;
	//	html = html + "				</td>";
//		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + "<input id='cmdSend' type='submit' value='ENT'/>";
		html = html + "				"	+ st6 + "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var optedField = request.getParameter('selectoption');
		var optedEvent = request.getParameter('cmdPrevious');
		var POarray=new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
    	
		
		var st7,st8,st9;
		 //case 20125642 start: spanish conversion,added "es_AR"
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{ //case 20125642 end
			st7 = "OPCI&#211;N V&#193;LIDA";
			//st8 = "MEN&#218; DEL EMBALAJE";
			st9 = "ESTA CARACTER&#205;STICA SE DISPONIBLE PR&#211;XIMO PARCHE / VERSION.";
		}
		else
		{
			st7 = "INVALID OPTION"	;
			//st8 = "PACKING MENU";
			st9 = "THIS FEATURE WILL BE AVAILABLE NEXT PATCH/VERSION.";
		}
		
    	POarray["custparam_error"] = st9;
    	POarray["custparam_screenno"] = 'Packingmenu';
    	
	
		
		
    	

		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);
		}
		else 
			if (optedField == '1') {
				response.sendRedirect('SUITELET', 'customscript_rf_mergepacking_orderno', 'customdeploy_rf_mergepacking_orderno_di', false, optedField);
			}
			else 
				if (optedField == '2') {
					response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, optedField);
				}
				else 
					if (optedField == '3') {
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanorder', 'customdeploy_ebiz_rf_pack_scanorder_di', false, POarray);
					}
					else 
						if (optedField == '4') {
							//response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, optedField);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						}
						else
							{
							POarray["custparam_error"] = st7;
					    	POarray["custparam_screenno"] = 'Packingmenu';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}