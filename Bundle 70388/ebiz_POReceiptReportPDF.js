/***************************************************************************
????????????????????????eBizNET
????????????????????eBizNET Solutions Inc
****************************************************************************
*
*? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_POReceiptReportPDF.js,v $
*? $Revision: 1.1.2.5.4.1.4.1 $
*? $Date: 2013/07/04 18:52:10 $
*? $Author: spendyala $
*? $Name: t_NSWMS_2013_1_7_16 $
*
* DESCRIPTION
*? Functionality
*
* REVISION HISTORY
*? $Log: ebiz_POReceiptReportPDF.js,v $
*? Revision 1.1.2.5.4.1.4.1  2013/07/04 18:52:10  spendyala
*? case# 20120118
*? Issue fixed against the case#201215488
*?
*? Revision 1.1.2.5.4.1  2012/11/01 14:55:15  schepuri
*? CASE201112/CR201113/LOG201121
*? Decimal Qty Conversions
*?
*? Revision 1.1.2.5  2012/08/31 01:42:08  snimmakayala
*? CASE201112/CR201113/LOG201121
*? Fisk Production Issue Fixes
*?
*? Revision 1.1.2.4  2012/07/16 10:41:12  mbpragada
*? 20120490
*? CODE FIX FOR INSERTING DUPLICATE LINES
*?
*? Revision 1.1.2.3  2012/06/20 07:18:52  mbpragada
*? 20120490
*?
*? Revision 1.3  2012/01/17 14:55:00  spendyala
*? CASE201112/CR201113/LOG201121
*? Added Partial Qty field.
*?
*? Revision 1.2  2012/01/16 07:24:04  spendyala
*? CASE201112/CR201113/LOG201121
*? design modification and data correction.
*?
*? Revision 1.1  2012/01/12 10:44:03  spendyala
*? CASE201112/CR201113/LOG201121
*? generates PDF file for POReceipt.
*?
*
****************************************************************************/


/**
 * @param request
 * @param response
 */
function POReceiptReportPDF(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('POReceipt Report');

		var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;

		var poreceipt = '';
		if (request.getParameter('custparam_poreceipt') != null && request.getParameter('custparam_poreceipt') != "") {
			poreceipt = request.getParameter('custparam_poreceipt');
		}
		var potrailer ='';
		if (request.getParameter('custparam_potrailer') != null && request.getParameter('custparam_potrailer') != "") {
			potrailer = request.getParameter('custparam_potrailer');
		}

		nlapiLogExecution('ERROR','poreceipt',poreceipt);
		nlapiLogExecution('ERROR','potrailer',potrailer);

		var filterPOReceipt=new Array();
		if(poreceipt!=null && poreceipt!="")
			filterPOReceipt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_receiptno',null,'is',poreceipt));
		if(potrailer!=null && potrailer!="")
			filterPOReceipt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_trailerno',null,'is',potrailer));	
		filterPOReceipt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_receiptqty', null, 'greaterthan',0));
		var columnPOReceipt=new Array();
		columnPOReceipt[0]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_item');
		columnPOReceipt[1]=new nlobjSearchColumn('Description','custrecord_ebiz_poreceipt_item');
		columnPOReceipt[2]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptqty');
		columnPOReceipt[3]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_trailerno');
		columnPOReceipt[4]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptno');
		columnPOReceipt[5]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_pono');
		columnPOReceipt[6]=new nlobjSearchColumn('custrecord_ebiz_poreceipt_lineno');

		var searchrecordPOReceipt=nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt',null,filterPOReceipt,columnPOReceipt);

		var item='';
		var itemId='';
		var itemdescription='';
		var palletqty='';
		var Noofpallet='';
		var totalqty='';
		var arrivalDate='';
		var containerNo='';
		var Noofpalletsgrandtotal=0;
		var totalqtygrandtotal=0;
		var partialqtygrandtotal=0;
		var partialqty=0;
		var lineno;
		var POID;

		if(searchrecordPOReceipt)
		{
			containerNo=searchrecordPOReceipt[0].getValue('custrecord_ebiz_poreceipt_trailerno');
			if(containerNo)
			{
				//get the arrival date from trailer table for the given trailer#.
				var filterTrailer=new Array();
				filterTrailer.push(new nlobjSearchFilter('custrecord_ebizappointmenttrailer',null,'is',containerNo));

				var columnTrailer=new Array();
				columnTrailer[0]=new nlobjSearchColumn('custrecord_ebizarrivaldate');

				var searchTrailer=nlapiSearchRecord('customrecord_ebiznet_trailer',null,filterTrailer,columnTrailer);

				if(searchTrailer)
					arrivalDate=searchTrailer[0].getValue('custrecord_ebizarrivaldate');
			}
		}

		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body  font-size=\"8\" padding-top=\" 13mm\" padding-right=\"3mm\"  padding-left=\"3mm\">\n";
		nlapiLogExecution('ERROR', 'xml0 ', "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"18\">\n");

		var strxml = "<table width='100%'>";
		strxml += "<tr>";
		strxml += "<td align='center'>";
		strxml += "<table width='100%'>";
		strxml += " <tr>";
		strxml += " <td align='center'>";
		strxml += "    <h3 style='height: 36px; margin-bottom: 3px'>";
		strxml += "         <b>Dynacraft BSC, Inc.</b></h3>";
		strxml += " </td>";
		strxml += " </tr>";
		strxml += " <tr>";
		strxml += "    <td align='center'>";				 
		strxml += " <h4> Expected Receipts - by Container</h4>";
		strxml += "</td>";
		strxml += " </tr>";
		strxml += " </table>";
		strxml += " </td>";
		strxml += "</tr>";
		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";
		strxml += " <tr>";
		strxml += " <td>";
		strxml += " <table width='100%'>";
		strxml += " <tr>";
		strxml += " <td align='left'>";
		strxml += "   <u style='padding-left: 30px;font-size:large'>Container</u>";
		strxml += " </td>";
		strxml += " <td align='left'>";
		//strxml += " <u style='font-size:large'>Name</u>";
		strxml += " </td>";
		strxml += " <td align='center'>";
		strxml += "  <u style='font-size:large'>Arrival Date</u>";
		strxml += " </td>";
		strxml += " </tr>";
		strxml += "<tr style='font-size:large'><td align='left'>";
		if(containerNo != null && containerNo!= "")
		{
			strxml += "<barcode codetype=\"code128\" showtext=\"false\" value=\"";
			strxml += containerNo.replace(replaceChar,'');
			strxml += "\"/>";
		}
		strxml += "</td>";
		strxml += "<td align='left'><b>";
		if(containerNo != null && containerNo!= "")
		{
			containerNo = containerNo.replace(replaceChar,'');
		}
		strxml += containerNo;
		strxml += "</b></td>" ;
		strxml += "<td align='center'><b>";
		strxml += arrivalDate ;
		strxml += "</b></td></tr>";
		strxml += "</table></td></tr>";

		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";
		strxml += "<tr><td></td></tr>";

		strxml += "<tr><td><table width='100%'>";
		strxml += "<tr><td align='center' ><u style='font-size:large'> Product ID</u></td>" ;
		strxml += "<td align='center'><u style='font-size:large'>Name</u></td>";
		strxml += "<td align='center'><u style='font-size:large'>Desc</u></td>";
		strxml += "<td align='center'><u style='font-size:large'>#Pallets</u></td>" ;
		strxml += "<td align='center'><u style='font-size:large'>Pallet Qty</u></td>";

		//strxml += "<td align='center'><u style='font-size:large'>Partial Qty</u></td>" ;
		strxml += "<td align='center'><u style='font-size:large'>Qty</u></td></tr>";
		if(searchrecordPOReceipt)
		{
			for ( var count = 0; count < searchrecordPOReceipt.length; count++) 
			{
				Noofpalletsgrandtotal=0;totalqtygrandtotal=0;
				item=searchrecordPOReceipt[count].getText('custrecord_ebiz_poreceipt_item');
				itemId=searchrecordPOReceipt[count].getValue('custrecord_ebiz_poreceipt_item');
				itemdescription=searchrecordPOReceipt[count].getValue('Description','custrecord_ebiz_poreceipt_item');
				totalqty=searchrecordPOReceipt[count].getValue('custrecord_ebiz_poreceipt_receiptqty');
				POID=searchrecordPOReceipt[count].getValue('custrecord_ebiz_poreceipt_pono');
				lineno=searchrecordPOReceipt[count].getValue('custrecord_ebiz_poreceipt_lineno');
				totalqtygrandtotal=parseFloat(totalqtygrandtotal)+parseFloat(totalqty);
				if(itemId)
				{
					//get the pallet qty from item Dimensions for the given item.
					var filterItemDim=new Array();
					filterItemDim.push(new nlobjSearchFilter('custrecord_ebizitemdims',null,'anyof',itemId));
					filterItemDim.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim',null,'anyof',3));//3=pallet

					var columnItemDim=new Array();
					columnItemDim[0]=new nlobjSearchColumn('custrecord_ebizqty');

					var searchItemDim=nlapiSearchRecord('customrecord_ebiznet_skudims',null,filterItemDim,columnItemDim);

					if(searchItemDim)
					{
						palletqty=searchItemDim[0].getValue('custrecord_ebizqty');
					}
					Noofpallet=parseFloat(totalqty)/parseFloat(palletqty);
					nlapiLogExecution('ERROR','POID',POID);
					var poload = nlapiLoadRecord('purchaseorder', POID);
					var itempackcode = poload.getLineItemValue('item', 'custcol_nswmspackcode', lineno);
					nlapiLogExecution('ERROR','totalqty',totalqty);
					nlapiLogExecution('ERROR','palletqty',palletqty);
					nlapiLogExecution('ERROR','itempackcode',itempackcode);
					if(itempackcode == null || itempackcode == '')
						itempackcode=1;
					//partialqty=(parseInt(totalqty)%parseInt(palletqty))/parseInt(itempackcode);
					partialqty=(parseInt(totalqty)%parseInt(palletqty));
					nlapiLogExecution('ERROR','partialqty',partialqty);
					partialqtygrandtotal=parseFloat(partialqtygrandtotal)+parseFloat(partialqty);
					Noofpalletsgrandtotal=parseFloat(Noofpalletsgrandtotal)+parseFloat(Noofpallet);
				}

				strxml += "<tr style='font-size:large'><td align='center'>" ;
				if(item != null && item!= "")
				{
					item = item.replace(replaceChar,'');				

					strxml += "<barcode codetype=\"code128\" showtext=\"false\" value=\"";
					strxml += item;
					strxml += "\"/>";
				}
				strxml += "</td>";
				strxml += "<td align='center'><b>";
				strxml += item ;
				strxml += "</b></td>" ;
				strxml += "<td align='center'>" ;
				if(itemdescription != null && itemdescription!= "")
				{
					itemdescription = itemdescription.replace(replaceChar,'');	
				}
				strxml += itemdescription;
				strxml += "</td>" ;
				strxml += "<td align='center'>" ;
				strxml += parseFloat(Noofpallet);
				strxml += "</td>";
				strxml += "<td align='center'>";
				strxml += parseFloat(palletqty);
				strxml += "</td>" ;
//				strxml += "<td align='center'>";
//				strxml += parseFloat(partialqty);
//				strxml += "</td>" ;
				strxml += "<td align='center'>" ;
				//strxml += totalqty ;
				strxml += parseFloat(Noofpallet)* parseFloat(palletqty);
				strxml += "</td></tr>";

				strxml += "<tr style='font-size:large'><td align='center'>" ;

				strxml += "</td>";
				strxml += "<td align='center'><b>";

				strxml += "</b></td>" ;
				strxml += "<td align='center'>" ;

				strxml += "</td>" ;
				if(partialqty == 0){
					strxml += "<td align='center'>0" ;				 
					strxml += "</td>";
				}
				else
				{
					strxml += "<td align='center'>1" ;				 
					strxml += "</td>";
				}
				strxml += "<td align='center'>";
				strxml += parseFloat(partialqty);
				strxml += "</td>" ;
//				strxml += "<td align='center'>";
//				strxml += parseFloat(partialqty);
//				strxml += "</td>" ;
				strxml += "<td align='center'>" ;
				strxml += parseFloat(1)* parseFloat(partialqty);
				strxml += "</td></tr>";

				strxml += "<tr style='font-size:large'><td></td><td></td><td align='center'><b>Total</b></td><td align='center'><b>";
				//strxml += Noofpalletsgrandtotal;
				if(partialqty == 0){
					strxml += parseFloat(Noofpallet)+ parseFloat(0);
				}
				else
				{
					strxml += parseFloat(Noofpallet)+ parseFloat(1);
				}
				strxml += "</b></td>" ;
//				strxml +="<td align='center'><b>";
//				strxml += partialqtygrandtotal;
//				strxml += "</b></td>" ;
				strxml += "<td></td><td align='center'><b>";
				//strxml += totalqtygrandtotal;
				strxml += parseFloat(Noofpallet)* parseFloat(palletqty) + parseFloat(1)* parseFloat(partialqty);
				strxml += "</b></td></tr>";
				strxml += "<tr><td colspan='8'>";
				strxml += " _________________________________________________________________________________________________________________________________________________";
				strxml += " </td></tr>";
				//strxml += "</table>";	
				//strxml += "</table></td></tr>";
				//strxml += "</table>";
			}
		}
//		strxml += "<tr style='font-size:large'><td></td><td></td><td></td><td align='center'><b>Total</b></td><td align='center'><b>";
//		strxml += Noofpalletsgrandtotal;
//		strxml += "</b></td>" ;
////		strxml +="<td align='center'><b>";
////		strxml += partialqtygrandtotal;
////		strxml += "</b></td>" ;
//		strxml += "<td align='center'><b>";
//		strxml += totalqtygrandtotal;
//		strxml += "</b></td></tr>";
//		strxml += "<tr><td colspan='8'>";
//		strxml += " _________________________________________________________________________________________________________________________________________________";
//		strxml += " </td></tr>";
//		//strxml += "</table>";	
		strxml += "</table></td></tr>";
		strxml += "</table>";

		if(searchrecordPOReceipt!=null && searchrecordPOReceipt!='' && searchrecordPOReceipt.length >= 3){
			nlapiLogExecution('ERROR', 'searchrecordPOReceipt.length', searchrecordPOReceipt.length);
			nlapiLogExecution('ERROR', 'into if', 'yes');
			strxml += "<table style='height: 380px;font-size:large'><tr valign='bottom'><td valign='bottom'><table align='left'><tr><td>";}
		else if(searchrecordPOReceipt!=null && searchrecordPOReceipt!='' && searchrecordPOReceipt.length >= 2){
			nlapiLogExecution('ERROR', 'searchrecordPOReceipt.length', searchrecordPOReceipt.length);
			nlapiLogExecution('ERROR', 'into else', 'yes');
			strxml += "<table style='height: 450px;font-size:large'><tr valign='bottom'><td valign='bottom'><table align='left'><tr><td>";}
		else {
			nlapiLogExecution('ERROR', 'into else', 'yes');
			strxml += "<table style='height: 500px;font-size:large'><tr valign='bottom'><td valign='bottom'><table align='left'><tr><td>";}
		//strxml += "<tr style='vertical-align:bottom'><td colspan='6' style='vertical-align:bottom'>";
		//strxml += "<table style='height: 500px;font-size:large'><tr valign='bottom'><td valign='bottom'><table align='left'><tr><td>";
		strxml += "<b>Dock # :_________________Date:_________________</b>";
		strxml += "</td>        </tr>        <tr>            <td>";
		strxml += "<b>Name :________________________________________</b>";
		strxml += "</td>        </tr>          <tr>            <td>";
		strxml += "<b>Name :________________________________________ </b></td></tr>";
		strxml += "<tr>            <td>";
		strxml += "<b>Start :_______________Warehouse:_______________</b> </td></tr>";
		strxml += "<tr>            <td>";
		strxml += "<b>Finish :_______________________________________ </b> </td></tr>";
		strxml += "</table></td></tr></table>";

		strxml += "\n</body>\n</pdf>";
		nlapiLogExecution('ERROR', 'strxml', strxml);
		xml = xml + strxml;
		nlapiLogExecution('ERROR', 'XML', xml);
		var file = nlapiXMLToPDF(xml);
		response.setContentType('PDF', 'PutawayReport-PO' + poreceipt + '.pdf');
		response.write(file.getValue());
	}
}

