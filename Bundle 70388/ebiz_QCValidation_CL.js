
/***************************************************************************
                                                                 
   eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_QCValidation_CL.js,v $
*  $Revision: 1.2.14.1 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_QCValidation_CL.js,v $
*  Revision 1.2.14.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function fnCheckQCValidation(type)
{	 
	 var vname =  nlapiGetFieldValue('name');	
	 
	 var filters = new Array();
	 filters[0] = new nlobjSearchFilter('name', null, 'is',vname);	

	 var searchresults = nlapiSearchRecord('customrecord_ebiznet_qc_validations', null, filters);

	 if(searchresults!=null && searchresults.length>0)
	 {
		 alert("This Record already exists.");
		 return false;
	 }
	 else
	 {
	 	return true;
	 } 

}

function fnCheckQCValidationEdit(type,name)
{	
 //alert(type);
	var vid=nlapiGetFieldValue('id');
	 var vname=nlapiGetFieldValue('name');

	 var varQCValidatioName =  nlapiGetFieldValue('custrecord_validation_name'); 	
	 var filters = new Array();
	 filters[0] = new nlobjSearchFilter('name', null, 'is',vname);
	 filters[1] = new nlobjSearchFilter('internalid', null, 'noneof',vid);	

	 var searchresults = nlapiSearchRecord('customrecord_ebiznet_qc_validations', null, filters);

	 if(searchresults!=null && searchresults.length>0)
	 {
		 alert("This Record already exists.");
		 return false;
	 }
	 else
	 {
	 	return true;
	 } 	
return true;
}