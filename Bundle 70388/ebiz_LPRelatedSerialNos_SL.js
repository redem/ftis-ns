/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_LPRelatedSerialNos_SL.js,v $
 *     	   $Revision: 1.4.4.1.4.1.4.3 $
 *     	   $Date: 2013/10/15 15:43:12 $
 *     	   $Author: rmukkera $
 *     	   $Name: t_NSWMS_2014_1_1_174 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_LPRelatedSerialNos_SL.js,v $
 * Revision 1.4.4.1.4.1.4.3  2013/10/15 15:43:12  rmukkera
 * Case# 20124192�
 *
 * Revision 1.4.4.1.4.1.4.2  2013/09/02 15:47:58  skreddy
 * Case# 20124201
 * standard bundle issue fix
 *
 * Revision 1.4.4.1.4.1.4.1  2013/06/19 22:58:31  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 *
 * Revision 1.4.4.1.4.1  2012/10/26 14:01:52  skreddy
 * CASE201112/CR201113/LOG201121
 * Issue related to Link for the Lp In inventory Report
 *
 * Revision 1.4.4.1  2012/05/04 13:30:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * confirm putaway
 *
 * Revision 1.4  2011/07/21 05:00:55  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.3  2011/07/21 04:59:52  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
function ebiznet_LPRelatedSerialNos_onclick(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm("Serial Numbers");
		var lp;
		var ItemId;
//		if (request.getParameter('custparam_serialskulp') != null) {
//			lp = request.getParameter('custparam_serialskulp');
//		}
		if (request.getParameter('custparam_lp') != null) {
			lp = request.getParameter('custparam_lp');
		}
//case # 20124201 start
		if (request.getParameter('custparam_itemid') != null && request.getParameter('custparam_itemid') != '') {
			ItemId = request.getParameter('custparam_itemid');
		}//end
		
		var sublist = form.addSubList("custpage_items", "list", "Serial #");
		sublist.addField("custpage_serialno", "text", "Serial #");
		
		nlapiLogExecution('ERROR','lp',lp);
		
		var filters = new Array();
		if (request.getParameter('custparam_lp') != null && request.getParameter('custparam_lp') != ""){
		filters.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp));
		filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));
		}
//case # 20124201 start
		if(ItemId != null && ItemId != '' && ItemId != 'null')
		filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', ItemId));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_serialnumber'));

		//var searchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp), new nlobjSearchColumn('custrecord_serialnumber'));
		var searchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			form.getSubList('custpage_items').setLineItemValue('custpage_serialno', i + 1, searchresults[i].getValue('custrecord_serialnumber'));
		}
		response.writePage(form);
	}
	else {
	}
}
