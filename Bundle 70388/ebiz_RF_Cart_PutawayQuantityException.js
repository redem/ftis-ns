/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_PutawayQuantityException.js,v $
 * $Revision: 1.2.2.12.4.9.2.41.2.1 $
 * $Date: 2015/11/19 14:18:42 $
 * $Author: schepuri $
 * $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Cart_PutawayQuantityException.js,v $
 * Revision 1.2.2.12.4.9.2.41.2.1  2015/11/19 14:18:42  schepuri
 * case# 201415763
 *
 * Revision 1.2.2.12.4.9.2.41  2015/09/02 15:32:00  aanchal
 * 2015.2 issue fixes
 * 201414233
 *
 * Revision 1.2.2.12.4.9.2.40  2015/08/04 15:32:40  skreddy
 * Case# 201413766
 * 2015.2 compatibility issue fix
 *
 * Revision 1.2.2.12.4.9.2.39  2015/05/26 22:27:50  sponnaganti
 * Case# 201412924
 * We have issue while doing qty exception if entered qty is lessthan expected qty then Transaction order line is upadting with negative value.
 *
 * Revision 1.2.2.12.4.9.2.38  2015/04/29 15:29:20  skreddy
 * Case# 201412539
 * LP SB issue fix
 *
 * Revision 1.2.2.12.4.9.2.37  2015/03/04 13:44:45  schepuri
 * issue fix # 201411604
 *
 * Revision 1.2.2.12.4.9.2.36  2015/01/22 13:35:24  schepuri
 * issue # 201411375
 *
 * Revision 1.2.2.12.4.9.2.35  2014/11/11 06:08:56  vmandala
 * case#  201411007 Stdbundle issue fixed
 *
 * Revision 1.2.2.12.4.9.2.34  2014/11/05 10:46:04  sponnaganti
 * Case# 201410939
 * True Fab SB Issue fixed
 *
 * Revision 1.2.2.12.4.9.2.33  2014/08/05 15:24:22  sponnaganti
 * Case# 20149823
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.12.4.9.2.32  2014/08/04 16:03:41  sponnaganti
 * Case# 20149820
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.12.4.9.2.31  2014/07/25 06:32:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149554
 *
 * Revision 1.2.2.12.4.9.2.30  2014/07/24 15:06:06  sponnaganti
 * Case# 20149611 20149612
 * Compatibility Issue fix
 *
 * Revision 1.2.2.12.4.9.2.29  2014/07/21 16:00:43  sponnaganti
 * Case# 20148231
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.12.4.9.2.28  2014/07/16 15:32:40  skavuri
 * Case# 20149237 Compatability Issue Fixed
 *
 * Revision 1.2.2.12.4.9.2.27  2014/07/04 23:24:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149309
 *
 * Revision 1.2.2.12.4.9.2.26  2014/07/04 14:15:59  skavuri
 * Case # 20149237 Compatibility Issue Fixed
 *
 * Revision 1.2.2.12.4.9.2.25  2014/06/16 15:25:17  sponnaganti
 * case# 20148587
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.12.4.9.2.24  2014/06/13 07:34:40  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.2.12.4.9.2.23  2014/06/04 15:06:57  sponnaganti
 * Case# 20148658  20147951
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.12.4.9.2.22  2014/05/29 15:28:45  skreddy
 * case # 20148139
 * Standard Bundle issue fix
 *
 * Revision 1.2.2.12.4.9.2.21  2014/05/28 15:08:13  sponnaganti
 * case# 20148593
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.12.4.9.2.20  2014/05/27 15:39:19  sponnaganti
 * case# 20148506
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.12.4.9.2.19  2014/05/02 15:55:47  skavuri
 * Case # 20148237 SB Issue Fixed
 *
 * Revision 1.2.2.12.4.9.2.18  2014/04/22 15:55:56  skavuri
 * Case # 20148130 SB Issue fixed
 *
 * Revision 1.2.2.12.4.9.2.17  2014/04/21 15:56:48  skavuri
 * Case # 20148101 SB Issue fixed
 *
 * Revision 1.2.2.12.4.9.2.16  2014/04/02 15:54:09  skavuri
 * Case # 20127917 issue fixed
 *
 * Revision 1.2.2.12.4.9.2.15  2014/01/23 13:39:33  schepuri
 * 20126923
 * Standard bundle issue fix
 *
 * Revision 1.2.2.12.4.9.2.14  2013/11/09 01:56:32  gkalla
 * Case# 20125644
 * TO Qty exception failure
 *
 * Revision 1.2.2.12.4.9.2.13  2013/10/18 15:47:20  skreddy
 * Case# 20124997
 * Affosa SB  issue fix
 *
 * Revision 1.2.2.12.4.9.2.12  2013/09/26 15:55:48  grao
 * Issue fixes for 20124616
 *
 * Revision 1.2.2.12.4.9.2.11  2013/09/25 15:49:56  rmukkera
 * Case# 20124534
 *
 * Revision 1.2.2.12.4.9.2.10  2013/08/19 16:14:14  rmukkera
 * Issue Fix related to 20123915�,20123912�
 *
 * Revision 1.2.2.12.4.9.2.9  2013/06/28 15:39:08  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *  Case# 20123242
 * Surftech ssue
 *
 * Revision 1.2.2.12.4.9.2.8  2013/06/26 15:55:27  skreddy
 * Case# 20123114
 * issue rellated to qty exception for multiple item
 *
 * Revision 1.2.2.12.4.9.2.7  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.2.12.4.9.2.6  2013/05/10 11:02:10  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.2.2.12.4.9.2.5  2013/04/25 13:10:03  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.2.2.12.4.9.2.4  2013/04/19 12:22:38  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.2.2.12.4.9.2.3  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.2.12.4.9.2.2  2013/03/08 14:35:02  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.2.2.12.4.9.2.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.2.2.12.4.9  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.2.2.12.4.8  2012/12/24 15:19:27  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.2.2.12.4.7  2012/12/21 15:30:36  skreddy
 * CASE201112/CR201113/LOG201121
 * display blank screen
 *
 * Revision 1.2.2.12.4.6  2012/11/11 03:40:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * GSUSA UAT issue fixes.
 *
 * Revision 1.2.2.12.4.5  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.12.4.4  2012/09/27 10:54:36  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.2.2.12.4.3  2012/09/26 12:51:50  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.2.12.4.2  2012/09/24 22:45:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issues related to Expiry date is resolved.
 *
 * Revision 1.2.2.12.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.2.2.12  2012/09/03 13:45:29  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.2.2.11  2012/08/20 23:37:41  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix as a part of 339 bundle i.e.,
 * Driving user to scan next lp task when
 * user choose zero quantity(at actual qty) in Qty exception screen
 *
 * Revision 1.2.2.10  2012/08/13 05:07:28  skreddy
 * t_NSWMS_LOG201121_343
 * Added new Lp field
 *
 * Revision 1.2.2.9  2012/05/24 15:21:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to wbc was resolved.
 *
 * Revision 1.2.2.8  2012/04/27 15:07:41  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new field i.e Remaining qty .
 *
 * Revision 1.2.2.7  2012/04/24 14:59:04  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to qty entering against Correct LP is fixed.
 *
 * Revision 1.2.2.6  2012/04/19 15:11:42  spendyala
 * CASE201112/CR201113/LOG201121
 * sys generates new chkn and putaway task when ever the user performs qty exception .
 *
 * Revision 1.2.2.5  2012/04/17 10:38:39  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Cart To Checkin
 *
 * Revision 1.2.2.4  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.2.3  2012/03/02 14:18:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * cart putaway qty exception
 *
 * Revision 1.2.2.2  2012/02/22 12:18:02  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2  2012/02/16 10:36:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.1  2012/02/02 09:00:33  rmukkera
 * CASE201112/CR201113/LOG201121
 *   cartlp new file
 *
 *
 ****************************************************************************/

function PutawayQuantityException(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		//	Get the LP#, Quantity, Location 
		//  from the previous screen, which is passed as a parameter

		var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
		nlapiLogExecution('DEBUG', 'lpNumbersFromArray', TempLPNoArray);

		var getLPNo = request.getParameter('custparam_lpno');
		var getQuantity = request.getParameter('custparam_quantity');
		var getFetchedLocation = request.getParameter('custparam_location');
		var getFetchedItem = request.getParameter('custparam_item');
		var getFetchedItemDescription = request.getParameter('custparam_itemDescription');
		var getconfirmedLpCount = request.getParameter('custparam_confirmedLpCount');
		var getrecordcount = request.getParameter('custparam_recordcount');
		var getpono = request.getParameter('custparam_pono');
		var getexceptionQuantityflag = request.getParameter('custparam_exceptionQuantityflag');
		var getoption = request.getParameter('custparam_option');
		var getlineno = request.getParameter('custparam_lineno');
		var getitempalletquantity = request.getParameter('custparam_itempalletquantity');
		var getlpCount = request.getParameter('custparam_lpCount');
		var getcartno = request.getParameter('custparam_cartno');
		var getbeginlocation = request.getParameter('custparam_beginlocation');
		var getexceptionquantity = request.getParameter('custparam_exceptionquantity');
		var getrecordid = request.getParameter('custparam_recordid');
		var getlpNumbersArray = request.getParameter('custparam_lpNumbersArray');


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		//case 20124997 Start : added one more if condition
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{//case 20124997 end
			st0 = "";
			st1 = "PLACA";
			st2 = "INGRESAR LA CANTIDAD REAL";
			st3 = "RAZ&#211;N";
			st4 = "CANTIDAD RESTANTE";			
			st5 = "ENVIAR";
			st6 = "ANTERIOR";


		}
		else
		{
			st0 = "";
			st1 = "LP";
			st2 = "ENTER ACTUAL QUANTITY";
			st3 = "REASON";
			st4 = "REMAINING QUANTITY ";
			st5 = "SEND";
			st6 = "PREV";

		}





		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enterquantity').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>"; 

		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" +  st1 +" : <label>" + getLPNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterquantity' id='enterquantity' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " : ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterreason' type='text'/>";
		html = html + "				<input type='hidden' name='hdnLPNo' value=" + getLPNo + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnFetchedLocation' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnFetchedItem' value=" + getFetchedItem + ">";
		html = html + "				<input type='hidden' name='hdnFetchedItemDescription' value='" + getFetchedItemDescription + "'>";// case# 201416898
		html = html + "				<input type='hidden' name='hdnconfirmedLpCount' value=" + getconfirmedLpCount + ">";
		html = html + "				<input type='hidden' name='hdnrecordcount' value=" + getrecordcount + ">";
		html = html + "				<input type='hidden' name='hdnpono' value=" + getpono + ">";
		html = html + "				<input type='hidden' name='hdnexceptionQuantityflag' value=" + getexceptionQuantityflag + ">";
		html = html + "				<input type='hidden' name='hdnoption' value=" + getoption + ">";
		html = html + "				<input type='hidden' name='hdnlineno' value=" + getlineno + ">";
		html = html + "				<input type='hidden' name='hdnitempalletquantity' value=" + getitempalletquantity + ">";
		html = html + "				<input type='hidden' name='hdnlpCount' value=" + getlpCount + ">";
		html = html + "				<input type='hidden' name='hdncartno' value='" + getcartno + "'>";
		html = html + "				<input type='hidden' name='hdnbeginlocation' value=" + getbeginlocation + ">";
		html = html + "				<input type='hidden' name='hdnexceptionquantity' value=" + getexceptionquantity + ">";
		html = html + "				<input type='hidden' name='hdnrecordid' value=" + getrecordid + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + ": ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterremqty' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + "  : ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";	

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" +  st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st6 +  " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "				<td><input type='hidden' name='hdnlpNumbersArray' value=" + getlpNumbersArray + "></td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterquantity').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{

		// This variable is to hold the LP entered.
		var QuantityEntered = request.getParameter('enterquantity');
		nlapiLogExecution('DEBUG', 'QuantityEntered', QuantityEntered);

		var getReason = request.getParameter('enterreason');
		nlapiLogExecution('DEBUG', 'getReason', getReason);

		var getLPNo = request.getParameter('custparam_lpno');
		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);

		var RemainingQtyEntered=request.getParameter('enterremqty');
		nlapiLogExecution('DEBUG', 'RemainingQtyEntered', RemainingQtyEntered);
		//code added by santosh on 09Aug2012
		var NewLp=request.getParameter('enterlp');
		var LPReturnValue = "";
		//End of the code on 09Aug2012

		var tempRemainingQtyEntered=RemainingQtyEntered;
		if(tempRemainingQtyEntered==""||tempRemainingQtyEntered==null)
			tempRemainingQtyEntered=0;

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', 2);//2 STATUS.INBOUND.LOCATIONS_ASSIGNED
		if(getLPNo!=null&&getLPNo!='')
			filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lpno');
		columns[1] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[3] = new nlobjSearchColumn('custrecord_line_no');
		columns[4] = new nlobjSearchColumn('custrecord_comp_id');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_sku_status');
		columns[7] = new nlobjSearchColumn('custrecord_transport_lp');
		columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[9] = new nlobjSearchColumn('custrecord_batch_no');
		columns[10] = new nlobjSearchColumn('custrecord_sku');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		//case 20124997 Start : added one more if condition
		var st7,st8,st9;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{//case 20124997 end
			st7 = "CANTIDAD INV&#193;LIDA";
			st8 = "LP NO V&#193;LIDOS";
			st9 = "NO V&#193;LIDO CANTIDAD RESTANTE";
		}
		else
		{
			st7 = "INVALID QUANTITY";
			st8 = "INVALID LP";
			st9 = "INVALID REMAINING QUANTITY";
		}

		POarray["custparam_error"] = st7;
		POarray["custparam_screenno"] = 'CRT13';
		POarray["custparam_lpno"] = request.getParameter('hdnLPNo');
		POarray["custparam_hdnlpno_qtyexception"] = request.getParameter('hdnLPNo');
		POarray["custparam_quantity"] = request.getParameter('hdnQuantity');
		POarray["custparam_location"] = request.getParameter('hdnFetchedLocation');
		POarray["custparam_item"] = request.getParameter('hdnFetchedItem');
		POarray["custparam_itemDescription"] = request.getParameter('hdnFetchedItemDescription');
		POarray["custparam_confirmedLpCount"] = request.getParameter('hdnconfirmedLpCount');
		POarray["custparam_recordcount"] = request.getParameter('hdnrecordcount');
		POarray["custparam_pono"] = request.getParameter('hdnpono');
		POarray["custparam_exceptionQuantityflag"] = request.getParameter('hdnexceptionQuantityflag');
		POarray["custparam_option"] = request.getParameter('hdnoption');
		POarray["custparam_lineno"] = request.getParameter('hdnlineno');
		POarray["custparam_itempalletquantity"] = request.getParameter('hdnitempalletquantity');
		POarray["custparam_lpCount"] = request.getParameter('hdnlpCount');
		POarray["custparam_cartno"] = request.getParameter('hdncartno');
		POarray["custparam_beginlocation"] = request.getParameter('hdnbeginlocation');
		POarray["custparam_exceptionquantity"] = request.getParameter('hdnexceptionquantity');
		POarray["custparam_recordid"] = request.getParameter('hdnrecordid');
		POarray["custparam_lpNumbersArray"] = request.getParameter('hdnlpNumbersArray');

		nlapiLogExecution('DEBUG','POArray',POarray["custparam_option"]);
		nlapiLogExecution('DEBUG','POArray',POarray["custparam_cartno"]);
		nlapiLogExecution('DEBUG','POArray',POarray["custparam_beginlocation"]);


		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		try 
		{
			//	if the previous button 'F7' is clicked, it has to go to the previous screen 
			//  ie., it has to go to accept PO #.

			if (optedEvent == 'F7') 
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
			}
			else 
			{
				if (searchresults != null && searchresults.length > 0) 
				{
					nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');

					var expectedQty=searchresults[0].getValue('custrecord_expe_qty');
					nlapiLogExecution('DEBUG','expectedQty',expectedQty);

//					var location = searchresults[0].getValue('custrecord_wms_location');
//					nlapiLogExecution('DEBUG','location',location);

//					var company = searchresults[0].getValue('custrecord_comp_id');
//					nlapiLogExecution('DEBUG','company',company);

					var vBatchNo = searchresults[0].getValue('custrecord_batch_no');
					nlapiLogExecution('DEBUG','vBatchNo',vBatchNo);

					var POId =searchresults[0].getValue('custrecord_ebiz_cntrl_no');
					nlapiLogExecution('DEBUG','POId',POId);

					var lineno =searchresults[0].getValue('custrecord_line_no');
					nlapiLogExecution('DEBUG','lineno',lineno);

					POarray["custparam_skustatus"] = searchresults[0].getValue('custrecord_sku_status');

					var containerLP=searchresults[0].getValue('custrecord_transport_lp');
					nlapiLogExecution('DEBUG','containerLP',containerLP);

					var trantype = nlapiLookupField('transaction', POId, 'recordType');
					var PORec = nlapiLoadRecord(trantype, POId);
					var vorglineno = lineno;
					var lineSeq;
					if(trantype=='transferorder'){
						var PORec = nlapiLoadRecord(trantype, POId);

						var vTempLineNo=lineno;
						if(PORec != null && PORec != '')
						{
							var POItemLength= PORec.getLineItemCount('item');
							if(POItemLength != null && POItemLength != '')
							{
								for(var l=1;l<=POItemLength;l++)
								{
									if(PORec.getLineItemValue('item','line',l)==lineno)
									{
										vTempLineNo=l;
									}
								}
							}

						}
						//lineno=vTempLineNo;
						lineSeq=vTempLineNo;
					}
					else
					{
						//var lineSeq;
						for(var icount=1;icount<=PORec.getLineItemCount('item');icount++)
						{
							var vlineno=PORec.getLineItemValue('item','line',icount);
							nlapiLogExecution('DEBUG','vlineno',vlineno);
							if(vlineno==lineno)
							{
								lineSeq=icount;
								break;
							}
						}
					}
					nlapiLogExecution('DEBUG','lineSeq',lineSeq);
					var POIdText =PORec.getFieldValue('tranid');
					nlapiLogExecution('DEBUG','POIdText',POIdText);

					var receiptTypeId=PORec.getFieldValue('custbody_nswmsporeceipttype');
					nlapiLogExecution('DEBUG','receiptTypeId',receiptTypeId);

					var itemid=PORec.getLineItemValue('item','item',lineSeq);
					nlapiLogExecution('DEBUG','itemid',itemid);

					var itemQuantity=PORec.getLineItemValue('item','quantity',lineSeq);
					nlapiLogExecution('DEBUG','itemQuantity',itemQuantity);
					var WmsLocation;
					if(trantype=='transferorder')
						WmsLocation=PORec.getFieldValue('transferlocation');
					else
						WmsLocation=PORec.getFieldValue('location');

					var itempackcode=PORec.getLineItemValue('item','custcol_nswmspackcode',lineSeq);
					nlapiLogExecution('DEBUG','itempackcode',itempackcode);



					if(itempackcode==null||itempackcode=="")
					{
						var filterItemDim=new Array();
						filterItemDim[0]=new nlobjSearchFilter('custrecord_ebizitemdims',null,'anyof',itemid);
						filterItemDim[1]=new nlobjSearchFilter('custrecord_ebizbaseuom',null,'is','T');

						var columnItemDim=new Array();
						columnItemDim[0]=new nlobjSearchColumn('custrecord_ebizpackcodeskudim');

						var rec=nlapiSearchRecord('customrecord_ebiznet_skudims',null,filterItemDim,columnItemDim);
						if(rec!=null&&rec!="")
							itempackcode=rec[0].getValue('custrecord_ebizpackcodeskudim');
					}

					var itemQuantityReceived=PORec.getLineItemValue('item','quantityreceived',lineSeq);
					nlapiLogExecution('DEBUG','itemQuantityReceived',itemQuantityReceived);
					//code added by santosh on 09Aug12
					if(NewLp!="" && NewLp!=null)
					{
						nlapiLogExecution('DEBUG', 'getsysItemLP', 'userdefined');
						//LPReturnValue = ebiznet_LPRange_CL(NewLp, '2',WmsLocation,1);
						LPReturnValue= MastLPExists(NewLp);//check for manual cart lp exists

						nlapiLogExecution('DEBUG', 'LPReturnValue', LPReturnValue);

						if(LPReturnValue !="")
						{
							nlapiLogExecution('DEBUG', 'LPReturnValue_Lpcheckin', 'LPReturnValue');
							POarray["custparam_error"] = st8;
							POarray["custparam_screenno"] = 'CRT13';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return ;
						}

					}				
					//end of the code on 09Aug 12	

					// Case# 20148101 starts
					//if((QuantityEntered=='')||(isNaN(parseFloat(QuantityEntered))))
					//if((QuantityEntered=='')||(QuantityEntered<0)||(isNaN(parseFloat(QuantityEntered))))
					// Case# 20148101 ends
					//case 201411007
					if((QuantityEntered=='')||(QuantityEntered<0)||(isNaN(QuantityEntered)))// case# 201415763
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
					// Case # 20148237
					/*	// Case# 20148130 starts
					if(RemainingQtyEntered!=''&&RemainingQtyEntered!=null&&RemainingQtyEntered!='null')// Case# 20148237 
						{
					if((RemainingQtyEntered<0)||(isNaN(parseFloat(RemainingQtyEntered))))
						{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
						}
					// Case# 20148130 ends
					 */				// Case # 20148237
					else
					{

						if(RemainingQtyEntered==null || RemainingQtyEntered=='' || isNaN(RemainingQtyEntered))
							RemainingQtyEntered=0;					
						// Case# 20148130 starts
						if(RemainingQtyEntered!=''&&RemainingQtyEntered!=null&&RemainingQtyEntered!='null')// Case# 20148237 
						{
							//case 201411007
							if(RemainingQtyEntered<0)
							{
								POarray["custparam_error"] = 'Remaining Quantity must be grater than Zero';
								POarray["custparam_screenno"] = 'CRT13';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
						}
						// Case# 20148130 ends
						else if((RemainingQtyEntered=='' || RemainingQtyEntered==null) && (NewLp!="" && NewLp!=null))// case# 201411375
						{
							POarray["custparam_error"] = 'Please Enter Remaining Quantity for NewLP';
							POarray["custparam_screenno"] = 'CRT13';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;
						}

						nlapiLogExecution('DEBUG','itemremainingQty',(parseFloat(RemainingQtyEntered)+parseFloat(QuantityEntered)));
						if((parseFloat(RemainingQtyEntered)+parseFloat(QuantityEntered))<=parseFloat(expectedQty))
						{

							if(parseFloat(QuantityEntered)>parseFloat(expectedQty))
							{
								var PoOveageQty=checkPOOverage(receiptTypeId);
								nlapiLogExecution('DEBUG','PoOveageQty',PoOveageQty);

								var ItemRemaininingQuantity = parseFloat(itemQuantity)- parseFloat(itemQuantityReceived);
								nlapiLogExecution('DEBUG','ItemRemaininingQuantity',ItemRemaininingQuantity);

								var recommendedQty=parseFloat(ItemRemaininingQuantity) + (parseFloat(PoOveageQty) * parseFloat(itemQuantity))/100;
								nlapiLogExecution('DEBUG','recommendedQty',recommendedQty);

								if(parseFloat(QuantityEntered)<=parseFloat(recommendedQty))
								{
									var filterTransactionOrderLine=new Array();
									filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no',null,'equalto',POId));
									filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no',null,'equalto',lineno));
									filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_item',null,'anyof',itemid));


									var columnTransactionOrderLine=new Array();
									columnTransactionOrderLine[0]=new nlobjSearchColumn('custrecord_orderlinedetails_putexcep_qty');

									var searchTransactionOrderLineRec=nlapiSearchRecord('customrecord_ebiznet_order_line_details',null,filterTransactionOrderLine,columnTransactionOrderLine);

									if(searchTransactionOrderLineRec!=null&&searchTransactionOrderLineRec!='')
									{
										var previousQtyException=searchTransactionOrderLineRec[0].getValue('custrecord_orderlinedetails_putexcep_qty');


										if(previousQtyException==null||previousQtyException=='')
											previousQtyException=0;
										nlapiLogExecution('DEBUG','previousQtyException',previousQtyException);

										var qtyException=parseFloat(QuantityEntered)-parseFloat(expectedQty);
										nlapiLogExecution('DEBUG','qtyException',qtyException);

										var newQtyException=parseFloat(previousQtyException)+parseFloat(qtyException);

										var recId=searchTransactionOrderLineRec[0].getId();
										nlapiLogExecution('DEBUG','recId',recId);
										/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
										//float conversion
										nlapiSubmitField('customrecord_ebiznet_order_line_details', recId, 'custrecord_orderlinedetails_putexcep_qty',parseFloat(newQtyException).toFixed(4));
										/*** up to here ***/
									}
									POarray["custparam_exceptionqty"]=QuantityEntered;
									POarray["custparam_qtyexceptionflag"]='true';
									//case# 20148231 starts(Remaining cube issue by giving remaining qty field)
									POarray["custparam_remainingqty"]=parseFloat(QuantityEntered)-parseFloat(expectedQty);
									nlapiLogExecution('Error','POarray["custparam_remainingqty"]',POarray["custparam_remainingqty"]);
									//case# 20148231 ends
									nlapiLogExecution('DEBUG','if','chkpt');
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
									nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

								}
								else
								{
									nlapiLogExecution('DEBUG','else','chkpt');
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
								}
							}
							else if(parseFloat(QuantityEntered)<=parseFloat(expectedQty))
							{
								nlapiLogExecution('DEBUG','into else ','');
								var filterTransactionOrderLine=new Array();
								filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no',null,'equalto',POId));
								filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no',null,'equalto',lineno));
								filterTransactionOrderLine.push(new nlobjSearchFilter('custrecord_orderlinedetails_item',null,'anyof',itemid));

								var columnTransactionOrderLine=new Array();
								columnTransactionOrderLine[0]=new nlobjSearchColumn('custrecord_orderlinedetails_putexcep_qty');

								var searchTransactionOrderLineRec=nlapiSearchRecord('customrecord_ebiznet_order_line_details',null,filterTransactionOrderLine,columnTransactionOrderLine);

								if(searchTransactionOrderLineRec!=null&&searchTransactionOrderLineRec!='')
								{
									var previousQtyException=searchTransactionOrderLineRec[0].getValue('custrecord_orderlinedetails_putexcep_qty');

									if(previousQtyException==null||previousQtyException=='')
										previousQtyException=0;
									nlapiLogExecution('DEBUG','previousQtyException',previousQtyException);

									//var qtyException=parseFloat(QuantityEntered)-parseFloat(expectedQty);
									var qtyException=parseFloat(expectedQty)-parseFloat(QuantityEntered);
									nlapiLogExecution('DEBUG','qtyException',qtyException);

									var newQtyException=parseFloat(previousQtyException)+parseFloat(qtyException);

									var recId=searchTransactionOrderLineRec[0].getId();
									nlapiLogExecution('DEBUG','recId',recId);
									/*** The below code is merged from Endchoice account on 08thMar13 by Santosh as part of Standard bundle ***/
									//float conversion
									nlapiSubmitField('customrecord_ebiznet_order_line_details', recId, 'custrecord_orderlinedetails_putexcep_qty',parseFloat(newQtyException).toFixed(4));
									/*** up to here ***/
								}

								POarray["custparam_exceptionqty"]=QuantityEntered;
								POarray["custparam_qtyexceptionflag"]='true';
								//case# 20148231 starts
								POarray["custparam_remainingqty"]=parseFloat(expectedQty)-parseFloat(QuantityEntered);
								nlapiLogExecution('DEBUG', 'POarray["custparam_remainingqty"]', POarray["custparam_remainingqty"]);
								//case# 20148231 ends
								nlapiLogExecution('DEBUG','elseif','chkpt');
								try
								{
									var ActualBeginDate = DateStamp();
									var ActualBeginTime = TimeStamp();

									nlapiLogExecution('DEBUG', 'ActualBeginDate', ActualBeginDate);
									nlapiLogExecution('DEBUG', 'ActualBeginTime', ActualBeginTime);

									POarray["custparam_actualbegindate"] = ActualBeginDate;
									var TimeArray = new Array();
									TimeArray = ActualBeginTime.split(' ');

									var getActualBeginTime = TimeArray[0];
									var getActualBeginTimeAMPM = TimeArray[1];

									nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
									nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);
									var expqty=parseFloat(expectedQty)-parseFloat(QuantityEntered);
									var qtyreceived=0;
									var itemremqty=0;
									// case no start 20126923
									// New cart lp should be updated in Transport LP field
									if(NewLp!=null&&NewLp!="")
										var containerLP = NewLp;
									else
										var containerLP = POarray["custparam_cartno"];
									// case no end 20126923
									var ItemInfo = eBiz_RF_GetItemCubeForItem(itemid);


									nlapiLogExecution('DEBUG','ITEMINFO[0]',ItemInfo[0]);
									nlapiLogExecution('DEBUG','ITEMINFO[1]',ItemInfo[1]);
									nlapiLogExecution('DEBUG','containerLP',containerLP);

									POarray["custparam_itemcube"]=ItemInfo[0];
									POarray["custparam_baseuomqty"]=ItemInfo[1];

									var TotQtyEntered=parseFloat(tempRemainingQtyEntered)+parseFloat(QuantityEntered);
									nlapiLogExecution('DEBUG','TotQtyEntered',TotQtyEntered);
									nlapiLogExecution('DEBUG','expectedQty',expectedQty);
									if(TotQtyEntered<=parseFloat(expectedQty))
									{
										updateTrnOrderLine(POId,vorglineno,expqty);
										updateChknTaskQty(getLPNo,containerLP,QuantityEntered,POarray["custparam_recordid"],vBatchNo);

										if(RemainingQtyEntered!=""&&RemainingQtyEntered!=null)
											expqty=parseFloat(RemainingQtyEntered);
										nlapiLogExecution('DEBUG','RemainingQtyEntered1',RemainingQtyEntered);
										nlapiLogExecution('DEBUG','expqty1',expqty);
										if(parseFloat(RemainingQtyEntered)!=0)
										{
											//case# 20149823 (changed because need to update serialparentid in serial entry for remaining qty)
											//POarray["custparam_remainingqty"]=RemainingQtyEntered;
											POarray["custparam_remainingqtyserial"]=RemainingQtyEntered;
											//case# 20149823 ends
											CheckinLp(itemid, POIdText,itemid, vorglineno,POId, expqty,itemremqty, 
													itempackcode,POarray["custparam_skustatus"],
													POarray["custparam_quantity"], qtyreceived,
													POarray["custparam_itemDescription"], POarray["custparam_itemcube"], 
													ActualBeginDate, getActualBeginTime, getActualBeginTimeAMPM,
													WmsLocation,containerLP,NewLp,vBatchNo,POarray);
										}
									}
									else
									{
										POarray["custparam_error"] = st9;
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
										return;
									}
								}
								catch(exp)
								{
									nlapiLogExecution('DEBUG','EXCEPTION in checkin lp',exp);
								}
								nlapiLogExecution('DEBUG','QuantityEntered CHKPT',QuantityEntered);

								var itemSubtype = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
								var serialFlag = itemSubtype.custitem_ebizserialin;

								if(parseFloat(QuantityEntered)==0)
								{
									POarray["custparam_hdnlpno_qtyexception"] = "";
									POarray["custparam_exceptionQuantityflag"] = 'false';
									POarray["custparam_exceptionquantity"] = 'false';
									POarray["custparam_qtyexceptionflag"]='false';
									//a Date object to be used for a random value
									var now = new Date();
									//now= now.getHours();
									//Getting time in hh:mm tt format.
									var a_p = "";
									var d = new Date();
									var curr_hour = now.getHours();
									if (curr_hour < 12) {
										a_p = "am";
									}
									else {
										a_p = "pm";
									}
									if (curr_hour == 0) {
										curr_hour = 12;
									}
									if (curr_hour > 12) {
										curr_hour = curr_hour - 12;
									}

									var curr_min = now.getMinutes();

									curr_min = curr_min + "";

									if (curr_min.length == 1) {
										curr_min = "0" + curr_min;
									}
									nlapiLogExecution("ERROR", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p));

									var currentUserID = getCurrentUser();
									nlapiLogExecution('DEBUG', 'currentUserID', currentUserID);

									var EndLocationId=searchresults[0].getValue('custrecord_actbeginloc');
									nlapiLogExecution('DEBUG', 'EndLocationId', EndLocationId);

									/*
									 * This block is to update the remaining cube for the bin location that was assigned
									 * by the system. 
									 */
									var vexpqty=parseFloat(expectedQty)-parseFloat(QuantityEntered);


									var itemDimensions = getSKUCubeAndWeight(itemid,"");
									var itemCube = itemDimensions[0];

									var TotalItemCube = parseFloat(itemCube) * parseFloat(vexpqty);
									nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );

									var oldBinLocationId = EndLocationId;
									var oldBinLocationRemainingCube =  GeteLocCube(oldBinLocationId);
									var actualRemainingCube = parseFloat(oldBinLocationRemainingCube) + parseFloat(TotalItemCube);

									nlapiLogExecution('DEBUG', 'oldBinLocationId ', oldBinLocationId );
									nlapiLogExecution('DEBUG', 'oldBinLocationRemainingCube ', oldBinLocationRemainingCube );
									nlapiLogExecution('DEBUG', 'actualRemainingCube ', actualRemainingCube );

									UpdateLocCube(oldBinLocationId, parseFloat(actualRemainingCube));
									//update the remaining loc cube.

									var recid=searchresults[0].getId();
									var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
									transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
									transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
									transaction.setFieldValue('custrecord_upd_date', DateStamp());
									transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
									transaction.setFieldValue('custrecord_act_end_date', DateStamp());
									transaction.setFieldValue('custrecord_wms_status_flag', 3);
									transaction.setFieldValue('custrecord_act_qty', parseFloat(QuantityEntered).toFixed(4));
									transaction.setFieldValue('custrecord_taskassignedto', currentUserID);
									nlapiLogExecution('DEBUG', 'POId', POId);
									transaction.setFieldValue('custrecord_ebiz_order_no', POId);
									var vputwrecid=nlapiSubmitRecord(transaction, false, true);
									nlapiLogExecution('DEBUG', 'vputwrecid', vputwrecid);

									MoveTaskRecord(vputwrecid);

									var getRecordCount;
									nlapiLogExecution('DEBUG','containerLP here',containerLP);
									var filters = new Array();
									filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);
									filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', containerLP);
									filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
									columns[1] = new nlobjSearchColumn('custrecord_lpno');

									var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

									if(searchresults!=null)
									{
										getRecordCount = searchresults.length;
									}
									nlapiLogExecution('DEBUG','getRecordCount',getRecordCount);
									if (parseFloat(getRecordCount) > 1) {

										if (searchresults != null && searchresults.length > 0) {
											nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');
											POarray["custparam_cartno"] = searchresults[0].getValue('custrecord_transport_lp');
											nlapiLogExecution('DEBUG', 'POarray["custparam_cartno"]', POarray["custparam_cartno"]);
											POarray["custparam_lpno"] =  getLPNo;
											POarray["custparam_recordcount"] = searchresults.length;

											if (POarray["custparam_beginlocation"] == null) {
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaysku', 'customdeploy_ebiz_rf_cart_putawaysku_di', false, POarray);
												nlapiLogExecution('DEBUG', 'Back to Putaway SKU', 'Record count is not zero');
												return;
											}
											else {
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
												nlapiLogExecution('DEBUG', 'Back to Putaway confirmation', 'Record count is not zero');
												return;
											}
										}
										else {
											POarray["custparam_cartno"]=request.getParameter('custparam_cartno');
											POarray["custparam_lpno"] = getLPNo;
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											nlapiLogExecution('DEBUG', 'Here the Search Results ', 'Length is null1');
										}
									}
									else {

										var filtersmlp = new Array(); 
										filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartno'));

										var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

										if (SrchRecord != null && SrchRecord.length > 0) {
											var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
											rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'F');
											var recid = nlapiSubmitRecord(rec, false, true);
											nlapiLogExecution('DEBUG', 'cart LP FOUND and Closed',recid);
										}
										else {
											nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');
										}
										POarray["custparam_cartno"]=request.getParameter('custparam_cartno');
										POarray["custparam_lpno"] = getLPNo;
										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaycmplete', 'customdeploy_ebiz_rf_cart_putawaycmplete', false, POarray);
									}
									/*
									 * 	This is to delete the check-in transactions from the inventory record while putaway confirmation. 
									 */	
									//case# 201411604
									
									DeleteInvtRecCreatedforCHKNTask(POId, getLPNo);
								}
								else
									/*response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
								nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');*/
								{
									//POarray["custparam_itemqty"]=parseFloat(expectedQty);
									//response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
									//POarray["custparam_polineitemlp"] = NewLp;
									nlapiLogExecution('DEBUG', 'NewGenLP in post before redirect', POarray["custparam_polineitemlp"]);
									if (itemSubtype.recordType == "serializedinventoryitem" || itemSubtype.recordType == "serializedassemblyitem" || serialFlag == 'T') 
									{
										response.sendRedirect('SUITELET', 'customscript_putawayqtyexp_serial_no', 'customdeploy_putawayqtyexp_serial_no', false, POarray);
										return;
									}
									else
									{
										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
										return;
									}
								}
								nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
							}

						}
						else
						{
							POarray["custparam_error"] = 'REMAINING QUANTITY EXCEEDS ACTUAL QUANTITY';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'EnteredRemainingQty', 'more than excepted');
						}
					}

//					var getLPId = searchresults[0].getId();
//					nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);

////					Load a record into a variable from opentask table for the LP# entered
//					var PORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getLPId);

//					POarray["custparam_lpno"] = PORec.getFieldValue('custrecord_lpno');
//					POarray["custparam_quantity"] = PORec.getFieldValue('custrecord_expe_qty');
//					POarray["custparam_location"] = PORec.getFieldValue('custrecord_actbeginloc');
//					POarray["custparam_item"] = PORec.getFieldValue('custrecord_sku');
//					POarray["custparam_itemDescription"] = PORec.getFieldValue('custrecord_skudesc');
				}

//				if (getQuantity != '') 
//				{
//				response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirm_loc', 'customdeploy_rf_putaway_confirm_loc_di', false, POarray);
//				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
//				}
//				else 
//				{
//				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
//				nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
//				}
			}
		} 
		catch (e) 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'SearchResults ', e);
		}
	}
}

function updateChknTaskQty(getLPNo,containerLP,QuantityEntered,putawayRecId,vBatchNo)
{
	try
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', 1);//1 STATUS.INBOUND.CHECK_IN 
		if(getLPNo!=null&&getLPNo!='')
			filters[1] = new nlobjSearchFilter('custrecord_ebiztask_lpno', null, 'is', getLPNo);
		if(containerLP!=null&&containerLP!='')
			filters[2] = new nlobjSearchFilter('custrecord_ebiztask_transport_lp', null, 'is', containerLP);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, null);
		if(searchresults!=null&&searchresults!="")
		{
			var RecId=searchresults[0].getId();
			var Id=nlapiSubmitField('customrecord_ebiznet_trn_ebiztask', RecId, 'custrecord_ebiztask_act_qty', parseFloat(QuantityEntered).toFixed(4));
			nlapiLogExecution('DEBUG','Chkn rec Id which is updated with new exp qty',Id);
		}
		if(putawayRecId!=null&&putawayRecId!="")
		{
			var fields=new Array();
			fields.push('custrecord_expe_qty');
			fields.push('custrecord_lotnowithquantity');

			var values=new Array();
			values.push(QuantityEntered);
			values.push(vBatchNo + "(" + QuantityEntered + ")");
			var vId=nlapiSubmitField('customrecord_ebiznet_trn_opentask', putawayRecId, fields, values);
			nlapiLogExecution('DEBUG','putawayRecId',vId);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in updateChknTaskQty',exp);	
	}

}

function checkPOOverage(receiptType){
	var poOverage = 0;

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('DEBUG','Out of check PO Overage', poOverage);
	return poOverage;
}

function CheckinLp(ItemId,POIdText,poitem,lineno,pointernalid,poqtyentered,poitemremainingqty,polinepackcode,
		polineitemstatus,polinequantity,polinequantityreceived,itemdescription,itemcube,
		ActualBeginDate,ActualBeginTime,ActualBeginTimeAMPM,WmsLocation,containerLP,NewLp,vBatchNo,POarray)
{
	var NewGenLP="";//added by santosh
	//var POarray=new Array();
	nlapiLogExecution('DEBUG','ItemId',ItemId);
	nlapiLogExecution('DEBUG','POIdText',POIdText);
	nlapiLogExecution('DEBUG','poitem',poitem);
	nlapiLogExecution('DEBUG','pointernalid',pointernalid);
	nlapiLogExecution('DEBUG','poqtyentered',poqtyentered);
	nlapiLogExecution('DEBUG','poitemremainingqty',poitemremainingqty);
	nlapiLogExecution('DEBUG','polinepackcode',polinepackcode);
	nlapiLogExecution('DEBUG','polineitemstatus',polineitemstatus);
	nlapiLogExecution('DEBUG','polinequantity',polinequantity);
	nlapiLogExecution('DEBUG','polinequantityreceived',polinequantityreceived);
	nlapiLogExecution('DEBUG','itemdescription',itemdescription);
	nlapiLogExecution('DEBUG','itemcube',itemcube);
	nlapiLogExecution('DEBUG','ActualBeginDate',ActualBeginDate);
	nlapiLogExecution('DEBUG','ActualBeginTime',ActualBeginTime);
	nlapiLogExecution('DEBUG','ActualBeginTimeAMPM',ActualBeginTimeAMPM);
	nlapiLogExecution('DEBUG','WmsLocation',WmsLocation);
	nlapiLogExecution('DEBUG','vBatchNo',vBatchNo);


	var getPONo = POIdText;
	var getPOItem = poitem;
	var getPOLineNo = lineno;

	var getPOInternalId = pointernalid;
	var getPOQtyEntered = poqtyentered;
	var getPOItemRemainingQty = poitemremainingqty;
	var getPOLinePackCode = polinepackcode;
	var getPOLineItemStatus = polineitemstatus;
	var getPOLineQuantity = polinequantity;
	var getPOLineQuantityReceived = polinequantityreceived;
	var getItemDescription = itemdescription;
	var getItemCube = itemcube;
	var getActualBeginDate = ActualBeginDate;
	var getFetchedItemId =ItemId;
	var getActualBeginTime = ActualBeginTime;
	var getActualBeginTimeAMPM = ActualBeginTimeAMPM;

	var getLineCount = 1;
	var getBaseUOM = 'EACH';
	var getBinLocation = "";

	POarray["custparam_batchno"] = vBatchNo;
	POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
	POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
	POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
	POarray["custparam_lastdate"] = request.getParameter('hdnLastAvlDate');
	POarray["custparam_fifodate"] = request.getParameter('hdnFifoDate');
	POarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');
	POarray["custparam_cartlp"] = request.getParameter('custparam_cartlp');



	var POfilters = new Array();
	nlapiLogExecution('DEBUG', 'getPOInternalId', pointernalid);
	POfilters[0] = new nlobjSearchFilter('name', null, 'is', pointernalid);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, POfilters, null);

	var getPOReceiptNo = '';

	if (searchresults != null && searchresults.length > 0) {
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			getPOReceiptNo = searchresults[i].getValue('custrecord_ebiz_poreceipt_receiptno');
		}

		nlapiLogExecution('DEBUG', 'PO Receipt No', getPOReceiptNo);
	}

	// case no start 20126923
	// New cart lp should be updated in Transport LP field

	//code added by santosh on 09Aug12
	//case# 20149820 starts (commented because from lp need to update from LP with system generated pallet LP)
	/*if(NewLp!=null && NewLp!="")
	{
		NewGenLP=NewLp;

	}
	else
	{*/
	//case# 20149820 ends 
	var getsysItemLP = GetMaxLPNo(1, 1,WmsLocation);
	nlapiLogExecution('DEBUG', 'getsysItemLP LP now added iis', getsysItemLP);	

	var LPReturnValue = "";
	nlapiLogExecution('DEBUG', 'getsysItemLP', getsysItemLP);
	LPReturnValue = true;
	nlapiLogExecution('DEBUG', 'LP Return Value new', LPReturnValue);
	var lpExists = 'N';

	/*//LP Checking in masterlp record starts
	if (LPReturnValue == true) {
		try {
			nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
			var filtersmlp = new Array();
			filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getsysItemLP);

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

			if (SrchRecord != null && SrchRecord.length > 0) {
				nlapiLogExecution('DEBUG', 'LP FOUND');

				lpExists = 'Y';
			}
			else {
				nlapiLogExecution('DEBUG', 'LP NOT FOUND');
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
				customrecord.setFieldValue('name', getsysItemLP);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getsysItemLP);
				var rec = nlapiSubmitRecord(customrecord, false, true);
			}
		} 
		catch (e) {
			nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
		}
	}*/
	getsysItemLP=CheckForLPExist(getsysItemLP,WmsLocation);
	nlapiLogExecution('DEBUG', 'getsysItemLP', getsysItemLP);
	NewGenLP=getsysItemLP;
	//}



	//end of the code by 09Aug12

	//case# 20148593 starts
	var stagelocid = getStagingLocation(WmsLocation);

	var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
	nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');
	var accountNumber = "";

	invtRec.setFieldValue('name', pointernalid);
	invtRec.setFieldValue('custrecord_ebiz_inv_binloc', stagelocid);
	invtRec.setFieldValue('custrecord_ebiz_inv_lp', NewGenLP);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', polineitemstatus);

	if(polinepackcode==null || polinepackcode=='')
		polinepackcode=1;

	invtRec.setFieldValue('custrecord_ebiz_inv_packcode', polinepackcode);
	invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(poqtyentered).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_inv_loc', WmsLocation);
	invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);

	invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');

	invtRec.setFieldValue('custrecord_wms_inv_status_flag','17');//17=FLAG.INVENTORY.INBOUND
	invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
	invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(poqtyentered).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdescription);
	invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
	invtRec.setFieldValue('custrecord_invttasktype', 1);
//	if(uomlevel!=null && uomlevel!='')
	//	invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);



	if(vBatchNo!=null && vBatchNo!=''&& vBatchNo!='null')
	{
		nlapiLogExecution('DEBUG', 'batchno into if', vBatchNo);
		try
		{

			var filterBatch=new Array();
			filterBatch[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',vBatchNo);
			//if(itemid!=null&&itemid!="") // Case# 20149237
			if(ItemId!=null && ItemId!="")
				filterBatch[1]=new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',ItemId);
			var columnBatch=new Array();
			columnBatch[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			columnBatch[1]=new nlobjSearchColumn('custrecord_ebizfifodate');
			var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatch,columnBatch);
			var BatchID=rec[0].getId();

			invtRec.setFieldValue('custrecord_ebiz_inv_lot', BatchID);
			invtRec.setFieldValue('custrecord_ebiz_expdate', rec[0].getValue('custrecord_ebizexpirydate'));
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', rec[0].getValue('custrecord_ebizfifodate'));
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception', exp);
		}
	}


	if(pointernalid!=null && pointernalid!="")
		invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointernalid);

	var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
	nlapiLogExecution('ERROR','invtRecordId',invtRecordId);

	//case# 20148593 ends

	if(lpExists != 'Y')
	{
		//POarray["custparam_polineitemlp"] = getsysItemLP;
		POarray["custparam_polineitemlp"] = NewGenLP;//added by santosh on 09Aug12
		nlapiLogExecution('DEBUG', 'custparam_polineitemlp', POarray["custparam_polineitemlp"]);
		POarray["custparam_poreceiptno"] = getPOReceiptNo;
		var TotalItemCube = parseFloat(itemcube) * parseFloat(poqtyentered);
		nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );

		var itemSubtype = nlapiLookupField('item', ItemId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
		POarray["custparam_recordtype"] = itemSubtype.recordType;
		var batchflag=itemSubtype.custitem_ebizbatchlot;
		nlapiLogExecution('DEBUG', 'itemSubtype.recordType =', itemSubtype.recordType);
		nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
		nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);



		var priorityPutawayLocnArr = priorityPutaway(ItemId,poqtyentered,WmsLocation,polineitemstatus);
		var priorityRemainingQty = priorityPutawayLocnArr[0];
		var priorityQty = priorityPutawayLocnArr[1];
		var priorityLocnID = priorityPutawayLocnArr[2];

		nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityRemainingQty', priorityRemainingQty);
		nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityQty', priorityQty);
		nlapiLogExecution('DEBUG', 'checkInPOSTRequest:POQtyEntered', poqtyentered);
		nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityLocnID', priorityLocnID);
		var remainingCube="";
		if(priorityQty <= poqtyentered){	
			var filters2 = new Array();
			filters2.push(new nlobjSearchFilter('internalid', null, 'is', ItemId));
			var columns2 = new Array();
			columns2[0] = new nlobjSearchColumn('custitem_item_family');
			columns2[1] = new nlobjSearchColumn('custitem_item_group');
			columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
			columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
			columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
			columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
			columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

			var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

			var filters3=new Array();
			filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			var columns3=new Array();
			columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
			columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
			columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
			columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
			columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
			columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
			columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
			columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
			columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
			columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
			columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
			columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
			columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
			columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
			columns3[14] = new nlobjSearchColumn('formulanumeric');
			columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
			columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
			//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
			columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
			// Upto here on 29OCT2013 - Case # 20124515
			columns3[14].setSort();

			var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);



			/*var getBeginLocation = generatePutawayLocation(ItemId, polinepackcode,
				polineitemstatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType,WmsLocation);
		nlapiLogExecution('DEBUG','getBeginLocation',getBeginLocation);*/


			nlapiLogExecution('DEBUG', 'Time Stamp at the start of GetPutawayLocationNew',TimeStampinSec());

			var getBeginLocation = GetPutawayLocationNew(ItemId, polinepackcode, polineitemstatus, getBaseUOM, 
					parseFloat(TotalItemCube),itemSubtype.recordType,WmsLocation,ItemInfoResults,putrulesearchresults);

			nlapiLogExecution('DEBUG', 'Time Stamp at the end of GetPutawayLocationNew',TimeStampinSec());

			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage 2 ',context.getRemainingUsage());
			nlapiLogExecution('DEBUG', 'Begin Location', getBeginLocation);

			var vLocationname="";
			if (getBeginLocation != null && getBeginLocation != '') {
				var getBeginLocationId = getBeginLocation[2];
				vLocationname= getBeginLocation[0];
				nlapiLogExecution('DEBUG', 'Begin Location Id', getBeginLocationId);
			}
			else {
				var getBeginLocationId = "";
				nlapiLogExecution('DEBUG', 'Begin Location Id is null', getBeginLocationId);
			}

			if(getBeginLocation!=null && getBeginLocation != '')
				remainingCube = getBeginLocation[1];

			nlapiLogExecution('DEBUG', 'remainingCube', remainingCube);
			if (getBeginLocation != null && getBeginLocation != '') {
				putmethod = getBeginLocation[9];
				putrule = getBeginLocation[10];
			}
			else
			{
				putmethod = "";
				putrule = "";
			}
		}
		var getActualEndDate = DateStamp();
		nlapiLogExecution('DEBUG', 'getActualEndDate', getActualEndDate);
		var getActualEndTime = TimeStamp();
		nlapiLogExecution('DEBUG', 'getActualEndTime', getActualEndTime);

		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','Remaining usage 3 ',context.getRemainingUsage());
		var trantype= nlapiLookupField('transaction',pointernalid,'recordType');
		TrnLineUpdation(trantype, 'CHKN', POIdText,pointernalid,lineno, ItemId, 
				polinequantity, poqtyentered,"",polineitemstatus);


		if (priorityQty >= poqtyentered){			//(priorityQty != 0){
			taskType = "2"; // PUTW
			wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
			getBeginLocationId = priorityLocnID;
			var LocRemCube = GeteLocCube(getBeginLocationId);

			nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
			if (parseFloat(LocRemCube) > parseFloat(TotalItemCube)) 
				remainingCube = parseFloat(LocRemCube) - parseFloat(TotalItemCube);
		}

		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','Remaining usage 4 ',context.getRemainingUsage());
		var vValid = custChknPutwRecCreation(getPOInternalId, getPONo, getPOQtyEntered, getPOLineNo, getPOItemRemainingQty, getLineCount, getFetchedItemId, 
				getPOItem, getItemDescription, getPOLineItemStatus, getPOLinePackCode, getBaseUOM, NewGenLP, getBinLocation, 
				getPOLineQuantityReceived, getActualEndDate, getActualEndTime, getPOReceiptNo, "", getActualBeginDate, 
				getActualBeginTime, getActualBeginTimeAMPM, getBeginLocationId, POarray["custparam_batchno"], 
				POarray["custparam_mfgdate"], POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
				POarray["custparam_lastdate"], POarray["custparam_fifodate"], POarray["custparam_fifocode"], 
				itemSubtype.recordType, containerLP, WmsLocation,batchflag,putmethod,putrule);
		var context = nlapiGetContext();
		nlapiLogExecution('DEBUG','Remaining usage 5 ',context.getRemainingUsage());
		nlapiLogExecution('DEBUG', 'vValid', vValid);
		if(vValid==false)
		{
			nlapiLogExecution('DEBUG','Exception in generating Locations');
		}
		else
		{
			/*	* This is to insert a record in inventory custom record.
			 * The data that is to be inserted is PO Internal Id, Item, Item Status, Pack Code, Quantity, LP#
			 * The parameters list is: 
			 * pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, warehouse location
			 * */

			TrnLineUpdation(trantype, 'ASPW', POIdText,pointernalid,lineno, ItemId, 
					polinequantity, poqtyentered,"",polineitemstatus);

			updatePutQtyExc(pointernalid,lineno);
			if(getBeginLocationId!=null && getBeginLocationId!='')
			{
				UpdateLocCube(getBeginLocationId, remainingCube);
			}
		}
	}
	else
	{
		nlapiLogExecution('DEBUG','exception :LP ALEADY Exist.so chkn and putaway task records are failed to create');
	}
}

function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, 
		ItemDesc, ItemStatus, PackCode, BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, 
		poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, ActualBeginTimeAMPM, BeginLocationId, 
		BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, itemrectype, cartlp, 
		WHLocation,batchflag,putmethod,putrule)
{
	var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
	var columns = nlapiLookupField('item', ItemId, fields);
	var ItemType = columns.recordType;					
	var batchflg = columns.custitem_ebizbatchlot;
	var itemfamId= columns.custitem_item_family;
	var itemgrpId= columns.custitem_item_group;
	var getlotnoid="";

//	if(ItemStatus==null || ItemStatus=='')
//	ItemStatus='12';

//	nlapiLogExecution('DEBUG', 'putLotBatch', putLotBatch);

	var vValid=true;
	nlapiLogExecution('DEBUG', 'Into custChknPutwRecCreation', 'custChknPutwRecCreation');
	var now = new Date();
	var stagelocid, docklocid;
	
	try
	{
		stagelocid = GetInboundStageLocation(WHLocation, null, 'INB');
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in  GetInboundStageLocation', exp);
	}

	if(stagelocid==null || stagelocid=='')
		stagelocid = getStagingLocation(WHLocation);
	
	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);
	if(stagelocid!=null && stagelocid!='')
		docklocid = stagelocid;
	
	if(docklocid==null || docklocid=='')
	docklocid = getDockLocation(WHLocation);
	nlapiLogExecution('DEBUG', 'Dock Location', docklocid);

	if (BeginLocationId != null && BeginLocationId != '' ) //Creating custom record with CHKN and PUTW task,
	{
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);
		nlapiLogExecution('DEBUG', 'Submitting CHKN record', 'TRN_OPENTASK');

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		//commit the record to NetSuite
		var reccheckid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		nlapiLogExecution('DEBUG', 'BeginLocationId', BeginLocationId);

		//create the openTask record With PUTW Task Type
		if(BeginLocationId==null || BeginLocationId=="")
			return false;
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)

		nlapiLogExecution('DEBUG', 'Fetched Begin Location', BeginLocation);

		putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//For eBiznet Receipt Number .
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

		putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//putwrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		if (BeginLocationId != "") {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 2);
		}
		else {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		}
		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);
		try
		{
			MoveTaskRecord(reccheckid);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in  MoveTaskRecord');
		}
		nlapiLogExecution('DEBUG', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');
		return true;
	}//end if binloc is not null
	else {
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);

		nlapiLogExecution('DEBUG', 'custrecord_ebiz_sku_no tesing', 'ItemId');

		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		customrecord.setFieldValue('custrecord_ebiz_order_no', po);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{

			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var reccheckid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);
		putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" ||itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");

		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		/*try
		{
			MoveTaskRecord(reccheckid);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in  MoveTaskRecord');
		}*/
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		return false;
	}
}
function getStagingLocation(WHLocation){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));
	var RoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR', 'RoleLocation', RoleLocation);
	if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
	{
//		stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', RoleLocation));
		if(WHLocation!=null && WHLocation!='')
			stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', WHLocation));
	}
	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}
function getDockLocation(site){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	if(site!=null && site!="")
		dockFilters.push(new nlobjSearchFilter('custrecord_ebizsiteloc', 'custrecord_inboundlocgroupid', 'anyof', ["NONE",site]));

	dockFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}

function updateTrnOrderLine(poid,lineno,expQty)
{
	try{
		nlapiLogExecution('DEBUG','poid',poid);
		nlapiLogExecution('DEBUG','lineno',lineno);
		nlapiLogExecution('DEBUG','expQty',expQty);
		var chkQty=0;
		var PwtQty=0;
		var RecId;	
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poid));
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
		columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');

		var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);

		if(searchResults!=null&&searchResults!="")
		{
			var chkQtynew=0;
			var PwtQtynew=0;

			chkQty=searchResults[0].getValue('custrecord_orderlinedetails_checkin_qty');
			PwtQty=searchResults[0].getValue('custrecord_orderlinedetails_putgen_qty');
			RecId=searchResults[0].getId();
			nlapiLogExecution('DEBUG','chkQty',chkQty);
			nlapiLogExecution('DEBUG','PwtQty',PwtQty);
			/*			// Case# 20127917 starts
			chkQty=parseFloat(chkQty)-parseFloat(expQty);
			PwtQty=parseFloat(PwtQty)-parseFloat(expQty);
			// Case# 20127917 ends */
			//case# 20148587 
			chkQtynew=parseFloat(chkQty)-parseFloat(expQty);
			PwtQtynew=parseFloat(PwtQty)-parseFloat(expQty);
			//nlapiLogExecution('DEBUG','chkQty AFTER',chkQty);
			//nlapiLogExecution('DEBUG','PwtQty AFTER',PwtQty);
			nlapiLogExecution('DEBUG','chkQty AFTER',chkQtynew);
			nlapiLogExecution('DEBUG','PwtQty AFTER',PwtQtynew);
			var Fields=new Array();
			Fields[0]='custrecord_orderlinedetails_checkin_qty';
			Fields[1]='custrecord_orderlinedetails_putgen_qty';
			Fields[2]='custrecord_orderlinedetails_putexcep_qty';

			var Values=new Array();
			Values[0]=parseFloat(chkQtynew).toFixed(4);
			Values[1]=parseFloat(PwtQtynew).toFixed(4);
			Values[2]=parseFloat(expQty).toFixed(4);
			var ID=nlapiSubmitField('customrecord_ebiznet_order_line_details', RecId, Fields, Values);
			nlapiLogExecution('DEBUG','Trn Order Line RecID',ID);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in UpdateTrnOrderLine function',exp);
	}
}

function updatePutQtyExc(poid,lineno)
{
	try{
		nlapiLogExecution('DEBUG','poid',poid);
		nlapiLogExecution('DEBUG','lineno',lineno);


		var RecId;	
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poid));
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));

		var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, null);

		if(searchResults!=null&&searchResults!="")
		{
			RecId=searchResults[0].getId();
			var ID=nlapiSubmitField('customrecord_ebiznet_order_line_details', RecId, 'custrecord_orderlinedetails_putexcep_qty', '');
			nlapiLogExecution('DEBUG','Trn Order Line RecID',ID);
		}
	}
	catch(exp)
	{

		nlapiLogExecution('DEBUG','Exception in UpdateTrnOrderLine function',exp);
	}
}

function CheckForLPExist(getsysItemLP,WmsLocation)
{
	try {
		nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getsysItemLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) {
			nlapiLogExecution('DEBUG', 'LP FOUND');

			getsysItemLP = GetMaxLPNo(1, 1,WmsLocation);
			nlapiLogExecution('DEBUG', 'getsysItemLP LP now added iis', getsysItemLP);	
			CheckForLPExist(getsysItemLP,WmsLocation);
		}
		else {
			nlapiLogExecution('DEBUG', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', getsysItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getsysItemLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
		}
	} 
	catch (e) {
		nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
	}
	return getsysItemLP;
}


function MastLPExists(manualCartLP)
{
	nlapiLogExecution('DEBUG','in MastLPExists');
	var message="";
	if(ebiznet_LPRange_CL_withLPType(manualCartLP,'2','4'))// for User Defined  with Cart LPType
	{
		
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
		customrecord.setFieldValue('name', manualCartLP);
		customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', manualCartLP);
		var rec = nlapiSubmitRecord(customrecord, false, true);
		return message;
	}
	else
	{
		message="Cart LP# is outof range";
		return message;
	}
	
	var filters =new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', manualCartLP));
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_cart_closeflag', null, 'is', 'T'));
	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_cart_closeflag'));
	// To get the data from Master LP based on selection criteria
	searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	if(searchresults!= null && searchresults.length>0)
	{
		if(searchresults[0].getValue('custrecord_ebiz_cart_closeflag')=='T')
		{
			message="Cart LP# already closed, Please enter another Cart LP#";
			nlapiLogExecution('DEBUG',"message ",message );

		}
		return message="";
//		else
//		{
//		message="Cart LP# already exists, Please enter another Cart LP#";
//		nlapiLogExecution('DEBUG',"message ",message );
//		return message;
//		}
	}

}


/**
 * @param pointid
 * @param lp
 */
function DeleteInvtRecCreatedforCHKNTask(pointid, lp)
{
	nlapiLogExecution('DEBUG','Inside DeleteInvtRecCreatedforCHKNTask ','Funciton');
	var Ifilters = new Array();
	//Ifilters.push(new nlobjSearchFilter('name', null, 'is', pointid));
	Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [1,17]));
	Ifilters.push(new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [1]));
	Ifilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp));

	var invtId="";
	var invtType="";
	var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (serchInvtRec) 
	{
		for (var s = 0; s < serchInvtRec.length; s++) {
			nlapiLogExecution('DEBUG','Inside loop','loop');
			var searchresult = serchInvtRec[ s ];
			nlapiLogExecution('DEBUG','need to print this','print');
			invtId = serchInvtRec[s].getId();
			invtType= serchInvtRec[s].getRecordType();
			nlapiLogExecution('DEBUG','CHKN INVT Id',invtId);
			nlapiLogExecution('DEBUG','CHKN invtType ',invtType);
			nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
			nlapiLogExecution('DEBUG','Invt Deleted record Id',invtId);

		}
	}
}





function GetInboundStageLocation(vSite, vCompany, stgDirection){

	nlapiLogExecution('DEBUG', 'Into GetInboundStageLocation');

	var str = 'vSite. = ' + vSite + '<br>';
	str = str + 'vCompany. = ' + vCompany + '<br>';	
	str = str + 'stgDirection. = ' + stgDirection + '<br>';

	nlapiLogExecution('DEBUG', 'GetInboundStageLocation Parameters', str);

	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}


	var columns = new Array();
	var filters = new Array();

	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}

	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < searchresults.length; i++) {

			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			var vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');

			var str = 'Stage Location ID. = ' + vstgLocation + '<br>';
			str = str + 'Stage Location Text. = ' + vstgLocationText + '<br>';	

			nlapiLogExecution('DEBUG', 'Stage Location Details', str);

			if (vstgLocation != null && vstgLocation != "") {
				nlapiLogExecution('DEBUG', 'Out of GetInboundStageLocation',vstgLocation);
				return vstgLocation;
			}

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('DEBUG', 'LocGroup ', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				filtersLocGroup.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('DEBUG', 'searchresultsloc ', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {

					var vLocid=searchresultsloc[0].getId();
					nlapiLogExecution('DEBUG', 'Out of GetInboundStageLocation',vLocid);
					return vLocid;

				}				
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of GetInboundStageLocation');

}