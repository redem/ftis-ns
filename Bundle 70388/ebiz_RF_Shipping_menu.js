/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_RF_Shipping_menu.js,v $
 *     	   $Revision: 1.1.4.6.4.3.4.10 $
 *     	   $Date: 2014/06/13 11:01:55 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Shipping_menu.js,v $
 * Revision 1.1.4.6.4.3.4.10  2014/06/13 11:01:55  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.4.6.4.3.4.9  2014/06/12 15:29:32  skavuri
 * Case # 20148880 (LinkButton functionality added to options in RF Screens)
 *
 * Revision 1.1.4.6.4.3.4.8  2014/05/30 00:41:06  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.4.6.4.3.4.7  2013/09/06 07:34:33  rmukkera
 * Case# 20124261
 *
 * Revision 1.1.4.6.4.3.4.6  2013/09/03 15:42:12  skreddy
 * Case# 20124195
 * standard bundle issue fix
 *
 * Revision 1.1.4.6.4.3.4.5  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.4.6.4.3.4.4  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.4.6.4.3.4.3  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.4.6.4.3.4.2  2013/03/19 11:46:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.4.6.4.3.4.1  2013/03/05 14:47:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.1.4.6.4.3  2012/12/24 13:22:14  schepuri
 * CASE201112/CR201113/LOG201121
 * added new screen for multiple sites
 *
 * Revision 1.1.4.6.4.2  2012/11/28 03:16:47  kavitha
 * CASE201112/CR201113/LOG201121
 * Update SRR# CR - GS USA
 *
 * Revision 1.1.4.6.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.1.4.6  2012/09/14 09:23:22  grao
 * no message
 *
 * Revision 1.1.4.2  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.1.4.1  2012/02/22 13:03:40  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.1  2011/09/23 16:22:25  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/08/26 11:16:15  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/08/26 10:50:49  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/08/25 15:01:14  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/07/21 07:01:02  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
//Common Variables

function RFShippingMenu(request, response){
	if (request.getMethod() == 'GET') {
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "ENV&#205;O MEN&#218;";
			st1 = "CONSTRUIR LA UNIDAD DEL BUQUE";
			st2 = "TRAILER DE CARGA / SALIDA";
			st3 = "ENTRAR SELECCI&#211;N";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";
			st6 = "ACTUALIZACI�N SRR#";
			
		}
		else
		{
			st0 = "SHIPPING MENU";
			st1 = "BUILD SHIP UNIT";
			st2 = "TRAILER LOAD/DEPART";
			st3 = "ENTER SELECTION";
			st4 = "SEND";
			st5 = "PREV";
			st6 = "UPDATE SRR#";
		}
		//Case# 20148880 (LinkButton Functionality)
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_rf_shiplp_location', 'customdeploy_rf_shiplp_location');
		var linkURL_1 = checkInURL_1; 
		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_rf_loadunloadtrlr', 'customdeploy_rf_loadunloadtrlr_di');
		var linkURL_2 = checkInURL_2; 
		var checkInURL_3 = nlapiResolveURL('SUITELET', 'customscript_rf_shipping_updatesrr', 'customdeploy_rf_shipping_updatesrr_di');
		var linkURL_3 = checkInURL_3; 
		
		var functionkeyHtml=getFunctionkeyScript('_rfmainmenu'); 
		var html = "<html><head><title>" + st0 +"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";		
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfmainmenu' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1. " + st1;
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 2. " + st2;
		html = html + "				<td align = 'left'> 2. <a href='" + linkURL_2 + "' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> "; 
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 3. " + st6;
		html = html + "				<td align = 'left'> 3. <a href='" + linkURL_3 + "' style='text-decoration: none'>" + st6 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> "; 
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + "<input id='cmdSend' type='submit' value='ENT'/>";
		html = html + "				"	+ st5 + "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var optedField = request.getParameter('selectoption');

		var optedEvent = request.getParameter('cmdPrevious');

//		if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to Main Menu.
		var POarray = new Array();  
		
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
		//case start 
		POarray["custparam_screenno"] = 'SHPMENU';
		POarray["custparam_error"] = "INVALID OPTION";
//end
		
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);
		}
		else 
		{
			//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
			//            if (optedEvent != '' && optedEvent != null) {
			if (optedField != null) 
			{
				//var POarray = new Array();
				POarray["custparam_option"] = optedField;
				nlapiLogExecution('DEBUG', 'optedField', optedField);
			}

			if (optedField == '1') 
			{
				//response.sendRedirect('SUITELET', 'customscript_rf_shipping_shiplp', 'customdeploy_rf_shipping_shiplp_di', false, POarray);
				response.sendRedirect('SUITELET', 'customscript_rf_shiplp_location', 'customdeploy_rf_shiplp_location', false, POarray);
			}
			else 
				if (optedField == '2') 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_loadunloadtrlr', 'customdeploy_rf_loadunloadtrlr_di', false, POarray);
				}
				else
					if (optedField == '3') 
					{
						response.sendRedirect('SUITELET', 'customscript_rf_shipping_updatesrr', 'customdeploy_rf_shipping_updatesrr_di', false, null);
					}
					else
						{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return;
						}
			//
		}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
